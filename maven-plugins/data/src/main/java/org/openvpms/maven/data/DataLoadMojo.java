/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.data;

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.openvpms.tools.data.loader.StaxArchetypeDataLoader;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;


/**
 * Loads data using the {@link StaxArchetypeDataLoader}.
 */
@Mojo(name = "load", requiresDependencyResolution = ResolutionScope.TEST)
public class DataLoadMojo extends AbstractDataMojo {

    /**
     * The batch size, used to reduce database access.
     */
    @Parameter(defaultValue = "1000")
    private int batchSize;

    /**
     * Loads data from the specified directory.
     *
     * @param loader the archetype data loader to use
     * @throws IOException        for any I/O error
     * @throws XMLStreamException if a file cannot be read
     */
    protected void doExecute(StaxArchetypeDataLoader loader) throws IOException, XMLStreamException {
        loader.setVerbose(isVerbose());
        loader.setBatchSize(batchSize);
        loader.load(getDir().getPath());
    }
}