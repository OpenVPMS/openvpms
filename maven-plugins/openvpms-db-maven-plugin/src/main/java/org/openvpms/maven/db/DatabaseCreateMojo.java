/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.db;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.db.service.impl.DatabaseAdminServiceImpl;
import org.openvpms.maven.archetype.AbstractHibernateMojo;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Mojo to create the OpenVPMS database.
 *
 * @author Tim Anderson
 */
@Mojo(name = "create", requiresDependencyResolution = ResolutionScope.TEST)
public class DatabaseCreateMojo extends AbstractHibernateMojo {

    /**
     * The JDBC admin user name.
     */
    @Parameter(required = true)
    private String adminUsername;

    /**
     * The JDBC admin password.
     */
    @Parameter(required = true)
    private String adminPassword;

    /**
     * The reporting username for read-only access.
     */
    @Parameter(required = true)
    private String reportingUsername;

    /**
     * The reporting password.
     */
    @Parameter(required = true)
    private String reportingPassword;

    /**
     * The checksum path.
     */
    @Parameter(required = true)
    private String checksum;

    /**
     * If {@code true}, skips execution.
     */
    @Parameter
    private boolean skip;

    /**
     * Perform whatever build-process behavior this <code>Mojo</code> implements.
     * <br/>
     * This implementation sets the context class loader to be that of the project's test class path,
     * before invoking {@link #doExecute()}.
     *
     * @throws MojoExecutionException if an unexpected problem occurs.
     * @throws MojoFailureException   an expected problem (such as a compilation failure) occurs.
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!skip) {
            super.execute();
        } else {
            getLog().info("Plugin is skipped");
        }
    }

    /**
     * Execute the plugin.
     *
     * @throws MojoExecutionException if an unexpected problem occurs.
     */
    @Override
    protected void doExecute() throws MojoExecutionException {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(getDriver());
        dataSource.setUsername(adminUsername);
        dataSource.setPassword(adminPassword);
        dataSource.setUrl(getUrl());

        // specify null checksums to force an update after table creation
        Checksums checksums = new Checksums() {
            @Override
            public Integer getArchetypeChecksum() {
                return null;
            }

            @Override
            public Integer getPluginChecksum() {
                return null;
            }
        };
        Credentials adminUser = new Credentials(adminUsername, adminPassword);
        Credentials user = new Credentials(getUsername(), getPassword());
        Credentials reportingUser = new Credentials(reportingUsername, reportingPassword);
        DatabaseAdminService tool = new DatabaseAdminServiceImpl(getDriver(), getUrl(), adminUser, checksums);

        try (Connection connection = dataSource.getConnection()) {
            DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, true);
            Schema<?> schema = dbSupport.getSchema(tool.getSchemaName());
            if (schema.exists()) {
                getLog().info("Dropping database " + tool.getSchemaName());
                schema.drop();
            }
        } catch (SQLException exception) {
            throw new MojoExecutionException("Failed to drop database " + tool.getSchemaName(), exception);
        }
        try {
            getLog().info("Creating database " + tool.getSchemaName());
            tool.create(user, reportingUser, "localhost", true);
        } catch (SQLException exception) {
            throw new MojoExecutionException("Failed to create database " + tool.getSchemaName(), exception);
        }
    }

}
