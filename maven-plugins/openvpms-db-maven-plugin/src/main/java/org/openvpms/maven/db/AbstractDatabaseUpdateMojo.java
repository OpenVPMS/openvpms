/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.db;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.openvpms.db.service.Checksums;
import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.db.service.impl.ArchetypeChecksumImpl;
import org.openvpms.db.service.impl.ChecksumsImpl;
import org.openvpms.db.service.impl.DatabaseAdminServiceImpl;
import org.openvpms.maven.archetype.AbstractArchetypeLoadMojo;

import java.sql.SQLException;

/**
 * Base class for database update Maven plugins.
 *
 * @author Tim Anderson
 */
public class AbstractDatabaseUpdateMojo extends AbstractArchetypeLoadMojo {

    /**
     * The JDBC admin user name.
     */
    @Parameter(required = true)
    private String adminUsername;

    /**
     * The JDBC admin password.
     */
    @Parameter(required = true)
    private String adminPassword;

    /**
     * The checksum path.
     */
    @Parameter(required = true)
    private String checksum;

    /**
     * Execute the plugin.
     *
     * @throws MojoExecutionException if an unexpected problem occurs.
     */
    @Override
    protected void doExecute() throws MojoExecutionException {
        Checksums checksums = new ChecksumsImpl() {
            @Override
            public Integer getArchetypeChecksum() {
                return new ArchetypeChecksumImpl(checksum).getChecksum();
            }
        };

        Credentials adminUser = new Credentials(adminUsername, adminPassword);
        DatabaseAdminService tool = new DatabaseAdminServiceImpl(getDriver(), getUrl(), adminUser, checksums, null);

        try {
            tool.update(() -> {
                try {
                    loadArchetypes();
                } catch (MojoExecutionException exception) {
                    throw new IllegalStateException(exception.getMessage(), exception);
                }
            }, () -> {
            });
        } catch (SQLException exception) {
            throw new MojoExecutionException("Failed to update database " + tool.getSchemaName(), exception);
        }
    }
}
