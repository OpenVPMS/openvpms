/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.archetype;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.tools.archetype.loader.ArchetypeLoader;
import org.springframework.context.ApplicationContext;

import java.io.File;

/**
 * Base class for mojos that load archetypes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractArchetypeLoadMojo extends AbstractHibernateMojo {

    /**
     * The directory to load.
     */
    @Parameter(required = true)
    private File dir;

    /**
     * The assertion types file. If not specified, defaults to {@code dir/assertionTypes.xml}
     */
    @Parameter
    private File assertionTypes;

    /**
     * Determines if verbose logging is enabled.
     */
    @Parameter
    private boolean verbose;

    /**
     * Loads archetypes.
     *
     * @throws MojoExecutionException for any error
     */
    protected void loadArchetypes() throws MojoExecutionException {
        if (dir == null || !dir.exists()) {
            throw new MojoExecutionException("Directory not found: " + dir);
        }
        if (!dir.isDirectory()) {
            throw new MojoExecutionException("Not a directory: " + dir);
        }
        ApplicationContext context = getContext();
        IArchetypeService service = context.getBean("archetypeService", IArchetypeService.class);
        ArchetypeLoader loader = ArchetypeLoader.newBootstrapLoader(service);
        loader.setVerbose(verbose);

        File mappingFile = (assertionTypes != null) ? assertionTypes : new File(dir, "assertionTypes.xml");
        try {
            if (mappingFile.exists()) {
                loader.loadAssertions(mappingFile.getPath());
            }
            loader.loadArchetypes(dir.getPath(), true);
        } catch (Exception throwable) {
            throw new MojoExecutionException("Failed to load archetypes", throwable);
        }
    }
}