/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.archetype;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.openvpms.tools.archetype.loader.ArchetypeLoader;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;


/**
 * Plugin to load archetypes using {@link ArchetypeLoader}.
 */
@Mojo(name = "load", requiresDependencyResolution = ResolutionScope.TEST)
public class ArchetypeLoadMojo extends AbstractArchetypeLoadMojo {

    /**
     * If {@code true}, skips execution.
     */
    @Parameter
    private boolean skip;

    /**
     * Executes the archetype load, unless, execution is skipped.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     * @throws MojoFailureException   if an expected problem (such as a compilation failure) occurs
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!skip) {
            super.execute();
        } else {
            getLog().info("Archetype load is skipped");
        }
    }

    /**
     * Execute the plugin.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     */
    protected void doExecute() throws MojoExecutionException {
        PlatformTransactionManager mgr = getContext().getBean(PlatformTransactionManager.class);
        TransactionStatus status = mgr.getTransaction(new DefaultTransactionDefinition());
        try {
            loadArchetypes();
            mgr.commit(status);
        } catch (Exception throwable) {
            mgr.rollback(status);
            throw new MojoExecutionException("Failed to load archetypes", throwable);
        }
    }
}
