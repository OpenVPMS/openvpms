/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.service;

import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.Messages;
import org.openvpms.sms.message.OutboundMessage;

/**
 * Sends SMS messages using an {@link SMSProvider}.
 * <p/>
 * If a provider is registered but not available, messages will be queued until they can be sent,
 * or the message expires.
 *
 * @author Tim Anderson
 */
public interface SMSService extends Messages {

    /**
     * Determines if SMS support is enabled.
     *
     * @return {@code true} if SMS support is enabled, otherwise {@code false}
     */
    boolean isEnabled();

    /**
     * Returns the number of message parts supported by the SMS provider.
     *
     * @return the number of parts
     */
    int getMaxParts();

    /**
     * Sends a message.
     * <p/>
     * The message will be sent using an {@link SMSProvider}.
     * <p/>
     * If it is not available, it will be periodically retried until sent, or it expires.
     *
     * @param message the message to send
     * @throws SMSException if the message cannot be sent due to an unrecoverable error
     */
    void send(OutboundMessage message);

}
