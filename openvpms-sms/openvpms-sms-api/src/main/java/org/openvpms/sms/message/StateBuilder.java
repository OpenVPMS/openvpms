/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.message.OutboundMessage.Status;

/**
 * Updates the state of an {@link OutboundMessage}.
 *
 * @author Tim Anderson
 */
public interface StateBuilder {

    /**
     * Sets the message identifier, issued by the SMS provider.
     * <p>
     * A message can have a single identifier issued by a provider. To avoid duplicates, each SMS provider must
     * use a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return this
     */
    StateBuilder providerId(String archetype, String id);

    /**
     * Sets the message status.
     * <p/>
     * This clears any status message.
     *
     * @param status the message status
     * @return this
     */
    StateBuilder status(Status status);

    /**
     * Sets the message status, along with any status message from the SMS provider.
     *
     * @param status        the status
     * @param statusMessage the status message. May be {@code null}
     * @return this
     */
    StateBuilder status(Status status, String statusMessage);

    /**
     * Returns a builder to add a reply.
     *
     * @return the reply builder
     */
    ReplyBuilder reply();

    /**
     * Builds the message.
     *
     * @return the message
     * @throws DuplicateSMSException if the message has a duplicate identifier
     */
    OutboundMessage build();
}
