/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

/**
 * Builder for {@link InboundMessage} instances.
 *
 * @author Tim Anderson
 */
public interface ReplyBuilder extends ReceivedMessageBuilder<ReplyBuilder> {

    /**
     * Builds the reply, and returns the state builder.
     * <p/>
     * This can be used when performing multiple changes to a message.
     *
     * @return the message state builder
     */
    StateBuilder add();

    /**
     * Builds the reply.
     *
     * @return the reply
     */
    @Override
    InboundMessage build();
}
