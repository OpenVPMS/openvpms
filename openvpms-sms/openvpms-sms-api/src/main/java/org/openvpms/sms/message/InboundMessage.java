/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import java.time.OffsetDateTime;

/**
 * An SMS message, received by OpenVPMS.
 *
 * @author Tim Anderson
 */
public interface InboundMessage extends Message {

    /**
     * Returns OpenVPMS identifier of the message that this message is a reply to.
     *
     * @return the message identifier, or {@code -1} if this message is not a reply
     */
    long getOutboundId();

    /**
     * Returns the time when the message was received.
     *
     * @return the message receipt time
     */
    OffsetDateTime getReceived();

}
