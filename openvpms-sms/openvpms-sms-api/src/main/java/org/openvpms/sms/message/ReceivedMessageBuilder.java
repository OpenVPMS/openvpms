/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.sms.exception.DuplicateSMSException;

import java.time.OffsetDateTime;

/**
 * Builder for {@link InboundMessage}s.
 *
 * @author Tim Anderson
 */
public interface ReceivedMessageBuilder<T extends ReceivedMessageBuilder<T>> extends MessageBuilder<T> {

    /**
     * Sets the message identifier, issued by the SMS provider.
     * <p>
     * A message can have a single identifier issued by a provider. To avoid duplicates, each SMS provider must
     * use a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return this
     */
    T providerId(String archetype, String id);

    /**
     * Sets the timestamp when the message was received.
     *
     * @param timestamp the received timestamp
     * @return this
     */
    T received(OffsetDateTime timestamp);

    /**
     * Builds the message.
     *
     * @return the message
     * @throws DuplicateSMSException if the message is a duplicate inbound message
     */
    @Override
    InboundMessage build();
}
