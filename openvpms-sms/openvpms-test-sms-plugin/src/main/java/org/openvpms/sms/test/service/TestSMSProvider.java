/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.test.service;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.Messages;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSProvider;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Test SMS provider.
 * <p/>
 * This can be configured to generate canned replies via the <em>entity.smsProviderTest</em> archetype.
 *
 * @author Tim Anderson
 */
public class TestSMSProvider implements SMSProvider {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The messages service.
     */
    private final Messages messages;

    /**
     * The executor service for sending replies.
     */
    private final ScheduledExecutorService executorService;

    /**
     * The provider archetype.
     */
    private static final String ARCHETYPE = "entity.smsProviderTest";

    /**
     * Text used to generate replies.
     */
    private static final String REPLY_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do " +
                                             "eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim " +
                                             "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut " +
                                             "aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                                             "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla " +
                                             "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in " +
                                             "culpa qui officia deserunt mollit anim id est laborum";

    /**
     * Constructs an {@link TestSMSProvider}.
     *
     * @param service   the archetype service
     * @param messages  the messages service
     * @param installer the archetype installer
     */
    public TestSMSProvider(ArchetypeService service, Messages messages, ArchetypeInstaller installer) {
        this.service = service;
        this.messages = messages;
        installer.install(getClass(), "/org/openvpms/sms/test/internal/archetype/" + ARCHETYPE + ".adl");
        executorService = Executors.newSingleThreadScheduledExecutor();
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Test SMS Provider";
    }

    /**
     * Returns the SMS service archetypes that this supports.
     *
     * @return a list of archetypes prefixed by <em>entity.smsProvider</em>
     */
    @Override
    public String[] getArchetypes() {
        return new String[]{ARCHETYPE};
    }

    /**
     * Sends an SMS.
     * <p/>
     * On successful send, the message status should be updated to {@link OutboundMessage.Status#SENT}.
     * <p/>
     * If the message cannot be sent due to an unrecoverable error, the message status should be updated to
     * {@link OutboundMessage.Status#ERROR}. It will not be resubmitted.
     * <p/>
     * If the message cannot be sent due to a temporary error, throw an {@link SMSException}; it will be resubmitted.
     *
     * @param message the message to send
     * @param config  the SMS provider configuration
     * @throws SMSException if the send fails and may be retried
     */
    @Override
    public void send(OutboundMessage message, Entity config) {
        IMObjectBean bean = service.getBean(config);
        String text = message.getMessage();
        message.setStatus(OutboundMessage.Status.SENT);
        if (contains(text, bean.getString("replyYesToMessagesContaining"))) {
            scheduleReply(message, "YES", bean);
        } else if (contains(text, bean.getString("replyNoToMessagesContaining"))) {
            scheduleReply(message, "NO", bean);
        } else {
            String reply;
            if (bean.getBoolean("replyWithRandomText")) {
                reply = generateText();
            } else {
                reply = bean.getString("replyWithText");
            }
            if (reply != null) {
                String count = bean.getString("replyCount", "1");
                int max;
                if (count.equals("1_TO_3")) {
                    max = RandomUtils.nextInt(1, 4);
                } else {
                    max = NumberUtils.toInt(count);
                }
                for (int i = 1; i <= max; ++i) {
                    scheduleReply(message, reply, bean);
                }
            }
        }
    }

    /**
     * Destroys this provider.
     */
    public void destroy() {
        executorService.shutdown();
    }

    /**
     * Generates random reply text.
     *
     * @return the text
     */
    private String generateText() {
        int length = RandomUtils.nextInt(17, 420);
        if (length > REPLY_TEXT.length()) {
            length = REPLY_TEXT.length();
        }
        return REPLY_TEXT.substring(0, length);
    }

    /**
     * Reply to a message.
     *
     * @param message the message to reply to
     * @param text    the reply text
     * @param config  the provider configuration
     */
    private void scheduleReply(OutboundMessage message, String text, IMObjectBean config) {
        int delay = config.getInt("replyDelay");
        executorService.schedule(() -> reply(message, text), delay, TimeUnit.SECONDS);
    }

    /**
     * Replies to a message.
     *
     * @param message the message to reply to
     * @param text    the reply text
     */
    private void reply(OutboundMessage message, String text) {
        messages.getInboundMessageBuilder()
                .phone("12345678")
                .message(text)
                .location(message.getLocation())
                .outboundId(message.getId())
                .build();
    }

    /**
     * Determines if a message contains the supplied text.
     * <p/>
     * This treats any new lines in the message as a space, to make regexp matches easier.
     *
     * @param message the message
     * @param regexp  the text to search for. May be {@code null}
     * @return {@code true} if the text matches, otherwise {@code false}
     */
    private boolean contains(String message, String regexp) {
        return regexp != null && message.replace('\n', ' ').matches(regexp);
    }
}