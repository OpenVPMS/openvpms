/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.ReplyBuilder;
import org.openvpms.sms.message.StateBuilder;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of {@link OutboundMessage}.
 *
 * @author Tim Anderson
 */
public class OutboundMessageImpl extends MessageImpl implements OutboundMessage {

    /**
     * Additional objects to save when the message is saved.
     */
    private final Set<IMObject> objects;

    /**
     * Constructs an {@link OutboundMessageImpl}.
     *
     * @param message       the message
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public OutboundMessageImpl(Act message, ArchetypeService service, DomainService domainService) {
        super(message, service, domainService);
        objects = null;
    }

    /**
     * Constructs an {@link OutboundMessageImpl}, for an unsaved message.
     *
     * @param message       the message
     * @param service       the archetype service
     * @param domainService the domain service
     * @param objects       additional objects to save when the message is saved
     */
    public OutboundMessageImpl(IMObjectBean message, ArchetypeService service, DomainService domainService,
                               Set<IMObject> objects) {
        super(message, service, domainService);
        this.objects = objects;
    }

    /**
     * Returns the timestamp when the message was created.
     *
     * @return the timestamp
     */
    public OffsetDateTime getCreated() {
        return DateRules.toOffsetDateTime(getBean().getDate("createdTime"));
    }

    /**
     * Returns the timestamp when the message was updated.
     *
     * @return the timestamp
     */
    @Override
    public OffsetDateTime getUpdated() {
        return DateRules.toOffsetDateTime(getBean().getDate("startTime"));
    }

    /**
     * Returns the timestamp when the message expires.
     * <p/>
     * It will no longer be sent after this.
     *
     * @return the timestamp, or {@code null} if the message never expires
     */
    @Override
    public OffsetDateTime getExpiry() {
        Date date = getExpiryDate();
        return (date != null) ? DateRules.toOffsetDateTime(date) : null;
    }

    /**
     * Determines if the message has expired.
     * <p/>
     * This only applies to messages that have {@code PENDING status}.
     *
     * @return {@code true} if the message has expired
     */
    @Override
    public boolean isExpired() {
        boolean result = false;
        Status status = getStatus();
        Date date = getExpiryDate();
        if (date != null && status == Status.PENDING) {
            result = date.before(new Date());
        }
        return result;
    }

    /**
     * Returns the timestamp when the message was sent.
     *
     * @return the timestamp when the message was sent, or {@code null} if it hasn't been sent
     */
    @Override
    public OffsetDateTime getSent() {
        Date date = getBean().getDate("endTime");
        return (date != null) ? DateRules.toOffsetDateTime(date) : null;
    }

    /**
     * Returns the party that this message is for.
     *
     * @return the party, or {@code null} if the party is not known
     */
    @Override
    public Party getRecipient() {
        return getBean().getTarget("contact", Party.class);
    }

    /**
     * Returns the message status.
     *
     * @return the message status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(getAct().getStatus());
    }

    /**
     * Sets the message status.
     * <p/>
     * This clears any status message.
     *
     * @param status the message status
     */
    @Override
    public void setStatus(Status status) {
        setStatus(status, null);
    }

    /**
     * Returns the status message.
     *
     * @return the status message. May be {@code null}
     */
    @Override
    public String getStatusMessage() {
        return getBean().getString("statusMessage");
    }

    /**
     * Sets the message status, along with any status message from the SMS provider.
     *
     * @param status        the status
     * @param statusMessage the status message. May be {@code null}
     */
    @Override
    public void setStatus(Status status, String statusMessage) {
        state().status(status, statusMessage).build();
    }

    /**
     * Determines if the message can transition to the specified status.
     *
     * @param status the status
     * @return {@code true} if the message can transition to the specified status, otherwise {@code false}
     */
    @Override
    public boolean canTransition(Status status) {
        Status current = getStatus();
        return current.canTransition(status);
    }

    /**
     * Determines if the message has a reply with the specified identifier.
     *
     * @param providerId the provider identifier
     * @return {@code true} if there is a reply with the specified identifier
     */
    @Override
    public boolean hasReply(String providerId) {
        for (InboundMessage reply : getReplies()) {
            if (providerId.equals(reply.getProviderId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns any replies to the message.
     *
     * @return the replies
     */
    @Override
    public List<InboundMessage> getReplies() {
        List<InboundMessage> result = new ArrayList<>();
        for (Act act : getBean().getTargets("replies", Act.class)) {
            result.add(new InboundMessageImpl(act, getService(), getDomainService()));
        }
        return result;
    }

    /**
     * Returns a builder to update the state of the message.
     *
     * @return a builder to update the message
     */
    @Override
    public StateBuilder state() {
        return new StateBuilderImpl(this, getBean(), getService(), getDomainService());
    }

    /**
     * Returns a builder to add a reply to the message.
     *
     * @return a builder to add a reply
     */
    @Override
    public ReplyBuilder reply() {
        return state().reply();
    }

    /**
     * Queues the message for send to the provider.
     */
    public void queue() {
        Act message = getAct();
        if (getStatus() != OutboundMessage.Status.PENDING) {
            throw new IllegalStateException("Cannot queue message with status=" + message.getStatus());
        }
        if (getQueueStatus() == QueueStatus.PENDING) {
            message.setStatus2(QueueStatus.QUEUED.toString());
            setUpdated();
            save();
        }
    }

    /**
     * Returns the queue status.
     *
     * @return the queue status
     */
    public QueueStatus getQueueStatus() {
        return QueueStatus.valueOf(getAct().getStatus2());
    }

    /**
     * Returns the <em>expiryTime</em> node as a Date.
     *
     * @return the expiry time, or {@code null} if the message doesn't expire
     */
    protected Date getExpiryDate() {
        return getBean().getDate("expiryTime");
    }

    /**
     * Sets the updated node.
     */
    void setUpdated() {
        getBean().setValue("startTime", new Date());
    }

    /**
     * Saves the message.
     */
    private void save() {
        IMObjectBean bean = getBean();
        if (objects != null) {
            bean.save(objects);
        } else {
            bean.save();
        }
    }
}