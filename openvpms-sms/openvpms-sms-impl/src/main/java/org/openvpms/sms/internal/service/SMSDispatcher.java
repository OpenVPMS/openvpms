/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.component.dispatcher.Dispatcher;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;

/**
 * Dispatcher for SMS messages.
 *
 * @author Tim Anderson
 */
public abstract class SMSDispatcher extends Dispatcher<Act, SMSDispatcher, SMSQueue> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The message archetype.
     */
    private final SMSQueryFactory queryFactory;

    /**
     * Constructs an {@link SMSDispatcher}.
     *
     * @param archetype       the message archetype
     * @param service         the archetype service
     * @param practiceService the practice service
     * @param log             the logger
     */
    public SMSDispatcher(String archetype, ArchetypeService service, PracticeService practiceService, Logger log) {
        super(practiceService, log);
        this.service = service;
        queryFactory = new SMSQueryFactory(archetype, service);
        init(new SMSQueues(this));
    }

    /**
     * Returns the next queued message.
     *
     * @return the next queued message, or {@code null} if there is no queued message.
     */
    protected Act getNext() {
        TypedQuery<Act> query = queryFactory.create();
        return query.getFirstResult();
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }
}
