/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.internal.message.QueueStatus;

/**
 * .
 *
 * @author Tim Anderson
 */
class SMSQueryFactory {

    private final String archetype;

    private final ArchetypeService service;

    public SMSQueryFactory(String archetype, ArchetypeService service) {
        this.archetype = archetype;
        this.service = service;
    }

    /**
     * Queries queued
     *
     * @return
     */
    public TypedQuery<Act> create() {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, archetype);
        query.select(root);
        query.where(builder.equal(root.get("status2"), QueueStatus.QUEUED.toString()));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query);
    }
}
