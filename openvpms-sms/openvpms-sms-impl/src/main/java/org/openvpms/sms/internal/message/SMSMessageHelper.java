/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.internal.SMSArchetypes;

/**
 * SMS helper methods.
 *
 * @author Tim Anderson
 */
class SMSMessageHelper {

    /**
     * Returns the provider id of a message.
     *
     * @param message the message bean
     * @return the provider id. May be {@code null}
     */
    public static String getProviderId(IMObjectBean message) {
        ActIdentity identity = message.getObject("providerId", ActIdentity.class);
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Returns the provider identity of a message.
     *
     * @param message the message bean
     * @return the provider identity. May be {@code null}
     */
    public static ActIdentity getProviderIdentity(IMObjectBean message) {
        return message.getObject("providerId", ActIdentity.class);
    }

    /**
     * Determines if a provider id in an outbound message is a duplicate.
     *
     * @param archetype the identity archetype
     * @param id        the provider id
     * @param exclude   the identifier of the message to exclude, or {@code <= 0} to not exclude any message
     * @param service   the archetype service
     * @return {@code true} if the id is a duplicate
     */
    public static boolean isDuplicateOutboundId(String archetype, String id, long exclude, ArchetypeService service) {
        return isDuplicate(SMSArchetypes.MESSAGE, archetype, id, exclude, service);
    }

    /**
     * Determines if a provider id in an inbound message is a duplicate.
     *
     * @param archetype the identity archetype
     * @param id        the provider id
     * @param service   the archetype service
     * @return {@code true} if the id is a duplicate, otherwise {@code false}
     */
    public static boolean isDuplicateInboundId(String archetype, String id, ArchetypeService service) {
        return isDuplicate(SMSArchetypes.REPLY, archetype, id, -1, service);
    }

    /**
     * Determines if a message is a duplicate.
     *
     * @param messageArchetype the message archetype
     * @param idArchetype      the id archetype. An <em>actIdentity.sms*</em>
     * @param id               the provider identifier
     * @param exclude          the identifier of the message to exclude, or {@code <= 0} to not exclude any message
     * @param service          the archetype service
     * @return {@code true} if the message is a duplicate, otherwise {@code false}
     */
    private static boolean isDuplicate(String messageArchetype, String idArchetype, String id, long exclude,
                                       ArchetypeService service) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Act> root = query.from(Act.class, messageArchetype);
        Join<Act, IMObject> providerId = root.join("providerId", idArchetype);
        providerId.on(builder.equal(providerId.get("identity"), id));
        if (exclude > 0) {
            query.where(builder.notEqual(root.get("id"), exclude));
        }
        query.select(root.get("id"));
        return service.createQuery(query).getFirstResult() != null;
    }

}