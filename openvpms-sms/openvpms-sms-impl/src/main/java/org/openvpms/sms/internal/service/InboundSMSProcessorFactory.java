/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

/**
 * Factory for {@link InboundSMSProcessor} instances.
 *
 * @author Tim Anderson
 */
public interface InboundSMSProcessorFactory {

    /**
     * Returns an {@link InboundSMSProcessor} for the specified archetype.
     *
     * @param archetype the archetype
     * @return the corresponding processor, or {@code null} if none exists
     */
    InboundSMSProcessor getProcessor(String archetype);
}