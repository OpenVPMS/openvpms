/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;
import org.openvpms.sms.message.Message;

/**
 * The default implementation of {@link Message}.
 *
 * @author Tim Anderson
 */
public class MessageImpl implements Message {

    /**
     * The underlying message.
     */
    private final Act message;

    /**
     * The bean wrapping the message.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link MessageImpl}.
     *
     * @param message       the message
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public MessageImpl(Act message, ArchetypeService service, DomainService domainService) {
        this(message, service.getBean(message), service, domainService);
    }

    /**
     * Constructs a {@link MessageImpl}.
     *
     * @param message       the message
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public MessageImpl(IMObjectBean message, ArchetypeService service, DomainService domainService) {
        this(message.getObject(Act.class), message, service, domainService);
    }

    /**
     * Constructs a {@link MessageImpl}.
     *
     * @param message       the message
     * @param bean          a bean wrapping the message
     * @param service       the archetype service
     * @param domainService the domain service
     */
    private MessageImpl(Act message, IMObjectBean bean, ArchetypeService service, DomainService domainService) {
        this.message = message;
        this.bean = bean;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns the OpenVPMS identifier for this message.
     *
     * @return the identifier
     */
    @Override
    public long getId() {
        return message.getId();
    }

    /**
     * Returns the message identifier, issued by the SMS provider.
     *
     * @return the message identifier, or {@code null} if none has been issued
     */
    @Override
    public String getProviderId() {
        return SMSMessageHelper.getProviderId(bean);
    }

    /**
     * Returns the phone number.
     *
     * @return the phone number
     */
    @Override
    public String getPhone() {
        return bean.getString("phone");
    }

    /**
     * Returns the message text.
     *
     * @return the message text
     */
    @Override
    public String getMessage() {
        return bean.getString("message");
    }

    /**
     * Returns the practice location that this message relates from.
     * <p/>
     * For outbound messages, it is always set, and indicates the practice location that sent the message.<br/>
     * For inbound messages, it indicates the location that the message is for.
     *
     * @return the practice location. May be {@code null} for inbound messages
     */
    @Override
    public Location getLocation() {
        Party location = bean.getTarget("location", Party.class);
        return (location != null) ? domainService.create(location, Location.class) : null;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MessageImpl) {
            return ((MessageImpl) obj).getAct().equals(getAct());
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return getAct().hashCode();
    }

    /**
     * Returns the message identity, as specified by the SMS provider.
     *
     * @return the message identity, or {@code null} if none is registered
     */
    protected ActIdentity getIdentity() {
        return SMSMessageHelper.getProviderIdentity(bean);
    }

    /**
     * Returns the message act.
     *
     * @return the message act
     */
    protected Act getAct() {
        return message;
    }

    /**
     * Returns the bean wrapping the message act.
     *
     * @return the bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the domain service.
     *
     * @return the domain service
     */
    protected DomainService getDomainService() {
        return domainService;
    }
}