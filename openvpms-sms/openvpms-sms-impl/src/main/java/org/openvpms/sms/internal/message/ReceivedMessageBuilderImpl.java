/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.ReceivedMessageBuilder;

import java.time.OffsetDateTime;
import java.util.HashSet;

/**
 * Default implementation of {@link ReceivedMessageBuilder}.
 *
 * @author Tim Anderson
 */
public class ReceivedMessageBuilderImpl<T extends ReceivedMessageBuilder<T>>
        extends MessageBuilderImpl<T> implements ReceivedMessageBuilder<T> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     */
    private String archetype;

    /**
     * The message identifier.
     */
    private String id;

    /**
     * The time when the reply was received.
     */
    private OffsetDateTime received;

    /**
     * Constructs an {@link ReceivedMessageBuilderImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public ReceivedMessageBuilderImpl(ArchetypeService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Sets the reply identifier, issued by the SMS provider.
     * <p>
     * A reply can have a single identifier issued by a provider. To avoid duplicates, each SMS provider must
     * use a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the reply identifier
     * @return this
     */
    @Override
    public T providerId(String archetype, String id) {
        this.archetype = archetype;
        this.id = id;
        return getThis();
    }

    /**
     * Sets the timestamp when the reply was received.
     *
     * @param timestamp the received timestamp
     * @return this
     */
    @Override
    public T received(OffsetDateTime timestamp) {
        received = timestamp;
        return getThis();
    }

    /**
     * Builds the message.
     *
     * @return the message
     * @throws DuplicateSMSException if the message is a duplicate
     */
    @Override
    public InboundMessage build() {
        IMObjectBean bean = doBuild();
        return new InboundMessageImpl(bean, service, domainService);
    }

    /**
     * Builds the message.
     *
     * @return the message bean
     * @throws DuplicateSMSException if the message is a duplicate
     */
    protected IMObjectBean doBuild() {
        if (archetype != null && id != null) {
            if (!TypeHelper.isA(archetype, "actIdentity.sms*")) {
                throw new IllegalStateException("Invalid identity archetype: " + archetype);
            }
            if (isDuplicate()) {
                throw new DuplicateSMSException(SMSMessages.duplicateInboundMessageId(id));
            }
        }
        Act act = service.create(SMSArchetypes.REPLY, Act.class);
        IMObjectBean bean = service.getBean(act);
        build(act, bean);
        return bean;
    }

    /**
     * Returns the archetype service.
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the domain service.
     *
     * @return the domain service
     */
    protected DomainService getDomainService() {
        return domainService;
    }

    /**
     * Builds the message.
     *
     * @param act  the reply act
     * @param bean the reply bean
     */
    protected void build(Act act, IMObjectBean bean) {
        super.build(bean, new HashSet<>());
        if (archetype != null && id != null) {
            ActIdentity identity = service.create(archetype, ActIdentity.class);
            identity.setIdentity(id);
            act.addIdentity(identity);
        }
        if (received != null) {
            bean.setValue("startTime", DateRules.toDate(received));
        }
    }

    /**
     * Copies details from a message to its reply.
     *
     * @param message the message
     * @param reply   the reply
     */
    protected static void copyMessageDetailsToReply(IMObjectBean message, IMObjectBean reply) {
        copyReference(message, reply, "contact");
        copyReference(message, reply, "customer");
        copyReference(message, reply, "patient");
        copyReference(message, reply, "location");
    }

    /**
     * Copies a reference from a message to its reply.
     *
     * @param message the message
     * @param reply   the reply
     * @param name    the reference node
     */
    private static void copyReference(IMObjectBean message, IMObjectBean reply, String name) {
        Reference target = message.getTargetRef(name);
        if (target != null) {
            reply.setTarget(name, target);
        }
    }

    /**
     * Determines if the message is a duplicate.
     *
     * @return {@code true} if it is a duplicate, otherwise {@code false}
     */
    private boolean isDuplicate() {
        return SMSMessageHelper.isDuplicateInboundId(archetype, id, service);
    }
}