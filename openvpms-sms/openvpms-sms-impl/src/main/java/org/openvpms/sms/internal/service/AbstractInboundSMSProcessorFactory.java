/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract implementation of {@link InboundSMSProcessorFactory}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractInboundSMSProcessorFactory implements InboundSMSProcessorFactory {

    /**
     * The processors, keyed on archetype.
     */
    private final Map<String, InboundSMSProcessor> processors = new HashMap<>();

    /**
     * Constructs an {@link AbstractInboundSMSProcessorFactory}.
     *
     * @param processors the available processors
     */
    public AbstractInboundSMSProcessorFactory(Collection<InboundSMSProcessor> processors) {
        for (InboundSMSProcessor processor : processors) {
            this.processors.put(processor.getArchetype(), processor);
        }
    }

    /**
     * Returns an {@link InboundSMSProcessor} for the specified archetype.
     *
     * @param archetype the archetype
     * @return the corresponding processor, or {@code null} if none exists
     */
    @Override
    public InboundSMSProcessor getProcessor(String archetype) {
        return processors.get(archetype);
    }
}