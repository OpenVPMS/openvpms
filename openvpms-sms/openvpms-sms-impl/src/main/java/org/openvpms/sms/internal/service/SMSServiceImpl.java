/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.message.MessagesImpl;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSProvider;
import org.openvpms.sms.service.SMSService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * Default implementation of {@link SMSService}.
 *
 * @author Tim Anderson
 */
public class SMSServiceImpl extends MessagesImpl implements SMSService, InitializingBean, DisposableBean {

    /**
     * The SMS dispatcher.
     */
    private final OutboundSMSDispatcher dispatcher;

    /**
     * Constructs an {@link SMSServiceImpl}.
     *
     * @param service         the archetype service
     * @param practiceService the practice service
     * @param pluginManager   the plugin manager
     * @param domainService   the domain object factory
     * @param mailProvider    the mail SMS provider
     */
    public SMSServiceImpl(IArchetypeRuleService service, PracticeService practiceService, PluginManager pluginManager,
                          DomainService domainService, SMSProvider mailProvider) {
        super(service, domainService);
        dispatcher = new OutboundSMSDispatcher(service, pluginManager, practiceService, domainService, mailProvider);
    }

    /**
     * Determines if SMS support is enabled.
     *
     * @return {@code true} if SMS support is enabled, otherwise {@code false}
     */
    @Override
    public boolean isEnabled() {
        Entity config = dispatcher.getConfig();
        return (config != null) && config.isActive();
    }

    /**
     * Returns the number of message parts supported by the SMS provider.
     *
     * @return the number of parts
     */
    @Override
    public int getMaxParts() {
        IMObjectBean bean = dispatcher.getConfigBean();
        return bean != null ? bean.getInt("parts") : 1;
    }

    /**
     * Sends a message.
     * <p/>
     * The message will be sent using an {@link SMSProvider}.
     * <p/>
     * If it is not available, it will be periodically retried until sent, or it expires.
     *
     * @param message the message to send
     * @throws SMSException if the message cannot be sent due to an unrecoverable error
     */
    @Override
    public void send(OutboundMessage message) {
        dispatcher.queue(message);
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     */
    @Override
    public void afterPropertiesSet() {
        dispatcher.initialise();
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     *
     * @throws Exception in case of shutdown errors. Exceptions will get logged
     *                   but not rethrown to allow other beans to release their resources as well.
     */
    @Override
    public void destroy() throws Exception {
        dispatcher.destroy();
    }
}
