/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;

/**
 * Messages reported by the SMS interface.
 *
 * @author Tim Anderson
 */
public class SMSMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("SMS", SMSMessages.class.getName());

    /**
     * Message indicating that the message identifier archetype cannot be changed.
     *
     * @param current the current archetype
     * @param other   the other archetype
     * @return a new message
     */
    public static Message differentMessageIdentifierArchetype(String current, String other) {
        return messages.create(10, current, other);
    }

    /**
     * Creates a message indicating that an outbound message identifier is a duplicate.
     *
     * @param id the provider id
     * @return a new message
     */
    public static Message duplicateOutboundMessageId(String id) {
        return messages.create(11, id);
    }

    /**
     * Creates a message for when an SMS provider is not available.
     *
     * @param config the provider configuration
     * @return a new message
     */
    public static Message providerNotAvailable(Entity config) {
        return messages.create(20, config.getName());
    }

    /**
     * Creates a message indicating that an inbound message identifier is a duplicate.
     *
     * @param id the provider id
     * @return a new message
     */
    public static Message duplicateInboundMessageId(String id) {
        return messages.create(30, id);
    }

    /**
     * Creates a message indicating that an outbound message has no phone number.
     *
     * @return a new message
     */
    public static Message noPhone() {
        return messages.create(40);
    }

    /**
     * Creates a message for the situation where there is no <em>entity.SMSConfig*</em> associated with the practice.
     *
     * @param practice the practice
     * @return a new message
     */
    public static Message SMSNotConfigured(Party practice) {
        return messages.create(100, practice.getName());
    }

    /**
     * Creates a message for failure to find the <em>party.organisationPractice</em>.
     *
     * @return a new message
     */
    public static Message practiceNotFound() {
        return messages.create(101);
    }

    /**
     * Creates a message for the situation where an email cannot be created.
     *
     * @param reason the reason
     * @return a new message
     */
    public static Message failedToCreateEmail(String reason) {
        return messages.create(200, reason);
    }

    /**
     * Creates a message for a mail server authentication failure.
     *
     * @param reason the reason
     * @return a new message
     */
    public static Message mailAuthenticationFailed(String reason) {
        return messages.create(201, reason);
    }

    /**
     * Creates a message for a mail connection failure.
     *
     * @param reason the reason
     * @return a new message
     */
    public static Message mailConnectionFailed(String reason) {
        return messages.create(202, reason);
    }

    /**
     * Creates a message for a mail send failure.
     *
     * @param reason the reason
     * @return a new message
     */
    public static Message mailSendFailed(String reason) {
        return messages.create(203, reason);
    }

    /**
     * Creates a message for failure to evaluate an expression.
     *
     * @param expression the expression
     * @return a new message
     */
    public static Message failedToEvaluateExpression(String expression) {
        return messages.create(300, expression);
    }

    /**
     * Creates a message for an invalid 'From' address.
     *
     * @param address the address
     * @return a new message
     */
    public static Message invalidFromAddress(String address) {
        return messages.create(301, address);
    }

    /**
     * Creates a message for an invalid 'To' address.
     *
     * @param address the address
     * @return a new message
     */
    public static Message invalidToAddress(String address) {
        return messages.create(302, address);
    }

    /**
     * Creates a message for an invalid 'Reply-To' address.
     *
     * @param address the address
     * @return a new message
     */
    public static Message invalidReplyToAddress(String address) {
        return messages.create(303, address);
    }

    /**
     * Creates a message for no message text.
     *
     * @return a new message
     */
    public static Message noMessageText() {
        return messages.create(304);
    }

}
