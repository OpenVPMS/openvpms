/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal;

/**
 * SMS provider archetypes.
 *
 * @author Tim Anderson
 */
public class SMSArchetypes {

    /**
     * SMS archetype.
     */
    public static final String MESSAGE = "act.smsMessage";

    /**
     * SMS reply archetype.
     */
    public static final String REPLY = "act.smsReply";

    /**
     * SMS provider configuration archetypes.
     */
    public static final String SMS_PROVIDERS = "entity.smsProvider*";

    /**
     * Legacy email to SMS provider SMS configuration archetypes.
     */
    public static final String EMAIL_CONFIGURATIONS = "entity.SMSConfigEmail*";

    /**
     * Legacy generic Email to SMS provider configuration.
     */
    public static final String GENERIC_SMS_EMAIL_CONFIG = "entity.SMSConfigEmailGeneric";

    /**
     * SMS source archetype.
     */
    public static final String SMS_SOURCE = "actRelationship.smsSource";

    /**
     * Default constructor.
     */
    private SMSArchetypes() {
    }

}
