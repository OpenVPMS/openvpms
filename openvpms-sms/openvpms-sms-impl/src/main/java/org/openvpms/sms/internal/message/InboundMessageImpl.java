/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.message.InboundMessage;

import java.time.OffsetDateTime;

/**
 * Default implementation of {@link InboundMessage}.
 *
 * @author Tim Anderson
 */
public class InboundMessageImpl extends MessageImpl implements InboundMessage {

    /**
     * Constructs a {@link InboundMessageImpl}.
     *
     * @param message       the message act
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public InboundMessageImpl(Act message, ArchetypeService service, DomainService domainService) {
        super(message, service, domainService);
    }

    /**
     * Constructs a {@link InboundMessageImpl}.
     *
     * @param bean          a bean wrapping the act
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public InboundMessageImpl(IMObjectBean bean, ArchetypeService service, DomainService domainService) {
        super(bean, service, domainService);
    }

    /**
     * Returns the OpenVPMS identifier of the message that this message is a reply to.
     *
     * @return the message identifier, or {@code -1} if this message is not a reply
     */
    @Override
    public long getOutboundId() {
        Reference reference = getBean().getSourceRef("sms");
        return reference != null ? reference.getId() : -1;
    }

    /**
     * Returns the time when the message was received.
     *
     * @return the message receipt time
     */
    @Override
    public OffsetDateTime getReceived() {
        return DateRules.toOffsetDateTime(getBean().getDate("startTime"));
    }

    /**
     * Returns the queue status.
     *
     * @return the queue status. May be {@code null}
     */
    public QueueStatus getQueueStatus() {
        String result = getAct().getStatus2();
        return result != null ? QueueStatus.valueOf(result) : null;
    }
}