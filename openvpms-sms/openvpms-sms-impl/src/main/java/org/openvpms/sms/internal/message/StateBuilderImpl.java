/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.ReplyBuilder;
import org.openvpms.sms.message.StateBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.openvpms.sms.internal.message.QueueStatus.COMPLETED;
import static org.openvpms.sms.message.OutboundMessage.Status.DELIVERED;
import static org.openvpms.sms.message.OutboundMessage.Status.SENT;

/**
 * Default implementation of {@link StateBuilder}.
 *
 * @author Tim Anderson
 */
public class StateBuilderImpl implements StateBuilder {

    /**
     * The message.
     */
    private final OutboundMessageImpl message;

    /**
     * The bean for accessing the message.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The replies.
     */
    private final List<Act> replies = new ArrayList<>();

    /**
     * The identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     */
    private String archetype;

    /**
     * The message identifier.
     */
    private String id;

    /**
     * The message status.
     */
    private OutboundMessage.Status status;

    /**
     * The status message.
     */
    private String statusMessage;

    /**
     * The delivered time node.
     */
    private static final String DELIVERED_NODE = "endTime";

    /**
     * The status node.
     */
    private static final String STATUS_NODE = "status";

    /**
     * The status message node.
     */
    private static final String STATUS_MESSAGE_NODE = "statusMessage";

    /**
     * The queue status node.
     */
    private static final String QUEUE_STATUS_NODE = "status2";

    /**
     * Constructs a {@link StateBuilderImpl}.
     *
     * @param message       the message
     * @param bean          the bean for accessing the message
     * @param service       the archetype service
     * @param domainService the domain service
     */
    StateBuilderImpl(OutboundMessageImpl message, IMObjectBean bean, ArchetypeService service,
                     DomainService domainService) {
        this.message = message;
        this.bean = bean;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Sets the message identifier, issued by the SMS provider.
     * <p>
     * A message can have a single identifier issued by a provider. To avoid duplicates, each SMS provider must
     * use a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return this
     */
    @Override
    public StateBuilder providerId(String archetype, String id) {
        this.archetype = archetype;
        this.id = id;
        return this;
    }

    /**
     * Sets the message status.
     * <p/>
     * This clears any status message.
     *
     * @param status the message status
     * @return this
     */
    @Override
    public StateBuilder status(OutboundMessage.Status status) {
        return status(status, null);
    }

    /**
     * Sets the message status, along with any status message from the SMS provider.
     *
     * @param status        the status
     * @param statusMessage the status message. May be {@code null}
     * @return this
     */
    @Override
    public StateBuilder status(OutboundMessage.Status status, String statusMessage) {
        this.status = status;
        this.statusMessage = statusMessage;
        return this;
    }

    /**
     * Returns a builder to add a reply.
     *
     * @return the reply builder
     */
    @Override
    public ReplyBuilder reply() {
        return new ReplyBuilderImpl(this, service, domainService);
    }

    /**
     * Builds the message.
     *
     * @return the message
     * @throws DuplicateSMSException if the message has a duplicate identifier
     */
    @Override
    public OutboundMessage build() {
        boolean changed = false;
        List<IMObject> toSave = new ArrayList<>();
        if (archetype != null) {
            changed = setIdentity();
        }
        if (status != null) {
            changed |= setStatus();
            changed |= setStatusMessage();
        }
        changed |= addReplies(toSave);
        if (changed) {
            message.setUpdated();
            toSave.add(0, bean.getObject());
            service.save(toSave);
        }

        return message;
    }

    /**
     * Adds a reply.
     *
     * @param reply the reply
     */
    void addReply(Act reply) {
        replies.add(reply);
    }

    /**
     * Sets the provider identity.
     *
     * @return {@code true} if the provider identity changed, otherwise {@code false}
     * @throws DuplicateSMSException if the identity is a duplicate
     */
    private boolean setIdentity() {
        boolean changed = false;
        if (!TypeHelper.isA(archetype, "actIdentity.sms*")) {
            throw new IllegalStateException("Invalid identity archetype: " + archetype);
        }
        if (SMSMessageHelper.isDuplicateOutboundId(archetype, id, bean.getObject().getId(), service)) {
            throw new DuplicateSMSException(SMSMessages.duplicateOutboundMessageId(id));
        }
        ActIdentity identity = message.getIdentity();
        if (identity == null) {
            identity = service.create(archetype, ActIdentity.class);
            bean.addValue("providerId", identity);
            changed = true;
        } else if (!identity.isA(archetype)) {
            throw new SMSException(SMSMessages.differentMessageIdentifierArchetype(identity.getArchetype(),
                                                                                   archetype));
        }
        if (!Objects.equals(id, identity.getIdentity())) {
            identity.setIdentity(id);
            changed = true;
        }
        return changed;
    }

    /**
     * Sets the message status.
     *
     * @return {@code true} if the status changed, otherwise {@code false}
     */
    private boolean setStatus() {
        boolean changed = false;
        OutboundMessage.Status current = message.getStatus();
        if (!current.canTransition(status)) {
            throw new IllegalStateException("Cannot transition from " + current + " to " + status);
        } else if (status != current) {
            if (status == SENT || (status == DELIVERED && bean.getDate(DELIVERED_NODE) == null)) {
                bean.setValue(DELIVERED_NODE, new Date());
            }
            bean.setValue(STATUS_NODE, status.toString());
            if (message.getQueueStatus() != COMPLETED) {
                bean.setValue(QUEUE_STATUS_NODE, COMPLETED.toString());
            }
            changed = true;
        }
        return changed;
    }

    /**
     * Sets the status message, truncating it if it is too long.
     *
     * @return {@code true} if the status message changed, otherwise {@code false}
     */
    private boolean setStatusMessage() {
        boolean changed = false;
        if (!StringUtils.isEmpty(statusMessage)) {
            statusMessage = StringUtils.abbreviate(statusMessage, bean.getMaxLength(STATUS_MESSAGE_NODE));
        }
        if (!Objects.equals(statusMessage, bean.getString(STATUS_MESSAGE_NODE))) {
            bean.setValue(STATUS_MESSAGE_NODE, statusMessage);
            changed = true;
        }
        return changed;
    }

    /**
     * Adds replies to the message.
     *
     * @param toSave the objects to save
     * @return {@code true} if the message changed, otherwise {@code false}
     */
    private boolean addReplies(List<IMObject> toSave) {
        boolean changed = false;
        if (!replies.isEmpty()) {
            Set<String> ids = new HashSet<>();
            for (Act reply : replies) {
                IMObjectBean replyBean = service.getBean(reply);
                String id = SMSMessageHelper.getProviderId(replyBean);
                if (id != null && !ids.add(id)) {
                    throw new DuplicateSMSException(SMSMessages.duplicateInboundMessageId(id));
                }
                ReceivedMessageBuilderImpl.copyMessageDetailsToReply(bean, replyBean);
                bean.addTarget("replies", reply, "sms");
            }
            toSave.addAll(replies);
            replies.clear(); // can't reuse
            changed = true;
        }
        return changed;
    }

}
