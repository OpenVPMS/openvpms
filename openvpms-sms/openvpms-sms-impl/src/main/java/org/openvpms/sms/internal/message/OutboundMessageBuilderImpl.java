/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessageBuilder;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Default implementation of {@link OutboundMessageBuilder}.
 *
 * @author Tim Anderson
 */
public class OutboundMessageBuilderImpl extends MessageBuilderImpl<OutboundMessageBuilder>
        implements OutboundMessageBuilder {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The party the message is for, or {@code null} if it is not known.
     */
    private Party recipient;

    /**
     * The timestamp when the message expires.
     */
    private OffsetDateTime expiry;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The subject of the SMS, for communication logging purposes.
     */
    private String subject;

    /**
     * The reason for the SMS, for communication logging purposes.
     */
    private String reason;

    /**
     * A note about the SMS, for communication logging purposes.
     */
    private String note;

    /**
     * The source of the message.
     */
    private Act source;

    /**
     * Constructs a {@link OutboundMessageBuilderImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public OutboundMessageBuilderImpl(ArchetypeService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Sets the recipient of the SMS.
     *
     * @param recipient the recipient
     * @return this
     */
    @Override
    public OutboundMessageBuilder recipient(Party recipient) {
        this.recipient = recipient;
        return this;
    }

    /**
     * Sets the timestamp when the message expires.
     * <p/>
     * If it has not been sent to the provider by this time, it's status will change to
     * {@link OutboundMessage.Status#EXPIRED}.
     * <p/>
     * If it has been sent to the provider, but not delivered by this time, the provider may choose not to deliver it.
     *
     * @param expiry the expiry, or {@code null} if the message never expires
     * @return this
     */
    @Override
    public OutboundMessageBuilder expiry(OffsetDateTime expiry) {
        this.expiry = expiry;
        return this;
    }

    /**
     * Sets the customer that the SMS refers to.
     * <p/>
     * Defaults to the {@link #recipient}, if it is a customer.
     *
     * @param customer the customer
     * @return this
     */
    @Override
    public OutboundMessageBuilder customer(Party customer) {
        this.customer = customer;
        return this;
    }

    /**
     * Sets the patient that the SMS refers to.
     *
     * @param patient the patient
     * @return this
     */
    @Override
    public OutboundMessageBuilder patient(Party patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Sets the subject of the SMS, for communication logging purposes.
     *
     * @param subject the SMS subject
     * @return this
     */
    @Override
    public OutboundMessageBuilder subject(String subject) {
        this.subject = subject;
        return this;
    }

    /**
     * Sets the reason for the SMS, for communication logging purposes.
     *
     * @param reason the reason for the SMS. A code for an <em>lookup.customerCommunicationReason</em>
     * @return this
     */
    @Override
    public OutboundMessageBuilder reason(String reason) {
        this.reason = reason;
        return this;
    }

    /**
     * Sets the note about the SMS, for communication logging purposes.
     *
     * @param note the note
     * @return this
     */
    @Override
    public OutboundMessageBuilder note(String note) {
        this.note = note;
        return this;
    }

    /**
     * Sets the source of the message. This is the act that triggered generation of the SMS.
     *
     * @param source the source
     * @return this
     */
    @Override
    public OutboundMessageBuilder source(Act source) {
        this.source = source;
        return this;
    }

    /**
     * Builds the message.
     *
     * @return the message
     */
    @Override
    public OutboundMessage build() {
        Act act = service.create(SMSArchetypes.MESSAGE, Act.class);
        IMObjectBean bean = service.getBean(act);
        HashSet<IMObject> objects = new HashSet<>();
        build(bean, objects);
        return new OutboundMessageImpl(bean, service, domainService, objects);
    }

    /**
     * Builds the message.
     *
     * @param bean    the message bean
     * @param objects collects the built objects
     */
    @Override
    protected void build(IMObjectBean bean, Set<IMObject> objects) {
        super.build(bean, objects);
        if (StringUtils.trimToNull(getPhone()) == null) {
            // for legacy data reasons, the act.smsMessage archetype doesn't specify a minCardinality=1 for the phone,
            // so need to check it has been populated
            throw new SMSException(SMSMessages.noPhone());
        }
        if (recipient != null) {
            bean.setTarget("contact", recipient);
        }
        if (expiry != null) {
            bean.setValue("expiryTime", DateRules.toDate(expiry));
        }

        if (customer == null && (recipient != null && recipient.isA(CustomerArchetypes.PERSON))) {
            bean.setTarget("customer", recipient);
        } else if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        setValue(bean, "description", subject);
        bean.setValue("reason", reason);
        setValue(bean, "note", note);
        if (source != null) {
            ActRelationship relationship = service.create(SMSArchetypes.SMS_SOURCE, ActRelationship.class);
            relationship.setSource(source.getObjectReference());
            relationship.setTarget(bean.getReference());
            source.addActRelationship(relationship);
            bean.getObject(Act.class).addActRelationship(relationship);
            objects.add(source);
        }
    }

    /**
     * Sets a node value, truncating it if it is too long.
     * <p/>
     * This should be used for non-critical fields that shouldn't prevent a message being sent if they are too long.
     *
     * @param bean  the message bean
     * @param name  the node name
     * @param value the node value. May be {@code null}
     */
    private void setValue(IMObjectBean bean, String name, String value) {
        if (value != null) {
            bean.setValue(name, StringUtils.abbreviate(value, bean.getMaxLength(name)));
        }
    }
}