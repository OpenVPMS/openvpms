/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.workflow.AppointmentReminderConfig;
import org.openvpms.archetype.rules.workflow.AppointmentReminderConfig.Match;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.internal.message.ReadStatus;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;

import java.util.Date;

import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CANCELLED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CONFIRMED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.PENDING;

/**
 * Processes replies to appointment reminders, based on the <em>entity.jobAppointmentReminder</em> configuration.
 * <p/>
 * This marks reminders as:
 * <ul>
 *     <li>CONFIRMED, if the reminder SMS contains the <em>confirmAppointment</em> text, and the reply message is a
 *     match, ignoring case; or
 *     </li>
 *     <li>CANCELLED, if the reminder SMS contains the <em>cancelAppointment</em> text, and the reply message is a
 *     match, ignoring case
 *     </li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class AppointmentReplyProcessor implements InboundSMSProcessor {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The SMS status node.
     */
    private static final String SMS_STATUS = "smsStatus";

    /**
     * Constructs an {@link AppointmentReplyProcessor}.
     *
     * @param service the archetype service
     * @param rules   the appointment rules
     */
    public AppointmentReplyProcessor(ArchetypeService service, AppointmentRules rules) {
        this.service = service;
        this.rules = rules;
    }

    /**
     * Returns the archetype of the source message that this can process.
     *
     * @return the archetype
     */
    @Override
    public String getArchetype() {
        return ScheduleArchetypes.APPOINTMENT;
    }

    /**
     * Processes an inbound message.
     *
     * @param inbound  the inbound message
     * @param outbound the corresponding outbound message
     * @param source   the source of the outbound message
     * @return the new read status of the inbound message. May be {@code null}
     */
    @Override
    public ReadStatus process(InboundMessage inbound, OutboundMessage outbound, Act source) {
        ReadStatus result = ReadStatus.PENDING;
        AppointmentReminderConfig config = rules.getAppointmentReminderConfig();
        IMObjectBean bean = service.getBean(source);
        boolean changed = false;
        boolean setToReceived = true;
        if (config != null && config.processReplies()) {
            String status = source.getStatus();
            if (PENDING.equals(status) || CONFIRMED.equals(status) || CANCELLED.equals(status)) {
                String outboundMessage = outbound.getMessage();
                String inboundMessage = StringUtils.trimToEmpty(inbound.getMessage());
                String newStatus = null;
                Match match = config.isConfirmation(inboundMessage, outboundMessage);
                if (match.isMatch()) {
                    newStatus = CONFIRMED;
                    if (match == Match.EXACT) {
                        // users don't need to read the reply, so mark it completed
                        result = ReadStatus.COMPLETED;
                    }
                } else {
                    match = config.isCancellation(inboundMessage, outboundMessage);
                    if (match.isMatch()) {
                        newStatus = CANCELLED;
                    }
                }
                if (newStatus != null) {
                    if (newStatus.equals(status)) {
                        setToReceived = Match.EXACT != match;
                    } else if (!CANCELLED.equals(status)) {
                        // cannot confirm a cancelled appointment
                        source.setStatus(newStatus);
                        if (newStatus.equals(CONFIRMED)) {
                            bean.setValue("confirmedTime", new Date());
                        }
                        changed = true;
                        setToReceived = Match.EXACT != match;
                    }
                }
            }
        }
        if (setToReceived && !AppointmentStatus.SMS_RECEIVED.equals(bean.getString(SMS_STATUS))) {
            // only update the status if the reply had text other than that expected
            bean.setValue(SMS_STATUS, AppointmentStatus.SMS_RECEIVED);
            changed = true;
        }
        if (changed) {
            service.save(source);
        }
        return result;
    }
}