/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.message.InboundMessageBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Default implementation of {@link InboundMessageBuilder}.
 *
 * @author Tim Anderson
 */
public class InboundMessageBuilderImpl extends ReceivedMessageBuilderImpl<InboundMessageBuilder>
        implements InboundMessageBuilder {

    /**
     * The messages service.
     */
    private final MessagesImpl messages;

    /**
     * The outbound OpenVPMS message id.
     */
    private long outboundId;

    /**
     * The outbound provider identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     */
    private String outboundArchetype;

    /**
     * The outbound provider message identifier.
     */
    private String outboundProviderId;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InboundMessageBuilderImpl.class);


    /**
     * Constructs an {@link InboundMessageBuilderImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain service
     * @param messages      the messages service
     */
    public InboundMessageBuilderImpl(ArchetypeService service, DomainService domainService, MessagesImpl messages) {
        super(service, domainService);
        this.messages = messages;
    }

    /**
     * Sets the OpenVPMS identifier of the outbound message, if this message is a reply to an existing message.
     *
     * @param id the OpenVPMS message identifier
     * @return this
     */
    @Override
    public InboundMessageBuilder outboundId(long id) {
        outboundId = id;
        return this;
    }

    /**
     * Sets the provider identifier of the outbound message, if this message is a reply to an existing message.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return this
     */
    @Override
    public InboundMessageBuilder outboundProviderId(String archetype, String id) {
        outboundArchetype = archetype;
        outboundProviderId = id;
        return this;
    }

    /**
     * Builds and saves the message.
     * <p/>
     * If the inbound message is a reply to an outbound message, the two will be linked.
     *
     * @param act  the reply act
     * @param bean the reply bean
     */
    @Override
    protected void build(Act act, IMObjectBean bean) {
        super.build(act, bean);
        Act outbound = getOutbound();
        if (outbound != null) {
            IMObjectBean original = getService().getBean(outbound);
            original.setValue("startTime", new Date());  // indicates when the outbound message was updated
            copyMessageDetailsToReply(original, bean);
            original.addTarget("replies", act, "sms");
            bean.save(outbound);
        } else {
            bean.save();
        }
    }

    /**
     * Returns the outbound message that this is a reply to, if any.
     * <p/>
     * NOTE that if no outbound message is found, but identifiers were provided, the inbound message will still be
     * created rather than an exception thrown.
     *
     * @return the outbound message, or {@code null} if none is found
     */
    private Act getOutbound() {
        Act result = null;
        if (outboundId > 0) {
            result = messages.getOutbound(outboundId);
            if (result == null) {
                log.warn("No outbound message found with id={}. The reply will be saved as an uncorrelated message",
                         outboundId);
            }
        } else if (outboundArchetype != null && outboundProviderId != null) {
            result = messages.getOutbound(outboundArchetype, outboundProviderId);
            if (result == null) {
                log.warn("No outbound message found with provider archetype={} and id={}. " +
                         "The reply will be saved as an uncorrelated message", outboundArchetype, outboundProviderId);
            }
        }
        return result;
    }
}