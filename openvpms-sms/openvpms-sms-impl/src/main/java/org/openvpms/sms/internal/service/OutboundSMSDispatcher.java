/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.internal.message.OutboundMessageImpl;
import org.openvpms.sms.internal.message.QueueStatus;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dispatches queued messages to the configured {@link SMSProvider}.
 *
 * @author Tim Anderson
 */
class OutboundSMSDispatcher extends SMSDispatcher {

    /**
     * The plugin manager.
     */
    private final PluginManager pluginManager;

    /**
     * The mail provider.
     */
    private final SMSProvider mailProvider;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OutboundSMSDispatcher.class);

    /**
     * Constructs a {@link OutboundSMSDispatcher}.
     *
     * @param service         the archetype service
     * @param pluginManager   the plugin manager
     * @param practiceService the practice service
     * @param domainService   the domain object service
     * @param mailProvider    the mail provider
     */
    public OutboundSMSDispatcher(ArchetypeService service, PluginManager pluginManager, PracticeService practiceService,
                                 DomainService domainService, SMSProvider mailProvider) {
        super(SMSArchetypes.MESSAGE, service, practiceService, log);
        this.pluginManager = pluginManager;
        this.mailProvider = mailProvider;
        this.domainService = domainService;
    }

    /**
     * Initialises the dispatcher.
     */
    public void initialise() {
        schedule();
    }

    /**
     * Queues a message for sending.
     *
     * @param message the message to queue
     */
    public void queue(OutboundMessage message) {
        if (message.getStatus() != OutboundMessage.Status.PENDING) {
            throw new IllegalStateException("Cannot queue message with status=" + message.getStatus());
        }
        requireConfig();
        ((OutboundMessageImpl) message).queue();
        schedule();
    }

    /**
     * Returns the SMS provider configuration.
     *
     * @return the configuration
     * @throws SMSException if no SMS provider configuration is available
     */
    protected Entity requireConfig() {
        Entity config = getConfig();
        if (config == null) {
            Party practice = getPracticeService().getPractice();
            if (practice == null) {
                throw new SMSException(SMSMessages.practiceNotFound());
            }
            throw new SMSException(SMSMessages.SMSNotConfigured(practice));
        }
        return config;
    }

    /**
     * Processes an object.
     * <p/>
     * If the object is successfully processed, {@link Queue#processed()} should be invoked, otherwise an
     * exception should be raised.
     *
     * @param object the object to process
     * @param queue  the queue
     */
    @Override
    protected void process(Act object, SMSQueue queue) {
        Entity config = requireConfig();
        SMSProvider provider = getProvider(config);
        if (provider != null) {
            OutboundMessageImpl message = new OutboundMessageImpl(object, getService(), domainService);
            if (message.isExpired()) {
                message.setStatus(OutboundMessage.Status.EXPIRED);
            } else if (message.getQueueStatus() == QueueStatus.QUEUED) {
                provider.send(message, config);
            }
            queue.processed();
        } else {
            throw new SMSException(SMSMessages.providerNotAvailable(config));
        }
    }

    /**
     * Returns the SMS provider for the SMS configuration.
     *
     * @param config the configuration
     * @return the SMS provider, or {@code null} if none is found
     */
    protected SMSProvider getProvider(Entity config) {
        SMSProvider result = null;
        String archetype = config.getArchetype();
        if (config.isA(mailProvider.getArchetypes())) {
            result = mailProvider;
        } else {
            for (SMSProvider provider : pluginManager.getServices(SMSProvider.class)) {
                if (ArrayUtils.indexOf(provider.getArchetypes(), archetype) != -1) {
                    result = provider;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns the SMS provider configuration.
     *
     * @return the SMS provider configuration, or {@code null} if there is none, or it is inactive
     */
    protected Entity getConfig() {
        Entity config = getPracticeService().getSMS();
        return (config != null && config.isActive()) ? config : null;
    }

    /**
     * Returns the SMS provider configuration as a bean.
     *
     * @return the SMS provider configuration, or {@code null} if there is none, or it is inactive
     */
    protected IMObjectBean getConfigBean() {
        Entity config = getConfig();
        return (config != null) ? getService().getBean(config) : null;
    }

    /**
     * Returns a string representation of an object.
     *
     * @param object the object
     * @return a string representation of the object
     */
    @Override
    protected String toString(Act object) {
        return object.getObjectReference().toString();
    }
}