/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Collections;

/**
 * Default implementation of the {@link InboundSMSProcessorFactory}.
 * <p/>
 * This supports processing replies to appointment reminder SMS.
 *
 * @author Tim Anderson
 */
public class DefaultInboundSMSProcessorFactory extends AbstractInboundSMSProcessorFactory {

    /**
     * Constructs a {@link DefaultInboundSMSProcessorFactory}.
     *
     * @param service          the archetype service
     * @param appointmentRules the appointment rules
     */
    public DefaultInboundSMSProcessorFactory(ArchetypeService service, AppointmentRules appointmentRules) {
        super(Collections.singletonList(new AppointmentReplyProcessor(service, appointmentRules)));
    }
}