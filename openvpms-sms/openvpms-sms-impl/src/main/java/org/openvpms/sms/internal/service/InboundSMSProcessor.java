/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.sms.internal.message.ReadStatus;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;

/**
 * Processor for inbound SMS, where the outbound SMS has a source act.
 *
 * @author Tim Anderson
 */
public interface InboundSMSProcessor {

    /**
     * Returns the archetype of the source message that this can process.
     *
     * @return the archetype
     */
    String getArchetype();

    /**
     * Processes an inbound message.
     *
     * @param inbound  the inbound message
     * @param outbound the corresponding outbound message
     * @param source   the source of the outbound message
     * @return the new read status of the inbound message. May be {@code null}
     */
    ReadStatus process(InboundMessage inbound, OutboundMessage outbound, Act source);
}