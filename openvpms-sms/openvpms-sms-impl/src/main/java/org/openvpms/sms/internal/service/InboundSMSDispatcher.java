/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.AbstractArchetypeServiceListener;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.message.InboundMessageImpl;
import org.openvpms.sms.internal.message.OutboundMessageImpl;
import org.openvpms.sms.internal.message.QueueStatus;
import org.openvpms.sms.internal.message.ReadStatus;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Dispatcher for inbound SMS.
 * <p/>
 * If an inbound SMS has an {@link OutboundMessage} with a source, the associated {@link InboundSMSProcessor}
 * will be used to process the SMS.
 *
 * @author Tim Anderson
 */
public class InboundSMSDispatcher extends SMSDispatcher {

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The factory for {@link InboundSMSProcessor}s.
     */
    private final InboundSMSProcessorFactory factory;

    /**
     * The listener for inbound messages.
     */
    private final AbstractArchetypeServiceListener listener;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InboundSMSDispatcher.class);

    /**
     * Queued status.
     */
    private static final String QUEUED = QueueStatus.QUEUED.toString();

    /**
     * Constructs an {@link InboundSMSProcessor}.
     *
     * @param service            the archetype service
     * @param practiceService    the practice service
     * @param domainService      the domain service
     * @param factory            the factory for {@link InboundSMSProcessor} instances
     * @param transactionManager the transaction manager
     */
    public InboundSMSDispatcher(IArchetypeRuleService service, PracticeService practiceService,
                                DomainService domainService, InboundSMSProcessorFactory factory,
                                PlatformTransactionManager transactionManager) {
        super(SMSArchetypes.REPLY, service, practiceService, log);
        this.domainService = domainService;
        this.factory = factory;
        this.transactionManager = transactionManager;
        listener = new AbstractArchetypeServiceListener() {
            public void saved(IMObject object) {
                schedule();
            }
        };
        service.addListener(SMSArchetypes.REPLY, listener);
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     *
     * @throws Exception in case of shutdown errors
     */
    @Override
    public void destroy() throws Exception {
        try {
            super.destroy();
        } finally {
            ((IArchetypeRuleService) getService()).removeListener(SMSArchetypes.REPLY, listener);
        }
    }

    /**
     * Processes an object.
     * <p/>
     * If the object is successfully processed, {@link Queue#processed()} should be invoked, otherwise an
     * exception should be raised.
     *
     * @param object the inbound SMS
     * @param queue  the queue
     * @throws Exception for any error
     */
    @Override
    protected void process(Act object, SMSQueue queue) throws Exception {
        if (QUEUED.equals(object.getStatus2())) {
            boolean processed = false;
            ArchetypeService service = getService();
            InboundMessage inbound = new InboundMessageImpl(object, service, domainService);
            Act outboundAct = getOutboundAct(inbound);
            if (outboundAct != null) {
                Act source = service.getBean(outboundAct).getSource("source", Act.class);
                if (source != null) {
                    InboundSMSProcessor processor = factory.getProcessor(source.getArchetype());
                    if (processor != null) {
                        OutboundMessage outbound = new OutboundMessageImpl(outboundAct, service, domainService);
                        process(inbound, object, outbound, source, processor);
                        processed = true;
                    }
                }
            }
            if (!processed) {
                // mark the act completed so it's not processed again
                completed(object, null);
            }
        }
        queue.processed();
    }

    /**
     * Processes an inbound message.
     * <p/>
     * On completion, sets the inbound message status to COMPLETED.
     *
     * @param inbound    the inbound message
     * @param inboundAct the inbound message act
     * @param outbound   the corresponding outbound message
     * @param source     the source of the outbound message
     * @param processor  the inbound message processor
     */
    private void process(InboundMessage inbound, Act inboundAct, OutboundMessage outbound, Act source,
                         InboundSMSProcessor processor) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                ReadStatus readStatus = processor.process(inbound, outbound, source);
                completed(inboundAct, readStatus);
            }
        });
    }

    /**
     * Marks an inbound message completed, to avoid re-processing.
     *
     * @param act         the inbound message
     * @param readStatus the new read status, or {@code null} to leave it unchanged
     */
    private void completed(Act act, ReadStatus readStatus) {
        act.setStatus2(QueueStatus.COMPLETED.name());
        if (readStatus != null) {
            act.setStatus(readStatus.name());
        }
        getService().save(act);
    }

    /**
     * Returns the act associated with an inbound message.
     *
     * @param inbound the inbound message
     * @return the outbound message act, or {@code null} if none is found
     */
    private Act getOutboundAct(InboundMessage inbound) {
        Act result = null;
        long id = inbound.getOutboundId();
        if (id != -1) {
            ArchetypeService service = getService();
            result = service.get(SMSArchetypes.MESSAGE, id, Act.class);
        }
        return result;
    }
}