/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.ReplyBuilder;
import org.openvpms.sms.message.StateBuilder;

/**
 * Default implementation of {@link ReplyBuilder}.
 *
 * @author Tim Anderson
 */
public class ReplyBuilderImpl extends ReceivedMessageBuilderImpl<ReplyBuilder> implements ReplyBuilder {

    /**
     * The parent builder.
     */
    private final StateBuilderImpl parent;


    /**
     * Constructs a {@link ReplyBuilderImpl}.
     *
     * @param parent        the parent builder
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public ReplyBuilderImpl(StateBuilderImpl parent, ArchetypeService service, DomainService domainService) {
        super(service, domainService);
        this.parent = parent;
    }

    /**
     * Adds the reply to the original message.
     * <p/>
     * This can be used when performing multiple changes to a message.
     *
     * @return the message state builder
     */
    @Override
    public StateBuilder add() {
        doBuild();
        return parent;
    }

    /**
     * Builds the message.
     *
     * @return the message
     * @throws DuplicateSMSException if the message is a duplicate
     */
    @Override
    public InboundMessage build() {
        InboundMessage result = super.build();
        parent.build();
        return result;
    }

    /**
     * Builds the message.
     *
     * @param act  the reply act
     * @param bean the reply bean
     */
    @Override
    protected void build(Act act, IMObjectBean bean) {
        super.build(act, bean);
        parent.addReply(act);
    }
}
