/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.mail;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.internal.mail.template.MailTemplate;
import org.openvpms.sms.internal.mail.template.MailTemplateFactory;
import org.openvpms.sms.internal.mail.template.TemplatedMailMessageFactory;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSProvider;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.net.ConnectException;
import java.util.Objects;

/**
 * An {@link SMSProvider} that sends SMS messages via an email-to-SMS gateway.
 */
public class MailSMSProvider implements SMSProvider {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The mail sender.
     */
    private final JavaMailSender sender;

    /**
     * Cached factory.
     */
    private MailMessageFactory factory;

    /**
     * Last used config, to avoid re-creating the factory if it hasn't changed.
     */
    private Entity lastConfig;

    /**
     * Constructs a {@link MailSMSProvider}
     *
     * @param service the archetype service
     * @param sender  the mail sender
     */
    public MailSMSProvider(IArchetypeService service, JavaMailSender sender) {
        this.service = service;
        this.sender = sender;
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     * @throws SMSException for any error
     */
    @Override
    public String getName() {
        return "Mail SMS Provider";
    }

    /**
     * Returns the SMS service archetypes that this supports.
     * <p/>
     * NOTE: for legacy reasons, the mail provider uses <em>entity.SMSConfigEmail</em>. Archetypes names are
     * case-sensitive.
     *
     * @return a list of archetypes
     * @throws SMSException for any error
     */
    @Override
    public String[] getArchetypes() {
        return service.getArchetypes(SMSArchetypes.EMAIL_CONFIGURATIONS, true).toArray(new String[0]);
    }

    /**
     * Sends an SMS.
     * <p/>
     * On successful send, the message status should be updated to {@link OutboundMessage.Status#SENT}.
     * <p/>
     * If the message cannot be sent due to an unrecoverable error, the message status should be updated to
     * {@link OutboundMessage.Status#ERROR}. It will not be resubmitted.
     * <p/>
     * If the message cannot be sent due to a temporary error, throw an {@link SMSException}; it will be resubmitted.
     *
     * @throws SMSException if the send fails and may be retried
     */
    @Override
    public void send(OutboundMessage message, Entity config) {
        if (message.getStatus() != OutboundMessage.Status.PENDING) {
            throw new IllegalStateException("Cannot send message with status=" + message.getStatus());
        }
        send(message.getPhone(), message.getMessage(), config);
        message.setStatus(OutboundMessage.Status.SENT);
    }

    /**
     * Sends an SMS.
     *
     * @param phone  the phone number to send the SMS to
     * @param text   the SMS text
     * @param config the provider configuration
     * @throws SMSException if the send fails
     */
    public void send(String phone, String text, Entity config) {
        MailMessageFactory factory = getFactory(config);
        send(phone, text, factory);
    }

    /**
     * Sends an SMS.
     *
     * @param phone   the phone number to send the SMS to
     * @param text    the SMS text
     * @param factory the mail message factory
     * @throws SMSException if the send fails
     */
    public void send(String phone, String text, MailMessageFactory factory) {
        MailMessage message = factory.createMessage(phone, text);
        MimeMessage mime = sender.createMimeMessage();
        try {
            message.copyTo(mime);
        } catch (MessagingException exception) {
            throw new SMSException(SMSMessages.failedToCreateEmail(exception.getLocalizedMessage()), exception);
        }
        try {
            sender.send(mime);
        } catch (MailAuthenticationException exception) {
            throw new SMSException(SMSMessages.mailAuthenticationFailed(exception.getLocalizedMessage()), exception);
        } catch (MailSendException exception) {
            if (exception.getCause() instanceof MessagingException) {
                // try and produce a better error for connection issues
                MessagingException mailEx = (MessagingException) exception.getCause();
                if (mailEx.getNextException() instanceof ConnectException) {
                    throw new SMSException(SMSMessages.mailConnectionFailed(mailEx.getLocalizedMessage()), exception);
                }
            }
            throw new SMSException(SMSMessages.mailSendFailed(exception.getLocalizedMessage()), exception);
        }
    }

    /**
     * Returns the mail message factory.
     *
     * @param config the mail configuration
     * @return the message factory
     */
    private synchronized MailMessageFactory getFactory(Entity config) {
        if (factory == null || lastConfig == null || !Objects.equals(lastConfig, config)
            || lastConfig.getVersion() != config.getVersion()) {
            MailTemplate template = new MailTemplateFactory(service).getTemplate(config);
            factory = new TemplatedMailMessageFactory(template);
            lastConfig = config;
        }
        return factory;
    }

}
