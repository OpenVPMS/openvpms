/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.tool;

import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Phone;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Generates an SMS and replies for an appointment.
 *
 * @author Tim Anderson
 */
@Command(name = "appointment")
public class AppointmentSMSCommand extends AbstractSMSCommand {

    /**
     * Option to generate an SMS for a particular appointment.
     */
    @Option(names="--id", description = "the appointment id", required = true)
    private Long appointmentId;

    /**
     * Option to generate an SMS for a particular appointment.
     */
    @Option(names = "--message", description = "the message")
    private String message;


    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     */
    @Override
    public Integer call() {
        int result = 1;
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        DomainService domainService = getBean(DomainService.class);
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, ScheduleArchetypes.APPOINTMENT);
        query.where(builder.equal(from.get("id"), appointmentId));
        Act appointment = service.createQuery(query).getFirstResult();
        if (appointment != null) {
            IMObjectBean bean = service.getBean(appointment);
            Party customerParty = bean.getTarget("customer", Party.class);
            Customer customer = domainService.create(customerParty, Customer.class);
            Phone phone = customer.getMobilePhone();

            Party patient = bean.getTarget("patient", Party.class);
            generateMessageWithReplies(customer, patient, phone != null ? phone.getPhoneNumber() : "123456789", message,
                                       null, appointment);
            bean.setValue("smsStatus", AppointmentStatus.SMS_SENT);
            bean.save();
            System.out.println("SMS added");
            result = 0;
        } else {
            System.err.println("Appointment not found");
        }
        return result;
    }
}