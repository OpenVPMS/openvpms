/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.mail;

import org.junit.Test;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.SMSArchetypes;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.net.ConnectException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


/**
 * Tests the {@link MailSMSProvider}.
 *
 * @author Tim Anderson
 */
public class MailSMSProviderTestCase extends AbstractSMSTest {

    /**
     * Verifies an SMS can be sent.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSend() throws Exception {
        Entity config = createConfig();
        MailSender sender = new MailSender();
        MailSMSProvider provider = new MailSMSProvider(getArchetypeService(), sender);
        provider.send("0411234567", "test", config);

        List<MimeMessage> sent = sender.getSent();
        assertEquals(1, sent.size());
        MimeMessage message = sent.get(0);
        assertEquals("test@openvpms.com", getFrom(message));
        assertEquals("61411234567@sms.com", getTo(message));
        assertEquals("subject", message.getSubject());
        assertEquals("test", getContent(message));
    }

    /**
     * Verifies an {@link SMSException} is thrown if the email cannot be created.
     */
    @Test
    public void testFailedToCreateEmail() {
        MailSender sender = new MailSender();
        MailSMSProvider provider = new MailSMSProvider(getArchetypeService(), sender);
        try {
            MailMessageFactory factory = new MailMessageFactory() {
                @Override
                public MailMessage createMessage(String phone, String text) {
                    MailMessage result = new MailMessage();
                    result.setFrom("foo");
                    result.setTo("foo");
                    result.setText(text);
                    return result;
                }

                @Override
                public int getMaxParts() {
                    return 1;
                }
            };
            provider.send("0411234567", "test", factory);
            fail("Expected SMSException to be thrown");
        } catch (SMSException expected) {
            assertEquals("SMS-0200: Failed to create email: Missing final '@domain'", expected.getLocalizedMessage());
        }
    }

    /**
     * Verifies an {@link SMSException} is thrown if there is a mail authentication exception.
     */
    @Test
    public void testMailAuthenticationException() {
        Entity config = createConfig();
        MailSender sender = new MailSender() {
            @Override
            public void send(MimeMessage mimeMessage) throws MailException {
                throw new MailAuthenticationException("foo");
            }
        };
        MailSMSProvider provider = new MailSMSProvider(getArchetypeService(), sender);
        try {
            provider.send("0411234567", "test", config);
            fail("Expected SMSException to be thrown");
        } catch (SMSException expected) {
            assertEquals("SMS-0201: Mail server authentication failed: foo", expected.getLocalizedMessage());
        }
    }

    /**
     * Verifies an {@link SMSException} is thrown if there is a mail connection exception.
     */
    @Test
    @SuppressWarnings("ThrowableInstanceNeverThrown")
    public void testMailConnectionFailed() {
        MailSender sender = new MailSender() {
            @Override
            public void send(MimeMessage mimeMessage) throws MailException {
                throw new MailSendException("foo", new MessagingException("bar", new ConnectException()));
            }
        };
        Entity config = createConfig();
        MailSMSProvider provider = new MailSMSProvider(getArchetypeService(), sender);
        try {
            provider.send("0411234567", "test", config);
            fail("Expected SMSException to be thrown");
        } catch (SMSException expected) {
            assertEquals("SMS-0202: Mail server connection failed: bar", expected.getLocalizedMessage());
        }
    }

    /**
     * Verifies an {@link SMSException} is thrown if there is a mail send exception.
     */
    @Test
    public void testMailSendFailed() {
        MailSender sender = new MailSender() {
            @Override
            public void send(MimeMessage mimeMessage) throws MailException {
                throw new MailSendException("foo");
            }
        };
        Entity config = createConfig();
        MailSMSProvider provider = new MailSMSProvider(getArchetypeService(), sender);
        try {
            provider.send("0411234567", "test", config);
            fail("Expected SMSException to be thrown");
        } catch (SMSException expected) {
            assertEquals("SMS-0203: Failed to send email: foo", expected.getLocalizedMessage());
        }
    }

    /**
     * Returns the message content.
     *
     * @param message the message
     * @return the message content
     * @throws Exception for any error
     */
    private String getContent(MimeMessage message) throws Exception {
        Object content = message.getContent();
        assertNotNull(content);
        return content.toString();
    }

    /**
     * Returns the to address.
     *
     * @param message the message
     * @return the to address
     * @throws MessagingException for any error
     */
    private String getTo(MimeMessage message) throws MessagingException {
        Address[] addresses = message.getRecipients(MimeMessage.RecipientType.TO);
        assertEquals(1, addresses.length);
        return addresses[0].toString();
    }

    /**
     * Returns the from address.
     *
     * @param message the message
     * @return the from address
     * @throws MessagingException for any error
     */
    private String getFrom(MimeMessage message) throws MessagingException {
        Address[] addresses = message.getFrom();
        assertEquals(1, addresses.length);
        return addresses[0].toString();
    }

    private Entity createConfig() {
        return createConfig("concat($phone, '@sms.com')");

    }

    private Entity createConfig(String toExpression) {
        Entity result = create(SMSArchetypes.GENERIC_SMS_EMAIL_CONFIG, Entity.class);
        IMObjectBean bean = getBean(result);
        bean.setValue("countryPrefix", "61");
        bean.setValue("areaPrefix", "0");
        bean.setValue("from", "test@openvpms.com");
        bean.setValue("toExpression", toExpression);
        bean.setValue("subject", "subject");
        return result;
    }

}
