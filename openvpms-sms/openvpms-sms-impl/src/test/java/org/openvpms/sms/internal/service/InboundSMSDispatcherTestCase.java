/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.internal.message.QueueStatus;
import org.openvpms.sms.internal.message.ReadStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link InboundSMSDispatcher}.
 *
 * @author Tim Anderson
 */
public class InboundSMSDispatcherTestCase extends AbstractAppointmentSMSTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The dispatcher.
     */
    private TestInboundSMSDispatcher dispatcher;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        completeReplies();  // mark any outstanding replies COMPLETED, so they don't interfere with the test

        // make sure there is a service user on the practice so the dispatcher can run
        practiceFactory.newPractice()
                .serviceUser()
                .build();

        // configure the appointment reminder job
        configureJob(true, "YES", "NO");

        IArchetypeService service = getArchetypeService();
        InboundSMSProcessorFactory factory = new DefaultInboundSMSProcessorFactory(service, rules);
        dispatcher = new TestInboundSMSDispatcher(
                (IArchetypeRuleService) service,
                practiceService, domainService, factory, transactionManager);
    }

    /**
     * Cleans up after the test.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        dispatcher.destroy();
    }

    /**
     * Tests the dispatcher.
     */
    @Test
    public void testDispatcher() {
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Entity schedule = createSchedule(appointmentType);

        Party customer1 = customerFactory.createCustomer();
        Party customer2 = customerFactory.createCustomer();
        Party customer3 = customerFactory.createCustomer();
        Act appointment1 = createAppointment(appointmentType, schedule, customer1);
        Act appointment2 = createAppointment(appointmentType, schedule, customer2);
        Act appointment3 = createAppointment(appointmentType, schedule, customer3);

        createSMS(customer1, appointment1, "test message 1. Reply YES to confirm", "YES");
        createSMS(customer2, appointment2, "test message 2. Reply NO to cancel", "no"); // case insensitive
        createSMS(customer3, appointment3, "test message 3", "yes");

        Act reply4 = smsFactory.newReply()
                .phone("12345789")
                .message("unsolicited reply")
                .build();

        assertTrue(dispatcher.waitForProcess(4)); // wait for all 4 replies to be processed

        checkAppointment(appointment1, AppointmentStatus.CONFIRMED, AppointmentStatus.SMS_SENT, ReadStatus.COMPLETED);
        checkAppointment(appointment2, AppointmentStatus.CANCELLED, AppointmentStatus.SMS_SENT, ReadStatus.PENDING);
        // for confirmation and cancellation, the SMS status doesn't change

        checkAppointment(appointment3, AppointmentStatus.PENDING, AppointmentStatus.SMS_RECEIVED, ReadStatus.PENDING);
        // outgoing SMS didn't contain 'yes'

        // no processor for the unsolicited reply, so should be COMPLETED, and have a PENDING read status
        reply4 = get(reply4);
        assertEquals(QueueStatus.COMPLETED.name(), reply4.getStatus2());
        assertEquals(ReadStatus.PENDING.name(), reply4.getStatus());
    }


    /**
     * Verify an appointment matches that expected.
     *
     * @param appointment the appointment
     * @param status      the expected status
     * @param smsStatus   the expected SMS status
     * @param readStatus  the expected reply read status
     */
    private void checkAppointment(Act appointment, String status, String smsStatus, ReadStatus readStatus) {
        appointment = get(appointment);
        assertEquals(status, appointment.getStatus());
        IMObjectBean bean = getBean(appointment);
        assertEquals(smsStatus, bean.getString("smsStatus"));

        // verify the associated reply has been marked COMPLETED, so it isn't reprocessed
        List<Act> sms = bean.getTargets("appointmentSMS", Act.class);
        assertEquals(1, sms.size());
        List<Act> replies = getBean(sms.get(0)).getTargets("replies", Act.class);
        assertEquals(1, replies.size());
        Act reply = replies.get(0);
        assertEquals(QueueStatus.COMPLETED.name(), reply.getStatus2());
        assertEquals(readStatus.name(), reply.getStatus());
    }

    /**
     * Marks any replies COMPLETED, so they don't interfere with tests.
     */
    private void completeReplies() {
        SMSQueryFactory factory = new SMSQueryFactory(SMSArchetypes.REPLY, getArchetypeService());
        for (Act reply : factory.create().getResultList()) {
            reply.setStatus2(QueueStatus.COMPLETED.name());
            save(reply);
        }
    }

    private static class TestInboundSMSDispatcher extends InboundSMSDispatcher {

        private final Semaphore processed = new Semaphore(0);

        /**
         * Constructs an {@link InboundSMSProcessor}.
         *
         * @param service            the archetype service
         * @param practiceService    the practice service
         * @param domainService      the domain service
         * @param factory            the factory for {@link InboundSMSProcessor} instances
         * @param transactionManager the transaction manager
         */
        public TestInboundSMSDispatcher(IArchetypeRuleService service, PracticeService practiceService,
                                        DomainService domainService, InboundSMSProcessorFactory factory,
                                        PlatformTransactionManager transactionManager) {
            super(service, practiceService, domainService, factory, transactionManager);
        }

        /**
         * Schedules {@code dispatch()} to be run, unless it is already running.
         */
        @Override
        public void schedule() {
            super.schedule();
        }

        /**
         * Waits for {@code #process()} to complete for the specified number of invocations.
         *
         * @param count the number of invocations to wait for
         * @return {@code true}  if the wait completed successfully, {@code false} if it was interrupted
         */
        public boolean waitForProcess(int count) {
            boolean result = false;
            try {
                result = processed.tryAcquire(count, 30, TimeUnit.SECONDS);
            } catch (InterruptedException exception) {
                // no-op
            }
            return result;
        }

        /**
         * Processes an object.
         *
         * @param object the inbound SMS
         * @param queue  the queue
         * @throws Exception for any error
         */
        @Override
        protected void process(Act object, SMSQueue queue) throws Exception {
            super.process(object, queue);
            processed.release();
        }
    }
}
