/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.tool;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Spec;

/**
 * Generates SMSes and replies for testing purposes.
 *
 * @author Tim Anderson
 */
@Command(name = "smsgen",
        subcommands = {AllSMSCommand.class, AppointmentSMSCommand.class, ReplySMSCommand.class})
public class TestSMSGenerator {

    /**
     * The command line specification.
     */
    @Spec
    private CommandSpec spec;

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new TestSMSGenerator());
        int result = commandLine.execute(args);
        System.exit(result);
    }
}