/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.tool;

import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.internal.SMSArchetypes;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * Generates a reply.
 *
 * @author Tim Anderson
 */
@Command(name = "reply", description = "adds a reply to an existing message")
public class ReplySMSCommand extends AbstractSMSCommand {

    /**
     * The message identifier to add the reply to.
     */
    @CommandLine.Option(names="--id", description = "the message id", required = true)
    private long smsId;

    /**
     * The message.
     */
    @CommandLine.Option(names = "--message", description = "the reply message")
    private String message;

    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     */
    @Override
    public Integer call() {
        int result = 1;
        Act sms = getSMS();
        if (sms != null) {
            TestSMSFactory factory = getBean(TestSMSFactory.class);
            factory.updateSMS(sms)
                    .addReply(getReplyDate(), message)
                    .build();
            System.out.println("Reply added");
            result = 0;
        } else {
            System.err.println("SMS not found");
        }
        return result;
    }

    /**
     * Returns the SMS.
     *
     * @return the SMS, or {@code null} if none is found
     */
    private Act getSMS() {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, SMSArchetypes.MESSAGE);
        query.where(builder.equal(from.get("id"), smsId));
        return service.createQuery(query).getFirstResult();
    }
}