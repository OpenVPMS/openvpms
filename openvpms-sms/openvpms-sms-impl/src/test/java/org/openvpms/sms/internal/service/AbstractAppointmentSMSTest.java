/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.job.appointment.TestAppointmentReminderJobBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.sms.TestSMSBuilder;
import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for appointment SMS tests.
 *
 * @author Tim Anderson
 */

public class AbstractAppointmentSMSTest extends ArchetypeServiceTest {
    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    protected TestSchedulingFactory schedulingFactory;

    /**
     * The SMS factory.
     */
    @Autowired
    protected TestSMSFactory smsFactory;

    /**
     * The user factory.
     */
    @Autowired
    protected TestUserFactory userFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Helper to create a schedule.
     *
     * @param appointmentType the appointment type
     * @return a new schedule
     */
    protected Entity createSchedule(Entity appointmentType) {
        return schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addAppointmentType(appointmentType, 1, true)
                .slotSize(15, DateUnits.MINUTES)
                .build();
    }

    /**
     * Creates an SMS linked to an appointment.
     *
     * @param customer    the customer
     * @param appointment the appointment
     * @param message     the message
     * @return the SMS act
     */
    protected Act createSMS(Party customer, Act appointment, String message) {
        return newSMS(customer, appointment, message)
                .build();
    }

    /**
     * Creates an SMS with a reply, linked to an appointment.
     *
     * @param customer    the customer
     * @param appointment the appointment
     * @param message     the message
     * @param reply       the reply
     * @return the reply act
     */
    protected Act createSMS(Party customer, Act appointment, String message, String reply) {
        TestSMSBuilder builder = newSMS(customer, appointment, message);
        builder.addReply(reply)
                .build();
        return builder.getReplies().get(0);
    }

    /**
     * Returns a pre-populated SMS builder.
     *
     * @param customer    the customer
     * @param appointment the appointment
     * @param message     the message
     * @return the builder
     */
    protected TestSMSBuilder newSMS(Party customer, Act appointment, String message) {
        return smsFactory.newSMS()
                .phone("12345678")
                .recipient(customer)
                .message(message)
                .source(appointment)
                .status(OutboundMessage.Status.SENT.name());
    }

    /**
     * Creates an appointment.
     *
     * @param appointmentType the appointment type
     * @param schedule        the schedule
     * @param customer        the customer
     * @return a new appointment
     */
    protected Act createAppointment(Entity appointmentType, Entity schedule, Party customer) {
        return schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .customer(customer)
                .patient(patientFactory.createPatient())
                .schedule(schedule)
                .appointmentType(appointmentType)
                .status(AppointmentStatus.PENDING)
                .smsStatus(AppointmentStatus.SMS_SENT)
                .build();
    }

    /**
     * Configures the appointment reminder job.
     *
     * @param processReplies     determines if replies should be processed
     * @param confirmAppointment the text to confirm appointments. May be {@code null}
     * @param cancelAppointment  the text to cancel appointments. May be {@code null}
     */
    protected void configureJob(boolean processReplies, String confirmAppointment, String cancelAppointment) {
        newJob(processReplies, confirmAppointment, cancelAppointment)
                .build();
    }

    /**
     * Returns a pre-populated appointment reminder job builder.
     *
     * @param processReplies     determines if replies should be processed
     * @param confirmAppointment the text to confirm appointments. May be {@code null}
     * @param cancelAppointment  the text to cancel appointments. May be {@code null}
     * @return a new builder
     */
    protected TestAppointmentReminderJobBuilder newJob(boolean processReplies, String confirmAppointment,
                                                       String cancelAppointment) {
        TestAppointmentReminderJobBuilder builder = new TestAppointmentReminderJobBuilder(getArchetypeService());
        builder.processReplies(processReplies)
                .confirmAppointment(confirmAppointment)
                .cancelAppointment(cancelAppointment)
                .runAs(userFactory.createUser());
        return builder;
    }

}
