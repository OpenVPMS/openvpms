/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.internal.message.InboundMessageImpl;
import org.openvpms.sms.internal.message.OutboundMessageImpl;
import org.openvpms.sms.internal.message.QueueStatus;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.ADMITTED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.BILLED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CANCELLED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CHECKED_IN;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.COMPLETED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.CONFIRMED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.NO_SHOW;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.PENDING;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.SMS_RECEIVED;
import static org.openvpms.archetype.rules.workflow.AppointmentStatus.SMS_SENT;

/**
 * Tests the {@link AppointmentReplyProcessor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AppointmentReplyProcessorTestCase extends AbstractAppointmentSMSTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;

    /**
     * Verifies appointments can be confirmed.
     * <p/>
     * NOTE that when a valid confirmation is received, the appointment smsStatus doesn't change, as there is no need
     * for users to see it.
     */
    @Test
    public void testConfirmAppointment() {
        configureJob(true, "YES", "NO");
        checkProcess("Reply YES to confirm appointment", "YES", CONFIRMED, SMS_SENT);
        checkProcess("Reply YES to confirm appointment", "Yes", CONFIRMED, SMS_SENT); // case insensitive
        checkProcess("Reply YES to confirm appointment", "yes", CONFIRMED, SMS_SENT); // case insensitive
    }

    /**
     * Verifies appointments can be cancelled.
     * <p/>
     * NOTE that when a valid cancellation is received, the appointment smsStatus doesn't change, as there is no need
     * for users to see it.
     */
    @Test
    public void testCancelAppointment() {
        configureJob(true, "YES", "NO");
        checkProcess("Reply NO to cancel appointment", "NO", CANCELLED, SMS_SENT);
        checkProcess("Reply NO to cancel appointment", "No", CANCELLED, SMS_SENT); // case insensitive
        checkProcess("Reply NO to cancel appointment", "no", CANCELLED, SMS_SENT); // case insensitive
    }

    /**
     * Verifies that when a reply is received that contains the expected text, the appointment status is changed,
     * but the appointment smsStatus changes to RECEIVED, as it needs to be reviewed.
     */
    @Test
    public void testReplyContainsExpectedText()  {
        configureJob(true, "YES", "NO");
        checkProcess("Reply YES to confirm, NO to cancel", "YES. Can we come earlier?", CONFIRMED, SMS_RECEIVED);
        checkProcess("Reply YES to confirm, NO to cancel", "NO. Can I rebook?", CANCELLED, SMS_RECEIVED);
    }

    /**
     * Verifies that when a reply is received that doesn't match that expected, the appointment status is unchanged,
     * but the appointment smsStatus changes to RECEIVED.
     */
    @Test
    public void testReplyWithoutExpectedText() {
        configureJob(true, "YES", "NO");
        checkProcess("Reply YES to confirm", "Y", PENDING, SMS_RECEIVED);
        checkProcess("Reply NO to cancel", "N", PENDING, SMS_RECEIVED);
        checkProcess("Reply YES to confirm, NO to CANCEL", "YES and NO", PENDING, SMS_RECEIVED);
    }

    /**
     * Verifies that if the original SMS doesn't have the correct confirm/cancel text, a reply with the correct
     * text doesn't trigger an appointment status change, but the appointment smsStatus changes to RECEIVED.
     * <p/>
     * This is because users can edit the SMS and could repurpose it.
     */
    @Test
    public void testReplyWithValidTextButNoMatchInOriginalSMS() {
        configureJob(true, "YES", "NO");
        checkProcess("Some random message", "YES", PENDING, SMS_RECEIVED);
        checkProcess("Some random message", "NO", PENDING, SMS_RECEIVED);
    }

    /**
     * Verifies that a confirmation after the appointment has been confirmed is ignored.
     */
    @Test
    public void testConfirmAfterConfirm() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        Act appointment = createAppointment(customer);
        assertEquals(PENDING, appointment.getStatus());

        Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
        checkProcess(appointment, sms, "YES", CONFIRMED, SMS_SENT);
        checkProcess(appointment, sms, "YES", CONFIRMED, SMS_SENT);
    }

    /**
     * Verifies that a cancellation after the appointment has been cancellation is ignored.
     */
    @Test
    public void testCancelAfterCancel() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        Act appointment = createAppointment(customer);
        assertEquals(PENDING, appointment.getStatus());

        Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
        checkProcess(appointment, sms, "NO", CANCELLED, SMS_SENT);
        checkProcess(appointment, sms, "NO", CANCELLED, SMS_SENT);
    }

    /**
     * Verifies that an appointment can be cancelled after confirmation.
     */
    @Test
    public void testCancelAfterConfirm() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        Act appointment = createAppointment(customer);
        assertEquals(PENDING, appointment.getStatus());

        Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
        checkProcess(appointment, sms, "YES", CONFIRMED, SMS_SENT);
        checkProcess(appointment, sms, "NO", CANCELLED, SMS_SENT);
    }

    /**
     * Verifies that an appointment cannot be confirmed after cancellation. The appointment status remains CANCELLED,
     * but the appointment smsStatus should change to RECEIVED.
     */
    @Test
    public void testConfirmAfterCancel() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        Act appointment = createAppointment(customer);
        assertEquals(PENDING, appointment.getStatus());

        Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
        checkProcess(appointment, sms, "NO", CANCELLED, SMS_SENT);
        checkProcess(appointment, sms, "YES", CANCELLED, SMS_RECEIVED);
    }

    /**
     * Verifies that if an appointment is CHECKED_IN, IN_PROGRESS, ADMITTED, COMPLETED, BILLED, or NO_SHOW,
     * a confirmation reply doesn't update the status to CONFIRMED, and the smsStatus is set to RECEIVED.
     */
    @Test
    public void testCannotConfirmNonConfirmed() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        for (String status : new String[]{CHECKED_IN, IN_PROGRESS, ADMITTED, COMPLETED, BILLED, NO_SHOW}) {
            Act appointment = createAppointment(customer);
            Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
            appointment.setStatus(status);
            save(appointment);
            checkProcess(appointment, sms, "YES", status, SMS_RECEIVED);
        }
    }

    /**
     * Verifies that if an appointment is CHECKED_IN, IN_PROGRESS, ADMITTED, COMPLETED, BILLED, or NO_SHOW,
     * a cancellation reply doesn't update the status to CANCELLED, and the smsStatus is set to RECEIVED.
     */
    @Test
    public void testCannotCancelNonConfirmed() {
        configureJob(true, "YES", "NO");

        Party customer = customerFactory.createCustomer();
        for (String status : new String[]{CHECKED_IN, IN_PROGRESS, ADMITTED, COMPLETED, BILLED, NO_SHOW}) {
            Act appointment = createAppointment(customer);
            Act sms = createSMS(customer, appointment, "Reply YES to confirm, NO to cancel");
            appointment.setStatus(status);
            save(appointment);
            checkProcess(appointment, sms, "NO", status, SMS_RECEIVED);
        }
    }

    /**
     * Verifies that when a reply is received and the <em>entity.jobAppointmentReminder</em> is configured to
     * not process replies, the appointment smsStatus updates to "RECEIVED"
     */
    @Test
    public void testWithProcessRepliesFalse() {
        configureJob(false, "YES", "NO");
        checkProcess("test message 1. Reply YES to confirm", "YES", PENDING, SMS_RECEIVED);
        checkProcess("test message 2. Reply NO to cancel", "no", PENDING, SMS_RECEIVED);
        checkProcess("test message 3", "yes", PENDING, SMS_RECEIVED);
    }

    /**
     * Verifies when the job is disabled, the appointment status doesn't changed, but the smsStatus is set to RECEIVED.
     */
    @Test
    public void testDisabledJob() {
        newJob(false, "YES", "NO")
                .active(false)
                .build();
        assertNull(rules.getAppointmentReminderConfig());

        checkProcess("Reply YES to confirm", "YES", PENDING, SMS_RECEIVED);
        checkProcess("Reply NO to cancel", "NO", PENDING, SMS_RECEIVED);
    }

    /**
     * Verifies that when cancellation is disabled, the the appointment status doesn't changed, but the smsStatus is
     * set to RECEIVED.
     */
    @Test
    public void testDisableCancellation() {
        configureJob(true, "YES", null);
        checkProcess("Reply YES to confirm, NO to cancel", "YES", CONFIRMED, SMS_SENT);
        checkProcess("Reply YES to confirm, NO to cancel", "Yes. May be a bit late", CONFIRMED, SMS_RECEIVED);
        checkProcess("Reply YES to confirm, NO to cancel", "NO", PENDING, SMS_RECEIVED);
        checkProcess("Reply YES to confirm, NO to cancel", "No. Can I reschedule?", PENDING, SMS_RECEIVED);
    }

    /**
     * Tests processing a reply to an appointment SMS.
     *
     * @param message   the appointment SMS
     * @param reply     the reply
     * @param status    the expected appointment status after processing
     * @param smsStatus the expected appointment smsStatus after processing
     */
    private void checkProcess(String message, String reply, String status, String smsStatus) {
        Party customer = customerFactory.createCustomer();
        Act appointment = createAppointment(customer);
        assertEquals(PENDING, appointment.getStatus());

        Act sms = createSMS(customer, appointment, message);
        checkProcess(appointment, sms, reply, status, smsStatus);
    }

    /**
     * Tests processing a reply to an appointment SMS.
     *
     * @param appointment the appointment
     * @param sms         the SMS
     * @param message     the reply
     * @param status      the expected appointment status after processing
     * @param smsStatus   the expected appointment smsStatus after processing
     */
    private void checkProcess(Act appointment, Act sms, String message, String status, String smsStatus) {
        Act reply = smsFactory.newReply()
                .message(message)
                .build();
        smsFactory.updateSMS(sms)
                .addReply(reply)
                .build();

        OutboundMessage out = new OutboundMessageImpl(sms, getArchetypeService(), domainService);
        InboundMessageImpl in = new InboundMessageImpl(reply, getArchetypeService(), domainService);

        assertEquals(QueueStatus.QUEUED, in.getQueueStatus());

        AppointmentReplyProcessor processor = new AppointmentReplyProcessor(getArchetypeService(), rules);
        processor.process(in, out, appointment);
        assertEquals(QueueStatus.QUEUED, in.getQueueStatus()); // processor doesn't update status, dispatcher does

        // check appointment statuses
        assertEquals(status, appointment.getStatus());
        assertEquals(smsStatus, getBean(appointment).getString("smsStatus"));
    }

    /**
     * Creates an appointment.
     *
     * @param customer the customer
     * @return the appointment
     */
    private Act createAppointment(Party customer) {
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Entity schedule = createSchedule(appointmentType);
        return createAppointment(appointmentType, schedule, customer);
    }
}
