/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.tool;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.sms.TestSMSBuilder;
import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import picocli.CommandLine;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Base class for SMS commands.
 *
 * @author Tim Anderson
 */
abstract class AbstractSMSCommand implements Callable<Integer> {

    /**
     * The number of replies to generate.
     */
    @CommandLine.Option(names = "--replies", description = "The number of replies per message", defaultValue = "0")
    private int replies;

    /**
     * The send date.
     */
    @CommandLine.Option(names = "--send-date", description = "The date for sent messages, in the format yyyy-mm-dd")
    private String sendDate;

    /**
     * The reply date.
     */
    @CommandLine.Option(names = "--reply-date", description = "The date for replies, in the format yyyy-mm-dd")
    private String replyDate;

    /**
     * The application context.
     */
    private ApplicationContext context;

    /**
     * Generates an SMS with replies.
     *
     * @param customer the customer
     * @param patient  the patient. May be {@code null}
     * @param phone    the phone number
     * @param message the message. If {@code null}, a dummy message will be generated
     * @param location the practice location. May be {@code null}
     * @param source   the source act. May be {@code null}
     */
    protected void generateMessageWithReplies(Party customer, Party patient, String phone, String message,
                                              Party location, Act source) {
        TestSMSFactory factory = getBean(TestSMSFactory.class);
        TestSMSBuilder builder = factory.newSMS();
        builder.message(message != null? message :
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
                        "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud " +
                        "exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")
                .location(location)
                .recipient(customer)
                .customer(customer)
                .patient(patient)
                .phone(phone)
                .source(source)
                .status(OutboundMessage.Status.SENT.name());
        Date now = new Date();
        if (sendDate != null) {
            builder.sent(DateRules.addDateTime(TestHelper.getDate(sendDate), now));
        }

        for (int i = 0; i < replies; ++i) {
            Date received = getReplyDate();
            builder.addReply(received, "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore "
                                       + "eu fugiat nulla pariatur")
                    .updated(received); // flag the original SMS as being updated when it was last received
        }
        builder.build();
    }

    /**
     * Returns the bean of the specified type.
     *
     * @param type the type of the bean
     * @return the corresponding bean
     */
    protected <T> T getBean(Class<T> type) {
        return getContext().getBean(type);
    }

    /**
     * Returns the reply date.
     *
     * @return the reply date
     */
    protected Date getReplyDate() {
        Date now = new Date();
        return (replyDate != null) ? DateRules.addDateTime(TestHelper.getDate(replyDate), now) : now;
    }

    /**
     * Returns the application context.
     *
     * @return the application context
     */
    private ApplicationContext getContext() {
        if (context == null) {
            context = new ClassPathXmlApplicationContext("applicationContext.xml");
        }
        return context;
    }
}