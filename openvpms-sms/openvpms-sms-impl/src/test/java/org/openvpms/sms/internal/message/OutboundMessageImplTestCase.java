/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.internal.mail.AbstractSMSTest;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessage.Status;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.sms.message.OutboundMessage.Status.DELIVERED;
import static org.openvpms.sms.message.OutboundMessage.Status.ERROR;
import static org.openvpms.sms.message.OutboundMessage.Status.EXPIRED;
import static org.openvpms.sms.message.OutboundMessage.Status.PENDING;
import static org.openvpms.sms.message.OutboundMessage.Status.SENT;

/**
 * Tests the {@link OutboundMessageImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OutboundMessageImplTestCase extends AbstractSMSTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The builder.
     */
    private OutboundMessageBuilderImpl builder;

    /**
     * Test recipient.
     */
    private Party recipient;

    /**
     * Test location.
     */
    private Party location;

    /**
     * Test customer.
     */
    private Party customer;

    /**
     * Test patient.
     */
    private Party patient;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        builder = new OutboundMessageBuilderImpl(getArchetypeService(), domainService);
        recipient = supplierFactory.createVet();
        location = practiceFactory.createLocation();
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient();
    }

    /**
     * Verifies a message can transition from PENDING to SENT to DELIVERED using {@link OutboundMessageImpl#state()}.
     * <p/>
     * The {@link OutboundMessageImpl#getUpdated()} timestamp should change on each state change
     */
    @Test
    public void testUpdateStatus() {
        String id = UUID.randomUUID().toString();
        OutboundMessageImpl message = createMessage();
        OffsetDateTime updated = backdateUpdated(message);
        // workaround to backdate the timestamp, so changes can be detected

        assertEquals(PENDING, message.getStatus());
        assertEquals(updated, message.getUpdated());

        message.state()
                .providerId("actIdentity.smsTest", id)
                .status(SENT)
                .build();
        checkStatus(message, SENT, null);
        assertEquals(id, message.getProviderId());
        assertNotEquals(updated, message.getUpdated());  // verify the timestamp has changed

        updated = backdateUpdated(message);
        message.state()
                .providerId("actIdentity.smsTest", id) // should be able to restate the id without issue
                .status(DELIVERED, "Delivered to handset")
                .build();

        checkStatus(message, DELIVERED, "Delivered to handset");
        assertEquals(id, message.getProviderId());
        assertNotEquals(updated, message.getUpdated());  // verify the timestamp has changed
    }

    /**
     * Tests setting the status.
     */
    @Test
    public void testStatus() {
        OutboundMessageImpl message = createMessage();
        assertEquals(PENDING, message.getStatus());

        message.setStatus(SENT);
        checkStatus(message, SENT, null);

        message.setStatus(DELIVERED, "Delivered to handset");
        checkStatus(message, DELIVERED, "Delivered to handset");
    }

    /**
     * Verifies that a message can only transition from ERROR to REVIEWED status.
     */
    @Test
    public void testErrorStatus() {
        OutboundMessageImpl message = createMessage();
        message.setStatus(ERROR, "No credit");
        checkStatus(message, ERROR, "No credit");

        checkNoStatusTransition(message, PENDING, SENT, EXPIRED, DELIVERED);

        message.setStatus(Status.REVIEWED);
        checkStatus(message, Status.REVIEWED, null);

        checkNoStatusTransition(message, PENDING, SENT, EXPIRED, DELIVERED, ERROR);
    }

    /**
     * Verifies a {@link DuplicateSMSException} is raised if the provider id is a duplicate.
     */
    @Test
    public void testDuplicate() {
        String id = UUID.randomUUID().toString();
        createMessage()
                .state()
                .providerId("actIdentity.smsTest", id)
                .build();

        try {
            createMessage()
                    .state()
                    .providerId("actIdentity.smsTest", id)
                    .build();
            fail("Expected DuplicateSMSException");
        } catch (DuplicateSMSException expected) {
            assertEquals("SMS-0011: There is already an outbound message with identifier: " + id,
                         expected.getMessage());
        }
    }

    /**
     * Tests adding replies to a message via the {@link OutboundMessageImpl#state()} method.
     * <p/>
     * The {@link OutboundMessageImpl#getUpdated()} timestamp should change when a reply is added.
     */
    @Test
    public void testReplies() {
        OutboundMessageImpl message = createMessage();
        OffsetDateTime updated = backdateUpdated(message);
        // workaround to backdate the timestamp, so changes can be detected

        String id1 = UUID.randomUUID().toString();
        String id2 = UUID.randomUUID().toString();
        message.state()
                .status(SENT)
                .reply()
                .providerId("actIdentity.smsTest", id1)
                .phone("1")
                .message("reply 1")
                .add()
                .reply()
                .providerId("actIdentity.smsTest", id2)
                .phone("2")
                .message("reply 2")
                .add()
                .build();
        checkStatus(message, SENT, null);
        List<InboundMessage> replies = message.getReplies();
        assertEquals(2, replies.size());
        checkReply(replies, id1, "1", "reply 1", message);
        checkReply(replies, id2, "2", "reply 2", message);
        assertNotEquals(updated, message.getUpdated());
    }

    /**
     * Verifies a duplicate reply cannot be added to a message.
     */
    @Test
    public void testDuplicateReplySameMessage() {
        OutboundMessageImpl message = createMessage();
        String id = UUID.randomUUID().toString();
        message.state()
                .reply()
                .providerId("actIdentity.smsTest", id)
                .phone("1")
                .message("reply 1")
                .build();

        try {
            message.state()
                    .reply()
                    .providerId("actIdentity.smsTest", id)
                    .phone("1")
                    .message("reply 1")
                    .build();
            fail("Expected DuplicateSMSException");
        } catch (DuplicateSMSException expected) {
            assertEquals("SMS-0030: There is already an inbound message with identifier: " + id, expected.getMessage());
        }
    }

    /**
     * Verifies a duplicate reply cannot be added to a message, within the one call to
     * {@link OutboundMessageImpl#state()}.
     */
    @Test
    public void testDuplicateReplyInSameBuild() {
        OutboundMessageImpl message = createMessage();
        String id = UUID.randomUUID().toString();
        try {
            message.state()
                    .reply()
                    .providerId("actIdentity.smsTest", id)
                    .phone("1")
                    .message("reply 1")
                    .add()
                    .reply()
                    .providerId("actIdentity.smsTest", id)
                    .phone("1")
                    .message("reply 1")
                    .build();
            fail("Expected DuplicateSMSException");
        } catch (DuplicateSMSException expected) {
            assertEquals("SMS-0030: There is already an inbound message with identifier: " + id, expected.getMessage());
        }
    }

    /**
     * Verifies a reply with the same as that as an existing reply cannot be added to a different message.
     */
    @Test
    public void testDuplicateReplyDifferentMessage() {
        OutboundMessageImpl message1 = createMessage();
        OutboundMessageImpl message2 = createMessage();
        String id = UUID.randomUUID().toString();
        message1.state()
                .reply()
                .providerId("actIdentity.smsTest", id)
                .phone("1")
                .message("reply 1")
                .build();

        try {
            message2.state()
                    .reply()
                    .providerId("actIdentity.smsTest", id)
                    .phone("1")
                    .message("reply 1")
                    .build();
            fail("Expected DuplicateSMSException");
        } catch (DuplicateSMSException expected) {
            assertEquals("SMS-0030: There is already an inbound message with identifier: " + id, expected.getMessage());
        }
    }

    /**
     * Verifies a reply is present in a list of replies.
     *
     * @param replies the replies
     * @param id      the provider id
     * @param phone   the phone
     * @param text    the message text
     * @param message the outbound message
     */
    private void checkReply(List<InboundMessage> replies, String id, String phone, String text,
                            OutboundMessage message) {
        boolean found = false;
        for (InboundMessage reply : replies) {
            if (id.equals(reply.getProviderId())) {
                checkMessage(reply, id, phone, text, customer, patient, location, message);
                found = true;
            }
        }
        assertTrue(found);
    }


    /**
     * Verifies that a message cannot transition to a status.
     *
     * @param message  the message
     * @param statuses the statuses to check
     */
    private void checkNoStatusTransition(OutboundMessageImpl message, Status... statuses) {
        Status original = message.getStatus();
        for (Status status : statuses) {
            try {
                message.setStatus(status);
            } catch (IllegalStateException exception) {
                assertEquals("Cannot transition from " + original + " to " + status, exception.getMessage());
            }
        }
    }

    /**
     * Checks the status of a message.
     *
     * @param message       the message
     * @param status        the expected status
     * @param statusMessage the expected status message. May be {@code null}
     */
    private void checkStatus(OutboundMessageImpl message, Status status, String statusMessage) {
        assertEquals(status, message.getStatus());
        assertEquals(statusMessage, message.getStatusMessage());
        Act act = getAct(message);
        assertEquals(status.toString(), act.getStatus());
        IMObjectBean bean = getBean(act);
        assertEquals(statusMessage, bean.getString("statusMessage"));
    }

    /**
     * Creates a new message.
     *
     * @return a new message
     */
    private OutboundMessageImpl createMessage() {
        return (OutboundMessageImpl) builder.message("a message")
                .phone("123456")
                .recipient(recipient)
                .location(location)
                .customer(customer)
                .patient(patient)
                .build();
    }

    /**
     * Backdate the updated timestamp in order to detect changes.
     *
     * @param message the message
     * @return the updated timestamp
     */
    private OffsetDateTime backdateUpdated(OutboundMessageImpl message) {
        return backdateUpdated(message.getAct());
    }
}