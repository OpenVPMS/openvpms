/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.tool;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.service.practice.PracticeService;
import picocli.CommandLine.Command;

import java.util.List;

/**
 * Generate SMS for all customers with mobiles.
 *
 * @author Tim Anderson
 */
@Command(name = "--all")
public class AllSMSCommand extends AbstractSMSCommand {

    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     */
    @Override
    public Integer call() {
        generate();
        return 0;
    }

    /**
     * Generates SMS messages and replies for each customer with a mobile.
     */
    private void generate() {
        int count = 0;
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        DomainService domainService = getBean(DomainService.class);
        List<Location> locations = getBean(PracticeService.class).getLocations();

        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> root = query.from(Party.class, CustomerArchetypes.PERSON);
        query.orderBy(builder.asc(root.get("id")));
        TypedQueryIterator<Party> iterator = new TypedQueryIterator<>(service.createQuery(query), 100);
        int index = 0;
        while (iterator.hasNext()) {
            Customer customer = domainService.create(iterator.next(), Customer.class);
            Phone phone = customer.getMobilePhone();
            if (phone != null) {
                if (index >= locations.size()) {
                    index = 0;
                }
                Patient patient = customer.getPatients().active().getObject();
                Location location = index < locations.size() ? locations.get(index++) : null;
                generateMessageWithReplies(customer, patient, phone.getPhoneNumber(), null, location, null);
                count++;
            }
        }
        System.out.println("Generated " + count + " SMS");
    }
}