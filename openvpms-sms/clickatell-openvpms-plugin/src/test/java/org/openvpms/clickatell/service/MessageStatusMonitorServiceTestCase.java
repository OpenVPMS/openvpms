/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.service;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Rule;
import org.junit.Test;
import org.openvpms.clickatell.AbstractClickatellTest;
import org.openvpms.clickatell.TestClickatellFactory;
import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.service.MessageStatusUpdater;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.message.OutboundMessage;

import javax.ws.rs.core.HttpHeaders;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link MessageStatusMonitorService}.
 *
 * @author Tim Anderson
 */
public class MessageStatusMonitorServiceTestCase extends AbstractClickatellTest {

    /**
     * Sets up a WireMock service.
     */
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Options.DYNAMIC_PORT);

    /**
     * Tests updating message statuses.
     *
     * @throws InterruptedException if the test times out
     */
    @Test
    public void testUpdate() throws InterruptedException {
        String url = "http://localhost:" + wireMockRule.port() + "/";

        ArchetypeService archetypeService = getPluginArchetypeService();
        TestClickatellFactory factory = new TestClickatellFactory(archetypeService, url, null);

        String id1 = createMessage(OutboundMessage.Status.PENDING, null);
        String id2 = createMessage(OutboundMessage.Status.SENT, "QUEUED");
        String id3 = createMessage(OutboundMessage.Status.SENT, MessageStatusUpdater.RECEIVED_BY_RECIPIENT);
        String id4 = createMessage(OutboundMessage.Status.SENT, MessageStatusUpdater.EXPIRED);
        String id5 = createMessage(OutboundMessage.Status.EXPIRED, null);
        String id6 = createMessage(OutboundMessage.Status.ERROR, "some invalid status");
        String id7 = createMessage(OutboundMessage.Status.DELIVERED, MessageStatusUpdater.RECEIVED_BY_RECIPIENT);
        String id8 = createMessage(OutboundMessage.Status.REVIEWED, "some invalid status");
        String id9 = createMessage(OutboundMessage.Status.SENT, MessageStatusUpdater.DELIVERED_TO_GATEWAY);
        String id10 = createMessage(OutboundMessage.Status.SENT, MessageStatusUpdater.ERROR_DELIVERING);

        Semaphore waiter = new Semaphore(0);
        MutableInt updated = new MutableInt();
        MessageStatusMonitorService service =
                new MessageStatusMonitorService(archetypeService, getMessages(), factory) {
                    @Override
                    protected void update(MessageStatusUpdater updater) {
                        int changed = updater.update();
                        updated.setValue(changed);
                        waiter.release();
                    }
                };

        try {
            service.setConfiguration(createConfig());
            waiter.tryAcquire(10000, TimeUnit.SECONDS);

            assertEquals(4, updated.intValue());

            check(id1, OutboundMessage.Status.PENDING);
            check(id2, OutboundMessage.Status.SENT);
            check(id3, OutboundMessage.Status.DELIVERED);
            check(id4, OutboundMessage.Status.EXPIRED);
            check(id5, OutboundMessage.Status.EXPIRED);
            check(id6, OutboundMessage.Status.ERROR);
            check(id7, OutboundMessage.Status.DELIVERED);
            check(id8, OutboundMessage.Status.REVIEWED);
            check(id9, OutboundMessage.Status.DELIVERED);
            check(id10, OutboundMessage.Status.ERROR);
        } finally {
            service.destroy();
        }
    }

    /**
     * Creates a message.
     *
     * @param status         the current message status
     * @param responseStatus the status to returning when simulating Clickatell calls
     * @return the message identifier
     */
    private String createMessage(OutboundMessage.Status status, String responseStatus) {
        OutboundMessage message = getMessages().getOutboundMessageBuilder()
                .phone("1234567").message("this is a test message").build();
        String id = UUID.randomUUID().toString();
        message.state()
                .status(status)
                .providerId(ClickatellArchetypes.ID, id)
                .build();
        if (status != OutboundMessage.Status.PENDING) {
            message.setStatus(status);
        }
        message.state().providerId(ClickatellArchetypes.ID, id).build();
        if (responseStatus != null) {
            stubFor(WireMock.get(urlPathMatching("/public-client/message/status"))
                            .withQueryParam("messageId", equalTo(id))
                            .willReturn(aResponse()
                                                .withStatus(200)
                                                .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                                .withBody("{\"status\":\"" + responseStatus + "\"}")));
        }

        return id;
    }

    /**
     * Verifies a message has the expected status.
     *
     * @param id       the message id
     * @param expected the expected status
     */
    private void check(String id, OutboundMessage.Status expected) {
        OutboundMessage message = getMessages().getOutboundMessage(ClickatellArchetypes.ID, id);
        assertNotNull(message);
        assertEquals(expected, message.getStatus());
    }
}
