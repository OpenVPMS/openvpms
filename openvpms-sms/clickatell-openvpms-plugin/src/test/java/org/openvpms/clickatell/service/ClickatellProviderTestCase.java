/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.service;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.openvpms.clickatell.AbstractClickatellTest;
import org.openvpms.clickatell.TestClickatellFactory;
import org.openvpms.clickatell.internal.model.SMS;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.OutboundMessage;

import javax.ws.rs.core.HttpHeaders;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link ClickatellProvider}.
 *
 * @author Tim Anderson
 */
public class ClickatellProviderTestCase extends AbstractClickatellTest {

    /**
     * Sets up a WireMock service.
     */
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Options.DYNAMIC_PORT);

    /**
     * Tracks the sent messages.
     */
    private final List<SMS> sent = new ArrayList<>();

    /**
     * Tests sending an SMS message.
     */
    @Test
    public void testSend() {
        ClickatellProvider provider = createProvider();
        String id = UUID.randomUUID().toString();

        String response = "{\"messages\":[{\"apiMessageId\":\"" + id + "\",\"accepted\":true," +
                          "\"to\":\"61412345678\",\"errorCode\":null,\"error\":null,\"errorDescription\":null}]}\n";
        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(202)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("0412345678")
                .message("this is a test message").build();
        Entity config = createConfig();
        provider.send(message, config);
        assertEquals(OutboundMessage.Status.SENT, message.getStatus());
        assertNull(message.getStatusMessage());
        assertEquals(id, message.getProviderId());

        // verify the message was sent with no validity period
        assertEquals(1, sent.size());
        assertNull(sent.get(0).getValidityPeriod());
    }

    /**
     * Tests sending an SMS message with an expiry time.
     */
    @Test
    public void testSendWithExpiry() {
        ClickatellProvider provider = createProvider();
        String id = UUID.randomUUID().toString();

        String response = "{\"messages\":[{\"apiMessageId\":\"" + id + "\",\"accepted\":true," +
                          "\"to\":\"61412345678\",\"errorCode\":null,\"error\":null,\"errorDescription\":null}]}\n";
        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(202)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("0412345678")
                .message("this is a test message").expiry(OffsetDateTime.now().plusMinutes(30)).build();
        Entity config = createConfig();
        provider.send(message, config);
        assertEquals(OutboundMessage.Status.SENT, message.getStatus());
        assertNull(message.getStatusMessage());
        assertEquals(id, message.getProviderId());

        // verify the message was sent with a validity period
        assertEquals(1, sent.size());
        Integer validityPeriod = sent.get(0).getValidityPeriod();
        assertNotNull(validityPeriod);
        assertTrue(validityPeriod >= 29 && validityPeriod <= 30);
    }

    /**
     * Tests when an incorrect API key is used.
     */
    @Test
    public void testInvalidAPIKey() {
        ClickatellProvider provider = createProvider();

        String response = "{\"messages\":[],\"errorCode\":108,\"error\":\"Invalid or missing integration API Key.\"," +
                          "\"errorDescription\":\"The integration API key is either incorrect or has not been " +
                          "included in the API call.\"}\n";
        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("0412345678")
                .message("this is a test message").expiry(OffsetDateTime.now().plusDays(1)).build();
        Entity config = createConfig();

        try {
            provider.send(message, config);
            fail("Expected send to fail");
        } catch (SMSException expected) {
            assertEquals("CLICKATELL-0002: Failed to send SMS. Clickatell responded with error 108 - Invalid or " +
                         "missing integration API Key.\n" +
                         "The integration API key is either incorrect or has not been included in the API call.",
                         expected.getMessage());
            assertEquals(OutboundMessage.Status.PENDING, message.getStatus());
        }
    }

    /**
     * Tests when an invalid phone number is used.
     */
    @Test
    public void testInvalidPhoneNumber() {
        ClickatellProvider provider = createProvider();

        String response = "{\"messages\":[{\"apiMessageId\":null,\"accepted\":false,\"to\":\"61a\",\"errorCode\":105," +
                          "\"error\":\"Invalid destination address.\"," +
                          "\"errorDescription\":\"Error while parsing destination number - 61a.\"}]}\n";

        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(202)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("a")
                .message("this is a test message").expiry(OffsetDateTime.now().plusDays(1)).build();
        Entity config = createConfig();

        provider.send(message, config);
        assertEquals(OutboundMessage.Status.ERROR, message.getStatus());
        assertEquals("105 - Invalid destination address.\n" +
                     "Error while parsing destination number - 61a.",
                     message.getStatusMessage());
    }

    /**
     * Tests when a number is too long.
     * <p/>
     * Clickatell handles these differently, returning a 400 Bad Request status code.
     */
    @Test
    public void testNumberTooLong() {
        ClickatellProvider provider = createProvider();
        String response = "{\"responseCode\":400,\"messages\":[{\"apiMessageId\":null,\"accepted\":false," +
                          "\"to\":\"+6140312345678901\",\"errorCode\":651,\"error\":\"Too long input phone.\"," +
                          "\"errorDescription\":\"Input phone %s is invalid, as it's too long.\"}]}";

        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(400)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("40312345678901")
                .message("this is a test message").expiry(OffsetDateTime.now().plusDays(1)).build();
        Entity config = createConfig();

        provider.send(message, config);
        assertEquals(OutboundMessage.Status.ERROR, message.getStatus());
        assertEquals("651 - Too long input phone.\n" +
                     "Input phone %s is invalid, as it's too long.",
                     message.getStatusMessage());
    }

    /**
     * Verifies an {@link SMSException} is thrown if the from no. is invalid.
     */
    @Test
    public void testInvalidFromNumber() {
        ClickatellProvider provider = createProvider();

        String response = "{\"messages\":[],\"errorCode\":607,\"error\":\"Invalid FROM number.\"," +
                          "\"errorDescription\":\"User specified FROM number, but integration isn't two-way.\"}\n";

        stubFor(WireMock.post(urlEqualTo("/messages"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("a")
                .message("this is a test message").expiry(OffsetDateTime.now().plusDays(1)).build();
        Entity config = createConfig("04987654321");

        try {
            provider.send(message, config);
            fail("Expected send to fail");
        } catch (SMSException expected) {
            assertEquals("CLICKATELL-0002: Failed to send SMS. Clickatell responded with error 607 - Invalid FROM " +
                         "number.\n" +
                         "User specified FROM number, but integration isn't two-way.",
                         expected.getMessage());
            assertEquals(OutboundMessage.Status.PENDING, message.getStatus());
        }
    }

    /**
     * Verifies that a message is expired immediately if it has an expiry time in the past.
     */
    @Test
    public void testExpireOnSend() {
        OutboundMessage message = getMessages().getOutboundMessageBuilder().phone("0412345678")
                .message("this is a test message").expiry(OffsetDateTime.now()).build();
        Entity config = createConfig();
        ClickatellProvider provider = createProvider();
        provider.send(message, config);
        assertEquals(OutboundMessage.Status.EXPIRED, message.getStatus());
        assertNull(message.getStatusMessage());
        assertNull(message.getProviderId());

        assertEquals(0, sent.size());
    }

    /**
     * Creates a {@link ClickatellProvider} that connects to the WireMock port.
     *
     * @return a new provider
     */
    private ClickatellProvider createProvider() {
        ArchetypeService service = getPluginArchetypeService();
        String url = "http://localhost:" + wireMockRule.port() + "/";

        TestClickatellFactory factory = new TestClickatellFactory(service, url, sent);
        return new ClickatellProvider(service, factory);
    }
}
