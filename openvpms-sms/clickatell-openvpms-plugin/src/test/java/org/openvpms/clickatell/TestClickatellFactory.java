/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell;

import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.client.ClickatellClient;
import org.openvpms.clickatell.internal.model.SMS;
import org.openvpms.clickatell.internal.model.SMSResponses;
import org.openvpms.clickatell.internal.service.ClickatellFactory;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.List;

/**
 * Test {@link ClickatellFactory}.
 *
 * @author Tim Anderson
 */
public class TestClickatellFactory extends ClickatellFactory {

    /**
     * The Clickatell url.
     */
    private final String url;

    /**
     * Collects messages.
     */
    private final List<SMS> messages;

    /**
     * Constructs a {@link TestClickatellFactory}.
     *
     * @param service  the archetype service
     * @param url      the Clickatell URL to use
     * @param messages if non-null, collects sent messages
     */
    public TestClickatellFactory(ArchetypeService service, String url, List<SMS> messages) {
        super(service, new PasswordEncryptor() {
            @Override
            public String encrypt(String password) {
                return password;
            }

            @Override
            public String decrypt(String encryptedPassword) {
                return encryptedPassword;
            }
        });
        this.url = url;
        this.messages = messages;
    }

    /**
     * Returns the Clickatell API client.
     *
     * @param apiKey the API id
     * @return a new client
     */
    @Override
    protected Clickatell create(String apiKey) {
        return new ClickatellClient(apiKey, url) {
            @Override
            public SMSResponses send(SMS message) {
                SMSResponses result = super.send(message);
                if (messages != null) {
                    messages.add(message);
                }
                return result;
            }
        };
    }
}
