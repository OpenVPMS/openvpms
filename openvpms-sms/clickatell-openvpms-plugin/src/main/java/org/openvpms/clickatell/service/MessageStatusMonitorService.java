/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.service;

import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.service.ClickatellFactory;
import org.openvpms.clickatell.internal.service.MessageStatusUpdater;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.plugin.service.config.ConfigurableService;
import org.openvpms.sms.message.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.time.OffsetDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Periodically checks to see if any SENT messages have been delivered or expired by Clickatell, updating their statuses
 * accordingly.
 *
 * @author Tim Anderson
 */
public class MessageStatusMonitorService implements ConfigurableService, DisposableBean {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The messages service.
     */
    private final Messages messages;

    /**
     * The Clickatell client factory.
     */
    private final ClickatellFactory factory;

    /**
     * The poll executor.
     */
    private final ScheduledExecutorService executor;

    /**
     * Thread id name counter.
     */
    private final AtomicLong counter = new AtomicLong(0);

    /**
     * Future used to cancel polling.
     */
    private ScheduledFuture<?> future;

    /**
     * The number of days to monitor.
     */
    private int days;

    /**
     * The frequency to poll (minutes).
     */
    private static final int MONITOR_FREQUENCY = 60;

    /**
     * The message status updater.
     */
    private MessageStatusUpdater updater;

    /**
     * The current configuration.
     */
    private IMObject config;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MessageStatusMonitorService.class);

    /**
     * Constructs a {@link MessageStatusMonitorService}.
     *
     * @param service  the archetype service
     * @param messages the messages
     * @param factory  the Clickatell client factory
     */
    public MessageStatusMonitorService(ArchetypeService service, Messages messages,
                                       ClickatellFactory factory) {
        this.service = service;
        this.messages = messages;
        this.factory = factory;
        executor = Executors.newSingleThreadScheduledExecutor(
                runnable -> new Thread(runnable, "ClickatellMessageStatusMonitor" + counter.incrementAndGet()));
    }

    /**
     * Returns the archetype that this service is configured with.
     *
     * @return the archetype short name
     */
    @Override
    public String getArchetype() {
        return ClickatellArchetypes.CONFIG;
    }

    /**
     * Invoked when the service is registered, and each time the configuration is updated.
     *
     * @param config may be {@code null}, if no configuration exists, or the configuration is deactivated or removed
     */
    @Override
    public synchronized void setConfiguration(IMObject config) {
        this.config = config;
        stopMonitoring();
        if (config != null) {
            IMObjectBean bean = service.getBean(config);
            boolean monitor = bean.getBoolean("monitorStatus");
            if (monitor) {
                days = bean.getInt("monitorDays");
                startMonitoring();
            }
        }
    }

    /**
     * Returns the configuration.
     *
     * @return the configuration. May be {@code null}
     */
    @Override
    public synchronized IMObject getConfiguration() {
        return config;
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        stopMonitoring();
    }

    /**
     * Updates message statuses.
     *
     * @param updater the status updater
     */
    protected void update(MessageStatusUpdater updater) {
        updater.update();
    }

    /**
     * Monitors SENT messages for status changes, and updates those that have changed.
     */
    private void monitor() {
        try {
            MessageStatusUpdater m = null;
            synchronized (this) {
                if (config != null) {
                    Clickatell client = factory.create(config);
                    m = new MessageStatusUpdater(client, messages, OffsetDateTime.now().minusDays(days));
                    if (log.isInfoEnabled()) {
                        log.info("Checking message statuses, daysToSync=" + days + ", monitorFrequency="
                                 + MONITOR_FREQUENCY);
                    }
                    this.updater = m;
                }
            }
            if (m != null) {
                update(m);
            }
        } catch (Throwable exception) {
            log.error("Synchronisation with Clickatell failed", exception);
        }
    }

    /**
     * Stops synchronisation.
     */
    private synchronized void stopMonitoring() {
        if (updater != null) {
            updater.stop();
            updater = null;
        }
        if (future != null) {
            future.cancel(true);
        }
    }

    /**
     * Starts monitoring.
     */
    private void startMonitoring() {
        stopMonitoring();
        future = executor.scheduleAtFixedRate(this::monitor, 0, MONITOR_FREQUENCY, TimeUnit.MINUTES);
    }

}
