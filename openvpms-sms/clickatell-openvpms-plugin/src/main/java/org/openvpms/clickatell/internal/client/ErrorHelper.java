/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.client;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.clickatell.internal.model.SMSResponse;

/**
 * Formats Clickatell errors.
 *
 * @author Tim Anderson
 */
public class ErrorHelper {

    /**
     * Formats an error.
     *
     * @param response the response
     * @return the formatted error
     */
    public static String formatError(SMSResponse response) {
        return formatError(response.getErrorCode(), response.getError(), response.getErrorDescription());
    }

    /**
     * Formats an error.
     *
     * @param code        the error code
     * @param error       the error
     * @param description the error description
     * @return the formatted error
     */
    public static String formatError(String code, String error, String description) {
        StringBuilder result = new StringBuilder();
        if (!StringUtils.isEmpty(code)) {
            result.append(code).append(" - ");
        }
        if (!StringUtils.isEmpty(error)) {
            result.append(error);
            if (!StringUtils.isEmpty(description)) {
                result.append("\n");
                result.append(description);
            }
        }
        return result.toString();
    }
}