/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.service;

import org.apache.commons.lang3.time.StopWatch;
import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.model.SMSStatus;
import org.openvpms.sms.message.Messages;
import org.openvpms.sms.message.OutboundMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Checks to see if any SENT messages have been delivered or expired by Clickatell, updating their statuses accordingly.
 *
 * @author Tim Anderson
 */
public class MessageStatusUpdater {

    /**
     * The Clickatell client.
     */
    private final Clickatell clickatell;

    /**
     * The message service.
     */
    private final Messages messages;

    /**
     * Process all SENT messages sent on/after this time.
     */
    private final OffsetDateTime from;

    /**
     * Flag to indicate if updates should stop.
     */
    private volatile boolean stop;

    /**
     * Status indicating message has been received by the recipient.
     */
    public static final String RECEIVED_BY_RECIPIENT = "RECEIVED_BY_RECIPIENT";

    /**
     * Status indicating message has expired.
     */
    public static final String EXPIRED = "EXPIRED";

    /**
     * Status indicating message has been delivered to the gateway.
     */
    public static final String DELIVERED_TO_GATEWAY = "DELIVERED_TO_GATEWAY";

    /**
     * Status indicating a message cannot be delivered.
     */
    public static final String ERROR_DELIVERING = "ERROR_DELIVERING";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MessageStatusUpdater.class);

    /**
     * Constructs a {@link MessageStatusUpdater}.
     *
     * @param clickatell the Clickatell client
     * @param messages   the messages service
     * @param from       processes all SENT messages sent on/after this time
     */
    public MessageStatusUpdater(Clickatell clickatell, Messages messages, OffsetDateTime from) {
        this.clickatell = clickatell;
        this.messages = messages;
        this.from = from;
    }

    /**
     * Runs the update.
     *
     * @return the number of updated messages
     */
    public int update() {
        Set<Long> seen = new HashSet<>();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int passes = 0;
        int updated = 0;
        try {
            while (!stop) {
                passes++;
                int changed = update(seen);
                if (changed == 0) {
                    break;
                }
                updated += changed;
            }
        } catch (Throwable exception) {
            log.error("Failed to update SMS message statuses: {}", exception.getMessage(), exception);
        }
        stopWatch.stop();
        log.debug("Processed={}" + ", updated={}, passes={} in {}", seen.size(), updated, passes, stopWatch);
        return updated;
    }

    /**
     * Indicates to stop updating.
     */
    public void stop() {
        stop = true;
    }

    /**
     * Updates the status of messages that have been sent since the {@code from} time.
     * <p/>
     * As any status changes will change the paged iteration over the messages, this needs to be called multiple
     * times to ensure messages aren't missed.
     *
     * @param seen the set of seen message identifiers, to avoid processing the same message twice
     * @return the number of updated messages
     */
    protected int update(Set<Long> seen) {
        int updated = 0;
        Iterable<OutboundMessage> sent = messages.getSent(from, ClickatellArchetypes.ID);
        for (OutboundMessage message : sent) {
            if (stop) {
                break;
            }
            if (seen.add(message.getId())) {
                try {
                    if (update(message)) {
                        updated++;
                    }
                } catch (Exception exception) {
                    log.debug("Failed to update message={}: {}", message.getId(), exception.getMessage(), exception);
                }
            }
        }
        return updated;
    }

    /**
     * Updates a message.
     *
     * @param message the message to update
     * @return {@code true} if the message was updated, otherwise {@code false}
     */
    private boolean update(OutboundMessage message) {
        boolean result = false;
        String messageId = message.getProviderId();
        if (messageId != null) {
            String status = getStatus(messageId);
            if (RECEIVED_BY_RECIPIENT.equals(status) || DELIVERED_TO_GATEWAY.equals(status)) {
                result = changeStatus(message, OutboundMessage.Status.DELIVERED);
            } else if (EXPIRED.equals(status)) {
                result = changeStatus(message, OutboundMessage.Status.EXPIRED);
            } else if (ERROR_DELIVERING.equals(status)) {
                result = changeStatus(message, OutboundMessage.Status.ERROR);
            } else {
                log.debug("Cannot update message={} with Clickatell Id={}, Clickatell status={}",
                          message.getId(), messageId, status);
            }
        }
        return result;
    }

    /**
     * Changes the status of a message.
     *
     * @param message the message
     * @param status  the new status
     * @return {@code true} if the message was updated, otherwise {@code false}
     */
    private boolean changeStatus(OutboundMessage message, OutboundMessage.Status status) {
        boolean result = false;
        if (message.canTransition(status)) {
            message.setStatus(status);
            result = true;
        } else {
            log.debug("Cannot update message={} with Clickatell Id={}, status={} to new status={}",
                      message.getId(), message.getProviderId(), message.getStatus(), status);
        }
        return result;
    }

    /**
     * Returns the SMS status for a message.
     *
     * @param messageId the message identifier
     * @return the status, or {@code null} if the message cannot be found
     */
    private String getStatus(String messageId) {
        String result = null;
        try {
            SMSStatus status = clickatell.getStatus(messageId);
            if (status != null) {
                result = status.getStatus();
            }
        } catch (NotFoundException ignore) {
            log.warn("failed to get status for message={}", messageId);
        }
        return result;
    }
}
