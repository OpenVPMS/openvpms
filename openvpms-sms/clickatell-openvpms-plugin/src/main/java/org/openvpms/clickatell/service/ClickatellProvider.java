/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.service;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.client.ErrorHelper;
import org.openvpms.clickatell.internal.i18n.ClickatellMessages;
import org.openvpms.clickatell.internal.model.SMS;
import org.openvpms.clickatell.internal.model.SMSResponse;
import org.openvpms.clickatell.internal.model.SMSResponses;
import org.openvpms.clickatell.internal.service.ClickatellFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSProvider;

import javax.ws.rs.BadRequestException;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * SMS provider implementation for the Clickatell REST API.
 *
 * @author Tim Anderson
 */
public class ClickatellProvider implements SMSProvider {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The Clickatell client factory.
     */
    private final ClickatellFactory factory;

    /**
     * Constructs a {@link ClickatellProvider}.
     *
     * @param service the archetype service
     * @param factory the Clickatell client factory
     */
    public ClickatellProvider(ArchetypeService service, ClickatellFactory factory) {
        this.service = service;
        this.factory = factory;
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Clickatell SMS Provider";
    }

    /**
     * Returns the SMS service archetypes that this supports.
     *
     * @return a list of archetypes prefixed by <em>entity.smsProvider</em>
     */
    @Override
    public String[] getArchetypes() {
        return new String[]{ClickatellArchetypes.CONFIG};
    }

    /**
     * Sends an SMS.
     * <p/>
     * On successful send, the message status should be updated to {@link OutboundMessage.Status#SENT}.
     * <p/>
     * If the send fails, and the message has an unrecoverable error, the message status should be updated to
     * {@link OutboundMessage.Status#ERROR}.
     *
     * @param message the message to send
     * @param config  the SMS provider configuration
     * @throws SMSException if the send fails and may be retried
     */
    @Override
    public void send(OutboundMessage message, Entity config) {
        int validityPeriod = getValidityPeriod(message);
        if (validityPeriod == 0) {
            message.setStatus(OutboundMessage.Status.EXPIRED);
        } else {
            IMObjectBean bean = service.getBean(config);
            SMS msg = createSMS(message, validityPeriod, bean);
            try {
                Clickatell client = factory.create(bean);
                SMSResponses responses = client.send(msg);
                List<SMSResponse> list = responses.getMessages();
                if (list.isEmpty()) {
                    throw new SMSException(ClickatellMessages.errorSendingMessage(responses.getErrorCode(),
                                                                                  responses.getError(),
                                                                                  responses.getErrorDescription()));
                } else {
                    SMSResponse response = list.get(0);
                    if (response.getAccepted() != null && response.getAccepted()) {
                        message.state()
                                .providerId(ClickatellArchetypes.ID, response.getApiMessageId())
                                .status(OutboundMessage.Status.SENT)
                                .build();
                    } else {
                        message.setStatus(OutboundMessage.Status.ERROR, ErrorHelper.formatError(response));
                    }
                }
            } catch (BadRequestException exception) {
                message.setStatus(OutboundMessage.Status.ERROR, exception.getMessage());
            } catch (SMSException exception) {
                throw exception;
            } catch (Throwable exception) {
                throw new SMSException(ClickatellMessages.exceptionSendingMessage(exception), exception);
            }
        }
    }

    /**
     * Creates a Clickatell SMS.
     *
     * @param message        the source message.
     * @param validityPeriod the validity period, or {@code -1} if it is unlimited
     * @param bean           the configuration bean
     * @return a new SMS
     */
    protected SMS createSMS(OutboundMessage message, int validityPeriod, IMObjectBean bean) {
        SMS msg = new SMS();
        String phone = message.getPhone();
        String countryPrefix = bean.getString("countryPrefix");
        String areaPrefix = bean.getString("areaPrefix");

        phone = getPhone(phone, countryPrefix, areaPrefix);
        msg.setFrom(getPhone(bean.getString("from"), countryPrefix, areaPrefix));
        msg.setTo(phone);
        msg.setContent(message.getMessage());
        msg.setClientMessageId(Long.toString(message.getId()));
        if (validityPeriod > 0) {
            msg.setValidityPeriod(validityPeriod);
        }
        return msg;
    }

    /**
     * Returns the validity period for the message, in minutes.
     *
     * @param message the message
     * @return -1, if there is no validity period, 0 if the message has expired, {@code > 0} indicating the number of
     * minutes when the message expires
     */
    private int getValidityPeriod(OutboundMessage message) {
        int result = -1;
        OffsetDateTime expiry = message.getExpiry();
        if (expiry != null) {
            OffsetDateTime now = OffsetDateTime.now();
            long minutes = 0;
            if (now.isBefore(expiry)) {
                minutes = Duration.between(now, expiry).toMinutes();
            }
            if (minutes == 0) {
                result = 0; // expired
            } else if (minutes <= Integer.MAX_VALUE) {
                // if > MAX_VALUE, effectively doesn't expire
                result = (int) minutes;
            }
        }
        return result;
    }

    /**
     * Formats a phone number.
     *
     * @param phone         the phone. May be {@code null}
     * @param countryPrefix the country prefix. May be {@code null}
     * @param areaPrefix    the area prefix. May be {@code null}
     * @return the formatted number. May be {@code null}
     */
    private String getPhone(String phone, String countryPrefix, String areaPrefix) {
        phone = StringUtils.trimToNull(phone);
        if (phone != null) {
            if (phone.startsWith("+")) {
                phone = phone.substring(1);
            } else {
                if (countryPrefix != null && !phone.startsWith(countryPrefix)) {
                    if (areaPrefix != null && phone.startsWith(areaPrefix)) {
                        // strip off the area prefix before adding the country prefix
                        phone = phone.substring(areaPrefix.length());
                    }
                    phone = countryPrefix + phone;
                }
            }
        }
        return phone;
    }

}
