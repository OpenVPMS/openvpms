/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.openvpms.clickatell.internal.api.Clickatell;
import org.openvpms.clickatell.internal.model.SMS;
import org.openvpms.clickatell.internal.model.SMSResponses;
import org.openvpms.clickatell.internal.model.SMSStatus;
import org.openvpms.ws.util.ErrorResponseFilter;
import org.openvpms.ws.util.SLF4JLoggingFeature;
import org.openvpms.ws.util.filter.JSONErrorMessageReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Collections;

/**
 * Default implementation of {@link Clickatell}.
 *
 * @author Tim Anderson
 */
public class ClickatellClient implements Clickatell {

    /**
     * The client to delegate to.
     */
    private final Clickatell client;

    /**
     * Connection & read timeout.
     */
    private static final int TIMEOUT = 30000;  // 30 seconds

    /**
     * The Clickatell URL.
     */
    private static final String URL = "https://platform.clickatell.com";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ClickatellClient.class);

    /**
     * Authorization header.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * Constructs a {@link ClickatellClient}.
     *
     * @param apiKey the API key
     */
    public ClickatellClient(String apiKey) {
        this(apiKey, URL);
    }

    /**
     * Constructs a {@link ClickatellClient}.
     *
     * @param apiKey the API key
     * @param url    the Clickatell URL
     */
    public ClickatellClient(String apiKey, String url) {
        ClientConfig config = new ClientConfig()
                .register(JacksonFeature.class);
        config.register(new SLF4JLoggingFeature(log, AUTHORIZATION));
        config.register(new ErrorResponseFilter(new ClickatellErrorReader()));
        Client client = ClientBuilder.newClient(config);
        client.property(ClientProperties.CONNECT_TIMEOUT, TIMEOUT);
        client.property(ClientProperties.READ_TIMEOUT, TIMEOUT);

        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add(AUTHORIZATION, apiKey);
        WebTarget target = client.target(url);
        this.client = WebResourceFactory.newResource(Clickatell.class, target, false, headers, Collections.emptyList(),
                                                     new Form());
    }

    /**
     * Sends a message.
     *
     * @param message the SMS message
     * @return the responses
     */
    @Override
    public SMSResponses send(SMS message) {
        return client.send(message);
    }

    /**
     * Returns the status of a message.
     *
     * @param messageId the message identifier
     * @return the status of the message
     */
    @Override
    public SMSStatus getStatus(String messageId) {
        return client.getStatus(messageId);
    }

    private static class ClickatellErrorReader extends JSONErrorMessageReader<SMSResponses> {

        /**
         * Constructs a {@link ClickatellErrorReader}.
         */
        public ClickatellErrorReader() {
            super(new ObjectMapper(), SMSResponses.class);
        }

        /**
         * Returns a string representation of the object.
         *
         * @param object the object
         * @return a string representation of the object. May be {@code null}
         */
        @Override
        protected String toString(SMSResponses object) {
            String result;
            if (!object.getMessages().isEmpty()) {
                result = ErrorHelper.formatError(object.getMessages().get(0));
            } else {
                result = ErrorHelper.formatError(object.getErrorCode(), object.getError(),
                                                 object.getErrorDescription());
            }
            return result;
        }
    }
}
