# OpenVPMS Veterinary Practice Management System

* **https://openvpms.org**
* [Features](https://www.openvpms.org/documentation/features) 
* [Demo](https://openvpms.org/demonstration)
* [Download](https://openvpms.org/download)
* [Installation](https://www.openvpms.org/documentation/csh/2.2/topics/install)
* [Documentation](https://openvpms.org/documentation/csh/2.2/topcs)
* [User forum](https://openvpms.org/category/forums/users/general)

## Development

* [Getting Started](https://www.openvpms.org/documentation/getting-started-openvpms)
* [Developer forum](https://www.openvpms.org/category/forums/developers/general-developers-discussion) 
* [JIRA](https://openvpms.atlassian.net) 
* [Source code](https://bitbucket.org/OpenVPMS)
* [License](https://openvpms.org/fileuploads/License.PDF)
