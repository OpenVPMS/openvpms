<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.openvpms</groupId>
        <artifactId>openvpms</artifactId>
        <version>2.4.0.8-SNAPSHOT</version>
    </parent>

    <artifactId>openvpms-archetypes</artifactId>
    <packaging>jar</packaging>
    <name>OpenVPMS Archetypes</name>


    <!-- ========================================================================================================== -->
    <!-- Changing archetypes                                                                                        -->
    <!--                                                                                                            -->
    <!-- After changing .adl files, run:                                                                            -->
    <!-- > mvn process-test-classes                                                                                 -->
    <!-- This generates the archetype checksum, and loads archetypes into the database.                             -->
    <!-- It is equivalent to running:                                                                               -->
    <!-- > mvn openvpms-archetype:checksum openvpms-db:auto-update                                                  -->
    <!--                                                                                                            -->
    <!-- Notes:                                                                                                     -->
    <!-- . if -DskipTests is specified, the update won't occur. To load archetypes and install the jar without      -->
    <!--   running tests use:                                                                                       -->
    <!--   > mvn -DskipTests install openvpms-db:update                                                             -->
    <!-- . to force archetypes to be loaded without changing checksums, use:                                        -->
    <!--   > mvn openvpms-archetype:load                                                                            -->
    <!-- ========================================================================================================== -->

    <!-- ========================================================================================================== -->
    <!-- Debugging notes                                                                                            -->
    <!--                                                                                                            -->
    <!-- p6spy can be enabled using:                                                                                -->
    <!--   -Djdbc.driverClassName=com.p6spy.engine.spy.P6SpyDriver                                                  -->
    <!--   -Djdbc.url=jdbc:p6spy:mysql://localhost:3306/openvpms_dev                                                -->
    <!-- ==========================================================================                                 -->

    <build>
        <resources>
            <resource>
                <directory>${basedir}/src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.hbm.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
            </resource>
            <resource>
                <directory>${basedir}/src/archetypes</directory>
                <includes>
                    <include>**/*.adl</include>
                    <include>**/*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/rules</directory>
                <includes>
                    <include>**/*.drl</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/conf/appcontext</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/conf/ehcache</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>${basedir}/src/conf/rules</directory>
                <includes>
                    <include>rules.xml</include>
                </includes>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>${basedir}/src/test/conf</directory>
                <includes>
                    <include>*.xml</include>
                </includes>
            </testResource>
            <testResource>
                <directory>${basedir}/src/test/resources</directory>
                <includes>
                    <include>*.xml</include>
                </includes>
                <filtering>true</filtering>
            </testResource>
            <testResource>
                <directory>${basedir}/src/test/resources</directory>
                <includes>
                    <include>*.properties</include>
                    <include>*.jrxml</include>
                </includes>
            </testResource>
            <testResource>
                <directory>${basedir}/src/test/resources/documents</directory>
                <targetPath>documents</targetPath>
            </testResource>
            <testResource>
                <directory>${basedir}/src/test/resources/archetypes</directory>
                <includes>
                    <include>**/*.adl</include>
                </includes>
            </testResource>
        </testResources>
        <plugins>
            <plugin>
                <groupId>org.openvpms</groupId>
                <artifactId>openvpms-archetype-maven-plugin</artifactId>
                <version>${project.version}</version>
                <configuration>
                    <dir>src/archetypes</dir>
                    <assertionTypes>src/archetypes/org/openvpms/archetype/assertionTypes.xml</assertionTypes>
                    <driver>${build.jdbc.driverClassName}</driver>
                    <url>${build.jdbc.url}</url>
                    <username>${build.jdbc.username}</username>
                    <password>${build.jdbc.password}</password>
                    <skip>${skipTests}</skip>
                </configuration>
                <executions>
                    <execution>
                        <!-- this generates the checksum using the production archetypes -->
                        <id>checksum</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>checksum</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>load</id>
                        <configuration>
                            <!-- this loads both production and test archetypes -->
                            <dir>target</dir>
                        </configuration>
                        <goals>
                            <goal>load</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>${jdbc.groupId}</groupId>
                        <artifactId>${jdbc.artifactId}</artifactId>
                        <version>${jdbc.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.openvpms</groupId>
                <artifactId>openvpms-db-maven-plugin</artifactId>
                <version>${project.version}</version>
                <configuration>
                    <!-- NOTE: this loads both production and test archetypes -->
                    <driver>${build.jdbc.driverClassName}</driver>
                    <url>${build.jdbc.url}</url>
                    <username>${build.jdbc.username}</username>
                    <password>${build.jdbc.password}</password>
                    <reportingUsername>${build.jdbc.reportingUsername}</reportingUsername>
                    <reportingPassword>${build.jdbc.reportingPassword}</reportingPassword>
                    <adminUsername>${jdbc.admin.username}</adminUsername>
                    <adminPassword>${jdbc.admin.password}</adminPassword>
                    <checksum>${project.build.outputDirectory}/META-INF/openvpms-archetypes.properties</checksum>
                    <skip>${skipTests}</skip>
                    <dir>target</dir>
                    <assertionTypes>target/classes/org/openvpms/archetype/assertionTypes.xml</assertionTypes>
                </configuration>
                <executions>
                    <execution>
                        <id>create-db</id>
                        <phase>process-test-classes</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>update-db-auto</id>
                        <phase>process-test-classes</phase>
                        <goals>
                            <!-- note: needs to run after the checksum has generated -->
                            <goal>auto-update</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>update-db</id>
                        <goals>
                            <!-- note: needs to run after the checksum has generated -->
                            <goal>update</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>${jdbc.groupId}</groupId>
                        <artifactId>${jdbc.artifactId}</artifactId>
                        <version>${jdbc.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.openvpms</groupId>
                <artifactId>openvpms-data-maven-plugin</artifactId>
                <version>${project.version}</version>
                <configuration>
                    <dir>src/data/org/openvpms/data/demo</dir>
                    <driver>${build.jdbc.driverClassName}</driver>
                    <url>${build.jdbc.url}</url>
                    <username>${build.jdbc.username}</username>
                    <password>${build.jdbc.password}</password>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>${jdbc.groupId}</groupId>
                        <artifactId>${jdbc.artifactId}</artifactId>
                        <version>${jdbc.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <!--debugForkedProcess>true</debugForkedProcess-->
                    <includes>
                        <include>**/*TestCase.java</include>
                    </includes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <!-- generates a jar of the test classes. TODO - these should be split from the unit tests -->
                            <goal>test-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- generates a jar of the test classes. TODO - these should be split from the unit tests -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                        <configuration>
                            <excludes>
                                <exclude>applicationContext.xml</exclude>
                            </excludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-framework</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-jxpath</groupId>
            <artifactId>commons-jxpath</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-resources</groupId>
            <artifactId>commons-resources</artifactId>
            <version>${commons.resources.version}</version>
        </dependency>
        <dependency>
            <groupId>org.ehcache</groupId>
            <artifactId>ehcache</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>jsap</groupId>
            <artifactId>JSAP</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-osgi</artifactId>
            <scope>runtime</scope>
        </dependency>
        <!--dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.jaxb-impl</artifactId>
            <scope>runtime</scope>
        </dependency-->
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
        </dependency>
        <dependency>
            <groupId>net.sf.opencsv</groupId>
            <artifactId>opencsv</artifactId>
            <version>2.3</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-dbcp2</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>${jdbc.groupId}</groupId>
            <artifactId>${jdbc.artifactId}</artifactId>
            <version>${jdbc.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>p6spy</groupId>
            <artifactId>p6spy</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

</project>
