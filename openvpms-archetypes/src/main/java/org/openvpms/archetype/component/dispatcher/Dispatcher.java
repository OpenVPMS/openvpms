/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.component.dispatcher;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.model.user.User;
import org.slf4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Manages processing of objects held by a set of {@link Queue}s.
 * <p/>
 * Queues are processed in a round robin fashion.
 *
 * @author Tim Anderson
 */
public abstract class Dispatcher<T, O, Q extends Queue<T, O>> implements DisposableBean,
        ApplicationListener<ContextRefreshedEvent> {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The service to schedule dispatching.
     */
    private final ExecutorService executor;

    /**
     * Used to wait if errors have occurred processing objects. This waits until a time expires or an act is queued.
     */
    private final Semaphore waiter = new Semaphore(0);

    /**
     * The logger.
     */
    private final Logger log;

    /**
     * Used to restrict the number of tasks that can be scheduled via the executor.
     */
    private final Semaphore scheduled = new Semaphore(1);

    /**
     * The queues.
     */
    private Queues<O, Q> queues;

    /**
     * Used to indicate that the dispatcher has been shut down.
     */
    private volatile boolean shutdown = false;

    /**
     * The user to initialise the security context in the dispatch thread.
     */
    private User serviceUser;

    /**
     * Limits the no. of times an error is logged if the user isn't configured.
     */
    private boolean missingUser;

    /**
     * Constructs a {@link  Dispatcher}.
     *
     * @param practiceService the practice service
     * @param log             the logger
     */
    protected Dispatcher(PracticeService practiceService, Logger log) {
        this.practiceService = practiceService;
        this.log = log;
        executor = Executors.newSingleThreadExecutor();
        serviceUser = getServiceUser();
    }

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        scheduleDispatch();
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     *
     * @throws Exception in case of shutdown errors
     */
    @Override
    public void destroy() throws Exception {
        shutdown = true;
        executor.shutdown();  // Disable new tasks from being submitted
        waiter.release();     // wake from sleep
        try {
            // Wait a while for existing tasks to terminate
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                executor.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                    log.error("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            executor.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
        if (queues != null) {
            queues.destroy();
        }
    }

    /**
     * Initialises this.
     *
     * @param queues the queues
     */
    protected void init(Queues<O, Q> queues) {
        this.queues = queues;
    }

    /**
     * Returns the queues.
     *
     * @return the queues
     */
    protected Queues<O, Q> getQueues() {
        return queues;
    }

    /**
     * Returns a queue given its owner.
     *
     * @param owner the queue owner
     * @return the corresponding queue, or {@code null if none is found}
     */
    protected Q getQueue(O owner) {
        return queues.getQueue(owner);
    }

    /**
     * Processes the first object in a queue, if any are present.
     *
     * @param queue the queue
     * @return {@code true} if there was an object, processed successfully or not
     */
    protected boolean processFirst(Q queue) {
        boolean result = false;
        Throwable failure = null;
        try {
            T object = queue.peekFirst();
            if (object != null) {
                result = true;
                failure = processProtected(object, queue);
            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            failure = exception;
        }
        if (failure != null) {
            // failed to process, so don't process this queue for another retryInterval seconds
            long retry = queue.getRetryInterval() * 1000L;
            queue.setWaitUntil(System.currentTimeMillis() + retry);
            queue.error(failure);
        }
        return result;
    }

    /**
     * Schedules dispatching.
     * <p/>
     * If a transaction is in progress, this will schedule dispatching once the transaction has committed.
     */
    protected void schedule() {
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            // a transaction is in progress, so only invoke schedule() once the transaction has committed
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCommit() {
                    scheduleDispatch();
                }
            });
        } else {
            scheduleDispatch();
        }
    }

    /**
     * Returns a string representation of an object.
     *
     * @param object the object
     * @return a string representation of the object
     */
    protected String toString(T object) {
        return object.toString();
    }

    /**
     * Processes an object.
     * <p/>
     * If the object is successfully processed, {@link Queue#processed()} should be invoked, otherwise an
     * exception should be raised.
     *
     * @param object the object to process
     * @param queue  the queue
     * @throws Exception for any error
     */
    protected abstract void process(T object, Q queue) throws Exception;

    /**
     * Returns the practice service.
     *
     * @return the practice service
     */
    protected PracticeService getPracticeService() {
        return practiceService;
    }

    /**
     * Processes all queued objects.
     */
    protected void dispatch() {
        boolean processed;
        int waiting;
        long minWait;
        log.debug("dispatch() - start");
        do {
            processed = false;
            waiting = 0;
            minWait = 0;
            for (Q queue : queues.getQueues()) {
                // process each queue in a round robin fashion
                if (queue.isSuspended()) {
                    log.debug("dispatch() - skipping suspended queue {}", queue);
                } else {
                    long wait = queue.getWaitUntil();
                    if (wait == -1 || wait <= System.currentTimeMillis()) {
                        processed |= processFirst(queue);
                    } else {
                        ++waiting;
                        if (minWait == 0 || wait < minWait) {
                            minWait = wait;
                        }
                    }
                }
            }
        } while (processed && !shutdown);
        postDispatch(waiting, minWait);
    }

    /**
     * Invoked after round-robin processing is complete.
     *
     * @param waiting   the number of queues that have experienced errors dispatching and are waiting
     * @param waitUntil the time to wait before processing again
     */
    protected void postDispatch(int waiting, long waitUntil) {
        if (!shutdown && waiting != 0) {
            long wait = waitUntil - System.currentTimeMillis();
            if (wait > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("Dispatcher waiting for {}", DurationFormatUtils.formatDurationHMS(wait));
                }
                // wait until the minimum wait time has expired, or an object is queued
                try {
                    waiter.drainPermits();
                    waiter.tryAcquire(wait, TimeUnit.MILLISECONDS);
                    // don't care if this doesn't acquire a permit within the allowed time as it
                    // means nothing was queued
                } catch (InterruptedException ignore) {
                    Thread.currentThread().interrupt();
                }
            }
            scheduleDispatch();
        }
        log.debug("dispatch() - end, rescheduled={}", (scheduled.availablePermits() == 0));
    }

    /**
     * Processes an object, trapping any thrown exception.
     *
     * @param object the object to process
     * @param queue  the queue
     * @return the failure, or {@code null} if the object was successfully processed
     */
    private Throwable processProtected(T object, Q queue) {
        Throwable failure = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            process(object, queue);
            log.debug("Processed object for {} in {}", queue, stopWatch);
        } catch (Exception exception) {
            log.error("Failed to process object={} for {}, time={}", toString(object), queue, stopWatch, exception);
            failure = exception;
        }
        return failure;
    }

    /**
     * Schedules {@link #dispatch()} to be run, unless it is already scheduled.
     */
    private void scheduleDispatch() {
        waiter.release(); // wakes up dispatch() if it is waiting

        User user = getServiceUser();
        if (shutdown) {
            log.debug("Dispatcher shutting down. Schedule request ignored");
        } else if (user == null) {
            log.warn("No service user. Schedule request ignored");
        } else if (scheduled.tryAcquire()) {
            log.debug("Scheduling dispatcher");
            executor.execute(() -> {
                scheduled.release(); // need to release here to enable dispatch() to reschedule
                try {
                    RunAs.run(user, this::dispatch);
                } catch (Exception exception) {
                    log.error(exception.getMessage(), exception);
                }
            });
        } else {
            log.debug("Dispatcher already scheduled");
        }
    }

    /**
     * Returns the user to set the security context in the dispatch thread.
     *
     * @return the user, or {@code null} if none has been configured
     */
    private synchronized User getServiceUser() {
        if (serviceUser == null) {
            serviceUser = practiceService.getServiceUser();
            if (serviceUser == null && !missingUser) {
                log.error("Missing party.organisationPractice serviceUser. Processing cannot occur until " +
                          "this is configured");
                missingUser = true;
            }
        }
        return serviceUser;
    }

}
