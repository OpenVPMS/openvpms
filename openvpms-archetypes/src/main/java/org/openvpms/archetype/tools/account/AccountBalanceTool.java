/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.tools.account;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.stringparsers.BooleanStringParser;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.account.BalanceCalculator;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRuleException;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceGenerator;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceUpdater;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NodeConstraint;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.File;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


/**
 * Tool to check and generate account balances for customers. This is intended
 * to be used when:
 * <ul>
 * <li>customer financial acts have been imported into the database that don't contain account balance information</li>
 * <li>account balance information must be regenerated for one or more customers</li>
 * </ul>
 * This must be run with rules disabled.
 *
 * @author Tim Anderson
 */
public class AccountBalanceTool {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The customer balance updater.
     */
    private final CustomerBalanceUpdater updater;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Determines if the generator should fail on error.
     */
    private boolean failOnError = false;

    /**
     * The no. of errors encountered.
     */
    private int errors = 0;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AccountBalanceTool.class);

    /**
     * The default application context.
     */
    private static final String APPLICATION_CONTEXT = "applicationContext.xml";

    /**
     * The archetypes to query.
     */
    private static final String[] SHORT_NAMES = new String[]{CustomerArchetypes.PERSON, CustomerArchetypes.OTC};


    /**
     * Constructs an {@link AccountBalanceTool}.
     *
     * @param service            the archetype service
     * @param updater            the customer balance updater
     * @param transactionManager the transaction manager
     * @throws IllegalStateException if rules are enabled on the archetype service
     */
    public AccountBalanceTool(IArchetypeService service, CustomerBalanceUpdater updater,
                              PlatformTransactionManager transactionManager) {
        if (service instanceof IArchetypeRuleService) {
            throw new IllegalStateException("Rules must be disabled to run " + AccountBalanceTool.class.getName());
        }
        this.service = service;
        this.updater = updater;
        this.transactionManager = transactionManager;
    }

    /**
     * Generates account balance information for all customers matching
     * the specified name.
     *
     * @param name the customer name. May be {@code null}
     */
    public void generate(String name) {
        ArchetypeQuery query = createNameQuery(name);
        generate(query);
    }

    /**
     * Generates account balance information for the customer with the specified
     * id.
     *
     * @param id the customer identifier
     */
    public void generate(long id) {
        ArchetypeQuery query = createIdQuery(id);
        generate(query);
    }

    /**
     * Generates account balance information for the specified customer.
     *
     * @param customer the customer
     */
    public void generate(Party customer) {
        log.info("Generating account balance for {}, ID={}", customer.getName(), customer.getId());
        BalanceCalculator calc = new BalanceCalculator(service);
        BigDecimal oldBalance = calc.getBalance(customer);
        Generator generator = new Generator(customer);
        BigDecimal balance = generator.generate();
        log.info("\tProcessed {} of {} acts", generator.getModified(), generator.getProcessed());
        log.info("\tUpdated account balance from {} to {}", oldBalance, balance);
    }

    /**
     * Determines if generation should fail when an error occurs.
     * Defaults to {@code true}.
     *
     * @param failOnError if {@code true} fail when an error occurs
     */
    public void setFailOnError(boolean failOnError) {
        this.failOnError = failOnError;
    }

    /**
     * Checks the account balance for all customers matching the specified name.
     *
     * @param name the customer name. May be {@code null}
     * @return {@code true} if the account balance is correct,
     * otherwise {@code false}
     */
    public boolean check(String name) {
        ArchetypeQuery query = createNameQuery(name);
        return check(query);
    }

    /**
     * Checks the account balance for the customer with the specified id.
     *
     * @param id the customer identifier
     * @return {@code true} if the account balance is correct,
     * otherwise {@code false}
     */
    public boolean check(long id) {
        ArchetypeQuery query = createIdQuery(id);
        return check(query);
    }

    /**
     * Checks the account balance for the specified customer.
     *
     * @param customer the customer
     * @return {@code true} if the account balance is correct,
     * otherwise {@code false}
     */
    public boolean check(Party customer) {
        log.info("Checking account balance for {}, ID={}", customer.getName(), customer.getId());
        BalanceCalculator calc = new BalanceCalculator(service);
        boolean result = false;
        try {
            BigDecimal expected = calc.getDefinitiveBalance(customer);
            BigDecimal actual = calc.getBalance(customer);
            result = expected.compareTo(actual) == 0;
            if (!result) {
                log.error("Failed to check account balance for {}, ID={}: expected balance={}, actual balance={}",
                          customer.getName(), customer.getId(), expected, actual);
            }
        } catch (CustomerAccountRuleException exception) {
            // thrown when an opening or closing balance doesn't match
            log.error("Failed to check account balance for {}, ID={}: {}",
                      customer.getName(), customer.getId(), exception.getMessage());
        }
        return result;
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        try {
            JSAP parser = createParser();
            JSAPResult config = parser.parse(args);
            if (!config.success()) {
                displayUsage(parser);
            } else {
                String contextPath = config.getString("context");
                String name = config.getString("name");
                long id = config.getLong("id");
                boolean generate = config.getBoolean("generate");
                boolean check = config.getBoolean("check");

                ApplicationContext context;
                if (!new File(contextPath).exists()) {
                    context = new ClassPathXmlApplicationContext(contextPath);
                } else {
                    context = new FileSystemXmlApplicationContext(contextPath);
                }
                IArchetypeService service = (IArchetypeService) context.getBean("archetypeService");
                CustomerBalanceUpdater updater = context.getBean(CustomerBalanceUpdater.class);
                PlatformTransactionManager transactionManager = context.getBean(PlatformTransactionManager.class);
                AccountBalanceTool tool = new AccountBalanceTool(service, updater, transactionManager);
                if (check) {
                    if (id == -1) {
                        tool.check(name);
                    } else {
                        tool.check(id);
                    }
                } else if (generate) {
                    tool.setFailOnError(config.getBoolean("failOnError"));
                    if (id == -1) {
                        tool.generate(name);
                    } else {
                        tool.generate(id);
                    }
                } else {
                    displayUsage(parser);
                }
            }
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
        }
    }

    /**
     * Generates account balances for all customers matching the specified
     * query.
     *
     * @param query the query
     */
    private void generate(ArchetypeQuery query) {
        Collection<String> nodes = Collections.singletonList("name");
        Iterator<Party> iterator = new IMObjectQueryIterator<>(service, query, nodes);
        int count = 0;
        while (iterator.hasNext()) {
            Party customer = iterator.next();
            try {
                generate(customer);
                ++count;
            } catch (OpenVPMSException exception) {
                if (failOnError) {
                    throw exception;
                } else {
                    ++errors;
                    log.error("Failed to generate account balance for {}", customer.getName(), exception);
                }
            }
        }
        log.info("Generated account balances for {} customers", count);
        if (errors != 0) {
            log.warn("There were {} errors", errors);
        } else {
            log.info("There were no errors");
        }
    }

    /**
     * Checks account balances for all customers matching the specified
     * query.
     *
     * @param query the query
     * @return {@code true} if the account balances are correct otherwise {@code false}
     */
    private boolean check(ArchetypeQuery query) {
        boolean result = true;
        Collection<String> nodes = Collections.singletonList("name");
        Iterator<Party> iterator = new IMObjectQueryIterator<>(service, query, nodes);
        int count = 0;
        while (iterator.hasNext()) {
            Party customer = iterator.next();
            try {
                boolean ok = check(customer);
                ++count;
                if (!ok) {
                    result = false;
                    if (failOnError) {
                        return result;
                    } else {
                        ++errors;
                    }
                }
            } catch (OpenVPMSException exception) {
                if (failOnError) {
                    throw exception;
                } else {
                    ++errors;
                    log.error("Failed to check account balance for {}", customer.getName(), exception);
                }
            }
        }
        log.info("Checked account balances for {} customers", count);
        if (errors != 0) {
            log.warn("There were {} errors", errors);
        } else {
            log.info("There were no errors");
        }
        return result;
    }


    /**
     * Creates a query on customer name.
     *
     * @param name the customer name/ May be {@code null}
     * @return a new query
     */
    private ArchetypeQuery createNameQuery(String name) {
        ArchetypeQuery query = new ArchetypeQuery(SHORT_NAMES, true, false);
        query.setMaxResults(1000);
        if (!StringUtils.isEmpty(name)) {
            query.add(new NodeConstraint("name", name));
        }
        query.add(new NodeSortConstraint("name"));
        query.add(new NodeSortConstraint("id"));
        return query;
    }

    /**
     * Creates a query on customer id.
     *
     * @param id the customer identifier
     * @return a new query
     */
    private ArchetypeQuery createIdQuery(long id) {
        ArchetypeQuery query = new ArchetypeQuery(SHORT_NAMES, true, false);
        query.add(new NodeConstraint("id", id));
        return query;
    }

    /**
     * Creates a new command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("name").setShortFlag('n')
                                         .setLongFlag("name")
                                         .setHelp("Customer name. May contain wildcards"));
        parser.registerParameter(new FlaggedOption("id").setShortFlag('i')
                                         .setLongFlag("id")
                                         .setStringParser(JSAP.LONG_PARSER).setDefault("-1")
                                         .setHelp("Customer identifier."));
        parser.registerParameter(new Switch("check").setShortFlag('c')
                                         .setLongFlag("check").setDefault("false")
                                         .setHelp("Check account balances."));
        parser.registerParameter(new Switch("generate").setShortFlag('g')
                                         .setLongFlag("generate").setDefault("false")
                                         .setHelp("Generate account balances."));
        parser.registerParameter(new FlaggedOption("failOnError")
                                         .setShortFlag('e')
                                         .setLongFlag("failOnError")
                                         .setDefault("false")
                                         .setStringParser(BooleanStringParser.getParser())
                                         .setHelp("Fail on error"));
        parser.registerParameter(new FlaggedOption("context")
                                         .setLongFlag("context")
                                         .setDefault(APPLICATION_CONTEXT)
                                         .setHelp("Application context path"));
        return parser;
    }

    /**
     * Prints usage information.
     *
     * @param parser the command line parser
     */
    private static void displayUsage(JSAP parser) {
        System.err.println();
        System.err.println("Usage: java "
                           + AccountBalanceTool.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.exit(1);
    }

    private class Generator extends CustomerBalanceGenerator {
        /**
         * Constructs a new {@code Generator} for a customer.
         *
         * @param customer the customer
         */
        Generator(Party customer) {
            super(customer, service, updater, transactionManager);
        }

        /**
         * Invoked when an act is changed.
         *
         * @param act       the act
         * @param fromTotal the original total
         * @param toTotal   the new total
         */
        @Override
        protected void changed(FinancialAct act, BigDecimal fromTotal, BigDecimal toTotal) {
            String displayName = DescriptorHelper.getDisplayName(act, service);
            log.warn("Updated {} dated {}  from {} to {}", displayName, act.getActivityStartTime(), fromTotal, toTotal);
        }
    }

}
