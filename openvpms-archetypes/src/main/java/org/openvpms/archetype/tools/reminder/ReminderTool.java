/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.tools.reminder;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.stringparsers.DateStringParser;
import com.martiansoftware.jsap.stringparsers.LongStringParser;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.join;

/**
 * Reminder tool.
 *
 * @author Tim Anderson
 */
public class ReminderTool {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The patient rules.
     */
    private final PatientRules rules;

    /**
     * Reminder type cache.
     */
    private final ReminderTypes reminderTypes;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ReminderTool.class);

    /**
     * The default name of the application context file.
     */
    private static final String APPLICATION_CONTEXT = "applicationContext.xml";

    /**
     * Constructs a {@link ReminderTool}.
     *
     * @param service the archetype service
     * @param rules   the patient rules
     */
    ReminderTool(IArchetypeRuleService service, PatientRules rules) {
        this.service = service;
        this.rules = rules;
        reminderTypes = new ReminderTypes(service);
    }

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            JSAP parser = createParser();
            JSAPResult config = parser.parse(args);
            if (!config.success()) {
                displayUsage(config);
            } else {
                Date due = config.getDate("due");
                long locationId = config.getLong("location", -1);
                boolean updateNextReminder = config.getBoolean("updatenextreminder");
                boolean remove = config.getBoolean("remove");
                boolean dryRun = config.getBoolean("dryrun") || !config.getBoolean("commit");
                boolean updateCount = config.getBoolean("updatecount");
                if (updateNextReminder && (updateCount || remove)) {
                    log("--update-next-reminder cannot be used in conjunction with --update-reminder-counts or " +
                        "--remove-unsent-items");
                    System.exit(1);
                } else if (updateNextReminder || remove || (updateCount && due != null)) {
                    String contextPath = config.getString("context");

                    ApplicationContext context;
                    if (!new File(contextPath).exists()) {
                        context = new ClassPathXmlApplicationContext(contextPath);
                    } else {
                        context = new FileSystemXmlApplicationContext(contextPath);
                    }
                    IArchetypeRuleService service = context.getBean(IArchetypeRuleService.class);
                    PatientRules rules = context.getBean(PatientRules.class);

                    Entity location = null;
                    if (locationId != -1) {
                        location = (Entity) service.get(new IMObjectReference(PracticeArchetypes.LOCATION, locationId));
                        if (location == null) {
                            log("ERROR: Failed to find practice location with id=" + locationId);
                            System.exit(1);
                        }
                        log("Restricting reminders to customers with location=" + location.getName());
                    }

                    ReminderTool tool = new ReminderTool(service, rules);
                    Status status = new Status();
                    if (updateNextReminder) {
                        status = status.add(tool.updateNextReminderDates(location, dryRun));
                    } else {
                        if (remove) {
                            status = status.add(tool.removeUnsentItems(location, dryRun));
                        }
                        if (updateCount) {
                            status = status.add(tool.updateCounts(due, location, dryRun));
                        }
                    }
                    if (status.errors != 0) {
                        log("WARNING: errors were encountered");
                    }
                    if (dryRun && status.updated != 0) {
                        log("Use --commit to commit updates");
                    }

                    System.exit(0);
                } else {
                    displayUsage(config);
                }
            }
        } catch (Throwable throwable) {
            log.error("ERROR: " + throwable.getMessage(), throwable);
            System.exit(1);
        }
    }

    /**
     * Updates the Next Reminder date based on the reminder type and reminder count, for IN_PROGRESS reminders.
     * <p/>
     * Any reminder that is updated will have its PENDING and ERROR reminder items removed.
     *
     * @param location the customer location to restrict reminders to, or {@code null} to not restrict by customer
     *                 location
     * @param dryRun   if {@code true}, show changes but do not commit them
     * @return the cumulative processing status
     */
    private Status updateNextReminderDates(Entity location, boolean dryRun) {
        Status status = forEachReminder(location, act -> updateNextReminderDate(act, dryRun));
        log("Updated Next Reminder dates: updated=" + status.updated + ", skipped=" + status.skipped
            + ", errors=" + status.errors);
        return status;
    }

    /**
     * Removes all reminder items that have PENDING or ERROR status, for IN_PROGRESS reminders.
     *
     * @param location the customer location to restrict reminders to, or {@code null} to not restrict by customer
     *                 location
     * @param dryRun   if {@code true}, show changes but do not commit them
     * @return the cumulative processing status
     */
    private Status removeUnsentItems(Entity location, boolean dryRun) {
        Status status = forEachReminder(location, act -> removeUnsentItems(act, dryRun));
        log("Removed unsent items from reminders: updated=" + status.updated + ", skipped=" + status.skipped
            + ", errors=" + status.errors);
        return status;
    }

    /**
     * Updates all IN_PROGRESS reminders with a due date less than that specified, until they are no longer due or
     * can no longer be updated.
     *
     * @param date     the date
     * @param location the customer location to restrict reminders to, or {@code null} to not restrict by customer
     *                 location
     * @param dryRun   if {@code true}, show changes but do not commit them
     * @return the cumulative processing status
     */
    private Status updateCounts(Date date, Entity location, boolean dryRun) {
        Status status = forEachReminder(location, act -> updateCount(act, date, dryRun));
        log("Update reminders: updated=" + status.updated + ", skipped=" + status.skipped + ", errors="
            + status.errors);
        return status;
    }

    /**
     * Updates the Next Reminder date for a reminder, based on its reminder type and reminder count.
     *
     * @param act    the reminder
     * @param dryRun if {@code true}, show changes but do not commit them
     * @return the processing status
     */
    private Status updateNextReminderDate(Act act, boolean dryRun) {
        Status result;
        IMObjectBean bean = service.getBean(act);
        Party patient = bean.getTarget("patient", Party.class);
        if (isValid(patient, act)) {
            ReminderType reminderType = getReminderType(bean);
            if (reminderType == null) {
                result = Status.skip();
                log("Skipping reminder=" + act.getId() + " for patient=" + patient.getId() + ", name='"
                    + patient.getName() + "': reminder has no reminder type");
            } else {
                int count = bean.getInt("reminderCount");
                Date date = reminderType.getNextDueDate(act.getActivityEndTime(), count);
                if (date == null) {
                    log("Skipping reminder=" + act.getId() + " for patient=" + patient.getId() + ", name='"
                        + patient.getName() + "': reminder type='" + reminderType.getName()
                        + "' has no reminder count=" + count);
                    result = Status.skip();
                } else {
                    Date existing = act.getActivityStartTime();
                    if (existing.compareTo(date) != 0) {
                        act.setActivityStartTime(date);
                        List<Act> items = getUnsentItems(bean);
                        RemoveStatus status;
                        boolean error = false;
                        if (!items.isEmpty()) {
                            status = removeUnsentItems(act, bean, patient, items, dryRun);
                            if (!status.updated) {
                                error = true;
                            }
                        } else {
                            status = null;
                            if (!dryRun) {
                                error = !save(act, patient);
                            }
                        }
                        if (!error) {
                            result = Status.update();
                            String message = "Updated reminder=" + act.getId() + ", patient=" + asString(patient)
                                             + ", from next reminder=" + asString(existing)
                                             + ", to next reminder=" + asString(date);
                            if (status != null) {
                                message += ": removed PENDING=" + status.pending + ", ERROR=" + status.errors;
                            }
                            log(message);
                        } else {
                            result = Status.error();
                        }
                    } else {
                        result = Status.noop();
                    }
                }
            }
        } else {
            result = Status.error();
        }
        return result;
    }

    /**
     * Removes all reminder items that have PENDING or ERROR status from the specified reminder.
     *
     * @param dryRun if {@code true}, show changes but do not commit them
     * @return the processing status
     */
    private Status removeUnsentItems(Act act, boolean dryRun) {
        Status result;
        IMObjectBean bean = service.getBean(act);
        Party patient = bean.getTarget("patient", Party.class);
        if (isValid(patient, act)) {
            List<Act> items = getUnsentItems(bean);
            if (items.isEmpty()) {
                result = Status.noop();
            } else {
                RemoveStatus status = removeUnsentItems(act, bean, patient, items, dryRun);
                if (status.updated) {
                    log("Updated reminder=" + act.getId() + ", patient=" + asString(patient)
                        + ", next reminder=" + asString(act.getActivityStartTime())
                        + ": removed PENDING=" + status.pending + ", ERROR=" + status.errors);
                    result = Status.update();
                } else {
                    result = Status.error();
                }
            }
        } else {
            result = Status.error();
        }
        return result;
    }

    /**
     * Removes unsent items.
     *
     * @param act     the reminder
     * @param bean    the reminder bean
     * @param patient the patient
     * @param items   the items
     * @param dryRun  if {@code true}, show changes but do not commit them
     * @return the processing status
     */
    private RemoveStatus removeUnsentItems(Act act, IMObjectBean bean, Party patient, List<Act> items, boolean dryRun) {
        List<Act> toSave = new ArrayList<>(items);
        int pending = 0;
        int errors = 0;
        for (Act item : items) {
            bean.removeTargets("items", item, "reminder");
            if (ReminderItemStatus.PENDING.equals(item.getStatus())) {
                pending++;
            } else {
                errors++;
            }
        }
        toSave.add(act);
        boolean error = false;
        if (!dryRun) {
            error = !save(act, patient, toSave);
        }
        return new RemoveStatus(pending, errors, !error);
    }

    /**
     * Saves a reminder.
     *
     * @param act     the reminder
     * @param patient the patient
     * @return {@code true} if the reminder was saved
     */
    private boolean save(Act act, Party patient) {
        return save(act, patient, Collections.singletonList(act));
    }

    /**
     * Saves a reminder.
     *
     * @param act     the reminder
     * @param patient the patient
     * @param toSave  all acts to save
     * @return {@code true} if the reminder was saved
     */
    private boolean save(Act act, Party patient, List<Act> toSave) {
        boolean saved = false;
        try {
            service.save(toSave);
            saved = true;
        } catch (Throwable exception) {
            log("Failed to update reminder=" + act.getId() + ", patient=" + asString(patient)
                + ", next reminder=" + asString(act.getActivityStartTime()) + ": " + exception.getMessage());
            log.error(exception.getMessage(), exception);
        }
        return saved;
    }

    /**
     * Determines if a patient is valid.
     *
     * @param patient the patient
     * @param act     the reminder act
     * @return {@code true} if the patient is valid
     */
    private boolean isValid(Party patient, Act act) {
        boolean valid = false;
        if (patient == null) {
            log("Skipping reminder=" + act.getId() + ": reminder has no patient");
        } else if (!patient.isActive()) {
            log("Skipping reminder=" + act.getId() + " for patient=" + asString(patient)
                + ": patient is inactive");
        } else if (rules.isDeceased(patient)) {
            log("Skipping reminder=" + act.getId() + " for patient=" + asString(patient)
                + ": patient is deceased");
        } else {
            valid = true;
        }
        return valid;
    }

    /**
     * Applies the specified function to each IN_PROGRESS reminder.
     *
     * @param location the customer location to restrict reminders to, or {@code null} to not restrict by customer
     *                 location
     * @param function the function to apply. Returns {@code true} if the reminder was updated
     * @return the processing status
     */
    private Status forEachReminder(Entity location, Function<Act, Status> function) {
        ArchetypeQuery query = new ArchetypeQuery(ReminderArchetypes.REMINDER);
        query.add(Constraints.eq("status", ActStatus.IN_PROGRESS));
        if (location != null) {
            query.add(join("patient")                                // participation.patient
                              .add(join("entity").                   // party.patientpet
                                      add(join("customers")          // entityRelationship.patientOwner, patientLocation
                                                  .add(join("source", "customer")  // party.customerperson
                                                               .add(join("practice").add(eq("target", location)))))));
        }
        query.add(Constraints.sort("id"));
        Iterator<Act> iterator = new IMObjectQueryIterator<>(service, query);
        Status result = new Status();
        while (iterator.hasNext()) {
            Act act = iterator.next();
            Status status = function.apply(act);
            result = result.add(status);
        }
        return result;
    }

    /**
     * Updates the reminder count and due date of a reminder if its Next Reminder date has passed.
     *
     * @param act    the reminder
     * @param date   the date
     * @param dryRun if {@code true}, show changes but do not commit them
     * @return the processing status
     */
    private Status updateCount(Act act, Date date, boolean dryRun) {
        Status result;
        IMObjectBean bean = service.getBean(act);
        Party patient = bean.getTarget("patient", Party.class);
        if (isValid(patient, act)) {
            ReminderType reminderType = getReminderType(bean);
            if (reminderType == null) {
                result = Status.skip();
                log("Skipping reminder=" + act.getId() + " for patient=" + patient.getId() + ", name='"
                    + patient.getName() + "': reminder has no reminder type");
            } else {
                if (act.getActivityStartTime().compareTo(date) < 0) {
                    if (!dryRun && hasUnsentItems(bean)) {
                        log("Skipping reminder=" + act.getId() + ", patient=" + asString(patient)
                            + ", reminderType=" + asString(reminderType.getEntity())
                            + ": reminder has unsent reminder items");
                        result = Status.skip();
                    } else {
                        result = updateReminderCountForPastDue(act, bean, patient, reminderType, date, dryRun);
                    }
                } else {
                    result = Status.noop();
                }
            }
        } else {
            result = Status.error();
        }
        return result;
    }

    /**
     * Updates the reminder count and Next Reminder date of a reminder if its Next Reminder date has passed.
     *
     * @param act          the reminder
     * @param bean         the reminder bean
     * @param patient      the patient
     * @param reminderType the reminder type
     * @param date         the date
     * @param dryRun       if {@code true}, show changes but do not commit them
     * @return the processing status
     */
    private Status updateReminderCountForPastDue(Act act, IMObjectBean bean, Party patient, ReminderType reminderType,
                                                 Date date, boolean dryRun) {
        Status result;
        boolean done = false;
        boolean updated = false;
        int count = bean.getInt("reminderCount");
        int oldCount = count;
        Date oldDue = act.getActivityStartTime();
        while (!done && act.getActivityStartTime().compareTo(date) < 0) {
            count++;
            Date dueDate = reminderType.getNextDueDate(act.getActivityEndTime(), count);
            if (dueDate != null) {
                bean.setValue("reminderCount", count);
                act.setActivityStartTime(dueDate);
                updated = true;
            } else {
                if (reminderType.getReminderCount(count - 1) != null) {
                    // Update the reminderCount, as it indicates that the previous count was sent
                    bean.setValue("reminderCount", count);
                    updated = true;
                }
                done = true;
            }
        }
        if (updated) {
            boolean error = false;
            if (!dryRun) {
                try {
                    service.save(act);
                } catch (Throwable exception) {
                    error = true;
                    log("Failed to update reminder=" + act.getId() + ", patient=" + asString(patient)
                        + ", reminderType=" + asString(reminderType.getEntity())
                        + ", from count=" + oldCount + ", next reminder=" + asString(oldDue)
                        + " to count=" + bean.getInt("reminderCount") + ", next reminder="
                        + asString(act.getActivityStartTime()) + ": " + exception.getMessage());
                    log.error(exception.getMessage(), exception);
                }
            }
            if (!error) {
                result = Status.update();
                log("Updated reminder=" + act.getId() + ", patient=" + asString(patient)
                    + ", reminderType=" + asString(reminderType.getEntity())
                    + ", from count=" + oldCount + ", next reminder=" + asString(oldDue)
                    + " to count=" + bean.getInt("reminderCount") + ", next reminder="
                    + asString(act.getActivityStartTime()));
            } else {
                result = Status.error();
            }
        } else {
            result = Status.skip();
            log("Skipping reminder=" + act.getId() + ", patient=" + asString(patient)
                + ", reminderType=" + asString(reminderType.getEntity())
                + ", next reminder=" + asString(act.getActivityStartTime())
                + ": no reminder count=" + count);
        }
        return result;
    }

    /**
     * Logs a message.
     *
     * @param message the message to log
     */
    private static void log(String message) {
        System.out.println(message);
    }

    /**
     * Determines if a reminder has unsent reminder items i.e. those with PENDING or ERROR status.
     *
     * @param bean the reminder bean
     * @return {@code true} if it has unsent items
     */
    private boolean hasUnsentItems(IMObjectBean bean) {
        return !getUnsentItems(bean).isEmpty();
    }

    /**
     * Returns the unsent reminder items for a reminder i.e. those with PENDING or ERROR status.
     *
     * @param bean the reminder bean
     * @return the unsent items
     */
    private List<Act> getUnsentItems(IMObjectBean bean) {
        List<Act> result = new ArrayList<>();
        for (Act act : bean.getTargets("items", Act.class)) {
            String status = act.getStatus();
            if (ReminderItemStatus.PENDING.equals(status) || ReminderItemStatus.ERROR.equals(status)) {
                result.add(act);
            }
        }
        return result;
    }

    /**
     * Returns the reminder type of a reminder.
     *
     * @param bean the reminder
     * @return the reminder type, or {@code null} if none is found
     */
    private ReminderType getReminderType(IMObjectBean bean) {
        Reference ref = bean.getTargetRef("reminderType");
        return reminderTypes.get(ref);
    }

    /**
     * Helper to generate a string for a date.
     *
     * @param date the date
     * @return a string
     */
    private String asString(Date date) {
        return new java.sql.Date(date.getTime()).toString();
    }

    /**
     * Creates a new command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new Switch("updatenextreminder").setLongFlag("update-next-reminder")
                                         .setDefault("false"));
        parser.registerParameter(new Switch("updatecount").setLongFlag("update-reminder-counts").setDefault("false"));
        DateStringParser date = DateStringParser.getParser();
        date.setProperty("format", "dd/MM/yyyy");
        parser.registerParameter(new FlaggedOption("due").setLongFlag("due").setStringParser(date));
        parser.registerParameter(new FlaggedOption("location").setLongFlag("location").setStringParser(
                LongStringParser.getParser()));
        parser.registerParameter(new Switch("remove").setLongFlag("remove-unsent-items").setDefault("false"));
        parser.registerParameter(new Switch("dryrun").setLongFlag("dry-run"));
        parser.registerParameter(new Switch("commit").setLongFlag("commit"));
        parser.registerParameter(new Switch("help").setLongFlag("help").setDefault("false"));
        parser.registerParameter(new FlaggedOption("context").setLongFlag("context").setDefault(APPLICATION_CONTEXT));
        return parser;
    }

    /**
     * Prints usage information and exits.
     *
     * @param result the parse result
     */
    private static void displayUsage(JSAPResult result) {
        Iterator iter = result.getErrorMessageIterator();
        while (iter.hasNext()) {
            System.err.println(iter.next());
        }
        System.err.println();
        System.err.println("Usage: remtool [options]");
        System.err.println();
        System.err.println("  --update-next-reminder [ --location=<id> ] [--dry-run | --commit]");
        System.err.println("    For each IN_PROGRESS reminder, updates the Next Reminder date based on the current");
        System.err.println("    Reminder Type and Reminder Count");
        System.err.println("    If a reminder is updated, any reminder items with PENDING or ERROR status will be " +
                           "removed.");
        System.err.println();
        System.err.println("  --remove-unsent-items [ --location=<id> ] [--dry-run | --commit]");
        System.err.println("    For each IN_PROGRESS reminder, removes any reminder items with PENDING");
        System.err.println("    or ERROR status.");
        System.err.println();
        System.err.println("  --update-reminder-counts --due <dd/mm/yyyy> [ --location=<id> ] [--dry-run | --commit]");
        System.err.println("    For each IN_PROGRESS reminder, updates the reminderCount until the due date");
        System.err.println("    is greater than that specified. This updates the Next Reminder date accordingly");
        System.err.println();
        System.err.println("  --help");
        System.err.println("    Displays this help");
        System.err.println();
        System.err.println("Use: ");
        System.err.println("  --location to limit reminders those customers that have the specified ");
        System.err.println("             Practice Location with the specified location identifier.");
        System.err.println("  --dry-run  to show what will happen without making changes.");
        System.err.println("  --commit   to commit changes.");
        System.err.println();
        System.err.println("If both --remove-unsent-items and --update-reminder-counts are specified,");
        System.err.println("--remove-unsent-items is applied first");
        System.exit(1);
    }

    /**
     * Helper to generate a debug string for an entity.
     *
     * @param entity the party. May be {@code null}
     * @return a string, or {@code null} if the party is null
     */
    private static String asString(Entity entity) {
        return (entity != null) ? entity.getName() + " (" + entity.getId() + ")" : null;
    }

    private static class Status {

        private final int updated;

        private final int skipped;

        private final int errors;

        Status() {
            this(0, 0, 0);
        }

        private Status(int updated, int skipped, int errors) {
            this.skipped = skipped;
            this.updated = updated;
            this.errors = errors;
        }

        Status add(Status other) {
            return new Status(updated + other.updated, skipped + other.skipped, errors + other.errors);
        }

        static Status noop() {
            return new Status();
        }

        static Status skip() {
            return new Status(0, 1, 0);
        }

        static Status error() {
            return new Status(0, 0, 1);
        }

        static Status update() {
            return new Status(1, 0, 0);
        }
    }

    private static class RemoveStatus {

        final int pending;

        final int errors;

        final boolean updated;

        RemoveStatus(int pending, int errors, boolean updated) {
            this.pending = pending;
            this.errors = errors;
            this.updated = updated;
        }

    }

}
