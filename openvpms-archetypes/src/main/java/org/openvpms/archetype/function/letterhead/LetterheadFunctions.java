/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.letterhead;

import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;
import org.openvpms.component.system.common.jxpath.FunctionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Letterhead functions for reporting.
 *
 * @author Tim Anderson
 */
public class LetterheadFunctions extends AbstractObjectFunctions {

    /**
     * The logo service.
     */
    private final LogoService logoService;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LetterheadFunctions.class);


    /**
     * Constructs a {@link LetterheadFunctions}.
     *
     * @param logoService the logo service
     * @param service     the archetype service
     */
    public LetterheadFunctions(LogoService logoService, ArchetypeService service) {
        super("letterhead");
        setObject(this);

        this.logoService = logoService;
        this.service = service;
    }

    /**
     * Returns a URL to the logo associated with a practice location's letterhead.
     * <p/>
     * This may be a URL to the logoFile or a stored image. If both are present, the logoFile is returned.
     * <p/>
     * The URL will typically be to a local file and therefore cannot be served outside the local host.
     *
     * @param object the practice location. May be {@code null}
     * @return the url. May be {@code null}
     */
    public URL logo(Object object) {
        object = FunctionHelper.unwrap(object);
        URL result = null;
        if (object instanceof Party) {
            IMObjectBean bean = service.getBean((Party) object);
            Entity letterhead = bean.getTarget("letterhead", Entity.class);
            if (letterhead != null) {
                try {
                    result = getLogo(letterhead);
                } catch (Exception exception) {
                    log.warn("Failed to retrieve logo for letterhead {}: {}", letterhead.getName(),
                             exception.getMessage(), exception);
                }
            }
        }
        return result;
    }

    /**
     * Returns a URL to the logo associated with an <em>entity.letterhead</em>.
     * <p/>
     * This returns logoFile in preference to logo as it is more efficient. The getLetterheadLogo() requires a query.
     *
     * @param letterhead the letterhead
     * @return the url. May be {@code null}
     * @throws DocumentException for any error
     */
    private URL getLogo(Entity letterhead) {
        URL result;
        IMObjectBean bean = service.getBean(letterhead);
        String logoFile = bean.getString("logoFile");
        if (logoFile != null) {
            try {
                result = new File(logoFile).toURI().toURL();
            } catch (IOException exception) {
                throw new DocumentException(DocumentException.ErrorCode.NotFound, logoFile);
            }
        } else {
            result = logoService.getURL(letterhead);
        }
        return result;
    }
}