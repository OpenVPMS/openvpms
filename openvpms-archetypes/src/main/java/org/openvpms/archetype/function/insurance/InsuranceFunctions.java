/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.insurance;

import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import static org.openvpms.component.system.common.jxpath.FunctionHelper.unwrap;
import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.join;

/**
 * Patient insurance functions.
 *
 * @author Tim Anderson
 */
public class InsuranceFunctions {

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs an {@link InsuranceFunctions}.
     *
     * @param insuranceRules the insurance rules
     * @param patientRules   the patient rules
     * @param service        the archetype service
     */
    public InsuranceFunctions(InsuranceRules insuranceRules, PatientRules patientRules, IArchetypeService service) {
        this.insuranceRules = insuranceRules;
        this.patientRules = patientRules;
        this.service = service;
    }

    /**
     * Determines if an invoice item has been claimed by the supplied claim.
     *
     * @param claim the claim
     * @param item  the invoice item
     * @return {@code true} if the item has been claimed in the specified claim, otherwise {@code false}
     */
    public boolean claimed(FinancialAct claim, FinancialAct item) {
        boolean result = false;
        if (claim != null && item != null) {
            ArchetypeQuery query = new ArchetypeQuery(claim.getObjectReference())
                    .add(join("items", "c").add(join("target", "ct").add(join("items", "i").add(eq("target", item)))))
                    .add(new NodeSelectConstraint("id"));
            query.setMaxResults(1);
            result = new ObjectSetQueryIterator(service, query).hasNext();
        }
        return result;
    }

    /**
     * Returns the current policy for a patient.
     *
     * @param object the patient. May be {@code null}
     * @return the current policy, or {@code null} if no patient was supplied or the patient has no current policy
     */
    public Act policy(Object object) {
        return policy(object, false);
    }

    /**
     * Returns the current policy for a patient, or the most recent, if there is no current policy.
     *
     * @param object     the patient. May be {@code null}
     * @param mostRecent if {@code true}, return the most recent policy if the patient doesn't have a current policy
     * @return the current policy, or {@code null} if no patient was supplied or the patient has no policy
     */
    public Act policy(Object object, boolean mostRecent) {
        Act policy = null;
        Party patient = unwrap(object, Party.class, PatientArchetypes.PATIENT);
        if (patient != null) {
            Party owner = patientRules.getOwner(patient);
            if (owner != null) {
                if (mostRecent) {
                    policy = insuranceRules.getPolicy(owner, patient);
                } else {
                    policy = insuranceRules.getCurrentPolicy(owner, patient);
                }
            }
        }
        return policy;
    }
}
