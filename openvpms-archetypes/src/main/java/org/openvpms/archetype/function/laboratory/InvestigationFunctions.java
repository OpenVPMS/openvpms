/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.laboratory;

import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.Function;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;
import org.openvpms.component.system.common.jxpath.FunctionHelper;

/**
 * Investigation functions.
 *
 * @author Tim Anderson
 */
public class InvestigationFunctions extends AbstractObjectFunctions {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link InvestigationFunctions}.
     */
    public InvestigationFunctions(ArchetypeService service) {
        super("investigation");
        this.service = service;
        setObject(this);
    }

    /**
     * Returns the account identifier associated with an investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the location
     * @return the location identifier, or {@code null} if none is defined
     */
    public String accountId(Entity investigationType, Party location) {
        return (investigationType != null && location != null)
               ? getAccountId(investigationType, location.getObjectReference()) : null;
    }

    /**
     * Returns the account identifier associated with an investigation type and practice location on an
     * investigation.
     * <p/>
     * This is invoked using: investigation:accountId()
     *
     * @param context the expression context
     * @return the account identifier, or {@code null} if none is defined
     */
    public String accountId(ExpressionContext context) {
        return accountId(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the account identifier associated with an investigation type and practice location on an
     * investigation.
     * <p/>
     * This is invoked using: investigation:accountId(investigation)
     *
     * @param object the investigation
     * @return the account identifier, or {@code null} if none is defined
     */
    public String accountId(Object object) {
        String result = null;
        object = FunctionHelper.unwrap(object);
        if (object instanceof Act && ((Act) object).isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            IMObjectBean bean = service.getBean((Act) object);
            IMObject investigationType = bean.getTarget("investigationType");
            Reference location = bean.getTargetRef("location");
            if (investigationType != null && location != null) {
                result = getAccountId(investigationType, location);
            }
        }
        return result;
    }

    /**
     * Returns the account identifier associated with an investigation type and practice location.
     * <p/>
     * This is invoked using: investigation:accountId(investigationType, location)
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the account identifier, or {@code null} if none is defined
     */
    public String accountIdForInvestigationType(Object investigationType, Object location) {
        investigationType = FunctionHelper.unwrap(investigationType);
        location = FunctionHelper.unwrap(location);
        if (investigationType instanceof IMObject
            && ((IMObject) investigationType).isA(LaboratoryArchetypes.INVESTIGATION_TYPE)
            && location instanceof Party && ((Party) location).isA(PracticeArchetypes.LOCATION)) {
            return getAccountId((IMObject) investigationType, ((Party) location).getObjectReference());
        }
        return null;
    }

    /**
     * Returns a Function, if any, for the specified namespace, name and parameter types.
     *
     * @param namespace  if it is not the namespace specified in the constructor, the method returns null
     * @param name       is a function name
     * @param parameters the function parameters
     * @return a MethodFunction, or null if there is no such function.
     */
    @Override
    public Function getFunction(String namespace, String name, Object[] parameters) {
        if ("accountId".equals(name) && parameters != null && parameters.length >= 2) {
            name = "accountIdForInvestigationType";
        }
        return super.getFunction(namespace, name, parameters);
    }

    /**
     * Returns the account identifier associated with an investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the location reference
     * @return the location identifier, or {@code null} if none is defined
     */
    private String getAccountId(IMObject investigationType, Reference location) {
        String result = null;
        IMObjectBean typeBean = service.getBean(investigationType);
        Relationship relationship = typeBean.getValue("locations", Relationship.class,
                                                      Predicates.targetEquals(location));
        if (relationship != null) {
            IMObjectBean relationshipBean = service.getBean(relationship);
            result = relationshipBean.getString("accountId");
        }
        return result;
    }
}
