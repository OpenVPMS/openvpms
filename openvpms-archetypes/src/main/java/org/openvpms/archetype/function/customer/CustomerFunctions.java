/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.customer;

import org.apache.commons.jxpath.ExpressionContext;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;

import static org.openvpms.component.system.common.jxpath.FunctionHelper.unwrap;

/**
 * JXPath extension functions for customers.
 *
 * @author Tim Anderson
 */
public class CustomerFunctions extends AbstractObjectFunctions {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Policy to return the first customer participation from an act.
     */
    private static final Policy<Participation> CUSTOMER_PARTICIPATION
            = Policies.any(Participation.class, Predicates.isA(CustomerArchetypes.CUSTOMER_PARTICIPATION));

    /**
     * Constructs a {@link CustomerFunctions}.
     *
     * @param service the archetype service
     */
    public CustomerFunctions(ArchetypeService service) {
        super("customer");
        setObject(this);
        this.service = service;
    }

    /**
     * Returns the customer associated with the supplied context, if any.
     * <p>
     * If the supplied context is an act, the first {@code participation.customer} will be used to return the customer.
     * <p>
     * Invoked using: {@code customer:get()}
     *
     * @param context the expression context. Expected to refer to a customer or act
     * @return the customer, or {@code null} if the context doesn't refer to a customer
     */
    public Party get(ExpressionContext context) {
        return get(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the customer associated with the supplied object, if any.
     * <p>
     * If the supplied object is an act, the first {@code participation.customer} will be used to return the customer.
     * <p>
     * Invoked using: {@code customer:get(object)}
     *
     * @param object the object. May be a customer, act, or {@code null}
     * @return the customer, or {@code null} if the object doesn't refer to a customer
     */
    public Party get(Object object) {
        Party result = null;
        object = unwrap(object);
        if (object instanceof Party) {
            result = getCustomer(object);
        } else if (object instanceof Act) {
            Act act = (Act) object;
            IMObjectBean bean = service.getBean(act);
            result = bean.getTarget(act.getParticipations(), Party.class, CUSTOMER_PARTICIPATION);
        }
        return result;
    }

    /**
     * Helper to get access to the actual patient supplied by JXPath.
     * <p>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object the object to unwrap
     * @return the unwrapped object. May be {@code null}
     */
    private Party getCustomer(Object object) {
        return unwrap(object, Party.class, CustomerArchetypes.PERSON);
    }
}