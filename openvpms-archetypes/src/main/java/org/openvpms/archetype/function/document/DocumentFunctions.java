/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.document;

import org.openvpms.archetype.rules.doc.LongTextReader;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.NodeResolver;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.FunctionHelper;
import org.openvpms.component.system.common.util.PropertyState;

/**
 * Document functions.
 *
 * @author Tim Anderson
 */
public class DocumentFunctions {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookups.
     */
    private final LookupService lookups;

    /**
     * The long text reader.
     */
    private final LongTextReader reader;


    /**
     * Constructs a {@link DocumentFunctions}.
     *
     * @param service the archetype service
     */
    public DocumentFunctions(IArchetypeService service, LookupService lookups) {
        this.service = service;
        this.lookups = lookups;
        reader = new LongTextReader(service);
    }

    /**
     * Returns the value of a text node that is backed by a document if the text is too long.
     *
     * @param object the object
     * @param node   the text node
     * @return the text. May be {@code null}
     */
    public String text(Object object, String node) {
        PropertyState state = getState(object, node);
        String result = getText(state);
        if (result == null && state != null && state.getParent() instanceof DocumentAct) {
            result = reader.getText((DocumentAct) state.getParent());
        }
        return result;
    }

    /**
     * Returns the value of a text node that is backed by a document on a related {@link DocumentAct} if the text is
     * too long.
     *
     * @param object           the object
     * @param node             the text node
     * @param relationshipNode the node for the related {@link DocumentAct}
     * @return the text. May be {@code null}
     */
    public String text(Object object, String node, String relationshipNode) {
        PropertyState state = getState(object, node);
        String result = getText(state);
        if (result == null && state != null && state.getParent() != null) {
            IMObjectBean bean = service.getBean(state.getParent());
            if (bean.hasNode(relationshipNode)) {
                DocumentAct act = bean.getTarget(relationshipNode, DocumentAct.class);
                if (act != null) {
                    result = reader.getText(act);
                }
            }
        }
        return result;
    }

    /**
     * Resolves the state corresponding to a node.
     *
     * @param object the object
     * @param node   the property name
     * @return the resolved state
     */
    private PropertyState getState(Object object, String node) {
        PropertyState result = null;
        object = FunctionHelper.unwrap(object);
        if (object instanceof IMObject) {
            NodeResolver resolver = new NodeResolver((IMObject) object, service, lookups);
            result = resolver.resolve(node);
        }
        return result;
    }

    /**
     * Returns the text from a state.
     *
     * @param state the state. May be {@code null}
     * @return the text. May be {@code null}
     */
    private String getText(PropertyState state) {
        String result = null;
        if (state != null) {
            Object value = state.getValue();
            if (value != null) {
                result = value.toString();
            }
        }
        return result;
    }
}
