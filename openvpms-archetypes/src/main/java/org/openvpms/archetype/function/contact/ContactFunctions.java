/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.contact;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;
import org.openvpms.component.system.common.jxpath.FunctionHelper;

/**
 * Base class for functions operating on contacts.
 *
 * @author Tim Anderson
 */
public abstract class ContactFunctions extends AbstractObjectFunctions {

    /**
     * The party rules.
     */
    private final PartyRules rules;

    /**
     * The contact archetype.
     */
    private final String archetype;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link ContactFunctions}.
     *
     * @param namespace the function namespace
     * @param archetype the contact archetype
     * @param rules     the party rules
     * @param service   the archetype service
     */
    protected ContactFunctions(String namespace, String archetype, PartyRules rules, ArchetypeService service) {
        super(namespace);
        this.rules = rules;
        this.archetype = archetype;
        this.service = service;
        setObject(this);
    }

    /**
     * Returns the preferred contact for the specified party, or the first available contact, if none is preferred.
     *
     * @param object the object. May be a party or party identifier
     * @return the corresponding contact, or {@code null}
     */
    public Contact preferred(Object object) {
        return getContact(getParty(object), false, null);
    }

    /**
     * Returns a contact for the specified party and <em>BILLING</em> purpose.
     * If cannot find one with matching purpose returns the preferred contact.
     * If cannot find with matching purpose and preferred returns the first available.
     *
     * @param object the object. May be a party or party identifier
     * @return the corresponding contact, or {@code null}
     */
    public Contact billing(Object object) {
        return getContact(getParty(object), false, ContactArchetypes.BILLING_PURPOSE);
    }

    /**
     * Returns a contact for the specified party and <em>CORRESPONDENCE</em> purpose.
     * If cannot find one with matching purpose returns the preferred contact.
     * If cannot find with matching purpose and preferred returns the first available.
     *
     * @param object the object. May be a party or party identifier
     * @return the corresponding contact, or {@code null}
     */
    public Contact correspondence(Object object) {
        return getContact(getParty(object), false, ContactArchetypes.CORRESPONDENCE_PURPOSE);
    }

    /**
     * Returns a contact for the specified party and <em>REMINDER</em> purpose.
     * If cannot find one with matching purpose returns the preferred contact.
     * If cannot find with matching purpose and preferred returns the first available.
     *
     * @param object the object. May be a party or party identifier
     * @return the corresponding contact, or {@code null}
     */
    public Contact reminder(Object object) {
        return getContact(getParty(object), false, ContactArchetypes.REMINDER_PURPOSE);
    }

    /**
     * Returns a contact for the specified party and purpose.
     *
     * @param object  the object. May be a party or party identifier
     * @param purpose the contact purpose. May be {@code null}
     * @return the corresponding contact, or {@code null} if no contact with the purpose exists
     */
    public Contact purpose(Object object, String purpose) {
        Party party = getParty(object);
        return (party != null && purpose != null) ? getContact(party, true, purpose) : null;
    }

    /**
     * Formats a contact.
     *
     * @param contact the contact. May be {@code null}
     * @return the formatted contact. May be {@code null}
     */
    public abstract String format(Contact contact);

    /**
     * Returns a contact for the specified party and purpose.
     * If cannot find one with matching purpose returns last preferred contact.
     * If cannot find with matching purpose and preferred returns last found.
     *
     * @param party   the party. May be {@code null}
     * @param exact   if {@code true}, the contact must have the specified purpose
     * @param purpose the contact purpose. May be {@code null}
     * @return the corresponding contact, or {@code null}
     */
    protected Contact getContact(Party party, boolean exact, String purpose) {
        return (party != null) ? rules.getContact(party, archetype, exact, null, purpose) : null;
    }

    /**
     * Returns a party given its identifier.
     *
     * @param id the party identifier
     * @return the corresponding party or {@code null} if none is found
     */
    protected Party getParty(long id) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> root = query.from(Party.class, "party.*", UserArchetypes.USER);
        query.where(builder.equal(root.get("id"), id));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns the party rules.
     *
     * @return the party rules
     */
    protected PartyRules getRules() {
        return rules;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    private Party getParty(Object object) {
        object = FunctionHelper.unwrap(object);
        if (object instanceof Party) {
            return (Party) object;
        } else if (object instanceof Number) {
            return getParty(((Number) object).longValue());
        }
        return null;
    }
}