/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.user;

import org.apache.commons.jxpath.Function;
import org.apache.commons.jxpath.Functions;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Variables;
import org.openvpms.archetype.rules.doc.ImageService;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.AbstractObjectFunctions;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

/**
 * User reporting functions.
 *
 * @author Tim Anderson
 */
public class UserFunctions extends AbstractObjectFunctions {

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The image service.
     */
    private final ImageService imageService;

    /**
     * Functions that may be invoked by the expressions.
     */
    private final Functions functions;

    /**
     * The user functions.
     */
    private static final Logger log = LoggerFactory.getLogger(UserFunctions.class);

    /**
     * Username node.
     */
    private static final String USERNAME = "username";

    /**
     * Title node.
     */
    private static final String TITLE = "title";

    /**
     * First name node.
     */
    private static final String FIRST_NAME = "firstName";

    /**
     * Last name node.
     */
    private static final String LAST_NAME = "lastName";

    /**
     * Qualifications node.
     */
    private static final String QUALIFICATIONS = "qualifications";

    /**
     * Name node.
     */
    private static final String NAME = "name";

    /**
     * Description node.
     */
    private static final String DESCRIPTION = "description";

    /**
     * Constructs an {@link UserFunctions}.
     *
     * @param userRules       the user rules
     * @param service         the archetype service
     * @param practiceService the practice service
     * @param lookups         the lookup service
     * @param imageService    the image service
     * @param functions       functions that may be invoked by the expressions
     */
    public UserFunctions(UserRules userRules, ArchetypeService service, PracticeService practiceService,
                         LookupService lookups, ImageService imageService, Functions functions) {
        super("user");
        setObject(this);
        this.userRules = userRules;
        this.service = service;
        this.practiceService = practiceService;
        this.lookups = lookups;
        this.imageService = imageService;
        this.functions = functions;
    }

    /**
     * Formats the name of a user, according to the specified style.
     *
     * @param user  the user. May be {@code null}
     * @param style the style. One of 'short', 'medium' or 'long'
     * @return the formatted name, or {@code null} if no user is specified
     */
    public String format(User user, String style) {
        String result = null;
        if (user != null) {
            if ("short".equalsIgnoreCase(style)) {
                result = formatName(user, "shortUserNameFormat");
            } else if ("medium".equalsIgnoreCase(style)) {
                result = formatName(user, "mediumUserNameFormat");
            } else {
                result = formatName(user, "longUserNameFormat");
            }
        }
        return result;
    }

    /**
     * Formats the name of a user, according to the specified style.
     *
     * @param id    the user id
     * @param style the style. One of 'short', 'medium' or 'long'
     * @return the formatted name, or {@code null} if no user can be found with the id
     */
    public String formatById(long id, String style) {
        User user = getUser(id);
        return user != null ? format(user, style) : null;
    }

    /**
     * Returns a URL to the signature associated with a user.
     * <p/>
     * Note that the returned URL is to a temporary file and therefore cannot (and for security, shouldn't) be
     * referenced in HTML.
     *
     * @param object either a user or user id. May be {@code null}
     * @return the url. May be {@code null}
     */
    public URL signature(Object object) {
        URL result = null;
        if (object instanceof User) {
            User user = (User) object;
            result = getSignature(user);

        } else if (object instanceof Number) {
            long id = ((Number) object).longValue();
            User user = getUser(id);
            result = getSignature(user);
        }
        return result;
    }

    /**
     * Returns a Function, if any, for the specified namespace, name and parameter types.
     * <p/>
     * This version changes format -> formatById if the first parameter is numeric.
     * <br/>
     * This is required as JXPath thinks the methods are ambiguous if they have the same name.
     *
     * @param namespace  if it is not the namespace specified in the constructor, the method returns null
     * @param name       is a function name.
     * @param parameters the function parameters
     * @return a MethodFunction, or null if there is no such function.
     */
    @Override
    public Function getFunction(String namespace, String name, Object[] parameters) {
        if ("format".equals(name) && parameters.length >= 1 && parameters[0] instanceof Number) {
            name = "formatById";
        }
        return super.getFunction(namespace, name, parameters);
    }

    /**
     * Returns the URL to a user signature.
     *
     * @param user the user
     * @return the URL or {@code null} if the user doesn't have none
     */
    private URL getSignature(User user) {
        URL result = null;
        try {
            DocumentAct signature = userRules.getSignature(user);
            if (signature != null) {
                result = imageService.getURL(signature);
            }
        } catch (Exception exception) {
            log.warn("Failed to retrieve signature for {}: {}", user.getUsername(), exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Formats a user name using the lookup.userNameFormat associated with the specified practice node.
     *
     * @param user the user to format
     * @param node the node
     * @return the formatted name
     */
    private String formatName(final User user, String node) {
        String result = null;
        Party practice = practiceService.getPractice();
        if (practice != null) {
            Lookup lookup = lookups.getLookup(practice, node);
            if (lookup != null) {
                IMObjectBean bean = service.getBean(lookup);
                String expression = bean.getString("expression");
                if (expression != null) {
                    try {
                        IMObjectBean userBean = service.getBean(user);
                        JXPathContext context = JXPathHelper.newContext(user, functions);
                        Variables variables = context.getVariables();
                        variables.declareVariable(USERNAME, userBean.getValue(USERNAME));
                        variables.declareVariable(TITLE, lookups.getName(user, TITLE));
                        variables.declareVariable(FIRST_NAME, userBean.getValue(FIRST_NAME));
                        variables.declareVariable(LAST_NAME, userBean.getValue(LAST_NAME));
                        variables.declareVariable(QUALIFICATIONS, userBean.getValue(QUALIFICATIONS));
                        variables.declareVariable(NAME, userBean.getValue(NAME));
                        variables.declareVariable(DESCRIPTION, userBean.getValue(DESCRIPTION));
                        Object value = context.getValue(expression);
                        if (value != null) {
                            result = value.toString();
                        }
                    } catch (Exception exception) {
                        log.warn(exception.getMessage(), exception);
                    }
                }
            }
        }
        if (result == null) {
            result = user.getName();
        }
        return result;
    }

    /**
     * Returns a user given its identifier.
     *
     * @param id the user identifier
     * @return the corresponding user, or {@code null} if none is found
     */
    private User getUser(long id) {
        return service.get(UserArchetypes.USER, id, User.class);
    }
}
