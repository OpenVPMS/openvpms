/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;

/**
 * Archetype messages.
 *
 * @author Tim Anderson
 */
public class ArchetypeMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("ARCH", ArchetypeMessages.class.getName());

    /**
     * Default constructor.
     */
    private ArchetypeMessages() {

    }

    /**
     * Returns a message to indicate that a unit price was created by a supplier delivery.
     *
     * @param delivery the delivery act
     * @param supplier the supplier
     * @return a new message
     */
    public static Message priceCreatedByDelivery(Act delivery, Party supplier) {
        return messages.create(1501, delivery.getId(), supplier.getName(), delivery.getActivityStartTime());
    }

    /**
     * Returns a message to indicate that a password cannot contain whitespace characters.
     *
     * @return the message
     */
    public static String passwordCannotContainWhitespace() {
        return messages.create(3000).getMessage();
    }

    /**
     * Returns a message to indicate that a password is too short.
     *
     * @param minLength the minimum password length
     * @return the message
     */
    public static String passwordLessThanMinimumLength(int minLength) {
        return messages.create(3001, minLength).getMessage();
    }

    /**
     * Returns a message to indicate that a password is too long.
     *
     * @param maxLength the maximum password length
     * @return the message
     */
    public static String passwordGreaterThanMaximumLength(int maxLength) {
        return messages.create(3002, maxLength).getMessage();
    }

    /**
     * Returns a message to indicate that a password must contain a lowercase letter.
     *
     * @return the message
     */
    public static String passwordMustContainLowercaseCharacter() {
        return messages.create(3003).getMessage();
    }

    /**
     * Returns a message to indicate that a password must contain an uppercase letter.
     *
     * @return the message
     */
    public static String passwordMustContainUppercaseCharacter() {
        return messages.create(3004).getMessage();
    }

    /**
     * Returns a message to indicate that a password must contain a number.
     *
     * @return the message
     */
    public static String passwordMustContainNumber() {
        return messages.create(3005).getMessage();
    }

    /**
     * Returns a message to indicate that a password must contain a special character.
     *
     * @return the message
     */
    public static String passwordMustContainSpecialCharacter() {
        return messages.create(3006).getMessage();
    }

    /**
     * Returns a message to indicate that a password character is invalid.
     *
     * @param character the character
     * @return the message
     */
    public static String passwordCharacterInvalid(char character) {
        return messages.create(3007, character).getMessage();
    }

    /**
     * Returns a message to indicate that a password character.
     * <p/>
     * This is provided for JSP pages; the page must prepend the invalid character.
     *
     * @return the message
     */
    public static String passwordCharacterInvalidSuffix() {
        return messages.create(3008).getMessage();
    }

    /**
     * Returns a message to indicate that the new password must be different to the old password.
     *
     * @return the message
     */
    public static String newPasswordMustBeDifferentToOldPassword() {
        return messages.create(3009).getMessage();
    }
}
