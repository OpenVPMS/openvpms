/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Base class for document templates.
 *
 * @author Tim Anderson
 */
public class BaseDocumentTemplate {

    /**
     * The bean to access the template's properties.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Cached template.
     */
    private DocumentAct act;

    /**
     * Constructs an {@link BaseDocumentTemplate}.
     *
     * @param template the template
     * @param service the archetype service
     */
    public BaseDocumentTemplate(Entity template, ArchetypeService service) {
        bean = service.getBean(template);
        this.service = service;
    }

    /**
     * Returns the document template name.
     *
     * @return the document template name. May be {@code null}
     */
    public String getName() {
        return getEntity().getName();
    }

    /**
     * Returns the document template description.
     *
     * @return the document template description. May be {@code null}
     */
    public String getDescription() {
        return getEntity().getDescription();
    }

    /**
     * Determines if the template is active.
     *
     * @return {@code true} if the template is active; otherwise it is inactive
     */
    public boolean isActive() {
        return getEntity().isActive();
    }

    /**
     * Determines if there is a document is associated with the template.
     *
     * @return {@code true}  if there is a document is associated with the template, otherwise {@code false}
     */
    public boolean hasDocument() {
        DocumentAct act = getDocumentAct();
        return act != null && act.getDocument() != null;
    }

    /**
     * Returns the file name of the document associated with the template.
     *
     * @return the file name, or {@code null} if no document is associated with the template
     */
    public String getFileName() {
        DocumentAct act = getDocumentAct();
        return (act != null) ? act.getFileName() : null;
    }

    /**
     * Returns the mime type of the template.
     *
     * @return the mime type, or {@code null} if no document is associated with the template
     */
    public String getMimeType() {
        DocumentAct act = getDocumentAct();
        return act != null ? act.getMimeType() : null;
    }

    /**
     * Returns the name of the document associated with the template.
     *
     * @return the document name, or {@code null} if none is found
     */
    public String getDocumentName() {
        DocumentAct act = getDocumentAct();
        return (act != null) ? act.getName() : null;
    }

    /**
     * Returns the document associated with the template.
     *
     * @return the corresponding document, or {@code null} if none is found
     */
    public Document getDocument() {
        DocumentAct act = getDocumentAct();
        return act != null && act.getDocument() != null ? service.get(act.getDocument(), Document.class) : null;
    }

    /**
     * Returns the document act associated with the template
     *
     * @return the document act, or {@code null} if none exists
     */
    public DocumentAct getDocumentAct() {
        if (act == null) {
            act = new TemplateHelper(service).getDocumentAct(getEntity());
        }
        return act;
    }

    /**
     * Returns the <em>entity.documentTemplate</em> entity.
     *
     * @return the entity
     */
    public Entity getEntity() {
        return bean.getObject(Entity.class);
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return getEntity().hashCode();
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof BaseDocumentTemplate)
               && ((BaseDocumentTemplate) obj).getEntity().equals(getEntity());
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the template bean.
     *
     * @return the template bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }
}