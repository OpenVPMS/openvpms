/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.reminder;

import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Customer account reminder rules.
 *
 * @author Tim Anderson
 */
public class AccountReminderRules {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Error node.
     */
    private static final String ERROR = "error";

    /**
     * Charge node.
     */
    private static final String CHARGE = "charge";

    /**
     * Status node.
     */
    private static final String STATUS = "status";

    /**
     * Send reminders node.
     */
    private static final String SEND_REMINDER = "sendReminder";

    /**
     * Reminders node.
     */
    private static final String REMINDERS = "reminders";

    /**
     * Constructs an {@link AccountReminderRules}.
     *
     * @param accountRules       the customer account rules
     * @param insuranceRules     the insurance rules
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public AccountReminderRules(CustomerAccountRules accountRules, InsuranceRules insuranceRules,
                                IArchetypeService service, PlatformTransactionManager transactionManager) {
        if (service instanceof IArchetypeRuleService) {
            throw new IllegalArgumentException("Argument 'service' should not implement "
                                               + IArchetypeRuleService.class.getSimpleName());
            // don't want to trigger rules when saving charges.
        }
        this.accountRules = accountRules;
        this.insuranceRules = insuranceRules;
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Determines if the customer should be notified of an unpaid charge.
     * <p/>
     * A customer should be notified if the charge:
     * <ul>
     *     <li>is POSTED; and</li>
     *     <li>its sendReminder flag is {@code true}</li>
     *     <li>has a positive total; and</li>
     *     <li>has a balance &gt;= the minimum balance; and</li>
     *     <li>is not in an invoice in a gap claim (an invoice in a standard claim is fully paid.)</li>
     * </ul>
     *
     * @param charge     the charge
     * @param minBalance the minimum balance. Any balance less than this does not require a reminder
     * @return {@code true} if the customer should be notified, otherwise {@code false}
     */
    public boolean needsReminder(FinancialAct charge, BigDecimal minBalance) {
        boolean result = false;
        if (FinancialActStatus.POSTED.equals(charge.getStatus())
            && hasBalanceGreaterThanOrEqualToMinBalance(charge, minBalance)) {
            IMObjectBean bean = service.getBean(charge);
            result = sendReminderFlagSet(bean) && isPaymentOverdue(bean) && notClaimed(charge);
        }
        return result;
    }

    /**
     * Resolves an error on a reminder, enabling it to be resent.
     *
     * @param reminder the reminder
     * @return {@code true} if the reminder was updated, otherwise {@code false}
     */
    public boolean resolveError(Act reminder) {
        boolean result = false;
        checkArchetype(reminder);
        String status = reminder.getStatus();
        if (ReminderItemStatus.ERROR.equals(status)) {
            IMObjectBean bean = service.getBean(reminder);
            bean.setValue(STATUS, ReminderItemStatus.PENDING);
            bean.setValue(ERROR, null);
            service.save(reminder);
            result = true;
        }
        return result;
    }

    /**
     * Enables or disables reminders for a charge.
     *
     * @param charge the charge
     * @return {@code true} if the charge was updated
     */
    public boolean enableReminders(FinancialAct charge) {
        boolean result = false;
        if (canHaveReminders(charge) && isUnpaid(charge)) {
            IMObjectBean bean = service.getBean(charge);
            if (!sendReminderFlagSet(bean)) {
                bean.setValue(SEND_REMINDER, true);
                result = true;
                service.save(charge);
            }
        }
        return result;
    }

    /**
     * Disables reminders for a charge.
     * <p/>
     * If there are any PENDING or ERROR reminders, these will be removed.
     *
     * @param charge the charge
     */
    public boolean disableReminders(FinancialAct charge) {
        boolean result = false;
        IMObjectBean bean = service.getBean(charge);
        if (canHaveReminders(charge) && sendReminderFlagSet(bean)) {
            TransactionTemplate template = new TransactionTemplate(transactionManager);
            template.executeWithoutResult(transactionStatus -> {
                bean.setValue(SEND_REMINDER, false);
                List<Act> toRemove = new ArrayList<>();
                for (Act reminder : bean.getTargets(REMINDERS, Act.class)) {
                    if (ReminderItemStatus.PENDING.equals(reminder.getStatus())
                        || ReminderItemStatus.ERROR.equals(reminder.getStatus())) {
                        bean.removeTargets(REMINDERS, reminder, CHARGE);
                        toRemove.add(reminder);
                    }
                }
                service.save(bean.getObject());
                for (Act reminder : toRemove) {
                    service.remove(reminder);
                }
            });
            result = true;
        }
        return result;
    }

    /**
     * Determines if reminders can be enabled for a charge.
     *
     * @param act the charge act
     * @return {@code true} if reminders can be enabled, otherwise {@code false}
     */
    public boolean canEnableReminders(FinancialAct act) {
        return canHaveReminders(act) && isUnpaid(act) && !sendReminderFlagSet(act);
    }

    /**
     * Determines if reminders can be disabled for a charge.
     *
     * @param act the charge act
     * @return {@code true} if reminders can be disabled, otherwise {@code false}
     */
    public boolean canDisableReminders(FinancialAct act) {
        return canHaveReminders(act) && sendReminderFlagSet(act);
    }

    /**
     * Determines if the 'send reminder' flag is set for a charge.
     *
     * @param charge the charge
     * @return {@code true} if the 'send reminder' flag is set, otherwise {@code false}
     */
    private boolean sendReminderFlagSet(FinancialAct charge) {
        return sendReminderFlagSet(service.getBean(charge));
    }

    /**
     * Determines if the 'send reminder' flag is set for a charge.
     *
     * @param charge the charge
     * @return {@code true} if the 'send reminder' flag is set, otherwise {@code false}
     */
    private boolean sendReminderFlagSet(IMObjectBean charge) {
        return charge.getBoolean(SEND_REMINDER);
    }

    /**
     * Determines if payment of a charge is overdue.
     *
     * @param charge the charge
     * @return {@code true} if payment is overdue, otherwise {@code false}
     */
    private boolean isPaymentOverdue(IMObjectBean charge) {
        boolean result = false;
        Date completed = charge.getDate("endTime");
        Party customer = charge.getTarget("customer", Party.class);
        if (completed != null && customer != null) {
            Date now = new Date();
            Date overdueDate = accountRules.getOverdueDate(customer, now);
            result = completed.before(overdueDate);
        }
        return result;
    }

    /**
     * Determines if a charge hasn't been claimed.
     *
     * @param charge the charge
     * @return {@code true} if the charge hasn't been claimed, {@code false} if it has
     */
    private boolean notClaimed(FinancialAct charge) {
        boolean result;
        if (charge.isA(CustomerAccountArchetypes.INVOICE)) {
            result = !insuranceRules.isClaimed(charge);
        } else {
            result = true;
        }
        return result;
    }

    /**
     * Determines if a charge has a balance {@code >=} than the minimum balance.
     *
     * @param charge     the charge
     * @param minBalance the minimum balance
     * @return {@code true} if the balance is {@code >=} than the minimum balance
     */
    private boolean hasBalanceGreaterThanOrEqualToMinBalance(FinancialAct charge, BigDecimal minBalance) {
        boolean result = false;
        BigDecimal total = charge.getTotal();
        if (MathRules.isPositive(total)) {
            BigDecimal remaining = total.subtract(charge.getAllocatedAmount());
            if (!MathRules.isZero(remaining) && remaining.compareTo(minBalance) >= 0) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Determines if an act can have reminders.
     *
     * @param act the act
     * @return {@code true} if the act is an invoice or counter sale
     */
    private boolean canHaveReminders(FinancialAct act) {
        return act.isA(CustomerAccountArchetypes.INVOICE, CustomerAccountArchetypes.COUNTER);
    }

    /**
     * Determines if an act is unpaid.
     * <p/>
     * NOTE: only applies to positive charges.
     *
     * @param act the act
     * @return {@code true} if the act is unpaid
     */
    private boolean isUnpaid(FinancialAct act) {
        return act.getTotal().compareTo(act.getAllocatedAmount()) > 0;
    }

    /**
     * Verifies a reminder has the correct archetype.
     *
     * @param reminder the reminder
     * @throws IllegalArgumentException if the archetype is invalid
     */
    private void checkArchetype(Act reminder) {
        if (!reminder.isA(AccountReminderArchetypes.CHARGE_REMINDER_SMS)) {
            throw new IllegalArgumentException("Invalid reminder archetype: " + reminder.getArchetype());
        }
    }
}
