/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.i18n.ArchetypeMessages;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.ActStatusHelper;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.PriceCollector;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ProductPriceUpdater;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;


/**
 * Processes <em>POSTED</em> deliveries and returns.
 * <p/>
 * For each item in a delivery/return, updates:
 * <ol>
 * <li>the <em>receivedQuantity</em> node of the associated order (if any)</li>
 * <li>the <em>entityLink.productSupplier</em> associated with the product and supplier, if the item is a delivery</li>
 * <li>the <em>quantity</em> node of the * <em>entityLink.productStockLocation</em> associated with the product and
 * stock location</li>
 * </ol>
 * If an order item changes status, the delivery status of the parent
 * order is then re-evaluated.
 * <p/>
 * For each delivery item with a <em>batchNumber</em> or <em>expiryDate</em>, determines if an
 * <em>entity.productBatch</em> exists. If not, one will be created.
 *
 * @author Tim Anderson
 */
public class DeliveryProcessor {

    /**
     * Cache of orders that need to have their delivery statuses re-evaluated.
     */
    private final Map<Reference, FinancialAct> orders = new HashMap<>();

    /**
     * Cache of order item statuses.
     */
    private final Map<Reference, DeliveryStatus> statuses = new HashMap<>();

    /**
     * The delivery/return act.
     */
    private final Act act;

    /**
     * Product rules.
     */
    private final ProductRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Product price updater.
     */
    private final ProductPriceUpdater priceUpdater;

    /**
     * If {@code true}, don't update prices if the list price is less than the existing list price
     * for the product-supplier relationship.
     */
    private final boolean ignoreListPriceDecreases;

    /**
     * Maintains the set of objects to save on completion.
     */
    private final Set<IMObject> toSave = new LinkedHashSet<>();

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The message to include in the notes field of new unit prices.
     */
    private String notes;

    /**
     * Quantity node name.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Received quantity node name.
     */
    private static final String RECEIVED_QUANTITY = "receivedQuantity";

    /**
     * Package size node name.
     */
    private static final String PACKAGE_SIZE = "packageSize";

    /**
     * Stock location node name.
     */
    private static final String STOCK_LOCATION = "stockLocation";

    /**
     * Constructs a {@link DeliveryProcessor}.
     *
     * @param act        the delivery/return act
     * @param service    the archetype service
     * @param currencies the currency cache
     * @param lookups    the lookup service
     */
    public DeliveryProcessor(Act act, IArchetypeService service, Currencies currencies, LookupService lookups) {
        this.act = act;
        this.service = service;
        this.rules = new ProductRules(service, lookups);
        ProductPriceRules priceRules = new ProductPriceRules(service);
        PracticeRules practiceRules = new PracticeRules(service, currencies);
        priceUpdater = new ProductPriceUpdater(priceRules, practiceRules, service);
        ignoreListPriceDecreases = practiceRules.ignoreListPriceDecreases(priceUpdater.getPractice());
    }

    /**
     * Applies changes, if the act is POSTED and hasn't already been posted.
     *
     * @throws ArchetypeServiceException  for any archetype service error
     * @throws DeliveryProcessorException if an item is missing a product
     */
    public void apply() {
        if (ActStatus.POSTED.equals(act.getStatus()) && !ActStatusHelper.isPosted(act, service)) {
            IMObjectBean bean = service.getBean(act);
            supplier = bean.getTarget("supplier", Party.class);
            stockLocation = bean.getTarget(STOCK_LOCATION, Party.class);
            for (Act item : bean.getTargets("items", Act.class)) {
                processItem(item);
            }

            // for each order that has order items that have changed, update
            // their delivery status where required
            for (Act order : orders.values()) {
                updateDeliveryStatus(order);
            }

            if (!toSave.isEmpty()) {
                service.save(toSave);
            }
        }
    }

    /**
     * Returns the delivery status of an <em>act.supplierOrderItem</em>.
     *
     * @param orderItem the order item
     * @param service   the archetype service
     * @return the delivery status of the order item
     */
    public static DeliveryStatus getDeliveryStatus(FinancialAct orderItem, ArchetypeService service) {
        IMObjectBean bean = service.getBean(orderItem);
        BigDecimal quantity = bean.getBigDecimal(QUANTITY, BigDecimal.ZERO);
        BigDecimal received = bean.getBigDecimal(RECEIVED_QUANTITY, BigDecimal.ZERO);
        BigDecimal cancelled = bean.getBigDecimal("cancelledQuantity", BigDecimal.ZERO);
        return DeliveryStatus.getStatus(quantity, received, cancelled);
    }

    /**
     * Processes a delivery/return item.
     *
     * @param item the delivery/return item
     * @throws ArchetypeServiceException  for any archetype service error
     * @throws DeliveryProcessorException if the item is missing a product
     */
    private void processItem(Act item) {
        IMObjectBean itemBean = service.getBean(item);
        BigDecimal receivedQuantity = itemBean.getBigDecimal(QUANTITY);
        int receivedPackSize = itemBean.getInt(PACKAGE_SIZE);
        boolean delivery = act.isA(SupplierArchetypes.DELIVERY);
        if (!delivery) {
            receivedQuantity = receivedQuantity.negate();
        }
        Product product = itemBean.getTarget("product", Product.class);
        if (product == null) {
            throw new DeliveryProcessorException(DeliveryProcessorException.ErrorCode.NoProduct,
                                                 DescriptorHelper.getDisplayName(item, service), item.getId(),
                                                 DescriptorHelper.getDisplayName(act, service), act.getId());
        }

        // update the associated order's received quantity
        for (FinancialAct orderItem : itemBean.getTargets("order", FinancialAct.class)) {
            updateReceivedQuantity(orderItem, receivedQuantity, receivedPackSize);
        }

        if (delivery) {
            // update the product-supplier relationship
            if (supplier != null) {
                updateProductSupplier(product, itemBean);
            }
            String batchNumber = StringUtils.trimToNull(itemBean.getString("batchNumber"));
            Date expiryDate = itemBean.getDate("expiryDate");
            Reference manufacturer = itemBean.getTargetRef("manufacturer");
            if (batchNumber != null || expiryDate != null) {
                updateBatch(product, batchNumber, expiryDate, manufacturer);
            }
        }

        // update the stock quantity for the product at the stock location
        if (stockLocation != null) {
            updateStockQuantity(product, stockLocation, receivedQuantity, receivedPackSize);
        }
    }

    /**
     * Creates a batch if one doesn't exist, or updates an existing batch if required.
     *
     * @param product      the product
     * @param batchNumber  the batch number. May be {@code null}
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the product manufacturer. May be {@code null}
     */
    private void updateBatch(Product product, String batchNumber, Date expiryDate, Reference manufacturer) {
        Reference productRef = product.getObjectReference();
        List<Entity> batches = rules.getBatches(productRef, batchNumber, expiryDate, manufacturer);
        if (batches.isEmpty()) {
            if (batchNumber == null) {
                batchNumber = product.getName();
            }
            if (!StringUtils.isEmpty(batchNumber)) {
                Entity batch = rules.createBatch(productRef, batchNumber, expiryDate, manufacturer);
                updateBatchLocation(batch);
                toSave.add(batch);
            }
        } else {
            Entity batch = batches.get(0);
            if (updateBatchLocation(batch)) {
                toSave.add(batch);
            }
        }
    }

    /**
     * Updates a batch with the stock location, if required.
     *
     * @param batch the batch
     * @return {@code true} if the location was added
     */
    private boolean updateBatchLocation(Entity batch) {
        IMObjectBean bean = service.getBean(batch);
        if (!bean.getTargetRefs("stockLocations").contains(stockLocation.getObjectReference())) {
            bean.addTarget("stockLocations", stockLocation);
            return true;
        }
        return false;
    }

    /**
     * Updates the quantity of a product at a stock location.
     * <p/>
     * If no <em>entityLink.productStockLocation</em> exists for the
     * product and stock  location, one will be created.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param quantity      the quantity received
     * @param packageSize   the size of the package
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void updateStockQuantity(Product product, Party stockLocation, BigDecimal quantity, int packageSize) {
        IMObjectBean bean = service.getBean(product);
        if (bean.hasNode("stockLocations")) {
            Predicate<Relationship> predicate = Predicates.activeNow().and(Predicates.targetEquals(stockLocation));
            Relationship relationship = bean.getValue("stockLocations", Relationship.class, predicate);
            if (relationship == null) {
                relationship = bean.addTarget("stockLocations", stockLocation);
                toSave.add(product);
            } else {
                toSave.add(relationship);
            }
            BigDecimal units = quantity.multiply(BigDecimal.valueOf(packageSize));
            IMObjectBean relBean = service.getBean(relationship);
            BigDecimal stockQuantity = relBean.getBigDecimal(QUANTITY);
            stockQuantity = stockQuantity.add(units);
            if (stockQuantity.compareTo(BigDecimal.ZERO) < 0) {
                stockQuantity = BigDecimal.ZERO;
            }
            relBean.setValue(QUANTITY, stockQuantity);
        }
    }

    /**
     * Updates the <em>receivedQuantity</em> node of an order item.
     *
     * @param orderItem   the order item
     * @param quantity    the quantity in the delivery/return. If return, the quantity will be negative
     * @param packageSize the size of the package
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void updateReceivedQuantity(FinancialAct orderItem, BigDecimal quantity, int packageSize) {
        IMObjectBean bean = service.getBean(orderItem);
        BigDecimal received = bean.getBigDecimal(RECEIVED_QUANTITY);
        BigDecimal cancelled = bean.getBigDecimal("cancelledQuantity");
        BigDecimal orderQuantity = orderItem.getQuantity();
        DeliveryStatus oldStatus = DeliveryStatus.getStatus(orderQuantity, received, cancelled);

        int orderedPackSize = bean.getInt(PACKAGE_SIZE);
        if (packageSize != orderedPackSize && orderedPackSize != 0) {
            // need to convert the quantity to the order package quantity
            quantity = quantity.multiply(BigDecimal.valueOf(packageSize));
            quantity = MathRules.divide(quantity, orderedPackSize, 3);
        }
        received = received.add(quantity);
        if (received.compareTo(BigDecimal.ZERO) < 0) {
            received = BigDecimal.ZERO;
        }
        bean.setValue(RECEIVED_QUANTITY, received);
        toSave.add(orderItem);

        DeliveryStatus newStatus = DeliveryStatus.getStatus(orderQuantity, received, cancelled);

        // cache the order item's delivery status
        statuses.put(orderItem.getObjectReference(), newStatus);
        if (oldStatus != newStatus) {
            // the order item's status has changed, so need to cache the
            // associated order in order to determine if its delivery status
            // needs to be updated once the delivery/return has been processed
            loadOrder(bean);
        }
    }

    /**
     * Updates the delivery status of an order, if required.
     *
     * @param order the order
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void updateDeliveryStatus(Act order) {
        IMObjectBean bean = service.getBean(order);
        DeliveryStatus current = DeliveryStatus.valueOf(order.getStatus2());
        DeliveryStatus newStatus = null;
        boolean pending = false;
        boolean part = false;
        boolean full = false;
        for (ActRelationship relationship : bean.getValues("items", ActRelationship.class)) {
            Reference target = relationship.getTarget();
            DeliveryStatus status = getDeliveryStatus(target);
            if (status == DeliveryStatus.PART) {
                part = true;
                break;
            } else if (status == DeliveryStatus.FULL) {
                full = true;
            } else {
                pending = true;
            }
        }
        if (part || (pending && full)) {
            newStatus = DeliveryStatus.PART;
        } else if (!pending && full) {
            newStatus = DeliveryStatus.FULL;
        } else if (pending) {
            newStatus = DeliveryStatus.PENDING;
        }
        if (newStatus != null && newStatus != current) {
            order.setStatus2(newStatus.toString());
            toSave.add(order);
        }
    }

    /**
     * Returns the delivery status of an order item.
     *
     * @param ref a reference to the order item. May be {@code null}
     * @return the delivery staus of the item, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    private DeliveryStatus getDeliveryStatus(Reference ref) {
        DeliveryStatus result = null;
        if (ref != null) {
            result = statuses.get(ref);
            if (result == null) {
                FinancialAct item = getAct(ref);
                if (item != null) {
                    result = getDeliveryStatus(item, service);
                }
            }
        }
        return result;
    }

    /**
     * Retrieves the order associated with an order item.
     * <p/>
     * The order is cached in {@link #orders}.
     *
     * @param bean the bean wrapping the order item
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void loadOrder(IMObjectBean bean) {
        Reference ref = bean.getSourceRef("order");
        if (ref != null) {
            FinancialAct order = orders.get(ref);
            if (order == null) {
                order = getAct(ref);
                if (order != null) {
                    orders.put(ref, order);
                }
            }
        }
    }

    /**
     * Retrieves an act given its reference.
     *
     * @param ref the act reference
     * @return the act, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    private FinancialAct getAct(Reference ref) {
        return (ref != null) ? service.get(ref, FinancialAct.class) : null;
    }

    /**
     * Updates an <em>entityLink.productSupplier</em> from an <em>act.supplierDeliveryItem</em>, if required.
     *
     * @param product          the product
     * @param deliveryItemBean a bean wrapping the delivery item
     */
    private void updateProductSupplier(Product product, IMObjectBean deliveryItemBean) {
        int size = deliveryItemBean.getInt(PACKAGE_SIZE);
        String units = deliveryItemBean.getString("packageUnits");
        String reorderCode = deliveryItemBean.getString("reorderCode");
        String reorderDesc = deliveryItemBean.getString("reorderDescription");
        BigDecimal listPrice = deliveryItemBean.getBigDecimal("listPrice");
        BigDecimal nettPrice = deliveryItemBean.getBigDecimal("unitPrice");
        ProductSupplier ps = rules.getProductSupplier(product, supplier, reorderCode, size, units);
        boolean save = true;
        if (ps == null) {
            // no product-supplier relationship, so create a new one
            ps = rules.createProductSupplier(product, supplier);
            ps.setPackageSize(size);
            ps.setPackageUnits(units);
            ps.setReorderCode(reorderCode);
            ps.setReorderDescription(reorderDesc);
            ps.setListPrice(listPrice);
            ps.setNettPrice(nettPrice);
            boolean preferred = rules.getPreferredSupplier(product) == null;
            ps.setPreferred(preferred);
        } else if (size != ps.getPackageSize()
                   || !Objects.equals(units, ps.getPackageUnits())
                   || !MathRules.equals(listPrice, ps.getListPrice())
                   || !MathRules.equals(nettPrice, ps.getNettPrice())
                   || !Objects.equals(ps.getReorderCode(), reorderCode)
                   || !Objects.equals(ps.getReorderDescription(), reorderDesc)) {
            // properties are different to an existing relationship
            ps.setPackageSize(size);
            ps.setPackageUnits(units);
            ps.setReorderCode(reorderCode);
            ps.setReorderDescription(reorderDesc);
            ps.setListPrice(listPrice);
            ps.setNettPrice(nettPrice);
        } else {
            save = false;
        }
        if (ps.isAutoPriceUpdate() && supplier.isActive()) {
            // only update prices if the supplier is active
            updateUnitPrices(product, ps);
        }

        if (save) {
            toSave.add(ps.getRelationship());
        }
    }

    /**
     * Recalculates the cost node of any <em>productPrice.unitPrice</em> associated with the product.
     *
     * @param product         the product
     * @param productSupplier the product supplier
     */
    private void updateUnitPrices(Product product, ProductSupplier productSupplier) {
        PriceCollector collector = new PriceCollector(service);
        priceUpdater.update(product, productSupplier, ignoreListPriceDecreases, collector);
        for (ProductPrice price : collector.getPrices()) {
            if (price.isNew()) {
                IMObjectBean bean = service.getBean(price);
                bean.setValue("notes", getUnitPriceNotes());
            }
        }
        toSave.addAll(collector.prepareSave());
    }

    /**
     * Returns the unit price notes.
     *
     * @return the unit price notes
     */
    private String getUnitPriceNotes() {
        if (notes == null) {
            notes = ArchetypeMessages.priceCreatedByDelivery(act, supplier).getMessage();
        }
        return notes;
    }

}