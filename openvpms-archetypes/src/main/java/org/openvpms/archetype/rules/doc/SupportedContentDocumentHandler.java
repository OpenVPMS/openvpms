/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.InputStream;

/**
 * A {@link DocumentHandler} that determines what documents are supported based on a list of file name extensions
 * and mime types passed at construction.
 *
 * @author Tim Anderson
 */
public abstract class SupportedContentDocumentHandler implements DocumentHandler {

    /**
     * The supported file extensions.
     */
    private final String[] supportedExtensions;

    /**
     * The supported mime types.
     */
    private final String[] supportedMimeTypes;

    /**
     * The document archetype.
     */
    private final String documentArchetype;

    /**
     * The handler to delegate to.
     */
    private final DocumentHandler handler;

    /**
     * Constructs a {@link SupportedContentDocumentHandler}.
     *
     * @param supportedExtensions the file name extensions supported by the handler
     * @param supportedMimeTypes  the mime types supported by the handler
     * @param service             the archetype service
     */
    public SupportedContentDocumentHandler(String[] supportedExtensions, String[] supportedMimeTypes,
                                           ArchetypeService service) {
        this(supportedExtensions, supportedMimeTypes, DocumentArchetypes.DEFAULT_DOCUMENT, service);
    }

    /**
     * Constructs a {@link SupportedContentDocumentHandler}.
     *
     * @param supportedExtensions the file name extensions supported by the handler
     * @param supportedMimeTypes  the mime types supported by the handler
     * @param documentArchetype   the document archetype
     * @param service             the archetype service
     */
    public SupportedContentDocumentHandler(String[] supportedExtensions, String[] supportedMimeTypes,
                                           String documentArchetype, ArchetypeService service) {
        this(supportedExtensions, supportedMimeTypes, documentArchetype,
             new DefaultDocumentHandler(documentArchetype, service));
    }

    /**
     * Constructs a {@link SupportedContentDocumentHandler}.
     *
     * @param supportedExtensions the file name extensions supported by the handler
     * @param supportedMimeTypes  the mime types supported by the handler
     * @param documentArchetype   the document archetype
     * @param handler             the document handler
     */
    public SupportedContentDocumentHandler(String[] supportedExtensions, String[] supportedMimeTypes,
                                           String documentArchetype, DocumentHandler handler) {
        this.supportedExtensions = supportedExtensions;
        this.supportedMimeTypes = supportedMimeTypes;
        this.documentArchetype = documentArchetype;
        this.handler = handler;
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name     the document name
     * @param mimeType the mime type of the document. May be {@code null}
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(String name, String mimeType) {
        return isSupportedExtension(name) || (mimeType != null && isSupportedMimeType(mimeType));
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name      the document name
     * @param archetype the document archetype
     * @param mimeType  the mime type of the document. May be {@code null}
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(String name, String archetype, String mimeType) {
        return documentArchetype.equals(archetype) && canHandle(name, mimeType);
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param document the document
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(org.openvpms.component.model.document.Document document) {
        return documentArchetype.equals(document.getArchetype());
    }

    /**
     * Creates a new {@link Document} from a stream.
     *
     * @param name     the document name. Any path information is removed.
     * @param stream   a stream representing the document content
     * @param mimeType the mime type of the document. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     * @return a new document
     * @throws DocumentException         if the document can't be created
     * @throws ArchetypeServiceException for any archetype service error
     * @throws OpenVPMSException         for any other error
     */
    @Override
    public Document create(String name, InputStream stream, String mimeType, int size) {
        return handler.create(name, stream, mimeType, size);
    }

    /**
     * Creates a new {@link Document} from a byte array.
     *
     * @param name     the document name. Any path information is removed.
     * @param content  the serialized content
     * @param mimeType the mime type of the content. May be {@code null}
     * @param size     the uncompressed document size
     * @return a new document
     * @throws DocumentException         if the document can't be created
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document create(String name, byte[] content, String mimeType, int size) {
        return handler.create(name, content, mimeType, size);
    }

    /**
     * Updates a {@link Document} from a stream.
     *
     * @param document the document to update
     * @param stream   a stream representing the new document content
     * @param mimeType the mime type of the document. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     */
    @Override
    public void update(Document document, InputStream stream, String mimeType, int size) {
        handler.update(document, stream, mimeType, size);
    }

    /**
     * Returns the document content as a stream.
     *
     * @param document the document
     * @return the document content
     * @throws DocumentException for any error
     */
    @Override
    public InputStream getContent(org.openvpms.component.model.document.Document document) {
        return handler.getContent(document);
    }

    /**
     * Determines if a file is supported based on its extension.
     *
     * @param name the file name
     * @return {@code true} if the name has a supported extension
     */
    protected boolean isSupportedExtension(String name) {
        for (String ext : supportedExtensions) {
            if (StringUtils.endsWithIgnoreCase(name, ext)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines if a file is supported based on its mime type.
     *
     * @param mimeType the file mime type
     * @return {@code true} if the mime type is supported
     */
    protected boolean isSupportedMimeType(String mimeType) {
        for (String type : supportedMimeTypes) {
            if (type.equalsIgnoreCase(mimeType)) {
                return true;
            }
        }
        return false;
    }
}
