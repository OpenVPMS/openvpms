/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;


/**
 * Rules for saving <em>act.customerAccountInvoiceItem</em>s.
 *
 * @author Tim Anderson
 */
class InvoiceItemSaveRules {

    /**
     * The invoice item.
     */
    private final IMObjectBean itemBean;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The product.
     */
    private IMObjectBean productBean;

    /**
     * Constructs an {@link InvoiceItemSaveRules}.
     *
     * @param act     the invoice item act
     * @param service the archetype service
     */
    public InvoiceItemSaveRules(Act act, IArchetypeService service) {
        this.service = service;
        if (!TypeHelper.isA(act, CustomerAccountArchetypes.INVOICE_ITEM)) {
            throw new IllegalArgumentException("Invalid argument 'act'");
        }
        itemBean = service.getBean(act);
        Product product = itemBean.getTarget("product", Product.class);
        if (product != null) {
            productBean = service.getBean(product);
        }
    }

    /**
     * Invoked after the invoice item is saved. This:
     * <ul>
     * <li>processes any demographic updates associated with the product</li>
     * </ul>
     * Note that the dispensing acts must be saved <em>prior</em> to the invoice
     * item.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void save() {
        if (productBean != null) {
            DemographicUpdateHelper helper = new DemographicUpdateHelper(itemBean, productBean, service);
            helper.processDemographicUpdates();
        }
    }

}
