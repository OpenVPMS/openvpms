/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.component.business.domain.im.common.SequencedPeriodRelationship;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.functor.SequenceComparator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;

import java.util.ArrayList;
import java.util.List;


/**
 * Rules for <em>party.organisationLocation</em> instances.
 *
 * @author Tim Anderson
 */
public class LocationRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Schedule views node name.
     */
    private static final String SCHEDULE_VIEWS = "scheduleViews";

    /**
     * Worklist views node name.
     */
    private static final String WORK_LIST_VIEWS = "workListViews";


    /**
     * Constructs a {@link LocationRules}.
     *
     * @param service the archetype service
     */
    public LocationRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the practice associated with a location.
     *
     * @param location the location
     * @return the practice associated with the location, or {@code null} if none is found
     */
    public Party getPractice(Party location) {
        return service.getBean(location).getSource("practice", Party.class);
    }

    /**
     * Returns the default deposit account associated with a location.
     *
     * @param location the location
     * @return the default deposit account or {@code null} if none is found
     */
    public Party getDefaultDepositAccount(Party location) {
        return (Party) EntityRelationshipHelper.getDefaultTarget(location, "depositAccounts", false, service);
    }

    /**
     * Returns the default till associated with a location.
     *
     * @param location the location
     * @return the default till or {@code null} if there is no default
     */
    public Entity getDefaultTill(Party location) {
        return EntityRelationshipHelper.getDefaultTarget(location, "tills", false, service);
    }

    /**
     * Returns the active tills associated with a location.
     *
     * @param location the location
     * @return the tills
     */
    public List<Entity> getTills(Party location) {
        IMObjectBean bean = service.getBean(location);
        return bean.getTargets("tills", Entity.class, Policies.active());
    }

    /**
     * Returns the default EFTPOS terminal associated with a till.
     *
     * @param till the till
     * @return the default terminal or {@code null} if there is no default
     */
    public Entity getDefaultTerminal(Entity till) {
        return EntityRelationshipHelper.getDefaultTarget(till, "terminals", false, service);
    }

    /**
     * Returns the active terminals associated with a till.
     *
     * @param till the till
     * @return the active terminals
     */
    public List<Entity> getTerminals(Entity till) {
        IMObjectBean bean = service.getBean(till);
        return bean.getTargets("terminals", Entity.class, Policies.active());
    }

    /**
     * Returns the default schedule view associated with a location.
     *
     * @param location the location
     * @return the default schedule or {@code null} if none is found
     */
    public Entity getDefaultScheduleView(Party location) {
        return EntityRelationshipHelper.getDefaultTarget(location, SCHEDULE_VIEWS, false, service);
    }

    /**
     * Returns the active <em>entity.organisationScheduleView</em>s associated with a location.
     *
     * @param location the location
     * @return the schedules views
     */
    public List<Entity> getScheduleViews(Party location) {
        IMObjectBean bean = service.getBean(location);
        return bean.getTargets(SCHEDULE_VIEWS, Entity.class, Policies.active());
    }

    /**
     * Determines if a location has a schedule view.
     *
     * @param location the practice location
     * @param view     the view reference
     * @return {@code true} if the location has the view, otherwise  {@code false}
     */
    public boolean hasScheduleView(Party location, Reference view) {
        return service.getBean(location).getTargetRefs(SCHEDULE_VIEWS).contains(view);
    }

    /**
     * Returns the default work list view associated with a location.
     *
     * @param location the location
     * @return the default work list view or {@code null} if none is found
     */
    public Entity getDefaultWorkListView(Party location) {
        return EntityRelationshipHelper.getDefaultTarget(location, WORK_LIST_VIEWS, false, service);
    }

    /**
     * Returns the <em>entity.organisationWorkListView</em>s associated with a location.
     *
     * @param location the location
     * @return the work list views
     */
    public List<Entity> getWorkListViews(Party location) {
        return service.getBean(location).getTargets(WORK_LIST_VIEWS, Entity.class, Policies.active());
    }

    /**
     * Determines if a location has a work list view.
     *
     * @param location the practice location
     * @param view     the view reference
     * @return {@code true} if the location has the view, otherwise  {@code false}
     */
    public boolean hasWorkListView(Party location, Reference view) {
        return service.getBean(location).getTargetRefs(WORK_LIST_VIEWS).contains(view);
    }

    /**
     * Returns the default stock location reference associated with a location.
     *
     * @param location the location
     * @return the default stock location reference, or {@code null} if none is found
     */
    public Reference getDefaultStockLocationRef(Party location) {
        return EntityRelationshipHelper.getDefaultTargetRef(location, "stockLocations", true, service);
    }

    /**
     * Returns the default stock location associated with a location.
     * <p>
     * NOTE: retrieval of stock locations may be an expensive operation,
     * due to the no. of relationships to products.
     *
     * @param location the location
     * @return the default location or {@code null} if none is found
     */
    public Party getDefaultStockLocation(Party location) {
        return (Party) EntityRelationshipHelper.getDefaultTarget(location, "stockLocations", true, service);
    }

    /**
     * Returns the pricing group for a practice location.
     *
     * @param location the practice location
     * @return the pricing group (an instance of <em>lookup.pricingGroup</em>), or {@code null} if none is found
     */
    public Lookup getPricingGroup(Party location) {
        IMObjectBean bean = service.getBean(location);
        List<Lookup> values = bean.getValues("pricingGroup", Lookup.class);
        return !values.isEmpty() ? values.get(0) : null;
    }

    /**
     * Returns the appointment reminder SMS template configured for the location.
     *
     * @param location the location
     * @return the template or {@code null} if none is configured
     */
    public Entity getAppointmentSMSTemplate(Party location) {
        return service.getBean(location).getTarget("smsAppointment", Entity.class);
    }

    /**
     * Returns the location mail server.
     *
     * @param location the practice location
     * @return the location server, or {@code null} if none is configured
     */
    public MailServer getMailServer(Party location) {
        Entity entity = service.getBean(location).getTarget("mailServer", Entity.class, Policies.active());
        return (entity != null) ? new MailServer(entity, service) : null;
    }

    /**
     * Returns the follow-up work lists associated with a location.
     *
     * @param location the practice location
     * @return the follow-up work lists, in the order they are defined in the location
     */
    public List<Entity> getFollowupWorkLists(Party location) {
        Policy<SequencedPeriodRelationship> policy = Policies.active(SequencedPeriodRelationship.class,
                                                                     SequenceComparator.INSTANCE);
        return service.getBean(location).getTargets("followupWorkLists", Entity.class, policy);
    }

    /**
     * Returns the default printer for a location.
     *
     * @param location the location
     * @return the default printer, or {@code null} if none is defined
     */
    public PrinterReference getDefaultPrinter(Party location) {
        IMObjectBean bean = service.getBean(location);
        return PrinterReference.fromString(bean.getString("defaultPrinter"));
    }

    /**
     * Returns the printers associated with a location.
     * <p>
     * If none are defined, it is assumed that all printers may be used
     *
     * @param location the location
     * @return the printer names
     */
    public List<PrinterReference> getPrinters(Party location) {
        List<PrinterReference> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(location);
        for (IMObject printer : bean.getTargets("printers", Policies.active())) {
            PrinterReference reference = PrinterReference.fromString(service.getBean(printer).getString("printer"));
            if (reference != null) {
                result.add(reference);
            }
        }
        return result;
    }

    /**
     * Returns the till used for gap claim benefit amount payments.
     *
     * @param location the practice location
     * @return the till, or {@code null} if none is defined
     */
    public Entity getGapBenefitTill(Party location) {
        return service.getBean(location).getTarget("gapBenefitTill", Entity.class);
    }

}
