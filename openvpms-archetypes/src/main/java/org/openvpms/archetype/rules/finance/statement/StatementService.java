/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.statement;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.util.BlockingExecutor;

import java.util.Date;

/**
 * Service for customer statements.
 *
 * @author Tim Anderson
 */
public class StatementService {

    /**
     * The archetype service.
     */
    private final IArchetypeRuleService service;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * BlockingExecutor used to single-thread end-of-period per customer.
     */
    private final BlockingExecutor<Long> endPeriodExecutor = new BlockingExecutor<>();

    /**
     * Constructs a {@link StatementService}.
     *
     * @param service         the archetype service
     * @param accountRules    the account rules
     * @param laboratoryRules the laboratory rules
     * @param practiceService the practice service
     */
    public StatementService(IArchetypeRuleService service, CustomerAccountRules accountRules,
                            LaboratoryRules laboratoryRules, PracticeService practiceService) {
        this.service = service;
        this.accountRules = accountRules;
        this.laboratoryRules = laboratoryRules;
        this.practiceService = practiceService;
    }

    /**
     * Runs end-of-period for a customer.
     *
     * @param customer             the customer
     * @param date                 the statement date
     * @param postCompletedCharges if {@code true}, POST charges with COMPLETED status
     */
    public void endPeriod(Party customer, Date date, boolean postCompletedCharges) {
        EndOfPeriodProcessor processor = createEndOfPeriodProcessor(date, postCompletedCharges);
        try {
            endPeriodExecutor.run(customer.getId(), () -> processor.process(customer));
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
            throw new StatementProcessorException(StatementProcessorException.ErrorCode.FailedToProcessStatement,
                                                  exception.getMessage());
        }
    }

    /**
     * Creates an end-of-period processor.
     *
     * @param date                 the statement date
     * @param postCompletedCharges if {@code true}, POST charges with COMPLETED status.
     * @return a new processor
     */
    private EndOfPeriodProcessor createEndOfPeriodProcessor(Date date, boolean postCompletedCharges) {
        Party practice = practiceService.getPractice();
        if (practice == null) {
            throw new IllegalStateException("No practice");
        }
        return new EndOfPeriodProcessor(date, postCompletedCharges, practice, service, accountRules, laboratoryRules);
    }
}