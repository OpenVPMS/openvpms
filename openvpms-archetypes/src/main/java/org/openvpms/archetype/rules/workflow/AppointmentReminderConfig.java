/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Appointment reminder configuration.
 *
 * @author Tim Anderson
 */
public class AppointmentReminderConfig {

    /**
     * Reply match status.
     */
    public enum Match {
        EXACT,     // the reply matches the expected text
        CONTAINS,  // the reply contains the expected text
        NO_MATCH;  // the reply doesn't contain the expected text

        /**
         * Determines if the reply is match.
         *
         * @return {@code true} if the reply is an exact match, or contains the expected text, otherwise {@code false}
         */
        public boolean isMatch() {
            return this != NO_MATCH;
        }
    }

    /**
     * The no-reminder period.
     */
    private final Period noReminderPeriod;

    /**
     * Determines if replies are being processed.
     */
    private final boolean processReplies;

    /**
     * The appointment confirmation text. May be {@code null}
     */
    private final String confirm;

    /**
     * The appointment cancellation text. May be {@code null}
     */
    private final String cancel;

    /**
     * Constructs a {@link AppointmentReminderConfig}.
     *
     * @param config  the configuration
     * @param service the archetype service
     */
    public AppointmentReminderConfig(Entity config, ArchetypeService service) {
        IMObjectBean bean = service.getBean(config);
        int period = bean.getInt("noReminder");
        DateUnits units = DateUnits.fromString(bean.getString("noReminderUnits"));
        noReminderPeriod = (period > 0 && units != null) ? units.toPeriod(period) : null;
        processReplies = bean.getBoolean("processReplies");
        confirm = StringUtils.trimToNull(bean.getString("confirmAppointment"));
        cancel = StringUtils.trimToNull(bean.getString("cancelAppointment"));
    }

    /**
     * Returns the period from the current time when no appointment reminder should be sent.
     *
     * @return the period. May be {@code null}
     */
    public Period getNoReminderPeriod() {
        return noReminderPeriod;
    }

    /**
     * Determines if replies are being processed.
     *
     * @return {@code true} if replies are being processed, otherwise {@code false}
     */
    public boolean processReplies() {
        return processReplies;
    }

    /**
     * Determines if a message contains appointment confirmation text.
     *
     * @param message the message
     * @return {@code true} if the message contains the appointment confirmation text, otherwise {@code false}
     */
    public boolean containsConfirmation(String message) {
        return StringUtils.containsIgnoreCase(message, confirm);
    }

    /**
     * Determines if a message contains appointment cancellation text.
     *
     * @param message the message
     * @return {@code true} if the message contains the appointment confirmation text, otherwise {@code false}
     */
    public boolean containsCancellation(String message) {
        return StringUtils.containsIgnoreCase(message, cancel);
    }

    /**
     * Determines if an inbound message matches the confirmation text, ignoring case.
     * <p/>
     * To be considered a match, the outbound message must also contain the confirmation text.
     *
     * @param inboundMessage  the inbound message
     * @param outboundMessage the outbound message
     * @return the match status
     */
    public Match isConfirmation(String inboundMessage, String outboundMessage) {
        return matches(inboundMessage, outboundMessage, confirm, cancel);
    }

    /**
     * Determines if an inbound message matches the cancellation text, ignoring case.
     * <p/>
     * To be considered a match, the outbound message must also contain the cancellation text.
     *
     * @param inboundMessage  the inbound message
     * @param outboundMessage the outbound message
     * @return the match status
     */
    public Match isCancellation(String inboundMessage, String outboundMessage) {
        return matches(inboundMessage, outboundMessage, cancel, confirm);
    }

    /**
     * Returns the appointment confirmation text.
     *
     * @return the confirmation text. May be {@code null}
     */
    public String getConfirmText() {
        return confirm;
    }

    /**
     * Returns the appointment cancellation text.
     *
     * @return the cancellation text. May be {@code null}
     */
    public String getCancelText() {
        return cancel;
    }

    /**
     * Determines if an inbound message matches the specified text, ignoring case.
     * <p/>
     * To be considered a match, the outbound message must also contain the text.
     *
     * @param inboundMessage  the inbound message
     * @param outboundMessage the outbound message
     * @param text            the text. May be {@code null}
     * @param notText         to be considered a match, the outboundMessage and inboundMessage cannot contain this.
     *                        May be {@code null}
     * @return {@code true} if the message matches the text, otherwise {@code false}
     */
    private Match matches(String inboundMessage, String outboundMessage, String text, String notText) {
        Match result = Match.NO_MATCH;
        if (StringUtils.containsIgnoreCase(outboundMessage, text)) {
            if (text.equalsIgnoreCase(inboundMessage)) {
                result = Match.EXACT;
            } else if (StringUtils.containsIgnoreCase(inboundMessage, text)) {
                if (notText == null) {
                    result = Match.CONTAINS;
                } else if (StringUtils.containsIgnoreCase(outboundMessage, notText)
                           && !StringUtils.containsIgnoreCase(inboundMessage, notText)) {
                    result = Match.CONTAINS;
                }
            }
        }
        return result;
    }
}