/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.component.business.service.archetype.helper.IMObjectCopier;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.openvpms.archetype.rules.stock.StockArchetypes.PRODUCT_STOCK_LOCATION_RELATIONSHIP;
import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_ORGANISATION;

/**
 * Copies products.
 *
 * @author Tim Anderson
 */
class ProductCopier extends IMObjectCopier {

    /**
     * Constructs a {@link ProductCopier}.
     *
     * @param product the product
     * @param service the archetype service
     */
    public ProductCopier(Product product, ArchetypeService service) {
        super(new ProductCopyHandler(product), service);
    }

    /**
     * Handle a missing reference.
     * <p/>
     * This implementation ignores missing suppliers on product - stock location relationships. Normally,
     * a supplier can't be deleted if other objects reference it, but the supplier reference in
     * <em>entityLink.productStockLocation</em> is managed as a details node (i.e. serialised to a string).
     *
     * @param reference the reference
     * @param parent    the parent
     */
    @Override
    protected void missingReference(Reference reference, IMObject parent) {
        if (!(PRODUCT_STOCK_LOCATION_RELATIONSHIP.equals(parent.getArchetype())
              && SUPPLIER_ORGANISATION.equals(reference.getArchetype()))) {
            super.missingReference(reference, parent);
        }
    }
}