/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.ObjectSet;

import java.util.Date;
import java.util.Map;


/**
 * Queries <em>act.customerAppointment</em> and <em>act.calendarBlock</em> acts associated with a schedule, returning a
 * limited set of data for display purposes.
 * <p/>
 * By default, the query returns all events intersecting the date range from..to using:<br/>
 * {@code (event.startTime < from and event.endTime > from) <br/>
 * or (event.startTime < to and event.endTime > to) <br/>
 * or (event.startTime >= from and event.endTime <= to) <br/>}
 * <p/>
 * If the schedule has a <em>maxDuration</em> configured, this will further restrict the query by ensuring the event
 * start and end times are between:
 * <ul>
 *     <li>lowerbound = from - maxDuration; and</li>
 *     <li>upperbound = to + maxDuration</li>
 * </ul>
 * i.e.:<br/>
 * {@code event.startTime >= lowerbound and event.endTime < upperbound and<br/>
 * event.endTime > lowerbound and event.endTime <= upperbound and
 * ((event.startTime < from and event.endTime > from) <br/>
 * or (event.startTime < to and event.endTime > to) <br/>
 *  (event.startTime >= from and event.endTime <= to)) <br/>}
 * <p/>
 * This limits index range scans which would otherwise become increasingly expensive as the database grows.<br/>
 * It requires <em>maxDuration</em> to be configured correctly to be as long as the longest event. If an event spans
 * multiple days, and the <em>maxDuration</em> is shorter, it may not be returned.
 * <p/>
 * Note that if <em>maxDuration</em> is changed on a schedule, changes won't be reflected in the
 * {@link AppointmentService} until caches are shed and reloaded.
 *
 * @author Tim Anderson
 * @see AppointmentService
 * @see AppointmentRules#getAppointmentEventLongerThanDuration(Entity, Date, int, DateUnits)
 */
class AppointmentQuery extends ScheduleEventQuery {

    /**
     * The lower bound of the query, to limit index range scans. May be {@code null}
     */
    private final Date lowerbound;

    /**
     * The upper bound of the query, to limit index range scans. May be {@code null}
     */
    private final Date upperbound;

    /**
     * Constructs an {@link AppointmentQuery}.
     *
     * @param schedule    the schedule
     * @param from        the 'from' start time
     * @param to          the 'to' start time
     * @param statusNames the status names, keyed on status code
     * @param reasonNames the reason names, keyed on reason code
     * @param service     the archetype service
     */
    public AppointmentQuery(Entity schedule, Date from, Date to, Map<String, String> statusNames,
                            Map<String, String> reasonNames, IArchetypeService service) {
        super(schedule, from, to, statusNames, reasonNames, service);
        IMObjectBean bean = service.getBean(schedule);
        int maxDuration = bean.getInt("maxDuration", -1);
        DateUnits units = DateUnits.fromString(bean.getString("maxDurationUnits"), null);
        if (maxDuration > 0 && units != null) {
            // restrict the query, to limit index range scans
            lowerbound = DateRules.getDate(from, -maxDuration, units);
            upperbound = DateRules.getDate(to, maxDuration, units);
        } else {
            lowerbound = null;
            upperbound = null;
        }
    }

    /**
     * Returns the name of the named query to execute.
     *
     * @return the name of the named query
     */
    protected String getQueryName() {
        return (lowerbound != null) ? "getAppointmentEventsBounded" : "getAppointmentEvents";
    }

    /**
     * Creates a new query.
     *
     * @param schedule the schedule
     * @param from     the from date
     * @param to       the to date
     * @return the query
     */
    @Override
    protected IArchetypeQuery createQuery(Entity schedule, Date from, Date to) {
        NamedQuery query = new NamedQuery(getQueryName(), NAMES);
        query.setParameter("scheduleId", schedule.getId());
        query.setParameter("from", from);
        query.setParameter("to", to);
        if (lowerbound != null) {
            // restrict the query, to limit index range scans
            query.setParameter("lowerbound", lowerbound);
            query.setParameter("upperbound", upperbound);
        }
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        return query;
    }

    /**
     * Returns the archetype of the schedule type.
     *
     * @param eventArchetype the event archetype
     * @return the archetype of the schedule type
     */
    protected String getScheduleType(String eventArchetype) {
        return ScheduleArchetypes.APPOINTMENT.equals(eventArchetype) ? ScheduleArchetypes.APPOINTMENT_TYPE
                                                                     : ScheduleArchetypes.CALENDAR_BLOCK_TYPE;
    }

    /**
     * Creates a new {@link ObjectSet ObjectSet} representing a scheduled event.
     *
     * @param actRef the reference of the event act
     * @param set    the source set
     * @return a new event
     */
    @Override
    protected ObjectSet createEvent(Reference actRef, ObjectSet set) {
        ObjectSet event = super.createEvent(actRef, set);
        if (actRef.isA(ScheduleArchetypes.APPOINTMENT)) {
            event.set(ScheduleEvent.CONFIRMED_TIME, null);
            event.set(ScheduleEvent.ARRIVAL_TIME, null);
            event.set(ScheduleEvent.SEND_REMINDER, null);
            event.set(ScheduleEvent.REMINDER_SENT, null);
            event.set(ScheduleEvent.REMINDER_ERROR, null);
            event.set(ScheduleEvent.SMS_STATUS, null);
        }
        return event;
    }
}
