/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

import org.openvpms.component.model.object.Reference;

/**
 * Global settings.
 * <p/>
 * These are backed by archetypes named <em>entity.globalSettings*</em>
 *
 * @author Tim Anderson
 */
public interface Settings {

    /**
     * Returns a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    Object get(String groupName, String name, Object defaultValue);

    /**
     * Returns the boolean value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    boolean getBoolean(String groupName, String name, boolean defaultValue);

    /**
     * Returns the integer value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    int getInt(String groupName, String name, int defaultValue);

    /**
     * Returns the long value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    long getLong(String groupName, String name, long defaultValue);

    /**
     * Returns the string value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    String getString(String groupName, String name, String defaultValue);

    /**
     * Returns the reference value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    Reference getReference(String groupName, String name, Reference defaultValue);

}