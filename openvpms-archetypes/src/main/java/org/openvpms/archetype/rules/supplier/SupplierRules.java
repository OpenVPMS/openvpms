/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;


/**
 * Supplier rules.
 *
 * @author Tim Anderson
 */
public class SupplierRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Scalar names returned by the getProductOnOrder named query.
     */
    private static final List<String> PRODUCT_ON_ORDER = Arrays.asList(
            "orderId", "date", "supplierId", "supplierArchetype", "supplierName", "quantity", "packageSize",
            "packageUnits");


    /**
     * Constructs a {@code SupplierRules}.
     *
     * @param service the archetype service
     */
    public SupplierRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the referral vet practice for a vet overlapping the specified time.
     *
     * @param vet  the vet
     * @param time the time
     * @return the practice the vet is associated with or {@code null} if the vet is not associated with any practice
     * for the time frame
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Party getReferralVetPractice(Party vet, Date time) {
        return getReferralVetPractice(vet, time, true);
    }

    /**
     * Returns the referral vet practice for a vet overlapping the specified time.
     *
     * @param vet    the vet
     * @param time   the time
     * @param active if {@code true}, returns active practices, otherwise returns active or inactive practices
     * @return the practice the vet is associated with or {@code null} if the vet is not associated with any practice
     * for the time frame
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Party getReferralVetPractice(Party vet, Date time, boolean active) {
        IMObjectBean bean = service.getBean(vet);
        return (Party) bean.getSource("practices", Policies.active(time, active));
    }

    /**
     * Determines if a supplier supplies a particular product.
     *
     * @param supplier the supplier
     * @param product  the product
     * @return {@code true} if {@code supplier} supplies {@code product}; otherwise {@code false}
     */
    public boolean isSuppliedBy(Party supplier, Product product) {
        IMObjectBean bean = service.getBean(product);
        Predicate<Relationship> predicate = Predicates.activeNow().and(Predicates.targetEquals(supplier));
        return bean.getValue("suppliers", Relationship.class, predicate) != null;
    }

    /**
     * Returns all active <em>entityLink.productSupplier</em> relationships for a particular supplier.
     *
     * @param supplier the supplier
     * @return the relationships, wrapped in {@link ProductSupplier} instances
     */
    public List<ProductSupplier> getProductSuppliers(Party supplier) {
        List<ProductSupplier> result = new ArrayList<>();
        ArchetypeQuery query = new ArchetypeQuery(ProductArchetypes.PRODUCT_SUPPLIER_RELATIONSHIP);
        query.add(Constraints.eq("target", supplier));
        Date now = new Date();
        query.add(Constraints.or(Constraints.lte("activeStartTime", now)));
        query.add(Constraints.lte("activeStartTime", now));
        query.add(Constraints.or(Constraints.gte("activeEndTime", now), Constraints.isNull("activeEndTime")));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IMObjectQueryIterator<IMObject> iterator = new IMObjectQueryIterator<>(service, query);
        while (iterator.hasNext()) {
            result.add(new ProductSupplier((Relationship) iterator.next(), service));
        }
        return result;
    }

    /**
     * Returns the <em>entityRelationship.supplierStockLocation*</em> between the supplier and stock location
     * associated with an order.
     *
     * @param order an <em>act.supplierOrder</em>
     * @return the supplier's <em>entityRelationship.supplierStockLocation*</em> or {@code null} if none is found
     */
    public EntityRelationship getSupplierStockLocation(Act order) {
        IMObjectBean bean = service.getBean(order);
        Party supplier = bean.getTarget("supplier", Party.class);
        Party stockLocation = bean.getTarget("stockLocation", Party.class);
        return (supplier != null && stockLocation != null) ? getSupplierStockLocation(supplier, stockLocation) : null;
    }

    /**
     * Returns the <em>entityRelationship.supplierStockLocation*</em> between a supplier and stock location.
     *
     * @param supplier      a <em>party.supplier*</em>
     * @param stockLocation a <em>party.organisationStockLocation</em>
     * @return the supplier's <em>entityRelationship.supplierStockLocation*</em> or {@code null} if none is found
     */
    public EntityRelationship getSupplierStockLocation(Party supplier, Party stockLocation) {
        IMObjectBean bean = service.getBean(supplier);
        List<EntityRelationship> relationships = bean.getValues(
                "stockLocations", EntityRelationship.class,
                Predicates.<EntityRelationship>activeNow().and(Predicates.targetEquals(stockLocation)));
        return (!relationships.isEmpty()) ? relationships.get(0) : null;
    }

    /**
     * Returns the account id for a supplier - practice location relationship.
     *
     * @param supplierId the supplier identifier
     * @param location   the practice location
     * @return the account id, or {@code null} if no relationship exists
     */
    public String getAccountId(long supplierId, Party location) {
        String accountId = null;
        ArchetypeQuery query = new ArchetypeQuery("party.supplier*", false);
        query.add(Constraints.eq("id", supplierId));
        IMObjectQueryIterator<Party> iterator = new IMObjectQueryIterator<>(service, query);
        if (iterator.hasNext()) {
            Party supplier = iterator.next();
            accountId = getAccountId(supplier, location);
        }
        return accountId;
    }

    /**
     * Returns the account id for a supplier - practice location relationship.
     *
     * @param supplier the supplier
     * @param location the practice location
     * @return the account id, or {@code null} if no relationship exists
     */
    public String getAccountId(Party supplier, Party location) {
        String accountId = null;
        EntityLink relationship = getSupplierLocation(supplier, location);
        if (relationship != null) {
            IMObjectBean bean = service.getBean(relationship);
            accountId = bean.getString("accountId");
        }
        return accountId;
    }

    /**
     * Returns the active relationship between a supplier and practice location.
     *
     * @param supplier the supplier
     * @param location the practice location
     * @return the relationship, or {@code null} if none is found
     */
    public EntityLink getSupplierLocation(Party supplier, Party location) {
        IMObjectBean bean = service.getBean(supplier);
        return bean.getValue("locations", EntityLink.class,
                             Predicates.<EntityLink>activeNow().and(Predicates.targetEquals(location)));
    }

    /**
     * Returns the order details of a product on order.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @return the order details
     */
    public List<ProductOrder> getOrders(Product product, Party stockLocation) {
        return getOrders(product.getObjectReference(), stockLocation.getObjectReference());
    }

    /**
     * Returns the order details of a product that has been ordered.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @return the order details
     */
    public List<ProductOrder> getOrders(Reference product, Reference stockLocation) {
        List<ProductOrder> result = new ArrayList<>();
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("productId", product.getId());
        parameters.put("stockLocationId", stockLocation.getId());
        NamedQuery query = new NamedQuery("getProductOnOrder", PRODUCT_ON_ORDER, parameters);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        while (iterator.hasNext()) {
            ObjectSet set = iterator.next();
            long orderId = set.getLong("orderId");
            long supplierId = set.getLong("supplierId");
            String supplierArchetype = set.getString("supplierArchetype");
            String supplierName = set.getString("supplierName");
            Date date = set.getDate("date");
            BigDecimal quantity = set.getBigDecimal("quantity");
            Integer packageSize = (Integer) set.get("packageSize");
            String packageUnits = set.getString("packageUnits");
            result.add(new ProductOrder(new IMObjectReference(SupplierArchetypes.ORDER, orderId),
                                        date, new IMObjectReference(supplierArchetype, supplierId),
                                        supplierName, quantity,
                                        packageSize, packageUnits));
        }
        return result;
    }
}
