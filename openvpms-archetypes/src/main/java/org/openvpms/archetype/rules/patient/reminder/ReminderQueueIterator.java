/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient.reminder;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.UpdatableQueryIterator;

import java.util.Date;

/**
 * Iterates over reminders returned by {@link ReminderQueueQueryFactory}.
 *
 * @author Tim Anderson
 */
public class ReminderQueueIterator extends UpdatableQueryIterator<Act> {

    /**
     * Constructs a {@link ReminderQueueIterator}.
     * <p>
     * This returns all {@code IN_PROGRESS} reminders with a {@code startTime} less than the specified date,
     * and have no items that have {@code PENDING} or {@code ERROR} status.
     *
     * @param date     the date
     * @param pageSize the page size
     * @param service  the archetype service
     */
    public ReminderQueueIterator(Date date, int pageSize, IArchetypeService service) {
        this(new ReminderQueueQueryFactory(), date, pageSize, service);
    }

    /**
     * Constructs a {@link ReminderQueueIterator}.
     * <p>
     * This returns all {@code IN_PROGRESS} reminders with a {@code startTime} less than the specified date,
     * and have no items that have {@code PENDING} or {@code ERROR} status.
     *
     * @param factory  the query factory
     * @param date     the date
     * @param pageSize the page size
     * @param service  the archetype service
     */
    protected ReminderQueueIterator(ReminderQueueQueryFactory factory, Date date, int pageSize,
                                    IArchetypeService service) {
        super(factory.createQuery(date), pageSize, service);
    }

    /**
     * Returns the next page.
     *
     * @param query   the query
     * @param service the archetype service
     * @return the next page
     */
    @Override
    @SuppressWarnings("unchecked")
    protected IPage<Act> getNext(ArchetypeQuery query, IArchetypeService service) {
        return (IPage) service.get(query);
    }

    /**
     * Returns a unique identifier for the object.
     *
     * @param object the object
     * @return a unique identifier for the object
     */
    @Override
    protected long getId(Act object) {
        return object.getId();
    }
}
