/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.object.Relationship;

import java.util.ArrayList;
import java.util.List;


/**
 * Invoice rules.
 *
 * @author Tim Anderson
 */
public class InvoiceRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Statuses of <em>act.patientReminder</em> acts that should be retained.
     */
    private static final String[] REMINDER_STATUSES = {ActStatus.COMPLETED};

    /**
     * Statuses of <em>act.patientAlerts</em> acts that should be retained.
     */
    private static final String[] ALERT_STATUSES = {ActStatus.COMPLETED};

    /**
     * Statuses of <em>act.patientDocument*</em> acts that should be retained.
     */
    private static final String[] DOCUMENT_STATUSES = {ActStatus.COMPLETED, ActStatus.POSTED};

    /**
     * Statuses of <em>act.customerTask*</em> acts that should be retained.
     */
    private static final String[] TASK_STATUSES = {TaskStatus.BILLED, TaskStatus.COMPLETED};


    /**
     * Constructs a {@link InvoiceRules}.
     *
     * @param service the archetype service
     */
    public InvoiceRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Invoked after an invoice item has been saved. Updates any reminders, alerts and documents associated with the
     * product.
     *
     * @param act the act
     */
    public void saveInvoiceItem(FinancialAct act) {
        InvoiceItemSaveRules rules = new InvoiceItemSaveRules(act, service);
        rules.save();
    }

    /**
     * Invoked <em>after</em> an invoice is saved. Processes any demographic
     * updates if the invoice is 'Posted'.
     *
     * @param invoice the invoice
     */
    public void saveInvoice(FinancialAct invoice) {
        if (!invoice.isA(CustomerAccountArchetypes.INVOICE)) {
            throw new IllegalArgumentException("Invalid argument 'invoice'");
        }
        if (ActStatus.POSTED.equals(invoice.getStatus())) {
            IMObjectBean bean = service.getBean(invoice);
            List<Act> acts = bean.getTargets("items", Act.class);
            for (Act act : acts) {
                DemographicUpdateHelper helper = new DemographicUpdateHelper(act, service);
                helper.processDemographicUpdates(invoice);
            }
        }
    }

    /**
     * Invoked <em>prior</em> to an invoice being removed. Removes any reminders
     * or documents that don't have status <em>COMPLETED</em>.
     *
     * @param invoice the invoice
     */
    public void removeInvoice(FinancialAct invoice) {
        if (!invoice.isA(CustomerAccountArchetypes.INVOICE)) {
            throw new IllegalArgumentException("Invalid argument 'invoice'");
        }
        IMObjectBean bean = service.getBean(invoice);
        List<Act> acts = bean.getTargets("items", Act.class);
        for (Act act : acts) {
            removeInvoiceItem((FinancialAct) act);
        }
    }

    /**
     * Invoked prior to an invoice item has been removed. Removes any reminders
     * or documents that don't have status <em>COMPLETED</em>.
     *
     * @param act the act
     */
    public void removeInvoiceItem(FinancialAct act) {
        if (!act.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            throw new IllegalArgumentException("Invalid argument 'act'");
        }
        List<Act> toRemove = new ArrayList<>();
        removeInvestigations(act, toRemove);
        removeRelatedActs(act, "reminders", REMINDER_STATUSES, toRemove);
        removeRelatedActs(act, "alerts", ALERT_STATUSES, toRemove);
        removeRelatedActs(act, "documents", DOCUMENT_STATUSES, toRemove);
        removeRelatedActs(act, "tasks", TASK_STATUSES, toRemove);

        if (!toRemove.isEmpty()) {
            // Need to save to update session prior to removing child acts
            service.save(act);
            service.save(toRemove);
            for (Act remove : toRemove) {
                service.remove(remove);
            }
        }
    }

    /**
     * Removes relationships between an invoice item and related investigations, iff the investigation is IN_PROGRESS,
     * and doesn't have any associated results.
     *
     * @param item     the invoice item
     * @param toRemove the acts to remove
     */
    private void removeInvestigations(FinancialAct item, List<Act> toRemove) {
        IMObjectBean bean = service.getBean(item);
        RelatedIMObjects<DocumentAct, Relationship> investigations
                = bean.getRelated("investigations", DocumentAct.class);
        for (ObjectRelationship<DocumentAct, Relationship> investigation : investigations.getObjectRelationships()) {
            DocumentAct act = investigation.getObject();
            String status = act.getStatus();
            if (ActStatus.IN_PROGRESS.equals(status) && act.getDocument() == null) {
                toRemove.add(act);
                ActRelationship relationship = (ActRelationship) investigation.getRelationship();
                act.removeActRelationship(relationship);
                item.removeActRelationship(relationship);
            }
        }
    }

    /**
     * Removes relationships between an invoice item and related acts that meet the specified criteria.
     * The acts that match the criteria are added to <tt>toRemove</tt>.
     *
     * @param item     the invoice item
     * @param node     the node of the related acts
     * @param statuses act statuses. If a related act has one of these, it will be retained
     * @param toRemove the acts to remove
     */
    private void removeRelatedActs(FinancialAct item, String node, String[] statuses, List<Act> toRemove) {
        IMObjectBean bean = service.getBean(item);
        RelatedIMObjects<Act, ActRelationship> relatedItems = bean.getRelated(node, Act.class, ActRelationship.class);

        for (ObjectRelationship<Act, ActRelationship> related : relatedItems.getObjectRelationships()) {
            Act act = related.getObject();
            String status = act.getStatus();
            ActRelationship relationship = related.getRelationship();

            boolean retain = false;
            for (String s : statuses) {
                if (s.equals(status)) {
                    retain = true;
                    break;
                }
            }
            if (!retain) {
                toRemove.add(act);
                act.removeActRelationship(relationship);
                item.removeActRelationship(relationship);
            }
        }
    }

}
