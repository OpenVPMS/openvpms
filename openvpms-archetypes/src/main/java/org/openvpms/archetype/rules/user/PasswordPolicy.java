/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

/**
 * Policy for user passwords.
 *
 * @author Tim Anderson
 */
public interface PasswordPolicy {

    /**
     * Returns the minimum password length.
     *
     * @return the minimum password length
     */
    int getMinLength();

    /**
     * Returns the maximum password length.
     *
     * @return the maximum password length
     */
    int getMaxLength();

    /**
     * Determines if at least one lowercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    boolean lowercaseRequired();

    /**
     * Determines if at least one uppercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    boolean uppercaseRequired();

    /**
     * Determines if at least one number is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    boolean numberRequired();

    /**
     * Determines if at least one special character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    boolean specialRequired();

    /**
     * Returns the special characters that may appear in passwords.
     *
     * @return the special characters
     */
    String getSpecialCharacters();

    /**
     * Returns the special characters that may appear in passwords, escaped for use in regular expressions.
     *
     * @return the supported special characters, with ^, $ etc. escaped with \
     */
    String getSpecialCharactersEscaped();

    /**
     * Returns all valid special characters that may appear in passwords, escaped for use in regular expressions.
     *
     * @return the supported characters, with ^, $ etc. escaped with \
     */
    String getValidCharactersEscaped();

}
