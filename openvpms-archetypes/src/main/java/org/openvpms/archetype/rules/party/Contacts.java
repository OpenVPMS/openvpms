/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.party;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Contact helpers.
 *
 * @author Tim Anderson
 */
public class Contacts {

    /**
     * The email address node.
     */
    public static final String EMAIL_ADDRESS = "emailAddress";

    /**
     * The telephone number node.
     */
    public static final String TELEPHONE_NUMBER = "telephoneNumber";

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link Contacts}.
     *
     * @param service the archetype service
     */
    public Contacts(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns fully populated email contacts for a party.
     *
     * @param party the party
     * @return the email contacts
     */
    public List<Contact> getEmailContacts(Party party) {
        return getContacts(party, new EmailPredicate());
    }

    /**
     * Returns the email contact for a party.
     *
     * @param party the party
     * @return the party's preferred email contact or the email contact with the lowest id if none is preferred,
     * or {@code null} if the party has no email contact
     */
    public Contact getEmailContact(Party party) {
        List<Contact> list = sortPreferred(getEmailContacts(party));
        return (!list.isEmpty()) ? list.get(0) : null;
    }

    /**
     * Returns the email address for a party.
     *
     * @param party the party
     * @return the party's preferred email or the email  with the lowest id if none is preferred,
     * or {@code null} if the party has no email contact
     */
    public String getEmail(Party party) {
        String result = null;
        Contact contact = getEmailContact(party);
        if (contact != null) {
            IMObjectBean bean = service.getBean(contact);
            result = StringUtils.trimToNull(bean.getString("emailAddress"));
        }
        return result;
    }

    /**
     * Returns fully populated SMS contacts for a party.
     *
     * @param party the party
     * @return the SMS contacts
     */
    public List<Contact> getSMSContacts(Party party) {
        return getContacts(party, new SMSPredicate());
    }

    /**
     * Determines if a party can receive SMS messages.
     *
     * @param party the party
     * @return {@code true} if the party can receive SMS messages
     */
    public boolean canSMS(Party party) {
        return IterableUtils.find(party.getContacts(), new SMSPredicate()) != null;
    }

    /**
     * Returns the phone number from a contact, extracting any formatting.
     *
     * @param contact the phone contact
     * @return the phone number. May be {@code null}
     */
    public String getPhone(Contact contact) {
        IMObjectBean bean = service.getBean(contact);
        String areaCode = bean.getString("areaCode");
        String phone = bean.getString(TELEPHONE_NUMBER);
        return getPhone(areaCode, phone);
    }

    /**
     * Sorts contacts on increasing identifier.
     * <p>
     * Note that this operation modifies the supplied list.
     *
     * @param contacts the contacts to sort
     * @return the sorted contact
     */
    public static <T extends Contact> List<T> sort(List<T> contacts) {
        if (contacts.size() > 1) {
            contacts.sort(Comparator.comparingLong(IMObject::getId));
        }
        return contacts;
    }

    /**
     * Sorts contacts on increasing identifier.
     *
     * @param contacts the contacts to sort
     * @return the sorted contacts
     */
    public static <T extends Contact> List<T> sort(Collection<T> contacts) {
        return sort(new ArrayList<>(contacts));
    }

    /**
     * Looks for a contact that matches the criteria.
     * <p>
     * For consistent results over multiple calls, sort the contacts first.
     *
     * @param contacts the contacts
     * @param matcher  the contact matcher
     * @return the matching contact or {@code null} if none is found
     */
    public static Contact find(Collection<Contact> contacts, ContactMatcher matcher) {
        for (Contact contact : contacts) {
            if (matcher.matches(contact)) {
                break;
            }
        }
        return matcher.getMatch();
    }

    /**
     * Returns a phone number, extracting any formatting.
     * <p/>
     * This ignores any area code if no phone number is present.
     *
     * @param areaCode    the area code. May be {@code null}
     * @param phoneNumber the phone number. May be {@code null}
     * @return the unformatted phone number. May be {@code null}
     */
    public static String getPhone(String areaCode, String phoneNumber) {
        String result = null;
        areaCode = StringUtils.trimToNull(areaCode);
        phoneNumber = StringUtils.trimToNull(phoneNumber);

        if (areaCode != null && phoneNumber != null) {
            result = areaCode;
            result += phoneNumber;
        } else if (phoneNumber != null) {
            result = phoneNumber;
        }
        return getPhone(result);
    }

    /**
     * Returns the phone number from a string, extracting any formatting.
     *
     * @param phone the formatted phone number
     * @return the phone number. May be {@code null}
     */
    public static String getPhone(String phone) {
        String result = phone;
        if (!StringUtils.isEmpty(result)) {
            // strip any spaces, hyphens, and brackets, and any characters after the last digit.
            result = result.replaceAll("[\\s\\-()]", "").replaceAll("[^\\d+].*", "");
        }
        return result;
    }

    /**
     * Returns contacts matching a predicate.
     *
     * @param party     the party
     * @param predicate the predicate
     * @return contacts matching the predicate
     */
    public static List<Contact> getContacts(Party party, Predicate<Contact> predicate) {
        List<Contact> result = new ArrayList<>();
        CollectionUtils.select(party.getContacts(), predicate, result);
        return result;
    }

    /**
     * Sorts contacts, ordering on preferred contact(s) first.
     * Contacts have a secondary sort on increasing id.
     *
     * @param list the contacts
     * @return the sorted contacts
     */
    private List<Contact> sortPreferred(List<Contact> list) {
        if (list.size() > 1) {
            list.sort((o1, o2) -> {
                boolean preferred1 = isPreferred(o1);
                boolean preferred2 = isPreferred(o1);
                int result;
                if (preferred1 == preferred2) {
                    result = Long.compare(o1.getId(), o2.getId());
                } else if (preferred1) {
                    result = -1;
                } else {
                    result = 1;
                }
                return result;
            });
        }
        return list;
    }

    /**
     * Determines if a contact is preferred.
     *
     * @param contact the contact
     * @return {@code true} if the contact is preferred, otherwise {@code false}
     */
    private boolean isPreferred(Contact contact) {
        IMObjectBean bean = service.getBean(contact);
        return bean.hasNode("preferred") && bean.getBoolean("preferred");
    }

    private class SMSPredicate implements Predicate<Contact> {

        /**
         * Use the specified parameter to perform a test that returns true or false.
         *
         * @param contact the object to evaluate, should not be changed
         * @return true or false
         */
        public boolean evaluate(Contact contact) {
            boolean result = false;
            if (TypeHelper.isA(contact, ContactArchetypes.PHONE)) {
                IMObjectBean bean = service.getBean(contact);
                if (bean.getBoolean("sms")) {
                    String phone = bean.getString(TELEPHONE_NUMBER);
                    if (!StringUtils.isEmpty(phone)) {
                        result = true;
                    }
                }
            }
            return result;
        }
    }

    private class EmailPredicate implements Predicate<Contact> {

        /**
         * Use the specified parameter to perform a test that returns true or false.
         *
         * @param contact the object to evaluate, should not be changed
         * @return true or false
         */
        public boolean evaluate(Contact contact) {
            boolean result = false;
            if (TypeHelper.isA(contact, ContactArchetypes.EMAIL)) {
                IMObjectBean bean = service.getBean(contact);
                if (!StringUtils.isEmpty(bean.getString(EMAIL_ADDRESS))) {
                    result = true;
                }
            }
            return result;
        }
    }

}
