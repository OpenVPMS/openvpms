/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;

/**
 * Default implementation of {@link Settings}.
 *
 * @author Tim Anderson
 */
public class SettingsImpl implements Settings {

    /**
     * The settings cache.
     */
    private final SettingsCache cache;

    /**
     * Constructs a {@link SettingsImpl}.
     *
     * @param cache the settings cache
     */
    public SettingsImpl(SettingsCache cache) {
        this.cache = cache;
    }

    /**
     * Returns a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public Object get(String groupName, String name, Object defaultValue) {
        Object value = getGroup(groupName).getObject(name);
        return value != null ? value : defaultValue;
    }

    /**
     * Returns the boolean value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public boolean getBoolean(String groupName, String name, boolean defaultValue) {
        return getGroup(groupName).getBoolean(name, defaultValue);
    }

    /**
     * Returns the integer value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public int getInt(String groupName, String name, int defaultValue) {
        return getGroup(groupName).getInt(name, defaultValue);
    }

    /**
     * Returns the long value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public long getLong(String groupName, String name, long defaultValue) {
        return getGroup(groupName).getLong(name, defaultValue);
    }

    /**
     * Returns the string value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public String getString(String groupName, String name, String defaultValue) {
        return getGroup(groupName).getString(name, defaultValue);
    }

    /**
     * Returns the reference value of a setting.
     *
     * @param groupName    the setting group name
     * @param name         the setting name
     * @param defaultValue the default value, if the setting is unset. May be {@code null}
     * @return the setting, or {@code defaultValue} if no setting is present
     */
    @Override
    public Reference getReference(String groupName, String name, Reference defaultValue) {
        Reference result = getGroup(groupName).getReference(name);
        return (result != null) ? result : defaultValue;
    }

    /**
     * Returns a settings group, given its name.
     * <p>
     * If the group doesn't exist, it will be created.
     *
     * @param name the group name
     * @return the group
     * @throws IllegalArgumentException if the name doesn't correspond to a valid group
     */
    private IMObjectBean getGroup(String name) {
        return cache.getGroup(name);
    }
}