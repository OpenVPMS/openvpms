/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.archetype.rules.util.MappingCopyHandler;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.product.ProductPrice;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopyHandler;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.product.Product;

/**
 * An {@link IMObjectCopyHandler} for copying {@link ProductPrice}s. This
 * ensures that any classifications referenced rather than copied, and excludes any notes node.
 *
 * @author Tim Anderson
 */
class PriceCopyHandler extends MappingCopyHandler {

    /**
     * Creates a {@link PriceCopyHandler}.
     */
    public PriceCopyHandler() {
        setReference(Lookup.class);
        setExclude(Product.class);
    }

    /**
     * Helper to determine if a node is copyable.
     * </p>
     * This implementation excludes <em>id</em> nodes, and derived nodes
     * where the node is the target.
     *
     * @param archetype the archetype descriptor
     * @param node      the node descriptor
     * @param source    if {@code true} the node is the source; otherwise its
     *                  the target
     * @return {@code true} if the node is copyable; otherwise {@code false}
     */
    @Override
    protected boolean isCopyable(ArchetypeDescriptor archetype, NodeDescriptor node, boolean source) {
        boolean result = super.isCopyable(archetype, node, source);
        if (result) {
            result = !"notes".equals(node.getName());
        }
        return result;
    }
}
