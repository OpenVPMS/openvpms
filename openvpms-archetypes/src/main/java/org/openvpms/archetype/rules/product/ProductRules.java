/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.JoinConstraint;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static org.openvpms.archetype.rules.math.MathRules.isZero;
import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.gte;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.lt;
import static org.openvpms.component.system.common.query.Constraints.sort;

/**
 * Product rules.
 *
 * @author Tim Anderson
 */
public class ProductRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * Exact dose match.
     */
    private static final int EXACT_DOSE_MATCH = 1;

    /**
     * Partial dose match.
     */
    private static final int PARTIAL_DOSE_MATCH = 2;

    /**
     * Suppliers node name.
     */
    private static final String SUPPLIERS = "suppliers";

    /**
     * Product node name.
     */
    private static final String PRODUCT = "product";

    /**
     * Target node name.
     */
    private static final String TARGET = "target";

    /**
     * Active end time node name.
     */
    private static final String ACTIVE_END_TIME = "activeEndTime";

    /**
     * Constructs a {@link ProductRules}.
     *
     * @param service the archetype service
     * @param lookups the lookup service
     */
    public ProductRules(IArchetypeService service, LookupService lookups) {
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Copies a product.
     *
     * @param product the product to copy
     * @return a copy of {@code product}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Product copy(Product product) {
        return copy(product, product.getName());
    }

    /**
     * Copies a product.
     *
     * @param product the product to copy
     * @param name    the new product name
     * @return a copy of {@code product}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Product copy(Product product, String name) {
        ProductCopier copier = new ProductCopier(product, service);
        List<IMObject> objects = copier.apply(product);
        Product copy = (Product) objects.get(0);
        copy.setName(name);
        service.save(objects);
        return copy;
    }

    /**
     * Returns the dose of a product for the specified patient weight and species.
     *
     * @param product the product
     * @param weight  the patient weight
     * @param species the patient species code. May be {@code null}
     * @return the dose or {@code 0} if there is no dose for the patient weight and species
     */
    public BigDecimal getDose(Product product, Weight weight, String species) {
        BigDecimal result = BigDecimal.ZERO;
        IMObjectBean bean = service.getBean(product);
        BigDecimal concentration = getConcentration(bean);
        if (!isZero(concentration)) {
            IMObjectBean match = null;
            WeightUnits matchUnits = null;
            List<IMObject> doses = bean.getTargets("doses", IMObject.class);
            for (IMObject dose : doses) {
                IMObjectBean doseBean = service.getBean(dose);
                BigDecimal minWeight = doseBean.getBigDecimal("minWeight", BigDecimal.ZERO);
                BigDecimal maxWeight = doseBean.getBigDecimal("maxWeight", BigDecimal.ZERO);
                WeightUnits units = WeightUnits.fromString(doseBean.getString("weightUnits"), WeightUnits.KILOGRAMS);
                if (weight.between(minWeight, maxWeight, units)) {
                    int doseMatch = isDoseSpeciesMatch(doseBean, species);
                    if (doseMatch > 0) {
                        if (match == null || doseMatch == EXACT_DOSE_MATCH) {
                            match = doseBean;
                            matchUnits = units;
                        }
                        if (doseMatch == EXACT_DOSE_MATCH) {
                            break;
                        }
                    }
                }
            }
            if (match != null) {
                result = calculateDose(match, weight, matchUnits, concentration);
            }
        }
        return result;
    }

    /**
     * Returns all active <em>entityLink.productSupplier</em> relationships for a particular product and supplier.
     *
     * @param product  the product
     * @param supplier the supplier
     * @return the relationships, wrapped in {@link ProductSupplier} instances, sorted on increasing identifier
     */
    public List<ProductSupplier> getProductSuppliers(Product product, Party supplier) {
        List<ProductSupplier> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(product);
        Predicate<Relationship> and = Predicates.activeNow().and(Predicates.targetEquals(supplier));
        List<Relationship> relationships = bean.getValues(SUPPLIERS, Relationship.class, and);
        relationships.sort(Comparator.comparingLong(IMObject::getId));
        for (Relationship relationship : relationships) {
            result.add(new ProductSupplier(relationship, service));
        }
        return result;
    }

    /**
     * Returns an <em>entityLink.productSupplier</em> relationship
     * for a product, supplier, reorder code, package size and units.
     * <p/>
     * If there is a match on reorder code
     * If there is a match on supplier and product, but no match on package
     * size, but there is a relationship where the size is {@code 0}, then
     * this will be returned.
     *
     * @param product      the product
     * @param supplier     the supplier
     * @param reorderCode  the reorder code. May be {@code null}
     * @param packageSize  the package size
     * @param packageUnits the package units. May be {@code null}
     * @return the relationship, wrapped in a {@link ProductSupplier}, or {@code null} if none is found
     */
    public ProductSupplier getProductSupplier(Product product, Party supplier, String reorderCode,
                                              int packageSize, String packageUnits) {
        ProductSupplier result = null;
        ProductSupplier reorderMatch = null;
        ProductSupplier packageMatch = null;
        ProductSupplier sizeMatch = null;
        ProductSupplier zeroMatch = null;
        List<ProductSupplier> list = getProductSuppliers(product, supplier);
        for (ProductSupplier ps : list) {
            boolean reorderEq = Objects.equals(reorderCode, ps.getReorderCode());
            boolean sizeEq = packageSize == ps.getPackageSize();
            boolean unitEq = Objects.equals(packageUnits, ps.getPackageUnits());
            if (reorderEq && sizeEq && unitEq) {
                result = ps;
                break;
            } else if (reorderEq && reorderCode != null) {
                reorderMatch = ps;
            } else if (sizeEq && unitEq) {
                packageMatch = ps;
            } else if (sizeEq) {
                sizeMatch = ps;
            } else if (ps.getPackageSize() == 0) {
                zeroMatch = ps;
            }
        }
        if (result == null) {
            if (reorderMatch != null) {
                result = reorderMatch;
            } else if (packageMatch != null) {
                result = packageMatch;
            } else if (sizeMatch != null) {
                result = sizeMatch;
            } else if (zeroMatch != null) {
                result = zeroMatch;
            }
        }
        return result;
    }

    /**
     * Returns all active <em>entityLink.productSupplier</em> relationships for a particular product.
     *
     * @param product the product
     * @return the relationships, wrapped in {@link ProductSupplier} instances, sorted on increasing identifier
     */
    public List<ProductSupplier> getProductSuppliers(Product product) {
        List<ProductSupplier> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(product);
        List<Relationship> relationships = bean.getValues(SUPPLIERS, Relationship.class, Predicates.activeNow());
        relationships.sort(Comparator.comparingLong(IMObject::getId));
        for (Relationship relationship : relationships) {
            result.add(new ProductSupplier(relationship, service));
        }
        return result;
    }

    /**
     * Creates a new <em>entityLink.productSupplier</em> relationship
     * between a supplier and product.
     *
     * @param product  the product
     * @param supplier the supplier
     * @return the relationship, wrapped in a {@link ProductSupplier}
     */
    public ProductSupplier createProductSupplier(Product product, Party supplier) {
        IMObjectBean bean = service.getBean(product);
        Relationship rel = bean.addTarget(SUPPLIERS, supplier);
        return new ProductSupplier(rel, service);
    }

    /**
     * Returns the preferred {@link ProductSupplier} for a product.
     *
     * @param product the product
     * @return the preferred product-supplier relationship, or {@code null} if none is preferred
     */
    public ProductSupplier getPreferredSupplier(Product product) {
        for (ProductSupplier supplier : getProductSuppliers(product)) {
            if (supplier.isPreferred()) {
                return supplier;
            }
        }
        return null;
    }

    /**
     * Returns the preferred {@link ProductSupplier} for a product, at a stock location.
     *
     * @param product the product
     * @return the preferred product-supplier relationship, or {@code null} if none is preferred
     */
    public ProductSupplier getPreferredSupplier(Product product, Party stockLocation) {
        ProductSupplier result = null;
        Party supplier = null;
        IMObjectRelationship relationship = new StockRules(service).getStockRelationship(product, stockLocation);
        if (relationship != null) {
            IMObjectBean bean = service.getBean(relationship);
            Reference reference = bean.getReference("supplier");
            if (reference != null) {
                supplier = service.get(reference, Party.class);
            }
        }
        if (supplier != null) {
            // the stock location has a preferred supplier. Find the best product-supplier relationship for that
            // supplier
            List<ProductSupplier> suppliers = getProductSuppliers(product, supplier);
            if (suppliers.size() == 1) {
                result = suppliers.get(0);
            } else if (suppliers.size() > 1){
                // use the preferred if available
                for (ProductSupplier ps : suppliers) {
                    if (ps.isPreferred()) {
                        result = ps;
                        break;
                    }
                }
                if (result == null) {
                    result = suppliers.get(0);
                }
            }
        } else {
            result = getPreferredSupplier(product);
        }
        return result;
    }

    /**
     * Returns all product batches matching the criteria.
     *
     * @param product      the product
     * @param batchNumber  the batch number
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the manufacturer. May be {@code null}
     * @return the batches
     */
    public List<Entity> getBatches(Product product, String batchNumber, Date expiryDate, Party manufacturer) {
        return getBatches(product.getObjectReference(), batchNumber, expiryDate,
                          (manufacturer != null) ? manufacturer.getObjectReference() : null);
    }

    /**
     * Returns all product batches matching the criteria, ordered on batch number and entity Id.
     * <p/>
     * Note that this should not be used if the expected number of results is large; use a query instead.
     *
     * @param product      the product
     * @param batchNumber  the batch number. May be {@code null}
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the manufacturer. May be {@code null}
     * @return the batches
     */
    public List<Entity> getBatches(Reference product, String batchNumber, Date expiryDate, Reference manufacturer) {
        ArchetypeQuery query = new ArchetypeQuery(ProductArchetypes.PRODUCT_BATCH, false, false);
        if (!StringUtils.isEmpty(batchNumber)) {
            query.add(eq("name", batchNumber));
        }
        JoinConstraint join = join(PRODUCT, "p");
        query.add(join.add(eq(TARGET, product)));
        if (expiryDate != null) {
            join.add(gte(ACTIVE_END_TIME, DateRules.getDate(expiryDate)));   // need to ignore any time components
            join.add(lt(ACTIVE_END_TIME, DateRules.getNextDate(expiryDate)));
        }
        if (manufacturer != null) {
            query.add(join("manufacturer").add(eq(TARGET, manufacturer)));
        }
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        IPage<IMObject> page = (IPage<IMObject>) (IPage<?>) service.get(query);
        query.add(sort("name"));
        query.add(sort("id"));

        List<Entity> batches = new ArrayList<>();
        for (IMObject object : page.getResults()) {
            batches.add((Entity) object);
        }
        return batches;
    }

    /**
     * Creates a new batch.
     *
     * @param product      the product
     * @param batchNumber  the batch number
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the manufacturer. May be {@code null}
     * @return a new batch
     */
    public Entity createBatch(Product product, String batchNumber, Date expiryDate, Party manufacturer) {
        return createBatch(product.getObjectReference(), batchNumber, expiryDate,
                           (manufacturer != null) ? manufacturer.getObjectReference() : null);
    }

    /**
     * Creates a new batch.
     *
     * @param product      the product
     * @param batchNumber  the batch number
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the manufacturer. May be {@code null}
     * @return a new batch
     */
    public Entity createBatch(Reference product, String batchNumber, Date expiryDate, Reference manufacturer) {
        Entity result = service.create(ProductArchetypes.PRODUCT_BATCH, Entity.class);
        IMObjectBean bean = service.getBean(result);
        bean.setValue("name", batchNumber);
        Relationship relationship = bean.setTarget(PRODUCT, product);
        IMObjectBean relBean = service.getBean(relationship);
        relBean.setValue(ACTIVE_END_TIME, expiryDate);
        if (manufacturer != null) {
            bean.setTarget("manufacturer", manufacturer);
        }
        return result;
    }

    /**
     * Returns the expiry date of a batch.
     *
     * @param batch the batch
     * @return the batch expiry date, or {@code null} if none is set
     */
    public Date getBatchExpiry(Entity batch) {
        Date result = null;
        IMObjectBean bean = service.getBean(batch);
        EntityLink product = bean.getObject(PRODUCT, EntityLink.class);
        if (product != null) {
            result = product.getActiveEndTime();
        }
        return result;
    }

    /**
     * Determines if a product can be used at the specified location.
     * <p/>
     * This is only applicable to service and template products. For other products, it always returns {@code true}.
     * For medication and merchandise products, the stock location must be checked.
     *
     * @param product  the product
     * @param location the practice location
     * @return {@code true} if the product can be used at the location
     */
    public boolean canUseProductAtLocation(Product product, Party location) {
        IMObjectBean bean = service.getBean(product);
        return !bean.getTargetRefs("locations").contains(location.getObjectReference());
    }

    /**
     * Determines if a drug is classed as restricted.
     *
     * @param product the product
     * @return {@code true} if the product has a drug schedule that is restricted, otherwise {@code false}
     */
    public boolean isRestricted(Product product) {
        boolean result = false;
        if (TypeHelper.isA(product, ProductArchetypes.MEDICATION)) {
            Lookup schedule = lookups.getLookup(product, "drugSchedule");
            if (schedule != null) {
                IMObjectBean bean = service.getBean(schedule);
                result = bean.getBoolean("restricted", false);
            }
        }
        return result;
    }

    /**
     * Determines if dose is for a species.
     *
     * @param bean    the dose
     * @param species the species code. May be {@code null}
     * @return <= 0 if there is no match, {@link #PARTIAL_DOSE_MATCH} if a species is specified, but the dose has
     * no species, or {@link #EXACT_DOSE_MATCH} if the species and dose species match
     */
    private int isDoseSpeciesMatch(IMObjectBean bean, String species) {
        int result = 0;
        Lookup doseSpecies = bean.getObject("species", Lookup.class);
        if (doseSpecies == null) {
            if (species == null) {
                result = EXACT_DOSE_MATCH;
            } else {
                result = PARTIAL_DOSE_MATCH;
            }
        } else if (doseSpecies.getCode().equals(species)) {
            result = EXACT_DOSE_MATCH;
        }
        return result;
    }

    /**
     * Calculates a dose for the given weight and concentration.
     *
     * @param dose          the does
     * @param weight        the weight
     * @param units         the weight units
     * @param concentration the concentration
     * @return the dose
     */
    private BigDecimal calculateDose(IMObjectBean dose, Weight weight, WeightUnits units, BigDecimal concentration) {
        BigDecimal result = BigDecimal.ZERO;
        BigDecimal converted = weight.convert(units);
        BigDecimal rate = dose.getBigDecimal("rate", BigDecimal.ZERO);
        BigDecimal quantity = dose.getBigDecimal("quantity", BigDecimal.ONE);
        int places = dose.getInt("roundTo");
        if (!isZero(concentration) && !isZero(rate) && !isZero(quantity)) {
            // math here is (rate (per weight unit) * concentration (per dispensing unit)) * quantity
            result = converted.multiply(rate).divide(concentration, places, RoundingMode.HALF_UP)
                    .multiply(quantity);
        }
        return result;
    }

    /**
     * Returns the product concentration.
     *
     * @param bean the product
     * @return the product concentration
     */
    private BigDecimal getConcentration(IMObjectBean bean) {
        if (bean.isA(ProductArchetypes.MEDICATION)) {
            return bean.getBigDecimal("concentration", BigDecimal.ZERO);
        }
        return BigDecimal.ZERO;
    }
}
