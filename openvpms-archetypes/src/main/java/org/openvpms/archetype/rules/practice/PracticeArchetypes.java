/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;


/**
 * Practice archetypes.
 *
 * @author Tim Anderson
 */
public class PracticeArchetypes {

    /**
     * The practice short name.
     */
    public static final String PRACTICE = "party.organisationPractice";

    /**
     * The practice location short name.
     */
    public static final String LOCATION = "party.organisationLocation";

    /**
     * Practice location relationship short name.
     */
    public static final String PRACTICE_LOCATION_RELATIONSHIP
            = "entityRelationship.practiceLocation";

    /**
     * Printer archetype.
     */
    public static final String PRINTER = "entity.printer";

    /**
     * Department archetype.
     */
    public static final String DEPARTMENT = "entity.department";

    /**
     * Default constructor.
     */
    private PracticeArchetypes() {
        // no-op
    }
}
