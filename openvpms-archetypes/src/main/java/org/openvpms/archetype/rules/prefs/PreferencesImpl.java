/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.prefs;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Default implementation of the {@link Preferences} interface.
 *
 * @author Tim Anderson
 */
class PreferencesImpl implements Preferences {

    /**
     * Groups node name.
     */
    protected static final String GROUPS = "groups";

    /**
     * The party the preferences belong to.
     */
    private final Reference party;

    /**
     * The party to use for default preferences. May be {@code null}
     */
    private final Reference source;

    /**
     * The preference groups.
     */
    private final List<PreferenceGroup> groups = new ArrayList<>();

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The transaction manager, or {@code null} if changes aren't persistent.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The preferences.
     */
    private IMObjectBean bean;

    /**
     * Target node name.
     */
    static final String TARGET = "target";

    /**
     * The user node name.
     */
    static final String USER = "user";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PreferencesImpl.class);

    /**
     * Constructs a  {@link PreferencesImpl}. Changes aren't persistent.
     *
     * @param party       the party the preferences belong to
     * @param source      the party to use for default preferences. May be {@code null}
     * @param preferences the preferences entity
     * @param service     the archetype service
     */
    public PreferencesImpl(Reference party, Reference source, Entity preferences, IArchetypeService service) {
        this(party, source, preferences, service, null);
    }

    /**
     * Constructs a {@link PreferencesImpl}.
     *
     * @param party              the user the preferences belong to
     * @param source             the party to use for default preferences. May be {@code null}
     * @param preferences        the preferences entity
     * @param service            the archetype service
     * @param transactionManager the transaction manager, or {@code null} if changes aren't persistent
     */
    public PreferencesImpl(Reference party, Reference source, Entity preferences, IArchetypeService service,
                           PlatformTransactionManager transactionManager) {
        this.party = party;
        this.source = source;
        bean = service.getBean(preferences);
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Returns a user preference, given its preference group name and name.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public Object getPreference(String groupName, String name, Object defaultValue) {
        Object value = getGroup(groupName).get(name);
        return value != null ? value : defaultValue;
    }

    /**
     * Sets a preference.
     *
     * @param groupName the preference group name
     * @param name      the preference name
     * @param value     the preference value. May be {@code null}
     */
    @Override
    public void setPreference(String groupName, String name, Object value) {
        boolean save = transactionManager != null;
        setPreference(groupName, name, value, save);
    }

    /**
     * Returns a preference, given its preference group name and name.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public boolean getBoolean(String groupName, String name, boolean defaultValue) {
        return getGroup(groupName).getBoolean(name, defaultValue);
    }

    /**
     * Returns a preference, given its preference group name and name.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public int getInt(String groupName, String name, int defaultValue) {
        return getGroup(groupName).getInt(name, defaultValue);
    }

    /**
     * Returns a preference, given its preference group name and name.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public long getLong(String groupName, String name, long defaultValue) {
        return getGroup(groupName).getLong(name, defaultValue);
    }

    /**
     * Returns a preference, given its preference group name and name.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public String getString(String groupName, String name, String defaultValue) {
        return getGroup(groupName).getString(name, defaultValue);
    }

    /**
     * Returns the reference value of a property.
     *
     * @param groupName    the preference group name
     * @param name         the preference name
     * @param defaultValue the default value, if the preference is unset. May be {@code null}
     * @return the preference. May be {@code null}
     */
    @Override
    public Reference getReference(String groupName, String name, Reference defaultValue) {
        Reference result = getGroup(groupName).getReference(name);
        return (result != null) ? result : defaultValue;
    }

    /**
     * Returns the available preference group names.
     *
     * @return the group name
     */
    @Override
    public Set<String> getGroupNames() {
        String[] relationshipTypes = getRelationshipTypes();
        return getGroupNames(relationshipTypes);
    }

    /**
     * Returns the available preferences in a group.
     *
     * @param groupName the group name.
     * @return the preference names
     */
    @Override
    public Set<String> getNames(String groupName) {
        PreferenceGroup group = getGroup(groupName);
        return group.getNames();
    }

    /**
     * Returns the available group names. These correspond to the archetype short names of the target node of
     * the relationships.
     *
     * @param relationshipTypes the relationship types
     * @return the available group names
     */
    protected Set<String> getGroupNames(String[] relationshipTypes) {
        return new LinkedHashSet<>(Arrays.asList(DescriptorHelper.getNodeShortNames(relationshipTypes, TARGET,
                                                                                    service)));
    }

    /**
     * Returns the available preference relationship types.
     *
     * @return the relationship types
     */
    protected String[] getRelationshipTypes() {
        return DescriptorHelper.getShortNames("entityLink.preferenceGroup*", service);
    }

    /**
     * Returns a group, given its name.
     * <p>
     * If the group doesn't exist, it will be created.
     *
     * @param name the group name
     * @return the group
     * @throws IllegalArgumentException if the name doesn't correspond to a valid group
     */
    private PreferenceGroup getGroup(String name) {
        PreferenceGroup result = findGroup(name);
        if (result == null) {
            result = loadGroup(name);
        }
        if (result == null) {
            boolean save = transactionManager != null;
            String[] relationshipTypes = getRelationshipTypes();
            for (String relationshipType : relationshipTypes) {
                String[] shortNames = DescriptorHelper.getNodeShortNames(relationshipType, TARGET, service);
                if (ArrayUtils.contains(shortNames, name)) {
                    result = addGroup(name, relationshipType, save);
                    break;
                }
            }
            if (result == null) {
                throw new IllegalArgumentException("Argument 'name' is not a valid preference group name");
            }
        }
        return result;
    }

    /**
     * Finds a cached preference group by name.
     *
     * @param name the group name
     * @return the corresponding preference group, or {@code null} if none is found
     */
    private PreferenceGroup findGroup(String name) {
        PreferenceGroup result = null;
        for (PreferenceGroup group : groups) {
            if (group.getEntity().isA(name)) {
                result = group;
                break;
            }
        }
        return result;
    }

    /**
     * Loads a preference group, if  it exists.
     *
     * @param name the group name
     * @return the corresponding group, or {@code null} if it doesn't exist
     */
    private PreferenceGroup loadGroup(String name) {
        PreferenceGroup result = null;
        for (EntityLink link : bean.getValues(GROUPS, EntityLink.class)) {
            Reference target = link.getTarget();
            if (target != null && target.isA(name)) {
                Entity entity = service.get(target, Entity.class);
                if (entity != null) {
                    result = new PreferenceGroup(entity, service);
                    groups.add(result);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Adds a preference group.
     *
     * @param name             the group archetype short name
     * @param relationshipType the relationship type
     * @param save             if {@code true} make the group persistent
     * @return the preference group
     */
    private PreferenceGroup addGroup(String name, String relationshipType, boolean save) {
        PreferenceGroup result;
        Entity entity = service.create(name, Entity.class);
        bean.addTarget(GROUPS, relationshipType, entity);
        if (save) {
            try {
                bean.save(entity);
            } catch (Exception exception) {
                // This can occur if the root preference has been updated in a different browser session.
                // NOTE that this will cause any outer transaction to roll back. See OVPMS-2046
                log.error("Failed to add group=" + name + " to preference=" + bean.getReference(), exception);
                reload();
                addGroup(name, relationshipType, false);
            }
        }
        result = new PreferenceGroup(entity, service);
        groups.add(result);
        return result;
    }

    /**
     * Sets a preference.
     *
     * @param groupName the group name
     * @param name      the preference name
     * @param value     the value
     * @param save      if {@code true}, make it persistent
     */
    private void setPreference(String groupName, String name, Object value, boolean save) {
        PreferenceGroup group = getGroup(groupName);
        Object current = group.get(name);
        if (!Objects.equals(current, value)) {
            group.set(name, value);
            if (save) {
                TransactionTemplate template = new TransactionTemplate(transactionManager);
                template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
                try {
                    template.execute(new TransactionCallbackWithoutResult() {
                        @Override
                        protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                            group.save();
                        }
                    });
                } catch (Exception exception) {
                    // This can occur if the group has been updated in a different browser session.
                    log.debug("Failed to save preference=" + group.getEntity().getObjectReference()
                              + ", name=" + name + ", value=" + value, exception);
                    reload();
                    setPreference(groupName, name, value, false);
                }
            }
        }
    }

    /**
     * Reloads preferences.
     */
    private void reload() {
        Entity prefs = PreferenceManager.getPreferences(party, source, service, transactionManager);
        bean = service.getBean(prefs);
        groups.clear();
    }

}
