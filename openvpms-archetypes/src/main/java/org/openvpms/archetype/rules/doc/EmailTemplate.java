/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Wrapper for <em>entity.documentTemplateEmail*</em>.
 *
 * @author Tim Anderson
 */
public class EmailTemplate extends BaseDocumentTemplate {

    /**
     * Determines how the email subject should be evaluated.
     */
    public enum SubjectType {
        TEXT,     // subject is plain text
        MACRO,    // subject is a macro, or contains macros
        XPATH     // subject is an xpath expression
    }

    /**
     * Determines how the email content should be evaluated.
     */
    public enum ContentType {
        TEXT,     // content is plain text
        MACRO,    // content is a macro, or contains macros
        XPATH,    // content is an xpath expression
        DOCUMENT  // content is a document
    }

    /**
     * Portable Document Format mime-type.
     */
    private static final String PDF_TYPE = "application/pdf";

    /**
     * Constructs an {@link EmailTemplate}.
     *
     * @param template the template
     * @param service  the archetype service
     */
    public EmailTemplate(Entity template, ArchetypeService service) {
        super(template, service);
    }

    /**
     * Returns the subject type.
     *
     * @return the subject type
     */
    public SubjectType getSubjectType() {
        return SubjectType.valueOf(getBean().getString("subjectType", SubjectType.TEXT.name()));
    }

    /**
     * Returns the subject.
     * <p/>
     * This should be evaluated as per the {@link #getSubjectType() subject type}.
     *
     * @return the subject
     */
    public String getSubject() {
        return getBean().getString("subject");
    }

    /**
     * Returns the subject source.
     * <p/>
     * This is an optional xpath expression to return the object the subject should be evaluated against, when
     * the {@link #getSubjectType() subject type} is {@code MACRO} or {@code XPATH}.
     *
     * @return the subject source. May be {@code null}
     */
    public String getSubjectSource() {
        return getBean().getString("subjectSource");
    }

    /**
     * Returns the content type.
     *
     * @return the content type
     */
    public ContentType getContentType() {
        return ContentType.valueOf(getBean().getString("contentType", ContentType.TEXT.name()));
    }

    /**
     * Returns the content.
     * <p/>
     * This should be evaluated as per the {@link #getContentType() content type}.
     *
     * @return the content. May be {@code null}
     */
    public String getContent() {
        return getBean().getString("content");
    }

    /**
     * Returns the content source.
     * <p/>
     * This is an optional xpath expression to return the object the subject should be evaluated against, when
     * the {@link #getContentType()} content type} is {@code MACRO}, {@code XPATH}, or {@code DOCUMENT}.
     *
     * @return the subject source. May be {@code null}
     */
    public String getContentSource() {
        return getBean().getString("contentSource");
    }

    /**
     * Returns the default email address to use when sending emails generated from this template.
     *
     * @return the default email address. May be {@code null}
     */
    public String getDefaultEmailAddress() {
        return getBean().getString("defaultEmailAddress");
    }

    /**
     * Returns the email template attachments.
     * <p/>
     * These are forms with PDF content.
     *
     * @return the active attachments, sorted on increasing entity identity
     */
    public List<DocumentTemplate> getAttachments() {
        return getAttachments(false);
    }

    /**
     * Returns the email template attachments.
     * <p/>
     * These are forms with PDF content.
     *
     * @param sortOnName if {@code true}, sort on template name, else sort on id
     * @return the attachments
     */
    public List<DocumentTemplate> getAttachments(boolean sortOnName) {
        Comparator<Entity> comparator = (sortOnName) ? Comparator.comparing(Entity::getName)
                                                     : Comparator.comparing(Entity::getId);
        // filter any document template that isn't a PDF. It shouldn't be possible to attach these to an email template,
        // but the template could be changed after it is attached
        return getBean().getTargets("attachments", Entity.class, Policies.active())
                .stream()
                .sorted(comparator)
                .map(template -> new DocumentTemplate(template, getService()))
                .filter(template -> PDF_TYPE.equals(template.getMimeType()))
                .collect(Collectors.toList());
    }
}