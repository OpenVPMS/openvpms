/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.message;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.JoinConstraint;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.openvpms.component.system.common.query.OrConstraint;
import org.openvpms.component.system.common.query.ParticipationConstraint;
import org.openvpms.component.system.common.query.RelationalOp;

import java.util.Date;

import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.ActShortName;
import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.StartTime;

/**
 * Rules for <em>act.userMessage</em>, <em>act.systemMessage</em> and <em>act.auditMessage</em>acts
 *
 * @author Tim Anderson
 */
public class MessageRules {

    private final IArchetypeService service;

    /**
     * The message archetypes.
     */
    private final String[] ARCHETYPES = MessageArchetypes.MESSAGES.toArray(new String[0]);

    /**
     * Constructs a {@link MessageRules}.
     *
     * @param service the archetype service
     */
    public MessageRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Determines if there are any unread (i.e <em>PENDING</em>) messages for a user.
     *
     * @param user the user
     * @return {@code true} if there are unread messages; otherwise {@code false}
     */
    public boolean hasNewMessages(User user) {
        return hasNewMessages(user, null);
    }

    /**
     * Determines if there are any unread (i.e <em>PENDING</em>) messages for a user, on or after the specified date.
     *
     * @param user the user
     * @param date the date
     * @return {@code true} if there are unread messages; otherwise {@code false}
     */
    public boolean hasNewMessages(User user, Date date) {
        String[] archetypes = DescriptorHelper.getShortNames(ARCHETYPES, true, service);
        // expand the archetypes, to avoid like clauses

        ArchetypeQuery query = new ArchetypeQuery(archetypes, true, false);
        query.add(new NodeSelectConstraint("id"));
        query.add(Constraints.eq("status", MessageStatus.PENDING));
        JoinConstraint to = Constraints.join("to");
        to.add(Constraints.eq("entity", user.getObjectReference()));

        // duplicate the archetypes onto the participation, so the best index is selected
        if (archetypes.length > 1) {
            OrConstraint or = new OrConstraint();
            for (String archetype : archetypes) {
                or.add(new ParticipationConstraint(ActShortName, archetype));
            }
            to.add(or);
        } else if (archetypes.length == 1) {
            to.add(new ParticipationConstraint(ActShortName, archetypes[0]));
        }
        if (date != null) {
            to.add(new ParticipationConstraint(StartTime, RelationalOp.GTE, date));
        }
        query.add(to);
        if (date != null) {
            query.add(Constraints.gte("startTime", date));
        }
        query.setMaxResults(1);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        return iterator.hasNext();
    }
}
