/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;

/**
 * A {@link PasswordPolicy} backed by {@link Settings}.
 *
 * @author Tim Anderson
 */
public class PasswordPolicySettings extends AbstractPasswordPolicy {

    /**
     * The settings.
     */
    private final Settings settings;

    /**
     * The minimum password length.
     */
    private static final int MIN_LENGTH = 6;

    /**
     * The default password length, if not available in the settings.
     */
    private static final int DEFAULT_LENGTH = 8;

    /**
     * Constructs a {@link PasswordPolicySettings}.
     *
     * @param settings the settings
     */
    public PasswordPolicySettings(Settings settings) {
        this.settings = settings;
    }

    /**
     * Returns the minimum password length.
     *
     * @return the minimum password length
     */
    @Override
    public int getMinLength() {
        int minLength = settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", DEFAULT_LENGTH);
        if (minLength < MIN_LENGTH) {
            minLength = MIN_LENGTH;
        }
        return minLength;
    }

    /**
     * Determines if at least one lowercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean lowercaseRequired() {
        return settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "lowercase", true);
    }

    /**
     * Determines if at least one uppercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean uppercaseRequired() {
        return settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "uppercase", true);
    }

    /**
     * Determines if at least one number is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean numberRequired() {
        return settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "number", true);
    }

    /**
     * Determines if at least one special character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean specialRequired() {
        return settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "special", true);
    }

}
