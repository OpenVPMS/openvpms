/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.math;

import org.apache.commons.resources.Messages;
import org.openvpms.component.exception.OpenVPMSException;


/**
 * Exception class for currencies.
 *
 * @author Tim Anderson
 */
public class CurrencyException extends OpenVPMSException {

    /**
     * An enumeration of error codes.
     */
    public enum ErrorCode {
        InvalidCurrencyCode,
        InvalidRoundingMode,
        NoLookupForCode
    }

    /**
     * The error code.
     */
    private final CurrencyException.ErrorCode errorCode;

    /**
     * The appropriate resource file is loaded cached into memory when this class is loaded.
     */
    private static final Messages messages = Messages.getMessages("org.openvpms.archetype.rules.math.errmessages");

    /**
     * Constructs a {@link CurrencyException}.
     *
     * @param errorCode the error code
     */
    public CurrencyException(CurrencyException.ErrorCode errorCode, Object... args) {
        super(org.openvpms.component.i18n.Messages.create(messages.getMessage(errorCode.toString(), args)));
        this.errorCode = errorCode;
    }

    /**
     * Returns the error code.
     *
     * @return the error code
     */
    public CurrencyException.ErrorCode getErrorCode() {
        return errorCode;
    }
}
