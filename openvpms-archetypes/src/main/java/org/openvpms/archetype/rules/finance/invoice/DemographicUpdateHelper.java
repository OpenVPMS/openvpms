/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.product.DemographicUpdater;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.NodeConstraint;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.util.Collections;
import java.util.List;


/**
 * Demographic update helper.
 *
 * @author Tim Anderson
 */
class DemographicUpdateHelper {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The item.
     */
    private final IMObjectBean itemBean;

    /**
     * The product.
     */
    private IMObjectBean productBean;

    /**
     * Constructs a {@link DemographicUpdateHelper}.
     *
     * @param item    the invoice item
     * @param service the archetype service
     */
    public DemographicUpdateHelper(Act item, IArchetypeService service) {
        itemBean = service.getBean(item);
        Product product = itemBean.getTarget("product", Product.class);
        if (product != null) {
            productBean = service.getBean(product);
        }
        this.service = service;
    }

    /**
     * Constructs a {@link DemographicUpdateHelper}.
     *
     * @param itemBean    the invoice item
     * @param productBean the product
     * @param service     the archetype service
     */
    public DemographicUpdateHelper(IMObjectBean itemBean, IMObjectBean productBean, IArchetypeService service) {
        this.itemBean = itemBean;
        this.productBean = productBean;
        this.service = service;
    }

    /**
     * Processes an demographic updates associated with a product, if the parent invoice is <em>POSTED</em>.
     *
     * @param invoice the parent invoice
     */
    public void processDemographicUpdates(Act invoice) {
        if (ActStatus.POSTED.equals(invoice.getStatus())) {
            List<Lookup> updates = getDemographicUpdates();
            if (!updates.isEmpty()) {
                processDemographicUpdates(updates);
            }
        }
    }

    /**
     * Processes demographic updates if the invoice is POSTED.
     */
    public void processDemographicUpdates() {
        List<Lookup> updates = getDemographicUpdates();
        if (!updates.isEmpty() && invoicePosted()) {
            processDemographicUpdates(updates);
        }
    }

    /**
     * Returns the demographic updates associated with a product.
     *
     * @return the demographic updates
     */
    private List<Lookup> getDemographicUpdates() {
        List<Lookup> updates = Collections.emptyList();
        if (productBean != null && productBean.hasNode("updates")) {
            updates = productBean.getValues("updates", Lookup.class);
        }
        return updates;
    }

    /**
     * Determines if the parent invoice is posted.
     *
     * @return {@code true} if the parent invoice is posted
     */
    private boolean invoicePosted() {
        Reference reference = itemBean.getSourceRef("invoice");
        if (reference != null) {
            ArchetypeQuery query = new ArchetypeQuery(new ObjectRefConstraint("act", reference));
            query.add(new NodeSelectConstraint("act.status"));
            query.add(new NodeConstraint("status", ActStatus.POSTED));
            query.setMaxResults(1);
            ObjectSetQueryIterator iter = new ObjectSetQueryIterator(service, query);
            return iter.hasNext();
        }
        return false;
    }

    /**
     * Returns the demographic updates associated with a product.
     *
     * @param updates the demographic updates
     */
    private void processDemographicUpdates(List<Lookup> updates) {
        DemographicUpdater updater = new DemographicUpdater(service);
        updater.evaluate(itemBean.getObject(), updates);
    }

}
