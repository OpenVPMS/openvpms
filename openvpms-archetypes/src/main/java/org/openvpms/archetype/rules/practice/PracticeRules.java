/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.math.CurrencyException;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.singleton.SingletonQuery;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.List;


/**
 * Practice rules.
 *
 * @author Tim Anderson
 */
public class PracticeRules {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The currencies.
     */
    private final Currencies currencies;

    /**
     * Constructs a {@link PracticeRules}.
     *
     * @param service    the archetype service
     * @param currencies the currencies
     */
    public PracticeRules(ArchetypeService service, Currencies currencies) {
        this.service = service;
        this.currencies = currencies;
    }

    /**
     * Determines if the specified practice is the only active practice.
     *
     * @param practice the practice
     * @throws ArchetypeServiceException for any archetype service error
     */
    public boolean isActivePractice(Party practice) {
        boolean result = false;
        if (practice.isActive()) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<Party> query = builder.createQuery(Party.class);
            Root<Party> root = query.from(Party.class, PracticeArchetypes.PRACTICE);
            query.select(root.get("id"));
            query.where(builder.equal(root.get("active"), true),
                        builder.notEqual(root.get("id"), practice.getId()));
            if (service.createQuery(query).getFirstResult() == null) {
                result = true; // no other active practice
            }
        }
        return result;
    }

    /**
     * Returns the practice.
     *
     * @return the practice, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Party getPractice() {
        return getPractice(service);
    }

    /**
     * Returns the active locations associated with a practice.
     *
     * @param practice the practice
     * @return the locations associated with the user
     * @throws ArchetypeServiceException for any archetype service error
     */
    public List<Party> getLocations(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getTargets("locations", Party.class, Policies.active());
    }

    /**
     * Returns the default user to be used by background services.
     *
     * @param practice the practice
     * @return the service user
     */
    public User getServiceUser(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getTarget("serviceUser", User.class);
    }

    /**
     * Returns the currency associated with a practice.
     *
     * @param practice the practice
     * @return the practice currency
     * @throws CurrencyException if the currency code is invalid or no <em>lookup.currency</em> is defined for the
     *                           currency
     */
    public Currency getCurrency(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        String code = bean.getString("currency");
        return currencies.getCurrency(code);
    }

    /**
     * Returns the default location associated with a practice.
     *
     * @param practice the practice
     * @return the default location, or the first location if there is no
     * default location or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Party getDefaultLocation(Party practice) {
        return (Party) EntityRelationshipHelper.getDefaultTarget(practice, "locations", service);
    }

    /**
     * Determines the period after which patient medical records are locked.
     *
     * @param practice the practice
     * @return the period, or {@code null} if no period is defined
     */
    public Period getRecordLockPeriod(Party practice) {
        return getPeriod(practice, "recordLockPeriod", "recordLockPeriodUnits");
    }

    /**
     * Returns the default estimate expiry date, based on the practice settings for the
     * <em>estimateExpiryPeriod</em> and <em>estimateExpiryUnits</em> nodes.
     *
     * @param startDate the estimate start date
     * @param practice  the practice configuration
     * @return the estimate expiry date, or {@code null} if there are no default expiry settings
     */
    public Date getEstimateExpiryDate(Date startDate, Party practice) {
        Date result = null;
        IMObjectBean bean = service.getBean(practice);
        int period = bean.getInt("estimateExpiryPeriod");
        String units = bean.getString("estimateExpiryUnits");
        if (period > 0 && !StringUtils.isEmpty(units)) {
            result = DateRules.getDate(startDate, period, DateUnits.valueOf(units));
        }
        return result;
    }

    /**
     * Returns the default prescription expiry date, based on the practice settings for the
     * <em>prescriptionExpiryPeriod</em> and <em>prescriptionExpiryUnits</em> nodes.
     *
     * @param startDate the prescription start date
     * @param practice  the practice configuration
     * @return the prescription expiry date, or {@code startDate} if there are no default expiry settings
     */
    public Date getPrescriptionExpiryDate(Date startDate, org.openvpms.component.model.party.Party practice) {
        IMObjectBean bean = service.getBean(practice);
        int period = bean.getInt("prescriptionExpiryPeriod");
        String units = bean.getString("prescriptionExpiryUnits");
        if (!StringUtils.isEmpty(units)) {
            return DateRules.getDate(startDate, period, DateUnits.valueOf(units));
        }
        return startDate;
    }

    /**
     * Returns the field separator to use when exporting files.
     *
     * @param practice the practice
     * @return the field separator
     */
    public char getExportFileFieldSeparator(Party practice) {
        char separator = ',';
        IMObjectBean bean = service.getBean(practice);
        if ("TAB".equals(bean.getString("fileExportFormat"))) {
            separator = '\t';
        }
        return separator;
    }

    /**
     * Determines if SMS is configured for the practice.
     *
     * @param practice the practice
     * @return {@code true} if SMS is configured, otherwise {@code false}
     */
    public boolean isSMSEnabled(Party practice) {
        boolean enabled = false;
        IMObjectBean bean = service.getBean(practice);
        Reference ref = bean.getTargetRef("sms");
        if (ref != null) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<?> query = builder.createQuery(Entity.class);
            Root<?> root = query.from(Entity.class, ref.getArchetype());
            query.select(root.get("id"));
            query.where(builder.equal(root.get("id"), ref.getId()), builder.equal(root.get("active"), true));
            enabled = service.createQuery(query).getFirstResult() != null;
        }
        return enabled;
    }

    /**
     * Determines if products should be filtered by location.
     *
     * @param practice the practice
     * @return {@code true} if products should be filtered by location
     */
    public boolean useLocationProducts(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getBoolean("useLocationProducts");
    }

    /**
     * Determines if list price decreases should be ignored when calculating product prices.
     *
     * @param practice the practice
     * @return {@code true} if decreases in price should be ignored, otherwise {@code false}
     */
    public boolean ignoreListPriceDecreases(org.openvpms.component.model.party.Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getBoolean("ignoreListPriceDecreases");
    }

    /**
     * Determines if departments are enabled..
     *
     * @param practice the practice
     * @return {@code true} if departments are enabled
     */
    public boolean departmentsEnabled(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getBoolean("departments");
    }

    /**
     * Determines if finalisation of orders containing restricted medications is limited to clinicians.
     *
     * @param practice the practice
     * @return {@code true} if only clinicians can finalize and send ESCI orders.
     */
    public boolean isOrderingRestricted(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        return bean.getBoolean("restrictOrdering");
    }

    /**
     * Determines the period after an invoice is finalised that pharmacy orders are discontinued.
     * <p/>
     * If no period is defined, orders are discontinued when invoices are finalised.
     *
     * @param practice the practice
     * @return the period, or {@code null} if no period is defined
     */
    public Period getPharmacyOrderDiscontinuePeriod(Party practice) {
        return getPeriod(practice, "pharmacyOrderDiscontinuePeriod", "pharmacyOrderDiscontinuePeriodUnits");
    }

    /**
     * Returns the base URL.
     * <p>
     * This contains the scheme, host and port and servlet context.
     *
     * @param practice the practice
     * @return the base URL. May be {@code null}
     */
    public String getBaseURL(Party practice) {
        return service.getBean(practice).getString("baseUrl");
    }

    /**
     * Determines if plugins are enabled.
     *
     * @param practice the practice
     * @return {@code true} if plugins are enabled, otherwise {@code false}
     */
    public boolean pluginsEnabled(Party practice) {
        return service.getBean(practice).getBoolean("enablePlugins");
    }

    /**
     * Returns the default weight units.
     *
     * @param practice the practice
     * @return the default weight units
     */
    public WeightUnits getDefaultWeightUnits(org.openvpms.component.model.party.Party practice) {
        IMObjectBean bean = service.getBean(practice);
        String units = bean.getString("defaultWeightUnits", WeightUnits.KILOGRAMS.toString());
        return WeightUnits.valueOf(units);
    }

    /**
     * Returns the practice.
     *
     * @param service the archetype service
     * @return the practice, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static Party getPractice(ArchetypeService service) {
        return new SingletonQuery(service).get(PracticeArchetypes.PRACTICE, Party.class);
    }

    /**
     * Returns a configured period.
     *
     * @param practice   the practice configuration
     * @param periodName the period node name
     * @param unitsName  the period units node name
     * @return the period, or {@code null} if none is defined
     */
    private Period getPeriod(Party practice, String periodName, String unitsName) {
        return PeriodHelper.getPeriod(service.getBean(practice), periodName, unitsName);
    }

}
