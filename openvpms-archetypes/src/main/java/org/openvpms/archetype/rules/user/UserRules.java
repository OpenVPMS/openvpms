/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.scheduler.JobScheduler;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NodeConstraint;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.openvpms.component.system.common.util.StringUtilities;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * User rules.
 *
 * @author Tim Anderson
 */
public class UserRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Locations node name.
     */
    private static final String LOCATIONS = "locations";


    /**
     * Constructs a {@link UserRules}.
     *
     * @param service the archetype service
     */
    public UserRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the user with the specified username (login name).
     *
     * @param username the user name
     * @return the corresponding user, or {@code null} if none is found
     */
    public User getUser(String username) {
        ArchetypeQuery query = new ArchetypeQuery(UserArchetypes.USER, true, true);
        query.add(new NodeConstraint("username", username));
        query.setMaxResults(1);
        Iterator<User> iterator = new IMObjectQueryIterator<>(service, query);
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }

    /**
     * Returns the user associated with an authentication request.
     *
     * @param authentication the authentication
     * @return the user, or {@code null} if one cannot be determined
     */
    public User getUser(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        User user = null;
        if (principal instanceof User) {
            user = (User) principal;
        } else if (principal instanceof UserDetails) {
            String name = ((UserDetails) principal).getUsername();
            user = getUser(name);
        }
        return user;
    }

    /**
     * Determines if a user exists with the specified user name.
     *
     * @param username the user name
     * @return {@code true} if a user exists, otherwise {@code false}
     */
    public boolean exists(String username) {
        return checkExists(username, null);
    }

    /**
     * Determines if another user exists with the specified user name.
     *
     * @param username the user name
     * @param user     the user to exclude
     * @return {@code true} if another user exists, otherwise {@code false}
     */
    public boolean exists(String username, User user) {
        return checkExists(username, user);
    }

    /**
     * Determines if a user is a clinician.
     *
     * @param user the user
     * @return {@code true} if the user is a clinician,
     * otherwise {@code false}
     */
    public boolean isClinician(User user) {
        return isA(user, UserArchetypes.CLINICIAN_USER_TYPE);
    }

    /**
     * Determines if a user has administrator privileges.
     *
     * @param user the user to check
     * @return {@code true} if the user is an administrator
     */
    public boolean isAdministrator(User user) {
        boolean result = isA(user, UserArchetypes.ADMINISTRATOR_USER_TYPE);
        if (!result) {
            result = user.isA(UserArchetypes.USER) && "admin".equals(user.getUsername());
        }
        return result;
    }

    /**
     * Determines if a user has a particular user type.
     *
     * @param user     the user
     * @param userType the user type code
     * @return {@code true} if the user has the user type
     */
    public boolean isA(User user, String userType) {
        if (user.isA(UserArchetypes.USER)) {
            for (Lookup lookup : user.getClassifications()) {
                if (lookup.isA(UserArchetypes.USER_TYPE) && userType.equals(lookup.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the active locations associated with a user.
     *
     * @param user the user
     * @return the locations associated with the user
     * @throws ArchetypeServiceException for any archetype service error
     */
    public List<Party> getLocations(User user) {
        IMObjectBean bean = service.getBean(user);
        return bean.getTargets(LOCATIONS, Party.class, Policies.active());
    }

    /**
     * Returns the active locations applicable to a user.
     * <p/>
     * These are the locations linked to the user, or if none, those linked to the practice.
     * <p/>
     * Any locations linked to the user but not the practice will be excluded.
     *
     * @param user     the user
     * @param practice the practice
     * @return the locations
     */
    public List<Party> getLocations(User user, Party practice) {
        List<Party> locations = getLocations(user);
        IMObjectBean bean = service.getBean(practice);
        if (locations.isEmpty()) {
            locations = bean.getTargets(LOCATIONS, Party.class, Policies.active());
        } else {
            // exclude any locations not linked to the practice
            List<Reference> references = bean.getTargetRefs(LOCATIONS);
            locations.removeIf(location -> !references.contains(location.getObjectReference()));
        }
        return locations;
    }

    /**
     * Returns the default location associated with a user.
     *
     * @param user the user
     * @return the default location, or the first location if there is no
     * default location or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Party getDefaultLocation(User user) {
        return (Party) EntityRelationshipHelper.getDefaultTarget(user, LOCATIONS, service);
    }

    /**
     * Returns the departments applicable to a user.
     * <p/>
     * These are the departments linked to the user, or if none, active departments.
     *
     * @param user the user
     * @return the departments
     */
    public List<Entity> getDepartments(User user) {
        IMObjectBean bean = service.getBean(user);
        List<Entity> departments = bean.getTargets("departments", Entity.class, Policies.active());
        if (departments.isEmpty()) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
            Root<Entity> root = query.from(Entity.class, PracticeArchetypes.DEPARTMENT);
            query.where(builder.equal(root.get("active"), true));
            departments = service.createQuery(query).getResultList();
        }
        return departments;
    }

    /**
     * Returns the default department for a user from a list of departments.
     *
     * @param user        the user
     * @param departments the available departments
     * @return the department, or {@code null} if one isn't the default
     */
    public Entity getDefaultDepartment(User user, List<Entity> departments) {
        Entity result = null;
        for (EntityLink link : service.getBean(user).getValues("departments", EntityLink.class)) {
            if (service.getBean(link).getBoolean("default")) {
                result = departments.stream()
                        .filter(department -> Objects.equals(department.getObjectReference(), link.getTarget()))
                        .findFirst()
                        .orElse(null);
                if (result != null) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns the signature act associated with a user.
     *
     * @param user the user
     * @return the signature, or {@code null} if none is found
     */
    public DocumentAct getSignature(User user) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> root = query.from(DocumentAct.class, UserArchetypes.SIGNATURE);
        Join<IMObject, IMObject> userJoin = root.join("user").join("entity");
        userJoin.on(builder.equal(userJoin.get("id"), user.getId()));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns all users in a list of users and groups.
     *
     * @param usersOrGroups the list of users and groups
     * @return the users in the list
     */
    public Set<User> getUsers(List<Entity> usersOrGroups) {
        Set<User> result = new HashSet<>();
        Set<Entity> groups = new HashSet<>();
        for (Entity entity : usersOrGroups) {
            if (entity instanceof User) {
                result.add((User) entity);
            } else if (entity != null && !groups.contains(entity)) {
                groups.add(entity);
                List<User> users = getUsers(entity);
                result.addAll(users);
            }
        }
        return result;
    }

    /**
     * Returns all users in a group.
     *
     * @param group the <em>entity.userGroup</em>.
     * @return the users
     */
    public List<User> getUsers(Entity group) {
        IMObjectBean bean = service.getBean(group);
        return bean.getTargets("users", User.class);
    }

    /**
     * Returns the follow-up work lists associated with a user.
     *
     * @param user the user
     * @return the follow-up work lists, in the order they are defined
     */
    public List<Entity> getFollowupWorkLists(User user) {
        return service.getBean(user).getTargets("followupWorkLists", Entity.class, Policies.orderBySequence());
    }

    /**
     * Returns all active users available at a practice location.
     * <p/>
     * These are the users that have a link to the location, or no links to any location.
     *
     * @param location the practice location
     * @return the active users available at the location
     */
    @SuppressWarnings("unchecked")
    public List<User> getClinicians(Party location) {
        IArchetypeQuery query = UserQueryFactory.createClinicianQuery(location, "id");
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        List<?> results = service.get(query).getResults();
        return (List<User>) results;
    }

    /**
     * Determines if a user can edit instances of an archetype.
     * <p/>
     * A user can edit they have create and save authorities for the archetype.
     *
     * @param user      the user
     * @param archetype the archetype. May contain wildcards
     * @return {@code true} if the user can edit instances of the archetype
     */
    public boolean canEdit(User user, String archetype) {
        boolean create = false;
        boolean save = false;
        Collection<GrantedAuthority> authorities
                = ((org.openvpms.component.business.domain.im.security.User) user).getAuthorities();
        for (GrantedAuthority grant : authorities) {
            if (grant instanceof ArchetypeAwareGrantedAuthority) {
                ArchetypeAwareGrantedAuthority authority = (ArchetypeAwareGrantedAuthority) grant;
                if ("archetypeService".equals(authority.getServiceName())) {
                    String method = authority.getMethod();
                    if (StringUtilities.matches(archetype, authority.getShortName())) {
                        if (!create && StringUtilities.matches("create", method)) {
                            create = true;
                        }
                        if (!save && StringUtilities.matches("save", method)) {
                            save = true;
                        }
                        if (create && save) {
                            break;
                        }
                    }
                }
            }
        }
        return create && save;
    }

    /**
     * Determines if a user can save instances of an archetype.
     *
     * @param user      the user
     * @param archetype the archetype. May contain wildcards
     * @return {@code true} if the user can remove instances of the archetype
     */
    public boolean canSave(User user, String archetype) {
        return checkPermission(user, archetype, "save");
    }

    /**
     * Determines if a user can remove instances of an archetype.
     * <p/>
     * A user can edit they have remove authorities for the archetype.
     *
     * @param user      the user
     * @param archetype the archetype. May contain wildcards
     * @return {@code true} if the user can remove instances of the archetype
     */
    public boolean canRemove(User user, String archetype) {
        return checkPermission(user, archetype, "remove");
    }

    /**
     * Returns the first active <em>entity.job*</em> where a user is referenced by the <em>runAs</em> or
     * <em>notify</em> node.
     *
     * @param user the user
     * @return the first active job, or {@code null} if none is found
     */
    public Entity getJobUsedBy(User user) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> job = query.from(Entity.class, JobScheduler.JOB_ARCHETYPE);
        query.select(job);
        Root<EntityLink> relationship = query.from(EntityLink.class, "entityLink.jobUser", "entityLink.jobUserGroup");
        query.where(builder.equal(job.get("id"), relationship.get("source")),
                    builder.equal(relationship.get("target"), user.getId()),
                    builder.equal(job.get("active"), true));
        query.orderBy(builder.asc(job.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Determines if another role exists with the specified name.
     *
     * @param name the role name
     * @param id   the identifier of the role to exclude, or {@code -1} if the role is unsaved
     * @return {@code true} if another role exists, otherwise {@code false}
     */
    public boolean roleExists(String name, long id) {
        return checkExists(name, id, SecurityRole.class, UserArchetypes.ROLE);
    }

    /**
     * Determines if another authority exists with the specified name.
     *
     * @param name the authority name
     * @param id   the identifier of the authority to exclude, or {@code -1} if the authority is unsaved
     * @return {@code true} if another authority exists, otherwise {@code false}
     */
    public boolean authorityExists(String name, long id) {
        return checkExists(name, id, ArchetypeAwareGrantedAuthority.class, UserArchetypes.AUTHORITY);
    }

    /**
     * Checks if a user has permission to perform an action on an archetype.
     *
     * @param user      the user
     * @param archetype the archetype
     * @param action    the action
     * @return {@code true} if the user has permission, otherwise {@code false}
     */
    private boolean checkPermission(User user, String archetype, String action) {
        Collection<GrantedAuthority> authorities
                = ((org.openvpms.component.business.domain.im.security.User) user).getAuthorities();
        for (GrantedAuthority grant : authorities) {
            if (grant instanceof ArchetypeAwareGrantedAuthority) {
                ArchetypeAwareGrantedAuthority authority = (ArchetypeAwareGrantedAuthority) grant;
                if ("archetypeService".equals(authority.getServiceName())) {
                    String method = authority.getMethod();
                    if (StringUtilities.matches(archetype, authority.getShortName())
                        && StringUtilities.matches(action, method)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Determines if there is an existing object with the same name.
     *
     * @param name      the name
     * @param id        the identifier of the object to exclude, or {@code -1} to not exclude any object
     * @param type      the type of the object
     * @param archetype the archetype of the object
     * @return {@code true} if another object exists, otherwise {@code false}
     */
    private <T extends IMObject> boolean checkExists(String name, long id, Class<T> type, String archetype) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> from = query.from(type, archetype);
        query.select(from.get("id"));
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(from.get("name"), name));
        if (id != -1) {
            predicates.add(builder.notEqual(from.get("id"), id));
        }
        query.where(predicates);
        return service.createQuery(query).getFirstResult() != null;
    }

    /**
     * Determines if a user exists with the specified user name.
     *
     * @param username the user name
     * @param user     the user to exclude. May be {@code null}
     * @return {@code true} if a user exists, otherwise {@code false}
     */
    private boolean checkExists(String username, User user) {
        ArchetypeQuery query = new ArchetypeQuery(UserArchetypes.USER, true, false);
        query.add(new NodeSelectConstraint("id"));
        query.add(new NodeConstraint("username", username));
        if (user != null && !user.isNew()) {
            query.add(Constraints.ne("id", user.getId()));
        }
        query.setMaxResults(1);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        return iterator.hasNext();
    }

}
