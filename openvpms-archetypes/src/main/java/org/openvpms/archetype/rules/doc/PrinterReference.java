/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Reference to a printer.
 *
 * @author Tim Anderson
 */
public class PrinterReference implements Comparable<PrinterReference> {

    /**
     * The archetype of the printer service that provides the printer, or {@code null} if it is provided by the
     * Java Print Service API
     */
    private final String archetype;

    /**
     * The display name for the service. May be {@code null}
     */
    private final String serviceName;

    /**
     * The printer identifier.
     */
    private final String id;

    /**
     * The display name for the printer. May be {@code null}
     */
    private final String name;

    /**
     * Used to escape colon characters when encoding the reference to a string.
     */
    private static final String ESCAPE = Pattern.quote("\\");

    /**
     * Used to split the encoded string.
     */
    private static final String SEPARATOR = "(?<!" + ESCAPE + "):";

    /**
     * Constructs a {@link PrinterReference}.
     *
     * @param archetype the printer service archetype
     * @param id        the printer identifier
     */
    public PrinterReference(String archetype, String id) {
        this(archetype, null, id, null);
    }

    /**
     * Constructs a {@link PrinterReference}.
     *
     * @param archetype   the printer service archetype
     * @param serviceName the printer service name
     * @param id          the printer identifier
     * @param name        the printer name
     */
    public PrinterReference(String archetype, String serviceName, String id, String name) {
        this.archetype = archetype;
        this.serviceName = serviceName;
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the archetype of the printer service that provides the printer.
     *
     * @return the archetype of the printer service, or {@code null} if it is provided by the Java Print Service API
     */
    public String getArchetype() {
        return archetype;
    }

    /**
     * Returns the display name of the service.
     *
     * @return the printer service display name. May be {@code null}.
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the display name of the printer.
     *
     * @return the printer display name
     */
    public String getName() {
        return name != null ? name : id;
    }

    /**
     * Determines if this reference is equal to another object.
     *
     * @param obj the object
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof PrinterReference) {
            PrinterReference other = (PrinterReference) obj;
            return Objects.equals(id, other.id) && Objects.equals(archetype, other.archetype);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, archetype);
    }

    /**
     * Encodes the reference to a string.
     *
     * @return the printer reference as a string
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (archetype != null) {
            result.append(escape(archetype));
        }
        result.append(':');
        if (serviceName != null) {
            result.append(escape(serviceName));
        }
        result.append(':');
        result.append(escape(id));
        result.append(':');
        if (name != null) {
            result.append(escape(name));
        }
        return result.toString();
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @param obj the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(PrinterReference obj) {
        int result;
        if (this == obj) {
            result = 0;
        } else {
            Comparator<String> comparator = ComparatorUtils.nullLowComparator(null);
            result = comparator.compare(serviceName, obj.serviceName);
            if (result == 0) {
                result = comparator.compare(archetype, obj.archetype);
                if (result == 0) {
                    result = comparator.compare(name, obj.name);
                    if (result == 0) {
                        result = comparator.compare(id, obj.id);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Parses a printer reference from a string.
     *
     * @param reference the reference. May be {@code null}
     * @return the corresponding printer reference, or {@code null} if it can't be parsed
     */
    public static PrinterReference fromString(String reference) {
        PrinterReference result = null;
        if (reference != null) {
            String[] parts = reference.split(SEPARATOR);
            String id = null;
            String archetype = null;
            String serviceName = null;
            String printerName = null;
            if (parts.length == 1) {
                id = unescape(parts[0]);
            } else if (parts.length == 2) {
                archetype = unescape(parts[0]);
                id = unescape(parts[1]);
            } else if (parts.length == 3) {
                archetype = unescape(parts[0]);
                serviceName = unescape(parts[1]);
                id = unescape(parts[2]);
            } else if (parts.length == 4) {
                archetype = unescape(parts[0]);
                serviceName = unescape(parts[1]);
                id = unescape(parts[2]);
                printerName = unescape(parts[3]);
            }
            if (id != null) {
                result = new PrinterReference(archetype, serviceName, id, printerName);
            }
        }
        return result;
    }

    /**
     * Escapes any colon characters in a value.
     *
     * @param value the value to escape. May be {@code null}
     * @return the escaped value. May be {@code null}
     */
    private String escape(String value) {
        return (value != null) ? value.replace(":", "\\:") : null;
    }

    /**
     * Unescapes any colon characters in a value.
     *
     * @param value the value to unescape. May be {@code null}
     * @return the unescaped value. May be {@code null}
     */
    private static String unescape(String value) {
        value = StringUtils.trimToNull(value);
        return value != null ? value.replaceAll(ESCAPE + ":", ":") : null;
    }
}
