/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient.reminder;

import org.openvpms.archetype.rules.act.ActStatus;


/**
 * Act status types for <em>act.patientReminder</em> acts.
 *
 * @author Tim Anderson
 */
public class ReminderStatus {

    /**
     * In progress reminder status.
     */
    public static final String IN_PROGRESS = ActStatus.IN_PROGRESS;

    /**
     * Completed reminder status.
     */
    public static final String COMPLETED = ActStatus.COMPLETED;

    /**
     * Cancelled act status.
     */
    public static final String CANCELLED = "CANCELLED";

    /**
     * Default constructor.
     */
    private ReminderStatus() {

    }

}

