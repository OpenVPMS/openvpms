/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.model.act.DocumentAct;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * A service to provide access to images associated with {@link DocumentAct}s.
 *
 * @author Tim Anderson
 */
public interface ImageService {

    interface Image {

        /**
         * Returns a URL to the image.
         * <p/>
         * Note that the returned URL can be to a local resource, so cannot be shared.
         *
         * @return the image URL
         * @throws IOException if the image cannot be read
         */
        URL getURL() throws IOException;

        /**
         * Returns a stream to the image.
         *
         * @return the stream
         * @throws IOException if the image cannot be read
         */
        InputStream getInputStream() throws IOException;

        /**
         * Returns the image mime type.
         *
         * @return the mime type
         */
        String getMimeType();

        /**
         * Returns the image size.
         *
         * @return the image size, in bytes, or {@code 0} if it cannot be determined
         */
        long getSize();

        /**
         * Returns the time when the image was last modified.
         *
         * @return the last modified time, or {@code null} if it cannot be determined
         */
        Date getModified();
    }

    /**
     * Returns a URL to the image associated with an act.
     * <p/>
     * Note that the returned URL can be to a local resource, so cannot be shared.
     *
     * @param act the act
     * @return the URL or {@code null} if the act has no image
     * @throws DocumentException for any error
     */
    URL getURL(DocumentAct act);

    /**
     * Returns an image associated with an act.
     *
     * @param act the act
     * @return the image, or {@code null} if the act has no image
     * @throws DocumentException for any error
     */
    Image getImage(DocumentAct act);
}