/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Helper to read text from nodes that are backed by documents if the text is too long to fit.
 * <p/>
 * This supports:
 * <ul>
 *     <li>nodes on {@link DocumentAct}s where the associated document is used to store the long text</li>
 *     <li>nodes on {@link IMObject}s</li> where a related {@link DocumentAct} is used to store the long text</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class LongTextReader {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link LongTextReader}.
     *
     * @param service the archetype service
     */
    public LongTextReader(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the text associated with a document.
     *
     * @param act the document act
     * @return the text. May be {@code null}
     */
    public String getText(DocumentAct act) {
        String text = null;
        Reference reference = act.getDocument();
        if (TypeHelper.isA(reference, DocumentArchetypes.TEXT_DOCUMENT)) {
            Document document = service.get(reference, Document.class);
            if (document != null) {
                text = TextDocumentHandler.asString(document);
            }
        }
        return text;
    }

    /**
     * Returns the text associated with a node, falling back to the document if none is present.
     *
     * @param act  the document act
     * @param node the text node
     * @return the text. May be {@code null}
     */
    public String getText(DocumentAct act, String node) {
        IMObjectBean bean = service.getBean(act);
        String text = bean.getString(node);
        if (text == null) {
            text = getText(act);
        }
        return text;
    }

    /**
     * Returns the text associated with a node, falling back to the document act associated with a relationship node
     * if none is present.
     *
     * @param object           the object
     * @param node             the text node
     * @param relationshipNode the document act relationship node
     * @return the text. May be {@code null}
     */
    public String getText(IMObject object, String node, String relationshipNode) {
        IMObjectBean bean = service.getBean(object);
        String text = bean.getString(node);
        if (text == null) {
            DocumentAct act = bean.getTarget(relationshipNode, DocumentAct.class);
            if (act != null) {
                text = getText(act);
            }
        }
        return text;
    }

}