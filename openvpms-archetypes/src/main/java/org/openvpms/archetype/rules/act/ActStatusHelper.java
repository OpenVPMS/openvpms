/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.act;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;


/**
 * Act status helper methods.
 *
 * @author Tim Anderson
 */
public class ActStatusHelper {

    /**
     * Default constructor.
     */
    private ActStatusHelper() {

    }

    /**
     * Determines if an act was posted previously.
     *
     * @param act     the act
     * @param service the archetype service
     * @return {@code true} if the act was posted previously.
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static boolean isPosted(Act act, ArchetypeService service) {
        return !act.isNew() && isPosted(act.getObjectReference(), service);
    }

    /**
     * Determines if an act is posted, given its reference.
     *
     * @param reference the act reference. May be {@code null}
     * @param service   the archetype service
     * @return {@code true} if the act is posted previously.
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static boolean isPosted(Reference reference, ArchetypeService service) {
        String status = getStatus(reference, service);
        return ActStatus.POSTED.equals(status);
    }

    /**
     * Returns the status of an act, given its reference.
     *
     * @param reference the act reference. May be {@code null}
     * @param service   the archetype service
     * @return the act status, or {@code null} if the {@code reference} is not specified, or the act does not exist
     */
    public static String getStatus(Reference reference, ArchetypeService service) {
        String result = null;
        if (reference != null) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<String> query = builder.createQuery(String.class);
            Root<Act> from = query.from(Act.class, reference.getArchetype());
            query.where(builder.equal(from.get("id"), reference.getId()));
            query.select(from.get("status"));
            result = service.createQuery(query).getFirstResult();
        }
        return result;
    }

}
