/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.security;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Firewall settings.
 *
 * @author Tim Anderson
 */
public class FirewallSettings {

    /**
     * Determines where users can connect from.
     */
    public enum AccessType {
        UNRESTRICTED,  // users can connect from anywhere
        ALLOWED_ONLY,  // users can only connect from addresses in the allowed list
        ALLOWED_USER   // users can only connect from addresses in the allowed list, unless they have
        // 'connectFromAnywhere' enabled
    }

    /**
     * The settings bean.
     */
    private final IMObjectBean settings;

    /**
     * Constructs an {@link FirewallSettings}.
     *
     * @param settings the settings. An instance of <em>entity.globalSettingsFirewall</em>
     * @param service  the archetype service
     */
    public FirewallSettings(Entity settings, ArchetypeService service) {
        this(service.getBean(settings));
    }

    /**
     * Constructs an {@link FirewallSettings}.
     *
     * @param settings the settings bean
     */
    public FirewallSettings(IMObjectBean settings) {
        this.settings = settings;
    }

    /**
     * Returns the firewall access type.
     * <p/>
     * This determines where users can connect from
     *
     * @return the access type
     */
    public AccessType getAccessType() {
        return AccessType.valueOf(settings.getString("accessType", AccessType.UNRESTRICTED.toString()));
    }

    /**
     * Sets the firewall access type.
     *
     * @param accessType the access type
     */
    public void setAccessType(AccessType accessType) {
        settings.setValue("accessType", accessType.toString());
    }

    /**
     * Determines if multifactor authentication is enabled.
     *
     * @return {@code true} if multifactor authentication is enabled, otherwise {@code false}
     */
    public boolean isMultifactorAuthenticationEnabled() {
        return settings.getBoolean("enableMfa");
    }

    /**
     * Determines if multifactor authentication is enabled.
     *
     * @param enable if {@code true}, enable multifactor authentication, else disable it
     */
    public void setMultifactorAuthenticationEnabled(boolean enable) {
        settings.setValue("enableMfa", enable);
    }

    /**
     * Returns the addresses that users may connect from.
     * <p/>
     * If empty, users can connect from any address.
     *
     * @return the allowed addresses
     */
    public List<FirewallEntry> getAllowedAddresses() {
        List<FirewallEntry> list = new ArrayList<>();
        for (String line : getAllowedLines()) {
            FirewallEntry entry = parse(line);
            if (entry != null) {
                list.add(entry);
            }
        }
        return list;
    }

    /**
     * Sets the allowed addresses.
     *
     * @param allowed the addresses that users can connect from
     * @throws IllegalArgumentException if there are too many addresses to encode onto the available nodes
     */
    public void setAllowedAddresses(List<FirewallEntry> allowed) {
        StringBuilder buffer = new StringBuilder();
        int i = 0;
        AddressNode bucket = AddressNode.create(settings, i);
        for (FirewallEntry entry : allowed) {
            String line = encode(entry);
            if (buffer.length() + line.length() < bucket.getMaxLength()) {
                buffer.append(line);
            } else {
                bucket.setAddresses(buffer.toString());
                i++;
                bucket = AddressNode.create(settings, i);
                buffer = new StringBuilder(line);
            }
        }
        bucket.setAddresses(buffer.toString());

        // clear remaining nodes
        boolean done = false;
        while (!done) {
            bucket = AddressNode.createIfExists(settings, ++i);
            if (bucket != null) {
                bucket.setAddresses(null);
            } else {
                done = true;
            }
        }
    }

    /**
     * Returns the underlying settings.
     *
     * @return the settings
     */
    public Entity getSettings() {
        return settings.getObject(Entity.class);
    }

    /**
     * Returns each line from the allowed addresses.
     * <p/>
     * The addresses are encoded using multiple nodes, starting at allowed0.
     *
     * @return the allowed addresses
     */
    private List<String> getAllowedLines() {
        List<String> result = new ArrayList<>();
        int i = 0;
        boolean done = false;
        while (!done) {
            AddressNode bucket = AddressNode.createIfExists(settings, i);
            if (bucket != null) {
                result.addAll(bucket.getAddresses());
                i++;
            } else {
                done = true;
            }
        }
        return result;
    }

    /**
     * Encodes a firewall entry to a string. This can be parsed using {@link #parse(String)}.
     * <p/>
     * This is of the format: {@code <address> <active>[ description]\n}.
     *
     * @param entry the entry to encode
     * @return the encoded string
     */
    private String encode(FirewallEntry entry) {
        StringBuilder line = new StringBuilder();
        String address = StringUtils.strip(entry.getAddress());
        String description = StringUtils.strip(entry.getDescription());
        line.append(address).append(' ').append(entry.isActive() ? '1' : '0');
        if (description != null) {
            line.append(' ').append(description);
        }
        line.append('\n');
        return line.toString();
    }

    /**
     * Parses an entry encoded using {@link #encode(FirewallEntry)}.
     *
     * @param line the line
     * @return the parsed entry
     */
    private FirewallEntry parse(String line) {
        FirewallEntry result = null;
        int first = line.indexOf(' ');
        if (first != -1) {
            String address = line.substring(0, first);
            int second = line.indexOf(' ', first + 1);
            boolean active;
            String description = null;
            if (second != -1) {
                active = Integer.parseInt(line.substring(first + 1, second)) > 0;
                if (second + 1 < line.length()) {
                    description = line.substring(second + 1);
                }
            } else {
                active = Integer.parseInt(line.substring(first + 1)) > 0;
            }
            result = new FirewallEntry(address, active, description);
        }
        return result;
    }

    /**
     * Helper to manage extracting addresses from an address node.
     */
    private static class AddressNode {

        /**
         * The settings bean.
         */
        private final IMObjectBean bean;

        /**
         * The address node name.
         */
        private final String node;

        /**
         * The maximum no. of characters the node can store.
         */
        private final int maxLength;

        /**
         * Constructs an {@link AddressNode}.
         *
         * @param bean the settings bean
         * @param node the node name
         */
        public AddressNode(IMObjectBean bean, String node) {
            this.bean = bean;
            this.node = node;
            maxLength = bean.getMaxLength(node);
        }

        /**
         * Returns the addresses.
         *
         * @return the addresses
         */
        public List<String> getAddresses() {
            List<String> result;
            String addresses = bean.getString(node);
            if (!StringUtils.isEmpty(addresses)) {
                result = Arrays.asList(StringUtils.split(addresses, '\n'));
            } else {
                result = Collections.emptyList();
            }
            return result;
        }

        /**
         * Sets the addresses, as an encoded string.
         *
         * @param addresses the addresses
         */
        public void setAddresses(String addresses) {
            bean.setValue(node, addresses);
        }

        /**
         * Returns the maximum length the node can store.
         *
         * @return the max length
         */
        public int getMaxLength() {
            return maxLength;
        }

        /**
         * Creates an {@link AddressNode}.
         *
         * @param bean  the settings bean
         * @param count the node count
         * @return a new node
         * @throws IllegalArgumentException if there is no node corresponding to the count
         */
        public static AddressNode create(IMObjectBean bean, int count) {
            AddressNode result = createIfExists(bean, count);
            if (result == null) {
                throw new IllegalArgumentException("No node for count: " + count);
            }
            return result;
        }

        /**
         * Creates an address node if one exists for the specified count.
         *
         * @param bean  the settings bean
         * @param count the node count
         * @return a new node, or {@code null} if there is no node for the count
         */
        public static AddressNode createIfExists(IMObjectBean bean, int count) {
            String node = "allowed" + count;
            return (bean.hasNode(node)) ? new AddressNode(bean, node) : null;
        }
    }
}