/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.archetype.rules.doc.ImageService.Image;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.springframework.beans.factory.DisposableBean;

import java.net.URL;

/**
 * Logo service.
 * <p/>
 * This maintains a cache of <em>act.documentLogo</em> to improve response times.
 *
 * @author Tim Anderson
 * @see ImageService
 */
public class LogoService implements DisposableBean {

    /**
     * The image service.
     */
    private final ImageService imageService;

    /**
     * The logo cache.
     */
    private final LogoCache cache;

    /**
     * Constructs a {@link LogoService}.
     * <p/>
     * This preloads the cache.
     *
     * @param imageService the image service
     * @param service      the archetype service
     * @param rules        the document rules
     */
    public LogoService(ImageService imageService, IArchetypeService service, DocumentRules rules) {
        this(imageService, service, rules, true);
    }

    /**
     * Constructs a {@link LogoService}.
     *
     * @param imageService the image service
     * @param service      the archetype service
     * @param rules        the document rules
     * @param load         if {@code true}, preload the cache
     */
    public LogoService(ImageService imageService, IArchetypeService service, DocumentRules rules, boolean load) {
        this.imageService = imageService;
        cache = new LogoCache(service, rules, load);
    }

    /**
     * Returns the URL to the logo for an entity.
     *
     * @param entity the entity
     * @return the logo URL or {@code null} if none is found
     * @throws DocumentException if the logo cannot be read
     */
    public Image getImage(Entity entity) {
        DocumentAct logo = getLogo(entity);
        return (logo != null) ? imageService.getImage(logo) : null;
    }

    /**
     * Returns the URL to the logo for an entity.
     *
     * @param entity the entity
     * @return the logo URL or {@code null} if none is found
     * @throws DocumentException if the logo cannot be read
     */
    public URL getURL(Entity entity) {
        DocumentAct act = getLogo(entity);
        return act != null ? getURL(act) : null;
    }

    /**
     * Returns the URL to a logo.
     *
     * @param act the logo act
     * @return the logo URL or {@code null} if none is found
     * @throws DocumentException if the logo cannot be read
     */
    public URL getURL(DocumentAct act) {
        return imageService.getURL(act);
    }

    /**
     * Returns the logo act for an entity.
     *
     * @param entity the entity
     * @return the logo act, or {@code null} if none is found
     */
    public DocumentAct getLogo(Entity entity) {
        return cache.getLogo(entity);
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        cache.destroy();
    }

}