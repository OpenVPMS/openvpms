/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.deposit;

import org.openvpms.archetype.rules.act.ActCalculator;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.component.business.dao.im.Page;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.ObjectSet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Deposit query.
 *
 * @author tony
 */
public class DepositQuery {

    /**
     * The bank deposit act.
     */
    public static final String BANK_DEPOSIT = "bankDeposit";

    /**
     * The till balance act.
     */
    public static final String TILL_BALANCE = "tillBalance";

    /**
     * The till balance act item.
     */
    public static final String ACT = "act";

    /**
     * The act item.
     */
    public static final String ACT_ITEM = "item";

    /**
     * The act amount.
     */
    public static final String AMOUNT = "amount";

    /**
     * The till balance.
     */
    private final FinancialAct bankDeposit;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The amount calculator.
     */
    private final ActCalculator calculator;

    /**
     * Constructs a {@link DepositQuery}.
     *
     * @param service the archetype service
     */
    public DepositQuery(FinancialAct bankDeposit, IArchetypeService service) {
        this.bankDeposit = bankDeposit;
        this.service = service;
        calculator = new ActCalculator(service);
    }

    /**
     * Executes the query.
     *
     * @return the query results.
     * @throws ArchetypeServiceException if the query fails
     */
    public IPage<ObjectSet> query() {
        List<ObjectSet> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(bankDeposit);
        for (Act tillBalance : bean.getTargets("items", Act.class)) {
            IMObjectBean tillBalanceBean = service.getBean(tillBalance);
            for (FinancialAct tillBalanceItem : tillBalanceBean.getTargets("items", FinancialAct.class)) {
                IMObjectBean tillBalanceItemBean = service.getBean(tillBalanceItem);
                if (tillBalanceItemBean.isA(TillArchetypes.TILL_BALANCE_ADJUSTMENT)) {
                    ObjectSet set = createCashAdjustmentSet(tillBalanceItem);
                    result.add(set);
                } else {
                    // payment or refund
                    for (Act item : tillBalanceItemBean.getTargets("items", Act.class)) {
                        // Skip any discounts
                        if (!item.getArchetype().endsWith("Discount")) {
                            ObjectSet set = createPaymentRefundSet(tillBalanceItem, item);
                            result.add(set);
                        }
                    }
                }
            }
        }
        sort(result);
        return new Page<>(result, 0, result.size(), result.size());
    }

    /**
     * Sorts object sets on item payment type.
     *
     * @param sets the sets to sort
     */
    private void sort(List<ObjectSet> sets) {
        sets.sort((o1, o2) -> {
            Act a1 = (Act) o1.get(ACT_ITEM);
            Act a2 = (Act) o2.get(ACT_ITEM);
            return getPaymentTypeNumber(a1.getArchetype()) - getPaymentTypeNumber(a2.getArchetype());
        });
    }

    /**
     * Creates an {@link ObjectSet} containing the bank deposit, the till balance adjustment and a cash payment or
     * refund corresponding to an till balance adjustment.
     *
     * @param adjustment the till balance adjustment
     * @return a new {@link ObjectSet}
     */
    private ObjectSet createCashAdjustmentSet(FinancialAct adjustment) {
        ObjectSet set = new ObjectSet();
        set.set(BANK_DEPOSIT, bankDeposit);
        set.set(ACT, adjustment);
        Act item;
        if (adjustment.isCredit()) {
            item = service.create(CustomerAccountArchetypes.PAYMENT_CASH, FinancialAct.class);
        } else {
            item = service.create(CustomerAccountArchetypes.REFUND_CASH, FinancialAct.class);
        }
        IMObjectBean itemBean = service.getBean(item);
        BigDecimal amount = getAmount(adjustment);

        itemBean.setValue(AMOUNT, amount);
        item.setDescription(adjustment.getDescription());
        set.set(ACT_ITEM, item);
        set.set(AMOUNT, amount.negate());
        return set;
    }

    /**
     * Creates an {@link ObjectSet} containing the bank deposit, payment or refund, and payment/refund item.
     *
     * @param paymentRefund the payment or refund
     * @param item          the payment or refund item
     * @return a new {@link ObjectSet}
     */
    private ObjectSet createPaymentRefundSet(FinancialAct paymentRefund, Act item) {
        ObjectSet set = new ObjectSet();
        set.set(BANK_DEPOSIT, bankDeposit);
        set.set(ACT, paymentRefund);
        set.set(ACT_ITEM, item);
        set.set(AMOUNT, getAmount(item).negate());
        return set;
    }

    /**
     * Returns an amount, taking into account any credit node.
     *
     * @param act the act
     * @return the amount corresponding to {@code node}
     * @throws ArchetypeServiceException for any archetype service error
     */
    private BigDecimal getAmount(Act act) {
        return calculator.getAmount(act, AMOUNT);
    }

    /**
     * Returns a number based on the payment type extracted from the shortname
     *
     * @param shortName the archetype shortname
     * @return a number corresponding to the payment type
     */
    private int getPaymentTypeNumber(String shortName) {
        if (shortName.endsWith("Cheque")) {
            return 0;
        } else if (shortName.endsWith("Credit")) {
            return 1;
        } else if (shortName.endsWith("EFT")) {
            return 2;
        } else if (shortName.endsWith("Cash")) {
            return 3;
        } else {
            return 4;
        }
    }

}
