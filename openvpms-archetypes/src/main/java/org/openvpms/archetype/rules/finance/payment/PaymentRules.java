/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.payment;

import org.openvpms.archetype.rules.act.ActStatusHelper;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT_EFT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND_EFT;
import static org.openvpms.component.model.bean.Policies.any;
import static org.openvpms.component.model.bean.Predicates.targetIsA;

/**
 * Payment rules.
 * <p/>
 * The methods of this class aren't designed to be invoked directly, but be triggered by rules.
 *
 * @author Tim Anderson
 */
public class PaymentRules {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link PaymentRules}.
     *
     * @param service the archetype service
     */
    public PaymentRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Invoked prior to a payment or refund being removed.
     * <p/>
     * Delegates to {@link #removeEFTItem(FinancialAct)} for each EFT item.
     * <p/>
     * This method is invoked by:
     * <ul>
     *     <li>archetypeService.remove.act.customerAccountPayment.before</li>
     *     <li>archetypeService.remove.act.customerAccountRefund.before</li>
     * </ul>
     *
     * @param act the payment or refund
     * @throws IllegalStateException if the act has been previously saved POSTED
     */
    public void removePaymentRefund(FinancialAct act) {
        if (!act.isA(PAYMENT, REFUND)) {
            throw new IllegalArgumentException("Invalid argument 'act'");
        }
        if (ActStatusHelper.isPosted(act, service)) {
            throw new IllegalStateException("Act is POSTED");
        }
        IMObjectBean bean = service.getBean(act);
        for (FinancialAct item : bean.getTargets("items", FinancialAct.class,
                                                 any(targetIsA(PAYMENT_EFT, REFUND_EFT)))) {
            removeEFTItem(item);
        }
    }

    /**
     * Invoked prior to an EFT item being removed.
     * <p/>
     * Removes any associated NO_TERMINAL EFTPOS transactions.
     * <p/>
     * This method is invoked by:
     * <ul>
     *     <li>archetypeService.remove.act.customerAccountPaymentEFT.before</li>
     *     <li>archetypeService.remove.act.customerAccountRefundEFT.before</li>
     * </ul>
     *
     * @param item the EFT item
     */
    public void removeEFTItem(FinancialAct item) {
        if (!item.isA(PAYMENT_EFT, REFUND_EFT)) {
            throw new IllegalArgumentException("Invalid argument 'act'");
        }
        IMObjectBean bean = service.getBean(item);
        for (Act act : bean.getTargets("eft", Act.class)) {
            if (act.getStatus().equals(EFTPOSTransactionStatus.NO_TERMINAL)) {
                bean.removeTargets("eft", act, "transaction");
                service.save(item);
                service.remove(act);
            }
        }
    }
}
