/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A handler for image documents.
 * <p/>
 * This supports .png, .jpg, .gif, and .bmp images.
 *
 * @author Tim Anderson
 */
public class SupportedImageDocumentHandler extends SupportedContentDocumentHandler {

    /**
     * Supported file extensions.
     */
    private static final String[] EXTENSIONS = {"png", "jpg", "jpeg", "gif", "bmp"};

    /**
     * Supported mime types.
     */
    private static final String[] MIME_TYPES = {"image/png", "image/x-png", "image/jpeg", "image/gif", "image/bmp"};

    /**
     * Constructs an {@link SupportedImageDocumentHandler}.
     *
     * @param service the archetype service
     */
    public SupportedImageDocumentHandler(ArchetypeService service) {
        super(EXTENSIONS, MIME_TYPES, DocumentArchetypes.IMAGE_DOCUMENT, new ImageDocumentHandler(service));
    }

    /**
     * Creates a new {@link Document} from a byte array.
     * <p/>
     * This isn't supported as the content is expected to be compressed. This operation should not be public. TODO
     *
     * @param name     the document name. Any path information is removed.
     * @param content  the serialized content
     * @param mimeType the mime type of the content. May be {@code null}
     * @param size     the uncompressed document size
     * @return a new document
     * @throws UnsupportedOperationException if invoked
     */
    @Override
    public Document create(String name, byte[] content, String mimeType, int size) {
        throw new UnsupportedOperationException();
    }
}