/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.prefs;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

import static org.openvpms.archetype.rules.prefs.PreferencesImpl.TARGET;
import static org.openvpms.archetype.rules.prefs.PreferencesImpl.USER;

/**
 * Preference manager.
 *
 * @author Tim Anderson
 */
class PreferenceManager {

    /**
     * Default constructor.
     */
    private PreferenceManager() {
        // no-op
    }

    /**
     * Returns preferences for a party.
     * <p>
     * If the party has no preferences, and a {@code source} party is specified, the
     * <p>
     * Changes will not be made persistent.
     *
     * @param party   the party
     * @param source  the party to use for default preferences. May be {@code null}
     * @param service the archetype service
     * @return the preferences
     */
    public static Preferences getPreferences(Reference party, Reference source, IArchetypeService service) {
        Entity prefs = getEntity(party, service);
        if (prefs == null && source != null) {
            prefs = getEntity(source, service);
        }
        if (prefs == null) {
            prefs = service.create(PreferenceArchetypes.PREFERENCES, Entity.class);
        }
        return new PreferencesImpl(party, source, prefs, service);
    }


    /**
     * Returns the root preference entity for a user or practice, creating and saving one if none exists.
     *
     * @param party              the party
     * @param source             if non-null, specifies the source to copy preferences from if the party has none
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @return the root preference entity
     */
    public static Entity getPreferences(Reference party, Reference source, IArchetypeService service,
                                        PlatformTransactionManager transactionManager) {
        Entity result;
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        result = template.execute(status -> {
            Entity prefs = getEntity(party, service);
            if (prefs == null && source != null) {
                Entity sourcePrefs = getEntity(source, service);
                if (sourcePrefs != null) {
                    prefs = copy(party, sourcePrefs, service);
                }
            }
            if (prefs == null) {
                prefs = service.create(PreferenceArchetypes.PREFERENCES, Entity.class);
                IMObjectBean bean = service.getBean(prefs);
                bean.addTarget(USER, party);
                bean.save();
            }
            return prefs;
        });
        return result;
    }

    /**
     * Copies preferences.
     *
     * @param party   the party to link the copied preferences to
     * @param prefs   the preferences to copy
     * @param service the archetype service
     * @return the copied preferences entity
     */
    protected static Entity copy(Reference party, Entity prefs, IArchetypeService service) {
        List<IMObject> objects = PreferencesCopier.copy(prefs, party, service);
        service.save(objects);
        return (Entity) objects.get(0);
    }

    /**
     * Returns the root preference entity for a party.
     *
     * @param party   the party
     * @param service the archetype service
     * @return the entity, or {@code null} if it doesn't exist
     */
    protected static Entity getEntity(Reference party, IArchetypeService service) {
        Entity prefs = null;
        final ArchetypeQuery query = new ArchetypeQuery(PreferenceArchetypes.PREFERENCES);
        query.add(Constraints.join(USER).add(Constraints.eq(TARGET, party)));
        query.add(Constraints.sort("id"));
        query.setMaxResults(1);
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(service, query);
        if (iterator.hasNext()) {
            prefs = iterator.next();
        }
        return prefs;
    }

}
