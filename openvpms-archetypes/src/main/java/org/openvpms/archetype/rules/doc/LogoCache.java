/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.MonitoringIMObjectCache;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.openvpms.archetype.rules.doc.DocumentArchetypes.LETTERHEAD;
import static org.openvpms.archetype.rules.doc.DocumentArchetypes.LOGO_ACT;
import static org.openvpms.archetype.rules.practice.PracticeArchetypes.LOCATION;
import static org.openvpms.archetype.rules.practice.PracticeArchetypes.PRACTICE;

/**
 * Caches logos.
 * <p/>
 * These are <em>act.documentLogo</em> instances.
 * <p/>
 * Logos are pre-cached for active practice, practice location and letterhead instances.
 */
class LogoCache extends MonitoringIMObjectCache<DocumentAct> {

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The logo acts, keyed on their entity references.
     */
    private final Map<Reference, DocumentAct> logos = Collections.synchronizedMap(new HashMap<>());

    /**
     * Constructs a {@link LogoCache}.
     *
     * @param service the archetype service
     * @param rules   the document rules
     * @param load    if {@code true}, preload the cache
     */
    public LogoCache(IArchetypeService service, DocumentRules rules, boolean load) {
        super(service, LOGO_ACT, DocumentAct.class, false);
        this.rules = rules;
        if (load) {
            load();
        }
    }

    /**
     * Returns the logo act for an entity.
     *
     * @param entity the entity
     * @return the logo act, or {@code null} if none is found
     */
    public DocumentAct getLogo(Entity entity) {
        DocumentAct act = logos.get(entity.getObjectReference());
        if (act == null) {
            // not cached. See if it can be loaded from the database, as logos for inactive entities aren't cached
            // initially.
            act = load(entity);
        }
        return act;
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     */
    @Override
    public void destroy() {
        super.destroy();
        logos.clear();
    }

    /**
     * Loads objects from the archetype service.
     * <p/>
     * This caches those <em>act.documentLogo</em> instances for the active practice, practice location and letterhead
     * instances.
     * <p/>
     * If there are multiple logos for an entity, the one with the lowest id will be cached.
     */
    @Override
    protected void load() {
        IArchetypeService service = getService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> from = query.from(DocumentAct.class, LOGO_ACT);
        Join<IMObject, IMObject> owner = from.join("owner").join("entity", PRACTICE, LOCATION, LETTERHEAD);
        owner.on(builder.equal(owner.get("active"), true));
        query.orderBy(builder.desc(from.get("id")));
        for (DocumentAct act : service.createQuery(query).getResultList()) {
            addObject(act);
        }
    }

    /**
     * Loads the cache with the logo for an entity, if present.
     * <p/>
     * If multiple logos are present, this returns the one with the lowest id.
     *
     * @param entity the entity
     * @return the corresponding logo, or {@code null} if none is found
     */
    protected DocumentAct load(Entity entity) {
        DocumentAct result = null;
        DocumentAct act = rules.getLogo(entity);
        if (act != null) {
            result = addObject(act);
        }
        return result;
    }

    /**
     * Invoked when an object is added to the cache.
     *
     * @param object the added object
     */
    @Override
    protected void added(DocumentAct object) {
        Reference owner = getOwner(object);
        if (owner != null) {
            synchronized (logos) {
                DocumentAct existing = logos.get(owner);
                if (existing == null || (object.getId() <= existing.getId())) {
                    // cache if there is no existing logo for the entity, or the logo has the same id or lower than
                    // the existing one
                    logos.put(owner, object);
                }
            }
        }
    }

    /**
     * Invoked when an object is removed from the cache.
     *
     * @param object the removed object
     */
    @Override
    protected void removed(DocumentAct object) {
        Reference owner = getOwner(object);
        if (owner != null) {
            logos.remove(owner, object);
        }
    }

    /**
     * Returns the owner for an entity.
     *
     * @param object the logo act
     * @return the owner reference, or {@code null} if none is found
     */
    private Reference getOwner(DocumentAct object) {
        IMObjectBean bean = getService().getBean(object);
        return bean.getTargetRef("owner");
    }
}
