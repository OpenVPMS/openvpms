/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.insurance;

import org.apache.commons.collections4.IteratorUtils;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.openvpms.component.system.common.query.ParticipationConstraint;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static org.openvpms.component.system.common.query.Constraints.and;
import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.in;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.not;
import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.ActShortName;

/**
 * Insurance rules.
 *
 * @author Tim Anderson
 */
public class InsuranceRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Customer node name.
     */
    private static final String CUSTOMER = "customer";

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Insurer node name.
     */
    private static final String INSURER = "insurer";

    /**
     * Insurer id node name.
     */
    private static final String INSURER_ID = "insurerId";

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * End time node name.
     */
    private static final String END_TIME = "endTime";

    /**
     * Act status node name.
     */
    private static final String STATUS = "status";

    /**
     * Claims node name.
     */
    private static final String CLAIMS = "claims";

    /**
     * Relationship source node name.
     */
    private static final String SOURCE = "source";

    /**
     * Relationship target node name.
     */
    private static final String TARGET = "target";

    /**
     * Items node name.
     */
    private static final String ITEMS = "items";

    /**
     * Entity node name.
     */
    private static final String ENTITY = "entity";

    /**
     * Constructs an {@link InsuranceRules}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public InsuranceRules(IArchetypeService service, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Creates a new insurance policy.
     * <p>
     * The caller is responsible for saving it.
     *
     * @param customer     the customer
     * @param patient      the patient
     * @param insurer      the insurer
     * @param policyNumber the policy number. May be {@code null}
     * @return a new policy
     */
    public Act createPolicy(Party customer, Party patient, Party insurer, String policyNumber) {
        Act policy = service.create(InsuranceArchetypes.POLICY, Act.class);
        IMObjectBean bean = service.getBean(policy);
        bean.setTarget(CUSTOMER, customer);
        bean.setTarget(PATIENT, patient);
        bean.setTarget(INSURER, insurer);
        if (policyNumber != null) {
            ActIdentity identity = service.create(InsuranceArchetypes.POLICY_IDENTITY, ActIdentity.class);
            identity.setIdentity(policyNumber);
            policy.addIdentity(identity);
        }
        return policy;
    }

    /**
     * Returns a policy to be associated with a claim.
     * <p>
     * This is designed to be used when associating policies with claims to limit the number of policies a patient has.
     * <ul>
     * <li>if there is already a policy with the same customer, patient, insurer and policy number, this is
     * returned; else</li>
     * <li>if an existing policy is supplied for the customer and patient, and has no claims associated with it,
     * this will be returned with the insurer and policy number set to that supplied; else</li>
     * <li>if there is a policy for the customer and patient, without any claims associated with it, this will
     * be returned with the insurer and policy number set to that supplied; else</li>
     * <li>a new policy will be created</li>
     * </ul>
     * Any policy returned will have its expiry date set to {@code null}.<br/>
     * Any new policy must be saved by the caller.
     *
     * @param customer       the customer
     * @param patient        the patient
     * @param insurer        the insurer
     * @param policyNumber   the policy number
     * @param existingClaim  if specified, ignore this claim when determining if a policy can be re-used
     * @param existingPolicy an existing policy. May be {@code null}
     * @return the policy
     */
    public Act getPolicyForClaim(Party customer, Party patient, Party insurer, String policyNumber,
                                 FinancialAct existingClaim, Act existingPolicy) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        return template.execute(transactionStatus -> {
            Act result = getPolicy(customer, patient, insurer, policyNumber);
            if (result == null) {
                // no match on customer, patient, insurer and policy number
                if (existingPolicy != null) {
                    result = tryReusePolicy(existingPolicy, customer, patient, insurer, policyNumber, existingClaim);
                }
                if (result == null) {
                    // see if there is an existing policy for the customer and patient
                    Act policy = getPolicy(customer, patient);
                    if (policy != null) {
                        result = tryReusePolicy(policy, insurer, policyNumber, existingClaim);
                    }
                    if (result == null) {
                        // need to create a new policy
                        result = createPolicy(customer, patient, insurer, policyNumber);
                    }
                }
            }
            result.setActivityEndTime(null);
            return result;
        });
    }

    /**
     * Returns the current policy for a patient, or the most recent, if there is no current policy.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return the policy for the patient, or {@code null} if there is none
     */
    public Act getPolicy(Party customer, Party patient) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        return template.execute(transactionStatus -> {
            Act policy = getCurrentPolicy(customer, patient);
            if (policy == null) {
                policy = getMostRecentPolicy(customer, patient, false);
            }
            return policy;
        });
    }

    /**
     * Returns the most recent policy for a patient.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param insurer  the insurer
     * @return the policy. May be {@code null}
     */
    public Act getPolicy(Party customer, Party patient, Party insurer) {
        ArchetypeQuery query = createPolicyQuery(customer, patient, insurer);
        return query(query);
    }

    /**
     * Returns a policy.
     *
     * @param customer     the customer
     * @param patient      the patient
     * @param insurer      the insurer
     * @param policyNumber the policy number. May be {@code null}
     * @return the corresponding policy, or {@code null} if none is found
     */
    public Act getPolicy(Party customer, Party patient, Party insurer, String policyNumber) {
        ArchetypeQuery query = createPolicyQuery(customer, patient, insurer);
        query.add(join(INSURER_ID).add(eq("identity", policyNumber)));
        return query(query);
    }

    /**
     * Returns the current policy for a patient.
     * <p>
     * This is the policy overlapping the current time.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return the current policy for the patient, or {@code null} if there is none
     */
    public Act getCurrentPolicy(Party customer, Party patient) {
        Date now = new Date();
        ArchetypeQuery query = createPolicyQuery(customer, patient);
        query.add(and(Constraints.lte(START_TIME, now),
                      Constraints.or(Constraints.isNull(END_TIME), Constraints.gt(END_TIME, now))));
        return query(query);
    }

    /**
     * Returns the current policies for a patient.
     * <p>
     * These are the policies overlapping the current time.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return the current policies for the patient, most recent first, if any
     */
    public List<Act> getCurrentPolicies(Party customer, Party patient) {
        Date now = new Date();
        ArchetypeQuery query = createPolicyQuery(customer, patient);
        query.add(and(Constraints.lte(START_TIME, now),
                      Constraints.or(Constraints.isNull(END_TIME), Constraints.gt(END_TIME, now))));
        IMObjectQueryIterator<Act> iterator = new IMObjectQueryIterator<>(service, query);
        return IteratorUtils.toList(iterator);
    }

    /**
     * Returns the policy for a patient, with the greatest <em>startTime</em>.
     *
     * @param customer              the customer
     * @param patient               the patient
     * @param excludeFuturePolicies if {@code true}, exclude future dated policies
     * @return the most recent policy, or {@code null} if there is none
     */
    public Act getMostRecentPolicy(Party customer, Party patient, boolean excludeFuturePolicies) {
        Date now = new Date();
        ArchetypeQuery query = createPolicyQuery(customer, patient);
        if (excludeFuturePolicies) {
            query.add(Constraints.lte(START_TIME, now));
        }
        return query(query);
    }

    /**
     * Returns the insurer associated with a policy or claim.
     *
     * @param act the policy or claim
     * @return the insurer, or {@code null} if the insurer cannot be found
     */
    public Party getInsurer(Act act) {
        Party insurer = null;
        IMObjectBean bean = service.getBean(act);
        if (bean.isA(InsuranceArchetypes.POLICY)) {
            insurer = bean.getTarget(INSURER, Party.class);
        } else {
            Act policy = bean.getTarget("policy", Act.class);
            if (policy != null) {
                insurer = getInsurer(policy);
            }
        }
        return insurer;
    }

    /**
     * Creates a new claim, linked to a policy.
     *
     * @param policy the policy
     * @return a new claim
     */
    public FinancialAct createClaim(Act policy) {
        FinancialAct claim = service.create(InsuranceArchetypes.CLAIM, FinancialAct.class);
        if (claim == null) {
            throw new IllegalStateException("Failed to create " + InsuranceArchetypes.CLAIM);
        }
        IMObjectBean bean = service.getBean(claim);
        bean.addTarget("policy", policy, CLAIMS);
        return claim;
    }

    /**
     * Determines if a policy's policy number can be changed.
     * <p>
     * A policy number can be changed if it has no claims with statuses other than {@code PENDING} or {@code POSTED}.
     *
     * @param policy the policy
     * @return {@code true} if the policy number can be changed, otherwise {@code false}
     */
    public boolean canChangePolicyNumber(Act policy) {
        ArchetypeQuery query = new ArchetypeQuery(policy.getObjectReference());
        query.add(join(CLAIMS).add(join(SOURCE).add(not(in(STATUS, ClaimStatus.PENDING, ClaimStatus.POSTED)))));
        query.add(new NodeSelectConstraint("id"));
        query.setMaxResults(1);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        return !iterator.hasNext();
    }

    /**
     * Returns the policy number of a policy.
     *
     * @param policy the policy
     * @return the policy number. May be {@code null}
     */
    public String getPolicyNumber(Act policy) {
        IMObjectBean bean = service.getBean(policy);
        Identity insurerId = bean.getObject(INSURER_ID, Identity.class);
        return (insurerId != null) ? insurerId.getIdentity() : null;
    }

    /**
     * Determines if an invoice is fully or partially claimed.
     * <p/>
     * This excludes claims with CANCELLED status.
     *
     * @param invoice the invoice
     * @return {@code true} if the invoice is claimed
     */
    public boolean isClaimed(FinancialAct invoice) {
        ArchetypeQuery query = new ArchetypeQuery(InsuranceArchetypes.CLAIM);
        query.setMaxResults(1);
        query.setDistinct(true);
        query.add(Constraints.ne(STATUS, ClaimStatus.CANCELLED));
        query.add(join(ITEMS, "conditions").add(
                join(TARGET, "condition").add(
                        join(ITEMS, "charges").add(
                                join(TARGET, "item").add(
                                        join("invoice").add(eq(SOURCE, invoice)))))));
        Iterator<FinancialAct> iterator = new IMObjectQueryIterator<>(service, query);
        return iterator.hasNext();
    }

    /**
     * Returns the current claims for an invoice.
     * <p>
     * These are those that aren't CANCELLED, SETTLED or DECLINED.
     *
     * @param invoice the invoice
     * @return the claims
     */
    @SuppressWarnings("unchecked")
    public List<FinancialAct> getCurrentClaims(FinancialAct invoice) {
        List<FinancialAct> result;
        ArchetypeQuery query = new ArchetypeQuery(InsuranceArchetypes.CLAIM);
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        query.setDistinct(true);
        query.add(Constraints.and(Constraints.ne(STATUS, ClaimStatus.CANCELLED),
                                  Constraints.ne(STATUS, ClaimStatus.SETTLED),
                                  Constraints.ne(STATUS, ClaimStatus.DECLINED)));
        query.add(join(ITEMS, "conditions").add(
                join(TARGET, "condition").add(
                        join(ITEMS, "charges").add(
                                join(TARGET, "item").add(
                                        join("invoice").add(eq(SOURCE, invoice)))))));
        result = (List) service.get(query).getResults();
        return result;
    }

    /**
     * Determines if an invoice has a gap claim that is yet to be paid, cancelled or declined.
     *
     * @param invoice the invoice
     * @return any current gap claims that the invoice is part of
     */
    public List<FinancialAct> getCurrentGapClaims(FinancialAct invoice) {
        List<FinancialAct> result = new ArrayList<>();
        for (FinancialAct claim : getCurrentClaims(invoice)) {
            if (isUnpaidGapClaim(claim)) {
                result.add(claim);
            }
        }
        return result;
    }

    /**
     * Determines if an invoice has a gap claim that is yet to be paid, cancelled or declined.
     *
     * @param invoice the invoice
     * @return {@code true} if an invoice has a gap claim that is yet to be paid, cancelled or declined
     */
    public boolean hasCurrentGapClaims(FinancialAct invoice) {
        boolean result = false;
        for (FinancialAct claim : getCurrentClaims(invoice)) {
            if (isUnpaidGapClaim(claim)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Determines if a gap claim has a benefit amount.
     * <p/>
     * A gap claim has a benefit amount (which may zero), if the <em>status2</em> node is non-null and not PENDING.
     *
     * @param claim the claim
     * @return {@code true} if the claim has a benefit amount
     */
    public boolean hasBenefitAmount(FinancialAct claim) {
        return (claim.getStatus2() != null && !ClaimStatus.GAP_CLAIM_PENDING.equals(claim.getStatus2()))
               && service.getBean(claim).getBigDecimal("benefitAmount") != null;
    }

    /**
     * Calculates the gap for a customer to pay in a gap claim.
     * <p/>
     * This is the difference between the claim total and the benefit amount.
     *
     * @param claim the gap claim
     * @return the gap amount
     */
    public BigDecimal getGapAmount(FinancialAct claim) {
        IMObjectBean bean = service.getBean(claim);
        BigDecimal total = bean.getBigDecimal("amount", BigDecimal.ZERO);
        BigDecimal benefit = bean.getBigDecimal("benefitAmount", BigDecimal.ZERO);
        return getGapAmount(total, benefit);
    }

    /**
     * Calculates the gap for a customer to pay in a gap claim.
     * <p/>
     * This is the difference between the claim total and the benefit amount.
     *
     * @param total   the claim total
     * @param benefit the benefit amount
     * @return the gap amount
     */
    public BigDecimal getGapAmount(BigDecimal total, BigDecimal benefit) {
        return (benefit.compareTo(total) <= 0) ? total.subtract(benefit) : BigDecimal.ZERO;
    }

    /**
     * Finds the most recent treatment end date for a patient claim submitted to the insurance service of an insurer.
     * <p/>
     * This can be used to avoid sending redundant patient history to an insurance service, if they have already been
     * sent history in prior claim(s).
     *
     * @param patient      the patient
     * @param insurer      the insurer
     * @param excludeClaim the claim to exclude from the query. May be {@code null}
     * @return the most recent treatment end date, or {@code null} if no claim has been submitted
     */
    public Date getMostRecentTreatmentDateForService(Party patient, Party insurer, Act excludeClaim) {
        Date result = null;
        Entity insuranceService = service.getBean(insurer).getTarget("service", Entity.class, Policies.active());
        if (insuranceService != null) {
            Act claim = getMostRecentClaimForPatient(patient, insuranceService, excludeClaim);
            if (claim != null) {
                List<Act> items = service.getBean(claim).getTargets("items", Act.class);
                result = items.stream().map(Act::getActivityEndTime).max(Comparator.naturalOrder()).orElse(null);
            }
        }
        return result;
    }

    /**
     * Determines if a gap claim has been paid.
     *
     * @param claim the gap claim
     * @return {@code true} if the gap claim has been paid, otherwise {@code false}
     */
    protected boolean isPaid(FinancialAct claim) {
        String status = claim.getStatus2();
        return ClaimStatus.GAP_CLAIM_PAID.equals(status) || ClaimStatus.GAP_CLAIM_NOTIFIED.equals(status);
    }

    /**
     * Creates a policy query for a customer, patient, and insurer.
     * <p>
     * This returns the most recent policy first.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param insurer  the insurer
     * @return the query
     */
    protected ArchetypeQuery createPolicyQuery(Party customer, Party patient, Party insurer) {
        ArchetypeQuery query = createPolicyQuery(customer, patient);
        query.add(join(INSURER).add(eq(ENTITY, insurer)).add(
                new ParticipationConstraint(ActShortName, InsuranceArchetypes.POLICY)));
        return query;
    }

    /**
     * Attempts to reuse an existing policy.
     * <p/>
     * Can only re-use if it is for the same customer and patient, and has no other claims.
     *
     * @param policy        an existing policy
     * @param customer      the customer
     * @param patient       the patient
     * @param insurer       the insurer
     * @param policyNumber  the policy number
     * @param existingClaim if specified, ignore this claim when determining if a policy can be re-used
     * @return the policy, or {@code null} if it cannot be reused
     */
    private Act tryReusePolicy(Act policy, Party customer, Party patient, Party insurer, String policyNumber,
                               FinancialAct existingClaim) {
        Act result = null;
        IMObjectBean bean = service.getBean(policy);
        if (canReusePolicy(bean, existingClaim, customer, patient)) {
            setPolicyNumber(policy, policyNumber, bean);
            bean.setTarget(INSURER, insurer);
            result = policy;
        }
        return result;
    }

    /**
     * Tries to reuse a policy.
     * <p/>
     * Can only re-use if it has no other claims.
     *
     * @param policy        an existing policy
     * @param insurer       the insurer
     * @param policyNumber  the policy number
     * @param existingClaim if specified, ignore this claim when determining if a policy can be re-used
     * @return the policy, or {@code null} if it cannot be reused
     */
    private Act tryReusePolicy(Act policy, Party insurer, String policyNumber, FinancialAct existingClaim) {
        Act result = null;
        IMObjectBean bean = service.getBean(policy);
        if (!hasClaims(bean, existingClaim)) {
            setPolicyNumber(policy, policyNumber, bean);
            bean.setTarget(INSURER, insurer);
            result = policy;
        }
        return result;
    }


    /**
     * Determines if a policy can be reused.
     *
     * @param bean     the policy bean
     * @param claim    if non-null, ignore this claim when performing the check
     * @param customer the customer
     * @param patient  the patient
     * @return {@code true} if the policy can be reused
     */
    private boolean canReusePolicy(IMObjectBean bean, FinancialAct claim, Party customer, Party patient) {
        return (bean.getTargetRef(PATIENT).equals(patient.getObjectReference())
                && bean.getTargetRef(CUSTOMER).equals(customer.getObjectReference())
                && !hasClaims(bean, claim));
    }

    /**
     * Determines if a claim is an unpaid gap claim.
     *
     * @param claim the claim
     * @return {@code true} if the claim is an unpaid gap claim
     */
    private boolean isUnpaidGapClaim(FinancialAct claim) {
        IMObjectBean bean = service.getBean(claim);
        return bean.getBoolean("gapClaim") && !isPaid(claim);
    }

    /**
     * Determines if a policy has any claims.
     *
     * @param bean  the policy
     * @param claim if non-null, ignore this claim when performing the check
     * @return {@code true} if the policy has claims, otherwise {@code false}
     */
    private boolean hasClaims(IMObjectBean bean, Act claim) {
        boolean result;
        if (claim == null) {
            result = !bean.getValues(CLAIMS).isEmpty();
        } else {
            Reference source = claim.getObjectReference();
            Predicate<Relationship> predicate = relationship -> !Objects.equals(source, relationship.getSource());
            result = bean.getValue(CLAIMS, Relationship.class, predicate) != null;
        }
        return result;
    }

    /**
     * Updates a policy's policy number.
     *
     * @param policy       the policy
     * @param policyNumber the policy number. May be {@code null}
     * @param bean         the bean wrapping the policy
     */
    private void setPolicyNumber(Act policy, String policyNumber, IMObjectBean bean) {
        ActIdentity identity = bean.getObject(INSURER_ID, ActIdentity.class);
        if (policyNumber != null) {
            if (identity == null) {
                identity = service.create(InsuranceArchetypes.POLICY_IDENTITY, ActIdentity.class);
                policy.addIdentity(identity);
            }
            identity.setIdentity(policyNumber);
        } else if (identity != null) {
            policy.removeIdentity(identity);
        }
    }

    /**
     * Returns the most recent SUBMITTED, ACCEPTED, DECLINED or SETTLED claim for a patient, submitted to a particular
     * insurance service.
     *
     * @param patient          the patient
     * @param insuranceService the insurance service
     * @param excludeClaim     the claim to exclude from the query. May be {@code null}
     * @return the most recent claim, or {@code null} if there is none
     */
    private Act getMostRecentClaimForPatient(Party patient, Entity insuranceService, Act excludeClaim) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, InsuranceArchetypes.CLAIM);
        query.select(from);
        Join<IMObject, IMObject> withPatient = from.join("patient").join("entity");
        withPatient.on(builder.equal(withPatient.reference(), patient.getObjectReference()));
        Join<IMObject, IMObject> withService = from.join("policy").join("target")
                .join("insurer").join("entity").join("service").join("target");
        withService.on(builder.equal(withService.reference(), insuranceService.getObjectReference()));
        List<javax.persistence.criteria.Predicate> where = new ArrayList<>();
        where.add(from.get("status").in(ClaimStatus.SUBMITTED, ClaimStatus.ACCEPTED, ClaimStatus.SETTLED,
                                        ClaimStatus.DECLINED));
        if (excludeClaim != null) {
            where.add(builder.notEqual(from.reference(), excludeClaim.getObjectReference()));
        }
        query.where(where);
        query.orderBy(builder.desc(from.get("startTime")), builder.desc(from.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Helper to return the first result of a query.
     *
     * @param query the query
     * @return the result or {@code null} if none is found
     */
    private Act query(ArchetypeQuery query) {
        query.setMaxResults(1);
        IMObjectQueryIterator<Act> iterator = new IMObjectQueryIterator<>(service, query);
        return iterator.hasNext() ? iterator.next() : null;
    }

    /**
     * Creates a policy query for a customer and patient.
     * <p>
     * This returns the most recent policy first.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return the query
     */
    private ArchetypeQuery createPolicyQuery(Party customer, Party patient) {
        ArchetypeQuery query = new ArchetypeQuery(Constraints.shortName("act", InsuranceArchetypes.POLICY));
        query.setMaxResults(1);
        query.add(join(CUSTOMER).add(eq(ENTITY, customer)).add(
                new ParticipationConstraint(ActShortName, InsuranceArchetypes.POLICY)));
        query.add(join(PATIENT).add(eq(ENTITY, patient)).add(
                new ParticipationConstraint(ActShortName, InsuranceArchetypes.POLICY)));

        // if there are multiple policies, pick the most recent
        query.add(Constraints.sort(START_TIME, false));
        query.add(Constraints.sort("id", false));
        return query;
    }

}
