/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient;

import org.openvpms.archetype.rules.act.ActStatus;

/**
 * Order status types for investigation acts.
 *
 * @author Tim Anderson
 */
public class InvestigationActStatus {

    /**
     * Investigation in progress status.
     */
    public static final String IN_PROGRESS = ActStatus.IN_PROGRESS;

    /**
     * Investigation cancelled status.
     */
    public static final String CANCELLED = ActStatus.CANCELLED;

    /**
     * Investigation posted status.
     */
    public static final String POSTED = ActStatus.POSTED;

    /**
     * Order pending status.
     */
    public static final String PENDING = "PENDING";

    /**
     * Confirm order status.
     */
    public static final String CONFIRM = "CONFIRM";

    /**
     * Confirm order deferred status.
     */
    public static final String CONFIRM_DEFERRED = "CONFIRM_DEFERRED";

    /**
     * Order sent status.
     */
    public static final String SENT = "SENT";

    /**
     * Order error status.
     */
    public static final String ERROR = "ERROR";

    /**
     * Waiting for sample status.
     */
    public static final String WAITING_FOR_SAMPLE = "WAITING_FOR_SAMPLE";

    /**
     * Received partial results status.
     */
    public static final String PARTIAL_RESULTS = "PARTIAL_RESULTS";

    /**
     * Results received status.
     */
    public static final String RECEIVED = "RECEIVED";

    /**
     * Results reviewed status.
     */
    public static final String REVIEWED = "REVIEWED";

    /**
     * Default constructor.
     */
    private InvestigationActStatus() {

    }
}
