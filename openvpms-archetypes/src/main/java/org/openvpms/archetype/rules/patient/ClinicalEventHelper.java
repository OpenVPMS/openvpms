/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.List;

/**
 * .
 *
 * @author Tim Anderson
 */
class ClinicalEventHelper {

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * End time node name.
     */
    private static final String END_TIME = "endTime";

    /**
     * Returns an <em>act.patientClinicalEvent<em> for the specified patient
     * reference and date.
     * NOTES:
     * <ul>
     * <li>this method will return the closest matching event for the specified date, ignoring any time component if
     * there is no exact match.</li>
     * <li>if two events have the same timestamp, the event with the smaller id will be returned</li>
     * </ul>
     *
     * @param patient the patient reference
     * @param date    the date
     * @return the corresponding <em>act.patientClinicalEvent</em> or {@code null} if none is found. The event may be
     * <em>IN_PROGRESS</em> or <em>COMPLETED}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static Act getEvent(Reference patient, Date date, ArchetypeService service) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, PatientArchetypes.CLINICAL_EVENT);
        Join<IMObject, IMObject> join = from.join("patient").join("entity");
        join.on(builder.equal(join.reference(), patient));
        Date lowerBound = DateRules.getDate(date);
        Date upperBound = DateRules.getNextDate(lowerBound);
        query.where(builder.lessThan(from.get(START_TIME), upperBound),
                    builder.or(builder.greaterThanOrEqualTo(from.get(END_TIME), lowerBound),
                               from.get(END_TIME).isNull()));
        query.orderBy(builder.asc(from.get(START_TIME)), builder.asc(from.get("id")));
        List<Act> acts = service.createQuery(query).getResultList();
        Act result = null;
        long resultDistance = 0;
        for (Act event : acts) {
            if (result == null) {
                resultDistance = distance(date, event);
                result = event;
            } else {
                long distance = distance(date, event);
                if (distance < resultDistance) {
                    resultDistance = distance;
                    result = event;
                }
            }
        }
        return result;
    }

    /**
     * Creates a new <em>act.patientClinicalEvent</em> for the patient and start time.
     *
     * @param patient   the patient reference
     * @param startTime the event start time
     * @param clinician the clinician reference. May be {@code null}
     * @return a new event
     */
    public static Act createEvent(Reference patient, Date startTime, Reference clinician, ArchetypeService service) {
        Act event;
        event = service.create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        event.setActivityStartTime(startTime);
        IMObjectBean eventBean = service.getBean(event);
        eventBean.setTarget("patient", patient);
        if (clinician != null) {
            eventBean.setTarget("clinician", clinician);
        }
        return event;
    }

    /**
     * Calculates the distance of a date to an event.
     * <p/>
     * The distance is the minimum difference between the date and the event's start or end times, ignoring
     * seconds.
     *
     * @param date  the date
     * @param event the event
     * @return the minimum difference between the date and the event's start or end times, ignoring seconds.
     */
    public static long distance(Date date, Act event) {
        long dateSecs = getSeconds(date);          // truncate milliseconds are not stored in db
        long startTime = getSeconds(event.getActivityStartTime());
        long endTime = (event.getActivityEndTime() != null) ? getSeconds(event.getActivityEndTime()) : 0;
        long distStartTime = Math.abs(startTime - dateSecs);
        if (endTime != 0) {
            return distStartTime;
        }
        long distEndTime = Math.abs(endTime - dateSecs);
        return Math.min(distStartTime, distEndTime);
    }

    /**
     * Returns a date in seconds.
     *
     * @param date the date
     * @return the date in seconds
     */
    private static long getSeconds(Date date) {
        return date.getTime() / 1000;
    }
}