/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.joda.time.Period;
import org.openvpms.archetype.rules.act.ActCopyHandler;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopier;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopyHandler;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.IterableIMObjectQuery;
import org.openvpms.component.system.common.query.JoinConstraint;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.ObjectRefSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.openvpms.component.system.common.query.ParticipationConstraint;
import org.openvpms.component.system.common.query.RelationalOp;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.openvpms.archetype.rules.workflow.ScheduleArchetypes.ORGANISATION_SCHEDULE;
import static org.openvpms.component.system.common.query.Constraints.and;
import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.gt;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.lt;
import static org.openvpms.component.system.common.query.Constraints.shortName;
import static org.openvpms.component.system.common.query.Constraints.sort;
import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.ActShortName;
import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.EndTime;
import static org.openvpms.component.system.common.query.ParticipationConstraint.Field.StartTime;


/**
 * Appointment rules.
 *
 * @author Tim Anderson
 */
public class AppointmentRules {

    /**
     * Appointment reminder job configuration.
     */
    public static final String APPOINTMENT_REMINDER_JOB = "entity.jobAppointmentReminder";

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Length of a day in minutes.
     */
    private static final int DAY_IN_MINUTES = 24 * 60;

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Entity node name.
     */
    private static final String ENTITY = "entity";

    /**
     * Status node name.
     */
    private static final String STATUS = "status";

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";


    /**
     * Constructs an {@link AppointmentRules}.
     *
     * @param service the archetype service
     */
    public AppointmentRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the first view that contain the specified schedule for a given practice location.
     *
     * @param location the practice location
     * @param schedule the schedule
     * @return the view, or {@code null} if none is found
     */
    public Entity getScheduleView(Party location, Entity schedule) {
        IMObjectBean bean = service.getBean(location);
        Policy<SequencedRelationship> policy = Policies.newPolicy(SequencedRelationship.class)
                .orderBySequence()
                .active()
                .build();
        for (Entity view : bean.getTargets("scheduleViews", Entity.class, policy)) {
            IMObjectBean viewBean = service.getBean(view);
            if (viewBean.hasTarget("schedules", schedule)) {
                return view;
            }
        }
        return null;
    }

    /**
     * Returns the practice location associated with a schedule.
     *
     * @param schedule the schedule
     * @return the location, or {@code null} if none is found
     */
    public Party getLocation(Entity schedule) {
        IMObjectBean bean = service.getBean(schedule);
        return bean.getTarget("location", Party.class);
    }

    /**
     * Returns the schedule slot size in minutes.
     *
     * @param schedule the schedule
     * @return the schedule slot size in minutes
     * @throws OpenVPMSException for any error
     */
    public int getSlotSize(Entity schedule) {
        IMObjectBean bean = service.getBean(schedule);
        return getSlotSize(bean);
    }

    /**
     * Returns the default appointment type associated with a schedule.
     *
     * @param schedule the schedule
     * @return the default appointment type, or {@code null} if there is no default
     * @throws OpenVPMSException for any error
     */
    public Entity getDefaultAppointmentType(Entity schedule) {
        return EntityRelationshipHelper.getDefaultTarget(schedule, "appointmentTypes", false, service);
    }

    /**
     * Calculates an appointment end time, given the start time, schedule and
     * appointment type.
     *
     * @param startTime       the start time
     * @param schedule        an instance of <em>party.organisationSchedule</em>
     * @param appointmentType an instance of <em>entity.appointmentType</em>
     * @return the appointment end time
     * @throws OpenVPMSException for any error
     */
    public Date calculateEndTime(Date startTime, Entity schedule, Entity appointmentType) {
        IMObjectBean bean = service.getBean(schedule);
        int noSlots = getSlots(bean, appointmentType);
        int minutes = getSlotSize(bean) * noSlots;
        return DateRules.getDate(startTime, minutes, DateUnits.MINUTES);
    }

    /**
     * Updates any <em>act.customerTask</em> associated with an
     * <em>act.customerAppointment</em> to ensure that it has the same status
     * (where applicable).
     *
     * @param act the appointment
     * @throws OpenVPMSException for any error
     */
    public void updateTask(Act act) {
        IMObjectBean bean = service.getBean(act);
        List<Act> tasks = bean.getTargets("tasks", Act.class);
        if (!tasks.isEmpty()) {
            Act task = tasks.get(0);
            updateStatus(act, task);
        }
    }

    /**
     * Updates any <em>act.customerAppointment</em> associated with an
     * <em>act.customerTask</em> to ensure that it has the same status
     * (where applicable).
     *
     * @param act the task
     * @throws OpenVPMSException for any error
     */
    public void updateAppointment(Act act) {
        IMObjectBean bean = service.getBean(act);
        List<Act> appointments = bean.getSources("appointments", Act.class);
        if (!appointments.isEmpty()) {
            Act appointment = appointments.get(0);
            updateStatus(act, appointment);
        }
    }

    /**
     * Returns the active appointment for a patient. This is the most recent appointment that is not PENDING,
     * CONFIRMED or CANCELLED.
     * <p/>
     * Note that this may return a COMPLETED appointment.
     *
     * @param patient the patient
     * @return an active appointment, or {@code null} if none exists
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Act getActivePatientAppointment(Party patient) {
        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.APPOINTMENT, true, true);
        query.add(Constraints.join(PATIENT).add(Constraints.eq(ENTITY, patient.getObjectReference())));
        query.add(Constraints.not(Constraints.in(STATUS, AppointmentStatus.PENDING, AppointmentStatus.CONFIRMED,
                                                 AppointmentStatus.CANCELLED, AppointmentStatus.NO_SHOW)));
        query.add(new NodeSortConstraint(START_TIME, false));
        query.setMaxResults(1);
        IMObjectQueryIterator<Act> iter = new IMObjectQueryIterator<>(service, query);
        return (iter.hasNext()) ? iter.next() : null;
    }

    /**
     * Returns active appointments for a customer at a practice location, optionally excluding an appointment.
     * Active appointments are those that aren't PENDING, CONFIRMED, COMPLETED or CANCELLED.<br/>
     * NOTE: this is different to {@link #getActivePatientAppointment(Party)} which may return a COMPLETED appointment.
     *
     * @param customer the customer
     * @param location the practice location
     * @param exclude  the appointment to exclude. May be {@code null}
     * @return the active appointments
     */
    public Iterable<Act> getActiveCustomerAppointments(Party customer, Party location, Act exclude) {
        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.APPOINTMENT, true, true);
        query.add(join("customer").add(eq("entity", customer)));
        query.add(join("schedule").add(join("entity", shortName(ORGANISATION_SCHEDULE))
                                               .add(join("location").add(eq("target", location)))));
        if (exclude != null) {
            query.add(Constraints.ne("id", exclude.getId()));
        }
        query.add(Constraints.not(Constraints.in("status", AppointmentStatus.PENDING, AppointmentStatus.CONFIRMED,
                                                 AppointmentStatus.COMPLETED, AppointmentStatus.CANCELLED,
                                                 AppointmentStatus.NO_SHOW)));
        query.add(Constraints.sort("id"));
        query.setMaxResults(IArchetypeQuery.ALL_RESULTS);
        return new IterableIMObjectQuery<>(service, query);
    }

    /**
     * Copies an appointment, excluding any act relationships it may have.
     *
     * @param appointment the appointment to copy
     * @return a copy of the appointment
     */
    public Act copy(Act appointment) {
        IMObjectCopyHandler handler = new AppointmentCopyHandler();
        IMObjectCopier copier = new IMObjectCopier(handler, service);
        return (Act) copier.apply(appointment).get(0);
    }

    /**
     * Determines if automatic reminders are enabled for a schedule or appointment type.
     *
     * @param entity the schedule or appointment type. May be {@code null}
     * @return {@code true} if reminders are enabled
     */
    public boolean isRemindersEnabled(Entity entity) {
        if (entity != null) {
            IMObjectBean bean = service.getBean(entity);
            return bean.getBoolean("sendReminders");
        }
        return false;
    }

    /**
     * Returns the period from the current time when no appointment reminder should be sent.
     *
     * @return the period, or {@code null} if appointment reminders haven't been configured
     */
    public Period getNoReminderPeriod() {
        AppointmentReminderConfig config = getAppointmentReminderConfig();
        return (config != null) ? config.getNoReminderPeriod() : null;
    }

    /**
     * Returns the appointment reminder configuration.
     *
     * @return the appointment reminder configuration, or {@code null} if appointment reminders haven't been configured
     */
    public AppointmentReminderConfig getAppointmentReminderConfig() {
        AppointmentReminderConfig config = null;
        Entity object = getAppointmentReminderJob();
        if (object != null) {
            config = new AppointmentReminderConfig(object, service);
        }
        return config;
    }

    /**
     * Determines if an appointment is a boarding appointment.
     *
     * @param appointment the appointment
     * @return {@code true} if the appointment is a boarding appointment
     */
    public boolean isBoardingAppointment(Act appointment) {
        return BoardingHelper.isBoardingAppointment(appointment, service);
    }

    /**
     * Returns the number of days to charge boarding for an appointment.
     *
     * @param appointment the appointment
     * @return the number of days
     */
    public int getBoardingDays(Act appointment) {
        return getBoardingDays(appointment.getActivityStartTime(), appointment.getActivityEndTime());
    }

    /**
     * Returns the number of days to charge boarding for.
     *
     * @param startTime the boarding start time
     * @param endTime   the boarding end time
     * @return the number of days
     */
    public int getBoardingDays(Date startTime, Date endTime) {
        DateMidnight end = new DateMidnight(endTime);
        int days = Days.daysBetween(new DateMidnight(startTime), end).getDays();
        if (days < 0) {
            days = 0;
        } else if (DateRules.compareTo(endTime, end.toDate()) != 0) {
            // any time after midnight is another day
            days++;
        }
        return days;
    }

    /**
     * Returns the number of nights to charge boarding for.
     *
     * @param startTime the boarding start time
     * @param endTime   the boarding end time
     * @return the number of nights
     */
    public int getBoardingNights(Date startTime, Date endTime) {
        int days = getBoardingDays(startTime, endTime);
        return days > 1 ? days - 1 : days;
    }

    /**
     * Determines if the roster should be checked for an appointment.
     * <br/>
     * This is to avoid checking the clinician roster for appointments dated into the future where the roster is
     * unlikely to be determined.
     * <p/>
     * This uses the supplied location's <em>rosterCheckPeriod</em> to determine if the appointment needs to be checked.
     * <p/>
     * If the appointment start time is dated on or after today, and is prior to {@code now + rosterCheckPeriod},
     * then the roster should be checked to ensure that the clinician is available.
     *
     * @param appointment the appointment
     * @param location    the appointment's practice location
     * @return {@code true} if the roster should be checked, {@code false} if no check should be performed
     */
    public boolean checkRoster(Act appointment, Party location) {
        boolean check = false;
        Date startTime = appointment.getActivityStartTime();
        Date endTime = appointment.getActivityEndTime();
        if (startTime != null && endTime != null && DateRules.compareDateToToday(startTime) >= 0) {
            IMObjectBean bean = service.getBean(location);
            if (bean.getBoolean("rostering")) {
                Date now = new Date();
                Period period = PeriodHelper.getPeriod(bean, "rosterCheckPeriod");
                if (period != null && DateRules.compareTo(startTime, DateRules.plus(now, period)) < 0) {
                    check = true;
                }
            }
        }
        return check;
    }

    /**
     * Determines if a roster area has a schedule.
     *
     * @param rosterArea the roster area
     * @param schedule   the schedule
     * @return {@code true} if the roster area has the schedule, otherwise {@code false}
     */
    public boolean rosterAreaHasSchedule(Entity rosterArea, Entity schedule) {
        IMObjectBean bean = service.getBean(rosterArea);
        return bean.hasTarget("schedules", schedule);
    }

    /**
     * Returns the patient clinical event associated with an appointment.
     *
     * @param appointment the appointment
     * @return the event, or {@code null} if none exists
     */
    public Act getEvent(Act appointment) {
        IMObjectBean bean = service.getBean(appointment);
        return bean.getTarget("event", Act.class);
    }

    /**
     * Returns pending appointments for a customer.
     * <p/>
     * These are appointments with PENDING or CONFIRMED status.
     *
     * @param customer the customer
     * @param interval the interval, relative to the current date/time
     * @param units    the interval units
     * @return the PENDING or CONFIRMED appointments for the customer
     */
    public Iterable<Act> getPendingCustomerAppointments(Party customer, int interval, DateUnits units) {
        ArchetypeQuery query = createPendingAppointmentQuery(customer, "customer", interval, units);
        return new IterableIMObjectQuery<>(service, query);
    }

    /**
     * Returns pending appointments for a patient.
     * <p/>
     * These are appointments with PENDING or CONFIRMED status.
     *
     * @param patient  the patient
     * @param interval the interval, relative to the current date/time
     * @param units    the interval units
     * @return the PENDING or CONFIRMED appointments for the patient
     */
    public Iterable<Act> getPendingPatientAppointments(Party patient, int interval, DateUnits units) {
        ArchetypeQuery query = createPendingAppointmentQuery(patient, PATIENT, interval, units);
        return new IterableIMObjectQuery<>(service, query);
    }

    /**
     * Returns the next PENDING or CONFIRMED appointment for a patient, starting on or after the specified date.
     *
     * @param patient the patient
     * @param date    the date
     * @return the next appointment for the patient, or {@code null} if there is none
     */
    public Act getNextPatientAppointment(Party patient, Date date) {
        ArchetypeQuery query = createPendingAppointmentQuery(patient, PATIENT, date);
        query.setMaxResults(1);
        IMObjectQueryIterator<Act> iterator = new IMObjectQueryIterator<>(service, query);
        return iterator.hasNext() ? iterator.next() : null;
    }

    /**
     * Returns the minutes from midnight for the specified time, rounded up or down to the nearest slot.
     *
     * @param time     the time
     * @param slotSize the slot size
     * @param roundUp  if {@code true} round up to the nearest slot, otherwise round down
     * @return the minutes from midnight for the specified time
     */
    public int getSlotMinutes(Date time, int slotSize, boolean roundUp) {
        int mins = getMinutes(time);
        int result = getNearestSlot(mins, slotSize);
        if (result != mins && roundUp) {
            result += slotSize;
        }
        return result;
    }

    /**
     * Returns the time of the slot closest to that of the specified time.
     *
     * @param time     the time
     * @param slotSize the size of the slot, in minutes
     * @param roundUp  if {@code true} round up to the nearest slot, otherwise round down
     * @return the nearest slot time to {@code time}
     */
    public Date getSlotTime(Date time, int slotSize, boolean roundUp) {
        Date result;
        int mins = getMinutes(time);
        int nearestSlot = getNearestSlot(mins, slotSize);
        if (nearestSlot != mins) {
            Date day = DateRules.getDate(time);
            if (slotSize != DAY_IN_MINUTES) {
                if (roundUp && nearestSlot < mins) {
                    nearestSlot += slotSize;
                }
                ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(day.toInstant(), ZoneId.systemDefault())
                        .plusMinutes(nearestSlot);
                result = Date.from(zonedDateTime.toInstant());
            } else {
                // for slot sizes of a day, do date addition to avoid daylight savings issues. See OVPMS-2200
                if (roundUp) {
                    result = DateRules.getDate(day, 1, DateUnits.DAYS);
                } else {
                    result = day;
                }
            }
        } else {
            result = time;
        }
        return result;
    }

    /**
     * Returns the first appointment that overlaps the specified date range.
     * <p>
     * This ignores cancelled appointments.
     *
     * @param startTime the start of the date range
     * @param endTime   the end of the date range
     * @param schedule  the schedule
     * @return the appointment times, or {@code null} if none overlaps
     */
    public Times getOverlap(Date startTime, Date endTime, Entity schedule) {
        Times result = null;

        // If the schedule has maxDuration set, use it to improve performance.
        // This determines the lower and upper bounds of appointment times and limits table range scans on the
        // activity_start_time and activity_end_time columns of the participations table.
        boolean limit = false;
        Date lowerbound = null; // the minimum date that an appointment can start on
        Date upperbound = null; // the maximum date that an appointment can end on
        IMObjectBean bean = service.getBean(schedule);
        int maxDuration = bean.getInt("maxDuration", -1);
        DateUnits units = DateUnits.fromString(bean.getString("maxDurationUnits"), null);
        if (maxDuration > 0 && units != null) {
            // can restrict the query to limit table range scans
            limit = true;
            lowerbound = DateRules.getDate(startTime, -maxDuration, units);
            upperbound = DateRules.getDate(endTime, maxDuration, units);
        }

        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.APPOINTMENT);
        query.getArchetypeConstraint().setAlias("act");
        query.add(new ObjectRefSelectConstraint("act"));
        query.add(new NodeSelectConstraint(START_TIME));
        query.add(new NodeSelectConstraint("endTime"));
        JoinConstraint participation = join("schedule");
        participation.add(eq(ENTITY, schedule));

        // to encourage mysql to use the correct index
        participation.add(new ParticipationConstraint(ActShortName, ScheduleArchetypes.APPOINTMENT));
        if (limit) {
            // limit range scans
            participation.add(new ParticipationConstraint(StartTime, RelationalOp.GTE, lowerbound));
            participation.add(new ParticipationConstraint(StartTime, RelationalOp.LT, upperbound));
            participation.add(new ParticipationConstraint(EndTime, RelationalOp.GT, lowerbound));
            participation.add(new ParticipationConstraint(EndTime, RelationalOp.LTE, upperbound));
        }
        participation.add(new ParticipationConstraint(StartTime, RelationalOp.LT, endTime));
        participation.add(new ParticipationConstraint(EndTime, RelationalOp.GT, startTime));
        query.add(participation);
        query.add(and(lt(START_TIME, endTime), gt("endTime", startTime)));
        query.add(sort(START_TIME));
        query.add(Constraints.ne(STATUS, ActStatus.CANCELLED));
        query.setMaxResults(1);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        if (iterator.hasNext()) {
            ObjectSet set = iterator.next();
            result = new Times(set.getReference("act.reference"), set.getDate("act.startTime"),
                               set.getDate("act.endTime"));
        }
        return result;
    }

    /**
     * Returns the first appointment or calendar block longer than the specified duration.
     * <p/>
     * This is designed to detect events that would be affected by changes to the maxDuration of an
     * <em>party.organisationSchedule</em>. Reducing this can't prevent retrieval of events that span multiple.
     * <p/>
     * As it is an expensive operation, the <em>from</em> parameter can be used to limit the range of events
     * examined. Setting it to now for example will examine all events with an endTime &gt; now or in the future.<p/>
     * As appointments are typically queried for an entire day, it is recommended to use today's date for <em>from</em>.
     *
     * @param schedule the schedule
     * @param from     the date to start searching from. All events with an end time &gt; this will be examined
     * @param duration the duration
     * @param units    the duration units
     * @return the first event, or {@code null} if none is found
     * @see AppointmentQuery
     */
    public Act getAppointmentEventLongerThanDuration(Entity schedule, Date from, int duration, DateUnits units) {
        NamedQuery query = new NamedQuery("getAppointmentEventsLongerThanDuration", "archetype", "id");
        query.setParameter("scheduleId", schedule.getId());
        query.setParameter("from", from);
        query.setParameter("duration", (long) units.toPeriod(duration).toStandardMinutes().getMinutes());
        query.setMaxResults(1);
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(query);
        ObjectSet set = iterator.hasNext() ? iterator.next() : null;
        return (set != null) ? service.get(set.getString("archetype"), set.getLong("id"), Act.class) : null;
    }

    /**
     * Determines if a reminder should be sent for an appointment.
     *
     * @param appointment the appointment
     * @return {@code true} if a reminder should be sent, otherwise {@code false}
     */
    public boolean sendReminder(Act appointment) {
        IMObjectBean bean = service.getBean(appointment);
        return bean.getBoolean("sendReminder");
    }

    /**
     * Indicates that an appointment reminder has been sent via SMS.
     * <p/>
     * NOTE: setting the date prevents any subsequent appointment reminder being sent.
     *
     * @param appointment the appointment
     * @param date        the date when it was sent. May be {@code null}
     */
    public void setSMSReminderSent(Act appointment, Date date) {
        IMObjectBean bean = service.getBean(appointment);
        bean.setValue("reminderSent", date);
        bean.setValue("reminderError", null);
        if (!AppointmentStatus.SMS_RECEIVED.equals(bean.getString("smsStatus"))) {
            // don't overwrite a RECEIVED status, as that would hide the fact that there is an unread SMS
            bean.setValue("smsStatus", AppointmentStatus.SMS_SENT);
        }
        bean.save();
    }

    /**
     * Returns the nearest slot.
     *
     * @param mins     the start minutes
     * @param slotSize the slot size
     * @return the minutes from midnight for the specified time
     */
    protected int getNearestSlot(int mins, int slotSize) {
        return (mins / slotSize) * slotSize;
    }

    /**
     * Creates an appointment query for a party that returns PENDING and CONFIRMED appointments, dated between
     * now and the specified interval.<br/>
     * Appointments are returned on ascending startTime and id.
     *
     * @param party    a customer or patient
     * @param node     the customer/patient node
     * @param interval the interval, relative to the current date/time
     * @param units    the interval units
     * @return a new query
     */
    protected ArchetypeQuery createPendingAppointmentQuery(Party party, String node, int interval, DateUnits units) {
        Date from = new Date();
        ArchetypeQuery query = createPendingAppointmentQuery(party, node, from);
        Date to = DateRules.getDate(from, interval, units);
        query.add(Constraints.lt(START_TIME, to));
        return query;
    }

    /**
     * Returns the appointment reminder job configuration, if one is present.
     *
     * @return the configuration, or {@code null} if none exists
     */
    protected Entity getAppointmentReminderJob() {
        ArchetypeQuery query = new ArchetypeQuery(APPOINTMENT_REMINDER_JOB, true, true);
        query.setMaxResults(1);
        Iterator<IMObject> iterator = new IMObjectQueryIterator<>(service, query);
        return (iterator.hasNext()) ? (Entity) iterator.next() : null;
    }

    /**
     * Returns the minutes from midnight for the specified time.
     * <p/>
     * This takes into account daylight saving.
     *
     * @param time the time
     * @return the minutes from midnight for {@code time}
     */
    private int getMinutes(Date time) {
        ZonedDateTime zoned = ZonedDateTime.ofInstant(time.toInstant(), ZoneId.systemDefault());
        ZonedDateTime start = zoned.toLocalDate().atStartOfDay(ZoneId.systemDefault());
        return (int) Duration.between(start, zoned).toMinutes();
    }

    /**
     * Creates a PENDING/CONFIRMED appointment query for a party, dated from the specified date.<br/>
     * Appointments are returned on ascending startTime and id.
     *
     * @param party a customer or patient
     * @param node  the customer/patient node
     * @param from  only return appointments starting on or after this date
     * @return a new query
     */
    private ArchetypeQuery createPendingAppointmentQuery(Party party, String node, Date from) {
        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.APPOINTMENT);
        query.add(Constraints.join(node).add(Constraints.eq(ENTITY, party)));
        query.add(Constraints.in(STATUS, AppointmentStatus.PENDING, AppointmentStatus.CONFIRMED));
        query.add(Constraints.gte(START_TIME, from));
        query.add(Constraints.sort(START_TIME));
        query.add(Constraints.sort("id"));
        return query;
    }

    /**
     * Returns the schedule slot size in minutes.
     *
     * @param schedule the schedule
     * @return the schedule slot size in minutes
     * @throws OpenVPMSException for any error
     */
    private int getSlotSize(IMObjectBean schedule) {
        int slotSize = schedule.getInt("slotSize");
        String slotUnits = schedule.getString("slotUnits");
        int result;
        if ("HOURS" .equals(slotUnits)) {
            result = slotSize * 60;
        } else {
            result = slotSize;
        }
        return result;
    }

    /**
     * Helper to return the no. of slots for an appointment type.
     *
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     * @return the no. of slots, or {@code 0} if unknown
     * @throws OpenVPMSException for any error
     */
    private int getSlots(IMObjectBean schedule, Entity appointmentType) {
        int noSlots = 0;
        EntityRelationship relationship = schedule.getValue("appointmentTypes", EntityRelationship.class,
                                                            Predicates.targetEquals(appointmentType));
        if (relationship != null) {
            IMObjectBean bean = service.getBean(relationship);
            noSlots = bean.getInt("noSlots");
        }
        return noSlots;
    }

    /**
     * Updates the status of a linked act to that of the supplied act,
     * where the statuses are common.
     *
     * @param act    the act
     * @param linked the act to update
     */
    private void updateStatus(Act act, Act linked) {
        String status = act.getStatus();
        // Only update the linked act status if workflow status not pending.
        if ((WorkflowStatus.IN_PROGRESS.equals(status) || WorkflowStatus.BILLED.equals(status)
             || WorkflowStatus.COMPLETED.equals(status) || WorkflowStatus.CANCELLED.equals(status))
            && !status.equals(linked.getStatus())) {
            linked.setStatus(status);
            if (linked.isA(ScheduleArchetypes.TASK) && WorkflowStatus.COMPLETED.equals(status)) {
                // update the task's end time to now
                linked.setActivityEndTime(new Date());
            }
            service.save(linked);
        }
    }

    /**
     * Handler to copy appointments.
     */
    private static class AppointmentCopyHandler extends ActCopyHandler {

        /**
         * Default constructor.
         */
        public AppointmentCopyHandler() {
            setCopy(Act.class, Participation.class);
            setExclude(ActRelationship.class);
        }

        /**
         * Determines if a node is copyable.
         *
         * @param archetype the node's archetype descriptor
         * @param node      the node
         * @return {@code true}
         */
        @Override
        protected boolean checkCopyable(ArchetypeDescriptor archetype, NodeDescriptor node) {
            return true;
        }
    }
}
