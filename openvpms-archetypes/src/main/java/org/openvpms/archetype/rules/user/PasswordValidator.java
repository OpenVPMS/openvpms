/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.openvpms.archetype.i18n.ArchetypeMessages;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates passwords according to the <em>entity.globalSettingsPassword</em> policy.
 *
 * @author Tim Anderson
 */
public class PasswordValidator {

    /**
     * The password policy.
     */
    private final PasswordPolicy policy;

    /**
     * The pattern matching allowed special characters.
     */
    private final Pattern specialPattern;

    /**
     * Pattern to match invalid characters.
     */
    private final Pattern invalidPattern;

    /**
     * Pattern matching passwords containing lowercase characters.
     */
    private static final Pattern LOWER_PATTERN = Pattern.compile("[a-z]");


    /**
     * Pattern matching passwords containing uppercase characters.
     */
    private static final Pattern UPPER_PATTERN = Pattern.compile("[A-Z]");

    /**
     * Pattern matching passwords containing numbers.
     */
    private static final Pattern NUMBER_PATTERN = Pattern.compile("[0-9]");

    /**
     * Constructs a {@link PasswordValidator}.
     *
     * @param policy the password policy
     */
    public PasswordValidator(PasswordPolicy policy) {
        this.policy = policy;
        specialPattern = Pattern.compile("[" + policy.getSpecialCharactersEscaped() + "]");
        invalidPattern = Pattern.compile("[^" + policy.getValidCharactersEscaped() + "]");
    }

    /**
     * Validates a password.
     *
     * @param password the password to validate
     * @return a list of validation error messages, or an empty list if the password is valid
     */
    public List<String> validate(String password) {
        List<String> errors = new ArrayList<>();
        int minLength = policy.getMinLength();
        if (password.matches(".*\\s.*")) {
            errors.add(ArchetypeMessages.passwordCannotContainWhitespace());
        }
        if (password.length() < minLength) {
            errors.add(ArchetypeMessages.passwordLessThanMinimumLength(minLength));
        } else {
            int maxLength = policy.getMaxLength();
            if (password.length() > maxLength) {
                errors.add(ArchetypeMessages.passwordGreaterThanMaximumLength(maxLength));
            }
        }

        if (policy.lowercaseRequired() && noMatch(password, LOWER_PATTERN)) {
            errors.add(ArchetypeMessages.passwordMustContainLowercaseCharacter());
        }
        if (policy.uppercaseRequired() && noMatch(password, UPPER_PATTERN)) {
            errors.add(ArchetypeMessages.passwordMustContainUppercaseCharacter());
        }
        if (policy.numberRequired() && noMatch(password, NUMBER_PATTERN)) {
            errors.add(ArchetypeMessages.passwordMustContainNumber());
        }
        if (policy.specialRequired() && noMatch(password, specialPattern)) {
            errors.add(ArchetypeMessages.passwordMustContainSpecialCharacter());
        }
        if (errors.isEmpty()) {
            Matcher matcher = invalidPattern.matcher(password);
            if (matcher.find()) {
                errors.add(ArchetypeMessages.passwordCharacterInvalid(matcher.group(0).charAt(0)));
            }
        }
        return errors;
    }

    /**
     * Determines if a password doesn't match a pattern
     *
     * @param password the password
     * @param pattern  the pattern
     * @return {@code true} if the password doesn't match the pattern, otherwise {@code alse}
     */
    private boolean noMatch(String password, Pattern pattern) {
        return !pattern.matcher(password).find();
    }
}
