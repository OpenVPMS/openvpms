/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.JoinConstraint;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.OrConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;

import java.util.List;


/**
 * Helper to create queries for customer account acts.
 *
 * @author Tim Anderson
 */
public class CustomerAccountQueryFactory {

    /**
     * The act id node.
     */
    private static final String ID = "id";

    /**
     * The act start time node.
     */
    private static final String START_TIME = "startTime";

    /**
     * The act status node.
     */
    private static final String STATUS = "status";

    /**
     * The act amount node.
     */
    private static final String AMOUNT = "amount";

    /**
     * The act allocated amount node.
     */
    private static final String ALLOCATED_AMOUNT = "allocatedAmount";

    /**
     * The act credit node.
     */
    private static final String CREDIT = "credit";

    /**
     * Default constructor.
     */
    private CustomerAccountQueryFactory() {

    }

    /**
     * Creates an object set query for unallocated acts for the specified customer.
     * <p/>
     * Returns only the amount, allocatedAmount and credit nodes, named <em>a.amount</em>, <em>a.allocatedAmount</em>
     * and <em>a.credit</em> respectively.
     *
     * @param customer   the customer
     * @param archetypes the act archetypes
     * @return a new query
     */
    public static ArchetypeQuery createUnallocatedObjectSetQuery(Party customer, List<String> archetypes) {
        return createUnallocatedObjectSetQuery(customer, archetypes.toArray(new String[0]));
    }

    /**
     * Creates an object set query for unallocated acts for the specified
     * customer. Returns only the amount, allocatedAmount and credit nodes,
     * named <em>a.amount</em>, <em>a.allocatedAmount</em> and <em>a.credit</em>
     * respectively.
     *
     * @param customer   the customer
     * @param shortNames the act short names
     * @return a new query
     */
    public static ArchetypeQuery createUnallocatedObjectSetQuery(Party customer, String[] shortNames) {
        ArchetypeQuery query = createUnallocatedQuery(customer, shortNames, null);
        query.add(new NodeSelectConstraint(AMOUNT));
        query.add(new NodeSelectConstraint(ALLOCATED_AMOUNT));
        query.add(new NodeSelectConstraint(CREDIT));
        return query;
    }

    /**
     * Creates a query for unallocated acts for the specified customer.
     *
     * @param customer   the customer
     * @param shortNames the act short names
     * @param exclude    the act to exclude. May be {@code null}
     * @return a new query
     */
    public static ArchetypeQuery createUnallocatedQuery(Party customer, String[] shortNames, Act exclude) {
        return createUnallocatedQuery(customer.getObjectReference(), shortNames, exclude);
    }

    /**
     * Creates a query for unallocated acts for the specified customer.
     *
     * @param customer   the customer
     * @param archetypes the act archetypes
     * @param exclude    the act to exclude. May be {@code null}
     * @return a new query
     */
    public static ArchetypeQuery createUnallocatedQuery(Reference customer, List<String> archetypes, Act exclude) {
        return createUnallocatedQuery(customer, archetypes.toArray(new String[0]), exclude);
    }

    /**
     * Creates a query for unallocated acts for the specified customer.
     *
     * @param customer   the customer
     * @param shortNames the act short names
     * @param exclude    the act to exclude. May be {@code null}
     * @return a new query
     */
    public static ArchetypeQuery createUnallocatedQuery(Reference customer, String[] shortNames, Act exclude) {
        ArchetypeQuery query = createBalanceParticipationQuery(customer, shortNames, exclude);
        query.add(Constraints.eq(STATUS, ActStatus.POSTED));
        return query;
    }

    /**
     * Creates a query for unbilled acts for the specified customer.
     *
     * @param customer   the customer
     * @param shortNames the act short names
     * @return a new query
     */
    public static ArchetypeQuery createUnbilledObjectSetQuery(Party customer, String[] shortNames) {
        return createUnbilledObjectSetQuery(customer.getObjectReference(), shortNames);
    }

    /**
     * Creates a query for unbilled acts for the specified customer.
     *
     * @param customer   the customer reference
     * @param shortNames the act short names
     * @return a new query
     */
    public static ArchetypeQuery createUnbilledObjectSetQuery(Reference customer, String[] shortNames) {
        ArchetypeQuery query = createBalanceParticipationQuery(
                customer, shortNames, null);
        query.add(Constraints.ne(STATUS, ActStatus.POSTED));
        query.add(new NodeSelectConstraint(AMOUNT));
        query.add(new NodeSelectConstraint(ALLOCATED_AMOUNT));
        query.add(new NodeSelectConstraint(CREDIT));
        return query;
    }

    /**
     * Creates an object set query for acts for the specified customer.
     * <p>
     * Returns only the amount, allocatedAmount and credit nodes,
     * named <em>a.amount</em>, <em>a.allocatedAmount</em> and <em>a.credit</em>
     * respectively.
     *
     * @param customer      the customer
     * @param archetypes    the act archetypes
     * @param sortAscending if {@code true}, sort on ascending <em>startTime</em>; otherwise sort descending
     * @return a new query
     */
    public static ArchetypeQuery createObjectSetQuery(Party customer, List<String> archetypes, boolean sortAscending) {
        return createObjectSetQuery(customer, archetypes.toArray(new String[0]), sortAscending);
    }

    /**
     * Creates an object set query for acts for the specified customer.
     * <p>
     * Returns only the amount, allocatedAmount and credit nodes,
     * named <em>a.amount</em>, <em>a.allocatedAmount</em> and <em>a.credit</em>
     * respectively.
     *
     * @param customer      the customer
     * @param shortNames    the act short names
     * @param sortAscending if {@code true}, sort on ascending
     *                      <em>startTime</em>; otherwise sort descending
     * @return a new query
     */
    public static ArchetypeQuery createObjectSetQuery(Party customer, String[] shortNames, boolean sortAscending) {
        ArchetypeQuery query = createQuery(customer, shortNames);
        query.add(Constraints.sort(START_TIME, sortAscending));
        query.add(Constraints.sort(ID, true));
        query.add(new NodeSelectConstraint(AMOUNT));
        query.add(new NodeSelectConstraint(ALLOCATED_AMOUNT));
        query.add(new NodeSelectConstraint(CREDIT));
        return query;
    }

    /**
     * Creates a query for all acts matching the specified archetype, for a customer.
     *
     * @param customer  the customer
     * @param archetype the act archetype
     * @return the corresponding query
     */
    public static ArchetypeQuery createQuery(Party customer, String archetype) {
        return createQuery(customer.getObjectReference(), new String[]{archetype});
    }

    /**
     * Creates a query for all acts matching the specified short names, for a customer.
     *
     * @param customer   the customer
     * @param archetypes the act archetypes
     * @return the corresponding query
     */
    public static ArchetypeQuery createQuery(Party customer, List<String> archetypes) {
        return createQuery(customer.getObjectReference(), archetypes.toArray(new String[0]));
    }

    /**
     * Creates a query for all acts matching the specified short names, for a customer.
     *
     * @param customer   the customer
     * @param shortNames the act archetype short names
     * @return the corresponding query
     */
    public static ArchetypeQuery createQuery(Party customer, String[] shortNames) {
        return createQuery(customer.getObjectReference(), shortNames);
    }

    /**
     * Creates a query for all acts matching the specified short names, for a customer.
     *
     * @param customer   the customer reference
     * @param shortNames the act archetype short names
     * @return the corresponding query
     */
    public static ArchetypeQuery createQuery(Reference customer, List<String> shortNames) {
        return createQuery(customer, shortNames.toArray(new String[0]));
    }

    /**
     * Creates a query for all acts matching the specified short names, for a customer.
     *
     * @param customer   the customer reference
     * @param shortNames the act archetype short names
     * @return the corresponding query
     */
    public static ArchetypeQuery createQuery(Reference customer, String[] shortNames) {
        ShortNameConstraint archetypes = Constraints.shortName("act", shortNames, false);
        ArchetypeQuery query = new ArchetypeQuery(archetypes);
        JoinConstraint join = Constraints.join("customer").add(Constraints.eq("entity", customer));
        OrConstraint or = new OrConstraint();

        // re-specify the act short names, this time on the participation act
        // node. Ideally wouldn't specify them on the acts at all, but this
        // is not supported by ArchetypeQuery.
        for (String shortName : shortNames) {
            ArchetypeId id = new ArchetypeId(shortName);
            or.add(Constraints.eq("customer.act", id));
        }
        query.add(join);
        query.add(or);
        return query;
    }

    /**
     * Creates an account balance participation query.
     * <p>
     * This sorts acts on ascending startTime and id.
     *
     * @param customer   the customer
     * @param shortNames the act archetype short names
     * @param exclude    the act to exclude fromj the result. May be {@code null}
     * @return the corresponding query
     */
    private static ArchetypeQuery createBalanceParticipationQuery(Reference customer, String[] shortNames, Act exclude) {
        ShortNameConstraint archetypes = Constraints.shortName("act", shortNames, false);
        ArchetypeQuery query = new ArchetypeQuery(archetypes);
        JoinConstraint join = Constraints.join("accountBalance").add(Constraints.eq("entity", customer));
        if (exclude != null) {
            join.add(Constraints.ne("act", exclude));
        }
        query.add(join);
        query.add(Constraints.sort(START_TIME));
        query.add(Constraints.sort(ID));
        return query;
    }

}
