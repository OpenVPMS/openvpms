/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;


/**
 * Act status types for <em>act.customerAppointment</em> acts.
 *
 * @author Tim Anderson
 */
public class AppointmentStatus {

    /**
     * Pending status.
     */
    public static final String PENDING = WorkflowStatus.PENDING;

    /**
     * Confirmed status.
     */
    public static final String CONFIRMED = "CONFIRMED";

    /**
     * Cancelled status.
     */
    public static final String CANCELLED = WorkflowStatus.CANCELLED;

    /**
     * No-show status, indicating the customer didn't turn up for the appointment.
     */
    public static final String NO_SHOW = "NO_SHOW";

    /**
     * Checked-in status.
     */
    public static final String CHECKED_IN = "CHECKED_IN";

    /**
     * In progress status.
     */
    public static final String IN_PROGRESS = WorkflowStatus.IN_PROGRESS;

    /**
     * Billed status.
     */
    public static final String BILLED = WorkflowStatus.BILLED;

    /**
     * Completed status.
     */
    public static final String COMPLETED = WorkflowStatus.COMPLETED;

    /**
     * Admitted status.
     */
    public static final String ADMITTED = "ADMITTED";

    /**
     * Indicates that an SMS has been sent.
     */
    public static final String SMS_SENT = "SENT";

    /**
     * Indicates that an SMS has been received.
     */
    public static final String SMS_RECEIVED = "RECEIVED";

    /**
     * Indicates that the received SMS has been read.
     */
    public static final String SMS_READ = "READ";

    /**
     * Default constructor.
     */
    private AppointmentStatus() {
        // no-op
    }
}
