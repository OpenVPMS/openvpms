/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.service.archetype.helper.DefaultIMObjectCopyHandler;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopier;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Document rules.
 *
 * @author Tim Anderson
 */
public class DocumentRules {

    /**
     * The versions node.
     */
    public static final String VERSIONS = "versions";

    /**
     * The archetype service.
     */
    private final ArchetypeService service;


    /**
     * Constructs a {@link DocumentRules}.
     *
     * @param service the archetype service
     */
    public DocumentRules(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Determines if a document act support multiple document versions.
     *
     * @param act the document act
     * @return {@code true} if the act supports versioning
     */
    public boolean supportsVersions(DocumentAct act) {
        return service.getBean(act).hasNode(VERSIONS);
    }

    /**
     * Adds a document to a document act.
     * <p/>
     * If the act currently has a document, and the act supports versioning, the original document will be saved
     * as a version using {@link #createVersion}.
     *
     * @param act      the act to add the document to
     * @param document the document to add
     * @return a list of objects to save
     */
    public List<IMObject> addDocument(DocumentAct act, Document document) {
        return addDocument(act, document, true);
    }

    /**
     * Adds a document to a document act.
     * <p/>
     * If {@code version} is {@code true}, the act is currently has a document, and the act supports versioning,
     * the original document will be saved as a version using {@link #createVersion}.
     *
     * @param act      the act to add the document to
     * @param document the document to add
     * @param version  if {@code true} version any old document if the act supports it
     * @return a list of objects to save
     */
    public List<IMObject> addDocument(DocumentAct act, Document document, boolean version) {
        List<IMObject> objects = new ArrayList<>();
        objects.add(act);

        if (version && act.getDocument() != null) {
            DocumentAct oldVersion = createVersion(act);
            if (oldVersion != null) {
                IMObjectBean bean = service.getBean(act);
                ActRelationship relationship = (ActRelationship) bean.addTarget(VERSIONS, oldVersion);
                oldVersion.addActRelationship(relationship);
                objects.add(oldVersion);
            }
        }
        act.setDocument(document.getObjectReference());
        act.setFileName(document.getName());
        act.setMimeType(document.getMimeType());
        act.setName(document.getName());

        if (document.isNew()) {
            objects.add(document);
        }
        return objects;
    }

    /**
     * Creates a version of a document act.
     * <p/>
     * For this to occur, the source act must have a <em>versions</em> node. An instance of the referred
     * to <em>version</em> act will be created, and a shallow copy of the source act's nodes copied to it.
     * This includes simple nodes and participations.
     *
     * @param source the act to version
     * @return a shallow copy of the act, or {@code null} if it can't be versioned
     */
    public DocumentAct createVersion(DocumentAct source) {
        DocumentAct result = null;
        Reference existing = source.getDocument();
        if (existing != null && supportsVersions(source)) {
            VersioningCopyHandler handler = new VersioningCopyHandler(source, service);
            IMObjectCopier copier = new IMObjectCopier(handler, service);
            List<IMObject> objects = copier.apply(source);
            result = (DocumentAct) objects.get(0);
        }
        return result;
    }

    /**
     * Determines if a document associated with an act duplicates that specified.
     * <p/>
     * The documents are considered duplicates if they both have the same size and checksum.
     * <p/>
     * If any of the sizes or checksums are zero, they are not considered duplicates.
     *
     * @param act      the document act
     * @param document the document to compare
     * @return {@code true} if the documents have the same length and checksum
     */
    public boolean isDuplicate(DocumentAct act, Document document) {
        boolean result = false;
        Reference reference = act.getDocument();
        if (reference != null && document.getSize() != 0 && document.getChecksum() != 0) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<Tuple> query = builder.createTupleQuery();
            Root<Document> root = query.from(Document.class, reference.getArchetype()).alias("doc");
            query.multiselect(root.get("size"), root.get("checksum"));
            query.where(builder.equal(root.get("id"), reference.getId()));
            Tuple tuple = service.createQuery(query).getFirstResult();
            if (tuple != null) {
                Integer size = tuple.get(0, Integer.class);
                Long checksum = tuple.get(1, Long.class);
                if (size != null && checksum != null && size != 0 && checksum != 0
                    && (size == document.getSize() && checksum == document.getChecksum())) {
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * Returns the versions of a document.
     *
     * @param act the document act
     * @return the versions of the document, or an empty list if the document has no versions
     */
    public List<DocumentAct> getVersions(DocumentAct act) {
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode(VERSIONS)) {
            return bean.getTargets(VERSIONS, DocumentAct.class);
        }
        return Collections.emptyList();
    }

    /**
     * Returns the logo associated with an entity.
     *
     * @param entity the entity
     * @return the logo, or {@code null} if none is found
     */
    public DocumentAct getLogo(Entity entity) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> root = query.from(DocumentAct.class, DocumentArchetypes.LOGO_ACT);
        Join<IMObject, IMObject> owner = root.join("owner").join("entity", entity.getArchetype());
        owner.on(builder.equal(owner.get("id"), entity.getId()));
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Copies a document.
     * <p/>
     * The contents are copied rather than referenced, to ensure that changes to the original aren't reflected in the
     * copy, and vice-versa.
     * <p/>
     * TODO - provide copy-on-write support for documents, to limit memory utilisation.
     *
     * @param document the document to copy
     * @return the copy of the document
     */
    public Document copy(Document document) {
        org.openvpms.component.business.domain.im.document.Document source
                = (org.openvpms.component.business.domain.im.document.Document) document;
        IMObjectCopier copier = new IMObjectCopier(new DefaultIMObjectCopyHandler(), service);
        List<IMObject> objects = copier.apply(document);
        org.openvpms.component.business.domain.im.document.Document result
                = (org.openvpms.component.business.domain.im.document.Document) objects.get(0);
        result.setContents(source.getContents().clone());
        return result;
    }
}
