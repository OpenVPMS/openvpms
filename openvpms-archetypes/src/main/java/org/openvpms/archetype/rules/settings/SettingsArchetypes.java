/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

/**
 * Settings archetypes.
 *
 * @author Tim Anderson
 */
public class SettingsArchetypes {

    /**
     * Global settings.
     */
    public static final String GLOBAL_SETTINGS = "entity.globalSettings*";

    /**
     * The password policy settings.
     */
    public static final String PASSWORD_POLICY = "entity.globalSettingsPassword";

    /**
     * Firewall settings.
     */
    public static final String FIREWALL_SETTINGS = "entity.globalSettingsFirewall";

    /**
     * OpenOffice settings
     */
    public static final String OPEN_OFFICE = "entity.globalSettingsOpenOffice";

    /**
     * Query settings.
     */
    public static final String QUERY_SETTINGS = "entity.globalSettingsQuery";

    /**
     * Report settings.
     */
    public static final String REPORT_SETTINGS = "entity.globalSettingsReport";
}
