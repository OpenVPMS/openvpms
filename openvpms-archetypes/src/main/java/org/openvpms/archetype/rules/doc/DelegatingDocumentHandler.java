/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.UnsupportedDoc;

/**
 * A {@link DocumentHandler} that delegates to different implementations based on name and mime type.
 * <p/>
 * If multiple handlers are registered that support a particular name or mime type, the first encountered will be used.
 *
 * @author Tim Anderson
 */
public class DelegatingDocumentHandler implements DocumentHandler {

    /**
     * The handlers to delegate to.
     */
    private final List<DocumentHandler> handlers;

    /**
     * Constructs a {@link DelegatingDocumentHandler}.
     *
     * @param handlers the handlers to delegate to
     */
    public DelegatingDocumentHandler(DocumentHandler... handlers) {
        this(Arrays.asList(handlers));
    }

    /**
     * Constructs a {@link DelegatingDocumentHandler}.
     *
     * @param handlers the handlers to delegate to
     */
    public DelegatingDocumentHandler(List<DocumentHandler> handlers) {
        this.handlers = handlers;
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name     the document name
     * @param mimeType the mime type of the document. May be {@code null}
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(String name, String mimeType) {
        return find(name, mimeType) != null;
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name      the document name
     * @param archetype the document archetype
     * @param mimeType  the mime type of the document. May be {@code null}
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(String name, String archetype, String mimeType) {
        return find(name, archetype, mimeType) != null;
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param document the document
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(Document document) {
        boolean result = false;
        for (DocumentHandler handler : handlers) {
            if (handler.canHandle(document)) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Creates a new {@link org.openvpms.component.business.domain.im.document.Document} from a stream.
     *
     * @param name     the document name. Any path information is removed.
     * @param stream   a stream representing the document content
     * @param mimeType the mime type of the document. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     * @return a new document
     * @throws DocumentException         if the document can't be created
     * @throws ArchetypeServiceException for any archetype service error
     * @throws OpenVPMSException         for any other error
     */
    @Override
    public org.openvpms.component.business.domain.im.document.Document create(String name, InputStream stream,
                                                                              String mimeType, int size) {
        DocumentHandler handler = find(name, mimeType);
        if (handler == null) {
            throw new DocumentException(UnsupportedDoc, name, mimeType);
        }
        return handler.create(name, stream, mimeType, size);
    }

    /**
     * Creates a new {@link org.openvpms.component.business.domain.im.document.Document} from a byte array.
     *
     * @param name     the document name. Any path information is removed.
     * @param content  the serialized content
     * @param mimeType the mime type of the content. May be {@code null}
     * @param size     the uncompressed document size
     * @return a new document
     * @throws DocumentException         if the document can't be created
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public org.openvpms.component.business.domain.im.document.Document create(String name, byte[] content,
                                                                              String mimeType, int size) {
        DocumentHandler handler = find(name, mimeType);
        if (handler == null) {
            throw new DocumentException(UnsupportedDoc, name, mimeType);
        }
        return handler.create(name, content, mimeType, size);
    }

    /**
     * Updates a {@link org.openvpms.component.business.domain.im.document.Document} from a stream.
     *
     * @param document the document to update
     * @param stream   a stream representing the new document content
     * @param mimeType the mime type of the document. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     */
    @Override
    public void update(org.openvpms.component.business.domain.im.document.Document document, InputStream stream,
                       String mimeType, int size) {
        String name = document.getName();
        DocumentHandler handler = find(name, mimeType);
        if (handler == null) {
            throw new DocumentException(UnsupportedDoc, name, mimeType);
        }
        handler.update(document, stream, mimeType, size);
    }

    /**
     * Returns the document content as a stream.
     *
     * @param document the document
     * @return the document content
     * @throws DocumentException for any error
     */
    @Override
    public InputStream getContent(Document document) {
        String name = document.getName();
        String mimeType = document.getMimeType();
        DocumentHandler handler = find(name, mimeType);
        if (handler == null) {
            throw new DocumentException(UnsupportedDoc, name, mimeType);
        }
        return handler.getContent(document);
    }

    /**
     * Finds a handler for a document.
     *
     * @param name     the document name
     * @param mimeType the mime type of the document
     * @return a handler for the document, or {@code null} if none is found
     */
    public DocumentHandler find(String name, String mimeType) {
        DocumentHandler result = null;
        for (DocumentHandler handler : handlers) {
            if (handler.canHandle(name, mimeType)) {
                result = handler;
                break;
            }
        }
        return result;
    }

    /**
     * Returns a handler for a document.
     *
     * @param name      the document name
     * @param archetype the document archetype
     * @param mimeType  the mime type of the document
     * @return a handler for the document, or {@code null} if none is found
     */
    public DocumentHandler find(String name, String archetype, String mimeType) {
        DocumentHandler result = null;
        for (DocumentHandler handler : handlers) {
            if (handler.canHandle(name, archetype, mimeType)) {
                result = handler;
                break;
            }
        }
        return result;
    }
}