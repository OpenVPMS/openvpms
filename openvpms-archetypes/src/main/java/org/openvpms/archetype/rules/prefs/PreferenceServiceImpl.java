/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.prefs;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.Objects;

import static org.openvpms.archetype.rules.prefs.PreferencesImpl.GROUPS;

/**
 * Default implementation of the {@link PreferenceService}.
 *
 * @author Tim Anderson
 */
public class PreferenceServiceImpl implements PreferenceService {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link PreferenceServiceImpl}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public PreferenceServiceImpl(IArchetypeService service, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Returns preferences for a user or practice.
     *
     * @param party  the party
     * @param source if non-null, specifies the source to copy preferences from if the party has none
     * @param save   if {@code true}, changes will be made persistent
     * @return the preferences
     */
    @Override
    public Preferences getPreferences(Party party, Party source, boolean save) {
        Preferences result;
        Reference reference = party.getObjectReference();
        Reference sourceReference = (source != null) ? source.getObjectReference() : null;
        if (save) {
            Entity entity = getEntity(reference, sourceReference);
            result = new PreferencesImpl(reference, sourceReference, entity, service, transactionManager);
        } else {
            result = getPreferences(reference, sourceReference);
        }
        return result;
    }

    /**
     * Returns the root preference entity for a user or practice, creating it if it doesn't exist.
     *
     * @param party  the party
     * @param source if non-null, specifies the party's preferences to copy to the user, if the user has none
     * @return the root preference entity
     */
    @Override
    public Entity getEntity(Party party, Party source) {
        Reference reference = party.getObjectReference();
        Reference sourceReference = source != null ? source.getObjectReference() : null;
        return getEntity(reference, sourceReference);
    }

    /**
     * Resets the preferences for a user or practice.
     *
     * @param party  the party
     * @param source if non-null, specifies the party's preferences to copy to the user
     */
    @Override
    public void reset(Party party, Party source) {
        Reference reference = party.getObjectReference();
        Reference sourceReference = source != null ? source.getObjectReference() : null;
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status) {
                reset(reference, sourceReference);
            }
        });
    }

    /**
     * Resets the preferences for a user or practice.
     * <p/>
     * NOTE: needs to be invoked within a transaction.
     *
     * @param party  the party
     * @param source if non-null, specifies the party's preferences to copy to the user
     */
    private void reset(Reference party, Reference source) {
        Entity prefs = getEntity(party);
        if (prefs != null) {
            IMObjectBean bean = service.getBean(prefs);
            List<IMObject> groups = bean.getTargets(GROUPS, IMObject.class);
            List<EntityLink> relationships = bean.getValues(GROUPS, EntityLink.class);
            for (EntityLink relationship : relationships) {
                prefs.removeEntityLink(relationship);
            }
            bean.save();
            for (IMObject group : groups) {
                service.remove(group);
            }
            service.remove(prefs);
        }
        if (source != null && !Objects.equals(party, source)) {
            Entity sourcePrefs = getEntity(source);
            if (sourcePrefs != null) {
                PreferenceManager.copy(party, sourcePrefs, service);
            }
        }
    }

    /**
     * Returns preferences for a party.
     * <p>
     * If the party has no preferences, and a {@code source} party is specified, the
     * <p>
     * Changes will not be made persistent.
     *
     * @param party  the party
     * @param source the party to use for default preferences. May be {@code null}
     * @return the preferences
     */
    private Preferences getPreferences(Reference party, Reference source) {
        return PreferenceManager.getPreferences(party, source, service);
    }

    /**
     * Returns the root preference entity for a party.
     *
     * @param party the party
     * @return the entity, or {@code null} if it doesn't exist
     */
    private Entity getEntity(Reference party) {
        return PreferenceManager.getEntity(party, service);
    }

    /**
     * Returns the root preference entity for a user or practice, creating and saving one if none exists.
     *
     * @param party  the party
     * @param source if non-null, specifies the source to copy preferences from if the party has none
     * @return the root preference entity
     */
    private Entity getEntity(Reference party, Reference source) {
        Entity result;
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        result = template.execute(status -> {
            Entity prefs = getEntity(party);
            if (prefs == null && source != null) {
                Entity sourcePrefs = getEntity(source);
                if (sourcePrefs != null) {
                    prefs = PreferenceManager.copy(party, sourcePrefs, service);
                }
            }
            if (prefs == null) {
                prefs = service.create(PreferenceArchetypes.PREFERENCES, Entity.class);
                IMObjectBean bean = service.getBean(prefs);
                bean.setTarget(PreferencesImpl.USER, party);
                bean.save();
            }
            return prefs;
        });
        return result;
    }
}
