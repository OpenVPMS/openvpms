/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.openvpms.component.business.service.archetype.helper.AbstractIMObjectCopyHandler;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopier;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopyHandler;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;


/**
 * {@link IMObjectCopyHandler} that creates reversals for acts.
 *
 * @author Tim Anderson
 */
public abstract class AbstractActReversalHandler extends AbstractIMObjectCopyHandler {

    /**
     * Determines if the act is a debit or a credit.
     */
    private final boolean debit;

    /**
     * Map of debit types to their corresponding credit types.
     */
    private final String[][] shortNames;


    /**
     * Constructs an {@link AbstractActReversalHandler}.
     *
     * @param debit      if {@code true} indicates that the act is a debit; {@code false} indicates its a credit
     * @param shortNames a list of archetype short names. The first column indicates the 'debit' short name, the second
     *                   the corresponding 'credit short name
     */
    protected AbstractActReversalHandler(boolean debit, String[][] shortNames) {
        this.debit = debit;
        this.shortNames = shortNames;
    }

    /**
     * Determines how {@link IMObjectCopier} should treat an object.
     *
     * @param object  the source object
     * @param service the archetype service
     * @return {@code object} if the object shouldn't be copied, {@code null} if it should be replaced with
     * {@code null}, or a new instance if the object should be copied
     */
    @Override
    public IMObject getObject(IMObject object, ArchetypeService service) {
        IMObject result;
        if (object instanceof Act || object instanceof ActRelationship || object instanceof Participation) {
            String shortName = getArchetype(object);
            result = service.create(shortName, IMObject.class);
        } else {
            result = object;
        }
        return result;
    }

    /**
     * Returns the archetype to map an object to.
     *
     * @param object the object
     * @return the archetype to map the object to
     */
    private String getArchetype(IMObject object) {
        String shortName = object.getArchetype();
        for (String[] map : shortNames) {
            String debitType = map[0];
            String creditType = map[1];
            if (debit) {
                if (debitType.equals(shortName)) {
                    shortName = creditType;
                    break;
                }
            } else {
                if (creditType.equals(shortName)) {
                    shortName = debitType;
                    break;
                }
            }
        }
        return shortName;
    }
}
