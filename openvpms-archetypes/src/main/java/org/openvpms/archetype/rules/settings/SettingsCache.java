/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.MonitoringIMObjectCache;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Cache of settings.
 *
 * @author Tim Anderson
 */
public class SettingsCache implements DisposableBean {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The singleton service.
     */
    private final SingletonService singletonService;

    /**
     * The authentication context.
     */
    private final AuthenticationContext context;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The cache.
     */
    private final Cache cache;

    /**
     * Constructs a {@link SettingsCache}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param singletonService   the singleton service
     * @param context            the authentication context
     * @param userRules          the user rules
     */
    public SettingsCache(IArchetypeService service, PlatformTransactionManager transactionManager,
                         SingletonService singletonService, AuthenticationContext context, UserRules userRules) {
        this.service = service;
        this.transactionManager = transactionManager;
        this.singletonService = singletonService;
        this.context = context;
        this.userRules = userRules;
        cache = new Cache(service);
    }

    /**
     * Returns a settings group, given its name.
     * <p>
     * If the group doesn't exist, it will be created.
     *
     * @param name the group name
     * @return the group
     * @throws IllegalArgumentException if the name doesn't correspond to a valid group
     */
    public IMObjectBean getGroup(String name) {
        return getGroup(name, false);
    }

    /**
     * Returns a settings group, given its name.
     * <p>
     * If the group doesn't exist, it will be created.
     *
     * @param name      the group name
     * @param useCached if {@code true}, use the cached instance, if any, else load a new instance
     * @return the group
     * @throws IllegalArgumentException if the name doesn't correspond to a valid group
     */
    public IMObjectBean getGroup(String name, boolean useCached) {
        IMObjectBean bean = null;
        if (useCached) {
            bean = cache.get(name);
        }
        if (bean == null) {
            bean = loadGroup(name);
        }
        return bean;
    }

    /**
     * Destroys this.
     */
    @Override
    public void destroy() {
        cache.destroy();
    }

    /**
     * Loads a settings group.
     * <p/>
     * If the group doesn't exist, it will be created. If the current user has authority to do so, it will also
     * be saved.
     *
     * @param name the group name
     * @return the corresponding group
     */
    private IMObjectBean loadGroup(String name) {
        // start an isolated transaction so if it rolls back, the parent won't
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        boolean create = canSaveSettings(name); // only create via the service if the user can save it
        Entity result = template.execute(status -> {
            Entity entity = singletonService.get(name, Entity.class, create);
            if (entity == null) {
                // user doesn't have authority to save, so create a transient instance, populated with defaults.
                entity = create(name);
            }
            return entity;
        });
        IMObjectBean bean = cache.cache(result);
        if (bean == null) {
            // will only be null with a race condition on the cache
            bean = service.getBean(result);
        }
        return bean;
    }

    /**
     * Creates an entity of the specified archetype.
     * <p/>
     * If the archetype service is proxied, this uses the underlying instance to avoid any security constraints
     * regarding the creation of objects. TODO - see OBF-285
     *
     * @param archetype the archetype
     * @return the corresponding entity
     */
    private Entity create(String archetype) {
        ArchetypeService archetypeService = null;
        if (AopUtils.isAopProxy(service) && service instanceof Advised) {
            try {
                archetypeService = (ArchetypeService) ((Advised) service).getTargetSource().getTarget();
            } catch (Exception ignore) {
                // no-op
            }
        }
        if (archetypeService == null) {
            archetypeService = service;
        }
        return archetypeService.create(archetype, Entity.class);
    }

    /**
     * Determines if settings can be stored.
     * <p/>
     * Settings can only be stored if an authenticated user is present, with permissions to save the settings.
     *
     * @param archetype the settings archetype
     * @return {@code true} if settings can be stored, otherwise {@code false}
     */
    private boolean canSaveSettings(String archetype) {
        User user = context.getUser();
        return user != null && userRules.canSave(context.getUser(), archetype);
    }

    /**
     * Cache for settings that updates when settings are saved externally.
     */
    private class Cache extends MonitoringIMObjectCache<Entity> {

        /**
         * The map of settings archetypes to their corresponding instances.
         */
        private final Map<String, IMObjectBean> groups = Collections.synchronizedMap(new HashMap<>());

        /**
         * Constructs a {@link Cache}.
         *
         * @param service the archetype service
         */
        public Cache(IArchetypeService service) {
            super(service, SettingsArchetypes.GLOBAL_SETTINGS, Entity.class, false);
        }

        /**
         * Caches a settings object.
         *
         * @param object the settings object
         * @return a bean wrapping the object
         */
        public IMObjectBean cache(Entity object) {
            addObject(object);
            return groups.get(object.getArchetype());
        }

        /**
         * Returns a settings group.
         *
         * @param archetype the settings group archetype
         * @return the corresponding group, wrapped in a bean, or {@code null} if none exists
         */
        public IMObjectBean get(String archetype) {
            return groups.get(archetype);
        }

        /**
         * Invoked when an object is added to the cache.
         *
         * @param object the added object
         */
        @Override
        protected void added(Entity object) {
            IMObjectBean bean = service.getBean(object);
            groups.put(object.getArchetype(), bean);
        }

        /**
         * Invoked when an object is removed from the cache.
         *
         * @param object the removed object
         */
        @Override
        protected void removed(Entity object) {
            groups.remove(object.getArchetype());
        }
    }
}