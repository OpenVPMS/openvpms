/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.CollectionNodeConstraint;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.ObjectRefNodeConstraint;
import org.openvpms.component.system.common.query.QueryIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.shortName;
import static org.openvpms.component.system.common.query.Constraints.sort;


/**
 * Patient medical record rules.
 *
 * @author Tim Anderson
 */
public class MedicalRecordRules {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Clinical event item short names.
     */
    private String[] clinicalEventItems;

    /**
     * Clinical problem item short names.
     */
    private String[] clinicalProblemItems;

    /**
     * Medical records that require locking.
     */
    private String[] lockableRecords;

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Clinician node name.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Items node name.
     */
    private static final String ITEMS = "items";

    /**
     * Relationship target node name.
     */
    private static final String TARGET = "target";


    /**
     * Creates a {@link MedicalRecordRules}.
     *
     * @param service the archetype service
     */
    public MedicalRecordRules(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a clinical note for a patient.
     * <p/>
     * The note is not saved.
     *
     * @param startTime the start time for the note
     * @param patient   the patient
     * @param note      the note
     * @param clinician the clinician. May be {@code null}
     * @return a new note
     */
    public Act createNote(Date startTime, Party patient, String note, User clinician) {
        Act act = service.create(PatientArchetypes.CLINICAL_NOTE, Act.class);
        IMObjectBean bean = service.getBean(act);
        bean.setValue(START_TIME, startTime);
        bean.setTarget(PATIENT, patient);
        bean.setValue("note", note);
        if (clinician != null) {
            bean.setTarget(CLINICIAN, clinician);
        }
        return act;
    }

    /**
     * Adds an <em>act.patientMedication</em>, <em>act.patientInvestigation*</em> or
     * <em>act.customerAccountInvoiceItem</em> to an <em>act.patientClinicalEvent</em> associated with the act's
     * patient.
     *
     * @param act       the act to add
     * @param startTime the startTime used to select the event
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void addToEvent(Act act, Date startTime) {
        addToEvents(Collections.singletonList(act), startTime);
    }

    /**
     * Links a patient medical child record to their parent act.
     * If the child is an <em>act.patientClinicalProblem</em>, all of its items will be also be linked to the event.
     *
     * @param parent the parent act. An <em>act.patientClinicalEvent</em> or <em>>act.patientClinicalProblem</em>
     * @param child  the child act
     */
    public void linkMedicalRecords(Act parent, Act child) {
        if (TypeHelper.isA(child, PatientArchetypes.CLINICAL_PROBLEM)) {
            linkMedicalRecords(parent, child, null, null);
        } else if (!TypeHelper.isA(child, PatientArchetypes.CLINICAL_ADDENDUM)) {
            linkMedicalRecords(parent, null, child, null);
        } else {
            linkMedicalRecords(parent, null, null, child);
        }
    }

    /**
     * Links a patient medical record to an <em>act.patientClinicalEvent</em>,
     * and optionally an <em>act.patientClinicalProblem</em>, if no relationship exists.
     * <p/>
     * If {@code problem} is specified:
     * <ul>
     * <li>it will be linked to the event, if no relationship exists
     * <li>any of its items not presently linked to the event will be linked
     * </ul>
     *
     * @param event    the <em>act.patientClinicalEvent</em>. May be {@code null}
     * @param problem  the <em>act.patientClinicalProblem</em>. May be {@code null}
     * @param item     the patient medical record or charge item. May be {@code null}
     * @param addendum the addendum. If specified, the item must be a note or medication. May be {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void linkMedicalRecords(Act event, Act problem, Act item, Act addendum) {
        if (event != null && !event.isA(PatientArchetypes.CLINICAL_EVENT)) {
            throw new IllegalArgumentException("Argument 'event' is of the wrong type: " + event.getArchetype());
        }
        if (TypeHelper.isA(item, PatientArchetypes.CLINICAL_PROBLEM)) {
            throw new IllegalArgumentException("Argument 'item' is of the wrong type: " + item.getArchetype());
        }
        if (problem != null && !problem.isA(PatientArchetypes.CLINICAL_PROBLEM)) {
            throw new IllegalArgumentException("Argument 'problem' is of the wrong type: " + problem.getArchetype());
        }
        if (addendum != null && !addendum.isA(PatientArchetypes.CLINICAL_ADDENDUM)) {
            throw new IllegalArgumentException("Argument 'addendum' is of the wrong type: "
                                               + addendum.getArchetype());
        }
        Set<Act> changed = new HashSet<>();
        if (item != null && addendum != null) {
            linkAddendumToItem(item, addendum, changed);
        }
        if (problem != null) {
            linkToProblem(problem, item, addendum, changed);
        }
        if (event != null) {
            linkToEvent(event, item, addendum, changed);
        }
        if (!changed.isEmpty()) {
            service.save(changed);
        }

        if (problem != null) {
            linkProblemToEvent(event, problem);
        }
    }

    /**
     * Adds a list of <em>act.patientMedication</em>, <em>act.patientInvestigation*</em>,
     * <em>act.patientDocument*</em> and <em>act.customerAccountInvoiceItem</em> acts to an
     * <em>act.patientClinicalEvent</em> associated with each act's patient.
     * <p/>
     * If an act already has a relationship, but it belongs to a different patient, the relationship will be removed
     * and a relationship to the patient's own event added.
     *
     * @param acts      the acts to add
     * @param startTime the startTime used to select the event
     */
    public void addToEvents(List<Act> acts, Date startTime) {
        PatientHistoryChanges changes = new PatientHistoryChanges(null, service);
        changes.addToEvents(acts, startTime);
        changes.save();
    }

    /**
     * Returns an <em>act.patientClinicalEvent</em> that may have acts added.
     * <p/>
     * The <em>act.patientClinicalEvent</em> is selected as follows:
     * <ol>
     * <li>find the event intersecting the start time for the patient
     * <li>if it is an <em>IN_PROGRESS</em> and
     * <pre>event.startTime &gt;= (startTime - 1 week)</pre>
     * use it
     * <li>if it is <em>COMPLETED</em> and
     * <pre>startTime &gt;= event.startTime && startTime <= event.endTime</pre>
     * use it; otherwise
     * <li>create a new event, with <em>IN_PROGRESS</em> status and startTime
     * </ol>
     *
     * @param patient   the patient
     * @param startTime the start time
     * @param clinician the clinician. May be {@code null}
     * @return an event. May be newly created
     */
    public Act getEventForAddition(Party patient, Date startTime, Entity clinician) {
        Reference clinicianRef = (clinician != null) ? clinician.getObjectReference() : null;
        return getEventForAddition(patient.getObjectReference(), startTime, clinicianRef);
    }

    /**
     * The <em>act.patientClinicalEvent</em> is selected as follows:
     * <ol>
     * <li>find the event intersecting the start time for the patient
     * <li>if it is an <em>IN_PROGRESS</em> and
     * <pre>event.startTime &gt;= (startTime - 1 week)</pre>
     * use it
     * <li>if it is <em>COMPLETED</em> and
     * <pre>startTime &gt;= event.startTime && startTime <= event.endTime</pre>
     * use it; otherwise
     * <li>create a new event, with <em>IN_PROGRESS</em> status and startTime
     * </ol>
     *
     * @param patient   the patient
     * @param startTime the start time
     * @param clinician the clinician. May be {@code null}
     * @return an event. May be newly created
     */
    public Act getEventForAddition(Reference patient, Date startTime, Reference clinician) {
        PatientHistoryChanges events = new PatientHistoryChanges(null, service);
        return events.getEventForAddition(patient, startTime, clinician);
    }

    /**
     * Creates a new <em>act.patientClinicalEvent</em> for the patient and start time.
     *
     * @param patient   the patient
     * @param startTime the event start time
     * @param clinician the clinician. May be {@code null}
     * @return a new event
     */
    public Act createEvent(Party patient, Date startTime, Entity clinician) {
        Reference clinicianRef = (clinician != null) ? clinician.getObjectReference() : null;
        return ClinicalEventHelper.createEvent(patient.getObjectReference(), startTime, clinicianRef, service);
    }

    /**
     * Adds a list of <em>act.patientMedication</em>, <em>act.patientInvestigation*</em>
     * and <em>act.patientDocument*</em> acts to the <em>act.patientClinicalEvent</em> associated with each act's
     * patient and the specified date. The event is obtained via {@link #getEvent(Reference, Date)}.
     * If no event exists, one will be created. If a relationship exists, it
     * will be ignored.
     *
     * @param acts the acts to add
     * @param date the event date
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void addToHistoricalEvents(List<Act> acts, Date date) {
        Map<Reference, List<Act>> map = PatientHistoryChanges.getByPatient(acts, service);
        for (Map.Entry<Reference, List<Act>> entry : map.entrySet()) {
            Reference patient = entry.getKey();
            Act event = getEvent(patient, date);
            if (event == null) {
                event = ClinicalEventHelper.createEvent(patient, date, null, service);
                event.setStatus(ActStatus.COMPLETED);
            }
            boolean save = false;
            IMObjectBean bean = service.getBean(event);
            for (Act a : entry.getValue()) {
                if (bean.getValue(ITEMS, Relationship.class, Predicates.targetEquals(a)) == null) {
                    ActRelationship relationship = (ActRelationship) bean.addTarget(ITEMS, a);
                    a.addActRelationship(relationship);
                    save = true;
                }
            }
            if (save) {
                bean.save();
            }
        }
    }

    /**
     * Returns an <em>act.patientClinicalEvent</em> for the specified patient.
     *
     * @param patient the patient
     * @return the corresponding <em>act.patientClinicalEvent</em> or {@code null} if none is found. The event may be
     * <em>IN_PROGRESS</em> or <em>COMPLETED</em>
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Act getEvent(Party patient) {
        return getEvent(patient.getObjectReference());
    }

    /**
     * Returns the most recent <em>act.patientClinicalEvent</em> for the specified patient.
     *
     * @param patient the patient
     * @return the corresponding <em>act.patientClinicalEvent</em> or
     * {@code null} if none is found. The event may be
     * <em>IN_PROGRESS</em> or <em>COMPLETED}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Act getEvent(Reference patient) {
        ArchetypeQuery query = new ArchetypeQuery(PatientArchetypes.CLINICAL_EVENT, true, true);
        query.add(new CollectionNodeConstraint(PATIENT).add(new ObjectRefNodeConstraint("entity", patient)));
        query.add(new NodeSortConstraint(START_TIME, false));

        // ensure that for events with the same start time, the one with the highest id is used
        query.add(new NodeSortConstraint("id", false));

        query.setMaxResults(1);
        QueryIterator<Act> iter = new IMObjectQueryIterator<>(service, query);
        return (iter.hasNext()) ? iter.next() : null;
    }

    /**
     * Returns an <em>act.patientClinicalEvent</em> for the specified patient
     * and date.
     *
     * @param patient the patient
     * @param date    the date
     * @return the corresponding <em>act.patientClinicalEvent</em> or
     * {@code null} if none is found. The event may be
     * <em>IN_PROGRESS</em> or <em>COMPLETED}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Act getEvent(Party patient, Date date) {
        return getEvent(patient.getObjectReference(), date);
    }

    /**
     * Returns an <em>act.patientClinicalEvent<em> for the specified patient reference and date.
     * NOTES:
     * <ul>
     * <li>this method will return the closest matching event for the specified date, ignoring any time component if
     * there is no exact match.</li>
     * <li>if two events have the same timestamp, the event with the smaller id will be returned</li>
     * </ul>
     *
     * @param patient the patient reference
     * @param date    the date
     * @return the corresponding <em>act.patientClinicalEvent</em> or {@code null} if none is found. The event may be
     * <em>IN_PROGRESS</em> or <em>COMPLETED}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public Act getEvent(Reference patient, Date date) {
        return ClinicalEventHelper.getEvent(patient, date, service);
    }

    /**
     * Determines if a medical record needs to be locked, given a period relative to the current time.
     *
     * @param act    the record act
     * @param period the period prior to the current from which medical records should be locked
     * @return {@code true} if the record needs locking
     */
    public boolean needsLock(Act act, Period period) {
        return needsLock(act, new DateTime().minus(period).toDate());
    }

    /**
     * Determines if a medical record needs to be locked.
     *
     * @param act      the record act
     * @param lockTime the time that medical records should be locked on or prior to
     * @return {@code true} if the record needs locking
     */
    public boolean needsLock(Act act, Date lockTime) {
        return !ActStatus.POSTED.equals(act.getStatus())
               && TypeHelper.isA(act, getLockableRecords())
               && DateRules.compareTo(act.getActivityStartTime(), lockTime) <= 0;
    }

    /**
     * Returns the archetype short names of patient medical records that may be locked.
     *
     * @return the archetype short names of patient medical records that may be locked
     */
    public synchronized String[] getLockableRecords() {
        if (lockableRecords == null) {
            List<String> items = new ArrayList<>(Arrays.asList(getClinicalEventItems()));
            items.remove(PatientArchetypes.CLINICAL_PROBLEM);
            Set<String> versionTargets = new HashSet<>();
            for (String item : items) {
                NodeDescriptor versions = DescriptorHelper.getNode(item, "versions", service);
                if (versions != null) {
                    for (String relationship : DescriptorHelper.getShortNames(versions, service)) {
                        String[] targets = DescriptorHelper.getNodeShortNames(relationship, TARGET, service);
                        versionTargets.addAll(Arrays.asList(targets));
                    }
                }
            }
            items.addAll(versionTargets);
            lockableRecords = items.toArray(new String[0]);
        }
        return lockableRecords;
    }

    /**
     * Returns the most recent attachment with the specified file name associated with a patient clinical event.
     *
     * @param fileName the file name
     * @param event    the <em>act.patientClinicalEvent</em>
     * @return the attachment, or {@code null} if none is found
     */
    public DocumentAct getAttachment(String fileName, Act event) {
        ArchetypeQuery query = createAttachmentQuery(fileName, event);
        IMObjectQueryIterator<DocumentAct> iterator = new IMObjectQueryIterator<>(service, query);
        return iterator.hasNext() ? iterator.next() : null;
    }

    /**
     * Returns the most recent attachment with the specified file name and identity, associated with a patient clinical
     * event.
     *
     * @param fileName          the file name
     * @param event             the <em>act.patientClinicalEvent</em>
     * @param identityArchetype the identity archetype
     * @param identity          the identity
     * @return the attachment, or {@code null} if none is found
     */
    public DocumentAct getAttachment(String fileName, org.openvpms.component.model.act.Act event,
                                     String identityArchetype, String identity) {
        ArchetypeQuery query = createAttachmentQuery(fileName, event);
        query.add(Constraints.join("identities", shortName(identityArchetype)).add(eq("identity", identity)));
        IMObjectQueryIterator<DocumentAct> iterator = new IMObjectQueryIterator<>(service, query);
        return iterator.hasNext() ? iterator.next() : null;
    }

    /**
     * Links an item and/or addendum to a clinical event.
     *
     * @param event    the clinical event
     * @param item     the item to link. May be {@code null}
     * @param addendum the addendum. May be {@code null}
     * @param changed  collects the changed acts
     */
    private void linkToEvent(Act event, Act item, Act addendum, Set<Act> changed) {
        IMObjectBean bean = service.getBean(event);
        if (item != null && (item.isA(CustomerAccountArchetypes.INVOICE_ITEM) || item.isA(getClinicalEventItems()))) {
            linkItemToEvent(bean, item, changed);
        }
        if (addendum != null) {
            linkItemToEvent(bean, addendum, changed);
        }
    }

    /**
     * Links an item and/or addendum to a problem.
     *
     * @param problem  the problem
     * @param item     the item to link. May be {@code null}
     * @param addendum the addendum. May be {@code null}
     * @param changed  collects the changed acts
     */
    private void linkToProblem(Act problem, Act item, Act addendum, Set<Act> changed) {
        IMObjectBean bean = service.getBean(problem);
        if (item != null && TypeHelper.isA(item, getClinicalProblemItems())) {
            linkItemToProblem(bean, item, changed);
        }
        if (addendum != null) {
            linkItemToProblem(bean, addendum, changed);
        }
    }

    /**
     * Returns the valid short names for an event item relationship.
     *
     * @return the short names
     */
    private synchronized String[] getClinicalEventItems() {
        if (clinicalEventItems == null) {
            clinicalEventItems = DescriptorHelper.getNodeShortNames(PatientArchetypes.CLINICAL_EVENT_ITEM, TARGET,
                                                                    service);
        }
        return clinicalEventItems;
    }

    /**
     * Returns the valid short names for a problem item relationship.
     *
     * @return the short names
     */
    private synchronized String[] getClinicalProblemItems() {
        if (clinicalProblemItems == null) {
            clinicalProblemItems = DescriptorHelper.getNodeShortNames(PatientArchetypes.CLINICAL_PROBLEM_ITEM, TARGET,
                                                                      service);
        }
        return clinicalProblemItems;
    }

    /**
     * Links an item to an <em>act.patientClinicalProblem</em>, if no relationship currently exists.
     *
     * @param bean    the problem
     * @param item    the item to link
     * @param changed collects the changed acts
     */
    private void linkItemToProblem(IMObjectBean bean, Act item, Set<Act> changed) {
        // link the problem and item if required
        if (addRelationship(bean, ITEMS, item)) {
            changed.add(bean.getObject(Act.class));
            changed.add(item);
        }
    }

    /**
     * Links an item to an <em>act.patientClinicalEvent</em>, if no relationship currently exists.
     *
     * @param bean    the event
     * @param item    the item to link
     * @param changed collects the changed acts
     */
    private void linkItemToEvent(IMObjectBean bean, Act item, Set<Act> changed) {
        Act event = bean.getObject(Act.class);
        // link the event and item
        if (TypeHelper.isA(item, CustomerAccountArchetypes.INVOICE_ITEM)) {
            if (addRelationship(bean, "chargeItems", item)) {
                changed.add(event);
                changed.add(item);
            }
        } else if (addRelationship(bean, ITEMS, item)) {
            changed.add(event);
            changed.add(item);
        }
    }

    /**
     * Adds a relationship between two acts, if none exists.
     *
     * @param bean   the source act bean
     * @param node   the relationship node
     * @param target the target act
     * @return {@code true} if the relationship was added
     */
    private boolean addRelationship(IMObjectBean bean, String node, Act target) {
        boolean result = false;
        if (bean.getValue(node, ActRelationship.class, Predicates.targetEquals(target)) == null) {
            ActRelationship relationship = (ActRelationship) bean.addTarget(node, target);
            target.addActRelationship(relationship);
            result = true;
        }
        return result;
    }

    /**
     * Links an <em>act.patientClinicalProblem> and its acts to an <em>act.patientClinicalEvent</em>,
     * if no relationship currently exists.
     *
     * @param event   the event
     * @param problem the problem to link
     */
    private void linkProblemToEvent(Act event, Act problem) {
        // link the event and problem
        IMObjectBean problemBean = service.getBean(problem);
        List<Act> toSave = new ArrayList<>();

        IMObjectBean bean = service.getBean(event);
        // if the problem is not linked to the event, add it
        if (addRelationship(bean, ITEMS, problem)) {
            toSave.add(event);
        }

        // add each of the problem's child acts not linked to an event
        List<Act> acts = problemBean.getTargets(ITEMS, Act.class);
        if (!acts.isEmpty()) {
            String[] shortNames = getClinicalEventItems();
            for (Act child : acts) {
                IMObjectBean childBean = service.getBean(child);
                if (childBean.isA(shortNames) && addRelationship(bean, ITEMS, child)) {
                    toSave.add(child);
                }
            }
        }
        if (!toSave.isEmpty()) {
            service.save(toSave);
        }
    }

    /**
     * Links an addendum to an <em>act.patientClinicalNote</em> or <em>act.patientMedication</em>, if no relationship
     * currently exists.
     *
     * @param item     the item
     * @param addendum the addendum to link
     */
    private void linkAddendumToItem(Act item, Act addendum, Set<Act> changed) {
        IMObjectBean bean = service.getBean(item);
        if (!addRelationship(bean, "addenda", addendum)) {
            changed.add(item);
            changed.add(addendum);
        }
    }

    /**
     * Creates a query to locate attachments with the specified file name, associated with an event.
     *
     * @param fileName the file name
     * @param event    the <em>act.patientClinicalEvent</em>
     * @return a new query
     */
    private ArchetypeQuery createAttachmentQuery(String fileName, org.openvpms.component.model.act.Act event) {
        ArchetypeQuery query = new ArchetypeQuery(PatientArchetypes.DOCUMENT_ATTACHMENT);
        query.add(join("event").add(eq("source", event)));
        query.add(eq("fileName", fileName));
        query.add(sort(START_TIME, false));
        query.add(sort("id", false));
        query.setMaxResults(1);
        return query;
    }

}
