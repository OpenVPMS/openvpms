/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.eft;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;

/**
 * EFTPOS transaction status.
 *
 * @author Tim Anderson
 */
public class EFTPOSTransactionStatus {

    /**
     * EFTPOS transaction hasn't been submitted.
     */
    public static final String PENDING = WorkflowStatus.PENDING;

    /**
     * EFTPOS transaction has been submitted.
     */
    public static final String IN_PROGRESS = ActStatus.IN_PROGRESS;

    /**
     * EFTPOS transaction has been approved.
     */
    public static final String APPROVED = "APPROVED";

    /**
     * EFTPOS transaction has been DECLINED.
     */
    public static final String DECLINED = "DECLINED";

    /**
     * EFTPOS transaction failed with an error.
     */
    public static final String ERROR = "ERROR";

    /**
     * Status indicating that an EFTPOS transaction was performed manually via terminal as the provider was offline.
     */
    public static final String NO_TERMINAL = "NO_TERMINAL";

    /**
     * Default constructor.
     */
    private EFTPOSTransactionStatus() {

    }
}
