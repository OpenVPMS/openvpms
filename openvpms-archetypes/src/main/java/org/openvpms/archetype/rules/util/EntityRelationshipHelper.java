/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.util;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.IMObjectBeanException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.List;


/**
 * Entity relationship helper methods.
 *
 * @author Tim Anderson
 */
public class EntityRelationshipHelper {

    /**
     * Default node name.
     */
    private static final String DEFAULT = "default";

    /**
     * Default constructor.
     */
    private EntityRelationshipHelper() {
        // no-op
    }

    /**
     * Returns the target from the default entity relationship from the specified relationship node.
     * <p/>
     * Note that this can return a reference to an inactive object.
     *
     * @param entity  the parent entity
     * @param node    the relationship node
     * @param service the archetype service
     * @return the default target, or the first target if there is no default, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws IMObjectBeanException     if the node doesn't exist or an element is of the wrong type
     */
    public static Reference getDefaultTargetRef(Entity entity, String node, ArchetypeService service) {
        return getDefaultTargetRef(entity, node, true, service);
    }

    /**
     * Returns the target from the default entity relationship from the specified relationship node.
     * <p/>
     * Note that this can return a reference to an inactive object.
     *
     * @param entity        the parent entity
     * @param node          the relationship node
     * @param useNonDefault if {@code true} use a non-default relationship if no default can be found
     * @param service       the archetype service
     * @return the default target, or the first target if there is no
     * default, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws IMObjectBeanException     if the node doesn't exist or an element is of the wrong type
     */
    public static Reference getDefaultTargetRef(Entity entity, String node, boolean useNonDefault,
                                                ArchetypeService service) {
        IMObjectBean bean = service.getBean(entity);
        return getDefaultTargetRef(bean.getValues(node, EntityRelationship.class), useNonDefault, service);
    }

    /**
     * Returns the active target from the default entity relationship from the specified relationship node.
     *
     * @param entity  the parent entity
     * @param node    the relationship node
     * @param service the archetype service
     * @return the default target, or the first target if there is no default, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws IMObjectBeanException     if the node doesn't exist or an element is of the wrong type
     */
    public static Entity getDefaultTarget(Entity entity, String node, ArchetypeService service) {
        return getDefaultTarget(entity, node, true, service);
    }

    /**
     * Returns the active target from the default entity relationship from the specified relationship node.
     *
     * @param entity        the parent entity
     * @param node          the relationship node
     * @param useNonDefault if {@code true} use a non-default relationship if no default can be found
     * @param service       the archetype service
     * @return the default target, or the first target if there is no default and {@code useNonDefault} is
     * {@code true}, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws IMObjectBeanException     if the node doesn't exist or an element is of the wrong type
     */
    public static Entity getDefaultTarget(Entity entity, String node, boolean useNonDefault,
                                          ArchetypeService service) {
        IMObjectBean bean = service.getBean(entity);
        return getDefaultTarget(bean.getValues(node, Relationship.class), useNonDefault, service);
    }

    /**
     * Returns the active target from the default entity relationship from the supplied relationship list.
     *
     * @param relationships a list of relationship objects
     * @param service       the archetype service
     * @return the default target, or the first target if there is no
     * default, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static Entity getDefaultTarget(List<Relationship> relationships, ArchetypeService service) {
        return getDefaultTarget(relationships, true, service);
    }

    /**
     * Returns the active target from the default entity relationship from the supplied relationship list.
     *
     * @param relationships a list of relationship objects
     * @param useNonDefault if {@code true} use a non-default relationship if no default can be found
     * @param service       the archetype service
     * @return the default target, or the first target if there is no default and {@code useNonDefault}
     * is {@code true}, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static Entity getDefaultTarget(List<Relationship> relationships, boolean useNonDefault,
                                          ArchetypeService service) {
        Entity result = null;
        for (Relationship relationship : relationships) {
            if (relationship.isActive()) {
                if (result == null && useNonDefault) {
                    result = getEntity(relationship.getTarget(), service);
                } else {
                    if (isDefault(relationship, service)) {
                        Entity entity = getEntity(relationship.getTarget(), service);
                        if (entity != null) {
                            result = entity;
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the active target reference from the default entity relationship
     * from the supplied relationship list.
     * <p/>
     * Note that this can return a reference to an inactive object.
     *
     * @param relationships a list of relationship objects
     * @param service       the archetype service
     * @return the default target, or the first target if there is no default, or {@code null} if none is found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static Reference getDefaultTargetRef(List<EntityRelationship> relationships, boolean useNonDefault,
                                                ArchetypeService service) {
        Reference result = null;
        for (EntityRelationship relationship : relationships) {
            if (relationship.isActive()) {
                if (result == null && useNonDefault) {
                    result = relationship.getTarget();
                } else if (isDefault(relationship, service)) {
                    result = relationship.getTarget();
                    if (result != null) {
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Makes a relationship the default, resetting the default status of any
     * other relationship associated with the specified node.
     *
     * @param entity       the entity
     * @param node         the relationship node
     * @param relationship the relationship to make the default
     * @param service      the archetype service
     * @throws ArchetypeServiceException for any archetype service error
     */
    public static void setDefault(Entity entity, String node, Relationship relationship, ArchetypeService service) {
        IMObjectBean bean = service.getBean(entity);
        List<Relationship> relationships = bean.getValues(node, Relationship.class);
        for (Relationship r : relationships) {
            if (r != relationship) {
                IMObjectBean relBean = service.getBean(r);
                relBean.setValue(DEFAULT, false);
            }
        }
        IMObjectBean relBean = service.getBean(relationship);
        relBean.setValue(DEFAULT, true);
    }

    /**
     * Determines if a relationship is the default.
     *
     * @param relationship the relationship
     * @param service      the archetype service
     * @return {@code true} if the relationship is the default
     */
    private static boolean isDefault(Relationship relationship, ArchetypeService service) {
        IMObjectBean bean = service.getBean(relationship);
        return bean.hasNode(DEFAULT) && bean.getBoolean(DEFAULT);
    }

    /**
     * Returns the entity associate with a reference.
     *
     * @param ref     the reference. May be {@code null}
     * @param service the archetype service.
     * @return the corresponding entity, or {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    private static Entity getEntity(Reference ref, ArchetypeService service) {
        Entity result = null;
        if (ref != null) {
            result = (Entity) service.get(ref, true);
        }
        return result;
    }

}
