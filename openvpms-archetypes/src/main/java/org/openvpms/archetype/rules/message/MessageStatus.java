/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.message;


import org.openvpms.archetype.rules.workflow.WorkflowStatus;

/**
 * Act status types for <em>act.userMessage</em>, <em>act.systemMessage</em>, <em>act.auditMessage</em>,
 * <em>act.smsMessage</em> and <em>act.smsReply</em> acts.
 *
 * @author Tim Anderson
 */
public class MessageStatus {

    /**
     * Pending status.
     */
    public static final String PENDING = WorkflowStatus.PENDING;

    /**
     * Read status.
     */
    public static final String READ = "READ";

    /**
     * Completed status.
     */
    public static final String COMPLETED = WorkflowStatus.COMPLETED;

    /**
     * Default constructor.
     */
    private MessageStatus() {
        // no-op
    }
}