/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * Abstract implementation of {@link ImageService}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractImageService implements ImageService {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link AbstractImageService}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     */
    public AbstractImageService(DocumentHandlers handlers, ArchetypeService service) {
        this.handlers = handlers;
        this.service = service;
    }

    /**
     * Returns a URL to the image associated with an act.
     *
     * @param act the act
     * @return the URL or {@code null} if the act has no image
     * @throws DocumentException for any error
     */
    @Override
    public URL getURL(DocumentAct act) {
        URL result;
        Image image = getImage(act);
        try {
            result = image != null ? image.getURL() : null;
        } catch (Exception exception) {
            throw new DocumentException(DocumentException.ErrorCode.ReadError, exception, act.getFileName());
        }
        return result;
    }

    /**
     * Returns an image associated with an act.
     *
     * @param act the act
     * @return the image, or {@code null} if the act has no image
     * @throws DocumentException for any error
     */
    @Override
    public Image getImage(DocumentAct act) {
        String fileName = act.getId() + "_" + act.getVersion() + "_" + sanitizeFilename(act);
        File dir = getDir();
        File file = new File(dir, fileName);
        Image result;
        try {
            if (!file.exists()) {
                Document document = service.get(act.getDocument(), Document.class);
                if (document != null) {
                    DocumentHandler handler = handlers.get(document);
                    File tmp = File.createTempFile("img", null, dir);
                    FileCopyUtils.copy(handler.getContent(document), new FileOutputStream(tmp));
                    if (!tmp.renameTo(file)) {
                        throw new DocumentException(DocumentException.ErrorCode.RenameError, tmp.getAbsolutePath(),
                                                    file.getAbsolutePath());
                    }
                }
            }
            result = new ImageImpl(file, act.getMimeType());
        } catch (IOException exception) {
            throw new DocumentException(DocumentException.ErrorCode.ReadError, exception, act.getFileName());
        }
        return result;
    }

    /**
     * Returns the directory to store images.
     *
     * @return the directory
     */
    protected abstract File getDir();

    /**
     * Cleans the filename associated with an act.
     *
     * @param act the act
     * @return the cleaned filename, or an empty string if none is present
     */
    private String sanitizeFilename(DocumentAct act) {
        String filename = act.getFileName();
        return filename != null ? filename.replaceAll("[^a-zA-Z0-9.]", "_") : "";
    }

    private static class ImageImpl implements Image {

        private final File file;

        private final String mimeType;

        public ImageImpl(File file, String mimeType) {
            this.file = file;
            this.mimeType = mimeType;
        }

        /**
         * Returns a URL to the image.
         *
         * @return the image URL
         * @throws IOException if the image cannot be read
         */
        @Override
        public URL getURL() throws IOException {
            return file.toURI().toURL();
        }

        /**
         * Returns a stream to the image.
         *
         * @return the stream
         * @throws IOException if the image cannot be read
         */
        @Override
        public InputStream getInputStream() throws IOException {
            return getURL().openStream();
        }

        /**
         * Returns the image mime type.
         *
         * @return the mime type
         */
        @Override
        public String getMimeType() {
            return mimeType;
        }

        /**
         * Returns the image size.
         *
         * @return the image size, in bytes, or {@code 0} if it cannot be determined
         */
        @Override
        public long getSize() {
            long result = 0;
            try {
                result = file.length();
            } catch (SecurityException ignore) {
                // no-op
            }
            return result;
        }

        /**
         * Returns the time when the image was last modified.
         *
         * @return the last modified time, or {@code null} if it cannot be determined
         */
        @Override
        public Date getModified() {
            Date result = null;
            try {
                long modified = file.lastModified();
                if (modified > 0) {
                    result = new Date(modified);
                }
            } catch (SecurityException ignore) {
                // no-op
            }
            return result;
        }
    }
}