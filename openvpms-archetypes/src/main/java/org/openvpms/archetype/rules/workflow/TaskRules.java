/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Task rules.
 *
 * @author Tim Anderson
 */
public class TaskRules {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TaskRules}.
     *
     * @param service the service
     */
    public TaskRules(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the work list associated with a task.
     *
     * @param task the task
     * @return the corresponding work list
     */
    public Entity getWorkList(Act task) {
        IMObjectBean bean = service.getBean(task);
        return bean.getTarget("worklist", Entity.class);
    }

    /**
     * Returns the first view that contain the specified work list for a given practice location.
     *
     * @param location the practice location
     * @param worklist the work list
     * @return the view, or {@code null} if none is found
     */
    public Entity getWorkListView(Party location, Entity worklist) {
        IMObjectBean bean = service.getBean(location);
        for (Entity view : bean.getTargets("workListViews", Entity.class, Policies.orderBySequence())) {
            IMObjectBean viewBean = service.getBean(view);
            if (viewBean.hasTarget("workLists", worklist)) {
                return view;
            }
        }
        return null;
    }

    /**
     * Determines if a work list has a task type.
     *
     * @param worklist the work list
     * @param taskType the task type
     * @return {@code true} if the work list has the task type, otherwise {@code false}
     */
    public boolean hasTaskType(Entity worklist, Entity taskType) {
        IMObjectBean bean = service.getBean(worklist);
        return bean.hasTarget("taskTypes", taskType);
    }
}