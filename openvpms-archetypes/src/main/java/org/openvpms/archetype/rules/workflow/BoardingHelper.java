/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Boarding helper methods.
 *
 * @author Tim Anderson
 */
public class BoardingHelper {

    /**
     * Determines if an appointment is a boarding appointment.
     * <p/>
     * NOTE: moved from {@link AppointmentRules} to enable use of cached-backed ArchetypeService instances.
     *
     * @param appointment the appointment
     * @param service     the archetype service
     * @return {@code true} if the appointment is a boarding appointment
     */
    public static boolean isBoardingAppointment(Act appointment, ArchetypeService service) {
        boolean result = false;
        IMObjectBean bean = service.getBean(appointment);
        Entity schedule = bean.getTarget("schedule", Entity.class);
        if (schedule != null) {
            IMObjectBean scheduleBean = service.getBean(schedule);
            if (scheduleBean.getTargetRef("cageType") != null) {
                result = true;
            }
        }
        return result;
    }
}