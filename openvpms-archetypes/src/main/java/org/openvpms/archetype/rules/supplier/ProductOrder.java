/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.openvpms.component.model.object.Reference;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Order details for a product.
 *
 * @author Tim Anderson
 */
public class ProductOrder {

    /**
     * The supplier.
     */
    private final Reference supplier;

    /**
     * The supplier name.
     */
    private final String supplierName;

    /**
     * The order.
     */
    private final Reference order;

    /**
     * The order date.
     */
    private final Date date;

    /**
     * The order quantity.
     */
    private final BigDecimal quantity;

    /**
     * The package size.
     */
    private final Integer packageSize;

    /**
     * The package units
     */
    private final String packageUnits;

    /**
     * Constructs a {@link ProductOrder}.
     *
     * @param order        the order reference
     * @param date         the order date
     * @param supplier     the supplier reference
     * @param supplierName the supplier name
     * @param quantity     the undelivered quantity
     * @param packageSize  the package size. May be {@code null}
     * @param packageUnits the package units. May be {@code null}
     */
    public ProductOrder(Reference order, Date date, Reference supplier, String supplierName, BigDecimal quantity,
                        Integer packageSize, String packageUnits) {
        this.order = order;
        this.date = date;
        this.supplier = supplier;
        this.supplierName = supplierName;
        this.quantity = quantity;
        this.packageSize = packageSize;
        this.packageUnits = packageUnits;
    }

    /**
     * Returns the order
     *
     * @return the order reference
     */
    public Reference getOrder() {
        return order;
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier reference
     */
    public Reference getSupplier() {
        return supplier;
    }

    /**
     * Returns the supplier name.
     *
     * @return the supplier name
     */
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * Returns the order date.
     *
     * @return the order date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Returns the quantity that has yet to be delivered.
     *
     * @return the undelivered quantity
     */
    public BigDecimal getUndelivered() {
        return quantity;
    }

    /**
     * Returns the package size.
     *
     * @return the package size. May be {@code null}
     */
    public Integer getPackageSize() {
        return packageSize;
    }

    /**
     * Returns the package units.
     *
     * @return the package units. May be {@code null}
     */
    public String getPackageUnits() {
        return packageUnits;
    }
}