/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * Manages documents associated with a charge item.
 *
 * @author Tim Anderson
 */
public class ChargeItemDocumentLinker {

    /**
     * The charge item.
     */
    private final FinancialAct item;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The product node name.
     */
    private static final String PRODUCT = "product";

    /**
     * The documents node name.
     */
    private static final String DOCUMENTS = "documents";

    /**
     * The patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * The clinician node name.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Constructs a {@link ChargeItemDocumentLinker}.
     *
     * @param item    the item
     * @param service the archetype service
     */
    public ChargeItemDocumentLinker(FinancialAct item, IArchetypeService service) {
        this.item = item;
        this.service = service;
    }

    /**
     * Creates or deletes acts related to the invoice item based on the document templates associated with the
     * charge item's product.
     * This:
     * <ol>
     * <li>gets all document templates associated with the product's {@code document} node</li>
     * <li>iterates through the acts associated with the invoice item's {@code document} node and:</li>
     * <ol>
     * <li>removes acts that don't have participation to any of the document templates</li>
     * <li>retains acts which have participations to the document templates</li>
     * </ol>
     * <li>creates acts for each document template that doesn't yet have an act</li>
     * </ol>
     * This is the same as invoking:
     * <pre>
     * prepare(changes);
     * changes.save();
     * </pre>
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void link() {
        PatientHistoryChanges changes = new PatientHistoryChanges(null, service);
        prepare(changes);
        changes.save();
    }

    /**
     * Prepares the charge item and documents for save.
     * <p/>
     * Invoke {@link PatientHistoryChanges#save()} to commit the changes.
     *
     * @param changes the patient history changes
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void prepare(PatientHistoryChanges changes) {
        // map of template references to their corresponding entity relationship, obtained from the product
        Map<Reference, Relationship> productTemplates = new HashMap<>();

        // template references associated with the current document acts
        Set<Reference> templateRefs = new HashSet<>();

        // determine the templates associated with the item's product
        IMObjectBean itemBean = service.getBean(item);
        Product product = itemBean.getTarget(PRODUCT, Product.class);
        if (product != null) {
            productTemplates = getProductDocumentTemplates(product);
        }

        // get document acts associated with the item
        List<Act> documents = itemBean.getTargets(DOCUMENTS, Act.class);

        // for each document, determine if the product, patient, or clinician has changed. If so, remove it
        for (Act document : documents.toArray(new Act[0])) {
            IMObjectBean bean = service.getBean(document);
            if (changed(bean, itemBean, product)) {
                changes.removeItemDocument(item, document);
                documents.remove(document);
            } else {
                Reference templateRef = bean.getTargetRef("documentTemplate");
                if (templateRef != null) {
                    templateRefs.add(templateRef);
                }
            }
        }

        // add any templates associated with the product where there is no corresponding act
        for (Map.Entry<Reference, Relationship> entry : productTemplates.entrySet()) {
            Reference typeRef = entry.getKey();
            if (!templateRefs.contains(typeRef)) {
                Entity entity = (Entity) getObject(typeRef);
                if (entity != null) {
                    addDocument(itemBean, entity, changes);
                }
            }
        }
    }

    /**
     * Returns the document templates associated with a product.
     *
     * @param product the product
     * @return a map of template references to their corresponding relationship
     */
    Map<Reference, Relationship> getProductDocumentTemplates(Product product) {
        Map<Reference, Relationship> result = new HashMap<>();
        IMObjectBean productBean = service.getBean(product);
        if (productBean.hasNode(DOCUMENTS)) {
            for (Relationship r : productBean.getValues(DOCUMENTS, Relationship.class)) {
                Reference target = r.getTarget();
                if (target != null) {
                    result.put(target, r);
                }
            }
        }
        return result;
    }

    /**
     * Adds an <em>act.patientDocument*</em> to the invoice item.
     *
     * @param itemBean the invoice item
     * @param document the document template
     * @param changes  the patient history changes
     * @throws ArchetypeServiceException for any error
     */
    private void addDocument(IMObjectBean itemBean, Entity document, PatientHistoryChanges changes) {
        IMObjectBean bean = service.getBean(document);
        Lookup typeLookup = bean.getObject("type", Lookup.class);
        String type = (typeLookup != null) ? typeLookup.getCode() : PatientArchetypes.DOCUMENT_FORM;
        if (TypeHelper.matches(type, "act.patientDocument*")) {
            Act act = service.create(type, Act.class);
            act.setActivityStartTime(((Act) itemBean.getObject()).getActivityStartTime());
            IMObjectBean documentAct = service.getBean(act);
            Reference patient = itemBean.getTargetRef(PATIENT);
            documentAct.setTarget(PATIENT, patient);
            documentAct.setTarget("documentTemplate", document);
            Reference clinician = itemBean.getTargetRef(CLINICIAN);
            if (clinician != null) {
                documentAct.setTarget(CLINICIAN, clinician);
            }

            if (documentAct.isA(PatientArchetypes.DOCUMENT_FORM, PatientArchetypes.DOCUMENT_LETTER)) {
                Reference product = itemBean.getTargetRef(PRODUCT);
                documentAct.setTarget(PRODUCT, product);
            }
            changes.addItemDocument(item, act);
        }
    }

    /**
     * Determines if the product has changed.
     *
     * @param bean    the document act bean
     * @param product the item product
     * @return {@code true} if the product has changed
     */
    private boolean productChanged(IMObjectBean bean, Product product) {
        return bean.hasNode(PRODUCT)
               && !Objects.equals(bean.getTargetRef(PRODUCT), product.getObjectReference());
    }

    /**
     * Determines if the product, patient or clinician has changed on a document.
     *
     * @param docBean  the document act bean
     * @param itemBean the item bean
     * @param product  the product
     * @return {@code true} if at least one of the attributes has changed
     */
    private boolean changed(IMObjectBean docBean, IMObjectBean itemBean, Product product) {
        return productChanged(docBean, product) || patientChanged(docBean, itemBean)
               || clinicianChanged(docBean, itemBean);
    }


    /**
     * Determines if the patient has changed.
     *
     * @param docBean  the document act bean
     * @param itemBean the item bean
     * @return {@code true} if the patient has changed
     */
    private boolean patientChanged(IMObjectBean docBean, IMObjectBean itemBean) {
        return checkParticipant(docBean, itemBean, PATIENT);
    }

    /**
     * Determines if the clinician has changed.
     *
     * @param docBean  the document act bean
     * @param itemBean the item bean
     * @return {@code true} if the clinician has changed
     */
    private boolean clinicianChanged(IMObjectBean docBean, IMObjectBean itemBean) {
        return checkParticipant(docBean, itemBean, CLINICIAN);
    }

    /**
     * Determines if a participant has changed.
     *
     * @param docBean  the document act bean
     * @param itemBean the item bean
     * @param node     the participant node to check
     * @return {@code true} if the participant has changed
     */
    private boolean checkParticipant(IMObjectBean docBean, IMObjectBean itemBean, String node) {
        return docBean.hasNode(node) && !Objects.equals(docBean.getTargetRef(node), itemBean.getTargetRef(node));
    }

    /**
     * Helper to retrieve an object given its reference.
     *
     * @param ref the reference
     * @return the object corresponding to the reference, or {@code null}if it can't be retrieved
     * @throws ArchetypeServiceException for any error
     */
    private IMObject getObject(Reference ref) {
        return (ref != null) ? service.get(ref) : null;
    }

}
