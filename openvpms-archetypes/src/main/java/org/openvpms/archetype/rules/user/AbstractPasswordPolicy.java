/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

/**
 * Abstract implementation of {@link PasswordPolicy}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPasswordPolicy implements PasswordPolicy {
    /**
     * The supported special characters.
     */
    protected static final String SPECIAL = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    /**
     * The supported special characters, escaped for use in regular expressions.
     * NOTE that " is escaped for when the string is used in javascript.
     */
    protected static final String SPECIAL_ESCAPED = SPECIAL.replaceAll("([-!\"/^$*+?.()|\\[\\]{}\\\\])", "\\\\$1");

    /**
     * The supported password characters.
     */
    protected static final String VALID_CHARACTERS_ESCAPED = "a-zA-Z0-9" + SPECIAL_ESCAPED;

    /**
     * The maximum password length.
     */
    private static final int MAX_LENGTH = 100;

    /**
     * Returns the maximum password length.
     *
     * @return the maximum password length
     */
    @Override
    public int getMaxLength() {
        return MAX_LENGTH;
    }

    /**
     * Returns the special characters that may appear in passwords.
     *
     * @return the special characters
     */
    @Override
    public String getSpecialCharacters() {
        return SPECIAL;
    }

    /**
     * Returns the supported special characters, escaped for use in regular expressions.
     *
     * @return the supported special characters, with ^, $ etc. escaped with \
     */
    @Override
    public String getSpecialCharactersEscaped() {
        return SPECIAL_ESCAPED;
    }

    /**
     * Returns all valid special characters that may appear in passwords, escaped for use in regular expressions.
     *
     * @return the supported characters, with ^, $ etc. escaped with \
     */
    @Override
    public String getValidCharactersEscaped() {
        return VALID_CHARACTERS_ESCAPED;
    }
}