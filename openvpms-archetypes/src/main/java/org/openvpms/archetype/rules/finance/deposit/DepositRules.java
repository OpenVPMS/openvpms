/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.deposit;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;

import java.util.Date;

import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.DepositAlreadyDeposited;
import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.InvalidDepositArchetype;
import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.MissingAccount;
import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.UndepositedDepositExists;


/**
 * Deposit rules.
 *
 * @author Tim Anderson
 */
public class DepositRules {

    /**
     * Default constructor.
     */
    private DepositRules() {

    }


    /**
     * Rule that determines if an <em>act.bankDeposit</em> can be saved.
     * One can be saved if:
     * <ul>
     * <li>it has status 'Deposited' and was previously 'UnDeposited'</li>
     * <li>it has status 'UnDeposited' and there are no other undeposited
     * act.bankDeposits for the deposit account</li>
     * </ul>
     *
     * @param act     the deposit act
     * @param service the archetype service
     * @throws DepositRuleException if the act can't be saved
     */
    public static void checkCanSaveBankDeposit(FinancialAct act, IArchetypeService service)
            throws DepositRuleException {
        IMObjectBean bean = service.getBean(act);
        if (!bean.isA(DepositArchetypes.BANK_DEPOSIT)) {
            throw new DepositRuleException(InvalidDepositArchetype, act.getArchetype());
        }

        Act oldAct = (Act) service.get(act.getObjectReference());
        if (oldAct != null) {
            // If the act already exists, make sure it hasn't been deposited
            if (DepositStatus.DEPOSITED.equals(oldAct.getStatus())) {
                throw new DepositRuleException(DepositAlreadyDeposited, act.getId());
            }
        } else {
            // its a new bank deposit so if status is undeposited
            // check no other undeposited act exists.
            if (DepositStatus.UNDEPOSITED.equals(act.getStatus())) {
                Entity account = bean.getTarget("depositAccount", Entity.class);
                if (account == null) {
                    throw new DepositRuleException(MissingAccount, act.getId());
                }

                Act match = DepositHelper.getUndepositedDeposit(account, service);
                if (match != null && match.getId() != act.getId()) {
                    throw new DepositRuleException(UndepositedDepositExists, account.getName());
                }
            }
        }
    }

    /**
     * Processes an <em>act.bankDeposit</em>.
     *
     * @param act     the deposit act
     * @param service the archetype service
     */
    public static void deposit(Act act, IArchetypeService service) {
        IMObjectBean bean = service.getBean(act);
        if (!bean.isA(DepositArchetypes.BANK_DEPOSIT)) {
            throw new DepositRuleException(InvalidDepositArchetype, act.getArchetype());
        }
        if (DepositStatus.DEPOSITED.equals(act.getStatus())) {
            throw new DepositRuleException(DepositAlreadyDeposited, act.getStatus());
        }
        act.setStatus(DepositStatus.DEPOSITED);
        IMObject account = bean.getTarget("depositAccount");
        if (account == null) {
            throw new DepositRuleException(MissingAccount, act.getId());
        }

        IMObjectBean accBean = service.getBean(account);
        accBean.setValue("lastDeposit", new Date());
        bean.save(account);
    }
}
