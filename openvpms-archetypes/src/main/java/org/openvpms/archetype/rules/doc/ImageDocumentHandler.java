/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.util.ClassHelper;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.spi.IIORegistry;
import javax.imageio.stream.ImageInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

/**
 * A handler for image documents.
 * <p/>
 * This uses {@code ImageIO} to determine image dimensions.
 *
 * @author Tim Anderson
 */
public class ImageDocumentHandler extends AbstractDocumentHandler {

    /**
     * Constructs an {@link ImageDocumentHandler}.
     *
     * @param service the archetype service
     */
    public ImageDocumentHandler(ArchetypeService service) {
        super(DocumentArchetypes.IMAGE_DOCUMENT, service);
        initImageIO();
    }

    /**
     * Creates a new {@link Document} from a stream.
     *
     * @param name     the document name. Any path information is removed.
     * @param stream   a stream representing the document content
     * @param mimeType the mime type of the content. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     * @return a new document
     * @throws DocumentException         if the document can't be created
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public Document create(String name, InputStream stream, String mimeType, int size) {
        Document document;
        ReusableInputStream buffered = new ReusableInputStream(stream);
        try {
            Dimension dimension = getDimension(buffered);
            buffered.reset();
            buffered.mark(0); // don't want to buffer any longer
            document = super.create(name, buffered, mimeType, size);
            updateDimension(document, dimension);
        } catch (IOException exception) {
            throw new DocumentException(DocumentException.ErrorCode.ReadError, name, exception);
        } finally {
            buffered.realClose();
        }
        return document;
    }

    /**
     * Updates a {@link Document} from a stream.
     *
     * @param document the document to update
     * @param stream   a stream representing the new document content
     * @param mimeType the mime type of the document. May be {@code null}
     * @param size     the size of stream, or {@code -1} if the size is not known
     */
    @Override
    public void update(Document document, InputStream stream, String mimeType, int size) {
        ReusableInputStream buffered = new ReusableInputStream(stream);
        try {
            Dimension dimension = getDimension(buffered);
            buffered.reset();
            buffered.mark(0); // don't want to buffer any longer
            super.update(document, buffered, mimeType, size);
            updateDimension(document, dimension);
        } catch (IOException exception) {
            throw new DocumentException(DocumentException.ErrorCode.ReadError, "stream", exception);
        } finally {
            buffered.realClose();
        }
    }

    /**
     * Initialises ImageIO for OVPMS-2940.
     */
    private void initImageIO() {
        // the ImageIO class calls the below in its static initialiser. It needs to be called from the main class
        // loader rather than that of a plugin, or it will fail (subsequent calls will trigger a NoClassDefError).
        ClassHelper.invoke(this.getClass().getClassLoader(), IIORegistry::getDefaultInstance);
    }

    /**
     * Updates the document with the image dimensions.
     *
     * @param document  the document
     * @param dimension the image dimensions
     */
    private void updateDimension(Document document, Dimension dimension) {
        Integer width = (dimension.width != -1) ? dimension.width : null;
        Integer height = (dimension.width != -1) ? dimension.height : null;
        IMObjectBean bean = getService().getBean(document);
        bean.setValue("width", width);
        bean.setValue("height", height);
    }

    /**
     * Returns the dimensions of an image.
     *
     * @param stream the stream
     * @return the image dimension
     * @throws IOException for any error
     */
    private Dimension getDimension(InputStream stream) throws IOException {
        int width = -1;
        int height = -1;
        try (ImageInputStream in = ImageIO.createImageInputStream(stream)) {
            Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                try {
                    reader.setInput(in);
                    width = reader.getWidth(0);
                    height = reader.getHeight(0);
                } finally {
                    reader.dispose();
                }
            }
        }
        return new Dimension(width, height);
    }

    private static class ReusableInputStream extends BufferedInputStream {

        ReusableInputStream(InputStream in) {
            super(in);
            mark(Integer.MAX_VALUE);
        }

        @Override
        public void close()  {
            // no-op, to avoid getDimension() closing the underlying stream
        }

        public void realClose() {
            try {
                super.close();
            } catch (IOException ignore) {
                // no-op
            }
        }
    }

    private static class Dimension {

        private final int height;

        private final int width;

        public Dimension(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
}
