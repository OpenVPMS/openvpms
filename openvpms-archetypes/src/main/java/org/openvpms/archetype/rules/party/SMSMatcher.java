/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.party;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.party.Contact;

/**
 * Matches phone contacts if they have SMS enabled.
 *
 * @author Tim Anderson
 */
public class SMSMatcher extends PurposeMatcher {

    /**
     * Constructs an {@link SMSMatcher}.
     *
     * @param factory the bean factory
     */
    public SMSMatcher(IMObjectBeanFactory factory) {
        this(null, false, factory);
    }

    /**
     * Constructs an {@link SMSMatcher}.
     *
     * @param purpose the contact purpose. May be {@code null}
     * @param exact   if {@code true} the contact must contain the purpose in order to be considered a match
     * @param factory the bean factory
     */
    public SMSMatcher(String purpose, boolean exact, IMObjectBeanFactory factory) {
        super(ContactArchetypes.PHONE, purpose, exact, factory);
    }

    /**
     * Determines if a contact matches the criteria.
     *
     * @param contact the contact
     * @return {@code true} if the contact is an exact match; otherwise {@code false}
     */
    @Override
    public boolean matches(Contact contact) {
        boolean result = isA(contact);
        if (result) {
            IMObjectBean bean = getBean(contact);
            // for historical reasons, telephoneNumber is not mandatory, so exclude any contact that doesn't have one
            result = bean.getBoolean("sms")
                     && StringUtils.trimToNull(bean.getString("telephoneNumber")) != null
                     && matchesPurpose(contact);
        }
        return result;
    }
}
