/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.security;

/**
 * A firewall entry for an allowed or blocked address.
 *
 * @author Tim Anderson
 */
public class FirewallEntry {

    /**
     * The address.
     */
    private final String address;

    /**
     * Determines if the entry is active or not.
     */
    private final boolean active;

    /**
     * The entry description.
     */
    private final String description;

    /**
     * Constructs a {@link FirewallEntry}.
     *
     * @param address the address
     */
    public FirewallEntry(String address) {
        this(address, true, null);
    }

    /**
     * Constructs a {@link FirewallEntry}.
     *
     * @param address     the address
     * @param active      determines if the address is active or not
     * @param description the description. May be {@code null}
     */
    public FirewallEntry(String address, boolean active, String description) {
        this.address = address;
        this.active = active;
        this.description = description;
    }

    /**
     * Returns the address.
     * <p/>
     * This may be a specific IP address or a range specified using an IP/Netmask
     * (e.g. 192.168.1.0/24 or 202.24.0.0/14).
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Determines if the entry is active or not.
     *
     * @return {@code true} if it is active, {@code false} if it is inactive
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Returns the entry description.
     *
     * @return the description. May be {@code null}
     */
    public String getDescription() {
        return description;
    }
}