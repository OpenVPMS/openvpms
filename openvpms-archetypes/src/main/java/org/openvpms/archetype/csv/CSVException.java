/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.csv;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.i18n.Message;

/**
 * Base class for CSV exceptions.
 *
 * @author Tim Anderson
 */
public abstract class CSVException extends OpenVPMSException {

    /**
     * The line the error occurred on.
     */
    private final int line;

    /**
     * Constructs a {@link CSVException}.
     *
     * @param message   the exception message
     * @param exception the cause
     * @param line      the line the error occurred on
     */
    protected CSVException(Message message, Throwable exception, int line) {
        super(message, exception);
        this.line = line;
    }

    /**
     * Constructs a {@link CSVException}.
     *
     * @param message the exception message
     * @param line    the line the error occurred on
     */
    protected CSVException(Message message, int line) {
        super(message);
        this.line = line;
    }

    /**
     * Returns the line the error occurred on.
     *
     * @return the line number
     */
    public int getLine() {
        return line;
    }
}
