<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
  -->
<archetypes>
    <archetype name="entity.accountReminderCount.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.common.Entity" displayName="Account Reminder Count">
        <node name="id" path="/id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true" minCardinality="1" derived="true"
              derivedValue="'Account Reminder Count'"/>
        <node name="description" type="java.lang.String" path="/description" hidden="true" minCardinality="0"/>
        <node name="interval" path="/details/interval" type="java.lang.Integer" minCardinality="1">
            <assertion name="units">
                <property name="node" value="units"/>
            </assertion>
        </node>
        <node name="units" path="/details/units" type="java.lang.String" minCardinality="1" defaultValue="'DAYS'">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="DAYS" value="days"/>
                    <property name="WEEKS" value="weeks"/>
                    <property name="MONTHS" value="months"/>
                    <property name="YEARS" value="years"/>
                </propertyList>
            </assertion>
        </node>
        <node name="active" path="/active" type="java.lang.Boolean" defaultValue="true()" hidden="true"/>
        <node name="smsTemplate" displayName="SMS Template" path="/entityLinks" type="java.util.HashSet"
              baseName="EntityLink" minCardinality="1" maxCardinality="1"
              filter="entityLink.accountReminderSMSTemplate"/>
    </archetype>
</archetypes>