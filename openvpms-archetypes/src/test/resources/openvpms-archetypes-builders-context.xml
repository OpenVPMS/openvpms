<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                           http://www.springframework.org/schema/beans/spring-beans.xsd">


    <!-- =========================================================================================================== -->
    <!-- Test Builders                                                                                               -->
    <!-- =========================================================================================================== -->

    <bean id="testContactFactory" class="org.openvpms.archetype.test.builder.party.TestContactFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testCustomerAccountFactory"
          class="org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="customerAccountRules"/>
        <constructor-arg ref="testCustomerFactory"/>
        <constructor-arg ref="testPatientFactory"/>
    </bean>

    <bean id="testCustomerFactory" class="org.openvpms.archetype.test.builder.customer.TestCustomerFactory">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="testDocumentFactory"/>
    </bean>

    <bean id="testInsurerFactory" class="org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testDocumentFactory" class="org.openvpms.archetype.test.builder.doc.TestDocumentFactory">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="documentHandlers"/>
    </bean>

    <bean id="testLaboratoryFactory" class="org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testHL7Factory" class="org.openvpms.archetype.test.builder.hl7.TestHL7Factory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testLookupFactory" class="org.openvpms.archetype.test.builder.lookup.TestLookupFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testMessageFactory" class="org.openvpms.archetype.test.builder.message.TestMessageFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testPatientFactory" class="org.openvpms.archetype.test.builder.patient.TestPatientFactory">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="laboratoryRules"/>
        <constructor-arg ref="testDocumentFactory"/>
    </bean>

    <bean id="testReminderFactory" class="org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory">
        <constructor-arg ref="reminderRules"/>
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="testDocumentFactory"/>
    </bean>

    <bean id="testPracticeFactory" class="org.openvpms.archetype.test.builder.practice.TestPracticeFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testProductFactory" class="org.openvpms.archetype.test.builder.product.TestProductFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testSchedulingFactory" class="org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory">
        <constructor-arg ref="appointmentRules"/>
        <constructor-arg ref="testProductFactory"/>
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testSMSFactory" class="org.openvpms.archetype.test.builder.sms.TestSMSFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testSupplierFactory" class="org.openvpms.archetype.test.builder.supplier.TestSupplierFactory">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <bean id="testUserFactory" class="org.openvpms.archetype.test.builder.user.TestUserFactory">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="userRules"/>
        <constructor-arg ref="testDocumentFactory"/>
    </bean>

</beans>