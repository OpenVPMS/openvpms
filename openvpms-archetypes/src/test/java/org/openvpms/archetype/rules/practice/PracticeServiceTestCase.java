/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.junit.Test;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.job.account.TestAccountReminderJobBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PracticeService}.
 *
 * @author Tim Anderson
 */
public class PracticeServiceTestCase extends ArchetypeServiceTest {

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests the {@link PracticeService#accountRemindersEnabled()} method.
     */
    @Test
    public void testAccountRemindersEnabled() {
        IArchetypeService service = getArchetypeService();

        // disable existing jobs
        disableJobs();

        PracticeService practiceService = new PracticeService(service, practiceRules, null);
        assertFalse(practiceService.accountRemindersEnabled());

        // create a job and verify account reminders are enabled
        Entity job = new TestAccountReminderJobBuilder(service)
                .count()
                .interval(2, DateUnits.DAYS)
                .template("'dummy'")
                .add()
                .runAs(userFactory.createUser())
                .build();
        assertTrue(practiceService.accountRemindersEnabled());

        // disable the job and verify account reminders are no longer enabled
        job.setActive(false);
        save(job);
        assertFalse(practiceService.accountRemindersEnabled());

        practiceService.destroy();
    }

    /**
     * Tests the {@link PracticeService#getLocations()} method.
     */
    @Test
    public void testGetLocations() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.newLocation().active(false).build(); // inactive location
        Party practice = practiceFactory.newPractice()
                .locations(location1, location2, location3)
                .build(false);

        PracticeService practiceService = new PracticeService(getArchetypeService(), practiceRules, null) {
            @Override
            public synchronized Party getPractice() {
                return practice;
            }
        };

        List<Party> locations1 = practiceService.getLocations();
        assertEquals(2, locations1.size());
        assertTrue(locations1.contains(location1));
        assertTrue(locations1.contains(location2));
        assertFalse(locations1.contains(location3)); // inactive locations aren't returned
    }


    /**
     * Tests the {@link PracticeService#getMailServer()} method.
     */
    @Test
    public void testGetMailServer() {
        Party practice = practiceFactory.newPractice()
                .mailServer(null)
                .build(false);

        PracticeService practiceService = new PracticeService(getArchetypeService(), practiceRules, null) {
            @Override
            public synchronized Party getPractice() {
                return practice;
            }
        };

        assertNull(practiceService.getMailServer());

        Entity mailServer = practiceFactory.newMailServer()
                .host("localhost")
                .build();

        practiceFactory.updatePractice(practice)
                .mailServer(mailServer)
                .build(false);
        assertEquals(mailServer.getId(), practiceService.getMailServer().getId());

        // mark the mail server inactive and verify it is no longer returned
        mailServer.setActive(false);
        save(mailServer);
        assertNull(practiceService.getMailServer());

        practiceService.destroy();
    }

    /**
     * Disable any existing account reminder jobs.
     */
    private void disableJobs() {
        IArchetypeService service = getArchetypeService();

        // disable any existing entity.jobAccountReminder
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, AccountReminderArchetypes.ACCOUNT_REMINDER_JOB);
        query.where(builder.equal(root.get("active"), true));
        for (Entity job : service.createQuery(query).getResultList()) {
            job.setActive(false);
            save(job);
        }
    }
}
