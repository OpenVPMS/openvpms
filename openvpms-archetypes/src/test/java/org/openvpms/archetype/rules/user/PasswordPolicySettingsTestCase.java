/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.junit.Test;
import org.openvpms.archetype.rules.settings.AbstractSettingsTest;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PasswordPolicySettings}.
 *
 * @author Tim Anderson
 */
public class PasswordPolicySettingsTestCase extends AbstractSettingsTest {

    /**
     * Verifies the default policy.
     */
    @Test
    public void testDefaults() {
        PasswordPolicy policy = new PasswordPolicySettings(getSettings());
        checkPolicy(policy, 8, true, true, true, true);
    }

    /**
     * Verifies that if the settings are modified externally, they are reflected in the policy.
     */
    @Test
    public void testUpdate() {
        PasswordPolicy policy = new PasswordPolicySettings(getSettings());
        assertEquals(8, policy.getMinLength());
        assertTrue(policy.lowercaseRequired());
        assertTrue(policy.specialRequired());

        Entity entity = getSingleton(SettingsArchetypes.PASSWORD_POLICY);
        IMObjectBean bean = getBean(entity);
        bean.setValue("minLength", 6);
        bean.setValue("lowercase", false);
        bean.setValue("special", false);
        bean.save();
        checkPolicy(policy, 6, false, true, true, false);

        bean.setValue("uppercase", false);
        bean.save();
        checkPolicy(policy, 6, false, false, true, false);

        bean.setValue("number", false);
        bean.save();
        checkPolicy(policy, 6, false, false, false, false);
    }

    /**
     * Verifies the password policy matches that expected.
     *
     * @param policy    the policy
     * @param minLength the expected minimum length
     * @param lowercase the expected lowercase setting
     * @param uppercase the expected uppercase setting
     * @param number    the expected number setting
     * @param special   the expected special setting
     */
    private void checkPolicy(PasswordPolicy policy, int minLength, boolean lowercase, boolean uppercase,
                             boolean number, boolean special) {
        assertEquals(minLength, policy.getMinLength());
        assertEquals(100, policy.getMaxLength());
        assertEquals(lowercase, policy.lowercaseRequired());
        assertEquals(uppercase, policy.uppercaseRequired());
        assertEquals(number, policy.numberRequired());
        assertEquals(special, policy.specialRequired());
    }
}
