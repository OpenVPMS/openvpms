/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.party;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.archetype.test.builder.party.TestLocationContactBuilder;
import org.openvpms.archetype.test.builder.party.TestPhoneContactBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PartyRules} class.
 *
 * @author Tim Anderson
 */
public class PartyRulesTestCase extends ArchetypeServiceTest {

    /**
     * The rules.
     */
    private PartyRules rules;

    /**
     * The contact factory.
     */
    @Autowired
    private TestContactFactory contactFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Tests the {@link PartyRules#getFullName(Party)} and {@link PartyRules#getFullName(Party, boolean)} method.
     */
    @Test
    public void testGetFullName() {
        Party customer1 = customerFactory.newCustomer()
                .title("MR")
                .firstName("Foo")
                .lastName("Bar")
                .build(false);
        assertEquals("Mr Foo Bar", rules.getFullName(customer1));
        assertEquals("Foo Bar", rules.getFullName(customer1, false));
        assertEquals("Mr Foo Bar", rules.getFullName(customer1, true));

        // verify that missing firstName doesn't introduce a space
        Party customer2 = customerFactory.newCustomer()
                .title("MS")
                .firstName(ValueStrategy.unset())
                .lastName("Jones")
                .build(false);
        assertEquals("Ms Jones", rules.getFullName(customer2));
        assertEquals("Jones", rules.getFullName(customer2, false));
        assertEquals("Ms Jones", rules.getFullName(customer2, true));

        // verify that missing title and firstName doesn't introduce spaces
        Party customer3 = customerFactory.newCustomer()
                .title(null)
                .firstName(ValueStrategy.unset())
                .lastName("Jones")
                .build(false);
        assertEquals("Jones", rules.getFullName(customer3));
        assertEquals("Jones", rules.getFullName(customer3, false));
        assertEquals("Jones", rules.getFullName(customer3, true));

        Party vet = supplierFactory.newVet()
                .title("DR")
                .firstName("Jenny")
                .lastName("Smith")
                .build(false);
        assertEquals("Dr Jenny Smith", rules.getFullName(vet));
        assertEquals("Jenny Smith", rules.getFullName(vet, false));
        assertEquals("Dr Jenny Smith", rules.getFullName(vet, true));

        // verify no special formatting for other party types
        Party pet = patientFactory.newPatient().name("T Rex").build(false);
        pet.setName("T Rex");
        assertEquals("T Rex", rules.getFullName(pet));
        assertEquals("T Rex", rules.getFullName(pet, false));
        assertEquals("T Rex", rules.getFullName(pet, true));
    }

    /**
     * Tests the {@link PartyRules#getDefaultContacts()} method.
     */
    @Test
    public void testDefaultContacts() {
        Set<Contact> contacts = rules.getDefaultContacts();
        assertNotNull(contacts);
        assertEquals(2, contacts.size());

        // expect a contact.location and a contact.phoneNumber
        assertNotNull(getContact(contacts, ContactArchetypes.LOCATION));
        assertNotNull(getContact(contacts, ContactArchetypes.PHONE));
    }

    /**
     * Tests the {@link PartyRules#getPreferredContacts(Party)} method.
     */
    @Test
    public void testGetPreferredContacts() {
        Party party = customerFactory.newCustomer()
                .firstName("ZFoo")
                .lastName("ZBar")
                .build(false);
        assertEquals(0, party.getContacts().size());
        for (Contact contact : rules.getDefaultContacts()) {
            party.addContact(contact);
        }
        assertEquals(2, party.getContacts().size());

        Contact location = getContact(party, ContactArchetypes.LOCATION);
        Contact phone = getContact(party, ContactArchetypes.PHONE);

        contactFactory.updateLocation(location)
                .preferred(false)
                .build();
        contactFactory.updatePhone(phone)
                .preferred(false)
                .build();

        // expect no preferred contacts to result in empty string
        assertEquals("", rules.getPreferredContacts(party));

        // now make the location a preferred contact
        contactFactory.updateLocation(location)
                .preferred()
                .address("1 Foo St")
                .suburb("BAR", "Bar")
                .state("VIC", "Vic")
                .postcode("3071")
                .purposes("HOME")
                .build();
        String address = "1 Foo St Bar 3071 (Home)";
        assertEquals(address, rules.getPreferredContacts(party));

        // and the phone number as well
        contactFactory.updatePhone(phone)
                .preferred()
                .areaCode("03")
                .phone("1234567")
                .purposes("WORK")
                .build();
        String phoneNo = "(03) 1234567 (Work)";

        save(party);  // needs to be persistent to ensure contact order
        String contacts = rules.getPreferredContacts(party);
        if (location.getId() < phone.getId()) { // check sort order
            assertEquals(address + ", " + phoneNo, contacts);
        } else {
            assertEquals(phoneNo + ", " + address, contacts);
        }
    }

    /**
     * Tests the {@link PartyRules#getContactPurposes(Contact)} method.
     */
    @Test
    public void testGetContactPurposes() {
        Contact contact = contactFactory.newLocation().build();
        assertEquals("", rules.getContactPurposes(contact));

        contactFactory.updateLocation(contact).purposes("HOME").build();
        assertEquals("(Home)", rules.getContactPurposes(contact));

        contactFactory.updateLocation(contact).purposes("WORK").build();
        String purposes = rules.getContactPurposes(contact);

        assertEquals("(Home, Work)", purposes);
    }

    /**
     * Tests the {@link PartyRules#getBillingAddress(Party, boolean)} method.
     */
    @Test
    public void testGetBillingAddress() {
        Party party = customerFactory.newCustomer()
                .title("MR")
                .name("Foo", "Bar")
                .addContact(createLocation("1 Foo St", null))
                .build(false);

        // no location with billing address, uses the first available.
        assertEquals("1 Foo St, Coburg Victoria 3071", rules.getBillingAddress(party, true));
        assertEquals("1 Foo St\nCoburg Victoria 3071", rules.getBillingAddress(party, false));

        // add a billing location
        customerFactory.updateCustomer(party)
                .addContact(createLocation("3 Bar St", "BILLING"))
                .build();

        // verify the billing address is that just added
        assertEquals("3 Bar St, Coburg Victoria 3071", rules.getBillingAddress(party, true));
        assertEquals("3 Bar St\nCoburg Victoria 3071", rules.getBillingAddress(party, false));

        // verify nulls aren't displayed if the state doesn't exist
        Contact location = rules.getContact(party, ContactArchetypes.LOCATION, "BILLING");
        assertNotNull(location);

        IMObjectBean locationBean = getBean(location);
        locationBean.setValue("state", "BAD_STATE");
        assertEquals("3 Bar St, 3071", rules.getBillingAddress(party, true));
        assertEquals("3 Bar St\n3071", rules.getBillingAddress(party, false));

        // verify nulls aren't displayed if the suburb doesn't exist
        locationBean.setValue("state", "VIC");
        locationBean.setValue("suburb", "BAD_SUBURB");
        assertEquals("3 Bar St, Victoria 3071", rules.getBillingAddress(party, true));
        assertEquals("3 Bar St\nVictoria 3071", rules.getBillingAddress(party, false));

        // remove all the contacts and verify there is no billing address
        party.getContacts().clear();

        assertEquals("", rules.getBillingAddress(party, true));
        assertEquals("", rules.getBillingAddress(party, false));

        // check nulls
        assertEquals("", rules.getBillingAddress(null, true));
        assertEquals("", rules.getBillingAddress(null, false));
    }

    /**
     * Tests the {@link PartyRules#getCorrespondenceAddress(Party, boolean)} method.
     */
    @Test
    public void testGetCorrespondenceAddress() {
        Party party = customerFactory.newCustomer()
                .title("MR")
                .name("Foo", "Bar")
                .addContact(createLocation("1 Foo St", null))
                .build(false);

        // no location with billing address, uses the first available.
        assertEquals("1 Foo St, Coburg Victoria 3071", rules.getCorrespondenceAddress(party, true));
        assertEquals("1 Foo St\nCoburg Victoria 3071", rules.getCorrespondenceAddress(party, false));

        // add a correspondence location
        party.addContact(createLocation("3 Bar St", "CORRESPONDENCE"));

        // verify the correspondence address is that just added
        assertEquals("3 Bar St, Coburg Victoria 3071", rules.getCorrespondenceAddress(party, true));
        assertEquals("3 Bar St\nCoburg Victoria 3071", rules.getCorrespondenceAddress(party, false));

        // remove all the contacts and verify there is no correspondence address
        Contact[] contacts = party.getContacts().toArray(new Contact[0]);
        for (Contact c : contacts) {
            party.removeContact(c);
        }

        assertEquals("", rules.getCorrespondenceAddress(party, true));
        assertEquals("", rules.getCorrespondenceAddress(party, false));
    }

    /**
     * Tests the {@link PartyRules#getTelephone(Party)} method.
     */
    @Test
    public void testGetTelephone() {
        Party party1 = customerFactory.newCustomer()
                .addContact(createPhone("12345", false, null))
                .build(false);

        assertEquals("(03) 12345", rules.getTelephone(party1));

        party1.addContact(createPhone("56789", true, null));
        assertEquals("(03) 56789", rules.getTelephone(party1));

        // test partially populated numbers
        Party party2 = customerFactory.newCustomer()
                .newPhone().areaCode("03").add()
                .build(false);
        Party party3 = customerFactory.newCustomer()
                .newPhone().areaCode("").phone("  ").add()
                .build(false);
        assertEquals("(03) ", rules.getTelephone(party2));
        assertEquals("", rules.getTelephone(party3));
    }

    /**
     * Tests the {@link PartyRules#getHomeTelephone(Party)} method.
     */
    @Test
    public void testGetHomeTelephone() {
        Contact phone1 = createPhone("12345", false, null);
        Party party = customerFactory.newCustomer().addContact(phone1).build(false);

        assertEquals("(03) 12345", rules.getHomeTelephone(party)); // OVPMS-718

        contactFactory.updatePhone(phone1).purposes("HOME").build();
        assertEquals("(03) 12345", rules.getHomeTelephone(party));

        Contact phone2 = createPhone("56789", true, null);
        party.addContact(phone2);
        assertEquals("(03) 12345", rules.getHomeTelephone(party));

        contactFactory.updatePhone(phone2).purposes("HOME").build();
        assertEquals("(03) 56789", rules.getHomeTelephone(party));
    }

    /**
     * Tests the {@link PartyRules#getWorkTelephone(Party)} method.
     */
    @Test
    public void testGetWorkTelephone() {
        Contact phone1 = createPhone("12345", false, null);
        Party party = customerFactory.newCustomer().addContact(phone1).build(false);

        assertEquals("", rules.getWorkTelephone(party));

        contactFactory.updatePhone(phone1).purposes("WORK").build();
        assertEquals("(03) 12345", rules.getWorkTelephone(party));

        Contact phone2 = createPhone("56789", true, null);
        party.addContact(phone2);
        assertEquals("(03) 12345", rules.getWorkTelephone(party));

        contactFactory.updatePhone(phone2).purposes("WORK").build();
        assertEquals("(03) 56789", rules.getWorkTelephone(party));
    }

    /**
     * Tests the {@link PartyRules#getSMSTelephone(Party)} method.
     */
    @Test
    public void testGetSMSTelephone() {
        Party party = customerFactory.newCustomer().build(false);
        assertEquals("", rules.getSMSTelephone(party));
        Contact contact1 = contactFactory.newPhone()
                .phone("1234")
                .preferred(false)
                .sms(false)
                .build();
        party.addContact(contact1);
        assertEquals("", rules.getSMSTelephone(party));

        contactFactory.updatePhone(contact1).sms().build();
        assertEquals("1234", rules.getSMSTelephone(party));

        Contact contact2 = contactFactory.newPhone()
                .phone("5678")
                .preferred()
                .sms(false)
                .build();
        party.addContact(contact2);
        assertEquals("1234", rules.getSMSTelephone(party));

        contactFactory.updatePhone(contact2).sms().build();
        assertEquals("5678", rules.getSMSTelephone(party));
    }

    /**
     * Tests the {@link PartyRules#getSMSContact(Party)} method.
     */
    @Test
    public void testGetSMSContact() {
        Party party = customerFactory.newCustomer().build(false);
        assertNull(rules.getSMSContact(party));

        // verify incomplete contacts are excluded
        Contact phone1 = contactFactory.newPhone()
                .sms(true)
                .preferred()
                .build();
        Contact phone2 = contactFactory.newPhone()
                .sms(true)
                .areaCode("03")
                .preferred()
                .build();
        party.addContact(phone1);
        party.addContact(phone2);
        assertNull(rules.getSMSContact(party));

        Contact phone3 = contactFactory.newPhone()
                .sms(true)
                .areaCode("03")
                .phone("1235678")
                .preferred(false)
                .build();
        party.addContact(phone3);
        assertEquals(phone3, rules.getSMSContact(party));

        // verify preferred contact is selected
        Contact phone4 = contactFactory.newPhone()
                .sms(true)
                .phone("1235678")
                .preferred(true)
                .build();
        party.addContact(phone4);
        assertEquals(phone4, rules.getSMSContact(party));
    }

    /**
     * Tests the {@link PartyRules#getFaxNumber(Party)} method.
     */
    @Test
    public void testGetFaxNumber() {
        Party party = customerFactory.newCustomer().build(false);

        assertEquals("", rules.getFaxNumber(party));

        Contact fax1 = contactFactory.newPhone()
                .areaCode("03")
                .phone("12345")
                .preferred(false)
                .purposes("FAX")
                .build();
        party.addContact(fax1);
        assertEquals("(03) 12345", rules.getFaxNumber(party));

        party.removeContact(fax1);
        Contact fax2 = contactFactory.newPhone()
                .phone("12345")
                .preferred(false)
                .purposes("FAX")
                .build();
        party.addContact(fax2);
        assertEquals("12345", rules.getFaxNumber(party));
    }

    /**
     * Tests the {@link PartyRules#getIdentities(Party)} method.
     */
    @Test
    public void testGetIdentities() {
        Party pet = patientFactory.newPatient().addPetTag("1234567").build(false);

        String tagString = "Pet Tag: 1234567";
        assertEquals(tagString, rules.getIdentities(pet));

        patientFactory.updatePatient(pet)
                .addAlias("Foo")
                .addAlias("Bar")
                .build();

        String identities = rules.getIdentities(pet);
        assertEquals("Alias: Bar, Alias: Foo, " + tagString, identities);
    }

    /**
     * Tests the {@link PartyRules#getContact(Party, String, String)}.
     */
    @Test
    public void testGetContact() {
        Party party = customerFactory.newCustomer()
                .name("Foo", "Bar")
                .build();

        // add 3 contacts, saving each time to ensure ordering of contacts by id
        Contact phone1 = createPhone("12345", false, null);
        customerFactory.updateCustomer(party).addContact(phone1).build();

        Contact phone2 = createPhone("45678", false, "HOME");
        customerFactory.updateCustomer(party).addContact(phone2).build();

        Contact phone3 = createPhone("90123", false, "HOME");
        customerFactory.updateCustomer(party).addContact(phone3).build();

        assertEquals(phone1, rules.getContact(party, ContactArchetypes.PHONE, null));
        assertEquals(phone2, rules.getContact(party, ContactArchetypes.PHONE, "HOME"));

        contactFactory.updatePhone(phone3).preferred().build();
        assertEquals(phone3, rules.getContact(party, ContactArchetypes.PHONE, "HOME"));

        // phone2 should now be returned as its id < phone3
        contactFactory.updatePhone(phone2).preferred().build();
        assertEquals(phone2, rules.getContact(party, ContactArchetypes.PHONE, "HOME"));
    }

    /**
     * Verifies that a phone contact with FAX classifications are never returned by the {@code get*Telephone()}
     * methods.
     */
    @Test
    public void getPhoneExcludesFaxContacts() {
        Party customer = customerFactory.newCustomer().name("Foo", "Bar").build(false);

        Contact fax = contactFactory.newPhone().phone("7777 1234").preferred().purposes("FAX").build();
        customerFactory.updateCustomer(customer).addContact(fax).build();

        // add the phone contact afterwards, to ensure it gets a higher id. Contacts are sorted by id in the rules
        // to ensure deterministic behaviour
        Contact phone = contactFactory.newPhone()
                .areaCode("03")
                .phone("9999 6789")
                .preferred(false)
                .build();
        customerFactory.updateCustomer(customer).addContact(phone).build();

        assertEquals("(03) 9999 6789", rules.getTelephone(customer));
        assertEquals("(03) 9999 6789", rules.getHomeTelephone(customer));
        assertEquals("", rules.getWorkTelephone(customer));
        assertEquals("", rules.getMobileTelephone(customer));

        Contact work = contactFactory.newPhone()
                .areaCode("03")
                .phone("8888 1234")
                .preferred(false)
                .purposes("WORK")
                .build();
        customerFactory.updateCustomer(customer).addContact(work).build();

        assertEquals("(03) 9999 6789", rules.getTelephone(customer));
        assertEquals("(03) 9999 6789", rules.getHomeTelephone(customer));
        assertEquals("(03) 8888 1234", rules.getWorkTelephone(customer));
        assertEquals("", rules.getMobileTelephone(customer));

        Contact mobile = contactFactory.newPhone()
                .areaCode("03")
                .phone("6666 5432")
                .preferred(false)
                .purposes("MOBILE")
                .build();
        customerFactory.updateCustomer(customer).addContact(mobile).build();

        assertEquals("(03) 9999 6789", rules.getTelephone(customer));
        assertEquals("(03) 9999 6789", rules.getHomeTelephone(customer));
        assertEquals("(03) 8888 1234", rules.getWorkTelephone(customer));
        assertEquals("(03) 6666 5432", rules.getMobileTelephone(customer));

        // add a FAX classification to the work contact. The work contact should no longer be returned
        contactFactory.updatePhone(work).purposes("FAX").build();
        assertEquals("(03) 9999 6789", rules.getTelephone(customer));
        assertEquals("(03) 9999 6789", rules.getHomeTelephone(customer));
        assertEquals("", rules.getWorkTelephone(customer));
        assertEquals("(03) 6666 5432", rules.getMobileTelephone(customer));
    }

    /**
     * Tests the {@link PartyRules#getPracticeAddress(boolean)} method.
     */
    @Test
    public void testGetPracticeAddress() {
        practiceFactory.newPractice()
                .addContact(createLocation("123 Foo St", null))
                .build();
        assertEquals("123 Foo St, Coburg Victoria 3071", rules.getPracticeAddress(true));
        assertEquals("123 Foo St\nCoburg Victoria 3071", rules.getPracticeAddress(false));
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new PartyRules(getArchetypeService(), getLookupService());
    }

    /**
     * Helper to get a contact from party by short name.
     *
     * @param party     the party
     * @param shortName contact short name
     * @return the associated short name or {@code null}
     */
    private Contact getContact(Party party, String shortName) {
        return getContact(party.getContacts(), shortName);
    }

    /**
     * Helper to get a contact from a collection by short name.
     *
     * @param contacts  the contacts
     * @param shortName contact short name
     * @return the associated short name or {@code null}
     */
    private Contact getContact(Collection<Contact> contacts, String shortName) {
        for (Contact contact : contacts) {
            if (TypeHelper.isA(contact, shortName)) {
                return contact;
            }
        }
        return null;
    }

    /**
     * Creates a new <em>contact.location</em>.
     *
     * @param address the address
     * @param purpose the contact purpose. May be {@code null}
     * @return a new location contact
     */
    private Contact createLocation(String address, String purpose) {
        Contact contact = contactFactory.newLocation().build();
        populateLocation(contact, address, purpose);
        return contact;
    }

    /**
     * Populates a <em>contact.location</em>
     *
     * @param contact the contact
     * @param address the address
     * @param purpose the contact purpose. May be {@code null}
     */
    private void populateLocation(Contact contact, String address, String purpose) {
        TestLocationContactBuilder<?, ?> builder = contactFactory.updateLocation(contact)
                .address(address)
                .state("VIC", "Victoria")
                .suburb("COBURG", "Coburg")
                .postcode("3071");
        if (purpose != null) {
            builder.purposes(purpose);
        }
        builder.build();
    }

    /**
     * Creates a new <em>contact.phoneNumber</em>.
     *
     * @param number    the phone number
     * @param preferred if {@code true}, marks the contact as the preferred contact
     * @param purpose   the contact purpose. May be {@code null}
     * @return a new phone contact
     */
    private Contact createPhone(String number, boolean preferred, String purpose) {
        return createPhone("03", number, preferred, purpose);
    }

    /**
     * Creates a new <em>contact.phoneNumber</em>.
     *
     * @param number    the phone number
     * @param preferred if {@code true}, marks the contact as the preferred contact
     * @param purpose   the contact purpose. May be {@code null}
     * @return a new phone contact
     */
    private Contact createPhone(String areaCode, String number, boolean preferred, String purpose) {
        Contact contact = contactFactory.newPhone().build();
        populatePhone(contact, areaCode, number, preferred, purpose);
        return contact;
    }

    /**
     * Populates a <em>contact.phoneNumber</em>.
     *
     * @param contact   the contact
     * @param areaCode  the area code. May be {@code null}
     * @param number    the phone number
     * @param preferred if {@code true}, marks the contact as the preferred contact
     * @param purpose   the contact purpose. May be {@code null}
     */
    private void populatePhone(Contact contact, String areaCode, String number, boolean preferred, String purpose) {
        TestPhoneContactBuilder<?, ?> builder = contactFactory.updatePhone(contact);
        builder.areaCode(areaCode)
                .phone(number)
                .preferred(preferred);
        if (purpose != null) {
            builder.purposes(purpose);
        }
        builder.build();
    }
}