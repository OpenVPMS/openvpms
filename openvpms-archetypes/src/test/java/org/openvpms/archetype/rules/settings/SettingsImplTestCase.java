/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

import org.junit.Test;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link SettingsImpl}.
 *
 * @author Tim Anderson
 */
public class SettingsImplTestCase extends AbstractSettingsTest {

    /**
     * Verifies that when the settings archetype provides defaults, these are used in preference to the defaults
     * supplied.
     */
    @Test
    public void testDefaults() {
        Settings settings = getSettings();
        assertEquals(8, settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", 10));
        assertTrue(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "lowercase", false));
        assertTrue(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "uppercase", false));
        assertTrue(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "number", false));
        assertTrue(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "special", false));
    }

    /**
     * Verifies that if the settings are modified externally, the changes are picked up.
     */
    @Test
    public void testUpdate() {
        Settings settings = getSettings();
        assertEquals(8, settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", 10));
        assertTrue(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "lowercase", false));

        Entity entity = getSingleton(SettingsArchetypes.PASSWORD_POLICY);
        IMObjectBean bean = getBean(entity);
        bean.setValue("minLength", 6);
        bean.setValue("lowercase", false);
        bean.save();

        assertEquals(6, settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", 10));
        assertFalse(settings.getBoolean(SettingsArchetypes.PASSWORD_POLICY, "lowercase", true));
    }

    /**
     * Verifies that if there is no current user, settings aren't made persistent. This is to support the case
     * where the settings are accessed by an anonymous user.
     */
    @Test
    public void testUnauthenticatedUser() {
        Settings settings = getSettings();
        SingletonService service = getSingletonService();

        // clear the security context. The SettingsService shouldn't save settings if they don't exist
        AuthenticationContext context = getContext();
        context.setUser(null);

        // verify the settings aren't persistent
        assertNull(service.get(SettingsArchetypes.PASSWORD_POLICY, Entity.class));

        // verify the default settings can be accessed, but no setting is made persistent
        assertEquals(8, settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", 10));
        assertNull(service.get(SettingsArchetypes.PASSWORD_POLICY, Entity.class));

        // now save settings
        Entity entity = service.get(SettingsArchetypes.PASSWORD_POLICY, Entity.class, true);
        IMObjectBean bean = getBean(entity);
        bean.setValue("minLength", 6);
        bean.save();

        // verify they are seen by the settings service
        assertEquals(6, settings.getInt(SettingsArchetypes.PASSWORD_POLICY, "minLength", 10));
    }
}
