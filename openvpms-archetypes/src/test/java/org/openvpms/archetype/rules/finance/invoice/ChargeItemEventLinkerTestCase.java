/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;


/**
 * Tests the {@link ChargeItemEventLinker} class.
 *
 * @author Tim Anderson
 */
public class ChargeItemEventLinkerTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The location.
     */
    private Party location;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The rules.
     */
    private MedicalRecordRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        clinician = TestHelper.createClinician();
        location = TestHelper.createLocation();
        rules = new MedicalRecordRules(getArchetypeService());
    }

    /**
     * Tests linking of a single charge item's act to an event.
     */
    @Test
    public void testSingleLink() {
        Date startTime = new Date();
        Party patient = TestHelper.createPatient();
        FinancialAct item = createInvoiceItem(startTime, patient);

        assertNull(rules.getEvent(patient, startTime));
        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        linker.link(item, changes);

        Act event = rules.getEvent(patient, startTime);
        assertNotNull(event);

        checkEvent(item, event, location);

        // verify that linking again succeeds
        linker.link(item, changes);
        checkEvent(item, event, location);
    }

    /**
     * Verifies that a single event is created if multiple charge items are saved for the same start time.
     */
    @Test
    public void testMultipleLinkSameStartTime() {
        Date startTime = new Date();
        Party patient = TestHelper.createPatient();
        FinancialAct item1 = createInvoiceItem(startTime, patient);
        FinancialAct item2 = createInvoiceItem(startTime, patient);

        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        List<FinancialAct> items = Arrays.asList(item1, item2);

        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));
        Act event = rules.getEvent(patient, startTime);
        assertNotNull(event);

        checkEvent(item1, event, location);
        checkEvent(item2, event, location);

        // verify that linking again succeeds
        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item1, event, location);
        checkEvent(item2, event, location);
    }

    /**
     * Verifies that multiple events are created if the charge items have start times on different days.
     */
    @Test
    public void testMultipleLinkDifferentStartTime() {
        Date startTime1 = getDate("2011-01-09");
        Date startTime2 = getDate("2011-01-02");
        Party patient = TestHelper.createPatient();
        FinancialAct item1 = createInvoiceItem(startTime1, patient);
        FinancialAct item2 = createInvoiceItem(startTime2, patient);

        ChargeItemEventLinker linker1 = new ChargeItemEventLinker(getArchetypeService());
        List<FinancialAct> items = Arrays.asList(item1, item2);
        linker1.link(items, new PatientHistoryChanges(location, getArchetypeService()));

        Act event1 = rules.getEvent(patient, startTime1);
        Act event2 = rules.getEvent(patient, startTime2);
        assertNotNull(event1);
        assertNotNull(event2);
        assertNotEquals(event1, event2);

        checkEvent(item1, event1, location);
        checkEvent(item2, event2, location);

        // verify that linking again succeeds
        linker1.link(items, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item1, event1, location);
        checkEvent(item2, event2, location);

        // now add a new item, and verify it links to event1. The author and location shouldn't change, as the event
        // already exists
        FinancialAct item3 = createInvoiceItem(startTime1, patient);
        Party location2 = TestHelper.createLocation();
        ChargeItemEventLinker linker2 = new ChargeItemEventLinker(getArchetypeService());
        linker2.link(item3, new PatientHistoryChanges(location2, getArchetypeService()));
        Act event1Updated = rules.getEvent(patient, startTime1);
        assertEquals(event1Updated.getId(), event1.getId());
        checkEvent(item1, event1Updated, location);
        checkEvent(item3, event1Updated, location);
    }

    /**
     * Verifies that multiple events are created if there are different patients.
     */
    @Test
    public void testDifferentPatients() {
        Date startTime = new Date();
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        FinancialAct item1 = createInvoiceItem(startTime, patient1);
        FinancialAct item2 = createInvoiceItem(startTime, patient2);

        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        List<FinancialAct> items = Arrays.asList(item1, item2);
        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));

        Act event1 = rules.getEvent(patient1, startTime);
        assertNotNull(event1);

        Act event2 = rules.getEvent(patient2, startTime);
        assertNotNull(event2);
        assertNotEquals(event1, event2);

        checkEvent(item1, event1, location);
        checkEvent(item2, event2, location);

        // verify that linking again succeeds
        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item1, event1, location);
        checkEvent(item2, event2, location);
    }

    /**
     * Verifies a charge item and related acts can't be linked to two different events.
     */
    @Test
    public void testLinkForDifferentEvents() {
        Date date1 = getDate("2013-05-15");
        Date date2 = getDate("2013-05-16");
        Party patient = TestHelper.createPatient();
        Act event1 = rules.createEvent(patient, date1, clinician);
        Act event2 = rules.createEvent(patient, date2, clinician);

        FinancialAct item = createInvoiceItem(date1, patient);

        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        linker.link(event1, item, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item, event1, location);

        // try linking to event1 again. Should be a no-op
        linker.link(event1, item, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item, event1, location);

        // try linking to event2, and verify that the item is still linked to event1
        linker.link(event2, item, new PatientHistoryChanges(location, getArchetypeService()));
        item = get(item);
        checkEvent(item, event1, location);

        // verify the item and its child acts don't have any links to event2
        IMObjectBean event2Bean = getBean(event2);
        assertFalse(event2Bean.hasTarget("items", item));

        IMObjectBean itemBean = getBean(item);
        assertEquals(3, item.getSourceActRelationships().size()); // dispensing, investigation and document acts
        for (Relationship relationship : item.getSourceActRelationships()) {
            assertFalse(event2Bean.hasTarget("items", relationship.getTarget()));
        }
    }

    /**
     * Verifies that when the patient on an item changes, the link to the old event is removed, and moved to an event
     * associated with the new patient.
     */
    @Test
    public void testChangePatient() {
        Date startTime = new Date();
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        FinancialAct item1 = createInvoiceItem(startTime, patient1);
        FinancialAct item2 = createInvoiceItem(startTime, patient2);

        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        List<FinancialAct> items = Arrays.asList(item1, item2);
        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));

        Act event1 = rules.getEvent(patient1, startTime);
        assertNotNull(event1);

        Act event2 = rules.getEvent(patient2, startTime);
        assertNotNull(event2);
        assertNotEquals(event1, event2);

        checkEvent(item1, event1, location);
        checkEvent(item2, event2, location);

        // now change the patient for item2
        IMObjectBean itemBean = getBean(item2);
        itemBean.setTarget("patient", patient1);
        itemBean.save();

        // verify event1 still linked to item, and now linked to item2
        linker.link(items, new PatientHistoryChanges(location, getArchetypeService()));
        checkEvent(item1, event1, location);

        // event1 should now be linked to item2
        event1 = get(event1);
        IMObjectBean event1Bean = getBean(event1);
        assertTrue(event1Bean.hasTarget("chargeItems", item2));

        // event2 should no longer have a link to item2
        event2 = get(event2);
        IMObjectBean event2Bean = getBean(event2);
        assertFalse(event2Bean.hasTarget("chargeItems", item2));
    }

    /**
     * Checks that a patient clinical event has links to the relevant charge items acts.
     *
     * @param item     the charge item
     * @param event    the event
     * @param location the expected location. May be {@code null}
     */
    private void checkEvent(FinancialAct item, Act event, Party location) {
        item = get(item);
        event = get(event);
        assertNotNull(item);
        assertNotNull(event);
        IMObjectBean bean = getBean(item);
        List<Act> dispensing = bean.getTargets("dispensing", Act.class);
        List<Act> investigations = bean.getTargets("investigations", Act.class);
        List<Act> documents = bean.getTargets("documents", Act.class);
        assertEquals(1, dispensing.size());
        assertEquals(1, investigations.size());
        assertEquals(1, documents.size());

        IMObjectBean eventBean = getBean(event);
        if (location == null) {
            assertNull(eventBean.getTarget("location"));
        } else {
            assertEquals(location.getObjectReference(), eventBean.getTargetRef("location"));
        }
        assertTrue(eventBean.hasTarget("chargeItems", item));
        assertTrue(eventBean.hasTarget("items", investigations.get(0)));
        assertTrue(eventBean.hasTarget("items", dispensing.get(0)));
        assertTrue(eventBean.hasTarget("items", documents.get(0)));
    }

    /**
     * Creates a new invoice item.
     * <p/>
     * Each item has a single medication, investigation and document act.
     *
     * @param startTime the start time
     * @param patient   the patient
     * @return a new invoice item
     */
    private FinancialAct createInvoiceItem(Date startTime, Party patient) {
        FinancialAct act = create(CustomerAccountArchetypes.INVOICE_ITEM, FinancialAct.class);
        IMObjectBean bean = getBean(act);
        act.setActivityStartTime(startTime);
        bean.setTarget("patient", patient);
        bean.setTarget("clinician", clinician);

        Entity investigationType = LaboratoryTestHelper.createInvestigationType();
        Entity test = LaboratoryTestHelper.createTest(investigationType);
        Entity template = createDocumentTemplate();
        Product product = createProduct(test, template);

        // add medication act
        Act medication = createMedication(product, patient);
        bean.setTarget("product", product);
        bean.addTarget("dispensing", medication);

        // add investigation act
        Act investigation = createInvestigation(investigationType, patient);
        bean.addTarget("investigations", investigation);

        save(act, medication, investigation);

        // add document acts
        ChargeItemDocumentLinker linker = new ChargeItemDocumentLinker(act, getArchetypeService());
        linker.link();
        return act;
    }

    /**
     * Creates and saves a new document template.
     *
     * @return a new document template
     */
    private Entity createDocumentTemplate() {
        return documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
    }

    /**
     * Creates and saves a new product.
     *
     * @param test     the laboratory test
     * @param template the document template
     * @return a new product
     */
    private Product createProduct(Entity test, Entity template) {
        Product product = create(ProductArchetypes.MEDICATION, Product.class);
        IMObjectBean bean = getBean(product);
        bean.setValue("name", "XProduct");
        bean.addTarget("tests", test);
        bean.addTarget("documents", template);
        bean.save();
        return product;
    }

    /**
     * Creates a new <em>act.patientMedication</em>.
     *
     * @param product the product
     * @param patient the patient
     * @return a new medication
     */
    private Act createMedication(Product product, Party patient) {
        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);
        bean.setTarget("product", product);
        return act;
    }

    /**
     * Creates an <em>act.patientInvestigation</em> for an investigation type.
     *
     * @param investigationType the investigation type
     * @param patient           the patient
     * @return a list of investigation acts
     */
    private Act createInvestigation(Entity investigationType, Party patient) {
        Act act = create(InvestigationArchetypes.PATIENT_INVESTIGATION, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);
        bean.setTarget("investigationType", investigationType);
        return act;
    }

}
