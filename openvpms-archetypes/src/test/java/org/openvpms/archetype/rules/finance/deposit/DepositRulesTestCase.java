/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.deposit;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.business.service.ruleengine.RuleEngineException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.InvalidDepositArchetype;
import static org.openvpms.archetype.rules.finance.deposit.DepositRuleException.ErrorCode.UndepositedDepositExists;


/**
 * Tests the {@link DepositRules} class.
 *
 * @author Tim Anderson
 */
public class DepositRulesTestCase extends ArchetypeServiceTest {

    /**
     * The account.
     */
    private Party account;


    /**
     * Verifies that an <em>act.bankDeposit</em> with 'UnDeposited' status
     * can only be saved if there are no other uncleared bank deposits for
     * the same account.<br/>
     * Requires the rule <em>archetypeService.save.act.bankDeposit.before</em>.
     */
    @Test
    public void testSaveUndepositedDeposit() {
        Act deposit1 = createDeposit(DepositStatus.UNDEPOSITED);
        save(deposit1);

        // can save the same deposit multiple times
        save(deposit1);

        Act deposit2 = createDeposit(DepositStatus.UNDEPOSITED);
        try {
            save(deposit2);
            fail("Expected save of second undeposited bank deposit to fail");
        } catch (RuleEngineException expected) {
            Throwable cause = expected.getCause();
            while (cause != null && !(cause instanceof DepositRuleException)) {
                cause = cause.getCause();
            }
            assertNotNull(cause);
            DepositRuleException exception = (DepositRuleException) cause;
            assertEquals(UndepositedDepositExists, exception.getErrorCode());
        }
    }

    /**
     * Verifies that multiple <em>act.bankDeposit</em> with 'Deposited' status
     * can be saved for the same deposit account.<br/>
     * Requires the rule <em>archetypeService.save.act.bankDeposit.before</em>.
     */
    @Test
    public void testSaveDepositedDeposit() {
        for (int i = 0; i < 3; ++i) {
            Act deposit = createDeposit(DepositStatus.DEPOSITED);
            save(deposit);
        }
    }

    /**
     * Verifies that {@link DepositRules#checkCanSaveBankDeposit} throws
     * DepositRuleException if invoked for an invalid act.
     */
    @Test
    public void testCheckCanSaveBankDepositWithInvalidAct() {
        FinancialAct act = create(TillArchetypes.TILL_BALANCE, FinancialAct.class);
        try {
            DepositRules.checkCanSaveBankDeposit(act, getArchetypeService());
        } catch (DepositRuleException expected) {
            assertEquals(InvalidDepositArchetype, expected.getErrorCode());
        }
    }

    /**
     * Tests the {@link DepositRules#deposit} method.
     */
    @Test
    public void testDeposit() {
        Act deposit = createDeposit(DepositStatus.UNDEPOSITED);
        DepositRules.deposit(deposit, getArchetypeService());

        // reload the account to pick up the updates
        account = get(account);
        assertNotNull(account);

        IMObjectBean bean = getBean(account);
        Date lastDeposit = bean.getDate("lastDeposit");
        Date now = new Date();

        assertTrue(now.compareTo(lastDeposit) >= 0); // expect now >= lastDeposit
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        account = DepositTestHelper.createDepositAccount();
    }

    /**
     * Helper to create an <em>act.bankDepsit</em> wrapped in a bean.
     *
     * @param status the act status
     * @return a new act
     */
    private Act createDeposit(String status) {
        Act act = create(DepositArchetypes.BANK_DEPOSIT, Act.class);
        act.setStatus(status);
        IMObjectBean bean = getBean(act);
        bean.setTarget("depositAccount", account);
        return act;
    }

}
