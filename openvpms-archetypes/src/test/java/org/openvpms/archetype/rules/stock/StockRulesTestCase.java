/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.stock;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link StockRules} class.
 *
 * @author Tim Anderson
 */
public class StockRulesTestCase extends AbstractStockTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The rules.
     */
    private StockRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new StockRules(getArchetypeService());
    }

    /**
     * Tests the {@link StockRules#getStockLocation(Product, Party)} method.
     */
    @Test
    public void testGetStockLocation() {
        // set up a location with a stock location
        Party stockLocation = practiceFactory.createStockLocation();
        Party location = practiceFactory.newLocation().stockLocation(stockLocation).build();

        Product product = productFactory.createMerchandise();

        IMObjectBean locationBean = getBean(location);

        // by default, stock control is disabled, so no stock location
        // should be returned for the product
        assertFalse(locationBean.getBoolean("stockControl"));
        assertNull(rules.getStockLocation(product, location));

        // now enable stock control
        locationBean.setValue("stockControl", true);

        // should return either stock location
        assertNotNull(rules.getStockLocation(product, location));

        // now create a relationship with stockLocation
        rules.updateStock(product, stockLocation, BigDecimal.ONE);

        // verify stockLocation now returned for the product
        assertEquals(stockLocation, rules.getStockLocation(product, location));
    }

    /**
     * Tests the {@link StockRules#getStock(Product, Party)} and
     * {@link StockRules#updateStock(Product, Party, BigDecimal)} methods.
     */
    @Test
    public void testGetAndUpdateStock() {
        BigDecimal quantity = new BigDecimal("10.00");

        Party stockLocation = practiceFactory.createStockLocation();
        Product product = productFactory.createMedication();

        // no product-stock location relationship to begin with
        checkEquals(BigDecimal.ZERO, rules.getStock(product, stockLocation));

        // add stock and verify it is added
        rules.updateStock(product, stockLocation, quantity);
        checkEquals(quantity, rules.getStock(product, stockLocation));

        // remove stock and verify it is removed
        rules.updateStock(product, stockLocation, quantity.negate());
        checkEquals(BigDecimal.ZERO, rules.getStock(product, stockLocation));
    }

    /**
     * Tests the {@link StockRules#getStock(Reference, Reference)} method.
     */
    @Test
    public void testQueryStock() {
        BigDecimal quantity = new BigDecimal("10.00");

        Party stockLocation = practiceFactory.createStockLocation();
        Product product = productFactory.createMedication();

        // no product-stock location relationship to begin with
        Reference productRef = product.getObjectReference();
        Reference stockRef = stockLocation.getObjectReference();
        checkEquals(BigDecimal.ZERO, rules.getStock(productRef, stockRef));

        // add stock and verify it is added
        rules.updateStock(product, stockLocation, quantity);
        checkEquals(quantity, rules.getStock(productRef, stockRef));

        // remove stock and verify it is removed
        rules.updateStock(product, stockLocation, quantity.negate());
        checkEquals(BigDecimal.ZERO, rules.getStock(productRef, stockRef));
    }

    /**
     * Tests the {@link StockRules#transferStock} method.
     */
    @Test
    public void testTransferStock() {
        BigDecimal quantity = new BigDecimal("10.00");
        Party from = practiceFactory.createStockLocation();
        Party to = practiceFactory.createStockLocation();
        Product product = productFactory.createMerchandise();

        rules.transfer(product, from, to, quantity);
        checkEquals(quantity.negate(), rules.getStock(product, from));
        checkEquals(quantity, rules.getStock(product, to));
    }

    /**
     * Tests the {@link StockRules#hasStockRelationship(Product, Party)}
     * and {@link StockRules#getStockRelationship(Product, Party)} methods.
     */
    @Test
    public void testStockRelationships() {
        Product product = productFactory.createMerchandise();
        Party stockLocation = practiceFactory.createStockLocation();

        assertFalse(rules.hasStockRelationship(product, stockLocation));
        assertNull(rules.getStockRelationship(product, stockLocation));

        rules.updateStock(product, stockLocation, BigDecimal.ONE);
        assertTrue(rules.hasStockRelationship(product, stockLocation));
        assertNotNull(rules.getStockRelationship(product, stockLocation));

        // verify relationship still exists with no stock
        rules.updateStock(product, stockLocation, BigDecimal.ZERO);
        assertTrue(rules.hasStockRelationship(product, stockLocation));
        assertNotNull(rules.getStockRelationship(product, stockLocation));
    }
}
