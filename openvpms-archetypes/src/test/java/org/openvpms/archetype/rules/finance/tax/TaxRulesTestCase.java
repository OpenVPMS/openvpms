/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.tax;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;

/**
 * Tests the {@link TaxRules} class.
 *
 * @author Tim Anderson
 */
public class TaxRulesTestCase extends ArchetypeServiceTest {

    /**
     * The tax type classification.
     */
    private Lookup taxType;

    /**
     * The tax rules.
     */
    private TaxRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        taxType = TestHelper.createTaxType(BigDecimal.TEN);
        Party practice = create("party.organisationPractice", Party.class);
        rules = new TaxRules(practice, getArchetypeService());
    }

    /**
     * Tests the {@link TaxRules#getTaxRate(Product)} method.
     */
    @Test
    public void testGetTaxRate() {
        Product productNoTax = createProduct();
        BigDecimal noTax = rules.getTaxRate(productNoTax);
        checkEquals(BigDecimal.ZERO, noTax);

        Product product10Tax = createProductWithTax();
        BigDecimal percent10 = new BigDecimal(10);
        checkEquals(percent10, rules.getTaxRate(product10Tax));

        Product productType10Tax = createProductWithProductTypeTax();
        checkEquals(percent10, rules.getTaxRate(productType10Tax));
    }

    /**
     * Tests the {@link TaxRules#calculateTax(BigDecimal, Product, boolean)}
     * method.
     */
    @Test
    public void testCalculateTax() {
        Product productNoTax = createProduct();

        BigDecimal ten = new BigDecimal(10);
        checkEquals(BigDecimal.ZERO, rules.calculateTax(ten, productNoTax, false));
        checkEquals(BigDecimal.ZERO, rules.calculateTax(ten, productNoTax, true));

        Product product10Tax = createProductWithTax();
        checkEquals(BigDecimal.ONE, rules.calculateTax(ten, product10Tax, false));
        checkEquals(new BigDecimal("0.909"), rules.calculateTax(ten, product10Tax, true));

        Product productType10Tax = createProductWithProductTypeTax();
        checkEquals(BigDecimal.ONE, rules.calculateTax(ten, productType10Tax, false));
        checkEquals(new BigDecimal("0.909"), rules.calculateTax(ten, productType10Tax, true));
    }

    /**
     * Verifies that if a product has a product type relationship, but with no product type, a zero tax rate
     * is returned.
     * <p/>
     * This tests the fix for <em>OVPMS-946 NullPointerException on supplier change, when editing
     * products</em>.
     */
    @Test
    public void testGetTaxRateForInvalidProductType() {
        Product productNoTax = createProduct();
        Entity productType = create(ProductArchetypes.PRODUCT_TYPE, Entity.class);
        IMObjectBean bean = getBean(productNoTax);
        Relationship relationship = bean.addTarget("type", productType);
        relationship.setTarget(null);
        BigDecimal rate = rules.getTaxRate(productNoTax);
        checkEquals(BigDecimal.ZERO, rate);
    }


    /**
     * Helper to create a product.
     *
     * @return a new product
     */
    private Product createProduct() {
        Product product = create("product.medication", Product.class);
        IMObjectBean bean = getBean(product);
        bean.setValue("name", "TaxRulesTestCase-product" + product.hashCode());
        return product;
    }

    /**
     * Helper to create and save a product with a 10% tax type classification.
     *
     * @return a new product
     */
    private Product createProductWithTax() {
        Product product = createProduct();
        product.addClassification(taxType);
        save(product);
        return product;
    }

    /**
     * Helper to create and save a product with a product type relationship.
     * The associated <em>entity.productType</em> has a 10% tax type
     * classification.
     *
     * @return a new product
     */
    private Product createProductWithProductTypeTax() {
        Product product = TestHelper.createProduct();
        Entity type = create(ProductArchetypes.PRODUCT_TYPE, Entity.class);
        type.setName("TaxRulesTestCase-entity" + type.hashCode());
        type.addClassification(taxType);
        IMObjectBean bean = getBean(product);
        bean.addTarget("type", type);
        save(product, type);
        return product;
    }

}
