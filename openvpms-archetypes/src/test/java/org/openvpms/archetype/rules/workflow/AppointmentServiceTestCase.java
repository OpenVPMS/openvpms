/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.cache.DayCache;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestAppointmentBuilder;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.cache.BasicEhcacheManager;
import org.openvpms.component.business.service.cache.EhcacheManager;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.util.PropertySet;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.createPatient;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link AppointmentService}.
 *
 * @author Tim Anderson
 */
public class AppointmentServiceTestCase extends AbstractScheduleServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The schedule.
     */
    private Entity schedule;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        location = practiceFactory.createLocation();
        schedule = schedulingFactory.createSchedule(location);
    }

    /**
     * Tests addition of an appointment.
     */
    @Test
    public void testAddEvent() {
        Date date1 = getDate("2008-01-01");
        Date date2 = getDate("2008-01-02");
        Date date3 = getDate("2008-01-03");

        // retrieve the appointments for date1 and date2 and verify they are empty.
        // This caches the appointments for each date.
        ScheduleService service = initScheduleService(30);
        long hash1 = checkEvents(schedule, date1, 0);
        checkEvents(schedule, date2, 0);

        // create and save appointment for date1
        Act appointment = createAppointment(date1);

        ScheduleEvents events3 = service.getScheduleEvents(schedule, date1);
        assertEquals(1, events3.size());
        PropertySet set = events3.getEvents().get(0);
        checkAppointment(appointment, set);
        assertNotEquals(hash1, events3.getModHash());  // hash should have changed

        checkEvents(schedule, date2, 0);
        checkEvents(schedule, date3, 0);
    }

    /**
     * Tests removal of an event.
     */
    @Test
    public void testRemoveEvent() {
        Date date1 = getDate("2008-01-01");
        Date date2 = getDate("2008-01-02");
        Date date3 = getDate("2008-01-03");

        initScheduleService(30);

        // retrieve the appointments for date1 and date2 and verify they are empty.
        // This caches the appointments for each date.

        long hash1 = checkEvents(schedule, date1, 0);
        long hash2 = checkEvents(schedule, date2, 0);
        long hash3 = checkEvents(schedule, date3, 0);
        assertNotEquals(hash1, hash2);
        assertNotEquals(hash1, hash3);

        // create and save appointment for date1
        Act appointment = createAppointment(date1);
        long hash4 = checkEvents(schedule, date1, 1);
        assertNotEquals(hash1, hash4);
        checkEvents(schedule, date2, 0, hash2);
        checkEvents(schedule, date3, 0, hash3);

        // now remove it
        remove(appointment);

        // verify it has been removed
        long hash5 = checkEvents(schedule, date1, 0);
        assertNotEquals(hash1, hash5);

        checkEvents(schedule, date2, 0, hash2);
        checkEvents(schedule, date3, 0, hash3);
    }

    /**
     * Tests the {@link AppointmentService#getEvents} method.
     */
    @Test
    public void testGetEvents() {
        final int count = 10;
        Entity schedule = schedulingFactory.createSchedule(location);
        Act[] appointments = new Act[count];
        Date date = getDate("2007-01-01");
        for (int i = 0; i < count; ++i) {
            Date startTime = DateRules.getDate(date, 15 * count, DateUnits.MINUTES);
            Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
            Date arrivalTime = (i % 2 == 0) ? new Date() : null;
            Party patient = patientFactory.createPatient();
            User clinician = userFactory.createClinician();
            Act appointment = newAppointment(startTime, endTime, schedule, patient)
                    .clinician(clinician)
                    .arrivalTime(arrivalTime)
                    .build();
            appointments[i] = appointment;
        }

        ScheduleService service = initScheduleService(30);
        List<PropertySet> results = service.getEvents(schedule, date);
        assertEquals(count, results.size());
        for (int i = 0; i < results.size(); ++i) {
            PropertySet set = results.get(i);
            checkAppointment(appointments[i], set);
        }
    }

    /**
     * Tests moving an event from one date to another.
     */
    @Test
    public void testChangeEventDate() {
        Date date1 = getDate("2008-01-01");
        Date date2 = getDate("2008-03-01");

        initScheduleService(30);
        long hash1 = checkEvents(schedule, date1, 0);
        long hash2 = checkEvents(schedule, date2, 0);

        Act act = createAppointment(date1);

        long hash3 = checkEvents(schedule, date1, 1);
        assertNotEquals(hash1, hash3);
        checkEvents(schedule, date2, 0, hash2);

        act.setActivityStartTime(date2); // move it to date2
        act.setActivityEndTime(DateRules.getDate(date2, 15, DateUnits.MINUTES));
        save(act);

        long hash4 = checkEvents(schedule, date1, 0);
        long hash5 = checkEvents(schedule, date2, 1);
        assertNotEquals(hash3, hash4);
        assertNotEquals(hash2, hash5);
    }

    /**
     * Verifies that a new to lookup.visitReason appears in new appointments.
     */
    @Test
    public void testAddReason() {
        Date date1 = getDate("2008-01-01");
        Date date2 = getDate("2008-01-02");
        Party patient = TestHelper.createPatient();

        // create and save appointment for date1
        Act appointment1 = createAppointment(date1);
        ScheduleService service = initScheduleService(30);
        ScheduleEvents events = service.getScheduleEvents(schedule, date1);
        assertEquals(1, events.size());
        PropertySet set = events.getEvents().get(0);
        checkAppointment(appointment1, set);

        String code = "XREASON" + System.currentTimeMillis();
        String name = "Added reason";
        Lookup reason = TestHelper.getLookup(ScheduleArchetypes.VISIT_REASON, code, name, false);
        save(reason); // make sure IArchetypeRuleService used

        Act appointment2 = createAppointment(date2, schedule, patient, false);
        appointment2.setReason(code);
        save(appointment2);

        // verify the reason code and name appears in the new event
        ScheduleEvents events2 = service.getScheduleEvents(schedule, date2);
        assertEquals(1, events2.size());
        set = events2.getEvents().get(0);
        assertEquals(code, set.getString(ScheduleEvent.ACT_REASON));
        assertEquals(name, set.getString(ScheduleEvent.ACT_REASON_NAME));
    }

    /**
     * Verifies that changes to lookup.visitReason get reflected in the cached appointments.
     */
    @Test
    public void testUpdateReason() {
        Date date = getDate("2008-01-01");
        ScheduleService service = initScheduleService(30);

        lookupFactory.createLookup(ScheduleArchetypes.VISIT_REASON, "CHECKUP", true);

        // create and save appointment for date
        Act appointment = createAppointment(date);
        ScheduleEvents events1 = service.getScheduleEvents(schedule, date);
        assertEquals(1, events1.size());
        PropertySet set1 = events1.getEvents().get(0);
        checkAppointment(appointment, set1);

        Lookup reason = getLookupService().getLookup(appointment, "reason");
        assertNotNull(reason);
        String name = "New reason: " + System.currentTimeMillis();
        reason.setName(name);
        save(reason);

        ScheduleEvents events2 = service.getScheduleEvents(schedule, date);
        assertEquals(1, events2.size());
        PropertySet set2 = events2.getEvents().get(0);
        assertEquals(reason.getCode(), set2.getString(ScheduleEvent.ACT_REASON));
        assertEquals(name, set2.getString(ScheduleEvent.ACT_REASON_NAME));
        assertNotEquals(events1.getModHash(), events2.getModHash());
    }

    /**
     * Verifies that if a lookup.visitReason is removed, the cache is updated.
     * <p>
     * Strictly speaking, the application shouldn't remove a lookup in use, but if it occurs,
     * this implementation will return null for the reason name.
     */
    @Test
    public void testRemoveReason() {
        Date date = getDate("2008-01-01");
        Party patient = TestHelper.createPatient();

        ScheduleService service = initScheduleService(30);
        // create and save appointment for date
        Act appointment = createAppointment(date, schedule, patient, false);
        String code = "XREASON" + System.currentTimeMillis();
        String name = "Reason to remove";
        Lookup reason = TestHelper.getLookup(ScheduleArchetypes.VISIT_REASON, code, name, false);
        save(reason); // make sure IArchetypeRuleService used
        appointment.setReason(code);
        save(appointment);

        ScheduleEvents events1 = service.getScheduleEvents(schedule, date);
        assertEquals(1, events1.size());
        PropertySet set1 = events1.getEvents().get(0);

        assertEquals(code, set1.getString(ScheduleEvent.ACT_REASON));
        assertEquals(name, set1.getString(ScheduleEvent.ACT_REASON_NAME));

        // now remove the appointment and the reason lookup
        remove(appointment);
        remove(reason);

        appointment = createAppointment(date, schedule, patient, false);
        appointment.setReason(code);
        save(appointment);

        ScheduleEvents events2 = service.getScheduleEvents(schedule, date);
        assertEquals(1, events2.size());
        PropertySet set2 = events2.getEvents().get(0);
        assertEquals(code, set2.getString(ScheduleEvent.ACT_REASON));
        assertNull(set2.getString(ScheduleEvent.ACT_REASON_NAME));
        assertNotEquals(events1.getModHash(), events2.getModHash());
    }

    /**
     * Verifies that appointments can span multiple days.
     */
    @Test
    public void testMultipleDayAppointment() {
        Date start = getDate("2008-01-01");
        Date end = getDate("2008-01-05");
        Party patient = TestHelper.createPatient();

        ScheduleService service = initScheduleService(30);

        // retrieve the appointments from start to end and verify they are empty.
        // This caches the appointments for each date.
        ScheduleEvents events1 = service.getScheduleEvents(schedule, start, end);
        assertEquals(0, events1.size());

        // create and save a multiple day appointment
        Date startTime = DateRules.getDate(start, 15, DateUnits.MINUTES);
        Date endTime = DateRules.getDate(end, 15, DateUnits.MINUTES);
        Act appointment = createAppointment(startTime, endTime, schedule, patient, true);

        // verify the appointment is returned for each day
        checkAppointment(appointment, 5);

        // update the start time for the appointment, and verify its not present on the original days
        startTime = DateRules.getDate(startTime, 2, DateUnits.DAYS);
        appointment.setActivityStartTime(startTime);
        save(appointment);
        ScheduleEvents events2 = service.getScheduleEvents(schedule, start, DateRules.getNextDate(start));
        assertEquals(0, events2.size());
        assertNotEquals(events1.getModHash(), events2.getModHash());
        assertEquals(events2.getModHash(), service.getModHash(schedule, start, DateRules.getNextDate(start)));

        // verify the appointment exists for the subsequent days
        checkAppointment(appointment, 3);

        // reduce the end time for the appointment, and verify its not present on the original days
        Date newEndTime = DateRules.getDate(endTime, -1, DateUnits.DAYS);
        appointment.setActivityEndTime(newEndTime);
        save(appointment);
        checkAppointment(appointment, 2);

        ScheduleEvents events3 = service.getScheduleEvents(schedule, endTime);
        assertEquals(0, events3.size());
    }

    /**
     * Tests repeatedly reading the same sets of events for a cache size smaller than the number of dates read.
     */
    @Test
    public void testRepeatedSequentialRead() {
        Date start = getDate("2008-01-01");
        Party patient = createPatient();

        Set<?>[] days = new Set[10];

        // create some appointments
        for (int i = 0; i < 10; ++i) {
            Set<Long> appointments = new HashSet<>();
            Date date = DateRules.getDate(start, i, DateUnits.DAYS);
            for (int j = 0; j < 100; ++j) {
                Act appointment = createAppointment(date, schedule, patient, true);
                appointments.add(appointment.getId());
            }
            days[i] = appointments;
        }

        ScheduleService service = initScheduleService(6); // cache 6 days

        // repeatedly read 10 days worth of appointments. This will force the cache to shed and re-read data.
        for (int j = 0; j < 10; ++j) {
            for (int k = 0; k < 10; ++k) {
                Date date = DateRules.getDate(start, k, DateUnits.DAYS);
                Set<Long> ids = getIds(date, schedule, service);
                assertEquals(days[k], ids);
            }
        }
    }

    /**
     * Tests the behaviour of {@link AppointmentService#getOverlappingEvent(Act)} method.
     */
    @Test
    public void testGetOverlappingEvent() {
        AppointmentService service = initScheduleService(1);
        Date start = getDatetime("2015-05-14 09:00:00");
        Date end = getDatetime("2015-05-14 09:15:00");

        Entity appointmentType = ScheduleTestHelper.createAppointmentType();
        Party schedule1 = ScheduleTestHelper.createSchedule(15, "MINUTES", 2, appointmentType, location);
        Party schedule2 = ScheduleTestHelper.createSchedule(15, "MINUTES", 2, appointmentType, location);
        save(schedule1);
        save(schedule2);

        Act appointment = createAppointment(start, end, schedule1, false);
        assertNull(service.getOverlappingEvent(appointment));
        save(appointment);
        assertNull(service.getOverlappingEvent(appointment));

        Act exactOverlap = createAppointment(start, end, schedule1, false);
        Times expected = Times.create(appointment);
        assertEquals(expected, service.getOverlappingEvent(exactOverlap));

        Act overlap = createAppointment(getDatetime("2015-05-14 09:05:00"), getDatetime("2015-05-14 09:10:00"),
                                        schedule1, true);
        assertEquals(expected, service.getOverlappingEvent(overlap));

        Act after = createAppointment(getDatetime("2015-05-14 09:15:00"), getDatetime("2015-05-14 09:30:00"),
                                      schedule1, false);
        assertNull(service.getOverlappingEvent(after));

        Act before = createAppointment(getDatetime("2015-05-14 08:45:00"), getDatetime("2015-05-14 09:00:00"),
                                       schedule1, false);
        assertNull(service.getOverlappingEvent(before));

        // now verify there are no overlaps for the same time but different schedule
        Act appointment2 = createAppointment(start, end, schedule2, false);
        assertNull(service.getOverlappingEvent(appointment2));
        save(appointment2);
        assertNull(service.getOverlappingEvent(appointment2));

        // verify there are no overlaps for an unpopulated act
        Act appointment3 = create(ScheduleArchetypes.APPOINTMENT, Act.class);
        assertNull(service.getOverlappingEvent(appointment3));
        appointment3.setActivityStartTime(null);
        appointment3.setActivityEndTime(null);
        assertNull(service.getOverlappingEvent(appointment3));
    }

    /**
     * Tests the {@link AppointmentService#getOverlappingEvent(List, Entity)} method.
     */
    @Test
    public void getOverlappingEventTimes() {
        AppointmentService service = initScheduleService(1);
        Date start1 = getDatetime("2015-05-14 09:00:00");
        Date end1 = getDatetime("2015-05-14 09:15:00");
        Date start2 = getDatetime("2015-05-15 09:00:00");
        Date end2 = getDatetime("2015-05-15 09:15:00");
        Date beforeStart = getDatetime("2015-05-15 08:45:00");
        Date beforeEnd = getDatetime("2015-05-15 09:00:00");
        Date afterStart = getDatetime("2015-05-15 09:30:00");
        Date afterEnd = getDatetime("2015-05-15 09:45:00");
        Date overlap1Start = getDatetime("2015-05-15 09:05:00");
        Date overlap1End = getDatetime("2015-05-15 09:20:00");
        Date overlap2Start = getDatetime("2015-05-15 09:10:00");
        Date overlap2End = getDatetime("2015-05-15 09:25:00");

        Times times1 = new Times(start1, end1);
        Times times2 = new Times(start2, end2);
        List<Times> list = Arrays.asList(times1, times2);
        assertNull(service.getOverlappingEvent(list, schedule));

        // overlaps time1 exactly
        Act appointment1 = createAppointment(start1, end1, schedule, true);
        assertEquals(Times.create(appointment1), service.getOverlappingEvent(list, schedule));
        remove(appointment1);

        // overlaps time2 exactly
        Act appointment2 = createAppointment(start2, end2, schedule, true);
        assertEquals(Times.create(appointment2), service.getOverlappingEvent(list, schedule));
        remove(appointment2);

        // before time2
        createAppointment(beforeStart, beforeEnd, schedule, true);
        assertNull(service.getOverlappingEvent(list, schedule));

        // after time2
        createAppointment(afterStart, afterEnd, schedule, true);
        assertNull(service.getOverlappingEvent(list, schedule));

        // intersects start of time2
        Act appointment5 = createAppointment(overlap1Start, overlap1End, schedule, true);
        assertEquals(Times.create(appointment5), service.getOverlappingEvent(list, schedule));
        remove(appointment5);

        // intersects end of time2
        Act appointment6 = createAppointment(overlap2Start, overlap2End, schedule, true);
        assertEquals(Times.create(appointment6), service.getOverlappingEvent(list, schedule));
    }

    /**
     * Tests the {@link AppointmentService#getOverlappingEvents(List, Entity, int)} method.
     */
    @Test
    public void testGetOverlappingEvents() {
        AppointmentService service = initScheduleService(1);
        Date start1 = getDatetime("2015-05-14 09:00:00");
        Date end1 = getDatetime("2015-05-14 09:15:00");
        Date start2 = getDatetime("2015-05-15 09:00:00");
        Date end2 = getDatetime("2015-05-15 09:15:00");
        Date beforeStart = getDatetime("2015-05-15 08:45:00");
        Date beforeEnd = getDatetime("2015-05-15 09:00:00");
        Date afterStart = getDatetime("2015-05-15 09:30:00");
        Date afterEnd = getDatetime("2015-05-15 09:45:00");
        Date overlap1Start = getDatetime("2015-05-15 09:05:00");
        Date overlap1End = getDatetime("2015-05-15 09:20:00");
        Date overlap2Start = getDatetime("2015-05-15 09:10:00");
        Date overlap2End = getDatetime("2015-05-15 09:25:00");

        Times times1 = new Times(start1, end1);
        Times times2 = new Times(start2, end2);
        List<Times> list = Arrays.asList(times1, times2);
        assertNull(service.getOverlappingEvents(list, schedule, 1));

        // overlaps time1 exactly
        Act appointment1 = createAppointment(start1, end1, schedule, true);
        checkOverlappingEvents(service.getOverlappingEvents(list, schedule, 1), times1);
        remove(appointment1);

        // overlaps time2 exactly
        Act appointment2 = createAppointment(start2, end2, schedule, true);
        checkOverlappingEvents(service.getOverlappingEvents(list, schedule, 1), times2);
        remove(appointment2);

        // before time2
        createAppointment(beforeStart, beforeEnd, schedule, true);
        assertNull(service.getOverlappingEvents(list, schedule, 1));

        // after time2
        createAppointment(afterStart, afterEnd, schedule, true);
        assertNull(service.getOverlappingEvents(list, schedule, 1));

        // intersects start of time2
        Act appointment5 = createAppointment(overlap1Start, overlap1End, schedule, true);
        checkOverlappingEvents(service.getOverlappingEvents(list, schedule, 1), Times.create(appointment5));
        remove(appointment5);

        // intersects end of time2
        Act appointment6 = createAppointment(overlap2Start, overlap2End, schedule, true);
        checkOverlappingEvents(service.getOverlappingEvents(list, schedule, 1), Times.create(appointment6));
    }

    /**
     * Tests addition of a calendar block.
     *
     * @throws Exception the exception
     */
    @Test
    public void testAddCalendarBlock() throws Exception {
        Date date1 = getDate("2008-01-01");
        Date date2 = getDate("2008-01-02");
        Date date3 = getDate("2008-01-03");

        // retrieve the events for date1 and date2 and verify they are empty.
        // This caches the events for each date.
        ScheduleService service = initScheduleService(30);
        long hash1 = checkEvents(schedule, date1, 0);
        checkEvents(schedule, date2, 0);

        // create and save a calendar block for date1
        Act block = createCalendarBlock(date1);

        ScheduleEvents events3 = service.getScheduleEvents(schedule, date1);
        assertEquals(1, events3.size());
        PropertySet set = events3.getEvents().get(0);
        checkCalendarBlock(block, set);
        assertNotEquals(hash1, events3.getModHash());  // hash should have changed

        checkEvents(schedule, date2, 0);
        checkEvents(schedule, date3, 0);

        destroyService(service);

        // verify that when loaded from the db, the event is the same
        service = initScheduleService(30);
        ScheduleEvents events4 = service.getScheduleEvents(schedule, date1);
        assertEquals(1, events4.size());
        checkCalendarBlock(block, events4.getEvents().get(0));
    }

    /**
     * Verifies that an appointment is cached when the save notification happens prior to the cache being loaded
     * for the first time.<p/>
     * This should be equivalent to the cache being loaded without notification, as the appointment notification
     * is discarded while the schedule isn't cached.
     *
     * @throws Exception for any error
     */
    @Test
    public void testAddAppointmentPriorToCacheLoad() throws Exception {
        Date date = new Date();
        Semaphore load = new Semaphore(0); // used to prevent load until after notification

        // flags to track if the cache has elements
        AtomicBoolean addEventCacheNonEmpty = new AtomicBoolean();
        AtomicBoolean preLoadCacheNonEmpty = new AtomicBoolean();

        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {
            @Override
            protected void addEvent(Act event) {
                super.addEvent(event);
                // the appointment should be discarded as the schedule won't be cached
                addEventCacheNonEmpty.set(getCache().iterator().hasNext());
                load.release();
            }

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {
                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache load(Reference reference, Entity entity, Date from, Date to) {
                        preLoadCacheNonEmpty.set(getCache().iterator().hasNext());    // cache should be empty
                        load.acquireUninterruptibly(); // wait for the save notification before loading
                        return super.load(reference, entity, from, to);
                    }
                };
            }
        };
        setScheduleService(service);

        Act[] actHandle = new Act[1];
        ScheduleEvents[] eventHandle = new ScheduleEvents[1];

        // run the save and retrieve in separate threads
        Runnable add = () -> actHandle[0] = createAppointment(date);
        Runnable retrieve = () -> eventHandle[0] = service.getScheduleEvents(schedule, date);
        runConcurrent(add, retrieve);

        // check flags
        assertFalse(addEventCacheNonEmpty.get());
        assertFalse(preLoadCacheNonEmpty.get());

        // cache should now be non-empty
        assertTrue(service.getCache().iterator().hasNext());

        // check appointment vs cache
        checkEvents(eventHandle[0], actHandle);
    }

    /**
     * Verifies that an appointment is cached when the save notification happens after the cache being created
     * but before being loaded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testAddAppointmentAfterCacheCreation() throws Exception {
        Date date = new Date();
        Semaphore cacheCreation = new Semaphore(0);
        Semaphore eventAdd = new Semaphore(0);
        AtomicInteger postAddSize = new AtomicInteger();
        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {
            @Override
            protected void addEvent(Act event) {
                cacheCreation.acquireUninterruptibly(); // wait for the cache to be created
                super.addEvent(event);
                eventAdd.release();                     // allow the cache to be loaded
            }

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {
                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache getDayCache(Reference reference, Date from, Date to) {
                        DayCache cache = super.getDayCache(reference, from, to);
                        cacheCreation.release();
                        eventAdd.acquireUninterruptibly();
                        postAddSize.set(cache.getEvents().size()); // should be zero
                        return cache;
                    }
                };
            }
        };
        setScheduleService(service);

        Act[] actHandle = new Act[1];

        Runnable add = () -> actHandle[0] = createAppointment(date);
        Runnable retrieve = () -> service.getEvents(schedule, date);
        runConcurrent(add, retrieve);

        assertEquals(0, postAddSize.get());

        ScheduleEvents events = service.getScheduleEvents(schedule, date);
        checkEvents(events, actHandle);
    }

    /**
     * Verifies that an appointment is cached when the save happens after the cache being created
     * but before being loaded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSaveAppointmentAfterCacheCreation() throws Exception {
        Date date = new Date();
        Semaphore cacheCreation = new Semaphore(0);
        Semaphore eventSave = new Semaphore(0);
        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {
                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache getDayCache(Reference reference, Date from, Date to) {
                        DayCache cache = super.getDayCache(reference, from, to);
                        cacheCreation.release();
                        eventSave.acquireUninterruptibly();
                        return cache;
                    }
                };
            }
        };
        setScheduleService(service);

        Act[] actHandle = new Act[1];

        Runnable add = () -> {
            cacheCreation.acquireUninterruptibly();
            actHandle[0] = createAppointment(date);
            eventSave.release();
        };
        Runnable retrieve = () -> service.getScheduleEvents(schedule, date);
        runConcurrent(add, retrieve);

        ScheduleEvents events = service.getScheduleEvents(schedule, date);
        checkEvents(events, actHandle);
    }

    /**
     * Verifies that an appointment is cached when the save happens after the cache is loaded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSaveAppointmentAfterCacheLoad() throws Exception {
        Date date = new Date();
        Semaphore load = new Semaphore(0);
        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {

                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache load(Reference reference, Entity entity, Date from, Date to) {
                        DayCache cache = super.load(reference, entity, from, to);
                        load.release();
                        return cache;
                    }
                };
            }
        };
        setScheduleService(service);

        Act[] actHandle = new Act[1];

        Runnable add = () -> {
            load.acquireUninterruptibly();
            actHandle[0] = createAppointment(date);
        };
        Runnable retrieve = () -> service.getScheduleEvents(schedule, date);
        runConcurrent(add, retrieve);

        ScheduleEvents events = service.getScheduleEvents(schedule, date);
        checkEvents(events, actHandle);
    }

    /**
     * Tests moving an appointment from one date to another, where the target date is not cached.
     */
    @Test
    public void testMoveAppointmentToUncachedDay() {
        Date date1 = DateRules.getToday();
        Date date2 = DateRules.getNextDate(date1);
        ScheduleService service = initScheduleService(10);

        Act appointment = createAppointment(date1);
        ScheduleEvents events1A = service.getScheduleEvents(schedule, date1);
        checkEvents(events1A, appointment);

        appointment.setActivityStartTime(date2);
        appointment.setActivityEndTime(DateRules.getDate(date2, 15, DateUnits.MINUTES));
        save(appointment);

        ScheduleEvents events1B = service.getScheduleEvents(schedule, date1);
        checkEvents(events1B);

        ScheduleEvents events2 = service.getScheduleEvents(schedule, date2);
        checkEvents(events2, appointment);
    }

    /**
     * Verifies that if an appointment is moved to a date that is not cached during the cache load.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMoveAppointmentToUncachedDayDuringCacheLoad() throws Exception {
        Date date1 = DateRules.getToday();
        Date date2 = DateRules.getNextDate(date1);

        // pre-create the appointment, to ensure it is not cached
        Act appointment = createAppointment(date1);

        AtomicInteger addEventCount = new AtomicInteger();
        AtomicInteger loadCount = new AtomicInteger();
        Semaphore move = new Semaphore(1);      // forces load() to wait till after the appointment is moved
        Semaphore load = new Semaphore(0);      // forces addEvent() to wait until load()  is invoked twice
        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {
            @Override
            protected void addEvent(Act event) {
                addEventCount.incrementAndGet();
                move.release();
                load.acquireUninterruptibly(2); // wait for 2 caches to be created
                super.addEvent(event);
            }

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {
                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache load(Reference reference, Entity entity, Date from, Date to) {
                        loadCount.incrementAndGet();
                        move.acquireUninterruptibly();
                        DayCache cache = super.load(reference, entity, from, to);
                        load.release();
                        return cache;
                    }
                };
            }
        };
        setScheduleService(service);

        // now load the events for date1
        ScheduleEvents events1 = service.getScheduleEvents(schedule, date1);
        checkEvents(events1, appointment);

        appointment.setActivityStartTime(date2);
        appointment.setActivityEndTime(DateRules.getDate(date2, 15, DateUnits.MINUTES));

        // move the appointment. The addEvent notification will block until date2 is loaded
        Runnable moveAppointment = () -> save(appointment);
        Runnable retrieve = () -> service.getScheduleEvents(schedule, date2);
        runConcurrent(moveAppointment, retrieve);

        ScheduleEvents events2 = service.getScheduleEvents(schedule, date2);
        checkEvents(events2, appointment);

        // event should no longer be present on day1
        ScheduleEvents events3 = service.getScheduleEvents(schedule, date1);
        checkEvents(events3);

        // check methods were called the right no. of times
        assertEquals(1, addEventCount.get());
        assertEquals(2, loadCount.get());
    }

    /**
     * Verifies that if an appointment is moved to a schedule that is not cached during the cache load.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMoveAppointmentToUncachedScheduleDuringCacheLoad() throws Exception {
        Date date = DateRules.getToday();

        // pre-create the appointment, to ensure it is not cached
        Act appointment = createAppointment(date);

        AtomicInteger addEventCount = new AtomicInteger();
        AtomicInteger loadCount = new AtomicInteger();
        Semaphore move = new Semaphore(1);      // forces load() to wait till after the appointment is moved
        Semaphore load = new Semaphore(0);      // forces addEvent() to wait until load()  is invoked twice
        AppointmentService service = new AppointmentService(getArchetypeService(), getLookupService(),
                                                            new BasicEhcacheManager(30)) {
            @Override
            protected void addEvent(Act event) {
                addEventCount.incrementAndGet();
                move.release();
                load.acquireUninterruptibly(2); // wait for 2 caches to be created
                super.addEvent(event);
            }

            @Override
            ScheduleEventCache createScheduleEventCache(EhcacheManager cacheManager, String cacheName,
                                                        ScheduleEventFactory factory) {
                return new ScheduleEventCache(cacheManager, cacheName, factory) {
                    @Override
                    protected DayCache load(Reference reference, Entity entity, Date from, Date to) {
                        loadCount.incrementAndGet();
                        move.acquireUninterruptibly();
                        DayCache cache = super.load(reference, entity, from, to);
                        load.release();
                        return cache;
                    }
                };
            }
        };
        setScheduleService(service);

        // now load the events for date1
        ScheduleEvents events1 = service.getScheduleEvents(schedule, date);
        checkEvents(events1, appointment);

        Entity schedule2 = ScheduleTestHelper.createSchedule(location);
        IMObjectBean bean = getBean(appointment);
        bean.setTarget("schedule", schedule2);

        // move the appointment. The addEvent notification will block until schedule2 is loaded
        Runnable moveAppointment = () -> save(appointment);
        Runnable retrieve = () -> service.getScheduleEvents(schedule2, date);
        runConcurrent(moveAppointment, retrieve);

        ScheduleEvents events2 = service.getScheduleEvents(schedule2, date);
        checkEvents(events2, appointment);

        // event should no longer be present in schedule
        ScheduleEvents events3 = service.getScheduleEvents(schedule, date);
        checkEvents(events3);

        // check methods were called the right no. of times
        assertEquals(1, addEventCount.get());
        assertEquals(2, loadCount.get());
    }

    /**
     * Tests the {@link AppointmentService#getAppointmentsForClinician(User, Date, Date, Act)}
     * and {@link AppointmentService#getAppointmentsForClinician(User, Date, Date)} methods.
     */
    @Test
    public void testGetAppointmentsForClinician() {
        User clinician1 = userFactory.createClinician();
        User clinician2 = userFactory.createClinician();
        Date yesterday = DateRules.getYesterday();
        Date today = DateRules.getToday();
        Date tomorrow = DateRules.getTomorrow();
        Act act1 = createAppointment(today, clinician1, AppointmentStatus.PENDING);
        Act act2 = createAppointment(today, clinician1, AppointmentStatus.CONFIRMED);
        Act act3 = createAppointment(today, clinician1, AppointmentStatus.CHECKED_IN);
        Act act4 = createAppointment(today, clinician1, AppointmentStatus.IN_PROGRESS);
        Act act5 = createAppointment(today, clinician1, AppointmentStatus.BILLED);
        Act act6 = createAppointment(today, clinician1, AppointmentStatus.ADMITTED);
        Act act7 = createAppointment(today, clinician1, AppointmentStatus.COMPLETED);
        createAppointment(today, clinician1, AppointmentStatus.CANCELLED);
        createAppointment(today, clinician1, AppointmentStatus.NO_SHOW);

        AppointmentService service = initScheduleService(30);
        assertEquals(0, service.getAppointmentsForClinician(clinician1, yesterday, today).size());
        List<ScheduleTimes> appointments1 = service.getAppointmentsForClinician(clinician1, today, tomorrow);
        assertEquals(7, appointments1.size());
        checkScheduleTimes(appointments1.get(0), act1);
        checkScheduleTimes(appointments1.get(1), act2);
        checkScheduleTimes(appointments1.get(2), act3);
        checkScheduleTimes(appointments1.get(3), act4);
        checkScheduleTimes(appointments1.get(4), act5);
        checkScheduleTimes(appointments1.get(5), act6);
        checkScheduleTimes(appointments1.get(6), act7);

        assertEquals(0, service.getAppointmentsForClinician(clinician2, today, tomorrow).size());

        List<ScheduleTimes> appointments2 = service.getAppointmentsForClinician(clinician1, today, tomorrow,
                                                                                act1);
        assertEquals(6, appointments2.size());
        checkScheduleTimes(appointments2.get(0), act2);
        checkScheduleTimes(appointments2.get(1), act3);
        checkScheduleTimes(appointments2.get(2), act4);
        checkScheduleTimes(appointments2.get(3), act5);
        checkScheduleTimes(appointments2.get(4), act6);
        checkScheduleTimes(appointments2.get(5), act7);
    }

    /**
     * Creates a new {@link ScheduleService}.
     *
     * @param scheduleCacheSize the maximum number of schedule days to cache
     * @return the new service
     */
    @Override
    protected AppointmentService createScheduleService(int scheduleCacheSize) {
        return new AppointmentService(getArchetypeService(), getLookupService(),
                                      new BasicEhcacheManager(scheduleCacheSize));
    }

    /**
     * Creates a new schedule.
     *
     * @return the new schedule
     */
    @Override
    protected Entity createSchedule() {
        return ScheduleTestHelper.createSchedule(location);
    }

    /**
     * Creates a new event for the specified schedule and date.
     *
     * @param schedule the schedule
     * @param date     the date
     * @param patient  the patient. May be {@code null}
     * @return the new event act
     */
    @Override
    protected Act createEvent(Entity schedule, Date date, Party patient) {
        return createAppointment(date, schedule, patient, true);
    }

    /**
     * Initialises the {@link ScheduleService}.
     * <p>
     * This will be destroyed by {@link #tearDown()}.
     *
     * @param scheduleCacheSize the maximum number of schedule days to cache
     * @return the new service
     */
    @Override
    protected AppointmentService initScheduleService(int scheduleCacheSize) {
        return (AppointmentService) super.initScheduleService(scheduleCacheSize);
    }

    /**
     * Checks a {@link ScheduleTimes} instance matches a corresponding appointment.
     *
     * @param times    the instance to check
     * @param expected the appointment
     */
    private void checkScheduleTimes(ScheduleTimes times, Act expected) {
        assertEquals(expected.getObjectReference(), times.getReference());
        assertEquals(expected.getActivityStartTime(), times.getStartTime());
        assertEquals(expected.getActivityEndTime(), times.getEndTime());
        assertEquals(schedule.getObjectReference(), times.getSchedule());
    }

    /**
     * Verify a list of appointments match events.
     *
     * @param events       the events
     * @param appointments the appointments
     */
    private void checkEvents(ScheduleEvents events, Act... appointments) {
        assertNotNull(events);
        assertEquals(events.size(), appointments.length);
        for (Act appointment : appointments) {
            assertNotNull(appointment);
            Reference reference = appointment.getObjectReference();
            boolean found = false;
            for (PropertySet event : events.getEvents()) {
                if (reference.equals(event.getReference(ScheduleEvent.ACT_REFERENCE))) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }
    }

    /**
     * Verifies that overlapping events match those expected.
     *
     * @param events the overlapping events
     * @param times  the expected times
     */
    private void checkOverlappingEvents(List<Times> events, Times... times) {
        assertEquals(times.length, events.size());
        for (int i = 0; i < times.length; ++i) {
            Times expected = times[i];
            Times actual = events.get(i);
            assertEquals(expected.getStartTime(), actual.getStartTime());
            assertEquals(expected.getEndTime(), actual.getEndTime());
        }
    }

    /**
     * Verifies the service returns the appointment for each day it spans.
     *
     * @param act          the appointment
     * @param expectedDays the expected no. of days that the appointment should appear on
     */
    private void checkAppointment(Act act, int expectedDays) {
        Date start = DateRules.getDate(act.getActivityStartTime());
        Date end = DateRules.getDate(act.getActivityEndTime());
        Date date = start;
        int count = 0;
        while (date.compareTo(end) <= 0) {
            List<PropertySet> results = getScheduleService().getEvents(schedule, date);
            assertEquals(1, results.size());
            PropertySet set = results.get(0);
            checkAppointment(act, set);
            ++count;

            date = DateRules.getDate(date, 1, DateUnits.DAYS);
        }
        assertEquals(expectedDays, count);
    }

    /**
     * Verifies that an appointment matches the {@link PropertySet} representing it.
     *
     * @param act the appointment
     * @param set the set
     */
    private void checkAppointment(Act act, PropertySet set) {
        IMObjectBean bean = getBean(act);
        assertEquals(act.getObjectReference(), set.get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act.getActivityStartTime(), set.get(ScheduleEvent.ACT_START_TIME));
        assertEquals(act.getActivityEndTime(), set.get(ScheduleEvent.ACT_END_TIME));
        assertEquals(act.getStatus(), set.get(ScheduleEvent.ACT_STATUS));
        assertEquals(TestHelper.getLookupName(act, "status"), set.get(ScheduleEvent.ACT_STATUS_NAME));
        assertEquals(act.getReason(), set.get(ScheduleEvent.ACT_REASON));
        assertEquals(TestHelper.getLookupName(act, "reason"), set.get(ScheduleEvent.ACT_REASON_NAME));
        assertEquals(bean.getString("notes"), set.get(ScheduleEvent.NOTES));
        assertEquals(bean.getTargetRef("customer"), set.get(ScheduleEvent.CUSTOMER_REFERENCE));
        assertEquals(bean.getTarget("customer").getName(), set.get(ScheduleEvent.CUSTOMER_NAME));
        assertEquals(bean.getTargetRef("patient"), set.get(ScheduleEvent.PATIENT_REFERENCE));
        assertEquals(bean.getTarget("patient").getName(), set.get(ScheduleEvent.PATIENT_NAME));
        assertEquals(bean.getTargetRef("clinician"), set.get(ScheduleEvent.CLINICIAN_REFERENCE));
        assertEquals(bean.getTarget("clinician").getName(), set.get(ScheduleEvent.CLINICIAN_NAME));
        assertEquals(bean.getTargetRef("appointmentType"), set.get(ScheduleEvent.SCHEDULE_TYPE_REFERENCE));
        assertEquals(bean.getTargetRef("schedule"), set.get(ScheduleEvent.SCHEDULE_REFERENCE));
        assertEquals(bean.getTarget("schedule").getName(), set.get(ScheduleEvent.SCHEDULE_NAME));
        assertEquals(bean.getTarget("appointmentType").getName(), set.get(ScheduleEvent.SCHEDULE_TYPE_NAME));
        assertEquals(bean.getDate("confirmedTime"), set.get(ScheduleEvent.CONFIRMED_TIME));
        assertEquals(bean.getDate("arrivalTime"), set.get(ScheduleEvent.ARRIVAL_TIME));
    }

    /**
     * Creates and saves a new appointment.
     *
     * @param date the date to create the appointment on
     * @return a new appointment
     */
    private Act createAppointment(Date date) {
        Party patient = TestHelper.createPatient();
        return createAppointment(date, schedule, patient, true);
    }

    /**
     * Creates and saves a new appointment for a clinician.
     *
     * @param date      the date to create the appointment on
     * @param clinician the clinician
     * @param status    the appointment status
     * @return a new appointment
     */
    private Act createAppointment(Date date, User clinician, String status) {
        Party patient = TestHelper.createPatient();
        return newAppointment(date, schedule, patient)
                .clinician(clinician)
                .status(status)
                .build();
    }

    /**
     * Creates a new appointment.
     *
     * @param date     the date to create the appointment on
     * @param schedule the schedule
     * @param patient  the patient. May be {@code null}
     * @param save     if {@code true} save the appointment
     * @return a new appointment
     */
    private Act createAppointment(Date date, Entity schedule, Party patient, boolean save) {
        return newAppointment(date, schedule, patient)
                .clinician(userFactory.createClinician())
                .build(save);
    }

    /**
     * Creates an appointment builder.
     *
     * @param date     the date to schedule the appointment on
     * @param schedule the schedule
     * @param patient  the patient
     * @return a new appointment builder
     */
    private TestAppointmentBuilder newAppointment(Date date, Entity schedule, Party patient) {
        Date startTime = DateRules.getDate(date, 15, DateUnits.MINUTES);
        Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
        return newAppointment(startTime, endTime, schedule, patient);
    }

    /**
     * Creates a new appointment.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param schedule  the schedule
     * @param save      if {@code true} save the appointment
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Date endTime, Entity schedule, boolean save) {
        return createAppointment(startTime, endTime, schedule, null, save);
    }

    /**
     * Creates a new appointment.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param schedule  the schedule
     * @param patient   the patient. May be {@code null}
     * @param save      if {@code true} save the appointment
     * @return a new appointment
     */
    private Act createAppointment(Date startTime, Date endTime, Entity schedule, Party patient, boolean save) {
        return newAppointment(startTime, endTime, schedule, patient)
                .clinician(userFactory.createClinician())
                .build(save);
    }

    /**
     * Creates a new appointment builder.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @param patient   the patient
     * @return a new appointment builder
     */
    private TestAppointmentBuilder newAppointment(Date startTime, Date endTime, Entity schedule, Party patient) {
        Party customer = customerFactory.createCustomer();
        Entity appointmentType = schedulingFactory.createAppointmentType();
        return newAppointment(startTime, endTime, schedule, patient, customer, appointmentType);
    }

    /**
     * Creates a new appointment builder.
     *
     * @param startTime       the appointment start time
     * @param endTime         the appointment end time
     * @param schedule        the schedule
     * @param patient         the patient
     * @param customer        the customer
     * @param appointmentType the appointment type
     * @return a new appointment builder
     */
    private TestAppointmentBuilder newAppointment(Date startTime, Date endTime, Entity schedule, Party patient,
                                                  Party customer, Entity appointmentType) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .endTime(endTime)
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customer)
                .patient(patient)
                .notes("some notes");
    }

    /**
     * Verifies that an appointment matches the {@link PropertySet} representing it.
     *
     * @param act the appointment
     * @param set the set
     */
    private void checkCalendarBlock(Act act, PropertySet set) {
        IMObjectBean bean = getBean(act);
        assertEquals(act.getObjectReference(), set.get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act.getActivityStartTime(), set.get(ScheduleEvent.ACT_START_TIME));
        assertEquals(act.getActivityEndTime(), set.get(ScheduleEvent.ACT_END_TIME));
        assertEquals(act.getStatus(), set.get(ScheduleEvent.ACT_STATUS));
        assertEquals(bean.getString("notes"), set.get(ScheduleEvent.NOTES));
        assertEquals(bean.getTargetRef("schedule"), set.get(ScheduleEvent.SCHEDULE_REFERENCE));
        assertEquals(bean.getTarget("schedule").getName(), set.get(ScheduleEvent.SCHEDULE_NAME));
        assertEquals(bean.getTargetRef("type"), set.get(ScheduleEvent.SCHEDULE_TYPE_REFERENCE));
        assertEquals(bean.getTarget("type").getName(), set.get(ScheduleEvent.SCHEDULE_TYPE_NAME));
    }

    /**
     * Creates and saves a new appointment.
     *
     * @param date the date to create the appointment on
     * @return a new appointment
     */
    private Act createCalendarBlock(Date date) {
        Date startTime = DateRules.getDate(date, 15, DateUnits.MINUTES);
        Date endTime = DateRules.getDate(startTime, 15, DateUnits.MINUTES);
        Act block = ScheduleTestHelper.createCalendarBlock(startTime, endTime, schedule,
                                                           ScheduleTestHelper.createCalendarBlockType());
        IMObjectBean bean = getBean(block);
        bean.setValue("notes", "block notes");
        save(block);
        return block;
    }
}
