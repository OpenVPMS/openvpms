/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.settings;

import org.junit.After;
import org.junit.Before;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.business.service.singleton.SingletonServiceImpl;
import org.openvpms.component.model.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import static org.junit.Assert.assertNotNull;

/**
 * Base class for {@link Settings} tests.
 *
 * @author Tim Anderson
 */
public class AbstractSettingsTest extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The user rules.
     */
    @Autowired
    private UserRules userRules;

    /**
     * The settings service.
     */
    private SettingsImpl settings;

    /**
     * The singleton service.
     */
    private SingletonService singletonService;

    /**
     * The authentication context.
     */
    private AuthenticationContext context;

    /**
     * The cache.
     */
    private SettingsCache cache;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        singletonService = new SingletonServiceImpl(service, transactionManager);

        // remove any existing password policy
        Entity policy;
        while ((policy = singletonService.get(SettingsArchetypes.PASSWORD_POLICY, Entity.class)) != null) {
            service.remove(policy);
        }
        context = new AuthenticationContextImpl();
        context.setUser(TestHelper.createUser());
        cache = new SettingsCache(service, transactionManager, new SingletonServiceImpl(service, transactionManager),
                                  context, userRules);
        settings = new SettingsImpl(cache);
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        cache.destroy();
    }

    /**
     * Returns the settings.
     *
     * @return the settings
     */
    protected Settings getSettings() {
        return settings;
    }

    /**
     * Returns a singleton.
     *
     * @param archetype the archetype
     * @return the singleton instance
     */
    protected Entity getSingleton(String archetype) {
        Entity entity = singletonService.get(archetype, Entity.class, true);
        assertNotNull(entity);
        return entity;
    }

    /**
     * Returns the  singleton service.
     *
     * @return the singleton service
     */
    protected SingletonService getSingletonService() {
        return singletonService;
    }

    /**
     * Returns the authentication context.
     *
     * @return the authentication context
     */
    protected AuthenticationContext getContext() {
        return context;
    }
}
