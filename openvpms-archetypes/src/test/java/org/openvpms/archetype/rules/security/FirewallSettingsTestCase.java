/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.security;

import org.junit.Test;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link FirewallSettings} class.
 *
 * @author Tim Anderson
 */
public class FirewallSettingsTestCase extends ArchetypeServiceTest {

    /**
     * Tests default settings.
     */
    @Test
    public void testDefaults() {
        FirewallSettings settings = createSettings();
        assertEquals(FirewallSettings.AccessType.UNRESTRICTED, settings.getAccessType());
        assertTrue(settings.getAllowedAddresses().isEmpty());
    }

    /**
     * Tests the {@link FirewallSettings#setAllowedAddresses(List)} and
     * {@link FirewallSettings#getAllowedAddresses()} methods.
     */
    @Test
    public void testAllowedAddresses() {
        FirewallSettings settings1 = createSettings();
        List<FirewallEntry> allowed = new ArrayList<>();
        for (int i = 0; i < 300; ++i) {
            allowed.add(new FirewallEntry("192.168.1.1", i % 2 == 0, "An entry"));
        }

        IMObjectBean bean = getBean(settings1.getSettings());
        assertNull(bean.getString("allowed0"));
        assertNull(bean.getString("allowed1"));

        settings1.setAllowedAddresses(allowed);

        // verify the entries and that they were encoded over two nodes
        checkEntries(allowed, settings1.getAllowedAddresses());
        assertNotNull(bean.getString("allowed0"));
        assertNotNull(bean.getString("allowed1"));

        // make the settings persistent verify they match those expected when read back
        save(settings1.getSettings());
        Entity reloaded = get(settings1.getSettings());
        FirewallSettings settings2 = new FirewallSettings(reloaded, getArchetypeService());
        checkEntries(allowed, settings2.getAllowedAddresses());
    }

    /**
     * Verifies that all nodes are updated when using {@link FirewallSettings#setAllowedAddresses(List)}.
     */
    @Test
    public void testClearAllowed() {
        FirewallSettings settings1 = createSettings();
        IMObjectBean bean = getBean(settings1.getSettings());
        bean.setValue("allowed0", "127.0.0.1 1");
        bean.setValue("allowed1", "192.168.1.1 0");

        settings1.setAllowedAddresses(new ArrayList<>());
        save(settings1.getSettings());
        Entity reloaded = get(settings1.getSettings());
        FirewallSettings settings2 = new FirewallSettings(reloaded, getArchetypeService());
        checkEntries(Collections.emptyList(), settings2.getAllowedAddresses());
    }

    /**
     * Checks entries.
     *
     * @param expectedList the list of expected entries
     * @param actualList   the actual list of entries
     */
    private void checkEntries(List<FirewallEntry> expectedList, List<FirewallEntry> actualList) {
        assertNotSame(expectedList, actualList);
        assertEquals(expectedList.size(), actualList.size());
        for (int i = 0; i < expectedList.size(); ++i) {
            FirewallEntry expected = expectedList.get(i);
            FirewallEntry actual = actualList.get(i);
            assertEquals(expected.getAddress(), actual.getAddress());
            assertEquals(expected.isActive(), actual.isActive());
            assertEquals(expected.getDescription(), actual.getDescription());
        }
    }

    /**
     * Creates a new {@link FirewallSettings}.
     *
     * @return the settings
     */
    private FirewallSettings createSettings() {
        return new FirewallSettings(create(SettingsArchetypes.FIREWALL_SETTINGS, Entity.class),
                                    getArchetypeService());
    }
}