/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.party;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountQueryFactory;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.statement.StatementService;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link CustomerMerger} class.
 *
 * @author Tim Anderson
 */
public class CustomerMergerTestCase extends AbstractPartyMergerTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * Customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * Patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules customerAccountRules;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The transaction template.
     */
    private TransactionTemplate template;

    /**
     * The statement service.
     */
    private StatementService statementService;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        template = new TransactionTemplate(transactionManager);

        Party practice = create(PracticeArchetypes.PRACTICE, Party.class);
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getPractice()).thenReturn(practice);
        statementService = new StatementService((IArchetypeRuleService) getArchetypeService(),
                                                customerAccountRules, laboratoryRules, practiceService);
    }

    /**
     * Merges two customers and verifies the merged customer contains the
     * contacts of both.
     */
    @Test
    public void testMergeContacts() {
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.createCustomer();

        int fromContactsSize = from.getContacts().size();
        int toContactsSize = to.getContacts().size();

        Party merged = checkMerge(from, to);

        // verify contacts copied across
        assertEquals(fromContactsSize + toContactsSize, merged.getContacts().size());
    }

    /**
     * Tests a merge where the merge-from customer has an account type,
     * and the merge-to customer doesn't.
     */
    @Test
    public void testMergeAccountType() {
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.createCustomer();
        Lookup accountType = customerFactory.newAccountType()
                .paymentTerms(30, DateUnits.DAYS)
                .build();
        from.addClassification(accountType);

        Party merged = checkMerge(from, to);
        assertEquals(accountType, customerRules.getAccountTypeLookup(merged));
    }

    /**
     * Tests a merge where both customers have different account types.
     * The merge-to customer's account type should take precedence.
     */
    @Test
    public void testMergeAccountTypes() {
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.createCustomer();
        Lookup accountType1 = customerFactory.newAccountType().paymentTerms(30, DateUnits.DAYS).build();
        Lookup accountType2 = customerFactory.newAccountType().paymentTerms(15, DateUnits.DAYS).build();

        from.addClassification(accountType1);
        to.addClassification(accountType2);

        Lookup fromAccountType = customerRules.getAccountTypeLookup(from);
        Lookup toAccountType = customerRules.getAccountTypeLookup(to);

        assertEquals(accountType1, fromAccountType);
        assertEquals(accountType2, toAccountType);

        Party merged = checkMerge(from, to);

        assertEquals(accountType2, customerRules.getAccountTypeLookup(merged));
    }

    /**
     * Tests that entity relationships are copied.
     */
    @Test
    public void testMergeEntityRelationships() {
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(from);
        Party patient2 = patientFactory.createPatient(to);

        Party merged = checkMerge(from, to);

        patient1 = get(patient1);
        patient2 = get(patient2);

        assertTrue(patientRules.isOwner(merged, patient1));
        assertTrue(patientRules.isOwner(merged, patient2));
    }

    /**
     * Tests that entity identities are copied.
     */
    @Test
    public void testMergeEntityIdentities() {
        Party from = customerFactory.newCustomer()
                .addCodeIdentity("ABC1234")
                .build();
        Party to = customerFactory.newCustomer()
                .addCodeIdentity("XYZ1234")
                .build();

        EntityIdentity id1 = from.getIdentities().stream().findFirst().orElse(null);
        assertNotNull(id1);

        EntityIdentity id2 = to.getIdentities().stream().findFirst().orElse(null);
        assertNotNull(id2);

        Party merged = checkMerge(from, to);

        assertEquals(2, merged.getIdentities().size());
        assertFalse(merged.getIdentities().contains(id1)); // the identity is copied rather than moved
        assertTrue(merged.getIdentities().stream().anyMatch(identity -> identity.getIdentity().equals("ABC1234")));
        assertTrue(merged.getIdentities().contains(id2));  // this doesn't change as it was present on the 'to' party
    }

    /**
     * Verifies that participations are moved to the merged customer.
     */
    @Test
    public void testMergeParticipations() {
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        Product product = productFactory.createMedication();

        assertEquals(0, countParticipations(from));
        assertEquals(0, countParticipations(to));

        for (int i = 0; i < 10; ++i) {
            List<FinancialAct> invoice = FinancialTestHelper.createChargesInvoice(MathRules.ONE_HUNDRED, from, patient,
                                                                                  product, ActStatus.POSTED);
            save(invoice);
        }
        int fromRefs = countParticipations(from);
        assertTrue(fromRefs >= 10);

        checkMerge(from, to);

        // verify the participations no longer reference the from customer
        assertEquals(0, countParticipations(from));

        // verify all participations moved to the 'to' customer
        int toRefs = countParticipations(to);
        assertEquals(toRefs, fromRefs);  //
    }

    /**
     * Verifies that only <em>party.customerperson</em> instances can be merged.
     */
    @Test
    public void testMergeInvalidParty() {
        Party from = customerFactory.createCustomer();
        Party to = supplierFactory.createSupplier();
        try {
            checkMerge(from, to);
            fail("Expected merge to invalid party to fail");
        } catch (MergeException expected) {
            assertEquals(MergeException.ErrorCode.InvalidType, expected.getErrorCode());
        }
    }

    /**
     * Verifies that a customer cannot be merged with itself.
     */
    @Test
    public void testMergeToSameCustomer() {
        Party from = customerFactory.createCustomer();
        try {
            checkMerge(from, from);
            fail("Expected merge to same customer to fail");
        } catch (MergeException expected) {
            assertEquals(MergeException.ErrorCode.CannotMergeToSameObject, expected.getErrorCode());
        }
    }

    /**
     * Verifies that the 'to' customer includes the 'from' customer's balance
     * after the merge, and that any opening and closing balance acts on or
     * after the first transaction of the from customer are removed.
     */
    @Test
    public void testMergeAccounts() {
        final Money eighty = new Money(80);
        final Money forty = new Money(40);
        final Money fifty = new Money(50);
        final Money ninety = new Money(90);
        Party from = customerFactory.createCustomer();
        Party fromPatient = patientFactory.createPatient();
        Party to = customerFactory.createCustomer();
        Party toPatient = patientFactory.createPatient();
        Product product = productFactory.createMedication();

        // add some transaction history for the 'from' customer
        Date firstStartTime = getDatetime("2007-01-02 10:0:0");
        addInvoice(firstStartTime, eighty, from, fromPatient, product);
        addPayment(getDatetime("2007-01-02 11:0:0"), forty, from);

        runEOP(from, getDate("2007-02-01"));

        // ... and the 'to' customer
        addInvoice(getDatetime("2007-01-01 10:0:0"), fifty, to, toPatient, product);
        runEOP(to, getDate("2007-01-01"));
        runEOP(to, getDate("2007-02-01"));

        // verify balances prior to merge
        assertEquals(0, forty.compareTo(customerAccountRules.getBalance(from)));
        assertEquals(0, fifty.compareTo(customerAccountRules.getBalance(to)));

        to = checkMerge(from, to);

        // verify balances after merge
        assertEquals(0, BigDecimal.ZERO.compareTo(customerAccountRules.getBalance(from)));
        assertEquals(0, ninety.compareTo(customerAccountRules.getBalance(to)));

        // now verify that the only opening and closing balance acts for the
        // to customer are prior to the first act of the from customer
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(
                to, new String[]{CustomerAccountArchetypes.OPENING_BALANCE,
                                 CustomerAccountArchetypes.CLOSING_BALANCE});
        IMObjectQueryIterator<Act> iter = new IMObjectQueryIterator<>(query);
        int count = 0;
        while (iter.hasNext()) {
            Act act = iter.next();
            long startTime = act.getActivityStartTime().getTime();
            assertTrue(startTime < firstStartTime.getTime());
            ++count;
        }
        assertEquals(2, count); // expect a closing and opening balance

        // verify there are no acts associated with the removed 'from' customer
        assertEquals(0, countParticipations(from));
    }

    /**
     * Verifies that the location node is merged as follows:
     * <p/>
     * Note that if both customer A and customer B had a Practice Location set, customer A's location should remain unchanged.
     * <p>
     * When two customers A and B are merged, and the customer A has no Practice Location but customer B does,
     * customer A should be assigned customer B's.
     */
    @Test
    public void testMergeLocationInFrom() {
        Party location = practiceFactory.createLocation();
        Party from = customerFactory.newCustomer()
                .practice(location)
                .build();
        Party to = customerFactory.createCustomer();
        Party merged = checkMerge(from, to);
        IMObjectBean bean = getBean(merged);
        assertEquals(location, bean.getTarget("practice"));
    }

    /**
     * Verifies that when the 'to' customer has a location, and the 'from' customer has none, the 'to' location
     * is retained.
     */
    @Test
    public void testMergeLocationInTo() {
        Party location = practiceFactory.createLocation();
        Party from = customerFactory.createCustomer();
        Party to = customerFactory.newCustomer()
                .practice(location)
                .build();
        Party merged = checkMerge(from, to);
        IMObjectBean bean = getBean(merged);
        assertEquals(location, bean.getTarget("practice"));
    }

    /**
     * Verifies that when both the 'from' and the 'to' customer have a location, the 'to' location is retained.
     */
    @Test
    public void testMergeLocation() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party from = customerFactory.newCustomer().practice(location1).build();
        Party to = customerFactory.newCustomer().practice(location2).build();
        Party merged = checkMerge(from, to);
        IMObjectBean bean = getBean(merged);
        assertEquals(location2, bean.getTarget("practice"));
    }

    /**
     * Verifies that when both the 'from' and the 'to' customer have the location, the 'to' location is retained.
     */
    @Test
    public void testMergeSameLocation() {
        Party location1 = practiceFactory.createLocation();
        Party from = customerFactory.newCustomer().practice(location1).build();
        Party to = customerFactory.newCustomer().practice(location1).build();
        Party merged = checkMerge(from, to);
        IMObjectBean bean = getBean(merged);
        assertEquals(location1, bean.getTarget("practice"));
    }

    /**
     * Runs end of period for a customer.
     *
     * @param customer      the customer
     * @param statementDate the statement date
     */
    private void runEOP(Party customer, Date statementDate) {
        statementService.endPeriod(customer, statementDate, true);
    }

    /**
     * Merges two customers in a transaction, and verifies the 'from' customer
     * has been deleted.
     *
     * @param from the customer to merge from
     * @param to   the customer to merge to
     * @return the merged customer
     */
    private Party checkMerge(Party from, Party to) {
        template.execute(transactionStatus -> {
            customerRules.mergeCustomers(from, to);
            return null;
        });

        // verify the from customer has been deleted
        assertNull(get(from));

        Party merged = get(to);
        assertNotNull(merged);
        return merged;
    }

    /**
     * Saves an invoice for a customer.
     *
     * @param startTime the invoice start time
     * @param amount    the invoice amount
     * @param customer  the customer
     * @param patient   the patient
     * @param product   the product
     */
    private void addInvoice(Date startTime, BigDecimal amount, Party customer, Party patient, Product product) {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(amount, customer, patient, product,
                                                                           ActStatus.POSTED);
        FinancialAct act = acts.get(0);
        act.setActivityStartTime(startTime);
        save(acts);
    }

    /**
     * Saves a payment for a customer.
     *
     * @param startTime the payment start time
     * @param amount    the payment amount
     * @param customer  the customer
     */
    private void addPayment(Date startTime, Money amount, Party customer) {
        Act act = FinancialTestHelper.createPaymentCash(amount, customer, FinancialTestHelper.createTill());
        act.setActivityStartTime(startTime);
        save(act);
    }
}
