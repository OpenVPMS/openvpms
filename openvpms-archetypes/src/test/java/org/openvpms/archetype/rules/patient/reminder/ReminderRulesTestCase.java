/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient.reminder;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.object.IMObjectGraphVerifier;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderBuilder;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.product.TestMedicationProductBuilder;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.COMPLETED;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.patient.reminder.ReminderRule.SendTo.ANY;
import static org.openvpms.archetype.rules.patient.reminder.ReminderRule.SendTo.FIRST;
import static org.openvpms.archetype.rules.patient.reminder.ReminderStatus.CANCELLED;
import static org.openvpms.archetype.test.TestHelper.assertIncluded;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link ReminderRules} class.
 *
 * @author Tim Anderson
 */
public class ReminderRulesTestCase extends ArchetypeServiceTest {

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The reminder rules.
     */
    private ReminderRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new ReminderRules(getArchetypeService(), patientRules);
    }

    /**
     * Tests the {@link ReminderRules#markMatchingRemindersCompleted(Act)}
     * method.
     */
    @Test
    public void testMarkMatchingRemindersCompleted() {
        Lookup group1 = reminderFactory.createReminderGroup();
        Lookup group2 = reminderFactory.createReminderGroup();

        Party patient1 = patientFactory.createPatient();
        Party patient2 = patientFactory.createPatient();

        // create a reminder for patient1, with an entity.reminderType with
        // no lookup.reminderGroup
        Act reminder0 = createReminder(patient1);
        checkReminder(reminder0, IN_PROGRESS);
        rules.markMatchingRemindersCompleted(reminder0);

        // create a reminder for patient1, with an entity.reminderType with
        // group1 lookup.reminderGroup. Verify it has not changed reminder0
        Act reminder1 = createReminder(patient1, group1);
        rules.markMatchingRemindersCompleted(reminder1);
        checkReminder(reminder1, IN_PROGRESS);
        checkReminder(reminder0, IN_PROGRESS);

        // create a reminder for patient2, with an entity.reminderType with
        // group2 lookup.reminderGroup. Verify it has not changed reminder1
        Act reminder2 = createReminder(patient2, group2);
        rules.markMatchingRemindersCompleted(reminder2);
        checkReminder(reminder2, IN_PROGRESS);
        checkReminder(reminder1, IN_PROGRESS);

        // create a reminder for patient1, with an entity.reminderType with
        // group1 lookup.reminderGroup. Verify it marks reminder1 COMPLETED.
        Act reminder3 = createReminder(patient1, group1);
        rules.markMatchingRemindersCompleted(reminder3);
        checkReminder(reminder3, IN_PROGRESS);
        checkReminder(reminder1, COMPLETED);

        // create a reminder for patient2, with an entity.reminderType with
        // both group1 and group2 lookup.reminderGroup. Verify it marks
        // reminder2 COMPLETED.
        Act reminder4 = createReminder(patient2, group1, group2);
        rules.markMatchingRemindersCompleted(reminder4);
        checkReminder(reminder4, IN_PROGRESS);
        checkReminder(reminder2, COMPLETED);

        // create a reminder type with no group, and create 2 reminders using it.
        Entity reminderType = reminderFactory.createReminderType();
        Act reminder5 = createReminder(patient1, reminderType);
        Act reminder6 = createReminder(patient2, reminderType);
        rules.markMatchingRemindersCompleted(reminder5);
        rules.markMatchingRemindersCompleted(reminder6);
        checkReminder(reminder5, IN_PROGRESS);
        checkReminder(reminder6, IN_PROGRESS);

        // now create a reminder for patient1. Verify it marks reminder5 COMPLETED
        Act reminder7 = createReminder(patient1, reminderType);
        rules.markMatchingRemindersCompleted(reminder7);
        checkReminder(reminder5, COMPLETED);
        checkReminder(reminder6, IN_PROGRESS);
        checkReminder(reminder7, IN_PROGRESS);
    }

    /**
     * Tests the {@link ReminderRules#markMatchingRemindersCompleted(List)} method.
     */
    @Test
    public void testMarkMatchingRemindersCompletedForList() {
        Entity reminderType = reminderFactory.createReminderType();

        Party patient1 = patientFactory.createPatient();
        Party patient2 = patientFactory.createPatient();

        // create reminders for patient1 and patient2
        Act reminder0 = createReminder(patient1, reminderType);
        Act reminder1 = createReminder(patient2, reminderType);
        checkReminder(reminder0, IN_PROGRESS);
        checkReminder(reminder1, IN_PROGRESS);

        Act reminder2 = createReminder(patient1, reminderType);
        Act reminder3 = createReminder(patient2, reminderType);
        Act reminder3dup = createReminder(patient2, reminderType); // duplicates reminder3
        List<Act> reminders = Arrays.asList(reminder2, reminder3, reminder3dup);
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(status -> {
            save(reminders);
            rules.markMatchingRemindersCompleted(reminders);
            return null;
        });

        checkReminder(reminder0, COMPLETED);
        checkReminder(reminder1, COMPLETED);
        checkReminder(reminder2, IN_PROGRESS);
        checkReminder(reminder3, IN_PROGRESS);
        checkReminder(reminder3dup, COMPLETED); // as it duplicates reminder3
    }

    /**
     * Verifies that a reminder with a due {@code date <= now} passed to
     * {@link ReminderRules#markMatchingRemindersCompleted(Act)} completes matching reminders, including itself.
     */
    @Test
    public void testStopperReminder() {
        Party patient = patientFactory.createPatient();

        Entity reminderType = reminderFactory.createReminderType();
        Act reminder1 = createReminder(patient, reminderType);
        checkReminder(reminder1, IN_PROGRESS);

        // verify creating a new reminder with the same reminder type completes reminder1
        Act reminder2 = createReminder(patient, reminderType);
        rules.markMatchingRemindersCompleted(reminder2);
        checkReminder(reminder1, COMPLETED);
        checkReminder(reminder2, IN_PROGRESS);

        // now create a reminder with a due date <= now, and verify it completes all reminders, including itself
        Act reminder3 = newReminder(patient, reminderType)
                .dueDate(new Date())
                .build();
        rules.markMatchingRemindersCompleted(reminder3);
        checkReminder(reminder1, COMPLETED);
        checkReminder(reminder2, COMPLETED);
        checkReminder(reminder3, COMPLETED);
    }

    /**
     * Tests the {@link ReminderRules#calculateReminderDueDate(Date, Entity)} method.
     */
    @Test
    public void testCalculateReminderDueDate() {
        checkCalculateReminderDueDate(1, DateUnits.DAYS, "2007-01-01", "2007-01-02");
        checkCalculateReminderDueDate(2, DateUnits.WEEKS, "2007-01-01", "2007-01-15");
        checkCalculateReminderDueDate(2, DateUnits.MONTHS, "2007-01-01", "2007-03-01");
        checkCalculateReminderDueDate(5, DateUnits.YEARS, "2007-01-01", "2012-01-01");
    }

    /**
     * Tests the {@link ReminderRules#calculateProductReminderDueDate} method.
     */
    @Test
    public void testCalculateProductReminderDueDate() {
        checkCalculateProductReminderDueDate(1, DateUnits.DAYS, "2007-01-01", "2007-01-02");
        checkCalculateProductReminderDueDate(2, DateUnits.WEEKS, "2007-01-01", "2007-01-15");
        checkCalculateProductReminderDueDate(2, DateUnits.MONTHS, "2007-01-01", "2007-03-01");
        checkCalculateProductReminderDueDate(5, DateUnits.YEARS, "2007-01-01", "2012-01-01");
    }

    /**
     * Tests the {@link ReminderRules#getNextReminderDate(Date, Entity, int)} method.
     */
    @Test
    public void testGetNextReminderDate() {
        Entity reminderType = reminderFactory.newReminderType().defaultInterval(1, DateUnits.YEARS)
                .newCount().count(0).interval(-1, DateUnits.MONTHS).add()
                .newCount().count(1).interval(-2, DateUnits.WEEKS).add()
                .newCount().count(2).interval(0, DateUnits.DAYS).add()
                .newCount().count(3).interval(1, DateUnits.MONTHS).add()
                .build();

        Date today = DateRules.getToday();
        Date due = DateRules.getDate(today, 1, DateUnits.YEARS);
        Date next0 = DateRules.getDate(due, -1, DateUnits.MONTHS);
        Date next1 = DateRules.getDate(due, -2, DateUnits.WEEKS);
        Date next2 = DateRules.getDate(due, 0, DateUnits.DAYS);
        Date next3 = DateRules.getDate(due, 1, DateUnits.MONTHS);

        assertEquals(next0, rules.getNextReminderDate(due, reminderType, 0));
        assertEquals(next1, rules.getNextReminderDate(due, reminderType, 1));
        assertEquals(next2, rules.getNextReminderDate(due, reminderType, 2));
        assertEquals(next3, rules.getNextReminderDate(due, reminderType, 3));
        assertNull(rules.getNextReminderDate(due, reminderType, 4));
    }

    /**
     * Tests the {@link ReminderRules#shouldCancel(Act, Date)} method.
     */
    @Test
    public void testShouldCancel() {
        Lookup group = reminderFactory.createReminderGroup();
        Party patient = patientFactory.createPatient();
        Entity reminderType = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .cancelInterval(0, DateUnits.MONTHS)
                .addGroups(group.getCode())
                .build();
        Act reminder = newReminder(patient, reminderType)
                .date("2007-01-01")
                .build();
        Date due = getDate("2007-02-01");
        assertEquals(due, reminder.getActivityStartTime());
        assertEquals(due, reminder.getActivityEndTime());

        checkShouldCancel(reminder, "2007-01-01", false);
        checkShouldCancel(reminder, "2007-01-31", false);
        checkShouldCancel(reminder, "2007-02-01", true);

        // Now add a cancel interval to the reminderType, due 2 weeks after the current due date.
        reminderFactory.updateReminderType(reminderType)
                .cancelInterval(2, DateUnits.WEEKS)
                .build();

        checkShouldCancel(reminder, "2007-02-01", false);
        checkShouldCancel(reminder, "2007-02-14", false);
        checkShouldCancel(reminder, "2007-02-15", true);

        // Now set patient to deceased
        patientFactory.updatePatient(patient)
                .deceased(true)
                .build();
        checkShouldCancel(reminder, "2007-02-01", true);
    }

    /**
     * Tests the {@link ReminderRules#updateReminder(Act, Act)} method.
     */
    @Test
    public void testUpdateReminder() {
        Party patient = patientFactory.createPatient();
        Entity reminderType = reminderFactory.newReminderType().defaultInterval(3, DateUnits.MONTHS)
                .newCount().count(0).interval(0, DateUnits.WEEKS).newRule().email().add().add()
                .newCount().count(1).interval(1, DateUnits.MONTHS).newRule().email().add().add()
                .build();

        Act reminder = createReminder(patient, reminderType, "2016-01-01", IN_PROGRESS);
        Date due = getDate("2016-04-01");
        assertEquals(due, reminder.getActivityStartTime());
        assertEquals(due, reminder.getActivityEndTime());
        TestReminderBuilder builder = reminderFactory.updateReminder(reminder);
        builder.newEmailReminder().dueDate(due).sendDate(due).add()
                .newPrintReminder().dueDate(due).sendDate(due).add()
                .build();
        Act item1 = builder.getItems().get(0);
        Act item2 = builder.getItems().get(1);
        assertFalse(rules.updateReminder(reminder, item1));
        item2.setStatus(ReminderItemStatus.COMPLETED);
        save(item2);
        assertTrue(rules.updateReminder(reminder, item1));
        assertEquals(getDate("2016-05-01"), reminder.getActivityStartTime());
    }

    /**
     * Tests the {@link ReminderRules#getReminderTypes(Product, String)} method.
     */
    @Test
    public void testGetReminderTypes() {
        Entity reminderType1 = reminderFactory.createReminderType();
        Entity reminderType2 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.YEARS)
                .addSpecies("CANINE")
                .build();
        Entity reminderType3 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.YEARS)
                .addSpecies("FELINE")
                .build();
        Entity reminderType4 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.YEARS)
                .addSpecies("CANINE", "FELINE")
                .build();
        Product product = productFactory.newMedication()
                .addProductReminder(reminderType1, 1, DateUnits.YEARS)
                .addProductReminder(reminderType2, 1, DateUnits.YEARS)
                .addProductReminder(reminderType3, 1, DateUnits.YEARS)
                .addProductReminder(reminderType4, 1, DateUnits.YEARS)
                .build();

        Map<Entity, Relationship> matches1 = rules.getReminderTypes(product, null);
        assertEquals(4, matches1.size());
        assertIncluded(matches1.keySet(), reminderType1, reminderType2, reminderType3, reminderType4);

        Map<Entity, Relationship> matches2 = rules.getReminderTypes(product, "CANINE");
        assertEquals(3, matches2.size());
        assertIncluded(matches2.keySet(), reminderType1, reminderType2, reminderType4);

        Map<Entity, Relationship> matches3 = rules.getReminderTypes(product, "FELINE");
        assertEquals(3, matches3.size());
        assertIncluded(matches3.keySet(), reminderType1, reminderType3, reminderType4);

        Map<Entity, Relationship> matches4 = rules.getReminderTypes(product, "BOVINE");
        assertEquals(1, matches4.size());
        assertIncluded(matches4.keySet(), reminderType1);
    }

    /**
     * Tests the {@link ReminderRules#getDocumentFormReminder} method, when the <em>act.patientDocumentForm</em> is
     * linked to an invoice item.
     */
    @Test
    public void testGetDocumentFormReminderForInvoiceItem() {
        Party patient = patientFactory.createPatient();
        DocumentAct form = patientFactory.createForm(patient);

        // verify a form not associated with any invoice item nor product returns null
        assertNull(rules.getDocumentFormReminder(form));

        // create an invoice item and associate the form with it
        FinancialAct item = accountFactory.newInvoiceItem()
                .patient(patient)
                .medicationProduct()
                .quantity(1)
                .unitPrice(1)
                .addDocument(form)
                .build();

        // should return null as neither the invoice nor product associated with the form have reminders
        assertNull(rules.getDocumentFormReminder(form));

        // associate a single reminder with the invoice item, and verify it is returned by getDocumentFormReminder()
        Entity reminderType1 = reminderFactory.createReminderType();
        Act reminder1 = newReminder(patient, reminderType1)
                .dueDate("2012-01-12")
                .build();
        accountFactory.updateInvoiceItem(item)
                .addReminders(reminder1)
                .build();
        assertEquals(reminder1, rules.getDocumentFormReminder(form));

        // associate another reminder with the invoice item, with a closer due date. This should be returned
        Entity reminderType2 = reminderFactory.createReminderType();
        Act reminder2 = newReminder(patient, reminderType2)
                .dueDate("2012-01-11")
                .build();
        accountFactory.updateInvoiceItem(item)
                .addReminders(reminder2)
                .build();
        assertEquals(reminder2, rules.getDocumentFormReminder(form));

        // associate another reminder with the invoice item, with the same due date. The reminder with the lower id
        // should be returned
        Entity reminderType3 = reminderFactory.createReminderType();
        Act reminder3 = newReminder(patient, reminderType3)
                .dueDate("2012-01-11")
                .build();
        accountFactory.updateInvoiceItem(item)
                .addReminders(reminder3)
                .build();
        assertEquals(reminder2, rules.getDocumentFormReminder(form));
    }

    /**
     * Tests the {@link ReminderRules#getDocumentFormReminder} method, when the <em>act.patientDocumentForm</em> is
     * linked to product with reminder types.
     */
    @Test
    public void testGetDocumentFormReminderForProduct() {
        Party patient = patientFactory.createPatient();
        Product product = productFactory.createMedication();
        DocumentAct form = patientFactory.newForm()
                .patient(patient)
                .template()
                .product(product)
                .build();

        // verify a form not associated with a product with reminders returns null
        assertNull(rules.getDocumentFormReminder(form));

        Entity reminderType1 = reminderFactory.createReminderType();
        TestMedicationProductBuilder productBuilder = productFactory.updateMedication(product);
        productBuilder.addProductReminder(reminderType1, 2, DateUnits.YEARS)
                .build();
        EntityLink productReminder1 = productBuilder.getProductReminders().get(0);

        Act reminder1 = rules.getDocumentFormReminder(form);
        assertNotNull(reminder1);
        assertTrue(reminder1.isNew()); // reminders from products should not be persistent
        Date dueDate1 = rules.calculateProductReminderDueDate(form.getActivityStartTime(), productReminder1);
        checkReminder(reminder1, reminderType1, patient, product, dueDate1);

        Entity reminderType2 = reminderFactory.createReminderType();
        productBuilder.addProductReminder(reminderType2, 1, DateUnits.YEARS)
                .build();
        Relationship productReminder2 = productBuilder.getProductReminders().get(0);

        Date dueDate2 = rules.calculateProductReminderDueDate(form.getActivityStartTime(), productReminder2);
        assertTrue(dueDate2.compareTo(dueDate1) < 0);

        Act reminder2 = rules.getDocumentFormReminder(form);
        assertNotNull(reminder2);
        assertTrue(reminder2.isNew()); // reminders from products should not be persistent
        checkReminder(reminder2, reminderType2, patient, product, dueDate2);
    }

    /**
     * Tests the {@link ReminderRules#getDocumentFormReminder} method to verify that the reminder associated with an
     * invoice item is returned in preference to one created from a product's reminder types.
     */
    @Test
    public void testGetDocumentFormReminderForInvoiceAndProduct() {
        Entity reminderType1 = reminderFactory.createReminderType();
        TestMedicationProductBuilder productBuilder = productFactory.newMedication();
        Product product = productBuilder
                .addProductReminder(reminderType1, 1, DateUnits.YEARS)
                .build();

        Party patient = patientFactory.createPatient();
        DocumentAct form = patientFactory.newForm()
                .patient(patient)
                .template()
                .product(product)
                .build();
        EntityLink relationship = productBuilder.getProductReminders().get(0);

        Date dueDate1 = rules.calculateProductReminderDueDate(form.getActivityStartTime(), relationship);
        Act reminder1 = rules.getDocumentFormReminder(form);
        assertNotNull(reminder1);
        assertTrue(reminder1.isNew()); // reminders from products should not be persistent
        checkReminder(reminder1, reminderType1, patient, product, dueDate1);

        // create an invoice item and associate the form with it
        FinancialAct item = accountFactory.newInvoiceItem()
                .patient(patient)
                .product(product)
                .unitPrice(1)
                .addDocument(form)
                .build();

        // associate a single reminder with the invoice item, and verify it is returned by getDocumentFormReminder()
        Entity reminderType2 = reminderFactory.createReminderType();
        Act reminder2 = newReminder(patient, reminderType2)
                .dueDate("2012-01-12")
                .build();
        accountFactory.updateInvoiceItem(item)
                .addReminders(reminder2)
                .build();
        assertEquals(reminder2, rules.getDocumentFormReminder(form));
    }

    /**
     * Tests the {@link ReminderRules#getDueState(Act, Date)} method.
     */
    @Test
    public void testGetDueState() {
        Lookup group = reminderFactory.createReminderGroup();
        Party patient = patientFactory.createPatient();
        Entity reminderType = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .addGroups(group.getCode())
                .build();
        Date start = getDate("2012-01-01");
        Date due = rules.calculateReminderDueDate(start, reminderType);
        Act reminder = newReminder(patient, reminderType)
                .date(start)
                .build();
        assertEquals(due, reminder.getActivityStartTime());
        assertEquals(due, reminder.getActivityEndTime());

        assertEquals(ReminderRules.DueState.NOT_DUE, rules.getDueState(reminder, getDate("2012-01-01")));
        assertEquals(ReminderRules.DueState.NOT_DUE, rules.getDueState(reminder, getDate("2012-01-31")));
        assertEquals(ReminderRules.DueState.DUE, rules.getDueState(reminder, getDate("2012-02-01")));
        assertEquals(ReminderRules.DueState.OVERDUE, rules.getDueState(reminder, getDate("2012-02-02")));

        // change the sensitivity from the default (0 DAYS)
        reminderFactory.updateReminderType(reminderType)
                .sensitivityInterval(5, DateUnits.DAYS)
                .build();

        assertEquals(ReminderRules.DueState.NOT_DUE, rules.getDueState(reminder, getDate("2012-01-01")));
        assertEquals(ReminderRules.DueState.DUE, rules.getDueState(reminder, getDate("2012-01-27")));
        assertEquals(ReminderRules.DueState.DUE, rules.getDueState(reminder, getDate("2012-02-01")));
        assertEquals(ReminderRules.DueState.DUE, rules.getDueState(reminder, getDate("2012-02-06")));
        assertEquals(ReminderRules.DueState.OVERDUE, rules.getDueState(reminder, getDate("2012-02-07")));
    }

    /**
     * Verifies that if a entityLink.productReminder is missing the periodUom, it is treated as YEARS.
     */
    @Test
    public void testCalculateProductReminderDueDateForMissingPeriodUOM() {
        Entity reminderType = reminderFactory.createReminderType();
        TestMedicationProductBuilder productBuilder = productFactory.newMedication();
        productBuilder
                .addProductReminder(reminderType, 1, DateUnits.MONTHS)
                .build();
        EntityLink productReminder = productBuilder.getProductReminders().get(0);

        Date start = getDate("2015-03-25");
        Date due1 = rules.calculateProductReminderDueDate(start, productReminder);
        assertEquals(getDate("2015-04-25"), due1);

        getBean(productReminder).setValue("periodUom", null);

        Date due2 = rules.calculateProductReminderDueDate(start, productReminder);
        assertEquals(getDate("2016-03-25"), due2);
    }

    /**
     * Tests the {@link ReminderRules#getReminders(Party, Date, Date)} method.
     */
    @Test
    public void testGetReminders() {
        Party patient = patientFactory.createPatient();
        Entity reminderType = reminderFactory.createReminderType();
        Act reminder1 = createReminder(patient, reminderType, "2016-04-13 11:59:59", IN_PROGRESS);
        Act reminder2 = createReminder(patient, reminderType, "2016-04-14 10:10:10", IN_PROGRESS);
        Act reminder3 = createReminder(patient, reminderType, "2016-04-14 11:10:10", COMPLETED);
        Act reminder4 = createReminder(patient, reminderType, "2016-04-15 10:10:10", CANCELLED);
        Act reminder5 = createReminder(patient, reminderType, "2016-04-15 11:00:00", IN_PROGRESS);

        List<Act> acts = getActs(rules.getReminders(patient, getDatetime("2016-04-14 10:00:00"),
                                                    getDatetime("2016-04-15 11:00:00")));
        assertEquals(3, acts.size());
        assertFalse(acts.contains(reminder1));
        assertTrue(acts.contains(reminder2));
        assertTrue(acts.contains(reminder3));
        assertTrue(acts.contains(reminder4));
        assertFalse(acts.contains(reminder5));
    }

    /**
     * Tests the {@link ReminderRules#getReminders(Party, String, Date, Date)} method.
     */
    @Test
    public void testGetRemindersForProductType() {
        Party patient = patientFactory.createPatient();
        Entity productType1 = productFactory.createProductType("Z Vaccination 1");
        Entity productType2 = productFactory.createProductType("Z Vaccination 2");
        Product product1 = productFactory.createMedication(productType1);
        Product product2 = productFactory.createMedication(productType2);
        Product product3 = productFactory.createMedication();
        Entity reminderType = reminderFactory.createReminderType();
        Act reminder1 = createReminder(patient, reminderType, product1, "2016-04-13 11:59:59", IN_PROGRESS);
        Act reminder2 = createReminder(patient, reminderType, product2, "2016-04-14 10:10:10", IN_PROGRESS);
        Act reminder3 = createReminder(patient, reminderType, product1, "2016-04-14 11:10:10", COMPLETED);
        Act reminder4 = createReminder(patient, reminderType, product3, "2016-04-15 10:10:10", CANCELLED);
        Act reminder5 = createReminder(patient, reminderType, product1, "2016-04-15 11:00:00", IN_PROGRESS);

        List<Act> acts1 = getActs(rules.getReminders(patient, productType1.getName(), getDatetime("2016-04-14 10:00:00"),
                                                     getDatetime("2016-04-15 11:00:00")));
        assertEquals(1, acts1.size());
        assertFalse(acts1.contains(reminder1));
        assertFalse(acts1.contains(reminder2));
        assertTrue(acts1.contains(reminder3));
        assertFalse(acts1.contains(reminder4));
        assertFalse(acts1.contains(reminder5));

        List<Act> acts2 = getActs(rules.getReminders(patient, productType2.getName(), getDatetime("2016-04-14 10:00:00"),
                                                     getDatetime("2016-04-15 11:00:00")));
        assertEquals(1, acts2.size());
        assertTrue(acts2.contains(reminder2));

        List<Act> acts3 = getActs(rules.getReminders(patient, "Z Vacc*", getDatetime("2016-04-14 10:00:00"),
                                                     getDatetime("2016-04-15 11:00:00")));
        assertEquals(2, acts3.size());
        assertTrue(acts3.contains(reminder2));
        assertTrue(acts3.contains(reminder3));
    }

    /**
     * Tests the {@link ReminderRules#markMatchingAlertsCompleted(Act)} method.
     */
    @Test
    public void testMarkMatchingAlertsCompleted() {
        Entity alertTypeA = patientFactory.createAlertType();
        Entity alertTypeB = patientFactory.createAlertType();
        Party patient1 = patientFactory.createPatient();
        Party patient2 = patientFactory.createPatient();

        // create an alert for patient1, and mark matching alerts completed. The alert should still be IN_PROGRESS
        Act alert0 = patientFactory.createAlert(patient1, alertTypeA);
        rules.markMatchingAlertsCompleted(alert0);
        checkAlert(alert0, IN_PROGRESS);

        // create another alert for patient1, with a different reminder type. Verify it has not changed alert0
        Act alert1 = patientFactory.createAlert(patient1, alertTypeB);
        rules.markMatchingAlertsCompleted(alert1);
        checkAlert(alert1, IN_PROGRESS);
        checkAlert(alert0, IN_PROGRESS);

        // create an alert for patient2. Marking matching alerts completed should not affect patient1 alerts
        Act alert2 = patientFactory.createAlert(patient2, alertTypeA);
        rules.markMatchingAlertsCompleted(alert2);
        checkAlert(alert2, IN_PROGRESS);
        checkAlert(alert1, IN_PROGRESS);
        checkAlert(alert0, IN_PROGRESS);

        // create another alert for patient1 for alertB. Verify it marks reminder1 COMPLETED.
        Act alert3 = patientFactory.createAlert(patient1, alertTypeB);
        rules.markMatchingAlertsCompleted(alert3);
        checkAlert(alert3, IN_PROGRESS);
        checkAlert(alert1, COMPLETED);
    }

    /**
     * Tests the {@link ReminderRules#markMatchingAlertsCompleted(List)} method.
     */
    @Test
    public void testMarkMatchingAlertsCompletedForList() {
        Entity alertType = patientFactory.createAlertType();

        Party patient1 = patientFactory.createPatient();
        Party patient2 = patientFactory.createPatient();

        // create alerts for patient1 and patient2
        Act alert0 = patientFactory.createAlert(patient1, alertType);
        Act alert1 = patientFactory.createAlert(patient2, alertType);

        Act alert2 = patientFactory.createAlert(patient1, alertType);
        Act alert3 = patientFactory.createAlert(patient2, alertType);
        Act alert3dup = patientFactory.createAlert(patient2, alertType); // duplicates alert3
        List<Act> alerts = Arrays.asList(alert2, alert3, alert3dup);
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(status -> {
            rules.markMatchingAlertsCompleted(alerts);
            return null;
        });

        checkAlert(alert0, COMPLETED);
        checkAlert(alert1, COMPLETED);
        checkAlert(alert2, IN_PROGRESS);
        checkAlert(alert3, IN_PROGRESS);
        checkAlert(alert3dup, COMPLETED); // as it duplicates alert3
    }

    /**
     * Tests the {@link ReminderRules#copyReminderType(Entity)} method.
     */
    @Test
    public void testCopyReminderType() {
        Entity template1 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template3 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template4 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Lookup group = reminderFactory.createReminderGroup();

        Entity reminderType = reminderFactory.newReminderType()
                .name("Vaccination")
                .description("Vaccination reminder")
                .groupBy(ReminderType.GroupBy.PATIENT)
                .interactive(true)
                .defaultInterval(1, DateUnits.YEARS)
                .cancelInterval(2, DateUnits.WEEKS)
                .sensitivityInterval(5, DateUnits.DAYS)
                .addSpecies("CANINE")
                .addGroups(group)
                .newCount().count(0).interval(-1, DateUnits.MONTHS).template(template1)
                .newRule().email().add()
                .add()
                .newCount().count(1).interval(-2, DateUnits.WEEKS).template(template2)
                .newRule().sms().add()
                .add()
                .newCount().count(2).interval(0, DateUnits.DAYS).template(template3)
                .newRule().print().add()
                .add()
                .newCount().count(3).interval(1, DateUnits.MONTHS).template(template4)
                .newRule().sendTo(ANY).contact().export().list().add()
                .add()
                .build();

        IMObjectGraph graph = rules.copyReminderType(reminderType);

        // verify only the expected objects have been copied
        IMObjectGraphVerifier verifier = new IMObjectGraphVerifier();
        verifier.primaryArchetype(ReminderArchetypes.REMINDER_TYPE)
                .count(ReminderArchetypes.REMINDER_TYPE, 1)
                .count(ReminderArchetypes.REMINDER_COUNT, 4)
                .count(ReminderArchetypes.REMINDER_RULE, 4)
                .verify(graph);
        for (IMObject object : graph.getObjects()) {
            assertTrue(object.isNew());
            assertTrue(object.isActive());
        }

        // verify the copy
        save(graph.getObjects());
        Entity copy = graph.getPrimary(Entity.class);
        assertEquals("Vaccination", copy.getName());
        assertEquals("Vaccination reminder", copy.getDescription());
        assertTrue(copy.isActive());
        ReminderType type = new ReminderType(copy, getArchetypeService());
        IMObjectBean copyBean = getBean(copy);
        List<Lookup> species = copyBean.getValues("species", Lookup.class);
        assertEquals(1, species.size());
        assertEquals("CANINE", species.get(0).getCode());
        assertEquals(ReminderType.GroupBy.PATIENT, type.getGroupBy());
        assertEquals(4, type.getReminderCounts().size());
        assertEquals(1, type.getDefaultInterval());
        assertEquals(DateUnits.YEARS, type.getDefaultUnits());
        assertEquals(2, type.getCancelInterval());
        assertEquals(DateUnits.WEEKS, type.getCancelUnits());
        assertEquals(5, copyBean.getInt("sensitivityInterval"));
        assertEquals(DateUnits.DAYS, DateUnits.fromString(copyBean.getString("sensitivityUnits")));
        assertTrue(type.isInteractive());
        assertEquals(1, type.getGroups().size());
        assertEquals(group, type.getGroups().get(0));
        List<ReminderRule> rules1 = checkCount(type, 0, -1, DateUnits.MONTHS, template1);
        assertEquals(1, rules1.size());
        checkRule(rules1.get(0), false, true, false, false, false, false, FIRST);
        List<ReminderRule> rules2 = checkCount(type, 1, -2, DateUnits.WEEKS, template2);
        assertEquals(1, rules2.size());
        checkRule(rules2.get(0), false, false, true, false, false, false, FIRST);
        List<ReminderRule> rules3 = checkCount(type, 2, 0, DateUnits.DAYS, template3);
        assertEquals(1, rules3.size());
        checkRule(rules3.get(0), false, false, false, true, false, false, FIRST);
        List<ReminderRule> rules4 = checkCount(type, 3, 1, DateUnits.MONTHS, template4);
        assertEquals(1, rules4.size());
        checkRule(rules4.get(0), true, false, false, false, true, true, ANY);

        // now verify the objects in the original reminder type are different to that of the copy
        Set<Entity> originalObjects = getReminderTypeObjects(reminderType);
        Set<Entity> copyObjects = getReminderTypeObjects(copy);
        assertEquals(9, originalObjects.size());
        assertEquals(originalObjects.size(), copyObjects.size());
        for (Entity object : originalObjects) {
            assertFalse(copyObjects.contains(object));
        }
    }

    /**
     * Verifies a reminder count matches that expected.
     *
     * @param type     the reminder type
     * @param count    the reminder count
     * @param interval the expected interval
     * @param units    the expected units
     * @param template the expected document template
     * @return the reminder rules
     */
    private List<ReminderRule> checkCount(ReminderType type, int count, int interval, DateUnits units,
                                          Entity template) {
        ReminderCount actual = type.getReminderCount(count);
        assertNotNull(actual);
        assertEquals(interval, actual.getInterval());
        assertEquals(units, actual.getUnits());
        assertEquals(template, actual.getTemplate().getEntity());
        return actual.getRules();
    }

    /**
     * Verifies a reminder rule matches that expected.
     *
     * @param rule    the rule to check
     * @param contact the expected contact flag
     * @param email   the expected email flag
     * @param sms     the expected SMS flag
     * @param print   the expected print flag
     * @param export  the expected export flag
     * @param list    the expected list flag
     * @param sendTo  the expected sendTo
     */
    private void checkRule(ReminderRule rule, boolean contact, boolean email, boolean sms, boolean print,
                           boolean export, boolean list, ReminderRule.SendTo sendTo) {
        assertEquals(contact, rule.isContact());
        assertEquals(email, rule.isEmail());
        assertEquals(sms, rule.isSMS());
        assertEquals(print, rule.isPrint());
        assertEquals(export, rule.isExport());
        assertEquals(list, rule.isList());
        assertEquals(sendTo == null ? ReminderRule.SendTo.ANY : sendTo, rule.getSendTo());
    }

    /**
     * Returns the objects associated with a reminder type.
     *
     * @param reminderType the reminder type
     * @return a set containing the reminder type, its counts and rules
     */
    private Set<Entity> getReminderTypeObjects(Entity reminderType) {
        Set<Entity> set = new HashSet<>();
        set.add(reminderType);
        for (Entity count : getBean(reminderType).getTargets("counts", Entity.class)) {
            set.add(count);
            set.addAll(getBean(count).getTargets("rules", Entity.class));
        }
        return set;
    }

    /**
     * Helper to convert an iterable of acts to a list.
     *
     * @param acts the acts
     * @return the list of acts
     */
    private List<Act> getActs(Iterable<Act> acts) {
        List<Act> result = new ArrayList<>();
        CollectionUtils.addAll(result, acts);
        return result;
    }

    /**
     * Creates a reminder.
     *
     * @param patient      the reminder
     * @param reminderType the reminder type
     * @param date         the start time
     * @param status       the status
     * @return a new reminder
     */
    private Act createReminder(Party patient, Entity reminderType, String date, String status) {
        return newReminder(patient, reminderType, date, status).build();
    }

    /**
     * Creates a reminder.
     *
     * @param patient      the reminder
     * @param reminderType the reminder type
     * @param product      the product
     * @param date         the start time
     * @param status       the status
     * @return a new reminder
     */
    private Act createReminder(Party patient, Entity reminderType, Product product, String date, String status) {
        return newReminder(patient, reminderType, date, status)
                .product(product)
                .build();
    }

    /**
     * Verifies a reminder matches that expected.
     *
     * @param reminder     the reminder
     * @param reminderType the expected reminder type
     * @param patient      the expected patient
     * @param product      the expected product
     * @param dueDate      the expected due date
     */
    private void checkReminder(Act reminder, Entity reminderType, Party patient, Product product, Date dueDate) {
        IMObjectBean bean = getBean(reminder);
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(reminderType, bean.getTarget("reminderType"));
        assertEquals(product, bean.getTarget("product"));
        assertEquals(dueDate, reminder.getActivityStartTime());
        assertEquals(dueDate, reminder.getActivityEndTime());
    }

    /**
     * Helper to create a reminder with a 1-month interval.
     *
     * @param patient the patient
     * @param groups  the reminder group classifications
     * @return a new reminder
     */
    private Act createReminder(Party patient, Lookup... groups) {
        Entity reminderType = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .addGroups(groups)
                .build();
        return createReminder(patient, reminderType);
    }

    /**
     * Helper to create a reminder.
     *
     * @param patient      the patient
     * @param reminderType the reminder type
     * @return a new reminder
     */
    private Act createReminder(Party patient, Entity reminderType) {
        return newReminder(patient, reminderType).build();
    }

    /**
     * Creates a reminder builder.
     *
     * @param patient      the patient
     * @param reminderType the reminder type
     * @param date         the date from which the due date is calculated
     * @param status       the reminder status
     * @return a reminder builder
     */
    private TestReminderBuilder newReminder(Party patient, Entity reminderType, String date, String status) {
        return newReminder(patient, reminderType)
                .date(date)
                .status(status);
    }

    /**
     * Creates a reminder builder with the patient and reminder type populated.
     *
     * @param patient      the patient
     * @param reminderType the reminder type
     * @return a reminder builder
     */
    private TestReminderBuilder newReminder(Party patient, Entity reminderType) {
        return reminderFactory.newReminder()
                .patient(patient)
                .reminderType(reminderType);
    }

    /**
     * Verifies a reminder has the expected state.
     * For COMPLETED status, checks that the 'completedDate' node is non-null.
     *
     * @param reminder the reminder
     * @param status   the expected reminder status
     */
    private void checkReminder(Act reminder, String status) {
        reminder = get(reminder);
        assertNotNull(reminder);
        assertEquals(status, reminder.getStatus());
        IMObjectBean bean = getBean(reminder);
        Date date = bean.getDate("completedDate");
        if (COMPLETED.equals(status)) {
            assertNotNull(date);
        } else {
            assertNull(date);
        }
    }

    /**
     * Verifies an alert has the expected state.
     * For COMPLETED status, checks that the 'endTime' node is non-null.
     *
     * @param alert  the reminder
     * @param status the expected alert status
     */
    private void checkAlert(Act alert, String status) {
        alert = get(alert);
        assertNotNull(alert);
        assertEquals(status, alert.getStatus());
        if (COMPLETED.equals(status)) {
            assertNotNull(alert.getActivityEndTime());
        }
    }

    /**
     * Checks the {@link ReminderRules#calculateReminderDueDate(Date, Entity)}
     * method.
     *
     * @param defaultInterval the default reminder interval
     * @param defaultUnits    the interval units
     * @param startDate       the reminder start date
     * @param expectedDate    the expected due date
     */
    private void checkCalculateReminderDueDate(int defaultInterval, DateUnits defaultUnits, String startDate,
                                               String expectedDate) {
        Lookup group = reminderFactory.createReminderGroup();
        Entity reminderType = reminderFactory.newReminderType()
                .defaultInterval(defaultInterval, defaultUnits)
                .addGroups(group)
                .build();
        Date start = getDate(startDate);
        Date expected = getDate(expectedDate);
        Date to = rules.calculateReminderDueDate(start, reminderType);
        assertEquals(expected, to);
    }

    /**
     * Checks the {@link ReminderRules#calculateProductReminderDueDate} method.
     *
     * @param period       the reminder interval
     * @param units        the interval units
     * @param startDate    the reminder start date
     * @param expectedDate the expected due date
     */
    private void checkCalculateProductReminderDueDate(int period, DateUnits units, String startDate,
                                                      String expectedDate) {
        Relationship relationship = create("entityLink.productReminder", Relationship.class);
        IMObjectBean bean = getBean(relationship);
        bean.setValue("period", period);
        bean.setValue("periodUom", units.toString());
        Date start = getDate(startDate);
        Date expected = getDate(expectedDate);
        Date to = rules.calculateProductReminderDueDate(start, relationship);
        assertEquals(expected, to);
    }

    /**
     * Checks if a reminder should be cancelled using {@link ReminderRules#shouldCancel(Act, Date)}.
     *
     * @param reminder the reminder
     * @param date     the date
     * @param expected the expected shouldCancel result
     */
    private void checkShouldCancel(Act reminder, String date, boolean expected) {
        assertEquals(expected, rules.shouldCancel(reminder, TestHelper.getDate(date)));
    }
}
