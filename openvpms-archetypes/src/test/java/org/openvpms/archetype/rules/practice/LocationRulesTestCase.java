/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.finance.deposit.DepositTestHelper;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link LocationRules} class.
 *
 * @author Tim Anderson
 */
public class LocationRulesTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The rules.
     */
    private LocationRules rules;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new LocationRules(getArchetypeService());
    }

    /**
     * Tests the {@link LocationRules#getPractice} method.
     */
    @Test
    public void testGetPractice() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getPractice(location));

        Party practice = practiceFactory
                .newPractice()
                .locations(location)
                .build();

        assertEquals(practice, rules.getPractice(location));
    }

    /**
     * Tests the {@link LocationRules#getDefaultDepositAccount} method.
     */
    @Test
    public void testGetDefaultDepositAccount() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getDefaultDepositAccount(location));

        Party account1 = DepositTestHelper.createDepositAccount();
        Party account2 = DepositTestHelper.createDepositAccount();
        IMObjectBean bean = getBean(location);
        bean.addTarget("depositAccounts", account1, "locations");

        assertNull(rules.getDefaultDepositAccount(location));

        Relationship rel2 = bean.addTarget("depositAccounts", account2, "locations");
        EntityRelationshipHelper.setDefault(location, "depositAccounts", rel2, getArchetypeService());

        assertEquals(account2, rules.getDefaultDepositAccount(location));
    }

    /**
     * Tests the {@link LocationRules#getDefaultTill} method.
     */
    @Test
    public void testGetDefaultTill() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getDefaultTill(location));

        Entity till1 = practiceFactory.createTill();
        Entity till2 = practiceFactory.createTill();
        IMObjectBean bean = getBean(location);
        bean.addTarget("tills", till1, "locations");

        assertNull(rules.getDefaultTill(location));

        Relationship rel2 = bean.addTarget("tills", till2);
        EntityRelationshipHelper.setDefault(location, "tills", rel2, getArchetypeService());

        assertEquals(till2, rules.getDefaultTill(location));
    }

    /**
     * Tests the {@link LocationRules#getDefaultTerminal(Entity)} method.
     */
    @Test
    public void testGetDefaultTerminal() {
        Entity till = practiceFactory.createTill();
        assertNull(rules.getDefaultTerminal(till));

        Entity terminal1 = practiceFactory.createEFTPOSTerminal();
        Entity terminal2 = practiceFactory.createEFTPOSTerminal();

        IMObjectBean bean = getBean(till);
        bean.addTarget("terminals", terminal1);

        assertNull(rules.getDefaultTerminal(till));

        Relationship rel2 = bean.addTarget("terminals", terminal2);
        EntityRelationshipHelper.setDefault(till, "terminals", rel2, getArchetypeService());

        assertEquals(terminal2, rules.getDefaultTerminal(till));
    }

    /**
     * Tests the {@link LocationRules#getTerminals(Entity)} method.
     */
    @Test
    public void testGetTerminals() {
        Entity till1 = practiceFactory.createTill();
        assertEquals(0, rules.getTerminals(till1).size());

        Entity terminal1 = practiceFactory.createEFTPOSTerminal();
        Entity terminal2 = practiceFactory.createEFTPOSTerminal();
        Entity till2 = practiceFactory.newTill().terminals(terminal1, terminal2).build();

        assertEquals(2, rules.getTerminals(till2).size());
    }

    /**
     * Tests the {@link LocationRules#getDefaultScheduleView} method.
     */
    @Test
    public void testGetDefaultScheduleView() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getDefaultScheduleView(location));

        Party schedule1 = ScheduleTestHelper.createSchedule(location);
        Party schedule2 = ScheduleTestHelper.createSchedule(location);
        Entity view1 = ScheduleTestHelper.createScheduleView(schedule1);
        Entity view2 = ScheduleTestHelper.createScheduleView(schedule1, schedule2);
        IMObjectBean bean = getBean(location);
        bean.addTarget("scheduleViews", view1);

        assertNull(rules.getDefaultScheduleView(location));

        Relationship rel2 = bean.addTarget("scheduleViews", view2);
        EntityRelationshipHelper.setDefault(location, "scheduleViews", rel2, getArchetypeService());

        assertEquals(view2, rules.getDefaultScheduleView(location));
    }

    /**
     * Tests the {@link LocationRules#getScheduleViews(Party)} method.
     */
    @Test
    public void testGetScheduleViews() {
        Party location = practiceFactory.createLocation();
        assertEquals(0, rules.getScheduleViews(location).size());

        Party schedule1 = ScheduleTestHelper.createSchedule(location);
        Party schedule2 = ScheduleTestHelper.createSchedule(location);
        Entity view1 = ScheduleTestHelper.createScheduleView(schedule1);
        Entity view2 = ScheduleTestHelper.createScheduleView(schedule1, schedule2);
        IMObjectBean bean = getBean(location);
        bean.addTarget("scheduleViews", view1);
        bean.addTarget("scheduleViews", view2);
        bean.save(location);

        assertEquals(2, rules.getScheduleViews(location).size());
        view2.setActive(false);
        save(view2);

        assertEquals(1, rules.getScheduleViews(location).size());
    }

    /**
     * Tests the {@link LocationRules#getDefaultWorkListView} method.
     */
    @Test
    public void testGetDefaultWorkListView() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getDefaultWorkListView(location));

        Party worklist1 = ScheduleTestHelper.createWorkList();
        Party worklist2 = ScheduleTestHelper.createWorkList();
        Entity view1 = ScheduleTestHelper.createWorkListView(worklist1);
        Entity view2 = ScheduleTestHelper.createWorkListView(worklist1, worklist2);
        IMObjectBean bean = getBean(location);
        bean.addTarget("workListViews", view1);

        assertNull(rules.getDefaultWorkListView(location));

        Relationship rel2 = bean.addTarget("workListViews", view2);
        EntityRelationshipHelper.setDefault(location, "workListViews", rel2, getArchetypeService());

        assertEquals(view2, rules.getDefaultWorkListView(location));
    }

    /**
     * Tests the {@link LocationRules#getWorkListViews(Party)} method.
     */
    @Test
    public void testGetWorkListViews() {
        Party location = practiceFactory.createLocation();
        assertEquals(0, rules.getWorkListViews(location).size());

        Party worklist1 = ScheduleTestHelper.createWorkList();
        Party worklist2 = ScheduleTestHelper.createWorkList();
        Entity view1 = ScheduleTestHelper.createWorkListView(worklist1);
        Entity view2 = ScheduleTestHelper.createWorkListView(worklist1, worklist2);
        IMObjectBean bean = getBean(location);
        bean.addTarget("workListViews", view1);
        bean.addTarget("workListViews", view2);
        bean.save(location);

        assertEquals(2, rules.getWorkListViews(location).size());
        view2.setActive(false);
        save(view2);

        assertEquals(1, rules.getWorkListViews(location).size());
    }

    /**
     * Tests the {@link LocationRules#getDefaultStockLocation}
     * and {@link LocationRules#getDefaultStockLocationRef} methods.
     */
    @Test
    public void testGetStockLocation() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getDefaultStockLocation(location));
        assertNull(rules.getDefaultStockLocationRef(location));
        Party stockLocation = ProductTestHelper.createStockLocation();
        IMObjectBean bean = getBean(location);
        bean.addTarget("stockLocations", stockLocation);

        assertEquals(stockLocation, rules.getDefaultStockLocation(location));
        assertEquals(stockLocation.getObjectReference(), rules.getDefaultStockLocationRef(location));
    }

    /**
     * Tests the {@link LocationRules#getMailServer(Party)} method.
     */
    @Test
    public void testGetMailServer() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getMailServer(location));

        Entity mailServer = practiceFactory.newMailServer().host("localhost").build();
        practiceFactory.updateLocation(location)
                .setMailServer(mailServer)
                .build();
        assertEquals(mailServer.getId(), rules.getMailServer(location).getId());

        // mark the mail server inactive and verify it is no longer returned
        mailServer.setActive(false);
        save(mailServer);
        assertNull(rules.getMailServer(location));
    }

    /**
     * Tests the {@link LocationRules#getPrinters} method.
     */
    @Test
    public void testGetPrinters() {
        Party location = practiceFactory.createLocation();
        assertEquals(0, rules.getPrinters(location).size());
        IMObject printer1 = createPrinter("printer1");
        IMObject printer2 = createPrinter("printer2");
        save(printer1, printer2);
        IMObjectBean bean = getBean(location);
        bean.addTarget("printers", printer1);
        bean.addTarget("printers", printer2);
        bean.save();

        List<PrinterReference> printers = rules.getPrinters(location);
        assertEquals(2, printers.size());
        assertTrue(printers.get(0).getId().equals("printer1")
                   || printers.get(1).getId().equals("printer1"));
        assertTrue(printers.get(0).getId().equals("printer2")
                   || printers.get(1).getId().equals("printer2"));
    }

    /**
     * Tests the {@link LocationRules#getGapBenefitTill} method.
     */
    @Test
    public void testGapBenefitTill() {
        Party location = practiceFactory.createLocation();
        assertNull(rules.getGapBenefitTill(location));

        IMObjectBean bean = getBean(location);
        Entity till = practiceFactory.createTill();
        bean.setTarget("gapBenefitTill", till);
        bean.save();

        assertEquals(till, rules.getGapBenefitTill(location));
    }

    /**
     * Creates a printer.
     *
     * @param printer the printer name
     * @return the printer
     */
    protected IMObject createPrinter(String printer) {
        IMObject object = create("entity.printer");
        IMObjectBean bean = getBean(object);
        bean.setValue("name", printer);
        bean.setValue("printer", ":" + printer);
        bean.save();
        return object;
    }
}
