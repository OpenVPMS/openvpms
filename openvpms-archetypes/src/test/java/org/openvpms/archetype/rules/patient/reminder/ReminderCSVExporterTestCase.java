/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.patient.reminder;

import au.com.bytecode.opencsv.CSVReader;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.lookup.LookupService;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openvpms.archetype.rules.util.DateUnits.HOURS;
import static org.openvpms.archetype.rules.util.DateUnits.MINUTES;
import static org.openvpms.archetype.test.TestHelper.getLookup;
import static org.openvpms.component.math.WeightUnits.KILOGRAMS;

/**
 * Tests the {@link ReminderCSVExporter} class.
 *
 * @author Tim Anderson
 */
public class ReminderCSVExporterTestCase extends ArchetypeServiceTest {

    /**
     * The exporter.
     */
    private ReminderCSVExporter exporter;

    /**
     * The document handlers.
     */
    private DocumentHandlers handlers;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        practice = create(PracticeArchetypes.PRACTICE, Party.class);
        PracticeRules practiceRules = new PracticeRules(service, null);
        PracticeService practiceService = new PracticeService(service, practiceRules, null) {
            @Override
            public synchronized Party getPractice() {
                return practice;
            }
        };

        LookupService lookups = getLookupService();
        PartyRules partyRules = new PartyRules(service, lookups);
        PatientRules patientRules = new PatientRules(practiceRules, practiceService, service, lookups);
        AppointmentRules appointmentRules = new AppointmentRules(service);
        handlers = new DocumentHandlers(getArchetypeService());
        exporter = new ReminderCSVExporter(practiceService, partyRules, patientRules, appointmentRules, service,
                                           handlers);
        location = TestHelper.createLocation();
    }

    /**
     * Tests export using comma separated values.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testCSVExport() throws IOException {
        Party customer = createCustomer("Foo", "F", "Bar", "Embedded, Commas");
        checkExport(customer, true);
    }

    /**
     * Tests export using tab separated values.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testTabExport() throws IOException {
        Party customer = createCustomer("Foo", "F", "Bar", "Embedded\tTabs");
        checkExport(customer, false);
    }

    /**
     * Tests export with embedded quotes.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testEmbeddedQuotes() throws IOException {
        Party customer = createCustomer("Foo", "F", "Bar", "\"Embedded Quotes\"");
        checkExport(customer, true);
    }

    /**
     * Checks export.
     *
     * @param customer       the customer
     * @param commaSeparated if {@code true} use comma separated values, otherwise use tab separated values
     * @throws IOException for  any I/O error
     */
    private void checkExport(Party customer, boolean commaSeparated) throws IOException {
        IMObjectBean practiceBean = getBean(practice);
        practiceBean.setValue("fileExportFormat", commaSeparated ? "COMMA" : "TAB");

        if (commaSeparated) {
            assertEquals(',', exporter.getSeparator());
        } else {
            assertEquals('\t', exporter.getSeparator());
        }

        Contact address = TestHelper.createLocationContact("Twenty Second Avenue", "SAWTELL",
                                                           "Sawtell", "NSW", "New South Wales", "2452");
        customer.addContact(address);
        customer.addContact(TestHelper.createPhoneContact("03", "1234 5678"));
        Contact mobile = TestHelper.createPhoneContact(null, "5678 1234");
        IMObjectBean phoneBean = getBean(mobile);
        phoneBean.setValue("sms", true);
        phoneBean.setValue("preferred", false);
        customer.addContact(mobile);
        customer.addContact(TestHelper.createEmailContact("foo@bar.com"));
        Party patient = createPatient(customer);
        PatientTestHelper.createWeight(patient, TestHelper.getDate("2014-01-01"), ONE, KILOGRAMS);
        PatientTestHelper.createWeight(patient, TestHelper.getDate("2015-01-01"), TEN, KILOGRAMS);
        Party schedule = ScheduleTestHelper.createSchedule(location);
        Date start1 = DateRules.getYesterday();
        Date start2 = DateRules.getTomorrow();
        Date start3 = DateRules.getDate(DateRules.getNextDate(start2), 15, HOURS);
        Act appointment1 = ScheduleTestHelper.createAppointment(start1, DateRules.getDate(start1, 15, MINUTES),
                                                                schedule, customer, patient);
        Act appointment2 = ScheduleTestHelper.createAppointment(start2, DateRules.getDate(start2, 15, MINUTES),
                                                                schedule, customer, patient);
        Act appointment3 = ScheduleTestHelper.createAppointment(start3, DateRules.getDate(start2, 15, MINUTES),
                                                                schedule, customer, patient);
        appointment1.setStatus(AppointmentStatus.PENDING);
        appointment2.setStatus(AppointmentStatus.CANCELLED);
        appointment3.setStatus(AppointmentStatus.CONFIRMED);
        save(appointment1, appointment2, appointment3);

        Entity reminderType = ReminderTestHelper.createReminderType();

        Act reminder = ReminderTestHelper.createReminder(patient, reminderType);
        IMObjectBean reminderBean = getBean(reminder);
        reminderBean.setValue("lastSent", TestHelper.getDate("2013-06-05"));
        reminderBean.save();

        ReminderEvent event = createReminderEvent(customer, address, patient, reminderType, reminder);
        Document document = exporter.export(Collections.singletonList(event), new Date());
        List<String[]> lines = readCSV(document);
        assertNotNull(lines);
        assertEquals(2, lines.size());
        assertArrayEquals(ReminderCSVExporter.HEADER, lines.get(0));

        IMObjectBean bean = getBean(customer);
        String firstName = bean.getString("firstName");
        String initials = bean.getString("initials");
        String lastName = bean.getString("lastName");
        String companyName = bean.getString("companyName");

        String startTime = ReminderCSVExporter.getDateTime(start3);

        String[] expected = {getId(customer), "Mr", firstName, initials, lastName, companyName, "Twenty Second Avenue",
                             "Sawtell", "New South Wales", "2452", "(03) 1234 5678", "5678 1234", "foo@bar.com",
                             getId(patient), patient.getName(), "Canine", "Kelpie", "Male", "Black", "2013-02-01",
                             getId(reminderType), reminderType.getName(),
                             getDate(reminder.getActivityEndTime()), "0", "2013-06-05", "10", "KILOGRAMS",
                             "2015-01-01", location.getName(), startTime};
        assertArrayEquals(expected, lines.get(1));
    }

    /**
     * Creates a new {@code EXPORT} reminder event.
     *
     * @param customer     the customer
     * @param contact      the customer contact
     * @param patient      the patient
     * @param reminderType the reminder type
     * @param reminder     the reminder
     * @return a new reminder event
     */
    private ReminderEvent createReminderEvent(Party customer, Contact contact, Party patient, Entity reminderType,
                                              Act reminder) {
        Act item = create(ReminderArchetypes.EXPORT_REMINDER, Act.class);
        ReminderEvent event = new ReminderEvent(reminder, item, patient, customer);
        event.setContact(contact);
        event.setReminderType(reminderType);
        return event;
    }

    /**
     * Creates  a new customer.
     *
     * @param firstName   the customer's first name
     * @param initials    the customer's initials. May be {@code null}
     * @param lastName    the customer's last name
     * @param companyName the company name. May be {@code null}
     * @return a new customer
     */
    private Party createCustomer(String firstName, String initials, String lastName, String companyName) {
        Party customer = create(CustomerArchetypes.PERSON, Party.class);
        IMObjectBean bean = getBean(customer);
        getLookup("lookup.personTitle", "MR", "Mr", true);
        bean.setValue("title", "MR");
        bean.setValue("firstName", firstName);
        bean.setValue("initials", initials);
        bean.setValue("lastName", lastName);
        bean.setValue("companyName", companyName);
        bean.setTarget("practice", location);
        bean.save();
        return customer;
    }

    /**
     * Reads a CSV document.
     *
     * @param document the document to read
     * @return the lines read
     * @throws IOException for any I/O error
     */
    private List<String[]> readCSV(Document document) throws IOException {
        InputStreamReader reader = new InputStreamReader(handlers.get(document).getContent(document));
        CSVReader csv = new CSVReader(reader, exporter.getSeparator());
        return csv.readAll();
    }

    /**
     * Creates a patient.
     *
     * @param customer the patient owner
     * @return the new patient
     */
    private Party createPatient(Party customer) {
        Lookup species = getLookup("lookup.species", "CANINE", "Canine", true);
        getLookup("lookup.breed", "KELPIE", "Kelpie", species, "lookupRelationship.speciesBreed");
        Party patient = TestHelper.createPatient(customer);
        IMObjectBean patientBean = getBean(patient);
        patientBean.setValue("breed", "KELPIE");
        patientBean.setValue("sex", "MALE");
        patientBean.setValue("dateOfBirth", TestHelper.getDate("2013-02-01"));
        patientBean.setValue("colour", "Black");
        patientBean.save();
        return patient;
    }

    /**
     * Converts a date to a string.
     *
     * @param date the date
     * @return the string value of the date
     */
    private String getDate(Date date) {
        return new java.sql.Date(date.getTime()).toString();
    }

    /**
     * Converts an object id to a string.
     *
     * @param object the object
     * @return the object's id, as a string
     */
    private String getId(IMObject object) {
        return Long.toString(object.getId());
    }

}
