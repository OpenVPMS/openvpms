/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.product.PricingGroup.ALL;
import static org.openvpms.archetype.rules.product.ProductArchetypes.FIXED_PRICE;
import static org.openvpms.archetype.rules.product.ProductArchetypes.UNIT_PRICE;
import static org.openvpms.archetype.rules.util.DateRules.getToday;
import static org.openvpms.archetype.rules.util.DateRules.getTomorrow;
import static org.openvpms.archetype.rules.util.DateRules.getYesterday;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link ProductPriceRules} class.
 *
 * @author Tim Anderson
 */
public class ProductPriceRulesTestCase extends AbstractProductTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The <em>party.organisationPractice</em>, for taxes.
     */
    private Party practice;

    /**
     * The practice location currency.
     */
    private Currency currency;

    /**
     * The rules.
     */
    private ProductPriceRules rules;

    /**
     * Sets up the test case.
     * <p>
     * This sets up the practice to have a 10% tax on all products.
     */
    @Before
    public void setUp() {
        practice = practiceFactory.newPractice()
                .addTaxType(BigDecimal.TEN)
                .build();
        rules = new ProductPriceRules(getArchetypeService());
        IMObjectBean bean = getBean(practice);
        Currencies currencies = new Currencies(getArchetypeService(), getLookupService());
        currency = currencies.getCurrency(bean.getString("currency"));
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, String, Date, Lookup)} method.
     */
    @Test
    public void testGetProductPrice() {
        checkProductPrice(createMedication(), false);
        checkProductPrice(createMerchandise(), false);
        checkProductPrice(createService(), false);
        checkProductPrice(createPriceTemplate(), false);
        checkProductPrice(createTemplate(), false);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, String, Date, Lookup)} method for products
     * with multiple pricing groups.
     */
    @Test
    public void testGetProductPriceWithPriceGroups() {
        checkProductPriceWithPriceGroups(createMedication(), false);
        checkProductPriceWithPriceGroups(createMerchandise(), false);
        checkProductPriceWithPriceGroups(createService(), false);
        checkProductPriceWithPriceGroups(createPriceTemplate(), false);
        checkProductPriceWithPriceGroups(createTemplate(), false);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)} method for
     * products that support <em>product.priceTemplate</em>.
     */
    @Test
    public void testGetProductPriceForProductWithPriceTemplate() {
        checkProductPrice(createMedication(), true);
        checkProductPrice(createMerchandise(), true);
        checkProductPrice(createService(), true);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, String, Date, Lookup)} method for products with
     * multiple pricing groups that support <em>product.priceTemplate</em>.
     */
    @Test
    public void testGetProductPriceWithPriceGroupsForProductWithPriceTemplate() {
        checkProductPriceWithPriceGroups(createMedication(), true);
        checkProductPriceWithPriceGroups(createMerchandise(), true);
        checkProductPriceWithPriceGroups(createService(), true);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method.
     */
    @Test
    public void testGetProductPriceForPrice() {
        checkGetProductPriceForPrice(createMedication(), false);
        checkGetProductPriceForPrice(createMerchandise(), false);
        checkGetProductPriceForPrice(createService(), false);
        checkGetProductPriceForPrice(createPriceTemplate(), false);
        checkGetProductPriceForPrice(createTemplate(), false);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method, for
     * for products that support <em>product.priceTemplate</em>.
     */
    @Test
    public void testGetProductPriceForPriceForProductWithPriceTemplate() {
        checkGetProductPriceForPrice(createMedication(), true);
        checkGetProductPriceForPrice(createMerchandise(), true);
        checkGetProductPriceForPrice(createService(), true);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method.
     */
    @Test
    public void testGetProductPriceForPriceWithPriceGroups() {
        checkGetProductPriceForPriceWithPriceGroups(createMedication(), false);
        checkGetProductPriceForPriceWithPriceGroups(createMerchandise(), false);
        checkGetProductPriceForPriceWithPriceGroups(createService(), false);
        checkGetProductPriceForPriceWithPriceGroups(createPriceTemplate(), false);
        checkGetProductPriceForPriceWithPriceGroups(createTemplate(), false);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method, for
     * for products that support <em>product.priceTemplate</em>.
     */
    @Test
    public void testGetProductPriceForPriceWithPriceGroupsForProductWithPriceTemplate() {
        checkGetProductPriceForPriceWithPriceGroups(createMedication(), true);
        checkGetProductPriceForPriceWithPriceGroups(createMerchandise(), true);
        checkGetProductPriceForPriceWithPriceGroups(createService(), true);
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method.
     */
    @Test
    public void testGetProductPrices() {
        checkGetProductPrices(createMedication());
        checkGetProductPrices(createMerchandise());
        checkGetProductPrices(createService());
        checkGetProductPrices(createPriceTemplate());
        checkGetProductPrices(createTemplate());
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method for products that may be associated with an
     * <em>product.priceTemplate</em> product.
     */
    @Test
    public void testGetProductPricesForProductWithPriceTemplate() {
        checkGetProductPricesForProductWithPriceTemplate(createMedication());
        checkGetProductPricesForProductWithPriceTemplate(createMerchandise());
        checkGetProductPricesForProductWithPriceTemplate(createService());
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method for products with multiple pricing groups.
     */
    @Test
    public void testGetProductPricesWithPriceGroups() {
        checkGetProductPricesWithPriceGroups(createMedication());
        checkGetProductPricesWithPriceGroups(createMerchandise());
        checkGetProductPricesWithPriceGroups(createService());
        checkGetProductPricesWithPriceGroups(createPriceTemplate());
        checkGetProductPricesWithPriceGroups(createTemplate());
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method for products that may be associated with an
     * <em>product.priceTemplate</em> product.
     */
    @Test
    public void testGetProductPricesWithPriceGroupsForProductWithPriceTemplate() {
        checkGetProductPricesWithPriceGroupsForProductWithPriceTemplate(createMedication());
        checkGetProductPricesWithPriceGroupsForProductWithPriceTemplate(createMerchandise());
        checkGetProductPricesWithPriceGroupsForProductWithPriceTemplate(createService());
    }

    /**
     * Verifies that the default price is returned if no price has a matching pricing group.
     */
    @Test
    public void testDefaultProductPriceForPriceGroup() {
        Lookup groupA = productFactory.createPricingGroup("A");

        Date today = getToday();
        ProductPrice price1 = productFactory.newFixedPrice()
                .cost(0)
                .markup(0)
                .price(0)
                .maxDiscount(0)
                .fromDate(today)
                .toDate(null)
                .defaultPrice(true)
                .build();
        ProductPrice price2 = productFactory.newFixedPrice()
                .cost(1)
                .markup(0)
                .price(0)
                .maxDiscount(0)
                .fromDate(today)
                .toDate(null)
                .defaultPrice(false)
                .build();
        Product product = productFactory.newMedication()
                .addPrice(price1)
                .addPrice(price2)
                .build();

        // for no pricing group, the default should be returned
        ProductPrice price = rules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, today, null);
        assertEquals(price, price1);

        // verify that when no price has a pricing group, and a group is specified, the default is returned
        price = rules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, today, groupA);
        assertEquals(price, price1);

        // verify that the default is not returned when another price has a matching group
        price2.addClassification(groupA);
        price = rules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, today, groupA);
        assertEquals(price, price2);

        // no pricing group, the default should still be returned
        price = rules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, today, null);
        assertEquals(price, price1);

        // now make price2 a default. This should be returned over price1 when groupA is specified. Without the group
        // which one is returned is non-deterministic
        productFactory.updateFixedPrice(price2)
                .defaultPrice(true)
                .build();
        price = rules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, today, groupA);
        assertEquals(price, price2);
    }

    /**
     * Tests the {@link ProductPriceRules#getTaxIncPrice(BigDecimal, Product, Party, Currency)} and
     * {@link ProductPriceRules#getTaxIncPrice(BigDecimal, BigDecimal, Currency)} methods.
     * <p>
     * This verifies that tax rates can be expressed to 2 decimal places.
     */
    @Test
    public void testGetTaxIncPrice() {
        BigDecimal taxRate = new BigDecimal("8.25");
        practice = practiceFactory.updatePractice(practice)
                .taxTypes(taxRate)
                .build();
        ProductPrice unitPrice = productFactory.newUnitPrice()
                .price("16.665")
                .cost("8.33")
                .markup("100.10")
                .maxDiscount(50)
                .fromDate(getToday())
                .toDate(null)  // active
                .build();

        Product product = productFactory.newMedication()
                .addPrice(unitPrice)
                .build();

        BigDecimal price1 = rules.getTaxIncPrice(unitPrice.getPrice(), product, practice, currency);
        checkEquals(new BigDecimal("18.04"), price1);

        BigDecimal price2 = rules.getTaxIncPrice(unitPrice.getPrice(), taxRate, currency);
        checkEquals(new BigDecimal("18.04"), price2);
    }

    /**
     * Tests the {@link ProductPriceRules#getTaxIncPrice(BigDecimal, Product, Party, Currency)} method,
     * when the currency has a non-zero {@code minPrice}.
     */
    @Test
    public void testGetTaxIncPriceWithPriceRounding() {
        java.util.Currency AUD = java.util.Currency.getInstance("AUD");

        // remove tax as it complicates rounding tests
        practice = practiceFactory.updatePractice(practice)
                .removeTaxTypes()
                .build();
        Product product = productFactory.createMedication();
        BigDecimal minDenomination = new BigDecimal("0.05");
        BigDecimal minPrice = new BigDecimal("0.20"); // round all prices to 0.20 increments

        // test HALF_UP rounding
        Currency currency1 = new Currency(AUD, RoundingMode.HALF_UP, minDenomination, minPrice);
        checkGetTaxIncPrice("0.18", "0.20", product, currency1);
        checkGetTaxIncPrice("0.30", "0.40", product, currency1);
        checkGetTaxIncPrice("0.44", "0.40", product, currency1);
        checkGetTaxIncPrice("0.50", "0.60", product, currency1);
        checkGetTaxIncPrice("1.50", "1.60", product, currency1);
        checkGetTaxIncPrice("2.50", "2.60", product, currency1);

        // test HALF_DOWN rounding
        Currency currency2 = new Currency(AUD, RoundingMode.HALF_DOWN, minDenomination, minPrice);
        checkGetTaxIncPrice("0.18", "0.20", product, currency2);
        checkGetTaxIncPrice("0.30", "0.20", product, currency2);
        checkGetTaxIncPrice("0.44", "0.40", product, currency2);
        checkGetTaxIncPrice("0.50", "0.40", product, currency2);
        checkGetTaxIncPrice("1.50", "1.40", product, currency2);
        checkGetTaxIncPrice("2.50", "2.40", product, currency2);

        // test HALF_EVEN rounding
        Currency currency3 = new Currency(AUD, RoundingMode.HALF_EVEN, minDenomination, minPrice);
        checkGetTaxIncPrice("0.18", "0.20", product, currency3);
        checkGetTaxIncPrice("0.30", "0.40", product, currency3);
        checkGetTaxIncPrice("0.44", "0.40", product, currency3);
        checkGetTaxIncPrice("0.50", "0.40", product, currency3); // round down as 0 is even
        checkGetTaxIncPrice("1.50", "1.60", product, currency3); // round up as 1 is odd
        checkGetTaxIncPrice("2.50", "2.40", product, currency3); // round down as 2 is even
    }

    /**
     * Tests the {@link ProductPriceRules#getMarkup} method.
     */
    @Test
    public void testGetMarkup() {
        BigDecimal cost = BigDecimal.ONE;
        BigDecimal price = new BigDecimal("2");
        BigDecimal markup = rules.getMarkup(cost, price);
        checkEquals(BigDecimal.valueOf(100), markup);
    }

    /**
     * Tests the {@link ProductPriceRules#getMaxDiscount(ProductPrice)} method.
     */
    @Test
    public void testGetMaxDiscount() {
        ProductPrice price = productFactory.newFixedPrice()
                .dateRange(new Date(), new Date())
                .build();
        checkEquals(new BigDecimal(100), rules.getMaxDiscount(price));

        productFactory.updateFixedPrice(price)
                .maxDiscount(10)
                .build();

        checkEquals(BigDecimal.TEN, rules.getMaxDiscount(price));
    }

    /**
     * Tests the {@link ProductPriceRules#updateUnitPrices(Product, BigDecimal, boolean, Currency)} method.
     */
    @Test
    public void testUpdateUnitPrices() {
        BigDecimal cost = BigDecimal.ONE;
        BigDecimal markup = BigDecimal.valueOf(50);
        BigDecimal price = new BigDecimal("1.5");
        BigDecimal maxDiscount = BigDecimal.valueOf(60);

        // NOTE: that without the pricing groups, unit2 and unit3 overlap, and unit2 should have an end date of
        // tomorrow. In a single location setting this it would be ambiguous as to which one to choose for today or
        // tomorrow. However, it is valid in the case of each price having a unique pricing group; the pricing group
        // determines which one is selected
        Date date1 = getYesterday();
        Date date2 = getToday();
        Date date3 = getTomorrow();
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");
        Lookup groupC = productFactory.createPricingGroup("C");

        ProductPrice unit1 = productFactory.newUnitPrice() // inactive
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(null, date1)
                .build();

        ProductPrice unit2 = productFactory.newUnitPrice() // active
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(date2, null)
                .pricingGroups(groupA)
                .build();

        ProductPrice unit3 = productFactory.newUnitPrice() // active
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .allDates()
                .pricingGroups(groupB)
                .build();

        ProductPrice unit4 = productFactory.newUnitPrice() // inactive
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(date3, null)
                .pricingGroups(groupC)
                .build();

        Date now = DateRules.getDate(date2, 12, DateUnits.HOURS); // used to close off prices at 12pm.

        ProductPriceRules rules = new ProductPriceRules(getArchetypeService()) {
            @Override
            protected Date getNow() {
                return now;
            }
        };

        Product product = productFactory.newMedication()
                .addPrices(unit1, unit2, unit3, unit4)
                .build();

        BigDecimal newCost = BigDecimal.valueOf(2);
        BigDecimal newPrice = BigDecimal.valueOf(3);
        List<ProductPrice> updated1 = rules.updateUnitPrices(product, newCost, false, currency);
        assertEquals(4, updated1.size());
        assertFalse(updated1.contains(unit1));
        assertTrue(updated1.contains(unit2));
        assertTrue(updated1.contains(unit3));
        assertFalse(updated1.contains(unit4));

        // verify that unit1 and unit4 are unchanged, and unit2 and unit3 have their to-date set to now
        ProductPriceTestHelper.checkPrice(unit1, price, cost, markup, maxDiscount, unit1.getFromDate(),
                                          unit1.getToDate(), null);
        ProductPriceTestHelper.checkPrice(unit2, price, cost, markup, maxDiscount, unit2.getFromDate(), now, groupA);
        ProductPriceTestHelper.checkPrice(unit3, price, cost, markup, maxDiscount, unit3.getFromDate(), now, groupB);
        ProductPriceTestHelper.checkPrice(unit4, price, cost, markup, maxDiscount, unit4.getFromDate(),
                                          unit4.getToDate(), groupC);

        // now check the list of updated prices contains unit2 and unit3 with their to-date set to now, and
        // that there is a new product price corresponding to each with a new cost and price starting now, and with
        // no to-date
        checkPrice(updated1, cost, markup, price, maxDiscount, unit2.getFromDate(), now, groupA, unit2);
        ProductPrice unit5 = checkPrice(updated1, newCost, markup, newPrice, maxDiscount, now, null, groupA, null);
        // replacement for unit2

        checkPrice(updated1, cost, markup, price, maxDiscount, unit3.getFromDate(), now, groupB, unit3);
        ProductPrice unit6 = checkPrice(updated1, newCost, markup, newPrice, maxDiscount, now, null, groupB, null);
        // replacement for unit3

        // verify that when list price decreases are ignored, no price updates
        List<ProductPrice> updated2 = rules.updateUnitPrices(product, BigDecimal.ONE, true, currency);
        assertEquals(0, updated2.size());

        ProductPriceTestHelper.checkPrice(unit1, price, cost, markup, maxDiscount, unit1.getFromDate(),
                                          unit1.getToDate(), null);
        ProductPriceTestHelper.checkPrice(unit2, price, cost, markup, maxDiscount, unit2.getFromDate(), now, groupA);
        ProductPriceTestHelper.checkPrice(unit3, price, cost, markup, maxDiscount, unit3.getFromDate(), now, groupB);
        ProductPriceTestHelper.checkPrice(unit4, price, cost, markup, maxDiscount, unit4.getFromDate(),
                                          unit4.getToDate(), groupC);
        ProductPriceTestHelper.checkPrice(unit5, newPrice, newCost, markup, maxDiscount, now, null, groupA);
        ProductPriceTestHelper.checkPrice(unit6, newPrice, newCost, markup, maxDiscount, now, null, groupB);
    }

    /**
     * Verifies that {@link ProductPriceRules#updateUnitPrices(Product, BigDecimal, boolean, Currency)} method does
     * not create a new price if the previous price is unsaved.
     * <p/>
     * This ensures that multiple prices aren't created when editing products.
     */
    @Test
    public void testUpdateUnitPricesForUnsavedPrices() {
        BigDecimal cost1 = BigDecimal.ONE;
        BigDecimal markup = BigDecimal.valueOf(50);
        BigDecimal price1 = new BigDecimal("1.5");
        BigDecimal maxDiscount = BigDecimal.valueOf(60);

        Date from = getToday();
        ProductPrice unit = productFactory.newMedication()
                .newUnitPrice()
                .price(price1)
                .cost(cost1)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(from, null)
                .build();
        Product product = productFactory.createMedication();
        product.addProductPrice(unit);  // unsaved

        BigDecimal cost2 = BigDecimal.valueOf(2);
        BigDecimal price2 = BigDecimal.valueOf(3);
        List<ProductPrice> updated1 = rules.updateUnitPrices(product, cost2, false, currency);
        assertEquals(1, updated1.size());

        // verify the price has been updated, and it is present in the list of updated prices
        ProductPriceTestHelper.checkPrice(unit, price2, cost2, markup, maxDiscount, from, null, null);
        checkPrice(updated1, cost2, markup, price2, maxDiscount, from, null, null, unit);

        // verify that when list price decreases are ignored, no price updates
        List<ProductPrice> updated2 = rules.updateUnitPrices(product, BigDecimal.ONE, true, currency);
        assertEquals(0, updated2.size());

        BigDecimal cost3 = BigDecimal.valueOf(4);
        BigDecimal price3 = BigDecimal.valueOf(6);
        List<ProductPrice> updated3 = rules.updateUnitPrices(product, cost3, true, currency);
        assertEquals(1, updated3.size());

        // verify the price has been updated, and it is present in the list of updated prices
        ProductPriceTestHelper.checkPrice(unit, price3, cost3, markup, maxDiscount, from, null, null);
        checkPrice(updated1, cost3, markup, price3, maxDiscount, from, null, null, unit);

    }

    /**
     * Tests the {@link ProductPriceRules#getMaxDiscount(BigDecimal)} method.
     */
    @Test
    public void testCalcMaxDiscount() {
        checkEquals(ProductPriceRules.DEFAULT_MAX_DISCOUNT, rules.getMaxDiscount(BigDecimal.ZERO));
        checkEquals(new BigDecimal("33.3"), rules.getMaxDiscount(BigDecimal.valueOf(50)));
        checkEquals(new BigDecimal(50), rules.getMaxDiscount(BigDecimal.valueOf(100)));
        checkEquals(new BigDecimal("66.7"), rules.getMaxDiscount(BigDecimal.valueOf(200)));
    }

    /**
     * Tests the {@link ProductPriceRules#getServiceRatio(Product, Entity, Party)} method.
     */
    @Test
    public void testGetServiceRatioForLocation() {
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();

        Entity department = practiceFactory.createDepartment();
        Party location = practiceFactory.createLocation();

        Entity productType1 = productFactory.createProductType();
        Entity productType2 = productFactory.createProductType();
        checkNoServiceRatio(product1, null, location);
        checkNoServiceRatio(product2, null, location);
        checkNoServiceRatio(product1, department, location);
        checkNoServiceRatio(product2, department, location);

        // add some product types and verify there are no service ratios
        productFactory.updateMedication(product1)
                .type(productType1)
                .build();
        productFactory.updateMedication(product2)
                .type(productType2)
                .build();
        checkNoServiceRatio(product1, null, location);
        checkNoServiceRatio(product2, null, location);
        checkNoServiceRatio(product1, department, location);
        checkNoServiceRatio(product2, department, location);

        // adds some service ratios
        Entity calendar = productFactory.createServiceRatioCalendar();
        BigDecimal ratioA = new BigDecimal("1.1");
        BigDecimal ratioB = new BigDecimal("1.2");
        BigDecimal ratioC = new BigDecimal("1.3");
        BigDecimal ratioD = new BigDecimal("1.4");

        practiceFactory.updateDepartment(department)
                .addServiceRatio(productType1, ratioA, calendar)
                .addServiceRatio(productType2, ratioB)
                .build();
        practiceFactory.updateLocation(location)
                .addServiceRatio(productType1, ratioC, calendar)
                .addServiceRatio(productType2, ratioD)
                .build();

        checkServiceRatio(product1, department, location, ratioA, calendar);
        checkServiceRatio(product2, department, location, ratioB, null);
        checkServiceRatio(product1, null, location, ratioC, calendar);
        checkServiceRatio(product2, null, location, ratioD, null);
    }

    /**
     * Tests the {@link ProductPriceRules#copy(ProductPrice, Date)} method.
     */
    @Test
    public void testCopy() {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");
        BigDecimal price = BigDecimal.TEN;
        BigDecimal cost = BigDecimal.ONE;
        BigDecimal markup = BigDecimal.valueOf(900);
        BigDecimal maxDiscount = BigDecimal.valueOf(90);
        Date fromDate = getYesterday();
        Date toDate = getTomorrow();
        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .allDates()
                .defaultPrice(true)
                .notes("notes")
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(fromDate, toDate)
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();

        ProductPrice unit1 = productFactory.newUnitPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .allDates()
                .pricingGroups(groupA)
                .build();
        ProductPrice unit2 = productFactory.newUnitPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(fromDate, toDate)
                .pricingGroups(groupB)
                .build();

        productFactory.newMedication()
                .addPrices(fixed1, fixed2, unit1, unit2)
                .build();

        checkCopy(fixed1);
        checkCopy(fixed2);
        checkCopy(unit1);
        checkCopy(unit2);
    }

    /**
     * Tests the {@link ProductPriceRules#copyAndClose(ProductPrice, Date)} method.
     */
    @Test
    public void testCopyAndClose() {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("A");
        BigDecimal price = BigDecimal.TEN;
        BigDecimal cost = BigDecimal.ONE;
        BigDecimal markup = BigDecimal.valueOf(900);
        BigDecimal maxDiscount = BigDecimal.valueOf(90);
        Date fromDate = getYesterday();
        Date toDate = getTomorrow();
        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .allDates()
                .defaultPrice(true)
                .pricingGroups(groupA)
                .notes("Some notes")
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(fromDate, toDate)
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();

        ProductPrice unit1 = productFactory.newUnitPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .allDates()
                .pricingGroups(groupA)
                .build();
        ProductPrice unit2 = productFactory.newUnitPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .dateRange(fromDate, toDate)
                .pricingGroups(groupB)
                .build();

        productFactory.newMedication()
                .addPrices(fixed1, fixed2, unit1, unit2)
                .build();

        checkCopyAndClose(fixed1);
        checkCopyAndClose(fixed2);
        checkCopyAndClose(unit1);
        checkCopyAndClose(unit2);
    }

    /**
     * Verifies there is no service ratio for the specified product, department and location.
     *
     * @param product    the product
     * @param department the department. May be {@code null}
     * @param location   the practice location
     */
    private void checkNoServiceRatio(Product product, Entity department, Party location) {
        checkServiceRatio(product, department, location, null, null);
    }

    /**
     * Checks the service ratio for a product, department and location
     *
     * @param product    the product
     * @param department the department. May be {@code null}
     * @param location   the practice location
     * @param ratio      the expected ratio. May be {@code null}
     * @param calendar   the expected calendar. May be {@code null}
     */
    private void checkServiceRatio(Product product, Entity department, Party location, BigDecimal ratio,
                                   Entity calendar) {
        ServiceRatio actual = rules.getServiceRatio(product, department, location);
        if (ratio == null) {
            assertNull(actual);
            assertNull(calendar);
        } else {
            checkEquals(ratio, actual.getRatio());
            if (calendar != null) {
                assertEquals(calendar.getObjectReference(), actual.getCalendar());
            } else {
                assertNull(actual.getCalendar());
            }
        }
    }

    /**
     * Tests the {@link ProductPriceRules#copy(ProductPrice, Date)} method.
     *
     * @param existing the existing price
     */
    private void checkCopy(ProductPrice existing) {
        IMObjectBean bean = getBean(existing);
        Lookup group = bean.getObject("pricingGroups", Lookup.class); // only support a single group for this test
        Date from = existing.getFromDate();
        Date to = existing.getToDate();
        BigDecimal price = existing.getPrice();
        BigDecimal cost = bean.getBigDecimal("cost");
        BigDecimal markup = bean.getBigDecimal("markup");
        BigDecimal maxDiscount = bean.getBigDecimal("maxDiscount");

        Date tomorrow = DateRules.getTomorrow();
        ProductPrice copy = rules.copy(existing, tomorrow);
        assertTrue(copy.isNew());
        assertEquals(existing.getArchetype(), copy.getArchetype());
        assertNull(copy.getProduct());

        // verify the existing price matches that expected
        ProductPriceTestHelper.checkPrice(existing, price, cost, markup, maxDiscount, from, to, group);

        // verify the new price matches that expected, but that the from-date is tomorrow
        ProductPriceTestHelper.checkPrice(copy, price, cost, markup, maxDiscount, tomorrow, null, group);

        IMObjectBean copyBean = getBean(copy);
        assertEquals(bean.getBoolean("fixed"), copyBean.getBoolean("fixed"));
        assertNull(copyBean.getString("notes"));  // notes not copied
    }

    /**
     * Tests the {@link ProductPriceRules#copyAndClose(ProductPrice, Date)} method.
     *
     * @param existing the existing price
     */
    private void checkCopyAndClose(ProductPrice existing) {
        IMObjectBean bean = getBean(existing);
        Lookup group = bean.getObject("pricingGroups", Lookup.class); // only support a single group for this test
        Date from = existing.getFromDate();
        BigDecimal price = existing.getPrice();
        BigDecimal cost = bean.getBigDecimal("cost");
        BigDecimal markup = bean.getBigDecimal("markup");
        BigDecimal maxDiscount = bean.getBigDecimal("maxDiscount");

        Date tomorrow = DateRules.getTomorrow();
        ProductPrice copy = rules.copyAndClose(existing, tomorrow);
        assertTrue(copy.isNew());
        assertEquals(existing.getArchetype(), copy.getArchetype());
        assertNull(copy.getProduct());

        // verify the existing price matches that expected, but that the to-date is now tomorrow
        ProductPriceTestHelper.checkPrice(existing, price, cost, markup, maxDiscount, from, tomorrow, group);

        // verify the new price matches that expected, but that the from-date is tomorrow
        ProductPriceTestHelper.checkPrice(copy, price, cost, markup, maxDiscount, tomorrow, null, group);

        IMObjectBean copyBean = getBean(copy);
        assertEquals(bean.getBoolean("fixed"), copyBean.getBoolean("fixed"));
        assertNull(copyBean.getString("notes"));  // notes not copied
    }

    /**
     * Verifies that there is price in a collection.
     *
     * @param prices       the prices to check
     * @param cost         the expected cost price
     * @param markup       the expected markup
     * @param price        the expected price
     * @param maxDiscount  the expected max discount
     * @param from         the expected from date
     * @param to           the expected to date
     * @param pricingGroup the expected pricing group
     * @param expected     the expected product price, if it is persistent, or {@code null} if not
     * @return the matching price
     */
    private ProductPrice checkPrice(List<ProductPrice> prices, BigDecimal cost, BigDecimal markup, BigDecimal price,
                                    BigDecimal maxDiscount, Date from, Date to,
                                    Lookup pricingGroup, ProductPrice expected) {
        ProductPrice match = ProductPriceTestHelper.checkPrice(prices, price, cost, markup, maxDiscount, from, to,
                                                               pricingGroup);
        if (expected != null) {
            assertEquals(expected, match);
        } else {
            assertTrue(match.isNew());
            assertNull(match.getProduct()); // should not be registered with the product
        }
        return match;
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)} method.
     *
     * @param product          the product to use
     * @param usePriceTemplate if {@code true} attach an <em>product.priceTemplate</em> to the product
     */
    private void checkProductPrice(Product product, boolean usePriceTemplate) {
        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-02-01")
                .defaultPrice(false)
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice().dateRange("2008-02-01", "2009-01-01")
                .price(1)
                .defaultPrice(false)
                .build();
        ProductPrice fixed3 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .build();

        ProductPrice unit1 = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .build();
        ProductPrice unit2 = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .build();

        assertNull(rules.getProductPrice(product, FIXED_PRICE, new Date(), null));
        assertNull(rules.getProductPrice(product, UNIT_PRICE, new Date(), null));

        productFactory.updateMedication(product)
                .addPrices(fixed1, fixed2, unit1, unit2)
                .build();

        checkPrice(null, FIXED_PRICE, "2007-01-01", product, null);
        checkPrice(fixed1, FIXED_PRICE, "2008-01-01", product, null);
        checkPrice(fixed1, FIXED_PRICE, "2008-01-31", product, null);
        checkPrice(fixed2, FIXED_PRICE, "2008-02-01", product, null);
        checkPrice(fixed2, FIXED_PRICE, "2008-12-31", product, null);
        checkPrice(null, FIXED_PRICE, "2009-01-01", product, null);

        checkPrice(null, UNIT_PRICE, "2007-12-31", product, null);
        checkPrice(unit1, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(unit1, UNIT_PRICE, "2008-01-10", product, null);
        checkPrice(null, UNIT_PRICE, "2008-01-11", product, null);
        checkPrice(unit2, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(unit2, UNIT_PRICE, "2010-02-01", product, null); // unbounded

        if (usePriceTemplate) {
            // verify that linked products are used if there are no matching prices for the date
            Product priceTemplate = productFactory.newPriceTemplate()
                    .addPrices(fixed3)
                    .build();

            productFactory.updateMedication(product)
                    .addPriceTemplate(priceTemplate, "2008-01-01", null)
                    .build();

            checkPrice(fixed2, FIXED_PRICE, "2008-02-01", product, null);

            // fixed3 overrides fixed2 as it is the default
            checkPrice(fixed3, FIXED_PRICE, "2008-03-01", product, null);

            // now deactivate the template and verify the price is no longer returned
            priceTemplate.setActive(false);
            save(priceTemplate);
            checkPrice(fixed2, FIXED_PRICE, "2008-03-01", product, null);
        }
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)} method
     * when a product has prices with different pricing groups.
     *
     * @param product          the product to use
     * @param usePriceTemplate if {@code true} attach an <em>product.priceTemplate</em> to the product
     */
    private void checkProductPriceWithPriceGroups(Product product, boolean usePriceTemplate) {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");

        ProductPrice fixed1A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-02-01")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed1B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-02-01")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed1C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-02-01")
                .defaultPrice(false)
                .build();

        ProductPrice fixed2A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed2B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed2C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .build();

        ProductPrice fixed3A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-03-01", null)
                .defaultPrice(true).pricingGroups(groupA)
                .build();
        ProductPrice fixed3B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-03-01", null)
                .defaultPrice(true).pricingGroups(groupB)
                .build();
        ProductPrice fixed3C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .build();

        ProductPrice unit1A = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .pricingGroups(groupA)
                .build();
        ProductPrice unit1B = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .pricingGroups(groupB)
                .build();
        ProductPrice unit1C = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .build();

        ProductPrice unit2A = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .pricingGroups(groupA)
                .build();
        ProductPrice unit2B = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .pricingGroups(groupB)
                .build();
        ProductPrice unit2C = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .build();

        assertNull(rules.getProductPrice(product, FIXED_PRICE, new Date(), null));
        assertNull(rules.getProductPrice(product, UNIT_PRICE, new Date(), null));

        product.addProductPrice(fixed1A);
        product.addProductPrice(fixed1B);
        product.addProductPrice(fixed1C);
        product.addProductPrice(fixed2A);
        product.addProductPrice(fixed2B);
        product.addProductPrice(fixed2C);
        product.addProductPrice(unit1A);
        product.addProductPrice(unit1B);
        product.addProductPrice(unit1C);
        product.addProductPrice(unit2A);
        product.addProductPrice(unit2B);
        product.addProductPrice(unit2C);

        checkPrice(null, FIXED_PRICE, "2007-01-01", product, groupA);
        checkPrice(null, FIXED_PRICE, "2007-01-01", product, groupB);
        checkPrice(null, FIXED_PRICE, "2007-01-01", product, null);

        checkPrice(fixed1A, FIXED_PRICE, "2008-01-01", product, groupA);
        checkPrice(fixed1A, FIXED_PRICE, "2008-01-31", product, groupA);
        checkPrice(fixed2A, FIXED_PRICE, "2008-02-01", product, groupA);
        checkPrice(fixed2A, FIXED_PRICE, "2008-12-31", product, groupA);
        checkPrice(null, FIXED_PRICE, "2009-01-01", product, null);

        checkPrice(null, UNIT_PRICE, "2007-12-31", product, null);
        checkPrice(unit1A, UNIT_PRICE, "2008-01-01", product, groupA);
        checkPrice(unit1A, UNIT_PRICE, "2008-01-10", product, groupA);
        checkPrice(unit1B, UNIT_PRICE, "2008-01-01", product, groupB);
        checkPrice(unit1B, UNIT_PRICE, "2008-01-10", product, groupB);
        checkPrice(unit1C, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(unit1C, UNIT_PRICE, "2008-01-10", product, null);

        checkPrice(null, UNIT_PRICE, "2008-01-11", product, null);
        checkPrice(unit2A, UNIT_PRICE, "2008-02-01", product, groupA);
        checkPrice(unit2A, UNIT_PRICE, "2010-02-01", product, groupA); // unbounded
        checkPrice(unit2B, UNIT_PRICE, "2008-02-01", product, groupB);
        checkPrice(unit2B, UNIT_PRICE, "2010-02-01", product, groupB); // unbounded
        checkPrice(unit2C, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(unit2C, UNIT_PRICE, "2010-02-01", product, null); // unbounded

        if (usePriceTemplate) {
            // verify that linked products are used if there are no matching prices for the date
            Product priceTemplate = productFactory.newPriceTemplate()
                    .addPrices(fixed3A, fixed3B, fixed3C)
                    .build();

            productFactory.updateProduct(product)
                    .addPriceTemplate(priceTemplate, "2008-01-01", null)
                    .build();

            checkPrice(fixed2A, FIXED_PRICE, "2008-02-01", product, groupA);

            // fixed3 overrides fixed2 as it is the default
            checkPrice(fixed3A, FIXED_PRICE, "2008-03-01", product, groupA);
            checkPrice(fixed3B, FIXED_PRICE, "2008-03-01", product, groupB);
            checkPrice(fixed3C, FIXED_PRICE, "2008-03-01", product, null);

            // now deactivate the template and verify its prices are no longer returned
            priceTemplate.setActive(false);
            save(priceTemplate);
            checkPrice(fixed2A, FIXED_PRICE, "2008-03-01", product, groupA);
        }
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method.
     *
     * @param product          the product to test
     * @param usePriceTemplate if {@code true} attach an <em>product.priceTemplate</em> to the product
     */
    private void checkGetProductPriceForPrice(Product product, boolean usePriceTemplate) {
        BigDecimal one = BigDecimal.ONE;
        BigDecimal two = new BigDecimal("2.0");
        BigDecimal three = new BigDecimal("3.0");

        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31 10:00:00")
                .defaultPrice(false)
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice()
                .price(2)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .build();
        ProductPrice fixed3 = productFactory.newFixedPrice()
                .price(3)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .build();

        ProductPrice unit1 = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .build();
        ProductPrice unit2 = productFactory.newUnitPrice()
                .price(2)
                .dateRange("2008-02-01", null)
                .build();

        // should be no prices returned until one is registered
        assertNull(rules.getProductPrice(product, one, FIXED_PRICE, new Date(), null));
        assertNull(rules.getProductPrice(product, one, UNIT_PRICE, new Date(), null));

        // add prices
        product.addProductPrice(fixed1);
        product.addProductPrice(fixed2);
        product.addProductPrice(unit1);
        product.addProductPrice(unit2);

        checkPrice(null, two, FIXED_PRICE, "2008-01-01", product, null);
        checkPrice(fixed1, one, FIXED_PRICE, "2008-01-01", product, null);
        checkPrice(null, two, FIXED_PRICE, "2008-01-31", product, null);
        checkPrice(fixed1, one, FIXED_PRICE, "2008-01-31", product, null);
        checkPrice(null, one, FIXED_PRICE, "2008-02-01", product, null);

        // verify time is ignored
        checkPrice(fixed2, two, FIXED_PRICE, getDatetime("2008-12-31 23:45:00"), product, null);

        checkPrice(null, two, FIXED_PRICE, "2009-01-01", product, null);

        checkPrice(null, one, UNIT_PRICE, "2007-12-31", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(unit1, one, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-10", product, null);
        checkPrice(unit1, one, UNIT_PRICE, "2008-01-10", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-11", product, null);
        checkPrice(null, three, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(unit2, two, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(null, three, UNIT_PRICE, "2010-02-01", product, null);
        checkPrice(unit2, two, UNIT_PRICE, "2010-02-01", product, null); // unbounded

        if (usePriceTemplate) {
            // verify that linked products are used if there are no matching prices
            // for the date
            Product priceTemplate = productFactory.newPriceTemplate()
                    .addPrice(fixed3)
                    .build();

            productFactory.updateProduct(product)
                    .addPriceTemplate(priceTemplate, "2008-01-01", null)
                    .build();

            checkPrice(fixed2, two, FIXED_PRICE, "2008-02-01", product, null);
            checkPrice(fixed3, three, FIXED_PRICE, "2008-03-01", product, null);
            checkPrice(fixed2, two, FIXED_PRICE, "2008-03-01", product, null);

            // now deactivate the template and verify its prices are no longer returned
            priceTemplate.setActive(false);
            save(priceTemplate);
            checkPrice(null, three, FIXED_PRICE, "2008-03-01", product, null);
        }
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrice(Product, BigDecimal, String, Date, Lookup)}  method
     * when a product has prices with different pricing groups.
     *
     * @param product          the product to test
     * @param usePriceTemplate if {@code true} attach an <em>product.priceTemplate</em> to the product
     */
    private void checkGetProductPriceForPriceWithPriceGroups(Product product, boolean usePriceTemplate) {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");
        BigDecimal one = BigDecimal.ONE;
        BigDecimal two = new BigDecimal("2.0");
        BigDecimal three = new BigDecimal("3.0");

        ProductPrice fixed1A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31 10:00:00")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed1B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31 10:00:00")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed1C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31 10:00:00")
                .defaultPrice(false)
                .build();
        ProductPrice fixed2A = productFactory.newFixedPrice()
                .price(2)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed2B = productFactory.newFixedPrice()
                .price(2)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed2C = productFactory.newFixedPrice()
                .price(2)
                .dateRange("2008-02-01", "2009-01-01")
                .defaultPrice(false)
                .build();
        ProductPrice fixed3A = productFactory.newFixedPrice()
                .price(3)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed3B = productFactory.newFixedPrice()
                .price(3)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed3C = productFactory.newFixedPrice()
                .price(3)
                .dateRange("2008-03-01", null)
                .defaultPrice(true)
                .build();

        ProductPrice unit1A = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .pricingGroups(groupA)
                .build();
        ProductPrice unit1B = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .pricingGroups(groupB)
                .build();
        ProductPrice unit1C = productFactory.newUnitPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-11")
                .build();
        ProductPrice unit2A = productFactory.newUnitPrice()
                .price(2)
                .dateRange("2008-02-01", null)
                .pricingGroups(groupA)
                .build();
        ProductPrice unit2B = productFactory.newUnitPrice()
                .price(2)
                .dateRange("2008-02-01", null)
                .pricingGroups(groupB)
                .build();
        ProductPrice unit2C = productFactory.newUnitPrice()
                .price(2)
                .dateRange("2008-02-01", null)
                .build();

        // should be no prices returned until one is registered
        assertNull(rules.getProductPrice(product, one, FIXED_PRICE, new Date(), null));
        assertNull(rules.getProductPrice(product, one, UNIT_PRICE, new Date(), null));

        // add prices
        product.addProductPrice(fixed1A);
        product.addProductPrice(fixed1B);
        product.addProductPrice(fixed1C);
        product.addProductPrice(fixed2A);
        product.addProductPrice(fixed2B);
        product.addProductPrice(fixed2C);
        product.addProductPrice(unit1A);
        product.addProductPrice(unit1B);
        product.addProductPrice(unit1C);
        product.addProductPrice(unit2A);
        product.addProductPrice(unit2B);
        product.addProductPrice(unit2C);

        checkPrice(null, two, FIXED_PRICE, "2008-01-01", product, null);
        checkPrice(fixed1A, one, FIXED_PRICE, "2008-01-01", product, groupA);
        checkPrice(fixed1B, one, FIXED_PRICE, "2008-01-01", product, groupB);
        checkPrice(fixed1C, one, FIXED_PRICE, "2008-01-01", product, null);
        checkPrice(null, two, FIXED_PRICE, "2008-01-31", product, null);
        checkPrice(fixed1A, one, FIXED_PRICE, "2008-01-31", product, groupA);
        checkPrice(fixed1B, one, FIXED_PRICE, "2008-01-31", product, groupB);
        checkPrice(fixed1C, one, FIXED_PRICE, "2008-01-31", product, null);
        checkPrice(null, one, FIXED_PRICE, "2008-02-01", product, null);

        // verify time is ignored
        checkPrice(fixed2A, two, FIXED_PRICE, getDatetime("2008-12-31 23:45:00"), product, groupA);
        checkPrice(fixed2B, two, FIXED_PRICE, getDatetime("2008-12-31 23:45:00"), product, groupB);
        checkPrice(fixed2C, two, FIXED_PRICE, getDatetime("2008-12-31 23:45:00"), product, null);

        checkPrice(null, two, FIXED_PRICE, "2009-01-01", product, null);

        checkPrice(null, one, UNIT_PRICE, "2007-12-31", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(unit1A, one, UNIT_PRICE, "2008-01-01", product, groupA);
        checkPrice(unit1B, one, UNIT_PRICE, "2008-01-01", product, groupB);
        checkPrice(unit1C, one, UNIT_PRICE, "2008-01-01", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-10", product, null);
        checkPrice(unit1A, one, UNIT_PRICE, "2008-01-10", product, groupA);
        checkPrice(unit1B, one, UNIT_PRICE, "2008-01-10", product, groupB);
        checkPrice(unit1C, one, UNIT_PRICE, "2008-01-10", product, null);
        checkPrice(null, two, UNIT_PRICE, "2008-01-11", product, null);
        checkPrice(null, three, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(unit2A, two, UNIT_PRICE, "2008-02-01", product, groupA);
        checkPrice(unit2B, two, UNIT_PRICE, "2008-02-01", product, groupB);
        checkPrice(unit2C, two, UNIT_PRICE, "2008-02-01", product, null);
        checkPrice(null, three, UNIT_PRICE, "2010-02-01", product, null);
        checkPrice(unit2A, two, UNIT_PRICE, "2010-02-01", product, groupA); // unbounded
        checkPrice(unit2B, two, UNIT_PRICE, "2010-02-01", product, groupB); // unbounded
        checkPrice(unit2C, two, UNIT_PRICE, "2010-02-01", product, null); // unbounded

        if (usePriceTemplate) {
            // verify that linked products are used if there are no matching prices for the date
            Product priceTemplate = productFactory.newPriceTemplate()
                    .addPrices(fixed3A, fixed3B, fixed3C)
                    .build();

            productFactory.updateProduct(product)
                    .addPriceTemplate(priceTemplate, "2008-01-01", null)
                    .build();

            checkPrice(fixed2A, two, FIXED_PRICE, "2008-02-01", product, groupA);
            checkPrice(fixed2B, two, FIXED_PRICE, "2008-02-01", product, groupB);
            checkPrice(fixed2C, two, FIXED_PRICE, "2008-02-01", product, null);
            checkPrice(fixed3A, three, FIXED_PRICE, "2008-03-01", product, groupA);
            checkPrice(fixed3B, three, FIXED_PRICE, "2008-03-01", product, groupB);
            checkPrice(fixed3C, three, FIXED_PRICE, "2008-03-01", product, null);
            checkPrice(fixed2A, two, FIXED_PRICE, "2008-03-01", product, groupA);
            checkPrice(fixed2B, two, FIXED_PRICE, "2008-03-01", product, groupB);
            checkPrice(fixed2C, two, FIXED_PRICE, "2008-03-01", product, null);

            // now deactivate the template and verify its prices are no longer returned
            priceTemplate.setActive(false);
            save(priceTemplate);
            checkPrice(fixed2A, two, FIXED_PRICE, "2008-02-01", product, groupA);
            checkPrice(fixed2B, two, FIXED_PRICE, "2008-02-01", product, groupB);
            checkPrice(fixed2C, two, FIXED_PRICE, "2008-02-01", product, null);
            checkPrice(null, three, FIXED_PRICE, "2008-03-01", product, groupA);
            checkPrice(null, three, FIXED_PRICE, "2008-03-01", product, groupB);
            checkPrice(null, three, FIXED_PRICE, "2008-03-01", product, null);
            checkPrice(fixed2A, two, FIXED_PRICE, "2008-03-01", product, groupA);
            checkPrice(fixed2B, two, FIXED_PRICE, "2008-03-01", product, groupB);
            checkPrice(fixed2C, two, FIXED_PRICE, "2008-03-01", product, null);
        }
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method.
     *
     * @param product the product
     */
    private void checkGetProductPrices(Product product) {
        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .build();

        product.addProductPrice(fixed1);
        product.addProductPrice(fixed2);
        save(product);

        product = get(product);

        List<ProductPrice> prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2007-01-01"), ALL);
        assertTrue(prices.isEmpty());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), ALL);
        assertEquals(2, prices.size());
        assertTrue(prices.contains(fixed1));
        assertTrue(prices.contains(fixed2));

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), ALL);
        assertEquals(1, prices.size());
        assertFalse(prices.contains(fixed1));
        assertTrue(prices.contains(fixed2));

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), ALL);
        assertEquals(0, prices.size());
    }

    /**
     * Tests the {@link ProductPriceRules#getProductPrices} method when a product has prices with different pricing
     * groups.
     *
     * @param product the product
     */
    private void checkGetProductPricesWithPriceGroups(Product product) {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");
        ProductPrice fixed1A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed1B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed1C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .build();
        ProductPrice fixed2A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed2B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed2C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false).
                build();

        product.addProductPrice(fixed1A);
        product.addProductPrice(fixed1B);
        product.addProductPrice(fixed1C);
        product.addProductPrice(fixed2A);
        product.addProductPrice(fixed2B);
        product.addProductPrice(fixed2C);
        save(product);

        product = get(product);

        List<ProductPrice> prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2007-01-01"), ALL);
        assertTrue(prices.isEmpty());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), ALL);
        checkPrices(prices, fixed1A, fixed1B, fixed1C, fixed2A, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed1A, fixed1C, fixed2A, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(groupB));
        checkPrices(prices, fixed1B, fixed1C, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(null));
        checkPrices(prices, fixed1C, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), ALL);
        checkPrices(prices, fixed2A, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed2A, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(groupB));
        checkPrices(prices, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(null));
        checkPrices(prices, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), ALL);
        assertEquals(0, prices.size());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(groupA));
        assertEquals(0, prices.size());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(groupB));
        assertEquals(0, prices.size());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(null));
        assertEquals(0, prices.size());
    }

    /**
     * Checks the {@link ProductPriceRules#getProductPrices(Product, String, Date, PricingGroup)} method for products
     * that may be linked to a price template.
     *
     * @param product the product. Either a medication, merchandise or service
     */
    private void checkGetProductPricesForProductWithPriceTemplate(Product product) {
        ProductPrice fixed1 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .build();
        ProductPrice fixed2 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .build();
        ProductPrice fixed3 = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .build();

        product.addProductPrice(fixed1);
        product.addProductPrice(fixed2);

        Product priceTemplate = productFactory.newPriceTemplate()
                .addPrice(fixed3)
                .build();

        productFactory.updateProduct(product)
                .addPriceTemplate(priceTemplate, "2008-01-01", null)
                .build();

        List<ProductPrice> prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2007-01-01"), ALL);
        assertTrue(prices.isEmpty());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), ALL);
        assertEquals(2, prices.size());
        assertTrue(prices.contains(fixed1));
        assertTrue(prices.contains(fixed2));
        assertFalse(prices.contains(fixed3));

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), ALL);
        assertEquals(2, prices.size());
        assertFalse(prices.contains(fixed1));
        assertTrue(prices.contains(fixed2));
        assertTrue(prices.contains(fixed3));

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), ALL);
        assertEquals(1, prices.size());
        assertFalse(prices.contains(fixed1));
        assertFalse(prices.contains(fixed2));
        assertTrue(prices.contains(fixed3));

        // now deactivate the template and verify the prices are no longer returned
        priceTemplate.setActive(false);
        save(priceTemplate);
        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), ALL);
        assertEquals(0, prices.size());
    }

    /**
     * Checks the {@link ProductPriceRules#getProductPrices(Product, String, Date, PricingGroup)} method for products
     * that may be linked to a price template.
     *
     * @param product the product. Either a medication, merchandise or service
     */
    private void checkGetProductPricesWithPriceGroupsForProductWithPriceTemplate(Product product) {
        Lookup groupA = productFactory.createPricingGroup("A");
        Lookup groupB = productFactory.createPricingGroup("B");

        ProductPrice fixed1A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed1B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed1C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-01-31")
                .defaultPrice(false)
                .build();

        ProductPrice fixed2A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed2B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed2C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-01-01", "2008-12-31")
                .defaultPrice(false)
                .build();

        ProductPrice fixed3A = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .defaultPrice(false)
                .pricingGroups(groupA)
                .build();
        ProductPrice fixed3B = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .defaultPrice(false)
                .pricingGroups(groupB)
                .build();
        ProductPrice fixed3C = productFactory.newFixedPrice()
                .price(1)
                .dateRange("2008-02-01", null)
                .defaultPrice(false)
                .build();

        Product priceTemplate = productFactory.newPriceTemplate()
                .addPrices(fixed3A, fixed3B, fixed3C)
                .build();

        productFactory.updateProduct(product)
                .addPrices(fixed1A, fixed1B, fixed1C, fixed2A, fixed2B, fixed2C)
                .addPriceTemplate(priceTemplate, "2008-01-01", null)
                .build();

        product = get(product);

        List<ProductPrice> prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2007-01-01"), ALL);
        assertTrue(prices.isEmpty());

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), ALL);
        checkPrices(prices, fixed1A, fixed1B, fixed1C, fixed2A, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed1A, fixed1C, fixed2A, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(groupB));
        checkPrices(prices, fixed1B, fixed1C, fixed2B, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-01-01"), new PricingGroup(null));
        checkPrices(prices, fixed1C, fixed2C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), ALL);
        checkPrices(prices, fixed2A, fixed2B, fixed2C, fixed3A, fixed3B, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed2A, fixed2C, fixed3A, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(groupB));
        checkPrices(prices, fixed2B, fixed2C, fixed3B, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(null));
        checkPrices(prices, fixed2C, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), ALL);
        checkPrices(prices, fixed3A, fixed3B, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed3A, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(groupB));
        checkPrices(prices, fixed3B, fixed3C);

        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2009-01-01"), new PricingGroup(null));
        checkPrices(prices, fixed3C);

        // now deactivate the template and verify the prices are no longer returned
        priceTemplate.setActive(false);
        save(priceTemplate);
        prices = rules.getProductPrices(product, FIXED_PRICE, getDate("2008-02-01"), new PricingGroup(groupA));
        checkPrices(prices, fixed2A, fixed2C);
    }

    /**
     * Tests the {@link ProductPriceRules#getTaxIncPrice(BigDecimal, Product, Party, Currency)} method.
     *
     * @param taxExPrice  the tax-exclusive price
     * @param taxIncPrice the expected tax-inclusive price
     * @param product     the product, to determine tax rates
     * @param currency    the currency, for rounding
     */
    private void checkGetTaxIncPrice(String taxExPrice, String taxIncPrice, Product product, Currency currency) {
        BigDecimal price = rules.getTaxIncPrice(new BigDecimal(taxExPrice), product, practice, currency);
        checkEquals(new BigDecimal(taxIncPrice), price);
    }

    /**
     * Checks prices against those expected.
     *
     * @param expected the expected prices
     * @param prices   the actual prices
     */
    private void checkPrices(List<ProductPrice> expected, ProductPrice... prices) {
        assertEquals(expected.size(), prices.length);
        for (ProductPrice price : prices) {
            assertTrue(expected.contains(price));
        }
    }

    /**
     * Helper to create a new medication product.
     *
     * @return a new medication product
     */
    private Product createMedication() {
        return productFactory.newMedication().build(false);
    }

    /**
     * Helper to create a new merchandise product.
     *
     * @return a new merchandise product
     */
    private Product createMerchandise() {
        return productFactory.newMerchandise().build(false);
    }

    /**
     * Helper to create a new service product.
     *
     * @return a new service product
     */
    private Product createService() {
        return productFactory.newService().build(false);
    }

    /**
     * Helper to create a new template product.
     *
     * @return a new template product
     */
    private Product createTemplate() {
        return productFactory.newTemplate().build(false);
    }

    /**
     * Helper to create a new price template product.
     *
     * @return a new price template product
     */
    private Product createPriceTemplate() {
        return productFactory.newPriceTemplate().build(false);
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param expected     the expected price
     * @param shortName    the price short name
     * @param date         the date that the price applies to
     * @param product      the product to use
     * @param pricingGroup the pricing group. May be {@code null}
     */
    private void checkPrice(ProductPrice expected, String shortName, String date, Product product,
                            Lookup pricingGroup) {
        checkPrice(expected, shortName, getDate(date), product, pricingGroup);
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param expected     the expected price
     * @param shortName    the price short name
     * @param date         the date that the price applies to
     * @param product      the product to use
     * @param pricingGroup the pricing group. May be {@code null}
     */
    private void checkPrice(ProductPrice expected, String shortName, Date date, Product product, Lookup pricingGroup) {
        assertEquals(expected, rules.getProductPrice(product, shortName, date, pricingGroup));
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param expected     the expected price
     * @param price        the price
     * @param shortName    the price short name
     * @param date         the date that the price applies to
     * @param product      the product to use
     * @param pricingGroup the pricing group. May be {@code null}
     */
    private void checkPrice(ProductPrice expected, BigDecimal price, String shortName, String date, Product product,
                            Lookup pricingGroup) {
        checkPrice(expected, price, shortName, getDate(date), product, pricingGroup);
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param expected     the expected price
     * @param price        the price
     * @param shortName    the price short name
     * @param date         the date that the price applies to
     * @param product      the product to use
     * @param pricingGroup the pricing group. May be {@code null}
     */
    private void checkPrice(ProductPrice expected, BigDecimal price, String shortName, Date date, Product product,
                            Lookup pricingGroup) {
        assertEquals(expected, rules.getProductPrice(product, price, shortName, date, pricingGroup));
    }

}
