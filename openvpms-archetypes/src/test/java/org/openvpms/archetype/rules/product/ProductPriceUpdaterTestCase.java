/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.openvpms.archetype.rules.math.MathRules.ONE_HUNDRED;
import static org.openvpms.archetype.rules.product.ProductPriceTestHelper.checkToDateSet;
import static org.openvpms.archetype.rules.product.ProductPriceTestHelper.createUnitPrice;
import static org.openvpms.archetype.rules.util.DateRules.getToday;
import static org.openvpms.archetype.rules.util.DateRules.getTomorrow;
import static org.openvpms.archetype.rules.util.DateRules.getYesterday;


/**
 * Tests the {@link ProductPriceUpdater} class.
 *
 * @author Tim Anderson
 */
public class ProductPriceUpdaterTestCase extends AbstractProductTest {

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The price rules.
     */
    @Autowired
    private ProductPriceRules priceRules;

    /**
     * The product price updater.
     */
    private ProductPriceUpdater updater;

    /**
     * Package units.
     */
    private static final String PACKAGE_UNITS = "BOX";

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        TestHelper.getPractice(); // make sure the practice exists and has no taxes

        TestHelper.getLookup("lookup.uom", PACKAGE_UNITS);
        updater = new ProductPriceUpdater(priceRules, practiceRules, getArchetypeService());
    }

    /**
     * Tests the {@link ProductPriceUpdater#update(Product)} for a medication.
     */
    @Test
    public void testUpdateForMedication() {
        Product product = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        checkUpdate(product);
    }

    /**
     * Tests the {@link ProductPriceUpdater#update(Product)} for a merchandise.
     */
    @Test
    public void testUpdateForMerchandise() {
        Product product = TestHelper.createProduct(ProductArchetypes.MERCHANDISE, null);
        checkUpdate(product);
    }

    /**
     * Verifies that a product can be saved with a custom unit price
     * i.e the unit price doesn't get overwritten when the product is saved despite having an auto-update
     * product-supplier relationship.
     */
    @Test
    public void testCustomUnitPrice() {
        Product product = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        Party supplier = TestHelper.createSupplier();

        // add a new price
        ProductPrice price1 = addUnitPrice(product, ONE, ZERO);

        // create a product-supplier relationship to trigger auto price updates
        Date now = new Date();
        int packageSize = 30;
        addProductSupplier(product, supplier, packageSize, "10.00", "20.00", true);
        updater.update(product);
        save(product);

        // verify price1 closed off
        Date toDate1 = checkToDateSet(price1, now);

        // verify expected prices are present
        product = get(product);
        assertEquals(2, product.getProductPrices().size());

        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price1.getFromDate(), toDate1);
        ProductPrice price2 = ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.34"),
                                                                new BigDecimal("0.67"), toDate1, null);

        // now save with a custom unit price, and verify it doesn't get overwritten
        price2.setPrice(new BigDecimal("1.35"));
        updater.update(product);
        save(product);

        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price1.getFromDate(), toDate1);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.35"), new BigDecimal("0.67"),
                                          toDate1, null);
    }

    /**
     * Tests update when a newly created product is saved with a relationship to an existing supplier.
     */
    @Test
    public void testUpdateWithPriceHistory() {
        BigDecimal cost = ONE;
        BigDecimal markup = BigDecimal.valueOf(100);
        BigDecimal price = BigDecimal.valueOf(2);
        BigDecimal maxDiscount = BigDecimal.valueOf(100);

        Product product = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        Party supplier = TestHelper.createSupplier();

        // add some prices
        ProductPrice unit1 = createUnitPrice(price, cost, markup, maxDiscount, null, getYesterday()); // inactive
        ProductPrice unit2 = createUnitPrice(price, cost, markup, maxDiscount, getToday(), null);     // active
        ProductPrice unit3 = createUnitPrice(price, cost, markup, maxDiscount, (Date) null, null);    // active
        ProductPrice unit4 = createUnitPrice(price, cost, markup, maxDiscount, getTomorrow(), null);  // inactive
        product.addProductPrice(unit1);
        product.addProductPrice(unit2);
        product.addProductPrice(unit3);
        product.addProductPrice(unit4);

        // create a product-supplier relationship.
        int packageSize = 30;
        ProductRules rules = new ProductRules(getArchetypeService(), getLookupService());
        ProductSupplier ps = rules.createProductSupplier(product, supplier);
        ps.setPackageUnits(PACKAGE_UNITS);
        ps.setPackageSize(packageSize);
        ps.setAutoPriceUpdate(true);
        ps.setNettPrice(new BigDecimal("10.00"));
        ps.setListPrice(new BigDecimal("20.00"));
        updater.update(product);

        // verify that the expected prices have updated
        BigDecimal newCost = new BigDecimal("0.67");
        BigDecimal newPrice = new BigDecimal("1.34");
        ProductPriceTestHelper.checkPrice(unit1, price, cost);              // inactive, so shouldn't update
        ProductPriceTestHelper.checkPrice(unit2, newPrice, newCost);
        ProductPriceTestHelper.checkPrice(unit3, newPrice, newCost);
        ProductPriceTestHelper.checkPrice(unit4, price, cost);              // inactive, so shouldn't update
    }

    /**
     * Verifies that when a product has multiple active unit prices with different pricing groups,
     * each is updated when the product-supplier relationship is saved.
     */
    @Test
    public void testPricingGroups() {
        Product product = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);

        // add prices
        ProductPrice price1 = addUnitPrice(product, "1", "100", "2", "GROUP1");
        ProductPrice price2 = addUnitPrice(product, "1", "200", "3", "GROUP2");
        ProductPrice price3 = addUnitPrice(product, "1", "300", "4", null);
        ProductPrice price4 = addUnitPrice(product, "1", "400", "5", null);
        price4.setToDate(DateRules.getYesterday()); // now inactive

        Party supplier = TestHelper.createSupplier();
        int packageSize = 30;
        addProductSupplier(product, supplier, packageSize, "10.00", "20.00", true);
        updater.update(product);
        save(product);

        price1 = get(price1);
        price2 = get(price2);
        price3 = get(price3);
        price4 = get(price4);

        ProductPriceTestHelper.checkPrice(price1, new BigDecimal("1.34"), new BigDecimal("0.67"));
        ProductPriceTestHelper.checkPrice(price2, new BigDecimal("2.01"), new BigDecimal("0.67"));
        ProductPriceTestHelper.checkPrice(price3, new BigDecimal("2.68"), new BigDecimal("0.67"));
        ProductPriceTestHelper.checkPrice(price4, new BigDecimal("5"), new BigDecimal("1"));
    }

    /**
     * Verifies that if the currency has a minimum price, prices are NOT rounded to it.
     * <p/>
     * The minimum price only takes effect when calculating the tax-inclusive price? TODO
     */
    @Test
    public void testMinPrice() {
        Product product1 = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        Product product2 = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        ProductPrice price1 = addUnitPrice(product1, ONE, ZERO);
        ProductPrice price2 = addUnitPrice(product2, ONE, ZERO);
        save(product1);
        save(product2);

        Date now1 = new Date();
        Party supplier = TestHelper.createSupplier();
        addProductSupplier(product1, supplier, 30, "10.00", "20.00", true);
        updater.update(product1);
        save(product1);

        product1 = get(product1);

        // verify price1 closed off
        Date toDate1 = checkToDateSet(price1, now1);

        // verify expected prices are present, rounded to 1 cent
        ProductPriceTestHelper.checkPrice(product1.getProductPrices(), ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED,
                                          price1.getFromDate(), toDate1, null);
        ProductPriceTestHelper.checkPrice(product1.getProductPrices(), new BigDecimal("1.34"), new BigDecimal("0.67"),
                                          ONE_HUNDRED, ONE_HUNDRED, toDate1, null, null);

        // change currency to round prices to nearest 20 cents.
        Lookup lookup = TestHelper.getCurrency("AUD");
        IMObjectBean bean = getBean(lookup);
        bean.setValue("minPrice", new BigDecimal("0.20"));
        bean.save();

        Date now2 = new Date();
        addProductSupplier(product2, supplier, 30, "12.00", "22.00", true);
        updater.update(product2);
        save(product2);

        product2 = get(product2);

        // verify price2 closed off
        Date toDate2 = checkToDateSet(price2, now2);

        // verify expected prices are present, not rounded to 20 cents
        ProductPriceTestHelper.checkPrice(product2.getProductPrices(), ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED,
                                          price2.getFromDate(), toDate2, null);
        ProductPriceTestHelper.checkPrice(product2.getProductPrices(), new BigDecimal("1.46"), new BigDecimal("0.73"),
                                          ONE_HUNDRED, ONE_HUNDRED, toDate2, null, null);
    }

    /**
     * Verifies that if a product-supplier relationship has a null list price, prices don't update.
     */
    @Test
    public void testSaveProductSupplierWithNullListPrice() {
        BigDecimal cost = ONE;
        BigDecimal markup = BigDecimal.valueOf(100);
        BigDecimal price = BigDecimal.valueOf(2);
        BigDecimal maxDiscount = BigDecimal.valueOf(100);

        Product product = TestHelper.createProduct(ProductArchetypes.MEDICATION, null);
        Party supplier = TestHelper.createSupplier();

        // add some prices
        Date unit1ToDate = getYesterday();
        Date unit2FromDate = getToday();
        Date unit3FromDate = getTomorrow();
        ProductPrice unit1 = createUnitPrice(price, cost, markup, maxDiscount, null, unit1ToDate);    // inactive
        ProductPrice unit2 = createUnitPrice(price, cost, markup, maxDiscount, unit2FromDate, null);  // active
        ProductPrice unit3 = createUnitPrice(price, cost, markup, maxDiscount, unit3FromDate, null);  // inactive
        product.addProductPrice(unit1);
        product.addProductPrice(unit2);
        product.addProductPrice(unit3);

        // create a product-supplier relationship.
        int packageSize = 30;
        ProductRules rules = new ProductRules(getArchetypeService(), getLookupService());
        ProductSupplier ps = rules.createProductSupplier(product, supplier);
        ps.setPackageUnits(PACKAGE_UNITS);
        ps.setPackageSize(packageSize);
        ps.setAutoPriceUpdate(true);
        ps.setNettPrice(new BigDecimal("10.00"));
        ps.setListPrice(new BigDecimal("20.00"));
        updater.update(product);
        save(product);

        // verify that the expected prices have updated. Only unit2 will update. If it was  saved, it would be
        // closed off, and a new price added
        product = get(product);
        BigDecimal newCost1 = new BigDecimal("0.67");
        BigDecimal newPrice1 = new BigDecimal("1.34");

        assertEquals(3, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, null, unit1ToDate);
        // unit1 - inactive, so shouldn't update

        ProductPriceTestHelper.checkPrice(product.getProductPrices(), newPrice1, newCost1, unit2FromDate, null);
        // unit2 - active, so should update

        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, unit3FromDate, null);
        // unit3 - inactive, so shouldn't update

        // now update the product supplier relationship, this time setting the list price to null.
        ps = getProductSupplier(product, ps);
        ps.setListPrice(null);
        updater.update(product);
        save(product);

        // prices should be the same
        product = get(product);
        assertEquals(3, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, null, unit1ToDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), newPrice1, newCost1, unit2FromDate, null);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, unit3FromDate, null);

        // now update the list price to a valid value
        Date now = new Date();
        ps = getProductSupplier(product, ps);
        ps.setListPrice(new BigDecimal("22.00"));
        updater.update(product);
        save(product);

        // unit2 should be closed off, and a new price added
        BigDecimal newCost2 = new BigDecimal("0.73");
        BigDecimal newPrice2 = new BigDecimal("1.46");

        product = get(product);
        assertEquals(4, product.getProductPrices().size());

        Date unit2ToDate = checkToDateSet(get(unit2), now);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, null, unit1ToDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), newPrice1, newCost1, unit2FromDate, unit2ToDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost, unit3FromDate, null);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), newPrice2, newCost2, unit2ToDate, null);
    }

    /**
     * Tests the {@link ProductPriceUpdater#update(Product, ProductSupplier, boolean, boolean)} method.
     */
    @Test
    public void testUpdateForProductSupplier() {
        Product product = TestHelper.createProduct();
        Party supplier1 = TestHelper.createSupplier();
        Party supplier2 = TestHelper.createSupplier();
        BigDecimal initialCost = BigDecimal.ZERO;
        BigDecimal initialPrice = ONE;

        // add a new price
        ProductPrice price = addUnitPrice(product, initialPrice, initialCost);

        // add a product-supplier relationship for supplier1
        ProductSupplier ps1 = addProductSupplier(product, supplier1);
        ps1.setAutoPriceUpdate(true);
        ps1.setPackageSize(10);
        ps1.setListPrice(new BigDecimal("15.00"));

        // add a product-supplier relationship with autoPriceUpdate=false for supplier2 and update using the
        // relationship. No update should occur
        int packageSize = 30;
        ProductSupplier ps2 = addProductSupplier(product, supplier2);
        ps2.setPackageSize(packageSize);
        assertFalse(ps2.isAutoPriceUpdate());
        updater.update(product, ps2, true);

        assertEquals(1, product.getProductPrices().size());
        checkPrice(product, initialCost, initialPrice);

        // set the product-supplier relationship to auto update prices
        Date now = new Date();
        ps2.setListPrice(new BigDecimal("20.00"));
        ps2.setAutoPriceUpdate(true);

        // re-run the update
        updater.update(product, ps2, true);

        // verify that the original price has been closed off
        Date toDate = checkToDateSet(price, now);

        // verify the expected prices are present
        save(product);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price.getFromDate(), toDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.34"), new BigDecimal("0.67"),
                                          toDate, null);

        // now deactivate the supplier to prevent price updates
        supplier2.setActive(false);
        save(supplier2);

        // now change the net and list price
        ps2.setNettPrice(new BigDecimal("15.00"));
        ps2.setListPrice(new BigDecimal("30.00"));

        // run the update  and verify that the prices haven't updated
        updater.update(product, ps2, true);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price.getFromDate(), toDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.34"), new BigDecimal("0.67"),
                                          toDate, null);
    }

    /**
     * Helper to reload a product-supplier relationship from a product.
     *
     * @param product  the product
     * @param existing the relationship
     * @return the reloaded relationship
     */
    private ProductSupplier getProductSupplier(Product product, ProductSupplier existing) {
        IMObjectBean bean = getBean(product);
        Relationship result = bean.getValue("suppliers", Relationship.class,
                                            relationship -> existing.getRelationship().equals(relationship));
        assertNotNull(result);
        return new ProductSupplier(result, getArchetypeService());
    }

    /**
     * Adds a unit price to a product.
     *
     * @param product      the product
     * @param cost         the cost
     * @param markup       the markup
     * @param price        the price
     * @param pricingGroup the pricing group
     * @return a new unit price
     */
    private ProductPrice addUnitPrice(Product product, String cost, String markup, String price, String pricingGroup) {
        ProductPrice unit = ProductPriceTestHelper.createUnitPrice(new BigDecimal(price), new BigDecimal(cost),
                                                                   new BigDecimal(markup), BigDecimal.valueOf(100),
                                                                   (Date) null, null);
        if (pricingGroup != null) {
            unit.addClassification(ProductPriceTestHelper.getPricingGroup(pricingGroup));
        }
        product.addProductPrice(unit);
        return unit;
    }

    /**
     * Verifies that product prices update when the associated product is
     * saved and the supplier is active.
     *
     * @param product the product
     */
    private void checkUpdate(Product product) {
        Party supplier = TestHelper.createSupplier();
        BigDecimal initialCost = BigDecimal.ZERO;
        BigDecimal initialPrice = ONE;

        // add a new price
        ProductPrice price = addUnitPrice(product, initialPrice, initialCost);

        // create a product-supplier relationship with autoPriceUpdate=false. The update shouldn't update prices
        int packageSize = 30;
        ProductSupplier ps = addProductSupplier(product, supplier);
        ps.setPackageSize(packageSize);
        assertFalse(ps.isAutoPriceUpdate());
        updater.update(product);

        assertEquals(1, product.getProductPrices().size());
        checkPrice(product, initialCost, initialPrice);

        // set product-supplier relationship to auto update prices
        Date now = new Date();
        ps.setNettPrice(new BigDecimal("10.00"));
        ps.setListPrice(new BigDecimal("20.00"));
        ps.setAutoPriceUpdate(true);

        // re-run the update
        updater.update(product);

        // verify that the original price has been closed off
        Date toDate = checkToDateSet(price, now);

        // verify the expected prices are present
        save(product);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price.getFromDate(), toDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.34"), new BigDecimal("0.67"),
                                          toDate, null);

        // now deactivate the supplier to prevent price updates
        supplier = get(supplier);
        supplier.setActive(false);
        save(supplier);

        // now change the net and list price
        ps.setNettPrice(new BigDecimal("15.00"));
        ps.setListPrice(new BigDecimal("30.00"));

        // run the update  and verify that the prices haven't updated
        updater.update(product);

        save(product);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), ONE, ZERO, price.getFromDate(), toDate);
        ProductPriceTestHelper.checkPrice(product.getProductPrices(), new BigDecimal("1.34"), new BigDecimal("0.67"),
                                          toDate, null);
    }

    /**
     * Verifies that a product has an <em>productPrice.unitPrice</em> with
     * the specified cost and price.
     *
     * @param product the product
     * @param cost    the expected cost
     * @param price   the expected price
     */
    private void checkPrice(Product product, BigDecimal cost, BigDecimal price) {
        product = get(product); // reload product
        Set<ProductPrice> prices = product.getProductPrices();
        assertEquals(1, prices.size());
        ProductPrice p = prices.toArray(new ProductPrice[0])[0];
        IMObjectBean bean = getBean(p);
        checkEquals(cost, bean.getBigDecimal("cost"));
        checkEquals(price, bean.getBigDecimal("price"));
    }

    /**
     * Helper to add a new product supplier relationship.
     *
     * @param product  the product
     * @param supplier the supplier
     * @return the new relationship
     */
    private ProductSupplier addProductSupplier(Product product, Party supplier) {
        ProductRules rules = new ProductRules(getArchetypeService(), getLookupService());
        ProductSupplier ps = rules.createProductSupplier(product, supplier);
        ps.setPackageUnits(PACKAGE_UNITS);
        return ps;
    }

    /**
     * Helper to add a new product supplier relationship.
     *
     * @param product         the product
     * @param supplier        the supplier
     * @param packageSize     the package size
     * @param nettPrice       the nett price
     * @param listPrice       the list price
     * @param autoPriceUpdate if {@code true}, update prices automatically
     * @return the new relationship
     */
    private ProductSupplier addProductSupplier(Product product, Party supplier, int packageSize,
                                               String nettPrice, String listPrice, boolean autoPriceUpdate) {
        ProductSupplier result = addProductSupplier(product, supplier);
        result.setPackageSize(packageSize);
        result.setNettPrice(new BigDecimal(nettPrice));
        result.setListPrice(new BigDecimal(listPrice));
        result.setAutoPriceUpdate(autoPriceUpdate);
        return result;
    }

}
