/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.junit.After;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestLogoBuilder;
import org.openvpms.archetype.test.builder.practice.TestLocationBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link LogoCache}.
 *
 * @author Tim Anderson
 */
public class LogoCacheTestCase extends ArchetypeServiceTest {

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The cache.
     */
    private LogoCache cache;

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        cache.destroy();
    }

    /**
     * Verifies that when a logo is updated, it updates the cache.
     */
    @Test
    public void testUpdateLogo() {
        Party practice = practiceFactory.newPractice()
                .build();
        removeLogos(practice);  // required because there can only be a single practice

        initCache();
        assertNull(cache.getLogo(practice));

        Document image1 = documentFactory.createImage();
        practiceFactory.updatePractice(practice)
                .logo(image1)
                .build();

        DocumentAct logo1 = cache.getLogo(practice);
        assertNotNull(logo1);
        assertEquals(image1.getObjectReference(), logo1.getDocument());

        // update the logo, and verify the cache updates
        Document image2 = documentFactory.createImage();
        logo1.setDocument(image2.getObjectReference());
        save(logo1);

        DocumentAct logo2 = cache.getLogo(practice);
        assertEquals(logo1, logo2);
        assertEquals(image2.getObjectReference(), logo2.getDocument());

        // now delete the logo and verify it is no longer returned by the cache
        removeLogo(logo2);
        assertNull(cache.getLogo(practice));
    }

    /**
     * Verifies that when multiple logos for an entity are available, the one with the lowest id is used.
     */
    @Test
    public void testMultipleLogos() {
        initCache();
        Party location = practiceFactory.newLocation()
                .logo(documentFactory.createImage())
                .build();
        DocumentAct logo1 = cache.getLogo(location);
        assertNotNull(logo1);

        DocumentAct logo2 = new TestLogoBuilder(false, getArchetypeService())
                .logo(documentFactory.createImage())
                .owner(location)
                .build();
        assertEquals(logo1, cache.getLogo(location));

        // now remove logo1 and verify logo2 returned
        removeLogo(logo1);
        assertEquals(logo2, cache.getLogo(location));
    }

    /**
     * Verifies that logos associated with inactive objects aren't loaded by default.
     */
    @Test
    public void testInactive() {
        TestLocationBuilder builder = practiceFactory.newLocation();
        Party location1 = builder.logo(documentFactory.createImage())
                .build();
        DocumentAct logo1 = builder.getLogo();

        Party location2 = builder.logo(documentFactory.createImage())
                .active(false)
                .build();
        DocumentAct logo2 = builder.getLogo();

        initCache();
        assertTrue(cache.getObjects().contains(logo1));
        assertFalse(cache.getObjects().contains(logo2));

        assertEquals(logo1, cache.getLogo(location1));

        // now verify that the logo can be retrieved explicitly for inactive objects, and it updates the cache
        assertEquals(logo2, cache.getLogo(location2));
        assertTrue(cache.getObjects().contains(logo2));
    }

    /**
     * Initialises the cache.
     */
    private void initCache() {
        cache = new LogoCache(getArchetypeService(), documentRules, true);
    }

    /**
     * Removes the logos associated with an entity.
     *
     * @param entity the entity
     */
    private void removeLogos(Entity entity) {
        DocumentAct act;
        while ((act = documentRules.getLogo(entity)) != null) {
            removeLogo(act);
        }
    }

    /**
     * Removes a logo.
     *
     * @param act the logo act
     */
    private void removeLogo(DocumentAct act) {
        Reference document = act.getDocument();
        if (document != null) {
            // need to remove the document after removing the reference to it
            act.setDocument(null);
            save(act);
            getArchetypeService().remove(document);
        }
        remove(act);
    }
}
