/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestLetterheadBuilder;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link DocumentRules} class.
 *
 * @author Tim Anderson
 */
public class DocumentRulesTestCase extends ArchetypeServiceTest {

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules rules;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link DocumentRules#supportsVersions} method.
     */
    @Test
    public void testSupportsVersions() {
        DocumentAct image = create(PatientArchetypes.DOCUMENT_IMAGE, DocumentAct.class);
        assertTrue(rules.supportsVersions(image));

        DocumentAct form = create(PatientArchetypes.DOCUMENT_FORM, DocumentAct.class);
        assertFalse(rules.supportsVersions(form));
    }

    /**
     * Tests the {@link DocumentRules#addDocument} method.
     */
    @Test
    public void testAddDocument() {
        // create an act.patientClinicalEvent and act.patientDocumentImage and add a relationship between them
        Party patient = patientFactory.createPatient();
        Act event = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        IMObjectBean eventBean = getBean(event);
        eventBean.setTarget("patient", patient);
        eventBean.save();

        DocumentAct act = create(PatientArchetypes.DOCUMENT_IMAGE, DocumentAct.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        eventBean.addTarget("items", act, "event");
        save(act, event);

        // now add a document.
        Document document1 = createDocument();
        List<IMObject> objects = rules.addDocument(act, document1);
        save(objects);

        assertEquals(document1.getObjectReference(), act.getDocument());

        Document document2 = createDocument();
        objects = rules.addDocument(act, document2);
        save(objects);

        assertEquals(document2.getObjectReference(), act.getDocument());

        List<DocumentAct> acts = bean.getTargets("versions", DocumentAct.class);
        assertEquals(1, acts.size());
        DocumentAct old = acts.get(0);
        assertEquals(document1.getObjectReference(), old.getDocument());

        // add another document.
        Document document3 = createDocument();
        objects = rules.addDocument(act, document3);
        save(objects);

        // verify the document3 is the latest
        assertEquals(document3.getObjectReference(), act.getDocument());

        // verify document1 and document2 are versioned
        acts = bean.getTargets("versions", DocumentAct.class);
        assertEquals(2, acts.size());
        Set<Reference> docs = new HashSet<>();
        for (DocumentAct version : acts) {
            assertEquals(1, version.getActRelationships().size()); // only one relationship, back to parent
            docs.add(version.getDocument());
        }
        assertTrue(docs.contains(document1.getObjectReference()));
        assertTrue(docs.contains(document2.getObjectReference()));
    }

    /**
     * Tests {@link DocumentRules#addDocument(DocumentAct, Document)} for an investigation.
     */
    @Test
    public void testAddDocumentForInvestigation() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();
        Product product = productFactory.createService();

        Entity laboratory = laboratoryFactory.createLaboratory();
        Entity investigationType = laboratoryFactory.createInvestigationType(laboratory);
        Entity device = laboratoryFactory.createDevice(laboratory);
        Party location = practiceFactory.createLocation();

        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .products(product)
                .laboratory(laboratory)
                .location(location)
                .device(device)
                .clinician(clinician)
                .build();

        Act order = laboratoryRules.createOrder(investigation);

        IMObjectBean bean = getBean(investigation);
        investigation.addIdentity(TestHelper.createActIdentity("actIdentity.laboratoryReportTest", "12456612"));
        Act results = create(InvestigationArchetypes.RESULTS, Act.class);
        IMObjectBean resultsBean = getBean(results);
        resultsBean.setValue("resultsId", "1");
        bean.addTarget("results", results, "investigation");
        save(investigation, results, order);

        patientFactory.newVisit()
                .patient(patient)
                .addItem(investigation)
                .build();

        // now add a document.
        Document document1 = createDocument();
        List<IMObject> objects = rules.addDocument(investigation, document1);
        save(objects);

        assertEquals(document1.getObjectReference(), investigation.getDocument());

        Document document2 = createDocument();
        objects = rules.addDocument(investigation, document2);
        save(objects);

        assertEquals(document2.getObjectReference(), investigation.getDocument());

        List<DocumentAct> acts = bean.getTargets("versions", DocumentAct.class);
        assertEquals(1, acts.size());
        DocumentAct old = acts.get(0);
        assertEquals(document1.getObjectReference(), old.getDocument());

        // add another document.
        Document document3 = createDocument();
        objects = rules.addDocument(investigation, document3);
        save(objects);

        // verify the document3 is the latest
        assertEquals(document3.getObjectReference(), investigation.getDocument());

        // verify document1 and document2 are versioned, and that none of order/investigation/event relationships
        // or identities are copied
        acts = bean.getTargets("versions", DocumentAct.class);
        assertEquals(2, acts.size());
        Set<Reference> docs = new HashSet<>();
        for (DocumentAct version : acts) {
            assertEquals(1, version.getActRelationships().size()); // only one relationship, back to parent
            assertEquals(0, version.getIdentities().size());       // identities not copied
            docs.add(version.getDocument());
        }
        assertTrue(docs.contains(document1.getObjectReference()));
        assertTrue(docs.contains(document2.getObjectReference()));
    }

    /**
     * Verifies that the appropriate version acts are created for <em>act.patientDocumentAttachment</em>,
     * <em>act.patientDocumentImage</em>, <em>act.patientDocumentLetter</em> and <em>act.patientInvestigation</em>
     * by {@link DocumentRules#createVersion(DocumentAct)}.
     */
    @Test
    public void testCreatePatientDocumentVersion() {
        checkCreatePatientVersion(PatientArchetypes.DOCUMENT_ATTACHMENT, PatientArchetypes.DOCUMENT_ATTACHMENT_VERSION);
        checkCreatePatientVersion(PatientArchetypes.DOCUMENT_IMAGE, PatientArchetypes.DOCUMENT_IMAGE_VERSION);
        checkCreatePatientVersion(PatientArchetypes.DOCUMENT_LETTER, PatientArchetypes.DOCUMENT_LETTER_VERSION);
        checkCreatePatientVersion(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                  InvestigationArchetypes.PATIENT_INVESTIGATION_VERSION);
    }

    /**
     * Verifies that the appropriate version acts are created for <em>act.customerDocumentAttachment</em> and
     * <em>act.customerDocumentLetter</em>
     */
    @Test
    public void testCreateCustomerDocumentVersion() {
        checkCreateCustomerSupplierVersion(CustomerArchetypes.DOCUMENT_ATTACHMENT,
                                           CustomerArchetypes.DOCUMENT_ATTACHMENT_VERSION);
        checkCreateCustomerSupplierVersion(CustomerArchetypes.DOCUMENT_LETTER,
                                           CustomerArchetypes.DOCUMENT_LETTER_VERSION);
    }

    /**
     * Verifies that the appropriate version acts are created for <em>act.customerDocumentAttachment</em> and
     * <em>act.customerDocumentLetter</em>
     */
    @Test
    public void testCreateSupplierDocumentVersion() {
        checkCreateCustomerSupplierVersion(SupplierArchetypes.DOCUMENT_ATTACHMENT,
                                           SupplierArchetypes.DOCUMENT_ATTACHMENT_VERSION);
        checkCreateCustomerSupplierVersion(SupplierArchetypes.DOCUMENT_LETTER,
                                           SupplierArchetypes.DOCUMENT_LETTER_VERSION);
    }

    /**
     * Tests the {@link DocumentRules#isDuplicate} method.
     */
    @Test
    public void testIsDuplicate() {
        // create an act.patientDocumentImage and link a patient
        Party patient = patientFactory.createPatient();
        DocumentAct act = create(PatientArchetypes.DOCUMENT_IMAGE, DocumentAct.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        // now add a document.
        Document document1 = createDocument();
        assertNotEquals(0, document1.getSize());
        assertNotEquals(0, document1.getChecksum());

        List<IMObject> objects = rules.addDocument(act, document1);
        save(objects);

        // verify that for the same document, isDuplicate returns true
        assertTrue(rules.isDuplicate(act, document1));

        // now change the checksum to 0 and verify that isDuplicate returns false
        document1.setChecksum(0);
        save(document1);
        assertFalse(rules.isDuplicate(act, document1));

        // verify that for a different document with different content, isDuplicate returns false
        Document document2 = createDocument();
        assertNotEquals(0, document2.getSize());
        assertNotEquals(0, document2.getChecksum());
        assertFalse(rules.isDuplicate(act, document2));

        // verify that for a different document with same checksum and length, isDuplicate returns tue
        document1.setChecksum(1);
        save(document1);
        document2.setSize(document1.getSize());
        document2.setChecksum(document1.getChecksum());
        assertTrue(rules.isDuplicate(act, document2));
    }

    /**
     * Tests the {@link DocumentRules#getLogo(Entity)} method.
     */
    @Test
    public void testGetLogo() {
        Entity letterhead1 = documentFactory.newLetterhead().build();
        assertNull(rules.getLogo(letterhead1));

        TestLetterheadBuilder builder = documentFactory.newLetterhead();
        Entity letterhead2 = builder
                .logo(createDocument())
                .build();
        DocumentAct logo = builder.getLogo();

        assertEquals(logo, rules.getLogo(letterhead2));
    }

    /**
     * Tests the {@link DocumentRules#copy(Document)} method.
     */
    @Test
    public void testCopyDocument() {
        Document image = documentFactory.createImage();
        assertTrue(image.isA(DocumentArchetypes.IMAGE_DOCUMENT));
        Document copy = rules.copy(image);

        assertNotEquals(image.getId(), copy.getId());

        documentFactory.newImageVerifier()
                .initialise(image)
                .verify(copy);

        //verify the byte array isn't the same. This is to ensure that changes to the original aren't reflected in
        // the copy, and vice-versa.
        assertNotSame(((org.openvpms.component.business.domain.im.document.Document) image).getContents(),
                      ((org.openvpms.component.business.domain.im.document.Document) copy).getContents());
    }

    /**
     * Verifies that versioning works for a patient document act.
     *
     * @param actShortName    the act archetype short name
     * @param expectedVersion the act version archetype short name
     */
    private void checkCreatePatientVersion(String actShortName, String expectedVersion) {
        DocumentAct act = create(actShortName, DocumentAct.class);
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();
        Entity investigationType = (act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) ?
                                   laboratoryFactory.createInvestigationType() : null;
        Product product = null;
        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);
        bean.setTarget("clinician", clinician);
        if (investigationType != null) {
            bean.setTarget("investigationType", investigationType);
        }
        if (bean.hasNode("product")) {
            product = productFactory.createService();
            bean.setTarget("product", product);
        }
        bean.save();

        patientFactory.newVisit()
                .patient(patient)
                .addItem(act)
                .build();

        DocumentAct version1 = checkCreateVersionForPatientDocument(act, expectedVersion, clinician,
                                                                    investigationType, product);
        bean.addTarget("versions", version1, "parent");
        bean.save(version1);

        // add another version to verify relationships aren't copied
        checkCreateVersionForPatientDocument(act, expectedVersion, clinician, investigationType, product);
    }

    /**
     * Verifies that versioning works for a patient document act.
     *
     * @param act               the act
     * @param expectedVersion   the act version archetype short name
     * @param clinician         the expected clinician
     * @param investigationType the expected investigation type
     * @param product           the expected product
     */
    private DocumentAct checkCreateVersionForPatientDocument(DocumentAct act, String expectedVersion, User clinician,
                                                             Entity investigationType, Product product) {
        DocumentAct version = checkCreateVersion(act, expectedVersion);
        IMObjectBean versionBean = getBean(version);
        assertEquals(clinician, versionBean.getTarget("clinician"));
        if (versionBean.hasNode("product")) {
            assertEquals(product, versionBean.getTarget("product"));
        }
        if (act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            assertEquals(investigationType, versionBean.getTarget("investigationType"));
        }

        // no relationships should be copied.
        assertEquals(0, version.getSourceActRelationships().size());
        assertEquals(0, version.getTargetActRelationships().size());
        return version;
    }

    /**
     * Verifies that versioning works for a customer or supplier document act.
     *
     * @param actShortName    the act archetype short name
     * @param expectedVersion the act version archetype short name
     */
    private void checkCreateCustomerSupplierVersion(String actShortName, String expectedVersion) {
        DocumentAct act = create(actShortName, DocumentAct.class);
        checkCreateVersion(act, expectedVersion);
    }

    /**
     * Verifies that versioning works for an act.
     *
     * @param act             the act
     * @param expectedVersion the act version archetype short name
     * @return the version
     */
    private DocumentAct checkCreateVersion(DocumentAct act, String expectedVersion) {
        act.setPrinted(true);

        IMObjectBean bean = getBean(act);
        assertTrue(bean.hasNode("document")); // make sure act has document node

        Document document = createDocument();
        save(document);
        act.setDocument(document.getObjectReference());
        assertNotNull(act);
        DocumentAct version = rules.createVersion(act);
        assertNotNull(version);
        assertEquals(expectedVersion, version.getArchetype());
        assertEquals(document.getObjectReference(), version.getDocument());
        assertEquals(act.getMimeType(), version.getMimeType());
        assertEquals(act.getFileName(), version.getFileName());
        assertEquals(act.isPrinted(), version.isPrinted());

        IMObjectBean versionBean = getBean(version);
        assertTrue(versionBean.hasNode("document"));
        return version;
    }

    /**
     * Helper to create a document.
     *
     * @return a new document
     */
    private Document createDocument() {
        return documentFactory.newDocument().name(ValueStrategy.suffix(".png"))
                .content(RandomUtils.nextBytes(10))
                .build();
    }
}
