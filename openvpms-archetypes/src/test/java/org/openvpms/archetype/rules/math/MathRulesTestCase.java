/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2015 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.math;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.openvpms.archetype.rules.math.MathRules.calculateTotal;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Tests the {@link MathRules} class.
 *
 * @author Tim Anderson
 */
public class MathRulesTestCase {

    /**
     * Tests the {@link MathRules#calculateTotal(BigDecimal, BigDecimal, BigDecimal, BigDecimal, int)} method.
     */
    @Test
    public void testCalculateTotal() {
        BigDecimal fixedPrice = BigDecimal.valueOf(10);
        BigDecimal discount = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("0.50");
        BigDecimal quantity = BigDecimal.valueOf(2);

        checkEquals(BigDecimal.valueOf(6), calculateTotal(fixedPrice, unitPrice1, quantity, discount, 2));
        checkEquals(BigDecimal.valueOf(-6), calculateTotal(fixedPrice, unitPrice1, quantity.negate(), discount, 2));

        // now ensure it is rounded to 2 decimal places
        BigDecimal unitPrice2 = new BigDecimal("0.514");
        checkEquals(new BigDecimal("6.03"), calculateTotal(fixedPrice, unitPrice2, quantity, discount, 2));
        checkEquals(new BigDecimal("-6.03"), calculateTotal(fixedPrice, unitPrice2, quantity.negate(), discount, 2));

        // simulate 100% discount
        BigDecimal fixedPrice3 = new BigDecimal("12.00");
        BigDecimal discount3 = new BigDecimal("106.08");
        BigDecimal unitPrice3 = new BigDecimal("37.63");
        BigDecimal quantity3 = new BigDecimal("2.50");
        checkEquals(ZERO, calculateTotal(fixedPrice3, unitPrice3, quantity3, discount3, 2));
        checkEquals(ZERO, calculateTotal(fixedPrice3, unitPrice3, quantity3.negate(), discount3, 2));

        // zero quantity
        checkEquals(ZERO, calculateTotal(fixedPrice, unitPrice1, ZERO, discount, 2));
        checkEquals(ZERO, calculateTotal(fixedPrice, unitPrice2, ZERO, discount, 2));
        checkEquals(ZERO, calculateTotal(fixedPrice3, unitPrice3, ZERO, discount3, 2));
        checkEquals(ZERO, calculateTotal(fixedPrice, unitPrice1, ZERO.negate(), discount, 2));
    }


    /**
     * Tests the {@link MathRules#intersects(BigDecimal, BigDecimal, BigDecimal, BigDecimal)}.
     */
    @Test
    public void testIntersects() {
        // range1 before range2
        checkIntersects(false, "1", "10", "10", "20");
        checkIntersects(false, "1", "10", "11", "20");
        checkIntersects(false, "1", "10", "10", null);
        checkIntersects(false, null, "10", "10", "20");
        checkIntersects(false, null, "10", "11", "20");
        checkIntersects(false, null, "10", "10", null);
        checkIntersects(false, null, "10", "11", null);

        // range1 after range2
        checkIntersects(false, "10", "20", "1", "10");
        checkIntersects(false, "11", "20", "1", "10");
        checkIntersects(false, "10", null, "1", "10");
        checkIntersects(false, "11", null, "1", "10");
        checkIntersects(false, "10", "20", null, "10");
        checkIntersects(false, "10", null, null, "10");
        checkIntersects(false, "11", null, null, "10");

        // range1 overlaps start of range2
        checkIntersects(true, "1", "11", "10", "20");

        // range1 overlaps end of range2
        checkIntersects(true, "9", "20", "1", "10");

        // range1 == range2
        checkIntersects(true, "1", "20", "1", "20");

        // range1 within range2
        checkIntersects(true, "5", "6", "1", "20");
        checkIntersects(true, "1", "5", "1", "20");
        checkIntersects(true, "5", "20", "1", "20");

        // range2 within range1
        checkIntersects(true, "1", "20", "5", "6");
        checkIntersects(true, "1", "20", "1", "5");
        checkIntersects(true, "1", "20", "5", "20");
    }

    /**
     * Verifies that an unbounded numeric range intersects everything.
     */
    @Test
    public void testIntersectsForUnboundedNumericRange() {
        checkIntersects(true, null, null, null, null);
        checkIntersects(true, null, null, "1", null);
        checkIntersects(true, null, null, "1", "31");
        checkIntersects(true, null, null, null, "31");
        checkIntersects(true, "1", null, null, null);
        checkIntersects(true, "1", "31", null, null);
        checkIntersects(true, null, "31", null, null);
    }

    /**
     * Tests the {@link MathRules#intersects(BigDecimal, BigDecimal, BigDecimal, BigDecimal)}  method.
     *
     * @param intersects the expected result
     * @param from1      the start of the first date range. May be {@code null}
     * @param to1        the end of the first date range. May be {@code null}
     * @param from2      the start of the second date range. May be {@code null}
     * @param to2        the end of the second date range. May be {@code null}
     */
    private void checkIntersects(boolean intersects, String from1, String to1, String from2, String to2) {
        Assert.assertEquals(intersects, MathRules.intersects(
                getValue(from1), getValue(to1), getValue(from2), getValue(to2)));
    }

    /**
     * Converts a string to a BigDecimal.
     *
     * @param value the value. May be {@code null}
     * @return the converted value or {@code null} if {@code value} is null
     */
    private BigDecimal getValue(String value) {
        return value != null ? new BigDecimal(value) : null;
    }
}
