/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.party;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link Contacts} class.
 *
 * @author Tim Anderson
 */
public class ContactsTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link Contacts#getPhone(String, String)} method.
     */
    @Test
    public void testGetPhone() {
        assertEquals("12345", Contacts.getPhone(null, "12345"));
        assertEquals("12345", Contacts.getPhone(null, "12 345"));
        assertEquals("12345", Contacts.getPhone("", "12345"));
        assertEquals("12345", Contacts.getPhone(" ", "12345"));
        assertEquals("12345", Contacts.getPhone("\n", "12345"));
        assertEquals("0312345", Contacts.getPhone("(03)", "12 345"));
        assertEquals("12345", Contacts.getPhone(null, "12345 - some migrated comment"));

        assertNull(Contacts.getPhone(null, null));
        assertNull(Contacts.getPhone(null, ""));
        assertNull(Contacts.getPhone("", null));
        assertNull(Contacts.getPhone("", ""));
        assertNull(Contacts.getPhone(" ", " "));
        assertNull(Contacts.getPhone("\n", "\n"));
        assertNull(Contacts.getPhone("(03)", null));
        assertNull(Contacts.getPhone("(03)", ""));
    }
}
