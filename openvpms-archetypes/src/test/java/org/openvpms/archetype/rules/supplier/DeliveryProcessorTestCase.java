/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.i18n.ArchetypeMessages;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceTestHelper;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;


/**
 * Tests the {@link DeliveryProcessor} class.
 *
 * @author Tim Anderson
 */
public class DeliveryProcessorTestCase extends AbstractSupplierTest {

    /**
     * The product rules.
     */
    @Autowired
    private ProductRules rules;

    /**
     * Sets up the test case.
     */
    @Override
    @Before
    public void setUp() {
        super.setUp();
        setIgnoreListPriceDecreases(false);
    }

    /**
     * Tests the {@link DeliveryProcessor} when invoked via the <em>archetypeService.save.act.supplierDelivery</em> and
     * the <em>archetypeService.save.act.supplierReturn</em> rules.
     */
    @Test
    public void testPostDelivery() {
        Party supplier = getSupplier();
        Product product = getProduct();
        BigDecimal quantity = new BigDecimal(5);
        Act delivery = createDelivery(supplier, product, quantity, 1, null);

        checkProductStockLocationRelationship(product, null);
        // there should be no relationship until the delivery is posted

        delivery.setStatus(ActStatus.POSTED);
        save(delivery);
        checkProductStockLocationRelationship(product, quantity);

        // re-save the delivery, and verify the relationship hasn't changed
        save(delivery);
        checkProductStockLocationRelationship(product, quantity);

        Act orderReturn = createReturn(supplier, product, quantity, 1, ONE);
        checkProductStockLocationRelationship(product, quantity);

        orderReturn.setStatus(ActStatus.POSTED);
        save(orderReturn);
        checkProductStockLocationRelationship(product, ZERO);
    }

    /**
     * Tests that the {@link DeliveryProcessor} updates orders associated with
     * the delivery/return.
     */
    @Test
    public void testPostDeliveryUpdatesOrder() {
        Party supplier = getSupplier();
        Product product = getProduct();
        BigDecimal quantity = new BigDecimal(5);
        BigDecimal delivery1Quantity = new BigDecimal(3);
        BigDecimal delivery2Quantity = new BigDecimal(7);

        // create an order with a single item, and post it
        FinancialAct orderItem = createOrderItem(product, quantity, 1, ONE);
        Act order = createOrder(supplier, orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order);

        checkOrder(order, orderItem, DeliveryStatus.PENDING, ZERO);

        // create a new delivery associated with the order item
        Act delivery1 = createDelivery(supplier, product, delivery1Quantity, 1, ONE, orderItem);

        // there should be no relationship until the delivery is posted,
        // and the order shouldn't update
        checkProductStockLocationRelationship(product, null);
        checkOrder(order, orderItem, DeliveryStatus.PENDING, ZERO);

        // now post the delivery
        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        checkOrder(order, orderItem, DeliveryStatus.PART, delivery1Quantity);
        checkProductStockLocationRelationship(product, delivery1Quantity);

        // save the delivery again, and verify quantities don't change
        save(delivery1);
        checkOrder(order, orderItem, DeliveryStatus.PART, delivery1Quantity);
        checkProductStockLocationRelationship(product, delivery1Quantity);

        // now return that which was delivered by delivery1
        orderItem = get(orderItem); // refresh
        Act return1 = createReturn(supplier, product, delivery1Quantity, 1, ONE, orderItem);
        return1.setStatus(ActStatus.POSTED);
        save(return1);
        checkOrder(order, orderItem, DeliveryStatus.PENDING, ZERO);
        checkProductStockLocationRelationship(product, ZERO);

        // create a new delivery associated with the order item
        orderItem = get(orderItem); // refresh
        Act delivery2 = createDelivery(supplier, product, delivery2Quantity, 1, ONE, orderItem);
        delivery2.setStatus(ActStatus.POSTED);
        save(delivery2);
        checkOrder(order, orderItem, DeliveryStatus.FULL, delivery2Quantity);
        checkProductStockLocationRelationship(product, delivery2Quantity);
    }

    /**
     * Tests that the {@link DeliveryProcessor} updates an order that is fulfilled by multiple deliveries.
     */
    @Test
    public void testOrderWithMultipleDeliveries() {
        Party supplier = getSupplier();
        Product product = getProduct();
        BigDecimal quantity = new BigDecimal(5);
        BigDecimal unitPrice = ONE;

        // create an order with two items, and post it
        FinancialAct orderItem1 = createOrderItem(product, TEN, 1, unitPrice);
        FinancialAct orderItem2 = createOrderItem(product, TEN, 1, unitPrice);
        Act order = createOrder(supplier, orderItem1, orderItem2);
        order.setStatus(ActStatus.POSTED);
        save(order);

        checkDeliveryStatus(order, DeliveryStatus.PENDING);

        // create a new delivery associated with the order item
        FinancialAct delivery1Item = createDeliveryItem(product, quantity, 1, unitPrice, orderItem1);
        FinancialAct delivery1 = createDelivery(supplier, delivery1Item);

        // there should be no relationship until the delivery is posted,
        // and the order shouldn't update
        checkProductStockLocationRelationship(product, null);
        checkDeliveryStatus(order, DeliveryStatus.PENDING);
        checkReceivedQuantity(orderItem1, ZERO);
        checkReceivedQuantity(orderItem2, ZERO);

        // now post the delivery
        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        checkDeliveryStatus(order, DeliveryStatus.PART);
        checkReceivedQuantity(orderItem1, quantity);
        checkReceivedQuantity(orderItem2, ZERO);
        checkProductStockLocationRelationship(product, quantity);

        // create a new delivery that fulfills the remainder of orderItem1
        orderItem1 = get(orderItem1);
        FinancialAct delivery2Item = createDeliveryItem(product, quantity, 1, unitPrice, orderItem1);
        FinancialAct delivery2 = createDelivery(supplier, delivery2Item);
        delivery2.setStatus(ActStatus.POSTED);
        save(delivery2);

        // check the order
        checkDeliveryStatus(order, DeliveryStatus.PART);

        // create a new delivery that fulfills the order
        FinancialAct delivery3Item = createDeliveryItem(product, TEN, 1, unitPrice, orderItem2);
        FinancialAct delivery3 = createDelivery(supplier, delivery3Item);
        delivery3.setStatus(ActStatus.POSTED);
        save(delivery3);

        // check the order
        checkDeliveryStatus(order, DeliveryStatus.FULL);
    }

    /**
     * Verifies that the order quantity is updated correctly when a
     * delivery/return is posted with a different package size.
     */
    @Test
    public void testQuantityConversion() {
        Party supplier = getSupplier();
        Product product = getProduct();
        BigDecimal quantity = ONE;
        int packageSize = 20;
        BigDecimal unitPrice = ONE;

        FinancialAct orderItem = createOrderItem(product, quantity, packageSize, unitPrice);
        Act order = createOrder(supplier, orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order);

        // deliver 2 units, containing 5 items each
        Act delivery1 = createDelivery(supplier, product, new BigDecimal(2), 5, unitPrice,
                                       orderItem);
        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        checkOrder(order, orderItem, DeliveryStatus.PART,
                   new BigDecimal("0.5"));

        // now return the units
        orderItem = get(orderItem); // refresh
        Act return1 = createReturn(supplier, product, new BigDecimal(1), 10, unitPrice, orderItem);
        return1.setStatus(ActStatus.POSTED);
        save(return1);
        checkOrder(order, orderItem, DeliveryStatus.PENDING, ZERO);

        // deliver 10 units, containing 2 items each
        orderItem = get(orderItem); // refresh
        Act delivery2 = createDelivery(supplier, product, new BigDecimal(10), 2, unitPrice, orderItem);
        delivery2.setStatus(ActStatus.POSTED);
        save(delivery2);
        checkOrder(order, orderItem, DeliveryStatus.FULL, quantity);
    }

    /**
     * Verifies that the <em>entityLink.productSupplier</em> is updated when a delivery is <em>POSTED</em>.
     */
    @Test
    public void testProductSupplierUpdate() {
        BigDecimal quantity = ONE;
        int packageSize = 20;
        BigDecimal unitPrice1 = new BigDecimal("10.00");
        BigDecimal unitPrice2 = new BigDecimal("12.50");

        Party supplier1 = getSupplier();
        Party supplier2 = TestHelper.createSupplier();
        Product product = getProduct();

        Act delivery1 = createDelivery(supplier1, product, quantity, packageSize, unitPrice1);
        checkProductSupplier(product, supplier1, -1, null, false); // not yet posted

        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        checkProductSupplier(product, supplier1, packageSize, unitPrice1, true);

        Act delivery2 = createDelivery(supplier1, product, quantity, packageSize, unitPrice2);
        delivery2.setStatus(ActStatus.POSTED);
        save(delivery2);
        checkProductSupplier(product, supplier1, packageSize, unitPrice2, true);

        // verify a return doesn't update the product-supplier relationship
        Act return1 = createReturn(supplier1, product, quantity, packageSize, unitPrice1);
        return1.setStatus(ActStatus.POSTED);
        save(return1);
        checkProductSupplier(product, supplier1, packageSize, unitPrice2, true);

        Act delivery3 = createDelivery(supplier2, product, quantity, packageSize, unitPrice1);
        delivery3.setStatus(ActStatus.POSTED);
        save(delivery3);

        // verify there is now 2 relationships, but that the supplier2 relationship is not preferred
        checkProductSupplier(product, supplier1, packageSize, unitPrice2, true);
        checkProductSupplier(product, supplier2, packageSize, unitPrice1, false);
    }

    /**
     * Verifies that <em>productPrice.unitPrice</em>s associated with a product are updated.
     */
    @Test
    public void testUnitPriceUpdate() {
        Product product = getProduct();
        Party supplier = getSupplier();
        int cost1 = 1;
        int price1 = 2;
        int cost2 = 2;
        int price2 = 4;
        BigDecimal cost3 = new BigDecimal("0.80");
        BigDecimal price3 = new BigDecimal("1.60");
        int packageSize = 20;

        // add a new price
        productFactory.updateMedication(product)
                .newUnitPrice()
                .fromDate(getDate("2017-05-07"))
                .costAndPrice(cost1, price1)
                .add()
                .newProductSupplier()
                .supplier(supplier)
                .packageSize(packageSize)
                .add()
                .build();

        // Get the product-supplier relationship. By default, it should not trigger auto price updates
        ProductSupplier ps = getProductSupplier(product, supplier, packageSize);
        assertNotNull(ps);
        assertFalse(ps.isAutoPriceUpdate());

        // post a delivery, and verify prices don't update
        BigDecimal quantity = ONE;
        BigDecimal unitPrice1 = new BigDecimal("20.00");
        BigDecimal listPrice1 = new BigDecimal("40.00");

        postDelivery(supplier, product, quantity, packageSize, unitPrice1, listPrice1);
        product = get(product);
        checkPrice(product, cost1, price1, true, true, null);

        // reload product-supplier relationship and set to auto update prices
        ps = getProductSupplier(product, supplier, packageSize);
        assertNotNull(ps);
        ps.setAutoPriceUpdate(true);
        ps.save();

        // post another delivery
        Act delivery2 = postDelivery(supplier, product, quantity, packageSize, unitPrice1, listPrice1);
        String notes2 = ArchetypeMessages.priceCreatedByDelivery(delivery2, supplier).getMessage();

        // verify that the price has updated
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        checkPrice(product, cost2, price2, false, true, notes2);

        // now post a return. The price shouldn't update
        BigDecimal unitPrice2 = new BigDecimal("8.00");
        BigDecimal listPrice2 = new BigDecimal("15.00");
        postReturn(supplier, product, quantity, packageSize, unitPrice2, listPrice2);

        // verify that the prices have not updated
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        checkPrice(product, cost2, price2, false, true, notes2);

        // post another delivery
        BigDecimal unitPrice3 = new BigDecimal("8.00");
        BigDecimal listPrice3 = new BigDecimal("16.00");
        Act delivery3 = postDelivery(supplier, product, quantity, packageSize, unitPrice3, listPrice3);
        String notes3 = ArchetypeMessages.priceCreatedByDelivery(delivery3, supplier).getMessage();

        product = get(product);
        assertEquals(3, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        checkPrice(product, cost2, price2, false, false, notes2);
        checkPrice(product, cost3, price3, false, true, notes3);

        // now mark the supplier inactive. Subsequent deliveries shouldn't update prices
        supplier = get(getSupplier());
        supplier.setActive(false);
        save(supplier);

        // post another delivery
        BigDecimal unitPrice4 = new BigDecimal("10.00");
        BigDecimal listPrice4 = new BigDecimal("20.00");
        postDelivery(supplier, product, quantity, packageSize, unitPrice4, listPrice4);

        // verify that the prices have not updated
        product = get(product);
        assertEquals(3, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        checkPrice(product, cost2, price2, false, false, notes2);
        checkPrice(product, cost3, price3, false, true, notes3);
    }

    /**
     * Verifies that when 2 unit prices with different pricing groups are active, both are closed when a delivery
     * is processed and new prices with the pricing groups created.
     */
    @Test
    public void testUnitPriceUpdateWithPricingGroups() {
        Product product = getProduct();
        Party supplier = getSupplier();
        BigDecimal cost1 = ONE;
        BigDecimal cost2 = BigDecimal.valueOf(2);
        int packageSize = 20;

        // add a product price for pricing group GROUP1 and GROUP2, with different prices
        productFactory.updateMedication(product)
                .newUnitPrice()
                .fromDate(getDate("2021-10-01"))
                .costAndMarkup(1, 50)    // 50% markup
                .pricingGroups("GROUP1")
                .add()
                .newUnitPrice()
                .fromDate(getDate("2021-10-01"))
                .costAndMarkup(2, 100)   // 100% markup
                .pricingGroups("GROUP2")
                .add()
                .newProductSupplier()
                .supplier(supplier)
                .packageSize(packageSize)
                .autoPriceUpdate(true)
                .add()
                .build();

        // verify the prices exist
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, new BigDecimal("1.5"), true, true, "GROUP1", null);
        checkPrice(product, cost2, BigDecimal.valueOf(4), true, true, "GROUP2", null);

        // process a delivery
        Act delivery = postDelivery(supplier, product, ONE, packageSize, new BigDecimal("25"), new BigDecimal("50"));
        String notes = ArchetypeMessages.priceCreatedByDelivery(delivery, supplier).getMessage();

        // verify the prices have been closed, and new prices added
        product = get(product);
        assertEquals(4, product.getProductPrices().size());
        checkPrice(product, cost1, new BigDecimal("1.5"), true, false, "GROUP1", null);
        checkPrice(product, cost2, BigDecimal.valueOf(4), true, false, "GROUP2", null);
        checkPrice(product, new BigDecimal("2.5"), new BigDecimal("3.75"), false, true, "GROUP1", notes);
        checkPrice(product, new BigDecimal("2.5"), BigDecimal.valueOf(5), false, true, "GROUP2", notes);
    }

    /**
     * Verifies that unit price updates are ignored if there is 2 active prices into the future.
     * <p/>
     * This is to avoid creating a chain of unit prices after the current active price.
     */
    @Test
    public void testUnitPriceUpdateWith2ExistingUnitPrices() {
        Product product = getProduct();
        Party supplier = getSupplier();
        Date fromDate = DateRules.getYesterday();
        Date toDate = DateRules.getTomorrow();
        int cost1 = 1;
        int price1 = 2;
        int cost2 = 2;
        int price2 = 3;
        int packageSize = 20;

        // add a new price with a date range.
        productFactory.updateMedication(product)
                .newUnitPrice()                 // price 1
                .costAndPrice(cost1, price1)
                .dateRange(fromDate, toDate)
                .add()
                .newUnitPrice()                 // price 2
                .costAndPrice(cost2, price2)
                .dateRange(toDate, null)
                .add()
                .newProductSupplier()
                .packageSize(packageSize)
                .autoPriceUpdate(true)
                .supplier(supplier)
                .add()
                .build();

        // process a delivery
        postDelivery(supplier, product, ONE, packageSize, BigDecimal.valueOf(30), BigDecimal.valueOf(60));

        // verify no price is added
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        checkPrice(product, cost2, price2, false, true, null);
    }

    /**
     * Verifies that if there is an existing unit price with a future end-date, a delivery will create a new
     * price starting from that end date, but subsequent deliveries will be ignored.
     * <p/>
     * This is a variation on {@link #testUnitPriceUpdateWith2ExistingUnitPrices}.
     */
    @Test
    public void testUnitPriceUpdateForExistingPriceWithFutureEndDate() {
        Product product = getProduct();
        Party supplier = getSupplier();
        int cost1 = 0;
        int price1 = 1;
        Date fromDate = DateRules.getYesterday();
        Date toDate = DateRules.getTomorrow();
        int packageSize = 20;

        // add a new price with a date range.
        productFactory.updateMedication(product)
                .newUnitPrice()
                .dateRange(fromDate, toDate)
                .costAndPrice(cost1, price1)
                .add()
                .newProductSupplier()
                .packageSize(packageSize)
                .autoPriceUpdate(true)
                .supplier(supplier)
                .add()
                .build();

        BigDecimal unitPriceA = new BigDecimal("20.00");
        BigDecimal listPriceA = new BigDecimal("40.00");
        BigDecimal quantity = ONE;
        int cost2 = 2;
        int price2 = 2;

        // process a delivery, and verify a price is added starting at the end of the existing price
        FinancialAct delivery1 = postDelivery(supplier, product, quantity, packageSize, unitPriceA, listPriceA);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());

        checkPrice(product, cost1, price1, true, false, null);
        String notes = ArchetypeMessages.priceCreatedByDelivery(delivery1, supplier).getMessage();
        ProductPrice productPrice2 = checkPrice(product, cost2, price2, false, true, notes);
        assertEquals(0, DateRules.compareTo(productPrice2.getFromDate(), toDate));
        assertNull(productPrice2.getToDate());

        // process another delivery with the same prices, and verify no new price is added starting at the end of the
        // existing price
        postDelivery(supplier, product, quantity, packageSize, unitPriceA, listPriceA);
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        ProductPrice productPrice3 = checkPrice(product, cost2, price2, false, true, notes);
        assertEquals(productPrice2, productPrice3);
        assertEquals(0, DateRules.compareTo(productPrice3.getFromDate(), toDate));
        assertNull(productPrice3.getToDate());

        // post a new delivery with a new unit price. Because there is already a price after the current price,
        // this delivery will be ignored.
        postDelivery(supplier, product, quantity, packageSize, new BigDecimal("25.0"), new BigDecimal("50.00"));
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, cost1, price1, true, false, null);
        ProductPrice productPrice4 = checkPrice(product, cost2, price2, false, true, notes);
        assertEquals(productPrice2, productPrice4);
        assertEquals(0, DateRules.compareTo(productPrice4.getFromDate(), toDate));
        assertNull(productPrice4.getToDate());
    }

    /**
     * Verifies that when the party.organisationPractice ignoreListPriceDecreases node is {@code true}, auto-price
     * updates are disabled if the delivery list price is less than the existing list price.
     */
    @Test
    public void testIgnoreListPriceDecreases() {
        Product product = getProduct();
        Party supplier = getSupplier();
        int packageSize = 20;

        // add a new price
        productFactory.updateMedication(product)
                .newUnitPrice()
                .cost(0)
                .markup(100)
                .price(1)
                .add()
                .newProductSupplier()
                .supplier(supplier)
                .packageSize(packageSize)
                .autoPriceUpdate(true)
                .add()
                .build();

        // post a delivery, and verify prices update
        BigDecimal unitPrice1 = new BigDecimal("10.00");
        BigDecimal quantity = ONE;
        BigDecimal listPrice1 = new BigDecimal("20.00");

        Act delivery1 = postDelivery(supplier, product, quantity, packageSize, unitPrice1, listPrice1);

        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ZERO, ONE, true, false, null);
        String notes1 = ArchetypeMessages.priceCreatedByDelivery(delivery1, supplier).getMessage();
        checkPrice(product, ONE, new BigDecimal("2.00"), false, true, notes1);

        // now disable price updates for decreases in list price
        setIgnoreListPriceDecreases(true);

        // post another delivery
        BigDecimal listPrice2 = new BigDecimal("15.00");
        postDelivery(supplier, product, quantity, packageSize, TEN, listPrice2);

        // verify that the prices haven't updated
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ZERO, ONE, true, false, null);
        checkPrice(product, ONE, new BigDecimal("2.00"), false, true, notes1);

        // post another delivery, increasing the list price
        BigDecimal listPrice3 = new BigDecimal("30.00");
        Act delivery3 = postDelivery(supplier, product, quantity, packageSize, TEN, listPrice3);
        save(delivery3);

        // verify that the price has updated
        product = get(product);
        String notes3 = ArchetypeMessages.priceCreatedByDelivery(delivery3, supplier).getMessage();
        assertEquals(3, product.getProductPrices().size());
        checkPrice(product, ZERO, ONE, true, false, null);
        checkPrice(product, ONE, new BigDecimal("2.00"), false, false, notes1);
        checkPrice(product, new BigDecimal("1.50"), new BigDecimal("3.00"), false, true, notes3);

        // now enable price updates for decreases in list price
        setIgnoreListPriceDecreases(false);

        // post another delivery
        BigDecimal listPrice4 = new BigDecimal("20.00");
        Act delivery4 = postDelivery(supplier, product, quantity, packageSize, TEN, listPrice4);

        // verify that the price has updated
        product = get(product);
        String notes4 = ArchetypeMessages.priceCreatedByDelivery(delivery4, supplier).getMessage();
        assertEquals(4, product.getProductPrices().size());
        checkPrice(product, ZERO, ONE, true, false, null);
        checkPrice(product, ONE, new BigDecimal("2.00"), false, false, notes1);
        checkPrice(product, new BigDecimal("1.50"), new BigDecimal("3.00"), false, false, notes3);
        checkPrice(product, ONE, new BigDecimal("2.00"), false, true, notes4);
    }

    /**
     * Verifies that when the party.organisationPractice ignoreListPriceDecreases node is {@code true}, auto-price
     * updates are disabled if the calculated cost price is less than the existing cost price.
     */
    @Test
    public void testIgnoreListPriceDecreasesIgnoresLowerCostPrice() {
        Product product = getProduct();
        Party supplier = getSupplier();
        int packageSize = 20;

        // add a new price
        productFactory.updateMedication(product)
                .newUnitPrice()
                .costAndPrice(1, 2)
                .add()
                .newProductSupplier()
                .supplier(supplier)
                .packageSize(packageSize)
                .autoPriceUpdate(true)
                .add()
                .build();

        // post a delivery, and verify prices update
        BigDecimal unitPrice1 = new BigDecimal("10.00");
        BigDecimal quantity = ONE;
        BigDecimal listPrice1 = new BigDecimal("15.00");

        Act delivery1 = createDelivery(supplier, product, quantity, packageSize, unitPrice1, listPrice1);
        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        String notes1 = ArchetypeMessages.priceCreatedByDelivery(delivery1, supplier).getMessage();

        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ONE, BigDecimal.valueOf(2), true, false, null);
        checkPrice(product, new BigDecimal("0.75"), new BigDecimal("1.50"), false, true, notes1);

        // now disable price updates for decreases in list price
        setIgnoreListPriceDecreases(true);

        // post another delivery, with a higher list price but greater package size.
        BigDecimal listPrice2 = new BigDecimal("20.00");
        Act delivery2 = createDelivery(supplier, product, quantity, 50, TEN, listPrice2);
        delivery2.setStatus(ActStatus.POSTED);
        save(delivery2);

        // verify that the price hasn't updated
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ONE, BigDecimal.valueOf(2), true, false, null);
        checkPrice(product, new BigDecimal("0.75"), new BigDecimal("1.50"), false, true, notes1);

        // post another delivery, increasing the list price
        BigDecimal listPrice3 = new BigDecimal("30.00");
        Act delivery3 = createDelivery(supplier, product, quantity, packageSize, TEN, listPrice3);
        delivery3.setStatus(ActStatus.POSTED);
        save(delivery3);
        String notes3 = ArchetypeMessages.priceCreatedByDelivery(delivery3, supplier).getMessage();

        // verify that the price has updated
        product = get(product);
        assertEquals(3, product.getProductPrices().size());
        checkPrice(product, ONE, BigDecimal.valueOf(2), true, false, null);
        checkPrice(product, new BigDecimal("0.75"), new BigDecimal("1.50"), false, false, notes1);
        checkPrice(product, new BigDecimal("1.50"), new BigDecimal("3.00"), false, true, notes3);
    }

    /**
     * Verifies that batches are created when a delivery is finalised.
     */
    @Test
    public void testBatchCreation() {
        Party supplier = getSupplier();
        Product product = getProduct();
        Party manufacturer = create(SupplierArchetypes.MANUFACTURER, Party.class);
        manufacturer.setName("Z Manufacturer");
        save(manufacturer);

        // create a new delivery associated with the order item
        FinancialAct item1 = createDeliveryItem(product, "batch1", null, null);
        Date expiry2 = getDate("2015-01-01");
        Date expiry3 = getDate("2015-06-01");
        Date expiry4 = getDate("2016-01-01");
        FinancialAct item2 = createDeliveryItem(product, null, expiry2, null);
        FinancialAct item3 = createDeliveryItem(product, "batch3", expiry3, manufacturer);
        FinancialAct item4 = createDeliveryItem(product, "batch4", expiry4, manufacturer);

        Entity batch4 = rules.createBatch(getProduct(), "batch4", expiry4, manufacturer);
        save(batch4);

        FinancialAct delivery1 = createDelivery(supplier, item1, item2, item3, item4);
        delivery1.setStatus(ActStatus.POSTED);
        save(delivery1);
        checkBatch("batch1", null, null);
        checkBatch(null, expiry2, null);
        checkBatch("batch3", expiry3, manufacturer);
        Entity actual = checkBatch("batch4", expiry4, manufacturer); // should be updated with the stock location
        assertEquals(batch4, actual);
    }

    /**
     * Creates a delivery item with batch details.
     *
     * @param product      the product
     * @param batchNumber  the batch number. May be {@code null}
     * @param expiryDate   the expiry date. May be {@code null}
     * @param manufacturer the manufacturer. May be {@code null}
     * @return a new delivery item
     */
    protected FinancialAct createDeliveryItem(Product product, String batchNumber, Date expiryDate,
                                              Party manufacturer) {
        FinancialAct item = createDeliveryItem(product, ONE, 1, TEN, null);
        IMObjectBean bean = getBean(item);
        bean.setValue("batchNumber", batchNumber);
        bean.setValue("expiryDate", expiryDate);
        if (manufacturer != null) {
            bean.setTarget("manufacturer", manufacturer);
        }
        return item;
    }

    /**
     * Verifies a batch exists with the specified attributes.
     *
     * @param batchNumber  the batch number. May be {@code null}
     * @param expiryDate   the batch expiry date. May be {@code null}
     * @param manufacturer the batch manufacturer. May be {@code null}
     * @return the batch
     */
    private Entity checkBatch(String batchNumber, Date expiryDate, Party manufacturer) {
        List<Entity> batches = rules.getBatches(getProduct(), batchNumber, expiryDate, manufacturer);
        assertEquals(1, batches.size());
        Entity batch = batches.get(0);
        IMObjectBean bean = getBean(batch);
        if (batchNumber != null) {
            assertEquals(batchNumber, bean.getString("name"));
        } else {
            assertEquals(getProduct().getName(), bean.getString("name"));
        }
        assertEquals(manufacturer, bean.getTarget("manufacturer"));

        List<EntityLink> product = bean.getValues("product", EntityLink.class);
        assertEquals(1, product.size());
        IMObjectBean productBean = getBean(product.get(0));
        assertEquals(expiryDate, productBean.getDate("activeEndTime"));
        assertTrue(bean.getTargetRefs("stockLocations").contains(getStockLocation().getObjectReference()));
        return batch;
    }

    /**
     * Verifies a product has a single price with the expected cost and price.
     *
     * @param product the product
     * @param cost    the expected cost
     * @param price   the expected price
     * @param first   if {@code true}, this should be the first price
     * @param last    if {@code true}, the {@link ProductPrice#getToDate()} should be {@code null} otherwise it should
     *                be the same as the {@link ProductPrice#getFromDate()} as the next price
     * @param notes   the expected notes. May be {@code null}
     * @return the price
     */
    private ProductPrice checkPrice(Product product, int cost, int price, boolean first, boolean last, String notes) {
        return checkPrice(product, BigDecimal.valueOf(cost), BigDecimal.valueOf(price), first, last, notes);
    }

    /**
     * Verifies a product has a single price with the expected cost and price.
     *
     * @param product the product
     * @param cost    the expected cost
     * @param price   the expected price
     * @param first   if {@code true}, this should be the first price
     * @param last    if {@code true}, the {@link ProductPrice#getToDate()} should be {@code null} otherwise it should
     *                be the same as the {@link ProductPrice#getFromDate()} as the next price
     * @param notes   the expected notes. May be {@code null}
     * @return the price
     */
    private ProductPrice checkPrice(Product product, BigDecimal cost, BigDecimal price, boolean first, boolean last,
                                    String notes) {
        return checkPrice(product, cost, price, first, last, null, notes);
    }

    /**
     * Verifies a product has a single price with the expected cost and price.
     *
     * @param product      the product
     * @param cost         the expected cost
     * @param price        the expected price
     * @param first        if {@code true}, this should be the first price
     * @param last         if {@code true}, the {@link ProductPrice#getToDate()} should be {@code null} otherwise
     *                     it should be the same as the {@link ProductPrice#getFromDate()} as the next price
     * @param pricingGroup the pricing group. If non-null, the price must have the group, else it cannot have any group
     * @param notes        the expected notes. May be {@code null}
     * @return the price
     */
    private ProductPrice checkPrice(Product product, BigDecimal cost, BigDecimal price, boolean first, boolean last,
                                    String pricingGroup, String notes) {
        List<ProductPrice> prices = getUnitPrices(product, pricingGroup);
        int index = 0;
        int matchIndex = 0;
        ProductPrice match = null;
        for (ProductPrice p : prices) {
            IMObjectBean bean = getBean(p);
            if (MathRules.equals(bean.getBigDecimal("cost"), cost) && MathRules.equals(price, p.getPrice())) {
                match = p;
                matchIndex = index;
                if (first && matchIndex == 0) {
                    break;
                } else if (!first && !last && matchIndex < prices.size()) {
                    break;
                }
            }
            index++;
        }
        assertNotNull(match);
        if (first) {
            assertEquals(0, matchIndex);
        } else {
            assertNotEquals(0, matchIndex);
            assertEquals(prices.get(matchIndex - 1).getToDate(), match.getFromDate());
        }
        if (last) {
            assertNull(match.getToDate());
        } else {
            assertNotNull(match.getToDate());
        }
        IMObjectBean bean = getBean(match);
        assertEquals(notes, bean.getString("notes"));
        return match;
    }

    private List<ProductPrice> getUnitPrices(Product product, String pricingGroup) {
        List<ProductPrice> result = new ArrayList<>();
        List<ProductPrice> prices = ProductPriceTestHelper.getPrices(product, ProductArchetypes.UNIT_PRICE);
        for (ProductPrice price : prices) {
            if (hasPricingGroup(price, pricingGroup)) {
                result.add(price);
            }
        }
        return result;
    }

    /**
     * Determines if a price has a pricing group/no pricing group classification.
     *
     * @param price        the price
     * @param pricingGroup if non-null, the price must have the pricing group, else it cannot have any pricing group
     * @return {@code true} the price matches, otherwise {@code false}
     */
    private boolean hasPricingGroup(ProductPrice price, String pricingGroup) {
        boolean result = false;
        IMObjectBean bean = getBean(price);
        List<Lookup> groups = bean.getValues("pricingGroups", Lookup.class);
        if (pricingGroup == null) {
            result = groups.isEmpty();
        } else {
            for (Lookup lookup : groups) {
                if (pricingGroup.equals(lookup.getCode())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Verifies that the delivery status and received quantity on an order
     * and order item matches that expected.
     *
     * @param order     the order
     * @param orderItem the order item
     * @param status    the expected delivery status
     * @param quantity  the expected quantity
     */
    private void checkOrder(Act order, FinancialAct orderItem, DeliveryStatus status, BigDecimal quantity) {
        checkDeliveryStatus(order, status);
        checkReceivedQuantity(orderItem, quantity);
    }

    /**
     * Verfies that an order item has the expected received quantity.
     *
     * @param orderItem        the order item
     * @param receivedQuantity the expected
     */
    private void checkReceivedQuantity(FinancialAct orderItem, BigDecimal receivedQuantity) {
        orderItem = get(orderItem);
        IMObjectBean itemBean = getBean(orderItem);
        checkEquals(receivedQuantity, itemBean.getBigDecimal("receivedQuantity"));
    }

    /**
     * Verifies that the delivery status of an order matches that expected.
     *
     * @param order  the order
     * @param status the expected delivery status
     */
    private void checkDeliveryStatus(Act order, DeliveryStatus status) {
        order = get(order);
        assertEquals(status.toString(), order.getStatus2());
    }

    /**
     * Verifies that the <em>entityLink.productSupplier</em> associated with the supplier and product matches that
     * expected.
     *
     * @param product     the product
     * @param supplier    the supplier
     * @param packageSize the expected package size, or {@code -1} if the relationship shouldn't exist
     * @param nettPrice   the expected nett price
     * @param preferred   the expected preferred flag
     */
    private void checkProductSupplier(Product product, Party supplier, int packageSize, BigDecimal nettPrice,
                                      boolean preferred) {
        if (packageSize < 0) {
            assertNull(getProductSupplier(product, supplier, packageSize));
        } else {
            ProductSupplier ps = getProductSupplier(product, supplier, packageSize);
            assertNotNull(ps);
            checkEquals(nettPrice, ps.getNettPrice());
            assertEquals(preferred, ps.isPreferred());
        }
    }

    /**
     * Returns product supplier for the specified package size.
     *
     * @param product     the product
     * @param supplier    the supplier
     * @param packageSize the package size
     * @return the product supplier, or {@code null} if none is found
     */
    private ProductSupplier getProductSupplier(Product product, Party supplier, int packageSize) {
        ProductRules rules = new ProductRules(getArchetypeService(), getLookupService());
        product = get(product); // use latest instance
        return rules.getProductSupplier(product, supplier, null, packageSize, PACKAGE_UNITS);
    }

    /**
     * Sets the ignoreListPriceDecreases node on the practice.
     *
     * @param ignore if {@code true}, ignore list price decreases
     */
    private void setIgnoreListPriceDecreases(boolean ignore) {
        IMObjectBean bean = getBean(getPractice());
        bean.setValue("ignoreListPriceDecreases", ignore);
        bean.save();
    }

    /**
     * Creates and posts a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new delivery
     */
    protected FinancialAct postDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                        BigDecimal unitPrice, BigDecimal listPrice) {
        FinancialAct delivery = createDelivery(supplier, product, quantity, packageSize, unitPrice, listPrice);
        delivery.setStatus(ActStatus.POSTED);
        save(delivery);
        return delivery;
    }

    /**
     * Creates and posts a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     */
    private void postReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                             BigDecimal unitPrice, BigDecimal listPrice) {
        Act result = createReturn(supplier, product, quantity, packageSize, unitPrice, listPrice);
        result.setStatus(ActStatus.POSTED);
        save(result);
    }
}
