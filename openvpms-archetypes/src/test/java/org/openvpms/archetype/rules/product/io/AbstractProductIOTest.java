/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product.io;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.product.TestFixedPriceBuilder;
import org.openvpms.archetype.test.builder.product.TestMedicationProductBuilder;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.product.TestUnitPriceBuilder;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.product.PricingGroup.ALL;

/**
 * Product I/O helper methods.
 *
 * @author Tim Anderson
 */
public abstract class AbstractProductIOTest extends ArchetypeServiceTest {

    /**
     * The product factory.
     */
    @Autowired
    protected TestProductFactory productFactory;

    /**
     * Creates and saves a product with the specified name, printed name and prices.
     *
     * @param name        the product name
     * @param printedName the product printed name. May be {@code null}
     * @param prices      the prices
     * @return a new product
     */
    protected Product createProduct(String name, String printedName, ProductPrice... prices) {
        return newProduct(name, printedName, prices)
                .build();
    }

    /**
     * Creates a pre-populated medication product builder.
     *
     * @param name        the product name
     * @param printedName the product printed name. May be {@code null}
     * @param prices      the prices
     * @return a medication product builder
     */
    protected TestMedicationProductBuilder newProduct(String name, String printedName, ProductPrice... prices) {
        return productFactory.newMedication()
                .name(name)
                .printedName(printedName)
                .addPrices(prices);
    }

    /**
     * Creates a new {@link ProductData} from a {@link Product}.
     *
     * @param product the product
     * @return the corresponding product data
     */
    protected ProductData createProduct(Product product) {
        return createProduct(product, BigDecimal.ZERO, true);
    }

    /**
     * Creates a new {@link ProductData} from a {@link Product}.
     *
     * @param product    the product
     * @param taxRate    the product tax rate, expressed as a percentage
     * @param copyPrices if {@code true} add the product's prices, else exclude them
     * @return the corresponding product data
     */
    protected ProductData createProduct(Product product, BigDecimal taxRate, boolean copyPrices) {
        IArchetypeService service = getArchetypeService();
        ProductPriceRules rules = new ProductPriceRules(service);
        IMObjectBean bean = getBean(product);
        ProductData result = new ProductData(product.getId(), product.getName(), bean.getString("printedName"), taxRate,
                                             1);
        result.setReference(product.getObjectReference());
        if (copyPrices) {
            for (ProductPrice price : rules.getProductPrices(product, ProductArchetypes.FIXED_PRICE, ALL)) {
                PriceData priceDate = new PriceData(price, service);
                result.addPrice(priceDate);
            }
            for (ProductPrice price : rules.getProductPrices(product, ProductArchetypes.UNIT_PRICE, ALL)) {
                PriceData priceDate = new PriceData(price, service);
                result.addPrice(priceDate);
            }
        }
        return result;
    }

    /**
     * Verifies that a product contains the expected price.
     *
     * @param product  the product
     * @param expected the expected price
     */
    protected void checkPrice(Product product, ProductPrice expected) {
        IMObjectBean bean = getBean(expected);
        BigDecimal expectedPrice = expected.getPrice();
        BigDecimal expectedCost = bean.getBigDecimal("cost");
        BigDecimal expectedMarkup = bean.getBigDecimal("markup");
        BigDecimal expectedMaxDiscount = bean.getBigDecimal("maxDiscount");
        Date expectedFrom = expected.getFromDate();
        Date expectedTo = expected.getToDate();
        boolean isDefault = bean.hasNode("default") && bean.getBoolean("default");

        String shortName = expected.getArchetype();
        checkPrice(product, shortName, expectedPrice, expectedCost, expectedMarkup, expectedMaxDiscount, expectedFrom,
                   expectedTo, isDefault);
    }

    /**
     * Verifies that a product contains the expected fixed price.
     *
     * @param product             the product
     * @param expectedPrice       the expected price
     * @param expectedCost        the expected cost
     * @param expectedMarkup      the expected markup
     * @param expectedMaxDiscount the expected max discount
     * @param expectedFrom        the expected price start date
     * @param expectedTo          the expected price end date
     * @param expectedDefault     the expected default
     */
    protected void checkFixedPrice(Product product, BigDecimal expectedPrice, BigDecimal expectedCost,
                                   BigDecimal expectedMarkup, BigDecimal expectedMaxDiscount, Date expectedFrom,
                                   Date expectedTo, boolean expectedDefault) {
        checkPrice(product, ProductArchetypes.FIXED_PRICE, expectedPrice, expectedCost, expectedMarkup,
                   expectedMaxDiscount, expectedFrom, expectedTo, expectedDefault);
    }

    /**
     * Verifies that a product contains the expected unit price.
     *
     * @param product             the product
     * @param expectedPrice       the expected price
     * @param expectedCost        the expected cost
     * @param expectedMarkup      the expected markup
     * @param expectedMaxDiscount the expected max discount
     * @param expectedFrom        the expected price start date
     * @param expectedTo          the expected price end date
     */
    protected void checkUnitPrice(Product product, BigDecimal expectedPrice, BigDecimal expectedCost,
                                  BigDecimal expectedMarkup, BigDecimal expectedMaxDiscount, Date expectedFrom,
                                  Date expectedTo) {
        checkPrice(product, ProductArchetypes.UNIT_PRICE, expectedPrice, expectedCost, expectedMarkup,
                   expectedMaxDiscount, expectedFrom, expectedTo, false);
    }

    /**
     * Verifies that a product contains the expected price.
     *
     * @param product             the product
     * @param shortName           the price archetype short name
     * @param expectedPrice       the expected price
     * @param expectedCost        the expected cost
     * @param expectedMarkup      the expected markup
     * @param expectedMaxDiscount the expected maximum discount
     * @param expectedFrom        the expected price start date
     * @param expectedTo          the expected price end date
     * @param expectedDefault     the expected default. Only applies to fixed prices
     */
    protected void checkPrice(Product product, String shortName, BigDecimal expectedPrice, BigDecimal expectedCost,
                              BigDecimal expectedMarkup, BigDecimal expectedMaxDiscount, Date expectedFrom,
                              Date expectedTo, boolean expectedDefault) {
        boolean found = false;
        for (ProductPrice price : product.getProductPrices()) {
            IMObjectBean priceBean = getBean(price);
            if (price.getArchetype().equals(shortName)
                && price.getPrice().compareTo(expectedPrice) == 0
                && priceBean.getBigDecimal("cost").compareTo(expectedCost) == 0
                && priceBean.getBigDecimal("markup").compareTo(expectedMarkup) == 0
                && priceBean.getBigDecimal("maxDiscount").compareTo(expectedMaxDiscount) == 0
                && Objects.equals(expectedFrom, price.getFromDate())
                && Objects.equals(expectedTo, price.getToDate())) {

                found = !priceBean.hasNode("default") || priceBean.getBoolean("default") == expectedDefault;
                break;
            }
        }
        assertTrue("Failed to find price", found);
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    protected ProductPrice createFixedPrice(String price, String cost, String markup, String maxDiscount,
                                            Date from, Date to, boolean defaultPrice) {
        return newFixedPrice(price, cost, markup, maxDiscount, from, to, defaultPrice)
                .build();
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    protected ProductPrice createFixedPrice(String price, String cost, String markup, String maxDiscount,
                                            String from, String to, boolean defaultPrice) {
        return newFixedPrice(price, cost, markup, maxDiscount, defaultPrice)
                .dateRange(from, to)
                .build();
    }

    /**
     * Returns a pre-populated fixed price builder.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a fixed price builder
     */
    protected TestFixedPriceBuilder<?> newFixedPrice(String price, String cost, String markup, String maxDiscount,
                                                     Date from, Date to, boolean defaultPrice) {
        return newFixedPrice(price, cost, markup, maxDiscount, defaultPrice)
                .dateRange(from, to);
    }

    /**
     * Returns a pre-populated fixed price builder.
     *
     * @param price        the price
     * @param cost         the cost
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param defaultPrice the default price. If {@code true}, this is the default fixed price for the product
     * @return a fixed price builder
     */
    private TestFixedPriceBuilder<?> newFixedPrice(String price, String cost, String markup, String maxDiscount,
                                                   boolean defaultPrice) {
        return productFactory.newFixedPrice()
                .price(price)
                .cost(cost)
                .markup(markup)
                .maxDiscount(maxDiscount)
                .defaultPrice(defaultPrice);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new unit price
     */
    protected ProductPrice createUnitPrice(String price, String cost, String markup, String maxDiscount,
                                           Date from, Date to) {
        return newUnitPrice(price, cost, markup, maxDiscount, from, to)
                .build();
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new unit price
     */
    protected ProductPrice createUnitPrice(String price, String cost, String markup, String maxDiscount,
                                           String from, String to) {
        return newUnitPrice(price, cost, markup, maxDiscount)
                .dateRange(from, to)
                .build();
    }

    /**
     * Returns a pre-populated unit price builder.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a unit price builder
     */
    protected TestUnitPriceBuilder<?> newUnitPrice(String price, String cost, String markup, String maxDiscount,
                                                   Date from, Date to) {
        return newUnitPrice(price, cost, markup, maxDiscount)
                .dateRange(from, to);
    }

    /**
     * Returns a pre-populated unit price builder.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @return a unit price builder
     */
    private TestUnitPriceBuilder<?> newUnitPrice(String price, String cost, String markup, String maxDiscount) {
        return productFactory.newUnitPrice()
                .price(price).cost(cost).markup(markup).maxDiscount(maxDiscount);
    }
}
