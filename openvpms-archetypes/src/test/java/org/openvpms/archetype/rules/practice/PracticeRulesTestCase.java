/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.practice;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.practice.TestPracticeBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


/**
 * Tests the {@link PracticeRules} class, in conjunction with the
 * <em>archetypeService.save.party.organisationPractice.before</em>
 * rule.
 *
 * @author Tim Anderson
 */
public class PracticeRulesTestCase extends ArchetypeServiceTest {

    /**
     * The currencies.
     */
    @Autowired
    private Currencies currencies;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The rules.
     */
    private PracticeRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        rules = new PracticeRules(service, currencies);
    }

    /**
     * Tests the {@link PracticeRules#getLocations(Party)} method.
     */
    @Test
    public void testGetLocations() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.newLocation().active(false).build(); // inactive location
        Party practice = newPractice()
                .locations(location1, location2, location3)
                .build(false);

        List<Party> locations1 = rules.getLocations(practice);
        assertEquals(2, locations1.size());
        assertTrue(locations1.contains(location1));
        assertTrue(locations1.contains(location2));
        assertFalse(locations1.contains(location3)); // inactive locations aren't returned
    }

    /**
     * Tests the {@link PracticeRules#getDefaultLocation(Party)} method.
     */
    @Test
    public void testGetDefaultLocation() {
        Party practice = createPractice();

        assertNull(rules.getDefaultLocation(practice));

        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        IMObjectBean bean = getBean(practice);

        bean.addTarget("locations", location1);
        EntityRelationship rel2 = (EntityRelationship) bean.addTarget("locations", location2);

        Party defaultLocation = rules.getDefaultLocation(practice);
        assertNotNull(defaultLocation);

        // location can be one of location1, or location2, as default not
        // specified
        assertTrue(defaultLocation.equals(location1) || defaultLocation.equals(location2));

        // mark rel2 as the default
        EntityRelationshipHelper.setDefault(practice, "locations", rel2,
                                            getArchetypeService());
        assertEquals(location2, rules.getDefaultLocation(practice));
    }

    /**
     * Tests the {@link PracticeRules#isActivePractice(Party)} method.
     */
    @Test
    public void testIsActivePractice() {
        Party practice = practiceFactory.getPractice();

        assertTrue(rules.isActivePractice(practice));
        save(practice); // save should succeed

        Party newPractice = createPractice();

        // try and save the new practice. Should fail
        try {
            save(newPractice);
            fail("Expected save of another active practice to fail");
        } catch (Exception expected) {
            // do nothing
        }

        // mark the new practice inactive and save again. Should succeed.
        newPractice.setActive(false);
        save(newPractice);

        assertFalse(rules.isActivePractice(newPractice));

        // verify original practice still active
        assertTrue(rules.isActivePractice(practice));
    }

    /**
     * Tests the {@link PracticeRules#getEstimateExpiryDate(Date, Party)} method.
     */
    @Test
    public void testEstimateExpiryDate() {
        Party practice = createPractice();
        IMObjectBean bean = getBean(practice);
        bean.setValue("estimateExpiryUnits", null);

        Date startDate = TestHelper.getDate("2018-08-04");
        assertNull(rules.getEstimateExpiryDate(startDate, practice));

        bean.setValue("estimateExpiryPeriod", 1);
        bean.setValue("estimateExpiryUnits", "YEARS");
        assertEquals(TestHelper.getDate("2019-08-04"), rules.getEstimateExpiryDate(startDate, practice));

        bean.setValue("estimateExpiryPeriod", 6);
        bean.setValue("estimateExpiryUnits", "MONTHS");
        assertEquals(TestHelper.getDate("2019-02-04"), rules.getEstimateExpiryDate(startDate, practice));
    }

    /**
     * Tests the {@link PracticeRules#getPrescriptionExpiryDate(Date, Party)} method.
     */
    @Test
    public void testPrescriptionExpiryDate() {
        Party practice = createPractice();
        IMObjectBean bean = getBean(practice);
        bean.setValue("prescriptionExpiryUnits", null);

        Date startDate = TestHelper.getDate("2013-07-01");
        assertEquals(startDate, rules.getPrescriptionExpiryDate(startDate, practice));

        bean.setValue("prescriptionExpiryPeriod", 1);
        bean.setValue("prescriptionExpiryUnits", "YEARS");
        assertEquals(TestHelper.getDate("2014-07-01"), rules.getPrescriptionExpiryDate(startDate, practice));

        bean.setValue("prescriptionExpiryPeriod", 6);
        bean.setValue("prescriptionExpiryUnits", "MONTHS");
        assertEquals(TestHelper.getDate("2014-01-01"), rules.getPrescriptionExpiryDate(startDate, practice));
    }

    /**
     * Tests the {@link PracticeRules#getServiceUser(Party)} method.
     */
    @Test
    public void testGetServiceUser() {
        Party practice = createPractice();
        assertNull(rules.getServiceUser(practice));

        User user = userFactory.createUser();
        IMObjectBean bean = getBean(practice);
        bean.addTarget("serviceUser", user);

        assertEquals(user, rules.getServiceUser(practice));
    }

    /**
     * Tests the {@link PracticeRules#getCurrency(Party)} method.
     */
    @Test
    public void testGetCurrency() {
        Party practice = createPractice();
        Currency currency = rules.getCurrency(practice);
        assertEquals("AUD", currency.getCode());
        assertEquals(2, currency.getDefaultFractionDigits());
    }

    /**
     * Tests the {@link PracticeRules#useLocationProducts(Party)} method.
     */
    @Test
    public void testUseLocationProducts() {
        Party practice = createPractice();

        assertFalse(rules.useLocationProducts(practice));

        IMObjectBean bean = getBean(practice);
        bean.setValue("useLocationProducts", true);
        assertTrue(rules.useLocationProducts(practice));
    }

    /**
     * Tests the {@link PracticeRules#getBaseURL} method.
     */
    @Test
    public void testGetBaseUrl() {
        Party practice = createPractice();
        assertNull(rules.getBaseURL(practice));
        IMObjectBean bean = getBean(practice);
        bean.setValue("baseUrl", "http://localhost:8080/openvpms");
        assertEquals("http://localhost:8080/openvpms", rules.getBaseURL(practice));
    }

    /**
     * Tests the {@link PracticeRules#pluginsEnabled(Party)} method.
     */
    @Test
    public void testPluginsEnabled() {
        Party practice = createPractice();
        IMObjectBean bean = getBean(practice);
        bean.setValue("enablePlugins", true);
        assertTrue(rules.pluginsEnabled(practice));
        bean.setValue("enablePlugins", false);
        assertFalse(rules.pluginsEnabled(practice));
    }

    /**
     * Tests the {@link PracticeRules#isSMSEnabled(Party)} method.
     */
    @Test
    public void testSMSEnabled() {
        Party practice = createPractice();
        assertFalse(rules.isSMSEnabled(practice));

        Entity sms = create("entity.SMSConfigEmailGeneric", Entity.class);
        sms.setName(TestHelper.randomName("zms"));
        save(sms);

        IMObjectBean bean = getBean(practice);
        bean.setTarget("sms", sms.getObjectReference());

        assertTrue(rules.isSMSEnabled(practice));

        sms.setActive(false);
        save(sms);
        assertFalse(rules.isSMSEnabled(practice));
    }

    /**
     * Tests the {@link PracticeRules#getDefaultWeightUnits(org.openvpms.component.model.party.Party)} method.
     */
    @Test
    public void testDefaultWeightUnits() {
        Party practice = createPractice();
        assertEquals(WeightUnits.KILOGRAMS, rules.getDefaultWeightUnits(practice));
    }

    /**
     * Helper to create a new practice.
     *
     * @return a new practice
     */
    private Party createPractice() {
        return newPractice()
                .name(ValueStrategy.random("xpractice"))
                .currency("AUD")
                .build(false);
    }

    /**
     * Creates a new practice builder.
     *
     * @return a new practice builder
     */
    private TestPracticeBuilder newPractice() {
        // NOTE: can't use TestPracticeFactory.newPractice() as it enforces a single active instance
        return practiceFactory.updatePractice(create(PracticeArchetypes.PRACTICE, Party.class));
    }
}
