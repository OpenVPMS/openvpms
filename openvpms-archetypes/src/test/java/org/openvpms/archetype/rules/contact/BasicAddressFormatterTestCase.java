/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.contact;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.party.Contact;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link BasicAddressFormatter}.
 *
 * @author Tim Anderson
 */
public class BasicAddressFormatterTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link BasicAddressFormatter#format(Contact, boolean)} method.
     */
    @Test
    public void testFormat() {
        BasicAddressFormatter formatter = new BasicAddressFormatter(getArchetypeService(), getLookupService());

        Contact contact = TestHelper.createLocationContact("123 Smith St", "RESEARCH", "VIC", "3095");
        assertEquals("123 Smith St, Research Vic 3095", formatter.format(contact, true));
        assertEquals("123 Smith St\nResearch Vic 3095", formatter.format(contact, false));
    }
}
