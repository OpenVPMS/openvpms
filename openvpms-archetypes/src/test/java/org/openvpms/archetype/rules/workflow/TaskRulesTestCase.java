/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link TaskRules} class.
 *
 * @author Tim Anderson
 */
public class TaskRulesTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The rules.
     */
    private TaskRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new TaskRules(getArchetypeService());
    }

    /**
     * Tests the {@link TaskRules#getWorkList(Act)} method.
     */
    @Test
    public void testGetWorkList() {
        Entity list = schedulingFactory.createWorkList();
        Act task = schedulingFactory.newTask()
                .workList(list)
                .build(false);
        assertEquals(list, rules.getWorkList(task));
    }

    /**
     * Tests the {@link TaskRules#getWorkListView(Party, Entity)} method.
     */
    @Test
    public void testGetWorkListView() {
        Entity listA = schedulingFactory.createWorkList();
        Entity listB = schedulingFactory.createWorkList();
        Entity listC = schedulingFactory.createWorkList();
        Entity listD = schedulingFactory.createWorkList();

        Entity view1 = schedulingFactory.createWorkListView(listA, listB);
        Entity view2 = schedulingFactory.createWorkListView(listC);

        Party location1 = practiceFactory.newLocation()
                .workListViews(view1)
                .build();
        Party location2 = practiceFactory.newLocation()
                .workListViews(view2)
                .build();

        assertEquals(view1, rules.getWorkListView(location1, listA));
        assertEquals(view1, rules.getWorkListView(location1, listB));
        assertEquals(view2, rules.getWorkListView(location2, listC));

        assertNull(rules.getWorkListView(location2, listA));
        assertNull(rules.getWorkListView(location2, listB));
        assertNull(rules.getWorkListView(location2, listA));
        assertNull(rules.getWorkListView(location1, listD));
        assertNull(rules.getWorkListView(location2, listD));
    }

    /**
     * Tests the {@link TaskRules#hasTaskType(Entity, Entity)} method.
     */
    @Test
    public void testHasTaskType() {
        Entity taskType = schedulingFactory.createTaskType();
        Entity worklist1 = schedulingFactory.newWorkList()
                .taskTypes(taskType)
                .build();
        Entity worklist2 = schedulingFactory.createWorkList();

        assertTrue(rules.hasTaskType(worklist1, taskType));
        assertFalse(rules.hasTaskType(worklist2, taskType));
    }
}
