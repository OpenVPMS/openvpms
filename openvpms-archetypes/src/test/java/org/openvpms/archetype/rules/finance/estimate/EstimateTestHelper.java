/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.estimate;

import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;

/**
 * Estimate test helper methods.
 *
 * @author Tim Anderson
 */
public class EstimateTestHelper {

    /**
     * Creates an estimate.
     *
     * @param customer the customer
     * @param items    the estimate items
     * @return a new estimate
     */
    public static Act createEstimate(Party customer, Act... items) {
        Act estimate = (Act) TestHelper.create(EstimateArchetypes.ESTIMATE);
        IMObjectBean bean = new IMObjectBean(estimate);
        bean.setTarget("customer", customer);
        BigDecimal lowTotal = BigDecimal.ZERO;
        BigDecimal highTotal = BigDecimal.ZERO;
        int sequence = 0;
        for (Act item : items) {
            IMObjectBean itemBean = new IMObjectBean(item);
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            relationship.setSequence(sequence++);
            item.addActRelationship(relationship);
            highTotal = highTotal.add(itemBean.getBigDecimal("highTotal"));
            lowTotal = lowTotal.add(itemBean.getBigDecimal("lowTotal"));
        }
        bean.setValue("highTotal", highTotal);
        bean.setValue("lowTotal", lowTotal);
        return estimate;
    }

    /**
     * Creates an estimate item.
     *
     * @param patient    the patient
     * @param product    the product
     * @param fixedPrice the fixed price
     * @return a new estimate item
     */
    public static Act createEstimateItem(Party patient, Product product, BigDecimal fixedPrice) {
        IMObjectBean bean = createEstimateItem(patient, product);
        bean.setValue("fixedPrice", fixedPrice);
        bean.setValue("lowTotal", fixedPrice);
        bean.setValue("highTotal", fixedPrice);
        return (Act) bean.getObject();
    }

    /**
     * Creates an estimate item.
     *
     * @param patient   the patient
     * @param product   the product
     * @param quantity  the quantity
     * @param unitPrice the unit price
     * @return a new estimation item
     */
    public static Act createEstimateItem(Party patient, Product product, BigDecimal quantity, BigDecimal unitPrice) {
        IMObjectBean bean = createEstimateItem(patient, product);
        bean.setValue("highQty", quantity);
        bean.setValue("highUnitPrice", unitPrice);
        bean.deriveValues();
        return (Act) bean.getObject();
    }

    /**
     * Creates an estimate item.
     *
     * @param patient   the patient
     * @param product   the product
     * @param template  the template
     * @param group     the template expansion group. Ignored if there is no template
     * @param quantity  the quantity
     * @param unitPrice the unit price
     * @return a new estimation item
     */
    public static Act createEstimateItem(Party patient, Product product, Product template, int group,
                                         BigDecimal quantity, BigDecimal unitPrice) {
        Act act = createEstimateItem(patient, product, quantity, unitPrice);
        if (template != null) {
            IMObjectBean bean = new IMObjectBean(act);
            bean.setTarget("template", template);
            Relationship relationship = bean.getObject("template", Relationship.class);
            IMObjectBean relBean = new IMObjectBean(relationship);
            relBean.setValue("group", group);
        }
        return act;
    }

    /**
     * Creates an estimate item.
     *
     * @param patient the patient
     * @param product the product
     * @return a bean wrapping the estimate item
     */
    private static IMObjectBean createEstimateItem(Party patient, Product product) {
        Act item = (Act) TestHelper.create(EstimateArchetypes.ESTIMATE_ITEM);
        IMObjectBean bean = new IMObjectBean(item);
        bean.setTarget("patient", patient);
        bean.setTarget("product", product);
        return bean;
    }

}
