/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.stock;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;

import static org.openvpms.archetype.rules.stock.StockArchetypes.STOCK_ADJUST;
import static org.openvpms.archetype.rules.stock.StockArchetypes.STOCK_ADJUST_ITEM;
import static org.openvpms.archetype.rules.stock.StockArchetypes.STOCK_TRANSFER;
import static org.openvpms.archetype.rules.stock.StockArchetypes.STOCK_TRANSFER_ITEM;


/**
 * Tests the {@link StockUpdater} class.
 *
 * @author Tim Anderson
 */
public class StockUpdaterTestCase extends AbstractStockTest {

    /**
     * The updater.
     */
    private StockUpdater updater;

    /**
     * The product.
     */
    private Product product;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Verifies that {@link StockUpdater#update} updates stock for posted <em>act.stockTransfer</em> acts.
     */
    @Test
    public void testTransfer() {
        BigDecimal quantity = new BigDecimal(100);
        Party xferLocation = createStockLocation();
        Act act = create(STOCK_TRANSFER, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("stockLocation", stockLocation);
        bean.setTarget("to", xferLocation);
        Act item = create(STOCK_TRANSFER_ITEM, Act.class);
        IMObjectBean itemBean = getBean(item);
        ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
        item.addActRelationship(relationship);
        itemBean.setTarget("product", product);
        itemBean.setValue("quantity", quantity);
        save(act, item);

        // verify transfer doesn't take place till the act is posted
        updater.update(act);
        checkEquals(BigDecimal.ZERO, getStock(stockLocation));
        checkEquals(BigDecimal.ZERO, getStock(xferLocation));

        // post the transfer and transfer again
        bean.setValue("status", ActStatus.POSTED);
        bean.save();
        updater.update(act);

        // verify stock at the from and to locations. Note that stock may go negative
        checkEquals(quantity.negate(), getStock(stockLocation));
        checkEquals(quantity, getStock(xferLocation));

        // verify subsequent save doesn't change the stock
        bean.save();
        checkEquals(quantity.negate(), getStock(stockLocation));
        checkEquals(quantity, getStock(xferLocation));
    }

    /**
     * Verifies that {@link StockUpdater#update} updates stock for posted <em>act.stockAdjust</em> acts.
     */
    @Test
    public void testAdjust() {
        BigDecimal quantity = new BigDecimal(100);
        Act act = create(STOCK_ADJUST, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("stockLocation", stockLocation);
        Act item = create(STOCK_ADJUST_ITEM, Act.class);
        IMObjectBean itemBean = getBean(item);
        bean.addTarget("items", item, "stockAdjust");
        itemBean.setTarget("product", product);
        itemBean.setValue("quantity", quantity);

        save(act, item);

        // verify stock is not adjusted till the act is posted
        updater.update(act);
        checkEquals(BigDecimal.ZERO, getStock(stockLocation));

        // post the act and transfer again
        bean.setValue("status", ActStatus.POSTED);
        bean.save();
        updater.update(act);

        // verify stock adjusted
        checkEquals(quantity, getStock(stockLocation));
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        updater = new StockUpdater(getArchetypeService());
        product = TestHelper.createProduct();
        stockLocation = createStockLocation();
    }

    /**
     * Returns the stock in hand for the product and specified stock location.
     *
     * @param location the stock location
     * @return the stock in hand
     */
    private BigDecimal getStock(Party location) {
        return getStock(location, product);
    }

}
