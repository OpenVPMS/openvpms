/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link DemographicUpdater} class.
 *
 * @author Tim Anderson
 */
public class DemographicUpdaterTestCase extends ArchetypeServiceTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Tests the {@link DemographicUpdater#evaluate(IMObject, Lookup)} method
     * without a node name.
     */
    @Test
    public void testEvaluate() {
        Lookup desex = productFactory.createDemographicUpdate("Desexed", null, "party:setPatientDesexed(.)");
        Party patient = patientFactory.createPatient();
        checkEvaluateDesex(desex, patient, patient);

        Lookup deceased = productFactory.createDemographicUpdate("Deceased", null,
                                                                 "openvpms:set(., 'deceased', true())");
        checkEvaluateDeceased(deceased, patient, patient);
    }

    /**
     * Tests the {@link DemographicUpdater#evaluate(IMObject, Lookup)} method
     * with a node name.
     */
    @Test
    public void testEvaluateWithNode() {
        Party patient = patientFactory.createPatient();
        Act invoiceItem = accountFactory.newInvoiceItem()
                .patient(patient)
                .build(false);

        Lookup desex = productFactory.createDemographicUpdate("Desexed", "patient.entity",
                                                              "party:setPatientDesexed(.)");
        checkEvaluateDesex(desex, invoiceItem, patient);

        Lookup deceased = productFactory.createDemographicUpdate("Deceased", "patient.entity",
                                                                 "openvpms:set(., 'deceased', true())");
        checkEvaluateDeceased(deceased, invoiceItem, patient);
    }

    /**
     * Tests the
     * {@link DemographicUpdater#evaluate(IMObject, Collection<Lookup>)} method.
     */
    @Test
    public void testEvaluateCollection() {
        // Verify that a patient is flagged as desexed and deceased.
        Lookup desex = productFactory.createDemographicUpdate("Desexed", null, "party:setPatientDesexed(.)");
        Lookup deceased = productFactory.createDemographicUpdate("Deceased", null, "party:setPatientDeceased(.)");

        List<Lookup> lookups = Arrays.asList(desex, deceased);
        Party patient = patientFactory.createPatient();
        DemographicUpdater updater = new DemographicUpdater(getArchetypeService());
        updater.evaluate(patient, lookups);

        IMObjectBean bean = getBean(get(patient));
        assertTrue(bean.getBoolean("desexed"));
        assertTrue(bean.getBoolean("deceased"));
    }

    /**
     * Verifies that a desexing lookup evaluates correctly to desex a patient.
     *
     * @param desex   the desexing <em>lookup.demographicUpdate</em>
     * @param context the context object to evaluate against
     * @param patient the patient that will be updated
     */
    private void checkEvaluateDesex(Lookup desex, IMObject context, Party patient) {
        IMObjectBean bean = getBean(patient);
        assertFalse(bean.getBoolean("desexed"));

        DemographicUpdater updater = new DemographicUpdater(getArchetypeService());
        updater.evaluate(context, desex);

        patient = get(patient);
        bean = getBean(patient);
        assertTrue(bean.getBoolean("desexed"));
    }

    /**
     * Verifies that a deceased lookup evaluates correctly to mark a patient deceased.
     *
     * @param deceased the deceased <em>lookup.demographicUpdate</em>
     * @param context  the context object to evaluate against
     * @param patient  the patient that will be updated
     */
    private void checkEvaluateDeceased(Lookup deceased, IMObject context, Party patient) {
        IMObjectBean bean = getBean(patient);
        assertFalse(bean.getBoolean("deceased"));

        DemographicUpdater updater = new DemographicUpdater(getArchetypeService());
        updater.evaluate(context, deceased);

        patient = get(patient);
        bean = getBean(patient);
        assertTrue(bean.getBoolean("deceased"));
    }

}
