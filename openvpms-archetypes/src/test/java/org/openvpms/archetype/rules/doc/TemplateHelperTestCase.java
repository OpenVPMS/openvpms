/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link TemplateHelper} class.
 *
 * @author Tim Anderson
 */
public class TemplateHelperTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The helper to test.
     */
    private TemplateHelper helper;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        helper = new TemplateHelper(getArchetypeService());
    }

    /**
     * Tests the {@link TemplateHelper#getDocument(String)} method.
     */
    @Test
    public void testGetDocument() {
        String filename = UUID.randomUUID() + ".jrxml";
        assertNull(helper.getDocument(filename));

        Document document1 = documentFactory.createJRXML(filename);
        assertNull(helper.getDocument(filename));  // not associated with a template

        Entity template1 = documentFactory.newTemplate()
                .name("A")
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(document1)
                .active(true)
                .build();
        assertEquals(document1, helper.getDocument(filename));

        Document document2 = documentFactory.createJRXML(filename);
        Entity template2 = documentFactory.newTemplate()
                .name("B")
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(document2)
                .active(true)
                .build();
        assertEquals(document1, helper.getDocument(filename)); // document with the lowest id is returned

        template1.setActive(false);
        save(template1);
        assertEquals(document2, helper.getDocument(filename));

        template2.setActive(false);
        save(template2);
        assertNull(helper.getDocument(filename));
    }

    /**
     * Tests the {@link TemplateHelper#getDocumentAct(String)} method.
     */
    @Test
    public void testGetDocumentAct() {
        String filename = UUID.randomUUID() + ".jrxml";
        assertNull(helper.getDocumentAct(filename));

        Document document1 = documentFactory.createJRXML(filename);
        assertNull(helper.getDocumentAct(filename));  // not associated with a template

        Entity template1 = documentFactory.newTemplate()
                .name("A")
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(document1)
                .active(true)
                .build();
        DocumentAct act1 = helper.getDocumentAct(template1);
        assertNotNull(act1);
        assertEquals(document1.getObjectReference(), act1.getDocument());

        Document document2 = documentFactory.createJRXML(filename);
        Entity template2 = documentFactory.newTemplate()
                .name("B")
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(document2)
                .active(true)
                .build();
        DocumentAct act2 = helper.getDocumentAct(template2);
        assertNotNull(act2);
        assertEquals(document2.getObjectReference(), act2.getDocument());

        assertEquals(act1, helper.getDocumentAct(filename)); // act with the lowest id is returned

        template1.setActive(false);
        save(template1);
        assertEquals(act2, helper.getDocumentAct(filename));

        template2.setActive(false);
        save(template2);
        assertNull(helper.getDocumentAct(filename));
    }

    /**
     * Tests the {@link TemplateHelper#getTemplateForType(String)} method.
     */
    @Test
    public void testGetTemplateForType() {
        String type = lookupFactory.newLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE)
                .uniqueCode()
                .build()
                .getCode();
        assertNull(helper.getTemplateForType(type));
        Entity template1 = documentFactory.newTemplate()
                .name("A")
                .type(type)
                .blankDocument()
                .active(true)
                .build();
        assertEquals(template1, helper.getTemplateForType(type));

        Entity template2 = documentFactory.newTemplate()
                .name("B")
                .type(type)
                .blankDocument()
                .active(true)
                .build();
        assertEquals(template1, helper.getTemplateForType(type)); // template with the lowest id returned

        template1.setActive(false);
        save(template1);
        assertEquals(template2, helper.getTemplateForType(type));

        template2.setActive(false);
        save(template2);
        assertNull(helper.getTemplateForType(type));
    }

    /**
     * Tests the {@link TemplateHelper#getTemplateForType(String, Party)} method.
     */
    @Test
    public void testGetOrganisationTemplateForType() {
        Party location = practiceFactory.createLocation();
        String typeA = lookupFactory.newLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE)
                .uniqueCode()
                .build()
                .getCode();
        String typeB = lookupFactory.newLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE)
                .uniqueCode()
                .build()
                .getCode();
        assertNull(helper.getTemplateForType(typeA, location));
        assertNull(helper.getTemplateForType(typeB, location));
        Entity template1 = documentFactory.newTemplate()
                .name("template1")
                .type(typeA)
                .blankDocument()
                .active(true)
                .build();
        assertNull(helper.getTemplateForType(typeA, location));
        documentFactory.updateTemplate(template1)
                .printer().printer("printer1").location(location).add()
                .build();
        assertEquals(template1, helper.getTemplateForType(typeA, location));

        Entity template2 = documentFactory.newTemplate()
                .name("template2")
                .type(typeB)
                .blankDocument()
                .active(true)
                .printer().printer("printer1").location(location).add()
                .build();
        assertEquals(template2, helper.getTemplateForType(typeB, location));

        // now link another template with the same type as template1 to the location. Verify template1 is returned,
        // as it has the lower id
        Entity template3 = documentFactory.newTemplate()
                .name("template3")
                .type(typeA)
                .blankDocument()
                .active(true)
                .printer().printer("printer2").location(location).add()
                .build();
        assertEquals(template1, helper.getTemplateForType(typeA, location));

        // deactivate template1, and verify template3 is now returned for typeA
        template1.setActive(false);
        save(template1);
        assertEquals(template3, helper.getTemplateForType(typeA, location));

        // deactivate template3 and verify nothing returned for typeA
        template3.setActive(false);
        save(template3);
        assertNull(helper.getTemplateForType(typeA, location));
    }
}
