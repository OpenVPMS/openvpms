/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.junit.Assert;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link ChargeItemDocumentLinker} class.
 *
 * @author Tim Anderson
 */
public class ChargeItemDocumentLinkerTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests linking of charge item documents to a charge item.
     */
    @Test
    public void testSimpleLink() {
        Party patient = TestHelper.createPatient();
        Entity template1 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_LETTER);
        Product product1 = createProduct(template1);
        User clinician = TestHelper.createClinician();
        Party location = TestHelper.createLocation();
        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        Product product2 = createProduct(template2);
        Product product3 = createProduct(template1, template2);
        Product product4 = createProduct();
        FinancialAct item = createItem(patient, product1, clinician);
        save(item);
        ChargeItemDocumentLinker linker = new ChargeItemDocumentLinker(item, getArchetypeService());
        linker.link();

        IMObjectBean bean = getBean(item);
        List<Act> documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act document1 = documents.get(0);
        assertTrue(TypeHelper.isA(document1, PatientArchetypes.DOCUMENT_FORM));
        checkDocument(document1, patient, product1, template1, clinician, item);

        bean.setTarget("product", product2);
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act document2 = documents.get(0);
        assertTrue(TypeHelper.isA(document2, PatientArchetypes.DOCUMENT_LETTER));
        checkDocument(document2, patient, product2, template2, clinician, item);

        // now test a product with 2 documents
        bean.setTarget("product", product3);
        linker.prepare(changes);
        saveInTxn(changes);
        documents = bean.getTargets("documents", Act.class);
        assertEquals(2, documents.size());
        Act doc3template1 = getDocument(documents, template1);
        Act doc3template2 = getDocument(documents, template2);
        assertTrue(TypeHelper.isA(doc3template1, PatientArchetypes.DOCUMENT_FORM));
        assertTrue(TypeHelper.isA(doc3template2, PatientArchetypes.DOCUMENT_LETTER));
        checkDocument(doc3template1, patient, product3, template1, clinician, item);
        checkDocument(doc3template2, patient, product3, template2, clinician, item);

        // now test a product with no documents
        bean.setTarget("product", product4);
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(0, documents.size());
    }

    /**
     * Verifies that the document acts are recreated if the charge item product, patient, author, or clinician changes.
     */
    @Test
    public void testRecreateDocumentActsForDifferentParticipant() {
        User author2 = TestHelper.createUser();
        User clinician1 = TestHelper.createClinician();
        User clinician2 = TestHelper.createClinician();
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        Entity template1 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_LETTER);
        Product product1 = createProduct(template1);
        Product product2 = createProduct(template2);

        /// create an item, and link a single document to it
        FinancialAct item = createItem(patient1, product1, clinician1);
        ChargeItemDocumentLinker linker = new ChargeItemDocumentLinker(item, getArchetypeService());
        linker.link();

        // verify the document matches that expected
        IMObjectBean bean = getBean(item);
        List<Act> documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act document1 = documents.get(0);
        checkDocument(document1, patient1, product1, template1, clinician1, item);

        // perform the link again, and verify that act is the same
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act same = documents.get(0);
        assertEquals(document1, same);

        // now change the product, and verify a new document has been created
        bean.setTarget("product", product2);
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act different1 = documents.get(0);
        assertNotEquals(document1, different1);
        assertNull(get(document1));  // should have been deleted
        checkDocument(different1, patient1, product2, template2, clinician1, item);

        // now change the patient, and verify a new document has been created
        bean.setTarget("patient", patient2);
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act different2 = documents.get(0);
        assertNotEquals(different1, different2);
        assertNull(get(different1));  // should have been deleted
        checkDocument(different2, patient2, product2, template2, clinician1, item);

        // now change the clinician, and verify a new document has been created
        bean.setTarget("clinician", clinician2);
        linker.link();
        documents = bean.getTargets("documents", Act.class);
        assertEquals(1, documents.size());
        Act different3 = documents.get(0);
        assertNotEquals(different2, different3);
        assertNull(get(different2));  // should have been deleted
        checkDocument(different3, patient2, product2, template2, clinician2, item);
    }

    /**
     * Verifies that the document acts that are recreated if the clinician changes, are correctly removed when they
     * are linked to an event.
     */
    @Test
    public void testRecreateDocumentsLinkedToEvent() {
        Party patient = TestHelper.createPatient();
        Entity template1 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_LETTER);
        Product product = createProduct(template1, template2);
        User clinician1 = TestHelper.createClinician();
        User clinician2 = TestHelper.createClinician();

        // create an invoice item with a product that is linked to two documents
        FinancialAct item = createItem(patient, product, clinician1);
        IMObjectBean bean = getBean(item);
        bean.save();

        // link the product documents to the item
        ChargeItemDocumentLinker linker = new ChargeItemDocumentLinker(item, getArchetypeService());
        linker.link();

        List<Act> documents = bean.getTargets("documents", Act.class);
        assertEquals(2, documents.size());

        // link the documents to an event
        MedicalRecordRules rules = new MedicalRecordRules(getArchetypeService());
        Act event = rules.getEventForAddition(patient, new Date(), null);
        for (Act document : documents) {
            rules.linkMedicalRecords(event, document);
        }

        // verify the documents have been linked
        event = get(event);
        assertEquals(2, event.getSourceActRelationships().size());

        // change the invoice item's clinician and re-link the documents
        bean.setTarget("clinician", clinician2);
        linker.link();

        // the event shouldn't have any relationships to documents any more
        event = get(event);
        assertEquals(0, event.getSourceActRelationships().size());
    }

    /**
     * Helper to create an invoice item.
     *
     * @param patient   the patient
     * @param product   the product
     * @param clinician the clinician
     * @return a new invoice item
     */
    private FinancialAct createItem(Party patient, Product product, User clinician) {
        FinancialAct item = FinancialTestHelper.createChargeItem(CustomerAccountArchetypes.INVOICE_ITEM, patient,
                                                                 product, BigDecimal.ONE);
        IMObjectBean bean = getBean(item);
        bean.setTarget("clinician", clinician);
        return item;
    }

    /**
     * Verifies a document act matches that expected.
     *
     * @param document  the document act
     * @param patient   the expected patient
     * @param product   the expected product
     * @param template  the expected template
     * @param clinician the expected clinician
     * @param item      the expected item
     */
    private void checkDocument(Act document, Party patient, Product product, Entity template, User clinician,
                               FinancialAct item) {
        // verify the start time is the same as the invoice item start time
        assertEquals(0, DateRules.compareTo(item.getActivityStartTime(), document.getActivityStartTime(), true));

        // check participants
        IMObjectBean docBean = getBean(document);
        assertEquals(patient, docBean.getTarget("patient"));
        assertEquals(template, docBean.getTarget("documentTemplate"));
        assertEquals(product, docBean.getTarget("product"));

        assertEquals(clinician, docBean.getTarget("clinician"));
    }

    /**
     * Saves the changes in a transaction.
     *
     * @param changes the changes
     */
    private void saveInTxn(final PatientHistoryChanges changes) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(status -> {
            changes.save();
            return null;
        });
    }


    /**
     * Returns the document with the associated template, failing if one isn't found.
     *
     * @param documents the documents
     * @param template  the template
     * @return the corresponding document
     */
    private Act getDocument(List<Act> documents, Entity template) {
        Reference templateRef = template.getObjectReference();
        for (Act document : documents) {
            IMObjectBean bean = getBean(document);
            if (Objects.equals(templateRef, bean.getTargetRef("documentTemplate"))) {
                return document;
            }
        }
        Assert.fail("Template not found");
        return null;
    }

    /**
     * Creates and saves a new product.
     *
     * @param templates the document templates
     * @return a new product
     */
    private Product createProduct(Entity... templates) {
        Product product = create(ProductArchetypes.MEDICATION, Product.class);
        IMObjectBean bean = getBean(product);
        bean.setValue("name", "XProduct");
        for (Entity template : templates) {
            bean.addTarget("documents", template);
        }
        List<IMObject> objects = new ArrayList<>();
        objects.add(product);
        objects.addAll(Arrays.asList(templates));
        save(objects);
        return product;
    }
}
