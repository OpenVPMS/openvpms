/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.tax.TaxRules;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


/**
 * Tests the {@link OrderRules} class.
 *
 * @author Tim Anderson
 */
public class OrderRulesTestCase extends AbstractSupplierTest {

    /**
     * The rules.
     */
    private OrderRules rules;


    /**
     * Tests the {@link OrderRules#getDeliveryStatus(FinancialAct)} method.
     */
    @Test
    public void testGetDeliveryStatus() {
        BigDecimal two = new BigDecimal("2.0");
        BigDecimal three = new BigDecimal("3.0");

        FinancialAct act = create(SupplierArchetypes.ORDER_ITEM, FinancialAct.class);
        assertEquals(DeliveryStatus.PENDING, rules.getDeliveryStatus(act));

        checkDeliveryStatus(act, three, ZERO, ZERO, DeliveryStatus.PENDING);
        checkDeliveryStatus(act, three, three, ZERO, DeliveryStatus.FULL);
        checkDeliveryStatus(act, three, two, ZERO, DeliveryStatus.PART);
        checkDeliveryStatus(act, three, two, ONE, DeliveryStatus.FULL);
        checkDeliveryStatus(act, three, ZERO, three, DeliveryStatus.FULL);
    }

    /**
     * Tests the {@link OrderRules#copyOrder(FinancialAct, String)} method.
     */
    @Test
    public void testCopyOrder() {
        BigDecimal quantity = new BigDecimal(50);
        int packageSize = 10;
        FinancialAct orderItem = newOrderItem(getProduct(), quantity, packageSize, ONE)
                .receivedQuantity(25)
                .cancelledQuantity(10)
                .build();
        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        order.setStatus2(DeliveryStatus.PART.toString());
        save(order);

        FinancialAct copy = rules.copyOrder(order, "Copy");
        assertTrue(copy.isA(SupplierArchetypes.ORDER));
        assertFalse(copy.isCredit());
        assertEquals(ActStatus.IN_PROGRESS, copy.getStatus());
        assertNotEquals(copy, order);

        IMObjectBean bean = getBean(copy);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        FinancialAct copyItem = items.get(0);
        assertTrue(copyItem.isA(SupplierArchetypes.ORDER_ITEM));
        assertFalse(copyItem.isCredit());
        assertNotEquals(copyItem, orderItem);
        checkEquals(quantity, copyItem.getQuantity());
        IMObjectBean copyBean = getBean(copyItem);
        assertEquals(0, copyBean.getInt("receivedQuantity"));
        assertEquals(0, copyBean.getInt("cancelledQuantity"));

        assertEquals(DeliveryStatus.PENDING.toString(), copy.getStatus2());
        assertEquals("Copy", copy.getTitle());
    }

    /**
     * Tests the {@link OrderRules#createDeliveryItem(FinancialAct)} method.
     */
    @Test
    public void testCreateDeliveryItem() {
        BigDecimal quantity = new BigDecimal(50);
        BigDecimal received = new BigDecimal(40);
        BigDecimal cancelled = new BigDecimal(4);
        BigDecimal expectedQuantity = new BigDecimal(6);

        int packageSize = 10;

        FinancialAct orderItem = newOrderItem(getProduct(), quantity, packageSize, ONE)
                .receivedQuantity(received)
                .cancelledQuantity(cancelled)
                .build();
        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order);

        FinancialAct item = rules.createDeliveryItem(orderItem);
        assertTrue(item.isA(SupplierArchetypes.DELIVERY_ITEM));
        assertFalse(item.isCredit());

        checkEquals(expectedQuantity, item.getQuantity());

        // the delivery item shouldn't have any relationships
        assertTrue(item.getSourceActRelationships().isEmpty());
    }

    /**
     * Tests the {@link OrderRules#createReturnItem} method.
     */
    @Test
    public void testCreateReturnItem() {
        BigDecimal quantity = new BigDecimal(50);
        BigDecimal received = new BigDecimal(40);
        BigDecimal cancelled = new BigDecimal(4);
        int packageSize = 10;

        FinancialAct orderItem = newOrderItem(getProduct(), quantity, packageSize, ONE)
                .receivedQuantity(received)
                .cancelledQuantity(cancelled)
                .build();
        assertFalse(orderItem.isCredit());
        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order);

        FinancialAct item = rules.createReturnItem(orderItem);

        assertTrue(item.isA(SupplierArchetypes.RETURN_ITEM));
        assertTrue(item.isCredit());
        checkEquals(received, item.getQuantity());

        // the return item shouldn't have any relationships
        assertTrue(item.getSourceActRelationships().isEmpty());
    }

    /**
     * Tests the {@link OrderRules#invoiceSupplier(Act)} method.
     */
    @Test
    public void testInvoiceSupplier() {
        BigDecimal quantity = new BigDecimal(50);
        int packageSize = 10;
        BigDecimal unitPrice = BigDecimal.ONE;
        BigDecimal total = quantity.multiply(unitPrice);

        Product product = getProduct();
        FinancialAct orderItem = createOrderItem(product, quantity, packageSize, unitPrice);

        FinancialAct delivery = createDelivery(getSupplier(), product, quantity, packageSize, unitPrice, orderItem);
        delivery.setStatus(ActStatus.POSTED);
        save(delivery);

        FinancialAct invoice = rules.invoiceSupplier(delivery);
        assertTrue(invoice.isA(SupplierArchetypes.INVOICE));
        assertEquals(ActStatus.IN_PROGRESS, invoice.getStatus());
        assertFalse(invoice.isCredit());

        IMObjectBean bean = getBean(invoice);
        assertEquals(1, invoice.getSourceActRelationships().size()); // only linked to an item
        FinancialAct item = bean.getTarget("items", FinancialAct.class);
        assertTrue(item.isA(SupplierArchetypes.INVOICE_ITEM));
        checkEquals(total, item.getTotal());

        // verify there is a relationship from the delivery to the invoice
        IMObjectBean deliveryBean = getBean(delivery);
        List<Act> invoices = deliveryBean.getTargets("invoice", Act.class);
        assertEquals(1, invoices.size());
        assertEquals(invoice, invoices.get(0));

        try {
            rules.invoiceSupplier(delivery);
            fail("Expected invoicing to fail");
        } catch (IllegalStateException exception) {
            // the expected
        }
    }

    /**
     * Tests the {@link OrderRules#creditSupplier(Act)} method.
     */
    @Test
    public void testCreditSupplier() {
        BigDecimal quantity = new BigDecimal(50);
        int packageSize = 10;
        BigDecimal unitPrice = BigDecimal.ONE;

        Product product = getProduct();
        FinancialAct orderItem = createOrderItem(product, quantity, packageSize, unitPrice);
        Act orderReturn = createReturn(getSupplier(), product, quantity, packageSize, unitPrice, orderItem);
        orderReturn.setStatus(ActStatus.POSTED);
        save(orderReturn);

        FinancialAct credit = rules.creditSupplier(orderReturn);
        assertTrue(credit.isA(SupplierArchetypes.CREDIT));
        assertTrue(credit.isCredit());
        assertEquals(ActStatus.IN_PROGRESS, credit.getStatus());

        assertEquals(1, credit.getSourceActRelationships().size());
        IMObjectBean bean = getBean(credit);
        List<FinancialAct> acts = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, acts.size());

        FinancialAct item = acts.get(0);
        assertTrue(item.isA(SupplierArchetypes.CREDIT_ITEM));
        assertTrue(item.isCredit());

        // verify there is a relationship from the return to the credit
        IMObjectBean returnBean = getBean(orderReturn);
        List<Act> credits = returnBean.getTargets("returnCredit", Act.class);
        assertEquals(1, credits.size());
        assertEquals(credit, credits.get(0));

        try {
            rules.creditSupplier(orderReturn);
            fail("Expected crediting to fail");
        } catch (IllegalStateException exception) {
            // the expected
        }
    }

    /**
     * Tests the {@link OrderRules#reverseDelivery(Act)} method.
     */
    @Test
    public void testReverseDelivery() {
        BigDecimal quantity = new BigDecimal(50);
        BigDecimal unitPrice = BigDecimal.ONE;
        int packageSize = 10;

        Product product = getProduct();
        FinancialAct orderItem = createOrderItem(product, quantity, packageSize, unitPrice);
        Act delivery = createDelivery(getSupplier(), product, quantity, packageSize, unitPrice, orderItem);
        delivery.setStatus(ActStatus.POSTED);
        save(delivery);
        rules.invoiceSupplier(delivery); // create an invoice from the delivery

        FinancialAct reversal = rules.reverseDelivery(delivery);
        checkReversal(reversal, SupplierArchetypes.RETURN, SupplierArchetypes.RETURN_ITEM, orderItem, true);

        // verify the invoice hasn't been reversed. Probably should be. TODO
        IMObjectBean bean = getBean(reversal);
        assertTrue(bean.getTargets("returnCredit").isEmpty());
    }

    /**
     * Tests the {@link OrderRules#reverseReturn(FinancialAct)} method.
     */
    @Test
    public void testReverseReturn() {
        BigDecimal quantity = new BigDecimal(50);
        int packageSize = 10;
        BigDecimal unitPrice = BigDecimal.ONE;

        Product product = getProduct();
        Party supplier = getSupplier();
        FinancialAct orderItem = createOrderItem(product, quantity, packageSize, unitPrice);
        FinancialAct orderReturn = createReturn(supplier, product, quantity, packageSize, unitPrice, orderItem);
        orderReturn.setStatus(ActStatus.POSTED);
        save(orderReturn);

        // create a credit for the return
        rules.creditSupplier(orderReturn);
        IMObjectBean returnBean = getBean(orderReturn);
        assertEquals(1, returnBean.getTargets("returnCredit").size());

        FinancialAct reversal = rules.reverseReturn(orderReturn);
        checkReversal(reversal, SupplierArchetypes.DELIVERY, SupplierArchetypes.DELIVERY_ITEM, orderItem, false);

        // verify the credit hasn't been reversed. Probably should be. TODO
        IMObjectBean bean = getBean(reversal);
        assertTrue(bean.getTargets("invoice").isEmpty());
    }

    /**
     * Tests the {@link OrderRules#hasRestrictedProducts(Act)}.
     */
    @Test
    public void testHasRestrictedProducts() {
        Product medication1 = productFactory.newMedication()
                .drugSchedule(false)
                .build();
        Product medication2 = productFactory.newMedication()
                .drugSchedule(true)
                .build();
        Product medication3 = productFactory.createMedication(); // no schedule
        Product merchandise = productFactory.createMerchandise();

        FinancialAct item1 = createOrderItem(medication1, BigDecimal.TEN, 1, BigDecimal.ONE);
        FinancialAct item2 = createOrderItem(medication2, BigDecimal.TEN, 1, BigDecimal.ONE);
        FinancialAct item3 = createOrderItem(medication3, BigDecimal.TEN, 1, BigDecimal.ONE);
        FinancialAct item4 = createOrderItem(merchandise, BigDecimal.TEN, 1, BigDecimal.ONE);
        FinancialAct order = createOrder(getSupplier(), item1, item2, item3, item4);
        assertTrue(rules.hasRestrictedProducts(order));

        IMObjectBean bean = getBean(order);
        bean.removeTarget("items", item2);

        assertFalse(rules.hasRestrictedProducts(order));
    }

    /**
     * Sets up the test case.
     */
    @Override
    public void setUp() {
        super.setUp();
        TaxRules taxRules = new TaxRules(getPractice(), getArchetypeService());
        ProductRules productRules = new ProductRules(getArchetypeService(), getLookupService());
        rules = new OrderRules(taxRules, getArchetypeService(), productRules);
    }

    /**
     * Verifies the delivery status matches that expected for the supplied values.
     *
     * @param act       the act
     * @param quantity  the quantity
     * @param received  the received quantity
     * @param cancelled the cancelled quantity
     * @param expected  the expected delivery status
     */
    private void checkDeliveryStatus(FinancialAct act, BigDecimal quantity, BigDecimal received, BigDecimal cancelled,
                                     DeliveryStatus expected) {
        IMObjectBean bean = getBean(act);
        bean.setValue("quantity", quantity);
        bean.setValue("receivedQuantity", received);
        bean.setValue("cancelledQuantity", cancelled);
        assertEquals(expected, rules.getDeliveryStatus(act));
    }

    /**
     * Verifies a reversal matches that expected.
     *
     * @param reversal      the reversal
     * @param archetype     the expected archetype
     * @param itemArchetype the expected item archetype
     * @param orderItem     the expected order item
     * @param credit        the expected credit flag
     */
    private void checkReversal(FinancialAct reversal, String archetype, String itemArchetype, FinancialAct orderItem,
                               boolean credit) {
        assertTrue(reversal.isA(archetype));
        assertEquals(credit, reversal.isCredit());
        assertEquals(ActStatus.IN_PROGRESS, reversal.getStatus());

        IMObjectBean bean = getBean(reversal);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        FinancialAct item = items.get(0);
        assertTrue(item.isA(itemArchetype));
        assertEquals(credit, item.isCredit());

        IMObjectBean itemBean = getBean(item);
        List<Act> orders = itemBean.getTargets("order", Act.class);
        assertEquals(1, orders.size());
        assertEquals(orderItem, orders.get(0));
    }
}
