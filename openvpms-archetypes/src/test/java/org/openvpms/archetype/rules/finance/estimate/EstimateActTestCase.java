/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.estimate;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * Tests the <em>act.customerEstimation</em> and <em>act.customerEstimationItem</em> archetypes.
 *
 * @author Tim Anderson
 */
public class EstimateActTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Verifies that when an estimate is deleted, any child act is deleted with it.
     */
    @Test
    public void testDelete() {
        Act item = accountFactory.newEstimateItem()
                .patient(patientFactory.createPatient())
                .product(productFactory.createMerchandise())
                .build(false);

        Act estimate = accountFactory.newEstimate()
                .customer(customerFactory.createCustomer())
                .add(item)
                .build();

        assertNotNull(get(estimate));
        assertNotNull(get(item));

        remove(estimate);
        assertNull(get(estimate));
        assertNull(get(item));
    }

    /**
     * Verifies that the low quantity must be less than the high quantity.
     */
    @Test
    public void testLowHighQuantity() {
        Party patient = patientFactory.createPatient();
        Product product = productFactory.createMerchandise();
        Act item1 = accountFactory.newEstimateItem()
                .patient(patient)
                .product(product)
                .lowQuantity(1)
                .highQuantity(0)
                .build(false);
        try {
            getArchetypeService().validateObject(item1);
            fail("Expected ValidationException");
        } catch (ValidationException expected) {
            List<ValidationError> errors = expected.getErrors();
            assertEquals(1, errors.size());
            assertEquals("The High Qty must be >= the Low Qty", errors.get(0).getMessage());
        }

        Act item2 = accountFactory.newEstimateItem()
                .patient(patient)
                .product(product)
                .lowQuantity(0)
                .highQuantity(0)
                .build(false);

        getArchetypeService().validateObject(item2);

        Act item3 = accountFactory.newEstimateItem()
                .patient(patient)
                .product(product)
                .lowQuantity(0)
                .highQuantity(1)
                .build(false);

        getArchetypeService().validateObject(item3);
    }
}
