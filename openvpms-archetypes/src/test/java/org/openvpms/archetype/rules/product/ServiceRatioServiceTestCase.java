/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.CalendarService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.business.service.cache.BasicEhcacheManager;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertNull;

/**
 * Tests the {@link ServiceRatioService}.
 *
 * @author Tim Anderson
 */
public class ServiceRatioServiceTestCase extends ArchetypeServiceTest {

    /**
     * The product price rules.
     */
    @Autowired
    private ProductPriceRules rules;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The calendar service.
     */
    private CalendarService calendarService;

    /**
     * The service ratio service.
     */
    private ServiceRatioService service;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        calendarService = new CalendarService(getArchetypeService(), new BasicEhcacheManager(30));
        service = new ServiceRatioService(calendarService, rules);
    }

    /**
     * Cleans up after the test.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        calendarService.destroy();
    }


    /**
     * Verifies that a service ratio only applies at all times when no calendar is attached.
     */
    @Test
    public void testServiceRatioWithoutCalendar() {
        Entity productType = productFactory.createProductType();

        // set up a service ratio at location1 for the product type.
        Party location1 = practiceFactory.newLocation()
                .addServiceRatio(productType, BigDecimal.TEN)
                .build();
        Party location2 = practiceFactory.createLocation();
        Entity department = null;

        Product product = productFactory.newMedication()
                .type(productType)
                .build();

        // ratio applies at all times at location1, as no calendar attached
        Date now = new Date();
        Date lastYear = DateRules.getDate(now, -1, DateUnits.YEARS);
        Date nextYear = DateRules.getDate(now, 1, DateUnits.YEARS);
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location1, now));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location1, lastYear));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location1, nextYear));

        // no ratio for location2
        assertNull(service.getServiceRatio(product, department, location2, now));
        assertNull(service.getServiceRatio(product, department, location2, lastYear));
        assertNull(service.getServiceRatio(product, department, location2, nextYear));
    }

    /**
     * Verifies that a service ratio only applies where events are present, for service ratios with a calendar.
     */
    @Test
    public void testServiceRatioWithCalendar() {
        Entity calendar = productFactory.createServiceRatioCalendar();
        Entity productType = productFactory.createProductType();
        Party location = practiceFactory.newLocation()
                .addServiceRatio(productType, BigDecimal.TEN, calendar)
                .build();
        Entity department = null;
        Product product = productFactory.newMedication()
                .type(productType)
                .build();

        BigDecimal ratio1 = service.getServiceRatio(product, department, location, new Date());
        assertNull(ratio1);

        Date start1 = TestHelper.getDatetime("2018-07-29 22:00:00");
        Date end1 = TestHelper.getDatetime("2018-07-30 07:00:00");
        Date start2 = TestHelper.getDatetime("2018-07-30 22:00:00");
        Date end2 = TestHelper.getDatetime("2018-07-31 07:00:00");
        Date start3 = TestHelper.getDatetime("2018-07-31 07:00:00");
        Date end3 = TestHelper.getDatetime("2018-07-31 12:00:00");
        schedulingFactory.createCalendarEvent(calendar, start1, end1);
        schedulingFactory.createCalendarEvent(calendar, start2, end2);
        schedulingFactory.createCalendarEvent(calendar, start3, end3); // starts right after end2

        Date beforeStart1 = DateRules.getDate(start1, -1, DateUnits.MINUTES);
        Date afterStart1 = DateRules.getDate(start1, 1, DateUnits.MINUTES);
        assertNull(service.getServiceRatio(product, department, location, beforeStart1));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, start1));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, afterStart1));

        Date beforeEnd1 = DateRules.getDate(end1, -1, DateUnits.MINUTES);
        Date afterEnd1 = DateRules.getDate(end1, 1, DateUnits.MINUTES);
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, beforeEnd1));
        assertNull(service.getServiceRatio(product, department, location, end1));
        assertNull(service.getServiceRatio(product, department, location, afterEnd1));

        Date beforeStart2 = DateRules.getDate(start2, -1, DateUnits.MINUTES);
        Date afterStart2 = DateRules.getDate(start2, 1, DateUnits.MINUTES);
        assertNull(service.getServiceRatio(product, department, location, beforeStart2));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, start2));
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, afterStart2));

        Date beforeEnd2 = DateRules.getDate(end2, -1, DateUnits.MINUTES);
        Date afterEnd2 = DateRules.getDate(end2, 1, DateUnits.MINUTES);
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, beforeEnd2));
        checkEquals(BigDecimal.TEN, (service.getServiceRatio(product, department, location, end2)));
        // runs into start3 event
        checkEquals(BigDecimal.TEN, service.getServiceRatio(product, department, location, afterEnd2));
    }

}
