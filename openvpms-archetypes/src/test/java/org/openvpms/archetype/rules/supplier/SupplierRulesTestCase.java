/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierOrganisationBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link SupplierRules} class.
 *
 * @author Tim Anderson
 */
public class SupplierRulesTestCase extends AbstractSupplierTest {

    /**
     * The rules.
     */
    private SupplierRules rules;

    /**
     * Product rules.
     */
    private ProductRules productRules;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Test supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        rules = new SupplierRules(getArchetypeService());
        productRules = new ProductRules(getArchetypeService(), getLookupService());
    }

    /**
     * Tests the {@link SupplierRules#getReferralVetPractice} method.
     */
    @Test
    public void testGetReferralVet() {
        Party vet = supplierFactory.createVet();
        Party practice = supplierFactory.newVetPractice()
                .addVets(vet)
                .build();
        EntityRelationship relationship = vet.getEntityRelationships().iterator().next();

        // verify the practice is returned for a time > the default start time
        Party practice2 = rules.getReferralVetPractice(vet, new Date());
        assertEquals(practice, practice2);

        // now set the start and end time and verify that there is no practice
        // for a later time (use time addition due to system clock granularity)
        Date start = new Date();
        Date end = new Date(start.getTime() + 1);
        Date later = new Date(end.getTime() + 1);
        relationship.setActiveStartTime(start);
        relationship.setActiveEndTime(end);
        assertNull(rules.getReferralVetPractice(vet, later));
    }

    /**
     * Tests the {@link SupplierRules#isSuppliedBy(Party, Product)} method.
     */
    @Test
    public void testIsSuppliedBy() {
        Party supplier = getSupplier();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();

        assertFalse(rules.isSuppliedBy(supplier, product1));
        assertFalse(rules.isSuppliedBy(supplier, product2));

        ProductSupplier relationship = productRules.createProductSupplier(product1, supplier);
        assertNotNull(relationship);

        assertTrue(rules.isSuppliedBy(supplier, product1));
        assertFalse(rules.isSuppliedBy(supplier, product2));
    }

    /**
     * Tests the {@link SupplierRules#getProductSuppliers(Party)} method.
     */
    @Test
    public void testGetProductSuppliersForSupplier() {
        Party supplier = getSupplier();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();

        ProductSupplier rel1 = productRules.createProductSupplier(product1, supplier);
        ProductSupplier rel2 = productRules.createProductSupplier(product2, supplier);
        save(supplier, product1, product2);

        List<ProductSupplier> relationships = rules.getProductSuppliers(supplier);
        assertEquals(2, relationships.size());
        assertTrue(relationships.contains(rel1));
        assertTrue(relationships.contains(rel2));

        // deactivate one of the relationships, and verify it is no longer returned
        deactivateRelationship(rel1);
        save(product1, supplier);

        relationships = rules.getProductSuppliers(supplier);
        assertEquals(1, relationships.size());
        assertFalse(relationships.contains(rel1));
        assertTrue(relationships.contains(rel2));
    }

    /**
     * Tests the {@link SupplierRules#getSupplierStockLocation(Act)} method.
     */
    @Test
    public void testGetSupplierStockLocationRelationship() {
        Party supplier = getSupplier();
        Party stockLocation = getStockLocation();
        Act order = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .build(false);

        assertNull(rules.getSupplierStockLocation(order));

        TestSupplierOrganisationBuilder builder = supplierFactory.updateSupplier(supplier);
        builder.addESCIConfiguration(stockLocation)
                .build();

        EntityRelationship supplierStockLocation = builder.getESCIConfigs().get(0);
        assertEquals(supplierStockLocation, rules.getSupplierStockLocation(order));
    }

    /**
     * Tests the {@link SupplierRules#getSupplierStockLocation(Party, Party)} method.
     */
    @Test
    public void testGetSupplierStockLocationRelationshipForSupplier() {
        Party supplier = getSupplier();
        Party stockLocation = getStockLocation();

        assertNull(rules.getSupplierStockLocation(supplier, stockLocation));

        TestSupplierOrganisationBuilder builder = supplierFactory.updateSupplier(supplier)
                .addESCIConfiguration(stockLocation);
        builder.build(false);

        EntityRelationship supplierStockLocation = builder.getESCIConfigs().get(0);
        assertEquals(supplierStockLocation, rules.getSupplierStockLocation(supplier, stockLocation));
    }

    /**
     * Tests the {@link SupplierRules#getAccountId(long, Party)} and {@link SupplierRules#getAccountId(Party, Party)}
     * methods.
     */
    @Test
    public void testGetAccountId() {
        Party supplier = getSupplier();
        Party location = getPracticeLocation();

        assertNull(rules.getAccountId(supplier.getId(), location));
        assertNull(rules.getAccountId(supplier, location));
        assertNull(rules.getAccountId(-1, location));  // non existent supplier

        IMObjectBean supplierBean = getBean(supplier);
        Relationship relationship = supplierBean.addTarget("locations", location);
        IMObjectBean bean = getBean(relationship);
        String expected = "1234567";
        bean.setValue("accountId", expected);
        supplierBean.save();

        assertEquals(expected, rules.getAccountId(supplier.getId(), location));
        assertEquals(expected, rules.getAccountId(supplier, location));
    }

    /**
     * Tests the {@link SupplierRules#getOrders(Product, Party)} method.
     */
    @Test
    public void testGetBackOrder() {
        Product product = getProduct();
        Party supplier = getSupplier();
        Party stockLocation = getStockLocation();

        assertEquals(0, rules.getOrders(product, stockLocation).size());

        Act order = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .status(OrderStatus.POSTED)
                .item()
                .product(product)
                .quantity(10)
                .receivedQuantity(2)
                .cancelledQuantity(3)
                .add().build();
        List<ProductOrder> productOrders = rules.getOrders(product, stockLocation);
        assertEquals(1, productOrders.size());

        ProductOrder productOrder = productOrders.get(0);
        assertEquals(order, get(productOrder.getOrder()));
        assertEquals(supplier, get(productOrder.getSupplier()));
        checkEquals(BigDecimal.valueOf(5), productOrder.getUndelivered());
        assertEquals(DateUtils.truncate(order.getActivityStartTime(), Calendar.SECOND), productOrder.getDate());
    }

    /**
     * Helper to deactivate a relationship and sleep for a second, so that
     * subsequent isActive() checks return false. The sleep in between
     * deactivating a relationship and calling isActive() is required due to
     * system time granularity.
     *
     * @param relationship the relationship to deactivate
     */
    private void deactivateRelationship(ProductSupplier relationship) {
        relationship.getRelationship().setActive(false);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
            // do nothing
        }
    }
}
