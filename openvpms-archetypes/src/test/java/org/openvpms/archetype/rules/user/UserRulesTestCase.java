/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.EntityRelationshipHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link UserRules} class.
 *
 * @author Tim Anderson
 */
public class UserRulesTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The rules.
     */
    private UserRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new UserRules(getArchetypeService());
    }

    /**
     * Tests the {@link UserRules#getUser(String)} method.
     */
    @Test
    public void testGetUser() {
        String username = ValueStrategy.random("zuser").toString();
        assertNull(rules.getUser(username));
        User user = userFactory.createUser(username);
        assertEquals(user, rules.getUser(username));
    }

    /**
     * Tests the  {@link UserRules#exists(String)} method.
     */
    @Test
    public void testExists() {
        String username = ValueStrategy.random("zuser").toString();
        assertFalse(rules.exists(username));
        User user = userFactory.createUser(username);
        assertTrue(rules.exists(username));
        user.setActive(false);
        save(user);
        assertTrue(rules.exists(username));
        remove(user);
        assertFalse(rules.exists(username));
    }

    /**
     * Tests the {@link UserRules#exists(String, User)} method.
     */
    @Test
    public void testExistsExcludingUser() {
        String username = ValueStrategy.random("zuser").toString();
        User user1 = userFactory.createUser(username);
        assertFalse(rules.exists(username, user1));

        User user2 = userFactory.newUser()
                .username(username)
                .build(false);
        assertTrue(rules.exists(username, user2));

        user1.setActive(false);
        save(user1);
        assertTrue(rules.exists(username, user2));

        remove(user1);
        assertFalse(rules.exists(username, user2));
    }


    /**
     * Tests the {@link UserRules#isClinician(User)} method.
     */
    @Test
    public void testIsClinician() {
        User user1 = userFactory.createUser();
        assertFalse(rules.isClinician(user1));

        User user2 = userFactory.createClinician();
        assertTrue(rules.isClinician(user2));
    }

    /**
     * Tests the {@link UserRules#getLocations(User)} method.
     */
    @Test
    public void testGetLocations() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();

        User user = userFactory.newUser()
                .addLocations(location1, location2)
                .build();

        List<Party> locations = rules.getLocations(user);
        assertEquals(2, locations.size());
        assertTrue(locations.contains(location1));
        assertTrue(locations.contains(location2));
    }

    /**
     * Tests the {@link UserRules#getLocations(User, Party)} method.
     */
    @Test
    public void testGetLocationsByUserAndPractice() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.createLocation();
        Party location4 = practiceFactory.newLocation().active(false).build(); // inactive

        Party practice = practiceFactory.newPractice()
                .locations(location1, location2, location4)
                .build(false);

        User user1 = userFactory.newUser()
                .addLocations(location1, location3, location4) // NOTE: location3 not linked to practice
                .build();

        List<Party> locations1 = rules.getLocations(user1, practice);
        assertEquals(1, locations1.size());
        assertTrue(locations1.contains(location1));
        assertFalse(locations1.contains(location2));
        assertFalse(locations1.contains(location3));
        assertFalse(locations1.contains(location4));

        User user2 = userFactory.createUser(); // no locations linked, so should see active locations linked to practice
        List<Party> locations2 = rules.getLocations(user2, practice);
        assertEquals(2, locations2.size());
        assertTrue(locations2.contains(location1));
        assertTrue(locations2.contains(location2));
        assertFalse(locations2.contains(location3));
        assertFalse(locations2.contains(location4));
    }

    /**
     * Tests the {@link UserRules#getDefaultLocation(User)} method.
     */
    @Test
    public void testGetDefaultLocation() {
        User user = userFactory.createUser();

        assertNull(rules.getDefaultLocation(user));

        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.newLocation().active(false).build(); // inactive

        userFactory.updateUser(user)
                .addLocations(location1, location2, location3)
                .build();

        Party defaultLocation = rules.getDefaultLocation(user);
        assertNotNull(defaultLocation);

        // location can be one of location1, or location2, as default not specified
        assertTrue(defaultLocation.equals(location1) || defaultLocation.equals(location2));

        // mark location2 as the default
        Relationship rel2 = getBean(user).getValue("locations", Relationship.class, Predicates.targetEquals(location2));
        assertNotNull(rel2);
        EntityRelationshipHelper.setDefault(user, "locations", rel2, getArchetypeService());

        // verify location2 is now returned
        assertEquals(location2, rules.getDefaultLocation(user));

        // now mark location2 inactive. It should no longer be returned
        practiceFactory.updateLocation(location2).active(false).build();
        assertEquals(location1, rules.getDefaultLocation(user));
    }

    /**
     * Tests the {@link UserRules#getDepartments(User)} method.
     */
    @Test
    public void testGetDepartments() {
        deactivateDepartments();

        // check no departments
        User user1 = userFactory.createUser();
        checkDepartments(user1);

        // check no departments linked to a user. Should return all
        Entity department1 = practiceFactory.createDepartment();
        Entity department2 = practiceFactory.createDepartment();
        Entity department3 = practiceFactory.createDepartment();
        checkDepartments(user1, department1, department2, department3);

        // verify inactive departments are not returned
        department2.setActive(false);
        save(department2);
        checkDepartments(user1, department1, department3);

        // now verify departments can be linked to a user. Where a user has linked departments, only these
        // are returned, and only if they are active
        User user2 = userFactory.newUser()
                .addDepartments(department1, department2)
                .build();
        checkDepartments(user2, department1);
    }

    /**
     * Tests the {@link UserRules#getDefaultDepartment(User, List)}.
     */
    @Test
    public void testGetDefaultDepartment() {
        Entity department1 = practiceFactory.createDepartment();
        Entity department2 = practiceFactory.createDepartment();
        Entity department3 = practiceFactory.createDepartment();
        List<Entity> departments = Arrays.asList(department1, department2, department3);

        User user1 = userFactory.createUser();
        assertNull(rules.getDefaultDepartment(user1, departments));

        User user2 = userFactory.newUser()
                .addDepartments(department1, department2)
                .build();

        assertNull(rules.getDefaultDepartment(user2, departments));

        User user3 = userFactory.newUser()
                .addDepartments(department1, department2, department3)
                .defaultDepartment(department2)
                .build();

        assertEquals(department2, rules.getDefaultDepartment(user3, departments));
    }

    /**
     * Tests the {@link UserRules#getSignature(User)} method.
     */
    @Test
    public void testGetSignature() {
        User user1 = userFactory.createUser();
        assertNull(rules.getSignature(user1));

        User user2 = userFactory.newUser()
                .signature()
                .build();
        DocumentAct signature = rules.getSignature(user2);
        assertNotNull(signature);
        assertTrue(signature.isA(UserArchetypes.SIGNATURE));
        assertNotNull(signature.getDocument());
    }

    /**
     * Tests the {@link UserRules#isAdministrator} method.
     */
    @Test
    public void testIsAdministrator() {
        User user1 = userFactory.createUser();
        assertFalse(rules.isAdministrator(user1));

        User user2 = userFactory.createAdministrator();
        assertTrue(rules.isAdministrator(user2));
    }

    /**
     * Tests the {@link UserRules#getClinicians(Party)} method.
     */
    @Test
    public void testGetClinicians() {
        Party locationA = practiceFactory.createLocation();
        Party locationB = practiceFactory.createLocation();
        User user1 = userFactory.createUser();   // user with no locations

        User user2 = userFactory.newUser()       // user linked to locationA
                .addLocations(locationA)
                .build();

        User user3 = userFactory.newUser()       // clinician linked to location A
                .clinician()
                .addLocations(locationA)
                .build();

        User user4 = userFactory.newUser()       // clinician linked to location B
                .clinician()
                .addLocations(locationB)
                .build();

        User user5 = userFactory.newUser()       // clinician linked to both locations
                .clinician()
                .addLocations(locationA, locationB)
                .build();

        User user6 = userFactory.createClinician(); // clinician linked to no locations

        List<User> clinicians1 = rules.getClinicians(locationA);
        assertFalse(clinicians1.contains(user1));
        assertFalse(clinicians1.contains(user2));
        assertTrue(clinicians1.contains(user3));
        assertFalse(clinicians1.contains(user4));
        assertTrue(clinicians1.contains(user5));
        assertTrue(clinicians1.contains(user6));

        List<User> clinicians2 = rules.getClinicians(locationB);
        assertFalse(clinicians2.contains(user1));
        assertFalse(clinicians2.contains(user2));
        assertFalse(clinicians2.contains(user3));
        assertTrue(clinicians2.contains(user4));
        assertTrue(clinicians2.contains(user5));
        assertTrue(clinicians2.contains(user5));
    }

    /**
     * Tests the {@link UserRules#canEdit(User, String)}, {@link UserRules#canSave(User, String)} and
     * {@link UserRules#canRemove(User, String)} methods.
     */
    @Test
    public void testCanEditSaveRemove() {
        ArchetypeAwareGrantedAuthority createAll = userFactory.createAuthority("create", "*");
        ArchetypeAwareGrantedAuthority saveAll = userFactory.createAuthority("save", "*");
        ArchetypeAwareGrantedAuthority removeAll = userFactory.createAuthority("remove", "*");
        ArchetypeAwareGrantedAuthority createProducts = userFactory.createAuthority("create", "product.*");
        ArchetypeAwareGrantedAuthority saveProducts = userFactory.createAuthority("save", "product.*");
        ArchetypeAwareGrantedAuthority removeProducts = userFactory.createAuthority("remove", "product.*");
        ArchetypeAwareGrantedAuthority createMedication = userFactory.createAuthority("create", "product.medication");
        ArchetypeAwareGrantedAuthority saveMedication = userFactory.createAuthority("save", "product.medication");
        ArchetypeAwareGrantedAuthority removeMedication = userFactory.createAuthority("remove", "product.medication");

        User admin = userFactory.newUser()
                .administrator()
                .addRoles(userFactory.createRole(createAll, saveAll, removeAll))
                .build();

        assertTrue(rules.canEdit(admin, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canEdit(admin, ProductArchetypes.PRODUCTS));
        assertTrue(rules.canSave(admin, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canSave(admin, ProductArchetypes.PRODUCTS));
        assertTrue(rules.canRemove(admin, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canRemove(admin, ProductArchetypes.PRODUCTS));

        // verify both create and save authorities are required to edit
        User user1 = userFactory.newUser()
                .addRoles(userFactory.createRole(createAll, saveAll))
                .build();
        assertTrue(rules.canEdit(user1, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canEdit(user1, ProductArchetypes.PRODUCTS));
        assertTrue(rules.canSave(user1, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canSave(user1, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canRemove(user1, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canRemove(user1, ProductArchetypes.PRODUCTS));

        User user2 = userFactory.newUser()
                .addRoles(userFactory.createRole(createAll))
                .build();
        assertFalse(rules.canEdit(user2, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canEdit(user2, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canSave(user2, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canSave(user2, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canRemove(user2, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canRemove(user2, ProductArchetypes.PRODUCTS));

        User user3 = userFactory.newUser()
                .addRoles(userFactory.createRole(saveAll))
                .build();
        assertFalse(rules.canEdit(user3, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canEdit(user3, ProductArchetypes.PRODUCTS));
        assertTrue(rules.canSave(user3, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canSave(user3, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canRemove(user3, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canRemove(user3, ProductArchetypes.PRODUCTS));

        // verify users with just remove authorities can't edit
        User user4 = userFactory.newUser()
                .addRoles(userFactory.createRole(removeAll))
                .build();
        assertFalse(rules.canEdit(user4, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canEdit(user4, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canSave(user4, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canSave(user4, ProductArchetypes.PRODUCTS));
        assertTrue(rules.canRemove(user4, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canRemove(user4, ProductArchetypes.PRODUCTS));

        // verify users with only product authorities can't edit other archetypes
        User user5 = userFactory.newUser()
                .addRoles(userFactory.createRole(createProducts, saveProducts, removeProducts))
                .build();
        assertFalse(rules.canEdit(user5, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canEdit(user5, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canSave(user5, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canSave(user5, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canRemove(user5, CustomerAccountArchetypes.INVOICE));
        assertTrue(rules.canRemove(user5, ProductArchetypes.PRODUCTS));

        // verify users with only product medication authorities can't edit other product archetypes
        User user6 = userFactory.newUser()
                .addRoles(userFactory.createRole(createMedication, saveMedication, removeMedication))
                .build();
        assertTrue(rules.canEdit(user6, ProductArchetypes.MEDICATION));
        assertTrue(rules.canSave(user6, ProductArchetypes.MEDICATION));
        assertFalse(rules.canEdit(user6, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canSave(user6, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canEdit(user6, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canSave(user6, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canEdit(user6, ProductArchetypes.TEMPLATE));
        assertFalse(rules.canSave(user6, ProductArchetypes.TEMPLATE));
        assertTrue(rules.canRemove(user6, ProductArchetypes.MEDICATION));
        assertFalse(rules.canRemove(user6, CustomerAccountArchetypes.INVOICE));
        assertFalse(rules.canRemove(user6, ProductArchetypes.PRODUCTS));
        assertFalse(rules.canRemove(user6, ProductArchetypes.TEMPLATE));
    }

    /**
     * Tests the {@link UserRules#getJobUsedBy(User)} method
     */
    @Test
    public void testGetJobUsedBy() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();

        assertNull(rules.getJobUsedBy(user1));
        assertNull(rules.getJobUsedBy(user2));

        IMObject job1 = create("entity.jobPharmacyOrderDiscontinuation");
        IMObjectBean bean1 = getBean(job1);
        bean1.addTarget("runAs", user1);
        bean1.save();

        assertEquals(job1, rules.getJobUsedBy(user1));

        IMObject job2 = create("entity.jobPharmacyOrderDiscontinuation");
        IMObjectBean bean2 = getBean(job2);
        bean2.addTarget("runAs", user1);
        bean2.addTarget("notify", user2);
        bean2.save();

        assertEquals(job2, rules.getJobUsedBy(user2));

        // deactivate job1 and verify it is no longer returned
        job1.setActive(false);
        save(job1);
        assertEquals(job2, rules.getJobUsedBy(user1));
    }

    /**
     * Tests the {@link UserRules#roleExists(String, long)} method.
     */
    @Test
    public void testRoleExists() {
        SecurityRole role = userFactory.newRole().build(false);
        String name = role.getName();
        assertFalse(rules.roleExists(name, -1));

        save(role);

        assertTrue(rules.roleExists(name, -1));
        assertFalse(rules.roleExists(name, role.getId()));

        // verify can't have inactive duplicates
        role.setActive(false);
        save(role);
        assertTrue(rules.roleExists(name, -1));
        assertFalse(rules.roleExists(name, role.getId()));
    }

    /**
     * Tests the {@link UserRules#authorityExists(String, long)} (String, long)} method.
     */
    @Test
    public void testAuthorityExists() {
        ArchetypeAwareGrantedAuthority authority = userFactory.newAuthority()
                .method("create")
                .archetype("*")
                .build(false);
        String name = authority.getName();
        assertFalse(rules.authorityExists(name, -1));

        save(authority);

        assertTrue(rules.authorityExists(name, -1));
        assertFalse(rules.authorityExists(name, authority.getId()));

        // verify can't have inactive duplicates
        authority.setActive(false);
        save(authority);
        assertTrue(rules.authorityExists(name, -1));
        assertFalse(rules.authorityExists(name, authority.getId()));
    }

    /**
     * Deactivates existing departments so they don't interfere with testing.
     */
    private void deactivateDepartments() {
        IArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, PracticeArchetypes.DEPARTMENT);
        query.where(builder.equal(root.get("active"), true));
        for (Entity department : service.createQuery(query).getResultList()) {
            department.setActive(false);
            save(department);
        }
    }

    /**
     * Verifies {@link UserRules#getDepartments(User)} returns the expected departments.
     *
     * @param user        the user
     * @param departments the expected departments
     */
    private void checkDepartments(User user, Entity... departments) {
        List<Entity> expected = Arrays.asList(departments);
        List<Entity> actual = rules.getDepartments(user);
        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual) && actual.containsAll(expected));
    }
}
