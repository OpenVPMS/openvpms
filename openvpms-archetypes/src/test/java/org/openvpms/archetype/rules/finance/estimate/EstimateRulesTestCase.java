/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.estimate;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.EstimateActStatus;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link EstimateRules} class.
 *
 * @author Tim Anderson
 */
public class EstimateRulesTestCase extends ArchetypeServiceTest {

    /**
     * The rules.
     */
    protected EstimateRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new EstimateRules(getArchetypeService());
    }

    /**
     * Tests the {@link EstimateRules#copy(Act)} method.
     */
    @Test
    public void testCopy() {
        AuthenticationContext context = new AuthenticationContextImpl();
        User user1 = TestHelper.createUser();
        User user2 = TestHelper.createUser();
        context.setUser(user1);

        Party customer = TestHelper.createCustomer();
        Product product = TestHelper.createProduct();
        Party patient = TestHelper.createPatient();
        BigDecimal fixedPrice = new BigDecimal("10.00");

        Act item = EstimateTestHelper.createEstimateItem(patient, product, fixedPrice);
        Act estimate = EstimateTestHelper.createEstimate(customer, item);
        estimate.setStatus(EstimateActStatus.INVOICED);

        // link the estimate to an invoice, to simulate invoicing.
        List<FinancialAct> invoice = FinancialTestHelper.createChargesInvoice(fixedPrice, customer, patient, product,
                                                                              ActStatus.POSTED);
        IMObjectBean bean = getBean(estimate);
        bean.addTarget("invoice", invoice.get(0), "estimates");

        IMObjectBean itemBean = getBean(item);
        itemBean.setValue("minQuantity", BigDecimal.ONE);
        save(estimate, item, invoice.get(0), invoice.get(1));

        assertNotNull(item.getCreated());
        assertEquals(user1.getObjectReference(), item.getCreatedBy());
        assertNull(item.getUpdated());
        assertNull(item.getUpdatedBy());


        String title = "Copy";
        context.setUser(user2);

        Act copy = rules.copy(estimate, title);
        assertNotEquals(copy.getId(), estimate.getId());
        assertNotNull(copy.getCreated());
        assertEquals(copy.getCreatedBy(), user2.getObjectReference());
        assertNull(copy.getUpdated());
        assertNull(copy.getUpdatedBy());
        IMObjectBean copyBean = getBean(copy);

        assertEquals(title, copy.getTitle());
        assertEquals(EstimateActStatus.COMPLETED, copy.getStatus());
        assertEquals(customer, copyBean.getTarget("customer"));

        checkEquals(bean.getBigDecimal("lowTotal"), copyBean.getBigDecimal("lowTotal"));
        checkEquals(bean.getBigDecimal("highTotal"), copyBean.getBigDecimal("highTotal"));

        List<Act> acts = copyBean.getTargets("items", Act.class);
        assertEquals(1, acts.size());
        Act itemCopy = acts.get(0);
        assertNotEquals(itemCopy.getId(), item.getId());
        IMObjectBean itemCopyBean = getBean(itemCopy);

        checkEquals(itemBean.getBigDecimal("minQuantity"), itemCopyBean.getBigDecimal("minQuantity"));
        checkEquals(itemBean.getBigDecimal("lowQty"), itemCopyBean.getBigDecimal("lowQty"));
        checkEquals(itemBean.getBigDecimal("highQty"), itemCopyBean.getBigDecimal("highQty"));
        checkEquals(itemBean.getBigDecimal("fixedPrice"), itemCopyBean.getBigDecimal("fixedPrice"));
        checkEquals(itemBean.getBigDecimal("lowTotal"), itemCopyBean.getBigDecimal("lowTotal"));
        checkEquals(itemBean.getBigDecimal("highTotal"), itemCopyBean.getBigDecimal("highTotal"));

        assertEquals(patient, itemCopyBean.getTarget("patient"));
        assertEquals(product, itemCopyBean.getTarget("product"));

        // verify there is no invoice relationship in the copy
        assertNull(copyBean.getTarget("invoice"));
    }

    /**
     * Tests the {@link EstimateRules#invoice(Act, User)} method.
     */
    @Test
    public void testInvoice() {
        Party customer = TestHelper.createCustomer();
        Product product = TestHelper.createProduct();
        Party patient = TestHelper.createPatient();
        User clinician = TestHelper.createClinician();
        BigDecimal fixedPrice = new BigDecimal("10.00");
        Act item = EstimateTestHelper.createEstimateItem(patient, product, fixedPrice);
        Act estimate = EstimateTestHelper.createEstimate(customer, item);

        IMObjectBean itemBean = getBean(item);
        itemBean.setValue("minQuantity", BigDecimal.ONE);

        save(estimate, item);

        FinancialAct invoice = rules.invoice(estimate, clinician);
        assertEquals(ActStatus.IN_PROGRESS, invoice.getStatus());
        assertEquals(EstimateActStatus.INVOICED, estimate.getStatus());

        IMObjectBean bean = getBean(invoice);
        IMObjectBean estimateBean = getBean(estimate);
        IMObjectBean estimateItemBean = getBean(item);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());
        itemBean = getBean(items.get(0));
        checkEquals(estimateBean.getBigDecimal("highTotal"), bean.getBigDecimal("amount"));

        // verify the estimate and invoice are linked.
        assertEquals(estimateBean.getTarget("invoice"), invoice);
        List<Act> estimates = bean.getSources("estimates", Act.class);
        assertEquals(1, estimates.size());
        assertTrue(estimates.contains(estimate));

        assertEquals(customer, bean.getTarget("customer"));
        assertEquals(clinician, bean.getTarget("clinician"));

        assertEquals(patient, itemBean.getTarget("patient"));
        assertEquals(product, itemBean.getTarget("product"));
        assertEquals(clinician, itemBean.getTarget("clinician"));

        checkEquals(itemBean.getBigDecimal("minQuantity"), estimateItemBean.getBigDecimal("minQuantity"));
        checkEquals(itemBean.getBigDecimal("total"), estimateItemBean.getBigDecimal("highTotal"));

        // check the dispensing act
        List<Act> dispensing = itemBean.getTargets("dispensing", Act.class);
        assertEquals(1, dispensing.size());
        IMObjectBean medicationBean = getBean(dispensing.get(0));
        assertEquals(patient, medicationBean.getTarget("patient"));
        assertEquals(product, medicationBean.getTarget("product"));

        // verify that deleting the estimate doesn't delete the invoice
        remove(estimate);
        assertNotNull(get(invoice));
    }

    /**
     * Tests the {@link EstimateRules#isPatientEstimate(Act, Party)} method.
     */
    @Test
    public void testIsPatientEstimate() {
        Party customer = TestHelper.createCustomer();
        Product product = TestHelper.createProduct();
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();

        BigDecimal fixedPrice = new BigDecimal("10.00");
        Act item1 = EstimateTestHelper.createEstimateItem(patient1, product, fixedPrice);
        Act item2 = EstimateTestHelper.createEstimateItem(patient2, product, fixedPrice);
        Act estimate1 = EstimateTestHelper.createEstimate(customer, item1, item2);
        save(estimate1, item1, item2);

        assertFalse(rules.isPatientEstimate(estimate1, patient1));
        assertFalse(rules.isPatientEstimate(estimate1, patient2));

        Act item3 = EstimateTestHelper.createEstimateItem(patient1, product, fixedPrice);
        Act item4 = EstimateTestHelper.createEstimateItem(patient1, product, fixedPrice);
        Act estimate2 = EstimateTestHelper.createEstimate(customer, item3, item4);
        save(estimate2, item3, item4);

        assertTrue(rules.isPatientEstimate(estimate2, patient1));
        assertFalse(rules.isPatientEstimate(estimate2, patient2));
    }

}
