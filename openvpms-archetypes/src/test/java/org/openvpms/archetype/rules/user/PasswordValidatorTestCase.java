/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link PasswordValidator}.
 *
 * @author Tim Anderson
 */
public class PasswordValidatorTestCase {

    /**
     * Verifies that passwords must not be less than the minimum length.
     */
    @Test
    public void testMinLength() {
        PasswordPolicy policy = new TestPasswordPolicy(5, 10, false, false, false, false);
        checkInvalid(policy, "abcd", "Password must have at least 5 characters");
        checkValid(policy, "abcde", "abcdef");
    }

    /**
     * Verifies that passwords must not be greater than the minimum length.
     */
    @Test
    public void testMaxLength() {
        PasswordPolicy policy = new TestPasswordPolicy(5, 5, false, false, false, false);
        checkInvalid(policy, "abcdef", "Password must not be longer than 5 characters");
        checkValid(policy, "abcde");
    }

    /**
     * Verifies that policies can require lowercase characters.
     */
    @Test
    public void testLowercase() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 4, true, false, false, false);
        checkInvalid(policy, "TEST", "Password must have at least one lowercase character");
        checkValid(policy, "TESt", "tEST", "TEsT", "test");
    }

    /**
     * Verifies that policies can require uppercase characters.
     */
    @Test
    public void testUppercase() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 4, false, true, false, false);
        checkInvalid(policy, "test", "Password must have at least one uppercase character");
        checkValid(policy, "Test", "tesT", "teSt", "TEST");
    }

    /**
     * Verifies that policies can require numbers.
     */
    @Test
    public void testNumber() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 10, false, false, true, false);
        checkInvalid(policy, "test", "Password must have at least one number");
        checkValid(policy, "1test", "test1", "te1st", "1234");
    }

    /**
     * Verifies that passwords can contain special characters.
     */
    @Test
    public void testSpecial() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 10, false, false, false, true);
        checkInvalid(policy, "test", "Password must have at least one special character");
        String special = policy.getSpecialCharacters();

        // verify characters that could trip up regexp are present
        String reserved = "-!\"/^$*+?.()|[]{}\\";
        for (char ch : reserved.toCharArray()) {
            assertTrue(special.indexOf(ch) != -1);
        }

        for (char ch : special.toCharArray()) {
            checkValid(policy, "test" + ch);
        }
    }

    /**
     * Verifies that passwords cannot contain whitespace.
     */
    @Test
    public void testSpace() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 10, false, false, false, false);
        checkInvalid(policy, " test", "Password cannot contain whitespace");
        checkInvalid(policy, "te st ", "Password cannot contain whitespace");
        checkInvalid(policy, "test ", "Password cannot contain whitespace");
        checkInvalid(policy, "\ntest", "Password cannot contain whitespace");
        checkInvalid(policy, "test\r", "Password cannot contain whitespace");
        checkInvalid(policy, "tes\tt", "Password cannot contain whitespace");
    }

    /**
     * Verifies invalid characters are rejected.
     */
    @Test
    public void testInvalidCharacter() {
        PasswordPolicy policy = new TestPasswordPolicy(4, 10, false, false, false, false);
        checkInvalid(policy, "test\u0000", "'\u0000' is not a valid password character");
        checkInvalid(policy, "test\u0080", "'\u0080' is not a valid password character");
    }

    /**
     * Verifies passwords are valid.
     *
     * @param policy    the password policy
     * @param passwords the passwords to check
     */
    private void checkValid(PasswordPolicy policy, String... passwords) {
        PasswordValidator validator = new PasswordValidator(policy);
        for (String password : passwords) {
            List<String> errors = validator.validate(password);
            assertTrue(password + ": " + StringUtils.join(errors), errors.isEmpty());
        }
    }

    /**
     * Verifies a password is invalid.
     *
     * @param policy   the password policy
     * @param password the password to check
     * @param errors   the expected errors
     */
    private void checkInvalid(PasswordPolicy policy, String password, String... errors) {
        List<String> expected = Arrays.asList(errors);
        PasswordValidator validator = new PasswordValidator(policy);
        List<String> actual = validator.validate(password);

        assertEquals(expected.size(), actual.size());
        if (!(expected.containsAll(actual) && actual.containsAll(expected))) {
            fail("Expected " + StringUtils.join(expected) + " but got " + StringUtils.join(actual));
        }
    }

}