/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.LookupHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.ObjectSet;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link AppointmentQuery} class.
 *
 * @author Tim Anderson
 */
public class AppointmentQueryTestCase extends ArchetypeServiceTest {

    /**
     * Status names keyed on status code.
     */
    private Map<String, String> statusNames;

    /**
     * Reason name keyed on reason code.
     */
    private Map<String, String> reasonNames;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Test user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        statusNames = LookupHelper.getNames(service, lookups, ScheduleArchetypes.APPOINTMENT, "status");
        reasonNames = LookupHelper.getNames(service, lookups, ScheduleArchetypes.APPOINTMENT, "reason");
    }

    /**
     * Tests the {@link AppointmentQuery#query()} method.
     */
    @Test
    public void testQuery() {
        final int count = 10;
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());
        Date from = DateUtils.truncate(new Date(), Calendar.SECOND); // MySQL doesn't store millis
        Act[] appointments = new Act[count];
        Date[] startTimes = new Date[count];
        Date[] endTimes = new Date[count];
        Date[] arrivalTimes = new Date[count];
        Party[] customers = new Party[count];
        Party[] patients = new Party[count];
        User[] clinicians = new User[count];
        Entity[] appointmentTypes = new Entity[count];
        for (int i = 0; i < count; ++i) {
            Date startTime = new Date();
            Date arrivalTime = (i % 2 == 0) ? new Date() : null;
            Date endTime = new Date();
            Party customer = customerFactory.createCustomer();
            Party patient = patientFactory.createPatient();
            User clinician = userFactory.createClinician();
            Entity appointmentType = schedulingFactory.createAppointmentType();
            Lookup reason = lookupFactory.newLookup(ScheduleArchetypes.VISIT_REASON)
                    .uniqueCode("XREASON")
                    .build();

            Act appointment = schedulingFactory.newAppointment()
                    .startTime(startTime)
                    .endTime(endTime)
                    .schedule(schedule)
                    .appointmentType(appointmentType)
                    .customer(customer)
                    .patient(patient)
                    .clinician(clinician)
                    .arrivalTime(arrivalTime)
                    .reason(reason.getCode())
                    .build();
            appointments[i] = appointment;
            startTimes[i] = getTimestamp(startTime);
            arrivalTimes[i] = arrivalTime;
            endTimes[i] = getTimestamp(endTime);
            appointmentTypes[i] = appointmentType;
            customers[i] = customer;
            patients[i] = patient;
            clinicians[i] = clinician;
        }
        Date to = new Date();

        AppointmentQuery query = new AppointmentQuery(schedule, from, to, statusNames, reasonNames,
                                                      getArchetypeService());
        IPage<ObjectSet> page = query.query();
        assertNotNull(page);
        List<ObjectSet> results = page.getResults();
        assertEquals(count, results.size());
        for (int i = 0; i < results.size(); ++i) {
            ObjectSet set = results.get(i);
            assertEquals(appointments[i].getObjectReference(), set.get(ScheduleEvent.ACT_REFERENCE));
            assertEquals(startTimes[i], set.get(ScheduleEvent.ACT_START_TIME));
            assertEquals(endTimes[i], set.get(ScheduleEvent.ACT_END_TIME));
            assertEquals(appointments[i].getStatus(), set.get(ScheduleEvent.ACT_STATUS));
            assertEquals(appointments[i].getReason(), set.get(ScheduleEvent.ACT_REASON));
            IMObjectBean bean = getBean(appointments[i]);
            assertEquals(bean.getString("notes"), set.get(ScheduleEvent.NOTES));
            assertEquals(customers[i].getObjectReference(), set.get(ScheduleEvent.CUSTOMER_REFERENCE));
            assertEquals(customers[i].getName(), set.get(ScheduleEvent.CUSTOMER_NAME));
            assertEquals(patients[i].getObjectReference(), set.get(ScheduleEvent.PATIENT_REFERENCE));
            assertEquals(patients[i].getName(), set.get(ScheduleEvent.PATIENT_NAME));
            assertEquals(clinicians[i].getObjectReference(), set.get(ScheduleEvent.CLINICIAN_REFERENCE));
            assertEquals(clinicians[i].getName(), set.get(ScheduleEvent.CLINICIAN_NAME));
            assertEquals(schedule.getObjectReference(), set.get(ScheduleEvent.SCHEDULE_REFERENCE));
            assertEquals(schedule.getName(), set.get(ScheduleEvent.SCHEDULE_NAME));
            assertEquals(appointmentTypes[i].getObjectReference(), set.get(ScheduleEvent.SCHEDULE_TYPE_REFERENCE));
            assertEquals(appointmentTypes[i].getName(), set.get(ScheduleEvent.SCHEDULE_TYPE_NAME));
            assertEquals(arrivalTimes[i], set.get(ScheduleEvent.ARRIVAL_TIME));
        }
    }

    /**
     * Verifies that appointments that intersect the query date range are returned.
     */
    @Test
    public void testDateRanges() {
        Party location = practiceFactory.createLocation();
        Entity schedule1 = schedulingFactory.newSchedule()
                .location(location)
                .unlimitedDuration()
                .build();
        Entity schedule2 = schedulingFactory.newSchedule()
                .location(location)
                .unlimitedDuration()
                .build();
        checkDateRanges(schedule1, schedule2);
    }

    /**
     * Verifies that appointments that intersect the query date range are returned.
     */
    @Test
    public void testDateRangesForSchedulesWithMaxDuration() {
        Party location = practiceFactory.createLocation();
        Entity schedule1 = schedulingFactory.newSchedule()
                .location(location)
                .maxDuration(5, DateUnits.DAYS)
                .build();
        Entity schedule2 = schedulingFactory.newSchedule()
                .location(location)
                .maxDuration(5, DateUnits.DAYS)
                .build();
        checkDateRanges(schedule1, schedule2);
    }

    /**
     * Verifies that if a schedule is configured with a maximum duration X less than that of appointments or calendar
     * blocks, the events won't be retrieved if they start > X before the query range or end > X after the query range
     */
    @Test
    public void testMaxDurationMisconfiguration() {
        // set up a schedule for events with no maximum duration
        Entity schedule1 = schedulingFactory.newSchedule()
                .noMaxDuration()
                .location(practiceFactory.createLocation())
                .build();

        Date from = getDate("2015-01-03");
        Date to = getDate("2015-01-04");

        // create some events before the query range
        createAppointment(schedule1, "2015-01-01 10:00:00", "2015-01-01 10:30:00");
        createBlock(schedule1, "2015-01-02 11:00:00", "2015-01-02 10:30:00");

        // create some events that intersect the query range
        Act act0 = createAppointment(schedule1, "2015-01-01 10:30:00", "2015-01-04 12:30:00"); // overlaps start,end
        Act act1 = createBlock(schedule1, "2015-01-02 22:00:00", "2015-01-04 02:00:00");       // overlaps start,end
        Act act2 = createBlock(schedule1, "2015-01-02 10:00:00", "2015-01-03 11:00:00");       // intersect start
        Act act3 = createAppointment(schedule1, "2015-01-02 22:00:00", "2015-01-03 11:00:00"); // intersect start
        Act act4 = createBlock(schedule1, "2015-01-03 00:00:00", "2015-01-03 11:00:00");       // at start
        Act act5 = createAppointment(schedule1, "2015-01-03 00:00:00", "2015-01-04 00:00:00"); // equals start,end
        Act act6 = createBlock(schedule1, "2015-01-03 11:00:00", "2015-01-03 12:00:00");       // between
        Act act7 = createAppointment(schedule1, "2015-01-03 12:00:00", "2015-01-04 00:00:00"); // at end
        Act act8 = createBlock(schedule1, "2015-01-03 12:00:00", "2015-01-04 02:00:00");       // intersect end
        Act act9 = createAppointment(schedule1, "2015-01-03 13:00:00", "2015-01-04 03:00:00"); // intersect end

        // create some events after the query range
        createAppointment(schedule1, "2015-01-04 12:30:00", "2015-01-04 13:30:00");
        createBlock(schedule1, "2015-01-04 12:30:00", "2015-01-05 10:30:00");

        // verify the expected events are returned
        AppointmentQuery query1 = new AppointmentQuery(schedule1, from, to, statusNames, reasonNames,
                                                       getArchetypeService());
        List<ObjectSet> events1 = query1.query().getResults();
        assertEquals(10, events1.size());

        assertEquals(act0.getObjectReference(), events1.get(0).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act1.getObjectReference(), events1.get(1).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act2.getObjectReference(), events1.get(2).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act3.getObjectReference(), events1.get(3).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act4.getObjectReference(), events1.get(4).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act5.getObjectReference(), events1.get(5).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act6.getObjectReference(), events1.get(6).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act7.getObjectReference(), events1.get(7).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act8.getObjectReference(), events1.get(8).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act9.getObjectReference(), events1.get(9).get(ScheduleEvent.ACT_REFERENCE));

        // now restrict events to only 2 hours. Any event starting 2 hours before the start of the query range, or
        // ending > 2 hours after will be excluded
        schedulingFactory.updateSchedule(schedule1)
                .maxDuration(2, DateUnits.HOURS)
                .build();
        AppointmentQuery query2 = new AppointmentQuery(schedule1, from, to, statusNames, reasonNames,
                                                       getArchetypeService());

        // verify act0, act2, and act9 are excluded
        List<ObjectSet> events2 = query2.query().getResults();
        assertEquals(7, events2.size());
        assertEquals(act1.getObjectReference(), events2.get(0).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act3.getObjectReference(), events2.get(1).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act4.getObjectReference(), events2.get(2).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act5.getObjectReference(), events2.get(3).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act6.getObjectReference(), events2.get(4).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act7.getObjectReference(), events2.get(5).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act8.getObjectReference(), events2.get(6).get(ScheduleEvent.ACT_REFERENCE));
    }

    /**
     * Verifies expected events are returned by {@link AppointmentQuery#query()}.
     *
     * @param schedule1 the first schedule
     * @param schedule2 the second schedule
     */
    private void checkDateRanges(Entity schedule1, Entity schedule2) {
        Date from = getDatetime("2015-01-03 10:30:00");
        Date to = getDatetime("2015-01-03 12:30:00");

        // create some events before the query range
        createAppointment(schedule1, "2015-01-01 10:00:00", "2015-01-01 10:30:00");
        createBlock(schedule1, "2015-01-02 11:00:00", "2015-01-03 10:30:00");
        createAppointment(schedule1, "2015-01-03 10:00:00", "2015-01-03 10:30:00");

        // create some events that intersect the query range
        Act act0 = createAppointment(schedule1, "2015-01-01 10:30:00", "2015-01-04 12:30:00"); // overlaps start,end
        Act act1 = createBlock(schedule1, "2015-01-03 10:00:00", "2015-01-03 11:00:00");       // intersect start
        Act act2 = createAppointment(schedule1, "2015-01-03 10:30:00", "2015-01-03 11:00:00"); // at start
        Act act3 = createBlock(schedule1, "2015-01-03 10:30:00", "2015-01-03 12:30:00");       // equals start,end
        Act act4 = createAppointment(schedule1, "2015-01-03 11:00:00", "2015-01-03 12:00:00"); // between
        Act act5 = createBlock(schedule1, "2015-01-03 12:00:00", "2015-01-03 12:30:00");       // at end
        Act act6 = createAppointment(schedule1, "2015-01-03 12:00:00", "2015-01-03 13:00:00"); // intersect end

        // create some events after the query range
        createAppointment(schedule1, "2015-01-03 12:30:00", "2015-01-03 13:30:00");
        createBlock(schedule1, "2015-01-03 12:30:00", "2015-01-04 10:30:00");
        createAppointment(schedule1, "2015-01-04 10:00:00", "2015-01-04 10:30:00");

        // create some appointments that intersect the range, but for a different schedule
        createAppointment(schedule2, "2015-01-01 10:30:00", "2015-01-04 12:30:00"); // overlaps start,end
        createBlock(schedule2, "2015-01-03 10:00:00", "2015-01-03 11:00:00");       // intersect start
        createAppointment(schedule2, "2015-01-03 10:30:00", "2015-01-03 11:00:00"); // at start

        AppointmentQuery query = new AppointmentQuery(schedule1, from, to, statusNames, reasonNames,
                                                      getArchetypeService());
        IPage<ObjectSet> page = query.query();
        List<ObjectSet> appointments = page.getResults();
        assertEquals(7, appointments.size());

        assertEquals(act0.getObjectReference(), appointments.get(0).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act1.getObjectReference(), appointments.get(1).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act2.getObjectReference(), appointments.get(2).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act3.getObjectReference(), appointments.get(3).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act4.getObjectReference(), appointments.get(4).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act5.getObjectReference(), appointments.get(5).get(ScheduleEvent.ACT_REFERENCE));
        assertEquals(act6.getObjectReference(), appointments.get(6).get(ScheduleEvent.ACT_REFERENCE));
    }

    /**
     * Helper to remove any seconds from a time, as the database may not store them.
     *
     * @param timestamp the timestamp
     * @return the timestamp with seconds and milliseconds removed
     */
    private Date getTimestamp(Date timestamp) {
        return DateUtils.truncate(new Date(timestamp.getTime()), Calendar.SECOND);
    }

    /**
     * Helper to create an appointment.
     *
     * @param schedule the schedule
     * @param from     the appointment start time
     * @param to       the appointment end time
     * @return a new appointment
     */
    private Act createAppointment(Entity schedule, String from, String to) {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        return schedulingFactory.newAppointment()
                .startTime(from)
                .endTime(to)
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customer)
                .patient(patient)
                .build();
    }

    /**
     * Helper to create a calendar block.
     *
     * @param schedule the schedule
     * @param from     the block start time
     * @param to       the block end time
     * @return a new calendar block
     */
    private Act createBlock(Entity schedule, String from, String to) {
        return schedulingFactory.newCalendarBlock()
                .startTime(from)
                .endTime(to)
                .schedule(schedule)
                .type(schedulingFactory.createCalendarBlockType())
                .build();
    }

}
