/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.message;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.message.TestMessageFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link MessageRules} class.
 *
 * @author Tim Anderson
 */
public class MessageRulesTestCase extends ArchetypeServiceTest {

    /**
     * The message factory.
     */
    @Autowired
    private TestMessageFactory messageFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests the {@link MessageRules#hasNewMessages(User)} method.
     */
    @Test
    public void testHasNewMessages() {
        MessageRules rules = new MessageRules(getArchetypeService());
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();

        assertFalse(rules.hasNewMessages(user1));
        assertFalse(rules.hasNewMessages(user2));

        Act message1 = messageFactory.newUserMessage()
                .to(user1)
                .subject("a subject")
                .reason("PHONE_CALL")
                .message("a message")
                .status(MessageStatus.PENDING)
                .build();

        assertTrue(rules.hasNewMessages(user1));
        assertFalse(rules.hasNewMessages(user2));

        messageFactory.updateUserMessage(message1)
                .status(MessageStatus.READ)
                .build();

        assertFalse(rules.hasNewMessages(user1));
        assertFalse(rules.hasNewMessages(user2));

        messageFactory.updateUserMessage(message1)
                .status(MessageStatus.COMPLETED)
                .build();

        assertFalse(rules.hasNewMessages(user1));
        assertFalse(rules.hasNewMessages(user2));

        Act message2 = messageFactory.newAuditMessage()
                .subject("locking enabled")
                .reason("MEDICAL_RECORD_LOCKING")
                .to(user2)
                .status(MessageStatus.PENDING)
                .build();

        assertFalse(rules.hasNewMessages(user1));
        assertTrue(rules.hasNewMessages(user2));
        messageFactory.updateAuditMessage(message2)
                .status(MessageStatus.COMPLETED)
                .build();
        assertFalse(rules.hasNewMessages(user1));
        assertFalse(rules.hasNewMessages(user2));

        User user3 = userFactory.createUser();
        assertFalse(rules.hasNewMessages(user3));

        Act message3 = messageFactory.newSystemMessage()
                .subject("order invoiced")
                .to(user3)
                .status(MessageStatus.PENDING)
                .build();

        assertTrue(rules.hasNewMessages(user3));
        messageFactory.updateSystemMessage(message3)
                .status(MessageStatus.READ)
                .build();
        assertFalse(rules.hasNewMessages(user3));
    }

    /**
     * Tests the {@link MessageRules#hasNewMessages(User, Date)} method.
     */
    @Test
    public void testHasNewMessagesSince() {
        MessageRules rules = new MessageRules(getArchetypeService());
        User user1 = userFactory.createUser();

        assertFalse(rules.hasNewMessages(user1, getDate("2023-07-01")));

        Act message1 = messageFactory.newUserMessage()
                .to(user1)
                .startTime("2023-07-01")
                .subject("a subject")
                .reason("PHONE_CALL")
                .message("a message")
                .status(MessageStatus.PENDING)
                .build();

        assertTrue(rules.hasNewMessages(user1, getDate("2023-07-01")));
        assertFalse(rules.hasNewMessages(user1, getDate("2023-07-02")));

        messageFactory.updateUserMessage(message1)
                .status(MessageStatus.READ)
                .build();

        assertFalse(rules.hasNewMessages(user1, getDate("2023-07-01")));
        assertFalse(rules.hasNewMessages(user1, getDate("2023-07-02")));
    }
}
