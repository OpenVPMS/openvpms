/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.junit.Before;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.supplier.delivery.TestDeliveryBuilder;
import org.openvpms.archetype.test.builder.supplier.order.TestOrderBuilder;
import org.openvpms.archetype.test.builder.supplier.order.TestOrderItemBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openvpms.component.model.bean.Predicates.targetEquals;


/**
 * Base class for supplier test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSupplierTest extends ArchetypeServiceTest {

    /**
     * Package units for act items.
     */
    protected static final String PACKAGE_UNITS = "BOX";

    /**
     * UN/CEFACT unit code corresponding to the package units.
     */
    protected static final String PACKAGE_UNIT_CODE = "BX";

    /**
     * The product factory.
     */
    @Autowired
    protected TestProductFactory productFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The product.
     */
    private Product product;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The practice location.
     */
    private Party practiceLocation;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        product = productFactory.createMedication();
        stockLocation = createStockLocation();

        // create a practice for currency and tax calculation purposes
        practiceLocation = practiceFactory.newLocation()
                .stockLocation(stockLocation)
                .build();
        practice = practiceFactory.newPractice()
                .addAddress("1234 Broadwater Avenue", "CAPE_WOOLAMAI", "VIC", "3925")
                .locations(practiceLocation)
                .build();

        supplier = supplierFactory.createSupplier();

        // set up a unit of measure
        lookupFactory.createUnitOfMeasure(PACKAGE_UNITS, PACKAGE_UNIT_CODE);
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    protected Party getSupplier() {
        return supplier;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    protected Product getProduct() {
        return product;
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     */
    protected Party getPractice() {
        return practice;
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    protected Party getPracticeLocation() {
        return practiceLocation;
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location
     */
    protected Party getStockLocation() {
        return stockLocation;
    }

    /**
     * Creates an order associated with order items.
     *
     * @param supplier   the supplier
     * @param orderItems the order item
     * @return a new order
     */
    protected FinancialAct createOrder(Party supplier, FinancialAct... orderItems) {
        return createOrder(supplier, stockLocation, orderItems);
    }

    /**
     * Creates an order associated with order items.
     *
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param orderItems    the order item
     * @return a new order
     */
    protected FinancialAct createOrder(Party supplier, Party stockLocation, FinancialAct... orderItems) {
        TestOrderBuilder builder = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation);
        for (FinancialAct item : orderItems) {
            builder.add(item);
        }
        return builder.build();
    }

    /**
     * Creates an order item.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @return a new order item
     */
    protected FinancialAct createOrderItem(Product product, BigDecimal quantity, int packageSize,
                                           BigDecimal unitPrice) {
        return newOrderItem(product, quantity, packageSize, unitPrice)
                .build();
    }

    /**
     * Creates and saves a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price. May be {@code null}
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice) {
        return createDelivery(supplier, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
    }

    /**
     * Creates and saves a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice, BigDecimal listPrice) {
        return supplierFactory.newDelivery()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .item().product(product).quantity(quantity).packageSize(packageSize).packageUnits(PACKAGE_UNITS)
                .unitPrice(unitPrice)
                .listPrice(listPrice)
                .add()
                .supplierNotes("Some notes")
                .build();
    }

    /**
     * Creates and saves a delivery, associated with an order item.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the delivery package size
     * @param unitPrice   the delivery unit price
     * @param orderItem   the order item
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice, FinancialAct orderItem) {
        FinancialAct item = createDeliveryItem(product, quantity, packageSize, unitPrice, orderItem);
        return createDelivery(supplier, item);
    }

    /**
     * Creates and saves a delivery, associated with a delivery item.
     *
     * @param supplier      the supplier
     * @param deliveryItems the delivery items
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, FinancialAct... deliveryItems) {
        TestDeliveryBuilder builder = supplierFactory.newDelivery()
                .supplier(supplier)
                .stockLocation(stockLocation);
        for (FinancialAct item : deliveryItems) {
            builder.add(item);
        }
        return builder.build();
    }

    /**
     * Creates a delivery item, associated with an order item.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new delivery item
     */
    protected FinancialAct createDeliveryItem(Product product, BigDecimal quantity, int packageSize,
                                              BigDecimal unitPrice, FinancialAct orderItem) {
        return supplierFactory.newDeliveryItem()
                .product(product)
                .quantity(quantity)
                .packageSize(packageSize)
                .packageUnits(PACKAGE_UNITS)
                .unitPrice(unitPrice)
                .order(orderItem)
                .build(false);
    }

    /**
     * Creates a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @return a new return
     */
    protected FinancialAct createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                        BigDecimal unitPrice) {
        return createReturn(supplier, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
    }

    /**
     * Creates a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new return
     */
    protected FinancialAct createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                        BigDecimal unitPrice, BigDecimal listPrice) {
        return supplierFactory.newReturn()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .item().product(product).quantity(quantity).packageSize(packageSize).packageUnits(PACKAGE_UNITS)
                .unitPrice(unitPrice).listPrice(listPrice)
                .add()
                .build();
    }

    /**
     * Create a new return, associated with an order item.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new return
     */
    protected FinancialAct createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                        BigDecimal unitPrice, FinancialAct orderItem) {
        return supplierFactory.newReturn()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .item()
                .product(product)
                .quantity(quantity)
                .packageSize(packageSize)
                .packageUnits(PACKAGE_UNITS)
                .unitPrice(unitPrice)
                .order(orderItem)
                .add()
                .build();
    }

    /**
     * Creates and saves a new stock location.
     *
     * @return a new stock location
     */
    protected Party createStockLocation() {
        return practiceFactory.createStockLocation();
    }

    /**
     * Verifies that the <em>entityLink.productStockLocation</em> associated with the product and stock location matches
     * that expected.
     *
     * @param product  the product
     * @param quantity the expected quantity, or {@code null} if the relationship shouldn't exist
     */
    protected void checkProductStockLocationRelationship(Product product, BigDecimal quantity) {
        product = get(product);  // make sure the latest instance is used
        IMObjectBean bean = getBean(product);
        List<Relationship> values = bean.getValues("stockLocations", Relationship.class, targetEquals(stockLocation));
        if (quantity == null) {
            assertTrue(values.isEmpty());
        } else {
            assertEquals(1, values.size());
            IMObjectBean relBean = getBean(values.get(0));
            checkEquals(quantity, relBean.getBigDecimal("quantity"));
        }
    }

    /**
     * Returns an order item builder, with details populated.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @return a new order item builder
     */
    protected TestOrderItemBuilder newOrderItem(Product product, BigDecimal quantity, int packageSize,
                                                BigDecimal unitPrice) {
        return supplierFactory.newOrderItem()
                .product(product)
                .quantity(quantity)
                .packageSize(packageSize)
                .packageUnits(PACKAGE_UNITS)
                .unitPrice(unitPrice)
                .listPrice(BigDecimal.ZERO);
    }
}
