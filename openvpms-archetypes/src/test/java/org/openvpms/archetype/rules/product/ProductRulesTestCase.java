/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;


/**
 * Tests the {@link ProductRules} class.
 *
 * @author Tim Anderson
 */
public class ProductRulesTestCase extends AbstractProductTest {

    /**
     * The stock rules.
     */
    @Autowired
    private StockRules stockRules;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The rules.
     */
    private ProductRules rules;


    /**
     * Tests the {@link ProductRules#copy(Product)} method.
     */
    @Test
    public void testCopy() {
        BigDecimal cost = new BigDecimal("14.5");
        BigDecimal price = new BigDecimal("31.90");
        BigDecimal listPrice = new BigDecimal("69.04");
        Party supplier = supplierFactory.createSupplier();
        Product linked = productFactory.newPriceTemplate().build();
        Product product = productFactory.newMedication()
                .addSpecies("CANINE")
                .newUnitPrice().cost(cost).price(price).add()
                .newDose().species("CANINE").weightRange(0, 10).rate(1).quantity(1).add()
                .addPriceTemplate(linked) // should not be copied
                .build();
        Entity dose = getBean(product).getTarget("doses", Entity.class);

        ProductPrice unitPrice = product.getProductPrices().iterator().next();
        ProductSupplier ps = rules.createProductSupplier(product, supplier);
        ps.setPackageSize(500);
        ps.setListPrice(listPrice);
        ps.setAutoPriceUpdate(true);
        save(product);

        Lookup species = lookupFactory.getSpecies("CANINE");

        Party stockLocation = practiceFactory.createStockLocation();
        stockRules.updateStock(product, stockLocation, TEN);

        // now perform the copy
        String name = "Copy";
        Product copy = rules.copy(product, name);
        assertFalse(copy.isNew());  // should be saved.

        // verify it is a copy
        assertNotEquals(product.getId(), copy.getId());
        assertEquals(product.getArchetype(), copy.getArchetype());
        assertEquals(name, copy.getName());

        // verify the copy has a different dose
        IMObjectBean copyBean = getBean(copy);
        List<Entity> doses = copyBean.getTargets("doses", Entity.class);
        assertEquals(1, doses.size());
        assertNotEquals(doses.get(0), dose);

        // verify the dose species is identical
        Lookup species2 = doses.get(0).getClassifications().iterator().next();
        assertEquals(species.getId(), species2.getId());

        // verify the copy refers to the same stock location
        assertEquals(copyBean.getTarget("stockLocations"), stockLocation);

        // verify the product price has been copied and not recalculated using the supplier cost price
        Set<ProductPrice> productPrices = copy.getProductPrices();
        assertEquals(1, productPrices.size());
        ProductPrice priceCopy = productPrices.toArray(new ProductPrice[0])[0];
        assertNotEquals(unitPrice.getId(), priceCopy.getId());
        checkEquals(unitPrice.getPrice(), priceCopy.getPrice());
        checkEquals(cost, getBean(priceCopy).getBigDecimal("cost"));

        // verify the product supplier relationship has been copied
        ProductSupplier psCopy = rules.getProductSupplier(copy, supplier, null, ps.getPackageSize(),
                                                          ps.getPackageUnits());
        assertNotNull(psCopy);
        assertNotEquals(psCopy.getRelationship().getId(), ps.getRelationship().getId());
        assertEquals(500, ps.getPackageSize());
        checkEquals(new BigDecimal("0.138"), ps.getCostPrice());
        assertTrue(ps.isAutoPriceUpdate());
        checkEquals(listPrice, ps.getListPrice());

        // verify the supplier is the same
        assertEquals(supplier.getObjectReference(), psCopy.getSupplierRef());

        // verify the linked product is the same
        List<Reference> linkedRefs = copyBean.getTargetRefs("linked");
        assertEquals(1, linkedRefs.size());
        assertEquals(linked.getObjectReference(), linkedRefs.get(0));

        // stock quantity should not be copied
        checkEquals(ZERO, stockRules.getStock(copy, stockLocation));
    }

    /**
     * Copies a service product with a linked location.
     */
    @Test
    public void testCopyProductWithLocation() {
        Party location = practiceFactory.createLocation();
        Product product = productFactory.newService()
                .addLocations(location)
                .build();

        // now perform the copy
        String name = "Copy";
        Product copy = rules.copy(product, name);

        // verify it is a copy
        assertNotEquals(product.getId(), copy.getId());
        assertEquals(product.getArchetype(), copy.getArchetype());
        assertEquals(name, copy.getName());

        // verify the copy refers to the same location
        IMObjectBean copyBean = getBean(copy);
        assertEquals(copyBean.getTarget("locations"), location);
    }

    /**
     * Verifies that if a <em>entityLink.productStockLocation</em> refers to a non-existent supplier, the
     * product may still be copied, and the supplier reference isn't copied.<p/>
     * This can occur because the supplier reference is managed internally as a string and therefore not subject to
     * database integrity constraints when a supplier is deleted.
     */
    @Test
    public void testCopyProductWithMissingSupplierOnStockLocation() {
        Party supplier1 = supplierFactory.createSupplier();
        Party supplier2 = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();

        // mark supplier1 as the preferred supplier at stockLocation
        Product product = productFactory.newMerchandise()
                .newProductStockLocation()
                .supplier(supplier1)
                .stockLocation(stockLocation)
                .add()
                .newProductSupplier().supplier(supplier1).add()
                .newProductSupplier().supplier(supplier2).preferred(true).add()
                .build();

        // verify supplier1 is the preferred supplier at stockLocation
        ProductSupplier ps1 = rules.getPreferredSupplier(product, stockLocation);
        assertNotNull(ps1);
        assertEquals(supplier1, ps1.getSupplier());

        // remove the product-supplier relationship and the supplier.
        remove(ps1.getRelationship());
        remove(supplier1);
        product = get(product);

        // verify the product still has a reference to supplier1 on the relationship
        IMObjectRelationship relationship1 = stockRules.getStockRelationship(product, stockLocation);
        assertEquals(supplier1.getObjectReference(), getBean(relationship1).getReference("supplier"));

        // copy the product
        Product copy = rules.copy(product);

        // verify supplier2 is the preferred supplier
        ProductSupplier ps2 = rules.getPreferredSupplier(copy, stockLocation);
        assertNotNull(ps2);
        assertEquals(supplier2, ps2.getSupplier());

        // verify there is no reference to supplier1 on the copied relationship
        IMObjectRelationship relationship2 = stockRules.getStockRelationship(copy, stockLocation);
        assertNull(getBean(relationship2).getReference("supplier"));
    }

    /**
     * Tests the {@link ProductRules#getDose(Product, Weight, String)} method.
     */
    @Test
    public void testGetDose() {
        Product product = productFactory.newMedication()
                .concentration(2)
                .newDose().species("CANINE").weightRange(0, 10).rate(1).quantity(1).add() // canine 0-10kg
                .newDose().species("FELINE").weightRange(0, 10).rate(2).quantity(2).add() // feline 0-10kg
                .newDose().weightRange(10, 20).rate(4).quantity(1).add()                  // all species 10-20kg
                .build();

        checkEquals(new BigDecimal("0.5"), rules.getDose(product, new Weight(1), "CANINE"));
        checkEquals(2, rules.getDose(product, new Weight(1), "FELINE"));

        checkEquals(new BigDecimal(20), rules.getDose(product, new Weight(10), "CANINE")); // picks up all species dose
        checkEquals(ZERO, rules.getDose(product, new Weight(20), "FELINE"));               // no dose for any species

        // check null species
        checkEquals(ZERO, rules.getDose(product, new Weight(1), null));
        checkEquals(new BigDecimal(20), rules.getDose(product, new Weight(10), null));
        checkEquals(ZERO, rules.getDose(product, new Weight(20), null));
    }

    /**
     * Verifies that {@link ProductRules#getDose(Product, Weight, String)} rounds doses correctly.
     */
    @Test
    public void testGetDoseRounding() {
        BigDecimal concentration = BigDecimal.valueOf(50);

        // use the same concentration and date, but round to different no. of places for each weight range
        Product product1 = productFactory.newMedication()
                .concentration(concentration)
                .newDose().weightRange(0, 100).rate(4).quantity(1).roundTo(0).add()
                .build();
        Product product2 = productFactory.newMedication().concentration(concentration)
                .newDose().weightRange(0, 100).rate(4).quantity(1).roundTo(1).add()
                .build();
        Product product3 = productFactory.newMedication().concentration(concentration)
                .newDose().weightRange(0, 100).rate(4).quantity(1).roundTo(2).add()
                .build();


        Weight weight = new Weight(new BigDecimal("15.5"));
        checkEquals(1, rules.getDose(product1, weight, "CANINE"));
        checkEquals(new BigDecimal("1.2"), rules.getDose(product2, weight, "CANINE"));
        checkEquals(new BigDecimal("1.24"), rules.getDose(product3, weight, "CANINE"));
    }

    /**
     * Tests the {@link ProductRules#getProductSuppliers} method.
     */
    @Test
    public void testGetProductSuppliersForSupplierAndProduct() {
        Party supplier = supplierFactory.createSupplier();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();

        ProductSupplier p1rel1 = rules.createProductSupplier(product1, supplier);
        ProductSupplier p1rel2 = rules.createProductSupplier(product1, supplier);
        ProductSupplier p2rel1 = rules.createProductSupplier(product2, supplier);
        p1rel1.save();
        p1rel2.save();
        p2rel1.save();

        List<ProductSupplier> relationships = rules.getProductSuppliers(product1, supplier);
        assertEquals(2, relationships.size());
        assertEquals(p1rel1, relationships.get(0));  // relationships sorted on increasing id
        assertEquals(p1rel2, relationships.get(1));
        assertFalse(relationships.contains(p2rel1));

        // deactivate one of the relationships, and verify it is no longer
        // returned. Need to sleep to allow for system clock granularity
        deactivateRelationship(p1rel1);

        relationships = rules.getProductSuppliers(product1, supplier);
        assertEquals(1, relationships.size());
        assertFalse(relationships.contains(p1rel1));
        assertEquals(p1rel2, relationships.get(0));
    }

    /**
     * Tests the {@link ProductRules#getProductSupplier(Product, Party, String, int, String)} method.
     */
    @Test
    public void testGetProductSupplier() {
        Party supplier = supplierFactory.createSupplier();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();
        Product product3 = productFactory.createMedication();

        // create some relationships
        ProductSupplier p1rel1 = rules.createProductSupplier(product1, supplier);
        ProductSupplier p1rel2 = rules.createProductSupplier(product1, supplier);
        ProductSupplier p2rel = rules.createProductSupplier(product2, supplier);
        ProductSupplier p3rel = rules.createProductSupplier(product3, supplier);

        assertEquals(0, p1rel1.getPackageSize()); // default value
        p1rel2.setPackageSize(3);
        p1rel2.setPackageUnits("AMPOULE");
        p2rel.setPackageSize(4);
        p2rel.setPackageUnits("PACKET");
        p3rel.setReorderCode("p3");
        p3rel.setPackageSize(4);
        p3rel.setPackageUnits("PACKET");

        // verify that p1rel is returned if there is no corresponding
        // relationship, as its package size isn't set
        ProductSupplier test1 = rules.getProductSupplier(product1, supplier, null, 4, "BOX");
        assertEquals(p1rel1, test1);

        p1rel1.setPackageSize(4);
        p1rel1.setPackageUnits("BOX");

        // verify that the correct relationship is returned for exact matches
        assertEquals(p1rel1, rules.getProductSupplier(product1, supplier, null, 4, "BOX"));
        assertEquals(p1rel2, rules.getProductSupplier(product1, supplier, null, 3, "AMPOULE"));
        assertEquals(p2rel, rules.getProductSupplier(product2, supplier, null, 4, "PACKET"));
        assertEquals(p3rel, rules.getProductSupplier(product3, supplier, "p3", 4, "PACKET"));

        // verify that the correct relationship is returned for a match on reorder code
        assertEquals(p3rel, rules.getProductSupplier(product3, supplier, "p3", -1, null));

        // verify that the correct relationship is returned for a match on package size and units
        assertEquals(p1rel1, rules.getProductSupplier(product1, supplier, "foo", 4, "BOX"));
        assertEquals(p1rel2, rules.getProductSupplier(product1, supplier, "bar", 3, "AMPOULE"));
        assertEquals(p2rel, rules.getProductSupplier(product2, supplier, "zoo", 4, "PACKET"));
        assertEquals(p3rel, rules.getProductSupplier(product3, supplier, "p?", 4, "PACKET"));

        // verify that the correct relationship is returned for a match on package size
        assertEquals(p1rel1, rules.getProductSupplier(product1, supplier, "foo", 4, null));
        assertEquals(p1rel2, rules.getProductSupplier(product1, supplier, "bar", 3, null));
        assertEquals(p2rel, rules.getProductSupplier(product2, supplier, "zoo", 4, null));
        assertEquals(p3rel, rules.getProductSupplier(product3, supplier, "p?", 4, null));

        // verify that nothing is returned if there is no direct match
        assertNull(rules.getProductSupplier(product1, supplier, "foo", 5, "BOX"));
        assertNull(rules.getProductSupplier(product1, supplier, "bar", 5, "PACKET"));
        assertNull(rules.getProductSupplier(product2, supplier, "zoo", 5, "PACKET"));
        assertNull(rules.getProductSupplier(product2, supplier, null, 5, null));
    }

    /**
     * Tests the {@link ProductRules#getProductSuppliers(Product)} method.
     */
    @Test
    public void testGetProductSuppliersForProduct() {
        Party supplier1 = supplierFactory.createSupplier();
        Party supplier2 = supplierFactory.createSupplier();
        Product product = productFactory.createMedication();

        ProductSupplier rel1 = rules.createProductSupplier(product, supplier1);
        ProductSupplier rel2 = rules.createProductSupplier(product, supplier2);
        rel1.save();
        rel2.save();

        List<ProductSupplier> relationships = rules.getProductSuppliers(product);
        assertEquals(2, relationships.size());
        assertEquals(rel1, relationships.get(0));  // relationships sorted on increasing id
        assertEquals(rel2, relationships.get(1));

        // deactivate one of the relationships, and verify it is no longer returned
        deactivateRelationship(rel1);

        relationships = rules.getProductSuppliers(product);
        assertEquals(1, relationships.size());
        assertEquals(rel2, relationships.get(0));
    }

    /**
     * Tests the {@link ProductRules#getBatches(Product, String, Date, Party)} method.
     */
    @Test
    public void testGetBatches() {
        Product product = productFactory.createMedication();
        Party manufacturer1 = supplierFactory.createManufacturer();
        Party manufacturer2 = supplierFactory.createManufacturer();
        Party manufacturer3 = supplierFactory.createManufacturer();
        List<Entity> batches = rules.getBatches(product, null, null, null);
        assertEquals(0, batches.size());

        Entity batch1 = rules.createBatch(product, "aa", getDatetime("2014-06-01 10:00:00"), manufacturer1);
        Entity batch2 = rules.createBatch(product, "ab", getDatetime("2014-07-01 07:00:00"), manufacturer2);
        Entity batch3 = rules.createBatch(product, "ac", getDatetime("2014-08-01 15:00:00"), manufacturer3);
        save(batch1, batch2, batch3);

        checkBatches(rules.getBatches(product, "a*", null, null), batch1, batch2, batch3);

        checkBatches(rules.getBatches(product, "a*", getDate("2014-06-01"), null), batch1);
        checkBatches(rules.getBatches(product, "a*", null, manufacturer1), batch1);

        checkBatches(rules.getBatches(product, null, null, manufacturer2), batch2);

        checkBatches(rules.getBatches(product, "ac", null, manufacturer3), batch3);
    }

    /**
     * Tests the {@link ProductRules#createBatch(Product, String, Date, Party)} and
     * {@link ProductRules#getBatchExpiry(Entity)} method.
     */
    @Test
    public void testCreateBatch() {
        Product product = productFactory.createMedication();
        Date expiry = getDate("2014-06-14");
        Party manufacturer = supplierFactory.createManufacturer();
        String batchNumber = "12345";
        Entity batch = rules.createBatch(product, batchNumber, expiry, manufacturer);
        assertTrue(batch.isNew());
        save(batch);

        batch = get(batch);
        IMObjectBean bean = getBean(batch);
        assertEquals(batchNumber, batch.getName());
        assertEquals(product.getObjectReference(), bean.getTargetRef("product"));
        assertEquals(expiry, rules.getBatchExpiry(batch));
        assertEquals(manufacturer.getObjectReference(), bean.getTargetRef("manufacturer"));
    }

    /**
     * Tests the {@link ProductRules#canUseProductAtLocation(Product, Party)} method.
     */
    @Test
    public void testCanUseProductAtLocation() {
        Product medication = productFactory.createMedication();
        Product merchandise = productFactory.createMerchandise();
        Product service = productFactory.createService();
        Product template = productFactory.createTemplate();
        Party location = practiceFactory.createLocation();

        // will always return true for medication and merchandise products
        assertTrue(rules.canUseProductAtLocation(medication, location));
        assertTrue(rules.canUseProductAtLocation(merchandise, location));
        assertTrue(rules.canUseProductAtLocation(service, location));
        assertTrue(rules.canUseProductAtLocation(template, location));

        // exclude the location for the service and template and verify they can no longer by used
        ProductTestHelper.addLocationExclusion(service, location);
        ProductTestHelper.addLocationExclusion(template, location);

        assertFalse(rules.canUseProductAtLocation(service, location));
        assertFalse(rules.canUseProductAtLocation(template, location));
    }

    /**
     * Tests the {@link ProductRules#isRestricted(Product)} method.
     */
    @Test
    public void testIsRestricted() {
        Product medication1 = productFactory.newMedication()
                .drugSchedule(false)
                .build();
        Product medication2 = productFactory.newMedication()
                .drugSchedule(true)
                .build();
        Product medication3 = productFactory.createMedication(); // no schedule
        Product merchandise = productFactory.createMerchandise();
        Product service = productFactory.createService();
        Product template = productFactory.createTemplate();
        assertFalse(rules.isRestricted(medication1));
        assertTrue(rules.isRestricted(medication2));
        assertFalse(rules.isRestricted(medication3));
        assertFalse(rules.isRestricted(merchandise));
        assertFalse(rules.isRestricted(service));
        assertFalse(rules.isRestricted(template));
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new ProductRules(getArchetypeService(), getLookupService());
    }

    /**
     * Verifies batches match those expected.
     *
     * @param matches the matches to check
     * @param batches the expected batches
     */
    private void checkBatches(List<Entity> matches, Entity... batches) {
        assertEquals(batches.length, matches.size());
        for (int i = 0; i < batches.length; ++i) {
            assertEquals(batches[i], matches.get(i));
        }
    }

    /**
     * Helper to deactivate a relationship and sleep for a second, so that
     * subsequent isActive() checks return false. The sleep in between
     * deactivating a relationship and calling isActive() is required due to
     * system time granularity.
     *
     * @param relationship the relationship to deactivate
     */
    private void deactivateRelationship(ProductSupplier relationship) {
        relationship.getRelationship().setActive(false);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ignore) {
            // do nothing
        }
    }
}
