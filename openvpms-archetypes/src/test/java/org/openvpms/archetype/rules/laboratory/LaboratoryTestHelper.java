/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.laboratory;

import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.party.Party;

import static org.openvpms.archetype.test.TestHelper.create;
import static org.openvpms.archetype.test.TestHelper.randomName;
import static org.openvpms.archetype.test.TestHelper.save;

/**
 * Laboratory helper methods.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestHelper {

    /**
     * Creates a test laboratory.
     *
     * @param locations the locations that the laboratory services
     * @return a new laboratory
     */
    public static Entity createLaboratory(Entity... locations) {
        Entity result = (Entity) TestHelper.create("entity.laboratoryServiceTest");
        result.setName(randomName("X-Laboratory-"));
        IMObjectBean bean = new IMObjectBean(result);
        for (Entity location : locations) {
            bean.addTarget("locations", location);
        }
        bean.save();
        return result;
    }

    /**
     * Creates a laboratory device.
     *
     * @param laboratory the laboratory that manages the device
     */
    public static Entity createDevice(Entity laboratory) {
        Entity result = TestHelper.create(LaboratoryArchetypes.DEVICE, Entity.class);
        result.setName(randomName("X-Device-"));

        EntityIdentity deviceId = TestHelper.create("entityIdentity.laboratoryDeviceTest", EntityIdentity.class);
        String id = randomName("DEVICE");
        deviceId.setIdentity(id);
        deviceId.setName(id);
        result.addIdentity(deviceId);

        IMObjectBean bean = new IMObjectBean(result);
        bean.setTarget("laboratory", laboratory);
        bean.save();
        return result;
    }

    /**
     * Creates a laboratory device.
     *
     * @param laboratory the laboratory that manages the device
     * @param locations  the locations where the device may be used
     */
    public static Entity createDevice(Entity laboratory, Party... locations) {
        Entity result = TestHelper.create(LaboratoryArchetypes.DEVICE, Entity.class);
        result.setName(randomName("X-Device-"));

        EntityIdentity deviceId = TestHelper.create("entityIdentity.laboratoryDeviceTest", EntityIdentity.class);
        String id = randomName("DEVICE");
        deviceId.setIdentity(id);
        deviceId.setName(id);
        result.addIdentity(deviceId);

        IMObjectBean bean = new IMObjectBean(result);
        bean.setTarget("laboratory", laboratory);
        for (Party location : locations) {
            bean.addTarget("locations", location);
        }
        bean.save();
        return result;
    }

    /**
     * Helper to create an <em>entity.laboratoryTest</em>.
     *
     * @param laboratories the laboratories that provide the test
     * @return a new test
     */
    public static Entity createTest(Entity investigationType, Entity... laboratories) {
        Entity result = createTest(investigationType);
        if (laboratories.length > 0) {
            IMObjectBean bean = new IMObjectBean(result);
            for (Entity lab : laboratories) {
                bean.addTarget("laboratories", lab);
            }
        }
        save(result);
        return result;
    }

    /**
     * Helper to create an <em>entity.laboratoryTest</em>.
     *
     * @return a new test
     */
    public static Entity createTest() {
        return createTest(createInvestigationType());
    }

    /**
     * Helper to create an <em>entity.laboratoryTest</em>.
     *
     * @param investigationType the investigation type
     * @return a new test
     */
    public static Entity createTest(Entity investigationType) {
        return createTest(investigationType, false);
    }

    /**
     * Helper to create an <em>entity.laboratoryTest</em>.
     *
     * @param investigationType the investigation type
     * @param group             if {@code true} the test can be grouped with others on an investigation
     * @return a new test
     */
    public static Entity createTest(Entity investigationType, boolean group) {
        return createTest(investigationType, group, "NO");
    }

    /**
     * Helper to create an <em>entity.laboratoryTest</em>.
     *
     * @param investigationType the investigation type
     * @param group             if {@code true} the test can be grouped with others on an investigation
     * @param useDevice         determines if a device is required
     * @return a new test
     */
    public static Entity createTest(Entity investigationType, boolean group, String useDevice) {
        Entity result = create(LaboratoryArchetypes.TEST, Entity.class);
        result.setName(randomName("X-LaboratoryTest-"));
        EntityIdentity code = create(LaboratoryArchetypes.TEST_CODE, EntityIdentity.class);
        code.setIdentity(result.getName());
        result.addIdentity(code);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setTarget("investigationType", investigationType);
        bean.setValue("group", group);
        bean.setValue("useDevice", useDevice);
        bean.save();
        return result;
    }

    /**
     * Helper to create an <em>entity.laboratoryTestHL7</em>.
     *
     * @param investigationType the investigation type
     * @return a new test
     */
    public static Entity createHL7Test(Entity investigationType) {
        Entity result = create(LaboratoryArchetypes.HL7_TEST, Entity.class);
        result.setName(randomName("X-HL7LaboratoryTest-"));
        EntityIdentity code = create(LaboratoryArchetypes.TEST_CODE, EntityIdentity.class);
        code.setIdentity(result.getName());
        result.addIdentity(code);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setTarget("investigationType", investigationType);
        bean.save();
        return result;
    }

    /**
     * Helper to create an <em>entity.investigationType</em>.
     *
     * @return a new investigation type
     */
    public static Entity createInvestigationType() {
        Entity result = create(LaboratoryArchetypes.INVESTIGATION_TYPE, Entity.class);
        result.setName(randomName("X-TestInvestigationType-"));
        save(result);
        return result;
    }

    /**
     * Helper to create an <em>entity.investigationType</em> linked to a laboratory.
     *
     * @param laboratory the laboratory
     * @param devices    the devices
     * @return a new investigation type
     */
    public static Entity createInvestigationType(Entity laboratory, Entity... devices) {
        Entity investigation = createInvestigationType();
        IMObjectBean bean = new IMObjectBean(investigation);
        bean.setTarget("laboratory", laboratory);
        for (Entity device : devices) {
            bean.addTarget("devices", device);
        }
        bean.save();
        return investigation;
    }
}
