/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.insurance;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link InsuranceRules} class.
 *
 * @author Tim Anderson
 */
public class InsuranceRulesTestCase extends ArchetypeServiceTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The insurance rules.
     */
    private InsuranceRules rules;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new InsuranceRules(getArchetypeService(), transactionManager);
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
    }

    /**
     * Tests the {@link InsuranceRules#createPolicy} method.
     */
    @Test
    public void testCreatePolicy() {
        Party insurer = insuranceFactory.createInsurer();

        Act policy1 = rules.createPolicy(customer, patient, insurer, null);
        assertTrue(policy1.isNew());
        assertTrue(policy1.isA(InsuranceArchetypes.POLICY));
        save(policy1);
        checkPolicy(policy1, customer, patient, insurer, null);

        Act policy2 = rules.createPolicy(customer, patient, insurer, "POL1234");
        save(policy2);
        checkPolicy(policy2, customer, patient, insurer, "POL1234");

        Act policy3 = rules.createPolicy(customer, patient, insurer, "POL987651");
        save(policy3);
        checkPolicy(policy3, customer, patient, insurer, "POL987651");
    }

    /**
     * Tests the {@link InsuranceRules#getPolicyForClaim} method when a policy is not associated with any claims.
     * <p>
     * These policies can be re-used.
     */
    @Test
    public void testGetPolicyForClaimForUnusedPolicy() {
        Party insurer1 = insuranceFactory.createInsurer();
        Party insurer2 = insuranceFactory.createInsurer();

        // verify that when no policy exists, a new one will be created
        Act policy1 = rules.getPolicyForClaim(customer, patient, insurer1, null, null, null);
        assertNotNull(policy1);
        assertTrue(policy1.isNew());
        save(policy1);
        checkPolicy(policy1, customer, patient, insurer1, null);

        // verify that the policy is reused when just the policy number changes
        Act policy2 = rules.getPolicyForClaim(customer, patient, insurer1, "POL1234", null, null);
        assertEquals(policy1, policy2);
        checkPolicy(policy2, customer, patient, insurer1, "POL1234");

        // verify that the policy is reused when just the policy number changes
        Act policy3 = rules.getPolicyForClaim(customer, patient, insurer1, "POL5678", null, null);
        assertEquals(policy1, policy3);
        checkPolicy(policy3, customer, patient, insurer1, "POL5678");

        // verify that the policy is reused when the insurer changes
        Act policy4 = rules.getPolicyForClaim(customer, patient, insurer2, "POL5678", null, null);
        assertEquals(policy1, policy4);
        checkPolicy(policy4, customer, patient, insurer2, "POL5678");

        // verify that the policy is reused when the insurer and policy number changes
        Act policy5 = rules.getPolicyForClaim(customer, patient, insurer1, "POL1234", null, null);
        assertEquals(policy1, policy5);
        checkPolicy(policy5, customer, patient, insurer1, "POL1234");
    }

    /**
     * Tests the {@link InsuranceRules#getPolicyForClaim} method.
     * <p>
     * Verifies that a policy won't be re-used if there is an existing policy for a different patient belonging
     * to the same customer.
     */
    @Test
    public void testGetPolicyForClaimForDifferentPatient() {
        Party patient2 = patientFactory.createPatient(customer);
        Party insurer1 = insuranceFactory.createInsurer();
        Act policy1 = rules.getPolicyForClaim(customer, patient, insurer1, null, null, null);
        save(policy1);

        Act policy2 = rules.getPolicyForClaim(customer, patient2, insurer1, null, null, null);
        assertNotEquals(policy1, policy2);
    }

    /**
     * Tests the behaviour of {@link InsuranceRules#getPolicyForClaim} when the patient has policies with
     * associated claims.
     */
    @Test
    public void testGetPolicyForClaimWithExistingClaims() {
        Party insurer1 = insuranceFactory.createInsurer();
        User clinician = userFactory.createClinician();
        Act policy1 = rules.createPolicy(customer, patient, insurer1, "POL12345");
        save(policy1);
        FinancialAct claim1 = insuranceFactory.newClaim()
                .policy(policy1)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item().diagnosis("VENOM_328").add()
                .build();

        Act same1 = rules.getPolicyForClaim(customer, patient, insurer1, "POL12345", null, null);
        assertEquals(policy1, same1);
        checkPolicy(same1, customer, patient, insurer1, "POL12345"); // verify the details haven't changed

        // verify a new policy is created if the policy number changes
        Act policy2 = rules.getPolicyForClaim(customer, patient, insurer1, "POL98765", null, null);
        assertNotEquals(policy1, policy2);
        checkPolicy(policy2, customer, patient, insurer1, "POL98765");
        save(policy2);

        // associate policy2 with a new claim
        FinancialAct claim2 = insuranceFactory.newClaim()
                .policy(policy2)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item().diagnosis("VENOM_328").add()
                .build();

        // verify policy2 is updated when the policy number changes, and policy2 is passed as the existing policy
        Act same2 = rules.getPolicyForClaim(customer, patient, insurer1, "POL222222", claim2, policy2);
        assertEquals(policy2, same2);
        checkPolicy(policy2, customer, patient, insurer1, "POL222222");
    }

    /**
     * Tests the {@link InsuranceRules#getCurrentPolicy} method.
     */
    @Test
    public void testGetCurrentPolicy() {
        assertNull(rules.getCurrentPolicy(customer, patient));
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);
        assertEquals(policy, rules.getCurrentPolicy(customer, patient));

        policy.setActivityStartTime(DateRules.getYesterday());
        policy.setActivityEndTime(DateRules.getToday());
        save(policy);
        assertNull(rules.getCurrentPolicy(customer, patient));

        policy.setActivityEndTime(null);
        save(policy);
        assertEquals(policy, rules.getCurrentPolicy(customer, patient));
    }

    /**
     * Tests the {@link InsuranceRules#getCurrentPolicies} method.
     */
    @Test
    public void testGetCurrentPolicies() {
        assertNull(rules.getCurrentPolicy(customer, patient));
        Act policy1 = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        Act policy2 = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        Act policy3 = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        Date today = DateRules.getToday();
        policy1.setActivityStartTime(DateRules.getDate(today, -1, DateUnits.YEARS));
        policy1.setActivityEndTime(DateRules.getYesterday());
        policy2.setActivityStartTime(DateRules.getDate(today, -6, DateUnits.MONTHS));
        policy2.setActivityEndTime(null);
        policy3.setActivityStartTime(today);
        save(policy1, policy2, policy3);
        List<Act> policies = rules.getCurrentPolicies(customer, patient);
        assertEquals(2, policies.size());
        assertEquals(policy3, policies.get(0));  // most recent first
        assertEquals(policy2, policies.get(1));
    }

    /**
     * Tests the {@link InsuranceRules#createClaim(Act)} method.
     */
    @Test
    public void testCreateClaim() {
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);
        FinancialAct claim = rules.createClaim(policy);
        assertNotNull(claim);
        assertTrue(claim.isA(InsuranceArchetypes.CLAIM));
        IMObjectBean bean = getBean(claim);
        assertEquals(policy, bean.getTarget("policy"));
    }

    /**
     * Tests the {@link InsuranceRules#isClaimed(FinancialAct)} method.
     */
    @Test
    public void testIsClaimed() {
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);

        User clinician = userFactory.createClinician();
        FinancialAct invoice1Item1 = createInvoiceItem();
        FinancialAct invoice1Item2 = createInvoiceItem();
        FinancialAct invoice1Item3 = createInvoiceItem();
        FinancialAct invoice1 = createInvoice(FinancialActStatus.POSTED, invoice1Item1, invoice1Item2, invoice1Item3);

        assertFalse(rules.isClaimed(invoice1));

        FinancialAct claim1 = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item(invoice1Item1, invoice1Item2)
                .build();
        assertTrue(rules.isClaimed(invoice1));

        claim1.setStatus(ClaimStatus.CANCELLED);
        save(claim1);
        assertFalse(rules.isClaimed(invoice1));
    }

    /**
     * Tests the {@link InsuranceRules#getCurrentClaims} method.
     */
    @Test
    public void testGetCurrentClaims() {
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);

        User clinician = userFactory.createClinician();
        FinancialAct invoice1Item1 = createInvoiceItem();
        FinancialAct invoice1Item2 = createInvoiceItem();
        FinancialAct invoice1Item3 = createInvoiceItem();
        FinancialAct invoice1 = createInvoice(FinancialActStatus.POSTED, invoice1Item1, invoice1Item2, invoice1Item3);

        FinancialAct invoice2Item1 = createInvoiceItem();
        FinancialAct invoice2 = createInvoice(FinancialActStatus.POSTED, invoice2Item1);

        FinancialAct claim1 = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item(invoice1Item1, invoice1Item2)
                .build();
        FinancialAct claim2 = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item(invoice1Item3)
                .build();

        checkCurrentClaims(invoice1, claim1, claim2);

        checkCurrentClaims(invoice2); // not associated with any claims

        claim1.setStatus(ClaimStatus.ACCEPTED);
        save(claim1);
        checkCurrentClaims(invoice1, claim1, claim2);

        claim1.setStatus(ClaimStatus.SETTLED);
        save(claim1);
        checkCurrentClaims(invoice1, claim2);

        claim1.setStatus(ClaimStatus.DECLINED);
        save(claim1);
        checkCurrentClaims(invoice1, claim2);

        claim1.setStatus(ClaimStatus.CANCELLED);
        save(claim1);
        checkCurrentClaims(invoice1, claim2);
    }

    /**
     * Tests the {@link InsuranceRules#getCurrentGapClaims} method.
     */
    @Test
    public void testGetCurrentGapClaims() {
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);

        User clinician = userFactory.createClinician();
        FinancialAct invoice1Item1 = createInvoiceItem();
        FinancialAct invoice1Item2 = createInvoiceItem();
        FinancialAct invoice1Item3 = createInvoiceItem();
        FinancialAct invoice1 = createInvoice(FinancialActStatus.POSTED, invoice1Item1, invoice1Item2, invoice1Item3);

        FinancialAct invoice2Item1 = createInvoiceItem();
        FinancialAct invoice2 = createInvoice(FinancialActStatus.POSTED, invoice2Item1);

        FinancialAct claim1 = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .gapClaim(true)
                .item(invoice1Item1, invoice1Item2)
                .build();
        FinancialAct claim2 = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .gapClaim(true)
                .item(invoice1Item3)
                .build();

        checkCurrentGapClaims(invoice1, claim1, claim2);

        checkCurrentGapClaims(invoice2); // not associated with any claims

        claim1.setStatus(ClaimStatus.ACCEPTED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim1, claim2);

        claim1.setStatus2(ClaimStatus.GAP_CLAIM_PENDING);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim1, claim2);

        claim1.setStatus2(ClaimStatus.GAP_CLAIM_RECEIVED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim1, claim2);

        claim1.setStatus2(ClaimStatus.GAP_CLAIM_PAID);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim2);

        claim1.setStatus2(ClaimStatus.GAP_CLAIM_NOTIFIED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim2);

        claim1.setStatus(ClaimStatus.SETTLED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim2);

        claim1.setStatus(ClaimStatus.DECLINED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim2);

        claim1.setStatus(ClaimStatus.CANCELLED);
        save(claim1);
        checkCurrentGapClaims(invoice1, claim2);
    }

    /**
     * Tests the {@link InsuranceRules#canChangePolicyNumber(Act)} method.
     */
    @Test
    public void testCanChangePolicyNumber() {
        Act policy = rules.createPolicy(customer, patient, insuranceFactory.createInsurer(), null);
        save(policy);

        assertTrue(rules.canChangePolicyNumber(policy));

        User clinician = userFactory.createClinician();
        FinancialAct claim = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .item().diagnosis("VENOM_328", "Abcess", "328").add()
                .build();
        assertEquals(ClaimStatus.PENDING, claim.getStatus());
        assertTrue(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.POSTED);
        save(claim);
        assertTrue(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.SUBMITTED);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.ACCEPTED);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.CANCELLING);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.CANCELLED);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.SETTLED);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));

        claim.setStatus(ClaimStatus.DECLINED);
        save(claim);
        assertFalse(rules.canChangePolicyNumber(policy));
    }

    /**
     * Tests the {@link InsuranceRules#hasBenefitAmount(FinancialAct)} method.
     */
    @Test
    public void testHasBenefitAmount() {
        // verifies that in order for a gap claim to have a benefit amount, status2 cannot be null or PENDING
        FinancialAct claim = create(InsuranceArchetypes.CLAIM, FinancialAct.class);
        assertNull(claim.getStatus2());
        assertFalse(rules.hasBenefitAmount(claim));

        claim.setStatus2(ClaimStatus.GAP_CLAIM_PENDING);
        assertFalse(rules.hasBenefitAmount(claim));

        claim.setStatus2(ClaimStatus.GAP_CLAIM_RECEIVED);
        assertTrue(rules.hasBenefitAmount(claim));

        claim.setStatus2(ClaimStatus.GAP_CLAIM_PAID);
        assertTrue(rules.hasBenefitAmount(claim));

        claim.setStatus2(ClaimStatus.GAP_CLAIM_NOTIFIED);
        assertTrue(rules.hasBenefitAmount(claim));
    }

    /**
     * Tests the {@link InsuranceRules#getGapAmount(FinancialAct)}
     * and {@link InsuranceRules#getGapAmount(FinancialAct)} methods.
     */
    @Test
    public void testGetGapAmount() {
        FinancialAct claim = create(InsuranceArchetypes.CLAIM, FinancialAct.class);
        checkEquals(ZERO, rules.getGapAmount(claim));

        IMObjectBean bean = getBean(claim);
        bean.setValue("amount", 1000);
        bean.setValue("benefitAmount", 800);
        checkEquals(BigDecimal.valueOf(200), rules.getGapAmount(claim));

        assertEquals(BigDecimal.valueOf(300), rules.getGapAmount(BigDecimal.valueOf(500), BigDecimal.valueOf(200)));
    }


    @Test
    public void testGetMostRecentTreatmentDate() {
        Party location = practiceFactory.createLocation();
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();
        Entity service1 = insuranceFactory.createInsuranceService();
        Party insurer1 = insuranceFactory.newInsurer()
                .insuranceService(service1)
                .build();
        Party insurer2 = insuranceFactory.createInsurer();
        Party insurer3 = insuranceFactory.newInsurer()
                .insuranceService(service1)
                .build();
        Act policy1 = insuranceFactory.createPolicy(customer, patient, insurer1, "P1");

        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer1, null));
        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer2, null));
        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer3, null));

        insuranceFactory.newClaim()
                .policy(policy1)
                .location(location)
                .item().treatmentDates("2021-01-01 09:00", "2021-01-01 10:00").add()
                .item().treatmentDates("2021-02-01 09:00", "2021-02-01 10:00").add()
                .clinician(clinician)
                .claimHandler(clinician)
                .status(ClaimStatus.SETTLED)
                .build();
        assertEquals(getDatetime("2021-02-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer1, null));
        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer2, null)); // no service
        assertEquals(getDatetime("2021-02-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer3, null));

        Act policy2 = insuranceFactory.createPolicy(customer, patient, insurer2, "P2");
        insuranceFactory.newClaim().policy(policy2).location(location)
                .item().treatmentDates("2022-03-01 09:00", "2022-03-01 10:00").add()
                .item().treatmentDates("2022-04-01 09:00", "2022-04-01 10:00").add()
                .clinician(clinician)
                .claimHandler(clinician)
                .status(ClaimStatus.ACCEPTED)
                .build();
        assertEquals(getDatetime("2021-02-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer1, null));
        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer2, null));
        assertEquals(getDatetime("2021-02-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer3, null));

        Act policy3 = insuranceFactory.createPolicy(customer, patient, insurer3, "P3");
        insuranceFactory.newClaim().policy(policy3).location(location)
                .item().treatmentDates("2023-03-01 09:00", "2023-03-01 10:00").add()
                .item().treatmentDates("2023-04-01 09:00", "2023-04-01 10:00").add()
                .clinician(clinician)
                .claimHandler(clinician)
                .status(ClaimStatus.SUBMITTED)
                .build();
        assertEquals(getDatetime("2023-04-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer1, null));
        assertNull(rules.getMostRecentTreatmentDateForService(patient, insurer2, null));
        assertEquals(getDatetime("2023-04-01 10:00"),
                     rules.getMostRecentTreatmentDateForService(patient, insurer3, null));
    }

    /**
     * Verifies that {@link InsuranceRules#getCurrentClaims} returns the expected claims.
     *
     * @param invoice the invoice
     * @param claims  the expected claims
     */
    private void checkCurrentClaims(FinancialAct invoice, FinancialAct... claims) {
        List<FinancialAct> currentClaims = rules.getCurrentClaims(invoice);
        assertEquals(claims.length, currentClaims.size());
        for (FinancialAct claim : claims) {
            assertTrue(currentClaims.contains(claim));
        }
    }

    /**
     * Verifies that {@link InsuranceRules#getCurrentGapClaims} returns the expected claims.
     *
     * @param invoice the invoice
     * @param claims  the expected claims
     */
    private void checkCurrentGapClaims(FinancialAct invoice, FinancialAct... claims) {
        List<FinancialAct> currentClaims = rules.getCurrentGapClaims(invoice);
        assertEquals(claims.length, currentClaims.size());
        for (FinancialAct claim : claims) {
            assertTrue(currentClaims.contains(claim));
        }
    }

    /**
     * Creates and saves an invoice.
     *
     * @param status the invoice status
     * @param items  the invoice items
     * @return the invoice
     */
    private FinancialAct createInvoice(String status, FinancialAct... items) {
        return accountFactory.newInvoice()
                .customer(customer)
                .clinician(userFactory.createClinician())
                .status(status)
                .add(items)
                .build();
    }

    /**
     * Creates an invoice item, with quantity=1, price=10, discount=1, tax=0.82, total=9
     *
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem() {
        return accountFactory.newInvoiceItem()
                .patient(patient)
                .clinician(userFactory.createClinician())
                .product(productFactory.createMedication())
                .quantity(1)
                .unitPrice(10)
                .discount(1)
                .tax(new BigDecimal("0.82"))
                .build(false);
    }

    /**
     * Checks a policy.
     *
     * @param policy       the policy
     * @param customer     the expected customer
     * @param patient      the expected patient
     * @param insurer      the expected insurer
     * @param policyNumber the expected policy number. May be {@code null}
     */
    private void checkPolicy(Act policy, Party customer, Party patient, Party insurer, String policyNumber) {
        IMObjectBean bean = getBean(policy);
        assertEquals(customer, bean.getTarget("customer"));
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(insurer, bean.getTarget("insurer"));
        assertEquals(policyNumber, rules.getPolicyNumber(policy));
        assertNotNull(policy.getActivityStartTime());
        assertNull(policy.getActivityEndTime());
    }
}
