/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.EmailTemplate.ContentType;
import org.openvpms.archetype.rules.doc.EmailTemplate.SubjectType;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EmailTemplate} class.
 *
 * @author Tim Anderson
 */
public class EmailTemplateTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Verifies the default values of the <em>entity.documentTemplateEmailSystem</em> and
     * <em>entity.documentTemplateEmailUser</em> archetypes.
     */
    @Test
    public void testDefaults() {
        checkDefaults(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE);
        checkDefaults(DocumentArchetypes.USER_EMAIL_TEMPLATE);
    }

    /**
     * Tests the accessors.
     */
    @Test
    public void testAccessors() {
        Document handout1 = documentFactory.createPDF("Patient Handout.pdf");
        Entity attachment1 = documentFactory.newTemplate()
                .name("Patient Handout")
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(handout1)
                .build();

        Document handout2 = documentFactory.createPDF("Customer Handout.pdf");
        Entity attachment2 = documentFactory.newTemplate()
                .name("Customer Handout")
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document(handout2)
                .build();

        Document handout3 = documentFactory.createPDF("Inactive Handout.pdf");
        Entity attachment3  = documentFactory.newTemplate()   // this attachment should be excluded, as it is inactive
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document(handout3)
                .active(false)
                .build();
        Entity attachment4 = documentFactory.newTemplate()  // this attachment should be excluded, as it is not a PDF
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .blankDocument()
                .build();

        Entity entity = documentFactory.newEmailTemplate()
                .name("Z Test Email Template")
                .subject("plain text subject")
                .subjectSource(".")  // ignored for plain text
                .content("content ignored when document set")
                .document("/documents/Vaccination Reminders.jrxml", "text/xml")
                .contentSource("$patient")
                .addAttachments(attachment1, attachment2, attachment3, attachment4)
                .build();

        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());
        assertEquals("Z Test Email Template", template.getName());
        assertEquals(SubjectType.TEXT, template.getSubjectType());
        assertEquals("plain text subject", template.getSubject());
        assertEquals(".", template.getSubjectSource());
        assertEquals(ContentType.DOCUMENT, template.getContentType());
        assertEquals("content ignored when document set", template.getContent());
        assertNotNull(template.getDocument());
        List<DocumentTemplate> attachmentsById = template.getAttachments();
        assertEquals(2, attachmentsById.size());                             // inactive, non PDF attachments ignored
        assertEquals(attachment1, attachmentsById.get(0).getEntity());       // attachments sorted on increasing id
        assertEquals(attachment2, attachmentsById.get(1).getEntity());

        List<DocumentTemplate> attachmentsByName = template.getAttachments(true);
        assertEquals(2, attachmentsByName.size());
        assertEquals(attachment2, attachmentsByName.get(0).getEntity());       // attachments sorted on increasing name
        assertEquals(attachment1, attachmentsByName.get(1).getEntity());
    }

    /**
     * Verifies the default values of an email template archetype match that expected.
     *
     * @param archetype the archetype
     */
    private void checkDefaults(String archetype) {
        Entity entity = create(archetype, Entity.class);
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());
        assertNull(template.getName());
        assertEquals(SubjectType.TEXT, template.getSubjectType());
        assertNull(template.getSubject());
        assertNull(template.getSubjectSource());
        assertEquals(ContentType.TEXT, template.getContentType());
        assertNull(template.getContent());
        assertNull(template.getDocument());
        assertTrue(template.getAttachments().isEmpty());
    }
}