/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.laboratory;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link LaboratoryRules} class.
 *
 * @author Tim Anderson
 */
public class LaboratoryRulesTestCase extends ArchetypeServiceTest {

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The rules.
     */
    private LaboratoryRules rules;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        rules = new LaboratoryRules(getArchetypeService());
    }

    /**
     * Tests the {@link LaboratoryRules#getLaboratory} method for an <em>entity.laboratoryService*</em> laboratory.
     */
    @Test
    public void testGetLaboratory() {
        Party locationA = practiceFactory.createLocation();
        Party locationB = practiceFactory.createLocation();
        Party locationC = practiceFactory.createLocation();

        // check a laboratory that is only available at a single location
        Entity laboratory1 = laboratoryFactory.createLaboratory(locationA);
        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory1);

        assertEquals(laboratory1, rules.getLaboratory(investigationType1, locationA));
        assertNull(rules.getLaboratory(investigationType1, locationB));

        // check a laboratory that is available at 2 locations
        Entity laboratory2 = laboratoryFactory.createLaboratory(locationA, locationB);
        Entity investigationType2 = laboratoryFactory.createInvestigationType(laboratory2);
        assertEquals(laboratory2, rules.getLaboratory(investigationType2, locationA));
        assertEquals(laboratory2, rules.getLaboratory(investigationType2, locationB));
        assertNull(rules.getLaboratory(investigationType2, locationC));

        // check a laboratory that is available at any location
        Entity laboratory3 = laboratoryFactory.createLaboratory();

        Entity investigationType3 = laboratoryFactory.createInvestigationType(laboratory3);
        assertEquals(laboratory3, rules.getLaboratory(investigationType3, locationA));
        assertEquals(laboratory3, rules.getLaboratory(investigationType3, locationB));
        assertEquals(laboratory3, rules.getLaboratory(investigationType3, locationC));

        // check an investigation not associated with any laboratory
        Entity investigationType4 = laboratoryFactory.createInvestigationType();
        assertNull(rules.getLaboratory(investigationType4, locationA));
    }

    /**
     * Tests the {@link LaboratoryRules#getLaboratory} method for HL7 laboratories and groups.
     */
    @Test
    public void testGetLaboratoryForHL7() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.createLocation();
        User user = userFactory.createUser();
        Entity laboratory1 = laboratoryFactory.createHL7Laboratory(location1, user);
        Entity laboratory2 = laboratoryFactory.createHL7Laboratory(location2, user);
        Entity group = laboratoryFactory.createHL7LaboratoryGroup(laboratory1, laboratory2);

        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory1);
        Entity investigationType2 = laboratoryFactory.createInvestigationType(group);

        assertEquals(laboratory1, rules.getLaboratory(investigationType1, location1));
        assertNull(rules.getLaboratory(investigationType1, location2));

        // check group handling
        assertEquals(laboratory1, rules.getLaboratory(investigationType2, location1));
        assertEquals(laboratory2, rules.getLaboratory(investigationType2, location2));
        assertNull(rules.getLaboratory(investigationType2, location3));
    }

    /**
     * Tests the {@link LaboratoryRules#createOrder(Act)} method.
     */
    @Test
    public void testCreateOrder() {
        Party patient = patientFactory.createPatient();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity laboratory = laboratoryFactory.createLaboratory(location);
        Entity device = laboratoryFactory.createDevice(laboratory);
        Entity test1 = laboratoryFactory.createTest(investigationType);
        Entity test2 = laboratoryFactory.createTest(investigationType);
        User clinician = userFactory.createClinician();

        Act investigation = patientFactory.newInvestigation()
                .patient(patient)
                .location(location)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .device(device)
                .addTests(test1, test2)
                .clinician(clinician)
                .description("some notes")
                .build();

        Act order = rules.createOrder(investigation);
        assertNotNull(order);
        assertTrue(order.isNew());
        save(order);

        // reload to compare persistent instances
        investigation = get(investigation);
        order = get(order);
        IMObjectBean bean = getBean(order);

        // investigation start time is duplicated to the order
        assertEquals(investigation.getActivityStartTime(), order.getActivityStartTime());

        // order status and result status should both be PENDING
        assertEquals(LaboratoryOrderStatus.PENDING, order.getStatus());
        assertEquals(LaboratoryOrderStatus.PENDING, order.getStatus2());

        // investigation id is copied to investigationId node
        ActIdentity investigationId = bean.getObject("investigationId", ActIdentity.class);
        assertNotNull(investigationId);
        assertEquals(Long.toString(investigation.getId()), investigationId.getIdentity());

        assertEquals("some notes", order.getDescription());
        assertEquals("NEW", bean.getString("type"));

        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(laboratory, bean.getTarget("laboratory"));
        assertEquals(investigationType, bean.getTarget("investigationType"));
        assertEquals(device, bean.getTarget("device"));
        assertEquals(location, bean.getTarget("location"));

        // check tests
        List<Entity> tests = bean.getTargets("tests", Entity.class);
        assertEquals(2, tests.size());
        assertTrue(tests.contains(test1));
        assertTrue(tests.contains(test2));

        assertEquals(clinician, bean.getTarget("clinician"));

        // verify there is a link to the investigation
        assertEquals(investigation, bean.getSource("investigation"));
    }

    /**
     * Tests the {@link LaboratoryRules#isUnsubmittedInvestigation(Act)} method.
     */
    @Test
    public void testIsUnsubmittedInvestigation() {
        Party patient = patientFactory.createPatient();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity laboratory = laboratoryFactory.createLaboratory(location);

        String[] unsubmitted = {InvestigationActStatus.PENDING, InvestigationActStatus.CONFIRM,
                                InvestigationActStatus.CONFIRM_DEFERRED};
        String[] submitted = {InvestigationActStatus.SENT, InvestigationActStatus.ERROR,
                              InvestigationActStatus.RECEIVED, InvestigationActStatus.WAITING_FOR_SAMPLE,
                              InvestigationActStatus.PARTIAL_RESULTS, InvestigationActStatus.RECEIVED,
                              InvestigationActStatus.REVIEWED};

        // verify default investigation status is PENDING
        Act pendingInvestigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .build();
        assertTrue(rules.isUnsubmittedInvestigation(pendingInvestigation));

        // verify unsubmitted investigations
        for (String status2 : unsubmitted) {
            Act investigation = patientFactory.newInvestigation()
                    .patient(patient)
                    .investigationType(investigationType)
                    .laboratory(laboratory)
                    .location(location)
                    .status2(status2)
                    .build();
            assertTrue(rules.isUnsubmittedInvestigation(investigation));
        }

        // verify submitted investigations
        for (String status2 : submitted) {
            Act investigation = patientFactory.newInvestigation()
                    .patient(patient)
                    .investigationType(investigationType)
                    .laboratory(laboratory)
                    .location(location)
                    .status2(status2)
                    .build();
            assertFalse(rules.isUnsubmittedInvestigation(investigation));
        }

        // no laboratory
        Act noLabInvestigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .build();
        assertFalse(rules.isUnsubmittedInvestigation(noLabInvestigation));

        // hl7 laboratory
        Act hl7investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratoryFactory.createHL7Laboratory(location, userFactory.createUser()))
                .build();
        assertFalse(rules.isUnsubmittedInvestigation(hl7investigation));

        // cancelled investigation
        Act cancelledInvestigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .status(InvestigationActStatus.CANCELLED)
                .status2(InvestigationActStatus.PENDING)
                .build();
        assertFalse(rules.isUnsubmittedInvestigation(cancelledInvestigation));
    }

    /**
     * Tests the {@link LaboratoryRules#canUseLaboratoryAtLocation} method for an <em>entity.laboratoryService*</em>
     * laboratory.
     */
    @Test
    public void testCanUseLaboratoryAtLocation() {
        Party locationA = practiceFactory.createLocation();
        Party locationB = practiceFactory.createLocation();
        Party locationC = practiceFactory.createLocation();

        // check a laboratory that is only available at a single location
        Entity laboratory1 = laboratoryFactory.createLaboratory(locationA);
        assertTrue(rules.canUseLaboratoryAtLocation(laboratory1, locationA));
        assertFalse(rules.canUseLaboratoryAtLocation(laboratory1, locationB));

        // check a laboratory that is available at 2 locations
        Entity laboratory2 = laboratoryFactory.createLaboratory(locationA, locationB);
        assertTrue(rules.canUseLaboratoryAtLocation(laboratory2, locationA));
        assertTrue(rules.canUseLaboratoryAtLocation(laboratory2, locationB));
        assertFalse(rules.canUseLaboratoryAtLocation(laboratory2, locationC));

        // check a laboratory that is available at any location
        Entity laboratory3 = laboratoryFactory.createLaboratory();
        assertTrue(rules.canUseLaboratoryAtLocation(laboratory3, locationA));
        assertTrue(rules.canUseLaboratoryAtLocation(laboratory3, locationB));
    }

    /**
     * Tests the {@link LaboratoryRules#canUseLaboratoryAtLocation} method for HL7 laboratories and groups.
     */
    @Test
    public void testUseLaboratoryAtLocationForHL7() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.createLocation();
        User user = userFactory.createUser();
        Entity laboratory1 = laboratoryFactory.createHL7Laboratory(location1, user);
        Entity laboratory2 = laboratoryFactory.createHL7Laboratory(location2, user);
        Entity group = laboratoryFactory.createHL7LaboratoryGroup(laboratory1, laboratory2);

        assertTrue(rules.canUseLaboratoryAtLocation(laboratory1, location1));
        assertFalse(rules.canUseLaboratoryAtLocation(laboratory1, location2));
        assertTrue(rules.canUseLaboratoryAtLocation(group, location1));
        assertTrue(rules.canUseLaboratoryAtLocation(group, location2));
        assertFalse(rules.canUseLaboratoryAtLocation(group, location3));
    }

    /**
     * Tests the {@link LaboratoryRules#canUseDeviceAtLocation(Entity, Party)} method.
     */
    @Test
    public void testCanUseDeviceAtLocation() {
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        Party location3 = practiceFactory.createLocation();
        Entity laboratory = laboratoryFactory.createLaboratory();

        // test a device available at a single location
        Entity device1 = laboratoryFactory.createDevice(laboratory, location1);
        assertTrue(rules.canUseDeviceAtLocation(device1, location1));
        assertFalse(rules.canUseDeviceAtLocation(device1, location2));

        // test a device available at 2 locations
        Entity device2 = laboratoryFactory.createDevice(laboratory, location1, location2);
        assertTrue(rules.canUseDeviceAtLocation(device2, location1));
        assertTrue(rules.canUseDeviceAtLocation(device2, location2));
        assertFalse(rules.canUseDeviceAtLocation(device1, location3));

        // test a device available at all locations
        Entity device3 = laboratoryFactory.createDevice(laboratory);
        assertTrue(rules.canUseDeviceAtLocation(device3, location1));
        assertTrue(rules.canUseDeviceAtLocation(device3, location3));
    }
}
