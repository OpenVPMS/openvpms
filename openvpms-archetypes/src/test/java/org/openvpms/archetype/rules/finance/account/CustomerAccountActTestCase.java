/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the <em>act.customerAccount*</em> archetypes.
 *
 * @author Tim Anderson
 */
public class CustomerAccountActTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies that when a payment is deleted, the payment items are also deleted, but not any EFTPOS acts.
     */
    @Test
    public void testDeletePayment() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = accountFactory.newEFTPOSPayment().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();

        FinancialAct item = accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);

        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();

        assertNotNull(get(payment));
        assertNotNull(get(item));
        assertNotNull(get(transaction));

        // verify the EFTPOS transaction is linked to the item
        List<IMObject> eft = getBean(item).getTargets("eft");
        assertEquals(1, eft.size());
        assertEquals(transaction, eft.get(0));

        // verify that deleting the payment deletes the item, but not the EFTPOS transaction
        remove(payment);
        assertNull(get(item));
        assertNotNull(get(transaction));
    }

    /**
     * Verifies that when a refund is deleted, the refund items are also deleted, but not any EFTPOS acts.
     */
    @Test
    public void testDeleteRefund() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = accountFactory.newEFTPOSRefund().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();

        FinancialAct item = accountFactory.newEFTRefundItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);

        FinancialAct refund = accountFactory.newRefund()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();

        assertNotNull(get(refund));
        assertNotNull(get(item));
        assertNotNull(get(transaction));

        // verify the EFTPOS transaction is linked to the item
        List<IMObject> eft = getBean(item).getTargets("eft");
        assertEquals(1, eft.size());
        assertEquals(transaction, eft.get(0));

        // verify that deleting the refund deletes the item, but not the EFTPOS transaction
        remove(refund);
        assertNull(get(item));
        assertNotNull(get(transaction));
    }

}
