/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.checkEquals;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Product price helper methods for testing purposes.
 *
 * @author Tim Anderson
 */
public class ProductPriceTestHelper {

    /**
     * Helper to create a new fixed price.
     *
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @param pricingGroup the pricing group
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(Date from, Date to, boolean defaultPrice, Lookup pricingGroup) {
        ProductPrice result = createFixedPrice(from, to, defaultPrice);
        result.setName("Fixed Price - " + pricingGroup.getName());
        result.addClassification(pricingGroup);
        return result;
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(Date from, Date to, boolean defaultPrice) {
        ProductPrice result = createPrice(ProductArchetypes.FIXED_PRICE, from, to);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setValue("default", defaultPrice);
        return result;
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(BigDecimal price, BigDecimal cost, BigDecimal markup,
                                                BigDecimal maxDiscount, String from, String to, boolean defaultPrice) {
        ProductPrice result = createFixedPrice(from, to, defaultPrice);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setValue("price", price);
        bean.setValue("cost", cost);
        bean.setValue("markup", markup);
        bean.setValue("maxDiscount", maxDiscount);
        return result;
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(BigDecimal price, BigDecimal cost, BigDecimal markup,
                                                BigDecimal maxDiscount, Date from, Date to, boolean defaultPrice) {
        ProductPrice result = createFixedPrice(from, to, defaultPrice);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setValue("price", price);
        bean.setValue("cost", cost);
        bean.setValue("markup", markup);
        bean.setValue("maxDiscount", maxDiscount);
        return result;
    }

    /**
     * Helper to create a new unit price.
     *
     * @param from the active from date. May be {@code null}
     * @param to   the active to date. May be {@code null}
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(String from, String to) {
        return createPrice(ProductArchetypes.UNIT_PRICE, from, to);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param from the active from date. May be {@code null}
     * @param to   the active to date. May be {@code null}
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(Date from, Date to) {
        return createPrice(ProductArchetypes.UNIT_PRICE, from, to);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param pricingGroup the pricing group
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(String from, String to, Lookup pricingGroup) {
        ProductPrice price = createPrice(ProductArchetypes.UNIT_PRICE, from, to);
        price.setName("Unit Price - " + pricingGroup.getName());
        price.addClassification(pricingGroup);
        return price;
    }

    /**
     * Helper to create a new unit price with 100% markup and max discount.
     *
     * @param price the price
     * @param cost  the cost price
     * @param from  the active from date. May be {@code null}
     * @param to    the active to date. May be {@code null}
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(BigDecimal price, BigDecimal cost, Date from, Date to) {
        return createUnitPrice(price, cost, MathRules.ONE_HUNDRED, MathRules.ONE_HUNDRED, from, to);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(BigDecimal price, BigDecimal cost, BigDecimal markup,
                                               BigDecimal maxDiscount, String from, String to) {
        return createUnitPrice(price, cost, markup, maxDiscount, getDate(from), getDate(to));
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new fixed price
     */
    public static ProductPrice createUnitPrice(BigDecimal price, BigDecimal cost, BigDecimal markup,
                                               BigDecimal maxDiscount, Date from, Date to) {
        ProductPrice result = createUnitPrice(from, to);
        IMObjectBean bean = new IMObjectBean(result);
        bean.setValue("price", price);
        bean.setValue("cost", cost);
        bean.setValue("markup", markup);
        bean.setValue("maxDiscount", maxDiscount);
        return result;
    }

    /**
     * Returns a pricing group lookup, creating and saving it if it doesn't exist.
     *
     * @param code the lookup code
     * @return the corresponding lookup
     */
    public static Lookup getPricingGroup(String code) {
        return TestHelper.getLookup(ProductArchetypes.PRICING_GROUP, code);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param pricingGroup the pricing group
     * @return a new unit price
     */
    public static ProductPrice createUnitPrice(BigDecimal price, BigDecimal cost, BigDecimal markup,
                                               BigDecimal maxDiscount, Date from, Date to, Lookup pricingGroup) {
        ProductPrice result = createUnitPrice(price, cost, markup, maxDiscount, from, to);
        result.addClassification(pricingGroup);
        return result;
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @param pricingGroup the pricing group
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(String from, String to, boolean defaultPrice, Lookup pricingGroup) {
        ProductPrice fixedPrice = createFixedPrice(from, to, defaultPrice);
        fixedPrice.addClassification(pricingGroup);
        return fixedPrice;
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(String from, String to, boolean defaultPrice) {
        Date fromDate = (from != null) ? getDate(from) : null;
        Date toDate = (to != null) ? getDate(to) : null;
        return createFixedPrice(fromDate, toDate, defaultPrice);
    }

    /**
     * Helper to create a new price.
     *
     * @param shortName the short name
     * @param from      the active from date. May be {@code null}
     * @param to        the active to date. May be {@code null}
     * @return a new price
     */
    public static ProductPrice createPrice(String shortName, Date from, Date to) {
        ProductPrice result = (ProductPrice) TestHelper.create(shortName);
        result.setName("XPrice");
        result.setPrice(BigDecimal.ONE);
        result.setFromDate(from);
        result.setToDate(to);
        return result;
    }

    /**
     * Helper to create a new price.
     *
     * @param shortName the short name
     * @param from      the active from date. May be {@code null}
     * @param to        the active to date. May be {@code null}
     * @return a new price
     */
    public static ProductPrice createPrice(String shortName, String from, String to) {
        Date fromDate = (from != null) ? getDate(from) : null;
        Date toDate = (to != null) ? getDate(to) : null;
        return createPrice(shortName, fromDate, toDate);
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(String price, String cost, String markup, String maxDiscount,
                                                String from, String to, boolean defaultPrice) {
        return createFixedPrice(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(markup),
                                new BigDecimal(maxDiscount), from, to, defaultPrice);
    }

    /**
     * Helper to create a new fixed price.
     *
     * @param price        the price
     * @param cost         the cost price
     * @param markup       the markup
     * @param maxDiscount  the maximum discount
     * @param from         the active from date. May be {@code null}
     * @param to           the active to date. May be {@code null}
     * @param defaultPrice {@code true} if the price is the default
     * @return a new fixed price
     */
    public static ProductPrice createFixedPrice(String price, String cost, String markup, String maxDiscount,
                                                Date from, Date to, boolean defaultPrice) {
        return createFixedPrice(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(markup),
                                new BigDecimal(maxDiscount), from, to, defaultPrice);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new unit price
     */
    public static ProductPrice createUnitPrice(String price, String cost, String markup, String maxDiscount,
                                               String from, String to) {
        return createUnitPrice(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(markup),
                               new BigDecimal(maxDiscount), from, to);
    }

    /**
     * Helper to create a new unit price.
     *
     * @param price       the price
     * @param cost        the cost price
     * @param markup      the markup
     * @param maxDiscount the maximum discount
     * @param from        the active from date. May be {@code null}
     * @param to          the active to date. May be {@code null}
     * @return a new unit price
     */
    public static ProductPrice createUnitPrice(String price, String cost, String markup, String maxDiscount,
                                               Date from, Date to) {
        return createUnitPrice(new BigDecimal(price), new BigDecimal(cost), new BigDecimal(markup),
                               new BigDecimal(maxDiscount), from, to);
    }

    /**
     * Helper to return prices of the specified archetype from a product, in from-date order.
     * <p/>
     * Where multiple prices have the same from-date, they will be ordered on id.
     *
     * @param product   the product
     * @param archetype the price archetype
     * @return the prices
     */
    public static List<ProductPrice> getPrices(Product product, String archetype) {
        return sort(product.getProductPrices().stream().filter(Predicates.isA(archetype)).collect(Collectors.toList()));
    }

    /**
     * Sorts prices in from-date order.
     * <p/>
     * Where multiple prices have the same from-date, they will be ordered on id.
     *
     * @param prices the prices to sort
     * @return the sorted prices
     */
    public static List<ProductPrice> sort(Collection<ProductPrice> prices) {
        List<ProductPrice> result = new ArrayList<>(prices);
        result.sort((o1, o2) -> {
            int compare = DateRules.compareTo(o1.getFromDate(), o2.getFromDate());
            if (compare == 0) {
                compare = Long.compare(o1.getId(), o2.getId());
            }
            return compare;
        });
        return result;
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param productPrice the price to check
     * @param price        the expected price
     * @param cost         the expected cost
     */
    public static void checkPrice(ProductPrice productPrice, BigDecimal price, BigDecimal cost) {
        IMObjectBean bean = new IMObjectBean(productPrice);
        checkEquals(price, bean.getBigDecimal("price"));
        checkEquals(cost, bean.getBigDecimal("cost"));
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param productPrice the price to check
     * @param price        the expected price
     * @param cost         the expected cost
     * @param markup       the expected markup
     * @param maxDiscount  the expected max discount
     */
    public static void checkPrice(ProductPrice productPrice, BigDecimal price, BigDecimal cost, BigDecimal markup,
                                  BigDecimal maxDiscount) {
        checkPrice(productPrice, price, cost, markup, maxDiscount, null);
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param productPrice the price to check
     * @param price        the expected price
     * @param cost         the expected cost
     * @param markup       the expected markup
     * @param maxDiscount  the expected max discount
     * @param pricingGroup the expected pricing group. May be {@code null}
     */
    public static void checkPrice(ProductPrice productPrice, BigDecimal price, BigDecimal cost, BigDecimal markup,
                                  BigDecimal maxDiscount, Lookup pricingGroup) {
        checkPrice(productPrice, price, cost);
        IMObjectBean bean = new IMObjectBean(productPrice);
        checkEquals(markup, bean.getBigDecimal("markup"));
        checkEquals(maxDiscount, bean.getBigDecimal("maxDiscount"));
        List<Lookup> groups = bean.getValues("pricingGroups", Lookup.class);
        if (pricingGroup != null) {
            assertEquals(1, groups.size());
            assertTrue(groups.contains(pricingGroup));
        } else {
            assertTrue(groups.isEmpty());
        }
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param productPrice the price to check
     * @param price        the expected price
     * @param cost         the expected cost
     * @param markup       the expected markup
     * @param maxDiscount  the expected max discount
     * @param from         the expected from date
     * @param to           the expected to date
     */
    public static void checkPrice(ProductPrice productPrice, BigDecimal price, BigDecimal cost, BigDecimal markup,
                                  BigDecimal maxDiscount, Date from, Date to) {
        checkPrice(productPrice, price, cost, markup, maxDiscount, from, to, null);
    }

    /**
     * Verifies a price matches that expected.
     *
     * @param productPrice the price to check
     * @param price        the expected price
     * @param cost         the expected cost
     * @param markup       the expected markup
     * @param maxDiscount  the expected max discount
     * @param from         the expected from date
     * @param to           the expected to date
     * @param pricingGroup the expected pricing group. May be {@code null}
     */
    public static void checkPrice(ProductPrice productPrice, BigDecimal price, BigDecimal cost, BigDecimal markup,
                                  BigDecimal maxDiscount, Date from, Date to, Lookup pricingGroup) {
        checkPrice(productPrice, price, cost, markup, maxDiscount, pricingGroup);
        assertEquals(0, DateRules.compareTo(from, productPrice.getFromDate()));
        assertEquals(0, DateRules.compareTo(to, productPrice.getToDate()));
    }

    /**
     * Verifies that there is a single matching price in a collection.
     *
     * @param prices the prices to check
     * @param price  the expected price
     * @param cost   the expected cost price
     * @param from   the expected from date
     * @param to     the expected to date
     * @return the matching price
     */
    public static ProductPrice checkPrice(Collection<ProductPrice> prices, BigDecimal price, BigDecimal cost,
                                          Date from, Date to) {
        return checkPrice(prices, price, cost, MathRules.ONE_HUNDRED, MathRules.ONE_HUNDRED, from, to, null);
    }

    /**
     * Verifies that there is a single matching price in a collection.
     *
     * @param prices       the prices to check
     * @param price        the expected price
     * @param cost         the expected cost price
     * @param markup       the expected markup
     * @param maxDiscount  the expected max discount
     * @param from         the expected from date
     * @param to           the expected to date
     * @param pricingGroup the expected pricing group. May be {@code null}
     * @return the matching price
     */
    public static ProductPrice checkPrice(Collection<ProductPrice> prices, BigDecimal price, BigDecimal cost,
                                          BigDecimal markup, BigDecimal maxDiscount, Date from, Date to,
                                          Lookup pricingGroup) {
        ProductPrice match = null;
        int matches = 0;
        for (ProductPrice p : prices) {
            IMObjectBean bean = new IMObjectBean(p);
            if (MathRules.equals(bean.getBigDecimal("cost"), cost) && MathRules.equals(price, p.getPrice())
                && DateRules.compareTo(from, p.getFromDate(), true) == 0
                && DateRules.compareTo(to, p.getToDate(), true) == 0
                && Objects.equals(pricingGroup, bean.getObject("pricingGroups"))) {
                match = p;
                matches++;
                checkEquals(markup, bean.getBigDecimal("markup"));
                checkEquals(maxDiscount, bean.getBigDecimal("maxDiscount"));
            }
        }
        assertNotNull(match);
        assertEquals(1, matches);
        return match;
    }

    /**
     * Verify a price has a to-date set.
     *
     * @param price the price to check
     * @param after the to date should be on or after the specified time
     * @return the to-date
     */
    public static Date checkToDateSet(ProductPrice price, Date after) {
        Date toDate = price.getToDate();
        assertNotNull(toDate);
        assertTrue(DateRules.compareTo(toDate, after, true) >= 0);
        return toDate;
    }
}
