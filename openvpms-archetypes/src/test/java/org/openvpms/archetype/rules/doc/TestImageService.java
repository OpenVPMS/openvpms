/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.apache.commons.io.FileUtils;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.beans.factory.DisposableBean;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Test implementation of {@link ImageService}.
 *
 * @author Tim Anderson
 */
public class TestImageService extends AbstractImageService implements DisposableBean {

    /**
     * The cache directory.
     */
    private final File dir;

    /**
     * If {@code true} delete on destroy.
     */
    private final boolean delete;


    /**
     * Constructs a {@link TestImageService}.
     * <p/>
     * A temporary directory will be created and removed when the service is destroyed.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     * @throws IOException if the directory could not be created
     */
    public TestImageService(DocumentHandlers handlers, ArchetypeService service) throws IOException {
        this(null, handlers, service);
    }

    /**
     * Constructs a {@link TestImageService}.
     *
     * @param dir      the directory to cache images. If {@code null}, one will be created
     * @param handlers the document handlers
     * @param service  the archetype service
     * @throws IOException if the directory wasn't specified and a temporary one could not be created
     */
    public TestImageService(File dir, DocumentHandlers handlers, ArchetypeService service) throws IOException {
        super(handlers, service);
        if (dir == null) {
            dir = Files.createTempDirectory("TestImageCache").toFile();
            delete = true;
        } else {
            delete = false;
        }
        this.dir = dir;
    }

    /**
     * Destroys the service.
     *
     * @throws Exception if the directory was created but cannot be deleted
     */
    @Override
    public void destroy() throws Exception {
        if (delete) {
            FileUtils.deleteDirectory(dir);
        }
    }

    /**
     * Returns the directory to store images.
     *
     * @return the directory
     */
    @Override
    protected File getDir() {
        return dir;
    }
}