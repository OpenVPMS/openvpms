/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.invoice;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderStatus;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemBuilder;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestInvestigationBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.COMPLETED;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;


/**
 * Tests the {@link InvoiceRules} class when invoked by the
 * <em>archetypeService.save.act.customerAccountInvoiceItem.after.drl</em>,
 * <em>archetypeService.remove.act.customerAccountChargesInvoice.before.drl</em> and
 * <em>archetypeService.remove.act.customerAccountInvoiceItem.before.drl</em> '
 * rules. In order for these tests to be successful, the archetype service
 * must be configured to trigger the above rules.
 *
 * @author Tim Anderson
 */
public class InvoiceRulesTestCase extends ArchetypeServiceTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        customer = customerFactory.createCustomer();
        clinician = userFactory.createClinician();
        patient = patientFactory.createPatient();
    }

    /**
     * Verifies that when an invoice is removed, the following occurs for acts associated with the invoice item:
     * <ul>
     *     <li>the visit is retained</li>
     *     <li>the medication is removed, regardless of status</li>
     *     <li>investigations with IN_PROGRESS status are removed</li>
     *     <li>investigations with CANCELLED and POSTED status are retained</li>
     *     <li>documents with IN_PROGRESS status are removed</li>
     *     <li>documents with COMPLETED and POSTED status are retained</li>
     *     <li>reminders with IN_PROGRESS and CANCELLED status are removed</li>
     *     <li>reminders with COMPLETED status are retained</li>
     *     <li>alerts with IN_PROGRESS status are removed</li>
     *     <li>alerts with COMPLETED status are retained</li>
     *     <li>tasks with PENDING, IN_PROGRESS or CANCELLED status are removed</li>
     *     <li>tasks with BILLED or COMPLETED status are retained</li>
     * </ul>
     */
    @Test
    public void testRemoveInvoice() {
        checkRemove(true, true);
        checkRemove(false, true);
    }

    /**
     * Verifies that when an invoice item is removed, the following occurs for acts associated with it:
     * <ul>
     *     <li>the visit is retained</li>
     *     <li>the medication is removed, regardless of status</li>
     *     <li>investigations with IN_PROGRESS status are removed</li>
     *     <li>investigations with CANCELLED and POSTED status are retained</li>
     *     <li>documents with IN_PROGRESS status are removed</li>
     *     <li>documents with COMPLETED and POSTED status are retained</li>
     *     <li>reminders with IN_PROGRESS and CANCELLED status are removed</li>
     *     <li>reminders with COMPLETED status are retained</li>
     *     <li>alerts with IN_PROGRESS status are removed</li>
     *     <li>alerts with COMPLETED status are retained</li>
     *     <li>tasks with PENDING, IN_PROGRESS or CANCELLED status are removed</li>
     *     <li>tasks with BILLED or COMPLETED status are retained</li>
     * </ul>
     */
    @Test
    public void testRemoveInvoiceItem() {
        checkRemove(true, false);
        checkRemove(false, false);
    }

    /**
     * Verifies that investigations with results are not removed, regardless of status.
     */
    @Test
    public void testRemoveInvoiceItemHavingInvestigationsWithResults() {
        Product product = productFactory.createMedication();
        TestInvoiceItemBuilder itemBuilder = newInvoiceItem(product);
        DocumentAct investigation1 = createInvestigation(InvestigationActStatus.IN_PROGRESS);
        DocumentAct investigation2 = createInvestigationWithReport(InvestigationActStatus.IN_PROGRESS);
        DocumentAct investigation3 = createInvestigationWithReport(InvestigationActStatus.CANCELLED);
        DocumentAct investigation4 = createInvestigationWithReport(InvestigationActStatus.POSTED);
        itemBuilder.addInvestigations(investigation1, investigation2, investigation3, investigation4);

        // remove the invoice and verify it can't be retrieved
        TestInvoiceBuilder invoiceBuilder = itemBuilder.add();
        FinancialAct invoice = invoiceBuilder.build();

        remove(invoice);
        assertNull(get(invoice));

        // verify only the IN_PROGRESS investigation with no document has been removed
        assertNull(get(investigation1));
        assertNotNull(get(investigation2));
        assertNotNull(get(investigation3));
        assertNotNull(get(investigation4));
    }

    /**
     * Verifies that demographic updates associated with a product are processed
     * when an invoice is posted.
     */
    @Test
    public void testDemographicUpdates() {
        FinancialAct invoice = createInvoice(createDesexingProduct());

        IMObjectBean bean = getBean(patient);
        assertFalse(bean.getBoolean("desexed"));

        invoice.setStatus(ActStatus.POSTED);
        save(invoice);

        bean = get(bean);
        assertTrue(bean.getBoolean("desexed"));
    }

    /**
     * Verifies that when an invoice or invoice item is removed, the following occurs for acts associated with the
     * invoice item:
     * <ul>
     *     <li>the visit is retained</li>
     *     <li>the medication is removed, regardless of status</li>
     *     <li>investigations with IN_PROGRESS status are removed</li>
     *     <li>investigations with CANCELLED and POSTED status are retained</li>
     *     <li>documents with IN_PROGRESS status are removed</li>
     *     <li>documents with COMPLETED and POSTED status are retained</li>
     *     <li>reminders with IN_PROGRESS and CANCELLED status are removed</li>
     *     <li>reminders with COMPLETED status are retained</li>
     *     <li>alerts with IN_PROGRESS status are removed</li>
     *     <li>alerts with COMPLETED status are retained</li>
     *     <li>tasks with PENDING, IN_PROGRESS or CANCELLED status are removed</li>
     *     <li>tasks with BILLED or COMPLETED status are retained</li>
     * </ul>
     *
     * @param inProgressMedication if {@code true}, the medication should have IN_PROGRESS status, else it should
     *                             have POSTED status
     * @param removeInvoice if {@code true}, remove the invoice, else remove the invoice item
     */
    private void checkRemove(boolean inProgressMedication, boolean removeInvoice) {
        Product product = productFactory.createMedication();
        TestInvoiceItemBuilder itemBuilder = newInvoiceItem(product);

        Act medication = patientFactory.newMedication()
                .patient(patient)
                .product(product)
                .status(inProgressMedication ? IN_PROGRESS : POSTED)
                .build();
        itemBuilder.medication(medication);

        Act visit = patientFactory.newVisit()
                .patient(patient)
                .status(IN_PROGRESS)
                .build();
        itemBuilder.visit(visit);

        // add investigation acts
        DocumentAct investigation1 = createInvestigation(InvestigationActStatus.IN_PROGRESS);
        DocumentAct investigation2 = createInvestigation(InvestigationActStatus.CANCELLED);
        DocumentAct investigation3 = createInvestigation(InvestigationActStatus.POSTED);
        itemBuilder.addInvestigations(investigation1, investigation2, investigation3);

        // add documents
        DocumentAct document1 = createDocument(product, IN_PROGRESS);
        DocumentAct document2 = createDocument(product, COMPLETED);
        DocumentAct document3 = createDocument(product, POSTED);
        itemBuilder.addDocuments(document1, document2, document3);

        // add reminders
        Act reminder1 = createReminder(ReminderStatus.IN_PROGRESS);
        Act reminder2 = createReminder(ReminderStatus.CANCELLED);
        Act reminder3 = createReminder(ReminderStatus.COMPLETED);
        itemBuilder.addReminders(reminder1, reminder2, reminder3);

        // add alerts
        Act alert1 = createAlert(IN_PROGRESS);
        Act alert2 = createAlert(COMPLETED);
        itemBuilder.addAlerts(alert1, alert2);

        // add tasks
        Act task1 = createTask(TaskStatus.PENDING);
        Act task2 = createTask(TaskStatus.IN_PROGRESS);
        Act task3 = createTask(TaskStatus.CANCELLED);
        Act task4 = createTask(TaskStatus.BILLED);
        Act task5 = createTask(TaskStatus.COMPLETED);
        itemBuilder.addTasks(task1, task2, task3, task4, task5);

        TestInvoiceBuilder invoiceBuilder = itemBuilder.add();
        FinancialAct invoice = invoiceBuilder.build();
        FinancialAct item = invoiceBuilder.getItems().get(0);

        item = get(item); // reload to ensure the item has saved correctly
        IMObjectBean bean = getBean(item);

        assertEquals(visit, bean.getSource("event"));
        assertEquals(medication, bean.getTarget("dispensing"));

        List<DocumentAct> investigations = bean.getTargets("investigations", DocumentAct.class);
        assertEquals(3, investigations.size());

        List<Act> documents = bean.getTargets("documents", Act.class);
        assertEquals(3, documents.size());

        List<Act> reminders = bean.getTargets("reminders", Act.class);
        assertEquals(3, reminders.size());

        List<Act> alerts = bean.getTargets("alerts", Act.class);
        assertEquals(2, alerts.size());

        if (removeInvoice) {
            remove(invoice);
            assertNull(get(invoice));
        } else {
            // remove the item
            remove(bean.getObject());
        }

        // verify the item can no longer be retrieved
        assertNull(get(item));

        assertNotNull(get(visit));         // visit should always be retained

        assertNull(get(medication));       // medication is removed, even if POSTED

        // verify investigations
        assertNull(get(investigation1));    // IN_PROGRESS
        assertNotNull(get(investigation2)); // CANCELLED
        assertNotNull(get(investigation3)); // POSTED

        // verify documents
        assertNull(get(document1));     // IN_PROGRESS
        assertNotNull(get(document2));  // COMPLETED
        assertNotNull(get(document3));  // POSTED

        // verify reminders
        assertNull(get(reminder1));     // IN_PROGRESS
        assertNull(get(reminder2));     // CANCELLED
        assertNotNull(get(reminder3));  // COMPLETED

        // verify alerts
        assertNull(get(alert1));        // IN_PROGRESS
        assertNotNull(get(alert2));     // COMPLETED

        // verify tasks
        assertNull(get(task1));    // PENDING
        assertNull(get(task2));    // IN_PROGRESS
        assertNull(get(task3));    // CANCELLED
        assertNotNull(get(task4)); // BILLED
        assertNotNull(get(task5)); // COMPLETED
    }

    /**
     * Creates a patient document.
     *
     * @param product the product
     * @param status  the act status
     * @return a new patient document
     */
    private DocumentAct createDocument(Product product, String status) {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        return patientFactory.newForm()
                .patient(patient)
                .template(template)
                .product(product)
                .clinician(clinician)
                .status(status)
                .build();
    }

    /**
     * Helper to create an <em>act.customerAccountChargesInvoice</em> with IN_PROGRESS status.
     *
     * @param product the product to invoice
     * @return an invoice, with a single item populated
     */
    private FinancialAct createInvoice(Product product) {
        return newInvoiceItem(product).add().build();
    }

    /**
     * Returns a populated invoice item builder.
     *
     * @param product the product to invoice
     * @return a new invoice item builder
     */
    private TestInvoiceItemBuilder newInvoiceItem(Product product) {
        return accountFactory.newInvoice()
                .customer(customer)
                .status(IN_PROGRESS)
                .item()
                .patient(patient)
                .clinician(clinician)
                .product(product);
    }

    /**
     * Helper to create a reminder.
     *
     * @param status the reminder status
     * @return a new reminder
     */
    private Act createReminder(String status) {
        Entity reminderType = reminderFactory.createReminderType();
        return reminderFactory.newReminder()
                .patient(patient)
                .reminderType(reminderType)
                .status(status)
                .build();
    }

    /**
     * Helper to create an alert.
     *
     * @param status the alert status
     * @return a new alert
     */
    private Act createAlert(String status) {
        Entity alertType = patientFactory.createAlertType();
        return patientFactory.newAlert()
                .patient(patient)
                .alertType(alertType)
                .status(status)
                .build();
    }

    /**
     * Helper to create a task.
     *
     * @param status the task status
     * @return a new task
     */
    private Act createTask(String status) {
        return schedulingFactory.newTask()
                .startTime(new Date())
                .taskType(schedulingFactory.createTaskType())
                .workList(schedulingFactory.createWorkList())
                .customer(customer)
                .patient(patient)
                .status(status)
                .build();
    }

    /**
     * Creates an investigation with the specified status.
     *
     * @param status the investigation status
     * @return a new investigation
     */
    private DocumentAct createInvestigation(String status) {
        return newInvestigation(status).build();
    }

    /**
     * Creates an investigation with the specified status and a report attached.
     *
     * @param status the investigation status
     * @return a new investigation
     */
    private DocumentAct createInvestigationWithReport(String status) {
        return newInvestigation(status)
                .report(documentFactory.createText())
                .build();
    }

    /**
     * Returns a populated investigation builder.
     *
     * @param status the investigation status
     * @return an investigation builder
     */
    private TestInvestigationBuilder newInvestigation(String status) {
        Entity investigationType = laboratoryFactory.createInvestigationType();
        return patientFactory.newInvestigation()
                .patient(patient)
                .clinician(clinician)
                .investigationType(investigationType)
                .status(status);
    }

    /**
     * Creates and saves a new desexing product, with associated
     * <em>lookup.demographicUpdate</em> to perform the desexing update.
     *
     * @return a new desexing product
     */
    private Product createDesexingProduct() {
        Lookup lookup = productFactory.createDemographicUpdate("Desexing", "patient.entity",
                                                               "party:setPatientDesexed(.)");
        return productFactory.newService()
                .addDemographicUpdate(lookup)
                .build();
    }
}
