/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.user;

/**
 * Test implementation of {@link PasswordPolicy}.
 *
 * @author Tim Anderson
 */
public class TestPasswordPolicy extends AbstractPasswordPolicy {

    /**
     * Minimum password length.
     */
    private final int minLength;

    /**
     * Maximum password length.
     */
    private final int maxLength;

    /**
     * Determines if a lowercase character is required.
     */
    private final boolean lowercase;

    /**
     * Determines if an uppercase character is required.
     */
    private final boolean uppercase;

    /**
     * Determines if a number is required.
     */
    private final boolean number;

    /**
     * Determines if a special character is required.
     */
    private final boolean special;

    /**
     * Constructs a {@link TestPasswordPolicy}.
     *
     * @param minLength the minimum length
     * @param maxLength the maximum length
     * @param lowercase if {@code true}, require a lowercase character
     * @param uppercase if {@code true}, require an uppercase character
     * @param number    if {@code true}, require a number
     * @param special   if {@code true}, require a special character
     */
    public TestPasswordPolicy(int minLength, int maxLength, boolean lowercase, boolean uppercase, boolean number,
                              boolean special) {
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.lowercase = lowercase;
        this.uppercase = uppercase;
        this.number = number;
        this.special = special;
    }

    /**
     * Returns the minimum password length.
     *
     * @return the minimum password length
     */
    @Override
    public int getMinLength() {
        return minLength;
    }

    /**
     * Returns the maximum password length.
     *
     * @return the maximum password length
     */
    @Override
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Determines if at least one lowercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean lowercaseRequired() {
        return lowercase;
    }

    /**
     * Determines if at least one uppercase character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean uppercaseRequired() {
        return uppercase;
    }

    /**
     * Determines if at least one number is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean numberRequired() {
        return number;
    }

    /**
     * Determines if at least one special character is required.
     *
     * @return {@code true} if one is required, otherwise {@code false}
     */
    @Override
    public boolean specialRequired() {
        return special;
    }
}
