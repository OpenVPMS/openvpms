/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.component.model.bean.Predicates.targetEquals;


/**
 * Tests the {@link DocumentTemplate} class.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests the default values of a new document template.
     */
    @Test
    public void testDefaults() {
        Lookup reportType = TestHelper.getLookup("lookup.reportType", "OTHER");
        reportType.setDefaultLookup(true);
        save(reportType);

        Entity entity = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());

        assertNull(template.getName());
        assertNull(template.getDescription());
        assertTrue(template.isActive());
        assertNull(template.getType());
        assertEquals("0", template.getUserLevel());
        assertEquals(reportType.getCode(), template.getReportType());
        assertEquals(DocumentTemplate.PrintMode.CHECK_OUT, template.getPrintMode());
        assertNull(template.getPaperSize());
        assertEquals(DocumentTemplate.PORTRAIT, template.getOrientation());
        assertEquals(1, template.getCopies());
        checkEquals(BigDecimal.ZERO, template.getPaperHeight());
        checkEquals(BigDecimal.ZERO, template.getPaperWidth());
        assertEquals(DocumentTemplate.MM, template.getPaperUnits());
        assertNull(template.getSMSTemplate());
        assertNull(template.getMediaSize());
        assertEquals(OrientationRequested.PORTRAIT, template.getOrientationRequested());
        assertTrue(template.getPrinters().isEmpty());
        assertNull(template.getFileNameExpression());
    }

    /**
     * Tests the {@link DocumentTemplate} getters.
     */
    @Test
    public void testAccessors() {
        BigDecimal height = new BigDecimal("10.00");
        BigDecimal width = new BigDecimal("5.00");

        TestDocumentTemplateBuilder builder = documentFactory.newTemplate();
        Entity entity = builder.name("test name")
                .description("test description")
                .active(false)
                .type("REPORT")
                .userLevel("1")
                .reportType("XX_REPORT_TYPE")
                .printMode(DocumentTemplate.PrintMode.IMMEDIATE)
                .paperSize(DocumentTemplate.A5)
                .orientation(DocumentTemplate.LANDSCAPE)
                .copies(5)
                .paperHeight(height)
                .paperWidth(width)
                .paperUnits(DocumentTemplate.MM)
                .build();
        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());
        assertEquals("test name", template.getName());
        assertEquals("test description", template.getDescription());
        assertFalse(template.isActive());
        assertEquals("REPORT", template.getType());
        assertEquals("1", template.getUserLevel());
        assertEquals("XX_REPORT_TYPE", template.getReportType());
        assertEquals(DocumentTemplate.PrintMode.IMMEDIATE, template.getPrintMode());
        assertEquals(DocumentTemplate.A5, template.getPaperSize());
        assertEquals(DocumentTemplate.LANDSCAPE, template.getOrientation());
        assertEquals(5, template.getCopies());
        checkEquals(height, template.getPaperHeight());
        checkEquals(width, template.getPaperWidth());
        assertEquals(DocumentTemplate.MM, template.getPaperUnits());
        assertEquals(MediaSizeName.ISO_A5, template.getMediaSize());
        assertEquals(OrientationRequested.LANDSCAPE, template.getOrientationRequested());
    }

    /**
     * Tests the various {@link DocumentTemplate} printer methods.
     */
    @Test
    public void testGetPrinter() {
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate();

        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();
        Party practice = TestHelper.getPractice();

        Entity entity = builder.printer()
                .location(location1)
                .add()
                .printer()
                .location(practice)
                .add()
                .build(false);

        IMObjectBean bean = getBean(entity);
        EntityRelationship printer1 = bean.getValue("printers",
                                                    EntityRelationship.class,
                                                    targetEquals(location1));
        EntityRelationship printer2 = bean.getValue("printers",
                                                    EntityRelationship.class,
                                                    targetEquals(practice));

        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());

        DocumentTemplatePrinter location1Printer = new DocumentTemplatePrinter(printer1, getArchetypeService());
        DocumentTemplatePrinter practicePrinter = new DocumentTemplatePrinter(printer2, getArchetypeService());

        assertEquals(location1Printer, template.getPrinter(location1));
        assertEquals(practicePrinter, template.getPrinter(practice));
        assertNull(template.getPrinter(location2));

        assertEquals(2, template.getPrinters().size());
        assertTrue(template.getPrinters().contains(location1Printer));
        assertTrue(template.getPrinters().contains(practicePrinter));
    }

    /**
     * Tests the {@link DocumentTemplate#getFileNameExpression()} method.
     */
    @Test
    public void testGetFileNameExpression() {
        Entity entity = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        Lookup lookup = create(DocumentArchetypes.FILE_NAME_FORMAT, Lookup.class);

        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());
        assertNull(template.getFileNameExpression());

        IMObjectBean bean = getBean(lookup);
        String expression = "concat($file, ' - ', date:format(java.util.Date.new(), 'd MMM yyyy'))";
        bean.setValue("expression", expression);
        entity.addClassification(lookup);

        assertEquals(expression, template.getFileNameExpression());
    }

    /**
     * Tests the {@link DocumentTemplate#isForm(String)} method.
     */
    @Test
    public void testIsForm() {
        assertTrue(DocumentTemplate.isForm(CustomerArchetypes.DOCUMENT_FORM));
        assertTrue(DocumentTemplate.isForm(PatientArchetypes.DOCUMENT_FORM));
        assertTrue(DocumentTemplate.isForm(SupplierArchetypes.DOCUMENT_FORM));

        assertFalse(DocumentTemplate.isForm(CustomerArchetypes.DOCUMENT_LETTER));
        assertFalse(DocumentTemplate.isForm(PatientArchetypes.DOCUMENT_LETTER));
        assertFalse(DocumentTemplate.isForm(SupplierArchetypes.DOCUMENT_LETTER));
    }

    /**
     * Tests the {@link DocumentTemplate#isLetter(String)} method.
     */
    @Test
    public void testIsLetter() {
        assertTrue(DocumentTemplate.isLetter(CustomerArchetypes.DOCUMENT_LETTER));
        assertTrue(DocumentTemplate.isLetter(PatientArchetypes.DOCUMENT_LETTER));
        assertTrue(DocumentTemplate.isLetter(SupplierArchetypes.DOCUMENT_LETTER));

        assertFalse(DocumentTemplate.isLetter(CustomerArchetypes.DOCUMENT_FORM));
        assertFalse(DocumentTemplate.isLetter(PatientArchetypes.DOCUMENT_FORM));
        assertFalse(DocumentTemplate.isLetter(SupplierArchetypes.DOCUMENT_FORM));
    }

    /**
     * Tests the {@link DocumentTemplate#isReport(String)} method.
     */
    @Test
    public void testIsReport() {
        assertTrue(DocumentTemplate.isReport(DocumentTemplate.REPORT));
        assertTrue(DocumentTemplate.isReport(DocumentTemplate.SUBREPORT));

        assertFalse(DocumentTemplate.isReport(PatientArchetypes.DOCUMENT_FORM));
    }
}
