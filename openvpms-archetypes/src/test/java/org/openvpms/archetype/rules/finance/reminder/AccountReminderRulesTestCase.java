/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.reminder;

import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.insurance.ClaimStatus;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link AccountReminderRules} class.
 *
 * @author Tim Anderson
 */
public class AccountReminderRulesTestCase extends ArchetypeServiceTest {

    /**
     * The account reminder rules.
     */
    @Autowired
    private AccountReminderRules rules;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    /**
     * Tests the {@link AccountReminderRules#needsReminder(FinancialAct, BigDecimal)} method.
     */
    @Test
    public void testNeedsReminder() {
        checkNeedsReminder(true);  // check for invoice
        checkNeedsReminder(false); // check for counter sale
    }

    /**
     * Verifies that if an invoice is in a gap claim that isn't CANCELLED,
     * {@link AccountReminderRules#needsReminder(FinancialAct, BigDecimal)} returns {@code false}
     */
    @Test
    public void testNeedsReminderForInvoiceInGapClaim() {
        Party customer = customerFactory.createCustomer();
        FinancialAct invoice = createInvoice(customer, 100, true, FinancialActStatus.POSTED);
        IMObjectBean bean = getBean(invoice);
        FinancialAct item = bean.getTarget("items", FinancialAct.class);
        assertNotNull(item);

        Act policy = insuranceFactory.createPolicy(customer, patientFactory.createPatient(customer),
                                                   insuranceFactory.createInsurer(), "ABCDEFG");

        User clinician = userFactory.createClinician();
        FinancialAct claim = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(clinician)
                .claimHandler(clinician)
                .gapClaim(true)
                .item()
                .diagnosis("VENOM_328", "Abcess", "328")
                .invoiceItems(item)
                .add()
                .build();

        assertFalse(rules.needsReminder(invoice, BigDecimal.ZERO));

        claim.setStatus(ClaimStatus.CANCELLED);
        save(claim);
        assertTrue(rules.needsReminder(invoice, BigDecimal.ZERO));
    }

    /**
     * Tests the {@link AccountReminderRules#resolveError(Act)} method.
     * <p/>
     * For reminders with ERROR status, this marks them PENDING and clears the error message.
     */
    @Test
    public void testResolveError() {
        Act reminder1 = createReminder(ReminderItemStatus.PENDING, null);
        Act reminder2 = createReminder(ReminderItemStatus.COMPLETED, null);
        Act reminder3 = createReminder(ReminderItemStatus.CANCELLED, null);
        Act reminder4 = createReminder(ReminderItemStatus.ERROR, "foo");

        assertFalse(rules.resolveError(reminder1));
        assertFalse(rules.resolveError(reminder2));
        assertFalse(rules.resolveError(reminder3));
        assertTrue(rules.resolveError(reminder4));

        IMObjectBean bean = getBean(get(reminder4));
        assertEquals(ReminderItemStatus.PENDING, bean.getString("status"));
        assertNull(bean.getString("error"));
    }

    /**
     * Tests the {@link AccountReminderRules#canEnableReminders(FinancialAct)} and
     * {@link AccountReminderRules#enableReminders(FinancialAct)} methods.
     */
    @Test
    public void testEnableReminders() {
        FinancialAct invoice1 = createInvoice(false);
        FinancialAct invoice2 = createInvoice(true);

        assertTrue(rules.canEnableReminders(invoice1));
        assertTrue(rules.enableReminders(invoice1));
        assertFalse(rules.enableReminders(invoice1));

        assertFalse(rules.enableReminders(invoice2));
        assertFalse(rules.canEnableReminders(invoice2));

        // now test with an invalid act
        FinancialAct openingBalance = create(CustomerAccountArchetypes.OPENING_BALANCE, FinancialAct.class);
        assertFalse(rules.canEnableReminders(openingBalance));
        assertFalse(rules.enableReminders(openingBalance));
    }

    /**
     * Tests the {@link AccountReminderRules#canEnableReminders(FinancialAct)} and
     * {@link AccountReminderRules#disableReminders(FinancialAct)} methods.
     * <p/>
     * If the charge has a PENDING or ERROR reminder, these are removed.
     */
    @Test
    public void testDisableReminders() {
        FinancialAct invoice1 = createInvoice(true);
        Act reminder1 = addReminder(invoice1, ReminderItemStatus.PENDING);

        FinancialAct invoice2 = createInvoice(true);
        Act reminder2 = addReminder(invoice2, ReminderItemStatus.ERROR);

        FinancialAct invoice3 = createInvoice(true);
        Act reminder3 = addReminder(invoice3, ReminderItemStatus.CANCELLED);

        FinancialAct invoice4 = createInvoice(true);
        Act reminder4 = addReminder(invoice4, ReminderItemStatus.COMPLETED);

        FinancialAct invoice5 = createInvoice(false);

        assertTrue(rules.canDisableReminders(invoice1));
        assertTrue(rules.disableReminders(invoice1));
        assertNull(get(reminder1)); // deleted as it is PENDING

        assertTrue(rules.canDisableReminders(invoice2));
        assertTrue(rules.disableReminders(invoice2));
        assertNull(get(reminder2)); // deleted as it is ERROR

        assertTrue(rules.canDisableReminders(invoice3));
        assertTrue(rules.disableReminders(invoice3));
        assertNotNull(get(reminder3)); // retained as it was CANCELLED

        assertTrue(rules.canDisableReminders(invoice4));
        assertTrue(rules.disableReminders(invoice4));
        assertNotNull(get(reminder4)); // retained as it was COMPLETED

        // verify reminders cannot be disabled if they already have been
        assertFalse(rules.canDisableReminders(invoice5));
        assertFalse(rules.disableReminders(invoice5));

        // now test with an invalid act
        FinancialAct openingBalance = create(CustomerAccountArchetypes.OPENING_BALANCE, FinancialAct.class);
        assertFalse(rules.canDisableReminders(openingBalance));
        assertFalse(rules.disableReminders(openingBalance));
    }

    /**
     * Adds a reminder to a charge.
     *
     * @param charge the charge
     * @param status the reminder status
     * @return the reminder
     */
    private Act addReminder(FinancialAct charge, String status) {
        IMObjectBean bean = getBean(charge);
        Act reminder = createReminder(status, null);
        bean.addTarget("reminders", reminder, "charge");
        save(charge, reminder);
        return reminder;
    }

    private Act createReminder(String status, String error) {
        Act reminder = create(AccountReminderArchetypes.CHARGE_REMINDER_SMS, Act.class);
        reminder.setActivityStartTime(new Date());
        reminder.setStatus(status);
        if (error != null) {
            IMObjectBean bean = getBean(reminder);
            bean.setValue("error", error);
        }
        save(reminder);
        return reminder;
    }

    /**
     * Tests the {@link AccountReminderRules#needsReminder(FinancialAct, BigDecimal)} method.
     *
     * @param invoice if {@code true}, test against an invoice, else test against a counter sale
     */
    private void checkNeedsReminder(boolean invoice) {
        Party customer = customerFactory.createCustomer();
        String[] statuses = {FinancialActStatus.IN_PROGRESS, FinancialActStatus.ON_HOLD, FinancialActStatus.COMPLETED,
                             FinancialActStatus.POSTED};

        // verify that if the sendReminder flag is false, needsReminder returns false.
        for (String status : statuses) {
            FinancialAct charge = createCharge(customer, 100, false, status, invoice);
            assertFalse(rules.needsReminder(charge, BigDecimal.ZERO));
        }

        // verify that if the sendReminder flag is true, needsReminder only returns true for POSTED acts
        for (String status : statuses) {
            FinancialAct charge = createCharge(customer, 50, true, status, invoice);
            boolean reminder = FinancialActStatus.POSTED.equals(status);
            assertEquals(reminder, rules.needsReminder(charge, BigDecimal.ZERO));
        }

        // verify no reminder is required if the balance is less than the minimum
        FinancialAct charge1 = createCharge(customer, 10, true, FinancialActStatus.POSTED, invoice);
        assertTrue(rules.needsReminder(charge1, BigDecimal.ZERO));
        assertTrue(rules.needsReminder(charge1, BigDecimal.TEN));
        assertFalse(rules.needsReminder(charge1, new BigDecimal("10.01")));

        Lookup accountType = customerFactory.newAccountType()
                .paymentTerms(30, DateUnits.DAYS)
                .build();
        customerFactory.updateCustomer(customer)
                .addClassifications(accountType)
                .build();
        assertFalse(rules.needsReminder(charge1, BigDecimal.ZERO));

        charge1.setActivityEndTime(DateRules.getDate(charge1.getActivityStartTime(), -31, DateUnits.DAYS));
        assertTrue(rules.needsReminder(charge1, BigDecimal.ZERO));
    }

    /**
     * Creates a charge.
     *
     * @param customer     the customer
     * @param amount       the amount
     * @param sendReminder if {@code true} enable reminders else disable them
     * @param status       the charge status
     * @param invoice      if {@code true} create an invoice, else create a counter sale
     * @return the charge
     */
    private FinancialAct createCharge(Party customer, int amount, boolean sendReminder, String status,
                                      boolean invoice) {
        return invoice ? createInvoice(customer, amount, sendReminder, status)
                       : createCounterSale(customer, amount, sendReminder, status);
    }

    /**
     * Creates a POSTED invoice.
     *
     * @param sendReminder determines if a reminder should be sent
     * @return a new invoice
     */
    private FinancialAct createInvoice(boolean sendReminder) {
        return createInvoice(customerFactory.createCustomer(), 100, sendReminder, FinancialActStatus.POSTED);
    }

    /**
     * Creates an invoice.
     *
     * @param customer     the customer
     * @param amount       the invoice amount
     * @param sendReminder determines if a reminder should be sent
     * @param status       the invoice status
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer, int amount, boolean sendReminder, String status) {
        return accountFactory.newInvoice()
                .customer(customer)
                .status(status)
                .endTime(FinancialActStatus.POSTED.equals(status) ? new Date() : null)
                .sendReminder(sendReminder)
                .item()
                .patient(patientFactory.createPatient(customer))
                .medicationProduct()
                .unitPrice(amount)
                .add()
                .build();
    }

    /**
     * Creates an invoice.
     *
     * @param customer     the customer
     * @param amount       the invoice amount
     * @param sendReminder determines if a reminder should be sent
     * @param status       the invoice status
     * @return a new invoice
     */
    private FinancialAct createCounterSale(Party customer, int amount, boolean sendReminder, String status) {
        return accountFactory.newCounterSale()
                .customer(customer)
                .status(status)
                .endTime(FinancialActStatus.POSTED.equals(status) ? new Date() : null)
                .sendReminder(sendReminder)
                .item()
                .medicationProduct()
                .unitPrice(amount)
                .add()
                .build();
    }
}