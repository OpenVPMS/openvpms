/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.insurance;

import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link InsuranceFunctions} class.
 *
 * @author Tim Anderson
 */
public class InsuranceFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The insurance rules.
     */
    @Autowired
    private InsuranceRules insuranceRules;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests the {@link InsuranceFunctions#claimed(FinancialAct, FinancialAct)} method.
     */
    @Test
    public void testClaimed() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        TestInvoiceBuilder invoiceBuilder = accountFactory.newInvoice();
        invoiceBuilder.customer(customer)
                .item().patient(patient).product(productFactory.createService()).unitPrice(10).add()
                .item().patient(patient).product(productFactory.createService()).unitPrice(20).add()
                .build();
        FinancialAct invoiceItem1 = invoiceBuilder.getItems().get(0);
        FinancialAct invoiceItem2 = invoiceBuilder.getItems().get(1);
        Party insurer = insuranceFactory.createInsurer();
        Act policy = insuranceFactory.createPolicy(customer, patient, insurer, "AB12345");
        FinancialAct claim = insuranceFactory.newClaim()
                .policy(policy)
                .location(practiceFactory.createLocation())
                .clinician(userFactory.createClinician())
                .claimHandler(userFactory.createUser())
                .item()
                .diagnosis("VENOM_328", "Abcess", "328")
                .invoiceItems(invoiceItem1)
                .add()
                .build();

        JXPathContext context1 = createContext(invoiceItem1);
        context1.getVariables().declareVariable("claim", claim);

        JXPathContext context2 = createContext(invoiceItem2);
        context2.getVariables().declareVariable("claim", claim);

        assertEquals(true, context1.getValue("insurance:claimed($claim, .)"));
        assertEquals(false, context2.getValue("insurance:claimed($claim, .)"));
    }

    /**
     * Tests the {@link InsuranceFunctions#policy(Object)} and {@link InsuranceFunctions#policy(Object, boolean)}
     * methods.
     */
    @Test
    public void testPolicy() {
        Party customer = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer);
        Party patient2 = patientFactory.createPatient(customer);
        Party insurer = insuranceFactory.createInsurer();

        JXPathContext context1 = createContext(patient1);
        assertNull(context1.getValue("insurance:policy(.)"));

        Act policy1 = insuranceFactory.newPolicy()
                .customer(customer)
                .patient(patient1)
                .insurer(insurer)
                .policyNumber("AB12345")
                .startTime("2023-01-01")
                .endTime("2024-01-01")
                .build();
        assertNull(context1.getValue("insurance:policy(.)"));
        assertNull(context1.getValue("insurance:policy(., false())"));
        assertEquals(policy1, context1.getValue("insurance:policy(., true())"));

        Act policy2 = insuranceFactory.newPolicy()
                .customer(customer)
                .patient(patient1)
                .insurer(insurer)
                .policyNumber("AB98765")
                .startTime(DateRules.getYesterday())
                .endTime(DateRules.getDate(DateRules.getYesterday(), 1, DateUnits.YEARS))
                .build();
        assertEquals(policy2, context1.getValue("insurance:policy(.)"));
        assertEquals(policy2, context1.getValue("insurance:policy(., false())"));
        assertEquals(policy2, context1.getValue("insurance:policy(., true())"));

        // patient without owner
        JXPathContext context2 = createContext(patient2);
        assertNull(context2.getValue("insurance:policy(.)"));
        assertNull(context2.getValue("insurance:policy(., false())"));
        assertNull(context2.getValue("insurance:policy(., true())"));

        // test null handling
        assertNull(context1.getValue("insurance:policy(null)"));
        assertNull(context1.getValue("insurance:policy(null, false())"));
        assertNull(context1.getValue("insurance:policy(null, true())"));
    }

    /**
     * Creates a new {@link JXPathContext} with the investigation functions registered.
     *
     * @param object the context object
     * @return a new JXPath context
     */
    private JXPathContext createContext(Object object) {
        FunctionLibrary library = new FunctionLibrary();
        InsuranceFunctions functions = new InsuranceFunctions(insuranceRules, patientRules, getArchetypeService());
        library.addFunctions(new ObjectFunctions(functions, "insurance"));
        return JXPathHelper.newContext(object, library);
    }
}