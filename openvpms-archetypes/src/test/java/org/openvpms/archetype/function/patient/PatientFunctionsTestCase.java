/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.patient;

import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.function.date.DateFunctions;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientAgeFormatter;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.supplier.SupplierRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.business.service.archetype.ArchetypeServiceFunctions;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.math.MathRules.ONE_THOUSAND;
import static org.openvpms.archetype.rules.util.DateRules.getDate;
import static org.openvpms.archetype.rules.util.DateUnits.HOURS;
import static org.openvpms.archetype.rules.util.DateUnits.MONTHS;
import static org.openvpms.archetype.rules.util.DateUnits.YEARS;
import static org.openvpms.archetype.rules.workflow.ScheduleTestHelper.createAppointment;
import static org.openvpms.archetype.rules.workflow.WorkflowStatus.PENDING;
import static org.openvpms.component.math.Weight.ONE_POUND_IN_GRAMS;
import static org.openvpms.component.math.Weight.ONE_POUND_IN_KILOS;

/**
 * Tests the {@link PatientFunctions} class.
 *
 * @author Tim Anderson
 */
public class PatientFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Tests the {@link PatientFunctions#age(ExpressionContext)}, {@link PatientFunctions#age(Object)}
     * and {@link PatientFunctions#age(Object, Date)} methods.
     */
    @Test
    public void testAge() {
        Date today = DateRules.getToday();
        Date yearAgo = getDate(today, -1, YEARS);
        Date monthAgo = getDate(today, -1, MONTHS);
        Date threeMonthsAgo = getDate(today, -3, MONTHS);
        Date fiveMonthsAgo = getDate(today, -5, MONTHS);
        Party patient = patientFactory.newPatient()
                .dateOfBirth(yearAgo)
                .build();

        Act event = patientFactory.createVisit(patient);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(event);
        JXPathContext context3 = createContext(null);
        JXPathContext context4 = createContext(create(PatientArchetypes.CLINICAL_EVENT)); // no patient
        context1.getVariables().declareVariable("date1", monthAgo);
        context1.getVariables().declareVariable("anull", null);
        context2.getVariables().declareVariable("date1", monthAgo);
        context1.getVariables().declareVariable("date2", fiveMonthsAgo);
        context2.getVariables().declareVariable("date2", fiveMonthsAgo);
        context3.getVariables().declareVariable("anull", null);
        context4.getVariables().declareVariable("anull", null);

        assertEquals("12 Months", context1.getValue("patient:age()")); // ExpressionContext
        assertEquals("12 Months", context1.getValue("patient:age(.)"));
        assertEquals("12 Months", context2.getValue("patient:age(openvpms:get(., 'patient.entity'))"));

        assertEquals("11 Months", context1.getValue("patient:age(., $date1)"));
        assertEquals("12 Months", context1.getValue("patient:age(., $anull)")); // null date treated as now
        assertEquals("11 Months", context2.getValue("patient:age(openvpms:get(., 'patient.entity'), $date1)"));

        // mark the patient deceased
        patientFactory.updatePatient(patient)
                .dateOfDeath(threeMonthsAgo)
                .build();

        assertEquals("9 Months", context1.getValue("patient:age()")); // ExpressionContext
        assertEquals("9 Months", context1.getValue("patient:age(.)"));
        assertEquals("9 Months", context2.getValue("patient:age(openvpms:get(., 'patient.entity'))"));

        // age as at 1 month ago is fixed at 9 months due to death
        assertEquals("9 Months", context1.getValue("patient:age(., $date1)"));
        assertEquals("9 Months", context1.getValue("patient:age(., null)"));
        assertEquals("9 Months", context1.getValue("patient:age(., $anull)"));
        assertEquals("9 Months", context2.getValue("patient:age(openvpms:get(., 'patient.entity'), $date1)"));

        // age as at 5 months ago
        assertEquals("7 Months", context1.getValue("patient:age(., $date2)"));
        assertEquals("7 Months", context2.getValue("patient:age(openvpms:get(., 'patient.entity'), $date2)"));

        // test null handling
        assertNull(context3.getValue("patient:age()"));
        assertNull(context3.getValue("patient:age(.)"));
        assertNull(context3.getValue("patient:age(., null)"));
        assertNull(context3.getValue("patient:age(., $anull)"));
        assertNull(context4.getValue("patient:age()"));     // context points to an event
        assertNull(context4.getValue("patient:age(openvpms:get(., 'patient.entity'))"));
        assertNull(context4.getValue("patient:age(openvpms:get(., 'patient.entity', $anull))"));
        assertNull(context4.getValue("patient:age(openvpms:get(., 'patient.entity', null))"));
    }

    /**
     * Tests the {@link PatientFunctions#alerts(Object)} method.
     */
    @Test
    public void testAlerts() {
        Party patient = patientFactory.createPatient();
        Act event = patientFactory.createVisit(patient);

        Date date1 = getDate(DateRules.getToday(), -12, MONTHS);
        Date date2 = getDate(DateRules.getToday(), -9, MONTHS);
        Date date3 = getDate(DateRules.getToday(), -6, MONTHS);
        Date date4 = getDate(DateRules.getToday(), -3, MONTHS);

        // check no alerts
        checkIterable(patient, "patient:alerts()");           // expression context
        checkIterable(patient, "patient:alerts(.)");
        checkIterable(event, "patient:alerts(patient:get())");
        checkIterable(event, "patient:alerts()");             // expression context - not a patient

        // create some alerts, and verify they are returned in date order
        Act alert1 = createAlert(patient, date1, createAlertType("Z Alert 1"));
        Act alert2 = createAlert(patient, date2, createAlertType("Z Alert 2"));
        Act alert3 = createAlert(patient, date3, createAlertType("Z Alert 3"));
        Act alert4 = createAlert(patient, date4, createAlertType("Z Alert 3"));

        checkIterable(patient, "patient:alerts()", alert1, alert2, alert3, alert4);  // expression context
        checkIterable(patient, "patient:alerts(.)", alert1, alert2, alert3, alert4);
        checkIterable(event, "patient:alerts(patient:get())", alert1, alert2, alert3, alert4);

        // mark an alert completed, and verify it is no longer returned
        alert3.setStatus(ActStatus.COMPLETED);
        save(alert3);

        checkIterable(patient, "patient:alerts()", alert1, alert2, alert4);
        checkIterable(patient, "patient:alerts(.)", alert1, alert2, alert4);
        checkIterable(event, "patient:alerts(patient:get())", alert1, alert2, alert4);

        // mark alert1 as in the past, and verify it is no longer returned
        alert1.setActivityStartTime(DateRules.getYesterday());
        alert1.setActivityEndTime(DateRules.getToday());
        save(alert1);

        checkIterable(patient, "patient:alerts()", alert2, alert4);
        checkIterable(patient, "patient:alerts(.)", alert2, alert4);
        checkIterable(event, "patient:alerts(patient:get())", alert2, alert4);

        // put alert4 into the future, and verify it is no longer returned
        alert4.setActivityStartTime(DateRules.getTomorrow());
        alert4.setActivityEndTime(DateRules.getNextDate(DateRules.getTomorrow()));
        save(alert4);

        checkIterable(patient, "patient:alerts()", alert2);
        checkIterable(patient, "patient:alerts(.)", alert2);
        checkIterable(event, "patient:alerts(patient:get())", alert2);

        // check null handling. In all cases, an empty collection should be returned
        checkIterable(null, "patient:alerts()");
        checkIterable(null, "patient:alerts(.)");
        checkIterable(create(PatientArchetypes.CLINICAL_EVENT), "patient:alerts(patient:get())");
    }

    /**
     * Tests the {@link PatientFunctions#appointments(Object, int, String)} method.
     */
    @Test
    public void testAppointments() {
        Party customer1 = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        Party customer2 = customerFactory.createCustomer();
        Party patient2 = patientFactory.createPatient();
        Party location = practiceFactory.createLocation();
        Entity schedule = schedulingFactory.createSchedule(location);
        Date now = new Date();
        Act act1a = createAppointment(getDate(now, -1, HOURS), schedule, customer1, patient1, PENDING);
        Act act1b = createAppointment(getDate(now, 6, MONTHS), schedule, customer1, patient1, PENDING);
        Act act1c = createAppointment(getDate(now, 9, MONTHS), schedule, customer1, null, PENDING);
        Act act1d = createAppointment(getDate(now, 2, YEARS), schedule, customer1, patient1, PENDING);
        Act act2a = createAppointment(getDate(now, -1, YEARS), schedule, customer2, patient2, PENDING);
        Act act2b = createAppointment(getDate(now, 1, MONTHS), schedule, customer2, patient2, PENDING);
        Act act2c = createAppointment(getDate(now, 6, MONTHS), schedule, customer2, patient2, PENDING);
        act2b.setStatus(AppointmentStatus.CANCELLED);
        save(act1a, act1b, act1c, act1d, act2a, act2b, act2c);

        checkIterable(customer1, "patient:appointments(., 1, 'YEARS')"); // must be invoked with patient
        checkIterable(patient1, "patient:appointments(., 3, 'YEARS')", act1b, act1d);
        checkIterable(patient2, "patient:appointments(., 1, 'YEARS')", act2c);

        // check null handling
        checkIterable(null, "patient:appointments(., 1, 'YEARS')");
        checkIterable(patient2, "patient:appointments(null, 1, 'YEARS')");
    }

    /**
     * Tests the {@link PatientFunctions#get(ExpressionContext)} and {@link PatientFunctions#get(Object)} methods.
     */
    @Test
    public void testGet() {
        Party patient = patientFactory.createPatient();
        Party customer = customerFactory.createCustomer();
        Act event = patientFactory.createVisit(patient);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(event);
        JXPathContext context3 = createContext(customer);
        JXPathContext context4 = createContext(null);

        assertEquals(patient, context1.getValue("patient:get()")); // ExpressionContext
        assertEquals(patient, context1.getValue("patient:get(.)"));
        assertEquals(patient, context2.getValue("patient:get()"));
        assertEquals(patient, context2.getValue("patient:get(.)"));

        // check null handling
        assertNull(context3.getValue("patient:get()"));          // context3 is a customer
        assertNull(context3.getValue("patient:get(.)"));
        assertNull(context4.getValue("patient:get()"));
        assertNull(context4.getValue("patient:get(.)"));
    }

    /**
     * Tests the {@link PatientFunctions#identity(ExpressionContext, String)} and
     * {@link PatientFunctions#identity(Object, String)} methods.
     */
    @Test
    public void testIdentity() {
        Party patient = patientFactory.newPatient().build(false);
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(act);
        JXPathContext context3 = createContext(null);

        assertNull(context1.getValue("patient:identity('entityIdentity.petTag')"));     // ExpressionContext
        assertNull(context1.getValue("patient:identity(., 'entityIdentity.petTag')"));    // Party

        assertNull(context2.getValue("patient:identity(patient:get(), 'entityIdentity.petTag')")); // by Act

        EntityIdentity tag = patientFactory.createPetTag("1234567");
        patient.addIdentity(tag);
        save(patient);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        // by Party
        assertEquals(tag, context1.getValue("patient:identity('entityIdentity.petTag')")); // ExpressionContext
        assertEquals(tag, context1.getValue("patient:identity(., 'entityIdentity.petTag')"));

        // by Act
        assertEquals(tag, context2.getValue("patient:identity(patient:get(), 'entityIdentity.petTag')"));

        // null handling
        assertNull(context1.getValue("patient:identity(null)"));
        assertNull(context3.getValue("patient:identity(null, 'entityIdentity.petTag')"));
        assertNull(context3.getValue("patient:identity(., 'entityIdentity.petTag')"));
    }

    /**
     * Tests the {@link PatientFunctions#identities(ExpressionContext, String)} and
     * {@link PatientFunctions#identities(Object, String)} methods.
     * methods.
     */
    @Test
    public void testIdentities() {
        Party patient = patientFactory.createPatient();
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        checkIterable(patient, "patient:identities('entityIdentity.petTag')");    // ExpressionContext
        checkIterable(patient, "patient:identities(., 'entityIdentity.petTag')"); // Party
        checkIterable(act, "patient:identities(patient:get(), 'entityIdentity.petTag')");
        // resolves patient from the act

        EntityIdentity tag1 = patientFactory.createPetTag("1234567");
        patient.addIdentity(tag1);
        save(patient); // tag should be returned second due to lower id

        EntityIdentity tag2 = patientFactory.createPetTag("9876543");
        patient.addIdentity(tag2);
        save(patient);

        checkIterable(patient, "patient:identities('entityIdentity.petTag')", tag2, tag1);
        checkIterable(patient, "patient:identities(., 'entityIdentity.petTag')", tag2, tag1);
        checkIterable(act, "patient:identities(patient:get(), 'entityIdentity.petTag')", tag2, tag1);

        // deactivate tag1 and verify its not returned
        tag1.setActive(false);
        save(patient);
        checkIterable(patient, "patient:identities('entityIdentity.petTag')", tag2);
        checkIterable(patient, "patient:identities(., 'entityIdentity.petTag')", tag2);
        checkIterable(act, "patient:identities(patient:get(), 'entityIdentity.petTag')", tag2);


        // null handling
        checkIterable(null, "patient:identities(null)");
        checkIterable(null, "patient:identities('entityIdentity.petTag')");
        checkIterable(act, "patient:identities(null)");
    }

    /**
     * Tests the {@link PatientFunctions#microchip(ExpressionContext)} and {@link PatientFunctions#microchip(Object)}
     * methods.
     */
    @Test
    public void testMicrochip() {
        Party patient = patientFactory.newPatient().build(false);
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(act);
        JXPathContext context3 = createContext(null);

        assertNull(context1.getValue("patient:microchip()"));     // ExpressionContext
        assertNull(context1.getValue("patient:microchip(.)"));    // Party

        assertNull(context2.getValue("patient:microchip(patient:get())")); // by Act

        EntityIdentity microchip = patientFactory.createMicrochip("1234567");
        patient.addIdentity(microchip);
        save(patient);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        // by Party
        assertEquals(microchip, context1.getValue("patient:microchip()")); // ExpressionContext
        assertEquals(microchip, context1.getValue("patient:microchip(.)"));

        // by Act
        assertEquals(microchip, context2.getValue("patient:microchip(patient:get())"));

        // null handling
        assertNull(context1.getValue("patient:microchip(null)"));
        assertNull(context3.getValue("patient:microchip()"));
        assertNull(context3.getValue("patient:microchip(.)"));
    }

    /**
     * Tests the {@link PatientFunctions#microchips(ExpressionContext)} and {@link PatientFunctions#microchips(Object)}
     * methods.
     */
    @Test
    public void testMicrochips() {
        Party patient = patientFactory.createPatient();
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        checkIterable(patient, "patient:microchips()");         // ExpressionContext
        checkIterable(patient, "patient:microchips(.)");        // Party
        checkIterable(act, "patient:microchips(patient:get())"); // resolves patient from the act

        EntityIdentity microchip1 = patientFactory.createMicrochip("1234567");
        patient.addIdentity(microchip1);
        save(patient); // microchip1 should be returned second due to lower id

        EntityIdentity microchip2 = patientFactory.createMicrochip("9876543");
        patient.addIdentity(microchip2);
        save(patient);

        checkIterable(patient, "patient:microchips()", microchip2, microchip1);
        checkIterable(patient, "patient:microchips(.)", microchip2, microchip1);
        checkIterable(act, "patient:microchips(patient:get())", microchip2, microchip1);

        // deactivate microchip1 and verify its not returned
        microchip1.setActive(false);
        save(patient);
        checkIterable(patient, "patient:microchips()", microchip2);
        checkIterable(patient, "patient:microchips(.)", microchip2);
        checkIterable(act, "patient:microchips(patient:get())", microchip2);


        // null handling
        checkIterable(null, "patient:microchips()");
        checkIterable(null, "patient:microchips(.)");
        checkIterable(act, "patient:microchips(null)");
    }

    /**
     * Tests the {@link PatientFunctions#owner(ExpressionContext)}, {@link PatientFunctions#owner(Object)}
     * and {@link PatientFunctions#owner(Object, Date)} methods.
     */
    @Test
    public void testOwner() {
        Party customer1 = customerFactory.createCustomer();
        Party customer2 = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();

        JXPathContext context1 = createContext(patient);

        // no customer
        assertNull(context1.getValue("patient:owner()"));
        assertNull(context1.getValue("patient:owner(.)"));

        patientFactory.updatePatient(patient)
                .addOwner(customer1, DateRules.getYesterday(), DateRules.getToday())
                .addOwner(customer2, DateRules.getToday(), null)  // current owner
                .build();

        assertEquals(customer2, context1.getValue("patient:owner()"));        // ExpressionContext
        assertEquals(customer2, context1.getValue("patient:owner(.)"));

        assertEquals(customer1, context1.getValue("patient:owner(., date:yesterday())"));
        assertEquals(customer2, context1.getValue("patient:owner(., date:now())"));

        // null handling
        assertNull(context1.getValue("patient:owner(null)"));
        assertNull(context1.getValue("patient:owner(null, null)"));
    }

    /**
     * Tests the {@link PatientFunctions#petTag(ExpressionContext)}  and {@link PatientFunctions#petTag(Object)}
     * methods.
     */
    @Test
    public void testPetTag() {
        Party patient = patientFactory.newPatient().build(false);
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(act);
        JXPathContext context3 = createContext(null);

        assertNull(context1.getValue("patient:petTag()"));     // ExpressionContext
        assertNull(context1.getValue("patient:petTag(.)"));    // Party
        assertNull(context2.getValue("patient:petTag(patient:get())")); // by Act

        EntityIdentity tag = patientFactory.createPetTag("1234567");
        patient.addIdentity(tag);
        save(patient);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        // by Party
        assertEquals(tag, context1.getValue("patient:petTag()")); // ExpressionContext
        assertEquals(tag, context1.getValue("patient:petTag(.)"));

        // by Act
        assertEquals(tag, context2.getValue("patient:petTag(patient:get())"));

        // null handling
        assertNull(context1.getValue("patient:petTag(null)"));
        assertNull(context3.getValue("patient:petTag()"));
        assertNull(context3.getValue("patient:petTag(.)"));
    }

    /**
     * Tests the {@link PatientFunctions#petTags(ExpressionContext)} and {@link PatientFunctions#petTags(Object)}
     * methods.
     */
    @Test
    public void testPetTags() {
        Party patient = patientFactory.createPatient();
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        checkIterable(patient, "patient:petTags()");          // ExpressionContext
        checkIterable(patient, "patient:petTags(.)");         // Party
        checkIterable(act, "patient:petTags(patient:get())"); // resolves patient from the act

        EntityIdentity tag1 = patientFactory.createPetTag("1234567");
        patient.addIdentity(tag1);
        save(patient);

        EntityIdentity tag2 = patientFactory.createPetTag("9876543");
        patient.addIdentity(tag2);
        save(patient);

        // tag1 should be returned second due to lower id
        checkIterable(patient, "patient:petTags()", tag2, tag1);
        checkIterable(patient, "patient:petTags(.)", tag2, tag1);
        checkIterable(act, "patient:petTags(patient:get())", tag2, tag1);

        // deactivate tag1 and verify its not returned
        tag1.setActive(false);
        save(patient);
        checkIterable(patient, "patient:petTags()", tag2);
        checkIterable(patient, "patient:petTags(.)", tag2);
        checkIterable(act, "patient:petTags(patient:get())", tag2);


        // null handling
        checkIterable(null, "patient:petTags()");
        checkIterable(null, "patient:petTags(.)");
        checkIterable(act, "patient:petTags(null)");
    }

    /**
     * Tests the {@link PatientFunctions#rabiesTag(ExpressionContext)}  and {@link PatientFunctions#rabiesTag(Object)}
     * methods.
     */
    @Test
    public void testRabiesTag() {
        Party patient = patientFactory.newPatient().build(false);
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(act);
        JXPathContext context3 = createContext(null);

        assertNull(context1.getValue("patient:rabiesTag()"));     // ExpressionContext
        assertNull(context1.getValue("patient:rabiesTag(.)"));    // Party
        assertNull(context2.getValue("patient:rabiesTag(patient:get())")); // by Act

        EntityIdentity tag = patientFactory.createRabiesTag("1234567");
        patient.addIdentity(tag);
        getArchetypeService().save(patient, false); // disable validation as rabies tags aren't enabled by default.

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        // by Party
        assertEquals(tag, context1.getValue("patient:rabiesTag()")); // ExpressionContext
        assertEquals(tag, context1.getValue("patient:rabiesTag(.)"));

        // by Act
        assertEquals(tag, context2.getValue("patient:rabiesTag(patient:get())"));

        // null handling
        assertNull(context1.getValue("patient:rabiesTag(null)"));
        assertNull(context3.getValue("patient:rabiesTag()"));
        assertNull(context3.getValue("patient:rabiesTag(.)"));
    }

    /**
     * Tests the {@link PatientFunctions#rabiesTags(ExpressionContext)} and {@link PatientFunctions#rabiesTags(Object)}
     * methods.
     */
    @Test
    public void testRabiesTags() {
        Party patient = patientFactory.createPatient();
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);

        IMObjectBean bean = getBean(act);
        bean.setTarget("patient", patient);

        checkIterable(patient, "patient:rabiesTags()");          // ExpressionContext
        checkIterable(patient, "patient:rabiesTags(.)");         // Party
        checkIterable(act, "patient:rabiesTags(patient:get())"); // resolves patient from the act

        EntityIdentity tag1 = patientFactory.createRabiesTag("1234567");
        patient.addIdentity(tag1);
        IArchetypeService service = getArchetypeService();
        service.save(patient, false); // disable validation as rabies tags aren't enabled by default.

        EntityIdentity tag2 = patientFactory.createRabiesTag("9876543");
        patient.addIdentity(tag2);
        service.save(patient, false);

        // tag1 should be returned second due to lower id
        checkIterable(patient, "patient:rabiesTags()", tag2, tag1);
        checkIterable(patient, "patient:rabiesTags(.)", tag2, tag1);
        checkIterable(act, "patient:rabiesTags(patient:get())", tag2, tag1);

        // deactivate tag1 and verify its not returned
        tag1.setActive(false);
        service.save(patient, false);
        checkIterable(patient, "patient:rabiesTags()", tag2);
        checkIterable(patient, "patient:rabiesTags(.)", tag2);
        checkIterable(act, "patient:rabiesTags(patient:get())", tag2);


        // null handling
        checkIterable(null, "patient:rabiesTags()");
        checkIterable(null, "patient:rabiesTags(.)");
        checkIterable(act, "patient:rabiesTags(null)");
    }

    /**
     * Tests the {@link PatientFunctions#referral(ExpressionContext)}, {@link PatientFunctions#referral(Object)},
     * {@link PatientFunctions#referral(Object, boolean)}, {@link PatientFunctions#referralByDate(Object, Date)},
     * and {@link PatientFunctions#referral(Object, boolean, Date)} methods.
     */
    @Test
    public void testReferral() {
        Party patient = patientFactory.createPatient();
        Party vet = supplierFactory.createVet();
        Party practice = supplierFactory.createVetPractice();
        Act act = create(EstimateArchetypes.ESTIMATE_ITEM, Act.class);
        JXPathContext context1 = createContext(patient);
        JXPathContext context2 = createContext(act);

        assertNull(context1.getValue("patient:referral()"));   // ExpressionContext method
        assertNull(context1.getValue("patient:referral(.)"));  // Object method

        assertNull(context2.getValue("patient:referral(patient:get())"));

        // create relationships between the patient, vet, and vet practice. Back date the relationships to avoid
        // timing issues (i.e. milliseconds are lost when saving)
        IMObjectBean bean = getBean(patient);
        PeriodRelationship referrals
                = (PeriodRelationship) bean.addTarget("referrals", PatientArchetypes.REFERRED_FROM, vet);
        referrals.setActiveStartTime(DateRules.getToday());
        IMObjectBean practiceBean = getBean(practice);
        PeriodRelationship veterinarians = (PeriodRelationship) practiceBean.addTarget("veterinarians", vet);
        veterinarians.setActiveStartTime(DateRules.getToday());
        save(patient, vet, practice);

        IMObjectBean actBean = getBean(act);
        actBean.setTarget("patient", patient);

        assertEquals(vet, context1.getValue("patient:referral()"));
        assertEquals(vet, context1.getValue("patient:referral(.)"));
        assertEquals(practice, context1.getValue("patient:referral(., true())"));
        assertEquals(vet, context1.getValue("patient:referral(., false())"));
        assertEquals(vet, context1.getValue("patient:referral(., date:today())"));
        assertEquals(practice, context1.getValue("patient:referral(., true(), date:today())"));
        assertEquals(vet, context1.getValue("patient:referral(., false(), date:today())"));

        // null handling
        assertNull(context1.getValue("patient:referral(null)"));
        assertNull(context1.getValue("patient:referral(., date:yesterday())"));
        assertNull(context1.getValue("patient:referral(., true(), date:yesterday())"));
        assertNull(context1.getValue("patient:referral(., false(), date:yesterday())"));
    }

    /**
     * Tests the {@link PatientFunctions#visit(ExpressionContext)} and {@link PatientFunctions#visit(Object)}
     * methods.
     */
    @Test
    public void testVisit() {
        Party patient = patientFactory.createPatient();

        JXPathContext context1 = createContext(patient);
        assertNull(context1.getValue("patient:visit()"));  // ExpressionContext
        assertNull(context1.getValue("patient:visit(.)"));  // Object

        Act visit1 = patientFactory.newVisit()
                .startTime(DateRules.getYesterday())
                .patient(patient)
                .build();
        assertEquals(visit1, context1.getValue("patient:visit()"));
        assertEquals(visit1, context1.getValue("patient:visit(.)"));

        Act visit2 = patientFactory.newVisit()
                .startTime(DateRules.getToday())
                .patient(patient)
                .build();
        assertEquals(visit2, context1.getValue("patient:visit()"));
        assertEquals(visit2, context1.getValue("patient:visit(.)"));

        // null handling
        assertNull(context1.getValue("patient:visit(null)"));
    }

    /**
     * Tests the {@link PatientFunctions#weight(ExpressionContext)}, {@link PatientFunctions#weight(Object)} and
     * {@link PatientFunctions#weight(Object, String)} methods.
     */
    @Test
    public void testWeight() {
        Party patient = patientFactory.createPatient();

        Act visit = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        IMObjectBean bean = getBean(visit);
        bean.setTarget("patient", patient);

        JXPathContext context1 = createContext(patient);

        assertEquals(ZERO, context1.getValue("patient:weight()"));
        assertEquals(ZERO, context1.getValue("patient:weight(.)"));

        Act weight1 = patientFactory.createWeight(patient, ONE, WeightUnits.KILOGRAMS);
        checkEquals(ONE, (BigDecimal) context1.getValue("patient:weight()"));
        checkEquals(ONE, (BigDecimal) context1.getValue("patient:weight(.)"));
        checkEquals(ONE, (BigDecimal) context1.getValue("patient:weight(., 'KILOGRAMS')"));
        checkEquals(ONE_THOUSAND, (BigDecimal) context1.getValue("patient:weight(., 'GRAMS')"));
        assertEquals(new BigDecimal("2.20462262"), context1.getValue("patient:weight(., 'POUNDS')"));

        remove(weight1);
        Act weight2 = patientFactory.createWeight(patient, ONE_THOUSAND, WeightUnits.GRAMS);
        checkEquals(ONE, (BigDecimal) context1.getValue("patient:weight()"));
        checkEquals(ONE, (BigDecimal) context1.getValue("patient:weight(., 'KILOGRAMS')"));
        checkEquals(ONE_THOUSAND, (BigDecimal) context1.getValue("patient:weight(., 'GRAMS')"));
        assertEquals(new BigDecimal("2.20462262"), context1.getValue("patient:weight(., 'POUNDS')"));

        remove(weight2);

        patientFactory.newWeight()
                .patient(patient)
                .weight(ONE, WeightUnits.POUNDS)
                .build();
        checkEquals(ONE_POUND_IN_KILOS, (BigDecimal) context1.getValue("patient:weight()"));
        checkEquals(ONE_POUND_IN_KILOS, (BigDecimal) context1.getValue("patient:weight(.)"));
        checkEquals(ONE_POUND_IN_KILOS, (BigDecimal) context1.getValue("patient:weight(., 'KILOGRAMS')"));
        checkEquals(ONE_POUND_IN_GRAMS, (BigDecimal) context1.getValue("patient:weight(., 'GRAMS')"));
        assertEquals(ONE, context1.getValue("patient:weight(., 'POUNDS')"));

        // null handling
        checkEquals(ZERO, (BigDecimal) context1.getValue("patient:weight(null)"));
        checkEquals(ZERO, (BigDecimal) context1.getValue("patient:weight(null, 'KILOGRAMS')"));
    }

    /**
     * Creates a new alert type.
     *
     * @param name the alert type name
     * @return a new alert type
     */
    private Entity createAlertType(String name) {
        return patientFactory.newAlertType()
                .name(name)
                .build();
    }

    /**
     * Creates a new alert for a patient.
     *
     * @param patient   the patient
     * @param date      the alert date
     * @param alertType the alert type
     * @return a new alert
     */
    private Act createAlert(Party patient, Date date, Entity alertType) {
        return patientFactory.newAlert()
                .patient(patient)
                .startTime(date)
                .alertType(alertType)
                .build();
    }

    /**
     * Verifies that a function returning an {@code Iterable} returns the expected results.
     *
     * @param object     the context object. May be {@code null}
     * @param expression the jxpath expression
     * @param expected   the expected results
     */
    @SuppressWarnings("unchecked")
    private <T> void checkIterable(Object object, String expression, T... expected) {
        List<T> actual = new ArrayList<>();
        JXPathContext context = createContext(object);
        Iterable<T> value = (Iterable<T>) context.getValue(expression);
        value.forEach(actual::add);
        assertArrayEquals(actual.toArray(), expected);
    }

    /**
     * Creates a new JXPathContext, with the party functions registered.
     *
     * @param object the context object. May  be {@code null}
     * @return a new JXPathContext
     */
    private JXPathContext createContext(Object object) {
        // use the non-rules based archetype service, as that is what is used at deployment
        IArchetypeService service = applicationContext.getBean("archetypeService", IArchetypeService.class);
        LookupService lookups = getLookupService();
        PracticeRules practiceRules = Mockito.mock(PracticeRules.class);
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        PatientAgeFormatter formatter = new PatientAgeFormatter(getLookupService(), practiceRules, service);
        PatientRules patientRules = new PatientRules(practiceRules, practiceService, service, getLookupService(),
                                                     formatter);
        SupplierRules supplierRules = new SupplierRules(service);
        AppointmentRules appointmentRules = new AppointmentRules(service);
        MedicalRecordRules recordRules = new MedicalRecordRules(service);
        ArchetypeServiceFunctions functions = new ArchetypeServiceFunctions(service, lookups);
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new ObjectFunctions(functions, "openvpms"));
        library.addFunctions(new PatientFunctions(patientRules, supplierRules, appointmentRules, recordRules, service));
        library.addFunctions(new ObjectFunctions(new DateFunctions(), "date"));
        return JXPathHelper.newContext(object, library);
    }
}
