/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.document;

import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link DocumentFunctions}.
 *
 * @author Tim Anderson
 */
public class DocumentFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * Tests the document:text(object, node) method for an <em>act.patientClinicalNote</em>
     */
    @Test
    public void testTextFromPatientNote() {
        checkTextFromNote(PatientArchetypes.CLINICAL_NOTE);
    }

    /**
     * Tests the document:text(object, node) method for an <em>act.patientClinicalAddendum</em>
     */
    @Test
    public void testTextFromPatientAddendum() {
        checkTextFromNote(PatientArchetypes.CLINICAL_ADDENDUM);
    }

    /**
     * Tests the document:text(object, node, relationshipNode) method for an <em>act.patientInvestigationResults</em>
     */
    @Test
    public void testTextFromInvestigationResults() {
        Act results = create(InvestigationArchetypes.RESULTS, Act.class);
        IMObjectBean bean = getBean(results);
        bean.setValue("resultsId", "1");

        // test no note
        JXPathContext context = createContext(results);
        assertNull(context.getValue("document:text(., 'notes', 'longNotes')"));

        // test short note
        bean.setValue("notes", "foo");
        assertEquals("foo", context.getValue("document:text(., 'notes', 'longNotes')"));

        // test long note
        bean.setValue("notes", null);
        DocumentAct text = create(InvestigationArchetypes.RESULT_NOTE, DocumentAct.class);
        text.setDocument(createDocument());
        bean.addTarget("longNotes", text, "parent");
        bean.save(text);

        assertEquals("long content", context.getValue("document:text(., 'notes', 'longNotes')"));
    }

    /**
     * Tests the document:text() method for a patient note archetype.
     *
     * @param archetype the patient note archetype
     */
    private void checkTextFromNote(String archetype) {
        DocumentAct act1 = create(archetype, DocumentAct.class);
        JXPathContext context1 = createContext(act1);
        assertNull(context1.getValue("document:text(., 'note')"));

        IMObjectBean bean1 = getBean(act1);
        bean1.setValue("note", "Some text");

        assertEquals("Some text", context1.getValue("document:text(., 'note')"));

        DocumentAct act2 = create(archetype, DocumentAct.class);
        act2.setDocument(createDocument());
        JXPathContext context2 = createContext(act2);

        assertEquals("long content", context2.getValue("document:text(., 'note')"));
    }

    /**
     * Creates a text document.
     *
     * @return a reference to a text document
     */
    private Reference createDocument() {
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        Document document = handler.create("note", "long content");
        save(document);
        return document.getObjectReference();
    }

    /**
     * Creates a new {@code JXPathContext} with the history functions registered.
     *
     * @param object the context object
     * @return a new JXPath context
     */
    private JXPathContext createContext(IMObject object) {
        DocumentFunctions functions = new DocumentFunctions(getArchetypeService(), getLookupService());
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new ObjectFunctions(functions, "document"));
        return JXPathHelper.newContext(object, library);
    }

}
