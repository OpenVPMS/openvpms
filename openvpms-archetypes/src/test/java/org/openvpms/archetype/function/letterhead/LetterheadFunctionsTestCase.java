/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.letterhead;

import org.apache.commons.io.FileUtils;
import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.doc.ImageService;
import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.archetype.rules.doc.TestImageService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestLetterheadBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link LetterheadFunctions} class.
 *
 * @author Tim Anderson
 */
public class LetterheadFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * Temporary directory.
     */
    @Rule
    public TemporaryFolder dir = new TemporaryFolder();

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The logo service.
     */
    private LogoService logoService;

    /**
     * Sets up the test case.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        ImageService imageService = new TestImageService(dir.getRoot(), handlers, getArchetypeService());
        logoService = new LogoService(imageService, getArchetypeService(), documentRules);
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        logoService.destroy();
    }

    /**
     * Tests the {@link LetterheadFunctions#logo(Object)} method.
     */
    @Test
    public void testLogo() throws IOException {
        // verify that letterhead with no logo returns a null URL
        Entity letterhead1 = documentFactory.newLetterhead().build();
        Party location1 = practiceFactory.newLocation()
                .letterhead(letterhead1)
                .build();
        checkLogo(location1, null);

        // verify that letterhead with logoFile set returns the URL of the file
        File logoFile = dir.newFile("logo.png");
        FileUtils.write(logoFile, "some data", StandardCharsets.UTF_8);
        Entity letterhead2 = documentFactory.newLetterhead()
                .logoFile(logoFile.getPath())
                .build();
        Party location2 = practiceFactory.newLocation()
                .letterhead(letterhead2)
                .build();
        checkLogo(location2, logoFile.toURI().toURL());

        // verify that letterhead with a logo set returns the URL of the cached image
        TestLetterheadBuilder builder = documentFactory.newLetterhead();
        Entity letterhead3 = builder
                .logo(documentFactory.createDocument(logoFile))
                .build();
        Party location3 = practiceFactory.newLocation()
                .letterhead(letterhead3)
                .build();
        DocumentAct logo = builder.getLogo();
        String url = dir.getRoot().toURI().toURL().toString();
        url += logo.getId() + "_" + logo.getVersion() + "_" + logoFile.getName();
        checkLogo(location3, new URL(url));

        // verify that if letterhead has both logoFile and logo, logoFile is returned
        Entity letterhead4 = documentFactory.newLetterhead()
                .logoFile(logoFile.getPath())
                .logo(documentFactory.createDocument(logoFile))
                .build();
        Party location4 = practiceFactory.newLocation()
                .letterhead(letterhead4)
                .build();
        checkLogo(location4, logoFile.toURI().toURL());

        // verify that null is returned if a location has no letterhead
        Party location5 = practiceFactory.createLocation();
        checkLogo(location5, null);

        // test null handling
        JXPathContext context6 = createContext(new Object());
        context6.getVariables().declareVariable("OpenVPMS.location", null); // null variable
        checkLogo(context6, "$OpenVPMS.location", null);

        JXPathContext context7 = createContext(new Object());
        checkLogo(context7, null, null);                                    // null parameter
    }

    /**
     * Verifies a logo URL matches that expected.
     *
     * @param location the practice location
     * @param expected the expected URL
     */
    private void checkLogo(Party location, URL expected) {
        checkLogo(createContext(location), ".", expected);
    }

    /**
     * Verifies a logo URL matches that expected.
     *
     * @param context   the JXPath context to evaluate against
     * @param parameter the parameter to pass to letterhead:logo(). May be {@code null}
     * @param expected  the expected URL. May be  {@code null}
     */
    private void checkLogo(JXPathContext context, String parameter, URL expected) {
        URL actual = (URL) context.getValue("letterhead:logo(" + parameter + ")");
        assertEquals(expected, actual);
    }

    /**
     * Creates a new {@code JXPathContext} with the letterhead functions registered.
     *
     * @param object the context object
     * @return a new JXPath context
     */
    private JXPathContext createContext(Object object) {
        LetterheadFunctions letterhead = new LetterheadFunctions(logoService, getArchetypeService());
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(letterhead);
        return JXPathHelper.newContext(object, library);
    }

}