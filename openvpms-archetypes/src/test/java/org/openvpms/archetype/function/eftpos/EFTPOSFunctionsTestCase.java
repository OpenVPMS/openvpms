/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.eftpos;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static java.math.BigDecimal.TEN;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EFTPOSFunctions}.
 *
 * @author Tim Anderson
 */
public class EFTPOSFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;


    /**
     * Tests the {@link EFTPOSFunctions#printableReceipts(Object)}
     * and {@link EFTPOSFunctions#printableReceipts(ExpressionContext)}
     * methods.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testPrintableReceipts() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.newEFTPOSTerminal()
                .printer(false)  // terminal not responsible for printing
                .build();
        FinancialAct transaction1 = accountFactory.newEFTPOSPayment()
                .customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .addMerchantReceipt("MERCHANT 1", true)
                .addCustomerReceipt("CUSTOMER 1")
                .status(EFTPOSTransactionStatus.DECLINED)
                .build();
        FinancialAct transaction2 = accountFactory.newEFTPOSPayment()
                .customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .addMerchantReceipt("MERCHANT 2", true)
                .addCustomerReceipt("CUSTOMER 2")
                .status(EFTPOSTransactionStatus.APPROVED)
                .build();
        FinancialAct item1 = accountFactory.newEFTPaymentItem()
                .amount(TEN)
                .addTransaction(transaction1)
                .addTransaction(transaction2)
                .build(false);
        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item1)
                .status(ActStatus.POSTED)
                .build();

        addParentId(transaction1, payment);
        addParentId(transaction2, payment);

        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new ObjectFunctions(new EFTPOSFunctions(getArchetypeService()), "eftpos"));
        JXPathContext context = JXPathHelper.newContext(payment, library);

        // ExpressionContext version
        Iterable<DocumentAct> receipt1 = (Iterable<DocumentAct>) context.getValue("eftpos:printableReceipts()");
        checkReceipts(receipt1, "CUSTOMER 1", "CUSTOMER 2");

        // Object version
        Iterable<DocumentAct> receipt2 = (Iterable<DocumentAct>) context.getValue("eftpos:printableReceipts(.)");
        checkReceipts(receipt2, "CUSTOMER 1", "CUSTOMER 2");

        // verifies that when the terminal prints receipts, no receipts are returned.
        IMObjectBean bean = getBean(terminal);
        bean.setValue("printer", true);
        bean.save();

        Iterable<DocumentAct> receipt3 = (Iterable<DocumentAct>) context.getValue("eftpos:printableReceipts()");
        checkReceipts(receipt3);
    }

    /**
     * Adds the parent id to an EFTPOS transaction.
     *
     * @param transaction the EFTPOS transaction
     * @param parent      the parent act
     */
    private void addParentId(FinancialAct transaction, Act parent) {
        ActIdentity identity = create(EFTPOSArchetypes.PARENT_ID, ActIdentity.class);
        identity.setIdentity(Long.toString(parent.getId()));
        transaction.addIdentity(identity);
        save(transaction);
    }

    private void checkReceipts(Iterable<DocumentAct> receipts, String... expected) {
        assertNotNull(receipts);
        List<DocumentAct> actual = IteratorUtils.toList(receipts.iterator());
        assertEquals(expected.length, actual.size());
        List<String> list = Arrays.asList(expected);
        for (DocumentAct act : actual) {
            IMObjectBean bean = getBean(act);
            String receipt = bean.getString("receipt");
            assertNotNull(receipt); // too short to be saved as a document
            assertTrue(list.contains(receipt));
        }
    }
}
