/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.contact;

import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Test the {@link EmailFunctions} class.
 *
 * @author Tim Anderson
 */
public class EmailFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The contact factory.
     */
    @Autowired
    private TestContactFactory contactFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * Verifies that the appropriate contacts are returned.
     */
    @Test
    public void testGetContacts() {
        Contact emailA = contactFactory.createEmail("a@foo.com", "BILLING");
        Contact emailB = contactFactory.createEmail("b@foo.com", "CORRESPONDENCE");
        Contact emailC = contactFactory.createEmail("c@foo.com", "REMINDER");
        Contact emailD = contactFactory.createEmail("d@foo.com", "HOME");
        Contact emailE = contactFactory.createEmail("e@foo.com", true);
        save(emailA);   // when contacts have equal relevance, they are returned lowest id first
        save(emailB);
        save(emailC);
        save(emailD);
        save(emailE);
        Party customer = customerFactory.newCustomer()
                .addContacts(emailA, emailB, emailC, emailD, emailE)
                .build();

        // test resolving the contact by customer
        JXPathContext context1 = createContext(customer);
        assertEquals(emailE, context1.getValue("email:preferred(.)"));
        assertEquals(emailA, context1.getValue("email:billing(.)"));
        assertEquals(emailB, context1.getValue("email:correspondence(.)"));
        assertEquals(emailC, context1.getValue("email:reminder(.)"));
        assertEquals(emailD, context1.getValue("email:purpose(., 'HOME')"));

        // test resolving the contact by customer id
        JXPathContext context2 = createContext(new Object());
        assertEquals(emailE, context2.getValue("email:preferred(" + customer.getId() + ")"));
        assertEquals(emailA, context2.getValue("email:billing(" + customer.getId() + ")"));
        assertEquals(emailB, context2.getValue("email:correspondence(" + customer.getId() + ")"));
        assertEquals(emailC, context2.getValue("email:reminder(" + customer.getId() + ")"));
        assertEquals(emailD, context2.getValue("email:purpose(" + customer.getId() + ", 'HOME')"));
    }

    /**
     * Checks behaviour where there are no contacts of the relevant type.
     */
    @Test
    public void testNoContacts() {
        Party customer1 = customerFactory.createCustomer();
        JXPathContext context1 = createContext(customer1);
        assertNull(context1.getValue("email:preferred(.)"));
        assertNull(context1.getValue("email:billing(.)"));
        assertNull(context1.getValue("email:correspondence(.)"));
        assertNull(context1.getValue("email:reminder(.)"));
        assertNull(context1.getValue("email:purpose(., 'HOME')"));

        Party customer2 = customerFactory
                .newCustomer()
                .newLocation().preferred().purposes("BILLING", "CORRESPONDENCE", "REMINDER", "HOME").add()
                .build();
        assertEquals(1, customer2.getContacts().size());

        JXPathContext context2 = createContext(customer2);
        assertNull(context2.getValue("email:preferred(.)"));
        assertNull(context2.getValue("email:billing(.)"));
        assertNull(context2.getValue("email:correspondence(.)"));
        assertNull(context2.getValue("email:reminder(.)"));
        assertNull(context2.getValue("email:purpose(., 'HOME')"));
    }

    /**
     * Tests null handling.
     */
    @Test
    public void testNull() {
        JXPathContext context3 = createContext(new Object());
        assertNull(context3.getValue("email:preferred(null)"));
        assertNull(context3.getValue("email:billing(null)"));
        assertNull(context3.getValue("email:correspondence(null)"));
        assertNull(context3.getValue("email:reminder(null)"));
        assertNull(context3.getValue("email:purpose(null, 'WORK')"));
    }

    /**
     * Creates a new JXPathContext, with the party functions registered.
     *
     * @param object the context object
     * @return a new JXPathContext
     */
    private JXPathContext createContext(Object object) {
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new EmailFunctions(customerRules, getArchetypeService()));
        return JXPathHelper.newContext(object, library);
    }

}

