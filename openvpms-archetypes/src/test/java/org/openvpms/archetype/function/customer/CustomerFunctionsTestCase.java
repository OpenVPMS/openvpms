/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.customer;

import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link CustomerFunctions} class.
 *
 * @author Tim Anderson
 */
public class CustomerFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests the {@link CustomerFunctions#get(ExpressionContext)} and {@link CustomerFunctions#get(Object)} methods.
     */
    @Test
    public void testGet() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        Act form = customerFactory.newForm()
                .customer(customer)
                .template()
                .build();

        JXPathContext context1 = createContext(customer);
        JXPathContext context2 = createContext(form);
        JXPathContext context3 = createContext(patient);
        JXPathContext context4 = createContext(null);

        assertEquals(customer, context1.getValue("customer:get()")); // ExpressionContext
        assertEquals(customer, context1.getValue("customer:get(.)"));
        assertEquals(customer, context2.getValue("customer:get()"));
        assertEquals(customer, context2.getValue("customer:get(.)"));

        // check null handling
        assertNull(context3.getValue("customer:get()"));          // context3 is a patient
        assertNull(context3.getValue("customer:get(.)"));
        assertNull(context4.getValue("customer:get()"));
        assertNull(context4.getValue("customer:get(.)"));
    }

    /**
     * Creates a new JXPathContext, with the party functions registered.
     *
     * @param object the context object. May  be {@code null}
     * @return a new JXPathContext
     */
    private JXPathContext createContext(Object object) {
        FunctionLibrary library = new FunctionLibrary();
        library.addFunctions(new CustomerFunctions(getArchetypeService()));
        return JXPathHelper.newContext(object, library);
    }
}