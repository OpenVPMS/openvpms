/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.user;

import org.apache.commons.jxpath.FunctionLibrary;
import org.apache.commons.jxpath.Functions;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.openvpms.archetype.function.expression.ExpressionFunctions;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.ImageService;
import org.openvpms.archetype.rules.doc.TestImageService;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link UserFunctions} and {@link CachingUserFunctions} classes.
 *
 * @author Tim Anderson
 */
public class UserFunctionsTestCase extends ArchetypeServiceTest {

    /**
     * Temporary directory.
     */
    @Rule
    public TemporaryFolder dir = new TemporaryFolder();

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The user rules.
     */
    @Autowired
    private UserRules userRules;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * The JXPath context.
     */
    private Party practice;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        practice = create(PracticeArchetypes.PRACTICE, Party.class);
        practiceService = Mockito.mock(PracticeService.class);
        Mockito.when(practiceService.getPractice()).thenReturn(practice);
    }

    /**
     * Tests the {@link UserFunctions#format(User, String)} and {@link UserFunctions#formatById(long, String)} methods.
     */
    @Test
    public void testFormat() {
        User user1 = createUser("Dr Jo Bloggs Name", "Dr Jo Bloggs Desc, BVSc MVS", "Dr", "Jo", "Bloggs", "BVSc MVS");
        User user2 = createUser("Fred Smith Name", "Fred Smith Desc", null, "Fred", "Smith", null);
        User user3 = createUser("M Foo", "Dr M Foo, BVSc MVS", null, null, null, null);
        Lookup shortFormat = createUserNameFormat(
                "NAME", "expr:ifempty(expr:concatIf($firstName, ' ', $lastName), $name)");
        Lookup mediumFormat = createUserNameFormat(
                "TITLE_NAME", "concat(\n" +
                              "     expr:concatIf($title,' '), \n" +
                              "     expr:ifempty(expr:concatIf($firstName, ' ', $lastName), $name))\n");
        Lookup longFormat = createUserNameFormat(
                "TITLE_NAME_QUALIFICATIONS", "concat(\n" +
                                             "     expr:concatIf($title,' '), \n" +
                                             "     expr:ifempty(expr:concatIf($firstName, ' ', $lastName), $name),\n" +
                                             "     expr:concatIf(', ', $qualifications))\n");

        IMObjectBean bean = getBean(practice);
        bean.setValue("shortUserNameFormat", shortFormat.getCode());
        bean.setValue("mediumUserNameFormat", mediumFormat.getCode());
        bean.setValue("longUserNameFormat", longFormat.getCode());
        checkFormat(user1, "short", "Jo Bloggs");
        checkFormat(user1, "medium", "Dr Jo Bloggs");
        checkFormat(user1, "long", "Dr Jo Bloggs, BVSc MVS");

        checkFormat(user2, "short", "Fred Smith");
        checkFormat(user2, "medium", "Fred Smith");
        checkFormat(user2, "long", "Fred Smith");

        checkFormat(user3, "short", "M Foo");
        checkFormat(user3, "medium", "M Foo");
        checkFormat(user3, "long", "M Foo");
    }

    /**
     * Verifies that the username node is defined as a variable.
     */
    @Test
    public void testUsername() {
        User user = userFactory.createUser();

        Lookup shortFormat = createUserNameFormat("USERNAME", "$username");
        IMObjectBean bean = getBean(practice);
        bean.setValue("shortUserNameFormat", shortFormat.getCode());
        checkFormat(user, "short", user.getUsername());
    }

    /**
     * Verifies that the description node is defined as a variable.
     */
    @Test
    public void testDescription() {
        User user = createUser("Dr Jo Bloggs Name", "Dr Jo Bloggs Desc, BVSc MVS", "Dr", "Jo", "Bloggs", "BVSc MVS");

        Lookup shortFormat = createUserNameFormat("DESCRIPTION", "$description");
        IMObjectBean bean = getBean(practice);
        bean.setValue("longUserNameFormat", shortFormat.getCode());
        checkFormat(user, "long", "Dr Jo Bloggs Desc, BVSc MVS");
    }

    /**
     * Tests the {@link UserFunctions#signature(Object)} method.
     */
    @Test
    public void testSignature() throws MalformedURLException {
        User user1 = userFactory.createUser();
        User user2 = userFactory.newUser().signature().build();

        JXPathContext context = JXPathHelper.newContext(new Object(), createFunctions(false));
        context.getVariables().declareVariable("user1", user1);
        context.getVariables().declareVariable("user2", user2);

        // invalid object
        assertNull(context.getValue("user:signature(.)"));

        // user with no signature
        assertNull(context.getValue("user:signature($user1)"));
        assertNull(context.getValue("user:signature(" + user1.getId() + ")"));

        // user with signature
        DocumentAct signature = userRules.getSignature(user2);
        assertNotNull(signature);
        URL expected = new URL(dir.getRoot().toURI().toURL(), signature.getId() + "_" + signature.getVersion() + "_"
                                                              + signature.getName());
        assertEquals(expected, context.getValue("user:signature($user2)"));
        assertEquals(expected, context.getValue("user:signature(" + user2.getId() + ")"));
    }

    /**
     * Verifies the {@link UserFunctions#format(User, String)} and {@link UserFunctions#formatById(long, String)}
     * return the expected values.
     *
     * @param user     the user
     * @param style    the format style
     * @param expected the expected result
     */
    private void checkFormat(User user, String style, String expected) {
        checkFormat(user, style, expected, false);
        checkFormat(user, style, expected, true);
    }

    /**
     * Verifies the {@link UserFunctions#format(User, String)} and {@link UserFunctions#formatById(long, String)}
     * return the expected values.
     *
     * @param user     the user
     * @param style    the format style
     * @param expected the expected result
     * @param cache    if {@code true}, use {@link CachingUserFunctions} otherwise use {@link UserFunctions}
     */
    private void checkFormat(User user, String style, String expected, boolean cache) {
        Functions functions = createFunctions(cache);

        // test the user based format
        JXPathContext context1 = JXPathHelper.newContext(user, functions);
        assertEquals(expected, context1.getValue("user:format(., '" + style + "')"));

        // test the id based format
        JXPathContext context2 = JXPathHelper.newContext(new Object(), functions);
        assertEquals(expected, context2.getValue("user:format(" + user.getId() + ", '" + style + "')"));
    }

    /**
     * Creates the JXPath functions.
     *
     * @param cache if {@code true}, cache objects
     * @return the functions
     */
    private Functions createFunctions(boolean cache) {
        FunctionLibrary library = new FunctionLibrary();
        ImageService imageService;
        try {
            imageService = new TestImageService(dir.getRoot(), documentHandlers, getArchetypeService());
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to create image service: " + exception.getMessage(), exception);
        }
        if (cache) {
            library.addFunctions(new CachingUserFunctions(userRules, getArchetypeService(), practiceService,
                                                          getLookupService(), imageService, library, 1024));
        } else {
            library.addFunctions(new UserFunctions(userRules, getArchetypeService(), practiceService,
                                                   getLookupService(), imageService, library));
        }
        library.addFunctions(new ExpressionFunctions("expr"));
        return library;
    }

    /**
     * Helper to create a new lookup.userNameFormat.
     *
     * @param code       the lookup code
     * @param expression the expression
     * @return a new lookup
     */
    private Lookup createUserNameFormat(String code, String expression) {
        Lookup lookup = TestHelper.getLookup("lookup.userNameFormat", code, false);
        IMObjectBean bean = getBean(lookup);
        bean.setValue("expression", expression);
        bean.save();
        return lookup;
    }

    /**
     * Helper to create a user.
     *
     * @param name           the name
     * @param description    the description
     * @param title          the title. May be {@code null}
     * @param firstName      the first name. May be {@code null}
     * @param lastName       the last name. May be {@code null}
     * @param qualifications the qualifications. May be {@code null}
     * @return a new user
     */
    private User createUser(String name, String description, String title, String firstName, String lastName,
                            String qualifications) {
        return userFactory.newUser()
                .title(title != null ? title.toUpperCase() : null)
                .name(name)
                .description(description)
                .firstName(firstName)
                .lastName(lastName)
                .qualifications(qualifications)
                .build();
    }

}
