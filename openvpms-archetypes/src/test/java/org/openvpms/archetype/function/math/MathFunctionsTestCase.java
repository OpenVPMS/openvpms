/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.math;

import org.apache.commons.jxpath.Functions;
import org.apache.commons.jxpath.JXPathContext;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.jxpath.ObjectFunctions;

import java.math.BigDecimal;

import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Tests the {@link MathFunctions} methods.
 *
 * @author Tim Anderson
 */
public class MathFunctionsTestCase {

    /**
     * The functions.
     */
    private final MathFunctions math = new MathFunctions();

    /**
     * JXPath context used to evaluate expressions.
     */
    private JXPathContext ctx;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // register the functions with JXPath.
        Functions functions = new ObjectFunctions(math, "math");
        ctx = JXPathHelper.newContext(new Object(), functions);
    }

    /**
     * Tests the {@link MathFunctions#roundAmount(BigDecimal)} method.
     * <p/>
     * Note: this currently rounds to 2 decimal places, using HALF_UP rounding convention.
     */
    @Test
    public void testRoundAmount() {
        checkEquals(new BigDecimal("12.12"), math.roundAmount(new BigDecimal("12.123")));
        checkEquals(new BigDecimal("1.11"), math.roundAmount(new BigDecimal("1.105")));
        checkEquals(new BigDecimal("1.15"), math.roundAmount(new BigDecimal("1.145")));
        checkEquals(new BigDecimal("1.14"), math.roundAmount(new BigDecimal("1.144")));

        // test evaluation using JXPath
        checkEquals(new BigDecimal("1.12"), (BigDecimal) ctx.getValue("math:roundAmount(1.124)"));
        checkEquals(new BigDecimal("1.13"), (BigDecimal) ctx.getValue("math:roundAmount(1.125)"));
    }

    /**
     * Tests the {@link MathFunctions#round(BigDecimal, int)} method.
     */
    @Test
    public void testRound() {
        checkEquals(new BigDecimal("12.12"), math.round(new BigDecimal("12.123"), 2));
        checkEquals(new BigDecimal("1.11"), math.round(new BigDecimal("1.105"), 2));
        checkEquals(new BigDecimal("1.15"), math.round(new BigDecimal("1.145"), 2));
        checkEquals(new BigDecimal("1.14"), math.round(new BigDecimal("1.144"), 2));

        // test evaluation using JXPath
        checkEquals(new BigDecimal("1.12"), (BigDecimal) ctx.getValue("math:round(1.124, 2)"));
        checkEquals(new BigDecimal("1.13"), (BigDecimal) ctx.getValue("math:round(1.125, 2)"));
        checkEquals(new BigDecimal("3.143"), (BigDecimal) ctx.getValue("math:round(22 div 7, 3)"));
    }

    /**
     * Tests the {@link MathFunctions#pow} method.
     */
    @Test
    public void testPow() {
        checkEquals(BigDecimal.valueOf(16), math.pow(BigDecimal.valueOf(2), 4));
        checkEquals(new BigDecimal("9.8596"), math.pow(BigDecimal.valueOf(3.14), 2));
        checkEquals(new BigDecimal("9.8596"), math.pow(BigDecimal.valueOf(3.14), 2));
        checkEquals(new BigDecimal("2"), math.pow(BigDecimal.valueOf(4), new BigDecimal("0.5")));

        // test evaluation using JXPath
        checkEquals(BigDecimal.valueOf(16), (BigDecimal) ctx.getValue("math:pow(2, 4)"));
        checkEquals(new BigDecimal("9.8596"), (BigDecimal) ctx.getValue("math:pow(3.14, 2)"));
        checkEquals(new BigDecimal("2"), (BigDecimal) ctx.getValue("math:pow(4, 0.5)"));
    }

}
