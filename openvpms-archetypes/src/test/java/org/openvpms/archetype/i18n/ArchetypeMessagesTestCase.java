/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.i18n;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.i18n.Message;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link ArchetypeMessages} class.
 *
 * @author Tim Anderson
 */
public class ArchetypeMessagesTestCase extends ArchetypeServiceTest {

    /**
     * Verify there are tests for each message.
     */
    @Test
    public void testCoverage() {
        for (Method method : ArchetypeMessages.class.getDeclaredMethods()) {
            if (Modifier.isPublic(method.getModifiers()) && Modifier.isStatic(method.getModifiers())) {
                String test = "test" + StringUtils.capitalize(method.getName());
                try {
                    ArchetypeMessagesTestCase.class.getDeclaredMethod(test);
                } catch (NoSuchMethodException exception) {
                    fail("Test needs to be expanded to include " + method.getName());
                }
            }
        }
    }
    
    /**
     * Tests the {@link ArchetypeMessages#priceCreatedByDelivery} method.
     */
    @Test
    public void testPriceCreatedByDelivery() {
        Party supplier = create(SupplierArchetypes.SUPPLIER_ORGANISATION, Party.class);
        supplier.setName("Cenvet");
        Act delivery = create(SupplierArchetypes.DELIVERY, Act.class);
        TestHelper.setId(delivery, 1005);
        delivery.setActivityStartTime(TestHelper.getDate("2020-02-17"));
        check("ARCH-1501: Price created by delivery 1005 from supplier Cenvet on 17/02/2020",
              ArchetypeMessages.priceCreatedByDelivery(delivery, supplier));
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordCannotContainWhitespace()} method.
     */
    @Test
    public void testPasswordCannotContainWhitespace() {
        assertEquals("Password cannot contain whitespace", ArchetypeMessages.passwordCannotContainWhitespace());
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordLessThanMinimumLength(int)} method.
     */
    @Test
    public void testPasswordLessThanMinimumLength() {
        assertEquals("Password must have at least 8 characters", ArchetypeMessages.passwordLessThanMinimumLength(8));
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordGreaterThanMaximumLength(int)} method.
     */
    @Test
    public void testPasswordGreaterThanMaximumLength() {
        assertEquals("Password must not be longer than 10 characters",
                     ArchetypeMessages.passwordGreaterThanMaximumLength(10));
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordMustContainLowercaseCharacter()} method.
     */
    @Test
    public void testPasswordMustContainLowercaseCharacter() {
        assertEquals("Password must have at least one lowercase character",
                     ArchetypeMessages.passwordMustContainLowercaseCharacter());
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordMustContainUppercaseCharacter()} method.
     */
    @Test
    public void testPasswordMustContainUppercaseCharacter() {
        assertEquals("Password must have at least one uppercase character",
                     ArchetypeMessages.passwordMustContainUppercaseCharacter());
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordMustContainNumber()} method.
     */
    @Test
    public void testPasswordMustContainNumber() {
        assertEquals("Password must have at least one number", ArchetypeMessages.passwordMustContainNumber());
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordMustContainSpecialCharacter()} method.
     */
    @Test
    public void testPasswordMustContainSpecialCharacter() {
        assertEquals("Password must have at least one special character",
                     ArchetypeMessages.passwordMustContainSpecialCharacter());
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordCharacterInvalid(char)} method.
     */
    @Test
    public void testPasswordCharacterInvalid() {
        assertEquals("' ' is not a valid password character", ArchetypeMessages.passwordCharacterInvalid(' '));
    }

    /**
     * Tests the {@link ArchetypeMessages#passwordCharacterInvalidSuffix()} method.
     */
    @Test
    public void testPasswordCharacterInvalidSuffix() {
        assertEquals("is not a valid password character", ArchetypeMessages.passwordCharacterInvalidSuffix());
    }

    /**
     * Tests the {@link ArchetypeMessages#newPasswordMustBeDifferentToOldPassword()}.
     */
    @Test
    public void testNewPasswordMustBeDifferentToOldPassword() {
        assertEquals("Password must be different to the old password",
                     ArchetypeMessages.newPasswordMustBeDifferentToOldPassword());
    }

    /**
     * Verifies a message matches that expected.
     *
     * @param expected the expected message
     * @param actual   the actual message
     */
    private void check(String expected, Message actual) {
        assertEquals(expected, actual.toString());
    }
}
