/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.tools.account;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.AbstractCustomerAccountTest;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceUpdater;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.math.MathRules.ONE_HUNDRED;


/**
 * Tests the {@link AccountBalanceTool} class.
 *
 * @author Tim Anderson
 */
public class AccountBalanceToolTestCase extends AbstractCustomerAccountTest {

    /**
     * The account balance tool.
     */
    private AccountBalanceTool tool;

    /**
     * The customer balance updater.
     */
    @Autowired
    private CustomerBalanceUpdater updater;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The archetype service. Does not trigger rules.
     */
    private IArchetypeService service;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        service = applicationContext.getBean("archetypeService", IArchetypeService.class);
        tool = new AccountBalanceTool(service, updater, transactionManager);
    }

    /**
     * Tests generation given a customer id.
     */
    @Test
    public void testGenerateForCustomerId() {
        Party customer = getCustomer();
        long id = customer.getId();
        BigDecimal amount = ONE_HUNDRED;
        FinancialAct debit = createInitialBalance(amount);
        service.save(debit);      // won't update balance as rules are not fired

        checkEquals(BigDecimal.ZERO, accountRules.getBalance(customer));
        assertFalse(tool.check(id));

        tool.generate(id);
        checkEquals(amount, accountRules.getBalance(customer));
        FinancialAct credit = createBadDebt(amount);
        service.save(credit);
        tool.generate(id);
        checkEquals(BigDecimal.ZERO, accountRules.getBalance(customer));
        assertTrue(tool.check(id));
    }

    /**
     * Tests generation by customer id, when the customer has a positive invoice.
     */
    @Test
    public void testGenerateForInvoice() {
        BigDecimal amount = new BigDecimal(100);
        checkGenerate(createChargesInvoice(amount), amount);
    }

    /**
     * Tests generation by customer id, when the customer has a negative invoice.
     */
    @Test
    public void testGenerateForNegativeInvoice() {
        BigDecimal amount = new BigDecimal(-100);
        checkGenerate(createChargesInvoice(amount), amount);
    }

    /**
     * Tests generation by customer id, when the customer has a positive credit.
     */
    @Test
    public void testGenerateForCredit() {
        BigDecimal amount = new BigDecimal(50);
        checkGenerate(createChargesCredit(amount), amount.negate());
    }

    /**
     * Tests generation by customer id, when the customer has a negative credit.
     */
    @Test
    public void testGenerateForNegativeCredit() {
        BigDecimal amount = new BigDecimal(50);
        checkGenerate(createChargesCredit(amount.negate()), amount);
    }

    /**
     * Tests generation given a customer name.
     */
    @Test
    public void testGenerateForCustomerName() {
        Party customer = getCustomer();
        String name = customer.getName();
        BigDecimal amount = ONE_HUNDRED;
        FinancialAct debit = createInitialBalance(amount);
        service.save(debit);        // won't update balance as rules are not fired

        checkEquals(BigDecimal.ZERO, accountRules.getBalance(customer));
        assertFalse(tool.check(name));

        tool.generate(name);
        checkEquals(amount, accountRules.getBalance(customer));
        FinancialAct credit = createBadDebt(amount);
        service.save(credit);
        tool.generate(name);
        checkEquals(BigDecimal.ZERO, accountRules.getBalance(customer));
        assertTrue(tool.check(name));
    }

    /**
     * Tests generation of a customer of a charge.
     *
     * @param charge  the charge acts
     * @param balance the expected balance
     */
    private void checkGenerate(List<FinancialAct> charge, BigDecimal balance) {
        Party customer = getCustomer();
        service.save(charge);

        checkEquals(BigDecimal.ZERO, accountRules.getBalance(customer));
        assertFalse(tool.check(customer.getId()));

        tool.generate(customer.getId());
        checkEquals(balance, accountRules.getBalance(customer));
    }
}
