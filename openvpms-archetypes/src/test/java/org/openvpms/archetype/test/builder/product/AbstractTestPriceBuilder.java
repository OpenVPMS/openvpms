/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>productPrice.*</em> archetypes, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestPriceBuilder<P extends TestProductBuilder<P>, B extends AbstractTestPriceBuilder<P, B>>
        extends AbstractTestIMObjectBuilder<ProductPrice, B> {

    /**
     * The parent product builder.
     */
    private final P parent;

    /**
     * The cost.
     */
    private ValueStrategy cost = ValueStrategy.unset();

    /**
     * The markup.
     */
    private ValueStrategy markup = ValueStrategy.unset();

    /**
     * The price.
     */
    private ValueStrategy price = ValueStrategy.unset();

    /**
     * The maximum discount.
     */
    private ValueStrategy maxDiscount = ValueStrategy.unset();

    /**
     * The from-date.
     */
    private ValueStrategy fromDate = ValueStrategy.unset();

    /**
     * The to-date.
     */
    private ValueStrategy toDate = ValueStrategy.unset();

    /**
     * The notes.
     */
    private ValueStrategy notes = ValueStrategy.unset();

    /**
     * The pricing groups.
     */
    private Lookup[] pricingGroups;

    /**
     * Constructs an {@link AbstractTestPriceBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestPriceBuilder(ProductPrice object, ArchetypeService service) {
        super(object, service);
        this.parent = null;
    }

    /**
     * Constructs an {@link AbstractTestPriceBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestPriceBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, ProductPrice.class, service);
        this.parent = parent;
    }

    /**
     * Sets the cost.
     *
     * @param cost the cost
     * @return this
     */
    public B cost(int cost) {
        return cost(BigDecimal.valueOf(cost));
    }

    /**
     * Sets the cost.
     *
     * @param cost the cost
     * @return this
     */
    public B cost(String cost) {
        return cost(cost != null ? new BigDecimal(cost) : null);
    }

    /**
     * Sets the cost.
     *
     * @param cost the cost
     * @return this
     */
    public B cost(BigDecimal cost) {
        this.cost = ValueStrategy.value(cost);
        return getThis();
    }

    /**
     * Sets the markup.
     *
     * @param markup the markup
     * @return this
     */
    public B markup(int markup) {
        return markup(BigDecimal.valueOf(markup));
    }

    /**
     * Sets the markup.
     *
     * @param markup the markup
     * @return this
     */
    public B markup(String markup) {
        return markup(markup != null ? new BigDecimal(markup) : null);
    }

    /**
     * Sets the markup.
     *
     * @param markup the markup
     * @return this
     */
    public B markup(BigDecimal markup) {
        this.markup = ValueStrategy.value(markup);
        return getThis();
    }

    /**
     * Sets the cost and price and calculates the markup.
     *
     * @param cost  the cost
     * @param price the price
     * @return this
     */
    public B costAndPrice(int cost, int price) {
        return costAndPrice(BigDecimal.valueOf(cost), BigDecimal.valueOf(price));
    }

    /**
     * Sets the cost and price and calculates the markup.
     *
     * @param cost  the cost
     * @param price the price
     * @return this
     */
    public B costAndPrice(int cost, String price) {
        return costAndPrice(BigDecimal.valueOf(cost), new BigDecimal(price));
    }

    /**
     * Sets the cost and price and calculates the markup.
     *
     * @param cost  the cost
     * @param price the price
     * @return this
     */
    public B costAndPrice(BigDecimal cost, BigDecimal price) {
        cost(cost);
        price(price);
        return markup(new ProductPriceRules(getService()).getMarkup(cost, price));
    }

    /**
     * Sets the price.
     *
     * @param price the price
     * @return this
     */
    public B price(int price) {
        return price(BigDecimal.valueOf(price));
    }

    /**
     * Sets the price.
     *
     * @param price the price
     * @return this
     */
    public B price(String price) {
        return price(new BigDecimal(price));
    }

    /**
     * Sets the price.
     *
     * @param price the price
     * @return this
     */
    public B price(BigDecimal price) {
        this.price = ValueStrategy.value(price);
        return getThis();
    }

    /**
     * Sets the cost and markup and calculates the price.
     *
     * @param cost   the cost price
     * @param markup the markup
     * @return this
     */
    public B costAndMarkup(int cost, int markup) {
        return costAndMarkup(BigDecimal.valueOf(cost), BigDecimal.valueOf(markup));
    }

    /**
     * Sets the cost and markup and calculates the price.
     *
     * @param cost   the cost price
     * @param markup the markup
     * @return this
     */
    public B costAndMarkup(BigDecimal cost, BigDecimal markup) {
        cost(cost);
        markup(markup);
        return price(new ProductPriceRules(getService()).getTaxExPrice(cost, markup));
    }

    /**
     * Sets the maximum discount.
     *
     * @param maxDiscount the maximum discount
     * @return this
     */
    public B maxDiscount(int maxDiscount) {
        return maxDiscount(BigDecimal.valueOf(maxDiscount));
    }

    /**
     * Sets the maximum discount.
     *
     * @param maxDiscount the maximum discount
     * @return this
     */
    public B maxDiscount(String maxDiscount) {
        return maxDiscount(maxDiscount != null ? new BigDecimal(maxDiscount) : null);
    }

    /**
     * Sets the maximum discount.
     *
     * @param maxDiscount the maximum discount
     * @return this
     */
    public B maxDiscount(BigDecimal maxDiscount) {
        this.maxDiscount = ValueStrategy.value(maxDiscount);
        return getThis();
    }

    /**
     * Sets the from-date.
     *
     * @param fromDate the from-date. May be {@code null} indicating an empty lower bound
     * @return this
     */
    public B fromDate(Date fromDate) {
        this.fromDate = ValueStrategy.value(fromDate);
        return getThis();
    }

    /**
     * Sets the to-date.
     *
     * @param toDate the to-date. May be {@code null} indicating an empty upper bound
     * @return this
     */
    public B toDate(Date toDate) {
        this.toDate = ValueStrategy.value(toDate);
        return getThis();
    }

    /**
     * Makes the price active over all dates.
     *
     * @return this
     */
    public B allDates() {
        return dateRange((Date) null, null);
    }

    /**
     * Sets the from and to dates.
     *
     * @param fromDate the from-date. May be {@code null} indicating an empty lower bound
     * @param toDate   the to-date. May be {@code null} indicating an empty upper bound
     * @return this
     */
    public B dateRange(String fromDate, String toDate) {
        return dateRange(parseDate(fromDate), parseDate(toDate));
    }

    /**
     * Sets the from and to dates.
     *
     * @param fromDate the from-date. May be {@code null} indicating an empty lower bound
     * @param toDate   the to-date. May be {@code null} indicating an empty upper bound
     * @return this
     */
    public B dateRange(Date fromDate, Date toDate) {
        fromDate(fromDate);
        return toDate(toDate);
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public B notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return getThis();
    }

    /**
     * Sets the pricing groups.
     *
     * @param pricingGroups the pricing groups
     * @return this
     */
    public B pricingGroups(String... pricingGroups) {
        Lookup[] lookups = new Lookup[pricingGroups.length];
        if (pricingGroups.length != 0) {
            TestLookupBuilder lookupBuilder = new TestLookupBuilder(ProductArchetypes.PRICING_GROUP, getService());
            for (int i = 0; i < pricingGroups.length; ++i) {
                for (String pricingGroup : pricingGroups) {
                    lookups[i] = lookupBuilder.code(pricingGroup).build();
                }
            }
        }
        return pricingGroups(lookups);
    }

    /**
     * Sets the pricing groups.
     *
     * @param pricingGroups the pricing groups
     * @return this
     */
    public B pricingGroups(Lookup... pricingGroups) {
        this.pricingGroups = pricingGroups;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public ProductPrice build() {
        return build(false);
    }

    /**
     * Adds the price to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        ProductPrice price = build();
        parent.addPrice(price);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(ProductPrice object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        price.setValue(bean, "price");
        cost.setValue(bean, "cost");
        markup.setValue(bean, "markup");
        maxDiscount.setValue(bean, "maxDiscount");
        fromDate.setValue(bean, "fromDate");
        toDate.setValue(bean, "toDate");
        notes.setValue(bean, "notes");
        if (pricingGroups != null && pricingGroups.length != 0) {
            bean.removeValues("pricingGroups");
            for (Lookup pricingGroup : pricingGroups) {
                object.addClassification(pricingGroup);
            }
        }
        super.build(object, bean, toSave, toRemove);
    }
}