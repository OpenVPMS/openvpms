/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.hl7;

import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A builder for <em>entity.HL7ReceiverMLLP</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestHL7ReceiverBuilder extends TestHL7ConnectorBuilder<TestHL7ReceiverBuilder> {

    /**
     * Constructs a {@link TestHL7ReceiverBuilder}.
     *
     * @param service the archetype service
     */
    public TestHL7ReceiverBuilder(ArchetypeService service) {
        super("entity.HL7ReceiverMLLP", service);
        name(ValueStrategy.random("zreceiver"));
    }
}