/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.eft;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.EFTPOS*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestEFTPOSTransactionBuilder<B extends AbstractTestEFTPOSTransactionBuilder<B>>
        extends AbstractTestActBuilder<FinancialAct, B> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The transaction amount,
     */
    private BigDecimal amount;

    /**
     * The terminal.
     */
    private Entity terminal;

    /**
     * The practice location.
     */
    private Entity location;

    /**
     * The receipts.
     */
    private final List<ReceiptState> receipts = new ArrayList<>();

    /**
     * Constructs an {@link AbstractTestEFTPOSTransactionBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestEFTPOSTransactionBuilder(String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     */
    public B customer(Party customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     * @return this
     */
    public B amount(BigDecimal amount) {
        this.amount = amount;
        return getThis();
    }

    /**
     * Sets the terminal.
     *
     * @param terminal the terminal
     * @return this
     */
    public B terminal(Entity terminal) {
        this.terminal = terminal;
        return getThis();
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public B location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Adds a merchant receipt.
     *
     * @param receipt           the merchant receipt. May be {@code null}
     * @param signatureRequired if {@code true} a signature is required
     * @return this
     */
    public B addMerchantReceipt(String receipt, boolean signatureRequired) {
        if (receipt != null) {
            receipts.add(new ReceiptState(receipt, false, signatureRequired));
        }
        return getThis();
    }

    /**
     * Adds a customer receipt.
     *
     * @param receipt the customer receipt. May be {@code null}
     * @return this
     */
    public B addCustomerReceipt(String receipt) {
        if (receipt != null) {
            receipts.add(new ReceiptState(receipt, true, false));
        }
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (amount != null) {
            bean.setValue("amount", amount);
        }
        if (terminal != null) {
            bean.setTarget("terminal", terminal);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (!receipts.isEmpty()) {
            int sequence = 0;
            for (ActRelationship relationship : bean.getValues("receipts", ActRelationship.class)) {
                if (relationship.getSequence() >= sequence) {
                    sequence = relationship.getSequence() + 1;
                }
            }
            for (ReceiptState receipt : receipts) {
                String archetype = receipt.customer ? EFTPOSArchetypes.CUSTOMER_RECEIPT
                                                    : EFTPOSArchetypes.MERCHANT_RECEIPT;
                DocumentAct act = create(archetype, DocumentAct.class);
                toSave.add(act);
                IMObjectBean receiptBean = getBean(act);

                if (receipt.text.length() > receiptBean.getMaxLength("receipt")) {
                    TextDocumentHandler handler = new TextDocumentHandler(getService());
                    Document document = handler.create(receiptBean.getDisplayName(), receipt.text);
                    act.setDocument(document.getObjectReference());
                    toSave.add(document);
                } else {
                    receiptBean.setValue("receipt", receipt.text);
                }
                if (!receipt.customer) {
                    receiptBean.setValue("signatureRequired", receipt.signatureRequired);
                }
                ActRelationship relationship = (ActRelationship) bean.addTarget("receipts", act, "transaction");
                relationship.setSequence(sequence);
            }
            receipts.clear();
        }
    }

    private static class ReceiptState {

        private final String text;

        private final boolean customer;

        private final boolean signatureRequired;

        public ReceiptState(String text, boolean customer, boolean signatureRequired) {
            this.text = text;
            this.customer = customer;
            this.signatureRequired = signatureRequired;
        }
    }
}
