/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Verifies an estimate item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestEstimateItemVerifier {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected patient.
     */
    private Reference patient;

    /**
     * The expected product.
     */
    private Reference product;

    /**
     * The expected minimum quantity.
     */
    private BigDecimal minimumQuantity = BigDecimal.ZERO;

    /**
     * The expected low quantity.
     */
    private BigDecimal lowQuantity = BigDecimal.ONE;

    /**
     * The expected high quantity.
     */
    private BigDecimal highQuantity = BigDecimal.ONE;

    /**
     * The expected fixed price.
     */
    private BigDecimal fixedPrice = BigDecimal.ZERO;

    /**
     * The expected low unit price.
     */
    private BigDecimal lowUnitPrice = BigDecimal.ZERO;

    /**
     * The expected high unit price.
     */
    private BigDecimal highUnitPrice = BigDecimal.ZERO;

    /**
     * The expected low discount.
     */
    private BigDecimal lowDiscount = BigDecimal.ZERO;

    /**
     * The expected high discount.
     */
    private BigDecimal highDiscount = BigDecimal.ZERO;

    /**
     * The expected print flag.
     */
    private boolean print = true;

    /**
     * The expected low total.
     */
    private BigDecimal lowTotal = BigDecimal.ZERO;

    /**
     * The expected high total.
     */
    private BigDecimal highTotal = BigDecimal.ZERO;

    /**
     * The expected product template.
     */
    private Reference template;

    /**
     * The expected product template expansion group.
     */
    private Integer group;

    /**
     * The expected user that created the estimate.
     */
    private Reference createdBy;

    /**
     * Constructs a {@link TestEstimateItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestEstimateItemVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public TestEstimateItemVerifier patient(Party patient) {
        this.patient = (patient != null) ? patient.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected product.
     *
     * @param product the expected product
     * @return this
     */
    public TestEstimateItemVerifier product(Product product) {
        this.product = (product != null) ? product.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected low quantity.
     *
     * @param lowQuantity the expected low quantity
     * @return this
     */
    public TestEstimateItemVerifier lowQuantity(int lowQuantity) {
        return lowQuantity(BigDecimal.valueOf(lowQuantity));
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minimumQuantity the expected minimum quantity
     * @return this
     */
    public TestEstimateItemVerifier minimumQuantity(int minimumQuantity) {
        return minimumQuantity(BigDecimal.valueOf(minimumQuantity));
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minimumQuantity the expected minimum quantity
     * @return this
     */
    public TestEstimateItemVerifier minimumQuantity(BigDecimal minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
        return this;
    }

    /**
     * Sets the expected low quantity.
     *
     * @param lowQuantity the expected low quantity
     * @return this
     */
    public TestEstimateItemVerifier lowQuantity(BigDecimal lowQuantity) {
        this.lowQuantity = lowQuantity;
        return this;
    }

    /**
     * Sets the expected high quantity.
     *
     * @param highQuantity the expected high quantity
     * @return this
     */
    public TestEstimateItemVerifier highQuantity(int highQuantity) {
        return highQuantity(BigDecimal.valueOf(highQuantity));
    }

    /**
     * Sets the expected high quantity.
     *
     * @param highQuantity the expected high quantity
     * @return this
     */
    public TestEstimateItemVerifier highQuantity(BigDecimal highQuantity) {
        this.highQuantity = highQuantity;
        return this;
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestEstimateItemVerifier fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestEstimateItemVerifier fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
        return this;
    }

    /**
     * Sets the expected low unit price.
     *
     * @param lowUnitPrice the expected low unit price
     * @return this
     */
    public TestEstimateItemVerifier lowUnitPrice(int lowUnitPrice) {
        return lowUnitPrice(BigDecimal.valueOf(lowUnitPrice));
    }

    /**
     * Sets the expected low unit price.
     *
     * @param lowUnitPrice the expected low unit price
     * @return this
     */
    public TestEstimateItemVerifier lowUnitPrice(BigDecimal lowUnitPrice) {
        this.lowUnitPrice = lowUnitPrice;
        return this;
    }

    /**
     * Sets the expected high unit price.
     *
     * @param highUnitPrice the expected high unit price
     * @return this
     */
    public TestEstimateItemVerifier highUnitPrice(int highUnitPrice) {
        return highUnitPrice(BigDecimal.valueOf(highUnitPrice));
    }

    /**
     * Sets the expected high unit price.
     *
     * @param highUnitPrice the expected high unit price
     * @return this
     */
    public TestEstimateItemVerifier highUnitPrice(BigDecimal highUnitPrice) {
        this.highUnitPrice = highUnitPrice;
        return this;
    }

    /**
     * Sets the expected low discount.
     *
     * @param lowDiscount the expected low discount
     * @return this
     */
    public TestEstimateItemVerifier lowDiscount(int lowDiscount) {
        return lowDiscount(BigDecimal.valueOf(lowDiscount));
    }

    /**
     * Sets the expected low discount.
     *
     * @param lowDiscount the expected low discount
     * @return this
     */
    public TestEstimateItemVerifier lowDiscount(BigDecimal lowDiscount) {
        this.lowDiscount = lowDiscount;
        return this;
    }

    /**
     * Sets the expected high discount.
     *
     * @param highDiscount the expected high discount
     * @return this
     */
    public TestEstimateItemVerifier highDiscount(int highDiscount) {
        return highDiscount(BigDecimal.valueOf(highDiscount));
    }

    /**
     * Sets the expected high discount.
     *
     * @param highDiscount the expected high discount
     * @return this
     */
    public TestEstimateItemVerifier highDiscount(BigDecimal highDiscount) {
        this.highDiscount = highDiscount;
        return this;
    }

    /**
     * Sets the expected print flag.
     *
     * @param print the expected print flag
     */
    public TestEstimateItemVerifier print(boolean print) {
        this.print = print;
        return this;
    }

    /**
     * Sets the expected low total.
     *
     * @param lowTotal the expected low total
     * @return this
     */
    public TestEstimateItemVerifier lowTotal(int lowTotal) {
        return lowTotal(BigDecimal.valueOf(lowTotal));
    }

    /**
     * Sets the expected low total.
     *
     * @param lowTotal the expected low total
     * @return this
     */
    public TestEstimateItemVerifier lowTotal(BigDecimal lowTotal) {
        this.lowTotal = lowTotal;
        return this;
    }

    /**
     * Sets the expected high total.
     *
     * @param highTotal the expected high total
     * @return this
     */
    public TestEstimateItemVerifier highTotal(int highTotal) {
        return highTotal(BigDecimal.valueOf(highTotal));
    }

    /**
     * Sets the expected high total.
     *
     * @param highTotal the expected high total
     * @return this
     */
    public TestEstimateItemVerifier highTotal(BigDecimal highTotal) {
        this.highTotal = highTotal;
        return this;
    }

    /**
     * Sets the expected product template.
     *
     * @param template the template. May be {@code null}
     * @return this
     */
    public TestEstimateItemVerifier template(Product template) {
        this.template = (template != null) ? template.getObjectReference() : null;
        if (template == null) {
            group = null;
        }
        return this;
    }

    /**
     * Sets the expected template expansion group.
     *
     * @param group the expected template expansion group
     * @return this
     */
    public TestEstimateItemVerifier group(int group) {
        this.group = group;
        return this;
    }

    /**
     * Sets the expected created-by user.
     *
     * @param createdBy the expected created-by user
     * @return this
     */
    public TestEstimateItemVerifier createdBy(User createdBy) {
        this.createdBy = (createdBy != null) ? createdBy.getObjectReference() : null;
        return this;
    }

    /**
     * Verifies that the estimate item matches that expected.
     *
     * @param item the estimate item
     */
    public void verify(Act item) {
        IMObjectBean bean = service.getBean(item);
        assertEquals(patient, bean.getTargetRef("patient"));
        assertEquals(product, bean.getTargetRef("product"));
        assertEquals(template, bean.getTargetRef("template"));
        checkEquals(minimumQuantity, bean.getBigDecimal("minQuantity"));
        checkEquals(lowQuantity, bean.getBigDecimal("lowQty"));
        checkEquals(highQuantity, bean.getBigDecimal("highQty"));
        checkEquals(fixedPrice, bean.getBigDecimal("fixedPrice"));
        checkEquals(lowUnitPrice, bean.getBigDecimal("lowUnitPrice"));
        checkEquals(highUnitPrice, bean.getBigDecimal("highUnitPrice"));
        checkEquals(lowDiscount, bean.getBigDecimal("lowDiscount"));
        checkEquals(highDiscount, bean.getBigDecimal("highDiscount"));
        assertEquals(print, bean.getBoolean("print"));
        checkEquals(lowTotal, bean.getBigDecimal("lowTotal"));
        checkEquals(highTotal, bean.getBigDecimal("highTotal"));
        Participation participation = bean.getObject("template", Participation.class);
        if (participation == null) {
            assertNull(group);
        } else {
            IMObjectBean participationBean = service.getBean(participation);
            assertEquals(group, participationBean.getValue("group"));
        }
        assertEquals(createdBy, item.getCreatedBy());
    }

    /**
     * Verifies there is an estimate item that matches the criteria.
     *
     * @param items the estimate items
     */
    public void verify(List<Act> items) {
        Act item = FinancialTestHelper.find(items, patient, product, null);
        if (item == null) {
            fail("Failed to find estimate item for patient=" + patient + ", product=" + product);
        }
        verify(item);
    }
}
