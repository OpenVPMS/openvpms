/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.object;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.entity.TestEntityIdentityBuilder;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.singleton.SingletonQuery;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * A builder of {@link IMObject} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestIMObjectBuilder<T extends IMObject, B extends AbstractTestIMObjectBuilder<T, B>> {

    /**
     * The object to update.
     */
    private final T existing;

    /**
     * The archetype to build.
     */
    private final String archetype;

    /**
     * The type.
     */
    private final Class<T> type;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Collects objects to be saved.
     */
    private final Set<IMObject> collector = new LinkedHashSet<>();

    /**
     * The name value strategy;
     */
    private ValueStrategy name = ValueStrategy.unset();

    /**
     * The description value strategy.
     */
    private ValueStrategy description = ValueStrategy.unset();

    /**
     * Determines if the object is active or not.
     */
    private Boolean active;

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param type    the type
     * @param service the archetype service
     */
    public AbstractTestIMObjectBuilder(Class<T> type, ArchetypeService service) {
        this(null, null, type, service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestIMObjectBuilder(String archetype, Class<T> type, ArchetypeService service) {
        this(null, archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    @SuppressWarnings("unchecked")
    public AbstractTestIMObjectBuilder(T object, ArchetypeService service) {
        this(object, object.getArchetype(), (Class<T>) object.getClass(), service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param object    the object to update. May be {@code null}
     * @param archetype the archetype. May be {@code null}
     * @param type      the type
     * @param service   the archetype service
     */
    private AbstractTestIMObjectBuilder(T object, String archetype, Class<T> type, ArchetypeService service) {
        this.existing = object;
        this.archetype = archetype;
        this.type = type;
        this.service = service;
    }

    /**
     * Sets the object name.
     *
     * @param name the name
     * @return this
     */
    public B name(String name) {
        return name(ValueStrategy.value(name));
    }

    /**
     * Sets the object name.
     *
     * @param name the name
     * @return this
     */
    public B name(ValueStrategy name) {
        this.name = name;
        return getThis();
    }

    /**
     * Sets the object description.
     *
     * @param description the description
     * @return this
     */
    public B description(String description) {
        return description(ValueStrategy.value(description));
    }

    /**
     * Sets the object description.
     *
     * @param description the description
     * @return this
     */
    public B description(ValueStrategy description) {
        this.description = description;
        return getThis();
    }

    /**
     * Determines if the object is active or not.
     *
     * @param active if {@code true} the object is active, otherwise it is inactive
     * @return this
     */
    public B active(boolean active) {
        this.active = active;
        return getThis();
    }

    /**
     * Collect objects that need to be saved when this is saved.
     * <p/>
     * This can be used by child builders to pass built objects back.
     */
    public B collect(Collection<IMObject> objects) {
        collector.addAll(objects);
        return getThis();
    }

    /**
     * Builds the object.
     * <p/>
     * This implementation saves it.
     *
     * @return the object
     */
    public T build() {
        return build(true);
    }

    /**
     * Builds the object.
     * <p/>
     * NOTE: if {@code save == false}, any persistent objects requiring removal will not be deleted.
     *
     * @param save if {@code true}, save the object, and any related objects
     * @return the entity
     */
    public T build(boolean save) {
        Set<IMObject> toSave = new LinkedHashSet<>();
        Set<Reference> remove = new LinkedHashSet<>();
        T entity = build(toSave, remove);
        if (save) {
            service.save(toSave);
            // remove objects after save, to avoid integrity violations
            for (Reference reference : remove) {
                service.remove(reference);
            }
        } else {
            for (IMObject object : toSave) {
                service.deriveValues(object);
            }
        }
        return entity;
    }

    /**
     * Builds the object, collecting the objects built without saving them.
     *
     * @param objects collects the built objects
     * @param remove  collects the references to objects to remove
     * @return the primary object
     */
    public T build(Set<IMObject> objects, Set<Reference> remove) {
        T entity = getObject(archetype);
        IMObjectBean bean = service.getBean(entity);
        build(entity, bean, objects, remove);
        objects.add(entity);
        if (!collector.isEmpty()) {
            objects.addAll(collector);
            collector.clear();
        }
        return entity;
    }

    /**
     * Returns the object to build.
     * <p/>
     * This implementation returns the existing instance, if one was supplied at construction and matches
     * the specified archetype else it returns a new instance.
     * <p/>
     * For builders that require unique instances, it may return an existing instance.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    protected T getObject(String archetype) {
        T result;
        if (existing != null && existing.isA(archetype)) {
            result = existing;
        } else {
            result = service.create(archetype, type);
        }
        return result;
    }

    /**
     * Returns the existing object, if it is being updated.
     *
     * @return the existing object, or {@code null} if a new object is being created
     */
    protected T getExisting() {
        return existing;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        name.setValue(bean, "name");
        description.setValue(bean, "description");
        if (active != null) {
            object.setActive(active);
        }
    }

    /**
     * Returns the name value strategy.
     *
     * @return the name value strategy
     */
    protected ValueStrategy getName() {
        return name;
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected B getThis() {
        return (B) this;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Helper to create an object.
     *
     * @param archetype the archetype
     * @param type      the expected type of the object
     */
    protected <O extends IMObject> O create(String archetype, Class<O> type) {
        return service.create(archetype, type);
    }

    /**
     * Returns the singleton instance of an object, or creates a new one if none exists.
     *
     * @param archetype the archetype
     * @param type      the expected type of the object
     * @return the object
     */
    protected <O extends IMObject> O getSingleton(String archetype, Class<O> type) {
        O result = new SingletonQuery(getService()).get(archetype, type);
        if (result == null) {
            result = service.create(archetype, type);
        }
        return result;
    }

    /**
     * Helper to create an {@link EntityIdentity}.
     *
     * @param archetype the identity archetype
     * @param id        the identity
     * @return a new entity identity
     */
    protected EntityIdentity createEntityIdentity(String archetype, ValueStrategy id) {
        return newEntityIdentity(archetype)
                .identity(id)
                .build();
    }

    /**
     * Returns a builder to create an {@link EntityIdentity}.
     *
     * @param archetype the identity archetype
     * @return a new builder
     */
    protected TestEntityIdentityBuilder newEntityIdentity(String archetype) {
        return new TestEntityIdentityBuilder(archetype, service);
    }

    /**
     * Helper to create an {@link ActIdentity}.
     *
     * @param archetype the identity archetype
     * @param id        the identity
     * @return a new act identity
     */
    protected ActIdentity createActIdentity(String archetype, ValueStrategy id) {
        ActIdentity identity = create(archetype, ActIdentity.class);
        id.setValue(getBean(identity), "identity");
        return identity;
    }

    /**
     * Returns the supported archetypes of a node.
     *
     * @param node the node
     * @return the supported archetypes
     */
    protected String[] getArchetypeRange(String node) {
        ArchetypeDescriptor archetypeDescriptor = DescriptorHelper.getArchetypeDescriptor(archetype, service);
        assertNotNull(archetypeDescriptor);
        NodeDescriptor nodeDescriptor = archetypeDescriptor.getNodeDescriptor(node);
        assertNotNull(nodeDescriptor);
        return DescriptorHelper.getShortNames(nodeDescriptor, service);
    }

    /**
     * Returns the archetype of a node.
     *
     * @param node the node
     * @return the archetype
     */
    protected String getNodeArchetype(String node) {
        String[] range = getArchetypeRange(node);
        if (range.length != 1) {
            throw new IllegalStateException("Archetype range must return 1 archetype for node=" + node
                                            + " of archetype=" + archetype + " but returned: "
                                            + ArrayUtils.toString(range));
        }
        return range[0];
    }

    /**
     * Parses a date or date time.
     *
     * @param value the value. May be {@code null}
     * @return the corresponding date. May be {@code null}
     */
    protected Date parseDate(String value) {
        return TestHelper.parseDate(value);
    }

    /**
     * Returns the next sequence for a {@link SequencedRelationship} node.
     *
     * @param bean the bean
     * @param node the node name
     * @return the next sequence
     */
    protected int getNextSequence(IMObjectBean bean, String node) {
        int sequence = 0;
        for (SequencedRelationship relationship : bean.getValues(node, SequencedRelationship.class)) {
            if (relationship.getSequence() >= sequence) {
                sequence = relationship.getSequence() + 1;
            }
        }
        return sequence;
    }

}
