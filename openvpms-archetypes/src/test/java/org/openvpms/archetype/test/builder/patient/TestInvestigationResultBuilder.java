/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.act.LongTextBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder of <em>act.patientInvestigationResultItem</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestInvestigationResultBuilder extends AbstractTestActBuilder<Act, TestInvestigationResultBuilder> {

    /**
     * The parent builder.
     */
    private final TestInvestigationResultsBuilder parent;

    /**
     * The result builder.
     */
    private final LongTextBuilder resultBuilder;

    /**
     * The notes builder.
     */
    private final LongTextBuilder notesBuilder;

    /**
     * The result id.
     */
    private ValueStrategy resultId = ValueStrategy.unset();

    /**
     * The result.
     */
    private ValueStrategy result = ValueStrategy.unset();

    /**
     * The notes.
     */
    private ValueStrategy notes = ValueStrategy.unset();

    /**
     * The image.
     */
    private ValueStrategy image = ValueStrategy.unset();

    /**
     * Constructs a {@link TestInvestigationResultBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestInvestigationResultBuilder(TestInvestigationResultsBuilder parent, ArchetypeService service) {
        super(InvestigationArchetypes.RESULT, Act.class, service);
        this.parent = parent;
        resultBuilder = new LongTextBuilder("result", "longResult", service);
        notesBuilder = new LongTextBuilder("notes", "longNotes", service);
    }

    /**
     * Sets the result id.
     *
     * @param resultId the result id
     * @return this
     */
    public TestInvestigationResultBuilder resultId(String resultId) {
        this.resultId = ValueStrategy.value(resultId);
        return this;
    }

    /**
     * Sets the result.
     *
     * @param result the result
     * @return this
     */
    public TestInvestigationResultBuilder result(String result) {
        this.result = ValueStrategy.value(result);
        return this;
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public TestInvestigationResultBuilder notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return this;
    }

    /**
     * Sets the image.
     *
     * @param image the image
     * @return this
     */
    public TestInvestigationResultBuilder image(Document image) {
        this.image = ValueStrategy.value(image);
        return this;
    }

    /**
     * Adds the result to the parent results.
     *
     * @return the parent results builder
     */
    public TestInvestigationResultsBuilder add() {
        parent.addResult(this);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        resultId.setValue(bean, "resultId");
        resultBuilder.build(result, bean, toSave, toRemove);
        notesBuilder.build(notes, bean, toSave, toRemove);
        if (image.isSet()) {
            DocumentAct imageAct = bean.getTarget("image", DocumentAct.class);
            if (imageAct != null && imageAct.getDocument() != null) {
                toRemove.add(imageAct.getObjectReference());
            }
            Document newImage = (Document) image.getValue();
            if (newImage == null) {
                if (imageAct != null) {
                    toRemove.add(imageAct.getObjectReference());
                }
            } else {
                toSave.add(newImage);
                if (imageAct == null) {
                    imageAct = create(InvestigationArchetypes.RESULT_IMAGE, DocumentAct.class);
                    bean.addTarget("image", imageAct, "parent");
                    toSave.add(imageAct);
                }
                imageAct.setDocument(newImage.getObjectReference());
            }
        }
    }
}