/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.test.builder.act.AbstractTestActVerifier;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.junit.Assert.assertEquals;

/**
 * Verifies a task matches that expected.
 *
 * @author Tim Anderson
 */
public class TestTaskVerifier extends AbstractTestActVerifier<Act, TestTaskVerifier> {

    /**
     * The expected work list.
     */
    private Reference workList;

    /**
     * The expected task type.
     */
    private Reference taskType;

    /**
     * The expected customer.
     */
    private Reference customer;

    /**
     * The expected patient.
     */
    private Reference patient;

    /**
     * The expected notes.
     */
    private ValueStrategy notes = ValueStrategy.unset();

    /**
     * Constructs a {@link TestTaskVerifier}.
     *
     * @param service the archetype service
     */
    public TestTaskVerifier(ArchetypeService service) {
        super(service);
        archetype(ScheduleArchetypes.TASK);
        status(TaskStatus.PENDING);
    }

    /**
     * Sets the expected work list.
     *
     * @param workList the expected work list
     * @return this
     */
    public TestTaskVerifier worklist(Entity workList) {
        this.workList = getReference(workList);
        return this;
    }

    /**
     * Sets the expected task type.
     *
     * @param taskType the expected task type
     * @return this
     */
    public TestTaskVerifier taskType(Entity taskType) {
        this.taskType = getReference(taskType);
        return this;
    }

    /**
     * Sets the expected customer.
     *
     * @param customer the expected customer
     * @return this
     */
    public TestTaskVerifier customer(Party customer) {
        this.customer = getReference(customer);
        return this;
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public TestTaskVerifier patient(Party patient) {
        this.patient = getReference(patient);
        return this;
    }

    /**
     * Sets the expected notes.
     *
     * @param notes the expected notes
     * @return this
     */
    public TestTaskVerifier notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return this;
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(Act object, IMObjectBean bean) {
        super.verify(object, bean);
        assertEquals(workList, bean.getTargetRef("worklist"));
        assertEquals(taskType, bean.getTargetRef("taskType"));
        assertEquals(customer, bean.getTargetRef("customer"));
        assertEquals(patient, bean.getTargetRef("patient"));
        checkEquals(notes, bean.getString("notes"));
    }
}
