/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.appointmentType</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAppointmentTypeBuilder extends AbstractTestEntityBuilder<Entity, TestAppointmentTypeBuilder> {

    /**
     * The default appointment reason.
     */
    private ValueStrategy reason = ValueStrategy.unset();

    /**
     * Determines if online booking is enabled.
     */
    private ValueStrategy onlineBooking = ValueStrategy.unset();

    /**
     * Determines if reminders should be sent.
     */
    private ValueStrategy sendReminders = ValueStrategy.unset();

    /**
     * Constructs a {@link TestAppointmentTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestAppointmentTypeBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.APPOINTMENT_TYPE, Entity.class, service);
        name(ValueStrategy.random("zappointmenttype"));
    }

    /**
     * Constructs a {@link TestAppointmentTypeBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestAppointmentTypeBuilder(Entity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the default appointment reason.
     *
     * @param reason the appointment reason
     * @return this
     */
    public TestAppointmentTypeBuilder reason(String reason) {
        this.reason = ValueStrategy.value(reason);
        return this;
    }

    /**
     * Make the appointment type available for online booking.
     *
     * @return this
     */
    public TestAppointmentTypeBuilder onlineBooking() {
        return onlineBooking(true);
    }

    /**
     * Determines if the appointment type is available for online booking.
     *
     * @param onlineBooking if {@code true}, the appointment type is available for online booking
     * @return this
     */
    public TestAppointmentTypeBuilder onlineBooking(boolean onlineBooking) {
        this.onlineBooking = ValueStrategy.value(onlineBooking);
        return this;
    }

    /**
     * Determines if reminders should be sent for the appointment type.
     *
     * @param sendReminders if {@code true}, send reminders
     * @return this
     */
    public TestAppointmentTypeBuilder sendReminders(boolean sendReminders) {
        this.sendReminders = ValueStrategy.value(sendReminders);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (reason.isSet()) {
            if (reason.getValue() != null) {
                String code = reason.toString();
                Lookup lookup = new TestLookupBuilder(ScheduleArchetypes.VISIT_REASON, getService()).code(code).build();
                object.addClassification(lookup);
            } else {
                bean.removeValues("reason");
            }
        }
        onlineBooking.setValue(bean, "onlineBooking");
        sendReminders.setValue(bean, "sendReminders");
    }
}
