/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

import static java.math.BigDecimal.ZERO;

/**
 * Builds <em>act.customerEstimationItem</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEstimateItemBuilder extends AbstractTestIMObjectBuilder<Act, TestEstimateItemBuilder> {

    /**
     * The parent builder.
     */
    private final TestEstimateBuilder parent;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The product.
     */
    private Product product;

    /**
     * The template the product was sourced from.
     */
    private Product template;

    /**
     * The template expansion group.
     */
    private Integer group;

    /**
     * The low quantity.
     */
    private BigDecimal lowQuantity;

    /**
     * The high quantity.
     */
    private BigDecimal highQuantity;

    /**
     * The fixed price.
     */
    private BigDecimal fixedPrice;

    /**
     * The low unit price.
     */
    private BigDecimal lowUnitPrice;

    /**
     * The high unit price.
     */
    private BigDecimal highUnitPrice;

    /**
     * The low discount.
     */
    private BigDecimal lowDiscount;

    /**
     * The high discount.
     */
    private BigDecimal highDiscount;

    /**
     * The print flag.
     */
    private Boolean print;

    /**
     * Constructs a {@link TestEstimateItemBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     * @param rules   the customer account rules
     */
    public TestEstimateItemBuilder(TestEstimateBuilder parent, ArchetypeService service, CustomerAccountRules rules) {
        super(EstimateArchetypes.ESTIMATE_ITEM, Act.class, service);
        this.parent = parent;
        this.rules = rules;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public TestEstimateItemBuilder patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public TestEstimateItemBuilder clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestEstimateItemBuilder product(Product product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the template.
     *
     * @param template the template
     * @return this
     */
    public TestEstimateItemBuilder template(Product template) {
        this.template = template;
        return getThis();
    }

    /**
     * Sets the template expansion group.
     *
     * @param group the template expansion group
     * @return this
     */
    public TestEstimateItemBuilder group(int group) {
        this.group = group;
        return getThis();
    }

    /**
     * Sets the low quantity.
     *
     * @param lowQuantity the low quantity
     * @return this
     */
    public TestEstimateItemBuilder lowQuantity(int lowQuantity) {
        return lowQuantity(BigDecimal.valueOf(lowQuantity));
    }

    /**
     * Sets the low quantity.
     *
     * @param lowQuantity the low quantity
     * @return this
     */
    public TestEstimateItemBuilder lowQuantity(BigDecimal lowQuantity) {
        this.lowQuantity = lowQuantity;
        return getThis();
    }

    /**
     * Sets the high quantity.
     *
     * @param highQuantity the high quantity
     */
    public TestEstimateItemBuilder highQuantity(int highQuantity) {
        return highQuantity(BigDecimal.valueOf(highQuantity));
    }

    /**
     * Sets the high quantity.
     *
     * @param highQuantity the high quantity
     * @return this
     */
    public TestEstimateItemBuilder highQuantity(BigDecimal highQuantity) {
        this.highQuantity = highQuantity;
        return getThis();
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public TestEstimateItemBuilder fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public TestEstimateItemBuilder fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
        return getThis();
    }

    /**
     * Sets the low unit price.
     *
     * @param lowUnitPrice the low unit price
     * @return this
     */
    public TestEstimateItemBuilder lowUnitPrice(int lowUnitPrice) {
        return lowUnitPrice(BigDecimal.valueOf(lowUnitPrice));
    }

    /**
     * Sets the low unit price.
     *
     * @param lowUnitPrice the low unit price
     * @return this
     */
    public TestEstimateItemBuilder lowUnitPrice(BigDecimal lowUnitPrice) {
        this.lowUnitPrice = lowUnitPrice;
        return getThis();
    }

    /**
     * Sets the high unit price.
     *
     * @param highUnitPrice the high unit price
     * @return this
     */
    public TestEstimateItemBuilder highUnitPrice(int highUnitPrice) {
        return highUnitPrice(BigDecimal.valueOf(highUnitPrice));
    }

    /**
     * Sets the high unit price.
     *
     * @param highUnitPrice the high unit price
     * @return this
     */
    public TestEstimateItemBuilder highUnitPrice(BigDecimal highUnitPrice) {
        this.highUnitPrice = highUnitPrice;
        return getThis();
    }

    /**
     * Sets the low discount.
     *
     * @param lowDiscount the low discount
     * @return this
     */
    public TestEstimateItemBuilder lowDiscount(BigDecimal lowDiscount) {
        this.lowDiscount = lowDiscount;
        return getThis();
    }

    /**
     * Sets the high discount.
     *
     * @param highDiscount the high discount
     * @return this
     */
    public TestEstimateItemBuilder highDiscount(BigDecimal highDiscount) {
        this.highDiscount = highDiscount;
        return getThis();
    }

    /**
     * Sets the print flag.
     *
     * @param print the print flag
     * @return this
     */
    public TestEstimateItemBuilder print(boolean print) {
        this.print = print;
        return getThis();
    }

    /**
     * Adds the item to the estimate.
     *
     * @return the estimate builder
     */
    public TestEstimateBuilder add() {
        Act item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        if (product != null) {
            bean.setTarget("product", product);
        }
        if (template != null) {
            bean.setTarget("template", template);
            Participation participation = bean.getObject("template", Participation.class);
            IMObjectBean participationBean = bean.getBean(participation);
            participationBean.setValue("group", group);
        }
        if (lowQuantity != null) {
            bean.setValue("lowQty", lowQuantity);
        }
        if (highQuantity != null) {
            bean.setValue("highQty", highQuantity);
        }
        if (fixedPrice != null) {
            bean.setValue("fixedPrice", fixedPrice);
        }
        if (lowUnitPrice != null) {
            bean.setValue("lowUnitPrice", lowUnitPrice);
        }
        if (highUnitPrice != null) {
            bean.setValue("highUnitPrice", highUnitPrice);
        }
        if (lowDiscount != null) {
            bean.setValue("lowDiscount", lowDiscount);
        }
        if (highDiscount != null) {
            bean.setValue("highDiscount", highDiscount);
        }
        if (print != null) {
            bean.setValue("print", print);
        }

        BigDecimal lowTotal = rules.calculateTotal(bean.getBigDecimal("fixedPrice", ZERO),
                                                   bean.getBigDecimal("lowUnitPrice", ZERO),
                                                   bean.getBigDecimal("lowQty", ZERO),
                                                   bean.getBigDecimal("lowDiscount", ZERO));
        BigDecimal highTotal = rules.calculateTotal(bean.getBigDecimal("fixedPrice", ZERO),
                                                    bean.getBigDecimal("highUnitPrice", ZERO),
                                                    bean.getBigDecimal("highQty", ZERO),
                                                    bean.getBigDecimal("highDiscount", ZERO));
        bean.setValue("lowTotal", lowTotal);
        bean.setValue("highTotal", highTotal);
    }
}
