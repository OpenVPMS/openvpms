/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.mailServer</em> instances.
 *
 * @author Tim Anderson
 */
public class TestMailServerBuilder extends AbstractTestEntityBuilder<Entity, TestMailServerBuilder> {

    /**
     * The host.
     */
    private ValueStrategy host = ValueStrategy.unset();

    /**
     * Constructs a {@link TestMailServerBuilder}.
     *
     * @param service the archetype service
     */
    public TestMailServerBuilder(ArchetypeService service) {
        super("entity.mailServer", Entity.class, service);
        name(ValueStrategy.random());
    }

    /**
     * Sets the mail server host.
     *
     * @param host the host
     * @return this
     */
    public TestMailServerBuilder host(String host) {
        this.host = ValueStrategy.value(host);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        host.setValue(bean, "host");
    }
}