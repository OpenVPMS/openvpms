/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.patientClinicalProblem</em>, instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProblemBuilder extends AbstractTestPatientActBuilder<Act, TestProblemBuilder> {

    /**
     * The items.
     */
    private final List<Act> items = new ArrayList<>();

    /**
     * Constructs a {@link TestProblemBuilder}.
     *
     * @param service the archetype service
     */
    public TestProblemBuilder(ArchetypeService service) {
        super(PatientArchetypes.CLINICAL_PROBLEM, Act.class, service);
    }

    /**
     * Constructs an {@link AbstractTestPatientActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestProblemBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds an item.
     *
     * @param item the item
     * @return this
     */
    public TestProblemBuilder addItem(Act item) {
        items.add(item);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (Act item : items) {
            bean.addTarget("items", item, "problem");
        }
        items.clear(); // can't reuse
    }
}