/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.rosterEvent</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestRosterEventBuilder extends AbstractTestCalendarEventBuilder<TestRosterEventBuilder> {

    /**
     * The user.
     */
    private User user;

    /**
     * The location.
     */
    private Party location;

    /**
     * Constructs a {@link TestRosterEventBuilder}.
     *
     * @param service the archetype service
     */
    public TestRosterEventBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.ROSTER_EVENT, service);
    }

    /**
     * Sets the user.
     *
     * @param user the user
     * @return this
     */
    public TestRosterEventBuilder user(User user) {
        this.user = user;
        return this;
    }

    /**
     * Sets the location.
     *
     * @param location the location
     * @return this
     */
    public TestRosterEventBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (user != null) {
            bean.setTarget("user", user);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
    }
}