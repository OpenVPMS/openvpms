/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;

/**
 * Factory for creating documents and document templates, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs an {@link TestDocumentFactory}.
     *
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    public TestDocumentFactory(ArchetypeService service, DocumentHandlers handlers) {
        this.service = service;
        this.handlers = handlers;
    }

    /**
     * Creates a JasperReports document.
     *
     * @return a JRXML document
     */
    public Document createJRXML() {
        return createJRXML("blank.jrxml");
    }

    /**
     * Creates a JasperReports document.
     *
     * @param name the document name
     * @return a JRXML document
     */
    public Document createJRXML(String name) {
        return newDocumentFromStream("/documents/template-blank.jrxml", "text/xml")
                .name(name)
                .build();
    }

    /**
     * Creates an ODT document.
     *
     * @return a new document
     */
    public Document createODT() {
        return createODT("blank.odt");
    }

    /**
     * Creates an ODT document.
     *
     * @param name the document name
     * @return a new document
     */
    public Document createODT(String name) {
        return newDocumentFromStream("/documents/blank.odt", "application/vnd.oasis.opendocument.text")
                .name(name)
                .build();
    }

    /**
     * Creates a Word document.
     *
     * @return a new document
     */
    public Document createDOC() {
        return createDOC("blank.doc");
    }

    /**
     * Creates n Word document.
     *
     * @param name the document name
     * @return a new document
     */
    public Document createDOC(String name) {
        return newDocumentFromStream("/documents/blank.doc", "application/msword")
                .name(name)
                .build();
    }

    /**
     * Creates an image document.
     *
     * @return a new document
     */
    public Document createImage() {
        return createPNG("image.png");
    }

    /**
     * Creates a PNG.
     *
     * @param name the image name
     * @return a new document
     */
    public Document createPNG(String name) {
        return newDocumentFromStream("/documents/image.png", "image/png")
                .name(name)
                .build();
    }

    /**
     * Creates a PDF.
     *
     * @return a PDF document
     */
    public Document createPDF() {
        return createPDF("doc.pdf");
    }

    /**
     * Creates a PDF.
     *
     * @param name the document name
     * @return a PDF document
     */
    public Document createPDF(String name) {
        return newDocumentFromStream("/documents/blank.pdf", "application/pdf")
                .name(name)
                .build();
    }

    /**
     * Creates a plain text document.
     *
     * @return a plain text document
     */
    public Document createText() {
        return newDocument().name("plain.txt")
                .mimeType("text/plain")
                .content("this is plain text")
                .build();
    }

    /**
     * Creates a document from a resource.
     *
     * @param path the resource path
     * @return a new document
     */
    public Document createDocument(String path) {
        return createDocument(path, null);
    }

    /**
     * Creates a document from a resource.
     *
     * @param path     the resource path
     * @param mimeType the mime type. May be {@code null}
     * @return a new document
     */
    public Document createDocument(String path, String mimeType) {
        return newDocumentFromStream(path, mimeType).build();
    }

    /**
     * Creates a document from a file.
     *
     * @param file the file
     * @return a new document
     * @throws IOException for any IO error
     */
    public Document createDocument(File file) throws IOException {
        return newDocument()
                .name(file.getName())
                .content(new FileInputStream(file))
                .build();
    }

    /**
     * Returns a builder for a document.
     *
     * @return a document builder
     */
    public TestDocumentBuilder newDocument() {
        return new TestDocumentBuilder(handlers, service);
    }

    /**
     * Creates an <em>entity.documentTemplate</em> with a blank document.
     *
     * @param type the document template type
     * @return a new template
     */
    public Entity createTemplate(String type) {
        return newTemplate().type(type).blankDocument().build();
    }

    /**
     * Returns a builder for a template.
     *
     * @return a template builder
     */
    public TestDocumentTemplateBuilder newTemplate() {
        return new TestDocumentTemplateBuilder(service, handlers);
    }

    /**
     * Returns a builder to update a template.
     *
     * @param template the template
     * @return a template builder
     */
    public TestDocumentTemplateBuilder updateTemplate(Entity template) {
        return new TestDocumentTemplateBuilder(template, service, handlers);
    }

    /**
     * Returns a builder for a user email template.
     *
     * @return an email template builder
     */
    public TestEmailTemplateBuilder newEmailTemplate() {
        return newEmailTemplate(DocumentArchetypes.USER_EMAIL_TEMPLATE);
    }

    /**
     * Returns a builder for an email template.
     *
     * @param archetype the email template archetype
     * @return an email template builder
     */
    public TestEmailTemplateBuilder newEmailTemplate(String archetype) {
        return new TestEmailTemplateBuilder(archetype, service, handlers);
    }

    /**
     * Returns a builder to update an email template.
     *
     * @param template the template
     * @return a template builder
     */
    public TestEmailTemplateBuilder updateEmailTemplate(Entity template) {
        return new TestEmailTemplateBuilder(template, service, handlers);
    }

    /**
     * Attaches a document to a template.
     *
     * @param template the template. May be an <em>entity.documentTemplate</em> or
     *                 <em>entity.documentTemplateEmail*</em>
     * @param document the document
     * @return the template act
     */
    public DocumentAct attachDocument(Entity template, Document document) {
        AbstractTestDocumentTemplateBuilder<?> builder;
        if (template.isA(DocumentArchetypes.DOCUMENT_TEMPLATE)) {
            builder = updateTemplate(template);
        } else if (template.isA(DocumentArchetypes.EMAIL_TEMPLATES)) {
            builder = updateEmailTemplate(template);
        } else {
            throw new IllegalArgumentException("Unsupported template " + template.getArchetype());
        }
        builder.document(document).build();
        return builder.getTemplateAct();
    }

    /**
     * Returns a builder for an SMS template.
     *
     * @param archetype the SMS template archetype
     * @return a new builder
     */
    public TestSMSTemplateBuilder newSMSTemplate(String archetype) {
        return new TestSMSTemplateBuilder(archetype, service);
    }

    /**
     * Returns a builder for letterhead.
     *
     * @return a new builder
     */
    public TestLetterheadBuilder newLetterhead() {
        return new TestLetterheadBuilder(service);
    }

    /**
     * Returns a builder to update letterhead.
     *
     * @param letterhead the letterhead to update
     * @return a new builder
     */
    public TestLetterheadBuilder updateLetterhead(Entity letterhead) {
        return new TestLetterheadBuilder(letterhead, service);
    }

    /**
     * Returns a verifier for a document type.
     *
     * @param archetype the document archetype
     * @return a new verifier
     */
    public TestDocumentVerifier<?> newVerifier(String archetype) {
        TestDocumentVerifier<?> result;
        if (archetype.equals(DocumentArchetypes.IMAGE_DOCUMENT)) {
            result = newImageVerifier();
        } else {
            result = new DefaultTestDocumentVerifier(handlers, service)
                    .archetype(archetype);
        }
        return result;
    }

    /**
     * Creates a verifier for image documents.
     *
     * @return a new verifier
     */
    public TestImageDocumentVerifier newImageVerifier() {
        return new TestImageDocumentVerifier(handlers, service);
    }

    /**
     * Returns a verifier for a document.
     *
     * @param document the expected document
     * @return a new verifier
     */
    public TestDocumentVerifier<?> newVerifier(Document document) {
        return newVerifier(document.getArchetype())
                .initialise(document);
    }

    /**
     * Converts a document to string.
     *
     * @param document the document
     * @return the string form of the document
     * @throws IOException for any I/O error
     */
    public String toString(Document document) throws IOException {
        return IOUtils.toString(handlers.get(document).getContent(document), StandardCharsets.UTF_8);
    }

    /**
     * Returns a document builder populated from a stream.
     *
     * @param path     the resource path
     * @param mimeType the mime type. May be {@code null}
     * @return a new document builder
     */
    private TestDocumentBuilder newDocumentFromStream(String path, String mimeType) {
        InputStream stream = getClass().getResourceAsStream(path);
        assertNotNull(stream);
        return newDocument()
                .name(FilenameUtils.getName(path))
                .mimeType(mimeType)
                .content(stream);
    }

}
