/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.lookup.TestCurrencyBuilder;
import org.openvpms.archetype.test.builder.lookup.TestTaxTypeBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.user.TestUserBuilder;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.organisationPractice</em> instances, for testing purposes.
 * <p/>
 * Note that the <em>party.organisationPractice</em> is a singleton, so any saved instance will be returned.
 *
 * @author Tim Anderson
 */
public class TestPracticeBuilder extends AbstractTestOrganisationBuilder<TestPracticeBuilder> {

    /**
     * The tax rates.
     */
    private final List<Lookup> taxes = new ArrayList<>();

    /**
     * Determines if existing taxes should be removed.
     */
    private boolean removeTaxes = false;

    /**
     * The mail server.
     */
    private Entity mailServer;

    /**
     * Determines if the mail server should be removed.
     */
    private boolean removeMailServer = false;

    /**
     * The currency.
     */
    private String currencyCode;

    /**
     * The currency lookup.
     */
    private Lookup currencyLookup;

    /**
     * The service user.
     */
    private User serviceUser;

    /**
     * The practice locations.
     */
    private Party[] locations;

    /**
     * Determines if minimum quantities should be enabled.
     */
    private ValueStrategy minQuantities = ValueStrategy.unset();

    /**
     * User type code who can override minimum quantities.
     */
    private ValueStrategy minQuantitiesOverride = ValueStrategy.unset();

    /**
     * Determines if departments are enabled.
     */
    private ValueStrategy departments = ValueStrategy.unset();

    /**
     * Determines if prices should be shown inclusive or exclusive of tax.
     */
    private ValueStrategy showPricesTaxInclusive = ValueStrategy.unset();

    /**
     * Determines if products should be filtered by location.
     */
    private ValueStrategy useLocationProducts = ValueStrategy.unset();

    /**
     * Determines if the logged-in user should be used for clinician fields, if the logged-in user is a clinician.
     */
    private ValueStrategy useLoggedInClinician = ValueStrategy.unset();

    /**
     * Determines if restricted drugs can be sold over the counter.
     */
    private ValueStrategy sellRestrictedDrugsOTC = ValueStrategy.unset();

    /**
     * Determines if finalisation of orders containing restricted medications is limited to clinicians.
     */
    private ValueStrategy restrictOrdering = ValueStrategy.unset();

    /**
     * The pharmacy order discontinue period.
     */
    private ValueStrategy pharmacyOrderDiscontinuePeriod = ValueStrategy.unset();

    /**
     * The pharmacy order discontinue period units.
     */
    private ValueStrategy pharmacyOrderDiscontinuePeriodUnits = ValueStrategy.unset();

    /**
     * The default weight units.
     */
    private ValueStrategy defaultWeightUnits = ValueStrategy.unset();

    /**
     * The prescription expiry period.
     */
    private ValueStrategy prescriptionExpiryPeriod = ValueStrategy.unset();

    /**
     * The prescription expiry units.
     */
    private ValueStrategy prescriptionExpiryUnits = ValueStrategy.unset();

    /**
     * Determines if plugins are enabled.
     */
    private ValueStrategy enablePlugins = ValueStrategy.unset();

    /**
     * The theme.
     */
    private ValueStrategy theme = ValueStrategy.unset();


    /**
     * Constructs a {@link TestPracticeBuilder}.
     *
     * @param service the archetype service
     */
    public TestPracticeBuilder(ArchetypeService service) {
        super(PracticeArchetypes.PRACTICE, service);
        name("zpractice");
        currencyCode = "AUD";
    }

    /**
     * Constructs a {@link TestPracticeBuilder}.
     *
     * @param object  the practice to update
     * @param service the archetype service
     */
    public TestPracticeBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the practice locations.
     *
     * @param locations the locations
     * @return this
     */
    public TestPracticeBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Sets the currency.
     * <p/>
     * If the currency lookup exists, it will be reset.
     *
     * @param currency the currency code
     * @return this
     */
    public TestPracticeBuilder currency(String currency) {
        this.currencyCode = currency;
        return this;
    }

    /**
     * Sets the currency.
     *
     * @param currency the currency lookup
     * @return this
     */
    public TestPracticeBuilder currency(Lookup currency) {
        currencyLookup = currency;
        return currency((currency != null) ? currency.getCode() : null);
    }

    /**
     * Sets the practice tax types.
     * <p/>
     * This removes existing types.
     *
     * @param rates the tax rates
     * @return this
     */
    public TestPracticeBuilder taxTypes(BigDecimal... rates) {
        removeTaxTypes();
        for (BigDecimal rate : rates) {
            addTaxType(rate);
        }
        return this;
    }

    /**
     * Adds a tax type.
     *
     * @param rate the tax rate
     * @return this
     */
    public TestPracticeBuilder addTaxType(BigDecimal rate) {
        return addTaxType(new TestTaxTypeBuilder(getService()).rate(rate).build(false));
    }

    /**
     * Adds a tax type.
     *
     * @param taxType the tax type
     * @return this
     */
    public TestPracticeBuilder addTaxType(Lookup taxType) {
        taxes.add(taxType);
        return this;
    }

    /**
     * Removes tax types.
     *
     * @return this
     */
    public TestPracticeBuilder removeTaxTypes() {
        removeTaxes = true;
        taxes.clear();
        return this;
    }

    /**
     * Sets the mail server.
     *
     * @param mailServer the mail server. May be {@code null}
     * @return this
     */
    public TestPracticeBuilder mailServer(Entity mailServer) {
        this.mailServer = mailServer;
        removeMailServer = mailServer == null;
        return this;
    }

    /**
     * Sets the service user.
     * <p/>
     * This creates a random user.
     *
     * @return this
     */
    public TestPracticeBuilder serviceUser() {
        return serviceUser(new TestUserBuilder(getService(), null, null).build());
    }

    /**
     * Sets the service user.
     *
     * @param serviceUser the service user
     * @return this
     */
    public TestPracticeBuilder serviceUser(User serviceUser) {
        this.serviceUser = serviceUser;
        return this;
    }

    /**
     * Determines if minimum quantities are enabled for charge and estimate items.
     *
     * @param enable if {@code true} enable minimum quantities, else disable them
     * @return this
     */
    public TestPracticeBuilder minimumQuantities(boolean enable) {
        minQuantities = ValueStrategy.value(enable);
        return this;
    }


    /**
     * Sets the code of the <em>lookup.userType</em> for users that can override minimum quantities, when minimum
     * quantities are enabled.
     *
     * @param code the user type code
     * @return this
     */
    public TestPracticeBuilder minimumQuantitiesOverride(String code) {
        this.minQuantitiesOverride = ValueStrategy.value(code);
        return this;
    }

    /**
     * Determines if departments should be enabled.
     *
     * @param departments if {@code true} enable departments, else disable them
     * @return this
     */
    public TestPracticeBuilder departments(boolean departments) {
        this.departments = ValueStrategy.value(departments);
        return this;
    }

    /**
     * Determines if prices should be shown tax-inclusive.
     *
     * @param showPricesTaxInclusive if {@code true}, show prices tax-inclusive, else show them tax-exclusive
     * @return this
     */
    public TestPracticeBuilder showPricesTaxInclusive(boolean showPricesTaxInclusive) {
        this.showPricesTaxInclusive = ValueStrategy.value(showPricesTaxInclusive);
        return this;
    }

    /**
     * Determines if products should be filtered by location.
     *
     * @param useLocationProducts if {@code true} products should be filtered by location
     * @return this
     */
    public TestPracticeBuilder useLocationProducts(boolean useLocationProducts) {
        this.useLocationProducts = ValueStrategy.value(useLocationProducts);
        return this;
    }

    /**
     * Determines if the logged-in user should be used for clinician fields, if the logged-in user is a clinician.
     *
     * @param useLoggedInClinician if {@code true} use the logged-in clinician for clinician fields
     * @return this
     */
    public TestPracticeBuilder useLoggedInClinician(boolean useLoggedInClinician) {
        this.useLoggedInClinician = ValueStrategy.value(useLoggedInClinician);
        return this;
    }

    /**
     * Determines if restricted drugs can be sold over the counter.
     *
     * @param sellRestrictedDrugsOTC if {@code true}, restricted drugs can be sold over the counter
     * @return this
     */
    public TestPracticeBuilder sellRestrictedDrugsOTC(boolean sellRestrictedDrugsOTC) {
        this.sellRestrictedDrugsOTC = ValueStrategy.value(sellRestrictedDrugsOTC);
        return this;
    }

    /**
     * Determines if finalisation of orders containing restricted medications is limited to clinicians.
     *
     * @param restrictOrdering if {@code true}, restrict ordering
     * @return this
     */
    public TestPracticeBuilder restrictOrdering(boolean restrictOrdering) {
        this.restrictOrdering = ValueStrategy.value(restrictOrdering);
        return this;
    }

    /**
     * Sets the pharmacy order discontinue period.
     *
     * @param period the period
     * @param units  the period units
     * @return this
     */
    public TestPracticeBuilder pharmacyOrderDiscontinuePeriod(int period, DateUnits units) {
        pharmacyOrderDiscontinuePeriod = ValueStrategy.value(period);
        pharmacyOrderDiscontinuePeriodUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the default weight units.
     *
     * @param units the units
     * @return this
     */
    public TestPracticeBuilder defaultWeightUnits(WeightUnits units) {
        defaultWeightUnits = ValueStrategy.value(units != null ? units.toString() : null);
        return this;
    }

    /**
     * Sets the prescription expiry period.
     *
     * @param period the period
     * @param units  the units
     * @return this
     */
    public TestPracticeBuilder prescriptionExpiryPeriod(int period, DateUnits units) {
        prescriptionExpiryPeriod = ValueStrategy.value(period);
        prescriptionExpiryUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Enables plugins.
     *
     * @return this
     */
    public TestPracticeBuilder enablePlugins() {
        return enablePlugins(true);
    }

    /**
     * Determines if plugins are enabled.
     *
     * @param enablePlugins if {@code true} enable plugins, else disable them
     * @return this
     */
    public TestPracticeBuilder enablePlugins(boolean enablePlugins) {
        this.enablePlugins = ValueStrategy.value(enablePlugins);
        return this;
    }

    /**
     * Sets the theme.
     *
     * @param theme the theme
     * @return this
     */
    public TestPracticeBuilder theme(String theme) {
        this.theme = ValueStrategy.value(theme);
        return this;
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Party getObject(String archetype) {
        Party practice = getExisting();
        if (practice == null) {
            practice = getSingleton(PracticeArchetypes.PRACTICE, Party.class);
            if (!practice.isNew()) {
                resetValues(practice);
            }
        }
        return practice;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location, "practice");
                toSave.add(location);
            }
            locations = null; // can't reuse
        }
        if (currencyLookup == null && currencyCode != null) {
            currencyLookup = new TestCurrencyBuilder(getService()).code(currencyCode).build();
        }
        if (currencyLookup != null) {
            bean.setValue("currency", currencyLookup.getCode());
        }

        if (serviceUser != null) {
            bean.setTarget("serviceUser", serviceUser);
        }
        minQuantities.setValue(bean, "minimumQuantities");
        minQuantitiesOverride.setValue(bean, "minimumQuantitiesOverride");
        departments.setValue(bean, "departments");
        showPricesTaxInclusive.setValue(bean, "showPricesTaxInclusive");
        useLocationProducts.setValue(bean, "useLocationProducts");
        useLoggedInClinician.setValue(bean, "useLoggedInClinician");
        sellRestrictedDrugsOTC.setValue(bean, "sellRestrictedDrugsOTC");
        restrictOrdering.setValue(bean, "restrictOrdering");
        pharmacyOrderDiscontinuePeriod.setValue(bean, "pharmacyOrderDiscontinuePeriod");
        pharmacyOrderDiscontinuePeriodUnits.setValue(bean, "pharmacyOrderDiscontinuePeriodUnits");
        if (removeTaxes) {
            bean.removeValues("taxes");
        }
        for (Lookup tax : taxes) {
            object.addClassification(tax);
        }
        taxes.clear();
        removeTaxes = false;

        if (mailServer != null) {
            bean.setTarget("mailServer", mailServer);
        } else if (removeMailServer) {
            bean.removeValues("mailServer");
        }

        defaultWeightUnits.setValue(bean, "defaultWeightUnits");
        prescriptionExpiryPeriod.setValue(bean, "prescriptionExpiryPeriod");
        prescriptionExpiryUnits.setValue(bean, "prescriptionExpiryUnits");
        enablePlugins.setValue(bean, "enablePlugins");
        theme.setValue(bean, "theme");
    }

    /**
     * Resets a practice to its defaults.
     *
     * @param practice the practice
     */
    private void resetValues(Party practice) {
        IMObjectBean bean = getBean(practice);
        // remove contacts
        for (Contact contact : bean.getValues("contacts", Contact.class)) {
            practice.removeContact(contact);
        }

        // remove locations
        for (Party location : bean.getTargets("locations", Party.class)) {
            bean.removeTarget("locations", location);
        }

        // remove taxes
        for (Lookup tax : bean.getValues("taxes", Lookup.class)) {
            bean.removeValue("taxes", tax);
        }

        useLocationProducts.reset(bean, "useLocationProducts");
        useLoggedInClinician.reset(bean, "useLoggedInClinician");
        sellRestrictedDrugsOTC.reset(bean, "sellRestrictedDrugsOTC");
        restrictOrdering.reset(bean, "restrictOrdering");
        defaultWeightUnits.reset(bean, "defaultWeightUnits");
        prescriptionExpiryPeriod.reset(bean, "prescriptionExpiryPeriod");
        prescriptionExpiryUnits.reset(bean, "prescriptionExpiryUnits");
        bean.removeValues("serviceUser");
    }
}
