/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Builds <em>act.customerAccountRefund</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestRefundBuilder extends AbstractTestPaymentRefundBuilder<TestRefundBuilder> {

    /**
     * Constructs a {@link TestRefundBuilder}.
     *
     * @param service the archetype service
     */
    public TestRefundBuilder(ArchetypeService service) {
        super(CustomerAccountArchetypes.REFUND, service);
    }

    /**
     * Returns a builder to add a cash item.
     *
     * @return a cash item builder
     */
    public TestCashRefundItemBuilder cash() {
        return new TestCashRefundItemBuilder(this, getService());
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder cash(int amount) {
        return cash().amount(amount).add();
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder cash(BigDecimal amount) {
        return cash().amount(amount).add();
    }

    /**
     * Returns a builder to add a cheque item.
     *
     * @return a cheque item builder
     */
    public TestChequeRefundItemBuilder cheque() {
        return new TestChequeRefundItemBuilder(this, getService());
    }

    /**
     * Adds a cheque item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder cheque(int amount) {
        return cheque(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a cheque item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder cheque(BigDecimal amount) {
        return cheque().amount(amount).add();
    }

    /**
     * Returns a builder to add a credit item.
     *
     * @return a credit item builder
     */
    public TestCreditRefundItemBuilder credit() {
        return new TestCreditRefundItemBuilder(this, getService());
    }

    /**
     * Adds a credit item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder credit(int amount) {
        return credit(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a credit item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder credit(BigDecimal amount) {
        return credit().amount(amount).add();
    }


    /**
     * Returns a builder to add a discount item.
     *
     * @return a discount item builder
     */
    public TestDiscountRefundItemBuilder discount() {
        return new TestDiscountRefundItemBuilder(this, getService());
    }

    /**
     * Adds a discount item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder discount(int amount) {
        return discount(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a discount item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder discount(BigDecimal amount) {
        return discount().amount(amount).add();
    }

    /**
     * Returns a builder to add an EFT item.
     *
     * @return an EFT item builder
     */
    @SuppressWarnings("unchecked")
    public TestEFTRefundItemBuilder eft() {
        return new TestEFTRefundItemBuilder(this, getService());
    }

    /**
     * Returns a builder to add an 'other' item.
     *
     * @return an 'other' item builder
     */
    public TestOtherRefundItemBuilder other() {
        return new TestOtherRefundItemBuilder(this, getService());
    }

    /**
     * Adds an 'other' item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder other(int amount) {
        return other(BigDecimal.valueOf(amount));
    }

    /**
     * Adds an 'other' item.
     *
     * @param amount the amount
     * @return this
     */
    public TestRefundBuilder other(BigDecimal amount) {
        return other().amount(amount).add();
    }
}
