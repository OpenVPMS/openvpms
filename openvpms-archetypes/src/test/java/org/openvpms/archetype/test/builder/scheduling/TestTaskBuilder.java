/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.customerTask</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestTaskBuilder extends AbstractTestScheduleActBuilder<TestTaskBuilder> {

    /**
     * The work list.
     */
    private Entity workList;

    /**
     * The task type.
     */
    private Entity taskType;

    /**
     * Constructs an {@link TestTaskBuilder}.
     *
     * @param service the archetype service
     */
    public TestTaskBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.TASK, service);
    }

    /**
     * Sets the work list.
     *
     * @param workList the work list
     * @return this
     */
    public TestTaskBuilder workList(Entity workList) {
        this.workList = workList;
        return this;
    }

    /**
     * Sets the task type.
     *
     * @param taskType the task type
     * @return this
     */
    public TestTaskBuilder taskType(Entity taskType) {
        this.taskType = taskType;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setTarget("worklist", workList);
        bean.setTarget("taskType", taskType);
    }
}