/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Base class for document verifies.
 *
 * @author Tim Anderson
 */
public class TestDocumentVerifier<B extends TestDocumentVerifier<B>> {
    /**
     * The document handlers.
     */
    protected final DocumentHandlers handlers;

    /**
     * The archetype service.
     */
    protected final ArchetypeService service;

    /**
     * The expected document archetype.
     */
    private String archetype;

    /**
     * The expected name.
     */
    private String name;

    /**
     * The expected mime type.
     */
    private String mimeType;

    /**
     * The expected size.
     */
    private long size;

    /**
     * The expected checksum.
     */
    private long checksum;

    /**
     * The expected contents.
     */
    private byte[] content;

    /**
     * Constructs an {@link TestDocumentVerifier}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     */
    public TestDocumentVerifier(DocumentHandlers handlers, ArchetypeService service) {
        this.handlers = handlers;
        this.service = service;
    }

    /**
     * Initialises this from a document.
     *
     * @param document the document
     * @return this
     */
    public B initialise(Document document) {
        archetype(document.getArchetype());
        name(document.getName());
        mimeType(document.getMimeType());
        size(document.getSize());
        checksum(document.getChecksum());
        content(document);
        return getThis();
    }

    /**
     * Sets the expected document archetype.
     *
     * @param archetype the archetype
     * @return this
     */
    public B archetype(String archetype) {
        this.archetype = archetype;
        return getThis();
    }

    /**
     * Sets the expected document name.
     *
     * @param name the name
     * @return this
     */
    public B name(String name) {
        this.name = name;
        return getThis();
    }

    /**
     * Sets the expected mime type.
     *
     * @param mimeType the mime type
     * @return this
     */
    public B mimeType(String mimeType) {
        this.mimeType = mimeType;
        return getThis();
    }

    /**
     * Sets the expected size.
     *
     * @param size the size
     * @return this
     */
    public B size(long size) {
        this.size = size;
        return getThis();
    }

    /**
     * Sets the expected checksum.
     *
     * @param checksum the checksum
     * @return this
     */
    public B checksum(long checksum) {
        this.checksum = checksum;
        return getThis();
    }

    /**
     * Sets the expected content.
     *
     * @param stream a stream to the content
     * @return this
     * @throws IOException for any I/O error
     */
    public B content(InputStream stream) throws IOException {
        return content(getContent(stream));
    }

    /**
     * Sets the expected content from a document.
     *
     * @param document the content
     * @return this
     */
    public B content(Document document) {
        return content(getContent(document));
    }

    /**
     * Sets the expected content.
     *
     * @param content the content
     * @return this
     */
    public B content(byte[] content) {
        this.content = content;
        return getThis();
    }

    /**
     * Verifies a document matches that expected.
     *
     * @param document the document
     */
    public void verify(Document document) {
        assertNotNull(document);
        assertEquals(archetype, document.getArchetype());
        assertEquals(name, document.getName());
        assertEquals(mimeType, document.getMimeType());
        assertEquals(size, document.getSize());
        assertEquals(checksum, document.getChecksum());
        assertArrayEquals(content, getContent(document));
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected B getThis() {
        return (B) this;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    private byte[] getContent(Document document) {
        byte[] content;
        try (InputStream input = handlers.get(document).getContent(document)) {
            content = getContent(input);
        } catch (IOException exception) {
            throw new DocumentException(DocumentException.ErrorCode.ReadError, document.getName());
        }
        return content;
    }

    private byte[] getContent(InputStream input) throws IOException {
        byte[] content;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        IOUtils.copy(input, output);
        content = output.toByteArray();
        return content;
    }
}
