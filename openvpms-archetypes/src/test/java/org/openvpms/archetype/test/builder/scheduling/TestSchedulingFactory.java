/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;

import java.util.Date;

/**
 * Factory for scheduling archetypes.
 *
 * @author Tim Anderson
 */
public class TestSchedulingFactory {

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The product factory.
     */
    private final TestProductFactory productFactory;

    /**
     * The archetype service.
     */
    private final IArchetypeRuleService service;

    /**
     * Constructs a {@link TestSchedulingFactory}.
     *
     * @param rules          the appointment rules
     * @param productFactory the product factory
     * @param service        the archetype service. Must be rules based to trigger appointment save rules
     */
    public TestSchedulingFactory(AppointmentRules rules, TestProductFactory productFactory,
                                 IArchetypeRuleService service) {
        this.rules = rules;
        this.productFactory = productFactory;
        this.service = service;
    }

    /**
     * Creates a new schedule at a practice location.
     *
     * @param location the practice location
     * @return the schedule
     */
    public Entity createSchedule(Party location) {
        return newSchedule().location(location).build();
    }

    /**
     * Returns a builder to create a new schedule.
     *
     * @return the builder
     */
    public TestScheduleBuilder newSchedule() {
        return new TestScheduleBuilder(service);
    }

    /**
     * Returns a builder to update  a schedule
     *
     * @param schedule the schedule to update
     * @return the builder
     */
    public TestScheduleBuilder updateSchedule(Entity schedule) {
        return new TestScheduleBuilder(schedule, service);
    }

    /**
     * Creates an appointment type.
     *
     * @return the appointment type
     */
    public Entity createAppointmentType() {
        return newAppointmentType().build();
    }

    /**
     * Creates an appointment type.
     *
     * @param name the appointment type name
     * @return the appointment type
     */
    public Entity createAppointmentType(String name) {
        return newAppointmentType().name(name).build();
    }

    /**
     * Returns a builder to create an appointment type.
     *
     * @return the builder
     */
    public TestAppointmentTypeBuilder newAppointmentType() {
        return new TestAppointmentTypeBuilder(service);
    }

    /**
     * Returns a builder to update an appointment type.
     *
     * @param appointmentType the appointment type
     * @return the builder
     */
    public TestAppointmentTypeBuilder updateAppointmentType(Entity appointmentType) {
        return new TestAppointmentTypeBuilder(appointmentType, service);
    }

    /**
     * Creates a cage type.
     *
     * @return the cage type
     */
    public Entity createCageType() {
        return newCageType().firstPetProductDay(productFactory.createService()).build();
    }

    /**
     * Returns a builder to create a cage type.
     *
     * @return the builder
     */
    public TestCageTypeBuilder newCageType() {
        return new TestCageTypeBuilder(service);
    }

    /**
     * Creates a new roster area at a practice location.
     *
     * @param location the practice location
     * @return the roster area
     */
    public Entity createRosterArea(Party location) {
        return newRosterArea().location(location).build();
    }

    /**
     * Creates a new roster area at a practice location.
     *
     * @param location  the practice location
     * @param schedules the schedules associated with the roster area
     * @return the roster area
     */
    public Entity createRosterArea(Party location, Entity... schedules) {
        return newRosterArea()
                .location(location)
                .schedules(schedules)
                .build();
    }

    /**
     * Returns a builder to create a new roster area.
     *
     * @return the builder
     */
    public TestRosterAreaBuilder newRosterArea() {
        return new TestRosterAreaBuilder(service);
    }

    /**
     * Returns a builder to create new roster event.
     *
     * @return the builder
     */
    public TestRosterEventBuilder newRosterEvent() {
        return new TestRosterEventBuilder(service);
    }

    /**
     * Creates a new synchronisation identity.
     *
     * @return a new synchronisation identity
     */
    public ActIdentity createSyncId() {
        return new TestSyncIdBuilder(service).build();
    }

    /**
     * Returns a builder to update synchronisation identity.
     *
     * @param identity the identity to update
     * @return a new builder
     */
    public TestSyncIdBuilder updateSyncId(ActIdentity identity) {
        return new TestSyncIdBuilder(identity, service);
    }

    /**
     * Creates a new schedule view.
     *
     * @param schedules the schedules
     * @return the schedule view
     */
    public Entity createScheduleView(Entity... schedules) {
        return newScheduleView().schedules(schedules).build();
    }

    /**
     * Returns a builder to create a new schedule view.
     *
     * @return the builder
     */
    public TestScheduleViewBuilder newScheduleView() {
        return new TestScheduleViewBuilder(service);
    }

    /**
     * Returns a builder to create an appointment.
     *
     * @return the builder
     */
    public TestAppointmentBuilder newAppointment() {
        return new TestAppointmentBuilder(rules, service);
    }

    /**
     * Returns a builder to update an appointment.
     *
     * @param appointment the appointment to update
     * @return the builder
     */
    public TestAppointmentBuilder updateAppointment(Act appointment) {
        return new TestAppointmentBuilder(appointment, rules, service);
    }

    /**
     * Creates a task type.
     *
     * @return the task type
     */
    public Entity createTaskType() {
        return newTaskType().build();
    }

    /**
     * Creates a work list.
     *
     * @return the work list
     */
    public Entity createWorkList() {
        return newWorkList().build();
    }

    /**
     * Returns a builder to create a work list.
     *
     * @return the builder
     */
    public TestWorkListBuilder newWorkList() {
        return new TestWorkListBuilder(service);
    }

    /**
     * Creates a new work list view.
     *
     * @param worklists the work lists
     * @return the work list view
     */
    public Entity createWorkListView(Entity... worklists) {
        return newWorkListView().worklists(worklists).build();
    }

    /**
     * Returns a builder to create a new work list view.
     *
     * @return the builder
     */
    public TestWorkListViewBuilder newWorkListView() {
        return new TestWorkListViewBuilder(service);
    }

    /**
     * Returns a builder to create a task type.
     *
     * @return the builder
     */
    public TestTaskTypeBuilder newTaskType() {
        return new TestTaskTypeBuilder(service);
    }

    /**
     * Returns a builder to create a task.
     *
     * @return the builder
     */
    public TestTaskBuilder newTask() {
        return new TestTaskBuilder(service);
    }

    /**
     * Creates and saves a new calendar event.
     *
     * @param calendar  the calendar
     * @param startTime the event start time
     * @param endTime   the event end time
     * @return a new event
     */
    public Act createCalendarEvent(Entity calendar, Date startTime, Date endTime) {
        return newCalendarEvent()
                .schedule(calendar)
                .startTime(startTime)
                .endTime(endTime)
                .build();
    }

    /**
     * Returns a builder to create a new calendar event.
     *
     * @return the builder
     */
    public TestCalendarEventBuilder newCalendarEvent() {
        return new TestCalendarEventBuilder(service);
    }

    /**
     * Returns a builder to create a new calendar block.
     *
     * @return the builder
     */
    public TestCalendarBlockBuilder newCalendarBlock() {
        return new TestCalendarBlockBuilder(service);
    }

    /**
     * Creates a calendar block type.
     *
     * @return the calendar block type
     */
    public Entity createCalendarBlockType() {
        return newCalenderBlockType().build();
    }

    /**
     * Returns a builder to create a calendar block type.
     *
     * @return the builder
     */
    public TestCalendarBlockTypeBuilder newCalenderBlockType() {
        return new TestCalendarBlockTypeBuilder(service);
    }
}
