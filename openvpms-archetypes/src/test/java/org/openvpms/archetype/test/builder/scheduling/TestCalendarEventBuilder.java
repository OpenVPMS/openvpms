/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>act.calendarEvent</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCalendarEventBuilder extends AbstractTestCalendarEventBuilder<TestCalendarEventBuilder> {

    /**
     * Constructs a {@link TestCalendarEventBuilder}.
     *
     * @param service the archetype service
     */
    public TestCalendarEventBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.CALENDAR_EVENT, service);
    }

}
