/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder to create <em>entity.cageType</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCageTypeBuilder extends AbstractTestEntityBuilder<Entity, TestCageTypeBuilder> {

    private Product firstPetProductDay;

    private Product firstPetProductNight;

    private Product secondPetProductDay;

    private Product secondPetProductNight;

    /**
     * Constructs a {@link TestCageTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestCageTypeBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.CAGE_TYPE, Entity.class, service);
        name(ValueStrategy.random("zcagetype"));
    }

    /**
     * Sets the first pet product, day rate.
     *
     * @param firstPetProductDay the product
     * @return this
     */
    public TestCageTypeBuilder firstPetProductDay(Product firstPetProductDay) {
        this.firstPetProductDay = firstPetProductDay;
        return this;
    }

    /**
     * Sets the first pet product, overnight rate.
     *
     * @param firstPetProductNight the product
     * @return this
     */
    public TestCageTypeBuilder firstPetProductNight(Product firstPetProductNight) {
        this.firstPetProductNight = firstPetProductNight;
        return this;
    }

    /**
     * Sets the second pet product, day rate.
     *
     * @param secondPetProductDay the product
     * @return this
     */
    public TestCageTypeBuilder secondPetProductDay(Product secondPetProductDay) {
        this.secondPetProductDay = secondPetProductDay;
        return this;
    }

    /**
     * Sets the second pet product, overnight rate.
     *
     * @param secondPetProductNight the product
     * @return this
     */
    public TestCageTypeBuilder secondPetProductNight(Product secondPetProductNight) {
        this.secondPetProductNight = secondPetProductNight;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (firstPetProductDay != null) {
            bean.setTarget("firstPetProductDay", firstPetProductDay);
        }
        if (firstPetProductNight != null) {
            bean.setTarget("firstPetProductNight", firstPetProductNight);
        }
        if (secondPetProductDay != null) {
            bean.setTarget("secondPetProductDay", secondPetProductDay);
        }
        if (secondPetProductNight != null) {
            bean.setTarget("secondPetProductNight", secondPetProductNight);
        }
    }
}