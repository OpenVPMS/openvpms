/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>act.customerAppointment</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAppointmentBuilder extends AbstractTestScheduleActBuilder<TestAppointmentBuilder> {

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The schedule.
     */
    private Entity schedule;

    /**
     * The appointment type.
     */
    private Entity appointmentType;

    /**
     * The arrival time.
     */
    private ValueStrategy arrivalTime = ValueStrategy.unset();

    /**
     * Determines if a reminder should be sent.
     */
    private ValueStrategy sendReminder = ValueStrategy.unset();

    /**
     * The date/time the reminder was sent.
     */
    private ValueStrategy reminderSent = ValueStrategy.unset();

    /**
     * The reminder error message.
     */
    private ValueStrategy reminderError = ValueStrategy.unset();

    /**
     * The task.
     */
    private Act task;

    /**
     * The event.
     */
    private Act event;

    /**
     * The SMS status.
     */
    private ValueStrategy smsStatus = ValueStrategy.unset();


    /**
     * Constructs a {@link TestAppointmentBuilder}.
     *
     * @param rules   the appointment rules
     * @param service the archetype service. Needs to be rules based to trigger appointment save rules
     */
    public TestAppointmentBuilder(AppointmentRules rules, IArchetypeRuleService service) {
        super(ScheduleArchetypes.APPOINTMENT, service);
        this.rules = rules;
    }

    /**
     * Constructs a {@link TestAppointmentBuilder}.
     *
     * @param appointment the appointment to update
     * @param rules       the appointment rules
     * @param service     the archetype service
     */
    public TestAppointmentBuilder(Act appointment, AppointmentRules rules, ArchetypeService service) {
        super(appointment, service);
        this.rules = rules;
    }

    /**
     * Sets the schedule.
     *
     * @param schedule the schedule
     * @return this
     */
    public TestAppointmentBuilder schedule(Entity schedule) {
        this.schedule = schedule;
        return this;
    }

    /**
     * Sets the appointment type.
     *
     * @param appointmentType the appointment type
     * @return this
     */
    public TestAppointmentBuilder appointmentType(Entity appointmentType) {
        this.appointmentType = appointmentType;
        return this;
    }

    /**
     * Sets the arrival time.
     *
     * @param arrivalTime the arrival time
     * @return this
     */
    public TestAppointmentBuilder arrivalTime(String arrivalTime) {
        return arrivalTime(parseDate(arrivalTime));
    }

    /**
     * Sets the arrival time.
     *
     * @param arrivalTime the arrival time
     * @return this
     */
    public TestAppointmentBuilder arrivalTime(Date arrivalTime) {
        this.arrivalTime = ValueStrategy.value(arrivalTime);
        return this;
    }

    /**
     * Determines if a reminder should be sent.
     *
     * @param sendReminder if {@code true}, send a reminder
     * @return this
     */
    public TestAppointmentBuilder sendReminder(boolean sendReminder) {
        this.sendReminder = ValueStrategy.value(sendReminder);
        return this;
    }

    /**
     * Sets the date/time when the reminder was sent.
     *
     * @param reminderSent the date/time when the reminder was sent
     * @return this
     */
    public TestAppointmentBuilder reminderSent(Date reminderSent) {
        this.reminderSent = ValueStrategy.value(reminderSent);
        return this;
    }

    /**
     * Sets the reminder error.
     *
     * @param reminderError the reminder error
     * @return this
     */
    public TestAppointmentBuilder reminderError(String reminderError) {
        this.reminderError = ValueStrategy.value(reminderError);
        return this;
    }

    /**
     * Sets the associated task.
     *
     * @param task the task
     * @return this
     */
    public TestAppointmentBuilder task(Act task) {
        this.task = task;
        return this;
    }

    /**
     * Sets the patient clinical event (aka the visit).
     *
     * @param event the event
     * @return this
     */
    public TestAppointmentBuilder event(Act event) {
        this.event = event;
        return this;
    }

    /**
     * Sets the SMS status.
     *
     * @param smsStatus the SMS status
     * @return this
     */
    public TestAppointmentBuilder smsStatus(String smsStatus) {
        this.smsStatus = ValueStrategy.value(smsStatus);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        Date startTime = object.getActivityStartTime();
        Date endTime = object.getActivityEndTime();
        if (endTime == null && startTime != null && schedule != null && appointmentType != null) {
            endTime = rules.calculateEndTime(startTime, schedule, appointmentType);
            object.setActivityEndTime(endTime);
        }
        if (schedule != null) {
            bean.setTarget("schedule", schedule);
        }
        if (appointmentType != null) {
            bean.setTarget("appointmentType", appointmentType);
        }
        arrivalTime.setValue(bean, "arrivalTime");
        sendReminder.setValue(bean, "sendReminder");
        reminderSent.setValue(bean, "reminderSent");
        reminderError.setValue(bean, "reminderError");
        if (task != null) {
            bean.addTarget("tasks", task, "appointments");
            toSave.add(task);
        }
        if (event != null) {
            bean.addTarget("event", event, "appointment");
            toSave.add(event);
        }
        smsStatus.setValue(bean, "smsStatus");
    }
}