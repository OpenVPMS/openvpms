/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.HL7ServiceLaboratoryGroup</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestHL7LaboratoryGroupBuilder extends AbstractTestEntityBuilder<Entity, TestHL7LaboratoryGroupBuilder> {

    /**
     * The laboratories in the group.
     */
    private Entity[] laboratories;

    /**
     * Constructs an {@link TestHL7LaboratoryGroupBuilder}.
     *
     * @param service the archetype service
     */
    public TestHL7LaboratoryGroupBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.HL7_LABORATORY_GROUP, Entity.class, service);
        name(ValueStrategy.random("zhl7group"));
    }

    /**
     * Sets the laboratories in the group.
     *
     * @param laboratories the laboratories
     * @return this
     */
    public TestHL7LaboratoryGroupBuilder laboratories(Entity... laboratories) {
        this.laboratories = laboratories;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (laboratories != null) {
            for (Entity laboratory : laboratories) {
                bean.addTarget("services", laboratory);
            }
        }
    }
}
