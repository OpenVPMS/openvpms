/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.organisationScheduleView</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestScheduleViewBuilder extends AbstractTestEntityBuilder<Entity, TestScheduleViewBuilder> {

    /**
     * The schedules.
     */
    private Entity[] schedules;

    /**
     * Constructs an {@link TestScheduleViewBuilder}.
     *
     * @param service the archetype service
     */
    public TestScheduleViewBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.SCHEDULE_VIEW, Entity.class, service);
        name(ValueStrategy.random("zscheduleview"));
    }

    /**
     * Sets the available schedules.
     *
     * @param schedules the schedules
     * @return this
     */
    public TestScheduleViewBuilder schedules(Entity... schedules) {
        this.schedules = schedules;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (schedules != null) {
            int sequence = bean.getValues("schedules").size();
            for (Entity schedule : schedules) {
                EntityRelationship relationship = (EntityRelationship) bean.addTarget("schedules", schedule, "views");
                relationship.setSequence(sequence++);
                toSave.add(schedule);
            }
        }
    }
}