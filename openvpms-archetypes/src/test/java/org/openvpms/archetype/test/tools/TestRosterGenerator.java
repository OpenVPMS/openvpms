/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.tools;

import org.apache.commons.lang3.RandomUtils;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.roster.RosterArchetypes;
import org.openvpms.archetype.rules.workflow.roster.RosterSyncStatus;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Generates <em>act.rosterEvent</em> acts for 2 weeks starting today, for each roster area.
 *
 * @author Tim Anderson
 */
public class TestRosterGenerator {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestRosterGenerator}.
     *
     * @param service the archetype service
     */
    public TestRosterGenerator(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Generates roster events for existing roster areas for the next two weeks for each active user.
     */
    public void generate() {
        List<User> users = getEntities(UserArchetypes.USER, User.class);
        List<Entity> areas = getEntities(RosterArchetypes.ROSTER_AREA, Entity.class);
        if (users.isEmpty()) {
            throw new IllegalStateException("Need at least one active user");
        }
        if (areas.isEmpty()) {
            throw new IllegalStateException("Need at least one active roster area");
        }
        Date date = DateRules.getToday();
        for (int i = 0; i < 14; ++i) {
            for (Entity area : areas) {
                System.out.println("Generating roster for " + area.getName() + " on " + date);
                IMObjectBean bean = service.getBean(area);
                Party location = bean.getTarget("location", Party.class);
                for (User user : users) {
                    String status = getStatus();
                    String error = null;
                    if (RosterSyncStatus.ERROR.equals(status)) {
                        error = "A sync error";
                    }
                    createEvent(area, location, user, date, status, error);
                }
            }
            date = DateRules.getNextDate(date);
        }
    }

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String contextPath = "openvpms-archetypes-test-context.xml";
        ApplicationContext context;
        if (!new File(contextPath).exists()) {
            context = new ClassPathXmlApplicationContext(contextPath);
        } else {
            context = new FileSystemXmlApplicationContext(contextPath);
        }
        TestRosterGenerator generator = new TestRosterGenerator(context.getBean(IArchetypeRuleService.class));
        generator.generate();
    }

    /**
     * Generates a random synchronisation status.
     *
     * @return the status
     */
    private String getStatus() {
        String result;
        int i = RandomUtils.nextInt(0, 3);
        switch (i) {
            case 0:
                result = RosterSyncStatus.PENDING;
                break;
            case 1:
                result = RosterSyncStatus.SYNC;
                break;
            default:
                result = RosterSyncStatus.ERROR;
                break;
        }
        return result;
    }

    /**
     * Creates a roster event.
     *
     * @param area     the roster area
     * @param location the practice location
     * @param user     the user
     * @param date     the date
     * @param status   the sync status
     * @param error    the sync error. May be {@code null}
     */
    private void createEvent(Entity area, Party location, User user, Date date, String status, String error) {
        Date startTime = DateRules.getDate(date, 8, DateUnits.HOURS);
        Date endTime = DateRules.getDate(date, 17, DateUnits.HOURS);
        ActIdentity identity = createIdentity(status, error);
        Act act = service.create(ScheduleArchetypes.ROSTER_EVENT, Act.class);
        act.addIdentity(identity);
        IMObjectBean bean = service.getBean(act);
        bean.setValue("startTime", startTime);
        bean.setValue("endTime", endTime);
        bean.setTarget("user", user);
        bean.setTarget("schedule", area);
        bean.setTarget("location", location);
        bean.save();
    }

    /**
     * Creates a synchronisation id.
     *
     * @param status the synchronisation status
     * @param error  the synchronisation error. May be {@code null}
     * @return the id
     */
    private ActIdentity createIdentity(String status, String error) {
        ActIdentity identity = service.create("actIdentity.syncTest", ActIdentity.class);
        IMObjectBean bean = service.getBean(identity);
        bean.setValue("status", status);
        bean.setValue("error", error);
        bean.setValue("identity", UUID.randomUUID().toString());
        return identity;
    }

    /**
     * Returns all active objects of the specified archetype.
     *
     * @param archetype the archetype
     * @param type      the type
     * @return the matching objects
     */
    private <T extends IMObject> List<T> getEntities(String archetype, Class<T> type) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(type);
        Root<T> from = query.from(type, archetype);
        query.where(builder.equal(from.get("active"), true));
        query.orderBy(builder.asc(from.get("id")));
        return service.createQuery(query).getResultList();
    }
}