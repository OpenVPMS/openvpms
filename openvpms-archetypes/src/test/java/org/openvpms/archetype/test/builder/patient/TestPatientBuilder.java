/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.entity.TestEntityIdentityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.patientpet</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientBuilder extends AbstractTestPartyBuilder<Party, TestPatientBuilder> {

    /**
     * Male patient.
     */
    public static final String SEX_MALE = PatientRules.SEX_MALE;

    /**
     * Female patient.
     */
    public static final String SEX_FEMALE = PatientRules.SEX_FEMALE;

    /**
     * Unspecified sex patient.
     */
    public static final String SEX_UNSPECIFIED = PatientRules.SEX_UNSPECIFIED;

    /**
     * The microchips.
     */
    private final List<String> microchips = new ArrayList<>();

    /**
     * The pet tags.
     */
    private final List<String> petTags = new ArrayList<>();

    /**
     * The rabies tags.
     */
    private final List<String> rabiesTags = new ArrayList<>();

    /**
     * The aliases.
     */
    private final List<String> aliases = new ArrayList<>();

    /**
     * The patient-owner relationships to add.
     */
    private final List<Owner> owners = new ArrayList<>();

    /**
     * The referrals.
     */
    private final List<Referral> referrals = new ArrayList<>();

    /**
     * The patient species.
     */
    private ValueStrategy species = ValueStrategy.unset();

    /**
     * The patient breed.
     */
    private ValueStrategy breed = ValueStrategy.unset();

    /**
     * The new breed.
     */
    private ValueStrategy newBreed = ValueStrategy.unset();

    /**
     * The patient date of birth.
     */
    private ValueStrategy dateOfBirth = ValueStrategy.unset();

    /**
     * The patient sex.
     */
    private ValueStrategy sex = ValueStrategy.unset();

    /**
     * Determines if the patient has been desexed.
     */
    private ValueStrategy desexed = ValueStrategy.unset();

    /**
     * Determines if the patient is deceased.
     */
    private ValueStrategy deceased = ValueStrategy.unset();

    /**
     * The patient date of death.
     */
    private ValueStrategy dateOfDeath = ValueStrategy.unset();

    /**
     * The patient colour.
     */
    private ValueStrategy colour = ValueStrategy.unset();

    /**
     * The discounts.
     */
    private Entity[] discounts;

    /**
     * Constructs a {@link TestPatientBuilder}.
     *
     * @param service the archetype service
     */
    public TestPatientBuilder(ArchetypeService service) {
        super(PatientArchetypes.PATIENT, Party.class, service);
        name(ValueStrategy.random("Spot-"));
        species("CANINE");
    }

    /**
     * Constructs a {@link TestPatientBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestPatientBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the patient species.
     * <p/>
     * This is the code for an <em>lookup.species</em>.
     * <p/>
     * If no species is specified, "CANINE" will be used.
     * <p/>
     * If the species does not exist, it will be created.
     *
     * @param species the patient species
     * @return this
     */
    public TestPatientBuilder species(String species) {
        this.species = ValueStrategy.value(species);
        return this;
    }

    /**
     * Sets the patient breed.
     * <p/>
     * This is the code for an <em>lookup.breed</em>.
     *
     * @param breed the patient breed
     * @return this
     */
    public TestPatientBuilder breed(String breed) {
        this.breed = ValueStrategy.value(breed);
        return this;
    }

    /**
     * Sets the patient breed, when no breed lookup is available.
     *
     * @param newBreed the new breed
     * @return this
     */
    public TestPatientBuilder newBreed(String newBreed) {
        this.newBreed = ValueStrategy.value(newBreed);
        return this;
    }

    /**
     * Sets the patient sex to {@link #SEX_MALE}.
     *
     * @return this
     */
    public TestPatientBuilder male() {
        return sex(SEX_MALE);
    }

    /**
     * Sets the patient sex to {@link #SEX_FEMALE}.
     *
     * @return this
     */
    public TestPatientBuilder female() {
        return sex(SEX_FEMALE);
    }

    /**
     * Sets the patient sex to {@link #SEX_UNSPECIFIED}.
     *
     * @return this
     */
    public TestPatientBuilder unspecifiedSex() {
        return sex(SEX_UNSPECIFIED);
    }

    /**
     * Sets the patient sex.
     *
     * @param sex the patient sex
     * @return this
     */
    public TestPatientBuilder sex(String sex) {
        this.sex = ValueStrategy.value(sex);
        return this;
    }

    /**
     * Determines if the patient has been desexed or not.
     *
     * @param desexed if {@code true}, the patient has been desexed
     * @return this
     */
    public TestPatientBuilder desexed(boolean desexed) {
        this.desexed = ValueStrategy.value(desexed);
        return this;
    }

    /**
     * Sets the patient date-of-birth.
     *
     * @param dateOfBirth the date of birth
     * @return this
     */
    public TestPatientBuilder dateOfBirth(String dateOfBirth) {
        return dateOfBirth(parseDate(dateOfBirth));
    }

    /**
     * Sets the patient date-of-birth.
     *
     * @param dateOfBirth the date of birth
     * @return this
     */
    public TestPatientBuilder dateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = ValueStrategy.value(dateOfBirth);
        return this;
    }

    /**
     * Sets the patient date-of-birth, given their age as at the current date.
     *
     * @param age   the age
     * @param units the age units
     * @return this
     */
    public TestPatientBuilder age(int age, DateUnits units) {
        return dateOfBirth(DateRules.getDate(DateRules.getToday(), -age, units));
    }

    /**
     * Determines if the patient is deceased.
     *
     * @param deceased if {@code true}, the patient is deceased
     * @return this
     */
    public TestPatientBuilder deceased(boolean deceased) {
        this.deceased = ValueStrategy.value(deceased);
        return this;
    }

    /**
     * Sets the patient date of death.
     *
     * @param dateOfDeath the date of death
     * @return this
     */
    public TestPatientBuilder dateOfDeath(String dateOfDeath) {
        return dateOfDeath(parseDate(dateOfDeath));
    }

    /**
     * Sets the patient date of death.
     *
     * @param dateOfDeath the date of death
     * @return this
     */
    public TestPatientBuilder dateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = ValueStrategy.value(dateOfDeath);
        return this;
    }

    /**
     * Sets the patient colour.
     *
     * @param colour the patient colour
     * @return this
     */
    public TestPatientBuilder colour(String colour) {
        this.colour = ValueStrategy.value(colour);
        return this;
    }

    /**
     * Sets the patient owner.
     *
     * @param owner the owner
     * @return this
     */
    public TestPatientBuilder owner(Party owner) {
        return addOwner(owner, new Date(), null);
    }

    /**
     * Adds a patient owner relationship.
     *
     * @param owner     the owner
     * @param startTime the ownership start-time
     * @param endTime   the ownership end-time
     * @return this
     */
    public TestPatientBuilder addOwner(Party owner, Date startTime, Date endTime) {
        owners.add(new Owner(owner, startTime, endTime));
        return this;
    }

    /**
     * Adds a microchip.
     *
     * @param microchip the microchip
     * @return this
     */
    public TestPatientBuilder addMicrochip(String microchip) {
        microchips.add(microchip);
        return this;
    }

    /**
     * Adds a pet tag.
     *
     * @param petTag the pet tag
     * @return this
     */
    public TestPatientBuilder addPetTag(String petTag) {
        petTags.add(petTag);
        return this;
    }

    /**
     * Adds a rabies tag.
     *
     * @param rabiesTag the rabies tag
     * @return this
     */
    public TestPatientBuilder addRabiesTag(String rabiesTag) {
        rabiesTags.add(rabiesTag);
        return this;
    }

    /**
     * Adds an alias.
     *
     * @param alias the alias
     * @return this
     */
    public TestPatientBuilder addAlias(String alias) {
        aliases.add(alias);
        return this;
    }

    /**
     * Adds patient discounts.
     *
     * @param discounts the discounts
     */
    public TestPatientBuilder addDiscounts(Entity... discounts) {
        this.discounts = discounts;
        return this;
    }

    /**
     * Adds a referred-from vet relationship.
     *
     * @param vet the vet
     * @return this
     */
    public TestPatientBuilder addReferredFrom(Party vet) {
        return addReferredFrom(vet, new Date());
    }

    /**
     * Adds a referred-from vet relationship.
     *
     * @param vet   the vet
     * @param start the date when the relationship starts
     * @return this
     */
    public TestPatientBuilder addReferredFrom(Party vet, Date start) {
        return addReferredFrom(vet, start, null);
    }

    /**
     * Adds a referred-from vet relationship.
     *
     * @param vet   the vet
     * @param start the date when the relationship starts
     * @return this
     */
    public TestPatientBuilder addReferredFrom(Party vet, Date start, String reason) {
        referrals.add(new Referral(vet, true, start, reason));
        return this;
    }

    /**
     * Adds a referred-to vet relationship.
     *
     * @param vet the vet
     * @return this
     */
    public TestPatientBuilder addReferredTo(Party vet) {
        return addReferredTo(vet, new Date());
    }

    /**
     * Adds a referred-to vet relationship.
     *
     * @param vet   the vet
     * @param start the date when the relationship starts
     * @return this
     */
    public TestPatientBuilder addReferredTo(Party vet, Date start) {
        return addReferredTo(vet, start, null);
    }

    /**
     * Adds a referred-to vet relationship.
     *
     * @param vet   the vet
     * @param start the date when the relationship starts
     * @return this
     */
    public TestPatientBuilder addReferredTo(Party vet, Date start, String reason) {
        referrals.add(new Referral(vet, false, start, reason));
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        ArchetypeService service = getService();
        Lookup speciesLookup = null;
        if (species.isSet()) {
            TestLookupBuilder speciesBuilder = new TestLookupBuilder(PatientArchetypes.SPECIES, service);
            speciesLookup = speciesBuilder.code(species).build();
            bean.setValue("species", speciesLookup.getCode());
        }

        if (breed.isSet()) {
            if (breed.getValue() != null) {
                if (speciesLookup == null) {
                    speciesLookup = bean.getLookup("species");
                }
                TestLookupBuilder breedBuilder = new TestLookupBuilder(PatientArchetypes.BREED, service);
                Lookup breedLookup = breedBuilder.code(breed).source(speciesLookup).build();
                bean.setValue("breed", breedLookup.getCode());
            } else {
                breed.setValue(bean, "breed");
            }
        }
        newBreed.setValue(bean, "newBreed");
        sex.setValue(bean, "sex");
        desexed.setValue(bean, "desexed");
        dateOfBirth.setValue(bean, "dateOfBirth");
        deceased.setValue(bean, "deceased");
        dateOfDeath.setValue(bean, "deceasedDate");
        colour.setValue(bean, "colour");
        if (!owners.isEmpty()) {
            for (Owner owner : owners) {
                IMObjectBean ownerBean = service.getBean(owner.owner);
                EntityRelationship relationship = (EntityRelationship) ownerBean.addTarget(
                        "patients", PatientArchetypes.PATIENT_OWNER, object, "customers");
                relationship.setActiveStartTime(owner.startTime);
                relationship.setActiveEndTime(owner.endTime);
                toSave.add(owner.owner);
            }
            setEndDates(bean.getValues("customers", PeriodRelationship.class));
        }

        for (String microchip : microchips) {
            EntityIdentity identity = new TestEntityIdentityBuilder(PatientArchetypes.MICROCHIP, getService())
                    .identity(microchip).build();
            object.addIdentity(identity);
        }

        for (String petTag : petTags) {
            EntityIdentity identity = new TestEntityIdentityBuilder(PatientArchetypes.PET_TAG, getService())
                    .identity(petTag).build();
            object.addIdentity(identity);
        }
        for (String rabiesTag : rabiesTags) {
            EntityIdentity identity = new TestEntityIdentityBuilder(PatientArchetypes.RABIES_TAG, getService())
                    .identity(rabiesTag).build();
            object.addIdentity(identity);
        }

        for (String alias : aliases) {
            EntityIdentity identity = new TestEntityIdentityBuilder("entityIdentity.alias", getService())
                    .identity(alias).build();
            object.addIdentity(identity);
        }

        if (discounts != null) {
            for (Entity discount : discounts) {
                bean.addTarget("discounts", discount);
            }
        }

        if (!referrals.isEmpty()) {
            for (Referral referral : referrals) {
                String archetype = referral.from ? PatientArchetypes.REFERRED_FROM : PatientArchetypes.REFERRED_TO;
                Party vet = referral.vet;
                EntityRelationship relationship = (EntityRelationship) bean.addTarget("referrals", archetype, vet);
                relationship.setActiveStartTime(referral.start);
                getBean(relationship).setValue("reason", referral.reason);
                vet.addEntityRelationship(relationship);
                toSave.add(vet);
            }
            setEndDates(bean.getValues("referrals", PeriodRelationship.class));
        }
    }

    private static class Owner {

        private final Party owner;

        private final Date startTime;

        private final Date endTime;

        public Owner(Party owner, Date startTime, Date endTime) {
            this.owner = owner;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }

    private static class Referral {

        private final Party vet;

        private final boolean from;

        private final Date start;

        private final String reason;

        public Referral(Party vet, boolean from, Date start, String reason) {
            this.vet = vet;
            this.from = from;
            this.start = start;
            this.reason = reason;
        }
    }
}
