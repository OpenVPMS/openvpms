/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.junit.Assert.assertEquals;

/**
 * Verifies an image document matches that expected.
 *
 * @author Tim Anderson
 */
public class TestImageDocumentVerifier extends TestDocumentVerifier<TestImageDocumentVerifier> {

    /**
     * The expected width.
     */
    private int width;

    /**
     * The expected height.
     */
    private int height;

    /**
     * Constructs a {@link DefaultTestDocumentVerifier}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     */
    public TestImageDocumentVerifier(DocumentHandlers handlers, ArchetypeService service) {
        super(handlers, service);
        archetype(DocumentArchetypes.IMAGE_DOCUMENT);
    }

    /**
     * Sets the expected width.
     *
     * @param width the width
     * @return this
     */
    public TestImageDocumentVerifier width(int width) {
        this.width = width;
        return this;
    }

    /**
     * Sets the expected height.
     *
     * @param height the height
     * @return this
     */
    public TestImageDocumentVerifier height(int height) {
        this.height = height;
        return this;
    }

    /**
     * Initialises this from a document.
     *
     * @param document the document
     * @return this
     */
    @Override
    public TestImageDocumentVerifier initialise(Document document) {
        super.initialise(document);
        IMObjectBean bean = getService().getBean(document);
        width(bean.getInt("width"));
        return height(bean.getInt("height"));
    }

    /**
     * Verifies a document matches that expected.
     *
     * @param document the document
     */
    @Override
    public void verify(Document document) {
        super.verify(document);
        IMObjectBean bean = getService().getBean(document);
        assertEquals(width, bean.getInt("width"));
        assertEquals(height, bean.getInt("height"));
    }
}
