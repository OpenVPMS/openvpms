/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Verifies <em>act.supplierOrderItem</em>, <em>act.supplierDeliveryItem</em> and <em>act.supplierReturnItem</em>
 * instances match that expected, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSupplierActItemVerifier<B extends TestSupplierActItemVerifier<B>> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected product.
     */
    private Reference product;

    /**
     * The expected reorder code.
     */
    private String reorderCode;

    /**
     * The expected reorder description.
     */
    private String reorderDescription;

    /**
     * The expected package size.
     */
    private Integer packageSize;

    /**
     * The expected package units.
     */
    private String packageUnits;

    /**
     * The expected quantity.
     */
    private BigDecimal quantity;

    /**
     * The expected unit price.
     */
    private BigDecimal unitPrice;

    /**
     * The expected list price.
     */
    private BigDecimal listPrice;

    /**
     * The expected tax.
     */
    private BigDecimal tax;

    /**
     * The expected total.
     */
    private BigDecimal total;

    /**
     * Constructs a {@link TestSupplierActItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestSupplierActItemVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Initialises this from an item.
     *
     * @param item the item
     * @return this
     */
    public B initialise(FinancialAct item) {
        IMObjectBean bean = service.getBean(item);
        initialise(bean);
        return getThis();
    }

    /**
     * Sets the expected product.
     *
     * @param product the product
     * @return this
     */
    public B product(Product product) {
        return product((product != null) ? product.getObjectReference() : null);
    }

    /**
     * Sets the expected product.
     *
     * @param product the product
     * @return this
     */
    public B product(Reference product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the expected reorder code.
     *
     * @param reorderCode the reorder code
     * @return this
     */
    public B reorderCode(String reorderCode) {
        this.reorderCode = reorderCode;
        return getThis();
    }

    /**
     * Sets the expected reorder description.
     *
     * @param reorderDescription the reorder description
     * @return this
     */
    public B reorderDescription(String reorderDescription) {
        this.reorderDescription = reorderDescription;
        return getThis();
    }

    /**
     * Sets the expected package size.
     *
     * @param packageSize the package size
     * @return this
     */
    public B packageSize(int packageSize) {
        this.packageSize = packageSize;
        return getThis();
    }

    /**
     * Sets the expected package units.
     *
     * @param packageUnits the package units
     * @return this
     */
    public B packageUnits(String packageUnits) {
        this.packageUnits = packageUnits;
        return getThis();
    }

    /**
     * Sets the expected quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the expected quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return getThis();
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return getThis();
    }

    /**
     * Sets the expected list price.
     *
     * @param listPrice the list price
     * @return this
     */
    public B listPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
        return getThis();
    }

    /**
     * Sets the expected tax.
     *
     * @param tax the tax
     * @return this
     */
    public B tax(BigDecimal tax) {
        this.tax = tax;
        return getThis();
    }

    /**
     * Sets the expected total.
     *
     * @param total the tax
     * @return this
     */
    public B total(BigDecimal total) {
        this.total = total;
        return getThis();
    }

    /**
     * Verifies there is an item that matches the criteria.
     *
     * @param items items
     * @return the matching item
     */
    public FinancialAct verify(List<FinancialAct> items) {
        FinancialAct item = items.stream().filter(act -> {
                    IMObjectBean bean = service.getBean(act);
                    return Objects.equals(product, bean.getTargetRef("product"));
                }).findFirst()
                .orElse(null);
        if (item == null) {
            fail("Failed to find item for product=" + product);
        }
        verify(item);
        return item;
    }

    /**
     * Verifies an item matches that expected.
     *
     * @param item the item
     */
    public void verify(FinancialAct item) {
        IMObjectBean bean = service.getBean(item);
        verify(bean);
    }

    /**
     * Initialises this from an item.
     *
     * @param bean the bean wrapping the item
     */
    protected void initialise(IMObjectBean bean) {
        product(bean.getTargetRef("product"));
        reorderCode(bean.getString("reorderCode"));
        reorderDescription(bean.getString("reorderDescription"));
        packageSize(bean.getInt("packageSize"));
        packageUnits(bean.getString("packageUnits"));
        quantity(bean.getBigDecimal("quantity"));
        unitPrice(bean.getBigDecimal("unitPrice"));
        listPrice(bean.getBigDecimal("listPrice"));
        tax(bean.getBigDecimal("tax"));
        total(bean.getBigDecimal("total"));
    }

    /**
     * Verifies an item matches that expected.
     *
     * @param bean the bean wrapping the item
     */
    protected void verify(IMObjectBean bean) {
        assertEquals(product, bean.getTargetRef("product"));
        assertEquals(reorderCode, bean.getString("reorderCode"));
        assertEquals(reorderDescription, bean.getString("reorderDescription"));
        assertEquals(packageSize, bean.getValue("packageSize"));
        assertEquals(packageUnits, bean.getString("packageUnits"));
        checkEquals(quantity, bean.getBigDecimal("quantity"));
        checkEquals(unitPrice, bean.getBigDecimal("unitPrice"));
        checkEquals(listPrice, bean.getBigDecimal("listPrice"));
        checkEquals(tax, bean.getBigDecimal("tax"));
        checkEquals(total, bean.getBigDecimal("total"));
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected B getThis() {
        return (B) this;
    }
}
