/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>security.role</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestRoleBuilder extends AbstractTestIMObjectBuilder<SecurityRole, TestRoleBuilder> {

    /**
     * The role authorities.
     */
    private ArchetypeAwareGrantedAuthority[] authorities;

    /**
     * Constructs a {@link TestRoleBuilder}.
     *
     * @param service the archetype service
     */
    public TestRoleBuilder(ArchetypeService service) {
        super(UserArchetypes.ROLE, SecurityRole.class, service);
        name(ValueStrategy.random("zrole"));
    }

    /**
     * Sets the role authorities.
     *
     * @param authorities the authorities
     * @return this
     */
    public TestRoleBuilder authorities(ArchetypeAwareGrantedAuthority... authorities) {
        this.authorities = authorities;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(SecurityRole object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (authorities != null) {
            for (ArchetypeAwareGrantedAuthority authority :authorities) {
                object.addAuthority(authority);
            }
        }
    }
}