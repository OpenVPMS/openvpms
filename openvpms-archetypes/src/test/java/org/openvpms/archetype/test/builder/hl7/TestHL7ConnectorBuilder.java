/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.hl7;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.HL7SenderMLLP</em> and <em>entity.HL7ReceiverMLLP</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestHL7ConnectorBuilder<B extends TestHL7ConnectorBuilder<B>>
        extends AbstractTestEntityBuilder<Entity, B> {

    /**
     * The port.
     */
    private ValueStrategy port = ValueStrategy.unset();

    /**
     * The sending application.
     */
    private ValueStrategy sendingApplication = ValueStrategy.unset();

    /**
     * The sending facility.
     */
    private ValueStrategy sendingFacility = ValueStrategy.unset();

    /**
     * The receiving application.
     */
    private ValueStrategy receivingApplication = ValueStrategy.unset();

    /**
     * The receiving facility.
     */
    private ValueStrategy receivingFacility = ValueStrategy.unset();

    /**
     * The mapping.
     */
    private Entity mapping;

    /**
     * Constructs a {@link TestHL7ConnectorBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestHL7ConnectorBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
    }

    /**
     * Sets the port.
     *
     * @param port the port
     * @return this
     */
    public B port(int port) {
        this.port = ValueStrategy.value(port);
        return getThis();
    }

    /**
     * Sets the sending application.
     *
     * @param sendingApplication the sending application
     * @return this
     */
    public B sendingApplication(String sendingApplication) {
        this.sendingApplication = ValueStrategy.value(sendingApplication);
        return getThis();
    }

    /**
     * Sets the sending facility.
     *
     * @param sendingFacility the sending facility
     * @return this
     */
    public B sendingFacility(String sendingFacility) {
        this.sendingFacility = ValueStrategy.value(sendingFacility);
        return getThis();
    }

    /**
     * Sets the receiving application.
     *
     * @param receivingApplication the receiving application
     * @return this
     */
    public B receivingApplication(String receivingApplication) {
        this.receivingApplication = ValueStrategy.value(receivingApplication);
        return getThis();
    }

    /**
     * Sets the receiving facility.
     *
     * @param receivingFacility the receiving facility
     * @return this
     */
    public B receivingFacility(String receivingFacility) {
        this.receivingFacility = ValueStrategy.value(receivingFacility);
        return getThis();
    }

    /**
     * Sets the mapping.
     *
     * @param mapping the mapping
     * @return this
     */
    public B mapping(Entity mapping) {
        this.mapping = mapping;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        port.setValue(bean, "port");
        sendingApplication.setValue(bean, "sendingApplication");
        sendingFacility.setValue(bean, "sendingFacility");
        receivingApplication.setValue(bean, "receivingApplication");
        receivingFacility.setValue(bean, "receivingFacility");
        if (mapping != null) {
            bean.setTarget("mapping", mapping);
        }
    }
}