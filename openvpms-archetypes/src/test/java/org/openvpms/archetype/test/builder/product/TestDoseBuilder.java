/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <em>entity.productDose</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDoseBuilder extends AbstractTestEntityBuilder<Entity, TestDoseBuilder> {

    /**
     * The species code.
     */
    private String species;

    /**
     * The minimum weight.
     */
    private BigDecimal minWeight;

    /**
     * The maximum weight.
     */
    private BigDecimal maxWeight;

    /**
     * The rate.
     */
    private BigDecimal rate;

    /**
     * The quantity.
     */
    private BigDecimal quantity;

    /**
     * The no. of decimal places to round to.
     */
    private Integer roundTo;

    /**
     * The parent builder.
     */
    private TestMedicationProductBuilder parent;

    /**
     * Constructs a {@link TestDoseBuilder}.
     *
     * @param parent  the parent medication
     * @param service the archetype service
     */
    public TestDoseBuilder(TestMedicationProductBuilder parent, ArchetypeService service) {
        super(ProductArchetypes.DOSE, Entity.class, service);
        this.parent = parent;
    }

    /**
     * Sets the species that the dose applies to.
     *
     * @param species the species code
     * @return this
     */
    public TestDoseBuilder species(String species) {
        this.species = species;
        return this;
    }

    /**
     * Sets the minimum patient weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @return this
     */
    public TestDoseBuilder minWeight(int minWeight) {
        return minWeight(BigDecimal.valueOf(minWeight));
    }

    /**
     * Sets the minimum patient weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @return this
     */
    public TestDoseBuilder minWeight(BigDecimal minWeight) {
        this.minWeight = minWeight;
        return this;
    }

    /**
     * Sets the maximum patient weight.
     *
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestDoseBuilder maxWeight(int maxWeight) {
        return maxWeight(BigDecimal.valueOf(maxWeight));
    }

    /**
     * Sets the maximum patient weight.
     *
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestDoseBuilder maxWeight(BigDecimal maxWeight) {
        this.maxWeight = maxWeight;
        return this;
    }

    /**
     * Sets the patient weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestDoseBuilder weightRange(int minWeight, int maxWeight) {
        return weightRange(BigDecimal.valueOf(minWeight), BigDecimal.valueOf(maxWeight));
    }

    /**
     * Sets the patient weight.
     *
     * @param minWeight the minimum weight, inclusive
     * @param maxWeight the maximum weight, exclusive
     * @return this
     */
    public TestDoseBuilder weightRange(BigDecimal minWeight, BigDecimal maxWeight) {
        minWeight(minWeight);
        return maxWeight(maxWeight);
    }


    /**
     * Sets the rate.
     *
     * @param rate the rate
     * @return this
     */
    public TestDoseBuilder rate(int rate) {
        return rate(BigDecimal.valueOf(rate));
    }

    /**
     * Sets the rate.
     *
     * @param rate the rate
     * @return this
     */
    public TestDoseBuilder rate(BigDecimal rate) {
        this.rate = rate;
        return this;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestDoseBuilder quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestDoseBuilder quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * Sets the no. of decimal places to round to.
     *
     * @param roundTo the no. of decimal places to round to
     * @return this
     */
    public TestDoseBuilder roundTo(int roundTo) {
        this.roundTo = roundTo;
        return this;
    }

    /**
     * Builds the dose, and adds it to the medication.
     *
     * @return the medication builder
     */
    public TestMedicationProductBuilder add() {
        return parent.addDose(build(false));
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (species != null) {
            object.addClassification(new TestLookupFactory(getService()).getSpecies(species));
        }
        if (minWeight != null) {
            bean.setValue("minWeight", minWeight);
        }
        if (maxWeight != null) {
            bean.setValue("maxWeight", maxWeight);
        }
        if (rate != null) {
            bean.setValue("rate", rate);
        }
        if (quantity != null) {
            bean.setValue("quantity", quantity);
        }
        if (roundTo != null) {
            bean.setValue("roundTo", roundTo);
        }
    }
}
