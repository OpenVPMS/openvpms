/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerEstimate</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEstimateBuilder extends AbstractTestActBuilder<Act, TestEstimateBuilder> {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * The estimate items.
     */
    private final List<Act> items = new ArrayList<>();

    /**
     * The build items.
     */
    private List<Act> builtItems = new ArrayList<>();

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Constructs an {@link TestEstimateBuilder}.
     *
     * @param service the archetype service
     */
    public TestEstimateBuilder(ArchetypeService service, CustomerAccountRules rules) {
        super(EstimateArchetypes.ESTIMATE, Act.class, service);
        this.rules = rules;
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public TestEstimateBuilder customer(Party customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public TestEstimateBuilder clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Adds an estimate item.
     *
     * @param item the item to add
     * @return this
     */
    public TestEstimateBuilder add(Act item) {
        items.add(item);
        return this;
    }

    /**
     * Returns a builder to add an estimate item.
     *
     * @return a charge item builder
     */
    public TestEstimateItemBuilder item() {
        return new TestEstimateItemBuilder(this, getService(), rules);
    }

    /**
     * Returns the built items.
     *
     * @return the built items
     */
    public List<Act> getItems() {
        return builtItems;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        BigDecimal lowTotal = BigDecimal.ZERO;
        BigDecimal highTotal = BigDecimal.ZERO;
        int sequence = 0;
        for (Act item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            relationship.setSequence(sequence++);
            item.addActRelationship(relationship);
            toSave.add(item);
            IMObjectBean itemBean = bean.getBean(item);
            highTotal = highTotal.add(itemBean.getBigDecimal("highTotal"));
            lowTotal = lowTotal.add(itemBean.getBigDecimal("lowTotal"));
        }
        bean.setValue("highTotal", highTotal);
        bean.setValue("lowTotal", lowTotal);
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }
}
