/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for breed lookups, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestBreedBuilder extends AbstractTestLookupBuilder<TestBreedBuilder> {

    /**
     * Constructs a {@link TestBreedBuilder}.
     *
     * @param service the archetype service
     */
    public TestBreedBuilder(ArchetypeService service) {
        super(PatientArchetypes.BREED, service);
    }

    /**
     * Sets the species this breed is associated with.
     *
     * @param species the species
     * @return this
     */
    public TestBreedBuilder species(Lookup species) {
        return source(species);
    }
}
