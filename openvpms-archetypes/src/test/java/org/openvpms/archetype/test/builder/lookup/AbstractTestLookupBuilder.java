/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>lookup.*</em> instance, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestLookupBuilder<B extends AbstractTestLookupBuilder<B>>
        extends AbstractTestIMObjectBuilder<Lookup, B> {

    /**
     * The lookup code.
     */
    private ValueStrategy code;

    /**
     * Determines if the lookup code must be unique.
     */
    private boolean uniqueCode;

    /**
     * The code to use.
     */
    private String builtCode;

    /**
     * The source lookup.
     */
    private Lookup source;

    /**
     * Determines if the lookup is the default.
     */
    private Boolean isDefault;

    /**
     * Constructs an {@link AbstractTestLookupBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestLookupBuilder(String archetype, ArchetypeService service) {
        super(archetype, Lookup.class, service);
    }

    /**
     * Sets the lookup code.
     *
     * @param code the lookup code
     * @return this
     */
    public B code(String code) {
        return code(ValueStrategy.value(code));
    }

    /**
     * Sets the lookup code.
     *
     * @param code the lookup code
     * @return this
     */
    public B code(ValueStrategy code) {
        this.code = ValueStrategy.value(code);
        this.uniqueCode = false;
        return getThis();
    }

    /**
     * Generates a unique code for the lookup.
     * <p/>
     * This ensures that a new lookup will be generated.
     *
     * @return this
     */
    public B uniqueCode() {
        return uniqueCode("");
    }

    /**
     * Generates a unique code for the lookup.
     * <p/>
     * This ensures that a new lookup will be generated.
     *
     * @param prefix the lookup code prefix
     * @return this
     */
    public B uniqueCode(String prefix) {
        this.code = ValueStrategy.random(prefix);
        this.uniqueCode = true;
        return getThis();
    }

    /**
     * Sets the source lookup.
     *
     * @param source the source lookup
     * @return this
     */
    public B source(Lookup source) {
        this.source = source;
        return getThis();
    }

    /**
     * Determines if the lookup is the default.
     * <p/>
     * If set {@code true}, all other lookups of the same archetype will have their flag unset.
     *
     * @param isDefault if {@code true}, the lookup is the default
     * @return this
     */
    public B isDefault(boolean isDefault) {
        this.isDefault = isDefault;
        return getThis();
    }

    /**
     * Clears the default lookup flag for each lookup of a particular archetype or archetypes.
     *
     * @param archetype the lookup archetype. May contain wildcards
     */
    public static void clearDefault(String archetype, ArchetypeService service) {
        Set<IMObject> toSave = new HashSet<>();
        clearDefault(archetype, null, toSave, service);
        if (!toSave.isEmpty()) {
            service.save(toSave);
        }
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        object.setCode(builtCode);
        if (source != null) {
            IMObjectBean sourceBean = getBean(source);
            if (object.isNew()
                || sourceBean.getValue("source", Relationship.class, Predicates.targetEquals(object)) == null) {
                sourceBean.addTarget("source", object, "target");
                toSave.add(source);
            }
        }
        if (isDefault != null) {
            bean.setValue("defaultLookup", isDefault);
            if (isDefault) {
                clearDefault(object, toSave);
            }
        }
    }

    /**
     * Returns the object to build.
     * <p/>
     * This implementation returns the existing instance, if one exists with the same code.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Lookup getObject(String archetype) {
        Lookup lookup = null;
        if (uniqueCode) {
            // ensure there is no lookup with the randomly generated code
            do {
                builtCode = code.toString();
            } while (getLookup(archetype, builtCode) != null);
        } else {
            builtCode = code.toString();
            lookup = getLookup(archetype, builtCode);
        }
        if (lookup == null) {
            // create the lookup
            lookup = super.getObject(archetype);
        } else {
            lookup.setActive(true);
        }
        return lookup;
    }

    /**
     * Returns the lookup with the specified code.
     *
     * @param archetype the lookup archetype
     * @param code      the code
     * @return the corresponding lookup or {@code null} if none exists
     */
    private Lookup getLookup(String archetype, String code) {
        ArchetypeService service = getService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Lookup> query = builder.createQuery(Lookup.class);
        Root<Lookup> root = query.from(Lookup.class, archetype);
        query.where(builder.equal(root.get("code"), code));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Reset the default lookup flag on all those lookups of the same archetype as that supplied.
     *
     * @param object the object to exclude
     * @param toSave objects to save
     */
    private void clearDefault(Lookup object, Set<IMObject> toSave) {
        ArchetypeService service = getService();
        clearDefault(object.getArchetype(), !object.isNew() ? object.getId() : null, toSave, service);
    }

    /**
     * Clears the default lookup flag for each lookup of a particular archetype or archetypes.
     *
     * @param archetype the archetype
     * @param id        the identifier of a lookup to exclude. May be {@code null}
     * @param toSave    objects to save, if any lookups where changed
     * @param service   the archetype service
     */
    private static void clearDefault(String archetype, Long id, Set<IMObject> toSave, ArchetypeService service) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Lookup> query = builder.createQuery(Lookup.class);
        Root<Lookup> root = query.from(Lookup.class, archetype);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(root.get("defaultLookup"), true));
        if (id != null) {
            predicates.add(builder.notEqual(root.get("id"), id));
        }
        query.where(predicates);
        for (Lookup lookup : service.createQuery(query).getResultList()) {
            lookup.setDefaultLookup(false);
            toSave.add(lookup);
        }
    }
}
