/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.sms;

import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating SMS.
 *
 * @author Tim Anderson
 */
public class TestSMSFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestProductFactory}.
     *
     * @param service the service
     */
    public TestSMSFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns a builder for a new SMS.
     *
     * @return an SMS builder
     */
    public TestSMSBuilder newSMS() {
        return new TestSMSBuilder(service);
    }

    /**
     * Returns a builder to update an SMS.
     *
     * @param sms the SMS to update
     * @return an SMS builder
     */
    public TestSMSBuilder updateSMS(Act sms) {
        return new TestSMSBuilder(sms, service);
    }

    /**
     * Returns a builder for a new reply.
     *
     * @return an SMS reply builder
     */
    public TestSMSReplyBuilder newReply() {
        return new TestSMSReplyBuilder(service);
    }

}