/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entityLink.productTask</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProductTaskBuilder<P extends TestProductBuilder<P>>
        extends AbstractTestIMObjectBuilder<EntityLink, TestProductTaskBuilder<P>> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The task type.
     */
    private Entity taskType;

    /**
     * The work list.
     */
    private Entity workList;

    /**
     * The interactive flag.
     */
    private ValueStrategy interactive = ValueStrategy.unset();

    /**
     * The start period.
     */
    private ValueStrategy start = ValueStrategy.unset();

    /**
     * The start period units.
     */
    private ValueStrategy units = ValueStrategy.unset();

    /**
     * Constructs a {@link TestProductTaskBuilder}.
     *
     * @param service the archetype service
     */
    public TestProductTaskBuilder(P parent, ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_TASK_RELATIONSHIP, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Sets the task type.
     *
     * @param taskType the task type
     * @return this
     */
    public TestProductTaskBuilder<P> taskType(Entity taskType) {
        this.taskType = taskType;
        return this;
    }

    /**
     * Sets the work list
     *
     * @param workList the work list
     * @return this
     */
    public TestProductTaskBuilder<P> workList(Entity workList) {
        this.workList = workList;
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityLink build() {
        return build(false);
    }

    /**
     * Adds the relationship to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        parent.addProductTask(build());
        return parent;
    }

    /**
     * Sets the interactive flag.
     *
     * @param interactive the interactive flag
     * @return this
     */
    public TestProductTaskBuilder<P> interactive(boolean interactive) {
        this.interactive = ValueStrategy.value(interactive);
        return this;
    }

    /**
     * Sets the start period.
     *
     * @param period the period
     * @param units  the period units
     * @return this
     */
    public TestProductTaskBuilder<P> start(int period, DateUnits units) {
        this.start = ValueStrategy.value(period);
        this.units = ValueStrategy.value(units != null ? units.toString() : null);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(EntityLink object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (taskType != null) {
            object.setTarget(taskType.getObjectReference());
        }
        if (workList != null) {
            bean.setValue("worklist", workList.getObjectReference());
        }
        start.setValue(bean, "start");
        units.setValue(bean, "startUnits");
        interactive.setValue(bean, "interactive");
    }
}