/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

import static java.math.BigDecimal.ZERO;

/**
 * Builds <em>act.customerAccount*Item</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerChargeItemBuilder<P extends TestCustomerChargeBuilder<P, B>,
        B extends TestCustomerChargeItemBuilder<P, B>>
        extends AbstractTestActBuilder<FinancialAct, B> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The product.
     */
    private Product product;

    /**
     * The template the product was sourced from.
     */
    private Product template;

    /**
     * The template expansion group.
     */
    private ValueStrategy group = ValueStrategy.unset();

    /**
     * The quantity.
     */
    private ValueStrategy quantity = ValueStrategy.unset();

    /**
     * The received quantity.
     */
    private ValueStrategy receivedQuantity = ValueStrategy.unset();

    /**
     * The fixed price.
     */
    private ValueStrategy fixedPrice = ValueStrategy.unset();

    /**
     * The unit price.
     */
    private ValueStrategy unitPrice = ValueStrategy.unset();

    /**
     * The discount.
     */
    private ValueStrategy discount = ValueStrategy.unset();

    /**
     * The tax amount.
     */
    private ValueStrategy tax = ValueStrategy.unset();

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Constructs a {@link TestCustomerChargeItemBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestCustomerChargeItemBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Constructs a {@link TestCustomerChargeItemBuilder}.
     *
     * @param parent  the parent builder
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestCustomerChargeItemBuilder(P parent, FinancialAct object, ArchetypeService service) {
        super(object, service);
        this.parent = parent;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public B patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Sets a medication product.
     *
     * @return this
     */
    public B medicationProduct() {
        return product(new TestProductFactory(getService()).createMedication());
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public B product(Product product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the template.
     *
     * @param template the template
     * @return this
     */
    public B template(Product template) {
        this.template = template;
        return getThis();
    }

    /**
     * Sets the template expansion group.
     *
     * @param group the template expansion group
     * @return this
     */
    public B group(int group) {
        this.group = ValueStrategy.value(group);
        return getThis();
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Sets the received quantity.
     * <p/>
     * This is the quantity that has been received from a pharmacy.
     *
     * @param quantity the received quantity
     * @return this
     */
    public B receivedQuantity(int quantity) {
        return receivedQuantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the received quantity.
     * <p/>
     * This is the quantity that has been received from a pharmacy.
     *
     * @param quantity the received quantity
     * @return this
     */
    public B receivedQuantity(BigDecimal quantity) {
        this.receivedQuantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public B fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public B fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = ValueStrategy.value(fixedPrice);
        return getThis();
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(BigDecimal unitPrice) {
        this.unitPrice = ValueStrategy.value(unitPrice);
        return getThis();
    }

    /**
     * Sets the discount.
     *
     * @param discount the discount
     * @return this
     */
    public B discount(int discount) {
        return discount(BigDecimal.valueOf(discount));
    }

    /**
     * Sets the discount.
     *
     * @param discount the discount
     * @return this
     */
    public B discount(BigDecimal discount) {
        this.discount = ValueStrategy.value(discount);
        return getThis();
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(int tax) {
        return tax(BigDecimal.valueOf(tax));
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(BigDecimal tax) {
        this.tax = ValueStrategy.value(tax);
        return getThis();
    }

    /**
     * Sets the stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public B stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return getThis();
    }

    /**
     * Adds the item to the parent charge.
     *
     * @return the parent charge builder
     */
    public P add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        if (product != null) {
            bean.setTarget("product", product);
        }
        if (template != null) {
            bean.setTarget("template", template);
            Participation participation = bean.getObject("template", Participation.class);
            IMObjectBean participationBean = bean.getBean(participation);
            group.setValue(participationBean, "group");
        }
        quantity.setValue(bean, "quantity");
        receivedQuantity.setValue(bean, "receivedQuantity");
        fixedPrice.setValue(bean, "fixedPrice");
        unitPrice.setValue(bean, "unitPrice");
        discount.setValue(bean, "discount");
        tax.setValue(bean, "tax");
        BigDecimal total = MathRules.calculateTotal(bean.getBigDecimal("fixedPrice", ZERO),
                                                    bean.getBigDecimal("unitPrice", ZERO),
                                                    bean.getBigDecimal("quantity", ZERO),
                                                    bean.getBigDecimal("discount", ZERO), 2);
        bean.setValue("total", total);

        if (stockLocation != null) {
            bean.setTarget("stockLocation", stockLocation);
        }
    }
}
