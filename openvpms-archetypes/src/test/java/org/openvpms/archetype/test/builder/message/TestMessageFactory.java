/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.message;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating test messages.
 *
 * @author Tim Anderson
 */
public class TestMessageFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestMessageFactory}.
     *
     * @param service the archetype service.
     */
    public TestMessageFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a builder for a new audit message.
     *
     * @return an audit message builder
     */
    public TestAuditMessageBuilder newAuditMessage() {
        return new TestAuditMessageBuilder(service);
    }

    /**
     * Creates a builder to update an audit message.
     *
     * @param message the message to update
     * @return an audit message builder
     */
    public TestAuditMessageBuilder updateAuditMessage(Act message) {
        return new TestAuditMessageBuilder(message, service);
    }

    /**
     * Creates a builder for a new system message.
     *
     * @return a system message builder
     */
    public TestSystemMessageBuilder newSystemMessage() {
        return new TestSystemMessageBuilder(service);
    }

    /**
     * Creates a builder to update a system message.
     *
     * @param message the message to update
     * @return a system message builder
     */
    public TestSystemMessageBuilder updateSystemMessage(Act message) {
        return new TestSystemMessageBuilder(message, service);
    }

    /**
     * Creates a builder for a new user message.
     *
     * @return a user message builder
     */
    public TestUserMessageBuilder newUserMessage() {
        return new TestUserMessageBuilder(service);
    }

    /**
     * Creates a builder to update a user message.
     *
     * @param message the message to update
     * @return a user message builder
     */
    public TestUserMessageBuilder updateUserMessage(Act message) {
        return new TestUserMessageBuilder(message, service);
    }
}
