/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.patientAlert</em> acts for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientAlertBuilder extends AbstractTestPatientActBuilder<Act, TestPatientAlertBuilder> {

    /**
     * The alert type.
     */
    private Entity alertType;

    /**
     * The alert notes.
     */
    private String notes;

    /**
     * Constructs a {@link TestPatientAlertBuilder}.
     *
     * @param service the archetype service
     */
    public TestPatientAlertBuilder(ArchetypeService service) {
        super(PatientArchetypes.ALERT, Act.class, service);
    }

    /**
     * Sets the alert type.
     *
     * @param alertType the alert type
     * @return this
     */
    public TestPatientAlertBuilder alertType(Entity alertType) {
        this.alertType = alertType;
        return this;
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public TestPatientAlertBuilder notes(String notes) {
        this.notes = notes;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        if (alertType != null) {
            bean.setTarget("alertType", alertType);
        }
        if (notes != null) {
            bean.setValue("notes", notes);
        }
        super.build(object, bean, toSave, toRemove);
    }
}
