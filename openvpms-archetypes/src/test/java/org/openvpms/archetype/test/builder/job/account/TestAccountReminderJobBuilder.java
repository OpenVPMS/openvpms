/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.job.account;

import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.job.AbstractTestJobBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>entity.jobAccountReminder</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAccountReminderJobBuilder extends AbstractTestJobBuilder<TestAccountReminderJobBuilder> {

    /**
     * The reminder counts.
     */
    private final List<Entity> counts = new ArrayList<>();

    /**
     * The minimum balance.
     */
    private BigDecimal minBalance;

    /**
     * The built counts.
     */
    private List<Entity> builtCounts = new ArrayList<>();

    /**
     * Constructs a {@link TestAccountReminderJobBuilder}.
     *
     * @param service the archetype service
     */
    public TestAccountReminderJobBuilder(ArchetypeService service) {
        super(AccountReminderArchetypes.ACCOUNT_REMINDER_JOB, service);
    }

    /**
     * Sets the minimum balance.
     *
     * @param minBalance the minimum balance
     * @return this
     */
    public TestAccountReminderJobBuilder minBalance(BigDecimal minBalance) {
        this.minBalance = minBalance;
        return this;
    }

    /**
     * Adds a reminder count.
     *
     * @param interval the interval
     * @param units    the interval units
     * @param template the SMS template
     * @return this
     */
    public TestAccountReminderJobBuilder count(int interval, DateUnits units, Entity template) {
        return count().interval(interval, units).template(template).add();
    }

    /**
     * Returns a builder to add a reminder count.
     *
     * @return a reminder count builder
     */
    public TestAccountReminderCountBuilder count() {
        return new TestAccountReminderCountBuilder(this, getService());
    }

    /**
     * Adds a reminder count.
     *
     * @param count the count to add
     * @return this
     */
    public TestAccountReminderJobBuilder add(Entity count) {
        counts.add(count);
        return this;
    }

    /**
     * Returns the built counts.
     *
     * @return the built counts
     */
    public List<Entity> getCounts() {
        return builtCounts;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (minBalance != null) {
            bean.setValue("minBalance", minBalance);
        }
        for (Entity count : counts) {
            int sequence = bean.getValues("reminders").size();
            EntityLink relationship = (EntityLink) bean.addTarget("reminders", count);
            relationship.setSequence(sequence);
        }
        toSave.addAll(counts);
        builtCounts = new ArrayList<>(counts);
        counts.clear(); // can't reuse
    }
}