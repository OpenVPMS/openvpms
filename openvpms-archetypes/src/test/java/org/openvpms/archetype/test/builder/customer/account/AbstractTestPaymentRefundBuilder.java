/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerAccountPayment</em> and <em>act.customerAccountRefund</em>instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestPaymentRefundBuilder<B extends AbstractTestPaymentRefundBuilder<B>>
        extends AbstractTestCustomerAccountActBuilder<B> {

    /**
     * The items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The built items. Available after building.
     */
    private List<FinancialAct> builtItems = new ArrayList<>();

    /**
     * The till.
     */
    private Entity till;

    /**
     * Constructs an {@link AbstractTestPaymentRefundBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    protected AbstractTestPaymentRefundBuilder(String archetype, ArchetypeService service) {
        super(archetype, service);
    }

    /**
     * Constructs a {@link AbstractTestPaymentRefundBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestPaymentRefundBuilder(FinancialAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the till.
     *
     * @param till the till
     * @return this
     */
    public B till(Entity till) {
        this.till = till;
        return getThis();
    }

    /**
     * Returns a builder to add an EFT item.
     *
     * @return an EFT item builder
     */
    public abstract <T extends AbstractTestEFTPaymentRefundItemBuilder<B, T>> T eft();

    /**
     * Adds an EFT item.
     *
     * @param amount the amount
     * @return this
     */
    public B eft(int amount) {
        return eft().amount(amount).add();
    }

    /**
     * Adds an EFT item.
     *
     * @param amount the amount
     * @return this
     */
    public B eft(BigDecimal amount) {
        return eft().amount(amount).add();
    }

    /**
     * Adds items.
     *
     * @param items the item to add
     * @return this
     */
    public B add(FinancialAct... items) {
        Collections.addAll(this.items, items);
        return getThis();
    }

    /**
     * Returns the built items.
     *
     * @return the built items
     */
    public List<FinancialAct> getItems() {
        return builtItems;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (till != null) {
            bean.setTarget("till", till);
        }
        BigDecimal amount = BigDecimal.ZERO;

        // sum the existing amounts
        for (FinancialAct item : bean.getTargets("items", FinancialAct.class)) {
            amount = sum(amount, item.getTotal());
        }
        for (FinancialAct item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            item.addActRelationship(relationship);
            toSave.add(item);
            amount = sum(amount, item.getTotal());
        }
        bean.setValue("amount", amount);
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }

    /**
     * Adds two values, treating {@code null} as zero.
     *
     * @param value1 the first value. May be {@code null}
     * @param value2 the second value. May be {@code null}
     * @return the sum of the two values
     */
    private BigDecimal sum(BigDecimal value1, BigDecimal value2) {
        if (value1 == null) {
            value1 = BigDecimal.ZERO;
        }
        if (value2 == null) {
            value2 = BigDecimal.ZERO;
        }
        return value1.add(value2);
    }
}
