/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.delivery;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.supplier.TestSupplierActBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds <em>act.supplierDelivery</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDeliveryBuilder extends TestSupplierActBuilder<TestDeliveryBuilder, TestDeliveryItemBuilder> {

    /**
     * The supplier notes.
     */
    private ValueStrategy supplierNotes = ValueStrategy.unset();

    /**
     * Constructs a {@link TestDeliveryBuilder}.
     *
     * @param service the archetype service
     */
    public TestDeliveryBuilder(ArchetypeService service) {
        super(SupplierArchetypes.DELIVERY, service);
    }

    /**
     * Sets the supplier notes.
     *
     * @param supplierNotes the supplier notes
     * @return this
     */
    public TestDeliveryBuilder supplierNotes(String supplierNotes) {
        this.supplierNotes = ValueStrategy.value(supplierNotes);
        return this;
    }

    /**
     * Returns a builder to add an item.
     *
     * @return an item builder
     */
    @Override
    public TestDeliveryItemBuilder item() {
        return new TestDeliveryItemBuilder(this, getService());
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        supplierNotes.setValue(bean, "supplierNotes");
    }
}