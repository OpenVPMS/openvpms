/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.lookup.TestSpeciesBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>entity.reminderType</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderTypeBuilder extends AbstractTestEntityBuilder<Entity, TestReminderTypeBuilder> {

    /**
     * The reminder counts to add.
     */
    private final List<Entity> reminderCounts = new ArrayList<>();

    /**
     * Determines if reminders can be grouped with others.
     */
    private ValueStrategy groupBy = ValueStrategy.unset();

    /**
     * Determines if the reminder is interactive.
     */
    private ValueStrategy interactive = ValueStrategy.unset();

    /**
     * The default interval.
     */
    private ValueStrategy defaultInterval = ValueStrategy.unset();

    /**
     * The default interval units.
     */
    private ValueStrategy defaultUnits = ValueStrategy.unset();

    /**
     * The cancel interval.
     */
    private ValueStrategy cancelInterval = ValueStrategy.unset();

    /**
     * The cancel interval units.
     */
    private ValueStrategy cancelUnits = ValueStrategy.unset();

    /**
     * The sensitivity interval.
     */
    private ValueStrategy sensitivityInterval = ValueStrategy.unset();

    /**
     * The sensitivity interval units.
     */
    private ValueStrategy sensitivityUnits = ValueStrategy.unset();

    /**
     * The species to add.
     */
    private String[] species;

    /**
     * The reminder groups to add.
     */
    private String[] groups;

    /**
     * Constructs a {@link TestReminderTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestReminderTypeBuilder(ArchetypeService service) {
        super(ReminderArchetypes.REMINDER_TYPE, Entity.class, service);
        name(ValueStrategy.random("zremindertype"));
    }

    /**
     * Constructs a {@link TestReminderTypeBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestReminderTypeBuilder(Entity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the group-by criteria. This determines if the reminder can be grouped with others.
     *
     * @param groupBy the group-by criteria
     * @return this
     */
    public TestReminderTypeBuilder groupBy(ReminderType.GroupBy groupBy) {
        this.groupBy = ValueStrategy.value(groupBy != null ? groupBy.toString() : null);
        return this;
    }

    /**
     * Determines if the reminder is interactive.
     *
     * @param interactive if {@code true} the reminder is interactive
     * @return this
     */
    public TestReminderTypeBuilder interactive(boolean interactive) {
        this.interactive = ValueStrategy.value(interactive);
        return this;
    }

    /**
     * Sets the default interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderTypeBuilder defaultInterval(int interval, DateUnits units) {
        defaultInterval = ValueStrategy.value(interval);
        defaultUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the cancel interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderTypeBuilder cancelInterval(int interval, DateUnits units) {
        cancelInterval = ValueStrategy.value(interval);
        cancelUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the sensitivity interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestReminderTypeBuilder sensitivityInterval(int interval, DateUnits units) {
        sensitivityInterval = ValueStrategy.value(interval);
        sensitivityUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Returns a builder for a new reminder count.
     *
     * @return a reminder count builder
     */
    public TestReminderCountBuilder newCount() {
        return new TestReminderCountBuilder(this, getService());
    }

    /**
     * Adds a reminder count.
     *
     * @param count the reminder count
     */
    public TestReminderTypeBuilder addReminderCount(Entity count) {
        reminderCounts.add(count);
        return this;
    }

    /**
     * Adds species.
     *
     * @param species the species codes
     * @return this
     */
    public TestReminderTypeBuilder addSpecies(String... species) {
        this.species = species;
        return this;
    }

    /**
     * Adds reminder groups.
     *
     * @param groups the reminder groups
     * @return this
     */
    public TestReminderTypeBuilder addGroups(Lookup... groups) {
        for (Lookup group : groups) {
            addGroups(group.getCode());
        }
        return this;
    }

    /**
     * Adds reminder groups.
     *
     * @param groups the reminder groups
     * @return this
     */
    public TestReminderTypeBuilder addGroups(String... groups) {
        this.groups = groups;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        groupBy.setValue(bean, "groupBy");
        interactive.setValue(bean, "interactive");
        defaultInterval.setValue(bean, "defaultInterval");
        defaultUnits.setValue(bean, "defaultUnits");
        cancelInterval.setValue(bean, "cancelInterval");
        cancelUnits.setValue(bean, "cancelUnits");
        sensitivityInterval.setValue(bean, "sensitivityInterval");
        sensitivityUnits.setValue(bean, "sensitivityUnits");
        for (Entity count : reminderCounts) {
            bean.addTarget("counts", count);
            toSave.add(count);
        }
        reminderCounts.clear();
        if (species != null && species.length != 0) {
            TestSpeciesBuilder builder = new TestSpeciesBuilder(getService());
            for (String code : species) {
                Lookup lookup = builder.code(code).build();
                object.addClassification(lookup);
            }
        }
        if (groups != null) {
            TestLookupBuilder builder = new TestLookupBuilder(ReminderArchetypes.GROUP, getService());
            for (String group : groups) {
                Lookup lookup = builder.code(group).build();
                object.addClassification(lookup);
            }
        }
    }
}