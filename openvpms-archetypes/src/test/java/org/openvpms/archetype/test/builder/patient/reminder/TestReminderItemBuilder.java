/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>act.patientReminderItem*</em> for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderItemBuilder extends AbstractTestActBuilder<Act, TestReminderItemBuilder> {

    /**
     * The parent builder.
     */
    private final TestReminderBuilder parent;

    /**
     * The reminder count.
     */
    private ValueStrategy count = ValueStrategy.unset();

    /**
     * Constructs a {@link TestReminderItemBuilder}.
     *
     * @param archetype the reminder item archetype
     * @param service   the archetype service
     */
    public TestReminderItemBuilder(String archetype, ArchetypeService service) {
        this(null, archetype, service);
    }

    /**
     * Constructs a {@link TestReminderItemBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the reminder item archetype
     * @param service   the archetype service
     */
    public TestReminderItemBuilder(TestReminderBuilder parent, String archetype, ArchetypeService service) {
        super(archetype, Act.class, service);
        this.parent = parent;
    }

    /**
     * Sets the date the reminder should be sent.
     *
     * @param sendDate the send date
     * @return this
     */
    public TestReminderItemBuilder sendDate(Date sendDate) {
        return startTime(sendDate);
    }

    /**
     * Sets the reminder due date.
     *
     * @param dueDate the due date
     * @return this
     */
    public TestReminderItemBuilder dueDate(Date dueDate) {
        return endTime(dueDate);
    }

    /**
     * Sets the reminder count.
     *
     * @param count the count
     * @return this
     */
    public TestReminderItemBuilder count(int count) {
        this.count = ValueStrategy.value(count);
        return this;
    }

    /**
     * Adds the item to the parent charge.
     *
     * @return the parent charge builder
     */
    public TestReminderBuilder add() {
        Act item = build(false);
        parent.addItems(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        count.setValue(bean, "count");
    }
}