/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.stock.StockArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>party.organisationStockLocation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestStockLocationBuilder extends AbstractTestPartyBuilder<Party, TestStockLocationBuilder> {

    /**
     * The locations.
     */
    private Party[] locations;

    /**
     * Constructs a {@link TestStockLocationBuilder}.
     *
     * @param service the archetype service
     */
    public TestStockLocationBuilder(ArchetypeService service) {
        super(StockArchetypes.STOCK_LOCATION, Party.class, service);
        name(ValueStrategy.random("zstock"));
    }

    /**
     * Sets the locations.
     *
     * @param locations the locations
     * @return this
     */
    public TestStockLocationBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (locations != null) {
            for (Party location : locations) {
                bean.addSource("locations", location, "stockLocations");
                toSave.add(location);
            }
        }
    }
}
