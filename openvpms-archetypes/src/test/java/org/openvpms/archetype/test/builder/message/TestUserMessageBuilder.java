/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.message;

import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.userMessage</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestUserMessageBuilder extends AbstractTestMessageBuilder<TestUserMessageBuilder> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * Constructs a {@link TestUserMessageBuilder}.
     *
     * @param service the archetype service
     */
    public TestUserMessageBuilder(ArchetypeService service) {
        super(MessageArchetypes.USER, service);
    }

    /**
     * Constructs a {@link TestUserMessageBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestUserMessageBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public TestUserMessageBuilder customer(Party customer) {
        this.customer = customer;
        return this;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public TestUserMessageBuilder patient(Party patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (object.getReason() != null) {
            new TestLookupBuilder("lookup.messageReason", getService())
                    .code(object.getReason())
                    .build();
        }
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
    }
}
