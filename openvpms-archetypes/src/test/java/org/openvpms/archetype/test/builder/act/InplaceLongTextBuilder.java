/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.act;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds a text node on a {@link DocumentAct}, where if the text is too long, it is stored in a document on
 * the act.
 *
 * @author Tim Anderson
 */
public class InplaceLongTextBuilder {

    /**
     * The node.
     */
    private final String node;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link InplaceLongTextBuilder}.
     *
     * @param node    the node
     * @param service the archetype service
     */
    public InplaceLongTextBuilder(String node, ArchetypeService service) {
        this.node = node;
        this.service = service;
    }

    /**
     * Builds the node.
     *
     * @param value    the value
     * @param act      the act
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    public void build(ValueStrategy value, DocumentAct act, IMObjectBean bean, Set<IMObject> toSave,
                      Set<Reference> toRemove) {
        if (value.isSet()) {
            String text = value.toString();
            Document content = act.getDocument() != null ? bean.getObject(act.getDocument(), Document.class) : null;
            if (text == null || text.length() <= bean.getMaxLength(node)) {
                bean.setValue(node, text);
                act.setDocument(null);
                if (content != null) {
                    // schedule the existing document for removal
                    toRemove.add(content.getObjectReference());
                }
            } else {
                bean.setValue(node, null);
                TextDocumentHandler handler = new TextDocumentHandler(service);
                if (content == null) {
                    content = handler.create("document", text);
                    act.setDocument(content.getObjectReference());
                } else {
                    handler.update(content, text);
                }
                toSave.add(content);
            }
        }
    }
}