/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate.SubjectType;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>entity.documentTemplateEmail*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEmailTemplateBuilder extends AbstractTestDocumentTemplateBuilder<TestEmailTemplateBuilder> {

    /**
     * The parent builder.
     */
    private final TestDocumentTemplateBuilder parent;

    /**
     * The attachments.
     */
    private final List<Entity> attachments = new ArrayList<>();

    /**
     * The subject type.
     */
    private ValueStrategy subjectType = ValueStrategy.unset();

    /**
     * The email subject.
     */
    private ValueStrategy subject = ValueStrategy.unset();

    /**
     * The email subject source.
     */
    private ValueStrategy subjectSource = ValueStrategy.unset();

    /**
     * The content type.
     */
    private ValueStrategy contentType = ValueStrategy.unset();

    /**
     * The content.
     */
    private ValueStrategy content = ValueStrategy.unset();

    /**
     * The email content source.
     */
    private ValueStrategy contentSource = ValueStrategy.unset();

    /**
     * The default email address.
     */
    private ValueStrategy defaultEmailAddress = ValueStrategy.unset();

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param archetype the email template archetype
     * @param service   the archetype service
     * @param handlers  the document handlers
     */
    public TestEmailTemplateBuilder(String archetype, ArchetypeService service, DocumentHandlers handlers) {
        this(null, archetype, service, handlers);
    }

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param template the template to update
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    TestEmailTemplateBuilder(Entity template, ArchetypeService service, DocumentHandlers handlers) {
        super(template, service, handlers);
        this.parent = null;
    }

    /**
     * Constructs a {@link TestEmailTemplateBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the email template archetype
     * @param service   the archetype service
     * @param handlers  the document handlers
     */
    TestEmailTemplateBuilder(TestDocumentTemplateBuilder parent, String archetype, ArchetypeService service,
                             DocumentHandlers handlers) {
        super(archetype, service, handlers);
        this.parent = parent;
        name(ValueStrategy.random("zemailtemplate"));
    }

    /**
     * Sets the email subject type.
     *
     * @param subjectType the subject type
     * @return this
     */
    public TestEmailTemplateBuilder subjectType(SubjectType subjectType) {
        this.subjectType = ValueStrategy.value(subjectType.name());
        return this;
    }

    /**
     * Sets the email subject.
     *
     * @param subject the subject
     * @return this
     */
    public TestEmailTemplateBuilder subject(String subject) {
        this.subject = ValueStrategy.value(subject);
        return this;
    }

    /**
     * Sets the email subject source.
     *
     * @param subjectSource the subject source. An xpath expression
     * @return this
     */
    public TestEmailTemplateBuilder subjectSource(String subjectSource) {
        this.subjectSource = ValueStrategy.value(subjectSource);
        return this;
    }

    /**
     * Sets the content type.
     *
     * @param contentType the content type
     * @return this
     */
    public TestEmailTemplateBuilder contentType(EmailTemplate.ContentType contentType) {
        this.contentType = ValueStrategy.value(contentType);
        return this;
    }

    /**
     * Sets the email content source.
     *
     * @param contentSource the content source. An xpath expression
     * @return this
     */
    public TestEmailTemplateBuilder contentSource(String contentSource) {
        this.contentSource = ValueStrategy.value(contentSource);
        return this;
    }

    /**
     * Sets the content.
     *
     * @param content the content
     * @return this
     */
    public TestEmailTemplateBuilder content(String content) {
        this.content = ValueStrategy.value(content);
        if (this.contentType.getValue() == null) {
            contentType(EmailTemplate.ContentType.TEXT);
        }
        return this;
    }

    /**
     * Sets the default email address.
     *
     * @param defaultEmailAddress the default email address
     * @return this
     */
    public TestEmailTemplateBuilder defaultEmailAddress(String defaultEmailAddress) {
        this.defaultEmailAddress = ValueStrategy.value(defaultEmailAddress);
        return this;
    }

    /**
     * Attaches a document.
     *
     * @param document the document
     * @return this
     */
    @Override
    public TestEmailTemplateBuilder document(Document document) {
        contentType(EmailTemplate.ContentType.DOCUMENT);
        return super.document(document);
    }

    /**
     * Adds <em>documentTemplate</em> attachments.
     *
     * @param attachments the attachments
     * @return this
     */
    public TestEmailTemplateBuilder addAttachments(Entity... attachments) {
        this.attachments.addAll(Arrays.asList(attachments));
        return this;
    }

    /**
     * Adds the template to the parent builder.
     *
     * @return the parent builder
     */
    public TestDocumentTemplateBuilder add() {
        return parent.emailTemplate(build(false));
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        subjectType.setValue(bean, "subjectType");
        subject.setValue(bean, "subject");
        subjectSource.setValue(bean, "subjectSource");
        contentType.setValue(bean, "contentType");
        content.setValue(bean, "content");
        contentSource.setValue(bean, "contentSource");
        defaultEmailAddress.setValue(bean, "defaultEmailAddress");
        for (Entity attachment : attachments) {
            bean.addTarget("attachments", attachment);
        }
    }
}
