/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>contact.phoneNumber</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPhoneContactBuilder<T extends Party, P extends AbstractTestPartyBuilder<T, P>>
        extends AbstractTestContactBuilder<T, P, TestPhoneContactBuilder<T, P>> {

    /**
     * The area code.
     */
    private ValueStrategy areaCode = ValueStrategy.unset();

    /**
     * The phone number.
     */
    private ValueStrategy phoneNumber = ValueStrategy.unset();

    /**
     * Determines if the phone can be sent SMS messages.
     */
    private ValueStrategy sms = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPhoneContactBuilder}.
     *
     * @param service the archetype service
     */
    public TestPhoneContactBuilder(ArchetypeService service) {
        super(ContactArchetypes.PHONE, service);
    }

    /**
     * Constructs a {@link TestPhoneContactBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestPhoneContactBuilder(P parent, ArchetypeService service) {
        super(parent, ContactArchetypes.PHONE, service);
    }

    /**
     * Constructs a {@link TestPhoneContactBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestPhoneContactBuilder(Contact object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the area code.
     *
     * @param areaCode the area code
     * @return this
     */
    public TestPhoneContactBuilder<T, P> areaCode(String areaCode) {
        this.areaCode = ValueStrategy.value(areaCode);
        return this;
    }

    /**
     * Sets the phone number.
     *
     * @param phoneNumber the phone number
     * @return this
     */
    public TestPhoneContactBuilder<T, P> phone(String phoneNumber) {
        this.phoneNumber = ValueStrategy.value(phoneNumber);
        return this;
    }

    /**
     * Marks the phone as accepting SMS messages.
     *
     * @return this
     */
    public TestPhoneContactBuilder<T, P> sms() {
        return sms(true);
    }

    /**
     * Determines if the phone can accept SMS messages.
     *
     * @param sms if {@code true} the phone can accept SMS messages
     * @return this
     */
    public TestPhoneContactBuilder<T, P> sms(boolean sms) {
        this.sms = ValueStrategy.value(sms);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Contact object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        areaCode.setValue(bean, "areaCode");
        phoneNumber.setValue(bean, "telephoneNumber");
        sms.setValue(bean, "sms");
    }
}
