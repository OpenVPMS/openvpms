/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <em>entityLink.productSupplier</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProductSupplierBuilder<P extends TestProductBuilder<P>>
        extends AbstractTestIMObjectBuilder<EntityLink, TestProductSupplierBuilder<P>> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The preferred flag.
     */
    private ValueStrategy preferred = ValueStrategy.unset();

    /**
     * The package size.
     */
    private ValueStrategy packageSize = ValueStrategy.unset();

    /**
     * The list price.
     */
    private ValueStrategy listPrice = ValueStrategy.unset();

    /**
     * The nett price.
     */
    private ValueStrategy nettPrice = ValueStrategy.unset();

    /**
     * The auto-price-update flag.
     */
    private ValueStrategy autoPriceUpdate = ValueStrategy.unset();

    /**
     * Constructs a {@link TestProductSupplierBuilder}.
     *
     * @param service the archetype service
     */
    public TestProductSupplierBuilder(P parent, ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_SUPPLIER_RELATIONSHIP, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Sets the supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public TestProductSupplierBuilder<P> supplier(Party supplier) {
        this.supplier = supplier;
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityLink build() {
        return build(false);
    }

    /**
     * Adds the relationship to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        parent.addProductSupplier(build());
        return parent;
    }

    /**
     * Sets the preferred flag.
     *
     * @param preferred the preferred flag
     * @return this
     */
    public TestProductSupplierBuilder<P> preferred(boolean preferred) {
        this.preferred = ValueStrategy.value(preferred);
        return this;
    }

    /**
     * Sets the package size.
     *
     * @param packageSize the package size
     * @return this
     */
    public TestProductSupplierBuilder<P> packageSize(int packageSize) {
        this.packageSize = ValueStrategy.value(packageSize);
        return this;
    }

    /**
     * Sets the list price.
     *
     * @param listPrice the list price
     * @return this
     */
    public TestProductSupplierBuilder<P> listPrice(BigDecimal listPrice) {
        this.listPrice = ValueStrategy.value(listPrice);
        return this;
    }

    /**
     * Sets the nett price.
     *
     * @param nettPrice the net price
     * @return this
     */
    public TestProductSupplierBuilder<P> nettPrice(int nettPrice) {
        return nettPrice(BigDecimal.valueOf(nettPrice));
    }

    /**
     * Sets the nett price.
     *
     * @param nettPrice the net price
     * @return this
     */
    public TestProductSupplierBuilder<P> nettPrice(BigDecimal nettPrice) {
        this.nettPrice = ValueStrategy.value(nettPrice);
        return this;
    }

    /**
     * Sets the auto-price-update flag.
     *
     * @param autoPriceUpdate the auto-price-update flag
     * @return this
     */
    public TestProductSupplierBuilder<P> autoPriceUpdate(boolean autoPriceUpdate) {
        this.autoPriceUpdate = ValueStrategy.value(autoPriceUpdate);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(EntityLink object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (supplier != null) {
            object.setTarget(supplier.getObjectReference());
        }
        preferred.setValue(bean, "preferred");
        packageSize.setValue(bean, "packageSize");
        listPrice.setValue(bean, "listPrice");
        nettPrice.setValue(bean, "nettPrice");
        autoPriceUpdate.setValue(bean, "autoPriceUpdate");
    }
}