/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Verifies <em>act.supplierOrder</em>, <em>act.supplierDelivery</em> and <em>act.supplierReturn</em> instances
 * match that expected, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestSupplierActVerifier<V extends TestSupplierActVerifier<V>> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected supplier.
     */
    private Reference supplier;

    /**
     * The expected location.
     */
    private Reference stockLocation;

    /**
     * The expected amount.
     */
    private BigDecimal amount;

    /**
     * The expected tax.
     */
    private BigDecimal tax;

    /**
     * The expected status.
     */
    private String status;

    /**
     * Constructs a {@link TestSupplierActVerifier}.
     *
     * @param service the archetype service
     */
    public TestSupplierActVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Initialises this from an act.
     *
     * @param act the act
     * @return this
     */
    public V initialise(FinancialAct act) {
        IMObjectBean bean = service.getBean(act);
        supplier(bean.getTargetRef("supplier"));
        stockLocation(bean.getTargetRef("stockLocation"));
        amount(bean.getBigDecimal("amount"));
        tax(bean.getBigDecimal("tax"));
        status(bean.getString("status"));
        return getThis();
    }

    /**
     * Sets the expected supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public V supplier(Party supplier) {
        return supplier((supplier != null) ? supplier.getObjectReference() : null);
    }

    /**
     * Sets the expected supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public V supplier(Reference supplier) {
        this.supplier = supplier;
        return getThis();
    }

    /**
     * Sets the stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public V stockLocation(Party stockLocation) {
        return stockLocation((stockLocation != null) ? stockLocation.getObjectReference() : null);
    }

    /**
     * Sets the expected stock location.
     *
     * @param stockLocation the expected stock location
     * @return this
     */
    public V stockLocation(Reference stockLocation) {
        this.stockLocation = stockLocation;
        return getThis();
    }

    /**
     * Sets the expected amount.
     *
     * @param amount the amount
     * @return this
     */
    public V amount(BigDecimal amount) {
        this.amount = amount;
        return getThis();
    }

    /**
     * Sets the expected tax.
     *
     * @param tax the tax
     * @return this
     */
    public V tax(BigDecimal tax) {
        this.tax = tax;
        return getThis();
    }

    /**
     * Sets the expected status.
     *
     * @param status the status
     * @return this
     */
    public V status(String status) {
        this.status = status;
        return getThis();
    }

    /**
     * Verifies an act matches that expected.
     *
     * @param act the act
     */
    public void verify(FinancialAct act) {
        IMObjectBean bean = service.getBean(act);
        assertEquals(supplier, bean.getTargetRef("supplier"));
        assertEquals(stockLocation, bean.getTargetRef("stockLocation"));
        checkEquals(amount, bean.getBigDecimal("amount"));
        checkEquals(tax, bean.getBigDecimal("tax"));
        assertEquals(status, bean.getString("status"));
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected V getThis() {
        return (V) this;
    }
}
