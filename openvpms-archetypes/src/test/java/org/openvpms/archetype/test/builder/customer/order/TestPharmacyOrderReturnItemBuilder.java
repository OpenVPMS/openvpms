/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.order;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builds <em>act.customerOrderItemPharmacy</em> and <em>act.customerReturnItemPharmacy</em>instances, for testing
 * purposes.
 *
 * @author Tim Anderson
 */
public class TestPharmacyOrderReturnItemBuilder<P extends TestPharmacyOrderReturnBuilder<P, B>,
        B extends TestPharmacyOrderReturnItemBuilder<P, B>>
        extends AbstractTestActBuilder<FinancialAct, B> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The product.
     */
    private Product product;

    /**
     * The quantity.
     */
    private ValueStrategy quantity = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPharmacyOrderReturnBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the pharmacy item archetype
     * @param service   the archetype service
     */
    public TestPharmacyOrderReturnItemBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public B patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public B product(Product product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Adds the item to the parent order.
     *
     * @return the parent order builder
     */
    public P add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setTarget("patient", patient);
        bean.setTarget("product", product);
        quantity.setValue(bean, "quantity");
    }
}