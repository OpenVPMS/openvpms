/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>security.archetypeAuthority</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAuthorityBuilder
        extends AbstractTestIMObjectBuilder<ArchetypeAwareGrantedAuthority, TestAuthorityBuilder> {

    /**
     * The service name.
     */
    private String serviceName = "archetypeService";

    private String method;

    /**
     * The archetype.
     */
    private String archetype;

    /**
     * Constructs a {@link TestAuthorityBuilder}.
     *
     * @param service the archetype service
     */
    public TestAuthorityBuilder(ArchetypeService service) {
        super(UserArchetypes.AUTHORITY, ArchetypeAwareGrantedAuthority.class, service);
        name(ValueStrategy.random("zauthority"));
    }

    /**
     * Sets the service name.
     * <p/>
     * This defaults to 'archetypeService'.
     *
     * @param serviceName the service name
     * @return this
     */
    public TestAuthorityBuilder serviceName(String serviceName) {
        this.serviceName = serviceName;
        return this;
    }

    /**
     * Sets the method.
     *
     * @param method the method
     * @return this
     */
    public TestAuthorityBuilder method(String method) {
        this.method = method;
        return this;
    }

    /**
     * Sets the archetype.
     *
     * @param archetype the archetype
     * @return this
     */
    public TestAuthorityBuilder archetype(String archetype) {
        this.archetype = archetype;
        return this;
    }


    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(ArchetypeAwareGrantedAuthority object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        object.setServiceName(serviceName);
        object.setMethod(method);
        object.setShortName(archetype);
    }
}