/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.alert;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerActBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds <em>act.customerAlert</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerAlertBuilder extends AbstractTestCustomerActBuilder<Act, TestCustomerAlertBuilder> {

    /**
     * The alert type.
     */
    private ValueStrategy alertType = ValueStrategy.unset();

    /**
     * Constructs a {@link TestCustomerAlertBuilder}.
     *
     * @param service the archetype service
     */
    public TestCustomerAlertBuilder(ArchetypeService service) {
        super(CustomerArchetypes.ALERT, Act.class, service);
    }

    /**
     * Sets the alert type
     *
     * @param alertType the alert type. A lookup.customerAlertType code
     * @return this
     */
    public TestCustomerAlertBuilder alertType(String alertType) {
        this.alertType = ValueStrategy.value(alertType);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (alertType.isSet()) {
            String code = alertType.toString();
            if (code != null) {
                new TestLookupBuilder(CustomerArchetypes.ALERT_TYPE, getService()).code(code).build();
            }
            alertType.setValue(bean, "alertType");
        }
    }
}
