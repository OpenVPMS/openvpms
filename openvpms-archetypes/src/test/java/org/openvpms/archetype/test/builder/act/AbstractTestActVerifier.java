/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.act;

import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectVerifier;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;

import static org.openvpms.archetype.test.TestHelper.parseDate;

/**
 * A builder of {@link Act} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestActVerifier<T extends Act, V extends AbstractTestActVerifier<T, V>>
        extends AbstractTestIMObjectVerifier<T, V> {

    /**
     * The expected start time.
     */
    private ValueStrategy startTime = ValueStrategy.unset();

    /**
     * The expected end time.
     */
    private ValueStrategy endTime = ValueStrategy.unset();

    /**
     * The expected reason.
     */
    private ValueStrategy reason = ValueStrategy.unset();

    /**
     * The expected status.
     */
    private ValueStrategy status = ValueStrategy.unset();

    /**
     * The expected secondary status.
     */
    private ValueStrategy status2 = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestActVerifier}.
     *
     * @param service the archetype service
     */
    public AbstractTestActVerifier(ArchetypeService service) {
        super(service);
    }

    /**
     * Sets the expected start time.
     *
     * @param startTime the start time
     * @return this
     */
    public V startTime(String startTime) {
        return startTime(parseDate(startTime));
    }

    /**
     * Sets the expected start time.
     *
     * @param startTime the start time
     * @return this
     */
    public V startTime(Date startTime) {
        this.startTime = ValueStrategy.value(startTime);
        return getThis();
    }

    /**
     * Returns the expected start time.
     *
     * @return the expected start time. May be {@code null}
     */
    public Date getStartTime() {
        return (Date) startTime.getValue();
    }

    /**
     * Sets the expected end time.
     *
     * @param endTime the end time
     * @return this
     */
    public V endTime(String endTime) {
        return endTime(parseDate(endTime));
    }

    /**
     * Sets the expected end time.
     *
     * @param endTime the end time
     * @return this
     */
    public V endTime(Date endTime) {
        this.endTime = ValueStrategy.value(endTime);
        return getThis();
    }

    /**
     * Sets the expected end time.
     *
     * @param endTime the end time
     * @return this
     */
    public V endTime(ValueStrategy endTime) {
        this.endTime = endTime;
        return getThis();
    }

    /**
     * Sets the expected reason.
     *
     * @param reason the reason
     * @return this
     */
    public V reason(String reason) {
        this.reason = ValueStrategy.value(reason);
        return getThis();
    }

    /**
     * Sets the status.
     *
     * @param status the status
     * @return this
     */
    public V status(String status) {
        this.status = ValueStrategy.value(status);
        return getThis();
    }

    /**
     * Sets the secondary status.
     *
     * @param status2 the secondary status
     * @return this
     */
    public V status2(String status2) {
        this.status2 = ValueStrategy.value(status2);
        return getThis();
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(T object, IMObjectBean bean) {
        super.verify(object, bean);
        checkEquals(startTime, object.getActivityStartTime());
        checkEquals(endTime, object.getActivityEndTime());
        checkEquals(reason, object.getReason());
        checkEquals(status, object.getStatus());
        checkEquals(status2, object.getStatus2());
    }
}
