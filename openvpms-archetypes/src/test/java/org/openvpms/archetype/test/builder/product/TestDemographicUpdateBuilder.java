/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.lookup.AbstractTestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>lookup.demographicUpdate</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDemographicUpdateBuilder extends AbstractTestLookupBuilder<TestDemographicUpdateBuilder> {

    /**
     * The node.
     */
    private ValueStrategy node = ValueStrategy.unset();

    /**
     * The expression.
     */
    private ValueStrategy expression = ValueStrategy.unset();

    /**
     * Constructs a {@link TestDemographicUpdateBuilder}.
     *
     * @param service the archetype service
     */
    public TestDemographicUpdateBuilder(ArchetypeService service) {
        super(ProductArchetypes.DEMOGRAPHIC_UPDATE, service);
    }

    /**
     * Sets the node.
     *
     * @param node the node
     * @return this
     */
    public TestDemographicUpdateBuilder node(String node) {
        this.node = ValueStrategy.value(node);
        return this;
    }

    /**
     * Sets the expression.
     *
     * @param expression the expression
     * @return this
     */
    public TestDemographicUpdateBuilder expression(String expression) {
        this.expression = ValueStrategy.value(expression);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        node.setValue(bean, "nodeName");
        expression.setValue(bean, "expression");
    }
}