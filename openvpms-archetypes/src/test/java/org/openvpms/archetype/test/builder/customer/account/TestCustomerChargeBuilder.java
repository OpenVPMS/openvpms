/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * Builds <em>act.customerAccountCharges*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestCustomerChargeBuilder<B extends TestCustomerChargeBuilder<B, I>,
        I extends TestCustomerChargeItemBuilder<B, I>>
        extends AbstractTestCustomerAccountActBuilder<B> {

    /**
     * The charge items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The built items. Available after building.
     */
    private List<FinancialAct> builtItems = new ArrayList<>();

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Determines if reminders should be sent.
     */
    private ValueStrategy sendReminder = ValueStrategy.unset();

    /**
     * Constructs an {@link TestCustomerChargeBuilder}.
     *
     * @param service the archetype service
     */
    public TestCustomerChargeBuilder(String archetype, ArchetypeService service) {
        super(archetype, service);
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Adds a charge item.
     *
     * @param items the items to add
     * @return this
     */
    public B add(FinancialAct... items) {
        return add(Arrays.asList(items));
    }

    /**
     * Adds charge items.
     *
     * @param items the items to add
     * @return this
     */
    public B add(List<FinancialAct> items) {
        this.items.addAll(items);
        return getThis();
    }

    /**
     * Returns a builder to add a charge item.
     *
     * @return a charge item builder
     */
    public abstract I item();

    /**
     * Returns the built items.
     *
     * @return the built items
     */
    public List<FinancialAct> getItems() {
        return builtItems;
    }

    /**
     * Returns the first built item with the specified product.
     *
     * @param product the product
     * @return the corresponding item
     */
    public FinancialAct getItem(Product product) {
        FinancialAct act = FinancialTestHelper.find(builtItems, product);
        assertNotNull(act);
        return act;
    }

    /**
     * Determines if reminders should be sent for unpaid counter sales.
     *
     * @param sendReminder if {@code true}, send reminders
     * @return this
     */
    public B sendReminder(boolean sendReminder) {
        this.sendReminder = ValueStrategy.value(sendReminder);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        int sequence = 0;
        for (FinancialAct item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            relationship.setSequence(sequence++);
            item.addActRelationship(relationship);
            toSave.add(item);
            IMObjectBean itemBean = bean.getBean(item);
            tax = tax.add(itemBean.getBigDecimal("tax", BigDecimal.ZERO));
            amount = amount.add(itemBean.getBigDecimal("total", BigDecimal.ZERO));
        }
        bean.setValue("amount", amount);
        bean.setValue("tax", tax);
        sendReminder.setValue(bean, "sendReminder");
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }
}
