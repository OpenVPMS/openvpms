/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>contact.*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestContactBuilder<T extends Party, P extends AbstractTestPartyBuilder<T, P>,
        B extends AbstractTestContactBuilder<T, P, B>>
        extends AbstractTestIMObjectBuilder<Contact, B> {

    /**
     * The parent builder. May be {@code null}.
     */
    private final P parent;

    /**
     * Determines if the contact is preferred.
     */
    private ValueStrategy preferred = ValueStrategy.unset();

    /**
     * The contact purposes.
     */
    private String[] purposes;

    /**
     * Constructs an {@link AbstractTestContactBuilder}.
     *
     * @param archetype the archetype
     * @param service   the archetype service
     */
    public AbstractTestContactBuilder(String archetype, ArchetypeService service) {
        this(null, archetype, service);
    }

    /**
     * Constructs an {@link AbstractTestContactBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype
     * @param service   the archetype service
     */
    public AbstractTestContactBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, Contact.class, service);
        this.parent = parent;
    }

    /**
     * Constructs an {@link AbstractTestContactBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestContactBuilder(Contact object, ArchetypeService service) {
        super(object, service);
        parent = null;
    }

    /**
     * Marks the contact as preferred.
     *
     * @return this
     */
    public B preferred() {
        return preferred(true);
    }

    /**
     * Determines if the contact is preferred.
     *
     * @param preferred if {@code true} the contact is the preferred contact
     * @return this
     */
    public B preferred(boolean preferred) {
        this.preferred = ValueStrategy.value(preferred);
        return getThis();
    }

    /**
     * Sets the contact purposes.
     *
     * @param purposes the purposes
     * @return this
     */
    public B purposes(String... purposes) {
        this.purposes = purposes;
        return getThis();
    }

    /**
     * Builds the contact.
     *
     * @return the contact
     */
    @Override
    public Contact build() {
        return build(false);
    }

    /**
     * Adds the item to the parent builder.
     *
     * @return the parent builder
     */
    public P add() {
        return parent.addContact(build());
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Contact object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        preferred.setValue(bean, "preferred");
        if (purposes != null) {
            TestLookupBuilder builder = new TestLookupBuilder(ContactArchetypes.PURPOSE, getService());
            for (String purpose : purposes) {
                object.addClassification(builder.code(purpose).build());
            }
        }
    }
}
