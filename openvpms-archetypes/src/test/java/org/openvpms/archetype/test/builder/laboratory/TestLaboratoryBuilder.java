/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.laboratoryService*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLaboratoryBuilder extends AbstractTestEntityBuilder<Entity, TestLaboratoryBuilder> {

    /**
     * The locations the laboratory is applicable for.
     */
    private Party[] locations;

    /**
     * Constructs a {@link TestLaboratoryBuilder}.
     * <p/>
     * This creates <em>entity.laboratoryServiceTest</em> instances.
     *
     * @param service the archetype service
     */
    public TestLaboratoryBuilder(ArchetypeService service) {
        this("entity.laboratoryServiceTest", service);
    }

    /**
     * Constructs a {@link TestLaboratoryBuilder}.
     * <p/>
     * This creates <em>entity.laboratoryServiceTest</em> instances.
     *
     * @param archetype the <em>entity.laboratoryService*</em> archetype
     * @param service   the archetype service
     */
    public TestLaboratoryBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
    }

    /**
     * Sets the locations that the laboratory is applicable for.
     *
     * @param locations the locations
     * @return this
     */
    public TestLaboratoryBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location);
            }
        }
    }
}
