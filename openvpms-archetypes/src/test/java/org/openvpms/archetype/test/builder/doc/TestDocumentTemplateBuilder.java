/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>entity.documentTemplate</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentTemplateBuilder extends AbstractTestDocumentTemplateBuilder<TestDocumentTemplateBuilder> {

    /**
     * The <em>entityRelationship.documentTemplatePrinter</em>s and their associated locations.
     */
    private final Map<EntityRelationship, Party> printers = new LinkedHashMap<>();

    /**
     * The document template type code.
     */
    private String type;

    /**
     * If {@code true}, reuse the existing entity with lowest id of the specified type.
     */
    private boolean singletonByType;

    /**
     * If {@code true}, reuse the existing entity with lowest id of the specified content name.
     */
    private boolean singletonByContent;

    /**
     * The document name, when {@code singletonByContent == true}.
     */
    private String contentName;

    /**
     * The user level.
     */
    private ValueStrategy userLevel = ValueStrategy.unset();

    /**
     * The report type code.
     */
    private String reportType;

    /**
     * The print mode.
     */
    private ValueStrategy printMode = ValueStrategy.unset();

    /**
     * The paper size.
     */
    private ValueStrategy paperSize = ValueStrategy.unset();

    /**
     * The orientation.
     */
    private ValueStrategy orientation = ValueStrategy.unset();

    /**
     * The output format.
     */
    private ValueStrategy outputFormat = ValueStrategy.unset();

    /**
     * The number of copies.
     */
    private ValueStrategy copies = ValueStrategy.unset();

    /**
     * The paper height.
     */
    private ValueStrategy paperHeight = ValueStrategy.unset();

    /**
     * The paper width.
     */
    private ValueStrategy paperWidth = ValueStrategy.unset();

    /**
     * The paper units.
     */
    private ValueStrategy paperUnits = ValueStrategy.unset();

    /**
     * The email template.
     */
    private Entity emailTemplate;

    /**
     * The SMS template.
     */
    private Entity smsTemplate;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TestDocumentTemplateBuilder.class);

    /**
     * Constructs a {@link TestDocumentTemplateBuilder}.
     *
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    public TestDocumentTemplateBuilder(ArchetypeService service, DocumentHandlers handlers) {
        super(DocumentArchetypes.DOCUMENT_TEMPLATE, service, handlers);
        name(ValueStrategy.random("zdocumenttemplate"));
    }

    /**
     * Constructs a {@link TestDocumentTemplateBuilder}.
     *
     * @param template the template to update
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    TestDocumentTemplateBuilder(Entity template, ArchetypeService service, DocumentHandlers handlers) {
        super(template, service, handlers);
    }

    /**
     * Sets the document template type.
     *
     * @param type the <em>lookup.documentTemplateType</em> code.
     * @return this
     */
    public TestDocumentTemplateBuilder type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Requires the existing instance of the template to be used, if present.
     * <p/>
     * Ignored if a template was specified at construction to be updated.
     *
     * @return this
     */
    public TestDocumentTemplateBuilder singletonByType() {
        this.singletonByType = true;
        return this;
    }

    /**
     * Requires the existing instance of the template to be used, if one exists with the same content name.
     * <p/>
     * This is required for JasperReports subreports which are loaded by content name.
     * <p/>
     * Ignored if a template was specified at construction to be updated.
     *
     * @param name the content name
     * @return this
     */
    public TestDocumentTemplateBuilder singletonByContent(String name) {
        this.singletonByContent = true;
        this.contentName = name;
        return this;
    }

    /**
     * Sets the user level that the template applies to.
     *
     * @param userLevel the user level
     * @return the this
     */
    public TestDocumentTemplateBuilder userLevel(String userLevel) {
        this.userLevel = ValueStrategy.value(userLevel);
        return this;
    }

    /**
     * Sets the report type code.
     *
     * @param reportType the report type code
     * @return this
     */
    public TestDocumentTemplateBuilder reportType(String reportType) {
        this.reportType = reportType;
        return this;
    }

    /**
     * Sets the print mode.
     *
     * @param printMode the print mode
     * @return this
     */
    public TestDocumentTemplateBuilder printMode(DocumentTemplate.PrintMode printMode) {
        this.printMode = ValueStrategy.value((printMode != null) ? printMode.toString() : null);
        return this;
    }

    /**
     * Sets the paper size.
     *
     * @param paperSize the paper size
     * @return this
     */
    public TestDocumentTemplateBuilder paperSize(String paperSize) {
        this.paperSize = ValueStrategy.value(paperSize);
        return this;
    }

    /**
     * Sets the orientation.
     *
     * @param orientation the orientation
     * @return this
     */
    public TestDocumentTemplateBuilder orientation(String orientation) {
        this.orientation = ValueStrategy.value(orientation);
        return this;
    }

    /**
     * Sets the output format.
     *
     * @param outputFormat the output format
     * @return this
     */
    public TestDocumentTemplateBuilder outputFormat(String outputFormat) {
        this.outputFormat = ValueStrategy.value(outputFormat);
        return this;
    }

    /**
     * Sets the number of copies to print.
     *
     * @param copies the number of copies
     * @return this
     */
    public TestDocumentTemplateBuilder copies(int copies) {
        this.copies = ValueStrategy.value(copies);
        return this;
    }

    /**
     * Sets the paper height.
     *
     * @param paperHeight the paper height
     */
    public TestDocumentTemplateBuilder paperHeight(BigDecimal paperHeight) {
        this.paperHeight = ValueStrategy.value(paperHeight);
        return this;
    }

    /**
     * Sets the paper width.
     *
     * @param paperWidth the paper width
     * @return this
     */
    public TestDocumentTemplateBuilder paperWidth(BigDecimal paperWidth) {
        this.paperWidth = ValueStrategy.value(paperWidth);
        return this;
    }

    /**
     * Sets the paper units.
     *
     * @param paperUnits the paper units
     */
    public TestDocumentTemplateBuilder paperUnits(String paperUnits) {
        this.paperUnits = ValueStrategy.value(paperUnits);
        return this;
    }

    /**
     * Returns a builder for the email template.
     *
     * @return an email template builder
     */
    public TestEmailTemplateBuilder emailTemplate() {
        return emailTemplate(DocumentArchetypes.USER_EMAIL_TEMPLATE);
    }

    /**
     * Returns a builder for the email template.
     *
     * @param archetype the email template archetype
     * @return an email template builder
     */
    public TestEmailTemplateBuilder emailTemplate(String archetype) {
        return new TestEmailTemplateBuilder(this, archetype, getService(), getHandlers());
    }

    /**
     * Sets a plain text email template.
     *
     * @param subject the email subject
     * @param content the email content
     * @return this
     */
    public TestDocumentTemplateBuilder emailTemplate(String subject, String content) {
        return emailTemplate().subject(subject).content(content).add();
    }

    /**
     * Sets the email template.
     *
     * @param emailTemplate the email template
     * @return this
     */
    public TestDocumentTemplateBuilder emailTemplate(Entity emailTemplate) {
        this.emailTemplate = emailTemplate;
        return this;
    }

    /**
     * Returns the email template.
     *
     * @return the email template, or {@code null} if one wasn't built
     */
    public Entity getEmailTemplate() {
        return emailTemplate;
    }

    /**
     * Returns a builder for the SMS template.
     *
     * @return an SMS template builder
     */
    public TestSMSTemplateBuilder smsTemplate() {
        return new TestSMSTemplateBuilder(this, DocumentArchetypes.REMINDER_SMS_TEMPLATE, getService());
    }

    /**
     * Sets the SMS template.
     *
     * @param smsTemplate the SMS template
     * @return this
     */
    public TestDocumentTemplateBuilder smsTemplate(Entity smsTemplate) {
        this.smsTemplate = smsTemplate;
        return this;
    }

    /**
     * Sets a plain text SMS template.
     *
     * @param content the SMS content
     * @return this
     */
    public TestDocumentTemplateBuilder smsTemplate(String content) {
        return smsTemplate().content(content).add();
    }

    /**
     * Returns the SMS template.
     *
     * @return the SMS template, or {@code null} if one wasn't built
     */
    public Entity getSMSTemplate() {
        return smsTemplate;
    }

    /**
     * Returns printer builder for the template.
     *
     * @return a new printer builder
     */
    public TestDocumentTemplatePrinterBuilder printer() {
        return new TestDocumentTemplatePrinterBuilder(this, getService());
    }

    /**
     * Returns printer builder for the template.
     *
     * @param printer the printer name
     * @return a new printer builder
     */
    public TestDocumentTemplatePrinterBuilder printer(String printer) {
        return printer().printer(printer);
    }

    /**
     * Adds a printer relationship.
     *
     * @param relationship the relationship
     * @param location     the location the printer applies to
     */
    protected void addPrinter(EntityRelationship relationship, Party location) {
        printers.put(relationship, location);
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Entity getObject(String archetype) {
        Entity result = getExisting();
        if (result == null) {
            if (singletonByType && type != null) {
                // reuse the active template matching the type with the lowest id, if present
                TemplateHelper helper = new TemplateHelper(getService());
                result = helper.getTemplateForType(type);
                if (result != null) {
                    DocumentAct act = helper.getDocumentAct(result);
                    if (act != null) {
                        templateAct(act);
                    }
                }
            } else if (singletonByContent) {
                TemplateHelper helper = new TemplateHelper(getService());
                DocumentAct act = helper.getDocumentAct(contentName);
                if (act != null) {
                    templateAct(act);
                    Entity template = getBean(act).getTarget("template", Entity.class);
                    if (template.isA(archetype)) {
                        result = template;
                    } else {
                        // the existing template shares content with the same name as the new template. It will
                        // be unlinked
                        log.warn("Existing template {} will no longer be able to use content from {}",
                                 template.getObjectReference(), act.getObjectReference());
                    }
                }
            }
            if (result == null) {
                result = create(archetype, Entity.class);
            }
        }
        return result;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (type != null) {
            bean.removeValues("type");
            TestLookupBuilder lookupBuilder = new TestLookupBuilder(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE,
                                                                    getService());
            Lookup lookup = lookupBuilder.code(type).build();
            object.addClassification(lookup);
        }
        userLevel.setValue(bean, "userLevel");
        if (reportType != null) {
            TestLookupBuilder lookupBuilder = new TestLookupBuilder("lookup.reportType", getService());
            Lookup lookup = lookupBuilder.code(reportType).build();
            bean.setValue("reportType", lookup.getCode());
        }
        printMode.setValue(bean, "printMode");
        paperSize.setValue(bean, "paperSize");
        orientation.setValue(bean, "orientation");
        outputFormat.setValue(bean, "outputFormat");
        copies.setValue(bean, "copies");
        paperHeight.setValue(bean ,"paperHeight");
        paperWidth.setValue(bean ,"paperWidth");
        paperUnits.setValue(bean, "paperUnits");
        if (emailTemplate != null) {
            bean.setTarget("email", emailTemplate);
            toSave.add(emailTemplate);
        }
        if (smsTemplate != null) {
            bean.setTarget("sms", smsTemplate);
            toSave.add(smsTemplate);
        }

        for (Map.Entry<EntityRelationship, Party> entry : printers.entrySet()) {
            EntityRelationship printer = entry.getKey();
            Party location = entry.getValue();
            printer.setSource(object.getObjectReference());
            printer.setTarget(location.getObjectReference());
            object.addEntityRelationship(printer);
            location.addEntityRelationship(printer);
            toSave.add(location);
        }
        printers.clear(); // can't reuse
    }
}