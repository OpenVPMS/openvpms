/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.user;

import org.apache.commons.lang3.RandomUtils;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>security.user</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestUserBuilder extends AbstractTestPartyBuilder<User, TestUserBuilder> {

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * The user name.
     */
    private ValueStrategy username = ValueStrategy.random("zuser");

    /**
     * The password.
     */
    private ValueStrategy password = ValueStrategy.random("zpassword");

    /**
     * The change password flag.
     */
    private ValueStrategy changePassword = ValueStrategy.unset();

    /**
     * The title code.
     */
    private String titleCode;

    /**
     * The first name.
     */
    private String firstName;

    /**
     * The last name.
     */
    private String lastName;

    /**
     * The qualifications.
     */
    private String qualifications;

    /**
     * Determines if the user is a clinician.
     */
    private boolean clinician;

    /**
     * Determines if the user is an administrator.
     */
    private boolean administrator;

    /**
     * Determines if the user is available for online booking.
     */
    private ValueStrategy onlineBooking = ValueStrategy.unset();

    /**
     * Determines if the user can connect from anywere.
     */
    private ValueStrategy connectFromAnywhere = ValueStrategy.unset();

    /**
     * The locations the user works at.
     */
    private Party[] locations;

    /**
     * The user roles.
     */
    private SecurityRole[] roles;

    /**
     * The departments.
     */
    private Entity[] departments;

    /**
     * The default department.
     */
    private Entity defaultDepartment;

    /**
     * Determines if a signature should be added.
     */
    private Boolean signature;

    /**
     * Constructs a {@link TestUserBuilder}.
     *
     * @param service         the archetype service
     * @param userRules       the user rules
     * @param documentFactory the document factory
     */
    public TestUserBuilder(ArchetypeService service, UserRules userRules, TestDocumentFactory documentFactory) {
        super(UserArchetypes.USER, User.class, service);
        this.userRules = userRules;
        this.documentFactory = documentFactory;
    }

    /**
     * Constructs a {@link TestUserBuilder}.
     *
     * @param object          the object to update
     * @param service         the archetype service
     * @param userRules       the user rules
     * @param documentFactory the document factory
     */
    public TestUserBuilder(User object, ArchetypeService service, UserRules userRules,
                           TestDocumentFactory documentFactory) {
        super(object, service);
        this.userRules = userRules;
        this.documentFactory = documentFactory;
    }

    /**
     * Sets the user name. This must be unique.
     *
     * @param username the user name
     * @return this
     */
    public TestUserBuilder username(String username) {
        return username(ValueStrategy.value(username));
    }

    /**
     * Sets the username. This must be unique.
     *
     * @param username the username
     * @return this
     */
    public TestUserBuilder username(ValueStrategy username) {
        this.username = username;
        return this;
    }

    /**
     * Sets the password.
     *
     * @param password the password
     * @return this
     */
    public TestUserBuilder password(String password) {
        this.password = ValueStrategy.value(password);
        return this;
    }

    /**
     * Determines if the user needs to change their password.
     *
     * @param changePassword if {@code true}, the user needs to change their password
     * @return this
     */
    public TestUserBuilder changePassword(boolean changePassword) {
        this.changePassword = ValueStrategy.value(changePassword);
        return this;
    }

    /**
     * Sets the title.
     *
     * @param titleCode the <em>lookup.title</em> code
     * @return this
     */
    public TestUserBuilder title(String titleCode) {
        this.titleCode = titleCode;
        return this;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestUserBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     * @return this
     */
    public TestUserBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Sets the qualifications.
     *
     * @param qualifications the qualifications
     * @return this
     */
    public TestUserBuilder qualifications(String qualifications) {
        this.qualifications = qualifications;
        return this;
    }

    /**
     * Indicates the user is a clinician.
     *
     * @return this
     */
    public TestUserBuilder clinician() {
        this.clinician = true;
        return this;
    }

    /**
     * Indicates the user is an administrator.
     *
     * @return this
     */
    public TestUserBuilder administrator() {
        this.administrator = true;
        return this;
    }

    /**
     * Make the user available for online booking.
     *
     * @return this
     */
    public TestUserBuilder onlineBooking() {
        return onlineBooking(true);
    }

    /**
     * Determines if a user can connect from anywhere, when the firewall is enabled.
     *
     * @param connectFromAnywhere if {@code true}, the user can connect from anywhere
     * @return this
     */
    public TestUserBuilder connectFromAnywhere(boolean connectFromAnywhere) {
        this.connectFromAnywhere = ValueStrategy.value(connectFromAnywhere);
        return this;
    }

    /**
     * Determines if the user is available for online booking.
     *
     * @param onlineBooking if {@code true}, the user is available for online booking
     * @return this
     */
    public TestUserBuilder onlineBooking(boolean onlineBooking) {
        this.onlineBooking = ValueStrategy.value(onlineBooking);
        return this;
    }

    /**
     * Sets the locations the user works at.
     *
     * @param locations the practice locations
     * @return this
     */
    public TestUserBuilder addLocations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Adds roles.
     *
     * @param roles the roles
     * @return this
     */
    public TestUserBuilder addRoles(SecurityRole... roles) {
        this.roles = roles;
        return this;
    }

    /**
     * Adds departments.
     *
     * @param departments the departments
     * @return this
     */
    public TestUserBuilder addDepartments(Entity... departments) {
        this.departments = departments;
        return this;
    }

    /**
     * Sets the default department.
     *
     * @param department the default department
     * @return this
     */
    public TestUserBuilder defaultDepartment(Entity department) {
        this.defaultDepartment = department;
        return this;
    }

    /**
     * Adds a signature, if one isn't already present.
     *
     * @return this
     */
    public TestUserBuilder signature() {
        signature = true;
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(User object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        Object usernameValue = username.setValue(bean, "username");
        Lookup title = (titleCode != null) ? new TestLookupBuilder(UserArchetypes.TITLE, getService())
                .code(titleCode).build() : null;

        if (getName().isUnset()) {
            if (firstName != null && lastName != null) {
                StringBuilder builder = new StringBuilder();
                if (title != null) {
                    builder.append(title.getName()).append(' ');
                }
                builder.append(firstName).append(' ').append(lastName);
                object.setName(builder.toString());
            } else if (usernameValue != null) {
                // make the username and name the same
                object.setName(usernameValue.toString());
            }
        }
        password.setValue(bean, "password");
        changePassword.setValue(bean, "changePassword");
        if (title != null) {
            bean.setValue("title", title.getCode());
        }
        if (firstName != null) {
            bean.setValue("firstName", firstName);
        }
        if (lastName != null) {
            bean.setValue("lastName", lastName);
        }
        if (qualifications != null) {
            bean.setValue("qualifications", qualifications);
        }
        if (clinician) {
            object.addClassification(getUserType(UserArchetypes.CLINICIAN_USER_TYPE));
        }
        if (administrator) {
            object.addClassification(getUserType(UserArchetypes.ADMINISTRATOR_USER_TYPE));
        }
        onlineBooking.setValue(bean, "onlineBooking");
        connectFromAnywhere.setValue(bean, "connectFromAnywhere");

        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location);
            }
        }
        if (roles != null) {
            for (SecurityRole role : roles) {
                bean.addValue("roles", role);
            }
        }
        if (departments != null) {
            for (Entity department : departments) {
                bean.addTarget("departments", department);
            }
        }
        if (defaultDepartment != null) {
            for (EntityLink link : bean.getValues("departments", EntityLink.class)) {
                IMObjectBean linkBean = getBean(link);
                if (link.getTarget().equals(defaultDepartment.getObjectReference())) {
                    linkBean.setValue("default", true);
                } else {
                    linkBean.setValue("default", false);
                }
            }
        }
        if (signature != null && signature) {
            addSignature(object, toSave);
        }
    }

    /**
     * Adds a signature, if one isn't already present.
     *
     * @param object the user to build
     * @param toSave objects to save, if the entity is to be saved
     */
    private void addSignature(User object, Set<IMObject> toSave) {
        if (object.isNew() || userRules.getSignature(object) == null) {
            Document document = documentFactory.newDocument().name(ValueStrategy.suffix(".png"))
                    .content(RandomUtils.nextBytes(10))
                    .build(false);
            DocumentAct signature = create(UserArchetypes.SIGNATURE, DocumentAct.class);
            signature.setFileName(document.getName());
            IMObjectBean bean = getBean(signature);
            bean.setTarget("user", object);
            signature.setDocument(document.getObjectReference());
            toSave.add(document);
            toSave.add(signature);
        }
    }

    /**
     * Returns a user type.
     *
     * @param code the code
     * @return lookup
     */
    private Lookup getUserType(String code) {
        return new TestLookupBuilder(UserArchetypes.USER_TYPE, getService()).code(code).build();
    }
}
