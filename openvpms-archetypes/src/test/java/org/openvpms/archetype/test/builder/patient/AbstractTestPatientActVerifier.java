/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.test.builder.act.AbstractTestActVerifier;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Verifier of patient acts, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestPatientActVerifier<T extends Act, V extends AbstractTestPatientActVerifier<T, V>>
        extends AbstractTestActVerifier<T, V> {

    /**
     * The expected patient.
     */
    private ValueStrategy patient = ValueStrategy.unset();

    /**
     * The expected practice location.
     */
    private ValueStrategy location = ValueStrategy.unset();

    /**
     * The expected clinician.
     */
    private ValueStrategy clinician = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestPatientActVerifier}.
     *
     * @param service the archetype service
     */
    public AbstractTestPatientActVerifier(ArchetypeService service) {
        super(service);
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public V patient(Party patient) {
        this.patient = ValueStrategy.value(getReference(patient));
        return getThis();
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public V location(Party location) {
        this.location = ValueStrategy.value(getReference(location));
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public V clinician(User clinician) {
        this.clinician = ValueStrategy.value(getReference(clinician));
        return getThis();
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(T object, IMObjectBean bean) {
        super.verify(object, bean);
        checkTargetEquals(patient, bean, "patient");
        checkTargetEquals(location, bean, "location");
        checkTargetEquals(clinician, bean, "clinician");
    }
}