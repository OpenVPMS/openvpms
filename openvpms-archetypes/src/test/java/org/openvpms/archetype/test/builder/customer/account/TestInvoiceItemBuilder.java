/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerAccountInvoiceItem</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestInvoiceItemBuilder extends TestCustomerChargeItemBuilder<TestInvoiceBuilder,
        TestInvoiceItemBuilder> {

    /**
     * The investigation acts.
     */
    private final List<DocumentAct> investigations = new ArrayList<>();

    /**
     * The document acts.
     */
    private final List<DocumentAct> documents = new ArrayList<>();

    /**
     * The reminder acts.
     */
    private final List<Act> reminders = new ArrayList<>();

    /**
     * The alert acts.
     */
    private final List<Act> alerts = new ArrayList<>();

    /**
     * The task acts.
     */
    private final List<Act> tasks = new ArrayList<>();

    /**
     * The medication act.
     */
    private Act medication;

    /**
     * The visit act.
     */
    private Act visit;

    /**
     * Constructs a {@link TestInvoiceItemBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestInvoiceItemBuilder(TestInvoiceBuilder parent, ArchetypeService service) {
        super(parent, CustomerAccountArchetypes.INVOICE_ITEM, service);
    }

    /**
     * Constructs a {@link TestInvoiceItemBuilder}.
     *
     * @param parent  the parent builder
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestInvoiceItemBuilder(TestInvoiceBuilder parent, FinancialAct object, ArchetypeService service) {
        super(parent, object, service);
    }

    /**
     * Associates a medication act with the invoice item.
     *
     * @param medication the medication act
     * @return this
     */
    public TestInvoiceItemBuilder medication(Act medication) {
        this.medication = medication;
        return this;
    }

    /**
     * Adds an investigation.
     *
     * @param investigation the investigation
     * @return this
     */
    public TestInvoiceItemBuilder addInvestigation(DocumentAct investigation) {
        return addInvestigations(investigation);
    }

    /**
     * Adds investigations.
     *
     * @param investigations the investigations to add
     * @return this
     */
    public TestInvoiceItemBuilder addInvestigations(DocumentAct ... investigations) {
        this.investigations.addAll(Arrays.asList(investigations));
        return this;
    }

    /**
     * Adds investigations.
     *
     * @param investigations the investigations to add
     * @return this
     */
    public TestInvoiceItemBuilder addInvestigations(List<DocumentAct> investigations) {
        this.investigations.addAll(investigations);
        return this;
    }

    /**
     * Adds a document.
     *
     * @param document the document to add
     * @return this
     */
    public TestInvoiceItemBuilder addDocument(DocumentAct document) {
        return addDocuments(document);
    }

    /**
     * Adds documents.
     *
     * @param documents the document to add
     * @return this
     */
    public TestInvoiceItemBuilder addDocuments(DocumentAct ... documents) {
        this.documents.addAll(Arrays.asList(documents));
        return this;
    }

    /**
     * Adds a reminder.
     *
     * @param reminder the reminder
     * @return this
     */
    public TestInvoiceItemBuilder addReminder(Act reminder) {
        return addReminders(reminder);
    }

    /**
     * Adds reminders.
     *
     * @param reminders the reminders
     * @return this
     */
    public TestInvoiceItemBuilder addReminders(Act ... reminders) {
        return addReminders(Arrays.asList(reminders));
    }

    /**
     * Adds reminders.
     *
     * @param reminders the reminders
     * @return this
     */
    public TestInvoiceItemBuilder addReminders(List<Act> reminders) {
        this.reminders.addAll(reminders);
        return this;
    }

    /**
     * Adds an alert.
     *
     * @param alert the alert
     * @return this
     */
    public TestInvoiceItemBuilder addAlert(Act alert) {
        return addAlerts(alert);
    }

    /**
     * Adds alerts.
     *
     * @param alerts the alerts
     * @return this
     */
    public TestInvoiceItemBuilder addAlerts(Act ... alerts) {
        return addAlerts(Arrays.asList(alerts));
    }

    /**
     * Adds alerts.
     *
     * @param alerts the alerts
     * @return this
     */
    public TestInvoiceItemBuilder addAlerts(List<Act> alerts) {
        this.alerts.addAll(alerts);
        return this;
    }

    /**
     * Adds a task.
     *
     * @param task the task
     * @return this
     */
    public TestInvoiceItemBuilder addTask(Act task) {
        return addTasks(task);
    }

    /**
     * Adds tasks.
     *
     * @param tasks the tasks
     * @return this
     */
    public TestInvoiceItemBuilder addTasks(Act... tasks) {
        return addTasks(Arrays.asList(tasks));
    }

    /**
     * Adds tasks.
     *
     * @param tasks the tasks
     * @return this
     */
    public TestInvoiceItemBuilder addTasks(List<Act> tasks) {
        this.tasks.addAll(tasks);
        return this;
    }

    /**
     * Sets the visit.
     *
     * @param visit the visit
     * @return this
     */
    public TestInvoiceItemBuilder visit(Act visit) {
        this.visit = visit;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (medication != null) {
            bean.addTarget("dispensing", medication, "invoiceItem");
            toSave.add(medication);
            medication = null; // can't reuse
        }
        if (!investigations.isEmpty()) {
            for (DocumentAct investigation : investigations) {
                bean.addTarget("investigations", investigation, "invoiceItems");
                toSave.add(investigation);
            }
            investigations.clear();  // can't reuse
        }
        if (!documents.isEmpty()) {
            for (DocumentAct document : documents) {
                bean.addTarget("documents", document, "invoiceItem");
                toSave.add(document);
            }
        }
        if (!reminders.isEmpty()) {
            for (Act reminder : reminders) {
                bean.addTarget("reminders", reminder, "invoiceItem");
                toSave.add(reminder);
            }
            reminders.clear(); // can't reuse
        }
        if (!alerts.isEmpty()) {
            for (Act alert : alerts) {
                bean.addTarget("alerts", alert, "invoiceItem");
                toSave.add(alert);
            }
            alerts.clear(); // can't reuse
        }
        if (!tasks.isEmpty()) {
            for (Act task : tasks) {
                bean.addTarget("tasks", task, "invoiceItem");
                toSave.add(task);
            }
            tasks.clear(); // can't reuse
        }
        if (visit != null) {
            bean.addSource("event", visit, "chargeItems");
            toSave.add(visit);
            visit = null; // can't reuse
        }
    }
}
