/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Verifies a credit item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestCreditItemVerifier extends TestChargeItemVerifier<TestCreditVerifier, TestCreditItemVerifier> {

    /**
     * Constructs a {@link TestCreditItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestCreditItemVerifier(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link TestCreditItemVerifier}.
     *
     * @param parent  the parent credit verifier
     * @param service the archetype service
     */
    public TestCreditItemVerifier(TestCreditVerifier parent, ArchetypeService service) {
        super(parent, service);
        archetype(CustomerAccountArchetypes.CREDIT_ITEM);
    }
}