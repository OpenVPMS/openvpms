/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>entity.investigationType</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestInvestigationTypeBuilder extends AbstractTestEntityBuilder<Entity, TestInvestigationTypeBuilder> {

    /**
     * Account identifiers keyed on their locations.
     */
    private final Map<Party, String> accountIds = new HashMap<>();

    /**
     * The typeId archetype.
     */
    private String typeIdArchetype;

    /**
     * The type id.
     */
    private ValueStrategy typeId = ValueStrategy.unset();

    /**
     * The type id name.
     */
    private ValueStrategy typeIdName = ValueStrategy.unset();

    /**
     * The laboratory that provides this investigation type.
     */
    private Entity laboratory;

    /**
     * The devices that may perform the investigation type.
     */
    private Entity[] devices;

    /**
     * Constructs a {@link TestInvestigationTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestInvestigationTypeBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.INVESTIGATION_TYPE, Entity.class, service);
        name(ValueStrategy.random("zinvestigationtype"));
    }

    /**
     * Sets the type id.
     *
     * @param typeId the type id
     * @return this
     */
    public TestInvestigationTypeBuilder typeId(String typeId) {
        return typeId("entityIdentity.investigationTypeTest", ValueStrategy.value(typeId),
                      ValueStrategy.value(typeId));
    }

    /**
     * Sets the type id.
     *
     * @param archetype the type id archetype
     * @param typeId    the type id
     * @return this
     */
    public TestInvestigationTypeBuilder typeId(String archetype, String typeId) {
        return typeId(archetype, ValueStrategy.value(typeId), ValueStrategy.unset());
    }

    /**
     * Sets the type id.
     *
     * @param archetype  the type id archetype
     * @param typeId     the type id
     * @param typeIdName the type id name
     * @return this
     */
    public TestInvestigationTypeBuilder typeId(String archetype, ValueStrategy typeId, ValueStrategy typeIdName) {
        typeIdArchetype = archetype;
        this.typeId = typeId;
        this.typeIdName = typeIdName;
        return this;
    }

    /**
     * Sets the laboratory that provides the investigation type.
     *
     * @param laboratory the laboratory
     * @return this
     */
    public TestInvestigationTypeBuilder laboratory(Entity laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Adds the devices that may perform this investigation type.
     *
     * @param devices the devices
     * @return the devices
     */
    public TestInvestigationTypeBuilder addDevices(Entity... devices) {
        this.devices = devices;
        return this;
    }

    /**
     * Adds an account identifier.
     *
     * @param accountId the account identifier
     * @param location  the practice location the account identifier applies to
     * @return this
     */
    public TestInvestigationTypeBuilder accountId(String accountId, Party location) {
        accountIds.put(location, accountId);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (typeId.isSet()) {
            EntityIdentity identity = newEntityIdentity(typeIdArchetype)
                    .identity(typeId)
                    .name(typeIdName)
                    .build();
            object.addIdentity(identity);
        }
        if (laboratory != null) {
            bean.setTarget("laboratory", laboratory);
        }
        if (devices != null) {
            for (Entity device : devices) {
                bean.addTarget("devices", device);
            }
        }
        for (Map.Entry<Party, String> entry : accountIds.entrySet()) {
            Relationship relationship = bean.addTarget("locations", entry.getKey());
            IMObjectBean relationshipBean = getBean(relationship);
            relationshipBean.setValue("accountId", entry.getValue());
        }
    }
}
