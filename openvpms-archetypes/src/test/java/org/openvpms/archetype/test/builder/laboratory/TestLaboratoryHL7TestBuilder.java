/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>entity.laboratoryTestHL7</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLaboratoryHL7TestBuilder extends AbstractTestLaboratoryTestBuilder<TestLaboratoryHL7TestBuilder> {

    /**
     * Constructs a {@link TestLaboratoryHL7TestBuilder}.
     *
     * @param service the archetype service
     */
    public TestLaboratoryHL7TestBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.HL7_TEST, service);
        name("zhl7test");
        code(ValueStrategy.random("zhl7test"));
    }

    /**
     * Sets the test code.
     *
     * @param code the test code
     * @return this
     */
    public TestLaboratoryHL7TestBuilder code(String code) {
        return code(ValueStrategy.value(code));
    }

    /**
     * Sets the test code.
     *
     * @param code the test code
     * @return this
     */
    public TestLaboratoryHL7TestBuilder code(ValueStrategy code) {
        code(LaboratoryArchetypes.TEST_CODE, code);
        return this;
    }
}