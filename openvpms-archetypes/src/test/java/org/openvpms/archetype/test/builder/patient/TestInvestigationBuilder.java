/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.laboratory.LaboratoryOrderStatus;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryTestBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.patientInvestigation</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestInvestigationBuilder extends AbstractTestPatientActBuilder<DocumentAct, TestInvestigationBuilder> {

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * The tests.
     */
    private final List<Entity> tests = new ArrayList<>();

    /**
     * The results.
     */
    private final List<TestInvestigationResultsBuilder> results = new ArrayList<>();

    /**
     * The investigation type.
     */
    private Entity investigationType;

    /**
     * The laboratory.
     */
    private Entity laboratory;

    /**
     * The device.
     */
    private Entity device;

    /**
     * The test names.
     */
    private String[] testNames;

    /**
     * The products.
     */
    private Product[] products;

    /**
     * The order status. If {@code null}, no order is created.
     */
    private String orderStatus;

    /**
     * Indicates if the order is a cancellation.
     */
    private boolean cancellation;

    /**
     * The report.
     */
    private Document report;

    /**
     * Constructs a {@link TestInvestigationBuilder}.
     *
     * @param service         the archetype service
     * @param laboratoryRules the laboratory rules
     */
    public TestInvestigationBuilder(ArchetypeService service, LaboratoryRules laboratoryRules) {
        super(InvestigationArchetypes.PATIENT_INVESTIGATION, DocumentAct.class, service);
        this.laboratoryRules = laboratoryRules;
    }

    /**
     * Constructs an {@link TestInvestigationBuilder}.
     *
     * @param object          the object to update
     * @param service         the archetype service
     * @param laboratoryRules the laboratory rules
     */
    public TestInvestigationBuilder(DocumentAct object, ArchetypeService service, LaboratoryRules laboratoryRules) {
        super(object, service);
        this.laboratoryRules = laboratoryRules;
    }

    /**
     * Sets the investigation type.
     *
     * @param investigationType the investigation type
     * @return this
     */
    public TestInvestigationBuilder investigationType(Entity investigationType) {
        this.investigationType = investigationType;
        return this;
    }

    /**
     * Sets the names of the tests to order.
     * <p/>
     * The tests will be created and linked to the investigation type.
     *
     * @param names the test names
     * @return this
     */
    public TestInvestigationBuilder addTests(String... names) {
        testNames = names;
        return this;
    }

    /**
     * Adds tests to order.
     *
     * @param tests the tests
     * @return this
     */
    public TestInvestigationBuilder addTests(Entity... tests) {
        this.tests.addAll(Arrays.asList(tests));
        return this;
    }

    /**
     * Sets the laboratory.
     *
     * @param laboratory the laboratory
     * @return this
     */
    public TestInvestigationBuilder laboratory(Entity laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Sets the device.
     *
     * @param device the device
     * @return this
     */
    public TestInvestigationBuilder device(Entity device) {
        this.device = device;
        return this;
    }

    /**
     * Sets the products that generated the investigation.
     *
     * @param products the products
     * @return this
     */
    public TestInvestigationBuilder products(Product... products) {
        this.products = products;
        return this;
    }

    /**
     * Creates an order for the investigation.
     *
     * @return this
     */
    public TestInvestigationBuilder order() {
        return order(LaboratoryOrderStatus.PENDING);
    }

    /**
     * Creates an order for the investigation.
     *
     * @param status the order status
     * @return this
     */
    public TestInvestigationBuilder order(String status) {
        return order(status, false);
    }

    /**
     * Creates an order for the investigation.
     *
     * @param status       the order status
     * @param cancellation if {@code true}, the order is a cancellation, otherwise it is a new order
     * @return this
     */
    public TestInvestigationBuilder order(String status, boolean cancellation) {
        this.orderStatus = status;
        this.cancellation = cancellation;
        return this;
    }

    /**
     * Sets the report.
     *
     * @param report the report
     * @return this
     */
    public TestInvestigationBuilder report(Document report) {
        this.report = report;
        return this;
    }

    /**
     * Returns a builder to add results.
     *
     * @return a results builder
     */
    public TestInvestigationResultsBuilder results() {
        return new TestInvestigationResultsBuilder(this, getService());
    }

    /**
     * Builds the entity.
     *
     * @param save if {@code true}, save the entity, and any related objects
     * @return the entity
     */
    @Override
    public DocumentAct build(boolean save) {
        if (!save && testNames != null && testNames.length != 0) {
            throw new IllegalArgumentException("Argument 'save' must be true when creating tests");
        }
        if (!save && orderStatus != null) {
            throw new IllegalArgumentException("Argument 'save' must be true when ordering investigations");
        }
        DocumentAct result = super.build(save);
        if (orderStatus != null) {
            // the investigation needs to be saved as the order has a copy of its id
            Act order = laboratoryRules.createOrder(result);
            order.setStatus(orderStatus);
            if (cancellation) {
                IMObjectBean bean = getBean(order);
                bean.setValue("type", "CANCEL");
            }
            getService().save(Arrays.asList(result, order));
        }
        return result;
    }

    /**
     * Adds results.
     *
     * @param results the results to add
     */
    protected void addResults(TestInvestigationResultsBuilder results) {
        this.results.add(results);
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);

        if (investigationType != null) {
            bean.setTarget("investigationType", investigationType);
        }
        if (laboratory != null) {
            bean.setTarget("laboratory", laboratory);
        }
        if (device != null) {
            bean.setTarget("device", device);
        }

        List<Entity> allTests = new ArrayList<>(tests);
        if (testNames != null && testNames.length != 0) {
            TestLaboratoryTestBuilder testBuilder = new TestLaboratoryTestBuilder(getService());
            for (String name : testNames) {
                Entity test = testBuilder.name(name).investigationType(investigationType).build();
                allTests.add(test);
                toSave.add(test);
            }
        }
        for (Entity test : allTests) {
            bean.addTarget("tests", test);
        }
        if (products != null) {
            for (Product product : products) {
                bean.addTarget("products", product);
            }
        }
        if (report != null) {
            DocumentRules rules = new DocumentRules(getService());
            toSave.addAll(rules.addDocument(object, report));
            report = null; // can't reuse
        }

        int sequence = getNextSequence(bean, "results");
        for (TestInvestigationResultsBuilder builder : results) {
            Act act = builder.build(toSave, toRemove);
            if (act.isNew()) {
                ActRelationship relationship = (ActRelationship) bean.addTarget("results", act, "investigation");
                relationship.setSequence(sequence++);
            }
        }
        results.clear(); // can't reuse
    }
}
