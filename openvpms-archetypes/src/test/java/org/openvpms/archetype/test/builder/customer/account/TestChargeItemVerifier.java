/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.apache.commons.collections4.IterableUtils;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.test.builder.act.AbstractTestActVerifier;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * Verifies a charge item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestChargeItemVerifier<P extends TestChargeVerifier<P, V>, V extends TestChargeItemVerifier<P, V>>
        extends AbstractTestActVerifier<FinancialAct, V> {

    /**
     * The parent verifier.
     */
    private final P parent;

    /**
     * The expected patient.
     */
    private ValueStrategy patient = ValueStrategy.value(null);

    /**
     * The expected product.
     */
    private ValueStrategy product = ValueStrategy.value(null);

    /**
     * The expected clinician.
     */
    private ValueStrategy clinician = ValueStrategy.value(null);

    /**
     * The expected quantity.
     */
    private ValueStrategy quantity = ValueStrategy.value(1);

    /**
     * The expected product template.
     */
    private ValueStrategy template = ValueStrategy.value(null);

    /**
     * The expected product template expansion group.
     */
    private ValueStrategy group = ValueStrategy.value(null);

    /**
     * The expected department.
     */
    private ValueStrategy department = ValueStrategy.value(null);

    /**
     * The expected fixed cost.
     */
    private ValueStrategy fixedCost = ValueStrategy.value(0);

    /**
     * The expected fixed price.
     */
    private ValueStrategy fixedPrice = ValueStrategy.value(0);

    /**
     * The expected unit cost.
     */
    private ValueStrategy unitCost = ValueStrategy.value(0);

    /**
     * The expected unit price.
     */
    private ValueStrategy unitPrice = ValueStrategy.value(0);

    /**
     * The expected service ratio.
     */
    private ValueStrategy serviceRatio = ValueStrategy.value(null);

    /**
     * The expected discount.
     */
    private ValueStrategy discount = ValueStrategy.value(0);

    /**
     * The expected tax amount.
     */
    private ValueStrategy tax = ValueStrategy.value(0);

    /**
     * The expected total.
     */
    private ValueStrategy total = ValueStrategy.value(0);

    /**
     * The expected print flag.
     */
    private ValueStrategy print = ValueStrategy.value(true);

    /**
     * Constructs a {@link TestChargeItemVerifier}.
     *
     * @param parent  the parent verifier
     * @param service the archetype service
     */
    public TestChargeItemVerifier(P parent, ArchetypeService service) {
        super(service);
        this.parent = parent;
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public V patient(Party patient) {
        return patient(getReference(patient));
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public V patient(Reference patient) {
        this.patient = ValueStrategy.value(patient);
        return getThis();
    }

    /**
     * Returns the patient reference.
     *
     * @return the patient reference. May be {@code null}
     */
    public Reference getPatient() {
        return (Reference) patient.getValue();
    }

    /**
     * Sets the expected product.
     *
     * @param product the expected product
     * @return this
     */
    public V product(Product product) {
        return product(getReference(product));
    }

    /**
     * Sets the expected product.
     *
     * @param product the expected product
     * @return this
     */
    public V product(Reference product) {
        this.product = ValueStrategy.value(product);
        return getThis();
    }

    /**
     * Returns the product reference.
     *
     * @return the product reference. May be {@code null}
     */
    public Reference getProduct() {
        return (Reference) product.getValue();
    }

    /**
     * Sets the expected clinician.
     *
     * @param clinician the expected clinician
     * @return this
     */
    public V clinician(User clinician) {
        return clinician(getReference(clinician));
    }

    /**
     * Sets the expected clinician.
     *
     * @param clinician the expected clinician
     * @return this
     */
    public V clinician(Reference clinician) {
        this.clinician = ValueStrategy.value(clinician);
        return getThis();
    }

    /**
     * Returns the clinician reference.
     *
     * @return the clinician reference. May be {@code null}
     */
    public Reference getClinician() {
        return (Reference) clinician.getValue();
    }

    /**
     * Sets the expected department.
     *
     * @param department the expected department
     * @return this
     */
    public V department(Entity department) {
        return department(getReference(department));
    }

    /**
     * Sets the expected department.
     *
     * @param department the expected department
     * @return this
     */
    public V department(Reference department) {
        this.department = ValueStrategy.value(department);
        return getThis();
    }

    /**
     * Sets the expected  quantity.
     *
     * @param quantity the expected quantity
     * @return this
     */
    public V quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the expected quantity.
     *
     * @param quantity the expected quantity
     * @return this
     */
    public V quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Returns the quantity.
     *
     * @return the quantity. May be {@code null}
     */
    public BigDecimal getQuantity() {
        return (BigDecimal) quantity.getValue();
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public V fixedCost(int fixedCost) {
        return fixedCost(BigDecimal.valueOf(fixedCost));
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public V fixedCost(String fixedCost) {
        return fixedCost(new BigDecimal(fixedCost));
    }

    /**
     * Sets the expected fixed cost.
     *
     * @param fixedCost the expected fixed cost
     * @return this
     */
    public V fixedCost(BigDecimal fixedCost) {
        this.fixedCost = ValueStrategy.value(fixedCost);
        return getThis();
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public V fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public V fixedPrice(String fixedPrice) {
        return fixedPrice(new BigDecimal(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public V fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = ValueStrategy.value(fixedPrice);
        return getThis();
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     * @return this
     */
    public V unitCost(int unitCost) {
        return unitCost(BigDecimal.valueOf(unitCost));
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     */
    public V unitCost(String unitCost) {
        return unitCost(new BigDecimal(unitCost));
    }

    /**
     * Sets the expected unit cost.
     *
     * @param unitCost the expected unit cost
     * @return this
     */
    public V unitCost(BigDecimal unitCost) {
        this.unitCost = ValueStrategy.value(unitCost);
        return getThis();
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public V unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public V unitPrice(String unitPrice) {
        return unitPrice(new BigDecimal(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public V unitPrice(BigDecimal unitPrice) {
        this.unitPrice = ValueStrategy.value(unitPrice);
        return getThis();
    }

    /**
     * Sets the expected service ratio.
     *
     * @param serviceRatio the expected service ratio
     * @return this
     */
    public V serviceRatio(int serviceRatio) {
        return serviceRatio(BigDecimal.valueOf(serviceRatio));
    }

    /**
     * Sets the expected service ratio.
     *
     * @param serviceRatio the expected service ratio
     * @return this
     */
    public V serviceRatio(BigDecimal serviceRatio) {
        this.serviceRatio = ValueStrategy.value(serviceRatio);
        return getThis();
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public V discount(int discount) {
        return discount(BigDecimal.valueOf(discount));
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public V discount(String discount) {
        return discount(new BigDecimal(discount));
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public V discount(BigDecimal discount) {
        this.discount = ValueStrategy.value(discount);
        return getThis();
    }

    /**
     * Sets the expected tax amount.
     *
     * @param tax the expected total
     * @return this
     */
    public V tax(int tax) {
        return tax(BigDecimal.valueOf(tax));
    }

    /**
     * Sets the expected tax amount.
     *
     * @param tax the expected total
     * @return this
     */
    public V tax(String tax) {
        return tax(new BigDecimal(tax));
    }

    /**
     * Sets the expected tax amount.
     *
     * @param tax the expected total
     * @return this
     */
    public V tax(BigDecimal tax) {
        this.tax = ValueStrategy.value(tax);
        return getThis();
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public V total(int total) {
        return total(BigDecimal.valueOf(total));
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public V total(String total) {
        return total(new BigDecimal(total));
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public V total(BigDecimal total) {
        this.total = ValueStrategy.value(total);
        return getThis();
    }

    /**
     * Sets the expected product template.
     *
     * @param template the template. May be {@code null}
     * @return this
     */
    public V template(Product template) {
        return template(getReference(template));
    }

    /**
     * Sets the expected product template.
     *
     * @param template the template. May be {@code null}
     * @return this
     */
    public V template(Reference template) {
        this.template = ValueStrategy.value(template);
        if (template == null) {
            group = ValueStrategy.value(null);
        }
        return getThis();
    }

    /**
     * Sets the expected template expansion group.
     *
     * @param group the expected template expansion group
     * @return this
     */
    public V group(Integer group) {
        this.group = ValueStrategy.value(group);
        return getThis();
    }

    /**
     * Sets the expected print flag.
     *
     * @param print the expected print flag
     * @return this
     */
    public V print(boolean print) {
        this.print = ValueStrategy.value(print);
        return getThis();
    }

    /**
     * Add this verifier to the parent verifier.
     *
     * @return the parent verifier
     */
    public P add() {
        return parent.add(getThis());
    }

    /**
     * Verifies there is an invoice item that matches the criteria.
     *
     * @param items invoice items
     * @return the matching item
     */
    public FinancialAct verify(List<FinancialAct> items) {
        Reference patientRef = (Reference) patient.getValue();
        Reference productRef = (Reference) product.getValue();
        FinancialAct item = FinancialTestHelper.find(items, patientRef, productRef, null);
        if (item == null) {
            fail("Failed to find invoice item for patient=" + patient + ", product=" + product);
        }
        verify(item);
        return item;
    }

    /**
     * Verifies there is an invoice item that matches the criteria.
     *
     * @param itemRelationships invoice item relationships
     * @param sequence          the expected relationship sequence
     * @return the matching item
     */
    public FinancialAct verify(RelatedIMObjects<FinancialAct, ActRelationship> itemRelationships, int sequence) {
        List<FinancialAct> items = IterableUtils.toList(itemRelationships.getObjects());
        FinancialAct act = verify(items);
        ActRelationship relationship = itemRelationships.getRelationships().stream()
                .filter(r -> r.getTarget().equals(act.getObjectReference()))
                .findFirst()
                .orElse(null);
        assertNotNull(relationship);
        assertEquals(sequence, relationship.getSequence());
        return act;
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(FinancialAct object, IMObjectBean bean) {
        super.verify(object, bean);
        checkEquals(patient, bean.getTargetRef("patient"));
        checkEquals(product, bean.getTargetRef("product"));
        checkEquals(template, bean.getTargetRef("template"));
        checkEquals(department, bean.getTargetRef("department"));
        checkEquals(quantity, bean.getBigDecimal("quantity"));
        checkEquals(fixedCost, bean.getBigDecimal("fixedCost"));
        checkEquals(fixedPrice, bean.getBigDecimal("fixedPrice"));
        checkEquals(unitCost, bean.getBigDecimal("unitCost"));
        checkEquals(unitPrice, bean.getBigDecimal("unitPrice"));
        checkEquals(serviceRatio, bean.getBigDecimal("serviceRatio"));
        checkEquals(discount, bean.getBigDecimal("discount"));
        checkEquals(tax, bean.getBigDecimal("tax"));
        checkEquals(total, bean.getBigDecimal("total"));
        Participation participation = bean.getObject("template", Participation.class);
        if (participation == null) {
            assertNull(group.getValue());
        } else {
            IMObjectBean participationBean = getBean(participation);
            checkEquals(group, participationBean.getValue("group"));
        }
        checkEquals(clinician, bean.getTargetRef("clinician"));
        checkEquals(print, bean.getBoolean("print"));
    }
}