/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.patientMedication</em> acts for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientMedicationBuilder extends AbstractTestPatientActBuilder<Act, TestPatientMedicationBuilder> {

    /**
     * The product.
     */
    private Product product;

    /**
     * The notes.
     */
    private ValueStrategy notes = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPatientMedicationBuilder}.
     *
     * @param service the archetype service
     */
    public TestPatientMedicationBuilder(ArchetypeService service) {
        super(PatientArchetypes.PATIENT_MEDICATION, Act.class, service);
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestPatientMedicationBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public TestPatientMedicationBuilder notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        if (product != null) {
            bean.setTarget("product", product);
        }
        notes.setValue(bean, "notes");
        super.build(object, bean, toSave, toRemove);
    }
}