/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.act;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Builds a text node on an {@link Act}, where if the text is too long, it is stored in a related {@link DocumentAct}.
 *
 * @author Tim Anderson
 */
public class LongTextBuilder {

    /**
     * The node.
     */
    private final String node;

    /**
     * The long text node.
     */
    private final String longNode;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link LongTextBuilder}.
     *
     * @param node      the node
     * @param longNode  the long text node
     * @param service   the archetype service
     */
    public LongTextBuilder(String node, String longNode, ArchetypeService service) {
        this.node = node;
        this.longNode = longNode;
        this.service = service;
    }

    /**
     * Builds the node.
     *
     * @param value    the value
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    public void build(ValueStrategy value, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        if (value.isSet()) {
            String text = value.toString();
            DocumentAct related = bean.getTarget(longNode, DocumentAct.class);
            Document content = (related != null && related.getDocument() != null) ?
                               bean.getObject(related.getDocument(), Document.class) : null;
            if (text == null || text.length() <= bean.getMaxLength(node)) {
                bean.setValue(node, text);
                // schedule the existing document for removal, if any
                if (related != null) {
                    toRemove.add(related.getObjectReference());
                }
                if (content != null) {
                    toRemove.add(content.getObjectReference());
                }
            } else {
                bean.setValue(node, null);
                TextDocumentHandler handler = new TextDocumentHandler(service);
                if (related == null) {
                    String[] relationshipArchetypes = bean.getArchetypeRange(longNode);
                    assertEquals(1, relationshipArchetypes.length);
                    String[] targets = DescriptorHelper.getNodeShortNames(relationshipArchetypes, "target", service);
                    assertEquals(1, targets.length);
                    related = service.create(targets[0], DocumentAct.class);
                    ActRelationship relationship = (ActRelationship) bean.addTarget(longNode, related);
                    related.addActRelationship(relationship);
                }
                if (content == null) {
                    content = handler.create("document", text);
                    toSave.add(content);
                    related.setDocument(content.getObjectReference());
                } else {
                    handler.update(content, text);
                }
                toSave.add(related);
            }
        }
    }
}
