/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.reminderConfigurationType</em> for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderConfigurationBuilder
        extends AbstractTestEntityBuilder<Entity, TestReminderConfigurationBuilder> {

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Determines if reminders should be emailed as attachments.
     */
    private ValueStrategy emailAttachments = ValueStrategy.unset();

    /**
     * The customer grouped reminder template.
     */
    private Entity customerTemplate;

    /**
     * The patient grouped reminder template.
     */
    private Entity patientTemplate;

    /**
     * Constructs a {@link TestReminderConfigurationBuilder}.
     *
     * @param service the archetype service
     */
    public TestReminderConfigurationBuilder(ArchetypeService service) {
        super(ReminderArchetypes.CONFIGURATION, Entity.class, service);
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public TestReminderConfigurationBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Determines if reminders should be emailed as attachments.
     *
     * @param emailAttachments if {@code true}, reminders should be emailed as attachments, else they should be
     *                         displayed within the body of the email
     * @return this
     */
    public TestReminderConfigurationBuilder emailAttachments(boolean emailAttachments) {
        this.emailAttachments = ValueStrategy.value(emailAttachments);
        return this;
    }

    /**
     * Sets the customer grouped reminder template.
     *
     * @param customerTemplate the customer template
     * @return this
     */
    public TestReminderConfigurationBuilder customerTemplate(Entity customerTemplate) {
        this.customerTemplate = customerTemplate;
        return this;
    }

    /**
     * Sets the patient template.
     *
     * @param patientTemplate the patient template
     * @return this
     */
    public TestReminderConfigurationBuilder patientTemplate(Entity patientTemplate) {
        this.patientTemplate = patientTemplate;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (location != null) {
            bean.setTarget("location", location);
        }
        emailAttachments.setValue(bean, "emailAttachments");
        if (customerTemplate != null) {
            bean.setTarget("customerTemplate", customerTemplate);
        }
        if (patientTemplate != null) {
            bean.setTarget("patientTemplate", patientTemplate);
        }
    }
}