/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;

/**
 * Factory for creating products.
 *
 * @author Tim Anderson
 */
public class TestProductFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestProductFactory}.
     *
     * @param service the service
     */
    public TestProductFactory(ArchetypeService service) {
        this.service = service;
    }


    /**
     * Returns a builder for a new product.
     *
     * @param archetype the product archetype
     * @return a new builder
     */
    public TestProductBuilder<?> newProduct(String archetype) {
        switch (archetype) {
            case ProductArchetypes.MEDICATION:
                return newMedication();
            case ProductArchetypes.MERCHANDISE:
                return newMerchandise();
            case ProductArchetypes.SERVICE:
                return newService();
            case ProductArchetypes.TEMPLATE:
                return newTemplate();
            default:
                throw new IllegalArgumentException("Invalid argument 'archetype'" + archetype);
        }
    }

    /**
     * Creates and saves a medication product.
     *
     * @return the medication product
     */
    public Product createMedication() {
        return newMedication().build();
    }

    /**
     * Creates and saves a medication product.
     *
     * @param productType the product type
     * @return the medication product
     */
    public Product createMedication(Entity productType) {
        return newMedication()
                .type(productType)
                .build();
    }

    /**
     * Returns a builder for a new medication.
     *
     * @return a medication builder
     */
    public TestMedicationProductBuilder newMedication() {
        return new TestMedicationProductBuilder(service);
    }

    /**
     * Returns a builder to update a medication product.
     *
     * @param product the product
     * @return a medication builder
     */
    public TestMedicationProductBuilder updateMedication(Product product) {
        return new TestMedicationProductBuilder(product, service);
    }

    /**
     * Creates and saves a merchandise product.
     *
     * @return the merchandise product
     */
    public Product createMerchandise() {
        return newMerchandise().build();
    }

    /**
     * Returns a builder for a new merchandise product.
     *
     * @return a merchandise builder
     */
    public TestMerchandiseProductBuilder newMerchandise() {
        return new TestMerchandiseProductBuilder(service);
    }

    /**
     * Returns a builder to update a merchandise product.
     *
     * @param product the product
     * @return a merchandise builder
     */
    public TestMerchandiseProductBuilder updateMerchandise(Product product) {
        return new TestMerchandiseProductBuilder(product, service);
    }

    /**
     * Creates and saves a service product.
     *
     * @return the service product
     */
    public Product createService() {
        return newService().build();
    }

    /**
     * Returns a builder for a new service.
     *
     * @return a service builder
     */
    public TestServiceProductBuilder newService() {
        return new TestServiceProductBuilder(service);
    }

    /**
     * Returns a builder to update a service product.
     *
     * @param product the product
     * @return a service builder
     */
    public TestServiceProductBuilder updateService(Product product) {
        return new TestServiceProductBuilder(product, service);
    }

    /**
     * Creates and saves a template.
     *
     * @return the template product
     */
    public Product createTemplate() {
        return newTemplate().build();
    }

    /**
     * Returns a builder for a new template.
     *
     * @return a new template builder
     */
    public TestTemplateProductBuilder newTemplate() {
        return new TestTemplateProductBuilder(service);
    }

    /**
     * Returns a builder to update a template product.
     *
     * @param product the product
     * @return a template builder
     */
    public TestTemplateProductBuilder updateTemplate(Product product) {
        return new TestTemplateProductBuilder(product, service);
    }

    /**
     * Returns a builder for a new price template.
     *
     * @return a new price template builder
     */
    public TestPriceTemplateProductBuilder newPriceTemplate() {
        return new TestPriceTemplateProductBuilder(service);
    }

    /**
     * Returns a builder to update a price template product.
     *
     * @param product the product
     * @return a price template builder
     */
    public TestPriceTemplateProductBuilder updatePriceTemplate(Product product) {
        return new TestPriceTemplateProductBuilder(product, service);
    }

    /**
     * Returns a builder to update a product.
     *
     * @param product the product to update
     * @return the builder
     */
    public TestProductBuilder<?> updateProduct(Product product) {
        if (product.isA(ProductArchetypes.MEDICATION)) {
            return updateMedication(product);
        } else if (product.isA(ProductArchetypes.MERCHANDISE)) {
            return updateMerchandise(product);
        } else if (product.isA(ProductArchetypes.SERVICE)) {
            return updateService(product);
        } else if (product.isA(ProductArchetypes.TEMPLATE)) {
            return updateTemplate(product);
        } else if (product.isA(ProductArchetypes.PRICE_TEMPLATE)) {
            return updatePriceTemplate(product);
        }
        throw new IllegalStateException("Unsupported product: " + product.getArchetype());
    }

    /**
     * Creates a new product type.
     *
     * @return a new product type
     */
    public Entity createProductType() {
        return newProductType().build();
    }

    /**
     * Creates a new product type.
     *
     * @param name the product type name
     * @return a new product type
     */
    public Entity createProductType(String name) {
        return newProductType().name(name).build();
    }

    /**
     * Returns a builder for a new product type.
     *
     * @return a new product type builder
     */
    public TestProductTypeBuilder newProductType() {
        return new TestProductTypeBuilder(service);
    }

    /**
     * Creates a new pricing group.
     *
     * @param codePrefix the pricing group code prefix
     * @return the pricing group
     */
    public Lookup createPricingGroup(String codePrefix) {
        return newPricingGroup()
                .uniqueCode(codePrefix)
                .build();
    }

    /**
     * Returns a builder for a new pricing group.
     *
     * @return a new pricing group builder
     */
    public TestLookupBuilder newPricingGroup() {
        return new TestLookupBuilder(ProductArchetypes.PRICING_GROUP, service);
    }

    /**
     * Creates and saves a new service ratio calendar.
     *
     * @return a new service ratio calendar
     */
    public Entity createServiceRatioCalendar() {
        return new TestServiceRatioCalendarBuilder(service).build();
    }

    /**
     * Returns a builder for a new fixed price.
     *
     * @return a new fixed price builder
     */
    public TestFixedPriceBuilder<?> newFixedPrice() {
        return new TestFixedPriceBuilder<>(service);
    }

    /**
     * Returns a builder to update a fixed price.
     *
     * @param price the fixed price to update
     * @return a new fixed price builder
     */
    public TestFixedPriceBuilder<?> updateFixedPrice(ProductPrice price) {
        return new TestFixedPriceBuilder<>(price, service);
    }

    /**
     * Returns a builder for a new unit price.
     *
     * @return a new fixed price builder
     */
    public TestUnitPriceBuilder<?> newUnitPrice() {
        return new TestUnitPriceBuilder<>(service);
    }

    /**
     * Creates a new product batch.
     *
     * @param product    the product
     * @param batchNo    the batch number
     * @param expiryDate the expiry date
     * @return a new product batch
     */
    public Entity createBatch(Product product, String batchNo, Date expiryDate) {
        return newBatch().product(product).batchNo(batchNo).expiryDate(expiryDate).build();
    }

    /**
     * Returns a builder for a new product batch.
     *
     * @return a new product batch builder
     */
    public TestBatchBuilder newBatch() {
        return new TestBatchBuilder(service);
    }

    /**
     * Returns a builder to update a product batch.
     *
     * @param batch the batch
     * @return a product batch builder
     */
    public TestBatchBuilder updateBatch(Entity batch) {
        return new TestBatchBuilder(batch, service);
    }

    /**
     * Returns a builder for a new discount.
     *
     * @return a new discount builder
     */
    public TestDiscountBuilder newDiscount() {
        return new TestDiscountBuilder(service);
    }

    /**
     * Creates a new demographic update.
     *
     * @param name the name
     * @param node the node. May be {@code null}
     * @param expression the expression
     * @return a demographic update lookup
     */
    public Lookup createDemographicUpdate(String name, String node, String expression) {
        return new TestDemographicUpdateBuilder(service)
                .uniqueCode()
                .name(name)
                .node(node)
                .expression(expression)
                .build();
    }
}

