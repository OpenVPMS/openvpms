/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * A builder of <em>act.customerAccount*</em> acts.
 *
 * @author Tim Anderson
 */
public class AbstractTestCustomerAccountActBuilder<B extends AbstractTestCustomerAccountActBuilder<B>>
        extends AbstractTestCustomerActBuilder<FinancialAct, B> {

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Constructs a {@link AbstractTestCustomerAccountActBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestCustomerAccountActBuilder(String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
    }

    /**
     * Constructs a {@link AbstractTestCustomerAccountActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestCustomerAccountActBuilder(FinancialAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public B location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location. May be {@code null}
     */
    public Party getLocation() {
        return location;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (location != null) {
            bean.setTarget("location", location);
        }
    }
}
