/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.order;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.supplier.TestSupplierActItemBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builds <em>act.supplierOrderItem</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestOrderItemBuilder extends TestSupplierActItemBuilder<TestOrderBuilder, TestOrderItemBuilder> {

    /**
     * The received quantity.
     */
    private ValueStrategy receivedQuantity = ValueStrategy.unset();

    /**
     * The cancelled quantity.
     */
    private ValueStrategy cancelledQuantity = ValueStrategy.unset();

    /**
     * Constructs a {@link TestOrderItemBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestOrderItemBuilder(TestOrderBuilder parent, ArchetypeService service) {
        super(SupplierArchetypes.ORDER_ITEM, parent, service);
    }

    /**
     * Constructs a\ {@link TestOrderItemBuilder}.
     *
     * @param object  the object to update
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestOrderItemBuilder(FinancialAct object, TestOrderBuilder parent, ArchetypeService service) {
        super(object, parent, service);
    }

    /**
     * Sets the received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestOrderItemBuilder receivedQuantity(int receivedQuantity) {
        return receivedQuantity(BigDecimal.valueOf(receivedQuantity));
    }

    /**
     * Sets the received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestOrderItemBuilder receivedQuantity(BigDecimal receivedQuantity) {
        this.receivedQuantity = ValueStrategy.value(receivedQuantity);
        return this;
    }

    /**
     * Sets the cancelled quantity.
     *
     * @param cancelledQuantity the cancelled quantity
     */
    public TestOrderItemBuilder cancelledQuantity(int cancelledQuantity) {
        return cancelledQuantity(BigDecimal.valueOf(cancelledQuantity));
    }

    /**
     * Sets the cancelled quantity.
     *
     * @param cancelledQuantity the cancelled quantity
     */
    public TestOrderItemBuilder cancelledQuantity(BigDecimal cancelledQuantity) {
        this.cancelledQuantity = ValueStrategy.value(cancelledQuantity);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        receivedQuantity.setValue(bean, "receivedQuantity");
        cancelledQuantity.setValue(bean, "cancelledQuantity");
        getService().deriveValues(object);
    }
}