/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Test insurance claim builder.
 *
 * @author Tim Anderson
 */
public class TestClaimBuilder extends AbstractTestIMObjectBuilder<FinancialAct, TestClaimBuilder> {

    public enum Status {
        PENDING,    // claim is pending. User can make changes
        POSTED,     // claim is finalised. No further changes may be made prior to submission.
        SUBMITTED,  // claim has been submitted to the insurer
        ACCEPTED,   // claim has been accepted, and is being processed
        SETTLED,    // claim has been settled by the insurer
        DECLINED,   // claim has been declined by the insurer
        CANCELLING, // claim is in the process of being cancelled
        CANCELLED   // claim has been cancelled
    }

    /**
     * The claim items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The built items
     */
    private final List<FinancialAct> builtItems = new ArrayList<>();

    /**
     * The attachments.
     */
    private final List<TestClaimAttachmentBuilder> attachments = new ArrayList<>();

    /**
     * The built attachments.
     */
    private final List<DocumentAct> builtAttachments = new ArrayList<>();

    /**
     * The policy.
     */
    private Act policy;

    /**
     * The claim id archetype.
     */
    private String insurerIdArchetype;

    /**
     * The claim id.
     */
    private ValueStrategy insurerId;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The claim handler.
     */
    private User claimHandler;

    /**
     * The claim status.
     */
    private ValueStrategy status = ValueStrategy.unset();

    /**
     * Determines if this is a gap claim.
     */
    private ValueStrategy gapClaim = ValueStrategy.unset();

    /**
     * The gap status.
     */
    private ValueStrategy gapStatus = ValueStrategy.unset();

    /**
     * The amount the customer has paid towards the claim.
     */
    private BigDecimal paid;

    /**
     * Indicates that the customer has paid the value of the claim.
     */
    private boolean fullyPaid;

    /**
     * The gap benefit amount.
     */
    private ValueStrategy benefitAmount = ValueStrategy.unset();

    /**
     * Constructs a {@link TestClaimBuilder}.
     *
     * @param service the archetype service
     */
    public TestClaimBuilder(ArchetypeService service) {
        super(InsuranceArchetypes.CLAIM, FinancialAct.class, service);
    }

    /**
     * Constructs a {@link TestClaimBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestClaimBuilder(FinancialAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the policy.
     *
     * @param policy the policy
     * @return this
     */
    public TestClaimBuilder policy(Act policy) {
        this.policy = policy;
        return this;
    }

    /**
     * Sets the insurer's id for the claim.
     *
     * @param archetype the claim identifier archetype
     * @param id        the claim identifier
     */
    public TestClaimBuilder claimId(String archetype, String id) {
        return claimId(archetype, ValueStrategy.value(id));
    }

    /**
     * Sets the insurer's id for the claim.
     *
     * @param archetype the claim identifier archetype
     * @param id        the claim identifier
     */
    public TestClaimBuilder claimId(String archetype, ValueStrategy id) {
        this.insurerIdArchetype = archetype;
        this.insurerId = id;
        return this;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public TestClaimBuilder clinician(User clinician) {
        this.clinician = clinician;
        return this;
    }

    /**
     * Sets the location.
     *
     * @param location the location
     * @return this
     */
    public TestClaimBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the claim handler.
     *
     * @param claimHandler the claim handler
     * @return this
     */
    public TestClaimBuilder claimHandler(User claimHandler) {
        this.claimHandler = claimHandler;
        return this;
    }

    /**
     * Sets the claim status.
     *
     * @param status the claim status
     * @return this
     */
    public TestClaimBuilder status(String status) {
        this.status = ValueStrategy.value(status);
        return this;
    }

    /**
     * Determines if this is gap claim.
     *
     * @param gapClaim if {@code true}, it's a gap claim, else it's a standard claim
     * @return this
     */
    public TestClaimBuilder gapClaim(boolean gapClaim) {
        this.gapClaim = ValueStrategy.value(gapClaim);
        return this;
    }

    /**
     * Sets the gap status.
     *
     * @param gapStatus the gap status
     * @return this
     */
    public TestClaimBuilder gapStatus(String gapStatus) {
        this.gapStatus = ValueStrategy.value(gapStatus);
        return this;
    }

    /**
     * Sets the amount the customer has paid towards the claim.
     *
     * @param paid the paid amount
     * @return this
     */
    public TestClaimBuilder paid(BigDecimal paid) {
        this.paid = paid;
        return this;
    }

    /**
     * Indicates that the customer has fully paid the claim.
     * <p/>
     * This sets the paid node to the total value being claimed.
     *
     * @return this
     */
    public TestClaimBuilder fullyPaid() {
        this.fullyPaid = true;
        return this;
    }

    /**
     * Sets the gap benefit amount.
     *
     * @param benefitAmount the benefit amount
     * @return this
     */
    public TestClaimBuilder benefitAmount(BigDecimal benefitAmount) {
        this.benefitAmount = ValueStrategy.value(benefitAmount);
        return this;
    }

    /**
     * Adds a claim item.
     *
     * @param item the item to add
     * @return this
     */
    public TestClaimBuilder addItem(FinancialAct item) {
        items.add(item);
        return this;
    }

    /**
     * Returns a builder to add a claim item.
     *
     * @return a charge item builder
     */
    public TestClaimItemBuilder item() {
        return new TestClaimItemBuilder(this, getService());
    }

    /**
     * Adds a claim item.
     *
     * @param invoiceItems the invoice items
     * @return a charge item builder
     */
    public TestClaimBuilder item(FinancialAct... invoiceItems) {
        return item(new Date(), new Date(), invoiceItems);
    }

    /**
     * Adds a claim item.
     *
     * @param treatmentStart the treatment start time
     * @param treatmentEnd   the treatment end time
     * @param invoiceItems   the invoice items
     * @return a charge item builder
     */
    public TestClaimBuilder item(Date treatmentStart, Date treatmentEnd, FinancialAct... invoiceItems) {
        return item().treatmentDates(treatmentStart, treatmentEnd)
                .invoiceItems(invoiceItems)
                .add();
    }

    /**
     * Returns the built items.
     *
     * @return the items
     */
    public List<FinancialAct> getItems() {
        return builtItems;
    }

    /**
     * Adds an attachment.
     *
     * @param act the attachment act. An invoice, customer or patient document or investigation
     * @return this
     */
    public TestClaimBuilder attachment(Act act) {
        return attachment().original(act).add();
    }

    /**
     * Adds attachments.
     *
     * @param acts the attachment acts. These may be invoices, customer or patient documents or investigations
     * @return this
     */
    public TestClaimBuilder attachments(Act... acts) {
        for (Act act : acts) {
            attachment(act);
        }
        return this;
    }

    /**
     * Returns a builder to add an attachment.,
     *
     * @return an attachment builder
     */
    public TestClaimAttachmentBuilder attachment() {
        return new TestClaimAttachmentBuilder(this, getService());
    }

    /**
     * Returns the built attachments.
     *
     * @return the attachments
     */
    public List<DocumentAct> getAttachments() {
        return builtAttachments;
    }

    /**
     * Adds an attachment.
     *
     * @param attachment  the attachment to add
     */
    protected void addAttachment(TestClaimAttachmentBuilder attachment) {
        this.attachments.add(attachment);
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (insurerIdArchetype != null && insurerId != null) {
            object.addIdentity(createActIdentity(insurerIdArchetype, insurerId));
        }

        IMObjectBean policyBean = getBean(policy);
        if (policy != null) {
            bean.addTarget("policy", policy, "claims");
            bean.setTarget("patient", policyBean.getTargetRef("patient"));
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (claimHandler != null) {
            bean.setTarget("user", claimHandler);
        }

        status.setValue(bean, "status");
        gapClaim.setValue(bean, "gapClaim");
        gapStatus.setValue(bean, "status2");
        if (!items.isEmpty()) {
            BigDecimal total = BigDecimal.ZERO;
            BigDecimal tax = BigDecimal.ZERO;
            builtItems.clear();
            for (FinancialAct item : items) {
                bean.addTarget("items", item, "claim");
                total = total.add(item.getTotal());
                tax = tax.add(item.getTaxAmount());
                toSave.add(item);
                builtItems.add(item);
            }
            bean.setValue("amount", total);
            bean.setValue("tax", tax);
        }
        if (paid != null) {
            bean.setValue("paid", paid);
        }
        if (fullyPaid) {
            bean.setValue("paid", bean.getBigDecimal("amount"));
        }

        benefitAmount.setValue(bean, "benefitAmount");
        builtAttachments.clear();
        for (TestClaimAttachmentBuilder builder : attachments) {
            DocumentAct attachment = builder.build(toSave, toRemove);
            if (attachment.isNew()) {
                bean.addTarget("attachments", attachment, "claim");
            }
            toSave.add(attachment);
            builtAttachments.add(attachment);
        }
        items.clear();        // can't reuse
        attachments.clear();
    }
}
