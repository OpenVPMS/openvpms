/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.entity.TestEntityIdentityBuilder;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Factory for creating patients and patient records.
 *
 * @author Tim Anderson
 */
public class TestPatientFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules laboratoryRules;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * Constructs a {@link TestPatientFactory}.
     *
     * @param service         the archetype service
     * @param laboratoryRules the laboratory rules
     * @param documentFactory the document factory
     */
    public TestPatientFactory(ArchetypeService service, LaboratoryRules laboratoryRules,
                              TestDocumentFactory documentFactory) {
        this.service = service;
        this.laboratoryRules = laboratoryRules;
        this.documentFactory = documentFactory;
    }

    /**
     * Creates a new patient alert.
     *
     * @return a patient alert
     */
    public Act createAlert(Party patient, Entity alertType) {
        return newAlert().patient(patient).alertType(alertType).build();
    }

    /**
     * Returns a builder for a new patient alert.
     *
     * @return an alert builder
     */
    public TestPatientAlertBuilder newAlert() {
        return new TestPatientAlertBuilder(service);
    }

    /**
     * Creates a patient alert type.
     *
     * @return a new patient alert type
     */
    public Entity createAlertType() {
        return newAlertType().build();
    }

    /**
     * Returns a builder for a new patient alert type.
     *
     * @return an alert builder
     */
    public TestPatientAlertTypeBuilder newAlertType() {
        return new TestPatientAlertTypeBuilder(service);
    }

    /**
     * Creates and saves a new patient.
     *
     * @return the patient
     */
    public Party createPatient() {
        return newPatient().build();
    }

    /**
     * Creates and saves a new patient linked to a customer.
     *
     * @param owner the customer
     * @return the patient
     */
    public Party createPatient(Party owner) {
        return newPatient().owner(owner).build();
    }

    /**
     * Creates and saves a new patient linked to a customer.
     *
     * @param name  the patient name
     * @param owner the customer
     * @return the patient
     */
    public Party createPatient(String name, Party owner) {
        return newPatient().name(name).owner(owner).build();
    }

    /**
     * Returns a builder for a new patient.
     *
     * @return a patient builder
     */
    public TestPatientBuilder newPatient() {
        return new TestPatientBuilder(service);
    }

    /**
     * Returns a builder to update a patient.
     *
     * @return a patient builder
     */
    public TestPatientBuilder updatePatient(Party patient) {
        return new TestPatientBuilder(patient, service);
    }

    /**
     * Creates a new microchip identity.
     *
     * @param microchip the microchip number
     * @return a new microchip identity
     */
    public EntityIdentity createMicrochip(String microchip) {
        return new TestEntityIdentityBuilder(PatientArchetypes.MICROCHIP, service).identity(microchip).build();
    }

    /**
     * Creates a new pet tag identity.
     *
     * @param tag the tag number
     * @return a new pet tag identity
     */
    public EntityIdentity createPetTag(String tag) {
        return new TestEntityIdentityBuilder(PatientArchetypes.PET_TAG, service).identity(tag).build();
    }

    /**
     * Creates a new rabies tag identity.
     *
     * @param tag the tag number
     * @return a new rabies tag identity
     */
    public EntityIdentity createRabiesTag(String tag) {
        return new TestEntityIdentityBuilder(PatientArchetypes.RABIES_TAG, service).identity(tag).build();
    }

    /**
     * Creates and saves an addendum.
     *
     * @param patient the patient
     * @param note    the note
     * @return the addendum act
     */
    public DocumentAct createAddendum(Party patient, String note) {
        return newAddendum().patient(patient).note(note).build();
    }

    /**
     * Returns a builder for a new note.
     *
     * @return a note builder
     */
    public TestAddendumBuilder newAddendum() {
        return new TestAddendumBuilder(service);
    }

    /**
     * Returns a builder for a new attachment.
     *
     * @return an attachment builder
     */
    public TestAttachmentBuilder newAttachment() {
        return new TestAttachmentBuilder(service);
    }

    /**
     * Returns a builder to update an attachment.
     *
     * @param attachment the attachment to update
     * @return an attachment builder
     */
    public TestAttachmentBuilder updateAttachment(DocumentAct attachment) {
        return new TestAttachmentBuilder(attachment, service);
    }

    /**
     * Creates and saves a customer note.
     *
     * @param patient the patient
     * @param note    the note
     * @return the customer note act
     */
    public Act createCustomerNote(Party patient, String note) {
        return newCustomerNote().patient(patient).note(note).build();
    }

    /**
     * Returns a builder for a customer note.
     *
     * @return a customer note builder
     */
    public TestCustomerNoteBuilder newCustomerNote() {
        return new TestCustomerNoteBuilder(service);
    }

    /**
     * Returns a builder for a new image.
     *
     * @return an image builder
     */
    public TestImageBuilder newImage() {
        return new TestImageBuilder(service);
    }

    /**
     * Returns a builder to update an image.
     *
     * @param image the image to update
     * @return an image builder
     */
    public TestImageBuilder updateImage(DocumentAct image) {
        return new TestImageBuilder(image, service);
    }

    /**
     * Returns a builder for a new investigation.
     *
     * @return an investigation builder
     */
    public TestInvestigationBuilder newInvestigation() {
        return new TestInvestigationBuilder(service, laboratoryRules);
    }

    /**
     * Returns a builder to update an investigation.
     *
     * @param investigation the investigation to update
     * @return an investigation builder
     */
    public TestInvestigationBuilder updateInvestigation(DocumentAct investigation) {
        return new TestInvestigationBuilder(investigation, service, laboratoryRules);
    }

    /**
     * Returns a letter builder.
     *
     * @return a letter builder
     */
    public TestLetterBuilder newLetter() {
        return new TestLetterBuilder(service);
    }

    /**
     * Returns a builder to update a letter.
     *
     * @param letter the letter to update
     * @return a letter builder
     */
    public TestLetterBuilder updateLetter(DocumentAct letter) {
        return new TestLetterBuilder(letter, service);
    }

    /**
     * Creates and saves a link.
     *
     * @param patient the patient
     * @param url     the url
     * @return the link act
     */
    public Act createLink(Party patient, String url) {
        return newLink().patient(patient).url(url).build();
    }

    /**
     * Returns a builder to create a link.
     *
     * @return a link builder
     */
    public TestLinkBuilder newLink() {
        return new TestLinkBuilder(service);
    }

    /**
     * Creates and saves a new visit.
     *
     * @param patient the patient
     * @return the visit act
     */
    public Act createVisit(Party patient) {
        return new TestVisitBuilder(service).patient(patient).build();
    }

    /**
     * Returns a builder for a new visit.
     *
     * @return a visit builder
     */
    public TestVisitBuilder newVisit() {
        return new TestVisitBuilder(service);
    }

    /**
     * Returns a builder to update a visit.
     *
     * @param visit the visit
     */
    public TestVisitBuilder updateVisit(Act visit) {
        return new TestVisitBuilder(visit, service);
    }

    /**
     * Creates and saves a patient form with a blank template.
     *
     * @param patient the patient
     */
    public DocumentAct createForm(Party patient) {
        return newForm().patient(patient).template().build();
    }

    /**
     * Creates and saves a patient form.
     *
     * @param patient  the patient
     * @param template the form template
     */
    public DocumentAct createForm(Party patient, Entity template) {
        return newForm().patient(patient).template(template).build();
    }

    /**
     * Returns a builder for a new patient form.
     *
     * @return a form builder
     */
    public TestPatientFormBuilder newForm() {
        return new TestPatientFormBuilder(service, documentFactory);
    }

    /**
     * Creates and saves a new patient weight.
     *
     * @param patient the patient
     * @param weight  the weight, in kilograms
     * @return the weight act
     */
    public Act createWeight(Party patient, int weight) {
        return createWeight(patient, BigDecimal.valueOf(weight), WeightUnits.KILOGRAMS);
    }

    /**
     * Creates and saves a new patient weight.
     *
     * @param patient the patient
     * @param weight  the weight, in kilograms
     * @return the weight act
     */
    public Act createWeight(Party patient, BigDecimal weight) {
        return createWeight(patient, weight, WeightUnits.KILOGRAMS);
    }

    /**
     * Creates and saves a new patient weight.
     *
     * @param patient the patient
     * @param weight  the weight
     * @param units   the weight units
     * @return the weight act
     */
    public Act createWeight(Party patient, BigDecimal weight, WeightUnits units) {
        return new TestWeightBuilder(service).patient(patient).weight(weight, units).build();
    }

    /**
     * Returns a builder for a new weight.
     *
     * @return a weight builder
     */
    public TestWeightBuilder newWeight() {
        return new TestWeightBuilder(service);
    }

    /**
     * Creates and saves a patient medication.
     *
     * @param patient the patient
     * @param product the product
     * @return the medication act
     */
    public Act createMedication(Party patient, Product product) {
        return newMedication().patient(patient).product(product).build();
    }

    /**
     * Returns a builder for a new patient medication record.
     *
     * @return a medication builder
     */
    public TestPatientMedicationBuilder newMedication() {
        return new TestPatientMedicationBuilder(service);
    }

    /**
     * Creates and saves a patient note.
     *
     * @param patient the patient
     * @param note    the note
     * @return the note act
     */
    public DocumentAct createNote(Party patient, String note) {
        return newNote().patient(patient).note(note).build();
    }

    /**
     * Returns a builder for a new note.
     *
     * @return a note builder
     */
    public TestNoteBuilder newNote() {
        return new TestNoteBuilder(service);
    }

    /**
     * Returns a builder to update a note.
     *
     * @param note the note to update
     * @return a note builder
     */
    public TestNoteBuilder updateNote(DocumentAct note) {
        return new TestNoteBuilder(note, service);
    }

    /**
     * Returns a builder for a new prescription.
     *
     * @return a prescription builder
     */
    public TestPrescriptionBuilder newPrescription() {
        return new TestPrescriptionBuilder(service);
    }

    /**
     * Returns a builder for a new problem.
     *
     * @return a problem builder
     */
    public TestProblemBuilder newProblem() {
        return new TestProblemBuilder(service);
    }
}
