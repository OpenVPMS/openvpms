/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Builder for <em>party.organisationLocation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLocationBuilder extends AbstractTestOrganisationBuilder<TestLocationBuilder> {

    /**
     * The service ratios.
     */
    private final List<EntityLink> serviceRatios = new ArrayList<>();

    /**
     * The work list views.
     */
    private final Map<Entity, Boolean> workListViews = new HashMap<>();

    /**
     * Determines if stock control is enabled.
     */
    private ValueStrategy stockControl = ValueStrategy.unset();

    /**
     * Determines if online booking is enabled.
     */
    private ValueStrategy onlineBooking = ValueStrategy.unset();

    /**
     * Determines if discounts should be disabled.
     */
    private ValueStrategy disableDiscounts = ValueStrategy.unset();

    /**
     * The tills.
     */
    private Entity[] tills;

    /**
     * The gap benefit till.
     */
    private Entity gapBenefitTill;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The mail server.
     */
    private Entity mailServer;

    /**
     * The schedule views.
     */
    private Entity[] scheduleViews;

    /**
     * The over-the-counter party.
     */
    private Party otc;

    /**
     * The letterhead.
     */
    private Entity letterhead;

    /**
     * The default printer.
     */
    private PrinterReference defaultPrinter;

    /**
     * The printers.
     */
    private PrinterReference[] printers;

    /**
     * Constructs a {@link TestLocationBuilder}.
     *
     * @param service the archetype service
     */
    public TestLocationBuilder(ArchetypeService service) {
        super(PracticeArchetypes.LOCATION, service);
        name(ValueStrategy.random("zlocation"));
    }

    /**
     * Constructs a {@link TestLocationBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestLocationBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Enable stock control.
     *
     * @return this
     */
    public TestLocationBuilder stockControl() {
        return stockControl(true);
    }

    /**
     * Determines if stock control is enabled.
     *
     * @param stockControl if {@code true}, stock control is enabled
     * @return this
     */
    public TestLocationBuilder stockControl(boolean stockControl) {
        this.stockControl = ValueStrategy.value(stockControl);
        return this;
    }

    /**
     * Make the location available for online booking.
     *
     * @return this
     */
    public TestLocationBuilder onlineBooking() {
        return onlineBooking(true);
    }

    /**
     * Determines if the location is available for online booking.
     *
     * @param onlineBooking if {@code true}, the location is available for online booking
     * @return this
     */
    public TestLocationBuilder onlineBooking(boolean onlineBooking) {
        this.onlineBooking = ValueStrategy.value(onlineBooking);
        return this;
    }

    /**
     * Determines if discounts should be disabled at this practice location.
     *
     * @param disableDiscounts if {@code true}, discounts should be disabled
     * @return this
     */
    public TestLocationBuilder disableDiscounts(boolean disableDiscounts) {
        this.disableDiscounts = ValueStrategy.value(disableDiscounts);
        return this;
    }

    /**
     * Sets the tills.
     *
     * @param tills the tills
     * @return this
     */
    public TestLocationBuilder tills(Entity... tills) {
        this.tills = tills;
        return this;
    }

    /**
     * Sets the gap benefit till.
     *
     * @param gapBenefitTill the gap benefit till
     * @return this
     */
    public TestLocationBuilder gapBenefitTill(Entity gapBenefitTill) {
        this.gapBenefitTill = gapBenefitTill;
        return this;
    }

    /**
     * Sets the stock location for the location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public TestLocationBuilder stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return this;
    }

    /**
     * Sets the mail server.
     *
     * @param mailServer the mail server
     * @return this
     */
    public TestLocationBuilder setMailServer(Entity mailServer) {
        this.mailServer = mailServer;
        return this;
    }

    /**
     * Sets the Over-the-Counter party for the location.
     *
     * @param otc the OTC party
     * @return this
     */
    public TestLocationBuilder otc(Party otc) {
        this.otc = otc;
        return this;
    }

    /**
     * Sets the schedule views.
     *
     * @param scheduleViews the schedule views
     * @return this
     */
    public TestLocationBuilder scheduleViews(Entity... scheduleViews) {
        this.scheduleViews = scheduleViews;
        return this;
    }

    /**
     * Sets the work list views.
     *
     * @param workListViews the work list views
     * @return this
     */
    public TestLocationBuilder workListViews(Entity... workListViews) {
        for (Entity workListView : workListViews) {
            addWorkListView(workListView, false);
        }
        return this;
    }

    /**
     * Adds a work list view.
     *
     * @param workListView the view
     * @param isDefault    if {@code true}, indicates this is the default task type
     */
    public TestLocationBuilder addWorkListView(Entity workListView, boolean isDefault) {
        workListViews.put(workListView, isDefault);
        return this;
    }

    /**
     * Sets the letterhead.
     *
     * @param letterhead the letterhead
     * @return this
     */
    public TestLocationBuilder letterhead(Entity letterhead) {
        this.letterhead = letterhead;
        return this;
    }

    /**
     * Sets the default printer.
     *
     * @param defaultPrinter the default printer
     * @return this
     */
    public TestLocationBuilder defaultPrinter(PrinterReference defaultPrinter) {
        this.defaultPrinter = defaultPrinter;
        return this;
    }

    /**
     * Sets the printers.
     *
     * @param printers the printers
     * @return this
     */
    public TestLocationBuilder printers(PrinterReference... printers) {
        this.printers = printers;
        return this;
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, int ratio) {
        return addServiceRatio(productType, BigDecimal.valueOf(ratio));
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, BigDecimal ratio) {
        return addServiceRatio(productType, ratio, null);
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @param calendar    the calendar when the ratio applies
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, int ratio, Entity calendar) {
        return addServiceRatio(productType, BigDecimal.valueOf(ratio), calendar);
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @param calendar    the calendar when the ratio applies
     * @return this
     */
    public TestLocationBuilder addServiceRatio(Entity productType, BigDecimal ratio, Entity calendar) {
        EntityLink link = create(getNodeArchetype("serviceRatios"), EntityLink.class);
        link.setTarget(productType.getObjectReference());
        IMObjectBean bean = getBean(link);
        bean.setValue("ratio", ratio);
        if (calendar != null) {
            bean.setValue("calendar", calendar.getObjectReference());
        }
        serviceRatios.add(link);
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        stockControl.setValue(bean, "stockControl");
        onlineBooking.setValue(bean, "onlineBooking");
        disableDiscounts.setValue(bean, "disableDiscounts");
        if (tills != null) {
            for (Entity till : tills) {
                bean.addTarget("tills", till, "locations");
                toSave.add(till);
            }
        }
        if (gapBenefitTill != null) {
            bean.setTarget("gapBenefitTill", gapBenefitTill);
        }
        if (stockLocation != null) {
            bean.addTarget("stockLocations", stockLocation, "locations");
            toSave.add(stockLocation);
        }
        if (mailServer != null) {
            bean.setTarget("mailServer", mailServer);
        }
        if (otc != null) {
            bean.addTarget("OTC", otc, "location");
            toSave.add(otc);
            otc = null; // can't reuse as only 1 relationship allowed
        }
        if (scheduleViews != null) {
            for (Entity scheduleView : scheduleViews) {
                bean.addTarget("scheduleViews", scheduleView);
            }
        }
        for (Map.Entry<Entity, Boolean> workListView : workListViews.entrySet()) {
            Relationship relationship = bean.addTarget("workListViews", workListView.getKey());
            IMObjectBean relBean = getBean(relationship);
            relBean.setValue("default", workListView.getValue());
        }
        if (letterhead != null) {
            bean.setTarget("letterhead", letterhead);
        }
        bean.setValue("defaultPrinter", defaultPrinter != null ? defaultPrinter.toString() : null);
        if (printers != null) {
            for (PrinterReference printer : printers) {
                Entity entity = create(PracticeArchetypes.PRINTER, Entity.class);
                IMObjectBean printerBean = getBean(entity);
                printerBean.setValue("name", printer.getName());
                printerBean.setValue("printer", printer.toString());
                bean.addTarget("printers", entity);
                toSave.add(entity);
            }
        }
        for (EntityLink serviceRatio : serviceRatios) {
            serviceRatio.setSource(object.getObjectReference());
            bean.addValue("serviceRatios", serviceRatio);
        }
        serviceRatios.clear(); // can't reuse
    }
}
