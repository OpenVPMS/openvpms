/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>party.organisationTill</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestTillBuilder extends AbstractTestEntityBuilder<Entity, TestTillBuilder> {

    /**
     * The printer.
     */
    private ValueStrategy printer = ValueStrategy.unset();

    /**
     * The drawer command.
     */
    private ValueStrategy drawerCommand = ValueStrategy.unset();

    /**
     * The EFTPOS terminals.
     */
    private Entity[] terminals;

    /**
     * Constructs a {@link TestTillBuilder}.
     *
     * @param service the archetype service
     */
    public TestTillBuilder(ArchetypeService service) {
        super(TillArchetypes.TILL, Entity.class, service);
        name(ValueStrategy.random("ztill"));
    }

    /**
     * Constructs a {@link TestTillBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestTillBuilder(Entity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the printer, used for sending commands to open the till drawer.
     *
     * @param printer the printer
     * @return this
     */
    public TestTillBuilder printer(String printer) {
        this.printer = ValueStrategy.value(printer);
        return this;
    }

    /**
     * Sets the command used to open the till drawer.
     *
     * @param drawerCommand a comma separate list of values in the range 0..255
     * @return this
     */
    public TestTillBuilder drawerCommand(String drawerCommand) {
        this.drawerCommand = ValueStrategy.value(drawerCommand);
        return this;
    }

    /**
     * Sets the EFTPOS terminals.
     *
     * @param terminals the EFTPOS terminals
     * @return this
     */
    public TestTillBuilder terminals(Entity... terminals) {
        this.terminals = terminals;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        printer.setValue(bean, "printerName");
        drawerCommand.setValue(bean, "drawerCommand");
        if (terminals != null) {
            for (Entity terminal : terminals) {
                bean.addTarget("terminals", terminal);
            }
        }
    }
}
