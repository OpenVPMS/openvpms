/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.account.AccountType.FeeType;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <em>lookup.customerAccountType</em> lookups, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerAccountTypeBuilder extends AbstractTestLookupBuilder<TestCustomerAccountTypeBuilder> {

    /**
     * The payment terms.
     */
    private ValueStrategy paymentTerms = ValueStrategy.unset();

    /**
     * The payment terms units.
     */
    private ValueStrategy paymentTermsUnits = ValueStrategy.unset();

    /**
     * The account fee amount.
     */
    private ValueStrategy accountFeeAmount = ValueStrategy.unset();

    /**
     * The account fee type.
     */
    private ValueStrategy accountFeeType = ValueStrategy.unset();


    /**
     * The account fee days.
     */
    private ValueStrategy accountFeeDays = ValueStrategy.unset();

    /**
     * The account fee balance.
     */
    private ValueStrategy accountFeeBalance = ValueStrategy.unset();

    /**
     * The account fee message.
     */
    private ValueStrategy accountFeeMessage = ValueStrategy.unset();

    /**
     * Constructs a {@link TestCustomerAccountTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestCustomerAccountTypeBuilder(ArchetypeService service) {
        super(CustomerArchetypes.ACCOUNT_TYPE, service);
        code(ValueStrategy.random("ZACCOUNT_TYPE").toString());
    }

    /**
     * Sets the payment terms.
     *
     * @param terms the terms
     * @param units the terms units
     * @return this
     */
    public TestCustomerAccountTypeBuilder paymentTerms(int terms, DateUnits units) {
        this.paymentTerms = ValueStrategy.value(terms);
        this.paymentTermsUnits = ValueStrategy.value(units.toString());
        return this;
    }

    /**
     * Sets the account fee.
     *
     * @param amount  the account fee amount
     * @param feeType the account fee type
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFee(int amount, FeeType feeType) {
        return accountFee(BigDecimal.valueOf(amount), feeType);
    }

    /**
     * Sets the account fee.
     *
     * @param amount  the account fee amount
     * @param feeType the account fee type
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFee(BigDecimal amount, FeeType feeType) {
        this.accountFeeAmount = ValueStrategy.value(amount);
        this.accountFeeType = ValueStrategy.value(feeType.toString());
        return this;
    }

    /**
     * Sets the account fee days.
     *
     * @param accountFeeDays the account fee days
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFeeDays(int accountFeeDays) {
        this.accountFeeDays = ValueStrategy.value(accountFeeDays);
        return this;
    }

    /**
     * Sets the minimum balance when an account fee applies.
     *
     * @param accountFeeBalance the minimum balance
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFeeBalance(int accountFeeBalance) {
        return accountFeeBalance(BigDecimal.valueOf(accountFeeBalance));
    }

    /**
     * Sets the minimum balance when an account fee applies.
     *
     * @param accountFeeBalance the minimum balance
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFeeBalance(BigDecimal accountFeeBalance) {
        this.accountFeeBalance = ValueStrategy.value(accountFeeBalance);
        return this;
    }

    /**
     * Sets the account fee message.
     *
     * @param accountFeeMessage the account fee message
     * @return this
     */
    public TestCustomerAccountTypeBuilder accountFeeMessage(String accountFeeMessage) {
        this.accountFeeMessage = ValueStrategy.value(accountFeeMessage);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        paymentTerms.setValue(bean, "paymentTerms");
        paymentTermsUnits.setValue(bean, "paymentUom");
        accountFeeType.setValue(bean, "accountFee");
        accountFeeAmount.setValue(bean, "accountFeeAmount");
        accountFeeDays.setValue(bean, "accountFeeDays");
        accountFeeBalance.setValue(bean, "accountFeeBalance");
        accountFeeMessage.setValue(bean, "accountFeeMessage");
    }
}