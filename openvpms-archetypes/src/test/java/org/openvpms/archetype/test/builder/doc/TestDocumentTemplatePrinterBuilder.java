/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entityRelationship.documentTemplatePrinter</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentTemplatePrinterBuilder extends AbstractTestIMObjectBuilder<EntityRelationship,
        TestDocumentTemplatePrinterBuilder> {

    /**
     * The parent builder.
     */
    private final TestDocumentTemplateBuilder parent;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The printer name.
     */
    private PrinterReference printer;

    /**
     * The paper tray.
     */
    private String paperTray;

    /**
     * Determines if the template should be printed interactively.
     */
    private Boolean interactive;


    /**
     * Constructs a {@link TestDocumentTemplatePrinterBuilder}.
     *
     * @param service the archetype service
     */
    public TestDocumentTemplatePrinterBuilder(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link TestDocumentTemplatePrinterBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestDocumentTemplatePrinterBuilder(TestDocumentTemplateBuilder parent, ArchetypeService service) {
        super(DocumentArchetypes.DOCUMENT_TEMPLATE_PRINTER, EntityRelationship.class, service);
        this.parent = parent;
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public TestDocumentTemplatePrinterBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the printer.
     *
     * @param printer the printer name
     * @return this
     */
    public TestDocumentTemplatePrinterBuilder printer(String printer) {
        return printer(PrinterReference.fromString(printer));
    }

    /**
     * Sets the printer.
     *
     * @param printer the printer
     * @return this
     */
    public TestDocumentTemplatePrinterBuilder printer(PrinterReference printer) {
        this.printer = printer;
        return this;
    }

    /**
     * Sets the paper tray.
     *
     * @param paperTray the paper tray
     * @return this
     */
    public TestDocumentTemplatePrinterBuilder paperTray(String paperTray) {
        this.paperTray = paperTray;
        return this;
    }

    /**
     * Determines if the template should be printed interactively.
     *
     * @param interactive if {@code true} the template should be printed interactively else it should be printed in
     *                    the background
     * @return this
     */
    public TestDocumentTemplatePrinterBuilder interactive(boolean interactive) {
        this.interactive = interactive;
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityRelationship build() {
        return build(false);
    }

    /**
     * Adds the printer to the parent template builder.
     *
     * @return the parent template builder
     */
    public TestDocumentTemplateBuilder add() {
        EntityRelationship relationship = build();
        parent.addPrinter(relationship, location);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(EntityRelationship object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (location != null) {
            object.setTarget(location.getObjectReference());
        }
        if (printer != null) {
            bean.setValue("printer", printer.toString());
        }
        if (paperTray != null) {
            bean.setValue("paperTray", paperTray);
        }
        if (interactive != null) {
            bean.setValue("interactive", interactive);
        }
    }

}
