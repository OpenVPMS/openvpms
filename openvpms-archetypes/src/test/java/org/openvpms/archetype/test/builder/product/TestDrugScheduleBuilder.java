/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.lookup.AbstractTestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>lookup.drugSchedule</em> lookups, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDrugScheduleBuilder extends AbstractTestLookupBuilder<TestDrugScheduleBuilder> {

    /**
     * The restricted flag.
     */
    private ValueStrategy restricted = ValueStrategy.unset();

    /**
     * Constructs a {@link TestDrugScheduleBuilder}.
     *
     * @param service the archetype service
     */
    public TestDrugScheduleBuilder(ArchetypeService service) {
        super(ProductArchetypes.DRUG_SCHEDULE, service);
    }

    /**
     * Determines if the drug schedule is restricted or not.
     *
     * @param restricted if {@code true}, the drug schedule is restricted
     * @return this
     */
    public TestDrugScheduleBuilder restricted(boolean restricted) {
        this.restricted = ValueStrategy.value(restricted);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        object.setName(object.getCode());
        restricted.setValue(bean, "restricted");
    }
}