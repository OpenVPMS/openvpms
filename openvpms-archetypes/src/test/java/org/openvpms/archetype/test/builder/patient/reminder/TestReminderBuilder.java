/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.test.builder.patient.AbstractTestPatientActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.patientReminder</em> for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderBuilder extends AbstractTestPatientActBuilder<Act, TestReminderBuilder> {

    /**
     * The reminder rules.¶
     */
    private final ReminderRules rules;

    /**
     * The reminder items.
     */
    private final List<Act> items = new ArrayList<>();

    /**
     * The date to base due date calculations on.
     */
    private Date date;

    /**
     * The due date.
     */
    private Date dueDate;

    /**
     * The next due date.
     */
    private Date nextDueDate;

    /**
     * The reminder type.
     */
    private Entity reminderType;

    /**
     * The reminder count.
     */
    private int count;

    /**
     * The product.
     */
    private Product product;

    /**
     * The built reminder items.
     */
    private List<Act> builtItems = Collections.emptyList();

    /**
     * Constructs a {@link TestReminderBuilder}.
     *
     * @param reminderRules the reminder rules
     * @param service       the archetype service
     */
    public TestReminderBuilder(ReminderRules reminderRules, ArchetypeService service) {
        super(ReminderArchetypes.REMINDER, Act.class, service);
        this.rules = reminderRules;
    }

    /**
     * Constructs a {@link TestReminderBuilder}.
     *
     * @param object        the object to update
     * @param reminderRules the reminder rules
     * @param service       the archetype service
     */
    public TestReminderBuilder(Act object, ReminderRules reminderRules, ArchetypeService service) {
        super(object, service);
        this.rules = reminderRules;
        IMObjectBean bean = getBean(object);
        date(bean.getDate("initialTime"));
        nextDueDate(object.getActivityStartTime());
        endTime(object.getActivityEndTime());
        reminderType(bean.getTarget("reminderType", Entity.class));
    }

    /**
     * Sets the reminder date.
     * <p/>
     * This is the date from which the due date is calculated.
     * <p/>
     * If not set, defaults to the time when the reminder is built.
     *
     * @param date the date
     * @return this
     */
    public TestReminderBuilder date(String date) {
        return date(parseDate(date));
    }

    /**
     * Sets the reminder date.
     * <p/>
     * This is the date from which the due date is calculated.
     * <p/>
     * If not set, defaults to the time when the reminder is built.
     *
     * @param date the date
     * @return this
     */
    public TestReminderBuilder date(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Sets the reminder due date.
     * <p/>
     * If not set, it will be calculated using the {@link #date(Date)} and the {@link #reminderType(Entity)}.
     *
     * @param dueDate the due date
     * @return this
     */
    public TestReminderBuilder dueDate(String dueDate) {
        return dueDate(parseDate(dueDate));
    }

    /**
     * Sets the reminder due date.
     * <p/>
     * If not set, it will be calculated using the {@link #date(Date)} and the {@link #reminderType(Entity)}.
     *
     * @param dueDate the due date
     * @return this
     */
    public TestReminderBuilder dueDate(Date dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    /**
     * Sets the reminder next due date.
     * <p/>
     * If not set, it will be calculated using the {@link #dueDate(Date) due date},
     * the {@link #reminderType(Entity) reminder type}, and the {@link #count(int) count}.
     *
     * @param nextDueDate the next due date
     * @return this
     */
    public TestReminderBuilder nextDueDate(Date nextDueDate) {
        this.nextDueDate = nextDueDate;
        return this;
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType the reminder type
     * @return this
     */
    public TestReminderBuilder reminderType(Entity reminderType) {
        this.reminderType = reminderType;
        return this;
    }

    /**
     * Sets the reminder count.
     *
     * @param count the reminder count
     * @return this
     */
    public TestReminderBuilder count(int count) {
        this.count = count;
        return this;
    }

    /**
     * Adds reminder items.
     *
     * @param items the reminder items
     * @return this
     */
    public TestReminderBuilder addItems(Act... items) {
        this.items.addAll(Arrays.asList(items));
        return this;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestReminderBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Returns a builder to add a new email reminder item.
     *
     * @return a new builder
     */
    public TestReminderItemBuilder newEmailReminder() {
        return new TestReminderItemBuilder(this, ReminderArchetypes.EMAIL_REMINDER, getService());
    }

    /**
     * Returns a builder to add a new export reminder item.
     *
     * @return a new builder
     */
    public TestReminderItemBuilder newExportReminder() {
        return new TestReminderItemBuilder(this, ReminderArchetypes.EXPORT_REMINDER, getService());
    }

    /**
     * Returns a builder to add a new list reminder item.
     *
     * @return a new builder
     */
    public TestReminderItemBuilder newListReminder() {
        return new TestReminderItemBuilder(this, ReminderArchetypes.LIST_REMINDER, getService());
    }

    /**
     * Returns a builder to add a new print reminder item.
     *
     * @return a new builder
     */
    public TestReminderItemBuilder newPrintReminder() {
        return new TestReminderItemBuilder(this, ReminderArchetypes.PRINT_REMINDER, getService());
    }

    /**
     * Returns a builder to add a new SMS reminder item.
     *
     * @return a new builder
     */
    public TestReminderItemBuilder newSMSReminder() {
        return new TestReminderItemBuilder(this, ReminderArchetypes.SMS_REMINDER, getService());
    }

    /**
     * Returns the built reminder items.
     * <p/>
     * This is only applicable after the reminder has been built.
     *
     * @return the built reminder items
     */
    public List<Act> getItems() {
        return builtItems;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        Date initial = (date != null) ? date : new Date();
        Date due = (dueDate != null) ? dueDate : rules.calculateReminderDueDate(initial, reminderType);
        endTime(due);

        Date nextDue = (nextDueDate != null) ? nextDueDate : rules.getNextReminderDate(due, reminderType, count);
        if (nextDue == null) {
            nextDue = due;
        }
        startTime(nextDue);
        super.build(object, bean, toSave, toRemove);
        bean.setValue("initialTime", initial);
        bean.setTarget("reminderType", reminderType);
        if (product != null) {
            bean.setTarget("product", product);
        }
        for (Act item : items) {
            bean.addTarget("items", item, "reminder");
            toSave.add(item);
        }
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }
}