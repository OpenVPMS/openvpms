/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.entity;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Builder for {@link Entity} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestEntityBuilder<T extends Entity, B extends AbstractTestEntityBuilder<T, B>>
        extends AbstractTestIMObjectBuilder<T, B> {

    /**
     * Identities to add.
     */
    private final List<EntityIdentity> identities = new ArrayList<>();

    /**
     * Classifications to add.
     */
    private List<Lookup> classifications = new ArrayList<>();

    /**
     * Constructs an {@link AbstractTestEntityBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestEntityBuilder(String archetype, Class<T> type, ArchetypeService service) {
        super(archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestEntityBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestEntityBuilder(T object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds classifications.
     *
     * @param classifications the classifications to add
     * @return this
     */
    public B addClassifications(Lookup... classifications) {
        this.classifications.addAll(Arrays.asList(classifications));
        return getThis();
    }

    /**
     * Adds identities.
     *
     * @param identities the identities to add
     * @return this
     */
    public B addIdentities(EntityIdentity... identities) {
        this.identities.addAll(Arrays.asList(identities));
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (Lookup classification : classifications) {
            object.addClassification(classification);
        }
        for (EntityIdentity identity : identities) {
            object.addIdentity(identity);
        }
        identities.clear(); // can't reuse
    }

    /**
     * Ensures that the end dates of relationships are set to the start date of the next relationship.
     *
     * @param relationships the relationships
     */
    protected void setEndDates(List<? extends PeriodRelationship> relationships) {
        if (relationships.size() > 1) {
            relationships.sort((o1, o2) -> DateRules.compareTo(o1.getActiveStartTime(), o2.getActiveStartTime()));
            for (int i = 1; i < relationships.size(); ++i) {
                relationships.get(i - 1).setActiveEndTime(relationships.get(i).getActiveStartTime());
            }
        }
    }
}
