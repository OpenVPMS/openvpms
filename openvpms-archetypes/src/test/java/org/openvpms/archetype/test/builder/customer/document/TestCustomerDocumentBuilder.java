/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.document;

import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerActBuilder;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for customer documents, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestCustomerDocumentBuilder<B extends TestCustomerDocumentBuilder<B>>
        extends AbstractTestCustomerActBuilder<DocumentAct, B> {

    /**
     * The document.
     */
    private Document document;

    /**
     * Constructs a {@link TestCustomerDocumentBuilder}.
     *
     * @param archetype the customer document archetype
     * @param service   the archetype service
     */
    public TestCustomerDocumentBuilder(String archetype, ArchetypeService service) {
        super(archetype, DocumentAct.class, service);
    }

    /**
     * Constructs a {@link TestCustomerDocumentBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestCustomerDocumentBuilder(DocumentAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the document.
     *
     * @param document the document
     * @return this
     */
    public B document(Document document) {
        this.document = document;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        if (document != null) {
            DocumentRules rules = new DocumentRules(getService());
            toSave.addAll(rules.addDocument(object, document));
            document = null; // can't reuse
        }
        super.build(object, bean, toSave, toRemove);
    }
}