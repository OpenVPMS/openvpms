/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder of customer acts, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestCustomerActBuilder<T extends Act, B extends AbstractTestCustomerActBuilder<T, B>>
        extends AbstractTestActBuilder<T, B> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * Constructs a {@link AbstractTestCustomerActBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestCustomerActBuilder(String archetype, Class<T> type, ArchetypeService service) {
        super(archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestCustomerActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestCustomerActBuilder(T object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public B customer(Party customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Returns the customer.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        return customer;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
    }
}
