/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.object;

import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.model.object.IMObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Verifies the objects in an {@link IMObjectGraph} matches that expected.
 *
 * @author Tim Anderson
 */
public class IMObjectGraphVerifier {

    /**
     * The archetypes and their expected counts.
     */
    private final Map<String, Integer> counts = new HashMap<>();

    /**
     * The primary archetype.
     */
    private String primaryArchetype;

    /**
     * Sets the expected primary archetype.
     *
     * @param archetype the primary archetype
     * @return this
     */
    public IMObjectGraphVerifier primaryArchetype(String archetype) {
        this.primaryArchetype = archetype;
        return this;
    }

    /**
     * Sets the expected count a particular archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @param expected  the expected count
     * @return this
     */
    public IMObjectGraphVerifier count(String archetype, int expected) {
        counts.put(archetype, expected);
        return this;
    }

    /**
     * Verifies a graph matches that expected
     *
     * @param graph the graph
     */
    public void verify(IMObjectGraph graph) {
        assertNotNull(graph.getPrimary());
        assertTrue(graph.getPrimary().isA(primaryArchetype));

        List<IMObject> objects = new ArrayList<>(graph.getObjects());
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            String archetype = entry.getKey();
            int count = entry.getValue();
            List<IMObject> matches = objects.stream().filter(object -> object.isA(archetype))
                    .collect(Collectors.toList());
            assertEquals("Expected " + count + " " + archetype + " but got " + matches.size(),
                         count, matches.size());
            objects.removeAll(matches);
        }

        if (!objects.isEmpty()) {
            fail("There are unexpected objects in the graph: " + objects.stream().map(IMObject::getArchetype).collect(
                    Collectors.joining(", ")));
        }
    }
}