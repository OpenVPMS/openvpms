/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.act.LongTextBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder of <em>act.patientInvestigationResults</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestInvestigationResultsBuilder extends AbstractTestActBuilder<Act, TestInvestigationResultsBuilder> {

    /**
     * The parent builder.
     */
    private final TestInvestigationBuilder parent;

    /**
     * The notes builder.
     */
    private final LongTextBuilder notesBuilder;

    /**
     * The results.
     */
    private final List<TestInvestigationResultBuilder> results = new ArrayList<>();

    /**
     * The results id.
     */
    private ValueStrategy resultsId = ValueStrategy.unset();

    /**
     * The test.
     */
    private Entity test;

    /**
     * The category name.
     */
    private ValueStrategy categoryName = ValueStrategy.unset();

    /**
     * The notes.
     */
    private ValueStrategy notes = ValueStrategy.unset();

    /**
     * Constructs a {@link TestInvestigationResultsBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestInvestigationResultsBuilder(TestInvestigationBuilder parent, ArchetypeService service) {
        super(InvestigationArchetypes.RESULTS, Act.class, service);
        this.parent = parent;
        notesBuilder = new LongTextBuilder("notes", "longNotes", service);
    }

    /**
     * Sets the results id.
     *
     * @param resultsId the results id
     */
    public TestInvestigationResultsBuilder resultsId(String resultsId) {
        this.resultsId = ValueStrategy.value(resultsId);
        return this;
    }

    /**
     * Sets the test.
     *
     * @param test the test
     */
    public TestInvestigationResultsBuilder test(Entity test) {
        this.test = test;
        return this;
    }

    /**
     * Sets the category name.
     *
     * @param categoryName the category name
     */
    public TestInvestigationResultsBuilder categoryName(String categoryName) {
        this.categoryName = ValueStrategy.value(categoryName);
        return this;
    }

    /**
     * Sets the notes.
     *
     * @param notes the notes
     * @return this
     */
    public TestInvestigationResultsBuilder notes(String notes) {
        this.notes = ValueStrategy.value(notes);
        return this;
    }

    /**
     * Returns a builder to add a result.
     *
     * @return a result builder
     */
    public TestInvestigationResultBuilder result() {
        return new TestInvestigationResultBuilder(this, getService());
    }

    /**
     * Adds the results to the parent investigation.
     *
     * @return the parent investigation builder
     */
    public TestInvestigationBuilder add() {
        parent.addResults(this);
        return parent;
    }

    /**
     * Adds a result.
     *
     * @param result the result to add
     */
    protected void addResult(TestInvestigationResultBuilder result) {
        results.add(result);
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        resultsId.setValue(bean, "resultsId");
        if (test != null) {
            bean.setTarget("test", test);
        }
        categoryName.setValue(bean, "categoryName");
        notesBuilder.build(notes, bean, toSave, toRemove);

        int sequence = getNextSequence(bean, "items");
        for (TestInvestigationResultBuilder builder : results) {
            Act result = builder.build(toSave, toRemove);
            if (result.isNew()) {
                ActRelationship relationship = (ActRelationship) bean.addTarget("items", result, "results");
                relationship.setSequence(sequence++);
            }
        }
        results.clear(); // can't reuse
    }
}