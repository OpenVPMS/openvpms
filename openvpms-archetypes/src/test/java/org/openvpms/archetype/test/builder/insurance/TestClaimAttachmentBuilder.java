/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Test insurance claim attachment builder.
 *
 * @author Tim Anderson
 */
public class TestClaimAttachmentBuilder extends AbstractTestActBuilder<DocumentAct, TestClaimAttachmentBuilder> {

    /**
     * The parent claim builder.
     */
    private final TestClaimBuilder parent;

    /**
     * The generated attachment.
     */
    private ValueStrategy attachment = ValueStrategy.unset();

    /**
     * The original attachment.
     */
    private ValueStrategy original = ValueStrategy.unset();

    /**
     * Constructs a {@link TestClaimAttachmentBuilder}.
     *
     * @param parent  the parent claim builder
     * @param service the archetype service
     */
    public TestClaimAttachmentBuilder(TestClaimBuilder parent, ArchetypeService service) {
        super(InsuranceArchetypes.ATTACHMENT, DocumentAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the attachment.
     *
     * @param attachment the attachment
     * @return this
     */
    public TestClaimAttachmentBuilder attachment(Document attachment) {
        this.attachment = ValueStrategy.value(attachment);
        return this;
    }

    /**
     * Sets the original that the attachment was generated from.
     *
     * @param original the original. An invoice, customer or patient document or investigation
     * @return this
     */
    public TestClaimAttachmentBuilder original(Act original) {
        this.original = ValueStrategy.value(original);
        return this;
    }

    /**
     * Adds the attachment to the parent claim.
     *
     * @return the parent claim builder
     */
    public TestClaimBuilder add() {
        parent.addAttachment(this);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (original.isSet()) {
            Act act = (Act) original.getValue();
            bean.setTarget("original", act);
            bean.setValue("startTime", act.getActivityStartTime());
            if (object.getName() == null) {
                bean.setValue("name", act.getName());
            }
            bean.setValue("type", act.getArchetype());
        }
        if (attachment.isSet()) {
            Document document = (Document) attachment.getValue();
            if (document != null) {
                if (object.getName() == null) {
                    bean.setValue("name", document.getName());
                }
                object.setDocument(document.getObjectReference());
                toSave.add(document);
            } else {
                if (object.getDocument() != null) {
                    toRemove.add(object.getDocument());
                }
                object.setDocument(null);
            }
        }
    }
}
