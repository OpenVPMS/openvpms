/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.component.model.party.Contact;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating test contacts.
 *
 * @author Tim Anderson
 */
public class TestContactFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestContactFactory}.
     *
     * @param service the archetype service
     */
    public TestContactFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates an email contact.
     *
     * @param email    the email address
     * @param purposes the contact purposes
     * @return a new contact
     */
    public Contact createEmail(String email, String... purposes) {
        return createEmail(email, false, purposes);
    }

    /**
     * Creates an email contact.
     *
     * @param email     the email address
     * @param preferred if {@code true}, the contact is the preferred contact
     * @param purposes  the contact purposes
     * @return a new contact
     */
    public Contact createEmail(String email, boolean preferred, String... purposes) {
        return newEmail().email(email).preferred(preferred).purposes(purposes).build();
    }

    /**
     * Returns a builder for a new email contact.
     *
     * @return a new email contact builder
     */
    public TestEmailContactBuilder<?, ?> newEmail() {
        return new TestEmailContactBuilder<>(service);
    }


    /**
     * Returns a location builder with the address and suburb code, state and post code populated.
     * <p/>
     * This creates the suburb and state lookups if required.
     *
     * @param address    the address
     * @param suburbCode the suburb code
     * @param postCode   the post code
     * @return a new builder
     */
    public Contact createLocation(String address, String suburbCode, String stateCode, String postCode) {
        return newLocation()
                .address(address)
                .suburbCode(suburbCode)
                .stateCode(stateCode)
                .postcode(postCode)
                .build();
    }

    /**
     * Returns a builder for a new location contact.
     *
     * @return a new location contact builder
     */
    public TestLocationContactBuilder<?, ?> newLocation() {
        return new TestLocationContactBuilder<>(service);
    }


    /**
     * Returns a builder to update a location contact.
     *
     * @param location the contact to update
     * @return a new location contact builder
     */
    public TestLocationContactBuilder<?, ?> updateLocation(Contact location) {
        return new TestLocationContactBuilder<>(location, service);
    }

    /**
     * Creates a phone contact.
     *
     * @param phone the phone number
     * @return a new phone contact
     */
    public Contact createPhone(String phone) {
        return newPhone().phone(phone).build();
    }

    /**
     * Returns a builder for a new phone contact.
     *
     * @return a new phone contact builder
     */
    public TestPhoneContactBuilder<?, ?> newPhone() {
        return new TestPhoneContactBuilder<>(service);
    }

    /**
     * Returns a builder to update a phone contact.
     *
     * @param contact the contact to update
     * @return a new phone contact builder
     */
    public TestPhoneContactBuilder<?, ?> updatePhone(Contact contact) {
        return new TestPhoneContactBuilder<>(contact, service);
    }

}