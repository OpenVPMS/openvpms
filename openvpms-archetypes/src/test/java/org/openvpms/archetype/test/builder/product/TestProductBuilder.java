/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestSpeciesBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>product.*</em> archetypes, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestProductBuilder<T extends TestProductBuilder<T>>
        extends AbstractTestEntityBuilder<Product, T> {

    /**
     * The prices.
     */
    private final List<ProductPrice> prices = new ArrayList<>();

    /**
     * The price template links.
     */
    private final List<EntityLink> priceTemplates = new ArrayList<>();

    /**
     * The product stock locations.
     */
    private final List<EntityLink> productStockLocations = new ArrayList<>();

    /**
     * The built product stock locations.
     */
    private final List<EntityLink> builtProductStockLocations = new ArrayList<>();

    /**
     * The product suppliers.
     */
    private final List<EntityLink> productSuppliers = new ArrayList<>();

    /**
     * The product reminders.
     */
    private final List<EntityLink> productReminders = new ArrayList<>();

    /**
     * The document templates.
     */
    private final List<Entity> documents = new ArrayList<>();

    /**
     * The product tasks.
     */
    private final List<EntityLink> productTasks = new ArrayList<>();

    /**
     * The product locations.
     */
    private final List<Party> locations = new ArrayList<>();

    /**
     * The alert types.
     */
    private final List<Entity> alertTypes = new ArrayList<>();

    /**
     * The laboratory tests.
     */
    private final List<Entity> tests = new ArrayList<>();

    /**
     * The product discounts.
     */
    private final List<Entity> discounts = new ArrayList<>();

    /**
     * The printed name.
     */
    private ValueStrategy printedName = ValueStrategy.unset();

    /**
     * The built product reminders.
     */
    private List<EntityLink> builtProductReminders = new ArrayList<>();

    /**
     * The built product suppliers.
     */
    private List<EntityLink> builtProductSuppliers = new ArrayList<>();

    /**
     * The built product tasks.
     */
    private List<EntityLink> builtProductTasks = new ArrayList<>();

    /**
     * The species.
     */
    private String[] species;

    /**
     * The product type.
     */
    private Entity type;

    /**
     * The tax types.
     */
    private Lookup[] taxTypes;

    /**
     * The demographic updates.
     */
    private List<Lookup> demographicUpdates = new ArrayList<>();

    /**
     * The pharmacy.
     */
    private Entity pharmacy;

    /**
     * The patient identity.
     */
    private ValueStrategy patientIdentity = ValueStrategy.unset();

    /**
     * Constructs a {@link TestProductBuilder}.
     *
     * @param archetype the product archetype
     * @param service   the archetype service
     */
    public TestProductBuilder(String archetype, ArchetypeService service) {
        super(archetype, Product.class, service);
        name("zproduct");
    }

    /**
     * Constructs a {@link TestProductBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestProductBuilder(Product object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the printed name.
     *
     * @param printedName the printed name
     * @return this
     */
    public T printedName(String printedName) {
        this.printedName = ValueStrategy.value(printedName);
        return getThis();
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public T fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public T fixedPrice(String fixedPrice) {
        return fixedPrice(new BigDecimal(fixedPrice));
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public T fixedPrice(BigDecimal fixedPrice) {
        return newFixedPrice()
                .price(fixedPrice)
                .add();
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedCost  the fixed cost
     * @param fixedPrice the fixed price
     */
    public T fixedPrice(int fixedCost, int fixedPrice) {
        return newFixedPrice()
                .costAndPrice(fixedCost, fixedPrice)
                .add();
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedCost  the fixed cost
     * @param fixedPrice the fixed price
     */
    public T fixedPrice(BigDecimal fixedCost, BigDecimal fixedPrice) {
        return newFixedPrice()
                .costAndPrice(fixedCost, fixedPrice)
                .add();
    }

    /**
     * Returns a builder to add a fixed price.
     *
     * @return a fixed price builder
     */
    public TestFixedPriceBuilder<T> newFixedPrice() {
        return new TestFixedPriceBuilder<>(getThis(), getService());
    }

    /**
     * Adds a unit price.
     *
     * @param unitPrice the unit price
     */
    public T unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Adds a unit price.
     *
     * @param unitPrice the unit price
     */
    public T unitPrice(String unitPrice) {
        return unitPrice(new BigDecimal(unitPrice));
    }

    /**
     * Adds a unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public T unitPrice(BigDecimal unitPrice) {
        return newUnitPrice()
                .price(unitPrice)
                .add();
    }

    /**
     * Adds a unit price.
     *
     * @param unitCost  the unit cost
     * @param unitPrice the unit price
     * @return this
     */
    public T unitPrice(int unitCost, int unitPrice) {
        return newUnitPrice()
                .costAndPrice(unitCost, unitPrice)
                .add();
    }

    /**
     * Adds a unit price.
     *
     * @param unitCost  the unit cost
     * @param unitPrice the unit price
     * @return this
     */
    public T unitPrice(BigDecimal unitCost, BigDecimal unitPrice) {
        return newUnitPrice()
                .costAndPrice(unitCost, unitPrice)
                .add();
    }

    /**
     * Returns a builder to add a unit price.
     *
     * @return a fixed price builder
     */
    public TestUnitPriceBuilder<T> newUnitPrice() {
        return new TestUnitPriceBuilder<>(getThis(), getService());
    }

    /**
     * Adds a price.
     *
     * @param price the price
     */
    public T addPrice(ProductPrice price) {
        prices.add(price);
        return getThis();
    }

    /**
     * Adds prices.
     *
     * @param prices the prices
     */
    public T addPrices(ProductPrice... prices) {
        for (ProductPrice price : prices) {
            addPrice(price);
        }
        return getThis();
    }

    /**
     * Sets the product type.
     *
     * @param type the product type. An <em>entity.productType</em>
     * @return this
     */
    public T type(Entity type) {
        this.type = type;
        return getThis();
    }

    /**
     * Adds laboratory tests.
     *
     * @param tests the tests
     * @return this
     */
    public T addTests(Collection<Entity> tests) {
        return addTests(tests.toArray(new Entity[0]));
    }

    /**
     * Adds laboratory tests.
     *
     * @param tests the tests
     * @return this
     */
    public T addTests(Entity... tests) {
        this.tests.addAll(Arrays.asList(tests));
        return getThis();
    }

    /**
     * Adds product discounts.
     *
     * @param discounts the discounts
     * @return this
     */
    public T addDiscounts(Entity... discounts) {
        this.discounts.addAll(Arrays.asList(discounts));
        return getThis();
    }

    /**
     * Adds species that this product is for.
     * <p/>
     * If no species are specified, the product applies to all species.
     *
     * @param species the species codes
     * @return this
     */
    public T addSpecies(String... species) {
        this.species = species;
        return getThis();
    }

    /**
     * Adds a product price template.
     *
     * @param template the template
     * @return this
     */
    public T addPriceTemplate(Product template) {
        return addPriceTemplate(template, new Date(), null);
    }

    /**
     * Adds a product price template.
     *
     * @param template the template
     * @param fromDate the from-date. May be {@code null}
     * @param toDate   the to-date. May be {@code null}
     * @return this
     */
    public T addPriceTemplate(Product template, String fromDate, String toDate) {
        return addPriceTemplate(template, parseDate(fromDate), parseDate(toDate));
    }

    /**
     * Adds a product price template.
     *
     * @param template the template
     * @param fromDate the from-date. May be {@code null}
     * @param toDate   the to-date. May be {@code null}
     * @return this
     */
    public T addPriceTemplate(Product template, Date fromDate, Date toDate) {
        EntityLink link = create(getNodeArchetype("linked"), EntityLink.class);
        link.setTarget(template.getObjectReference());
        link.setActiveStartTime(fromDate);
        link.setActiveEndTime(toDate);
        priceTemplates.add(link);
        return getThis();
    }

    /**
     * Initialises the stock quantity for the product at a stock location.
     *
     * @param stockLocation the stock location
     * @param quantity      the quantity
     * @return this
     */
    public T stockQuantity(Party stockLocation, int quantity) {
        return stockQuantity(stockLocation, BigDecimal.valueOf(quantity));
    }

    /**
     * Initialises the stock quantity for the product at a stock location.
     *
     * @param stockLocation the stock location
     * @param quantity      the quantity
     * @return this
     */
    public T stockQuantity(Party stockLocation, BigDecimal quantity) {
        return newProductStockLocation()
                .stockLocation(stockLocation)
                .quantity(quantity)
                .add();
    }

    /**
     * Adds practice locations where the products is available at.
     *
     * @param locations the practice locations
     * @return this
     */
    public T addLocations(Party... locations) {
        this.locations.addAll(Arrays.asList(locations));
        return getThis();
    }

    /**
     * Returns a builder for a product-stock location relationship.
     *
     * @return a new product-stock location builder
     */
    public TestProductStockLocationBuilder<T> newProductStockLocation() {
        return new TestProductStockLocationBuilder<>(getThis(), getService());
    }

    /**
     * Adds a product-stock location relationship.
     * <p/>
     * NOTE: if there is an existing relationship with the same stock location, it will be updated from the supplied
     * instance.
     *
     * @param productStockLocation the product-stock location relationship
     */
    public T addProductStockLocation(EntityLink productStockLocation) {
        productStockLocations.add(productStockLocation);
        return getThis();
    }

    /**
     * Returns the built product-stock location relationships.
     *
     * @return the built relationships
     */
    public List<EntityLink> getProductStockLocations() {
        return builtProductStockLocations;
    }

    /**
     * Returns a builder for a product-supplier relationship.
     *
     * @return a new product-supplier builder
     */
    public TestProductSupplierBuilder<T> newProductSupplier() {
        return new TestProductSupplierBuilder<>(getThis(), getService());
    }

    /**
     * Adds a product-supplier relationship.
     *
     * @param productSupplier the product-supplier relationship
     * @return this
     */
    public T addProductSupplier(EntityLink productSupplier) {
        productSuppliers.add(productSupplier);
        return getThis();
    }

    /**
     * Returns the built product-supplier relationships.
     *
     * @return the product-supplier relationships
     */
    public List<EntityLink> getProductSuppliers() {
        return builtProductSuppliers;
    }

    /**
     * Adds a product reminder.
     *
     * @param reminderType the reminder type
     * @param period       the period
     * @param units        the period units
     * @return this
     */
    public T addProductReminder(Entity reminderType, int period, DateUnits units) {
        return newProductReminder().reminderType(reminderType).period(period, units).add();
    }

    /**
     * Returns a builder for a product-reminder relationship.
     *
     * @return a new product-reminder builder
     */
    public TestProductReminderBuilder<T> newProductReminder() {
        return new TestProductReminderBuilder<>(getThis(), getService());
    }

    /**
     * Adds a product-reminder relationship.
     *
     * @param productReminder the product-reminder relationship
     * @return this
     */
    public T addProductReminder(EntityLink productReminder) {
        productReminders.add(productReminder);
        return getThis();
    }

    /**
     * Adds a document.
     *
     * @param document the document template
     * @return this
     */
    public T addDocument(Entity document) {
        return addDocuments(document);
    }

    /**
     * Adds documents.
     *
     * @param documents the document templates to add
     * @return this
     */
    public T addDocuments(Entity... documents) {
        this.documents.addAll(Arrays.asList(documents));
        return getThis();
    }

    /**
     * Returns the built product reminders.
     *
     * @return the built product reminders
     */
    public List<EntityLink> getProductReminders() {
        return builtProductReminders;
    }

    /**
     * Returns a builder for a product-task relationship.
     *
     * @return a new product-task builder
     */
    public TestProductTaskBuilder<T> newProductTask() {
        return new TestProductTaskBuilder<>(getThis(), getService());
    }

    /**
     * Adds a product-task relationship.
     *
     * @param productTask the product-task relationship
     * @return this
     */
    public T addProductTask(EntityLink productTask) {
        productTasks.add(productTask);
        return getThis();
    }

    /**
     * Returns the built product tasks.
     *
     * @return the built product tasks
     */
    public List<EntityLink> getProductTasks() {
        return builtProductTasks;
    }

    /**
     * Adds alert types.
     *
     * @param alertTypes the alert types to add
     * @return this
     */
    public T addAlertTypes(Entity... alertTypes) {
        this.alertTypes.addAll(Arrays.asList(alertTypes));
        return getThis();
    }

    /**
     * Adds tax types.
     *
     * @param taxTypes the tax types to add
     * @return this
     */
    public T addTaxTypes(Lookup... taxTypes) {
        this.taxTypes = taxTypes;
        return getThis();
    }

    /**
     * Sets the pharmacy.
     *
     * @param pharmacy the pharmacy
     * @return this
     */
    public T pharmacy(Entity pharmacy) {
        this.pharmacy = pharmacy;
        return getThis();
    }

    /**
     * Sets the patient identity archetype.
     * <p/>
     * When set, this is used to indicate a patient identity should be created and added to the patient when the product
     * is charged.
     *
     * @param patientIdentity the patient identity archetype
     * @return this
     */
    public T patientIdentity(String patientIdentity) {
        this.patientIdentity = ValueStrategy.value(patientIdentity);
        return getThis();
    }

    /**
     * Adds a demographic update.
     *
     * @param demographicUpdate the demographic update
     * @return this
     */
    public T addDemographicUpdate(Lookup demographicUpdate) {
        this.demographicUpdates.add(demographicUpdate);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Product object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        printedName.setValue(bean, "printedName");
        for (ProductPrice price : prices) {
            object.addProductPrice(price);
        }
        for (EntityLink priceTemplate : priceTemplates) {
            priceTemplate.setSource(object.getObjectReference());
            object.addEntityLink(priceTemplate);
        }
        if (type != null) {
            bean.setTarget("type", type);
        }
        for (Entity test : tests) {
            bean.addTarget("tests", test);
        }
        for (Entity discount : discounts) {
            bean.addTarget("discounts", discount);
        }
        if (species != null) {
            TestSpeciesBuilder speciesBuilder = new TestSpeciesBuilder(getService());
            for (String species : species) {
                object.addClassification(speciesBuilder.code(species).build());
            }
        }

        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location);
            }
        }

        builtProductStockLocations.clear();
        for (EntityLink productStockLocation : productStockLocations) {
            EntityLink existing = bean.getValue("stockLocations", EntityLink.class,
                                                Predicates.targetEquals(productStockLocation.getTarget()));
            if (existing != null) {
                // need to update the existing relationship
                new TestProductStockLocationBuilder<>(getThis(), existing, getService())
                        .copy(productStockLocation)
                        .build();
                productStockLocation = existing;
            } else {
                productStockLocation.setSource(object.getObjectReference());
                object.addEntityLink(productStockLocation);
            }
            builtProductStockLocations.add(productStockLocation);
        }
        for (EntityLink productSupplier : productSuppliers) {
            productSupplier.setSource(object.getObjectReference());
            object.addEntityLink(productSupplier);
        }

        for (EntityLink productReminder : productReminders) {
            productReminder.setSource(object.getObjectReference());
            object.addEntityLink(productReminder);
        }
        for (EntityLink productTask : productTasks) {
            productTask.setSource(object.getObjectReference());
            object.addEntityLink(productTask);
        }
        for (Entity document : documents) {
            bean.addTarget("documents", document);
        }

        for (Entity alertType : alertTypes) {
            bean.addTarget("alerts", alertType);
        }
        if (taxTypes != null) {
            for (Lookup taxType : taxTypes) {
                object.addClassification(taxType);
            }
        }

        if (pharmacy != null) {
            bean.setTarget("pharmacy", pharmacy);
        }
        patientIdentity.setValue(bean, "patientIdentity");

        for (Lookup update : demographicUpdates) {
            object.addClassification(update);
        }

        // can't re-use
        prices.clear();
        priceTemplates.clear();
        productStockLocations.clear();
        builtProductSuppliers = new ArrayList<>(productSuppliers);
        productSuppliers.clear();
        builtProductReminders = new ArrayList<>(productReminders);
        productReminders.clear();
        builtProductTasks = new ArrayList<>(productTasks);
        productTasks.clear();
    }
}
