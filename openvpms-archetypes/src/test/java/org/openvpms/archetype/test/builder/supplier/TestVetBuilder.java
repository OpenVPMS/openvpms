/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builds <em>party.supplierVeterinaryPractice</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestVetBuilder extends AbstractTestPartyBuilder<Party, TestVetBuilder> {

    /**
     * The practices and the date they started.
     */
    private final Map<Party, Date> practices = new LinkedHashMap<>();

    /**
     * The title.
     */
    private String title;

    /**
     * The first name.
     */
    private ValueStrategy firstName = ValueStrategy.random();

    /**
     * The last name.
     */
    private ValueStrategy lastName = ValueStrategy.random("zvet");

    /**
     * Constructs a {@link TestVetBuilder}.
     *
     * @param service the archetype service
     */
    public TestVetBuilder(ArchetypeService service) {
        super(SupplierArchetypes.SUPPLIER_VET, Party.class, service);
        title("DR");
        firstName = ValueStrategy.random();
        lastName = ValueStrategy.random();
    }

    /**
     * Sets the title.
     *
     * @param title the title
     * @return this
     */
    public TestVetBuilder title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestVetBuilder firstName(String firstName) {
        this.firstName = ValueStrategy.value(firstName);
        return this;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     * @return this
     */
    public TestVetBuilder lastName(String lastName) {
        this.lastName = ValueStrategy.value(lastName);
        return this;
    }

    /**
     * Adds a practice.
     *
     * @param practice the practice
     * @return this
     */
    public TestVetBuilder addPractice(Party practice) {
        return addPractice(practice, new Date());
    }

    /**
     * Adds a practice.
     *
     * @param practice the practice
     * @param started  the vet's start date
     * @return this
     */
    public TestVetBuilder addPractice(Party practice, Date started) {
        practices.put(practice, started);
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (title != null) {
            new TestLookupBuilder("lookup.personTitle", getService()).code(title).build();
            bean.setValue("title", title);
        }
        bean.setValue("firstName", firstName.getValue());
        bean.setValue("lastName", lastName.getValue());
        if (!practices.isEmpty()) {
            for (Map.Entry<Party, Date> entry : practices.entrySet()) {
                Party practice = entry.getKey();
                PeriodRelationship relationship
                        = (PeriodRelationship) bean.addSource("practices", practice, "veterinarians");
                relationship.setActiveStartTime(entry.getValue());
                toSave.add(practice);
            }
            setEndDates(bean.getValues("practices", PeriodRelationship.class));
        }
    }
}