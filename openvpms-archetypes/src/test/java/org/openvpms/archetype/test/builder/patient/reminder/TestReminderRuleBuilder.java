/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRule;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.reminderRule</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestReminderRuleBuilder extends AbstractTestEntityBuilder<Entity, TestReminderRuleBuilder> {

    /**
     * The parent builder.
     */
    private final TestReminderCountBuilder parent;

    /**
     * Determines if reminders should be sent to the customer's REMINDER contacts.
     */
    private ValueStrategy contact = ValueStrategy.unset();

    /**
     * Determines if reminders should be sent to the customer's appropriate email contact.
     */
    private ValueStrategy email = ValueStrategy.unset();

    /**
     * Determines if reminders should be sent to the customer's appropriate SMS contact.
     */
    private ValueStrategy sms = ValueStrategy.unset();

    /**
     * Determines if reminders should be sent to the customer's appropriate print contact.
     */
    private ValueStrategy print = ValueStrategy.unset();

    /**
     * Determines if reminders should be exported.
     */
    private ValueStrategy export = ValueStrategy.unset();

    /**
     * Determines if reminders should be listed.
     */
    private ValueStrategy list = ValueStrategy.unset();

    /**
     * Determines how many options must apply for the rule to be satisfied.
     */
    private ValueStrategy sendTo = ValueStrategy.unset();

    /**
     * Constructs a {@link TestReminderRuleBuilder}.
     *
     * @param parent the parent builder
     * @param service the archetype service
     */
    public TestReminderRuleBuilder(TestReminderCountBuilder parent, ArchetypeService service) {
        super(ReminderArchetypes.REMINDER_RULE, Entity.class, service);
        this.parent = parent;
    }

    /**
     * Sends reminders to the customer's contacts that REMINDER purpose.
     *
     * @return this
     */
    public TestReminderRuleBuilder contact() {
        contact = ValueStrategy.value(true);
        return this;
    }

    /**
     * Sends reminders to the customer's email contact.
     *
     * @return this
     */
    public TestReminderRuleBuilder email() {
        email = ValueStrategy.value(true);
        return this;
    }

    /**
     * Sends reminders to the customer's SMS contact.
     *
     * @return this
     */
    public TestReminderRuleBuilder sms() {
        sms = ValueStrategy.value(true);
        return this;
    }

    /**
     * Indicates that reminders should be printed.
     *
     * @return this
     */
    public TestReminderRuleBuilder print() {
        print = ValueStrategy.value(true);
        return this;
    }

    /**
     * Indicates that reminders should be exported.
     *
     * @return this
     */
    public TestReminderRuleBuilder export() {
        export = ValueStrategy.value(true);
        return this;
    }

    /**
     * Indicates that reminders should be listed.
     *
     * @return this
     */
    public TestReminderRuleBuilder list() {
        list = ValueStrategy.value(true);
        return this;
    }

    /**
     * Determines how many options must apply for the rule to be satisfied.
     *
     * @return this
     */
    public TestReminderRuleBuilder sendTo(ReminderRule.SendTo sendTo) {
        this.sendTo = ValueStrategy.value(sendTo.name());
        return this;
    }

    /**
     * Adds the rule to the reminder count.
     *
     * @return the reminder type builder
     */
    public TestReminderCountBuilder add() {
        Entity rule = build(false);
        parent.addRule(rule);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        contact.setValue(bean, "contact");
        email.setValue(bean, "email");
        sms.setValue(bean, "sms");
        print.setValue(bean, "print");
        export.setValue(bean, "export");
        list.setValue(bean, "list");
        sendTo.setValue(bean, "sendTo");
    }
}