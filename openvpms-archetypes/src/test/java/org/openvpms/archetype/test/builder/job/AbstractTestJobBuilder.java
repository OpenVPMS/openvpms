/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.job;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.job*</em>> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestJobBuilder<B extends AbstractTestJobBuilder<B>>
        extends AbstractTestEntityBuilder<Entity, B> {

    /**
     * The user to run the job as.
     */
    private User runAs;

    /**
     * The user/group to notify on completion.
     */
    private Entity notify;

    /**
     * Constructs an {@link AbstractTestJobBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public AbstractTestJobBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
    }

    /**
     * Sets the user to run the job as.
     *
     * @param user the user
     * @return this
     */
    public B runAs(User user) {
        this.runAs = user;
        return getThis();
    }

    /**
     * Set the user/group to notify on completion.
     *
     * @param notify the user/group to notify
     * @return this
     */
    public B notify(Entity notify) {
        this.notify = notify;
        return getThis();
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Entity getObject(String archetype) {
        Entity job = getExisting();
        if (job == null) {
            job = getSingleton(archetype, Entity.class);
            if (!job.isNew()) {
                resetValues(job);
            }
        }
        return job;
    }

    /**
     * Resets values of a job.
     *
     * @param job the job
     */
    protected void resetValues(Entity job) {
        for (EntityLink link : job.getEntityLinks().toArray(new EntityLink[0])) {
            job.removeEntityLink(link);
        }
        for (EntityRelationship relationship : job.getEntityRelationships().toArray(new EntityRelationship[0])) {
            job.removeEntityRelationship(relationship);
        }
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (runAs != null) {
            bean.setTarget("runAs", runAs);
        }
        if (notify != null) {
            bean.setTarget("notify", notify);
        }
    }
}