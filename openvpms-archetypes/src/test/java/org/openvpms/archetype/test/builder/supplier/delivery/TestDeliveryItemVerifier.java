/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.delivery;

import org.openvpms.archetype.test.builder.supplier.TestSupplierActItemVerifier;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import static org.junit.Assert.assertEquals;

/**
 * Verifies  <em>act.supplierDeliveryItem</em> instances match that expected, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDeliveryItemVerifier extends TestSupplierActItemVerifier<TestDeliveryItemVerifier> {

    /**
     * The expected order item.
     */
    private Reference orderItem;

    /**
     * Constructs a {@link TestDeliveryItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestDeliveryItemVerifier(ArchetypeService service) {
        super(service);
    }

    /**
     * Sets the expected order item
     *
     * @param orderItem the order item
     * @return this
     */
    public TestDeliveryItemVerifier orderItem(FinancialAct orderItem) {
        return orderItem((orderItem != null) ? orderItem.getObjectReference() : null);
    }

    /**
     * Sets the expected order item
     *
     * @param orderItem the order item
     * @return this
     */
    public TestDeliveryItemVerifier orderItem(Reference orderItem) {
        this.orderItem = orderItem;
        return this;
    }

    /**
     * Initialises this from an item.
     *
     * @param bean the bean wrapping the item
     */
    @Override
    protected void initialise(IMObjectBean bean) {
        super.initialise(bean);
        orderItem(bean.getTargetRef("order"));
    }

    /**
     * Verifies an item matches that expected.
     *
     * @param bean the bean wrapping the item
     */
    @Override
    protected void verify(IMObjectBean bean) {
        super.verify(bean);
        assertEquals(orderItem, bean.getTargetRef("order"));
    }
}
