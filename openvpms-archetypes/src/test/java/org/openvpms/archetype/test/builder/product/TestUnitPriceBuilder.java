/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>productPrice.unitPrice</em> archetypes, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestUnitPriceBuilder<P extends TestProductBuilder<P>>
        extends AbstractTestPriceBuilder<P, TestUnitPriceBuilder<P>> {

    /**
     * Constructs a {@link TestUnitPriceBuilder}.
     *
     * @param service the archetype service
     */
    public TestUnitPriceBuilder(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link TestUnitPriceBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestUnitPriceBuilder(P parent, ArchetypeService service) {
        super(parent, ProductArchetypes.UNIT_PRICE, service);
    }
}