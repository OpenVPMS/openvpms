/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.hl7;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>entity.HL7MappingCubex</em> and <em>entity.HL7MappingIDEXX</em> instances, for testing purposes.
 * Note that this treats the instances as singletons to avoid a proliferation of largely read-only objects.
 *
 * @author Tim Anderson
 */
public class TestMappingBuilder extends AbstractTestEntityBuilder<Entity, TestMappingBuilder> {

    /**
     * Constructs a {@link TestMappingBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestMappingBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
    }

    /**
     * Returns the object to build.
     * <p/>
     * This implementation returns the existing instance, if one was supplied at construction and matches
     * the specified archetype else it returns a new instance.
     * <p/>
     * For builders that require unique instances, it may return an existing instance.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Entity getObject(String archetype) {
        Entity mapping = getExisting();
        if (mapping == null) {
            mapping = getSingleton(archetype, Entity.class);
        }
        return mapping;
    }
}