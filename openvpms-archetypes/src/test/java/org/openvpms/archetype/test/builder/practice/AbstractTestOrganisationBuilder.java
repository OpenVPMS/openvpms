/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.test.builder.doc.TestLogoBuilder;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for practice and location instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestOrganisationBuilder<B extends AbstractTestOrganisationBuilder<B>>
        extends AbstractTestPartyBuilder<Party, B> {

    /**
     * The logo.
     */
    private Document logo;

    /**
     * The logo act.
     */
    private DocumentAct logoAct;

    /**
     * Constructs an {@link AbstractTestOrganisationBuilder}.
     *
     * @param archetype the archetype
     * @param service   the archetype service
     */
    public AbstractTestOrganisationBuilder(String archetype, ArchetypeService service) {
        super(archetype, Party.class, service);
    }

    /**
     * Constructs an {@link AbstractTestOrganisationBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestOrganisationBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the practice logo.
     *
     * @param logo the image
     * @return this
     */
    public B logo(Document logo) {
        this.logo = logo;
        return getThis();
    }

    /**
     * Returns the logo.
     * <p/>
     * This is only valid after the organisation has been built.
     *
     * @return the logo, or {@code null} if none was built
     */
    public DocumentAct getLogo() {
        return logoAct;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (logo != null) {
            TestLogoBuilder logoBuilder = new TestLogoBuilder(getService());
            logoAct = logoBuilder.logo(logo)
                    .owner(object)
                    .build(false);
            toSave.add(logoAct);
            toSave.add(logo);
            logo = null;  // can't reuse
        } else {
            logoAct = null;
        }
    }
}