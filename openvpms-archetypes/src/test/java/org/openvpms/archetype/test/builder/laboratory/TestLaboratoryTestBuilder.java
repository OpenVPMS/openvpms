/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.laboratoryTest</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLaboratoryTestBuilder extends AbstractTestLaboratoryTestBuilder<TestLaboratoryTestBuilder> {

    public enum UseDevice {
        YES,
        NO,
        OPTIONAL
    }

    /**
     * Determines if a device is required.
     */
    private ValueStrategy useDevice = ValueStrategy.unset();

    /**
     * Constructs a {@link TestLaboratoryTestBuilder}.
     *
     * @param service the archetype service
     */
    public TestLaboratoryTestBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.TEST, service);
        name("ztest");
    }

    /**
     * Determines if a device is required.
     *
     * @param useDevice determines if a device is required
     * @return this
     */
    public TestLaboratoryTestBuilder useDevice(UseDevice useDevice) {
        this.useDevice = ValueStrategy.value(useDevice.toString());
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        useDevice.setValue(bean, "useDevice");
    }
}
