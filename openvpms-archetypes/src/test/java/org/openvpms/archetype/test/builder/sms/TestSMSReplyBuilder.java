/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.sms;

import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;

/**
 * Builder for <em>act.smsReply</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSMSReplyBuilder extends AbstractTestSMSBuilder<TestSMSReplyBuilder> {

    /**
     * Constructs a {@link TestSMSReplyBuilder}.
     *
     * @param service the archetype service
     */
    public TestSMSReplyBuilder(ArchetypeService service) {
        super("act.smsReply", service);
    }

    /**
     * Sets the sender.
     *
     * @param sender the sender
     * @return this
     */
    public TestSMSReplyBuilder sender(Party sender) {
        return contact(sender);
    }

    /**
     * Sets the time when the reply was received.
     *
     * @param received the received time
     * @return this
     */
    public TestSMSReplyBuilder received(Date received) {
        return startTime(received);
    }
}