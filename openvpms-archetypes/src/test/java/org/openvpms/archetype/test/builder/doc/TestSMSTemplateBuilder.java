/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.documentTemplateSMS*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSMSTemplateBuilder extends AbstractTestEntityBuilder<Entity, TestSMSTemplateBuilder> {

    /**
     * The parent builder.
     */
    private final TestDocumentTemplateBuilder parent;

    /**
     * The content type.
     */
    private String contentType;

    /**
     * The content.
     */
    private String content;

    /**
     * Constructs a {@link TestSMSTemplateBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestSMSTemplateBuilder(String archetype, ArchetypeService service) {
        this(null, archetype, service);
    }

    /**
     * Constructs a {@link TestSMSTemplateBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    TestSMSTemplateBuilder(TestDocumentTemplateBuilder parent, String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
        this.parent = parent;
        contentType("TEXT");
    }

    /**
     * Sets the content type.
     *
     * @param contentType the content type
     * @return this
     */
    public TestSMSTemplateBuilder contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    /**
     * Sets the content.
     *
     * @param content the content
     * @return this
     */
    public TestSMSTemplateBuilder content(String content) {
        this.content = content;
        return this;
    }

    /**
     * Adds the template to the parent builder.
     *
     * @return the parent builder
     */
    public TestDocumentTemplateBuilder add() {
        return parent.smsTemplate(build(false));
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (contentType != null) {
            bean.setValue("contentType", contentType);
        }
        if (content != null) {
            bean.setValue("content", content);
        }
    }
}
