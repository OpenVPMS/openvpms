/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.sms;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.smsMessage</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSMSBuilder extends AbstractTestSMSBuilder<TestSMSBuilder> {

    /**
     * The replies.
     */
    private final List<Reply> replies = new ArrayList<>();

    /**
     * The built replies. Available after building.
     */
    private final List<Act> builtReplies = new ArrayList<>();

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The source of the message, i.e. the act that triggered generation of the SMS.
     */
    private Act source;

    /**
     * Constructs a {@link TestSMSBuilder}.
     *
     * @param service the archetype service
     */
    public TestSMSBuilder(ArchetypeService service) {
        super("act.smsMessage", service);
    }

    /**
     * Constructs a {@link TestSMSBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestSMSBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the recipient.
     *
     * @param recipient the recipient
     * @return this
     */
    public TestSMSBuilder recipient(Party recipient) {
        return contact(recipient);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    public TestSMSBuilder customer(Party customer) {
        this.customer = customer;
        return this;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public TestSMSBuilder patient(Party patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Sets the time when the SMS was updated.
     * <p/>
     * This uses the startTime node for indexing purposes.
     *
     * @param updated the updated time
     * @return this
     */
    public TestSMSBuilder updated(Date updated) {
        return startTime(updated);
    }

    /**
     * Sets the time when the SMS was sent.
     * <p/>
     * This uses the endTime node.
     *
     * @param sent the sent time
     * @return this
     */
    public TestSMSBuilder sent(Date sent) {
        return endTime(sent);
    }

    /**
     * Sets the source of the message. This is the act that triggered generation of the SMS.
     *
     * @param source the source
     * @return this
     */
    public TestSMSBuilder source(Act source) {
        this.source = source;
        return this;
    }

    /**
     * Adds a reply.
     *
     * @param message the message
     * @return this
     */
    public TestSMSBuilder addReply(String message) {
        return addReply(new Date(), message);
    }

    /**
     * Adds a reply.
     *
     * @param received the timestamp when the reply was received
     * @param message  the message
     * @return this
     */
    public TestSMSBuilder addReply(Date received, String message) {
        replies.add(new Reply(received, message));
        return this;
    }

    /**
     * Adds a reply.
     *
     * @param reply the reply act
     * @return this
     */
    public TestSMSBuilder addReply(Act reply) {
        replies.add(new Reply(reply));
        return this;
    }

    /**
     * Returns the built replies.
     *
     * @return the built replies
     */
    public List<Act> getReplies() {
        return builtReplies;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        builtReplies.clear();
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (source != null) {
            ActRelationship relationship = create("actRelationship.smsSource", ActRelationship.class);
            relationship.setSource(source.getObjectReference());
            relationship.setTarget(bean.getReference());
            object.addActRelationship(relationship);
            source.addActRelationship(relationship);
            toSave.add(source);
        }
        if (!replies.isEmpty()) {
            TestSMSReplyBuilder builder = new TestSMSReplyBuilder(getService());
            for (Reply reply : replies) {
                Act act;
                if (reply.act == null) {
                    act = builder.sender(getContact())
                            .location(getLocation())
                            .phone(getPhone())
                            .message(reply.message)
                            .received(reply.received)
                            .build(false);
                } else {
                    act = reply.act;
                }
                bean.addTarget("replies", act, "sms");
                builtReplies.add(act);
                toSave.add(act);
            }
            replies.clear();
        }
    }

    private static class Reply {
        private final Date received;

        private final String message;

        private final Act act;

        public Reply(Date received, String message) {
            this(received, message, null);
        }

        public Reply(Act reply) {
            this(null, null, reply);
        }

        private Reply(Date received, String message, Act reply) {
            this.received = received;
            this.message = message;
            this.act = reply;
        }
    }
}