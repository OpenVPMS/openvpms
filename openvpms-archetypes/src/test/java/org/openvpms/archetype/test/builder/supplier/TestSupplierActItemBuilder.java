/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builds <em>act.supplierOrderItem</em>, <em>act.supplierDeliveryItem</em> and <em>act.supplierReturnItem</em>
 * instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSupplierActItemBuilder<P extends TestSupplierActBuilder<P, B>,
        B extends TestSupplierActItemBuilder<P, B>>
        extends AbstractTestActBuilder<FinancialAct, B> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The product.
     */
    private Product product;

    /**
     * The reorder code.
     */
    private ValueStrategy reorderCode = ValueStrategy.unset();

    /**
     * The reorder description.
     */
    private ValueStrategy reorderDescription = ValueStrategy.unset();

    /**
     * The package size.
     */
    private ValueStrategy packageSize = ValueStrategy.unset();

    /**
     * The package units.
     */
    private ValueStrategy packageUnits = ValueStrategy.unset();

    /**
     * The quantity.
     */
    private ValueStrategy quantity = ValueStrategy.unset();

    /**
     * The unit price.
     */
    private ValueStrategy unitPrice = ValueStrategy.unset();

    /**
     * The list price.
     */
    private ValueStrategy listPrice = ValueStrategy.unset();

    /**
     * The tax.
     */
    private ValueStrategy tax = ValueStrategy.unset();

    /**
     * Constructs a {@link TestSupplierActItemBuilder}.
     *
     * @param archetype the item archetype
     * @param parent    the parent builder
     * @param service   the archetype service
     */
    public TestSupplierActItemBuilder(String archetype, P parent, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Constructs a {@link TestSupplierActItemBuilder}.
     *
     * @param object  the object to update
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestSupplierActItemBuilder(FinancialAct object, P parent, ArchetypeService service) {
        super(object, service);
        this.parent = parent;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public B product(Product product) {
        this.product = product;
        return getThis();
    }

    /**
     * Sets the reorder code.
     *
     * @param reorderCode the reorder code
     * @return this
     */
    public B reorderCode(String reorderCode) {
        this.reorderCode = ValueStrategy.value(reorderCode);
        return getThis();
    }

    /**
     * Sets the reorder description.
     *
     * @param reorderDescription the reorder description
     * @return this
     */
    public B reorderDescription(String reorderDescription) {
        this.reorderDescription = ValueStrategy.value(reorderDescription);
        return getThis();
    }

    /**
     * Sets the package size.
     *
     * @param packageSize the package size
     * @return this
     */
    public B packageSize(int packageSize) {
        this.packageSize = ValueStrategy.value(packageSize);
        return getThis();
    }

    /**
     * Sets the package units.
     *
     * @param packageUnits the package units
     * @return this
     */
    public B packageUnits(String packageUnits) {
        this.packageUnits = ValueStrategy.value(packageUnits);
        return getThis();
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public B quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public B unitPrice(BigDecimal unitPrice) {
        this.unitPrice = ValueStrategy.value(unitPrice);
        return getThis();
    }

    /**
     * Sets the list price.
     *
     * @param listPrice the list price
     * @return this
     */
    public B listPrice(BigDecimal listPrice) {
        this.listPrice = ValueStrategy.value(listPrice);
        return getThis();
    }

    /**
     * Sets the tax.
     *
     * @param tax the tax
     * @return this
     */
    public B tax(BigDecimal tax) {
        this.tax = ValueStrategy.value(tax);
        return getThis();
    }

    /**
     * Adds the item to the act.
     *
     * @return the parent builder
     */
    public P add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (product != null) {
            bean.setTarget("product", product);
        }
        reorderCode.setValue(bean, "reorderCode");
        reorderDescription.setValue(bean, "reorderDescription");
        packageSize.setValue(bean, "packageSize");
        if (packageUnits.isSet()) {
            String code = packageUnits.toString();
            if (code != null)  {
                new TestLookupBuilder(ProductArchetypes.UNIT_OF_MEASURE, getService())
                        .code(code)
                        .build();
            }
        }
        packageUnits.setValue(bean, "packageUnits");
        quantity.setValue(bean, "quantity");
        unitPrice.setValue(bean, "unitPrice");
        listPrice.setValue(bean, "listPrice");
        tax.setValue(bean, "tax");
        getService().deriveValues(object);
    }
}
