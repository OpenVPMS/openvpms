/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.insurance;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Test policy builder.
 *
 * @author Tim Anderson
 */
public class TestPolicyBuilder extends AbstractTestActBuilder<Act, TestPolicyBuilder> {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The insurer.
     */
    private Party insurer;

    /**
     * The policy identity archetype.
     */
    private String policyNumberArchetype;

    /**
     * The policy number.
     */
    private ValueStrategy policyNumber = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPolicyBuilder}.
     *
     * @param service the archetype service
     */
    public TestPolicyBuilder(ArchetypeService service) {
        super(InsuranceArchetypes.POLICY, Act.class, service);
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return the customer
     */
    public TestPolicyBuilder customer(Party customer) {
        this.customer = customer;
        return this;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return the patient
     */
    public TestPolicyBuilder patient(Party patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Sets the insurer.
     *
     * @param insurer the insurer
     * @return the insurer
     */
    public TestPolicyBuilder insurer(Party insurer) {
        this.insurer = insurer;
        return this;
    }

    /**
     * Sets the policy number.
     *
     * @param policyNumber the policy number
     * @return this
     */
    public TestPolicyBuilder policyNumber(String policyNumber) {
        return policyNumber(ValueStrategy.value(policyNumber));
    }

    /**
     * Sets the policy number.
     *
     * @param policyNumber the policy number
     * @return this
     */
    public TestPolicyBuilder policyNumber(ValueStrategy policyNumber) {
        return policyNumber(policyNumberArchetype, policyNumber);
    }

    /**
     * Sets the policy number.
     *
     * @param archetype    the policy number archetype
     * @param policyNumber the policy number
     * @return this
     */
    public TestPolicyBuilder policyNumber(String archetype, String policyNumber) {
        return policyNumber(archetype, ValueStrategy.value(policyNumber));
    }

    /**
     * Sets the policy number.
     *
     * @param archetype    the policy number archetype
     * @param policyNumber the policy number
     * @return this
     */
    public TestPolicyBuilder policyNumber(String archetype, ValueStrategy policyNumber) {
        this.policyNumberArchetype = archetype;
        this.policyNumber = policyNumber;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setTarget("customer", customer);
        bean.setTarget("patient", patient);
        bean.setTarget("insurer", insurer);
        if (policyNumber.isSet()) {
            object.addIdentity(createActIdentity(InsuranceArchetypes.POLICY_IDENTITY, policyNumber));
        }
    }
}
