/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSPaymentBuilder;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSRefundBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating customer account acts.
 *
 * @author Tim Anderson
 */
public class TestCustomerAccountFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * The customer factory.
     */
    private final TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    private final TestPatientFactory patientFactory;

    /**
     * Constructs a {@link TestCustomerAccountFactory}.
     *
     * @param service         the archetype service
     * @param rules           the customer account rules
     * @param customerFactory the customer factory
     * @param patientFactory  the patient factory
     */
    public TestCustomerAccountFactory(ArchetypeService service, CustomerAccountRules rules,
                                      TestCustomerFactory customerFactory, TestPatientFactory patientFactory) {
        this.service = service;
        this.rules = rules;
        this.customerFactory = customerFactory;
        this.patientFactory = patientFactory;
    }

    /**
     * Returns a builder for a new initial balance.
     *
     * @return an initial balance builder
     */
    public TestInitialBalanceBuilder newInitialBalance() {
        return new TestInitialBalanceBuilder(service);
    }

    /**
     * Returns a builder for a new opening balance.
     *
     * @return an opening balance builder
     */
    public TestOpeningBalanceBuilder newOpeningBalance() {
        return new TestOpeningBalanceBuilder(service);
    }

    /**
     * Returns a builder for a new closing balance.
     *
     * @return a closing balance builder
     */
    public TestClosingBalanceBuilder newClosingBalance() {
        return new TestClosingBalanceBuilder(service);
    }

    /**
     * Returns a builder for a new bad debt.
     *
     * @return a bad debt builder
     */
    public TestBadDebtBuilder newBadDebt() {
        return new TestBadDebtBuilder(service);
    }

    /**
     * Creates a counter sale.
     *
     * @return the counter sale
     */
    public FinancialAct createCounterSale(int amount, String status) {
        return newCounterSale(amount, status).build();
    }

    /**
     * Returns a builder for a new counter sale, fully populated with the amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return the counter sale builder
     */
    public TestCounterSaleBuilder newCounterSale(int amount, String status) {
        Party customer = customerFactory.createCustomer();
        return newCounterSale()
                .customer(customer)
                .item().medicationProduct().unitPrice(amount).add()
                .status(status);
    }

    /**
     * Returns a builder for a new counter sale.
     *
     * @return a counter sale builder
     */
    public TestCounterSaleBuilder newCounterSale() {
        return new TestCounterSaleBuilder(service);
    }

    /**
     * Returns a builder for a new estimate.
     *
     * @return an estimate builder
     */
    public TestEstimateBuilder newEstimate() {
        return new TestEstimateBuilder(service, rules);
    }

    /**
     * Returns a builder for a new estimate item.
     *
     * @return an estimate item builder
     */
    public TestEstimateItemBuilder newEstimateItem() {
        return new TestEstimateItemBuilder(null, service, rules);
    }

    /**
     * Creates an invoice for the specified amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return a new invoice
     */
    public FinancialAct createInvoice(int amount, String status) {
        return newInvoice(amount, status).build();
    }

    /**
     * Creates an invoice for the specified customer, amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return a new invoice
     */
    public FinancialAct createInvoice(Party customer, int amount, String status) {
        return newInvoice(customer, amount, status).build();
    }

    /**
     * Returns a builder for a new customer invoice, fully populated with the amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return the invoice builder
     */
    public TestInvoiceBuilder newInvoice(int amount, String status) {
        return newInvoice(customerFactory.createCustomer(), amount, status);
    }

    /**
     * Returns a builder for a new customer invoice, fully populated with the customer, amount and status.
     *
     * @param customer the customer
     * @param amount   the amount
     * @param status   the status
     * @return the invoice builder
     */
    public TestInvoiceBuilder newInvoice(Party customer, int amount, String status) {
        Party patient = patientFactory.createPatient(customer);
        return newInvoice()
                .customer(customer)
                .item().patient(patient).medicationProduct().unitPrice(amount).add()
                .status(status);
    }

    /**
     * Returns a builder for a new customer invoice.
     *
     * @return a customer invoice builder
     */
    public TestInvoiceBuilder newInvoice() {
        return new TestInvoiceBuilder(service);
    }

    /**
     * Returns a builder for a new invoice item.
     */
    public TestInvoiceItemBuilder newInvoiceItem() {
        return new TestInvoiceItemBuilder(null, service);
    }

    /**
     * Returns a builder to update an invoice item.
     * <p/>
     * NOTE: this does not update the parent invoice, and no methods referring to the parent invoice may be invoked.
     */
    public TestInvoiceItemBuilder updateInvoiceItem(FinancialAct item) {
        return new TestInvoiceItemBuilder(null, item, service);
    }

    /**
     * Creates a customer credit for the specified amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return the credit
     */
    public FinancialAct createCredit(int amount, String status) {
        return newCredit(amount, status).build();
    }

    /**
     * Returns a builder for a new customer credit, fully populated with the amount and status.
     *
     * @param amount the amount
     * @param status the status
     * @return the credit builder
     */
    public TestCreditBuilder newCredit(int amount, String status) {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        return newCredit()
                .customer(customer)
                .item().patient(patient).medicationProduct().unitPrice(amount).add()
                .status(status);
    }

    /**
     * Returns a builder for a new customer credit.
     *
     * @return a customer credit builder
     */
    public TestCreditBuilder newCredit() {
        return new TestCreditBuilder(service);
    }

    /**
     * Returns a builder for a new credit adjustment.
     *
     * @return a new credit adjustment builder
     */
    public TestCreditAdjustBuilder newCreditAdjust() {
        return new TestCreditAdjustBuilder(service);
    }

    /**
     * Returns a builder for a new debit adjustment.
     *
     * @return a new debit adjustment builder
     */
    public TestDebitAdjustBuilder newDebitAdjust() {
        return new TestDebitAdjustBuilder(service);
    }

    /**
     * Returns a builder for a new customer payment.
     *
     * @return a customer payment builder
     */
    public TestPaymentBuilder newPayment() {
        return new TestPaymentBuilder(service);
    }

    /**
     * Returns a builder to update a customer payment.
     *
     * @param payment the payment to update
     * @return a customer payment builder
     */
    public TestPaymentBuilder updatePayment(FinancialAct payment) {
        return new TestPaymentBuilder(payment, service);
    }

    /**
     * Returns a builder for a new customer cash payment item.
     *
     * @return a customer cash payment item builder
     */
    public TestCashPaymentItemBuilder newCashPaymentItem() {
        return new TestCashPaymentItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new customer EFT payment item.
     *
     * @return a customer EFT payment item builder
     */
    public TestEFTPaymentItemBuilder newEFTPaymentItem() {
        return new TestEFTPaymentItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new customer other payment item.
     *
     * @return a customer payment builder
     */
    public TestOtherPaymentItemBuilder newOtherPaymentItem() {
        return new TestOtherPaymentItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new EFTPOS payment.
     *
     * @return an EFTPOS payment builder
     */
    public TestEFTPOSPaymentBuilder newEFTPOSPayment() {
        return new TestEFTPOSPaymentBuilder(service);
    }

    /**
     * Returns a builder for a new EFTPOS refund.
     *
     * @return an EFTPOS refund builder
     */
    public TestEFTPOSRefundBuilder newEFTPOSRefund() {
        return new TestEFTPOSRefundBuilder(service);
    }

    /**
     * Returns a builder for a new customer refund.
     *
     * @return a refund payment builder
     */
    public TestRefundBuilder newRefund() {
        return new TestRefundBuilder(service);
    }

    /**
     * Returns a builder for a new customer cash refund item.
     *
     * @return a customer cash refund item builder
     */
    public TestCashRefundItemBuilder newCashRefundItem() {
        return new TestCashRefundItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new customer EFT refund item.
     *
     * @return a customer EFT refund item builder
     */
    public TestEFTRefundItemBuilder newEFTRefundItem() {
        return new TestEFTRefundItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new customer other refund item.
     *
     * @return a customer payment builder
     */
    public TestOtherRefundItemBuilder newOtherRefundItem() {
        return new TestOtherRefundItemBuilder(null, service);
    }

    /**
     * Creates a new custom payment type.
     *
     * @param code the code
     * @return the custom payment type
     */
    public Lookup createCustomPaymentType(String code) {
        return newCustomPaymentType().code(code).build();
    }

    /**
     * Returns a builder to build a custom payment type.
     *
     * @return a custom payment type builder
     */
    public TestLookupBuilder newCustomPaymentType() {
        return new TestLookupBuilder(CustomerAccountArchetypes.CUSTOM_PAYMENT_TYPE, service);
    }
}
