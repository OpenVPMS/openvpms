/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.sms;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Base class for SMS builders.
 *
 * @author Tim Anderson
 */
public class AbstractTestSMSBuilder<B extends AbstractTestSMSBuilder<B>> extends AbstractTestActBuilder<Act, B> {

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The recipient for outgoing messages, the sender for incoming messages.
     */
    private Party contact;

    /**
     * The phone.
     */
    private ValueStrategy phone = ValueStrategy.unset();

    /**
     * The message.
     */
    private ValueStrategy message = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestSMSBuilder}.
     *
     * @param archetype the SMS archetype
     * @param service   the archetype service
     */
    public AbstractTestSMSBuilder(String archetype, ArchetypeService service) {
        super(archetype, Act.class, service);
    }

    /**
     * Constructs an {@link AbstractTestSMSBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestSMSBuilder(Act object, ArchetypeService service) {
        super(object, service);
        IMObjectBean bean = getBean(object);
        location = bean.getTarget("location", Party.class);
        contact = bean.getTarget("contact", Party.class);
        phone = ValueStrategy.value(bean.getString("phone"));
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    public Party getLocation() {
        return location;
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public B location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Returns the contact. This is the recipient for outgoing messages, the sender for incoming messages.
     *
     * @return the contact
     */
    public Party getContact() {
        return contact;
    }

    /**
     * Sets contact. This is the recipient for outgoing messages, the sender for incoming messages.
     *
     * @param contact the contact
     * @return this
     */
    public B contact(Party contact) {
        this.contact = contact;
        return getThis();
    }

    /**
     * Returns the phone number.
     *
     * @return the phone number
     */
    public String getPhone() {
        return phone.toString();
    }

    /**
     * Sets the phone number.
     *
     * @param phone the phone number
     * @return this
     */
    public B phone(String phone) {
        this.phone = ValueStrategy.value(phone);
        return getThis();
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @return this
     */
    public B message(String message) {
        this.message = ValueStrategy.value(message);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (contact != null) {
            bean.setTarget("contact", contact);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        phone.setValue(bean, "phone");
        message.setValue(bean, "message");
    }
}