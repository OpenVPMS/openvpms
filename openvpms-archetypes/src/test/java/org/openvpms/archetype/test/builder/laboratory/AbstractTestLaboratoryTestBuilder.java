/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for <em>entity.laboratoryTest*</em> archetypes for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestLaboratoryTestBuilder<B extends AbstractTestLaboratoryTestBuilder<B>>
        extends AbstractTestEntityBuilder<Entity, B> {

    /**
     * The test code archetype.
     */
    private String testCodeArchetype;

    /**
     * The test code.
     */
    private ValueStrategy testCode;

    /**
     * The built test code.
     */
    private String testCodeValue;

    /**
     * The test code name.
     */
    private ValueStrategy testCodeName = ValueStrategy.unset();

    /**
     * The test price.
     */
    private ValueStrategy price = ValueStrategy.unset();

    /**
     * The investigation type.
     */
    private Entity investigationType;

    /**
     * Determines if the test can be grouped with others on an investigation.
     */
    private ValueStrategy group = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestLaboratoryTestBuilder}.
     *
     * @param archetype the test archetype
     * @param service   the archetype service
     */
    public AbstractTestLaboratoryTestBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
    }

    /**
     * Sets the test code.
     *
     * @param archetype the test code archetype
     * @param code      the test code
     * @return this
     */
    public B code(String archetype, String code) {
        return code(archetype, ValueStrategy.value(code));
    }

    /**
     * Sets the test code.
     *
     * @param archetype the test code archetype
     * @param code      the test code
     * @return this
     */
    public B code(String archetype, ValueStrategy code) {
        testCodeArchetype = archetype;
        testCode = code;
        return getThis();
    }

    /**
     * Sets the test code.
     *
     * @param archetype the test code archetype
     * @param code      the test code
     * @param name      the test code name
     * @return this
     */
    public B code(String archetype, String code, String name) {
        code(archetype, code);
        testCodeName = ValueStrategy.value(name);
        return getThis();
    }

    /**
     * Returns the test code.
     * <p/>
     * This is only applicable after the test is built.
     *
     * @return the test code. May be {@code null}
     */
    public String getTestCode() {
        return testCodeValue;
    }

    /**
     * Sets the price.
     *
     * @param price the price
     * @return this
     */
    public B price(int price) {
        return price(BigDecimal.valueOf(price));
    }

    /**
     * Sets the price.
     *
     * @param price the price
     * @return this
     */
    public B price(BigDecimal price) {
        this.price = ValueStrategy.value(price);
        return getThis();
    }

    /**
     * Sets the investigation type.
     *
     * @param investigationType the investigation type
     * @return this
     */
    public B investigationType(Entity investigationType) {
        this.investigationType = investigationType;
        return getThis();
    }

    /**
     * Determines if the test can be grouped with others on an investigation.
     *
     * @param group if {@code true} the test can be grouped with others on an investigation
     * @return this
     */
    public B group(boolean group) {
        this.group = ValueStrategy.value(group);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (testCode != null) {
            EntityIdentity identity = newEntityIdentity(testCodeArchetype)
                    .identity(testCode)
                    .name(testCodeName)
                    .build();
            object.addIdentity(identity);
            testCodeValue = identity.getIdentity();
        } else {
            testCodeValue = null;
        }
        price.setValue(bean, "price");
        if (investigationType != null) {
            bean.setTarget("investigationType", investigationType);
        }
        group.setValue(bean, "group");
    }
}