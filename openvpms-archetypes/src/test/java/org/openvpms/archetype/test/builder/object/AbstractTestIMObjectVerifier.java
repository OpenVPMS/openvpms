/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.object;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * A verifier of {@link IMObject} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestIMObjectVerifier<T extends IMObject, V extends AbstractTestIMObjectVerifier<T, V>> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected archetype.
     */
    private ValueStrategy archetype = ValueStrategy.unset();

    /**
     * The expected created-by user.
     */
    private ValueStrategy createdBy = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestIMObjectVerifier}.
     *
     * @param service the archetype service
     */
    public AbstractTestIMObjectVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Sets the expected archetype.
     *
     * @param archetype the archetype
     * @return this
     */
    public V archetype(String archetype) {
        this.archetype = ValueStrategy.value(archetype);
        return getThis();
    }

    /**
     * Sets the expected created-by user
     *
     * @param createdBy the expected created-by user
     * @return this
     */
    public V createdBy(User createdBy) {
        return createdBy(getReference(createdBy));
    }

    /**
     * Sets the expected created-by user
     *
     * @param createdBy the expected created-by user
     * @return this
     */
    public V createdBy(Reference createdBy) {
        this.createdBy = ValueStrategy.value(createdBy);
        return getThis();
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     */
    public void verify(T object) {
        verify(object, getBean(object));
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    protected void verify(T object, IMObjectBean bean) {
        if (archetype.isSet()) {
            assertTrue(object.isA(archetype.toString()));
        }
        if (object instanceof AuditableIMObject) {
            checkEquals(createdBy, ((AuditableIMObject) object).getCreatedBy());
        } else {
            assertNull(createdBy.getValue());
        }
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Returns a reference for an object.
     *
     * @param object the object. May be {@code null}
     * @return the corresponding reference. May be {@code null}
     */
    protected Reference getReference(IMObject object) {
        return object != null ? object.getObjectReference() : null;
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected V getThis() {
        return (V) this;
    }

    /**
     * Verifies a value matches that expected.
     *
     * @param expected the expected value
     * @param actual   the actual value
     */
    protected void checkEquals(ValueStrategy expected, Object actual) {
        if (expected.isSet()) {
            Object value = expected.getValue();
            if (actual instanceof BigDecimal) {
                if (value instanceof Integer) {
                    checkEquals((Integer) value, (BigDecimal) actual);
                } else if (value instanceof String) {
                    checkEquals((String) value, (BigDecimal) actual);
                } else {
                    checkEquals((BigDecimal) value, (BigDecimal) actual);
                }
            } else {
                assertEquals(expected.getValue(), actual);
            }
        }
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(String expected, BigDecimal actual) {
        checkEquals(new BigDecimal(expected), actual);
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(int expected, BigDecimal actual) {
        checkEquals(BigDecimal.valueOf(expected), actual);
    }

    /**
     * Verifies two {@code BigDecimal} instances are equal.
     *
     * @param expected the expected value. May be {@code null}
     * @param actual   the actual value. May be {@code null}
     */
    protected void checkEquals(BigDecimal expected, BigDecimal actual) {
        TestHelper.checkEquals(expected, actual);
    }


    /**
     * Verifies a date matches that expected, ignoring any milliseconds, as these aren't made persistent.
     *
     * @param expected the expected date
     * @param actual   the actual date
     */
    protected void checkEquals(ValueStrategy expected, Date actual) {
        if (expected.isSet()) {
            assertEquals(0, DateRules.compareTo((Date) expected.getValue(), actual, true));
        }
    }

    /**
     * Verifies a target reference matches that expected.
     *
     * @param expected the expected value
     * @param bean     the bean
     * @param node     the node of the actual value
     */
    protected void checkTargetEquals(ValueStrategy expected, IMObjectBean bean, String node) {
        if (expected.isSet()) {
            assertEquals(expected.getValue(), bean.getTargetRef(node));
        }
    }
}