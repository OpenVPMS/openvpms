/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.HL7ServiceLaboratory</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestHL7LaboratoryBuilder extends AbstractTestEntityBuilder<Entity, TestHL7LaboratoryBuilder> {

    /**
     * The sender.
     */
    private Entity sender;

    /**
     * The practice location this laboratory is used at.
     */
    private Party location;

    /**
     * The user.
     */
    private User user;

    /**
     * Constructs an {@link TestHL7LaboratoryBuilder}.
     *
     * @param service the archetype service
     */
    public TestHL7LaboratoryBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.HL7_LABORATORY, Entity.class, service);
        name(ValueStrategy.random("zhl7"));
    }

    /**
     * Sets the sender.
     *
     * @param sender the sender
     * @return this
     */
    public TestHL7LaboratoryBuilder sender(Entity sender) {
        this.sender = sender;
        return this;
    }

    /**
     * Sets the location where the laboratory may be used.
     *
     * @param location the location
     * @return this
     */
    public TestHL7LaboratoryBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the user.
     *
     * @param user the user
     * @return this
     */
    public TestHL7LaboratoryBuilder user(User user) {
        this.user = user;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (sender != null) {
            bean.setTarget("sender", sender);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (user != null) {
            bean.setTarget("user", user);
        }
    }
}
