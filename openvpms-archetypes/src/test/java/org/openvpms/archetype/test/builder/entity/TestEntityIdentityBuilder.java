/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.entity;

import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entityIdentity.*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEntityIdentityBuilder extends AbstractTestIMObjectBuilder<EntityIdentity, TestEntityIdentityBuilder> {

    /**
     * The identity value strategy.
     */
    private ValueStrategy identity = ValueStrategy.unset();

    /**
     * Constructs an {@link TestEntityIdentityBuilder}.
     *
     * @param archetype the entity identity archetype
     * @param service   the archetype service
     */
    public TestEntityIdentityBuilder(String archetype, ArchetypeService service) {
        super(archetype, EntityIdentity.class, service);
    }

    /**
     * Sets the identity.
     *
     * @param identity the identity
     * @return this
     */
    public TestEntityIdentityBuilder identity(String identity) {
        return identity(ValueStrategy.value(identity));
    }

    /**
     * Sets the identity.
     *
     * @param identity the identity
     * @return this
     */
    public TestEntityIdentityBuilder identity(ValueStrategy identity) {
        this.identity = identity;
        return this;
    }

    /**
     * Builds the identity.
     *
     * @return the identity.
     */
    @Override
    public EntityIdentity build() {
        return super.build(false);
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(EntityIdentity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        identity.setValue(bean, "identity");
    }
}
