/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;

/**
 * Verifier for <em>act.patientAlert</em> acts for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientAlertVerifier extends AbstractTestPatientActVerifier<Act, TestPatientAlertVerifier> {

    /**
     * The expected alert type.
     */
    private ValueStrategy alertType = ValueStrategy.unset();

    /**
     * The alert type duration.
     */
    private int duration = -1;

    /**
     * The alert type duration units.
     */
    private DateUnits durationUnits;

    /**
     * The expected product.
     */
    private ValueStrategy product = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPatientAlertVerifier}.
     *
     * @param service the archetype service
     */
    public TestPatientAlertVerifier(ArchetypeService service) {
        super(service);
        archetype(PatientArchetypes.ALERT);
        status(ActStatus.IN_PROGRESS);
    }

    /**
     * Sets the expected alert type.
     *
     * @param alertType the alert type
     */
    public TestPatientAlertVerifier alertType(Entity alertType) {
        this.alertType = ValueStrategy.value(getReference(alertType));
        IMObjectBean bean = getBean(alertType);
        if (bean.getString("durationUnits") != null) {
            duration = bean.getInt("duration");
            durationUnits = DateUnits.fromString(bean.getString("durationUnits"));
        } else {
            duration = -1;
        }
        return this;
    }

    /**
     * Calculates the end time from the alert type.
     *
     * @return this
     */
    public TestPatientAlertVerifier calculateEndTime() {
        Date endTime = null;
        if (duration >= 0) {
            Date startTime = getStartTime();
            if (startTime != null) {
                endTime = DateRules.getDate(startTime, duration, durationUnits);
            }
        }
        return endTime(endTime);
    }

    /**
     * Sets the expected product.
     *
     * @param product the product
     */
    public TestPatientAlertVerifier product(Product product) {
        this.product = ValueStrategy.value(getReference(product));
        return this;
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(Act object, IMObjectBean bean) {
        super.verify(object, bean);
        checkTargetEquals(alertType, bean, "alertType");
        checkTargetEquals(product, bean, "product");
    }
}