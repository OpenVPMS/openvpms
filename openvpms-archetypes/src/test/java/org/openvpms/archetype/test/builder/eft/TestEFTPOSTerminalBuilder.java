/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.eft;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.EFTPOSTerminal*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEFTPOSTerminalBuilder extends AbstractTestEntityBuilder<Entity, TestEFTPOSTerminalBuilder> {

    /**
     * Determines if the terminal prints receipts.
     */
    private Boolean printer;

    /**
     * Constructs a {@link TestEFTPOSTerminalBuilder}.
     *
     * @param service the archetype service
     */
    public TestEFTPOSTerminalBuilder(ArchetypeService service) {
        this("entity.EFTPOSTerminalTest", service);
    }

    /**
     * Constructs a {@link TestEFTPOSTerminalBuilder}.
     *
     * @param archetype the archetype to build. Must be an <em>entity.EFTPOSTerminal*</em>
     * @param service   the archetype service
     */
    public TestEFTPOSTerminalBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
        name(ValueStrategy.random("zterminal"));
    }

    /**
     * Indicates that the terminal is responsible for printing receipts.
     *
     * @return this
     */
    public TestEFTPOSTerminalBuilder printer() {
        return printer(true);
    }

    /**
     * Determines if the terminal is responsible for printing receipts.
     *
     * @param printer if {@code true}, the terminal is responsible for printing receipts
     * @return this
     */
    public TestEFTPOSTerminalBuilder printer(boolean printer) {
        this.printer = printer;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (printer != null) {
            bean.setValue("printer", printer);
        }
    }
}
