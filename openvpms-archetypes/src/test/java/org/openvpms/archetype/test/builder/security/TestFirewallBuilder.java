/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.security;

import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Test builder for <em>entity.globalSettingsFirewallInstances</em>.
 *
 * @author Tim Anderson
 */
public class TestFirewallBuilder extends AbstractTestIMObjectBuilder<Entity, TestFirewallBuilder> {

    /**
     * The access type.
     */
    private AccessType accessType;

    /**
     * Determines if multifactor authentication is enabled.
     */
    private ValueStrategy enableMfa = ValueStrategy.unset();

    /**
     * The allowed addresses.
     */
    private String[] allowedAddresses;

    /**
     * Constructs a {@link TestFirewallBuilder}.
     *
     * @param service the archetype service
     */
    public TestFirewallBuilder(ArchetypeService service) {
        super(SettingsArchetypes.FIREWALL_SETTINGS, Entity.class, service);
    }

    /**
     * Sets the access type.
     *
     * @param accessType the access type
     * @return this
     */
    public TestFirewallBuilder accessType(AccessType accessType) {
        this.accessType = accessType;
        return this;
    }

    /**
     * Determines if multifactor authentication is enabled.
     *
     * @param enable if {@code true}, enable multifactor authentication, else disable it
     * @return this
     */
    public TestFirewallBuilder enableMultifactorAuthentication(boolean enable) {
        this.enableMfa = ValueStrategy.value(enable);
        return this;
    }

    /**
     * Sets the allowed addresses.
     *
     * @param allowedAddresses the allowed addresses
     * @return this
     */
    public TestFirewallBuilder allowedAddresses(String... allowedAddresses) {
        this.allowedAddresses = allowedAddresses;
        return this;
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Entity getObject(String archetype) {
        Entity entity = getExisting();
        if (entity == null) {
            entity = getSingleton(archetype, Entity.class);
        }
        return entity;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        FirewallSettings settings = new FirewallSettings(object, getService());
        if (accessType != null) {
            settings.setAccessType(accessType);
        }
        enableMfa.setValue(bean, "enableMfa");
        if (allowedAddresses != null) {
            List<FirewallEntry> entries = new ArrayList<>();
            for (String address : allowedAddresses) {
                entries.add(new FirewallEntry(address));
            }
            settings.setAllowedAddresses(entries);
        }
    }
}
