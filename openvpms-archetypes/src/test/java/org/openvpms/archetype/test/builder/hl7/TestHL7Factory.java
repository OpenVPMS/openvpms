/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.hl7;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A factory for HL7 objects, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestHL7Factory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestHL7Factory}.
     *
     * @param service the archetype service
     */
    public TestHL7Factory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new HL7 pharmacy.
     *
     * @param location the practice location
     * @param user     the user
     * @return a new HL7 pharmacy
     */
    public Entity createHL7Pharmacy(Party location, User user) {
        return createHL7Pharmacy(location, false, user);
    }

    /**
     * Creates a new HL7 pharmacy.
     *
     * @param location the practice location
     * @param oneway   if {@code true}, the pharmacy is one-way, else it is two-way
     * @param user     the user
     * @return a new HL7 pharmacy
     */
    public Entity createHL7Pharmacy(Party location, boolean oneway, User user) {
        TestHL7PharmacyBuilder builder = newHL7Pharmacy();
        builder.location(location)
                .user(user);
        if (oneway) {
            builder.oneway()
                    .defaultSender();
        } else {
            builder.defaultSenderReceiver();
        }
        return builder.build();
    }

    /**
     * Returns a builder for a new HL7 pharmacy.
     *
     * @return a new builder
     */
    public TestHL7PharmacyBuilder newHL7Pharmacy() {
        return new TestHL7PharmacyBuilder(service);
    }

    /**
     * Creates a builder for a new sender.
     *
     * @return a new builder
     */
    public TestHL7SenderBuilder newSender() {
        return new TestHL7SenderBuilder(service);
    }

    /**
     * Creates a builder for a new receiver.
     *
     * @return a new builder
     */
    public TestHL7ReceiverBuilder newReceiver() {
        return new TestHL7ReceiverBuilder(service);
    }

    /**
     * Returns the Cubex mapping.
     *
     * @return the mapping. An instance of <em>entity.HL7MappingCubex</em>
     */
    public Entity getCubexMapping() {
        return new TestMappingBuilder("entity.HL7MappingCubex", service).build();
    }

    /**
     * Returns the IDEXX mapping.
     *
     * @return the mapping. An instance of <em>entity.HL7MappingIDEXX</em>
     */
    public Entity getIDEXXMapping() {
        return new TestMappingBuilder("entity.HL7MappingIDEXX", service).build();
    }
}
