/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.rosterArea</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestRosterAreaBuilder extends AbstractTestEntityBuilder<Entity, TestScheduleBuilder> {

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The schedules in the roster.
     */
    private Entity[] schedules;

    /**
     * Constructs a {@link TestRosterAreaBuilder}.
     *
     * @param service the archetype service
     */
    public TestRosterAreaBuilder(ArchetypeService service) {
        super(ScheduleArchetypes.ROSTER_AREA, Entity.class, service);
        name(ValueStrategy.random("zrosterarea"));
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public TestRosterAreaBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the schedules.
     *
     * @param schedules the schedules
     * @return this
     */
    public TestRosterAreaBuilder schedules(Entity... schedules) {
        this.schedules = schedules;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
    */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (schedules != null) {
            for (Entity schedule : schedules) {
                bean.addTarget("schedules", schedule);
            }
        }
    }
}
