/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Builds <em>act.customerAccountPayment</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestPaymentBuilder extends AbstractTestPaymentRefundBuilder<TestPaymentBuilder> {

    /**
     * Constructs a {@link TestPaymentBuilder}.
     *
     * @param service the archetype service
     */
    public TestPaymentBuilder(ArchetypeService service) {
        super(CustomerAccountArchetypes.PAYMENT, service);
    }

    /**
     * Constructs a {@link TestPaymentBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestPaymentBuilder(FinancialAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Returns a builder to add a cash item.
     *
     * @return a cash item builder
     */
    public TestCashPaymentItemBuilder cash() {
        return new TestCashPaymentItemBuilder(this, getService());
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cash(int amount) {
        return cash().amount(amount).add();
    }

    /**
     * Adds a cash item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cash(BigDecimal amount) {
        return cash().amount(amount).add();
    }

    /**
     * Returns a builder to add a cheque item.
     *
     * @return a cheque item builder
     */
    public TestChequePaymentItemBuilder cheque() {
        return new TestChequePaymentItemBuilder(this, getService());
    }

    /**
     * Adds a cheque item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cheque(int amount) {
        return cheque(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a cheque item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder cheque(BigDecimal amount) {
        return cheque().amount(amount).add();
    }

    /**
     * Returns a builder to add a credit item.
     *
     * @return a credit item builder
     */
    public TestCreditPaymentItemBuilder credit() {
        return new TestCreditPaymentItemBuilder(this, getService());
    }

    /**
     * Adds a credit item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder credit(int amount) {
        return credit(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a credit item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder credit(BigDecimal amount) {
        return credit().amount(amount).add();
    }


    /**
     * Returns a builder to add a discount item.
     *
     * @return a discount item builder
     */
    public TestDiscountPaymentItemBuilder discount() {
        return new TestDiscountPaymentItemBuilder(this, getService());
    }

    /**
     * Adds a discount item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder discount(int amount) {
        return discount(BigDecimal.valueOf(amount));
    }

    /**
     * Adds a discount item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder discount(BigDecimal amount) {
        return discount().amount(amount).add();
    }

    /**
     * Returns a builder to add an EFT item.
     *
     * @return an EFT item builder
     */
    @Override
    @SuppressWarnings("unchecked")
    public TestEFTPaymentItemBuilder eft() {
        return new TestEFTPaymentItemBuilder(this, getService());
    }

    /**
     * Returns a builder to add an 'other' item.
     *
     * @return an 'other' item builder
     */
    public TestOtherPaymentItemBuilder other() {
        return new TestOtherPaymentItemBuilder(this, getService());
    }

    /**
     * Adds an 'other' item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder other(int amount) {
        return other(BigDecimal.valueOf(amount));
    }

    /**
     * Adds an 'other' item.
     *
     * @param amount the amount
     * @return this
     */
    public TestPaymentBuilder other(BigDecimal amount) {
        return other().amount(amount).add();
    }
}
