/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.lookup;

import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder for currency lookups, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCurrencyBuilder extends AbstractTestLookupBuilder<TestCurrencyBuilder> {

    /**
     * The minimum denomination.
     */
    private BigDecimal minDenomination = new BigDecimal("0.05");

    /**
     * The minimum price.
     */
    private BigDecimal minPrice = null;

    /**
     * Constructs a {@link TestCurrencyBuilder}.
     *
     * @param service the archetype service
     */
    public TestCurrencyBuilder(ArchetypeService service) {
        super(Currencies.LOOKUP, service);
    }

    /**
     * Sets the minimum denomination.
     * <p/>
     * Defaults to {@code 0.05} if none set.
     *
     * @param minDenomination the minimum denomination
     * @return this
     */
    public TestCurrencyBuilder minDenomination(BigDecimal minDenomination) {
        this.minDenomination = minDenomination;
        return this;
    }

    /**
     * Sets the minimum price.
     * <p/>
     * Defaults to {@code null}.
     *
     * @param minPrice the minimum price
     * @return this
     */
    public TestCurrencyBuilder minPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Lookup object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setValue("minDenomination", minDenomination);
        bean.setValue("minPrice", minPrice);
    }
}
