/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer;

import org.openvpms.archetype.test.builder.customer.alert.TestCustomerAlertBuilder;
import org.openvpms.archetype.test.builder.customer.communication.TestEmailCommunicationBuilder;
import org.openvpms.archetype.test.builder.customer.communication.TestMailCommunicationBuilder;
import org.openvpms.archetype.test.builder.customer.communication.TestNoteCommunicationBuilder;
import org.openvpms.archetype.test.builder.customer.communication.TestPhoneCommunicationBuilder;
import org.openvpms.archetype.test.builder.customer.document.TestCustomerAttachmentBuilder;
import org.openvpms.archetype.test.builder.customer.document.TestCustomerFormBuilder;
import org.openvpms.archetype.test.builder.customer.document.TestCustomerLetterBuilder;
import org.openvpms.archetype.test.builder.customer.order.TestPharmacyOrderBuilder;
import org.openvpms.archetype.test.builder.customer.order.TestPharmacyReturnBuilder;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.lookup.TestCustomerAccountTypeBuilder;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating test customers.
 *
 * @author Tim Anderson
 */
public class TestCustomerFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * Constructs a {@link TestCustomerFactory}.
     *
     * @param service         the service
     * @param documentFactory the document factory
     */
    public TestCustomerFactory(ArchetypeService service, TestDocumentFactory documentFactory) {
        this.service = service;
        this.documentFactory = documentFactory;
    }

    /**
     * Creates and saves a customer.
     *
     * @return the customer
     */
    public Party createCustomer() {
        return newCustomer().build();
    }

    /**
     * Creates and saves a customer.
     *
     * @param firstName the customer's first name
     * @param lastName  the customer's last name
     * @return the customer
     */
    public Party createCustomer(String firstName, String lastName) {
        return newCustomer().name(firstName, lastName).build();
    }

    /**
     * Creates and saves a customer.
     *
     * @param title     the customer title code
     * @param firstName the customer's first name
     * @param lastName  the customer's last name
     * @return the customer
     */
    public Party createCustomer(String title, String firstName, String lastName) {
        return newCustomer(title, firstName, lastName).build();
    }

    /**
     * Returns a builder for a new customer with the title, first and last name pre-populated.
     *
     * @param title     the customer title code
     * @param firstName the customer's first name
     * @param lastName  the customer's last name
     * @return the customer
     */
    public TestCustomerBuilder newCustomer(String title, String firstName, String lastName) {
        return newCustomer().title(title).name(firstName, lastName);
    }

    /**
     * Returns a builder for a new customer.
     *
     * @return a customer builder
     */
    public TestCustomerBuilder newCustomer() {
        return new TestCustomerBuilder(service);
    }

    /**
     * Returns a builder to update a customer.
     *
     * @param customer the customer
     * @return a customer builder
     */
    public TestCustomerBuilder updateCustomer(Party customer) {
        return new TestCustomerBuilder(customer, service);
    }

    /**
     * Returns a builder to create a customer account type.
     *
     * @return a customer account type builder
     */
    public TestCustomerAccountTypeBuilder newAccountType() {
        return new TestCustomerAccountTypeBuilder(service);
    }

    /**
     * Returns a builder to create a customer alert.
     *
     * @return a customer alert builder
     */
    public TestCustomerAlertBuilder newAlert() {
        return new TestCustomerAlertBuilder(service);
    }

    /**
     * Returns a builder to create an email communication.
     *
     * @return an email communication builder
     */
    public TestEmailCommunicationBuilder newEmailCommunication() {
        return new TestEmailCommunicationBuilder(service);
    }

    /**
     * Returns a builder to create a mail communication.
     *
     * @return a mail communication builder
     */
    public TestMailCommunicationBuilder newMailCommunication() {
        return new TestMailCommunicationBuilder(service);
    }

    /**
     * Returns a builder to create a note communication.
     *
     * @return a note communication builder
     */
    public TestNoteCommunicationBuilder newNoteCommunication() {
        return new TestNoteCommunicationBuilder(service);
    }

    /**
     * Returns a builder to create a phone communication.
     *
     * @return a phone communication builder
     */
    public TestPhoneCommunicationBuilder newPhoneCommunication() {
        return new TestPhoneCommunicationBuilder(service);
    }

    /**
     * Returns a builder to create an attachment.
     *
     * @return an attachment builder
     */
    public TestCustomerAttachmentBuilder newAttachment() {
        return new TestCustomerAttachmentBuilder(service);
    }

    /**
     * Returns a builder to update an attachment.
     *
     * @return an attachment builder
     */
    public TestCustomerAttachmentBuilder updateAttachment(DocumentAct attachment) {
        return new TestCustomerAttachmentBuilder(attachment, service);
    }

    /**
     * Returns a builder to create a form.
     *
     * @return a form builder
     */
    public TestCustomerFormBuilder newForm() {
        return new TestCustomerFormBuilder(service, documentFactory);
    }

    /**
     * Returns a builder to create a letter.
     *
     * @return a letter builder
     */
    public TestCustomerLetterBuilder newLetter() {
        return new TestCustomerLetterBuilder(service);
    }

    /**
     * Returns a builder to update a letter.
     *
     * @param letter the letter to update
     * @return a letter builder
     */
    public TestCustomerLetterBuilder updateLetter(DocumentAct letter) {
        return new TestCustomerLetterBuilder(letter, service);
    }

    /**
     * Returns a builder to create a pharmacy order.
     *
     * @return a pharmacy order builder
     */
    public TestPharmacyOrderBuilder newPharmacyOrder() {
        return new TestPharmacyOrderBuilder(service);
    }

    /**
     * Returns a builder to create a pharmacy return.
     *
     * @return a pharmacy return builder
     */
    public TestPharmacyReturnBuilder newPharmacyReturn() {
        return new TestPharmacyReturnBuilder(service);
    }
}
