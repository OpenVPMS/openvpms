/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.message;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.*Message</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestMessageBuilder<B extends AbstractTestMessageBuilder<B>>
        extends AbstractTestActBuilder<Act, B> {

    /**
     * The user the message is for.
     */
    private User to;

    /**
     * The message.
     */
    private ValueStrategy message = ValueStrategy.unset();

    /**
     * Constructs an {@link AbstractTestMessageBuilder}.
     *
     * @param archetype the message archetype
     * @param service   the archetype service
     */
    public AbstractTestMessageBuilder(String archetype, ArchetypeService service) {
        super(archetype, Act.class, service);
    }

    /**
     * Constructs an {@link AbstractTestMessageBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestMessageBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the user the message is to.
     *
     * @param to the to-user
     * @return this
     */
    public B to(User to) {
        this.to = to;
        return getThis();
    }

    /**
     * Sets the subject.
     *
     * @param subject the subject
     * @return this
     */
    public B subject(String subject) {
        return description(subject);
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @return this
     */
    public B message(String message) {
        this.message = ValueStrategy.value(message);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (to != null) {
            bean.setTarget("to", to);
        }
        message.setValue(bean, "message");
    }
}
