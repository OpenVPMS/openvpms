/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * A builder of <em>act.customerAccount*</em> acts that have no child acts.
 *
 * @author Tim Anderson
 */
public class AbstractTestSingleAccountActBuilder<B extends AbstractTestSingleAccountActBuilder<B>>
        extends AbstractTestCustomerAccountActBuilder<B> {

    /**
     * The amount.
     */
    private ValueStrategy amount = ValueStrategy.unset();

    /**
     * The tax amount.
     */
    private ValueStrategy tax = ValueStrategy.unset();

    public AbstractTestSingleAccountActBuilder(String archetype, ArchetypeService service) {
        super(archetype, service);
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     * @return this
     */
    public B amount(int amount) {
        return amount(BigDecimal.valueOf(amount));
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     * @return this
     */
    public B amount(BigDecimal amount) {
        this.amount = ValueStrategy.value(amount);
        return getThis();
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(int tax) {
        return tax(BigDecimal.valueOf(tax));
    }

    /**
     * Sets the tax amount.
     *
     * @param tax the tax amount
     * @return this
     */
    public B tax(BigDecimal tax) {
        this.tax = ValueStrategy.value(tax);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        amount.setValue(bean, "amount");
        tax.setValue(bean, "tax");
    }
}
