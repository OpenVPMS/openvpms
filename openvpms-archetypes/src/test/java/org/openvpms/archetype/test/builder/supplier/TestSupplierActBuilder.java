/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.supplierOrder</em>, <em>act.supplierDelivery</em> and <em>act.supplierReturn</em> instances,
 * for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestSupplierActBuilder<B extends TestSupplierActBuilder<B, I>,
        I extends TestSupplierActItemBuilder<B, I>>
        extends AbstractTestActBuilder<FinancialAct, B> {
    /**
     * The items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The built items.
     */
    private List<FinancialAct> builtItems = new ArrayList<>();

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Constructs a {@link TestSupplierActBuilder}.
     *
     * @param archetype the supplier act archetype
     * @param service   the archetype service
     */
    public TestSupplierActBuilder(String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
    }

    /**
     * Sets the supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public B supplier(Party supplier) {
        this.supplier = supplier;
        return getThis();
    }

    /**
     * Sets the stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public B stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return getThis();
    }

    /**
     * Returns a builder to add an item.
     *
     * @return an item builder
     */
    public abstract I item();

    /**
     * Adds an item.
     *
     * @param item the item
     * @return this
     */
    public B add(FinancialAct item) {
        items.add(item);
        return getThis();
    }

    /**
     * Returns the built items.
     *
     * @return the built items
     */
    public List<FinancialAct> getItems() {
        return builtItems;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (supplier != null) {
            bean.setTarget("supplier", supplier);
        }
        if (stockLocation != null) {
            bean.setTarget("stockLocation", stockLocation);
        }
        BigDecimal tax = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;
        for (FinancialAct item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            item.addActRelationship(relationship);
            toSave.add(item);
            tax = tax.add(item.getTaxAmount());
            total = total.add(item.getTotal());
        }
        object.setTotal(tax);
        object.setTotal(total);
        builtItems = new ArrayList<>(items);
        items.clear(); // can't reuse
    }
}
