/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.letterhead</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLetterheadBuilder extends AbstractTestEntityBuilder<Entity, TestLetterheadBuilder> {

    /**
     * The logo.
     */
    private Document logo;

    /**
     * The logo file path.
     */
    private ValueStrategy logoFile = ValueStrategy.unset();

    /**
     * The practice or location to source contacts from.
     */
    private Party contacts;

    /**
     * The subreport.
     */
    private ValueStrategy subreport = ValueStrategy.unset();

    /**
     * The built logo.
     */
    private DocumentAct logoAct;

    /**
     * Constructs a {@link TestLetterheadBuilder}.
     *
     * @param service the archetype service
     */
    public TestLetterheadBuilder(ArchetypeService service) {
        super(DocumentArchetypes.LETTERHEAD, Entity.class, service);
        name(ValueStrategy.random());
    }

    /**
     * Constructs a {@link TestLetterheadBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestLetterheadBuilder(Entity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the logo.
     *
     * @param logo the logo
     * @return this
     */
    public TestLetterheadBuilder logo(Document logo) {
        this.logo = logo;
        return this;
    }

    /**
     * Sets the logo file path.
     *
     * @param logoFile the logo file path
     * @return this
     */
    public TestLetterheadBuilder logoFile(String logoFile) {
        this.logoFile = ValueStrategy.value(logoFile);
        return this;
    }

    /**
     * Sets the practice or location to source contacts from.
     *
     * @param contacts the contacts practice/location
     * @return this
     */
    public TestLetterheadBuilder contacts(Party contacts) {
        this.contacts = contacts;
        return this;
    }

    /**
     * Sets the subreport.
     *
     * @param subreport the subreport
     * @return this
     */
    public TestLetterheadBuilder subreport(String subreport) {
        this.subreport = ValueStrategy.value(subreport);
        return this;
    }

    /**
     * Returns the built logo.
     *
     * @return the built logo, or {@code null} if none was built
     */
    public DocumentAct getLogo() {
        return logoAct;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        logoAct = null;
        if (logo != null) {
            logoAct = createLogo(object);
            toSave.add(logoAct);
            toSave.add(logo);
        }
        logoFile.setValue(bean, "logoFile");
        if (contacts != null) {
            bean.setTarget("contacts", contacts);
        }
        subreport.setValue(bean, "subreport");
    }

    /**
     * Creates a <em>act.documentLogo</em> and associates it with the letterhead.
     *
     * @param letterhead the letterhead
     * @return the logo
     */
    private DocumentAct createLogo(Entity letterhead) {
        return new TestLogoBuilder(getService())
                .logo(logo)
                .owner(letterhead)
                .build(false);
    }
}