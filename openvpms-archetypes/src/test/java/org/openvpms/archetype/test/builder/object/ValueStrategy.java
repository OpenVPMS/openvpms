/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.object;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;

/**
 * Value strategy.
 *
 * @author Tim Anderson
 */
public class ValueStrategy {

    /**
     * The value.
     */
    private final Object value;

    /**
     * If {@code true}, the value is a prefix for a randomly generated string.
     */
    private final boolean prefix;

    /**
     * A suffix to append.
     */
    private final String suffix;

    /**
     * The maximum length, for randomly generated strings.
     */
    private final int maxLength;

    /**
     * Indicates that a node shouldn't be assigned a value.
     */
    private static final ValueStrategy UNSET = new ValueStrategy(null, false, null, -1);

    /**
     * Constructs a {@link ValueStrategy}.
     *
     * @param value     the value
     * @param prefix    if {@code true}, the value is a prefix for a randomly generated string
     * @param suffix    a suffix to append to the value. May be {@code null}
     * @param maxLength the maximum length, for randomly generated strings
     */
    private ValueStrategy(Object value, boolean prefix, String suffix, int maxLength) {
        this.value = value;
        this.prefix = prefix;
        this.suffix = suffix;
        this.maxLength = maxLength;
    }

    /**
     * Returns the value.
     *
     * @return the value. May be {@code null}
     */
    public Object getValue() {
        Object result = value;
        if (prefix || suffix != null) {
            String str;
            if (prefix) {
                if (value != null) {
                    str = TestHelper.randomName(value.toString());
                } else {
                    str = TestHelper.randomName("");
                }
            } else {
                str = (value != null) ? value.toString() : "";
            }
            if (maxLength > 0) {
                int length = maxLength;
                if (suffix != null) {
                    length = length - suffix.length();
                }
                str = StringUtils.truncate(str, length);
            }
            if (suffix != null) {
                str += suffix;
            }
            result = str;
        }
        return result;
    }

    /**
     * Returns a string version of the value.
     * <p/>
     * For random values, this returns a different result each time.
     *
     * @return a string version of the value
     */
    @Override
    public String toString() {
        Object value = getValue();
        return (value != null) ? value.toString() : null;
    }

    /**
     * Populates the named node with the value generated according to the strategy.
     * <p/>
     * The one exception is the {@link #unset()} strategy, which leaves the node unchanged.
     *
     * @param bean the bean to populate
     * @param name the node name
     * @return the value. May be {@code null}
     */
    public Object setValue(IMObjectBean bean, String name) {
        Object value;
        if (isSet()) {
            value = getValue();
            bean.setValue(name, value);
        } else if (bean.hasNode(name)) {
            value = bean.getValue(name);
        } else {
            value = null;
        }
        return value;
    }

    /**
     * Resets a node to its default value.
     *
     * @param bean the bean to populate
     * @param name the node name
     * @return the value. May be {@code null}
     */
    public Object reset(IMObjectBean bean, String name) {
        Object value = bean.getDefaultValue(name);
        bean.setValue(name, value);
        return value;
    }

    /**
     * Determines if the node should be populated.
     *
     * @return {@code true} if the node should be populated, otherwise {@code false}
     */
    public boolean isSet() {
        return !isUnset();
    }

    /**
     * Determines if the node should not be populated.
     *
     * @return {@code true} if the node should not be populated, {@code false} if it should
     */
    public boolean isUnset() {
        return this == UNSET;
    }

    /**
     * Creates a strategy to indicate that the node should not be populated.
     *
     * @return the strategy
     */
    public static ValueStrategy unset() {
        return UNSET;
    }

    /**
     * Determines if the default value should be used.
     *
     * @return {@code true} if the default value should be used
     * @deprecated use {@link #isUnset}
     */
    @Deprecated
    public boolean useDefault() {
        return isUnset();
    }

    /**
     * Creates a strategy to indicate that the node should not be populated.
     *
     * @return the strategy
     * @deprecated use {@link #unset}
     */
    @Deprecated
    public static ValueStrategy defaultValue() {
        return unset();
    }

    /**
     * Creates a strategy to populate a node with a value.
     *
     * @param value the value. May be {@code null}
     * @return the strategy
     */
    public static ValueStrategy value(Object value) {
        return new ValueStrategy(value, false, null, -1);
    }

    /**
     * Creates a strategy to populate a node with a random value.
     *
     * @return the strategy
     */
    public static ValueStrategy random() {
        return random("");
    }

    /**
     * Creates a strategy to populate a node with a random value, with the specified prefix.
     *
     * @param prefix the prefix
     * @return the strategy
     */
    public static ValueStrategy random(String prefix) {
        return random(prefix, null);
    }

    /**
     * Creates a strategy to populate a node with a random value, with optional prefix and suffix.
     *
     * @param prefix the prefix. May be {@code null}
     * @param suffix the suffix. May be {@code null}
     * @return the strategy
     */
    public static ValueStrategy random(String prefix, String suffix) {
        return new ValueStrategy(prefix != null ? prefix : "", true, suffix, -1);
    }

    /**
     * Creates a strategy to populate a node with a random value with the specified suffix.
     *
     * @param suffix the suffix
     * @return the strategy
     */
    public static ValueStrategy suffix(String suffix) {
        return new ValueStrategy("", true, suffix, -1);
    }

    /**
     * Creates a strategy to populate a node with a random value, with the specified prefix, up to a maximum length.
     *
     * @param prefix    the prefix
     * @param maxLength the maximum length of the resulting string
     * @return the strategy
     */
    public static ValueStrategy random(String prefix, int maxLength) {
        return new ValueStrategy(prefix, true, null, maxLength);
    }

    /**
     * Helper to generate a random string.
     *
     * @return a random string
     */
    public static String randomString() {
        return ValueStrategy.random().toString();
    }

    /**
     * Helper to generate a random string with a prefix.
     *
     * @return a random string
     */
    public static String randomString(String prefix) {
        return ValueStrategy.random(prefix).toString();
    }

}
