/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.documentLogo</em> acts.
 *
 * @author Tim Anderson
 */
public class TestLogoBuilder extends AbstractTestActBuilder<DocumentAct, TestLogoBuilder> {

    /**
     * Determines if the existing <em>act.documentLogo</em> should be updated.
     */
    private final boolean update;

    /**
     * The logo.
     */
    private Document logo;

    /**
     * The owner.
     */
    private Entity owner;

    /**
     * Constructs a {@link TestLogoBuilder}.
     *
     * @param service the archetype service
     */
    public TestLogoBuilder(ArchetypeService service) {
        this(true, service);
    }

    /**
     * Constructs a {@link TestLogoBuilder}.
     *
     * @param update  if {@code true}, update the existing <em>act.documentLogo</em> if present
     * @param service the archetype service
     */
    public TestLogoBuilder(boolean update, ArchetypeService service) {
        super(DocumentArchetypes.LOGO_ACT, DocumentAct.class, service);
        this.update = update;
    }

    /**
     * Sets the logo.
     *
     * @param logo the logo
     * @return this
     */
    public TestLogoBuilder logo(Document logo) {
        this.logo = logo;
        return this;
    }

    /**
     * Sets the logo owner.
     *
     * @param owner the owner
     * @return this
     */
    public TestLogoBuilder owner(Entity owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Returns the existing object, if it is being updated.
     *
     * @return the existing object, or {@code null} if a new object is being created
     */
    @Override
    protected DocumentAct getExisting() {
        DocumentAct result = super.getExisting();
        if (result == null && update) {
            DocumentRules rules = new DocumentRules(getService());
            result = rules.getLogo(owner);
        }
        return result;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        object.setFileName(logo.getName());
        object.setMimeType(logo.getMimeType());
        object.setDocument(logo.getObjectReference());
        bean.setTarget("owner", owner);
    }
}