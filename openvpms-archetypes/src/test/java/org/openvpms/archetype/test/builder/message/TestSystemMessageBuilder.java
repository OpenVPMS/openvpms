/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.message;

import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>act.systemMessage</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSystemMessageBuilder extends AbstractTestMessageBuilder<TestSystemMessageBuilder> {

    /**
     * Constructs a {@link TestSystemMessageBuilder}.
     *
     * @param service the archetype service
     */
    public TestSystemMessageBuilder(ArchetypeService service) {
        super(MessageArchetypes.SYSTEM, service);
    }

    /**
     * Constructs a {@link TestSystemMessageBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestSystemMessageBuilder(Act object, ArchetypeService service) {
        super(object, service);
    }

}
