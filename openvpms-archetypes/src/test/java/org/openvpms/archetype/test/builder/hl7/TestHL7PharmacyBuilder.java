/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.hl7;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.HL7ServicePharmacy</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestHL7PharmacyBuilder extends AbstractTestEntityBuilder<Entity, TestHL7PharmacyBuilder> {

    /**
     * The one-way flag.
     */
    private ValueStrategy oneway = ValueStrategy.unset();

    /**
     * The order connector.
     */
    private Entity sender;

    /**
     * The dispense connector.
     */
    private Entity receiver;

    /**
     * The practice location this pharmacy is used at.
     */
    private Party location;

    /**
     * The user.
     */
    private User user;

    /**
     * The default application.
     */
    private static final String DEFAULT_APP = "OpenVPMS";

    /**
     * The default facility.
     */
    private static final String DEFAULT_FACILITY = "MainClinic";

    /**
     * The default pharmacy application.
     */
    private static final String PHARMACY_APP = "PHARMAPP";

    /**
     * The default pharmacy facility.
     */
    private static final String PHARMACY_FACILITY = "PHARMFACILITY";

    /**
     * Constructs an {@link TestHL7PharmacyBuilder}.
     *
     * @param service the archetype service
     */
    public TestHL7PharmacyBuilder(ArchetypeService service) {
        super("entity.HL7ServicePharmacy", Entity.class, service);
        name(ValueStrategy.random("zhl7"));
    }

    /**
     * Indicates that the pharmacy is one-way, i.e. no dispense messages will be generated from orders.
     *
     * @return this
     */
    public TestHL7PharmacyBuilder oneway() {
        return oneway(true);
    }

    /**
     * Determines if the pharmacy is one-way or not. A one-way pharmacy will not generate dispense messages from orders.
     *
     * @param oneway if {@code true}, the pharmacy is one-way, else it is two-way
     * @return this
     */
    public TestHL7PharmacyBuilder oneway(boolean oneway) {
        this.oneway = ValueStrategy.value(oneway);
        return this;
    }

    /**
     * Populates a default sender and receiver.
     *
     * @return this
     */
    public TestHL7PharmacyBuilder defaultSenderReceiver() {
        ArchetypeService service = getService();
        TestHL7Factory hl7Factory = new TestHL7Factory(service);
        Entity mapping = hl7Factory.getCubexMapping();
        sender(createDefaultSender(hl7Factory, mapping));
        return receiver(createReceiver(hl7Factory, mapping));
    }

    /**
     * Populates a default sender.
     *
     * @return this
     */
    public TestHL7PharmacyBuilder defaultSender() {
        ArchetypeService service = getService();
        TestHL7Factory hl7Factory = new TestHL7Factory(service);
        Entity mapping = hl7Factory.getCubexMapping();
        return sender(createDefaultSender(hl7Factory, mapping));
    }

    /**
     * Populates a default receiver.
     *
     * @return this
     */
    public TestHL7PharmacyBuilder defaultReceiver() {
        ArchetypeService service = getService();
        TestHL7Factory hl7Factory = new TestHL7Factory(service);
        Entity mapping = hl7Factory.getCubexMapping();
        return receiver(createReceiver(hl7Factory, mapping));
    }

    /**
     * Sets the sender. This is the connector used for sending pharmacy orders.
     *
     * @param sender the sender. An <em>entity.HL7SenderMLLP</em>
     * @return this
     */
    public TestHL7PharmacyBuilder sender(Entity sender) {
        this.sender = sender;
        return this;
    }

    /**
     * Sets the receiver. This is the connector used for receiving dispense messages.
     *
     * @param receiver the receiver. An <em>entity.HL7ReceiverMLLP</em>
     * @return this
     */
    public TestHL7PharmacyBuilder receiver(Entity receiver) {
        this.receiver = receiver;
        return this;
    }

    /**
     * Sets the location where the pharmacy may be used.
     *
     * @param location the location
     * @return this
     */
    public TestHL7PharmacyBuilder location(Party location) {
        this.location = location;
        return this;
    }

    /**
     * Sets the user.
     *
     * @param user the user
     * @return this
     */
    public TestHL7PharmacyBuilder user(User user) {
        this.user = user;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        oneway.setValue(bean, "oneway");
        if (sender != null) {
            bean.setTarget("sender", sender);
        }
        if (receiver != null) {
            bean.setTarget("receiver", receiver);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (user != null) {
            bean.setTarget("user", user);
        }
    }

    /**
     * Creates a default sender.
     *
     * @param hl7Factory the HL7 factory
     * @param mapping    the mapping to use
     * @return a new default sender
     */
    private Entity createDefaultSender(TestHL7Factory hl7Factory, Entity mapping) {
        return hl7Factory.newSender()
                .port(0)
                .sendingApplication(DEFAULT_APP)
                .sendingFacility(DEFAULT_FACILITY)
                .receivingApplication(PHARMACY_APP)
                .receivingFacility(PHARMACY_FACILITY)
                .mapping(mapping)
                .build();
    }

    /**
     * Creates a default receiver.
     *
     * @param hl7Factory the HL7 factory
     * @param mapping    the mapping to use
     * @return a new default receiver
     */
    private Entity createReceiver(TestHL7Factory hl7Factory, Entity mapping) {
        return hl7Factory.newReceiver()
                .port(0)
                .sendingApplication(PHARMACY_APP)
                .sendingFacility(PHARMACY_FACILITY)
                .receivingApplication(DEFAULT_APP)
                .receivingFacility(DEFAULT_FACILITY)
                .mapping(mapping)
                .build();
    }
}