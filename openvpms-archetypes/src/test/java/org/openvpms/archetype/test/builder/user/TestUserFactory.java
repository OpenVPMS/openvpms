/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.user;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating users.
 *
 * @author Tim Anderson
 */
public class TestUserFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * Constructs a {@link TestUserFactory}.
     *
     * @param service         the service
     * @param userRules       the user rules
     * @param documentFactory the document factory
     */
    public TestUserFactory(ArchetypeService service, UserRules userRules, TestDocumentFactory documentFactory) {
        this.service = service;
        this.userRules = userRules;
        this.documentFactory = documentFactory;
    }

    /**
     * Creates and saves a user.
     *
     * @return the user
     */
    public User createUser() {
        return newUser().build();
    }

    /**
     * Creates and saves a user.
     *
     * @param username the user name
     * @return the user
     */
    public User createUser(String username) {
        return newUser().username(username).build();
    }

    /**
     * Creates and saves a user.
     *
     * @param firstName the user's first name
     * @param lastName  the user's last name
     * @return the user
     */
    public User createUser(String firstName, String lastName) {
        return newUser().firstName(firstName).lastName(lastName).build();
    }

    /**
     * Creates and saves a user with a first and last name populated.
     *
     * @return the user
     */
    public User createEmployee() {
        return newUser().firstName(ValueStrategy.randomString()).lastName(ValueStrategy.randomString()).build();
    }

    /**
     * Creates and saves a clinician.
     *
     * @return the clinician
     */
    public User createClinician() {
        return newUser().clinician().build();
    }

    /**
     * Creates and saves a clinician.
     *
     * @param firstName the clinician's first name
     * @param lastName  the clinician's last name
     * @return the clinician
     */
    public User createClinician(String firstName, String lastName) {
        return newUser().firstName(firstName).lastName(lastName).clinician().build();
    }

    /**
     * Creates and saves a clinician.
     *
     * @param title     the clinician title code
     * @param firstName the clinician's first name
     * @param lastName  the clinician's last name
     * @return the clinician
     */
    public User createClinician(String title, String firstName, String lastName) {
        return newUser().title(title).firstName(firstName).lastName(lastName).clinician().build();
    }

    /**
     * Creates and saves an administrator.
     *
     * @return the administrator
     */
    public User createAdministrator() {
        return newUser().administrator().build();
    }

    /**
     * Returns a builder for a new user.
     *
     * @return a user builder
     */
    public TestUserBuilder newUser() {
        return new TestUserBuilder(service, userRules, documentFactory);
    }

    /**
     * Returns a builder to update a user.
     *
     * @param user the user
     * @return a user builder
     */
    public TestUserBuilder updateUser(User user) {
        return new TestUserBuilder(user, service, userRules, documentFactory);
    }

    /**
     * Creates a role.
     *
     * @param authorities the role authorities
     * @return a new role
     */
    public SecurityRole createRole(ArchetypeAwareGrantedAuthority... authorities) {
        return newRole().authorities(authorities).build();
    }

    /**
     * Returns a builder to create a role.
     *
     * @return a role builder
     */
    public TestRoleBuilder newRole() {
        return new TestRoleBuilder(service);
    }

    /**
     * Creates an archetype service authority.
     *
     * @param method    the method
     * @param archetype the archetype
     * @return a new authority
     */
    public ArchetypeAwareGrantedAuthority createAuthority(String method, String archetype) {
        return newAuthority()
                .method(method)
                .archetype(archetype)
                .build();
    }

    /**
     * Returns a builder to create an authority.
     *
     * @return an authority builder
     */
    public TestAuthorityBuilder newAuthority() {
        return new TestAuthorityBuilder(service);
    }

}
