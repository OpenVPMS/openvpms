/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.job.account;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestSMSTemplateBuilder;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Builder for <em>entity.accountReminderCount</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAccountReminderCountBuilder
        extends AbstractTestEntityBuilder<Entity, TestAccountReminderCountBuilder> {

    /**
     * The parent builder.
     */
    private final TestAccountReminderJobBuilder parent;

    /**
     * The reminder interval.
     */
    private Integer interval;

    /**
     * The interval units.
     */
    private DateUnits units;

    /**
     * The SMS template.
     */
    private Entity template;

    /**
     * Constructs a {@link TestAccountReminderCountBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestAccountReminderCountBuilder(TestAccountReminderJobBuilder parent, ArchetypeService service) {
        super(AccountReminderArchetypes.ACCOUNT_REMINDER_COUNT, Entity.class, service);
        this.parent = parent;
    }

    /**
     * Sets the reminder interval.
     *
     * @param interval the interval
     * @param units    the interval units
     * @return this
     */
    public TestAccountReminderCountBuilder interval(int interval, DateUnits units) {
        this.interval = interval;
        this.units = units;
        return this;
    }

    /**
     * Sets the template.
     *
     * @param template an XPATH expression
     * @return the SMS template
     */
    public TestAccountReminderCountBuilder template(String template) {
        return template(new TestSMSTemplateBuilder(DocumentArchetypes.ACCOUNT_SMS_TEMPLATE, getService())
                                .contentType("XPATH")
                                .content(template)
                                .build());
    }

    /**
     * Sets the SMS template.
     *
     * @param template the template
     * @return this
     */
    public TestAccountReminderCountBuilder template(Entity template) {
        this.template = template;
        return this;
    }

    /**
     * Adds the count to the parent builder.
     *
     * @return the parent builder
     */
    public TestAccountReminderJobBuilder add() {
        Set<IMObject> objects = new HashSet<>();
        Entity count = build(objects, new LinkedHashSet<>());
        parent.add(count);
        parent.collect(objects);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (interval != null) {
            bean.setValue("interval", interval);
        }
        if (units != null) {
            bean.setValue("units", units.toString());
        }
        if (template != null) {
            bean.setTarget("smsTemplate", template);
            toSave.add(template);
        }
    }
}