/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>contact.location</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLocationContactBuilder<T extends Party, P extends AbstractTestPartyBuilder<T, P>>
        extends AbstractTestContactBuilder<T, P, TestLocationContactBuilder<T, P>> {

    /**
     * The street address.
     */
    private ValueStrategy address = ValueStrategy.unset();

    /**
     * The suburb code.
     */
    private ValueStrategy suburbCode = ValueStrategy.unset();

    /**
     * The suburb name.
     */
    private ValueStrategy suburbName = ValueStrategy.unset();

    /**
     * The state code.
     */
    private ValueStrategy stateCode = ValueStrategy.unset();

    /**
     * The state name.
     */
    private ValueStrategy stateName = ValueStrategy.unset();

    /**
     * The postcode.
     */
    private ValueStrategy postcode = ValueStrategy.unset();

    /**
     * Constructs a {@link TestLocationContactBuilder}.
     *
     * @param service the archetype service
     */
    public TestLocationContactBuilder(ArchetypeService service) {
        super(ContactArchetypes.LOCATION, service);
    }

    /**
     * Constructs a {@link TestLocationContactBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestLocationContactBuilder(P parent, ArchetypeService service) {
        super(parent, ContactArchetypes.LOCATION, service);
    }

    /**
     * Constructs a {@link TestLocationContactBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestLocationContactBuilder(Contact object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the street address.
     *
     * @param address the street address
     * @return this
     */
    public TestLocationContactBuilder<T, P> address(String address) {
        this.address = ValueStrategy.value(address);
        return this;
    }

    /**
     * Sets the suburb code and name.
     *
     * @param code the suburb code
     * @param name the suburb name
     * @return this
     */
    public TestLocationContactBuilder<T, P> suburb(String code, String name) {
        suburbCode(code);
        return suburbName(name);
    }

    /**
     * Sets the suburb code.
     *
     * @param suburbCode the suburb code
     * @return this
     */
    public TestLocationContactBuilder<T, P> suburbCode(String suburbCode) {
        this.suburbCode = ValueStrategy.value(suburbCode);
        return this;
    }

    /**
     * Sets the suburb name.
     *
     * @param suburbName the suburb name
     * @return this
     */
    public TestLocationContactBuilder<T, P> suburbName(String suburbName) {
        this.suburbName = ValueStrategy.value(suburbName);
        return this;
    }

    /**
     * Sets the state code and name.
     *
     * @param stateCode the state code
     * @param stateName the state name
     * @return this
     */
    public TestLocationContactBuilder<T, P> state(String stateCode, String stateName) {
        stateCode(stateCode);
        return stateName(stateName);
    }

    /**
     * Sets the state code.
     *
     * @param stateCode the state code
     * @return this
     */
    public TestLocationContactBuilder<T, P> stateCode(String stateCode) {
        this.stateCode = ValueStrategy.value(stateCode);
        return this;
    }

    /**
     * Sets the state name.
     *
     * @param stateName the state name
     * @return this
     */
    public TestLocationContactBuilder<T, P> stateName(String stateName) {
        this.stateName = ValueStrategy.value(stateName);
        return this;
    }

    /**
     * Sets the postcode.
     *
     * @param postcode the postcode
     * @return this
     */
    public TestLocationContactBuilder<T, P> postcode(String postcode) {
        this.postcode = ValueStrategy.value(postcode);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Contact object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        Lookup state = null;
        Lookup suburb = null;
        if (stateCode.isSet()) {
            state = new TestLookupBuilder(ContactArchetypes.STATE, getService())
                    .code(stateCode)
                    .name(stateName).build();
        }
        if (suburbCode.isSet()) {
            TestLookupBuilder builder = new TestLookupBuilder(ContactArchetypes.SUBURB, getService())
                    .code(suburbCode)
                    .name(suburbName);
            if (state != null) {
                builder.source(state);
            }
            suburb = builder.build();
            if (state == null && stateCode.isUnset()) {
                state = getBean(suburb).getSource("target", Lookup.class);
                if (state != null) {
                    stateCode = ValueStrategy.value(state.getCode());
                }
            }
        }
        address.setValue(bean, "address");
        suburbCode.setValue(bean, "suburb");
        stateCode.setValue(bean, "state");

        if (postcode.isUnset() && suburb != null) {
            String code = getBean(suburb).getString("postCode");
            if (code != null) {
                postcode = ValueStrategy.value(code);
            }
        }
        postcode.setValue(bean, "postcode");
        super.build(object, bean, toSave, toRemove);
    }
}
