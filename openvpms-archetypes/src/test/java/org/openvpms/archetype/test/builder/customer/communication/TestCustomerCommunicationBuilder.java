/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.communication;

import org.openvpms.archetype.test.builder.act.InplaceLongTextBuilder;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerActBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder of <em>act.customerCommunication*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestCustomerCommunicationBuilder<B extends TestCustomerCommunicationBuilder<B>>
        extends AbstractTestCustomerActBuilder<DocumentAct, B> {

    /**
     * The message builder.
     */
    private final InplaceLongTextBuilder messageBuilder;

    /**
     * The message.
     */
    private ValueStrategy message = ValueStrategy.unset();

    /**
     * Constructs a {@link TestCustomerCommunicationBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestCustomerCommunicationBuilder(String archetype, ArchetypeService service) {
        super(archetype, DocumentAct.class, service);
        messageBuilder = new InplaceLongTextBuilder("message", service);
    }

    /**
     * Sets the message.
     *
     * @param message the message
     * @return this
     */
    public B message(String message) {
        this.message = ValueStrategy.value(message);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        messageBuilder.build(message, object, bean, toSave, toRemove);
    }
}
