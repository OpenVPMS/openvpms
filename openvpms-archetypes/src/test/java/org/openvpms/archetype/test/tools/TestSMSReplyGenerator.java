/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.tools;

import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.Iterator;

/**
 * Tool to generate SMS replies for all SMS with SEND and DELIVERED status that don't hove one.
 *
 * @author Tim Anderson
 */
public class TestSMSReplyGenerator {

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String contextPath = "openvpms-archetypes-test-context.xml";
        ApplicationContext context;
        if (!new File(contextPath).exists()) {
            context = new ClassPathXmlApplicationContext(contextPath);
        } else {
            context = new FileSystemXmlApplicationContext(contextPath);
        }

        ArchetypeService archetypeService = context.getBean(IArchetypeRuleService.class);
        TestSMSFactory smsFactory = context.getBean(TestSMSFactory.class);
        CriteriaBuilder builder = archetypeService.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, "act.smsMessage");
        query.where(from.get("status").in("SENT", "DELIVERED"));
        query.orderBy(builder.asc(from.get("id")));
        int count = 0;
        Iterator<Act> iterator = new TypedQueryIterator<>(archetypeService.createQuery(query), 50);
        while (iterator.hasNext()) {
            Act sms = iterator.next();
            IMObjectBean bean = archetypeService.getBean(sms);
            if (bean.getValues("replies").isEmpty()) {
                smsFactory.updateSMS(sms)
                        .addReply("this is a reply")
                        .build();
                count++;
            }
        }
        System.out.println("Added " + count + " replies");
    }
}