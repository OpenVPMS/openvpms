/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>act.patientDocumentAttachment</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAttachmentBuilder extends TestPatientDocumentBuilder<TestAttachmentBuilder> {

    /**
     * Constructs a {@link TestAttachmentBuilder}.
     *
     * @param service the archetype service
     */
    public TestAttachmentBuilder(ArchetypeService service) {
        super(PatientArchetypes.DOCUMENT_ATTACHMENT, service);
    }

    /**
     * Constructs a {@link TestAttachmentBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestAttachmentBuilder(DocumentAct object, ArchetypeService service) {
        super(object, service);
    }
}
