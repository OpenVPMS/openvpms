/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>act.patientPrescription</em> acts for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPrescriptionBuilder extends AbstractTestPatientActBuilder<Act, TestPrescriptionBuilder> {

    /**
     * The quantity.
     */
    private ValueStrategy quantity = ValueStrategy.unset();

    /**
     * The repeats.
     */
    private ValueStrategy repeats = ValueStrategy.unset();

    /**
     * The product.
     */
    private Product product;

    /**
     * The label.
     */
    private ValueStrategy label = ValueStrategy.unset();

    /**
     * Constructs a {@link TestPrescriptionBuilder}.
     *
     * @param service the archetype service
     */
    public TestPrescriptionBuilder(ArchetypeService service) {
        super(PatientArchetypes.PRESCRIPTION, Act.class, service);
    }

    /**
     * Sets the expiry date.
     *
     * @param expiryDate the expiry date
     * @return this
     */
    public TestPrescriptionBuilder expiryDate(Date expiryDate) {
        return endTime(expiryDate);
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestPrescriptionBuilder quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestPrescriptionBuilder quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return this;
    }

    /**
     * Sets the repeats.
     *
     * @param repeats the repeats
     * @return this
     */
    public TestPrescriptionBuilder repeats(int repeats) {
        this.repeats = ValueStrategy.value(repeats);
        return this;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestPrescriptionBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the label.
     *
     * @param label the label
     * @return this
     */
    public TestPrescriptionBuilder label(String label) {
        this.label = ValueStrategy.value(label);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Act object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        quantity.setValue(bean, "quantity");
        repeats.setValue(bean, "repeats");
        if (product != null) {
            bean.setTarget("product", product);
        }
        label.setValue(bean, "label");
    }
}