/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.job.appointment;

import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.test.builder.job.AbstractTestJobBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.jobAppointmentReminder</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestAppointmentReminderJobBuilder extends AbstractTestJobBuilder<TestAppointmentReminderJobBuilder> {

    /**
     * The SMS-from period.
     */
    private ValueStrategy smsFrom = ValueStrategy.unset();

    /**
     * The SMS-from period units.
     */
    private ValueStrategy smsFromUnits = ValueStrategy.unset();

    /**
     * The SMS-to period.
     */
    private ValueStrategy smsTo = ValueStrategy.unset();

    /**
     * The SMS-to period units.
     */
    private ValueStrategy smsToUnits = ValueStrategy.unset();

    /**
     * The no-reminder period.
     */
    private ValueStrategy noReminder = ValueStrategy.unset();

    /**
     * The no-reminder period units.
     */
    private ValueStrategy noReminderUnits = ValueStrategy.unset();

    /**
     * Determines if replies to appointment reminders should be processed.
     */
    private ValueStrategy processReplies = ValueStrategy.unset();

    /**
     * The reply text used to confirm appointments.
     */
    private ValueStrategy confirmAppointment = ValueStrategy.unset();

    /**
     * The reply text used to cancel appointments.
     */
    private ValueStrategy cancelAppointment = ValueStrategy.unset();

    /**
     * Constructs a {@link TestAppointmentReminderJobBuilder}.
     *
     * @param service the archetype service
     */
    public TestAppointmentReminderJobBuilder(ArchetypeService service) {
        super(AppointmentRules.APPOINTMENT_REMINDER_JOB, service);
    }

    /**
     * Sets the SMS-from period.
     *
     * @param smsFrom the SMS-from period
     * @param units   the period units
     * @return this
     */
    public TestAppointmentReminderJobBuilder smsFrom(int smsFrom, DateUnits units) {
        this.smsFrom = ValueStrategy.value(smsFrom);
        this.smsFromUnits = ValueStrategy.value(units);
        return this;
    }

    /**
     * Sets the SMS-to period.
     *
     * @param smsTo the SMS-to period
     * @param units the period units
     * @return this
     */
    public TestAppointmentReminderJobBuilder smsTo(int smsTo, DateUnits units) {
        this.smsTo = ValueStrategy.value(smsTo);
        this.smsToUnits = ValueStrategy.value(units);
        return this;
    }

    /**
     * Sets the no-reminder period.
     *
     * @param noReminder the no-reminder period
     * @param units      the period units
     * @return this
     */
    public TestAppointmentReminderJobBuilder noReminder(int noReminder, DateUnits units) {
        this.noReminder = ValueStrategy.value(noReminder);
        this.noReminderUnits = ValueStrategy.value(units);
        return this;
    }

    /**
     * Indicates that replies should be processed.
     *
     * @return this
     */
    public TestAppointmentReminderJobBuilder processReplies() {
        return processReplies(true);
    }

    /**
     * Determines if replies should be processed.
     *
     * @return this
     */
    public TestAppointmentReminderJobBuilder processReplies(boolean processReplies) {
        this.processReplies = ValueStrategy.value(processReplies);
        return this;
    }

    /**
     * Sets the reply text used to confirm appointments.
     *
     * @param confirmAppointment the text
     * @return this
     */
    public TestAppointmentReminderJobBuilder confirmAppointment(String confirmAppointment) {
        this.confirmAppointment = ValueStrategy.value(confirmAppointment);
        return this;
    }

    /**
     * Sets the reply text used to cancel appointments.
     *
     * @param cancelAppointment the text
     * @return this
     */
    public TestAppointmentReminderJobBuilder cancelAppointment(String cancelAppointment) {
        this.cancelAppointment = ValueStrategy.value(cancelAppointment);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        smsFrom.setValue(bean, "smsFrom");
        smsFromUnits.setValue(bean, "smsFromUnits");
        smsTo.setValue(bean, "smsTo");
        smsToUnits.setValue(bean, "smsToUnits");
        noReminder.setValue(bean, "noReminder");
        noReminderUnits.setValue(bean, "noReminderUnits");
        processReplies.setValue(bean, "processReplies");
        cancelAppointment.setValue(bean, "cancelAppointment");
        confirmAppointment.setValue(bean, "confirmAppointment");
    }
}