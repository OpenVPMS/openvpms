/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.order;

import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerActBuilder;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.customerOrderPharmacy</em> and <em>act.customerReturnPharmacy</em>instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class TestPharmacyOrderReturnBuilder<B extends TestPharmacyOrderReturnBuilder<B, I>,
        I extends TestPharmacyOrderReturnItemBuilder<B, I>>
        extends AbstractTestCustomerActBuilder<FinancialAct, B> {

    /**
     * The order/return items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Constructs a {@link TestPharmacyOrderReturnBuilder}.
     *
     * @param archetype the order/return archetype
     * @param service   the archetype service
     */
    public TestPharmacyOrderReturnBuilder(String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Returns a builder to add an item.
     *
     * @return an item builder
     */
    public abstract I item();

    /**
     * Adds an order item.
     *
     * @param item the item to add
     * @return this
     */
    public B add(FinancialAct item) {
        items.add(item);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        for (FinancialAct item : items) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            item.addActRelationship(relationship);
            toSave.add(item);
        }
        items.clear(); // can't reuse
    }
}
