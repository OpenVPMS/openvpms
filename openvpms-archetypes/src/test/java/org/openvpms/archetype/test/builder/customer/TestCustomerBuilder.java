/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.customerperson</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerBuilder extends AbstractTestPartyBuilder<Party, TestCustomerBuilder> {

    /**
     * The patients.
     */
    private final List<Party> patients = new ArrayList<>();

    /**
     * The customer title code.
     */
    private ValueStrategy titleCode = ValueStrategy.unset();

    /**
     * The first name.
     */
    private ValueStrategy firstName = ValueStrategy.unset();

    /**
     * The last name.
     */
    private ValueStrategy lastName = ValueStrategy.unset();

    /**
     * The company name.
     */
    private ValueStrategy companyName = ValueStrategy.unset();

    /**
     * The referral.
     */
    private ValueStrategy referredBy = ValueStrategy.unset();

    /**
     * The referred-by customer.
     */
    private ValueStrategy referredByCustomer = ValueStrategy.unset();

    /**
     * The customer discounts.
     */
    private Entity[] discounts;

    /**
     * The <em>lookup.taxType</em> codes that the customer is exempt from.
     */
    private String[] taxExemptionCodes;

    /**
     * The customer's preferred practice location.
     */
    private Party practice;

    /**
     * Constructs a {@link TestCustomerBuilder}.
     *
     * @param service the archetype service
     */
    public TestCustomerBuilder(ArchetypeService service) {
        super(CustomerArchetypes.PERSON, Party.class, service);
        firstName("J");
        lastName(ValueStrategy.random("Smith"));
    }

    /**
     * Constructs a {@link TestCustomerBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestCustomerBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the title.
     *
     * @param titleCode the <em>lookup.title</em> code
     * @return this
     */
    public TestCustomerBuilder title(String titleCode) {
        this.titleCode = ValueStrategy.value(titleCode);
        return this;
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestCustomerBuilder firstName(String firstName) {
        return firstName(ValueStrategy.value(firstName));
    }

    /**
     * Sets the first name.
     *
     * @param firstName the first name
     * @return this
     */
    public TestCustomerBuilder firstName(ValueStrategy firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     * @return this
     */
    public TestCustomerBuilder lastName(String lastName) {
        return lastName(ValueStrategy.value(lastName));
    }

    /**
     * Sets the last name.
     *
     * @param lastName the last name
     */
    public TestCustomerBuilder lastName(ValueStrategy lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Sets the customer's name.
     *
     * @param firstName the first name
     * @param lastName  the last name
     * @return this
     */
    public TestCustomerBuilder name(String firstName, String lastName) {
        firstName(firstName);
        return lastName(lastName);
    }

    /**
     * Sets the customer's name.
     *
     * @param titleCode the title code
     * @param firstName the first name
     * @param lastName  the last name
     * @return this
     */
    public TestCustomerBuilder name(String titleCode, String firstName, String lastName) {
        name(firstName, lastName);
        return title(titleCode);
    }

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     */
    public TestCustomerBuilder companyName(String companyName) {
        this.companyName = ValueStrategy.value(companyName);
        return this;
    }

    /**
     * Sets the referred-by code.
     *
     * @param referredBy the <em>lookup.customerReferral</em> code
     * @return this
     */
    public TestCustomerBuilder referredBy(String referredBy) {
        this.referredBy = ValueStrategy.value(referredBy);
        return this;
    }

    /**
     * Sets the referred-by customer.
     *
     * @param customer the customer
     * @return this
     */
    public TestCustomerBuilder referredByCustomer(Party customer) {
        referredByCustomer = ValueStrategy.value(customer);
        return this;
    }

    /**
     * Adds a home phone.
     *
     * @param phone the phone number
     * @return this
     */
    public TestCustomerBuilder addHomePhone(String phone) {
        addPhone(phone, ContactArchetypes.HOME_PURPOSE);
        return this;
    }

    /**
     * Adds a work phone.
     *
     * @param phone the phone number
     * @return this
     */
    public TestCustomerBuilder addWorkPhone(String phone) {
        addPhone(phone, ContactArchetypes.WORK_PURPOSE);
        return this;
    }

    /**
     * Adds customer discounts.
     *
     * @param discounts the discounts
     */
    public TestCustomerBuilder addDiscounts(Entity... discounts) {
        this.discounts = discounts;
        return this;
    }

    /**
     * Adds tax exemptions.
     *
     * @param taxCodes the <em>lookup.taxType</em> codes
     * @return this
     */
    public TestCustomerBuilder addTaxExemptions(String... taxCodes) {
        this.taxExemptionCodes = taxCodes;
        return this;
    }

    /**
     * Sets the customer's preferred practice location.
     *
     * @param practice the preferred practice location.
     * @return this
     */
    public TestCustomerBuilder practice(Party practice) {
        this.practice = practice;
        return this;
    }

    /**
     * Adds a code identity.
     *
     * @param code the code
     * @return this
     */
    public TestCustomerBuilder addCodeIdentity(String code) {
        addIdentities(createEntityIdentity(CustomerArchetypes.CODE_IDENTITY, ValueStrategy.value(code)));
        return this;
    }

    /**
     * Adds a patient.
     *
     * @param patient the patient
     * @return this
     */
    public TestCustomerBuilder addPatient(Party patient) {
        patients.add(patient);
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (titleCode.isSet()) {
            // for new customers, the default lookup.personTitle will be used if one is present,
            // so allow a null title to override this
            if (titleCode.getValue() != null) {
                new TestLookupBuilder(UserArchetypes.TITLE, getService())
                    .code(titleCode.toString())
                    .build();
            }
            titleCode.setValue(bean, "title");
        }
        firstName.setValue(bean, "firstName");
        lastName.setValue(bean, "lastName");
        companyName.setValue(bean, "companyName");
        if (referredBy.isSet()) {
            new TestLookupBuilder(CustomerArchetypes.CUSTOMER_REFERRAL, getService())
                    .code(referredBy.toString())
                    .build();
            referredBy.setValue(bean, "referral");
        }
        if (referredByCustomer.isSet()) {
            bean.setTarget("referredByCustomer", (Party) referredByCustomer.getValue());
        }

        if (discounts != null) {
            for (Entity discount : discounts) {
                bean.addTarget("discounts", discount);
            }
        }
        if (taxExemptionCodes != null) {
            for (String code : taxExemptionCodes) {
                Lookup taxType = new TestLookupBuilder("lookup.taxType", getService()).code(code).build();
                object.addClassification(taxType);
            }
        }
        if (practice != null) {
            bean.setTarget("practice", practice);
        }
        if (!patients.isEmpty()) {
            for (Party patient : patients) {
                bean.addTarget("patients", PatientArchetypes.PATIENT_OWNER, patient);
            }
            patients.clear(); // can't reuse
        }
    }
}
