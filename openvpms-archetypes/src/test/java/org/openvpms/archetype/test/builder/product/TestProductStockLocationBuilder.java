/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.stock.StockArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builder of <em>entityLink.productStockLocation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProductStockLocationBuilder<P extends TestProductBuilder<P>>
        extends AbstractTestIMObjectBuilder<EntityLink, TestProductStockLocationBuilder<P>> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The stock quantity.
     */
    private ValueStrategy quantity = ValueStrategy.unset();

    /**
     * The ideal quantity.
     */
    private ValueStrategy idealQuantity = ValueStrategy.unset();

    /**
     * The critical quantity.
     */
    private ValueStrategy criticalQuantity = ValueStrategy.unset();

    /**
     * The supplier.
     */
    private ValueStrategy supplier = ValueStrategy.unset();

    /**
     * Constructs a {@link TestProductSupplierBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestProductStockLocationBuilder(P parent, ArchetypeService service) {
        super(StockArchetypes.PRODUCT_STOCK_LOCATION_RELATIONSHIP, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Constructs a {@link TestProductSupplierBuilder}.
     *
     * @param parent  the parent builder
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestProductStockLocationBuilder(P parent, EntityLink object, ArchetypeService service) {
        super(object, service);
        this.parent = parent;
    }

    /**
     * Sets the stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public TestProductStockLocationBuilder<P> stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return this;
    }

    /**
     * Sets the stock quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the stock quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> quantity(BigDecimal quantity) {
        this.quantity = ValueStrategy.value(quantity);
        return getThis();
    }

    /**
     * Sets the ideal quantity.
     *
     * @param idealQuantity the ideal quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> idealQuantity(int idealQuantity) {
        return idealQuantity(BigDecimal.valueOf(idealQuantity));
    }

    /**
     * Sets the ideal quantity.
     *
     * @param idealQuantity the ideal quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> idealQuantity(BigDecimal idealQuantity) {
        this.idealQuantity = ValueStrategy.value(idealQuantity);
        return getThis();
    }

    /**
     * Sets the critical quantity.
     *
     * @param criticalQuantity the critical quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> criticalQuantity(int criticalQuantity) {
        return criticalQuantity(BigDecimal.valueOf(criticalQuantity));
    }

    /**
     * Sets the critical quantity.
     *
     * @param criticalQuantity the critical quantity
     * @return this
     */
    public TestProductStockLocationBuilder<P> criticalQuantity(BigDecimal criticalQuantity) {
        this.criticalQuantity = ValueStrategy.value(criticalQuantity);
        return getThis();
    }

    /**
     * Sets the supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public TestProductStockLocationBuilder<P> supplier(Party supplier) {
        this.supplier = ValueStrategy.value(supplier != null ? supplier.getObjectReference() : null);
        return getThis();
    }

    /**
     * Copies values from an existing relationship.
     *
     * @param productStockLocation the relationship to copy
     */
    public TestProductStockLocationBuilder<P> copy(EntityLink productStockLocation) {
        IMObjectBean bean = getBean(productStockLocation);
        stockLocation(getService().get(productStockLocation.getTarget(), Party.class));
        quantity(bean.getBigDecimal("quantity"));
        idealQuantity(bean.getBigDecimal("idealQty"));
        criticalQuantity(bean.getBigDecimal("criticalQty"));
        return supplier(bean.getObject("supplier", Party.class));
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityLink build() {
        return build(false);
    }

    /**
     * Adds the relationship to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        return parent.addProductStockLocation(build());
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(EntityLink object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (stockLocation != null) {
            object.setTarget(stockLocation.getObjectReference());
        }
        quantity.setValue(bean, "quantity");
        idealQuantity.setValue(bean, "idealQty");
        criticalQuantity.setValue(bean, "criticalQty");
        supplier.setValue(bean, "supplier");
    }
}
