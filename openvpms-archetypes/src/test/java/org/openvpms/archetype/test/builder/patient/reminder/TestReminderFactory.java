/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.openvpms.archetype.rules.doc.DocumentArchetypes.REMINDER_SMS_TEMPLATE;

/**
 * Factory for creating patient reminders.
 *
 * @author Tim Anderson
 */
public class TestReminderFactory {

    /**
     * The reminder rules.
     */
    private final ReminderRules reminderRules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * Creates a new {@link TestReminderFactory}.
     *
     * @param reminderRules   the reminder rules
     * @param service         the archetype service
     * @param documentFactory the document factory
     */
    public TestReminderFactory(ReminderRules reminderRules, ArchetypeService service,
                               TestDocumentFactory documentFactory) {
        this.reminderRules = reminderRules;
        this.service = service;
        this.documentFactory = documentFactory;
    }

    /**
     * Creates a reminder type with a one-month interval.
     *
     * @return a new reminder type
     */
    public Entity createReminderType() {
        return newReminderType().defaultInterval(1, DateUnits.MONTHS).build();
    }

    /**
     * Creates a reminder type with the specified name and a one-month interval.
     *
     * @return a new reminder type
     */
    public Entity createReminderType(String name) {
        return newReminderType().name(name).defaultInterval(1, DateUnits.MONTHS).build();
    }

    /**
     * Returns a builder for a new reminder type.
     *
     * @return a reminder type builder
     */
    public TestReminderTypeBuilder newReminderType() {
        return new TestReminderTypeBuilder(service);
    }

    /**
     * Returns a builder to update a reminder type.
     *
     * @param reminderType the reminder type
     * @return a reminder type builder
     */
    public TestReminderTypeBuilder updateReminderType(Entity reminderType) {
        return new TestReminderTypeBuilder(reminderType, service);
    }

    /**
     * Creates an <em>entity.documentTemplate</em> for a vaccination reminder.
     *
     * @return a new vaccination reminder template
     */
    public Entity createVaccinationReminderTemplate() {
        return newVaccinationVaccinationReminderTemplate().build();
    }

    /**
     * Returns a builder for a vaccination reminder template.
     *
     * @return a new vaccination reminder template builder
     */
    public TestDocumentTemplateBuilder newVaccinationVaccinationReminderTemplate() {
        String file = "/documents/Vaccination Reminders.jrxml";
        String mimeType = "text/xml";
        InputStream stream = TestReminderFactory.class.getResourceAsStream(file);
        assertNotNull(stream);

        return documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(file, mimeType);
    }

    /**
     * Creates an <em>entity.documentTemplateSMSReminder</em>.
     *
     * @param type       the content type
     * @param expression the content
     * @return a new SMS template
     */
    public Entity createSMSTemplate(String type, String expression) {
        return documentFactory.newSMSTemplate(REMINDER_SMS_TEMPLATE)
                .contentType(type)
                .content(expression)
                .build();
    }

    /**
     * Creates a reminder for the patient and reminder type.
     *
     * @param patient      the patient
     * @param reminderType the reminder type
     * @return a new reminder
     */
    public Act createReminder(Party patient, Entity reminderType) {
        return newReminder()
                .patient(patient)
                .reminderType(reminderType)
                .build();
    }

    /**
     * Creates a new reminder.
     *
     * @param due          the due date
     * @param patient      the patient
     * @param reminderType the reminder type
     * @param items        the reminder items
     * @return a new reminder
     */
    public Act createReminder(Date due, Party patient, Entity reminderType, Act... items) {
        return newReminder()
                .patient(patient)
                .reminderType(reminderType)
                .dueDate(due)
                .addItems(items)
                .build();
    }

    /**
     * Returns a builder for a new reminder.
     *
     * @return a reminder builder
     */
    public TestReminderBuilder newReminder() {
        return new TestReminderBuilder(reminderRules, service);
    }

    /**
     * Updates an existing reminder.
     *
     * @param reminder the reminder to update
     * @return a reminder builder
     */
    public TestReminderBuilder updateReminder(Act reminder) {
        return new TestReminderBuilder(reminder, reminderRules, service);
    }

    /**
     * Returns a builder for a new email reminder item.
     *
     * @return a reminder item builder
     */
    public TestReminderItemBuilder newEmailReminder() {
        return new TestReminderItemBuilder(ReminderArchetypes.EMAIL_REMINDER, service);
    }

    /**
     * Creates an export reminder item with {@code PENDING} status and count {@code 0}.
     *
     * @param sendDate the send date
     * @param dueDate  the due date
     * @return an export reminder item
     */
    public Act createExportReminder(Date sendDate, Date dueDate) {
        return newExportReminder()
                .sendDate(sendDate)
                .dueDate(dueDate)
                .build();
    }

    /**
     * Returns a builder for a new export reminder item.
     *
     * @return a reminder item builder
     */
    public TestReminderItemBuilder newExportReminder() {
        return new TestReminderItemBuilder(ReminderArchetypes.EXPORT_REMINDER, service);
    }

    /**
     * Creates a list reminder item with {@code PENDING} status and count {@code 0}.
     *
     * @param sendDate the send date
     * @param dueDate  the due date
     * @return an export reminder item
     */
    public Act createListReminder(Date sendDate, Date dueDate) {
        return newListReminder()
                .sendDate(sendDate)
                .dueDate(dueDate)
                .build();
    }

    /**
     * Returns a builder for a new list reminder item.
     *
     * @return a reminder item builder
     */
    public TestReminderItemBuilder newListReminder() {
        return new TestReminderItemBuilder(ReminderArchetypes.LIST_REMINDER, service);
    }

    /**
     * Returns a builder for a new print reminder item.
     *
     * @return a reminder item builder
     */
    public TestReminderItemBuilder newPrintReminder() {
        return new TestReminderItemBuilder(ReminderArchetypes.PRINT_REMINDER, service);
    }

    /**
     * Returns a builder for a new SMS reminder item.
     *
     * @return a reminder item builder
     */
    public TestReminderItemBuilder newSMSReminder() {
        return new TestReminderItemBuilder(ReminderArchetypes.SMS_REMINDER, service);
    }

    /**
     * Creates a <em>lookup.reminderGroup</em> lookup, with a unique code.
     *
     * @return a new reminder group lookup
     */
    public Lookup createReminderGroup() {
        return new TestLookupFactory(service).newLookup("lookup.reminderGroup")
                .uniqueCode("XREMINDERGROUP_")
                .build();
    }

    /**
     * Returns a builder for a new reminder configuration.
     *
     * @return a reminder configuration builder
     */
    public TestReminderConfigurationBuilder newReminderConfiguration() {
        return new TestReminderConfigurationBuilder(service);
    }
}