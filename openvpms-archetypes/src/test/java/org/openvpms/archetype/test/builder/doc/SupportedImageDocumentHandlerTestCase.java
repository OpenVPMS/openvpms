/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.SupportedImageDocumentHandler;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.document.Document;
import org.springframework.beans.factory.annotation.Autowired;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.doc.DocumentArchetypes.DEFAULT_DOCUMENT;
import static org.openvpms.archetype.rules.doc.DocumentArchetypes.IMAGE_DOCUMENT;

/**
 * Tests the {@link SupportedImageDocumentHandler} class.
 *
 * @author Tim Anderson
 */
public class SupportedImageDocumentHandlerTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests the {@link SupportedImageDocumentHandler#canHandle(String, String)} method.
     */
    @Test
    public void testCanHandleFileAndMimeType() {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        assertTrue(handler.canHandle("foo.bmp", "image/bmp"));
        assertTrue(handler.canHandle("foo.gif", "image/gif"));
        assertTrue(handler.canHandle("foo.png", "image/png"));
        assertTrue(handler.canHandle("foo.png", "image/x-png"));
        assertTrue(handler.canHandle("foo.jpg", "image/jpg"));

        assertFalse(handler.canHandle("foo.pdf", "application/pdf"));
        assertFalse(handler.canHandle("foo.txt", "text/plain"));
    }

    /**
     * Tests the {@link SupportedImageDocumentHandler#canHandle(String, String, String)} method.
     */
    @Test
    public void testCanHandleFileDocumentAndMimeType() {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        assertTrue(handler.canHandle("foo.bmp", IMAGE_DOCUMENT, "image/bmp"));
        assertTrue(handler.canHandle("foo.gif", IMAGE_DOCUMENT, "image/gif"));
        assertTrue(handler.canHandle("foo.png", IMAGE_DOCUMENT, "image/png"));
        assertTrue(handler.canHandle("foo.png", IMAGE_DOCUMENT, "image/x-png"));
        assertTrue(handler.canHandle("foo.jpg", IMAGE_DOCUMENT, "image/jpg"));

        assertFalse(handler.canHandle("foo.bmp", DEFAULT_DOCUMENT, "image/bmp"));
    }

    /**
     * Tests the {@link SupportedImageDocumentHandler#canHandle(Document)} method.
     */
    @Test
    public void testCanHandleDocument() {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        Document doc1 = create(IMAGE_DOCUMENT, Document.class);
        Document doc2 = create(DEFAULT_DOCUMENT, Document.class);

        assertTrue(handler.canHandle(doc1));
        assertFalse(handler.canHandle(doc2));
    }

    /**
     * Tests the {@link SupportedImageDocumentHandler#create(String, InputStream, String, int)} method.
     */
    @Test
    public void testCreateFromStream() throws Exception {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        byte[] bytes = getImage("/documents/image.png");

        String name = "image.png";
        String mimeType = "image/png";
        Document document = handler.create(name, new ByteArrayInputStream(bytes), mimeType, -1);

        documentFactory.newImageVerifier()
                .name(name)
                .mimeType(mimeType)
                .size(7547)
                .checksum(84220208)
                .width(26)
                .height(27)
                .content(bytes)
                .verify(document);
    }

    /**
     * Verifies a large PNG can be read.
     *
     * @throws Exception for any error
     */
    @Test
    public void testReadLargePNG() throws Exception {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        byte[] bytes = getImage("/documents/largeimage.png");

        String name = "largeimage.png";
        String mimeType = "image/png";
        Document document = handler.create(name, new ByteArrayInputStream(bytes), mimeType, -1);

        documentFactory.newImageVerifier()
                .name(name)
                .mimeType(mimeType)
                .size(74865)
                .checksum(2122638163)
                .width(1000)
                .height(1000)
                .content(bytes)
                .verify(document);
    }

    /**
     * Verifies a large JPG can be read.
     *
     * @throws Exception for any error
     */
    @Test
    public void testReadLargeJPG() throws Exception {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        byte[] bytes = getImage("/documents/largeimage.jpg");

        String name = "largeimage.jpg";
        String mimeType = "image/jpg";
        Document document = handler.create(name, new ByteArrayInputStream(bytes), mimeType, -1);

        documentFactory.newImageVerifier()
                .name(name)
                .mimeType(mimeType)
                .size(122842)
                .checksum(4280779)
                .width(1000)
                .height(1000)
                .content(bytes)
                .verify(document);
    }

    /**
     * Tests the {@link SupportedImageDocumentHandler#create(String, byte[], String, int)} method.
     * <p/>
     * This method is not supported as the content is expected to be compressed.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testCreateFromArray() {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        handler.create("image.png", new byte[0], "image/png", -1);
    }

    /**
     * Tests the {@link SupportedImageDocumentHandler#update(
     *org.openvpms.component.business.domain.im.document.Document, InputStream, String, int)} method.
     * <p/>
     * This is largely intended for editing documents via WebDAV, so makes little sense for images. It is therefore
     * not possible to change the name or mime type. TODO - make this an optional operation
     *
     * @throws Exception for any error
     */
    @Test
    public void testUpdate() throws Exception {
        DocumentHandler handler = new SupportedImageDocumentHandler(getArchetypeService());
        String name = "image.png";
        String mimeType = "image/png";
        org.openvpms.component.business.domain.im.document.Document document
                = handler.create(name, new ByteArrayInputStream(getImage("/documents/image.png")), mimeType, -1);
        byte[] bytes = getImage("/documents/image2.png");
        handler.update(document, new ByteArrayInputStream(bytes), mimeType, bytes.length);

        documentFactory.newImageVerifier()
                .name(name)
                .mimeType(mimeType)
                .size(189)
                .checksum(3336140538L)
                .width(16)
                .height(16)
                .content(bytes)
                .verify(document);
    }

    /**
     * Returns the image resource at the specified path as a byte array.
     *
     * @param path the path
     * @return the image content
     * @throws Exception for any error
     */
    private byte[] getImage(String path) throws Exception {
        InputStream stream = getClass().getResourceAsStream(path);
        assertNotNull(stream);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        IOUtils.copy(stream, byteStream);
        return byteStream.toByteArray();
    }

    /**
     * Helper to generate an image.
     *
     * @param format the image format
     * @return the image contents
     * @throws Exception for any error
     */
    private byte[] generateImage(String format) throws Exception {
        int size = 1000;
        int square = 10;
        BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        int r = 0;
        for (int row = 0; row < (size / square); row++) {
            for (int col = 0; col < (size / square); col++) {
                graphics.setColor(new Color(r, row, col));
                r += 25;
                if (r > 255) {
                    r = 0;
                }
                graphics.fillRect(col * square, row * square, square, square);
            }
        }
        ByteArrayOutputStream output = new ByteArrayOutputStream(8192);
        ImageIO.write(image, format, output);
        return output.toByteArray();
    }
}