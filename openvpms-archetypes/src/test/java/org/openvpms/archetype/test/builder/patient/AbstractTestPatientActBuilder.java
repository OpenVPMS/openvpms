/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for patient acts, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestPatientActBuilder<T extends Act, B extends AbstractTestPatientActBuilder<T, B>>
        extends AbstractTestActBuilder<T, B> {

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Constructs an {@link AbstractTestPatientActBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestPatientActBuilder(String archetype, Class<T> type, ArchetypeService service) {
        super(archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestPatientActBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestPatientActBuilder(T object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public B patient(Party patient) {
        this.patient = patient;
        return getThis();
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location
     * @return this
     */
    public B location(Party location) {
        this.location = location;
        return getThis();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public B clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (location != null) {
            bean.setTarget("location", location);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
    }
}
