/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.test.builder.eft.TestEFTPOSTerminalBuilder;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builder for <em>party.organisationPractice</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPracticeFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestPracticeFactory}.
     *
     * @param service the archetype service
     */
    public TestPracticeFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates and saves a practice.
     *
     * @return the practice
     */
    public Party getPractice() {
        return newPractice().build();
    }

    /**
     * Returns a builder for a new practice.
     *
     * @return a practice builder
     */
    public TestPracticeBuilder newPractice() {
        return new TestPracticeBuilder(service);
    }

    /**
     * Returns a builder to update a practice.
     *
     * @param practice the practice to update
     * @return a practice builder
     */
    public TestPracticeBuilder updatePractice(Party practice) {
        return new TestPracticeBuilder(practice, service);
    }

    /**
     * Creates and saves a practice location.
     *
     * @return the practice location
     */
    public Party createLocation() {
        return newLocation().build();
    }

    /**
     * Returns a builder for a new practice location.
     *
     * @return a practice location builder
     */
    public TestLocationBuilder newLocation() {
        return new TestLocationBuilder(service);
    }

    /**
     * Returns a builder to update a practice location.
     *
     * @return a practice location builder
     */
    public TestLocationBuilder updateLocation(Party location) {
        return new TestLocationBuilder(location, service);
    }

    /**
     * Creates a new EFTPOS terminal.
     *
     * @return a new EFTPOS terminal
     */
    public Entity createEFTPOSTerminal() {
        return newEFTPOSTerminal().build();
    }

    /**
     * Returns a builder for a new EFTPOS terminal.
     *
     * @return an EFTPOS terminal builder
     */
    public TestEFTPOSTerminalBuilder newEFTPOSTerminal() {
        return new TestEFTPOSTerminalBuilder(service);
    }

    /**
     * Creates a new till.
     *
     * @return a new till
     */
    public Entity createTill() {
        return newTill().build();
    }

    /**
     * Returns a builder for a new till.
     *
     * @return a till builder
     */
    public TestTillBuilder newTill() {
        return new TestTillBuilder(service);
    }

    /**
     * Returns a builder to update a till.
     *
     * @param till the till to update
     * @return a till builder
     */
    public TestTillBuilder updateTill(Entity till) {
        return new TestTillBuilder(till, service);
    }

    /**
     * Creates a new stock location.
     *
     * @param locations locations to link the stock location to
     * @return a new stock location
     */
    public Party createStockLocation(Party... locations) {
        return newStockLocation()
                .locations(locations)
                .build();
    }

    /**
     * Returns a builder for a new stock location.
     *
     * @return a new stock location builder
     */
    public TestStockLocationBuilder newStockLocation() {
        return new TestStockLocationBuilder(service);
    }

    /**
     * Creates a new OTC party.
     *
     * @return a new OTC party
     */
    public Party createOTC() {
        return newOTC().build();
    }

    /**
     * Returns a builder for a new OTC party.
     *
     * @return an OTC builder
     */
    public TestOTCBuilder newOTC() {
        return new TestOTCBuilder(service);
    }

    /**
     * Creates a new department.
     *
     * @return a new department
     */
    public Entity createDepartment() {
        return newDepartment().build();
    }

    /**
     * Returns a builder for a new department.
     *
     * @return a department builder
     */
    public TestDepartmentBuilder newDepartment() {
        return new TestDepartmentBuilder(service);
    }

    /**
     * Returns a builder to update a department.
     *
     * @param department the department to update
     * @return a department builder
     */
    public TestDepartmentBuilder updateDepartment(Entity department) {
        return new TestDepartmentBuilder(department, service);
    }

    /**
     * Returns a builder to create a mail server.
     *
     * @return the mail server
     */
    public TestMailServerBuilder newMailServer() {
        return new TestMailServerBuilder(service);
    }
}
