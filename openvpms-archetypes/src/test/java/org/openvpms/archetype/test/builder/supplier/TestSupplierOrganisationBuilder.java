/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>party.supplierorganisation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSupplierOrganisationBuilder extends AbstractTestPartyBuilder<Party, TestSupplierOrganisationBuilder> {

    /**
     * The ESCI configurations.
     */
    private final List<EntityRelationship> esciConfigs = new ArrayList<>();

    /**
     * The built ESCI configs.
     */
    private List<EntityRelationship> builtESCIConfigs = Collections.emptyList();

    /**
     * Constructs a {@link TestSupplierOrganisationBuilder}.
     *
     * @param service the archetype service
     */
    public TestSupplierOrganisationBuilder(ArchetypeService service) {
        super(SupplierArchetypes.SUPPLIER_ORGANISATION, Party.class, service);
        name(ValueStrategy.random("zsupplier"));
    }

    /**
     * Constructs a {@link TestSupplierOrganisationBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestSupplierOrganisationBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds an ESCI configuration for a stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public TestSupplierOrganisationBuilder addESCIConfiguration(Party stockLocation) {
        EntityRelationship config = create(SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI,
                                           EntityRelationship.class);
        IMObjectBean bean = getBean(config);
        bean.setValue("accountId", "ANACCOUNTID");
        bean.setValue("serviceURL", "http://localhost:8080/esci/RegistryService?wsdl");
        config.setTarget(stockLocation.getObjectReference());
        esciConfigs.add(config);
        return this;
    }

    /**
     * Returns the built ESCI configurations.
     *
     * @return the ESCI configurations
     */
    public List<EntityRelationship> getESCIConfigs() {
        return builtESCIConfigs;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (EntityRelationship relationship : esciConfigs) {
            relationship.setSource(object.getObjectReference());
            object.addEntityRelationship(relationship);
        }
        builtESCIConfigs = new ArrayList<>(esciConfigs);
        esciConfigs.clear();  // can't reuse
    }
}