/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Builds <em>party.supplierVeterinaryPractice</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestVetPracticeBuilder extends AbstractTestPartyBuilder<Party, TestVetPracticeBuilder> {

    /**
     * The vets and the date they started.
     */
    private final Map<Party, Date> vets = new LinkedHashMap<>();

    /**
     * Constructs a {@link TestVetPracticeBuilder}.
     *
     * @param service the archetype service
     */
    public TestVetPracticeBuilder(ArchetypeService service) {
        super(SupplierArchetypes.SUPPLIER_VET_PRACTICE, Party.class, service);
        name(ValueStrategy.random("zvetpractice"));
    }

    /**
     * Constructs an {@link AbstractTestPartyBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestVetPracticeBuilder(Party object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds a vet to the practice.
     *
     * @param vet   the vet
     * @param start their start date
     * @return this
     */
    public TestVetPracticeBuilder addVet(Party vet, Date start) {
        vets.put(vet, start);
        return this;
    }

    /**
     * Add vets to the practice, starting now.
     *
     * @param vets the vets
     * @return this
     */
    public TestVetPracticeBuilder addVets(Party... vets) {
        for (Party vet : vets) {
            this.vets.put(vet, new Date());
        }
        return this;
    }

    /**
     * Builds the party.
     *
     * @param object   the party to build
     * @param bean     a bean wrapping the party
     * @param toSave   objects to save, if the entity is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (Map.Entry<Party, Date> entry : vets.entrySet()) {
            Party vet = entry.getKey();
            PeriodRelationship relationship = (PeriodRelationship) bean.addTarget("veterinarians", vet, "practices");
            relationship.setActiveStartTime(entry.getValue());
            toSave.add(vet);
        }
    }
}