/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Date;
import java.util.Set;

/**
 * Builder for <em>entity.productBatch</em> for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestBatchBuilder extends AbstractTestEntityBuilder<Entity, TestBatchBuilder> {

    /**
     * The batch expiry date.
     */
    private ValueStrategy expiryDate = ValueStrategy.unset();

    /**
     * The product this is a batch for.
     */
    private Product product;

    /**
     * The stock locations where this batch is available.
     */
    private Entity[] stockLocations;

    /**
     * Constructs a {@link TestBatchBuilder}.
     *
     * @param service the archetype service
     */
    public TestBatchBuilder(ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_BATCH, Entity.class, service);
    }

    /**
     * Constructs a {@link TestBatchBuilder}.
     *
     * @param batch   the batch to update
     * @param service the archetype service
     */
    public TestBatchBuilder(Entity batch, ArchetypeService service) {
        super(batch, service);
    }

    /**
     * Sets the batch number.
     *
     * @param batchNo the batch number
     * @return this
     */
    public TestBatchBuilder batchNo(String batchNo) {
        return name(batchNo);
    }

    /**
     * Sets the batch expiry date.
     *
     * @param expiryDate the expiry date
     * @return this
     */
    public TestBatchBuilder expiryDate(Date expiryDate) {
        this.expiryDate = ValueStrategy.value(expiryDate);
        return this;
    }

    /**
     * Sets the product this is a batch for.
     *
     * @param product the product
     * @return this
     */
    public TestBatchBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Makes this batch available at the specified stock locations.
     *
     * @param stockLocations the stock locations
     * @return this
     */
    public TestBatchBuilder addStockLocations(Entity... stockLocations) {
        this.stockLocations = stockLocations;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        Relationship relationship;
        if (product != null) {
            relationship = bean.setTarget("product", product);
        } else {
            relationship = bean.getObject("product", Relationship.class);
        }
        if (expiryDate.isSet()) {
            expiryDate.setValue(getBean(relationship), "activeEndTime");
        }
        if (stockLocations != null) {
            for (Entity stockLocation : stockLocations) {
                bean.addTarget("stockLocations", stockLocation);
            }
        }
    }
}