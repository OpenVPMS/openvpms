/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>entity.department</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDepartmentBuilder extends AbstractTestEntityBuilder<Entity, TestDepartmentBuilder> {

    /**
     * The service ratios.
     */
    private final List<EntityLink> serviceRatios = new ArrayList<>();

    /**
     * Constructs a {@link TestDepartmentBuilder}.
     *
     * @param service the archetype service
     */
    public TestDepartmentBuilder(ArchetypeService service) {
        super(PracticeArchetypes.DEPARTMENT, Entity.class, service);
        name(ValueStrategy.random("zdepartment"));
    }

    /**
     * Constructs a {@link TestDepartmentBuilder}.
     *
     * @param object  the department to update
     * @param service the archetype service
     */
    public TestDepartmentBuilder(Entity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @return this
     */
    public TestDepartmentBuilder addServiceRatio(Entity productType, int ratio) {
        return addServiceRatio(productType, BigDecimal.valueOf(ratio));
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @return this
     */
    public TestDepartmentBuilder addServiceRatio(Entity productType, BigDecimal ratio) {
        return addServiceRatio(productType, ratio, null);
    }

    /**
     * Adds service ratio.
     *
     * @param productType the product type
     * @param ratio       the ratio
     * @param calendar    the calendar when the ratio applies
     * @return this
     */
    public TestDepartmentBuilder addServiceRatio(Entity productType, BigDecimal ratio, Entity calendar) {
        EntityLink link = create(getNodeArchetype("serviceRatios"), EntityLink.class);
        link.setTarget(productType.getObjectReference());
        IMObjectBean bean = getBean(link);
        bean.setValue("ratio", ratio);
        if (calendar != null) {
            bean.setValue("calendar", calendar.getObjectReference());
        }
        serviceRatios.add(link);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (EntityLink serviceRatio : serviceRatios) {
            serviceRatio.setSource(object.getObjectReference());
            bean.addValue("serviceRatios", serviceRatio);
        }
        serviceRatios.clear(); // can't reuse
    }
}
