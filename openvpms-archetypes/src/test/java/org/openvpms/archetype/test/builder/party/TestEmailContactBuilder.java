/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>contact.emailAddress</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestEmailContactBuilder<T extends Party, P extends AbstractTestPartyBuilder<T, P>>
        extends AbstractTestContactBuilder<T, P, TestEmailContactBuilder<T, P>> {

    /**
     * The email address.
     */
    private String email;

    /**
     * Constructs a {@link TestEmailContactBuilder}.
     *
     * @param service the archetype service
     */
    public TestEmailContactBuilder(ArchetypeService service) {
        super(ContactArchetypes.EMAIL, service);
    }

    /**
     * Constructs a {@link TestEmailContactBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestEmailContactBuilder(P parent, ArchetypeService service) {
        super(parent, ContactArchetypes.EMAIL, service);
    }

    /**
     * Sets the email address.
     *
     * @param email the email address
     * @return this
     */
    public TestEmailContactBuilder<T, P> email(String email) {
        this.email = email;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Contact object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setValue("emailAddress", email);
    }
}
