/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.laboratoryDevice*</em>
 *
 * @author Tim Anderson
 */
public class TestDeviceBuilder extends AbstractTestEntityBuilder<Entity, TestDeviceBuilder> {

    /**
     * The device id archetype.
     */
    private String deviceIdArchetype;

    /**
     * The device id.
     */
    private ValueStrategy deviceId;

    /**
     * The laboratory.
     */
    private Entity laboratory;

    /**
     * The locations that the device applies to.
     */
    private Party[] locations;

    /**
     * Constructs a {@link TestDeviceBuilder}.
     *
     * @param service the archetype service
     */
    public TestDeviceBuilder(ArchetypeService service) {
        this(LaboratoryArchetypes.DEVICE, service);
        deviceId(ValueStrategy.random());
    }

    /**
     * Constructs a {@link TestDeviceBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestDeviceBuilder(String archetype, ArchetypeService service) {
        super(archetype, Entity.class, service);
        name(ValueStrategy.random("zdevice"));
    }

    /**
     * Sets the device identifier.
     *
     * @param deviceId the device identifier
     * @return this
     */
    public TestDeviceBuilder deviceId(ValueStrategy deviceId) {
        return deviceId("entityIdentity.laboratoryDeviceTest", deviceId);
    }

    /**
     * Sets the device identifier.
     *
     * @param archetype the device identifier archetype
     * @param deviceId  the device identifier
     * @return this
     */
    public TestDeviceBuilder deviceId(String archetype, ValueStrategy deviceId) {
        this.deviceIdArchetype = archetype;
        this.deviceId = deviceId;
        return this;
    }

    /**
     * Sets the laboratory that the device is applicable for.
     *
     * @param laboratory the laboratory
     * @return this
     */
    public TestDeviceBuilder laboratory(Entity laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Sets the locations that the device is applicable for.
     *
     * @param locations the locations
     * @return this
     */
    public TestDeviceBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        if (deviceIdArchetype != null && deviceId != null) {
            EntityIdentity identity = createEntityIdentity(deviceIdArchetype, deviceId);
            object.addIdentity(identity);
        }
        if (laboratory != null) {
            bean.setTarget("laboratory", laboratory);
        }
        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location);
            }
        }
    }
}
