/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;

/**
 * Builder for <em>document.*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentBuilder extends AbstractTestIMObjectBuilder<Document, TestDocumentBuilder> {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The content.
     */
    private InputStream content;

    /**
     * The mime type.
     */
    private String mimeType;

    /**
     * Constructs a {@link TestDocumentBuilder}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     */
    public TestDocumentBuilder(DocumentHandlers handlers, ArchetypeService service) {
        super(null, Document.class, service);
        this.handlers = handlers;
    }

    /**
     * Sets the mime type.
     *
     * @param mimeType the mime type
     * @return this
     */
    public TestDocumentBuilder mimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    /**
     * Sets the content.
     *
     * @param content stream to the content
     * @return this
     */
    public TestDocumentBuilder content(InputStream content) {
        this.content = content;
        return this;
    }

    /**
     * Sets the content from a string.
     *
     * @param content the content
     * @return this
     */
    public TestDocumentBuilder content(String content) {
        return content(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * Sets the content.
     *
     * @param content to the content
     * @return this
     */
    public TestDocumentBuilder content(byte[] content) {
        this.content = new ByteArrayInputStream(content);
        return this;
    }

    /**
     * Builds the object, collecting the objects built without saving them.
     *
     * @param objects  collects the built objects
     * @param remove  collects the references to objects to remove
     * @return the primary object
     */
    @Override
    public Document build(Set<IMObject> objects, Set<Reference> remove) {
        String name = getName().toString();
        DocumentHandler handler = handlers.get(name, mimeType);
        Document document = handler.create(name, content, mimeType, -1);
        objects.add(document);
        return document;
    }
}