/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;
import java.util.UUID;

/**
 * Builder for <em>actIdentity.sync*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSyncIdBuilder extends AbstractTestIMObjectBuilder<ActIdentity, TestSyncIdBuilder> {

    /**
     * The identity.
     */
    private ValueStrategy identity = ValueStrategy.unset();

    /**
     * The status.
     */
    private ValueStrategy status = ValueStrategy.unset();

    /**
     * Constructs a {@link TestSyncIdBuilder}.
     *
     * @param service the archetype service
     */
    public TestSyncIdBuilder(ArchetypeService service) {
        super("actIdentity.syncTest", ActIdentity.class, service);
        identity(UUID.randomUUID().toString());
    }

    /**
     * Constructs a {@link TestSyncIdBuilder}.
     *
     * @param archetype the archetype to build
     * @param service   the archetype service
     */
    public TestSyncIdBuilder(String archetype, ArchetypeService service) {
        super(archetype, ActIdentity.class, service);
    }

    /**
     * Constructs a {@link TestSyncIdBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestSyncIdBuilder(ActIdentity object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Sets the identity.
     *
     * @param identity the identity
     * @return this
     */
    public TestSyncIdBuilder identity(String identity) {
        this.identity = ValueStrategy.value(identity);
        return this;
    }

    /**
     * Sets the status.
     *
     * @param status the status
     * @return this
     */
    public TestSyncIdBuilder status(String status) {
        this.status = ValueStrategy.value(status);
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public ActIdentity build() {
        return build(false);
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(ActIdentity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        identity.setValue(bean, "identity");
        status.setValue(bean, "status");
    }
}