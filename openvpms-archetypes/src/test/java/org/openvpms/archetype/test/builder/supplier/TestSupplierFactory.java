/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.test.builder.supplier.delivery.TestDeliveryBuilder;
import org.openvpms.archetype.test.builder.supplier.delivery.TestDeliveryItemBuilder;
import org.openvpms.archetype.test.builder.supplier.delivery.TestReturnBuilder;
import org.openvpms.archetype.test.builder.supplier.order.TestOrderBuilder;
import org.openvpms.archetype.test.builder.supplier.order.TestOrderItemBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for supplier objects.
 *
 * @author Tim Anderson
 */
public class TestSupplierFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestSupplierFactory}.
     *
     * @param service the service
     */
    public TestSupplierFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new supplier organisation.
     *
     * @return the supplier
     */
    public Party createSupplier() {
        return newSupplier().build();
    }

    /**
     * Returns a builder for a new supplier organisation.
     *
     * @return a supplier organisation builder
     */
    public TestSupplierOrganisationBuilder newSupplier() {
        return new TestSupplierOrganisationBuilder(service);
    }

    /**
     * Returns a builder to update a supplier organisation.
     *
     * @param supplier the supplier
     * @return a supplier organisation builder
     */
    public TestSupplierOrganisationBuilder updateSupplier(Party supplier) {
        return new TestSupplierOrganisationBuilder(supplier, service);
    }

    /**
     * Creates a new manufacturer.
     *
     * @return the manufacturer
     */
    public Party createManufacturer() {
        return newManufacturer().build();
    }

    /**
     * Returns a builder for a new manufacturer.
     *
     * @return a manufacturer builder
     */
    public TestManufacturerBuilder newManufacturer() {
        return new TestManufacturerBuilder(service);
    }

    /**
     * Creates a new vet.
     *
     * @return the vet
     */
    public Party createVet() {
        return newVet().build();
    }

    /**
     * Returns a builder for a new vet.
     *
     * @return a vet builder
     */
    public TestVetBuilder newVet() {
        return new TestVetBuilder(service);
    }

    /**
     * Creates a new vet practice.
     *
     * @return the vet practice
     */
    public Party createVetPractice() {
        return newVetPractice().build();
    }

    /**
     * Returns a builder for a new vet practice.
     *
     * @return a vet practice builder
     */
    public TestVetPracticeBuilder newVetPractice() {
        return new TestVetPracticeBuilder(service);
    }

    /**
     * Returns a builder to update a vet practice.
     *
     * @param practice the practice to update
     * @return a vet practice builder
     */
    public TestVetPracticeBuilder updateVetPractice(Party practice) {
        return new TestVetPracticeBuilder(practice, service);
    }

    /**
     * Returns a builder for a new supplier order.
     *
     * @return a supplier order builder
     */
    public TestOrderBuilder newOrder() {
        return new TestOrderBuilder(service);
    }

    /**
     * Returns a builder to create an order item.
     *
     * @return a supplier order item builder
     */
    public TestOrderItemBuilder newOrderItem() {
        return new TestOrderItemBuilder(null, service);
    }

    /**
     * Returns a builder to update an order item.
     *
     * @param orderItem the order item
     * @return a supplier order item builder
     */
    public TestOrderItemBuilder updateOrderItem(FinancialAct orderItem) {
        return new TestOrderItemBuilder(orderItem, null, service);
    }

    /**
     * Returns a builder for a new supplier delivery.
     *
     * @return a supplier delivery builder
     */
    public TestDeliveryBuilder newDelivery() {
        return new TestDeliveryBuilder(service);
    }

    /**
     * Returns a builder for a new supplier delivery item.
     *
     * @return a supplier delivery item builder
     */
    public TestDeliveryItemBuilder newDeliveryItem() {
        return new TestDeliveryItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new supplier return.
     *
     * @return a supplier return builder
     */
    public TestReturnBuilder newReturn() {
        return new TestReturnBuilder(service);
    }
}