/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.document;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestCustomerActBuilder;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>act.customerDocumentForm</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestCustomerFormBuilder extends AbstractTestCustomerActBuilder<DocumentAct, TestCustomerFormBuilder> {

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * The template.
     */
    private Entity template;

    /**
     * Constructs a {@link TestCustomerFormBuilder}.
     *
     * @param service         the archetype service
     * @param documentFactory the document factory
     */
    public TestCustomerFormBuilder(ArchetypeService service, TestDocumentFactory documentFactory) {
        super(CustomerArchetypes.DOCUMENT_FORM, DocumentAct.class, service);
        this.documentFactory = documentFactory;
    }

    /**
     * Sets the document template.
     *
     * @param template the document template
     * @return this
     */
    public TestCustomerFormBuilder template(Entity template) {
        this.template = template;
        return this;
    }

    /**
     * Sets a blank template.
     *
     * @return this
     */
    public TestCustomerFormBuilder template() {
        return template(documentFactory.createTemplate(CustomerArchetypes.DOCUMENT_FORM));
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        bean.setTarget("documentTemplate", template);
    }
}
