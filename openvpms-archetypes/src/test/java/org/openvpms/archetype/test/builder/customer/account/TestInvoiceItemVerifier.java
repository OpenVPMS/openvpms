/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Calendar.SECOND;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Verifies an invoice item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestInvoiceItemVerifier extends TestChargeItemVerifier<TestInvoiceVerifier, TestInvoiceItemVerifier> {

    /**
     * The expected reminder types references and corresponding reminder details.
     */
    private final Map<Reference, Reminder> expectedReminders = new HashMap<>();

    /**
     * The expected alert types and corresponding due dates.
     */
    private final Map<Reference, Alert> expectedAlerts = new HashMap<>();

    /**
     * The expected batch.
     */
    private ValueStrategy batch = ValueStrategy.value(null);

    /**
     * The expected minimum quantity.
     */
    private ValueStrategy minQuantity = ValueStrategy.value(0);

    /**
     * Medication label, if a medication is present.
     */
    private ValueStrategy label = ValueStrategy.value(null);

    /**
     * The medication expiry date, if a medication is present.
     */
    private ValueStrategy expiryDate = ValueStrategy.value(null);

    /**
     * The expected visit.
     */
    private ValueStrategy visit = ValueStrategy.unset();

    /**
     * The expected received quantity.
     */
    private ValueStrategy receivedQuantity = ValueStrategy.value(null);

    /**
     * The expected returned quantity.
     */
    private ValueStrategy returnedQuantity = ValueStrategy.value(null);

    /**
     * Determines if medication verification should be ignored.
     */
    private boolean ignoreMedication = false;

    /**
     * Constructs a {@link TestInvoiceItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestInvoiceItemVerifier(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link TestInvoiceItemVerifier}.
     *
     * @param parent the parent invoice verifier
     * @param service the archetype service
     */
    public TestInvoiceItemVerifier(TestInvoiceVerifier parent, ArchetypeService service) {
        super(parent, service);
        archetype(CustomerAccountArchetypes.INVOICE_ITEM);
    }

    /**
     * Initialises this from an item.
     *
     * @param item the item
     * @return this
     */
    public TestInvoiceItemVerifier initialise(Act item) {
        IMObjectBean bean = getBean(item);
        patient(bean.getTargetRef("patient"));
        product(bean.getTargetRef("product"));
        clinician(bean.getTargetRef("clinician"));
        department(bean.getTargetRef("department"));
        batch(bean.getTargetRef("batch"));
        minQuantity(bean.getBigDecimal("minQuantity"));
        quantity(bean.getBigDecimal("quantity"));
        fixedCost(bean.getBigDecimal("fixedCost"));
        fixedPrice(bean.getBigDecimal("fixedPrice"));
        unitCost(bean.getBigDecimal("unitCost"));
        unitPrice(bean.getBigDecimal("unitPrice"));
        serviceRatio(bean.getBigDecimal("serviceRatio"));
        discount(bean.getBigDecimal("discount"));
        tax(bean.getBigDecimal("tax"));
        total(bean.getBigDecimal("total"));
        Participation templateParticipation = bean.getObject("template", Participation.class);
        if (templateParticipation != null) {
            template(templateParticipation.getTarget());
            group((Integer) getBean(templateParticipation).getValue("group"));
        } else {
            template((Reference) null);
            group(null);
        }
        createdBy(item.getCreatedBy());
        print(bean.getBoolean("print"));
        visit = ValueStrategy.value(bean.getSourceRef("event"));
        status(item.getStatus());
        receivedQuantity(bean.getBigDecimal("receivedQuantity"));
        returnedQuantity(bean.getBigDecimal("returnedQuantity"));
        return this;
    }

    /**
     * Sets the expected batch.
     *
     * @param batch the expected batch
     */
    public TestInvoiceItemVerifier batch(Entity batch) {
        return batch(getReference(batch));
    }

    /**
     * Sets the expected batch.
     *
     * @param batch the expected batch
     */
    public TestInvoiceItemVerifier batch(Reference batch) {
        this.batch = ValueStrategy.value(batch);
        return this;
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minQuantity the expected minimum quantity
     * @return this
     */
    public TestInvoiceItemVerifier minQuantity(int minQuantity) {
        return minQuantity(BigDecimal.valueOf(minQuantity));
    }

    /**
     * Sets the expected minimum quantity.
     *
     * @param minQuantity the expected minimum quantity
     * @return this
     */
    public TestInvoiceItemVerifier minQuantity(BigDecimal minQuantity) {
        this.minQuantity = ValueStrategy.value(minQuantity);
        return this;
    }

    /**
     * Sets the medication label and expiry date, for medication products.
     *
     * @param label      the medication label
     * @param expiryDate the medication expiry date
     */
    public TestInvoiceItemVerifier medication(String label, Date expiryDate) {
        this.label = ValueStrategy.value(label);
        this.expiryDate = ValueStrategy.value(expiryDate);
        return this;
    }

    /**
     * Sets the visit.
     *
     * @param visit the visit
     * @return this
     */
    public TestInvoiceItemVerifier visit(Act visit) {
        this.visit = ValueStrategy.value(getReference(visit));
        return this;
    }

    /**
     * Sets the expected received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestInvoiceItemVerifier receivedQuantity(int receivedQuantity) {
        return receivedQuantity(BigDecimal.valueOf(receivedQuantity));
    }

    /**
     * Sets the expected received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestInvoiceItemVerifier receivedQuantity(BigDecimal receivedQuantity) {
        this.receivedQuantity = ValueStrategy.value(receivedQuantity);
        return this;
    }

    /**
     * Sets the expected returned quantity.
     *
     * @param returnedQuantity the returned quantity
     * @return this
     */
    public TestInvoiceItemVerifier returnedQuantity(BigDecimal returnedQuantity) {
        this.returnedQuantity = ValueStrategy.value(returnedQuantity);
        return this;
    }

    /**
     * Adds a reminder.
     *
     * @param reminderType the expected reminder type
     * @param dueDate      the expected due date
     * @param nextDueDate  the expected next due date
     * @param status       the expected status
     */
    public TestInvoiceItemVerifier addReminder(Entity reminderType, Date dueDate, Date nextDueDate, String status) {
        expectedReminders.put(reminderType.getObjectReference(), new Reminder(dueDate, nextDueDate, status));
        return this;
    }

    /**
     * Removes reminders.
     *
     * @return this
     */
    public TestInvoiceItemVerifier resetReminders() {
        expectedReminders.clear();
        return this;
    }

    /**
     * Adds an alert.
     *
     * @param alertType the expected alert type
     * @param endDate   the expected end date
     * @param status    the expected status
     */
    public TestInvoiceItemVerifier addAlert(Entity alertType, Date endDate, String status) {
        expectedAlerts.put(alertType.getObjectReference(), new Alert(alertType, endDate, status));
        return this;
    }

    /**
     * Removes alerts.
     *
     * @return this
     */
    public TestInvoiceItemVerifier resetAlerts() {
        expectedAlerts.clear();
        return this;
    }

    /**
     * Determines if medication verification should be ignored.
     * This can be used if an invoice item is only partially populated.
     *
     * @param ignoreMedication if {@code true}, ignore medication act verification if the product is a medication,
     *                         otherwise verify it
     * @return this
     */
    public TestInvoiceItemVerifier ignoreMedication(boolean ignoreMedication) {
        this.ignoreMedication = ignoreMedication;
        return this;
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(FinancialAct object, IMObjectBean bean) {
        super.verify(object, bean);
        checkEquals(batch, bean.getTargetRef("batch"));
        checkEquals(minQuantity, bean.getBigDecimal("minQuantity"));
        checkEquals(visit, bean.getSourceRef("event"));
        checkEquals(receivedQuantity, bean.getBigDecimal("receivedQuantity"));
        checkEquals(returnedQuantity, bean.getBigDecimal("returnedQuantity"));

        Reference productRef = getProduct();
        if (!ignoreMedication && productRef.isA(ProductArchetypes.MEDICATION)) {
            verifyMedication(bean.getTarget("dispensing", Act.class));
        } else {
            assertNull(bean.getTarget("dispensing", Act.class));
            assertNull(label.getValue());
            assertNull(expiryDate.getValue());
        }

        verifyReminders(object, bean.getTargets("reminders", Act.class));
        verifyAlerts(object, bean.getTargets("alerts", Act.class));
    }

    /**
     * Verifies a medication act is present with the expected details.
     *
     * @param medication the medication
     */
    private void verifyMedication(Act medication) {
        assertNotNull(medication);
        IMObjectBean bean = getBean(medication);
        assertEquals(getPatient(), bean.getTargetRef("patient"));
        assertEquals(getProduct(), bean.getTargetRef("product"));
        checkEquals(batch, bean.getTargetRef("batch"));
        checkEquals(getQuantity(), bean.getBigDecimal("quantity"));
        checkEquals(label, bean.getString("label"));
        checkEquals(expiryDate, medication.getActivityEndTime());
        assertEquals(getClinician(), bean.getTargetRef("clinician"));
        checkEquals(visit, bean.getSourceRef("event"));
    }

    /**
     * Verifies the expected reminders are present.
     *
     * @param item      the invoice item
     * @param reminders the actual reminders
     */
    private void verifyReminders(Act item, List<Act> reminders) {
        assertEquals(expectedReminders.size(), reminders.size());
        for (Act reminder : reminders) {
            IMObjectBean bean = getBean(reminder);
            Reminder expected = expectedReminders.get(bean.getTargetRef("reminderType"));
            assertNotNull(expected);
            assertEquals(getPatient(), bean.getTargetRef("patient"));
            assertEquals(getProduct(), bean.getTargetRef("product"));
            Date initialTime = DateUtils.truncate(bean.getDate("initialTime"), SECOND); // details nodes store ms
            assertEquals(0, DateRules.compareTo(item.getActivityStartTime(), initialTime));
            assertEquals(0, DateRules.compareTo(expected.getDueDate(), reminder.getActivityEndTime()));
            assertEquals(0, DateRules.compareTo(expected.getNextDueDate(), reminder.getActivityStartTime()));
            assertEquals(expected.getStatus(), reminder.getStatus());
        }
    }

    /**
     * Verifies the expected alerts are present.
     *
     * @param item   the invoice item
     * @param alerts the actual alerts
     */
    private void verifyAlerts(Act item, List<Act> alerts) {
        assertEquals(expectedAlerts.size(), alerts.size());
        for (Act alert : alerts) {
            IMObjectBean bean = getBean(alert);
            Alert expected = expectedAlerts.get(bean.getTargetRef("alertType"));
            assertNotNull(expected);
            assertEquals(item.getActivityStartTime(), alert.getActivityStartTime());
            assertEquals(expected.getEndDate(), alert.getActivityEndTime());

            assertEquals(getPatient(), bean.getTargetRef("patient"));
            assertEquals(getProduct(), bean.getTargetRef("product"));
            assertEquals(getClinician(), bean.getTargetRef("clinician"));
            assertEquals(expected.getReason(), alert.getReason());
            assertEquals(expected.getStatus(), alert.getStatus());
        }
    }

    private class Alert {

        private final Entity alertType;

        private final Date endDate;

        private final String status;

        private final String reason;

        public Alert(Entity alertType, Date endDate, String status) {
            this.alertType = alertType;
            this.endDate = endDate;
            this.status = status;
            reason = getBean(alertType).getString("reason");
        }

        public Entity getAlertType() {
            return alertType;
        }

        public Date getEndDate() {
            return endDate;
        }

        public String getReason() {
            return reason;
        }

        public String getStatus() {
            return status;
        }
    }

    /**
     * Expected reminder details.
     */
    private static class Reminder {

        /**
         * The expected due date.
         */
        private final Date dueDate;

        /**
         * Expected next due date.
         */
        private final Date nextDueDate;

        /**
         * Expected status.
         */
        private final String status;

        /**
         * Constructs a {@link Reminder}.
         *
         * @param dueDate     the expected due date
         * @param nextDueDate the expected next due date
         * @param status      the expected status
         */
        public Reminder(Date dueDate, Date nextDueDate, String status) {
            this.dueDate = dueDate;
            this.nextDueDate = nextDueDate;
            this.status = status;
        }

        /**
         * Returns the expected due date.
         *
         * @return the expected due date
         */
        public Date getDueDate() {
            return dueDate;
        }

        /**
         * Returns the expected next due date.
         *
         * @return the expected next due date
         */
        public Date getNextDueDate() {
            return nextDueDate;
        }

        /**
         * Returns the expected status.
         *
         * @return the expected status
         */
        public String getStatus() {
            return status;
        }
    }
}
