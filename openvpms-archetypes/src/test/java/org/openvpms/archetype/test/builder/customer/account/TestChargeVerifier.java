/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.test.builder.act.AbstractTestActVerifier;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Verifies a charge matches that expected.
 *
 * @author Tim Anderson
 */
public abstract class TestChargeVerifier<V extends TestChargeVerifier<V, I>, I extends TestChargeItemVerifier<V, I>>
        extends AbstractTestActVerifier<FinancialAct, V> {

    /**
     * The charge item verifiers.
     */
    private final List<I> itemVerifiers = new ArrayList<>();

    /**
     * The expected customer.
     */
    private ValueStrategy customer = ValueStrategy.value(null);

    /**
     * The expected clinician.
     */
    private ValueStrategy clinician = ValueStrategy.value(null);

    /**
     * The expected amount.
     */
    private ValueStrategy amount = ValueStrategy.value(0);

    /**
     * The expected tax amount.
     */
    private ValueStrategy tax = ValueStrategy.value(0);

    /**
     * Constructs a {@link TestChargeVerifier}.
     *
     * @param service the archetype service
     */
    public TestChargeVerifier(ArchetypeService service) {
        super(service);
    }

    /**
     * Sets the expected customer.
     *
     * @param customer the expected customer
     * @return this
     */
    public V customer(Party customer) {
        this.customer = ValueStrategy.value(getReference(customer));
        return getThis();
    }

    /**
     * Sets the expected clinician.
     *
     * @param clinician the expected clinician
     * @return this
     */
    public V clinician(User clinician) {
        this.clinician = ValueStrategy.value(getReference(clinician));
        return getThis();
    }

    /**
     * Sets the expected amount.
     *
     * @param amount the expected amount
     * @return this
     */
    public V amount(int amount) {
        return amount(BigDecimal.valueOf(amount));
    }

    /**
     * Sets the expected amount.
     *
     * @param amount the expected amount
     * @return this
     */
    public V amount(BigDecimal amount) {
        this.amount = ValueStrategy.value(amount);
        return getThis();
    }

    /**
     * Sets the expected tax amount.
     *
     * @param tax the expected tax amount
     * @return this
     */
    public V tax(BigDecimal tax) {
        this.tax = ValueStrategy.value(tax);
        return getThis();
    }

    /**
     * Returns a verifier for an item.
     *
     * @return an item verifier
     */
    public abstract I item();

    /**
     * Adds an item verifier.
     *
     * @param verifier the item verifier
     * @return this
     */
    public V add(I verifier) {
        itemVerifiers.add(verifier);
        return getThis();
    }

    /**
     * Verifies an object matches that expected.
     *
     * @param object the object to verify
     * @param bean   a bean wrapping the object
     */
    @Override
    protected void verify(FinancialAct object, IMObjectBean bean) {
        super.verify(object, bean);
        checkEquals(customer, bean.getTargetRef("customer"));
        checkEquals(clinician, bean.getTargetRef("clinician"));
        checkEquals(amount, bean.getBigDecimal("amount"));
        checkEquals(tax, bean.getBigDecimal("tax"));

        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(itemVerifiers.size(), items.size());
        for (I verifier : itemVerifiers) {
            FinancialAct verified = verifier.verify(items);
            items.remove(verified);
        }
    }
}