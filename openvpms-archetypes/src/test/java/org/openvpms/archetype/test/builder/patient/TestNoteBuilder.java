/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>act.patientClinicalNote</em>, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestNoteBuilder extends AbstractTestNoteBuilder<TestNoteBuilder> {

    /**
     * The addenda.
     */
    private List<DocumentAct> addenda = new ArrayList<>();

    /**
     * Constructs a {@link TestNoteBuilder}.
     *
     * @param service the archetype service
     */
    public TestNoteBuilder(ArchetypeService service) {
        super(PatientArchetypes.CLINICAL_NOTE, DocumentAct.class, service);
    }

    /**
     * Constructs a {@link TestNoteBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public TestNoteBuilder(DocumentAct object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds addenda.
     *
     * @param addenda the addenda to add
     * @return this
     */
    public TestNoteBuilder addAddenda(DocumentAct... addenda) {
        this.addenda.addAll(Arrays.asList(addenda));
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(DocumentAct object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);
        for (DocumentAct addendum : addenda) {
            ActRelationship relationship = (ActRelationship) bean.addTarget("addenda", addendum);
            addendum.addActRelationship(relationship);
            toSave.add(addendum);
        }
        addenda.clear(); // can't reuse
    }
}