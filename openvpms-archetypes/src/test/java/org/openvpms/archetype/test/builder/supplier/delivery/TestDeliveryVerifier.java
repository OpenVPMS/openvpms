/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.delivery;

import org.openvpms.archetype.test.builder.supplier.TestSupplierActVerifier;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Verifies  <em>act.supplierDelivery</em> instances match that expected, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDeliveryVerifier extends TestSupplierActVerifier<TestDeliveryVerifier> {

    /**
     * Constructs a {@link TestDeliveryVerifier}.
     *
     * @param service the archetype service
     */
    public TestDeliveryVerifier(ArchetypeService service) {
        super(service);
    }
}
