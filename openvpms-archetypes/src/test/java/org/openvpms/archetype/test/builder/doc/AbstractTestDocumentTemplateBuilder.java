/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentHelper;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.File;
import java.io.InputStream;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * Builder for <em>entity.documentTemplate*</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestDocumentTemplateBuilder<B extends AbstractTestDocumentTemplateBuilder<B>>
        extends AbstractTestEntityBuilder<Entity, B> {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The template content, when generated from a document.
     */
    private Document document;

    /**
     * The template content that was attached.
     */
    private Document attached;

    /**
     * The template act to use.
     */
    private DocumentAct act;

    /**
     * The template act that was used.
     */
    private DocumentAct usedAct;

    /**
     * Constructs an {@link AbstractTestDocumentTemplateBuilder}.
     *
     * @param archetype the template archetype
     * @param service   the archetype service
     * @param handlers  the document handlers
     */
    public AbstractTestDocumentTemplateBuilder(String archetype, ArchetypeService service, DocumentHandlers handlers) {
        super(archetype, Entity.class, service);
        this.handlers = handlers;
    }

    /**
     * Constructs an {@link AbstractTestDocumentTemplateBuilder}.
     *
     * @param template the template to update
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    protected AbstractTestDocumentTemplateBuilder(Entity template, ArchetypeService service,
                                                  DocumentHandlers handlers) {
        super(template, service);
        this.handlers = handlers;
    }

    /**
     * Attaches a blank document template.
     *
     * @return this
     */
    public B blankDocument() {
        return document("/documents/template-blank.jrxml", "text/xml");
    }

    /**
     * Attaches a document template that accepts parameters.
     *
     * @return this
     */
    public B documentWithParameters() {
        return document("/documents/template-parameters.jrxml");
    }


    /**
     * Attaches a document from a resource.
     *
     * @param path the resource path
     * @return this
     */
    public B document(String path) {
        return document(path, null);
    }

    /**
     * Attaches a document from a file.
     *
     * @param file     the file
     * @param mimeType the mime type
     * @return this
     */
    public B document(File file, String mimeType) {
        return document(DocumentHelper.create(file, mimeType, handlers));
    }

    /**
     * Attaches a document from a resource.
     *
     * @param path     the resource path
     * @param mimeType the mime type
     * @return this
     */
    public B document(String path, String mimeType) {
        DocumentHandler handler = handlers.get(path, mimeType);
        assertNotNull(handler);
        InputStream stream = getClass().getResourceAsStream(path);
        assertNotNull(stream);
        return document(handler.create(path, stream, mimeType, -1));
    }

    /**
     * Attaches a document.
     *
     * @param document the document
     * @return this
     */
    public B document(Document document) {
        this.document = document;
        return getThis();
    }

    /**
     * Returns the document, if one was added.
     *
     * @return the document
     */
    public Document getDocument() {
        return attached;
    }

    /**
     * Sets the template act to use to associate the template with a document.
     * <p/>
     * If not set and one is required, it will be created.
     *
     * @param act the act
     * @return this
     */
    public B templateAct(DocumentAct act) {
        this.act = act;
        return getThis();
    }

    /**
     * Returns the template act that was used or built.
     *
     * @return the template act. May be {@code null}
     */
    public DocumentAct getTemplateAct() {
        return usedAct;
    }

    /**
     * Returns the document handlers.
     *
     * @return the document handlers
     */
    protected DocumentHandlers getHandlers() {
        return handlers;
    }

    /**
     * Builds the object.
     *
     * @param object   the object
     * @param bean     a bean wrapping the object
     * @param toSave   objects to save, if the object is to be saved
     * @param toRemove the objects to remove
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave, Set<Reference> toRemove) {
        super.build(object, bean, toSave, toRemove);

        usedAct = null;
        if (document != null) {
            ArchetypeService service = getService();
            if (act == null) {
                act = create(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, DocumentAct.class);
            }
            act.setDocument(document.getObjectReference());
            act.setFileName(document.getName());
            act.setMimeType(document.getMimeType());
            act.setDescription(DescriptorHelper.getDisplayName(document, service));
            IMObjectBean actBean = getBean(act);
            actBean.setTarget("template", object);
            toSave.add(act);
            toSave.add(document);
            attached = document;
            document = null; // can't reuse
        } else {
            attached = null;
        }
        usedAct = act;
        act = null; // can't reuse
    }
}
