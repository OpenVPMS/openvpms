/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.etl.load;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.etl.load.LoaderException.ErrorCode.ArchetypeNotFound;
import static org.openvpms.etl.load.LoaderException.ErrorCode.InvalidMapping;
import static org.openvpms.etl.load.LoaderException.ErrorCode.MissingRowValue;
import static org.openvpms.etl.load.LoaderException.ErrorCode.NullReference;


/**
 * Maps an {@link ETLRow} to one or more {@link IMObject} instances, using
 * mappings defined by an {@link Mappings}.
 *
 * @author Tim Anderson
 */
class RowMapper {

    /**
     * The mappings.
     */
    private final Mappings mappings;

    /**
     * The object resolver.
     */
    private final ObjectHandler handler;

    /**
     * Lookup handler.
     */
    private final LookupHandler lookupHandler;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The nodes to map, keyed on input field name.
     */
    private final Map<String, Node> nodes = new HashMap<>();

    /**
     * Map of archetype short names and their corresponding nodes descriptors,
     * cached for performance reasons.
     */
    private final Map<String, Map<String, NodeDescriptor>> descriptors = new HashMap<>();

    /**
     * Mapped objects, keyed on object {@link Node#getObjectPath}, in the order they were mapped.
     */
    private final Map<String, IMObject> objects = new LinkedHashMap<>();

    /**
     * Lookup codes for the current row.
     */
    private final Map<NodeDescriptor, LookupData> lookups = new HashMap<>();

    /**
     * The current legacy row identifier.
     */
    private String rowId;

    /**
     * Constructs a {@link RowMapper}.
     *
     * @param mappings      the row mappings
     * @param handler       the object handler
     * @param lookupHandler the lookup handler, or {@code null} if lookups
     *                      aren't being generated
     * @param service       the archetype service
     */
    public RowMapper(Mappings mappings, ObjectHandler handler, LookupHandler lookupHandler, IArchetypeService service) {
        this.mappings = mappings;
        this.handler = handler;
        this.service = service;

        for (Mapping mapping : mappings.getMapping()) {
            String target = mapping.getTarget();
            Node node = NodeParser.parse(target);
            if (node == null) {
                throw new LoaderException(InvalidMapping, target);
            }
            nodes.put(target, node);

            // cache node descriptors
            while (node != null) {
                addDescriptor(node.getArchetype());
                node = node.getChild();
            }
        }
        this.lookupHandler = lookupHandler;
    }

    /**
     * Maps a row to one or more objects.
     *
     * @param row the row to map
     * @return the mapped objects
     * @throws LoaderException           for any loader error
     * @throws ArchetypeServiceException for any archetype service error
     */
    public List<IMObject> map(ETLRow row) {
        objects.clear();
        lookups.clear();
        rowId = row.getRowId();
        for (Mapping mapping : mappings.getMapping()) {
            if (!row.exists(mapping.getSource())) {
                throw new LoaderException(MissingRowValue, mapping.getSource());
            }
            Object value = row.get(mapping.getSource());
            if (!mapping.getExcludeNull() || value != null) {
                mapValue(value, mapping, row);
            }
        }
        if (lookupHandler != null && !lookups.isEmpty()) {
            lookupHandler.add(lookups);
        }
        List<IMObject> result = new ArrayList<>(objects.values());
        objects.clear();
        return result;
    }

    /**
     * Creates a new {@link IMObject}.
     *
     * @param node    the node
     * @param mapping the node mapping
     * @return a new object, or {@code null} if there is no corresponding
     * archetype descriptor for {@code archetype}
     * @throws LoaderException           if an archetype short name is invalid
     * @throws ArchetypeServiceException if the object can't be created
     */
    protected IMObject create(Node node, Mapping mapping) {
        org.openvpms.component.business.domain.im.common.IMObject result = service.create(node.getArchetype());
        if (result == null) {
            throw new LoaderException(ArchetypeNotFound, node.getArchetype());
        }
        if (mapping.getRemoveDefaultObjects()) {
            Map<String, NodeDescriptor> nodes = descriptors.get(node.getArchetype());
            for (NodeDescriptor nodeDesc : nodes.values()) {
                if (nodeDesc.isCollection()) {
                    IMObject[] children = nodeDesc.getChildren(result).toArray(new IMObject[0]);
                    for (IMObject child : children) {
                        nodeDesc.removeChildFromCollection(result, child);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Caches an archetype descriptor.
     *
     * @param shortName the archetype
     */
    private void addDescriptor(String shortName) {
        if (!descriptors.containsKey(shortName)) {
            ArchetypeDescriptor archetype = service.getArchetypeDescriptor(shortName);
            if (archetype == null) {
                throw new LoaderException(ArchetypeNotFound, shortName);
            }
            Map<String, NodeDescriptor> map = new HashMap<>();
            for (NodeDescriptor descriptor : archetype.getAllNodeDescriptors()) {
                map.put(descriptor.getName(), descriptor);
            }
            descriptors.put(shortName, map);
        }
    }

    /**
     * Maps a value.
     *
     * @param value   the value to map
     * @param mapping the mapping
     * @param row     the row being mapped
     * @throws LoaderException           for any loader error
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void mapValue(Object value, Mapping mapping, ETLRow row) {
        Node node = nodes.get(mapping.getTarget());
        Node parentNode;
        IMObject object = getObject(node, null, null, mapping, row);
        while (node.getChild() != null) {
            parentNode = node;
            node = node.getChild();
            object = getObject(node, parentNode, object, mapping, row);
        }
        NodeDescriptor descriptor = getNode(node.getArchetype(), node.getName());
        if (descriptor.isObjectReference()) {
            String targetValue = getStringValue(value, mapping);
            descriptor.setValue(object, handler.getReference(targetValue));
        } else if (descriptor.isCollection()) {
            String targetValue = getStringValue(value, mapping);
            descriptor.addValue(object, handler.getObject(targetValue));
        } else if (lookupHandler != null && descriptor.isLookup() && lookupHandler.isGeneratedLookup(descriptor)) {
            LookupData lookup = getLookupValue(value, mapping);
            if (lookup != null) {
                descriptor.setValue(object, lookup.getCode());
                lookups.put(descriptor, lookup);
            }
        } else {
            Object targetValue = getValue(value, mapping);
            descriptor.setValue(object, targetValue);
        }
    }

    /**
     * Gets an object for the specified node.
     * If the node references an existing object, this will be returned,
     * otherwise the object will be created.
     *
     * @param node       the node
     * @param parentNode the parent node. May be {@code null}
     * @param parent     the parent object. May be {@code null}
     * @param mapping    the mapping
     * @param row        the row being mapped
     * @return the object corresponding to the node
     */
    private IMObject getObject(Node node, Node parentNode, IMObject parent, Mapping mapping, ETLRow row) {
        IMObject object;
        object = objects.get(node.getObjectPath());
        if (object == null) {
            if (node.getField() != null) {
                object = getObject(node, row);
            } else {
                object = create(node, mapping);
            }
            objects.put(node.getObjectPath(), object);
            int index = (parent != null) ? parentNode.getIndex() : -1;
            if (parent == null) {
                handler.add(rowId, object, index);
            } else {
                IMObjectBean bean = service.getBean(parent);
                String archetype = parent.getArchetype();
                String name = parentNode.getName();
                NodeDescriptor descriptor = getNode(archetype, name);
                if (descriptor.isCollection()) {
                    bean.addValue(name, object);
                } else if (descriptor.isObjectReference()) {
                    bean.setValue(parent.getName(), object.getObjectReference());
                    handler.add(rowId, object, index);
                } else {
                    bean.setValue(parent.getName(), object);
                }
            }
        }
        return object;
    }

    /**
     * Returns an object identified by reference.
     *
     * @param node the node containing the reference
     * @param row  the row being mapped
     * @return the object corresponding to the reference
     */
    private IMObject getObject(Node node, ETLRow row) {
        Object value = row.get(node.getField());
        if (value == null) {
            throw new LoaderException(NullReference, node.getArchetype(), node.getField());
        }
        String ref = SymbolicReference.create(node.getArchetype(), value.toString());
        return handler.getObject(ref);
    }

    /**
     * Returns a value based on the corresponding {@link Mapping}.
     *
     * @param object  the object
     * @param mapping the mapping
     * @return the value
     */
    private Object getValue(Object object, Mapping mapping) {
        Object result;
        if (!StringUtils.isEmpty(mapping.getValue())) {
            String value = mapping.getValue();
            if (object != null) {
                result = LoaderHelper.replaceValue(value, object.toString());
            } else {
                result = value;
            }
        } else {
            result = object;
        }
        return result;
    }

    /**
     * Returns a stringified version of an object.
     * If the mapping specifies a value, any occurrences of <em>$value</em>
     * are replaced with {@code value} and the result returned.
     *
     * @param object  the value to transform
     * @param mapping the mapping
     * @return the transformed value
     */
    private String getStringValue(Object object, Mapping mapping) {
        Object result = getValue(object, mapping);
        return (result != null) ? result.toString() : null;
    }

    /**
     * Returns the lookup data associated with an object.
     * <p/>
     * If the mapping specifies a reference of the form {@code <archetype>$value}, the archetype will be used to
     * create the lookup on commit, if it doesn't already exist. This must be used where a lookup node
     * specifies a wildcard archetype, to indicate which archetype to created.
     *
     * @param object  the value to transform
     * @param mapping the mapping
     * @return the transformed value
     */
    private LookupData getLookupValue(Object object, Mapping mapping) {
        LookupData result = null;
        String targetValue;
        String archetype = null;
        String mappingValue = mapping.getValue();
        if (mappingValue != null) {
            SymbolicReference reference = SymbolicReferenceParser.parse(mappingValue);
            if (reference != null) {
                targetValue = LoaderHelper.replaceValue(reference.getRowId(), object.toString());
                archetype = reference.getArchetype();
            } else {
                targetValue = getStringValue(object, mapping);
            }
        } else {
            targetValue = getStringValue(object, mapping);
        }
        if (targetValue != null) {
            String code = lookupHandler.getCode(targetValue);
            result = new LookupData(archetype, code, targetValue);
        }
        return result;
    }

    /**
     * Helper to get the named node descriptor from an archetype.
     *
     * @param archetype the archetype
     * @param name      the node name
     * @return the corresponding node descriptor
     */
    private NodeDescriptor getNode(String archetype, String name) {
        Map<String, NodeDescriptor> map = descriptors.get(archetype);
        return map.get(name);
    }

}
