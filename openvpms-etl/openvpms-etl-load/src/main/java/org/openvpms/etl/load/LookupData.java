/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.etl.load;


import java.util.Objects;

/**
 * Lookup data.
 *
 * @author Tim Anderson
 */
public class LookupData {

    /**
     * The lookup archetype. May be {@code null}
     */
    private final String archetype;

    /**
     * The lookup code.
     */
    private final String code;

    /**
     * The lookup name.
     */
    private final String name;

    /**
     * Constructs a {@link LookupData}.
     *
     * @param archetype the archetype. May be {@code null}
     * @param code      the code
     * @param name      the name
     */
    public LookupData(String archetype, String code, String name) {
        this.archetype = archetype;
        this.code = code;
        this.name = name;
    }

    /**
     * Returns the lookup archetype.
     *
     * @return the archetype, or {@code null} if it is unknown
     */
    public String getArchetype() {
        return archetype;
    }

    /**
     * Returns the lookup code.
     *
     * @return the lookup code
     */
    public String getCode() {
        return code;
    }

    /**
     * Returns the lookup name.
     *
     * @return the lookup name
     */
    public String getName() {
        return name;
    }

    /**
     * Determines if this equals another object.
     *
     * @param other the other object
     * @return {@code true} if this equals {@code other}, otherwise {@code false}
     */
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (!(other instanceof LookupData)) {
            return false;
        }
        LookupData data = (LookupData) other;
        return Objects.equals(archetype, data.archetype)
               && Objects.equals(code, data.code)
               && Objects.equals(name, data.name);
    }

    /**
     * Returns the hash code.
     *
     * @return the hash code
     */
    public int hashCode() {
        return Objects.hash(archetype, code, name);
    }

}
