/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.etl.extract;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.tools.data.loader.StaxArchetypeDataLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Extracts data from an OpenVPMS database, in the format expected by {@link StaxArchetypeDataLoader}.
 *
 * @author Tim Anderson
 */
public class ArchetypeDataExtracter {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * If {@code true}, only extract active instances of the specified archetypes. Referenced objects may be inactive.
     */
    private final boolean activeOnly;

    /**
     * If true, output related objects.
     */
    private final boolean related;

    /**
     * The writer.
     */
    private final XMLStreamWriter writer;

    /**
     * Used to format dates in a format acceptable to {@link StaxArchetypeDataLoader}.
     */
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * The references of objects yet to be output.
     */
    private final Set<Reference> pending = new LinkedHashSet<>();

    /**
     * The set of references of output objects.
     */
    private final Set<Reference> processed = new HashSet<>();

    /**
     * The archetype short names that are required but were not output.
     */
    private final Set<String> dependencies = new HashSet<>();

    /**
     * Archetype element name.
     */
    private static final String ARCHETYPE = "archetype";

    /**
     * Constructs an {@link ArchetypeDataExtracter}.
     *
     * @param service    the archetype service
     * @param activeOnly if {@code true}, only extract active instances of the specified archetypes. Referenced objects
     * @param related    if {@code true}, extract related objects
     * @param out        the output stream
     * @throws XMLStreamException for an XML error
     */
    public ArchetypeDataExtracter(IArchetypeService service, boolean activeOnly, boolean related, OutputStream out)
            throws XMLStreamException {
        this.service = service;
        this.activeOnly = activeOnly;
        this.related = related;
        writer = new Writer(XMLOutputFactory.newInstance().createXMLStreamWriter(out, "UTF-8"));
        writer.writeStartDocument("UTF-8", "1.0");
        writer.writeStartElement(ARCHETYPE);
    }

    /**
     * Extracts objects matching the supplied archetypes.
     *
     * @param archetypes the archetypes
     * @param id         the object identifier, or {@code <= 0} if retrieving multiple objects
     * @param name       the name to match. May contain wildcards. May be {@code null}
     * @throws XMLStreamException for any XML error
     */
    public void extract(String[] archetypes, long id, String name) throws XMLStreamException {
        String[] shortNames = DescriptorHelper.getShortNames(archetypes, true, service);
        for (String shortName : shortNames) {
            extract(shortName, id, name, shortNames);
            dependencies.remove(shortName);
        }
    }

    /**
     * Closes the extracter.
     *
     * @throws XMLStreamException for any XML error
     */
    public void close() throws XMLStreamException {
        if (!dependencies.isEmpty()) {
            writer.writeCharacters("\n");
            List<String> list = new ArrayList<>(dependencies);
            Collections.sort(list);
            writer.writeComment("\nThe following archetypes were referenced but not output:\n"
                                + StringUtils.join(list, "\n") + "\n");
        }
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new Main());
        int result = commandLine.execute(args);
        System.exit(result);
    }

    /**
     * Extracts objects matching the supplied archetype(s).
     *
     * @param archetype the archetype. Should not contain wildcards
     * @param id        the object identifier, or {@code -1} if retrieving multiple objects
     * @param name      the name to match. May contain wildcards. May be {@code null}
     * @param include   archetypes to include in the extraction. If related objects aren't being automatically pulled
     *                  in, these determine if references to inactive objects should be included
     * @throws XMLStreamException for any XML error
     */
    private void extract(String archetype, long id, String name, String[] include) throws XMLStreamException {
        ArchetypeQuery query = new ArchetypeQuery(archetype, false, activeOnly);
        if (id > 0) {
            query.add(Constraints.eq("id", id));
        }
        if (name != null) {
            query.add(Constraints.eq("name", name));
        }
        query.add(Constraints.sort("id"));
        IMObjectQueryIterator<IMObject> iterator = new IMObjectQueryIterator<>(service, query);
        while (iterator.hasNext()) {
            extract(iterator.next(), include);
        }
        while (!pending.isEmpty()) {
            Reference next = pending.iterator().next();
            pending.remove(next);
            if (!processed.contains(next)) {
                IMObject child = service.get(next);
                if (child != null) {
                    extract(child, include);
                }
            }
        }
    }

    /**
     * Extracts an object.
     *
     * @param object  the object
     * @param include archetypes to include in the extraction. If related objects aren't being automatically pulled
     *                in, these determine if references to inactive objects should be included
     * @throws XMLStreamException for any XML error
     */
    private void extract(IMObject object, String[] include) throws XMLStreamException {
        IMObjectReference reference = object.getObjectReference();
        if (processed.add(reference)) {
            write(object, null, include);
            writer.writeCharacters("\n");
        }
    }

    /**
     * Returns the objects from a collection.
     * <p/>
     * This filters out any relationships where the object is the target, as it expected these will be handled
     * from the source object.
     *
     * @param bean the object bean
     * @param node the collection node name
     * @return the collection objects
     */
    private List<IMObject> getCollection(IMObjectBean bean, String node) {
        IMObjectReference reference = bean.getReference();
        List<IMObject> result = new ArrayList<>();
        List<IMObject> values = bean.getValues(node, IMObject.class);
        for (IMObject value : values) {
            if (value instanceof IMObjectRelationship) {
                if (!Objects.equals(reference, ((IMObjectRelationship) value).getTarget())) {
                    result.add(value);
                }
            } else {
                result.add(value);
            }
        }
        return result;
    }

    /**
     * Writes a reference to an object in a collection.
     *
     * @param object     the object
     * @param collection the collection node name
     * @param include    archetypes to include in the extraction. If related objects aren't being automatically pulled
     *                   in, these determine if references to inactive objects should be included
     * @throws XMLStreamException for any XML error
     */
    private void writeCollectionReference(IMObject object, String collection, String[] include)
            throws XMLStreamException {
        writer.writeEmptyElement("data");
        writer.writeAttribute("collection", collection);
        writer.writeAttribute(ARCHETYPE, object.getArchetypeId().getShortName());
        writer.writeAttribute("childId", getReference(object.getArchetypeId(), object.getId()));
        queue(object.getObjectReference(), include);
    }

    /**
     * Queues an object for output, if related objects are being output and it hasn't already been processed.
     *
     * @param reference the object reference
     * @param include   archetypes to include in the extraction. If related objects aren't being automatically pulled
     *                  in, these determine if references to inactive objects should be included
     */
    private void queue(IMObjectReference reference, String[] include) {
        if (!related && !TypeHelper.isA(reference, include)) {
            dependencies.add(reference.getArchetypeId().getShortName());
        } else if (!processed.contains(reference)) {
            pending.add(reference);
        }
    }

    /**
     * Writes an object to the stream.
     *
     * @param object     the object to write
     * @param collection the collection node name, or {@code null} if it is not part of a collection
     * @param include    archetypes to include in the extraction. If related objects aren't being automatically pulled
     *                   in, these determine if references to inactive objects should be included
     * @throws XMLStreamException for any XML error
     */
    private void write(IMObject object, String collection, String[] include) throws XMLStreamException {
        IMObjectBean bean = new IMObjectBean(object, service);
        List<NodeDescriptor> nodes = new ArrayList<>();
        Map<NodeDescriptor, List<IMObject>> collectionNodes = new LinkedHashMap<>();
        ArchetypeDescriptor archetype = bean.getArchetype();
        for (NodeDescriptor node : archetype.getAllNodeDescriptors()) {
            if (!node.isDerived()) {
                if (node.isCollection()) {
                    List<IMObject> objects = getCollection(bean, node.getName());
                    if (!objects.isEmpty()) {
                        collectionNodes.put(node, objects);
                    }
                } else {
                    nodes.add(node);
                }
            }
        }

        if (!collectionNodes.isEmpty()) {
            writer.writeStartElement("data");
        } else {
            writer.writeEmptyElement("data");
        }
        if (collection != null) {
            writer.writeAttribute("collection", collection);
        }
        writer.writeAttribute(ARCHETYPE, object.getArchetypeId().getShortName());

        for (NodeDescriptor node : nodes) {
            String name = node.getName();
            String value = null;
            if (node.isObjectReference()) {
                IMObjectReference child = bean.getReference(name);
                if (child != null) {
                    queue(child, include);
                    value = getReference(child.getArchetypeId(), child.getId());
                }
            } else if ("id".equals(name)) {
                if (collection == null) {
                    value = getId(object.getArchetypeId(), object.getId());
                }
            } else if (node.isDate()) {
                Date date = bean.getDate(name);
                if (date != null) {
                    value = dateFormat.format(date);
                }
            } else {
                value = bean.getString(name);
            }
            if (value != null) {
                writer.writeAttribute(name, value);
            } else if (!StringUtils.isEmpty(node.getDefaultValue())) {
                writer.writeAttribute(name, "");
            }
        }
        for (Map.Entry<NodeDescriptor, List<IMObject>> entry : collectionNodes.entrySet()) {
            NodeDescriptor node = entry.getKey();
            List<IMObject> objects = entry.getValue();
            String name = node.getName();
            for (IMObject child : objects) {
                if (node.isParentChild()) {
                    write(child, name, include);
                } else {
                    writeCollectionReference(child, name, include);
                }
            }
        }
        if (!collectionNodes.isEmpty()) {
            writer.writeEndElement();
        }
    }

    /**
     * Generates a reference to an object in the format required by {@link StaxArchetypeDataLoader}.
     *
     * @param archetypeId the object archetype identifier
     * @param id          the object identifier
     * @return the object reference
     */
    private String getReference(ArchetypeId archetypeId, long id) {
        return "id:" + getId(archetypeId, id);
    }

    /**
     * Generates an identifier for an object.
     *
     * @param archetypeId the object archetype identifier
     * @param id          the object identifier
     * @return the identifier
     */
    private String getId(ArchetypeId archetypeId, long id) {
        return archetypeId.getShortName() + "-" + id;
    }

    /**
     * An XMLStreamWriter that pretty-prints output.
     */
    private static class Writer implements XMLStreamWriter {

        private final XMLStreamWriter writer;

        private int depth = 0;

        public Writer(XMLStreamWriter writer) {
            this.writer = writer;
        }

        @Override
        public void close() throws XMLStreamException {
            writer.close();
        }

        @Override
        public void flush() throws XMLStreamException {
            writer.flush();
        }

        @Override
        public NamespaceContext getNamespaceContext() {
            return writer.getNamespaceContext();
        }

        @Override
        public String getPrefix(String uri) throws XMLStreamException {
            return writer.getPrefix(uri);
        }

        @Override
        public Object getProperty(String name) throws IllegalArgumentException {
            return writer.getProperty(name);
        }

        @Override
        public void setDefaultNamespace(String uri) throws XMLStreamException {
            writer.setDefaultNamespace(uri);
        }

        @Override
        public void setNamespaceContext(NamespaceContext context) throws XMLStreamException {
            writer.setNamespaceContext(context);
        }

        @Override
        public void setPrefix(String prefix, String uri) throws XMLStreamException {
            writer.setPrefix(prefix, uri);
        }

        @Override
        public void writeAttribute(String localName, String value) throws XMLStreamException {
            writer.writeAttribute(localName, value);
        }

        @Override
        public void writeAttribute(String namespaceURI, String localName, String value) throws XMLStreamException {
            writer.writeAttribute(namespaceURI, localName, value);
        }

        @Override
        public void writeAttribute(String prefix, String namespaceURI, String localName, String value)
                throws XMLStreamException {
            writer.writeAttribute(prefix, namespaceURI, localName, value);
        }

        @Override
        public void writeCData(String data) throws XMLStreamException {
            writer.writeCData(data);
        }

        @Override
        public void writeCharacters(char[] text, int start, int len) throws XMLStreamException {
            writer.writeCharacters(text, start, len);
        }

        @Override
        public void writeCharacters(String text) throws XMLStreamException {
            writer.writeCharacters(text);
        }

        @Override
        public void writeComment(String data) throws XMLStreamException {
            writer.writeComment(data);
        }

        @Override
        public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException {
            writer.writeDefaultNamespace(namespaceURI);
        }

        @Override
        public void writeDTD(String dtd) throws XMLStreamException {
            writer.writeDTD(dtd);
        }

        @Override
        public void writeEmptyElement(String localName) throws XMLStreamException {
            newElement();
            writer.writeEmptyElement(localName);
        }

        @Override
        public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException {
            newElement();
            writer.writeEmptyElement(namespaceURI, localName);
        }

        @Override
        public void writeEmptyElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
            newElement();
            writer.writeEmptyElement(prefix, namespaceURI, localName);
        }

        @Override
        public void writeEndDocument() throws XMLStreamException {
            writer.writeEndDocument();
        }

        @Override
        public void writeStartElement(String localName) throws XMLStreamException {
            startElement();
            writer.writeStartElement(localName);
        }

        @Override
        public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException {
            startElement();
            writer.writeStartElement(namespaceURI, localName);
        }

        @Override
        public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
            startElement();
            writer.writeStartElement(prefix, namespaceURI, localName);
        }

        @Override
        public void writeEndElement() throws XMLStreamException {
            endElement();
            writer.writeEndElement();
        }

        @Override
        public void writeEntityRef(String name) throws XMLStreamException {
            writer.writeEntityRef(name);
        }

        @Override
        public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException {
            writer.writeNamespace(prefix, namespaceURI);
        }

        @Override
        public void writeProcessingInstruction(String target) throws XMLStreamException {
            writer.writeProcessingInstruction(target);
        }

        @Override
        public void writeProcessingInstruction(String target, String data) throws XMLStreamException {
            writer.writeProcessingInstruction(target, data);
        }

        @Override
        public void writeStartDocument() throws XMLStreamException {
            writer.writeStartDocument();
            writer.writeCharacters("\n");
        }

        @Override
        public void writeStartDocument(String version) throws XMLStreamException {
            writer.writeStartDocument(version);
            writer.writeCharacters("\n");
        }

        @Override
        public void writeStartDocument(String encoding, String version) throws XMLStreamException {
            writer.writeStartDocument(encoding, version);
            writer.writeCharacters("\n");
        }

        private void startElement() throws XMLStreamException {
            newElement();
            depth++;
        }

        private void endElement() throws XMLStreamException {
            writer.writeCharacters("\n");
            depth--;
            indent();
        }

        private void newElement() throws XMLStreamException {
            if (depth > 0) {
                writer.writeCharacters("\n");
            }
            indent();
        }

        private void indent() throws XMLStreamException {
            for (int i = 0; i < depth; ++i) {
                writer.writeCharacters("  ");
            }
        }
    }

    @Command(name = "dataextract", mixinStandardHelpOptions = true)
    private static class Main implements Callable<Integer> {
        @Option(names = {"--context", "-c"}, description = "Application context path.",
                defaultValue = "applicationContext.xml")
        private String contextPath;

        @Option(names = {"--archetypes", "-a"}, description = "Archetype(s). May contain wildcards.", required = true,
                split = ",", paramLabel = "archetype")
        private String[] archetypes;

        @Option(names = {"--id", "-i"}, description = "Object identifier.")
        private long id;

        @Option(names = {"--name", "-n"}, description = "Match object name. May contain wildcards.")
        private String name;

        @Option(names = {"--file", "-f"}, description = "Output file name.")
        private String file;

        @Option(names = {"--related", "-r"}, description = "Extract related objects.")
        private boolean related;

        @Option(names = {"--inactive"}, description = "Include inactive objects of the specified archetypes. "
                                                      + "Referenced objects may be inactive.")
        private boolean inactive;

        /**
         * Computes a result, or throws an exception if unable to do so.
         *
         * @return computed result
         * @throws Exception if unable to compute a result
         */
        @Override
        public Integer call() throws Exception {
            ApplicationContext context;
            if (!new File(contextPath).exists()) {
                context = new ClassPathXmlApplicationContext(contextPath);
            } else {
                context = new FileSystemXmlApplicationContext(contextPath);
            }

            FileOutputStream fileOutput;
            OutputStream out;
            if (file != null) {
                fileOutput = new FileOutputStream(file);
                out = fileOutput;
            } else {
                out = System.out;
            }

            IArchetypeService service = (IArchetypeService) context.getBean("archetypeService");
            ArchetypeDataExtracter extract = new ArchetypeDataExtracter(service, !inactive, related, out);
            extract.extract(archetypes, id, name);
            extract.close();
            return 0;
        }
    }
}
