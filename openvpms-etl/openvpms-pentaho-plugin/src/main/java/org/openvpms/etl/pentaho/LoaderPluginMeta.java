/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.etl.pentaho;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.widgets.Shell;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.etl.load.LoaderHelper;
import org.openvpms.etl.load.Mapping;
import org.openvpms.etl.load.Mappings;
import org.openvpms.etl.load.NodeParser;
import org.openvpms.etl.load.SymbolicReferenceParser;
import org.pentaho.di.core.CheckResult;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.annotations.Step;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.metastore.api.IMetaStore;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Node;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import static org.pentaho.di.core.CheckResultInterface.TYPE_RESULT_ERROR;
import static org.pentaho.di.core.CheckResultInterface.TYPE_RESULT_OK;
import static org.pentaho.di.core.CheckResultInterface.TYPE_RESULT_WARNING;


/**
 * The OpenVPMS Loader plugin meta data.
 *
 * @author Tim Anderson
 */
@Step(id = "OpenVPMSLoaderPlugin",
        name = "LoaderPluginMeta.Name",
        description = "LoaderPluginMeta.TooltipDesc",
        image = "openvpms.svg",
        categoryDescription = "i18n:org.pentaho.di.trans.step:BaseStep.Category.Output",
        i18nPackageName = "org.openvpms.etl.pentaho")
public class LoaderPluginMeta extends org.pentaho.di.trans.step.BaseStepMeta implements StepMetaInterface {

    /**
     * The database.
     */
    private DatabaseMeta database;

    /**
     * The mappings.
     */
    private Mappings mappings = new Mappings();


    /**
     * Repository attribute names.
     */
    private static final String CONNECTION = "connection";

    private static final String ID_COLUMN = "idColumn";

    private static final String SOURCE = "source";

    private static final String TARGET = "target";

    private static final String VALUE = "value";

    private static final String EXCLUDE_NULL = "excludeNull";

    private static final String SKIP_PROCESSED = "skipProcessed";

    private static final String REMOVE_DEFAULT_OBJECTS = "removeDefaultObjects";

    private static final String BATCH_SIZE = "batchSize";


    /**
     * Constructs a {@link LoaderPluginMeta}.
     */
    public LoaderPluginMeta() {
        super();
    }

    /**
     * Constructs a  {@link LoaderPluginMeta} from an XML Node.
     *
     * @param stepNode  the Node to get the info from
     * @param databases the available list of databases to reference
     * @param metaStore the meta store
     * @throws KettleXMLException for any XML error
     */
    public LoaderPluginMeta(Node stepNode, List<DatabaseMeta> databases, IMetaStore metaStore)
            throws KettleXMLException {
        loadXML(stepNode, databases, metaStore);
    }

    /**
     * Constructs a  {@link LoaderPluginMeta} from a Kettle repository.
     *
     * @param repository the repository to read from
     * @param metaStore  the MetaStore to read external information from
     * @param stepId     the step ID
     * @param databases  the databases to reference
     * @throws KettleException when an unexpected error occurred (database, network, etc)
     * @throws KettleException for any error
     */
    public LoaderPluginMeta(Repository repository, IMetaStore metaStore, ObjectId stepId, List<DatabaseMeta> databases)
            throws KettleException {
        readRep(repository, metaStore, stepId, databases);
    }

    /**
     * Called by Spoon to get a new instance of the SWT dialog for the step.
     * A standard implementation passing the arguments to the constructor of the step dialog is recommended.
     *
     * @param shell     an SWT Shell
     * @param meta      description of the step
     * @param transMeta description of the the transformation
     * @param name      the name of the step
     * @return new instance of a dialog for this step
     */
    public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta, TransMeta transMeta, String name) {
        return new LoaderPluginDialog(shell, (LoaderPluginMeta) meta, transMeta, name);
    }

    /**
     * Set default values.
     */
    @Override
    public void setDefault() {
        mappings = new Mappings();
    }

    /**
     * Clones this.
     *
     * @return a clone, or {@code null} if the cloning fails
     */
    @Override
    public Object clone() {
        LoaderPluginMeta result = (LoaderPluginMeta) super.clone();
        if (result != null) {
            StringWriter writer = new StringWriter();
            try {
                Marshaller.marshal(mappings, writer);
                String xml = writer.toString();
                result.mappings = (Mappings) Unmarshaller.unmarshal(Mappings.class, new StringReader(xml));
            } catch (Exception exception) {
                logError("Failed to copy mappings", exception);
                result = null;
            }
        }
        return result;
    }

    /**
     * Produces the XML string that describes this step's information.
     *
     * @return String containing the XML describing this step.
     */
    @Override
    public String getXML() {
        String result = "";
        if (mappings != null) {
            StringWriter writer = new StringWriter();
            try {
                Marshaller marshaller = new Marshaller(writer);
                marshaller.setSupressXMLDeclaration(true);
                marshaller.marshal(mappings);
                result = writer.toString();
            } catch (Exception exception) {
                logError("Failed to generate XML for LoaderPluginMeta", exception);
            }
        }
        return result;
    }

    /**
     * Load the values for this step from an XML Node.
     *
     * @param stepnode  the node to get the info from
     * @param databases the available list of databases to reference to
     * @param metaStore the meta store
     * @throws KettleXMLException when an unexpected XML error occurred. (malformed etc.)
     */
    @Override
    public void loadXML(Node stepnode, List<DatabaseMeta> databases, IMetaStore metaStore) throws KettleXMLException {
        try {
            Node node = XMLHandler.getSubNode(stepnode, "mappings");
            mappings = (Mappings) Unmarshaller.unmarshal(Mappings.class, node);
            database = DatabaseMeta.findDatabase(databases, mappings.getConnection());
        } catch (Exception exception) {
            throw new KettleXMLException("Unable to read step info from XML node", exception);
        }
    }

    /**
     * Read the steps information from a Kettle repository.
     *
     * @param repository the repository to read from
     * @param metaStore  the MetaStore to read external information from
     * @param stepId     the step ID
     * @param databases  the databases to reference
     * @throws KettleException when an unexpected error occurred (database, network, etc)
     */
    @Override
    public void readRep(Repository repository, IMetaStore metaStore, ObjectId stepId, List<DatabaseMeta> databases)
            throws KettleException {
        mappings = new Mappings();
        String connection = this.repository.getStepAttributeString(stepId, CONNECTION);
        mappings.setConnection(connection);
        database = DatabaseMeta.findDatabase(databases, connection);

        String idColumn = this.repository.getStepAttributeString(stepId, ID_COLUMN);
        mappings.setIdColumn(idColumn);

        mappings.setSkipProcessed(this.repository.getStepAttributeBoolean(stepId, SKIP_PROCESSED));

        mappings.setBatchSize((int) this.repository.getStepAttributeInteger(stepId, BATCH_SIZE));

        int count = this.repository.countNrStepAttributes(stepId, SOURCE);
        for (int i = 0; i < count; ++i) {
            String source = this.repository.getStepAttributeString(stepId, i, SOURCE);
            String target = this.repository.getStepAttributeString(stepId, i, TARGET);
            String value = this.repository.getStepAttributeString(stepId, i, VALUE);
            boolean excludeNull = this.repository.getStepAttributeBoolean(stepId, i, EXCLUDE_NULL);
            boolean removeDefaultObjects = this.repository.getStepAttributeBoolean(stepId, i, REMOVE_DEFAULT_OBJECTS);
            Mapping mapping = new Mapping();
            mapping.setSource(source);
            mapping.setTarget(target);
            mapping.setValue(value);
            mapping.setExcludeNull(excludeNull);
            mapping.setRemoveDefaultObjects(removeDefaultObjects);
            mappings.addMapping(mapping);
        }
    }

    /**
     * Save the steps data into a Kettle repository
     *
     * @param repository       the Kettle repository to save to
     * @param metaStore        the metaStore to optionally write to
     * @param transformationId the transformation ID
     * @param stepId           the step ID
     * @throws KettleException when an unexpected error occurred (database, network, etc)
     */
    @Override
    public void saveRep(Repository repository, IMetaStore metaStore, ObjectId transformationId, ObjectId stepId)
            throws KettleException {
        repository.saveStepAttribute(transformationId, stepId, CONNECTION, mappings.getConnection());
        repository.saveStepAttribute(transformationId, stepId, ID_COLUMN, mappings.getIdColumn());
        repository.saveStepAttribute(transformationId, stepId, SKIP_PROCESSED, mappings.getSkipProcessed());
        repository.saveStepAttribute(transformationId, stepId, BATCH_SIZE, mappings.getBatchSize());

        for (int i = 0; i < mappings.getMappingCount(); ++i) {
            Mapping mapping = mappings.getMapping(i);
            repository.saveStepAttribute(transformationId, stepId, i, SOURCE, mapping.getSource());
            repository.saveStepAttribute(transformationId, stepId, i, TARGET, mapping.getTarget());
            repository.saveStepAttribute(transformationId, stepId, i, VALUE, mapping.getValue());
            repository.saveStepAttribute(transformationId, stepId, i, EXCLUDE_NULL, mapping.getExcludeNull());
            repository.saveStepAttribute(transformationId, stepId, i, REMOVE_DEFAULT_OBJECTS,
                                         mapping.getRemoveDefaultObjects());
        }
    }

    /**
     * Checks the settings of this step and puts the findings in a remarks List.
     *
     * @param remarks    the list to put the remarks in
     * @param stepMeta   the stepMeta to help checking
     * @param prev       the fields coming from the previous step
     * @param input      the input step names
     * @param output     the output step names
     * @param info       the fields that are used as information by the step
     * @param space      the variable space to resolve variable expressions with
     * @param repository the repository to use to load Kettle metadata objects impacting the output fields
     * @param metaStore  the MetaStore to use to load additional external data or metadata impacting the output fields
     */
    @Override
    public void check(List<CheckResultInterface> remarks, TransMeta transMeta, StepMeta stepMeta, RowMetaInterface prev,
                      String[] input, String[] output, RowMetaInterface info, VariableSpace space,
                      Repository repository, IMetaStore metaStore) {
        Thread thread = Thread.currentThread();
        ClassLoader loader = thread.getContextClassLoader();
        try {
            thread.setContextClassLoader(LoaderPluginMeta.class.getClassLoader());
            if (database == null) {
                addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.NoConnection");
            }

            if (prev == null || prev.size() == 0) {
                addRemark(remarks, TYPE_RESULT_WARNING, stepMeta, "LoaderPluginMeta.NoFields");
            } else {
                // check mappings
                String idColumn = mappings.getIdColumn();
                if (StringUtils.isEmpty(idColumn)) {
                    addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.NoId");
                } else {
                    if (prev.searchValueMeta(idColumn) == null) {
                        addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.NoField", idColumn);
                    }
                }
                IArchetypeService service = getService();
                for (Mapping mapping : mappings.getMapping()) {
                    checkMapping(remarks, mapping, prev, stepMeta, service);
                }
                addRemark(remarks, TYPE_RESULT_OK, stepMeta, "LoaderPluginMeta.StepConnected", prev.size());
            }
            // See if we have input streams leading to this step
            if (input.length > 0) {
                addRemark(remarks, TYPE_RESULT_OK, stepMeta, "LoaderPluginMeta.StepReceiveInput");
            } else {
                addRemark(remarks, TYPE_RESULT_OK, stepMeta, "LoaderPluginMeta.StepReceiveNoInput");
            }
        } finally {
            thread.setContextClassLoader(loader);
        }
    }

    /**
     * Get the executing step, needed by Trans to launch a step.
     *
     * @param stepMeta  The step info
     * @param stepData  the step data interface linked to this step.  Here the
     *                  step can store temporary data, database connections, etc
     * @param copyNr    The copy nr to get
     * @param transMeta The transformation info
     * @param trans     The launching transformation
     * @return a new {@link LoaderPlugin}
     */
    public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepData,
                                 int copyNr, TransMeta transMeta, Trans trans) {
        LoaderPluginData data = (LoaderPluginData) stepData;
        data.setContext(getContext());
        return new LoaderPlugin(stepMeta, data, copyNr, transMeta, trans);
    }

    /**
     * Returns a new instance of the appropriate data class.
     * This data class implements the StepDataInterface.
     * It basically contains the persisting data that needs to live on, even if
     * a worker thread is terminated.
     *
     * @return a new {@link LoaderPluginData}
     */
    public StepDataInterface getStepData() {
        return new LoaderPluginData();
    }

    /**
     * Returns the mappings.
     *
     * @return the mappings
     */
    public Mappings getMappings() {
        return mappings;
    }

    /**
     * Sets the mappings.
     *
     * @param mappings the mappings
     */
    public void setMappings(Mappings mappings) {
        this.mappings = mappings;
    }

    /**
     * Gets the application context.
     *
     * @return the application context, or {@code null} if the load fails or
     * the database is not specified
     */
    protected ApplicationContext getContext() {
        ApplicationContext context = null;
        if (database != null) {
            try {
                context = ApplicationContextMgr.getContext(database, getLog());
            } catch (Exception exception) {
                logError("Failed to load application context", exception);
            }
        } else {
            logError("Failed to load application context", Messages.get("LoaderPluginMeta.NoConnection"));
        }

        return context;
    }

    /**
     * Helper to check a mapping.
     *
     * @param remarks  the remarks to add to
     * @param mapping  the mapping to check
     * @param row      the row
     * @param stepMeta the step meta
     * @param service  the archetype service. May be {@code null}
     */
    private void checkMapping(List<CheckResultInterface> remarks, Mapping mapping, RowMetaInterface row,
                              StepMeta stepMeta,
                              IArchetypeService service) {
        if (row.searchValueMeta(mapping.getSource()) == null) {
            addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.NoField", mapping.getSource());
        }
        org.openvpms.etl.load.Node node = NodeParser.parse(mapping.getTarget());
        if (node == null) {
            addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.InvalidMapping", mapping.getSource(),
                      mapping.getTarget());
        } else if (service != null) {
            while (node != null) {
                checkNode(remarks, node, mapping, stepMeta, service);
                node = node.getChild();
            }
        }
    }

    /**
     * Helper to check a node mapping.
     *
     * @param remarks  the remarks to add to
     * @param node     the node to check
     * @param mapping  the mapping to check
     * @param stepMeta the step meta
     * @param service  the archetype service
     */
    private void checkNode(List<CheckResultInterface> remarks, org.openvpms.etl.load.Node node,
                           Mapping mapping, StepMeta stepMeta, IArchetypeService service) {
        ArchetypeDescriptor archetype = service.getArchetypeDescriptor(node.getArchetype());
        if (archetype == null) {
            addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.InvalidArchetype",
                      node.getArchetype(), mapping.getTarget());
        } else {
            NodeDescriptor descriptor = archetype.getNodeDescriptor(node.getName());
            if (descriptor == null) {
                addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.InvalidNode",
                          node.getArchetype(), node.getName(), mapping.getTarget());
            } else {
                checkNode(remarks, node, mapping, stepMeta, descriptor);
            }
        }
    }

    /**
     * Checks a node.
     *
     * @param remarks    the remarks to add to
     * @param node       the node to check
     * @param mapping    the mapping to check
     * @param stepMeta   the step meta
     * @param descriptor the node descriptor
     */
    private void checkNode(List<CheckResultInterface> remarks, org.openvpms.etl.load.Node node, Mapping mapping,
                           StepMeta stepMeta, NodeDescriptor descriptor) {
        boolean checkReference = false;
        if (descriptor.isCollection()) {
            checkReference = true;
            if (node.getIndex() < 0) {
                addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.ExpectedCollection",
                          node.getArchetype(), node.getName(), mapping.getTarget());
            }
        } else {
            if (node.getIndex() >= 0) {
                addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.NodeNotCollection",
                          node.getArchetype(), node.getName(), mapping.getTarget());
            }
            if (descriptor.isObjectReference()) {
                checkReference = true;
            }
        }
        if (checkReference && !StringUtils.isEmpty(mapping.getValue())) {
            // replace any instances of $value with 'dummy' in order to check the reference
            String ref = LoaderHelper.replaceValue(mapping.getValue(),
                                                   "dummy");
            if (SymbolicReferenceParser.parse(ref) == null) {
                addRemark(remarks, TYPE_RESULT_ERROR, stepMeta, "LoaderPluginMeta.InvalidReference",
                          mapping.getValue(), mapping.getTarget());
            }
        }
    }

    /**
     * Helper to add a new {@code CheckResult} to remarks.
     *
     * @param remarks  the remarks
     * @param type     the result type
     * @param stepMeta the step meta
     * @param key      the localisation message key
     * @param args     the localisation message args
     */
    private void addRemark(List<CheckResultInterface> remarks, int type, StepMeta stepMeta, String key,
                           Object... args) {
        CheckResult remark = new CheckResult(type, Messages.get(key, args), stepMeta);
        remarks.add(remark);
    }

    /**
     * Helper to return the archetype service.
     *
     * @return the archetype service, or {@code null} if it cannot be initialised
     */
    private IArchetypeService getService() {
        ApplicationContext context = getContext();
        return (context != null) ? context.getBean("archetypeService", IArchetypeService.class) : null;
    }


}
