/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.openvpms.ws.util.ObjectMapperContextResolver;
import org.openvpms.ws.util.SLF4JLoggingFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ApplicationPath;
import java.util.TimeZone;

/**
 * The resource configuration for the booking API.
 *
 * @author Tim Anderson
 */
@ApplicationPath("ws/booking/v2")
public class BookingApplication extends ResourceConfig {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(BookingApplication.class);

    /**
     * Default constructor.
     */
    public BookingApplication() {
        register(JacksonFeature.class);
        register(new ObjectMapperContextResolver(TimeZone.getDefault()));
        register(BadRequestExceptionMapper.class);
        register(NotFoundExceptionMapper.class);
        register(LocationServiceImpl.class);
        register(ScheduleServiceImpl.class);
        register(UserServiceImpl.class);
        register(BookingServiceImpl.class);
        register(new SLF4JLoggingFeature(log));
    }

}
