/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentService;
import org.openvpms.archetype.rules.workflow.ScheduleTimes;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.booking.domain.Range;
import org.openvpms.booking.domain.ScheduleRange;
import org.openvpms.booking.domain.UserFreeBusy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Calendar to determine the free/busy times for users at a particular location, using their roster and appointments,
 * for the purposes of online booking.
 *
 * @author Tim Anderson
 */
public class BookingCalendar {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The roster service.
     */
    private final RosterService rosterService;

    /**
     * The appointment service.
     */
    private final AppointmentService appointmentService;

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * Constructs a {@link BookingCalendar}.
     *
     * @param service            the archetype service
     * @param rosterService      the roster service
     * @param appointmentService the appointment service
     * @param rules              the appointment rules
     */
    public BookingCalendar(ArchetypeService service, RosterService rosterService,
                           AppointmentService appointmentService, AppointmentRules rules) {
        this.service = service;
        this.rosterService = rosterService;
        this.appointmentService = appointmentService;
        this.rules = rules;
    }

    /**
     * Returns free time ranges for a user between two dates, for a particular location.
     * <p/>
     * This returns the time ranges in the periods where the user is rostered on, for each schedule in their
     * roster(s) at the practice location.
     * <p/>
     * If a user is busy in one schedule, the same slot in other schedules will not be free.
     *
     * @param user     the user
     * @param location the practice location
     * @param from     the start of the date range, in ISO date/time format
     * @param to       the end of the date range, in ISO date/time format
     * @return the free time ranges
     */
    public List<ScheduleRange> getFree(User user, Party location, String from, String to) {
        List<ScheduleRange> free = new ArrayList<>();
        query(location, user, from, to, free, null);
        return free;
    }

    /**
     * Returns busy time ranges for a user between two dates, for a particular location.
     * <p/>
     * This returns the time ranges in the periods where the user is rostered on, for each schedule in their
     * roster(s) at the practice location.
     * <p/>
     * If a user is busy in one schedule, this will be marked as busy in all schedules.
     *
     * @param user     the user
     * @param location the practice location
     * @param from     the start of the date range, in ISO date/time format
     * @param to       the end of the date range, in ISO date/time format
     * @return the busy time ranges
     */
    public List<ScheduleRange> getBusy(User user, Party location, String from, String to) {
        List<ScheduleRange> busy = new ArrayList<>();
        query(location, user, from, to, null, busy);
        return busy;
    }

    /**
     * Returns free and busy time ranges for a user between two dates, for a particular location.
     * <p/>
     * This returns the time ranges in the periods where the user is rostered on, for each schedule in their
     * roster(s) at the practice location.
     * <p/>
     * If a user is busy in one schedule:
     * <ul>
     *     <li>the same slot in other schedules will not be free</li>
     *     <li>the slot will be busy in all other schedules.</li>
     * </ul>
     *
     * @param user     the user
     * @param location the practice location
     * @param from     the start of the date range, in ISO date/time format
     * @param to       the end of the date range, in ISO date/time format
     * @return the free and busy time ranges
     */
    public UserFreeBusy getFreeBusy(User user, Party location, String from, String to) {
        List<ScheduleRange> free = new ArrayList<>();
        List<ScheduleRange> busy = new ArrayList<>();
        query(location, user, from, to, free, busy);
        return new UserFreeBusy(user.getId(), free, busy);
    }

    /**
     * Queries free/busy ranges between the from and to dates for a user.
     * <p/>
     * This returns ranges in the periods where the user is rostered on, for each schedule in their roster.
     *
     * @param location the practice location
     * @param user     the user
     * @param from     the from date
     * @param to       the to date
     * @param free     the free ranges to add to, or {@code null} if they aren't being queried
     * @param busy     the busy ranges to add to, or {@code null} if they aren't being queried
     */
    private void query(Party location, User user, String from, String to, List<ScheduleRange> free,
                       List<ScheduleRange> busy) {
        Date fromTime = DateHelper.getDate("from", from);
        Date toTime = DateHelper.getDate("to", to);

        Map<Entity, List<ScheduleEntityRange>> roster = getRosterEvents(user, location, fromTime, toTime);
        List<ScheduleTimes> allAppointments = appointmentService.getAppointmentsForClinician(user, fromTime, toTime);
        allAppointments = new ArrayList<>(allAppointments);
        Collections.sort(allAppointments);

        Map<Entity, AppointmentSchedule> schedules = new HashMap<>();

        for (Date date = fromTime; date.compareTo(toTime) < 0; date = DateRules.getDate(date, 1, DateUnits.DAYS)) {
            Date startTime = DateRules.getDate(date);
            Date endTime = DateRules.getNextDate(startTime);
            Date min = DateRules.max(startTime, fromTime);
            Date max = DateRules.min(endTime, toTime);

            query(date, min, max, allAppointments, roster, free, busy, schedules);
        }
    }

    /**
     * Queries appointments for a date.
     *
     * @param date            the date
     * @param min             the minimum time on the date
     * @param max             the maximum time on the date
     * @param allAppointments all appointments for the clinician
     * @param roster          the roster for the clinician
     * @param free            the free ranges to add to, or {@code null} if they aren't being queried
     * @param busy            the busy ranges to add to, or {@code null} if they aren't being queried
     * @param schedules       collects the appointment schedules
     */
    private void query(Date date, Date min, Date max, List<ScheduleTimes> allAppointments,
                       Map<Entity, List<ScheduleEntityRange>> roster, List<ScheduleRange> free,
                       List<ScheduleRange> busy, Map<Entity, AppointmentSchedule> schedules) {
        List<ScheduleRange> appointments = getAppointments(date, allAppointments);
        for (Map.Entry<Entity, List<ScheduleEntityRange>> entry : roster.entrySet()) {
            List<ScheduleRange> events = getRoster(date, entry.getValue());
            AppointmentSchedule schedule = schedules.computeIfAbsent(
                    entry.getKey(), entity -> new AppointmentSchedule(entity, service, rules));

            if (free != null && !events.isEmpty()) {
                // can only have free ranges when rostered on

                // verify the schedule is available for the date
                Date scheduleStart = schedule.getStartTime(date);
                Date scheduleEnd = (scheduleStart != null) ? schedule.getEndTime(date) : null;
                if (scheduleStart != null && scheduleEnd != null) {
                    // restrict the minimum and maximum times to the schedule availability for the day
                    Date scheduleMin = DateRules.max(min, scheduleStart);
                    Date scheduleMax = DateRules.min(max, scheduleEnd);
                    List<ScheduleRange> slots = getFree(schedule.getId(), events, appointments, scheduleMin,
                                                        scheduleMax);
                    free.addAll(slots);
                }
            }
            if (busy != null && !appointments.isEmpty()) {
                busy.addAll(getBusy(schedule.getId(), appointments));
            }
        }
    }

    /**
     * Returns the roster events for a user at a practice location, between the specified date, keyed on
     * schedule identifier.
     *
     * @param user     the user
     * @param location the practice location
     * @param from     the from date
     * @param to       the to date
     * @return the roster events
     */
    private Map<Entity, List<ScheduleEntityRange>> getRosterEvents(User user, Party location, Date from, Date to) {
        List<RosterService.UserEvent> roster = rosterService.getUserEvents(user, location, from, to);
        Map<Entity, List<ScheduleEntityRange>> result = new LinkedHashMap<>();
        for (RosterService.UserEvent event : roster) {
            for (Entity schedule : getSchedules(event.getArea())) {
                List<ScheduleEntityRange> list = result.computeIfAbsent(schedule, k -> new ArrayList<>());
                list.add(createRange(schedule, event.getStartTime(), event.getEndTime()));
            }
        }
        return result;
    }

    /**
     * Returns the schedules associated with a roster area.
     *
     * @param area the roster area reference
     * @return the schedule identifiers
     */
    private List<Entity> getSchedules(Reference area) {
        List<Entity> schedules = rosterService.getSchedules(area);
        if (schedules.size() > 1) {
            schedules.sort(Comparator.comparingLong(IMObject::getId));
        }
        return schedules;
    }

    /**
     * Returns date ranges for all appointments falling on a date.
     *
     * @param date            the date
     * @param allAppointments all appointments for the clinician
     * @return the date ranges
     */
    private List<ScheduleRange> getAppointments(Date date, List<ScheduleTimes> allAppointments) {
        List<ScheduleRange> result = new ArrayList<>();
        Date min = DateRules.getDate(date);
        Date max = DateRules.getNextDate(min);
        for (ScheduleTimes appointment : allAppointments) {
            addRange(result, appointment.getStartTime(), appointment.getEndTime(), min, max,
                     appointment.getScheduleId());
        }
        return result;
    }

    /**
     * Collects free ranges.
     *
     * @param scheduleId   the schedule identifier
     * @param events       the roster events for the user on the date for the schedule
     * @param appointments the appointments for the user on the date
     * @param min          the minimum date/time. Any range prior to this should be discarded or truncated if it
     *                     overlaps
     * @param max          the maximum date/time. Any range after to this should be discarded or truncated if it
     *                     overlaps
     * @return the free ranges
     */
    private List<ScheduleRange> getFree(long scheduleId, List<ScheduleRange> events, List<ScheduleRange> appointments,
                                        Date min, Date max) {
        List<ScheduleRange> free = new ArrayList<>();
        Date freeStart = null;

        Date freeEnd = null;
        for (ScheduleRange event : events) {
            Date eventStart = event.getStart();
            Date eventEnd = event.getEnd();
            if (freeStart == null || DateRules.compareTo(eventStart, freeEnd) > 0) {
                if (freeStart != null) {
                    addRange(free, freeStart, freeEnd, min, max, scheduleId);
                }
                freeStart = eventStart;
                freeEnd = eventEnd;
            } else if (DateRules.compareTo(eventEnd, freeEnd) > 0) {
                freeEnd = eventEnd;
            }
        }
        if (freeStart != null) {
            addRange(free, freeStart, freeEnd, min, max, scheduleId);
        }
        subtractAppointments(scheduleId, free, appointments);
        return free;
    }

    /**
     * Subtracts appointments from the free ranges.
     *
     * @param scheduleId   the schedule identifier
     * @param free         the free ranges
     * @param appointments the appointments
     */
    private void subtractAppointments(long scheduleId, List<ScheduleRange> free, List<ScheduleRange> appointments) {
        for (Range appointment : appointments) {
            ListIterator<ScheduleRange> iterator = free.listIterator();
            Date appointmentStart = appointment.getStart();
            Date appointmentEnd = appointment.getEnd();
            while (iterator.hasNext()) {
                Range range = iterator.next();
                Date freeStart = range.getStart();
                Date freeEnd = range.getEnd();
                if (appointmentEnd.compareTo(freeStart) <= 0) {
                    // appointment before free ranges, so skip to next appointment
                    break;
                } else if (appointmentStart.compareTo(freeEnd) < 0) {
                    int startToStart = appointmentStart.compareTo(freeStart);
                    int endToEnd = appointmentEnd.compareTo(freeEnd);
                    if (startToStart <= 0 && endToEnd < 0) {
                        // appointment overlaps start of free range
                        iterator.set(createRange(scheduleId, appointmentEnd, freeEnd));
                    } else if (startToStart <= 0) {  // endToEnd >= 0
                        // appointment covers free range
                        iterator.remove();
                    } else if (endToEnd < 0) {       // startToStart > 0
                        // appointment is within free range
                        iterator.set(createRange(scheduleId, freeStart, appointmentStart));
                        iterator.add(createRange(scheduleId, appointmentEnd, freeEnd));
                    } else {
                        // appointment overlaps end of free range
                        iterator.set(createRange(scheduleId, freeStart, appointmentStart));
                    }
                }
            }
        }
    }

    /**
     * Generates busy ranges from a set of appointments.
     * <p/>
     * This assumes that the user cannot work on multiple appointments at once so if a user is rostered to multiple
     * schedules, an appointment's times will be added to each.
     * <p/>
     * Only those appointments for the same schedule will be marked busy.
     *
     * @param scheduleId   the schedule identifier
     * @param appointments the appointments, from multiple schedules
     */
    private List<ScheduleRange> getBusy(long scheduleId, List<ScheduleRange> appointments) {
        List<ScheduleRange> busy = new ArrayList<>();
        boolean modified = false;
        for (ScheduleRange appointment : appointments) {
            if (appointment.getSchedule() == scheduleId) {
                ListIterator<ScheduleRange> iterator = busy.listIterator();
                Date appointmentStart = appointment.getStart();
                Date appointmentEnd = appointment.getEnd();
                boolean found = false;
                while (iterator.hasNext()) {
                    ScheduleRange range = iterator.next();
                    Date busyStart = range.getStart();
                    Date busyEnd = range.getEnd();
                    if (DateRules.intersects(appointmentStart, appointmentEnd, busyStart, busyEnd)) {
                        // expand the range to include the appointment
                        iterator.set(createRange(range.getSchedule(), DateRules.min(appointmentStart, busyStart),
                                                 DateRules.max(appointmentEnd, busyEnd)));
                        found = true;
                        modified = true;
                    }
                }
                if (!found) {
                    // insert the appointment into the busy range
                    ScheduleRange range = createRange(scheduleId, appointmentStart, appointmentEnd);
                    int index = Collections.binarySearch(busy, range,
                                                         (o1, o2) -> DateRules.compareTo(o1.getStart(), o2.getStart()));
                    if (index < 0) {
                        // new value to insert
                        index = -index - 1;
                    }
                    busy.add(index, range);
                    modified = true;
                }
            }
        }
        if (modified) {
            // merge overlapping ranges
            busy = mergeRanges(scheduleId, busy);
        }
        return busy;
    }

    /**
     * Merges overlapping ranges.
     *
     * @param scheduleId the schedule id
     * @param ranges     the ranges to merge
     * @return the merged ranges
     */
    private List<ScheduleRange> mergeRanges(long scheduleId, List<ScheduleRange> ranges) {
        List<ScheduleRange> merged = new ArrayList<>();
        Date start = null;
        Date end = null;

        for (ScheduleRange range : ranges) {
            if (start == null) {
                start = range.getStart();
                end = range.getEnd();
            } else if (end.compareTo(range.getStart()) >= 0) {
                end = DateRules.max(range.getEnd(), end);
            } else {
                merged.add(createRange(scheduleId, start, end));
                start = range.getStart();
                end = range.getEnd();
            }
        }
        merged.add(createRange(scheduleId, start, end));
        return merged;
    }

    /**
     * Returns the roster events for a user for date.
     *
     * @param date   the date
     * @param events all events for the user for a schedule
     * @return the events
     */
    private List<ScheduleRange> getRoster(Date date, List<ScheduleEntityRange> events) {
        List<ScheduleRange> result = new ArrayList<>();
        Date min = DateRules.getDate(date);
        Date max = DateRules.getNextDate(min);
        for (ScheduleRange event : events) {
            addRange(result, event.getStart(), event.getEnd(), min, max, event.getSchedule());
        }
        return result;
    }

    /**
     * Adds a range, if it intersects the min..max range. If it overlaps, it will be truncated.
     *
     * @param ranges   the ranges to add to
     * @param from     the start of the range
     * @param to       the end of the range
     * @param min      the minimum
     * @param max      the maximum
     * @param schedule the schedule that the range is for
     */
    private void addRange(List<ScheduleRange> ranges, Date from, Date to, Date min, Date max, long schedule) {
        if (DateRules.compareTo(to, min) >= 0) {
            if (DateRules.compareTo(from, min) <= 0) {
                from = min;
            }
            if (DateRules.compareTo(from, max) < 0) {
                if (DateRules.compareTo(to, max) > 0) {
                    to = max;
                }
                if (from.compareTo(to) < 0) {
                    ranges.add(createRange(schedule, from, to));
                }
            }
        }
    }

    /**
     * Creates a schedule range.
     *
     * @param schedule the schedule
     * @param from     the start time
     * @param to       the end time
     * @return a new range
     */
    private ScheduleRange createRange(long schedule, Date from, Date to) {
        from = DateHelper.convert(from); // make sure Timestamps are converted to Dates in the local timezone
        to = DateHelper.convert(to);
        return new ScheduleRange(schedule, from, to);
    }

    /**
     * Creates a schedule range.
     *
     * @param schedule the schedule
     * @param from     the start time
     * @param to       the end time
     * @return a new range
     */
    private ScheduleEntityRange createRange(Entity schedule, Date from, Date to) {
        from = DateHelper.convert(from); // make sure Timestamps are converted to Dates in the local timezone
        to = DateHelper.convert(to);
        return new ScheduleEntityRange(schedule, from, to);
    }

}
