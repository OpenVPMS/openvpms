/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.Times;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.booking.api.BookingService;
import org.openvpms.booking.domain.Booking;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.customer.CustomerQuery;
import org.openvpms.domain.service.customer.Customers;
import org.openvpms.domain.service.patient.Patients;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.util.Date;

/**
 * Appointment booking service.
 *
 * @author Tim Anderson
 */
@Component
public class BookingServiceImpl implements BookingService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The customers service.
     */
    private final Customers customers;

    /**
     * The patients service.
     */
    private final Patients patients;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * Contact rules.
     */
    private final Contacts contacts;

    /**
     * The appointment rules
     */
    private final AppointmentRules appointmentRules;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Invalid booking reference message.
     */
    private static final String INVALID_BOOKING_REFERENCE = "Invalid booking reference";

    /**
     * Constructs a {@link BookingServiceImpl}.
     *
     * @param service            the archetype service
     * @param appointmentRules   the appointment rules
     * @param userRules          the user rules
     * @param transactionManager the transaction manager
     */
    public BookingServiceImpl(IArchetypeService service, Customers customers, Patients patients,
                              DomainService domainService, AppointmentRules appointmentRules, UserRules userRules,
                              PlatformTransactionManager transactionManager) {
        this.service = service;
        this.customers = customers;
        this.patients = patients;
        this.domainService = domainService;
        contacts = new Contacts(service);
        this.appointmentRules = appointmentRules;
        this.userRules = userRules;
        this.transactionManager = transactionManager;
    }

    /**
     * Creates a new appointment from a booking request.
     *
     * @param booking the booking
     * @param uriInfo the URI info
     * @return the appointment reference
     */
    @Override
    public Response create(Booking booking, UriInfo uriInfo) {
        if (booking == null) {
            throw new BadRequestException("Booking is required");
        }
        Date start = getRequired("start", booking.getStart());
        Date end = getRequired("end", booking.getEnd());
        checkBookingDates(start, end);

        Entity schedule = getSchedule(booking);
        Entity appointmentType = getAppointmentType(booking);
        checkBookingSlot(start, end, schedule);

        StringBuilder notes = new StringBuilder();
        Customer customer = getCustomer(booking);
        Patient patient = null;
        if (customer == null) {
            addNewCustomerNotes(booking, notes);
        } else {
            patient = getPatient(customer, booking);
        }
        if (patient == null && !StringUtils.isEmpty(booking.getPatientName())) {
            append(notes, "Patient", booking.getPatientName());
        }
        User user = getUser(booking);

        Act act = create(booking, start, end, schedule, appointmentType, notes, customer, patient, user);
        String reference = act.getId() + ":" + act.getLinkId();
        // require both the id, and linkId, to make it harder to cancel appointments not created through the service
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(reference);
        return Response.created(builder.build()).type(MediaType.TEXT_PLAIN).entity(reference).build();
    }

    /**
     * Cancels a booking.
     *
     * @param reference the booking reference
     * @return a 204 response on success
     * @throws BadRequestException if the booking doesn't exist, or the associated appointment isn't pending
     */
    @Override
    public Response cancel(String reference) {
        Act act = getAppointment(reference);
        if (!WorkflowStatus.PENDING.equals(act.getStatus())) {
            throw new BadRequestException("The booking must be pending to cancel");
        }
        act.setStatus(ActStatus.CANCELLED);
        service.save(act);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    /**
     * Returns a booking given its reference.
     *
     * @param reference the booking reference
     * @return the booking
     * @throws BadRequestException if the booking reference is invalid
     * @throws NotFoundException   if the booking cannot be found
     */
    @Override
    public Booking getBooking(String reference) {
        if (reference == null) {
            throw new BadRequestException(INVALID_BOOKING_REFERENCE);
        }
        Act act = getAppointment(reference);
        IMObjectBean bean = service.getBean(act);
        Entity schedule = bean.getTarget("schedule", Entity.class);
        Party customer = bean.getTarget("customer", Party.class);
        Reference appointmentType = bean.getTargetRef("appointmentType");
        Reference clinician = bean.getTargetRef("clinician");
        Booking booking = new Booking();
        if (schedule != null) {
            IMObjectBean scheduleBean = service.getBean(schedule);
            booking.setSchedule(schedule.getId());
            Reference location = scheduleBean.getTargetRef("location");
            if (location != null) {
                booking.setLocation(location.getId());
            }
        }
        booking.setStart(act.getActivityStartTime());
        booking.setEnd(act.getActivityEndTime());
        if (customer != null) {
            Customer c = domainService.create(customer, Customer.class);
            booking.setTitle(c.getTitleCode());
            booking.setFirstName(c.getFirstName());
            booking.setLastName(c.getLastName());

            // NOTE: the mobile, phone and email could be different to those requested
            booking.setMobile(getPhone(c.getMobilePhone()));
            booking.setPhone(getPhone(c.getPhone()));
            booking.setEmail(getEmail(c.getEmail()));
        }
        if (appointmentType != null) {
            booking.setAppointmentType(appointmentType.getId());
        }
        Party patient = bean.getTarget("patient", Party.class);
        if (patient != null) {
            booking.setPatientName(patient.getName());
        }
        if (clinician != null) {
            booking.setUser(clinician.getId());
        }
        return booking;
    }

    /**
     * Saves the appointment after ensuring there is no double booking.
     *
     * @param act      the appointment
     * @param schedule the schedule
     */
    protected void save(Act act, Entity schedule) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                Times existing = appointmentRules.getOverlap(act.getActivityStartTime(), act.getActivityEndTime(),
                                                             schedule);
                if (existing != null) {
                    throw new BadRequestException("An appointment is already scheduled for "
                                                  + existing.getStartTime() + "-" + existing.getEndTime());
                }
                service.save(act);
            }
        });
    }

    /**
     * Creates the appointment for a booking.
     *
     * @param booking         the booking
     * @param start           the appointment start time
     * @param end             the appointment end time
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     * @param notes           the booking notes
     * @param customer        the customer. May be {@code null}
     * @param patient         the patient. May be {@code null}
     * @param user            the user the appointment is for. May be {@code null}
     * @return the appointment
     */
    private Act create(Booking booking, Date start, Date end, Entity schedule, Entity appointmentType,
                       StringBuilder notes, Party customer, Party patient, User user) {
        Act act = service.create(ScheduleArchetypes.APPOINTMENT, Act.class);
        IMObjectBean bean = service.getBean(act);
        bean.setValue("startTime", start);
        bean.setValue("endTime", end);
        bean.setTarget("schedule", schedule);
        if (customer != null) {
            bean.setTarget("customer", customer);
        }
        bean.setTarget("appointmentType", appointmentType);
        String reason = getReason(appointmentType);
        if (reason != null) {
            bean.setValue("reason", reason);
        }

        if (patient != null) {
            bean.setTarget("patient", patient);
        }
        if (user != null) {
            if (userRules.isClinician(user)) {
                bean.setTarget("clinician", user);
            } else {
                // appointment with a user who isn't a clinician (e.g. a puppy class booking).
                // This isn't directly supported so make a note.
                append(notes, "User", user.getName());
            }
        }
        bean.setValue("onlineBooking", true);

        if (!StringUtils.isEmpty(booking.getNotes())) {
            append(notes, "Notes", booking.getNotes());
        }

        String bookingNotes = StringUtils.abbreviate(notes.toString(), 5000);
        if (!bookingNotes.isEmpty()) {
            bean.setValue("bookingNotes", bookingNotes);
        }
        if (customer != null && appointmentRules.isRemindersEnabled(schedule)
            && appointmentRules.isRemindersEnabled(appointmentType) && contacts.canSMS(customer)) {
            Period noReminderPeriod = appointmentRules.getNoReminderPeriod();
            if (noReminderPeriod != null) {
                Date date = DateRules.plus(new Date(), noReminderPeriod);
                if (start.after(date)) {
                    bean.setValue("sendReminder", true);
                }
            }
        }
        save(act, schedule);
        return act;
    }

    /**
     * Returns the appointment type reason code.
     *
     * @param appointmentType the appointment type
     * @return the reason code. May be {@code null}
     */
    private String getReason(Entity appointmentType) {
        IMObjectBean bean = service.getBean(appointmentType);
        Lookup reason = bean.getObject("reason", Lookup.class);
        return (reason != null) ? reason.getCode() : null;
    }

    /**
     * Adds notes for a new customer.
     *
     * @param booking the booking
     * @param notes   the notes to add to
     */
    private void addNewCustomerNotes(Booking booking, StringBuilder notes) {
        if (!StringUtils.isEmpty(booking.getTitle())) {
            append(notes, "Title", booking.getTitle());
        }
        append(notes, "First Name", booking.getFirstName());
        append(notes, "Last Name", booking.getLastName());
        if (!StringUtils.isEmpty(booking.getPhone())) {
            append(notes, "Phone", booking.getPhone());
        }
        if (!StringUtils.isEmpty(booking.getMobile())) {
            append(notes, "Mobile", booking.getMobile());
        }
        if (!StringUtils.isEmpty(booking.getEmail())) {
            append(notes, "Email", booking.getEmail());
        }
    }

    /**
     * Verifies that the booking is for the future and the start is less than the end.
     *
     * @param start the booking start
     * @param end   the booking end
     * @throws BadRequestException if the dates are invalid
     */
    private void checkBookingDates(Date start, Date end) {
        Date now = new Date();
        if (start.compareTo(now) <= 0) {
            throw new BadRequestException("Cannot make a booking in the past");
        }
        if (end.compareTo(start) <= 0) {
            throw new BadRequestException("Booking start must be less than end");
        }
    }

    /**
     * Verifies the booking falls on slot boundaries.
     *
     * @param start    the booking start
     * @param end      the booking end
     * @param schedule the schedule
     * @throws BadRequestException if the dates are invalid
     */
    private void checkBookingSlot(Date start, Date end, Entity schedule) {
        int slotSize = appointmentRules.getSlotSize(schedule);
        Date slotStart = appointmentRules.getSlotTime(start, slotSize, false);
        if (slotStart.compareTo(start) != 0) {
            throw new BadRequestException("Booking start is not on a slot boundary");
        }
        Date slotEnd = appointmentRules.getSlotTime(end, slotSize, true);
        if (slotEnd.compareTo(end) != 0) {
            throw new BadRequestException("Booking end is not on a slot boundary");
        }
    }

    /**
     * Verifies that an entity is available for online booking.
     * <p/>
     * This ensures that it is both active and has its online booking flag set.
     *
     * @param bean the object
     * @throws BadRequestException if the entity isn't available for online booking
     */
    private void checkAvailable(IMObjectBean bean) {
        IMObject object = bean.getObject();
        if (!object.isActive() || !bean.getBoolean("onlineBooking")) {
            throw new BadRequestException(object.getName() + " is not available for booking");
        }
    }

    /**
     * Returns an appointment given the booking reference.
     *
     * @param reference the booking reference
     * @return the appointment
     * @throws BadRequestException if the appointment cannot be found
     */
    private Act getAppointment(String reference) {
        String[] parts = reference.split(":");
        if (parts.length != 2) {
            throw new BadRequestException(INVALID_BOOKING_REFERENCE);
        }
        long appointmentId;
        try {
            appointmentId = Long.parseLong(parts[0]);
        } catch (NumberFormatException exception) {
            throw new BadRequestException(INVALID_BOOKING_REFERENCE);
        }
        Act act = (Act) service.get(ScheduleArchetypes.APPOINTMENT, appointmentId);
        if (act == null || !act.getLinkId().equals(parts[1]) || ActStatus.CANCELLED.equals(act.getStatus())) {
            throw new NotFoundException("Booking not found");
        }
        return act;
    }

    /**
     * Returns the schedule.
     * <p/>
     * This verifies that the schedule and corresponding location are still active, and available for online booking.
     *
     * @param booking the booking request
     * @return the schedule
     * @throws BadRequestException if the schedule is invalid
     */
    private Entity getSchedule(Booking booking) {
        Entity schedule = getRequired("Schedule", booking.getSchedule(), ScheduleArchetypes.ORGANISATION_SCHEDULE);
        IMObjectBean bean = service.getBean(schedule);
        checkAvailable(bean);
        Party location = bean.getTarget("location", Party.class);
        if (location == null || location.getId() != booking.getLocation()) {
            throw new BadRequestException("Schedule is not available at location " + booking.getLocation());
        }
        checkAvailable(service.getBean(location));
        return schedule;
    }

    /**
     * Returns the customer.
     * <p/>
     * This tries to match on:
     * <ul>
     *     <li>first and last name + 1 contact; or if no match</li>
     *     <li>last name + 2 contacts</li>
     * </ul>
     *
     * @param booking the booking request
     * @return the customer, or {@code null} if the customer can't be found
     * @throws BadRequestException if the booking is incomplete
     */
    private Customer getCustomer(Booking booking) {
        String firstName = getRequired("firstName", booking.getFirstName());
        String lastName = getRequired("lastName", booking.getLastName());
        Match match = getMatchOnFirstAndLastName(booking, firstName, lastName);
        if (match.getCustomer() == null && !match.isDuplicate()) {
            // don't requery if multiple customers matched
            match = getMatchOnLastName(booking, lastName);
        }
        return match.getCustomer();
    }

    /**
     * Returns the customer that matches on first and last name, and at least one contact.
     *
     * @param booking   the booking
     * @param firstName the first name
     * @param lastName  the last name
     * @return the corresponding customer, if any
     */
    private Match getMatchOnFirstAndLastName(Booking booking, String firstName, String lastName) {
        CustomerQuery query = newCustomerQuery().name(lastName, firstName);
        return getCustomer(query, booking, 1);
    }

    /**
     * Returns the customer that matches on last name, and at least two contacts.
     *
     * @param booking  the booking
     * @param lastName the last name
     * @return the corresponding customer, if any
     */
    private Match getMatchOnLastName(Booking booking, String lastName) {
        CustomerQuery query = newCustomerQuery().name(Filter.equal(lastName), null);
        return getCustomer(query, booking, 2);
    }

    /**
     * Returns the customer that matches the query, and at least {@code count} contacts.
     * <p/>
     * If multiple customers match, none will be returned, and the match will indicate a duplicate.
     *
     * @param query   the query
     * @param booking the booking
     * @param count   the number of contacts that must match
     * @return the corresponding customer
     */
    private Match getCustomer(CustomerQuery query, Booking booking, int count) {
        Match result = null;
        for (Customer customer : query.query()) {
            if (matchesContacts(customer, booking, count)) {
                if (result != null) {
                    result = Match.duplicate(); // multiple matches for the details, so don't return any
                    break;
                }
                result = Match.match(customer);
            }
        }
        return result != null ? result : Match.none();
    }

    /**
     * Returns a query that selects active customers.
     *
     * @return a new query
     */
    private CustomerQuery newCustomerQuery() {
        return customers.getQuery().active().orderById();
    }

    /**
     * Returns the patient associated with a customer in a booking request.
     *
     * @param customer the customer
     * @param booking  the booking request
     * @return the patient, or {@code null} if no patient was specified or the patient could not be found
     */
    private Patient getPatient(Customer customer, Booking booking) {
        Patient match = null;
        String name = StringUtils.trimToNull(booking.getPatientName());
        if (name != null) {
            Iterable<Patient> matches = patients.getQuery()
                    .name(name)
                    .owner(customer)
                    .active()
                    .orderById()
                    .query();
            for (Patient patient : matches) {
                match = patient;
                break;
            }
        }
        return match;
    }

    /**
     * Determines if a booking matches the contacts for a customer.
     *
     * @param customer the customer
     * @param booking  the booking
     * @param count    the minimum number of contact matches
     * @return {@code true} if the minimum no. of contacts match, otherwise {@code false}
     */
    private boolean matchesContacts(Customer customer, Booking booking, int count) {
        int match = 0;
        String bookingPhone = Contacts.getPhone(booking.getPhone());
        String bookingMobile = Contacts.getPhone(booking.getMobile());
        String bookingEmail = StringUtils.trimToNull(booking.getEmail());
        for (Phone phone : customer.getPhones()) {
            String number = phone.getPhoneNumber();
            if (number != null && (number.equals(bookingPhone) || number.equals(bookingMobile))) {
                match++;
                if (match >= count) {
                    break;
                }
            }
        }
        if (match < count && bookingEmail != null) {
            for (Email email : customer.getEmails()) {
                String address = email.getEmailAddress();
                if (address != null && address.equalsIgnoreCase(bookingEmail)) {
                    match++;
                    if (match >= count) {
                        break;
                    }
                }
            }
        }
        return match >= count;
    }

    /**
     * Returns the appointment type.
     * <p/>
     * Note that the appointment type may no longer be available for online booking (i.e. its onlineBooking flag
     * may be off).<p/>
     * In this case, the appointment can still be made, the rationale being that the clinic will rebook if necessary.
     *
     * @param booking the booking request
     * @return the customer
     * @throws BadRequestException if the appointment type cannot be found
     */
    private Entity getAppointmentType(Booking booking) {
        return getRequired("Appointment Type", booking.getAppointmentType(), ScheduleArchetypes.APPOINTMENT_TYPE);
    }

    /**
     * Returns the user.
     *
     * @param booking the booking request
     * @return the user, or {@code null} if no user is specified
     * @throws BadRequestException if the user cannot be found
     */
    private User getUser(Booking booking) {
        long userId = booking.getUser();
        return (userId > 0) ? (User) getRequired("User", userId, UserArchetypes.USER) : null;
    }

    /**
     * Helper to append a key and value pair to booking notes, separated by new lines.
     *
     * @param notes the booking notes
     * @param key   the key
     * @param value the value
     */
    private void append(StringBuilder notes, String key, String value) {
        if (notes.length() != 0) {
            notes.append('\n');
        }
        notes.append(key).append(": ").append(value);
    }

    /**
     * Returns a required entity.
     *
     * @param name      the name of the value
     * @param id        the entity id
     * @param archetype the entity archetype
     * @return the corresponding entity
     * @throws BadRequestException if it cannot be found
     */
    private Entity getRequired(String name, long id, String archetype) {
        Entity result = service.get(archetype, id, Entity.class);
        if (result == null) {
            throw new BadRequestException(name + " not found: " + id);
        }
        return result;
    }

    /**
     * Returns a required date value.
     *
     * @param name  the name of the value
     * @param value the value
     * @return the date
     * @throws BadRequestException if the value is missing
     */
    private Date getRequired(String name, Date value) {
        if (value == null) {
            throw new BadRequestException("'" + name + "' is a required field");
        }
        return value;
    }

    /**
     * Returns a required string value.
     *
     * @param name  the name of the value
     * @param value the value
     * @return the value
     * @throws BadRequestException if the value is missing
     */
    private String getRequired(String name, String value) {
        value = StringUtils.trimToNull(value);
        if (value == null) {
            throw new BadRequestException("'" + name + "' is a required field");
        }
        return value;
    }

    /**
     * Returns a phone number.
     *
     * @param phone the phone contact. May be {@code null}
     * @return the phone number, or an empty string if none is present
     */
    private String getPhone(Phone phone) {
        String result = phone != null ? phone.getPhoneNumber() : null;
        return result != null ? result : "";
    }

    /**
     * Returns an email address.
     *
     * @param email the email contact. May be {@code null}
     * @return the email address, or an empty string if none is present
     */
    private String getEmail(Email email) {
        String result = email != null ? email.getEmailAddress() : null;
        return result != null ? result : "";
    }

    /**
     * Customer match.
     */
    private static class Match {

        private final Customer customer;

        private final boolean duplicate;

        private Match() {
            customer = null;
            duplicate = true;
        }

        private Match(Customer customer) {
            this.customer = customer;
            duplicate = false;
        }

        /**
         * Determines if duplicate customers were detected.
         *
         * @return {@code true} if duplicate customers were detected, otherwise {@code false}
         */
        public boolean isDuplicate() {
            return duplicate;
        }

        /**
         * Returns the matching customer.
         *
         * @return the matching customer, or {@code null} if none/duplicate matches were found
         */
        public Customer getCustomer() {
            return customer;
        }

        /**
         * Creates a match for a customer.
         *
         * @param customer the customer
         * @return a new match
         */
        public static Match match(Customer customer) {
            return new Match(customer);
        }

        /**
         * Creates a duplicate match.
         *
         * @return a new match
         */
        public static Match duplicate() {
            return new Match();
        }

        /**
         * Creates an instance indicating no match was found.
         *
         * @return a new match
         */
        public static Match none() {
            return new Match(null);
        }
    }
}
