/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.booking.domain.AppointmentType;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Appointment schedule.
 *
 * @author Tim Anderson
 */
class AppointmentSchedule {

    /**
     * The schedule bean.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * Determines if online booking times needs to be initialised.
     */
    private boolean initTimes = true;

    /**
     * The online booking times bean,
     */
    private IMObjectBean timesBean;

    /**
     * Constructs an {@link AppointmentSchedule}.
     *
     * @param schedule the schedule
     * @param service  the archetype service
     * @param rules    the appointment rules
     */
    public AppointmentSchedule(Entity schedule, ArchetypeService service, AppointmentRules rules) {
        bean = service.getBean(schedule);
        this.service = service;
        this.rules = rules;
    }

    /**
     * Returns the schedule identifier.
     *
     * @return the schedule identifier
     */
    public long getId() {
        return bean.getObject().getId();
    }

    /**
     * Returns the appointment types available to the schedule for online booking.
     *
     * @return the appointment types
     */
    public List<AppointmentType> getAppointmentTypes() {
        List<AppointmentType> result = new ArrayList<>();
        for (IMObject relationship : bean.getValues("appointmentTypes")) {
            IMObjectBean relationshipBean = service.getBean(relationship);
            IMObject appointmentType = relationshipBean.getObject("target");
            if (appointmentType != null && appointmentType.isActive()) {
                IMObjectBean appointmentTypeBean = service.getBean(appointmentType);
                int slots = relationshipBean.getInt("noSlots");
                if (appointmentTypeBean.getBoolean("onlineBooking") && slots > 0) {
                    result.add(new AppointmentType(appointmentType.getId(), appointmentType.getName(), slots));
                }
            }
        }
        return result;
    }

    /**
     * Returns the schedule start time for a date.
     *
     * @param date the date
     * @return the start time, or {@code null} if the schedule is not available on the date
     */
    public Date getStartTime(Date date) {
        Date startTime;
        IMObjectBean times = getOnlineBookingTimes();
        boolean open;
        if (times != null) {
            String key = getNodePrefix(date);
            open = times.getBoolean(key + "Open");
            startTime = (open) ? times.getDate(key + "StartTime") : null;
        } else {
            open = true;
            startTime = bean.getDate("startTime");
        }
        if (open) {
            if (startTime == null) {
                startTime = date;
            } else {
                startTime = DateRules.addDateTime(date, startTime);
            }
        }
        return startTime;
    }

    /**
     * Returns the schedule end time for a date.
     *
     * @param date the date
     * @return the end time, or {@code null} if the schedule is not available on the date
     */
    public Date getEndTime(Date date) {
        Date endTime;
        boolean open;
        IMObjectBean times = getOnlineBookingTimes();
        if (times != null) {
            String key = getNodePrefix(date);
            open = times.getBoolean(key + "Open");
            endTime = (open) ? times.getDate(key + "EndTime") : null;
        } else {
            open = true;
            endTime = bean.getDate("endTime");
        }
        if (open) {
            if (endTime != null) {
                endTime = DateRules.addDateTime(date, endTime);
            } else {
                endTime = DateRules.getNextDate(date);
            }
        }
        return endTime;
    }

    /**
     * Returns the slot size.
     *
     * @return the slot size
     */
    public int getSlotSize() {
        return rules.getSlotSize(getSchedule());
    }

    /**
     * Returns the schedule.
     *
     * @return the schedule
     */
    public Entity getSchedule() {
        return (Entity) bean.getObject();
    }

    /**
     * Returns the online booking times for the schedule.
     *
     * @return the online booking times for the schedule, or  {@code null} if there are none
     */
    protected IMObjectBean getOnlineBookingTimes() {
        if (initTimes) {
            IMObject times = bean.getTarget("onlineBookingTimes");
            timesBean = (times != null) ? service.getBean(times) : null;
            initTimes = false;
        }
        return timesBean;
    }

    /**
     * Returns the node name prefix for a given date.
     *
     * @param date the date
     * @return the corresponding node name prefix
     */
    private String getNodePrefix(Date date) {
        String key;
        switch (new DateTime(date).getDayOfWeek()) {
            case DateTimeConstants.MONDAY:
                key = "mon";
                break;
            case DateTimeConstants.TUESDAY:
                key = "tue";
                break;
            case DateTimeConstants.WEDNESDAY:
                key = "wed";
                break;
            case DateTimeConstants.THURSDAY:
                key = "thu";
                break;
            case DateTimeConstants.FRIDAY:
                key = "fri";
                break;
            case DateTimeConstants.SATURDAY:
                key = "sat";
                break;
            default:
                key = "sun";
        }
        return key;
    }

}
