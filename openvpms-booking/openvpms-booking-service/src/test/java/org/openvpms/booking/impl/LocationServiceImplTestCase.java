/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.booking.api.LocationService;
import org.openvpms.booking.domain.Location;
import org.openvpms.booking.domain.Schedule;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link LocationServiceImpl}.
 *
 * @author Tim Anderson
 */
public class LocationServiceImplTestCase extends AbstractBookingServiceTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules appointmentRules;

    /**
     * The location service.
     */
    private LocationServiceImpl locationService;

    /**
     * Test location 1.
     */
    private Party location1;

    /**
     * Test location 2.
     */
    private Party location2;

    /**
     * Test location 3, with {@code onlineBooking=false}.
     */
    private Party location3;

    /**
     * Test user 1.
     */
    private User user1;

    /**
     * Test user 2.
     */
    private User user2;

    /**
     * Test user 3, with {@code onlineBooking=false}.
     */
    private User user3;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // set up 3 locations, 2 with online booking enabled
        location1 = createLocation(true);
        location2 = createLocation(true);
        location3 = createLocation(false);

        user1 = createClinician(location1);
        user2 = createClinician();
        user3 = createClinician();
        enableOnlineBooking(user3, false);

        PracticeService practiceService = Mockito.mock(PracticeService.class);
        BookingLocations locations = new BookingLocations(getArchetypeService(), practiceService);
        BookingUsers users = new TestBookingUsers(locations, user1, user2, user3);

        locationService = new LocationServiceImpl(getArchetypeService(), locations, users, appointmentRules);

        when(practiceService.getLocations()).thenReturn(Arrays.asList(location1, location2, location3));
    }

    /**
     * Tests the {@link LocationServiceImpl#getLocations()} method.
     */
    @Test
    public void testGetLocations() {
        List<Location> list1 = locationService.getLocations();
        assertEquals(2, list1.size());
        list1.sort(Comparator.comparingLong(o -> o.getId()));
        checkLocation(location1, list1.get(0));
        checkLocation(location2, list1.get(1));

        // now mark location1 inactive
        location1.setActive(false);
        save(location1);
        List<Location> list2 = locationService.getLocations();
        assertEquals(1, list2.size());
        checkLocation(location2, list2.get(0));
    }

    /**
     * Tests the {@link LocationService#getLocation(long)} method.
     */
    @Test
    public void testGetLocation() {
        checkLocation(location1, locationService.getLocation(location1.getId()));
        checkLocation(location2, locationService.getLocation(location2.getId()));

        // now mark location2 inactive. It should fail with a NotFoundException on retrieval
        location2.setActive(false);
        save(location2);

        try {
            locationService.getLocation(location2.getId());
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }

        // now try location3. It should fail with a NotFoundException on retrieval
        try {
            locationService.getLocation(location3.getId());
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationServiceImpl#getSchedules(long)} method.
     */
    @Test
    public void testGetSchedules() {
        Entity schedule1 = createSchedule(location1);
        Entity schedule2 = createSchedule(location1);
        Entity schedule3 = newSchedule(location1)
                .onlineBooking(false)
                .build();

        // verify online those schedules with onlineBooking=true are returned
        List<Schedule> list1 = locationService.getSchedules(location1.getId());
        assertEquals(2, list1.size());
        list1.sort(Comparator.comparingLong(o -> o.getId()));

        checkSchedule(schedule1, 15, list1.get(0));
        checkSchedule(schedule2, 15, list1.get(1));

        // now mark schedule2 inactive and verify it is no longer returned
        schedule2.setActive(false);
        save(schedule2);

        List<Schedule> list2 = locationService.getSchedules(location1.getId());
        assertEquals(1, list2.size());
        checkSchedule(schedule1, 15, list1.get(0));

        // verify a location with no schedules returns an empty list
        List<Schedule> list3 = locationService.getSchedules(location2.getId());
        assertTrue(list3.isEmpty());
    }

    /**
     * Tests the {@link LocationServiceImpl#getLocation(long)} method when the location does not exist.
     */
    @Test
    public void testGetLocationForNonExistentLocation() {
        try {
            locationService.getLocation(0);
            fail("expected getLocation() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationServiceImpl#getLocation(long)} method when the location is inactive.
     */
    @Test
    public void testGetLocationForInactiveLocation() {
        location1.setActive(false);
        save(location1);
        try {
            locationService.getLocation(location1.getId());
            fail("expected getLocation() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationServiceImpl#getLocation(long)} when the location has onlineBooking=false.
     */
    @Test
    public void testGetLocationForOnlineBookingDisabled() {
        try {
            locationService.getLocation(location3.getId());
            fail("expected getLocation() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationService#getUsers(long)} method.
     * <p/>
     * Note that locations with no users attached return all active users.
     */
    @Test
    public void testGetUsers() {
        // location1 has one user directly linked
        List<org.openvpms.booking.domain.User> list1 = locationService.getUsers(location1.getId());
        assertEquals(2, list1.size());
        list1.sort(Comparator.comparingLong(org.openvpms.booking.domain.User::getId));

        checkUser(user1, list1.get(0));
        checkUser(user2, list1.get(1));

        // location2 has no users linked, so sees all unlinked users
        List<org.openvpms.booking.domain.User> list2 = locationService.getUsers(location2.getId());
        assertEquals(1, list2.size());
        checkUser(user2, list2.get(0));

        // now mark user2 inactive and verify it is no longer returned.
        user2.setActive(false);
        save(user2);

        List<org.openvpms.booking.domain.User> list3 = locationService.getUsers(location1.getId());
        assertEquals(1, list3.size());
        checkUser(user1, list3.get(0));

        List<org.openvpms.booking.domain.User> list4 = locationService.getUsers(location2.getId());
        assertTrue(list4.isEmpty());

        // turn off online booking for user1 and verify it is no longer returned
        enableOnlineBooking(user1, false);

        List<org.openvpms.booking.domain.User> list6 = locationService.getUsers(location1.getId());
        assertTrue(list6.isEmpty());
    }

    /**
     * Tests the {@link LocationServiceImpl#getUsers} method when the location does not exist.
     */
    @Test
    public void testGetUsersForNonExistentLocation() {
        try {
            locationService.getUsers(0);
            fail("expected getUsers() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationServiceImpl#getUsers} method when the location is inactive.
     */
    @Test
    public void testGetIsersForInactiveLocation() {
        location1.setActive(false);
        save(location1);
        try {
            locationService.getLocation(location1.getId());
            fail("expected getLocation() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link LocationServiceImpl#getUsers} method when the location has onlineBooking=false.
     */
    @Test
    public void testGetUsersForOnlineBookingDisabled() {
        try {
            locationService.getLocation(location3.getId());
            fail("expected getLocation() to fail");
        } catch (NotFoundException expected) {
            assertEquals("Practice location not found", expected.getMessage());
        }
    }

    /**
     * Verifies a user matches that expected.
     *
     * @param expected the expected user
     * @param actual   the actual user
     */
    private void checkUser(User expected, org.openvpms.booking.domain.User actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
    }

    /**
     * Overrides the {@link BookingUsers#queryUsers(Party)} method to only return specific users.
     * <p/>
     * This is to avoid the default behaviour which would return any user not linked to a practice location (which
     * indicates that the user can work anywhere).
     */
    private class TestBookingUsers extends BookingUsers {
        private final User[] users;

        public TestBookingUsers(BookingLocations locations, User... users) {
            super(LocationServiceImplTestCase.this.getArchetypeService(), locations);
            this.users = users;
        }

        @Override
        protected Iterator<User> queryUsers(Party location) {
            List<User> result = new ArrayList<>();
            for (User user : users) {
                // make sure the user is active
                User current = (User) getArchetypeService().get(user.getObjectReference(), true);
                if (current != null) {
                    IMObjectBean bean = getBean(current);
                    if (bean.hasTarget("locations", location) || bean.getValues("locations").isEmpty()) {
                        result.add(current);
                    }
                }
            }
            return result.iterator();
        }

        /**
         * Adds a user to a list if its persistent version is active.
         *
         * @param user the user
         * @param list the list
         */
        private void addIfActive(User user, List<User> list) {
            IMObject current = getArchetypeService().get(user.getObjectReference(), true);
            if (current != null) {
                list.add((User) current);
            }
        }
    }
}
