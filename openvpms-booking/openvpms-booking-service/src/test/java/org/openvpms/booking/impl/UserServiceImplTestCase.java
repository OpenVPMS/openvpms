/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.booking.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentService;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.booking.api.UserService;
import org.openvpms.booking.domain.Location;
import org.openvpms.booking.domain.Range;
import org.openvpms.booking.domain.ScheduleRange;
import org.openvpms.booking.domain.UserFreeBusy;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.cache.BasicEhcacheManager;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.NotFoundException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link UserServiceImpl}.
 *
 * @author Tim Anderson
 */
public class UserServiceImplTestCase extends AbstractBookingServiceTest {

    /**
     * Test location 1.
     */
    private Party location1;

    /**
     * Test location 2.
     */
    private Party location2;

    /**
     * Test location3, with {@code onlineBooking=false}.
     */
    private Party location3;

    /**
     * The roster service.
     */
    private RosterService rosterService;

    /**
     * The appointment service.
     */
    private AppointmentService appointmentService;

    /**
     * The test user.
     */
    private User clinician1;

    /**
     * The user service.
     */
    private UserService service;

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        IArchetypeService service = getArchetypeService();
        rosterService = new RosterService(service, new BasicEhcacheManager(30));
        appointmentService = new AppointmentService(service, getLookupService(),
                                                    new BasicEhcacheManager(30));

        location1 = createLocation(true);
        location2 = createLocation(true);
        location3 = createLocation(false);

        PracticeService practiceService = Mockito.mock(PracticeService.class);
        Mockito.when(practiceService.getLocations()).thenReturn(Arrays.asList(location1, location2, location3));
        clinician1 = createClinician();
        BookingLocations locations = new BookingLocations(service, practiceService);
        BookingCalendar calendar = new BookingCalendar(service, rosterService, appointmentService, rules);
        BookingUsers users = new BookingUsers(service, locations);
        this.service = new UserServiceImpl(calendar, locations, users);
    }

    /**
     * Cleans up after the test.
     *
     * @throws Exception for any error
     */
    @After
    public void tearDown() throws Exception {
        rosterService.destroy();
        appointmentService.destroy();
    }

    /**
     * Verifies that if a user has no roster, no time ranges are returned.
     */
    @Test
    public void testNoRoster() {
        User user = createClinician();
        List<ScheduleRange> free = service.getFree(user.getId(), location1.getId(), getISODate("2018-11-25"),
                                                   getISODate("2018-11-26"));
        List<ScheduleRange> busy = service.getBusy(user.getId(), location1.getId(), getISODate("2018-11-25"),
                                                   getISODate("2018-11-26"));
        UserFreeBusy freeBusy = service.getFreeBusy(user.getId(), location1.getId(), getISODate("2018-11-25"),
                                                    getISODate("2018-11-26"));
        checkRanges(free);
        checkRanges(busy);
        checkRanges(freeBusy.getFree());
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Test behaviour if a roster has a single 24-hour event, for a single 24-hour schedule.
     * <p/>
     * Only a single free range should be returned, with no busy ranges.
     */
    @Test
    public void testFullDayRosterEventSingleSchedule() {
        Entity schedule = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule);
        createEvent("2018-11-28 00:00", "2018-11-29 00:00", clinician1, area, location1);

        // request 3 days of ranges overlapping the roster event
        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-30");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);

        // verify there is only a single free range, covering the roster event, and no busy ranges
        Range free1 = createRange("2018-11-28 00:00", "2018-11-29 00:00");
        checkRanges(free, free1);
        checkRanges(busy);
        checkRanges(freeBusy.getFree(), free1);
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Test behaviour if a roster has a single 24-hour event, for two 24-hour schedules.
     * <p/>
     * There should be a free range for each schedule, with no busy ranges.
     */
    @Test
    public void testFullDayRosterEventForTwoSchedules() {
        Entity schedule1 = createSchedule(location1);
        Entity schedule2 = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule1, schedule2);
        createEvent("2018-11-28 00:00", "2018-11-29 00:00", clinician1, area, location1);

        // request 3 days of ranges overlapping the roster event
        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-30");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);

        // verify there is only a single free range for each schedule, covering the roster event, and no busy ranges
        assertEquals(2, free.size());
        assertEquals(2, freeBusy.getFree().size());

        Range free1 = createRange("2018-11-28 00:00", "2018-11-29 00:00");
        checkRanges(free, schedule1.getId(), free1);
        checkRanges(free, schedule2.getId(), free1);
        checkRanges(busy);
        checkRanges(freeBusy.getFree(), schedule1.getId(), free1);
        checkRanges(freeBusy.getFree(), schedule2.getId(), free1);
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Test behaviour if a roster has a 24 hour event but the schedule is 9-to-5.
     * <p/>
     * Only a single free range should be returned restricted to the schedule times, with no busy ranges.
     */
    @Test
    public void testFullDayRosterEventFor9To5Schedule() {
        Entity schedule = createNineToFiveSchedule(location1);
        Entity area = createRosterArea(location1, schedule);
        createEvent("2018-11-28 00:00", "2018-11-29 00:00", clinician1, area, location1);

        // request 3 days of ranges overlapping the roster event
        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-30");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);

        // verify the free range is limited to the schedule times. There should be no busy ranges.
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 17:00");

        checkRanges(free, free1);
        checkRanges(busy);
        checkRanges(freeBusy.getFree(), free1);
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Test behaviour if a roster has a 24 hour event and there are two 9-to-5 schedules.
     * <p/>
     * There should be a free range for each schedule, with no busy ranges.
     */
    @Test
    public void testFullDayRosterEventForTwo9To5Schedules() {
        Entity schedule1 = createNineToFiveSchedule(location1);
        Entity schedule2 = createNineToFiveSchedule(location1);
        Entity area = createRosterArea(location1, schedule1, schedule2);
        createEvent("2018-11-28 00:00", "2018-11-29 00:00", clinician1, area, location1);

        // request 3 days of ranges overlapping the roster event
        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-30");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);

        // verify the free range is limited to the schedule times. There should be no busy ranges.
        assertEquals(2, free.size());
        assertEquals(2, freeBusy.getFree().size());

        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 17:00");
        checkRanges(free, schedule1.getId(), free1);
        checkRanges(free, schedule2.getId(), free1);
        checkRanges(busy);
        checkRanges(freeBusy.getFree(), schedule1.getId(), free1);
        checkRanges(freeBusy.getFree(), schedule2.getId(), free1);
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Test behaviour if a roster has a 2 events in a day for a single 24 hour schedule.
     * <p/>
     * There should be free ranges for each event and no busy ranges.
     */
    @Test
    public void test2RosterEventsForSingle24HourSchedule() {
        Entity schedule = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule);
        createEvent("2018-11-28 09:00", "2018-11-28 12:00", clinician1, area, location1);
        createEvent("2018-11-28 13:00", "2018-11-28 15:00", clinician1, area, location1);
        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 12:00");
        Range free2 = createRange("2018-11-28 13:00", "2018-11-28 15:00");
        checkRanges(free, free1, free2);
        checkRanges(busy);
        checkRanges(freeBusy.getFree(), free1, free2);
        checkRanges(freeBusy.getBusy());
    }

    /**
     * Verifies that appointments are reflected in the free and busy times for a single 24 hour schedule.
     * <p/>
     * If an appointment is outside of a clinicians rostered-on period, they will still appear as busy times.
     */
    @Test
    public void testAppointmentsWith24HourSchedule() {
        Entity schedule = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule);
        createEvent("2018-11-28 09:00", "2018-11-28 12:00", clinician1, area, location1);
        createEvent("2018-11-28 13:00", "2018-11-28 15:00", clinician1, area, location1);
        createAppointment("2018-11-28 09:30", "2018-11-28 10:00", schedule, clinician1); // within 1st range
        createAppointment("2018-11-28 10:00", "2018-11-28 10:30", schedule, clinician1);
        createAppointment("2018-11-28 12:45", "2018-11-28 13:15", schedule, clinician1); // overlaps start of 2nd range
        createAppointment("2018-11-28 15:30", "2018-11-28 15:45", schedule, clinician1); // outside range

        String from = getISODate("2018-11-28");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 09:30");
        Range free2 = createRange("2018-11-28 10:30", "2018-11-28 12:00");
        Range free3 = createRange("2018-11-28 13:15", "2018-11-28 15:00");
        Range busy1 = createRange("2018-11-28 09:30", "2018-11-28 10:30");
        Range busy2 = createRange("2018-11-28 12:45", "2018-11-28 13:15"); // outside of roster period, but user
        Range busy3 = createRange("2018-11-28 15:30", "2018-11-28 15:45"); // has appointments
        checkRanges(free, free1, free2, free3);
        checkRanges(busy, busy1, busy2, busy3);
        checkRanges(freeBusy.getFree(), free1, free2, free3);
        checkRanges(freeBusy.getBusy(), busy1, busy2, busy3);
    }

    /**
     * Tests that if a roster area contains multiple schedules, a clinician's appointments are reflected in the
     * free/busy times of each schedule.
     */
    @Test
    public void testAppointmentsWithMultiple24HourSchedules() {
        Entity schedule1 = createSchedule(location1);
        Entity schedule2 = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule1, schedule2);
        createEvent("2018-11-28 09:00", "2018-11-28 12:00", clinician1, area, location1);
        createEvent("2018-11-28 13:00", "2018-11-28 15:00", clinician1, area, location1);
        createAppointment("2018-11-28 09:30", "2018-11-28 10:00", schedule1, clinician1); // within 1st range
        createAppointment("2018-11-28 10:00", "2018-11-28 10:30", schedule2, clinician1);
        createAppointment("2018-11-28 12:45", "2018-11-28 13:15", schedule1, clinician1); // overlaps start of 2nd range
        createAppointment("2018-11-28 15:30", "2018-11-28 15:45", schedule2, clinician1); // outside range

        String from = getISODate("2018-11-28");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 09:30");
        Range free2 = createRange("2018-11-28 10:30", "2018-11-28 12:00");
        Range free3 = createRange("2018-11-28 13:15", "2018-11-28 15:00");
        Range busy1 = createRange("2018-11-28 09:30", "2018-11-28 10:00");
        Range busy2 = createRange("2018-11-28 10:00", "2018-11-28 10:30");
        Range busy3 = createRange("2018-11-28 12:45", "2018-11-28 13:15"); // outside of roster period, but user
        Range busy4 = createRange("2018-11-28 15:30", "2018-11-28 15:45"); // has appointments
        checkRanges(free, schedule1.getId(), free1, free2, free3);
        checkRanges(free, schedule2.getId(), free1, free2, free3);
        checkRanges(busy, schedule1.getId(), busy1, busy3);
        checkRanges(busy, schedule2.getId(), busy2, busy4);
        checkRanges(freeBusy.getFree(), schedule1.getId(), free1, free2, free3);
        checkRanges(freeBusy.getFree(), schedule2.getId(), free1, free2, free3);
        checkRanges(freeBusy.getBusy(), schedule1.getId(), busy1, busy3);
        checkRanges(freeBusy.getBusy(), schedule2.getId(), busy2, busy4);
    }

    /**
     * Tests that if a roster area contains multiple schedules, a clinician's appointments are reflected in the
     * free/busy times of each schedule.
     */
    @Test
    public void testAppointmentsWithMultiple9to5Schedules() {
        Entity schedule1 = createNineToFiveSchedule(location1);
        Entity schedule2 = createNineToFiveSchedule(location1);
        Entity area = createRosterArea(location1, schedule1, schedule2);
        createEvent("2018-11-28 08:00", "2018-11-28 12:00", clinician1, area, location1);
        createEvent("2018-11-28 13:00", "2018-11-28 16:00", clinician1, area, location1);
        createAppointment("2018-11-28 09:30", "2018-11-28 10:00", schedule1, clinician1);
        createAppointment("2018-11-28 10:00", "2018-11-28 10:30", schedule2, clinician1);
        createAppointment("2018-11-28 12:45", "2018-11-28 13:15", schedule1, clinician1);
        createAppointment("2018-11-28 15:30", "2018-11-28 15:45", schedule2, clinician1);

        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 09:30");
        Range free2 = createRange("2018-11-28 10:30", "2018-11-28 12:00");
        Range free3 = createRange("2018-11-28 13:15", "2018-11-28 15:30");
        Range free4 = createRange("2018-11-28 15:45", "2018-11-28 16:00");
        Range busy1 = createRange("2018-11-28 09:30", "2018-11-28 10:00");
        Range busy2 = createRange("2018-11-28 10:00", "2018-11-28 10:30");
        Range busy3 = createRange("2018-11-28 12:45", "2018-11-28 13:15");
        Range busy4 = createRange("2018-11-28 15:30", "2018-11-28 15:45");
        checkRanges(free, schedule1.getId(), free1, free2, free3, free4);
        checkRanges(free, schedule2.getId(), free1, free2, free3, free4);
        checkRanges(busy, schedule1.getId(), busy1, busy3);
        checkRanges(busy, schedule2.getId(), busy2, busy4);
        checkRanges(freeBusy.getFree(), schedule1.getId(), free1, free2, free3, free4);
        checkRanges(freeBusy.getFree(), schedule2.getId(), free1, free2, free3, free4);
        checkRanges(freeBusy.getBusy(), schedule1.getId(), busy1, busy3);
        checkRanges(freeBusy.getBusy(), schedule2.getId(), busy2, busy4);
    }

    /**
     * Tests that if a roster area contains multiple schedules with different start and end times, the times returned
     * are different for each schedule.
     */
    @Test
    public void testAppointmentsWithMultipleDifferentTimedSchedules() {
        Entity schedule1 = createSchedule(location1, "09:00", "13:00");
        Entity schedule2 = createSchedule(location1, "12:00", "17:00");
        Entity area = createRosterArea(location1, schedule1, schedule2);
        createEvent("2018-11-28 08:00", "2018-11-28 12:00", clinician1, area, location1);
        createEvent("2018-11-28 13:00", "2018-11-28 16:00", clinician1, area, location1);
        createAppointment("2018-11-28 09:30", "2018-11-28 10:00", schedule1, clinician1);
        createAppointment("2018-11-28 10:00", "2018-11-28 10:30", schedule1, clinician1);
        createAppointment("2018-11-28 12:45", "2018-11-28 13:15", schedule2, clinician1);
        createAppointment("2018-11-28 15:30", "2018-11-28 15:45", schedule2, clinician1);

        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 09:00", "2018-11-28 09:30");
        Range free2 = createRange("2018-11-28 10:30", "2018-11-28 12:00");
        Range free3 = createRange("2018-11-28 13:15", "2018-11-28 15:30");
        Range free4 = createRange("2018-11-28 15:45", "2018-11-28 16:00");
        Range busy1 = createRange("2018-11-28 09:30", "2018-11-28 10:30");
        Range busy2 = createRange("2018-11-28 12:45", "2018-11-28 13:15");
        Range busy3 = createRange("2018-11-28 15:30", "2018-11-28 15:45");
        checkRanges(free, schedule1.getId(), free1, free2);
        checkRanges(free, schedule2.getId(), free3, free4);
        checkRanges(busy, schedule1.getId(), busy1);
        checkRanges(busy, schedule2.getId(), busy2, busy3);
        checkRanges(freeBusy.getFree(), schedule1.getId(), free1, free2);
        checkRanges(freeBusy.getFree(), schedule2.getId(), free3, free4);
        checkRanges(freeBusy.getBusy(), schedule1.getId(), busy1);
        checkRanges(freeBusy.getBusy(), schedule2.getId(), busy2, busy3);
    }

    /**
     * Verifies that consecutive appointment slots are treated as a single busy slot.
     */
    @Test
    public void testBusyRangesMerged() {
        Entity schedule1 = createSchedule(location1);
        Entity area = createRosterArea(location1, schedule1);
        createEvent("2018-11-28 08:00", "2018-11-28 17:00", clinician1, area, location1);
        createAppointment("2018-11-28 08:30", "2018-11-28 09:00", schedule1, clinician1);
        createAppointment("2018-11-28 09:00", "2018-11-28 09:30", schedule1, clinician1);
        createAppointment("2018-11-28 09:30", "2018-11-28 10:00", schedule1, clinician1);

        String from = getISODate("2018-11-27");
        String to = getISODate("2018-11-29");
        List<ScheduleRange> free = service.getFree(clinician1.getId(), location1.getId(), from, to);
        List<ScheduleRange> busy = service.getBusy(clinician1.getId(), location1.getId(), from, to);
        UserFreeBusy freeBusy = service.getFreeBusy(clinician1.getId(), location1.getId(), from, to);
        Range free1 = createRange("2018-11-28 08:00", "2018-11-28 08:30");
        Range free2 = createRange("2018-11-28 10:00", "2018-11-28 17:00");
        Range busy1 = createRange("2018-11-28 08:30", "2018-11-28 10:00");
        checkRanges(free, free1, free2);
        checkRanges(busy, busy1);
        checkRanges(freeBusy.getFree(), free1, free2);
        checkRanges(freeBusy.getBusy(), busy1);
    }

    /**
     * Tests the {@link UserServiceImpl#getUser(long)} method.
     */
    @Test
    public void testGetUser() {
        org.openvpms.booking.domain.User user = service.getUser(clinician1.getId());
        assertNotNull(user);
        assertEquals(clinician1.getName(), user.getName());
        assertEquals(clinician1.getId(), user.getId());
    }

    /**
     * Tests the {@link UserServiceImpl#getUser(long)} method for a non-existent user.
     */
    @Test
    public void testGetUserForNonExistentUser() {
        try {
            service.getUser(0);
            fail("Expected getUser() to fail");
        } catch (NotFoundException expected) {
            assertEquals("User not found", expected.getMessage());
        }
    }

    /**
     * Verifies that the {@link UserServiceImpl#getUser(long)} method thows {@code NotFoundException} if the user is
     * inactive.
     */
    @Test
    public void testGetUserForInactiveUser() {
        clinician1.setActive(false);
        save(clinician1);
        try {
            service.getUser(clinician1.getId());
            fail("Expected getUser() to fail");
        } catch (NotFoundException expected) {
            assertEquals("User not found", expected.getMessage());
        }
    }

    /**
     * Verifies that the {@link UserServiceImpl#getUser(long)} method throws {@code NotFoundException} if the user is
     * inactive.
     */
    @Test
    public void testGetUserForOnlineBookingDisabled() {
        enableOnlineBooking(clinician1, false);
        try {
            service.getUser(clinician1.getId());
            fail("Expected getUser() to fail");
        } catch (NotFoundException expected) {
            assertEquals("User not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link UserServiceImpl#getLocations(long)} method where the clinician has no location relationships.
     */
    @Test
    public void testGetLocationsForClinicianWithNoLocationRelationships() {
        // user not associated with any locations, so will return all online booking locations.
        List<Location> list1 = service.getLocations(clinician1.getId());
        assertEquals(2, list1.size());

        list1.sort(Comparator.comparingLong(o -> o.getId()));
        checkLocation(location1, list1.get(0));
        checkLocation(location2, list1.get(1));

        // mark location2 inactive and verify it is no longer returned
        location2.setActive(false);
        save(location2);
        List<Location> list2 = service.getLocations(clinician1.getId());
        assertEquals(1, list2.size());
        checkLocation(location1, list2.get(0));

        // turn off online booking for location1 and verify it is no longer returned
        enableOnlineBooking(location1, false);
        assertTrue(service.getLocations(clinician1.getId()).isEmpty());
    }

    /**
     * Tests the {@link UserServiceImpl#getLocations(long)} method where the clinician has location relationships.
     */
    @Test
    public void testGetLocationsForClinicianWithLocations() {
        IMObjectBean bean = getBean(clinician1);
        bean.addTarget("locations", location1);
        bean.addTarget("locations", location2);
        bean.addTarget("locations", location3); // booking not enabled
        bean.save();

        List<Location> list1 = service.getLocations(clinician1.getId());
        list1.sort(Comparator.comparingLong(o -> o.getId()));
        checkLocation(location1, list1.get(0));
        checkLocation(location2, list1.get(1));

        // now deactivate location1 and verify it is no longer returned
        location1.setActive(false);
        save(location1);
        List<Location> list2 = service.getLocations(clinician1.getId());
        assertEquals(1, list2.size());
        checkLocation(location2, list2.get(0));

        // deactivate location2 - no locations should be returned
        enableOnlineBooking(location2, false);
        assertTrue(service.getLocations(clinician1.getId()).isEmpty());
    }

    /**
     * Tests the {@link UserServiceImpl#getLocation(long, long)} method for a user with no location relationships.
     * All online booking locations should be returned.
     */
    @Test
    public void testGetLocationForUserWithNoLocationRelationships() {
        Location l1 = service.getLocation(clinician1.getId(), location1.getId());
        assertEquals(location1.getId(), l1.getId());

        Location l2 = service.getLocation(clinician1.getId(), location2.getId());
        assertEquals(location2.getId(), l2.getId());

        // Now mark location2 inactive. Should now throw an exception
        location2.setActive(false);
        save(location2);
        try {
            service.getLocation(clinician1.getId(), location2.getId());
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }

        // Now try with a location with onlineBooking=false
        try {
            service.getLocation(clinician1.getId(), location3.getId());
            fail("Expected getLocation() to fail for onlineBooking=false location");
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }
    }

    /**
     * Tests the {@link UserServiceImpl#getLocation(long, long)} method.
     */
    @Test
    public void testGetLocationForUserWithLocationRelationship() {
        // associate user with location 1. Only it should be returned
        IMObjectBean bean = getBean(clinician1);
        bean.addTarget("locations", location1);
        bean.save();

        Location l1 = service.getLocation(clinician1.getId(), location1.getId());
        checkLocation(location1, l1);

        // now mark location1 inactive
        location1.setActive(false);
        save(location1);
        try {
            service.getLocation(clinician1.getId(), location1.getId());
            fail("Expected getLocation() to fail when location inactive");
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }

        try {
            service.getLocation(clinician1.getId(), location2.getId());
            fail("Expected getLocation() to fail when location not associated with user");
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }

        try {
            service.getLocation(clinician1.getId(), location3.getId());
            fail("Expected getLocation() to fail for onlineBooking=false location");
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }
    }

    /**
     * Verifies that the {@link UserServiceImpl#getLocation(long, long)} method throws {@code NotFoundException}
     * for invalid inputs.
     */
    @Test
    public void testGetLocationForInvalidInputs() {
        try {
            service.getLocation(0, location1.getId());
            fail("Expected getLocation() to fail for non-existent user");
        } catch (NotFoundException expected) {
            assertEquals("User not found", expected.getMessage());
        }

        try {
            service.getLocation(clinician1.getId(), 0);
            fail("Expected getLocation() to fail for non-existent location");
        } catch (NotFoundException expected) {
            assertEquals("Location not found", expected.getMessage());
        }
    }

    /**
     * Creates a roster event.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param user      the rostered user. May be {@code null}
     * @param area      the area
     * @param location  the location
     * @return a new event
     */
    private Act createEvent(String startTime, String endTime, User user, Entity area, Party location) {
        Act event = ScheduleTestHelper.createRosterEvent(TestHelper.getDatetime(startTime),
                                                         TestHelper.getDatetime(endTime), user, area, location);
        save(event);
        return event;
    }

}
