/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tilldrawer.service;

import org.openvpms.domain.till.Till;
import org.openvpms.tilldrawer.exception.TillDrawerException;

/**
 * Service for opening drawers associated with tills.
 *
 * @author Tim Anderson
 */
public interface TillDrawerService {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    String getName();

    /**
     * Determines if the service can open the drawer associated with a till.
     *
     * @param till the till
     * @return {@code true} if the drawer can be opened, otherwise {@code false}
     * @throws TillDrawerException for any error
     */
    boolean canOpen(Till till);

    /**
     * Opens the drawer associated with a till.
     *
     * @param till the till
     * @throws TillDrawerException for any error
     */
    void open(Till till);

}