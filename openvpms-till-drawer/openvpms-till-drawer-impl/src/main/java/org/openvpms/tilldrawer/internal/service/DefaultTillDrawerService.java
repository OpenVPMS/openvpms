/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tilldrawer.internal.service;

import org.openvpms.domain.till.DrawerCommand;
import org.openvpms.domain.till.Till;
import org.openvpms.tilldrawer.exception.TillDrawerException;
import org.openvpms.tilldrawer.internal.i18n.TillDrawerMessages;
import org.openvpms.tilldrawer.service.TillDrawerService;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;

/**
 * Default implementation of the {@link TillDrawerService} that opens till drawers printing the drawer command
 * to the printer associated with the till.
 *
 * @author Tim Anderson
 */
public class DefaultTillDrawerService implements TillDrawerService {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Default Till Drawer Service";
    }

    /**
     * Determines if the service can open the drawer associated with a till.
     *
     * @param till the till
     * @return {@code true} if the drawer can be opened, otherwise {@code false}
     */
    @Override
    public boolean canOpen(Till till) {
        DrawerCommand command = till.getDrawerCommand();
        return command != null && command.getPrinterServiceArchetype() == null;
    }

    /**
     * Opens the drawer associated with a till.
     *
     * @param till the till
     */
    @Override
    public void open(Till till) {
        DrawerCommand command = till.getDrawerCommand();
        if (command == null || command.getCommand().length == 0) {
            throw new TillDrawerException(TillDrawerMessages.noDrawerCommand());
        }
        String printer = command.getPrinter();
        PrintService printService = getPrintService(printer);
        if (printService == null) {
            throw new TillDrawerException(TillDrawerMessages.printerNotFound(printer));
        }
        DocPrintJob job = printService.createPrintJob();
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        try {
            Doc doc = new SimpleDoc(command.getCommand(), flavor, null);
            job.print(doc, null);
        } catch (PrintException exception) {
            throw new TillDrawerException(TillDrawerMessages.openFailed(printer, exception.getMessage()), exception);
        }
    }

    /**
     * Returns a {@link PrintService} given its name.
     *
     * @param printer the printer name
     * @return the service, or {@code null} if none is found
     */
    private PrintService getPrintService(String printer) {
        AttributeSet attrSet = new HashPrintServiceAttributeSet(new PrinterName(printer, null));
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, attrSet);
        return printServices.length > 0 ? printServices[0] : null;
    }
}
