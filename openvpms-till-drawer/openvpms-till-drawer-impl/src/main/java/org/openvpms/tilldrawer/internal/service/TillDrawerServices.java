/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tilldrawer.internal.service;

import org.openvpms.domain.till.Till;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.tilldrawer.exception.TillDrawerException;
import org.openvpms.tilldrawer.internal.i18n.TillDrawerMessages;
import org.openvpms.tilldrawer.service.TillDrawerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of {@link TillDrawerService} that uses the {@link PluginManager} to locate a
 * {@link TillDrawerService}, falling back to {@link DefaultTillDrawerService} if there is no plugin available.
 *
 * @author Tim Anderson
 */
public class TillDrawerServices implements TillDrawerService {

    /**
     * The plugin manager.
     */
    private final PluginManager pluginManager;

    /**
     * The fallback service.
     */
    private final TillDrawerService defaultService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TillDrawerServices.class);

    /**
     * Constructs a {@link TillDrawerService}
     *
     * @param pluginManager the plugin manager
     */
    public TillDrawerServices(PluginManager pluginManager) {
        this.pluginManager = pluginManager;
        defaultService = new DefaultTillDrawerService();
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Till Drawer Services";
    }

    /**
     * Determines if the service can open the drawer associated with a till.
     *
     * @param till the till
     * @return {@code true} if the drawer can be opened, otherwise {@code false}
     * @throws TillDrawerException for any error
     */
    @Override
    public boolean canOpen(Till till) {
        return getService(till) != null;
    }

    /**
     * Opens the drawer associated with a till.
     *
     * @param till the till
     * @throws TillDrawerException for any error
     */
    @Override
    public void open(Till till) {
        TillDrawerService service = getService(till);
        if (service != null) {
            service.open(till);
        } else {
            throw new TillDrawerException(TillDrawerMessages.noServiceToOpenDrawer());
        }
    }

    /**
     * Returns a service to open the till drawer.
     *
     * @param till the till
     * @return the corresponding service, or {@code null} if none can open the drawer
     */
    public TillDrawerService getService(Till till) {
        TillDrawerService result = null;
        if (pluginManager != null) {
            for (TillDrawerService service : pluginManager.getServices(TillDrawerService.class)) {
                if (canOpen(service, till)) {
                    result = service;
                    break;
                }
            }
        }
        if (result == null && canOpen(defaultService, till)) {
            result = defaultService;
        }
        return result;
    }

    /**
     * Determines if a service can open a till.
     *
     * @param service the service
     * @param till the till
     * @return {@code true} if the service can open the till, otherwise {@code false}
     */
    private boolean canOpen(TillDrawerService service, Till till) {
        boolean result = false;
        try {
            result = service.canOpen(till);
        } catch (Exception exception) {
            log.warn("Failed to determine if TillDrawerService '{}' can open till '{}': {}", service.getName(),
                     till.getName(), exception.getMessage(), exception);
        }
        return result;
    }

}
