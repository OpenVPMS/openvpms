/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tilldrawer.internal.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;

/**
 * .
 *
 * @author Tim Anderson
 */
public class TillDrawerMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("CASHDRAWER", TillDrawerMessages.class.getName());

    /**
     * Default constructor.
     */
    private TillDrawerMessages() {
        // no-op
    }

    public static Message noServiceToOpenDrawer() {
        return messages.create(1);
    }

    public static Message printerNotFound(String printer) {
        return messages.create(2, printer);
    }

    public static Message noDrawerCommand() {
        return messages.create(3);
    }

    public static Message openFailed(String printer, String message) {
        return messages.create(4, printer, message);
    }
}
