/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.esci.adapter.map.UBLDocument;
import org.openvpms.esci.adapter.util.ESCIAdapterException;

/**
 * Resolves orders.
 *
 * @author Tim Anderson
 */
public interface OrderResolver {

    /**
     * Returns an order associated with an invoice, if one is referenced.
     * <p/>
     * This verifies it has a relationship to the expected supplier and stock location, if it exists.
     *
     * @param invoice       the invoice
     * @param supplier      the expected supplier of the order
     * @param stockLocation the expected stock location of the order
     * @return the corresponding order. May be {@code null}
     * @throws ESCIAdapterException      if the order reference is malformed, or the expected supplier and stock
     *                                   location don't match the actual order
     * @throws ArchetypeServiceException for any archetype service error
     */
    FinancialAct getOrder(UBLInvoice invoice, Party supplier, Party stockLocation);

    /**
     * Verifies that an order has a relationship to the expected supplier and stock location.
     *
     * @param order         the order
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param document      the document where the order is referenced
     * @return {@code true} if the order is for the correct supplier and stock location, otherwise {@code false}
     * @throws ESCIAdapterException      if the expected supplier and stock location don't match the actual order
     * @throws ArchetypeServiceException for any archetype service error
     */
    boolean checkOrder(FinancialAct order, Party supplier, Party stockLocation, UBLDocument document);

    /**
     * Returns the order item associated with an invoice line.
     * <p/>
     * If there is no order item specified, but there is a document-level order, the result will contain it.
     * <p/>
     * This can be used when matching unreferenced invoice lines.
     *
     * @param line    the invoice line
     * @param context the mapping context
     * @return the corresponding order item, or {@code null} if the line isn't associated with an order item
     * @throws ESCIAdapterException      if the order item reference is malformed or references an invalid order
     * @throws ArchetypeServiceException for any archetype service error
     */
    OrderItem getOrderItem(UBLInvoiceLine line, MappingContext context);

}
