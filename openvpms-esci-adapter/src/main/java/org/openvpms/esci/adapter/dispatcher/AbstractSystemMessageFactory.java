/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Base class for listeners that create an <em>act.systemMessage</em> for the events they receive.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSystemMessageFactory {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractSystemMessageFactory.class);

    /**
     * Constructs an {@link AbstractSystemMessageFactory}.
     *
     * @param service the archetype service
     */
    protected AbstractSystemMessageFactory(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a system message linked to the supplied act and addressed to the act's author.
     * <p/>
     * The act's author is determined using {@link #getAddressee}. If there is no author, no system message will be
     * created.
     *
     * @param act     the act
     * @param subject the message subject
     * @param reason  the reason
     */
    protected void createMessage(Act act, String subject, String reason) {
        User user = getAddressee(act, reason);
        if (user != null) {
            IMObjectBean message = service.getBean(service.create(MessageArchetypes.SYSTEM, Act.class));
            message.addTarget("item", act);
            message.setTarget("to", user);
            message.setValue("description", subject);
            message.setValue("reason", reason);
            message.save();
        }
    }

    /**
     * Returns the user to address a message to.
     * <p/>
     * By default, this is the createdBy user of the supplied act. If no createdBy user is present, and the act
     * has an associated stock location, the stock location's default author will be used, if any.
     *
     * @param act    the act
     * @param reason the reason, for logging purposes
     * @return the author, or {@code null} if none is found
     */
    protected User getAddressee(Act act, String reason) {
        IMObjectBean bean = service.getBean(act);
        User result = null;
        if (act.getCreatedBy() != null) {
            result = service.get(act.getCreatedBy(), User.class);
        }
        Entity stockLocation = null;
        if (result == null && bean.hasNode("stockLocation")) {
            stockLocation = bean.getTarget("stockLocation", Entity.class);
            if (stockLocation != null) {
                IMObjectBean locBean = service.getBean(stockLocation);
                result = (User) locBean.getTarget("defaultAuthor");
            }
        }
        if (result == null && log.isInfoEnabled()) {
            StringBuilder message = new StringBuilder("Cannot create ");
            message.append(MessageArchetypes.SYSTEM);
            message.append(" for ");
            message.append(act.getArchetype());
            message.append(":");
            message.append(act.getId());
            message.append(" with reason ");
            message.append(reason);
            message.append(". The act has no author");
            if (stockLocation != null) {
                message.append(" and stock location ");
                message.append(stockLocation.getName());
                message.append(" has no defaultAuthor configured");
            }
            log.info(message.toString());
        }

        return result;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }
}
