/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.esci.adapter.client.SupplierServiceLocator;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.service.InboxService;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;


/**
 * Connects to each configured InboxService, and dispatches documents to the registered
 * {@link DocumentProcessor}s.
 * <p>
 * NOTE: methods are synchronised to avoid duplicate processing. In reality, only individual inboxes need to be
 * synchronised, using a technique similar to https://dzone.com/articles/synchronized-by-the-value-of-the-object-in-java
 * This could be done using an object based on the inbox user name and URL. TODO
 *
 * @author Tim Anderson
 */
public class DefaultESCIDispatcher implements ESCIDispatcher {

    /**
     * The service locator.
     */
    private final SupplierServiceLocator locator;

    /**
     * The document processors.
     */
    private final List<DocumentProcessor> processors;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Determines if dispatching should stop.
     */
    private volatile boolean stop;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DefaultESCIDispatcher.class);

    /**
     * Constructs a {@link DefaultESCIDispatcher}.
     *
     * @param locator    the supplier service locator
     * @param processors the document processors
     * @param service    the archetype service
     */
    public DefaultESCIDispatcher(SupplierServiceLocator locator, List<DocumentProcessor> processors,
                                 IArchetypeService service) {
        this.locator = locator;
        this.processors = processors;
        this.service = service;
    }

    /**
     * Dispatch documents.
     * <p/>
     * This will dispatch documents until there is either:
     * <ul>
     * <li>no more documents available</li>
     * <li>the {@link #stop} method is invoked, from another thread</li>
     * </ul>
     * If {@link #stop} is called, only the executing dispatch terminates.
     */
    public void dispatch() {
        dispatch(new ErrorHandler() {
            @Override
            public boolean terminateOnError() {
                return false;
            }

            @Override
            public void error(Throwable exception) {
                log.error(exception.getMessage(), exception);
            }
        });
    }

    /**
     * Dispatch documents.
     * <p/>
     * This will dispatch documents until there is either:
     * <ul>
     * <li>no more documents available</li>
     * <li>the {@link #stop} method is invoked, from another thread</li>
     * <li>an error occurs, and the supplied handler's {@link ErrorHandler#terminateOnError} method returns
     * {@code true}</li>
     * </ul>
     * If {@link #stop} is called, only the executing dispatch terminates.
     *
     * @return the no. of documents processed
     */
    @Override
    public synchronized int dispatch(ErrorHandler handler) {
        // NOTE: synchronized as individual inboxes need to be processed synchronously
        int result = 0;
        try {
            ESCISuppliers helper = new ESCISuppliers(service);
            List<ESCIConfig> suppliers = helper.getESCIConfigs();
            Iterator<ESCIConfig> iterator = suppliers.iterator();
            while (!stop && iterator.hasNext()) {
                ESCIConfig config = iterator.next();
                Inbox inbox = getInbox(config, handler);
                if (inbox != null) {
                    result += dispatch(inbox, handler);
                }
            }
        } finally {
            stop = false;
        }
        return result;
    }

    /**
     * Process a single document.
     *
     * @param inbox     the inbox
     * @param reference the document reference
     * @param config    configures the document processing behaviour. May be {@code null}
     * @throws ESCIAdapterException for any error
     */
    @Override
    public synchronized void process(Inbox inbox, DocumentReferenceType reference, ProcessingConfig config) {
        // NOTE: synchronized as individual inboxes need to be processed synchronously
        InboxDispatcher dispatcher = new InboxDispatcher(inbox, processors);
        dispatcher.process(reference, config);
    }

    /**
     * Remove a document without processing it.
     *
     * @param inbox     the inbox
     * @param reference the document reference
     * @throws ESCIAdapterException for any error
     */
    @Override
    public synchronized void delete(Inbox inbox, DocumentReferenceType reference) {
        // NOTE: synchronized as individual inboxes need to be processed synchronously
        InboxDispatcher dispatcher = new InboxDispatcher(inbox, processors);
        dispatcher.delete(reference);
    }

    /**
     * Flags the current dispatch to stop.
     * <p/>
     * This does not block waiting for the dispatch to complete.
     */
    public void stop() {
        this.stop = true;
    }

    /**
     * Dispatch documents from the supplied inbox.
     *
     * @param inbox   the inbox to read
     * @param handler the error handler
     * @return the number of documents processed
     * @throws ESCIAdapterException      for any ESCI adapter error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public synchronized int dispatch(Inbox inbox, ErrorHandler handler) {
        // NOTE: synchronized as individual inboxes need to be processed synchronously
        int result = 0;
        try {
            InboxDispatcher dispatcher = new InboxDispatcher(inbox, processors);
            while (!stop && dispatcher.hasNext()) {
                if (dispatcher.dispatch()) {
                    ++result;
                }
            }
        } catch (ESCIAdapterException exception) {
            handler.error(exception);
            if (handler.terminateOnError()) {
                throw exception;
            }
        } catch (Exception cause) {
            ESCIAdapterException exception = new ESCIAdapterException(ESCIAdapterMessages.failedToProcessInbox(
                    inbox.getSupplier(), inbox.getStockLocation(), cause.getMessage()), cause);
            handler.error(exception);
            if (handler.terminateOnError()) {
                throw exception;
            }
        }
        return result;
    }

    /**
     * Returns the inbox for the given ESCI configuration.
     *
     * @param config  the ESCI configuration
     * @param handler the error handler
     * @return the inbox, or {@code null} if no inbox can be obtained
     * @throws ESCIAdapterException      for any ESCI adapter error
     * @throws ArchetypeServiceException for any archetype service error
     */
    private Inbox getInbox(ESCIConfig config, ErrorHandler handler) {
        Inbox result = null;
        try {
            InboxService inboxService = locator.getInboxService(config);
            result = new Inbox(config, inboxService);
        } catch (ESCIAdapterException exception) {
            handler.error(exception);
            if (handler.terminateOnError()) {
                throw exception;
            }
        } catch (Exception cause) {
            ESCIAdapterException exception = new ESCIAdapterException(ESCIAdapterMessages.failedToProcessInbox(
                    config.getSupplier(), config.getStockLocation(), cause.getMessage()), cause);
            handler.error(exception);
            if (handler.terminateOnError()) {
                throw exception;
            }
        }
        return result;
    }

}
