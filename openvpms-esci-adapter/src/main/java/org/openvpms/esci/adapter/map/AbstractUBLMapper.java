/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.map;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.map.invoice.DefaultOrderResolver;
import org.openvpms.esci.adapter.util.ESCIAdapterException;


/**
 * Base class for mappers between UBL to OpenVPMS types.
 *
 * @author Tim Anderson
 */
public class AbstractUBLMapper {

    /**
     * Expected UBL version.
     */
    protected static final String UBL_VERSION = "2.0";

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs an {@link AbstractUBLMapper}.
     *
     * @param service the archetype service
     */
    public AbstractUBLMapper(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Verifies that the UBL version matches that expected.
     *
     * @param document the UBL document
     * @throws ESCIAdapterException if the UBL identifier is {@code null} or not the expected value
     */
    protected void checkUBLVersion(UBLDocument document) {
        String version = document.getUBLVersionID();
        if (!UBL_VERSION.equals(version)) {
            throw new ESCIAdapterException(ESCIAdapterMessages.ublInvalidValue("UBLVersionID", document.getType(),
                                                                               document.getID(), UBL_VERSION, version));
        }
    }

    /**
     * Verifies that an order has a relationship to the expected supplier and stock location.
     *
     * @param order         the order
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param document      the invoice
     * @throws ESCIAdapterException      if the order wasn't submitted by the supplier
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected void checkOrder(FinancialAct order, Party supplier, Party stockLocation, UBLDocument document) {
        DefaultOrderResolver resolver = new DefaultOrderResolver(service);
        if (!resolver.checkOrder(order, supplier, stockLocation, document)) {
            throw new ESCIAdapterException(ESCIAdapterMessages.invalidOrder(document.getType(), document.getID(),
                                                                            Long.toString(order.getId())));
        }
    }

}
