/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.dispatcher.invoice;

import org.openvpms.component.model.act.FinancialAct;


/**
 * A listener for invoices received by the {@link InvoiceProcessor}.
 *
 * @author Tim Anderson
 */
public interface InvoiceListener {

    /**
     * Invoked when an invoice has been received and mapped to a delivery.
     *
     * @param delivery the delivery
     */
    void receivedInvoice(FinancialAct delivery);

}
