/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Helper to contain the invoice mapping context.
 */
class MappingContext {

    /**
     * The invoice.
     */
    private final UBLInvoice invoice;

    /**
     * The supplier.
     */
    private final Party supplier;

    /**
     * The stock location.
     */
    private final Party stockLocation;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document-level order.  May be {@code null}
     */
    private FinancialAct docOrder;

    /**
     * The orders associated with the invoice.
     */
    private Map<Reference, FinancialAct> orders = new HashMap<>();

    /**
     * The orders items keyed on their orders.
     */
    private Map<FinancialAct, List<FinancialAct>> items = new HashMap<>();

    /**
     * The invoice lines, and associated order items.
     */
    private List<InvoiceLineState> lines;


    /**
     * Constructs a {@link MappingContext}.
     *
     * @param invoice       the invoice
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param docOrder      the document-level order. May be {@code null}
     * @param service       the archetype service
     */
    public MappingContext(UBLInvoice invoice, Party supplier, Party stockLocation, FinancialAct docOrder,
                          ArchetypeService service) {
        this.invoice = invoice;
        this.supplier = supplier;
        this.stockLocation = stockLocation;
        this.docOrder = docOrder;
        this.service = service;
        if (docOrder != null) {
            addOrder(docOrder);
        }
    }

    /**
     * Returns the invoice.
     *
     * @return the invoice
     */
    public UBLInvoice getInvoice() {
        return invoice;
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    public Party getSupplier() {
        return supplier;
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location
     */
    public Party getStockLocation() {
        return stockLocation;
    }

    /**
     * Returns the document-level order.
     *
     * @return the document level order, or {@code null} if none was specified
     */
    public FinancialAct getDocumentOrder() {
        return docOrder;
    }

    /**
     * Returns an order given its reference.
     * <p/>
     * The order must have been added previously via {@link #addOrder}, or be the document order.
     *
     * @param reference the order reference
     * @return the corresponding order, or {@code null} if it is not found
     */
    public FinancialAct getOrder(Reference reference) {
        return orders.get(reference);
    }

    /**
     * Returns all orders associated with the invoice.
     *
     * @return the orders
     */
    public List<FinancialAct> getOrders() {
        return new ArrayList<>(orders.values());
    }

    /**
     * Adds an order associated with the invoice.
     *
     * @param order the order
     */
    public void addOrder(FinancialAct order) {
        orders.put(order.getObjectReference(), order);
    }

    /**
     * Adds order items associated with an order.
     *
     * @param order the order
     */
    public void addOrderItems(FinancialAct order) {
        IMObjectBean bean = service.getBean(order);
        List<FinancialAct> list = bean.getTargets("items", FinancialAct.class);
        items.put(order, list);
    }

    /**
     * Returns the order items, keyed on their orders.
     *
     * @return the order items
     */
    public Map<FinancialAct, List<FinancialAct>> getOrderItems() {
        return items;
    }

    /**
     * Returns an order item given its reference.
     *
     * @param order   the order
     * @param itemRef the order item reference
     * @return the order item, or {@code null} if none can be found
     */
    public FinancialAct getOrderItem(FinancialAct order, Reference itemRef) {
        List<FinancialAct> list = items.get(order);
        if (list != null) {
            for (FinancialAct item : list) {
                if (item.getId() == itemRef.getId()) {
                    return item;
                }
            }
        }
        return null;
    }

    /**
     * Registers the invoice lines.
     *
     * @param lines the invoice lines
     */
    public void setInvoiceLines(List<InvoiceLineState> lines) {
        this.lines = lines;
    }

    /**
     * Returns the invoice lines.
     *
     * @return the invoice lines
     */
    public List<InvoiceLineState> getInvoiceLines() {
        return lines;
    }

}
