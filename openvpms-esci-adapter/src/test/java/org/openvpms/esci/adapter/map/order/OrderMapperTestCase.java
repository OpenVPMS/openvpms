/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.order;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.ArchetypeServiceFunctions;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.esci.adapter.AbstractESCITest;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.common.AmountType;
import org.openvpms.esci.ubl.common.IdentifierType;
import org.openvpms.esci.ubl.common.QuantityType;
import org.openvpms.esci.ubl.common.aggregate.AddressType;
import org.openvpms.esci.ubl.common.aggregate.ContactType;
import org.openvpms.esci.ubl.common.aggregate.CustomerPartyType;
import org.openvpms.esci.ubl.common.aggregate.ItemType;
import org.openvpms.esci.ubl.common.aggregate.LineItemType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineType;
import org.openvpms.esci.ubl.common.aggregate.PartyType;
import org.openvpms.esci.ubl.common.aggregate.SupplierPartyType;
import org.openvpms.esci.ubl.common.basic.PayableAmountType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.io.UBLDocumentWriter;
import org.openvpms.esci.ubl.order.OrderType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;


/**
 * Tests the {@link OrderMapperImpl} class.
 *
 * @author Tim Anderson
 */
public class OrderMapperTestCase extends AbstractESCITest {

    /**
     * The currencies.
     */
    @Autowired
    private Currencies currencies;

    /**
     * The practice location contact.
     */
    private Contact practiceContact;

    /**
     * Phone contact.
     */
    private Contact phoneContact;

    /**
     * Fax contact.
     */
    private Contact faxContact;

    /**
     * Email contact.
     */
    private Contact emailContact;

    /**
     * The supplier contact.
     */
    private Contact supplierContact;

    /**
     * Order author.
     */
    private User author;

    /**
     * Helper functions.
     */
    private ArchetypeServiceFunctions functions;

    /**
     * Id for the customer, assigned by the supplier.
     */
    private static final String SUPPLIER_ACCOUNT_ID = "ANACCOUNTID";

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        functions = new ArchetypeServiceFunctions(getArchetypeService(), getLookupService());
        Party practice = getPractice();
        practice.getContacts().clear();
        practiceContact = TestHelper.createLocationContact("1 Broadwater Avenue", "CAPE_WOOLAMAI", "VIC", "3925");
        practice.addContact(practiceContact);

        phoneContact = createContact(ContactArchetypes.PHONE, "telephoneNumber", "59527054");
        practice.addContact(phoneContact);
        faxContact = createContact(ContactArchetypes.PHONE, "telephoneNumber", "59527053");
        faxContact.addClassification(TestHelper.getLookup(ContactArchetypes.PURPOSE, ContactArchetypes.FAX_PURPOSE));
        practice.addContact(faxContact);

        emailContact = createContact(ContactArchetypes.EMAIL, "emailAddress", "foo@bar.com");
        practice.addContact(emailContact);

        save(practice);

        supplierContact = TestHelper.createLocationContact("2 Peko Rd", "TENNANT_CREEK", "NT", "0862");
        Party supplier = getSupplier();
        supplier.addContact(supplierContact);

        // create a user for associating with orders
        author = TestHelper.createUser();

        // add a supplier/stock location relationship
        Party stockLocation = getStockLocation();
        IMObjectBean supplierBean = getBean(supplier);
        Relationship relationship = supplierBean.addTarget("stockLocations",
                                                           SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI,
                                                           stockLocation);
        IMObjectBean bean = getBean(relationship);
        bean.setValue("accountId", SUPPLIER_ACCOUNT_ID);
        bean.setValue("serviceURL", "https://foo.openvpms.org/orderservice");

        save(supplier, stockLocation);

        // set up the authentication context, to ensure createdBy/updatedBy populated.
        new AuthenticationContextImpl().setUser(author);
    }

    /**
     * Verifies that an <em>act.supplierOrder</em> can be mapped to a UBL order, and serialized.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMap() throws Exception {
        BigDecimal quantity = new BigDecimal(5);
        BigDecimal unitPrice = new BigDecimal(10);
        String reorderCode = "ABC123";
        String reorderDesc = "A reorderDesc";

        // create an order with a single item, and post it
        FinancialAct actItem = createOrderItem(getProduct(), quantity, 1, unitPrice);
        IMObjectBean itemBean = getBean(actItem);
        itemBean.setValue("reorderCode", reorderCode);
        itemBean.setValue("reorderDescription", reorderDesc);
        itemBean.setValue("packageSize", 25);

        FinancialAct act = createOrder(getSupplier(), actItem);
        act.setStatus(ActStatus.POSTED);
        save(act);

        OrderMapper mapper = createMapper();
        OrderType order = mapper.map(act);

        // serialize the order and re-read it, to ensure it passes validation
        UBLDocumentContext context = new UBLDocumentContext();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        context.createWriter().write(order, stream);
        order = (OrderType) context.createReader().read(new ByteArrayInputStream(stream.toByteArray()));

        UBLDocumentWriter writer = context.createWriter();
        writer.setFormat(true);
        writer.write(order, System.out);

        // verify the order has the expected content
        assertEquals("2.0", order.getUBLVersionID().getValue());
        checkID(order.getID(), act.getId());
        assertFalse(order.getCopyIndicator().isValue());

        checkDate(order.getIssueDate().getValue(), act.getActivityStartTime());

        checkCustomer(order.getBuyerCustomerParty(), getPractice().getName(), getStockLocation(), practiceContact);
        checkContact(order.getBuyerCustomerParty().getParty().getContact(), author.getName(), phoneContact, faxContact,
                     emailContact);
        checkSupplier(order.getSellerSupplierParty(), getSupplier(), supplierContact);

        PayableAmountType amount = order.getAnticipatedMonetaryTotal().getPayableAmount();
        checkAmount(amount, 50);

        assertEquals(1, order.getOrderLine().size());
        OrderLineType line1 = order.getOrderLine().get(0);
        LineItemType item1 = line1.getLineItem();
        checkID(item1.getID(), actItem.getId());
        checkQuantity(item1.getQuantity(), "BX", 5);
        checkQuantity(item1.getItem().getPackQuantity(), "BX", 1);
        checkEquals(new BigDecimal("25"), item1.getItem().getPackSizeNumeric().getValue());
        checkAmount(item1.getLineExtensionAmount(), 50);
        checkAmount(item1.getTotalTaxAmount(), 0);
        checkAmount(item1.getPrice().getPriceAmount(), 10);
        checkQuantity(item1.getPrice().getBaseQuantity(), "BX", 1);
        checkItem(item1.getItem(), getProduct(), reorderCode, reorderDesc);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if there is no relationship between a supplier
     * and stock location.
     */
    @Test
    public void testNoSupplierStockLocationRelationship() {
        FinancialAct order = createOrder();

        // remove the supplier/stock location
        Party supplier = getSupplier();
        Party location = getStockLocation();
        IMObjectBean bean = getBean(supplier);
        EntityRelationship relationship = bean.getValue("stockLocations", EntityRelationship.class,
                                                        Predicates.targetEquals(location));
        supplier.removeEntityRelationship(relationship);
        location.removeEntityRelationship(relationship);
        save(supplier, location);

        String expected = "ESCIA-0001: e-Supply Chain Interface support is not configured for " + supplier.getName()
                          + " (" + supplier.getId() + ") and " + location.getName() + " (" + location.getId() + ")";
        checkMappingException(order, expected);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if the product has no supplier order code.
     */
    @Test
    public void testNoSupplierOrderCode() {
        Product product = getProduct();
        Party supplier = getSupplier();
        // create an order with a single item, and no reorder code, and post it
        FinancialAct orderItem = createOrderItem(product, BigDecimal.ONE, 1, BigDecimal.ONE);
        IMObjectBean itemBean = getBean(orderItem);
        itemBean.setValue("reorderCode", null);

        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order);

        String expected = "ESCIA-0300: Supplier " + supplier.getName() + " (" + supplier.getId()
                          + ") has no order code for product " + product.getName() + " (" + product.getId() + ")";
        checkMappingException(order, expected);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if the stock location is not linked to a practice
     * location.
     */
    @Test
    public void testNoPracticeLocationForStockLocation() {
        FinancialAct order = createOrder();

        Party stockLocation = getStockLocation();
        Party location = getPracticeLocation();

        IMObjectBean bean = getBean(location);
        EntityRelationship relationship = bean.getValue("stockLocations", EntityRelationship.class,
                                                        Predicates.targetEquals(stockLocation));
        stockLocation.removeEntityRelationship(relationship);
        location.removeEntityRelationship(relationship);
        save(stockLocation, location);

        String expected = "ESCIA-0301: Stock location " + stockLocation.getName() + " (" + stockLocation.getId()
                          + ") is not associated with any practice location";
        checkMappingException(order, expected);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if an amount exceeds the allowed no. of decimal places.
     */
    @Test
    public void testLossOfAmountPrecision() {
        FinancialAct orderItem = createOrderItem(getProduct(), BigDecimal.ONE, 1, new BigDecimal("1.625"));
        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        IMObjectBean bean = getBean(orderItem);
        bean.setValue("reorderCode", "AREORDERCODE");
        save(orderItem, order);
        String expected = "ESCIA-0115: Too many decimal places in amount: 1.625";
        checkMappingException(order, expected);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if a quantity exceeds the allowed no. of decimal places.
     */
    @Test
    public void testLossOfQuantityPrecision() {
        BigDecimal quantity = new BigDecimal("0.625");
        BigDecimal unitPrice = BigDecimal.valueOf(2);
        FinancialAct orderItem = createOrderItem(getProduct(), quantity, 1, unitPrice);
        FinancialAct order = createOrder(getSupplier(), orderItem);
        order.setStatus(ActStatus.POSTED);
        IMObjectBean bean = getBean(orderItem);
        bean.setValue("reorderCode", "AREORDERCODE");
        save(orderItem, order);

        String expected = "ESCIA-0116: Too many decimal places in quantity: 0.625";
        checkMappingException(order, expected);
    }

    /**
     * Verifies that when a unit price is not present, the item price is zero.
     */
    @Test
    public void testMissingUnitPrice() {
        BigDecimal quantity = new BigDecimal(5);

        // create an order with a single item, and post it
        FinancialAct actItem = createOrderItem(getProduct(), quantity, 1, null);
        FinancialAct act = createOrder(getSupplier(), actItem);
        act.setStatus(ActStatus.POSTED);
        IMObjectBean bean = getBean(actItem);
        String reorderCode = "AREORDERCODE";
        bean.setValue("reorderCode", reorderCode);
        save(actItem, act);
        OrderMapper mapper = createMapper();
        OrderType order = mapper.map(act);

        assertEquals(1, order.getOrderLine().size());
        OrderLineType line1 = order.getOrderLine().get(0);
        LineItemType item1 = line1.getLineItem();
        checkID(item1.getID(), actItem.getId());
        checkQuantity(item1.getQuantity(), "BX", 5);
        checkQuantity(item1.getItem().getPackQuantity(), "BX", 1);
        checkEquals(1, item1.getItem().getPackSizeNumeric().getValue());
        checkAmount(item1.getLineExtensionAmount(), 0);
        checkAmount(item1.getTotalTaxAmount(), 0);
        checkAmount(item1.getPrice().getPriceAmount(), 0);
        checkQuantity(item1.getPrice().getBaseQuantity(), "BX", 1);
        checkItem(item1.getItem(), getProduct(), reorderCode, null);

    }

    /**
     * Helper to create a new POSTED order.
     *
     * @return a new order
     */
    @Override
    protected FinancialAct createOrder() {
        BigDecimal quantity = new BigDecimal(5);
        BigDecimal unitPrice = new BigDecimal(10);

        // create an order with a single item, and post it
        FinancialAct actItem = createOrderItem(getProduct(), quantity, 1, unitPrice);
        FinancialAct act = createOrder(getSupplier(), actItem);
        act.setStatus(ActStatus.POSTED);
        IMObjectBean bean = getBean(act);
        bean.save();
        return act;
    }

    /**
     * Creates a new order mapper.
     *
     * @return a new mapper
     */
    private OrderMapper createMapper() {
        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        return new OrderMapperImpl(new PracticeRules(service, currencies),
                                   new LocationRules(service), new PartyRules(service, lookups),
                                   new SupplierRules(service), lookups, currencies, service);
    }

    /**
     * Verifies that an invalid order fails mapping with a exception.
     *
     * @param order           the order
     * @param expectedMessage the expected error message
     */
    private void checkMappingException(FinancialAct order, String expectedMessage) {
        OrderMapper mapper = createMapper();
        try {
            mapper.map(order);
            fail("Expected mapping to fail");
        } catch (ESCIAdapterException exception) {
            assertEquals(expectedMessage, exception.getMessage());
        }
    }

    /**
     * Verifies a customer matches that expected.
     *
     * @param customer the customer to check
     * @param name     the expected customer name
     * @param expected the expected customer
     * @param contact  the expected contact
     */
    private void checkCustomer(CustomerPartyType customer, String name, Party expected, Contact contact) {
        checkID(customer.getCustomerAssignedAccountID(), expected.getId());
        assertEquals(SUPPLIER_ACCOUNT_ID, customer.getSupplierAssignedAccountID().getValue());
        checkParty(customer.getParty(), name, contact);
    }

    /**
     * Verifies a contact matches that expected.
     *
     * @param contact      the contact to check
     * @param name         the expected contact name
     * @param phoneContact the expected phone contact
     * @param faxContact   the expected fax contact
     * @param emailContact the expected email contact
     */
    private void checkContact(ContactType contact, String name, Contact phoneContact, Contact faxContact,
                              Contact emailContact) {
        assertEquals(name, contact.getName().getValue());
        assertEquals(getBean(phoneContact).getString("telephoneNumber"), contact.getTelephone().getValue());
        assertEquals(getBean(faxContact).getString("telephoneNumber"), contact.getTelefax().getValue());
        assertEquals(getBean(emailContact).getString("emailAddress"), contact.getElectronicMail().getValue());
    }

    /**
     * Verifies that a supplier matches that expected.
     *
     * @param supplier the supplier
     * @param expected the expected supplier
     * @param contact  the expected contact
     */
    private void checkSupplier(SupplierPartyType supplier, Party expected, Contact contact) {
        checkID(supplier.getCustomerAssignedAccountID(), expected.getId());
        checkParty(supplier.getParty(), expected.getName(), contact);
    }

    /**
     * Verifies an ID matches that expected.
     *
     * @param id       the to check
     * @param expected the expected id
     */
    private void checkID(IdentifierType id, long expected) {
        assertEquals(Long.toString(expected), id.getValue());
    }

    /**
     * Verifies a date matches that expected.
     *
     * @param calendar     the date to check
     * @param expectedDate the expected value
     */
    private void checkDate(XMLGregorianCalendar calendar, Date expectedDate) {
        java.sql.Date date = new java.sql.Date(expectedDate.getTime());
        assertEquals(date.toString(), calendar.toString());
    }

    /**
     * Checks a quanity.
     *
     * @param quantity the quantity to check
     * @param code     the expected code
     * @param value    the expected value
     */
    private void checkQuantity(QuantityType quantity, String code, int value) {
        assertEquals(code, quantity.getUnitCode());
        assertEquals(value, quantity.getValue().toBigInteger().intValue());
    }

    /**
     * Checks an amount.
     *
     * @param amount the amount to check
     * @param value  the expected value
     */
    private void checkAmount(AmountType amount, double value) {
        assertEquals("AUD", amount.getCurrencyID().value());
        checkEquals(new BigDecimal(value), amount.getValue());
    }

    /**
     * Checks an item.
     *
     * @param item        the item to check
     * @param product     the expected product
     * @param reorderCode the expected reorder code
     * @param reorderDesc the expected reorder description
     */
    private void checkItem(ItemType item, Product product, String reorderCode, String reorderDesc) {
        assertEquals(product.getName(), item.getName().getValue());
        checkID(item.getBuyersItemIdentification().getID(), product.getId());
        assertEquals(reorderCode, item.getSellersItemIdentification().getID().getValue());
        if (reorderDesc != null) {
            assertEquals(reorderDesc, item.getDescription().get(0).getValue());
        } else {
            assertEquals(0, item.getDescription().size());
        }
    }

    /**
     * Checks a party.
     *
     * @param party   the party to check
     * @param name    the expected party name
     * @param contact the expected contact
     */
    private void checkParty(PartyType party, String name, Contact contact) {
        assertEquals(1, party.getPartyName().size());
        assertEquals(name, party.getPartyName().get(0).getName().getValue());

        IMObjectBean bean = getBean(contact);
        String address = bean.getString("address");
        String cityName = functions.lookup(contact, "suburb");
        String countrySubentity = functions.lookup(contact, "state");
        String postalZone = bean.getString("postcode");

        AddressType postalAddress = party.getPostalAddress();
        assertEquals(1, postalAddress.getAddressLine().size());
        assertEquals(address, postalAddress.getAddressLine().get(0).getLine().getValue());
        assertEquals(cityName, postalAddress.getCityName().getValue());
        assertEquals(postalZone, postalAddress.getPostalZone().getValue());
        assertEquals(countrySubentity, postalAddress.getCountrySubentity().getValue());
    }

    /**
     * Helper to create a new contact.
     *
     * @param shortName the contact archetype short name
     * @param name      the node name to populate
     * @param value     the value to populate the node with
     * @return a new contact
     */
    private Contact createContact(String shortName, String name, String value) {
        Contact contact = create(shortName, Contact.class);
        IMObjectBean bean = getBean(contact);
        bean.setValue(name, value);
        return contact;
    }
}
