/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.client.jaxws;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.model.party.Party;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.adapter.AbstractESCITest;
import org.openvpms.esci.adapter.client.InVMSupplierServiceLocator;
import org.openvpms.esci.adapter.client.SupplierServiceLocator;
import org.openvpms.esci.adapter.dispatcher.ESCIConfig;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.service.DelegatingOrderService;
import org.openvpms.esci.service.DelegatingRegistryService;
import org.openvpms.esci.service.OrderService;
import org.openvpms.esci.service.RegistryService;
import org.openvpms.esci.service.client.DefaultServiceLocatorFactory;
import org.openvpms.esci.service.exception.DuplicateOrderException;
import org.openvpms.esci.ubl.order.Order;
import org.openvpms.esci.ubl.order.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.remoting.jaxws.JaxWsPortClientInterceptor;
import org.springframework.test.context.ContextConfiguration;

import javax.xml.ws.WebServiceException;
import java.net.SocketException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


/**
 * Tests the {@link SupplierWebServiceLocator} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("/OrderWebServiceTest-context.xml")
public class SupplierWebServiceLocatorTestCase extends AbstractESCITest {

    /**
     * The delegating registry service.
     */
    @Autowired
    private DelegatingRegistryService delegatingRegistryService;

    /**
     * The delegating order service.
     */
    @Autowired
    private DelegatingOrderService delegatingOrderService;


    /**
     * Verifies that the OrderService can be obtained with
     * {@link SupplierServiceLocator#getOrderService(ESCIConfig)} and its methods invoked.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGetServiceByESCIConfiguration() throws Exception {
        FutureValue<OrderType> future = new FutureValue<>();
        registerOrderService(future);

        Party supplier = getSupplier();
        Party location = getStockLocation();
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        addESCIConfiguration(supplier, location, wsdl);

        SupplierServiceLocator locator = createLocator(0);
        ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
        assertNotNull(config);
        OrderService service = locator.getOrderService(config);
        service.submitOrder(new Order());

        OrderType received = future.get(1000);
        assertNotNull(received);
    }

    /**
     * Verifies that the OrderService can be obtained with
     * {@link SupplierServiceLocator#getOrderService(String, String, String)} and its methods invoked.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGetServiceByURL() throws Exception {
        FutureValue<OrderType> future = new FutureValue<>();
        registerOrderService(future);

        SupplierServiceLocator locator = createLocator(0);
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        OrderService service = locator.getOrderService(wsdl, "foo", "bar");
        service.submitOrder(new Order());

        OrderType received2 = future.get(1000);
        assertNotNull(received2);
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if the supplier order service URL is invalid.
     */
    @Test
    public void testInvalidOrderServiceURLForSupplier() {
        Party supplier = getSupplier();
        Party location = getStockLocation();
        addESCIConfiguration(supplier, location, "invalidURL");
        try {
            SupplierServiceLocator locator = createLocator(0);
            ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
            locator.getOrderService(config);
            fail("Expected getOrderService() to fail");
        } catch (ESCIAdapterException exception) {
            assertEquals("ESCIA-0002: invalidURL is not a valid service URL for supplier " + supplier.getName() + " ("
                         + supplier.getId() + ")", exception.getMessage());
        }
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if the supplier URL is invalid.
     */
    @Test
    public void testInvalidURL() {
        try {
            SupplierServiceLocator locator = createLocator(0);
            locator.getOrderService("invalidURL", "foo", "bar");
            fail("Expected getOrderService() to fail");
        } catch (ESCIAdapterException exception) {
            assertEquals("ESCIA-0003: invalidURL is not a valid service URL", exception.getMessage());
        }
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if a service cannot be contacted.
     */
    @Test
    public void testConnectionFailed() {
        try {
            SupplierServiceLocator locator = createLocator(0);
            locator.getOrderService("http://localhost:8888", "foo", "bar");
            fail("Expected getOrderService() to fail");
        } catch (ESCIAdapterException exception) {
            String message = "ESCIA-0004: Failed to connect to web service http://localhost:8888";
            assertEquals(message, exception.getMessage());
        }
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if a supplier service cannot be contacted.
     */
    @Test
    public void testConnectionFailedForSupplierService() {
        Party supplier = getSupplier();
        Party location = getStockLocation();
        addESCIConfiguration(supplier, location, "http://localhost:8888");
        try {
            SupplierServiceLocator locator = createLocator(0);
            ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
            locator.getOrderService(config);
            fail("Expected getOrderService() to fail");
        } catch (ESCIAdapterException exception) {
            String message = "ESCIA-0005: Failed to connect to web service http://localhost:8888 for supplier "
                             + supplier.getName() + " (" + supplier.getId() + ")";
            assertEquals(message, exception.getMessage());
        }
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is thrown if a call cannot be made in the required time..
     *
     * @throws Exception for any error
     */
    @Test
    public void testConnectionTimeout() throws Exception {
        delegatingOrderService.setOrderService(order -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ignore) {
                // do nothing
            }
        });
        Party supplier = getSupplier();
        Party location = getStockLocation();
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        addESCIConfiguration(supplier, location, wsdl);

        SupplierServiceLocator locator = createLocator(1);
        ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
        OrderService service = locator.getOrderService(config);
        try {
            service.submitOrder(new Order());
            fail("Expected submitOrder() to fail");
        } catch (ESCIAdapterException expected) {
            assertEquals("ESCIA-0007: Web service did not respond in time " + wsdl + " for supplier "
                         + supplier.getName() + " (" + supplier.getId() + ")", expected.getMessage());
        }
    }

    /**
     * Verifies that {@link DuplicateOrderException} are propagated back to the client.
     */
    @Test
    public void testDuplicateOrderException() {
        delegatingOrderService.setOrderService(order -> {
            throw new DuplicateOrderException("Duplicate order");
        });
        Party supplier = getSupplier();
        Party location = getStockLocation();
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        addESCIConfiguration(supplier, location, wsdl);

        SupplierServiceLocator locator = createLocator(1);
        ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
        OrderService service = locator.getOrderService(config);
        try {
            service.submitOrder(new Order());
            fail("Expected submitOrder() to fail");
        } catch (DuplicateOrderException expected) {
            assertEquals("Duplicate order", expected.getMessage());
        }
    }

    /**
     * Verifies that server failures that trigger {@code SOAPFaultException} are rethrown on the client as
     * {@link ESCIAdapterException}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSoapException() throws Exception {
        delegatingOrderService.setOrderService(order -> {
            throw new RuntimeException("Server failure");
        });
        Party supplier = getSupplier();
        Party location = getStockLocation();
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        addESCIConfiguration(supplier, location, wsdl);

        SupplierServiceLocator locator = createLocator(1);
        ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
        OrderService service = locator.getOrderService(config);
        try {
            service.submitOrder(new Order());
            fail("Expected submitOrder() to fail");
        } catch (ESCIAdapterException expected) {
            assertEquals("ESCIA-0008: Web service error: Client received SOAP Fault from server: Server failure " +
                         "Please see the server log to find more detail regarding exact cause of the failure.",
                         expected.getMessage());
        }
    }

    /**
     * Verifies that if a {@link RemoteAccessException} thrown by {@link JaxWsPortClientInterceptor} is rethrown
     * as an {@link ESCIAdapterException}.
     */
    @Test
    public void testRemoteAccessException() {
        Party supplier = getSupplier();
        Party location = getStockLocation();
        String wsdl = getWSDL("wsdl/RegistryService.wsdl");
        addESCIConfiguration(supplier, location, wsdl);

        // create a SupplierServiceLocator that simulates a RemoteAccessException when OrderService.order()
        // is invoked.
        SupplierServiceLocator locator = new InVMSupplierServiceLocator(new DefaultServiceLocatorFactory()) {
            @Override
            public OrderService getOrderService(ESCIConfig config) {
                SupplierServices services = new SupplierServices(config) {
                    @Override
                    @SuppressWarnings("unchecked")
                    protected <T> T doGetService(Class<T> clazz, String url, String endpointAddress) {
                        if (OrderService.class.equals(clazz)) {
                            return (T) (OrderService) order -> {
                                throw new RemoteAccessException(
                                        "Could not access remote service ",
                                        new WebServiceException(new SocketException("Connection reset")));
                            };
                        }
                        return super.doGetService(clazz, url, endpointAddress);
                    }
                };
                return services.getOrderService("in-vm://orderService", "in-vm://registryService");
            }
        };
        ESCIConfig config = ESCIConfig.create(supplier, location, getArchetypeService());
        OrderService service = locator.getOrderService(config);
        try {
            service.submitOrder(new Order());
            fail("Expected exception to be thrown");
        } catch (Exception exception) {
            String message = "ESCIA-0005: Failed to connect to web service " + wsdl + " for supplier "
                             + supplier.getName() + " (" + supplier.getId() + ")";
            assertEquals(message, exception.getMessage());
        }
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        applicationContext.getBean("registryService"); // force registration of the registry dispatcher
        applicationContext.getBean("orderService");    // force registration of the order dispatcher
        delegatingRegistryService.setRegistry(new RegistryService() {
            public String getInboxService() {
                return getWSDL("wsdl/InboxService.wsdl");
            }

            public String getOrderService() {
                return getWSDL("wsdl/OrderService.wsdl");
            }
        });
    }

    /**
     * Creates a new supplier service locator.
     *
     * @param timeout the connection timeout
     * @return a new supplier service locator
     */
    private SupplierWebServiceLocator createLocator(int timeout) {
        return new InVMSupplierServiceLocator(new DefaultServiceLocatorFactory(), timeout); // to test method invocation
    }

    /**
     * Registers the order service.
     *
     * @param future the value that will be updated when an order is received
     */
    private void registerOrderService(final FutureValue<OrderType> future) {
        delegatingOrderService.setOrderService(future::set);
    }

}
