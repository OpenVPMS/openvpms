/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.openvpms.component.model.object.Relationship;

import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

/**
 * A {@link PolicyBuilder} for {@link RelatedObjects}.
 * <p/>
 * This builds {@link RelatedObjects} instances with a new policy.
 *
 * @author Tim Anderson
 */
public interface RelatedObjectPolicyBuilder<T, R extends Relationship, P extends RelatedObjects<T, R, P>> {

    /**
     * Selects active relationships, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> active();

    /**
     * Selects relationships active at the specified time, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @param time the time
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> active(Date time);

    /**
     * Selects active objects.
     *
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> activeObjects();

    /**
     * Selects inactive objects.
     *
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> inactiveObjects();

    /**
     * Selects both active and inactive objects.
     *
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> anyObject();

    /**
     * Adds a predicate for filtering relationships.
     *
     * @param predicate the predicate
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> predicate(Predicate<R> predicate);

    /**
     * Adds a predicate that is a logical AND of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> and(Predicate<R> predicate);

    /**
     * Adds a predicate that is a logical OR of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> or(Predicate<R> predicate);

    /**
     * Sets the comparator for ordering relationships.
     *
     * @param comparator the comparator
     * @return this
     */
    RelatedObjectPolicyBuilder<T, R, P> comparator(Comparator<R> comparator);

    /**
     * Builds the {@link RelatedObjects} for the policy.
     *
     * @return an instance that selects objects according to the policy
     */
    P build();
}
