/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.archetype;

import java.util.List;

/**
 * A range of archetypes, with an optional default archetype.
 * <p/>
 * Archetypes may contain wildcards.
 *
 * @author Tim Anderson
 */
public interface ArchetypeRange {

    /**
     * Returns the range of archetypes.
     *
     * @return the range of archetypes. May contain wildcards.
     */
    List<String> getArchetypes();

    /**
     * Returns the default archetype.
     *
     * @return the default archetype. May be {@code null}
     */
    String getDefaultArchetype();

}