/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;

import java.util.Date;
import java.util.List;

/**
 * Queries related objects.
 *
 * @author Tim Anderson
 */
public interface RelatedObjects<T, R extends Relationship, P extends RelatedObjects<T, R, P>> {

    /**
     * Selects all related objects for retrieval.
     * <p/>
     * This is the default.
     *
     * @return an instance that selects all related objects
     */
    P all();

    /**
     * Selects active relationships and objects for retrieval.
     *
     * @return an instance that selects active relationships and objects
     */
    P active();

    /**
     * Selects relationships active at the specified time and returns active objects.
     *
     * @param time the time
     * @return an instance that selects active relationships at {@code time}, and active objects
     */
    P active(Date time);

    /**
     * Selects objects based on the specified policy.
     *
     * @param policy the policy
     * @return an instance that selects objects according to {@code policy}
     */
    P policy(Policy<R> policy);

    /**
     * Returns a policy builder to select objects.
     *
     * @return a new policy builder
     */
    RelatedObjectPolicyBuilder<T, R, P> newPolicy();

    /**
     * Returns the first object to match the {@link #policy(Policy) policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object, or {@code null} if none is found
     */
    T getObject();

    /**
     * Returns the first object and relationship to match the {@link #policy(Policy) policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object and relationship, or {@code null} if none is found
     */
    ObjectRelationship<T, R> getObjectRelationship();

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @return the related objects
     */
    Iterable<T> getObjects();

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the related objects
     */
    Iterable<T> getObjects(int firstResult, int maxResults);

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @param order the order criteria
     * @return the related objects
     */
    Iterable<T> getObjects(int firstResult, int maxResults, Order... order);

    /**
     * Returns the objects and their corresponding relationships matching the {@link #getPolicy() policy}.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @return the objects and their corresponding relationships
     */
    Iterable<ObjectRelationship<T, R>> getObjectRelationships();

    /**
     * Returns the objects and their corresponding relationships matching the {@link #getPolicy() policy},
     * supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the objects and their corresponding relationships
     */
    Iterable<ObjectRelationship<T, R>> getObjectRelationships(int firstResult, int maxResults);

    /**
     * Returns the relationships matching the {@link #getPolicy() policy}.
     *
     * @return the relationships matching the criteria
     */
    List<R> getRelationships();

    /**
     * Returns the relationship references matching the {@link #getPolicy() policy}.
     * <p/>
     * NOTE: this does not determine if the objects are active or inactive.
     *
     * @return the related object references
     */
    List<Reference> getReferences();

    /**
     * Returns the policy being used to select objects.
     *
     * @return the policy. May be {@code null} to indicate that all objects are returned
     */
    Policy<R> getPolicy();

}
