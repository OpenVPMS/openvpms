/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.archetype;

/**
 * The units of a {@link NodeDescriptor}.
 * <p/>
 * These can be held by another node, if the units vary, or be fixed, and provide a display name.
 *
 * @author Tim Anderson
 */
public interface Units {

    /**
     * Returns the name of the node holding the units.
     *
     * @return the node name, or {@code null} if the units are fixed
     */
    String getNode();

    /**
     * Returns the units display name.
     *
     * @return the units display name, or {@code null} if the units are held by a node.
     */
    String getDisplayName();
}