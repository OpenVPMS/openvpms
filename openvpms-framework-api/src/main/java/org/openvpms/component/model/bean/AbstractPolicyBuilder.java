/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.openvpms.component.model.bean.Policy.State;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.object.SequencedRelationship;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

/**
 * Abstract implementation of {@link PolicyBuilder}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPolicyBuilder<R extends Relationship> implements PolicyBuilder<R> {

    /**
     * The relationship type.
     */
    private final Class<R> type;

    /**
     * The predicate to select relationships.
     */
    private Predicate<R> predicate;

    /**
     * The active state that objects must have for retrieval.
     */
    private State state = State.ANY;

    /**
     * The relationship comparator, used to order relationships.
     */
    private Comparator<R> comparator;

    /**
     * Constructs an {@link AbstractPolicyBuilder}.
     *
     * @param type the relationship type
     */
    protected AbstractPolicyBuilder(Class<R> type) {
        this.type = type;
    }

    /**
     * Selects active relationships, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @return this
     */
    @Override
    public PolicyBuilder<R> active() {
        predicate = Predicates.activeNow();
        return activeObjects();
    }

    /**
     * Selects relationships active at the specified time, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @param time the time
     * @return this
     */
    @Override
    public PolicyBuilder<R> active(Date time) {
        predicate = Predicates.activeAt(time);
        return activeObjects();
    }

    /**
     * Selects active objects.
     *
     * @return this
     */
    @Override
    public PolicyBuilder<R> activeObjects() {
        state = State.ACTIVE;
        return this;
    }

    /**
     * Selects inactive objects.
     *
     * @return this
     */
    @Override
    public PolicyBuilder<R> inactiveObjects() {
        state = State.INACTIVE;
        return this;
    }

    /**
     * Selects both active and inactive objects.
     *
     * @return this
     */
    @Override
    public PolicyBuilder<R> anyObject() {
        state = State.ANY;
        return this;
    }

    /**
     * Sets the predicate for filtering relationships.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public PolicyBuilder<R> predicate(Predicate<R> predicate) {
        return and(predicate);
    }

    /**
     * Adds a predicate that is a logical AND of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public PolicyBuilder<R> and(Predicate<R> predicate) {
        this.predicate = (this.predicate == null) ? predicate : this.predicate.and(predicate);
        return this;
    }

    /**
     * Adds a predicate that is a logical OR of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public PolicyBuilder<R> or(Predicate<R> predicate) {
        this.predicate = (this.predicate == null) ? predicate : this.predicate.or(predicate);
        return this;
    }

    /**
     * Orders relationships on ascending sequence.
     * <p/>
     * Ignored if the relationships aren't sequenced.
     *
     * @return this
     */
    public PolicyBuilder<R> orderBySequence() {
        return orderBySequence(true);
    }

    /**
     * Orders relationships by sequence.
     * <p/>
     * Ignored if the relationships aren't sequenced.
     *
     * @param ascending if {@code true}, order on ascending sequence, else order on descending sequence
     * @return this
     */
    @SuppressWarnings("unchecked")
    public PolicyBuilder<R> orderBySequence(boolean ascending) {
        if (SequencedRelationship.class.isAssignableFrom(type)) {
            Comparator<R> comparator = (Comparator<R>) Comparator.comparingInt(SequencedRelationship::getSequence);
            this.comparator = (ascending) ? comparator : Collections.reverseOrder(comparator);
        } else {
            this.comparator = null;
        }
        return this;
    }

    /**
     * Sets the comparator for ordering relationships.
     *
     * @param comparator the comparator
     * @return this
     */
    @Override
    public PolicyBuilder<R> comparator(Comparator<R> comparator) {
        this.comparator = comparator;
        return this;
    }

    /**
     * Builds the policy
     *
     * @return the new policy
     */
    @Override
    public Policy<R> build() {
        return new Policies.DefaultPolicy<>(state, type, predicate, comparator);
    }
}
