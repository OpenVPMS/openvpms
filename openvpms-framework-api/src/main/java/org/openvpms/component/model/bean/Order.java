/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

/**
 * Order criteria.
 *
 * @author Tim Anderson
 */
public class Order {

    /**
     * The name of the node to sort on.
     */
    private final String name;

    /**
     * Determines if to order ascending or descending.
     */
    private final boolean ascending;

    /**
     * Constructs an {@link Order}.
     *
     * @param name      the name of the node to sort on
     * @param ascending determines if to order ascending or descending
     */
    private Order(String name, boolean ascending) {
        this.name = name;
        this.ascending = ascending;
    }

    /**
     * Returns the name of the node to sort on.
     *
     * @return the node name
     */
    public String getName() {
        return name;
    }

    /**
     * Determines if to order ascending or descending.
     *
     * @return {@code true} to order ascending, {@code false} to order descending
     */
    public boolean isAscending() {
        return ascending;
    }

    /**
     * Reverses the order.
     *
     * @return the reverse order
     */
    public Order reverse() {
        return new Order(name, !ascending);
    }

    /**
     * Creates an {@link Order} to order on a node in ascending order.
     *
     * @param name the node name
     * @return a new order
     */
    public static Order ascending(String name) {
        return new Order(name, true);
    }

    /**
     * Creates an {@link Order} to order on a node in descending order.
     *
     * @param name the node name
     * @return a new order
     */
    public static Order descending(String name) {
        return new Order(name, false);
    }

}