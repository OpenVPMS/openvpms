/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.service.archetype;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;

import java.util.Collection;
import java.util.List;

/**
 * The archetype service provides support to create, retrieve, validate, save and remove {@link IMObject}s.
 *
 * @author Tim Anderson
 */
public interface ArchetypeService extends IMObjectBeanFactory {

    /**
     * Return all archetypes that match that specified.
     *
     * @param archetype   the archetype. May contain a wildcard character
     * @param primaryOnly return only the primary archetypes
     * @return a list of archetypes
     */
    List<String> getArchetypes(String archetype, boolean primaryOnly);

    /**
     * Returns the {@link ArchetypeDescriptor} for the given archetype.
     *
     * @param archetype the archetype
     * @return the descriptor corresponding to the archetype, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    ArchetypeDescriptor getArchetypeDescriptor(String archetype);

    /**
     * Return all the {@link ArchetypeDescriptor} instances that match the specified archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @return a list of matching archetype descriptors
     */
    List<ArchetypeDescriptor> getArchetypeDescriptors(String archetype);

    /**
     * Returns all the {@link ArchetypeDescriptor} managed by this service.
     *
     * @return the archetype descriptors
     */
    List<ArchetypeDescriptor> getArchetypeDescriptors();

    /**
     * Return the {@link AssertionTypeDescriptor} with the specified name.
     *
     * @param name the name of the assertion type
     * @return the assertion type descriptor corresponding to the name or {@code null} if none is found
     */
    AssertionTypeDescriptor getAssertionTypeDescriptor(String name);

    /**
     * Return all the {@link AssertionTypeDescriptor} instances supported by this service.
     *
     * @return the assertion type descriptors
     */
    List<AssertionTypeDescriptor> getAssertionTypeDescriptors();

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @return a new object
     * @throws OpenVPMSException for any error
     */
    IMObject create(String archetype);

    /**
     * Creates an object given its archetype.
     *
     * @param archetype the archetype name
     * @param type      the expected type of the object
     * @return a new object
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    <T extends IMObject> T create(String archetype, Class<T> type);

    /**
     * Derived values for the specified {@link IMObject}, based on its corresponding {@link ArchetypeDescriptor}.
     *
     * @param object the object to derived values for
     * @throws OpenVPMSException for any error
     */
    void deriveValues(IMObject object);

    /**
     * Derive the value for the {@link NodeDescriptor} with the specified name.
     *
     * @param object the object to operate on.
     * @param node   the name of the {@link NodeDescriptor}, which will be used to derive the value
     * @throws OpenVPMSException for any error
     */
    void deriveValue(IMObject object, String node);

    /**
     * Saves an object, executing any <em>save</em> rules associated with its archetype.
     *
     * @param object the object to save
     * @throws OpenVPMSException for any error
     */
    void save(IMObject object);

    /**
     * Save a collection of {@link IMObject} instances.
     *
     * @param objects the objects to insert or update
     * @throws OpenVPMSException for any error
     */
    void save(Collection<? extends IMObject> objects);

    /**
     * Remove the specified object.
     *
     * @param object the object to remove
     * @throws OpenVPMSException for any error
     */
    void remove(IMObject object);

    /**
     * Removes an object given its reference.
     *
     * @param reference the object reference
     * @throws OpenVPMSException for any error
     */
    void remove(Reference reference);

    /**
     * Validates an object.
     *
     * @param object the object to validate
     * @return a list of validation errors, if any
     * @throws OpenVPMSException for any error
     */
    List<ValidationError> validate(IMObject object);

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    IMObject get(Reference reference);

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the corresponding object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    IMObject get(Reference reference, boolean active);

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    <T extends IMObject> T get(Reference reference, Class<T> type);

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    IMObject get(String archetype, long id);

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    IMObject get(String archetype, long id, boolean active);

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    <T extends IMObject> T get(String archetype, long id, Class<T> type);

    /**
     * Returns a builder to create queries.
     *
     * @return the criteria builder
     */
    CriteriaBuilder getCriteriaBuilder();

    /**
     * Creates a {@link TypedQuery} for executing a criteria query.
     *
     * @param query the criteria query
     * @return the new query instance
     */
    <T> TypedQuery<T> createQuery(CriteriaQuery<T> query);

}
