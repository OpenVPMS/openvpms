/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link Order} class.
 *
 * @author Tim Anderson
 */
public class OrderTestCase {

    /**
     * Tests the {@link Order#ascending(String)} method.
     */
    @Test
    public void testCreateAscending() {
        Order order = Order.ascending("foo");
        checkOrder(order, "foo", true);
    }

    /**
     * Tests the {@link Order#descending(String)} method.
     */
    @Test
    public void testCreateDescending() {
        Order order = Order.descending("bar");
        checkOrder(order, "bar", false);
    }

    /**
     * Tests the {@link Order#reverse()} method.
     */
    @Test
    public void testReverse() {
        Order asc = Order.ascending("foo");
        checkOrder(asc.reverse(), "foo", false);

        Order desc = Order.descending("bar");
        checkOrder(desc.reverse(), "bar", true);
    }

    /**
     * Verifies an order matches that expected.
     *
     * @param order     the order to check
     * @param name      the expected name
     * @param ascending the expected ascending flag
     */
    private void checkOrder(Order order, String name, boolean ascending) {
        assertEquals(name, order.getName());
        assertEquals(ascending, order.isAscending());
    }
}
