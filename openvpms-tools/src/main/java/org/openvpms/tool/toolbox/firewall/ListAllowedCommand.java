/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.firewall;

import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import picocli.CommandLine;

import java.util.List;

/**
 * Lists the allowed addresses from the firewall configuration.
 *
 * @author Tim Anderson
 */
@CommandLine.Command(name = "--list", description = "List allowed addresses that users can connect from")
public class ListAllowedCommand extends AbstractFirewallCommand {

    /**
     * Displays the allowed addresses.
     *
     * @param settings the firewall settings
     */
    public static void showAllowedAddresses(FirewallSettings settings) {
        List<FirewallEntry> allowed = settings.getAllowedAddresses();
        if (allowed.isEmpty()) {
            System.err.println("No allowed addresses configured");
        } else {
            int width = allowed.stream().mapToInt(value -> value.getAddress().length()).max().orElse(20);
            String format = "%-" + width + "s %-6s %s\n";
            System.out.printf(format, "Address", "Active", "Description");
            for (FirewallEntry entry : allowed) {
                System.out.printf(format, entry.getAddress(), entry.isActive() ? 'Y' : 'N',
                                  entry.getDescription() != null ? entry.getDescription() : "");
            }
        }
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        FirewallSettings settings = getSettings();
        showAllowedAddresses(settings);
        return 0;
    }
}