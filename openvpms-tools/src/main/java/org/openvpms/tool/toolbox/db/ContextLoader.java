/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Properties;

/**
 * Lazily loads the Spring application context.
 *
 * @author Tim Anderson
 */
public class ContextLoader {

    /**
     * The database username.
     */
    private final String username;

    /**
     * The database password.
     */
    private final String password;

    /**
     * The database driver class name.
     */
    private final String driver;

    /**
     * The database URL.
     */
    private final String url;

    /**
     * The context.
     */
    private ClassPathXmlApplicationContext context;

    /**
     * Constructs a {@link ContextLoader}.
     *
     * @param properties the configuration properties
     */
    public ContextLoader(ConfigProperties properties) {
        this(properties.getDriver(), properties.getUrl(), properties.getUsername(), properties.getPassword());
    }

    /**
     * Constructs a {@link ContextLoader}.
     *
     * @param driver   the database driver class name
     * @param url      the database URL
     * @param username the database username
     * @param password the database password
     */
    public ContextLoader(String driver, String url, String username, String password) {
        this.username = username;
        this.password = password;
        this.driver = driver;
        this.url = url;
    }

    /**
     * Returns the Spring application context.
     *
     * @return the context
     */
    public ApplicationContext getContext() {
        if (context == null) {
            loadContext();
        }
        return context;
    }

    /**
     * Loads the context.
     */
    private void loadContext() {
        context = new ClassPathXmlApplicationContext();
        PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
        Properties properties = new Properties();
        properties.setProperty("db.username", username);
        properties.setProperty("db.password", password);
        properties.setProperty("db.driver", driver);
        properties.setProperty("db.url", url);
        configurer.setProperties(properties);
        context.addBeanFactoryPostProcessor(configurer);
        context.setConfigLocation("toolbox-context.xml");
        context.refresh();
    }
}