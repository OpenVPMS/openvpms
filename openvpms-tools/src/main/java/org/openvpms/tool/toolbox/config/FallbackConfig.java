/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import java.util.Properties;

/**
 * A {@link Config} where values are sourced from a set of fallback properties if they aren't present in
 * the primary properties.
 *
 * @author Tim Anderson
 */
class FallbackConfig implements Config {

    /**
     * The properties.
     */
    private final Properties properties;

    /**
     * The fallback properties.
     */
    private final Properties fallback;

    /**
     * Constructs a {@link FallbackConfig}.
     *
     * @param properties the properties
     * @param fallback   the fallback properties
     */
    public FallbackConfig(Properties properties, Properties fallback) {
        this.properties = properties;
        this.fallback = fallback;
    }

    /**
     * Returns the JDBC driver class name.
     *
     * @return the JDBC driver class name. May be {@code null}
     */
    @Override
    public String getDriver() {
        return getProperty(DB_DRIVER);
    }

    /**
     * Returns the database URL.
     *
     * @return the database URL. May be {@code null}
     */
    @Override
    public String getUrl() {
        return getProperty(DB_URL);
    }

    /**
     * Returns the database username.
     *
     * @return the database username. May be {@code null}
     */
    @Override
    public String getUsername() {
        return getProperty(DB_USERNAME);
    }

    /**
     * Returns the database password.
     *
     * @return the database password. May be {@code null}
     */
    @Override
    public String getPassword() {
        return getProperty(DB_PASSWORD);
    }

    /**
     * Returns the reporting database URL.
     * <p/>
     * This returns the db.url if the reporting.db.url isn't present.
     *
     * @return the reporting database URL. May be {@code null}
     */
    @Override
    public String getReportingUrl() {
        String value = properties.getProperty(REPORTING_DB_URL);
        return value != null ? value : properties.getProperty(DB_URL);
    }

    /**
     * Returns the reporting username.
     *
     * @return the reporting username. May be {@code null}
     */
    @Override
    public String getReportingUsername() {
        return getProperty(REPORTING_DB_USERNAME);
    }

    /**
     * Returns the reporting password.
     *
     * @return the reporting password. May be {@code null}
     */
    @Override
    public String getReportingPassword() {
        return getProperty(REPORTING_DB_PASSWORD);
    }

    /**
     * Returns the encryption key.
     *
     * @return the encryption key. May be {@code null}
     */
    @Override
    public String getKey() {
        return getProperty(OPENVPMS_KEY);
    }

    /**
     * Returns a property value.
     *
     * @param key the property key
     * @return the property value
     */
    private String getProperty(String key) {
        String value = properties.getProperty(key);
        return (value != null) ? value : fallback.getProperty(key);
    }
}