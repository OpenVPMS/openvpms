/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Default configuration.
 * <p/>
 * This generates a random database password.
 *
 * @author Tim Anderson
 */
class DefaultConfig extends AbstractConfigProperties {

    /**
     * Constructs a {@link DefaultConfig}.
     *
     * @throws IOException if the default-openvpms.properties resource cannot be read
     */
    public DefaultConfig() throws IOException {
        super(new Properties());
        Properties properties = getProperties();
        properties.load(getClass().getResourceAsStream("default-openvpms.properties"));
        PasswordGenerator generator = new PasswordGenerator();
        if (getPassword() == null) {
            properties.setProperty(DB_PASSWORD, generator.generate());
        }
        if (getReportingPassword() == null) {
            properties.setProperty(REPORTING_DB_PASSWORD, generator.generate());
        }
    }
}
