/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.archetype;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


/**
 * Command to load archetypes.
 * <p/>
 * By default, this loads all archetypes.
 * <p/>
 * Individual archetypes can be loaded using the --file option.
 *
 * @author Tim Anderson
 */
@Command(name = "--load", description = "Load archetypes")
public class ArchetypeLoadCommand extends AbstractApplicationContextCommand {

    /**
     * The archetype directory.
     */
    @Option(names = {"-d", "--dir"}, description = "The archetype directory.", defaultValue = DEFAULT_PATH)
    String dir = DEFAULT_PATH;

    /**
     * The archetype file.
     */
    @Option(names = {"-f", "--file"}, description = "Used to load a single file.")
    String file;

    /**
     * Verbose flag.
     */
    @Option(names = {"-v", "--verbose"}, description = "Displays verbose info to the console.")
    boolean verbose;

    /**
     * The default archetype path.
     */
    public static final String DEFAULT_PATH = "${openvpms.home}/archetypes";

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        ArchetypeLoadHelper helper = new ArchetypeLoadHelper(getBean(IArchetypeRuleService.class));
        helper.setVerbose(verbose);
        if (file != null) {
            helper.load(file);
        } else {
            helper.loadAll(dir);
        }
        System.out.println("Archetypes successfully loaded");
        return 0;
    }
}
