/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.firewall;

import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import picocli.CommandLine;

import java.util.List;

/**
 * Adds an allowed address.
 *
 * @author Tim Anderson
 */
@CommandLine.Command(name = "--add", description = "Add allowed address")
public class AddAllowedCommmand extends AbstractFirewallCommand {

    /**
     * The address.
     */
    @CommandLine.Parameters(index = "0", arity = "1")
    String address;

    /**
     * The plugin file.
     */
    @CommandLine.Option(names = "-d", description = "address description")
    private String description;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        FirewallSettings settings = getSettings();
        List<FirewallEntry> allowed = settings.getAllowedAddresses();
        new IpAddressMatcher(address);  // ensure that the address is valid
        allowed.removeIf(entry -> address.equals(entry.getAddress()));
        allowed.add(new FirewallEntry(address, true, description));
        settings.setAllowedAddresses(allowed);
        save(settings);
        return 0;
    }
}