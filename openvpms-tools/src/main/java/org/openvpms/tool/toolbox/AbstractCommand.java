/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox;

import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.PathHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Option;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

/**
 * Base class for commands.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCommand implements Callable<Integer> {

    @Option(names = {"-h", "--help"}, usageHelp = true, description = "Display this help message")
    boolean usageHelpRequested;

    /**
     * The properties path.
     */
    @Option(names = "--properties", description = "Specifies the path to openvpms.properties", hidden = true)
    private Path path;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractCommand.class);

    /**
     * Sets the path to openvpms.properties.
     *
     * @param path the path
     */
    public void setPath(Path path) {
        this.path = path;
    }

    /**
     * Executes the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    public Integer call() {
        int result;
        try {
            init();
            result = run();
        } catch (Throwable exception) {
            result = 1;
            System.err.println(exception.getMessage());
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Initialises the command.
     * <p/>
     * This implementation is a no-op.
     *
     * @throws Exception for any error
     */
    protected void init() throws Exception {

    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    protected abstract int run() throws Exception;

    /**
     * Returns the path to openvpms.properties.
     *
     * @return the path to openvpms.properties
     * @throws IllegalStateException if path is not set, and openvpms.home is not defined or is invalid
     */
    protected Path getPropertiesPath() {
        return path != null ? path : PathHelper.getHome().resolve("conf/openvpms.properties");
    }

    /**
     * Loads the openvpms.properties file.
     *
     * @return the properties
     * @throws IOException for any I/O error
     */
    protected ConfigProperties loadProperties() throws IOException {
        return new ConfigProperties(getPropertiesPath());
    }

    /**
     * Checks the properties.
     * <p/>
     * This implementation throws an exception if they are incomplete or invalid.
     *
     * @param properties the properties
     * @throws Exception for any error
     */
    protected void checkProperties(ConfigProperties properties) throws Exception {
        if (!properties.isComplete()) {
            Path relative = PathHelper.getPathRelativeToCWD(properties.getPath());
            if (!properties.exists()) {
                throw new Exception("The " + relative + " configuration does not exist.\nRun toolbox configure");
            } else {
                throw new Exception("The " + relative + " configuration is incomplete.\nRun toolbox configure");
            }
        }
        properties.validate();
    }

    /**
     * Verifies that the database driver exists.
     *
     * @param properties the configuration properties
     * @throws IllegalStateException if the driver cannot be found
     */
    protected void checkDriver(ConfigProperties properties) {
        String driver = properties.getDriver();
        try {
            Class.forName(driver);
        } catch (Throwable exception) {
            Path lib = PathHelper.getPathRelativeToCWD(PathHelper.getHome().resolve("lib"));
            throw new IllegalStateException("The database driver " + driver + " cannot be found.\n" +
                                            "Check that the jar file exists and is readable in the " + lib
                                            + " directory");
        }
    }


}
