/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

/**
 * Patches log4j2.xml to change the openvpms.log and openvpms-full.log files to ones named after the war name.
 *
 * @author Tim Anderson
 */
class Log4JPatch extends LineReplacingPatch {

    /**
     * The war name.
     */
    private final String name;

    /**
     * The log file name.
     */
    private static final String LOG_NAME = "openvpms.log";

    /**
     * The full log file name.
     */
    private static final String FULL_LOG_NAME = "openvpms-full.log";

    /**
     * Constructs a {@link Log4JPatch}.
     *
     * @param name the war name
     */
    public Log4JPatch(String name) {
        this.name = name;
    }

    /**
     * Performs replacement on a line of text.
     *
     * @param line the line
     * @return the (possibly modified) line
     */
    @Override
    protected String replace(String line) {
        if (line.contains(LOG_NAME)) {
            line = line.replace(LOG_NAME, name + ".log");
        } else if (line.contains(FULL_LOG_NAME)) {
            line = line.replace(FULL_LOG_NAME, name + "-full.log");
        }
        return line;
    }
}
