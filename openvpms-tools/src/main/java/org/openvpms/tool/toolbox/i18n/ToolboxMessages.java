/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.i18n;

import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

/**
 * Toolbox messages.
 *
 * @author Tim Anderson
 */
public class ToolboxMessages {

    /**
     * The message source.
     */
    private static final ResourceBundleMessageSource source;

    static {
        source = new ResourceBundleMessageSource();
        source.setBasenames(ToolboxMessages.class.getName());
    }

    /**
     * Returns a message given its key.
     *
     * @param key       the key
     * @param arguments arguments to be inserted into the message
     * @return the message
     */
    public static String get(String key, Object... arguments) {
        try {
            return source.getMessage(key, arguments, Locale.getDefault());
        } catch (NoSuchMessageException exception) {
            return "!" + key + "!";
        }
    }
}