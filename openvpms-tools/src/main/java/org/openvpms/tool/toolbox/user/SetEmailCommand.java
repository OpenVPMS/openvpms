/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * Sets the email address of a user.
 *
 * @author Tim Anderson
 */
@Command(name = "--set-email", description = "Sets the email address of a user")
public class SetEmailCommand extends AbstractUserCommand {

    /**
     * The user name.
     */
    @CommandLine.Parameters(index = "0", arity = "1")
    private String username;

    /**
     * The email address.
     */
    @CommandLine.Parameters(index = "1", arity = "1")
    private String email;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        int result = -1;
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        Contacts contacts = new Contacts(service);
        User user = getUser(username);
        if (user != null) {
            String current = contacts.getEmail(user);
            if (!email.equals(current)) {
                Contact contact = contacts.getEmailContact(user);
                if (contact == null) {
                    contact = service.create(ContactArchetypes.EMAIL, Contact.class);
                    contact.setName(user.getName());
                    user.addContact(contact);
                }
                IMObjectBean bean = service.getBean(contact);
                bean.setValue("preferred", true);
                bean.setValue("emailAddress", email);
                bean.save();
            }
            result = 1;
        } else {
            System.err.println("User not found");
        }
        return result;
    }
}