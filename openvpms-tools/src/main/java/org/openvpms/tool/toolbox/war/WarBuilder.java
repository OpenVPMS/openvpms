/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * Patches the openvpms.war with properties from openvpms.properties.
 *
 * @author Tim Anderson
 */
public class WarBuilder {

    /**
     * The default master zip path.
     */
    public static final String MASTER_ZIP_PATH = "${openvpms.home}/webapps/openvpms-master.zip";

    /**
     * The default output directory.
     */
    public static final String OUTPUT_DIR_PATH = "${openvpms.home}/webapps";

    /**
     * Generates a .war using the default archive path, to the default output directory.
     *
     * @param name       the war name, minus any extension
     * @param properties the path to openvpms.properties
     * @return the path of the output war
     * @throws IOException for any I/O error
     */
    public Path build(String name, Path properties) throws IOException {
        return build(name, OUTPUT_DIR_PATH, MASTER_ZIP_PATH, properties);
    }

    /**
     * Generates a .war from the specified archive.
     *
     * @param name       the war file name, minus any extension.
     * @param dir        the directory to place the generated war in. May contain ${openvpms.home}
     * @param archive    the path to the source archive. May contain ${openvpms.home}
     * @param properties the path to openvpms.properties
     * @return the path of the output war
     * @throws IOException for any I/O error
     */
    public Path build(String name, String dir, String archive, Path properties) throws IOException {
        dir = PathHelper.replaceHome(dir);
        archive = PathHelper.replaceHome(archive);

        Path tempJarFile = Files.createTempFile(Paths.get(dir), "openvpms", ".war");
        Path result = Paths.get(dir, name + ".war");
        Map<String, Patch> patches = getPatches(name, properties);

        try (JarFile jar = new JarFile(archive);
             OutputStream out = Files.newOutputStream(tempJarFile)) {
            JarStream tempJar = new JarStream(out);

            for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements(); ) {
                JarEntry entry = entries.nextElement();
                try (InputStream inputStream = jar.getInputStream(entry)) {
                    Patch patch = patches.remove(entry.getName());
                    if (patch == null) {
                        // copy the file as is
                        tempJar.putNextEntry(entry);
                        IOUtils.copy(inputStream, tempJar);
                    } else {
                        tempJar.putNextEntry(new JarEntry(entry.getName()));
                        // the new file will have different size, CRC
                        patch.apply(inputStream, tempJar);
                    }
                    tempJar.closeEntry();
                }
            }
            tempJar.realClose();

            Files.move(tempJarFile, result, StandardCopyOption.REPLACE_EXISTING);
        }
        if (!patches.isEmpty()) {
            throw new IllegalStateException("Not all patches were applied: "
                                            + StringUtils.join(patches.keySet(), ", "));
        }
        return result;
    }

    /**
     * Returns the patches to apply to the war.
     *
     * @param name       the war name, minus suffix
     * @param properties the path to openvpms.properties
     * @return the patches, keyed on the paths that they apply to
     */
    protected Map<String, Patch> getPatches(String name, Path properties) {
        Map<String, Patch> result = new HashMap<>();
        result.put("WEB-INF/classes/openvpms.properties", new CopyPropertiesPatch(properties));
        if (!"openvpms".equals(name)) {
            result.put("WEB-INF/web.xml", new WebXMLPatch(name));
            result.put("WEB-INF/classes/log4j2.xml", new Log4JPatch(name));
        }
        return result;
    }


    private static class JarStream extends JarOutputStream {

        /**
         * Constructs a {@link JarStream}.
         *
         * @param out the actual output stream
         * @throws IOException if an I/O error has occurred
         */
        public JarStream(OutputStream out) throws IOException {
            super(out);
        }

        /**
         * Closes the ZIP output stream as well as the stream being filtered.
         */
        @Override
        public void close() {

        }

        public void realClose() throws IOException {
            super.close();
        }
    }
}