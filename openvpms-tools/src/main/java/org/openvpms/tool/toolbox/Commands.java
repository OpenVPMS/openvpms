/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox;

import picocli.CommandLine;
import picocli.CommandLine.Spec;

/**
 * Base class for groups of commands.
 * <p/>
 * This is used to display the usage if the group is invoked without a subcommand.
 *
 * @author Tim Anderson
 */
public abstract class Commands implements Runnable {

    /**
     * The command spec.
     */
    @Spec
    CommandLine.Model.CommandSpec spec;

    /**
     * Display usage for the commands.
     */
    @Override
    public void run() {
        spec.commandLine().usage(System.err);
    }

}
