/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Command to force a user or users to change their passwords on next login.
 *
 * @author Tim Anderson
 */
@Command(name = "--force-change-password", description = "Force users to change passwords on next login")
public class ChangePasswordCommand extends AbstractUserCommand {

    /**
     * The reset type.
     */
    @ArgGroup(multiplicity = "1")
    Type type;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        int result;
        if (type.user != null) {
            User user = getUser(type.user);
            if (user != null) {
                if (!user.getChangePassword()) {
                    user.setChangePassword(true);
                    save(user);
                }
                result = 0;
                System.out.println("User updated");
            } else {
                result = 1;
                System.err.println("User not found");
            }
        } else if (type.all) {
            ArchetypeService service = getBean(IArchetypeRuleService.class);
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<User> query = builder.createQuery(User.class);
            Root<User> from = query.from(User.class, UserArchetypes.USER);
            query.orderBy(builder.asc(from.get("id")));
            TypedQueryIterator<User> iterator = new TypedQueryIterator<>(service.createQuery(query), 100);
            while (iterator.hasNext()) {
                User user = iterator.next();
                if (!user.getChangePassword()) {
                    user.setChangePassword(true);
                    save(user);
                }
            }
            System.out.println("Users updated");
            result = 0;
        } else {
            result = 1;
            System.err.println("One of --all or --user must be specified");
        }
        return result;
    }

    /**
     * The reset type. Either a single user or all users.
     */
    static class Type {
        /**
         * The user name.
         */
        @Option(names = {"-u", "--user"}, description = {"Force password change for the specified user"})
        private String user;

        @Option(names = {"-a", "--all"}, description = {"Force password change for all users"})
        private boolean all;
    }
}