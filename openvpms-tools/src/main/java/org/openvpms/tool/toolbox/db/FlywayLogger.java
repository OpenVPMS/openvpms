/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.apache.commons.lang3.time.StopWatch;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.callback.BaseFlywayCallback;
import org.flywaydb.core.internal.util.TimeFormat;
import org.slf4j.Logger;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Logs Flyway events.
 *
 * @author Tim Anderson
 */
public class FlywayLogger extends BaseFlywayCallback {

    /**
     * The logger to use.
     */
    private final Logger log;

    /**
     * Determines if logging to the console is suppressed.
     */
    private final boolean headless;

    /**
     * Times for each migration.
     */
    private final Map<MigrationInfo, StopWatch> state = new HashMap<>();

    /**
     * Constructs a {@link FlywayLogger}.
     *
     * @param log      the logger to use
     * @param headless determines if {@code true}, suppresses logging to the console,
     */
    public FlywayLogger(Logger log, boolean headless) {
        this.log = log;
        this.headless = headless;
    }

    /**
     * Runs before each migration script is executed.
     *
     * @param connection a connection to the database
     * @param info       the current MigrationInfo for this migration
     */
    @Override
    public void beforeEachMigrate(Connection connection, MigrationInfo info) {
        StopWatch watch = new StopWatch();
        state.put(info, watch);
        if (info.getVersion() != null) {
            if (headless) {
                log.info("Updating to {} - {}", info.getVersion(), info.getDescription());
            } else {
                System.out.print("Updating to " + info.getVersion() + " - " + info.getDescription() + " ... ");
            }
        } else {
            if (headless) {
                log.info("Running {} ", info.getDescription());
            } else {
                System.out.print("Running " + info.getDescription() + " ... ");
            }
        }
        watch.start();
    }

    /**
     * Runs after each migration script is executed.
     *
     * @param connection a connection to the database
     * @param info       the current MigrationInfo for this migration
     */
    @Override
    public void afterEachMigrate(Connection connection, MigrationInfo info) {
        StopWatch watch = state.get(info);
        if (watch != null) {
            watch.stop();
            String time = TimeFormat.format(watch.getTime());
            if (headless) {
                log.info("Completed {} in {}", info.getDescription(), time);
            } else {
                System.out.println(time);
            }
        }
    }
}
