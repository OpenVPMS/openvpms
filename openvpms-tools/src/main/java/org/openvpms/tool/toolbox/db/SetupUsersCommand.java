/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.i18n.ToolboxMessages;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.sql.SQLException;

/**
 * Command to set up the OpenVPMS database users.
 *
 * @author Tim Anderson
 */
@Command(name = "--setup-users", header = "Sets up OpenVPMS database users")
public class SetupUsersCommand extends AbstractAdminDBCommand {

    /**
     * The host the users are connecting from.
     */
    @CommandLine.Option(names = "--host", description = "Host users are connecting from", required = true,
            defaultValue = "localhost", showDefaultValue = CommandLine.Help.Visibility.ALWAYS)
    private String host;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws SQLException for any error
     */
    @Override
    protected int run() throws SQLException {
        DatabaseAdminService service = getAdminService();
        ConfigProperties properties = getProperties();
        service.createUsers(new Credentials(properties.getUsername(), properties.getPassword()),
                            new Credentials(properties.getReportingUsername(), properties.getReportingPassword()),
                            host);
        System.out.println(ToolboxMessages.get("db.setupusers", properties.getUsername(),
                                               properties.getReportingUsername()));
        return 0;
    }
}
