/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import com.atlassian.plugin.JarPluginArtifact;
import org.apache.commons.io.FileUtils;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.db.service.PluginMigrator;
import org.openvpms.tool.toolbox.plugin.PluginArtifactHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Abstract implementation of {@link PluginMigrator}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPluginMigrator implements PluginMigrator {

    /**
     * The plugin DAO.
     */
    private final PluginDAO dao;

    /**
     * The plugins directory.
     */
    private final String dir;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractPluginMigrator.class);

    /**
     * Constructs an {@link AbstractPluginMigrator}.
     *
     * @param dao the plugin DAO
     * @param dir the plugins directory
     */
    public AbstractPluginMigrator(PluginDAO dao, String dir) {
        this.dao = dao;
        this.dir = dir;
    }

    /**
     * Updates existing plugins.
     *
     * @throws IllegalStateException if a plugin cannot be saved
     */
    protected void update() {
        Iterator<Plugin> plugins = getInstalledPlugins();
        if (plugins.hasNext()) {
            Map<String, JarPluginArtifact> availablePlugins = getAvailablePlugins();
            if (log.isDebugEnabled()) {
                String available = availablePlugins.entrySet().stream()
                        .map(entry -> "[key=" + entry.getKey() + ", name=" + entry.getValue().getName() + "]")
                        .collect(Collectors.joining(", "));
                log.debug("Available plugins: {}", available);
            }
            while (plugins.hasNext()) {
                Plugin plugin = plugins.next();
                String key = plugin.getKey();
                JarPluginArtifact artifact = availablePlugins.get(key);
                if (artifact != null) {
                    log.debug("Updating plugin [key={}, name={}] with {}", key, plugin.getName(), artifact.getName());
                    save(key, artifact);
                } else {
                    log.debug("Not updating plugin [key={}, name={}]. No plugin artifact available.", key,
                              plugin.getName());
                }
            }
        } else {
            log.debug("Not updating plugins, no plugins installed");
        }
    }

    /**
     * Saves a plugin.
     *
     * @param key      the plugin key
     * @param artifact the plugin artifact
     * @throws IllegalStateException if the plugin cannot be saved
     */
    protected void save(String key, JarPluginArtifact artifact) {
        try (InputStream stream = artifact.getInputStream()) {
            dao.save(key, artifact.getName(), stream);
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to save plugin " + artifact.getName() + ": "
                                            + exception.getMessage(), exception);
        }
    }

    /**
     * Returns the available plugins.
     *
     * @return the available plugins, keyed on plugin key
     */
    protected Map<String, JarPluginArtifact> getAvailablePlugins() {
        Map<String, JarPluginArtifact> result = new HashMap<>();
        for (File file : FileUtils.listFiles(new File(dir), new String[]{"jar"}, false)) {
            JarPluginArtifact artifact = new JarPluginArtifact(file);
            if (artifact.containsJavaExecutableCode()) {
                String key = PluginArtifactHelper.getPluginKey(artifact);
                if (key != null) {
                    result.put(key, artifact);
                }
            }
        }
        return result;
    }

    /**
     * Returns the installed plugins.
     *
     * @return the installed plugins
     */
    protected Iterator<Plugin> getInstalledPlugins() {
        return dao.getPlugins();
    }
}