/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.template;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.tools.TemplateLoader;
import org.openvpms.report.tools.Templates;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import org.openvpms.tool.toolbox.util.PathHelper;
import org.springframework.transaction.PlatformTransactionManager;
import picocli.CommandLine;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Loads report templates.
 *
 * @author Tim Anderson
 */
@Command(name = "--load")
public class LoadTemplateCommand extends AbstractApplicationContextCommand {

    /**
     * Indicates the type to load, i.e by file or by size.
     */
    @ArgGroup(multiplicity = "1")
    Type type;

    /**
     * The load options.
     */
    @ArgGroup(multiplicity = "1")
    Options options;

    /**
     * The available template descriptors in the requested size.
     */
    private final List<Templates> descriptors = new ArrayList<>();

    /**
     * Constructs a {@link LoadTemplateCommand}.
     */
    public LoadTemplateCommand() {
        super();
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        TemplateLoader loader = new TemplateLoader(getBean(IArchetypeRuleService.class),
                                                   getBean(DocumentHandlers.class),
                                                   getBean(PlatformTransactionManager.class),
                                                   getBean(LookupService.class));
        if (type.size != null) {
            loadBySize(type.size, loader);
        } else {
            loadFromFile(type.file, loader);
        }

        return 0;
    }

    /**
     * Loads templates by size.
     *
     * @param size   the document size
     * @param loader the loader
     * @throws JAXBException for any JAXB error
     */
    private void loadBySize(Size size, TemplateLoader loader) throws JAXBException {
        Map<String, Entity> emailTemplates = new HashMap<>();
        Set<String> templates;
        if (options.all) {
            templates = new LinkedHashSet<>(Arrays.asList("documents", "reports"));
        } else {
            templates = getTemplates();
        }
        for (String template : templates) {
            if (isReportsOrDocuments(template)) {
                if (!exists(template, size)) {
                    throw new IllegalArgumentException(template + " are not available in size " + size);
                }
            }
        }
        for (String template : templates) {
            if (isReportsOrDocuments(template)) {
                loader.load(getPath(template, size).toString(), emailTemplates);
            } else {
                loadBySize(template, loader, size, emailTemplates);
            }
        }
    }

    /**
     * Loads templates from a file.
     *
     * @param file   the file
     * @param loader the loader
     * @throws JAXBException for any JAXB error
     */
    private void loadFromFile(String file, TemplateLoader loader) throws JAXBException {
        Path path = Paths.get(file);
        if (!Files.isRegularFile(path)) {
            throw new IllegalStateException("Cannot read " + path);
        }
        if (options.all) {
            loader.load(file);
        } else {
            Set<String> templates = getTemplates();
            Templates descriptors = loader.getTemplates(path.toFile());
            File dir = path.toFile().getParentFile();
            HashMap<String, Entity> emailTemplates = new HashMap<>();
            for (String template : templates) {
                loader.load(template, descriptors, dir, emailTemplates);
            }
        }
    }

    private Set<String> getTemplates() {
        return new LinkedHashSet<>(Arrays.asList(options.templates));
    }

    /**
     * Loads the named template.
     *
     * @param template       the template name
     * @param loader         the loader
     * @param size           the document size
     * @param emailTemplates the email templates
     * @throws JAXBException for any JAXB error
     */
    private void loadBySize(String template, TemplateLoader loader, Size size, Map<String, Entity> emailTemplates)
            throws JAXBException {
        if (descriptors.isEmpty()) {
            Path documents = getPath("documents", size);
            if (Files.exists(documents)) {
                descriptors.add(loader.getTemplates(documents.toFile()));
            }
            Path reports = getPath("reports", size);
            if (Files.exists(reports)) {
                descriptors.add(loader.getTemplates(reports.toFile()));
            }
        }
        if (descriptors.isEmpty()) {
            throw new IllegalStateException("Cannot find templates in " + size);
        }
        File dir = getReportsDir().toFile();
        boolean found = false;
        for (Templates templates : descriptors) {
            if (loader.load(template, templates, dir, emailTemplates)) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new IllegalArgumentException("Template not found: " + template);
        }
    }

    private boolean isReportsOrDocuments(String template) {
        return "reports".equals(template) || "documents".equals(template);
    }

    private boolean exists(String descriptors, Size size) {
        Path path = getPath(descriptors, size);
        return Files.exists(path);
    }

    private Path getPath(String descriptors, Size size) {
        Path reports = getReportsDir();
        return reports.resolve(descriptors + "-" + size + ".xml");
    }

    private Path getReportsDir() {
        return PathHelper.getHome().resolve("reports");
    }

    static class Type {
        @CommandLine.Option(names = {"-f", "--file"}, description = "Load templates from a file")
        String file;

        @CommandLine.Option(names = {"-s", "--size"},
                description = "The template size. One of: ${COMPLETION-CANDIDATES}")
        Size size;
    }

    static class Options {
        @CommandLine.Option(names = {"-a", "--all"}, description = "Load all templates")
        boolean all;

        /**
         * The templates to load.
         */
        @CommandLine.Parameters(description = "The templates to load", arity = "1..*")
        String[] templates;
    }

    enum Size {
        A4,
        A5,
        Letter
    }

}
