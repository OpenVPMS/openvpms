/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.apache.commons.lang3.time.StopWatch;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.db.service.ArchetypeMigrator;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.db.service.DbVersionInfo;
import org.openvpms.db.service.PluginMigrator;
import org.openvpms.tool.toolbox.archetype.ArchetypeLoadHelper;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.i18n.ToolboxMessages;
import org.openvpms.tools.archetype.loader.ArchetypeLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Updates the database.
 * <p/>
 * This runs any necessary migrations, including loading archetypes and plugins.
 *
 * @author Tim Anderson
 */
public class DatabaseUpdater {

    /**
     * The configuration properties.
     */
    private final ConfigProperties properties;

    /**
     * The Spring application context loader.
     */
    private final ContextLoader loader;

    /**
     * The database administration service.
     */
    private final DatabaseAdminService service;

    /**
     * Plugin migrator that does nothing. This will ensure that the plugin checksum is updated.
     */
    private static final PluginMigrator NO_OP_MIGRATOR = () -> {
    };

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DatabaseUpdater.class);

    /**
     * Constructs a {@link DatabaseUpdater}.
     *
     * @param properties the configuration properties
     * @param loader     the Spring application context loader
     * @param service    the database administration service
     */
    public DatabaseUpdater(ConfigProperties properties, ContextLoader loader, DatabaseAdminService service) {
        this.properties = properties;
        this.service = service;
        this.loader = loader;
    }

    /**
     * Updates the database if required.
     * <p/>
     * This runs any necessary migrations, including loading archetypes.
     *
     * @param archetypesDir the archetypes directory
     * @param pluginsDir    the plugins directory, or {@code null} if plugins aren't being loaded
     * @throws SQLException for any SQL error
     */
    public void update(String archetypesDir, String pluginsDir) throws SQLException {
        DbVersionInfo info = service.getVersionInfo();
        if (info.hasFutureVersion()) {
            throw new SQLException(ToolboxMessages.get("db.update.futurechanges", info.getFutureChanges()));
        } else if (info.needsUpdate()) {
            doUpdate(archetypesDir, pluginsDir);
        } else {
            log.info("Database '{}' is up to date", service.getSchemaName());
        }
    }

    /**
     * Performs the update.
     *
     * @param archetypesDir the archetypes directory
     * @param pluginsDir    the plugins directory, or {@code null} if plugins aren't being loaded
     * @throws SQLException for any SQL error
     */
    protected void doUpdate(String archetypesDir, String pluginsDir) throws SQLException {
        try {
            String version = service.getVersion();
            String schemaName = service.getSchemaName();
            String key = properties.getKey();
            System.setProperty(ConfigProperties.OPENVPMS_KEY, key); // need the key for legacy migrations
            ArchetypeMigrator migrator = () -> {
                IArchetypeRuleService service = loader.getContext().getBean(IArchetypeRuleService.class);
                ArchetypeLoadHelper helper = new ArchetypeLoadHelper(service);
                helper.setVerbose(LoggerFactory.getLogger(ArchetypeLoader.class).isDebugEnabled());
                helper.loadAll(archetypesDir);
            };
            PluginMigrator pluginMigrator = (pluginsDir == null) ? NO_OP_MIGRATOR
                                                                 : () -> {
                // lazily create the actual migrator as the spring context is loaded after the database has been
                // migrated
                PluginDAO dao = loader.getContext().getBean(PluginDAO.class);
                PluginMigrator delegate = createPluginMigrator(dao, pluginsDir);
                delegate.migrate();
            };
            StopWatch stopWatch = StopWatch.createStarted();
            service.update(migrator, pluginMigrator);
            if (version == null) {
                // no version information
                log.info("Database '{}' updated to version {} in {}", schemaName, service.getVersion(),
                         stopWatch);
            } else {
                log.info("Database '{}' updated from version {} to {} in {}", schemaName, version,
                         service.getVersion(), stopWatch);
            }
        } finally {
            System.getProperties().remove(ConfigProperties.OPENVPMS_KEY);
        }
    }

    /**
     * Creates a plugin migrator to update plugins.
     *
     * @param dao        the plugin DAO
     * @param pluginsDir the plugins directory
     * @return a new plugin migrator
     */
    protected PluginMigrator createPluginMigrator(PluginDAO dao, String pluginsDir) {
        return new PluginMigratorImpl(dao, pluginsDir);
    }
}