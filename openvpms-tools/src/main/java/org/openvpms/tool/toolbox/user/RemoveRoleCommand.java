/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.domain.im.security.User;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

/**
 * Removes roles from a user.
 *
 * @author Tim Anderson
 */
@Command(name = "--remove-role", description = "Removes roles from a user")
public class RemoveRoleCommand extends AbstractRoleCommand {

    /**
     * The roles to remove.
     */
    @Parameters(description = "The roles to remove", index = "1..*", arity = "1..*")
    String[] roles;

    /**
     * Updates roles on a user.
     *
     * @param user the user to update
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int updateUser(User user) {
        int result = 0;
        for (String name : roles) {
            if (!removeRole(name, user)) {
                System.err.println("Role not found: " + name);
                result = 1;
                break;
            }
        }
        if (result != 1) {
            save(user);
            System.out.println("User updated");
        }
        return result;
    }

    /**
     * Removes a role from a user.
     *
     * @param name the role name
     * @param user the user
     * @return {@code true} if the role was removed
     */
    private boolean removeRole(String name, User user) {
        boolean result = false;
        for (SecurityRole role : user.getRoles().toArray(new SecurityRole[0])) {
            if (name.equals(role.getName())) {
                user.removeRole(role);
                result = true;
            }
        }
        return result;
    }
}
