/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.db.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Command;

/**
 * Command to display the database version.
 *
 * @author Tim Anderson
 */
@Command(name = "--version", header = "Displays the database version")
public class DBVersionCommand extends AbstractDBCommand {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DBVersionCommand.class);

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        DatabaseService service = getDatabaseService();
        String version = service.getVersion();
        if (version == null) {
            log.info("Database '{}' has no version information", service.getSchemaName());
        } else {
            log.info("Database '{}' is at version {}", service.getSchemaName(), version);
        }
        return 0;
    }

}
