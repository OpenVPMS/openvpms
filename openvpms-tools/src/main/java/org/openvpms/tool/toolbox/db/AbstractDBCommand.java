/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseService;
import org.openvpms.db.service.impl.DatabaseServiceImpl;
import org.openvpms.tool.toolbox.AbstractCommand;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.File;

/**
 * Base class for database commands.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDBCommand extends AbstractCommand {

    /**
     * The configuration properties.
     */
    private ConfigProperties properties;

    /**
     * The database service.
     */
    private DatabaseService service;

    /**
     * Initialises the command.
     *
     * @throws Exception for any error
     */
    @Override
    protected void init() throws Exception {
        properties = loadProperties();
        checkProperties(properties);
        checkDriver(properties);
    }

    /**
     * Returns the configuration properties.
     *
     * @return the properties
     */
    protected ConfigProperties getProperties() {
        return properties;
    }

    /**
     * Returns the database service.
     *
     * @return the database service
     */
    protected DatabaseService getDatabaseService() {
        if (service == null) {
            ConfigProperties properties = getProperties();
            Credentials user = new Credentials(properties.getUsername(), properties.getPassword());
            service = new DatabaseServiceImpl(properties.getDriver(), properties.getUrl(), user);
        }
        return service;
    }

    /**
     * Gets a directory.
     *
     * @param dir      the directory. May contain ${openvpms.home}
     * @param argument the commandline argument, for error reporting
     * @return the directory, with any ${openvpms.home} replaced with the actual path
     * @throws IllegalStateException if the directory is invalid
     */
    protected String getDir(String dir, String argument) {
        dir = PathHelper.replaceHome(dir);
        if (!new File(dir).isDirectory()) {
            throw new IllegalStateException("Argument " + argument + " is not a valid directory");
        }
        return dir;
    }
}