/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.firewall;

import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;

import java.util.List;
import java.util.ListIterator;

/**
 * Base class for firewall commands.
 *
 * @author Tim Anderson
 */
public abstract class AbstractFirewallCommand extends AbstractApplicationContextCommand {

    /**
     * Returns the firewall settings.
     *
     * @return the firewall settings
     */
    protected FirewallSettings getSettings() {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        SingletonService singletonService = getBean(SingletonService.class);
        Entity settings = singletonService.get(SettingsArchetypes.FIREWALL_SETTINGS, Entity.class, true);
        return new FirewallSettings(settings, service);
    }

    /**
     * Sets the active flag on an allowed address to enable/disable it.
     *
     * @param address the address
     * @param active  the new active flag
     * @return {@code true} if the address was found, otherwise {@code false}
     */
    protected boolean updateActive(String address, boolean active) {
        boolean updated = false;
        boolean found = false;
        FirewallSettings settings = getSettings();
        List<FirewallEntry> allowed = settings.getAllowedAddresses();
        ListIterator<FirewallEntry> iterator = allowed.listIterator();
        while (iterator.hasNext()) {
            FirewallEntry entry = iterator.next();
            if (entry.getAddress().equals(address)) {
                found = true;
                if (active != entry.isActive()) {
                    iterator.set(new FirewallEntry(address, active, entry.getDescription()));
                    updated = true;
                }
            }
        }
        if (updated) {
            settings.setAllowedAddresses(allowed);
            save(settings);
            System.out.println("Firewall updated");
        } else if (!found) {
            System.err.println("Address not found");
        }  else {
            System.err.println("No update required");
        }
        return found;
    }

    /**
     * Saves the firewall settings.
     *
     * @param settings the settings
     */
    protected void save(FirewallSettings settings) {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        service.save(settings.getSettings());
    }
}