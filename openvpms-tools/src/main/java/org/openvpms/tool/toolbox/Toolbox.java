/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox;

import org.openvpms.tool.toolbox.archetype.ArchetypeCommands;
import org.openvpms.tool.toolbox.config.ConfigCommand;
import org.openvpms.tool.toolbox.db.DatabaseCommands;
import org.openvpms.tool.toolbox.firewall.FirewallCommands;
import org.openvpms.tool.toolbox.plugin.PluginCommands;
import org.openvpms.tool.toolbox.ssl.SSLCommands;
import org.openvpms.tool.toolbox.template.TemplateCommands;
import org.openvpms.tool.toolbox.user.UserCommands;
import org.openvpms.tool.toolbox.war.WarCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * OpenVPMS command line toolbox.
 *
 * @author Tim Anderson
 */
@Command(name = "toolbox")
public class Toolbox extends Commands {

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int result = execute(args);
        System.exit(result);
    }

    /**
     * Executes the command.
     *
     * @param args the command line arguments
     * @return 0 on success, non-zero on failure
     */
    static int execute(String[] args) {
        CommandLine commandLine = new CommandLine(new Toolbox())
                .addSubcommand(new ConfigCommand())
                .addSubcommand(new DatabaseCommands())
                .addSubcommand(new ArchetypeCommands())
                .addSubcommand(new WarCommand())
                .addSubcommand(new TemplateCommands())
                .addSubcommand(new UserCommands())
                .addSubcommand(new PluginCommands())
                .addSubcommand(new FirewallCommands())
                .addSubcommand(new SSLCommands());
        return commandLine.execute(args);
    }
}