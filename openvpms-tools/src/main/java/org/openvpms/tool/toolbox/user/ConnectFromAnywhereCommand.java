/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import picocli.CommandLine;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.util.ArrayList;
import java.util.List;

/**
 * Command to set/unset the 'connectFromAnywhere' flag on users.
 *
 * @author Tim Anderson
 */
@Command(name = "--connect-from-anywhere", description = "Sets the 'Connect From Anywhere' flag for users")
public class ConnectFromAnywhereCommand extends AbstractUserCommand {

    /**
     * The connect-from-anywhere action.
     */
    @ArgGroup(multiplicity = "1")
    Action enableOrDisable;

    /**
     * The users to update.
     */
    @CommandLine.Parameters(description = "The usernames of the users to update", arity = "1..*")
    private String[] usernames;

    /**
     * The connect-from-anywhere node.
     */
    private static final String CONNECT_FROM_ANYWHERE = "connectFromAnywhere";

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        int result;
        List<User> users = getUsers();
        if (users != null) {
            for (User user : users) {
                IMObjectBean bean = service.getBean(user);
                if (bean.getBoolean(CONNECT_FROM_ANYWHERE) != enableOrDisable.enable) {
                    bean.setValue(CONNECT_FROM_ANYWHERE, enableOrDisable.enable);
                    bean.save();
                    System.out.println("User " + user.getUsername() + " updated");
                } else {
                    System.out.println("No update required for " + user.getUsername());
                }
            }
            result = 0;
        } else {
            result = 1;
        }
        return result;
    }

    /**
     * Returns the users.
     *
     * @return the users, or {@code null} if a user cannot be found
     */
    private List<User> getUsers() {
        List<User> result = new ArrayList<>();
        for (String username : usernames) {
            User user = getUser(username);
            if (user != null) {
                result.add(user);
            } else {
                System.err.println("User " + username + " not found");
                result = null;
                break;
            }
        }
        return result;
    }

    /**
     * The action. Picocli only allows one option to be set.
     */
    static class Action {

        @Option(names = {"--enable"}, description = {"Allows users to connect from anywhere"})
        private boolean enable;

        @Option(names = {"--disable"}, description = {"Prevents users from connecting from anywhere"})
        private boolean disable;
    }
}