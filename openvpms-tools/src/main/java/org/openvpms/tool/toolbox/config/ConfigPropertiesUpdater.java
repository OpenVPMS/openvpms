/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Class for reading and writing openvpms.properties files.
 *
 * @author Tim Anderson
 */
public class ConfigPropertiesUpdater extends ConfigProperties {

    /**
     * Determines if the properties have changed.
     */
    private boolean changed;

    /**
     * Creates a {@link ConfigPropertiesUpdater}.
     *
     * @param path the configuration file path
     * @throws IOException for any I/O error
     */
    public ConfigPropertiesUpdater(Path path) throws IOException {
        super(path);
    }

    /**
     * Sets the JDBC driver class name.
     *
     * @param driver the JDBC driver class name
     */
    public void setDriver(String driver) {
        set(DB_DRIVER, driver);
    }

    /**
     * Sets the database URL.
     *
     * @param url the database URL
     */
    public void setUrl(String url) {
        set(Config.DB_URL, url);
    }

    /**
     * Sets the database username.
     *
     * @param username the database username
     */
    public void setUsername(String username) {
        set(DB_USERNAME, username);
    }

    /**
     * Sets the database password.
     *
     * @param password the database password
     */
    public void setPassword(String password) {
        set(DB_PASSWORD, password);
    }

    /**
     * Sets the reporting database URL.
     *
     * @param url the reporting database URL
     */
    public void setReportingUrl(String url) {
        set(REPORTING_DB_URL, url);
    }

    /**
     * Sets the reporting username.
     *
     * @param username the reporting username
     */
    public void setReportingUsername(String username) {
        set(REPORTING_DB_USERNAME, username);
    }

    /**
     * Sets the reporting password.
     *
     * @param password the reporting password
     */
    public void setReportingPassword(String password) {
        set(REPORTING_DB_PASSWORD, password);
    }

    /**
     * Sets the encryption key.
     *
     * @param key the encryption key
     */
    public void setKey(String key) {
        set(OPENVPMS_KEY, key);
    }

    /**
     * Determines if the properties have changed.
     *
     * @return {@code true} if the properties have changed
     */
    public boolean isModified() {
        return changed;
    }

    /**
     * Writes the properties.
     *
     * @throws IOException for any I/O error
     */
    public void write() throws IOException {
        try (FileOutputStream stream = new FileOutputStream(getPath().toFile())) {
            getProperties().store(stream, "WARNING: This file must be kept secure!");
        }
    }

    /**
     * Attempts to restrict permissions to the file owner.
     *
     * @throws Exception if the permissions can't be set. This fails on Windows
     */
    public void setPermissions() throws Exception {
        PathHelper.restrictPermissions(getPath());
    }

    /**
     * Sets a property.
     *
     * @param name  the property name
     * @param value the property value
     * @return {@code true} if the property was changed
     */
    @Override
    protected boolean set(String name, String value) {
        boolean result = super.set(name, value);
        changed |= result;
        return result;
    }

}
