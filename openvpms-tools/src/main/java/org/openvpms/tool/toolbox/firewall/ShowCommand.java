/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.archetype.rules.security.FirewallSettings.AccessType;
import picocli.CommandLine;

/**
 * Command to set the firewall access type.
 *
 * @author Tim Anderson
 */
@CommandLine.Command(name = "--show", description = "Displays the firewall configuration")
public class ShowCommand extends AbstractFirewallCommand {

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        FirewallSettings settings = getSettings();
        AccessType type = settings.getAccessType();
        System.out.println("Access type: " + type);
        if (type == AccessType.UNRESTRICTED) {
            System.out.println("    Users can connect from anywhere");
        } else if (type == AccessType.ALLOWED_ONLY) {
            System.out.println("    Users can only connect from allowed addresses");
        } else {
            System.out.println("    Users can only connect from allowed addresses, unless they have " +
                               "'Connect from Anywhere' set");
        }
        System.out.println("Allowed addresses:");
        ListAllowedCommand.showAllowedAddresses(settings);
        return 0;
    }
}