/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox;

import org.openvpms.db.service.DatabaseService;
import org.openvpms.db.service.DatabaseVersionChecker;
import org.openvpms.db.service.DbVersionInfo;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.db.ContextLoader;
import org.openvpms.tool.toolbox.util.PathHelper;
import org.springframework.context.ApplicationContext;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Base class for commands requiring a Spring {@code ApplicationContext}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractApplicationContextCommand extends AbstractCommand {

    /**
     * Determines if the command can run without the database being up-to-date.
     */
    private final boolean ignoreDatabaseVersion;

    /**
     * The application context.
     */
    private ApplicationContext context;

    /**
     * The configuration properties.
     */
    private ConfigProperties properties;

    /**
     * Constructs an {@link AbstractApplicationContextCommand}.
     */
    public AbstractApplicationContextCommand() {
        this(false);
    }

    /**
     * Constructs an {@link AbstractApplicationContextCommand}.
     *
     * @param ignoreDatabaseVersion if {@code false}, abort if the database is not up-to-date
     */
    public AbstractApplicationContextCommand(boolean ignoreDatabaseVersion) {
        this.ignoreDatabaseVersion = ignoreDatabaseVersion;
    }

    /**
     * Returns the bean of the specified type.
     *
     * @param type the type of the bean
     * @return the corresponding bean
     */
    protected <T> T getBean(Class<T> type) {
        return getContext().getBean(type);
    }

    /**
     * Check preconditions before accessing the database.
     * <p/>
     * This implementation verifies that the database version is up-to-date
     *
     * @throws Exception if preconditions are not met
     */
    @Override
    protected void init() throws Exception {
        properties = loadProperties();
        checkProperties(properties);
        checkDriver(properties);
        checkDatabaseConnection();
        checkDatabaseVersion();
    }

    /**
     * Verifies that  connection can be established to the database.
     *
     * @throws IllegalStateException if a connection can't be established
     */
    private void checkDatabaseConnection() {
        try (Connection connection = DriverManager.getConnection(properties.getUrl(), properties.getUsername(),
                                                                 properties.getPassword())) {
            // no-op
        } catch (SQLException exception) {
            Path path = PathHelper.getPathRelativeToCWD(properties.getPath());
            throw new IllegalStateException("Failed to connect to the database: " + exception.getMessage() + "\n" +
                                            "Verify the details in " + path + " are correct");
        }
    }

    /**
     * Verifies that the database is up-to-date.
     *
     * @throws SQLException if the database is not up-to-date
     */
    private void checkDatabaseVersion() throws SQLException {
        DatabaseService service = getContext().getBean(DatabaseService.class);
        if (!ignoreDatabaseVersion) {
            DatabaseVersionChecker versionChecker = new DatabaseVersionChecker();
            versionChecker.check(service);
        } else {
            DbVersionInfo versionInfo = service.getVersionInfo();
            if (versionInfo.needsUpdate() || versionInfo.hasFutureVersion()) {
                System.err.println("WARNING: the database needs to be updated");
            }
        }
    }

    /**
     * Returns the application context.
     *
     * @return the application context
     */
    private ApplicationContext getContext() {
        if (context == null) {
            context = new ContextLoader(properties).getContext();
        }
        return context;
    }
}
