/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import static org.openvpms.tool.toolbox.archetype.ArchetypeLoadCommand.DEFAULT_PATH;

/**
 * Command to create the database.
 * <p/>
 * This supports creating empty databases, which should  be used when restoring backups of a prior version of OpenVPMS
 * to a new server.
 *
 * @author Tim Anderson
 */
@Command(name = "--create", header = "Create the database")
public class CreateDBCommand extends AbstractAdminDBCommand {

    /**
     * The host the users are connecting from.
     */
    @Option(names = "--host", description = "Host users are connecting from", required = true,
            defaultValue = "localhost", showDefaultValue = CommandLine.Help.Visibility.ALWAYS)
    private String host;

    /**
     * Determines if an empty database should be created.
     */
    @Option(names = "--empty", description = "Create an empty database.\n" +
                                             "Use this when restoring a backup to a new server.")
    private boolean empty;

    /**
     * The archetype directory.
     */
    @Option(names = {"-d", "--dir"}, description = "The archetype directory.", defaultValue = DEFAULT_PATH)
    private String dir = DEFAULT_PATH;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result;
        DatabaseAdminService service = getAdminService();
        ConfigProperties properties = getProperties();
        service.create(new Credentials(properties.getUsername(), properties.getPassword()),
                       new Credentials(properties.getReportingUsername(), properties.getReportingPassword()),
                       host, !empty);
        if (!empty) {
            updateDatabase(dir, null); // loads archetypes
            result = 0;
        } else {
            result = 0;
        }
        return result;
    }

}
