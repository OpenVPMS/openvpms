/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.user;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import picocli.CommandLine.Command;

import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 * List users.
 *
 * @author Tim Anderson
 */
@Command(name = "--list", description = "List users")
public class ListUsersCommand extends AbstractApplicationContextCommand {

    /**
     * Row format.
     */
    private static final String FORMAT = "%10s %-16s %-24s %-6s %-24s %-8s %s\n";

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        Contacts contacts = new Contacts(getBean(IArchetypeRuleService.class));
        Iterator<User> iterator = getUsers();
        System.out.printf(FORMAT, "id", "username", "name", "active", "email", "connect", "roles");
        System.out.printf(FORMAT, "", "", "", "", "", "from", "");
        System.out.printf(FORMAT, "", "", "", "", "", "anywhere", "");
        System.out.println(StringUtils.repeat('-', 108));
        while (iterator.hasNext()) {
            User user = iterator.next();
            write(user, contacts);
        }
        return 0;
    }

    /**
     * Returns the users.
     *
     * @return the users
     */
    private Iterator<User> getUsers() {
        ArchetypeService service = getBean(IArchetypeRuleService.class);
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> from = query.from(User.class, UserArchetypes.USER);
        query.orderBy(builder.asc(from.get("username")));
        return new TypedQueryIterator<>(service.createQuery(query), 100);
    }

    /**
     * Outputs a user.
     *
     * @param user     the user
     * @param contacts the contacts helper
     */
    private void write(User user, Contacts contacts) {
        IMObjectBean bean = getBean(IArchetypeRuleService.class).getBean(user);
        String email = contacts.getEmail(user);
        if (email == null) {
            email = "";
        }
        boolean connectFromAnywhere = bean.getBoolean("connectFromAnywhere");
        System.out.printf(FORMAT, user.getId(), user.getUsername(), user.getName(), user.isActive(),
                          email, connectFromAnywhere, getRoles(user));
    }

    /**
     * Returns the roles of a user as a comma-separated string.
     *
     * @param user the user
     * @return the user roles
     */
    private String getRoles(User user) {
        Comparator<SecurityRole> comparator = (o1, o2) -> StringUtils.compare(o1.getName(), o2.getName());
        return user.getRoles().stream().sorted(comparator).map(IMObject::getName).collect(Collectors.joining(", "));
    }
}