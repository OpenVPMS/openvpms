/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.plugin;

import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Constants;

import java.util.jar.Attributes;
import java.util.jar.Manifest;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getManifest;

/**
 * .
 *
 * @author Tim Anderson
 */
public class PluginArtifactHelper {

    /**
     * Returns the plugin key.
     * <p/>
     * This implementation requires the manifest to contain both OSGi headers and the Atlassian-Plugin-Key.
     *
     * @param artifact The plugin artifact
     * @return the plugin key if a manifest is present and contains {@link OsgiPlugin#ATLASSIAN_PLUGIN_KEY},
     * {@link Constants#BUNDLE_SYMBOLICNAME}, and {@link Constants#BUNDLE_VERSION}
     */
    public static String getPluginKey(PluginArtifact artifact) {
        String result = null;
        Manifest manifest = getManifest(artifact);
        if (manifest != null) {
            Attributes attributes = manifest.getMainAttributes();
            String key = attributes.getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
            String name = attributes.getValue(Constants.BUNDLE_SYMBOLICNAME);
            String version = attributes.getValue(Constants.BUNDLE_VERSION);
            if (!StringUtils.isEmpty(key) && !StringUtils.isEmpty(name) && !StringUtils.isEmpty(version)) {
                result = OsgiHeaderUtil.getPluginKey(manifest);
            }
        }
        return result;
    }
}