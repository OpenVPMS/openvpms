/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import picocli.CommandLine;

/**
 * Command to set the firewall access type.
 *
 * @author Tim Anderson
 */
@CommandLine.Command(name = "--set-access", description = "Sets the firewall access type")
public class SetAccessCommand extends AbstractFirewallCommand {

    /**
     * The type.
     */
    @CommandLine.Parameters(index = "0", arity = "1", description = "one of UNRESTRICTED, ALLOWED_ONLY, ALLOWED_USER")
    FirewallSettings.AccessType type;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        FirewallSettings settings = getSettings();
        if (settings.getAccessType() != type) {
            settings.setAccessType(type);
            save(settings);
            System.out.println("Firewall updated");
        } else {
            System.err.println("No update required");
        }
        return 0;
    }
}