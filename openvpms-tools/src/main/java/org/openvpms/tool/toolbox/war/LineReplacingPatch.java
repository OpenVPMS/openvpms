/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

import org.apache.commons.io.IOUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.jar.JarOutputStream;

/**
 * Patch to replace lines in text files.
 *
 * @author Tim Anderson
 */
abstract class LineReplacingPatch implements Patch {

    /**
     * Applies a patch.
     *
     * @param input  input stream to the entry to patch
     * @param output the entry to write to
     * @throws IOException for any I/O error
     */
    @Override
    public void apply(InputStream input, JarOutputStream output) throws IOException {
        List<String> lines = IOUtils.readLines(input, StandardCharsets.UTF_8);
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(output, StandardCharsets.UTF_8)))) {
            for (String line : lines) {
                line = replace(line);
                writer.println(line);
            }
        }
    }

    /**
     * Performs replacement on a line of text.
     *
     * @param line the line
     * @return the (possibly modified) line
     */
    protected abstract String replace(String line);
}
