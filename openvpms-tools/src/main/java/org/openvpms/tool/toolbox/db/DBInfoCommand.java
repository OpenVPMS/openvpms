/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.flywaydb.core.internal.info.MigrationInfoDumper;
import org.openvpms.db.service.impl.DatabaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Command;

/**
 * Command to display database version information.
 *
 * @author Tim Anderson
 */
@Command(name = "--info", header = "Displays database migration information")
public class DBInfoCommand extends AbstractDBCommand {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DBInfoCommand.class);

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     */
    @Override
    protected int run() {
        DatabaseServiceImpl service = (DatabaseServiceImpl) getDatabaseService();
        log.info(MigrationInfoDumper.dumpToAsciiTable(service.getInfo().all()));
        return 0;
    }

}
