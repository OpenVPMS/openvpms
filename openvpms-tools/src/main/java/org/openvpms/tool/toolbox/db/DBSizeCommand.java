/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.apache.commons.io.FileUtils;
import org.openvpms.db.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine.Command;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Displays the database size.
 *
 * @author Tim Anderson
 */
@Command(name = "--size", header = "Displays the database size")
public class DBSizeCommand extends AbstractDBCommand {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DBSizeCommand.class);

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        DatabaseService service = getDatabaseService();
        long size = service.getSize();
        String value;
        if (size / (10 * FileUtils.ONE_GB) > 0) {
            value = getSize(size, FileUtils.ONE_GB, "GB");
        } else if (size / FileUtils.ONE_MB > 0) {
            value = getSize(size, FileUtils.ONE_MB, "MB");
        } else if (size / FileUtils.ONE_KB > 0) {
            value = getSize(size, FileUtils.ONE_KB, "KB");
        } else {
            value = size + " bytes";
        }
        log.info(service.getSchemaName() + " " + value);
        return 0;
    }

    /**
     * Helper to return a formatted size, rounded.
     *
     * @param size    the size
     * @param divisor the divisor
     * @param suffix  the size suffix
     * @return the formatted size
     */
    private String getSize(long size, long divisor, String suffix) {
        BigDecimal result = new BigDecimal(size).divide(BigDecimal.valueOf(divisor), 2, RoundingMode.CEILING);
        DecimalFormat format = new DecimalFormat("#,##.##");
        return format.format(result) + " " + suffix;
    }
}
