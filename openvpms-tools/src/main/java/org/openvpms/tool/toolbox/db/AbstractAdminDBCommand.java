/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import org.openvpms.db.service.Credentials;
import org.openvpms.db.service.DatabaseAdminService;
import org.openvpms.db.service.impl.DatabaseAdminServiceImpl;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.sql.SQLException;

/**
 * Base class for database commands that require admin access.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAdminDBCommand extends AbstractDBCommand {

    /**
     * The logger.
     */
    private final Logger log;

    /**
     * The admin username.
     */
    @CommandLine.Option(names = "-u", required = true, description = "Database administrator user name")
    private String user;

    /**
     * The admin password.
     */
    @CommandLine.Option(names = "-p", required = true, description = "Database administrator password",
            interactive = true, arity = "0..1")
    private String password;

    /**
     * The database administration service.
     */
    private DatabaseAdminService adminService;

    /**
     * Constructs an {@link AbstractAdminDBCommand}.
     */
    protected AbstractAdminDBCommand() {
        this.log = LoggerFactory.getLogger(getClass());
    }

    /**
     * Initialises the command.
     *
     * @throws Exception for any error
     */
    @Override
    protected void init() throws Exception {
        super.init();
        ConfigProperties properties = getProperties();
        adminService = new DatabaseAdminServiceImpl(properties.getDriver(), properties.getUrl(),
                                                    new Credentials(user, password),
                                                    new FlywayLogger(log, false));
    }

    /**
     * Returns the database administration service.
     *
     * @return the service
     */
    protected DatabaseAdminService getAdminService() {
        return adminService;
    }

    /**
     * Updates the database.
     * <p/>
     * This runs any necessary migrations, including loading archetypes.
     *
     * @param archetypesDir the archetypes directory
     * @param pluginsDir    the plugins directory, or {@code null} if plugins aren't being loaded
     */
    protected void updateDatabase(String archetypesDir, String pluginsDir) throws SQLException {
        ConfigProperties properties = getProperties();
        ContextLoader loader = new ContextLoader(properties.getDriver(), properties.getUrl(), user, password);
        DatabaseUpdater updater = new DatabaseUpdater(properties, loader, adminService);
        updater.update(archetypesDir, pluginsDir);
    }
}
