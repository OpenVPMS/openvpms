/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.plugin;

import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import picocli.CommandLine.Command;

import java.util.Iterator;

/**
 * Command to list installed plugins.
 *
 * @author Tim Anderson
 */
@Command(name = "--list", description = "Lists installed plugins")
public class ListPluginCommand extends AbstractApplicationContextCommand {

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        PluginDAO dao = getBean(PluginDAO.class);
        Iterator<Plugin> plugins = dao.getPlugins();
        boolean found = false;
        while (plugins.hasNext()) {
            Plugin plugin = plugins.next();
            System.out.println(plugin.getKey());
            found = true;
        }
        if (!found) {
            System.err.println("No plugins installed");
        }
        return (found) ? 0 : 1;
    }

}
