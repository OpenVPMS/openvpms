/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PasswordGenerator}.
 *
 * @author Tim Anderson
 */
public class PasswordGeneratorTestCase {

    /**
     * Tests the {@link PasswordGenerator#generate()} method.
     */
    @Test
    public void testGenerate() {
        PasswordGenerator generator = new PasswordGenerator();
        for (int i = 0; i < 100; ++i) {
            String password = generator.generate();

            // verify the password is between 8 and 16 chars long
            assertTrue(password.length() >= 8 && password.length() <= 16);

            // check that the password contains at least 2 characters from the specified dictionaries.
            checkMatches(password, PasswordGenerator.UPPER);
            checkMatches(password, PasswordGenerator.LOWER);
            checkMatches(password, PasswordGenerator.NUMERIC);
            checkMatches(password, PasswordGenerator.SPECIAL);
        }
    }

    /**
     * Verifies that a password contains at least 2 characters from the specified dictionary.
     * <p/>
     * A character may appear twice.
     *
     * @param password   the password
     * @param dictionary the dictionary
     */
    private void checkMatches(String password, String dictionary) {
        int matches = 0;
        for (char ch : dictionary.toCharArray()) {
            matches += StringUtils.countMatches(password, "" + ch);
        }
        assertTrue(matches >= 2);
    }
}