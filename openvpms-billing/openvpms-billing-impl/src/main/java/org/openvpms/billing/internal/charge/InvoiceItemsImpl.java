/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.billing.charge.ChargeItems;
import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.billing.charge.InvoiceItemBuilder;
import org.openvpms.billing.charge.InvoiceItems;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;

import java.util.List;

/**
 * Implementation of {@link ChargeItems} for {@link InvoiceItem}.
 *
 * @author Tim Anderson
 */
class InvoiceItemsImpl extends ChargeItemsImpl<Invoice, InvoiceItem, InvoiceBuilder, InvoiceItemBuilder>
        implements InvoiceItems {

    /**
     * Constructs an {@link InvoiceItemsImpl}.
     *
     * @param items the item builders
     */
    public InvoiceItemsImpl(List<InvoiceItemBuilder> items) {
        super(items);
    }
}