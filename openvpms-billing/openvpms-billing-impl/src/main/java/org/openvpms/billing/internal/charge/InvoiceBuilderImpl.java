/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.billing.charge.ChargeItems;
import org.openvpms.billing.charge.ChargeObjects;
import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.billing.charge.InvoiceItemBuilder;
import org.openvpms.billing.charge.InvoiceItems;
import org.openvpms.billing.charge.InvoiceObjects;
import org.openvpms.billing.exception.BillingException;
import org.openvpms.billing.internal.i18n.BillingMessages;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.product.Template;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of {@link InvoiceBuilder}.
 *
 * @author Tim Anderson
 */
public class InvoiceBuilderImpl
        extends ChargeBuilderImpl<Invoice, InvoiceItem, InvoiceBuilder, InvoiceItemBuilder>
        implements InvoiceBuilder {

    /**
     * Builders to generate reminders, keyed on patient.
     */
    private final Map<Patient, List<ReminderBuilder>> reminderBuilders = new HashMap<>();

    /**
     * Builders to generate alerts, keyed on patient.
     */
    private final Map<Patient, List<AlertBuilder>> alertBuilders = new HashMap<>();

    /**
     * Builders to add visit notes for templates.
     */
    private final List<TemplateVisitNoteBuilder> visitNoteBuilders = new ArrayList<>();

    /**
     * Constructs an {@link InvoiceBuilderImpl}.
     *
     * @param services the build services
     */
    public InvoiceBuilderImpl(BuilderServices services) {
        super(CustomerAccountArchetypes.INVOICE, services);
    }

    /**
     * Returns a builder to add a charge item.
     * <p/>
     * Use {@link InvoiceItemBuilder#add()} to add the item.
     *
     * @return the builder
     */
    @Override
    public InvoiceItemBuilderImpl newItem() {
        return new InvoiceItemBuilderImpl(this, getServices());
    }

    /**
     * Expands a template.
     * <p/>
     * All items will be added to the charge.
     *
     * @param template  the product template
     * @param patient   the patient
     * @param quantity  the template quantity
     * @param clinician the clinician
     * @return the charge items generated from the template
     */
    @Override
    public InvoiceItems expand(Template template, Patient patient, BigDecimal quantity, User clinician) {

        // If the template has a visit note, add a builder to add the note to the patient history
        // Only do this once per patient/template pair.
        String note = template.getVisitNote();
        if (!StringUtils.isEmpty(note)
            && visitNoteBuilders.stream().noneMatch(builder -> builder.matches(patient, template))) {
            visitNoteBuilders.add(new TemplateVisitNoteBuilder(patient, template, note, clinician, getServices()));
        }

        return (InvoiceItems) super.expand(template, patient, quantity, clinician);
    }

    /**
     * Returns the charge item builders, for new and updated items.
     *
     * @return the charge item builders
     */
    @Override
    public InvoiceItems getChangedItems() {
        return (InvoiceItems) super.getChangedItems();
    }

    /**
     * Builds the charge, and returns the built objects.
     *
     * @return the built objects
     */
    @Override
    public InvoiceObjects buildObjects() {
        return (InvoiceObjects) super.buildObjects();
    }

    /**
     * Returns the object to update.
     * <p/>
     * This implementation returns the latest {@code IN_PROGRESS} or {@code COMPLETED} invoice for the customer
     * if one is present. Invoices with {@code IN_PROGRESS} will be returned in preference to {@code COMPLETED} ones.
     * <p/>
     * If no invoice exists, creates a new one.
     *
     * @return the charge
     */
    @Override
    protected FinancialAct getObject() {
        Customer customer = getCustomer();
        if (customer == null) {
            throw new BillingException(BillingMessages.valueRequired("customer"));
        }
        CustomerAccountRules rules = getServices().getCustomerAccountRules();
        FinancialAct invoice = rules.getInvoice(customer);
        if (invoice == null) {
            invoice = super.getObject();
        }
        return invoice;
    }

    /**
     * Creates a {@link ChargeItems}.
     *
     * @param builders the item builders
     * @return the items
     */
    @Override
    protected InvoiceItems createChargeItems(List<InvoiceItemBuilder> builders) {
        return new InvoiceItemsImpl(builders);
    }

    /**
     * Creates a {@link ChargeObjects}
     *
     * @param context the build context
     * @return a new charge state
     */
    @Override
    protected ChargeObjects<Invoice, InvoiceItem> createChargeObjects(BuildContext context) {
        return new InvoiceObjectsImpl((InvoiceContext) context, getServices().getDomainService());
    }

    /**
     * Creates a charge context.
     *
     * @param charge the charge
     * @return the charge context
     */
    @Override
    protected BuildContext createBuildContext(IMObjectBean charge) {
        return new InvoiceContext(charge, createPricingContext(), getArchetypeService(),
                                  getServices().getReminderRules());
    }

    /**
     * Adds a reminder builder.
     *
     * @param builder the reminder builder
     */
    protected void addReminder(ReminderBuilder builder) {
        List<ReminderBuilder> builders = reminderBuilders.computeIfAbsent(
                builder.getPatient(), (k) -> new ArrayList<>());
        builders.add(builder);
    }

    /**
     * Adds an alert builder.
     *
     * @param builder the alert builder
     */
    protected void addAlert(AlertBuilder builder) {
        List<AlertBuilder> builders = alertBuilders.computeIfAbsent(builder.getPatient(), (k) -> new ArrayList<>());
        builders.add(builder);
    }

    /**
     * Builds the charge.
     *
     * @param charge  the charge
     * @param bean    a bean wrapping the charge
     * @param date    the date for determining prices and discounts
     * @param context the build context
     */
    @Override
    protected void build(FinancialAct charge, IMObjectBean bean, Date date, BuildContext context) {
        super.build(charge, bean, date, context);

        InvoiceContext invoiceContext = (InvoiceContext) context;

        buildReminders(invoiceContext);
        buildAlerts(invoiceContext);
        buildVisitNotes(date, invoiceContext);
    }

    /**
     * Resets the builder.
     * <p/>
     * This clears any state that should not be retained across builds.
     */
    @Override
    protected void reset() {
        super.reset();
        reminderBuilders.clear();
        visitNoteBuilders.clear();
    }

    /**
     * Adds any visit notes from templates to patient history.
     *
     * @param date    the date used to select the visit
     * @param context the build context
     */
    private void buildVisitNotes(Date date, InvoiceContext context) {
        for (TemplateVisitNoteBuilder visitNoteBuilder : visitNoteBuilders) {
            visitNoteBuilder.build(date, context);
        }
    }

    /**
     * Builds reminders.
     * <p/>
     * This excludes any reminders for a patient for the same reminder type.
     *
     * @param context the build context
     */
    private void buildReminders(InvoiceContext context) {
        for (List<ReminderBuilder> builders : reminderBuilders.values()) {
            // process reminders by patient
            builders = removeDuplicateReminders(builders);
            for (ReminderBuilder builder : builders) {
                builder.build(context);
            }
        }
    }

    /**
     * Builds alerts.
     * <p/>
     * This excludes any alerts for a patient for the same alert type.
     *
     * @param context the build context
     */
    private void buildAlerts(InvoiceContext context) {
        for (List<AlertBuilder> builders : alertBuilders.values()) {
            // process builders by patient
            builders = removeDuplicateAlerts(builders);
            for (AlertBuilder builder : builders) {
                builder.build(context);
            }
        }
    }

    /**
     * Removes any builders for the same reminder type.
     * <p/>
     * If multiple reminders exist with the same reminder type but different products, the one with the earliest
     * due date will be returned. If multiple have the same due date, the on with the lowest product id will be
     * returned.
     *
     * @param builders the builders. Must be for the same patient
     * @return the reminder builders
     */
    private List<ReminderBuilder> removeDuplicateReminders(List<ReminderBuilder> builders) {
        List<ReminderBuilder> result;
        if (builders.size() > 1) {
            Map<Entity, ReminderBuilder> map = new HashMap<>(); // reminder builders by reminder type
            for (ReminderBuilder builder : builders) {
                ReminderBuilder existing = map.get(builder.getReminderType());
                if (existing == null) {
                    map.put(builder.getReminderType(), builder);
                } else {
                    int compareTo = existing.getDueDate().compareTo(builder.getDueDate());
                    if (compareTo > 0 || compareTo == 0
                                         && (existing.getProduct().getId() > builder.getProduct().getId())) {
                        map.put(builder.getReminderType(), builder);
                    }
                }
            }
            result = new ArrayList<>(map.values());
        } else {
            result = builders;
        }
        return result;
    }

    /**
     * Removes any builders for the same alert type.
     * <p/>
     * If multiple alerts exist with the same alert type the first encountered will be selected.
     *
     * @param builders the builders. Must be for the same patient
     * @return the alert builders
     */
    private List<AlertBuilder> removeDuplicateAlerts(List<AlertBuilder> builders) {
        List<AlertBuilder> result;
        if (builders.size() > 1) {
            Map<Entity, AlertBuilder> map = new HashMap<>(); // alert builders by alert type
            for (AlertBuilder builder : builders) {
                map.putIfAbsent(builder.getAlertType(), builder);
            }
            result = new ArrayList<>(map.values());
        } else {
            result = builders;
        }
        return result;
    }

}