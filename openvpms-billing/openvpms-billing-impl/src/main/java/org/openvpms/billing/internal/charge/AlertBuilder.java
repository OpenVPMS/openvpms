/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.patient.Patient;

import java.util.Date;

/**
 * Builds patient alerts.
 *
 * @author Tim Anderson
 */
class AlertBuilder {

    /**
     * The builder service.
     */
    private final BuilderServices services;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The alert type.
     */
    private Entity alertType;

    /**
     * The product.
     */
    private Product product;

    /**
     * The date.
     */
    private Date date;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The invoice item.
     */
    private IMObjectBean invoiceItem;

    /**
     * Constructs a {@link AlertBuilder}.
     *
     * @param services the builder services
     */
    public AlertBuilder(BuilderServices services) {
        this.services = services;
    }

    /**
     * Returns the patient.
     *
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public AlertBuilder patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Returns the alert type.
     *
     * @return the alert type
     */
    public Entity getAlertType() {
        return alertType;
    }

    /**
     * Sets the alert type.
     *
     * @param alertType the alert type
     * @return this
     */
    public AlertBuilder alertType(Entity alertType) {
        this.alertType = alertType;
        return this;
    }

    /**
     * Sets the date.
     * <p/>
     * The reminder due date will be calculated from this
     *
     * @param date the date
     * @return the date
     */
    public AlertBuilder date(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public AlertBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public AlertBuilder clinician(User clinician) {
        this.clinician = clinician;
        return this;
    }

    /**
     * Sets the invoice item.
     *
     * @param bean a bean wrapping the invoice item
     * @return this
     */
    public AlertBuilder invoiceItem(IMObjectBean bean) {
        this.invoiceItem = bean;
        return this;
    }

    /**
     * Builds an alert, adding it to the build context.
     *
     * @param context the build context
     */
    public void build(InvoiceContext context) {
        ArchetypeService service = services.getArchetypeService();
        Act alert = service.create(PatientArchetypes.ALERT, Act.class);
        IMObjectBean bean = service.getBean(alert);
        bean.setTarget("patient", patient);
        bean.setTarget("alertType", alertType);
        bean.setTarget("product", product);
        bean.setTarget("clinician", clinician);
        IMObjectBean alertTypeBean = service.getBean(alertType);
        alert.setReason(alertTypeBean.getString("reason"));

        Date endTime = null;
        int duration = alertTypeBean.getInt("duration");
        String units = alertTypeBean.getString("durationUnits");
        if (duration > 0 && units != null) {
            endTime = DateRules.getDate(date, duration, DateUnits.valueOf(units));
        }
        alert.setActivityStartTime(date);
        alert.setActivityEndTime(endTime);

        invoiceItem.addTarget("alerts", alert, "invoiceItem");
        context.addAlert(alert);
    }
}