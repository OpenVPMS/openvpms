/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.rules.finance.tax.CustomerTaxRules;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.PricingGroup;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ServiceRatioService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Pricing context, used to determine prices and discounts at a practice location.
 *
 * @author Tim Anderson
 */
public class PricingContext {

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The currency.
     */
    private final Currency currency;

    /**
     * The practice location.
     */
    private final Party location;

    /**
     * The price rules.
     */
    private final ProductPriceRules priceRules;

    /**
     * The discount rules.
     */
    private final DiscountRules discountRules;

    /**
     * The service ratio service.
     */
    private final ServiceRatioService serviceRatios;

    /**
     * The pricing group.
     */
    private final PricingGroup pricingGroup;

    /**
     * The customer tax rules.
     */
    private final CustomerTaxRules taxRules;

    /**
     * Determines if discounts are disabled.
     */
    private final boolean disableDiscounts;

    /**
     * Constructs a {@link PricingContext}.
     *
     * @param practice      the practice
     * @param location      the practice location
     * @param practiceRules the practice rules
     * @param priceRules    the price rules
     * @param discountRules the discount rules
     * @param locationRules the location rules
     * @param serviceRatios the service ratio service
     */
    public PricingContext(Party practice, Party location, PracticeRules practiceRules, ProductPriceRules priceRules,
                          DiscountRules discountRules, LocationRules locationRules, ServiceRatioService serviceRatios,
                          IArchetypeService archetypeService) {
        this.practice = practice;
        this.currency = practiceRules.getCurrency(practice);
        if (currency == null) {
            throw new IllegalStateException("Practice has no currency set");
        }
        this.location = location;
        this.pricingGroup = new PricingGroup(locationRules.getPricingGroup(location));
        this.priceRules = priceRules;
        this.discountRules = discountRules;
        this.serviceRatios = serviceRatios;
        taxRules = new CustomerTaxRules(practice, archetypeService);
        disableDiscounts = archetypeService.getBean(location).getBoolean("disableDiscounts");
    }

    /**
     * Returns the tax-inclusive price given a tax-exclusive price and service ratio.
     * <p>
     * This takes into account customer tax exclusions.
     *
     * @param product      the product
     * @param price        the tax-exclusive price
     * @param serviceRatio the service ratio. May be {@code null}
     * @return the tax-inclusive price, rounded according to the practice currency conventions
     */
    public BigDecimal getPrice(Product product, Party customer, ProductPrice price, BigDecimal serviceRatio) {
        BigDecimal result = BigDecimal.ZERO;
        BigDecimal taxExPrice = price.getPrice();
        if (taxExPrice != null) {
            if (serviceRatio != null) {
                taxExPrice = taxExPrice.multiply(serviceRatio);
            }
            BigDecimal taxRate = taxRules.getTaxRate(product, customer);
            result = priceRules.getTaxIncPrice(taxExPrice, taxRate, currency);
        }
        return result;
    }

    /**
     * Returns the default fixed price for a product.
     *
     * @param product the product
     * @param date    the date
     * @return the fixed price, or {@code null} if none is found
     */
    public ProductPrice getFixedPrice(Product product, Date date) {
        return priceRules.getProductPrice(product, ProductArchetypes.FIXED_PRICE, date, pricingGroup.getGroup());
    }

    /**
     * Returns the unit price for a product.
     *
     * @param product the product
     * @param date    the date
     * @return the unit price, or {@code null} if none is found
     */
    public ProductPrice getUnitPrice(Product product, Date date) {
        return priceRules.getProductPrice(product, ProductArchetypes.UNIT_PRICE, date, pricingGroup.getGroup());
    }

    /**
     * Returns the service ratio for a product and date.
     *
     * @param product the product
     * @param date    the date
     * @return the service ratio, or {@code null} if none is defined
     */
    public BigDecimal getServiceRatio(Product product, Date date) {
        return serviceRatios.getServiceRatio(product, null, location, date);
    }

    /**
     * Returns the maximum discount for a price.
     *
     * @param price the price
     * @return the maximum discount for the product price, or {@code 100} if there is no maximum discount associated
     * with the price.
     */
    public BigDecimal getMaxDiscount(ProductPrice price) {
        return priceRules.getMaxDiscount(price);
    }

    /**
     * Calculates the discount.
     *
     * @param unitPrice the unit price
     * @param quantity  the quantity
     * @return the discount or {@code BigDecimal.ZERO} if the discount can't be calculated or discounts are disabled
     */
    public BigDecimal getDiscount(Party customer, Party patient, Product product, BigDecimal fixedCost,
                                  BigDecimal fixedPrice, BigDecimal fixedPriceMaxDiscount, BigDecimal unitCost,
                                  BigDecimal unitPrice, BigDecimal unitPriceMaxDiscount, BigDecimal quantity,
                                  Date date) {
        BigDecimal amount = BigDecimal.ZERO;
        if (!disableDiscounts) {
            amount = discountRules.calculateDiscount(date, practice, customer, patient, product,
                                                     fixedCost, unitCost, fixedPrice, unitPrice, quantity,
                                                     fixedPriceMaxDiscount, unitPriceMaxDiscount);
        }
        return amount;
    }

    /**
     * Returns the cost of a product.
     *
     * @param price the product price
     * @return the cost
     */
    public BigDecimal getCost(ProductPrice price) {
        return priceRules.getCostPrice(price);
    }

    /**
     * Returns the tax amount for a product and customer.
     * <p/>
     * This excludes an taxes the customer has exemptions for.
     *
     * @param total    the total, tax-inclusive
     * @param product  the product, used to determine tax rates
     * @param customer the customer, used to determine excluded tax rates
     * @return the tax amount
     */
    public BigDecimal getTax(BigDecimal total, Product product, Party customer) {
        return taxRules.calculateTax(total, taxRules.getTaxRates(product, customer), true);
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    public Party getLocation() {
        return location;
    }
}