/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.billing.exception.BillingException;
import org.openvpms.billing.internal.i18n.BillingMessages;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.product.Template;
import org.openvpms.domain.product.TemplateItem;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Expands product templates.
 *
 * @author Tim Anderson
 */
public class TemplateExpander {

    /**
     * Determines if products should be restricted to those available at the location or stock location.
     */
    private final boolean useLocationProducts;

    /**
     * The practice location to restrict products to. May be {@code null}
     */
    private final Party location;

    /**
     * The stock location to restrict products to. May be {@code null}
     */
    private final Party stockLocation;

    /**
     * Stock rules.
     */
    private final StockRules stockRules;

    /**
     * Product rules.
     */
    private final ProductRules productRules;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link TemplateExpander}.
     *
     * @param useLocationProducts if {@code true}, products must be present at the location or stock location to be
     *                            included, unless the relationship is set to always include products
     * @param location            the practice location to restrict products to. Only relevant when
     *                            {@code useLocationProducts == true}. May be {@code null}
     * @param stockLocation       the stock location to restrict products to. Only relevant when
     *                            {@code useLocationProducts == true}. May be {@code null}
     * @param domainService       the domain object service
     */
    public TemplateExpander(boolean useLocationProducts, Party location, Party stockLocation, StockRules stockRules,
                            ProductRules productRules, DomainService domainService) {
        this.useLocationProducts = useLocationProducts;
        this.location = location;
        this.stockLocation = stockLocation;
        this.stockRules = stockRules;
        this.productRules = productRules;
        this.domainService = domainService;
    }

    /**
     * Expands a template for the given patient weight.
     *
     * @param template the template
     * @param weight   the patient weight
     * @param quantity the quantity
     * @return the result of the template expansion
     */
    public Collection<ProductQuantity> expand(Template template, Weight weight, BigDecimal quantity) {
        Map<ProductQuantity, ProductQuantity> includes = new LinkedHashMap<>();
        ArrayDeque<Product> parents = new ArrayDeque<>();
        expand(template, template, weight, includes, quantity, quantity, false, parents);
        return includes.values();
    }

    /**
     * Expands a template.
     *
     * @param template     the template to expand
     * @param root         the root template
     * @param weight       the patient weight. If {@code 0}, indicates the weight is unknown
     * @param includes     the existing includes
     * @param lowQuantity  the low quantity
     * @param highQuantity the high quantity
     * @param zeroPrice    if {@code true}, zero prices for all included products
     * @param parents      the parent templates
     */
    protected void expand(Template template, Template root, Weight weight,
                          Map<ProductQuantity, ProductQuantity> includes, BigDecimal lowQuantity,
                          BigDecimal highQuantity, boolean zeroPrice, Deque<Product> parents) {
        if (template.isActive()) {
            if (!parents.contains(template)) {
                parents.push(template);
                for (TemplateItem item : template.getItems()) {
                    add(item, template, root, weight, includes, lowQuantity, highQuantity, zeroPrice, parents);
                }
                parents.pop();
            } else {
                // recursive template expansion
                List<Product> products = new ArrayList<>(parents);
                Collections.reverse(products);
                products.add(template);
                List<String> names = products.stream().map(IMObject::getName).collect(Collectors.toList());
                throw new BillingException(BillingMessages.recursiveTemplateExpansion(root.getName(),
                                                                                      template.getName(), names));
            }
        }
    }

    /**
     * Adds a product.
     *
     * @param item         the template item
     * @param template     the template to expand
     * @param root         the root template
     * @param weight       the patient weight. If {@code 0}, indicates the weight is unknown
     * @param includes     the existing includes
     * @param lowQuantity  the low quantity
     * @param highQuantity the high quantity
     * @param zeroPrice    if {@code true}, zero prices for all included products
     * @param parents      the parent templates
     */
    protected void add(TemplateItem item, Template template, Template root, Weight weight,
                       Map<ProductQuantity, ProductQuantity> includes, BigDecimal lowQuantity,
                       BigDecimal highQuantity, boolean zeroPrice, Deque<Product> parents) {
        Product product = item.getProduct();
        if (item.requiresWeight() && weight.isZero()) {
            throw new BillingException(BillingMessages.weightRequired(product.getName(), template.getName()));
        } else if (!item.requiresWeight() || item.inWeightRange(weight)) {
            checkAvailability(item, template);
            BigDecimal newLowQty = item.getLowQuantity().multiply(lowQuantity);
            BigDecimal newHighQty = item.getHighQuantity().multiply(highQuantity);
            boolean zero = item.getZeroPrice() || zeroPrice;
            if (product instanceof Template) {
                expand((Template) product, root, weight, includes, newLowQty, newHighQty, zero, parents);
            } else {
                ProductQuantity current = new ProductQuantity(product, newLowQty, newHighQty, zero, item.getPrint());
                ProductQuantity existing = includes.get(current);
                if (existing == null) {
                    includes.put(current, current);
                } else {
                    existing.add(newLowQty, newHighQty);
                }
            }
        }
    }

    /**
     * Verifies that a product is a available at a location.
     * <p/>
     * For medication and merchandise products, the product must have a relationship to the stock location, if
     * specified. <br/>
     * For service and template products, the product must have a relationship to the practice location, if specified.
     *
     * @param product the product
     * @return the practice or stock location where the product is available, or {@code null} if it is not available
     */
    private Party checkProductLocation(Product product) {
        Party result = null;
        if (product.isA(ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE)) {
            if (stockLocation != null && !stockRules.hasStockRelationship(product, stockLocation)) {
                result = stockLocation;
            }
        } else if (product.isA(ProductArchetypes.SERVICE, ProductArchetypes.TEMPLATE)) {
            if (location != null && !productRules.canUseProductAtLocation(product, location)) {
                result = location;
            }
        }
        return result;
    }

    /**
     * Checks the availability of a template item.
     *
     * @param item     the template item
     * @param template the template
     * @throws BillingException if the product isn't available at the location
     */
    private void checkAvailability(TemplateItem item, Template template) {
        if (useLocationProducts) {
            Product product = item.getProduct();
            Party location = checkProductLocation(product);
            if (location != null) {
                if (!item.getSkipIfMissing()) {
                    throw new BillingException(BillingMessages.templateProductNotAvailableAtLocation(
                            template.getName(), product.getName(), location.getName()));
                }
            }
        }
    }

}
