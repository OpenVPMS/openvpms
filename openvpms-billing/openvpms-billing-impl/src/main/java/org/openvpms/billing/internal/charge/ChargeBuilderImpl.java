/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.act.ActCalculator;
import org.openvpms.billing.charge.ChargeBuilder;
import org.openvpms.billing.charge.ChargeItemBuilder;
import org.openvpms.billing.charge.ChargeItems;
import org.openvpms.billing.charge.ChargeObjects;
import org.openvpms.billing.exception.BillingException;
import org.openvpms.billing.internal.i18n.BillingMessages;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.product.Template;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of {@link ChargeBuilder}.
 *
 * @author Tim Anderson
 */
public abstract class ChargeBuilderImpl<C extends Charge<I>, I extends ChargeItem,
        CB extends ChargeBuilder<C, I, CB, IB>, IB extends ChargeItemBuilder<C, I, CB, IB>>
        extends AbstractChargeBuilder
        implements ChargeBuilder<C, I, CB, IB> {

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The collected item builders.
     */
    private final Set<IB> builders = new LinkedHashSet<>();

    /**
     * The identities.
     */
    private final List<ActIdentity> identities = new ArrayList<>();

    /**
     * The current charge. This may be loaded prior to building in order to determine the practice location
     * to use when expanding templates.
     */
    private IMObjectBean currentCharge;

    /**
     * The location.
     */
    private Location location;

    /**
     * The customer.
     */
    private Customer customer;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Next act relationship sequence.
     */
    private int nextSequence;

    /**
     * Template expansion group.
     */
    private int templateGroup;

    /**
     * The template expander.
     */
    private TemplateExpander templateExpander;

    /**
     * Constructs a {@link ChargeBuilderImpl}.
     *
     * @param archetype the charge archetype
     * @param services  the build services
     */
    public ChargeBuilderImpl(String archetype, BuilderServices services) {
        super(archetype, services);
        practice = services.getPricingContextFactory().getPractice();
        if (practice == null) {
            throw new IllegalStateException("No practice");
        }
    }

    /**
     * Returns the location.
     *
     * @return the location. May be {@code null}
     */
    @Override
    public Location getLocation() {
        return location;
    }

    /**
     * Sets the location.
     *
     * @param location the location
     * @return this
     */
    @Override
    public CB location(Location location) {
        if (templateExpander != null) {
            throw new IllegalStateException("Cannot change location once templates expanded");
        }
        this.location = location;
        return getThis();
    }

    /**
     * Returns the customer.
     *
     * @return the customer. May be {@code null}
     */
    @Override
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     * @return this
     */
    @Override
    public CB customer(Customer customer) {
        this.customer = customer;
        return getThis();
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        return clinician;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    @Override
    public CB clinician(User clinician) {
        this.clinician = clinician;
        return getThis();
    }

    /**
     * Adds an identity.
     * <p/>
     * The identity must be unique for the charge type.
     *
     * @param archetype the identity archetype
     * @param identity  the identity
     * @return this
     */
    @Override
    public CB addIdentity(String archetype, String identity) {
        ActIdentity id = create(archetype, ActIdentity.class);
        id.setIdentity(identity);
        identities.add(id);
        return getThis();
    }

    /**
     * Expands a template.
     * <p/>
     * All items will be added to the charge.
     *
     * @param template  the product template
     * @param patient   the patient
     * @param quantity  the template quantity
     * @param clinician the clinician
     * @return the charge items generated from the template
     */
    @Override
    public ChargeItems<C, I, CB, IB> expand(Template template, Patient patient, BigDecimal quantity, User clinician) {
        List<IB> builders = new ArrayList<>();
        Weight weight = patient.getWeight();
        Collection<ProductQuantity> expanded = getTemplateExpander().expand(template, weight, quantity);
        if (expanded.isEmpty()) {
            throw new BillingException(BillingMessages.templateExpansionGeneratedNoItems(template.getName(), weight));
        }
        for (ProductQuantity productQuantity : expanded) {
            IB builder = add(template, patient, productQuantity, clinician);
            builders.add(builder);
        }
        templateGroup++;
        return createChargeItems(builders);
    }

    /**
     * Returns the charge item builders, for new and updated items.
     *
     * @return the charge item builders
     */
    @Override
    public ChargeItems<C, I, CB, IB> getChangedItems() {
        return createChargeItems(new ArrayList<>(builders));
    }

    /**
     * Adds an item builder.
     *
     * @param builder the builder
     * @return this
     */
    public IB add(IB builder) {
        IB result;
        Product product = builder.getProduct();
        if (product instanceof Template) {
            ChargeItems<C, I, CB, IB> expanded = expand((Template) product, builder.getPatient(), builder.getQuantity(),
                                                        builder.getClinician());
            result = expanded.getItems().get(0);
        } else {
            builders.add(builder);
            result = builder;
        }
        return result;
    }

    /**
     * Builds the charge.
     *
     * @return the charge
     */
    @Override
    public C build() {
        return buildObjects().getCharge();
    }

    /**
     * Builds the charge, and returns the built objects.
     *
     * @return the built objects
     */
    @Override
    public ChargeObjects<C, I> buildObjects() {
        if (customer == null) {
            throw new BillingException(BillingMessages.valueRequired("customer"));
        }
        Date date = new Date();
        IMObjectBean bean = getCurrentCharge();
        FinancialAct charge = bean.getObject(FinancialAct.class);

        BuildContext context = createBuildContext(bean);

        build(charge, bean, date, context);

        saveInTransaction(context);
        reset();
        return createChargeObjects(context);
    }

    /**
     * Returns the current charge. If there is no current charge, {@link #init()} will be invoked.
     *
     * @return the current charge
     */
    protected IMObjectBean getCurrentCharge() {
        if (currentCharge == null) {
            currentCharge = init();
        }
        return currentCharge;
    }

    /**
     * Creates a {@link ChargeItems}.
     *
     * @param builders the item builders
     * @return the items
     */
    protected ChargeItems<C, I, CB, IB> createChargeItems(List<IB> builders) {
        return new ChargeItemsImpl<>(builders);
    }

    /**
     * Creates a {@link ChargeObjects}
     *
     * @param context the build context
     * @return a new charge state
     */
    protected abstract ChargeObjects<C, I> createChargeObjects(BuildContext context);

    /**
     * Saves the charge and related changes.
     * <p/>
     * This is invoked within a transaction.
     *
     * @param context the build context
     */
    protected void save(BuildContext context) {
        context.save();
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected CB getThis() {
        return (CB) this;
    }

    /**
     * Builds the charge.
     *
     * @param charge  the charge
     * @param bean    a bean wrapping the charge
     * @param date    the date for determining prices and discounts. For new charges, it is the charge date
     * @param context the build context
     */
    protected void build(FinancialAct charge, IMObjectBean bean, Date date, BuildContext context) {
        context.addChange(charge);
        if (charge.isNew()) {
            charge.setActivityStartTime(date);
            bean.setTarget("customer", customer);
            // don't change the location for existing charges
            bean.setTarget("location", location);
        }
        if (clinician != null) {
            bean.setTarget("clinician", clinician);
        }
        for (ActIdentity identity : identities) {
            checkIdentity(identity, charge);
            charge.addIdentity(identity);
        }

        List<Act> items = new ArrayList<>();
        for (IB itemBuilder : builders) {
            ChargeItemBuilderImpl<?, ?, ?, ?> builder = (ChargeItemBuilderImpl<?, ?, ?, ?>) itemBuilder;
            Act item = builder.build(date, context);
            ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
            relationship.setSequence(nextSequence++);
            item.addActRelationship(relationship);
            items.add(item);
        }
        BigDecimal total = charge.getTotal();
        BigDecimal tax = charge.getTaxAmount();
        ActCalculator calculator = new ActCalculator(getArchetypeService());
        total = total.add(calculator.sum(charge, items, "total"));
        tax = tax.add(calculator.sum(charge, items, "tax"));
        bean.setValue("amount", total);
        bean.setValue("tax", tax);
    }

    /**
     * Resets the builder.
     * <p/>
     * This clears any state that should not be retained across builds.
     */
    protected void reset() {
        currentCharge = null;
        templateExpander = null;
        builders.clear();
        identities.clear();
    }

    /**
     * Creates a charge context.
     *
     * @param charge the charge
     * @return the charge context
     */
    protected BuildContext createBuildContext(IMObjectBean charge) {
        return new BuildContext(charge, createPricingContext(), getArchetypeService());
    }

    /**
     * Returns the pricing context.
     *
     * @return the pricing context
     */
    protected PricingContext createPricingContext() {
        return getServices().getPricingContextFactory().createContext(practice, location);
    }

    /**
     * Determines practice location to use for charging.
     * <p/>
     * For saved charges, this is the location where the charge was saved, else it's the {@link #location(Location)}.
     *
     * @param charge the charge bean
     */
    private void initLocation(IMObjectBean charge) {
        if (!charge.getObject().isNew()) {
            Party existing = charge.getTarget("location", Party.class);
            if (existing != null) {
                location = getServices().getDomainService().create(existing, Location.class);
            }
        }
        if (location == null) {
            throw new BillingException(BillingMessages.valueRequired("location"));
        }
    }

    /**
     * Saves the charge.
     *
     * @param context the build context
     */
    private void saveInTransaction(BuildContext context) {
        TransactionTemplate template = new TransactionTemplate(getServices().getTransactionManager());
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                save(context);
            }
        });
    }

    /**
     * Adds an item expanded from a template.
     *
     * @param template        the template
     * @param patient         the patient
     * @param productQuantity the product and quantity
     * @param clinician       the clinician
     * @return the builder
     */
    @SuppressWarnings("unchecked")
    private IB add(Template template, Patient patient, ProductQuantity productQuantity, User clinician) {
        IB item = newItem();
        item.patient(patient)
                .product(productQuantity.getProduct())
                .quantity(productQuantity.getHighQuantity())
                .clinician(clinician)
                .add();
        ChargeItemBuilderImpl<C, I, CB, IB> builder = (ChargeItemBuilderImpl<C, I, CB, IB>) item;
        builder.template(template, productQuantity.getLowQuantity(), productQuantity.getPrint(), templateGroup);
        return item;
    }

    /**
     * Initialises the builder.
     *
     * @return the charge
     */
    private IMObjectBean init() {
        nextSequence = 0;
        templateGroup = 0;

        FinancialAct charge = getObject();
        IMObjectBean bean = getBean(charge);
        initLocation(bean);
        if (!charge.isNew()) {
            // determine the act relationship sequence
            RelatedIMObjects<Act, ActRelationship> items
                    = bean.getRelated("items", Act.class, ActRelationship.class).policy(
                    Policies.newPolicy(ActRelationship.class).orderBySequence().build());
            List<ActRelationship> relationships = items.getRelationships();
            if (!relationships.isEmpty()) {
                nextSequence = relationships.get(relationships.size() - 1).getSequence() + 1;
            }
            // determine the template expansion group
            int max = -1;
            for (Act item : items.getObjects()) {
                IMObjectBean itemBean = getBean(item);
                Participation template = itemBean.getObject("template", Participation.class);
                if (template != null) {
                    max = Math.max(max, getBean(template).getInt("group"));
                }
            }
            templateGroup = (max >= 0) ? max + 1 : 0;
        }
        return bean;
    }

    /**
     * Returns the template expander.
     *
     * @return the template expander
     */
    private TemplateExpander getTemplateExpander() {
        if (templateExpander == null) {
            if (customer == null) {
                throw new IllegalStateException("Customer must be set to expand templates");
            }
            getCurrentCharge();  // ensure the charge is created/loaded
            BuilderServices services = getServices();
            boolean useLocationProducts = services.getPricingContextFactory().useLocationProducts(practice);
            templateExpander = new TemplateExpander(useLocationProducts, practice, location, services.getStockRules(),
                                                    services.getProductRules(), services.getDomainService());
        }
        return templateExpander;
    }

    /**
     * Verifies an identity is unique to this charge.
     * <p/>
     * NOTE: the same identity can be applied to charges of different archetypes.
     *
     * @param identity the identity
     * @param charge   the charge
     */
    private void checkIdentity(ActIdentity identity, FinancialAct charge) {
        for (ActIdentity existing : charge.getIdentities()) {
            if (existing.getIdentity().equals(identity.getIdentity())
                && existing.getArchetype().equals(identity.getArchetype())) {
                throw new IllegalStateException("Duplicate identity: " + identity.getArchetype()
                                                + ":" + identity.getIdentity());
            }
        }
        ArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<FinancialAct> from = query.from(FinancialAct.class, charge.getArchetype());
        query.select(from.get("id"));
        Join<FinancialAct, IMObject> identities = from.join("identities", identity.getArchetype());
        identities.on(builder.equal(identities.get("identity"), identity.getIdentity()));
        Long id = service.createQuery(query).getFirstResult();
        if (id != null) {
            throw new IllegalStateException("Duplicate identity: " + identity.getArchetype()
                                            + ":" + identity.getIdentity() + " found on charge " + id);
        }
    }
}
