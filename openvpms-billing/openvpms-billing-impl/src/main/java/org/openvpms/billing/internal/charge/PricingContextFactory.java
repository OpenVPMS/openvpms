/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ServiceRatioService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.party.Party;

/**
 * A factory for {@link PricingContext} instances.
 *
 * @author Tim Anderson
 */
public class PricingContextFactory {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The practice rules.
     */
    private final PracticeRules practiceRules;

    /**
     * The price rules.
     */
    private final ProductPriceRules priceRules;

    /**
     * The discount rules.
     */
    private final DiscountRules discountRules;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The service ratio service.
     */
    private final ServiceRatioService serviceRatios;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs a {@link PricingContextFactory}.
     *
     * @param practiceService the practice service
     * @param practiceRules   the practice rules
     * @param locationRules   the location rules
     * @param priceRules      the price rules
     * @param discountRules   the discount rules
     * @param serviceRatios   the service ratio service
     * @param service         the archetype service
     */
    public PricingContextFactory(PracticeService practiceService, PracticeRules practiceRules,
                                 LocationRules locationRules, ProductPriceRules priceRules, DiscountRules discountRules,
                                 ServiceRatioService serviceRatios, IArchetypeService service) {
        this.practiceService = practiceService;
        this.practiceRules = practiceRules;
        this.priceRules = priceRules;
        this.locationRules = locationRules;
        this.discountRules = discountRules;
        this.serviceRatios = serviceRatios;
        this.service = service;
    }

    /**
     * Returns the practice.
     *
     * @return the practice, or {@code null} if there is no practice
     */
    public Party getPractice() {
        return practiceService.getPractice();
    }

    /**
     * Determines if products should be filtered by location.
     *
     * @param practice the practice
     * @return {@code true} if products should be filtered by location
     */
    public boolean useLocationProducts(Party practice) {
        return practiceRules.useLocationProducts(practice);
    }

    /**
     * Creates a new {@link PricingContext}.
     *
     * @param practice the practice
     * @param location the practice location
     * @return a new pricing context
     */
    public PricingContext createContext(Party practice, Party location) {
        return new PricingContext(practice, location, practiceRules, priceRules, discountRules, locationRules,
                                  serviceRatios, service);
    }
}
