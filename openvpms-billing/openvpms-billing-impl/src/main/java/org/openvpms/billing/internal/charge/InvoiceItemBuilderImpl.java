/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.billing.charge.InvoiceItemBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.product.Batch;
import org.openvpms.domain.product.Medication;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of {@link InvoiceItemBuilder}.
 *
 * @author Tim Anderson
 */
public class InvoiceItemBuilderImpl
        extends ChargeItemBuilderImpl<Invoice, InvoiceItem, InvoiceBuilder, InvoiceItemBuilder>
        implements InvoiceItemBuilder {

    /**
     * The product batch.
     */
    private Batch batch;

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Product node name.
     */
    private static final String PRODUCT = "product";

    /**
     * Quantity node name.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Batch node name.
     */
    private static final String BATCH = "batch";

    /**
     * Clinician node name.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Dispensing node name.
     */
    private static final String DISPENSING = "dispensing";

    /**
     * Invoice item node name.
     */
    private static final String INVOICE_ITEM = "invoiceItem";

    /**
     * Batch expiry node name.
     */
    private static final String BATCH_EXPIRY = "endTime";

    /**
     * Constructs an {@link InvoiceItemBuilderImpl}.
     *
     * @param parent   the parent builder
     * @param services the build services
     */
    public InvoiceItemBuilderImpl(InvoiceBuilderImpl parent, BuilderServices services) {
        super(CustomerAccountArchetypes.INVOICE_ITEM, parent, services);
    }

    /**
     * Returns the product batch.
     *
     * @return the product batch. May be {@code null}
     */
    @Override
    public Batch getBatch() {
        return batch;
    }

    /**
     * Sets the product batch.
     *
     * @param batch the batch
     * @return this
     */
    @Override
    public InvoiceItemBuilder batch(Batch batch) {
        this.batch = batch;
        return this;
    }

    /**
     * Builds the charge item.
     *
     * @param act     the charge item act
     * @param bean    a bean wrapping the act
     * @param date    the date for determining prices and discounts
     * @param context the build context
     */
    @Override
    protected void build(FinancialAct act, IMObjectBean bean, Date date, BuildContext context) {
        super.build(act, bean, date, context);
        Product product = getProduct();
        if (batch != null) {
            bean.setTarget(BATCH, batch);
        }
        InvoiceContext invoiceContext = (InvoiceContext) context;
        if (product instanceof Medication) {
            buildMedication(act, bean, (Medication) product, date, invoiceContext);
        } else {
            invoiceContext.addRecord(act, date);
        }
        generateReminders(bean, date);
        generateAlerts(bean, date);
    }

    /**
     * Returns the parent builder.
     *
     * @return the parent builder
     */
    @Override
    protected InvoiceBuilderImpl getParent() {
        return (InvoiceBuilderImpl) super.getParent();
    }

    /**
     * Builds an <em>act.patientMedication</em> record for a medication product. This is linked to the invoice item.
     *
     * @param act            the invoice item act
     * @param bean           a bean wrapping the act
     * @param product        the medication product
     * @param date           the date for determining prices and discounts
     * @param invoiceContext the build context
     */
    private void buildMedication(FinancialAct act, IMObjectBean bean, Medication product, Date date,
                                 InvoiceContext invoiceContext) {
        Act medication = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        IMObjectBean medicationBean = getBean(medication);
        medicationBean.setTarget(PATIENT, getPatient());
        medicationBean.setTarget(PRODUCT, product);
        if (batch != null) {
            medicationBean.setTarget(BATCH, batch);
            OffsetDateTime expiry = batch.getExpiry();
            medicationBean.setValue(BATCH_EXPIRY, DateRules.toDate(expiry));
        }
        medicationBean.setValue(QUANTITY, getQuantity());
        medicationBean.setValue("label", product.getLabelInstructions());
        User clinician = getClinician();
        if (clinician != null) {
            medicationBean.setTarget(CLINICIAN, clinician);
        }
        bean.addTarget(DISPENSING, medication, INVOICE_ITEM);
        invoiceContext.addRecord(medication, act, date);
    }

    /**
     * Adds reminder builders for each reminder type associated with the product.
     * <p/>
     * The actual building of reminders is deferred until the invoice is built, as duplicates need to be removed.
     *
     * @param bean a bean wrapping the act
     * @param date the date for determining prices and discounts
     */
    private void generateReminders(IMObjectBean bean, Date date) {
        Map<Entity, Relationship> reminderTypes = getReminderTypes();
        for (Map.Entry<Entity, Relationship> entry : reminderTypes.entrySet()) {
            ReminderBuilder builder = new ReminderBuilder(getServices()).patient(getPatient())
                    .reminderType(entry.getKey(), entry.getValue())
                    .date(date)
                    .product(getProduct())
                    .clinician(getClinician())
                    .invoiceItem(bean);
            getParent().addReminder(builder);
        }
    }

    /**
     * Returns reminder types and their associated product relationship, for the product and patient species.
     *
     * @return a map of <em>entity.reminderType</em> to <em>entityLink.productReminder</em>
     */
    private Map<Entity, Relationship> getReminderTypes() {
        ReminderRules rules = getServices().getReminderRules();
        String species = getPatient().getSpeciesCode();
        return rules.getReminderTypes(getProduct(), species);
    }

    /**
     * Adds alert builders for each alert type associated with the product.
     * <p/>
     * The actual building of alert is deferred until the invoice is built, as duplicates need to be removed.
     *
     * @param bean a bean wrapping the act
     * @param date the date for determining prices and discounts
     */
    private void generateAlerts(IMObjectBean bean, Date date) {
        for (Entity alertType : getAlertTypes()) {
            AlertBuilder builder = new AlertBuilder(getServices()).patient(getPatient())
                    .alertType(alertType)
                    .date(date)
                    .product(getProduct())
                    .clinician(getClinician())
                    .invoiceItem(bean);
            getParent().addAlert(builder);
        }
    }

    /**
     * Returns alert types for the product.
     *
     * @return the alert types
     */
    private List<Entity> getAlertTypes() {
        IMObjectBean bean = getBean(getProduct());
        return bean.hasNode("alerts") ? bean.getTargets("alerts", Entity.class) : Collections.emptyList();
    }

}