/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.patient.Patient;

import java.util.Date;

/**
 * Builds reminders.
 *
 * @author Tim Anderson
 */
class ReminderBuilder {

    /**
     * The builder service.
     */
    private final BuilderServices services;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The reminder type.
     */
    private Entity reminderType;

    /**
     * The product-reminder relationship.
     */
    private Relationship productReminder;

    /**
     * The product.
     */
    private Product product;

    /**
     * The date.
     */
    private Date date;

    /**
     * The due date.
     */
    private Date dueDate;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The invoice item.
     */
    private IMObjectBean invoiceItem;

    /**
     * Constructs a {@link ReminderBuilder}.
     *
     * @param services the builder services
     */
    public ReminderBuilder(BuilderServices services) {
        this.services = services;
    }

    /**
     * Returns the patient.
     *
     * @return the patient
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    public ReminderBuilder patient(Patient patient) {
        this.patient = patient;
        return this;
    }

    /**
     * Returns the reminder type.
     *
     * @return the reminder type
     */
    public Entity getReminderType() {
        return reminderType;
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType    the reminder type
     * @param productReminder the product-reminder relationship
     * @return this
     */
    public ReminderBuilder reminderType(Entity reminderType, Relationship productReminder) {
        this.reminderType = reminderType;
        this.productReminder = productReminder;
        return this;
    }

    /**
     * Sets the date.
     * <p/>
     * The reminder due date will be calculated from this
     *
     * @param date the date
     * @return the date
     */
    public ReminderBuilder date(Date date) {
        this.date = date;
        return this;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public ReminderBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician
     * @return this
     */
    public ReminderBuilder clinician(User clinician) {
        this.clinician = clinician;
        return this;
    }

    /**
     * Sets the invoice item.
     *
     * @param bean a bean wrapping the invoice item
     * @return this
     */
    public ReminderBuilder invoiceItem(IMObjectBean bean) {
        this.invoiceItem = bean;
        return this;
    }

    /**
     * Builds a reminder, adding it to the build context.
     *
     * @param context the build context
     */
    public void build(InvoiceContext context) {
        ArchetypeService service = services.getArchetypeService();
        Act reminder = service.create(ReminderArchetypes.REMINDER, Act.class);
        IMObjectBean reminderBean = service.getBean(reminder);
        reminderBean.setValue("initialTime", date);
        reminderBean.setTarget("patient", patient);
        reminderBean.setTarget("reminderType", reminderType);
        reminderBean.setTarget("product", product);
        reminderBean.setTarget("clinician", clinician);
        Date dueDate = getDueDate();
        reminder.setActivityEndTime(dueDate);

        ReminderRules rules = services.getReminderRules();
        Date next = rules.getNextReminderDate(dueDate, reminderType, 0);
        if (next == null) {
            next = dueDate;
        }
        reminder.setActivityStartTime(next);

        invoiceItem.addTarget("reminders", reminder, "invoiceItem");
        context.addReminder(reminder);
    }

    /**
     * Returns the reminder due date.
     *
     * @return the reminder due date
     */
    public Date getDueDate() {
        if (dueDate == null) {
            dueDate = services.getReminderRules().calculateProductReminderDueDate(date, productReminder);
        }
        return dueDate;
    }
}