/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Build state to be shared among builders.
 *
 * @author Tim Anderson
 */
class BuildContext {

    /**
     * The charge being built.
     */
    private final IMObjectBean charge;

    /**
     * Objects that need to be saved, in the order they were added.
     */
    private final Set<IMObject> changes = new LinkedHashSet<>();

    /**
     * The pricing context.
     */
    private final PricingContext pricingContext;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link BuildContext}.
     *
     * @param charge         the charge being built
     * @param pricingContext the pricing context
     * @param service        the archetype service
     */
    public BuildContext(IMObjectBean charge, PricingContext pricingContext, ArchetypeService service) {
        this.charge = charge;
        this.pricingContext = pricingContext;
        this.service = service;
    }

    /**
     * Returns the charge being built.
     *
     * @return the charge
     */
    public IMObjectBean getCharge() {
        return charge;
    }

    /**
     * Adds an object that has changed.
     *
     * @param object the object
     */
    public void addChange(IMObject object) {
        changes.add(object);
    }

    /**
     * Saves changes.
     */
    public void save() {
        service.save(changes);
    }

    /**
     * Returns the pricing context.
     *
     * @return the pricing context
     */
    public PricingContext getPricingContext() {
        return pricingContext;
    }

    /**
     * Returns all changed objects of the specified archetype.
     *
     * @param archetype the archetype
     * @return the changed objects
     */
    public List<IMObject> getChanges(String archetype) {
        return changes.stream().filter(object -> object.isA(archetype)).collect(Collectors.toList());
    }

    /**
     * Returns the archetype service.
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }
}
