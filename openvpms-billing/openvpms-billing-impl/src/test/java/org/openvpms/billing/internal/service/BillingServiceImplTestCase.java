/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.service;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.billing.internal.charge.BuilderServices;
import org.openvpms.billing.service.BillingService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Tests the {@link BillingServiceImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class BillingServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The builder service.
     */
    @Autowired
    private BuilderServices builderServices;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    /**
     * Tests the {@link BillingServiceImpl#getInvoice(long)}, {@link BillingServiceImpl#getInvoice(String, String)}
     * and {@link BillingServiceImpl#getInvoiceId(String, String)} methods.
     */
    @Test
    public void testGetInvoice() {
        BillingService billingService = new BillingServiceImpl(builderServices);

        String identity = UUID.randomUUID().toString();
        FinancialAct act = accountFactory.newInvoice()
                .customer(customerFactory.createCustomer())
                .location(practiceFactory.createLocation())
                .addIdentity("actIdentity.syncTest", identity)
                .build();
        Invoice invoice1 = billingService.getInvoice(act.getId());
        assertNotNull(invoice1);
        assertEquals(act.getId(), invoice1.getId());

        Invoice invoice2 = billingService.getInvoice("actIdentity.syncTest", identity);
        assertNotNull(invoice2);
        assertEquals(act.getId(), invoice2.getId());

        assertEquals(invoice1, invoice2);

        long invoiceId = billingService.getInvoiceId("actIdentity.syncTest", identity);
        assertEquals(act.getId(), invoiceId);

        assertEquals(-1, billingService.getInvoiceId("actIdentity.syncTest", "badid"));
    }

    /**
     * Tests the {@link BillingServiceImpl#invoice(Customer, Patient, Product, BigDecimal, Location, User)} method.
     */
    @Test
    public void testInvoice() {
        practiceFactory.newPractice()
                .addTaxType(BigDecimal.TEN)
                .serviceUser()
                .build();
        BillingService billingService = new BillingServiceImpl(builderServices);

        Customer customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient = domainService.create(patientFactory.createPatient(customer), Patient.class);
        Product product1 = productFactory.newMedication().fixedPrice(10).build();
        Product product2 = productFactory.newService().unitPrice(5).build();
        Location location = domainService.create(practiceFactory.createLocation(), Location.class);
        User clinician = userFactory.createClinician();

        Invoice invoice1 = billingService.invoice(customer, patient, product1, BigDecimal.ONE, location, clinician);
        checkEquals(0, invoice1.getDiscount());
        checkEquals(0, invoice1.getDiscountTax());
        checkEquals(11, invoice1.getTotal());
        checkEquals(1, invoice1.getTotalTax());
        assertFalse(invoice1.isFinalised());

        // verify invoicing again adds the charge to the same invoice
        Invoice invoice2 = billingService.invoice(customer, patient, product2, BigDecimal.valueOf(2), location, null);
        assertEquals(invoice1.getId(), invoice2.getId());
        checkEquals(0, invoice2.getDiscount());
        checkEquals(0, invoice2.getDiscountTax());
        checkEquals(22, invoice2.getTotal());
        checkEquals(2, invoice2.getTotalTax());
    }

    /**
     * Verifies invoices can be created using {@link BillingServiceImpl#getInvoiceBuilder()}.
     */
    @Test
    public void testGetInvoiceBuilder() {
        practiceFactory.newPractice()
                .addTaxType(BigDecimal.TEN)
                .serviceUser()
                .build();
        BillingService billingService = new BillingServiceImpl(builderServices);

        Customer customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient1 = domainService.create(patientFactory.createPatient(customer), Patient.class);
        Patient patient2 = domainService.create(patientFactory.createPatient(customer), Patient.class);
        Product product1 = productFactory.newMedication().unitPrice(10).build();
        Product product2 = productFactory.newService().unitPrice(5).build();
        Location location = domainService.create(practiceFactory.createLocation(), Location.class);
        User clinician = userFactory.createClinician();

        Invoice invoice = billingService.getInvoiceBuilder()
                .customer(customer)
                .location(location)
                .clinician(clinician)
                .newItem().patient(patient1).product(product1).quantity(1).clinician(clinician).add()
                .newItem().patient(patient2).product(product2).quantity(2).clinician(clinician).add()
                .build();
        checkEquals(22, invoice.getTotal());
        checkEquals(2, invoice.getTotalTax());
    }
}
