/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.internal.charge;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderStatus;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemVerifier;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.billing.charge.InvoiceObjects;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.product.Batch;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static java.math.BigDecimal.TEN;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.openvpms.archetype.rules.util.DateUnits.MONTHS;
import static org.openvpms.archetype.rules.util.DateUnits.YEARS;

/**
 * Tests the {@link InvoiceBuilderImpl} class.
 *
 * @author Tim Anderson
 */
public class InvoiceBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The builder service.
     */
    @Autowired
    private BuilderServices builderServices;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * The stock rules.
     */
    @Autowired
    private StockRules stockRules;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Test location.
     */
    private Party location;

    /**
     * Test stock location.
     */
    private Party stockLocation;

    /**
     * Test customer.
     */
    private Customer customer;

    /**
     * First test patient.
     */
    private Patient patient1;

    /**
     * The service user.
     */
    private User serviceUser;

    /**
     * The tax type, used to determine taxes on products.
     */
    private Lookup taxType;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        serviceUser = userFactory.createUser();
        taxType = lookupFactory.createTaxType(BigDecimal.TEN);
        practiceFactory.newPractice()
                .currency("AUD")
                .addTaxType(taxType)
                .minimumQuantities(true)
                .minimumQuantitiesOverride(null)  // no-one can override minimum quantities
                .serviceUser(serviceUser)
                .build();

        stockLocation = practiceFactory.createStockLocation();

        // create a location with stock control enabled
        location = practiceFactory.newLocation()
                .stockControl(true)
                .stockLocation(stockLocation)
                .build();

        customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        patient1 = domainService.create(patientFactory.createPatient(customer), Patient.class);
    }

    /**
     * Verifies that an invoice can be created without any line items
     */
    @Test
    public void testCreateEmptyInvoice() {
        InvoiceBuilderImpl builder = new InvoiceBuilderImpl(builderServices);
        Invoice invoice = builder.customer(domainService.create(customerFactory.createCustomer(), Customer.class))
                .location(domainService.create(location, Location.class))
                .build();
        assertFalse(invoice.isFinalised());
        checkEquals(invoice.getTotal(), BigDecimal.ZERO);
        checkEquals(invoice.getTotalTax(), BigDecimal.ZERO);
    }

    /**
     * Tests creation of an invoice.
     */
    @Test
    public void testCreateInvoice() {
        Product product1 = productFactory.newMedication()
                .labelInstructions("a label")
                .newFixedPrice().cost("0.1").price(1).add()
                .newUnitPrice().cost("0.2").price(2).add()
                .build();
        Product product2 = productFactory.newMerchandise()
                .newFixedPrice().cost("0.2").price(2).add()
                .newUnitPrice().cost("0.3").price(3).add()
                .build();
        Product product3 = productFactory.newService()
                .newFixedPrice().cost("0.3").price(3).add()
                .newUnitPrice().cost("0.4").price(4).add()
                .build();
        User clinician = userFactory.createClinician();

        Entity batch = productFactory.newBatch()
                .batchNo("1234567")
                .product(product1)
                .expiryDate(DateRules.getTomorrow())
                .addStockLocations(stockLocation)
                .build();

        final int product1Stock = 100;
        final int product2Stock = 50;
        initStock(product1, stockLocation, product1Stock);
        initStock(product2, stockLocation, product2Stock);

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .clinician(clinician)
                .newItem()
                .patient(patient1)
                .product(product1)
                .batch(domainService.create(batch, Batch.class))
                .quantity(1)
                .clinician(clinician)
                .add()
                .newItem()
                .patient(patient1)
                .product(product2)
                .quantity(2)
                .clinician(clinician)
                .add()
                .newItem()
                .patient(patient1)
                .product(product3)
                .quantity(3)
                .clinician(clinician)
                .add()
                .buildObjects();
        Invoice invoice = objects.getCharge();

        assertEquals(customer, invoice.getCustomer());
        assertEquals(clinician, invoice.getClinician());
        checkEquals(0, invoice.getDiscount());
        checkEquals(0, invoice.getDiscountTax());
        checkEquals("28.60", invoice.getTotal());
        checkEquals("2.60", invoice.getTotalTax());
        assertEquals(3, invoice.getItems().size());

        assertEquals(1, objects.getVisits().size());
        Visit visit = objects.getVisit(patient1).get();

        // verify stock has updated
        checkStock(product1, stockLocation, product1Stock - 1);
        checkStock(product2, stockLocation, product2Stock - 2);
        checkStock(product3, stockLocation, 0);

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(3, items.getRelationships().size());
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.patient(patient1)
                .createdBy(serviceUser);

        verifier.product(product1)
                .batch(batch)
                .quantity(1)
                .fixedCost("0.1").fixedPrice("1.10")
                .unitCost("0.2").unitPrice("2.20")
                .total("3.30").tax("0.30")
                .clinician(clinician)
                .medication("a label", DateRules.getTomorrow())
                .visit(visit)
                .verify(items, 0);
        verifier.product(product2)
                .batch((Reference) null)
                .quantity(2)
                .fixedCost("0.2").fixedPrice("2.20")
                .unitCost("0.3").unitPrice("3.30")
                .total("8.80").tax("0.80")
                .clinician(clinician)
                .medication(null, null)
                .visit(visit)
                .verify(items, 1);
        verifier.product(product3)
                .batch((Reference) null)
                .quantity(3)
                .fixedCost("0.3").fixedPrice("3.30")
                .unitCost("0.4").unitPrice("4.40")
                .total("16.50").tax("1.50")
                .clinician(clinician)
                .medication(null, null)
                .visit(visit)
                .verify(items, 2);
    }

    /**
     * Verifies that an existing IN_PROGRESS invoice is added to if one exists.
     */
    @Test
    public void testUpdateInvoice() {
        Product product1 = productFactory.newMedication()
                .fixedPrice(1)
                .unitPrice(1)
                .build();
        Product product2 = productFactory.newMedication()
                .fixedPrice(10)
                .build();

        InvoiceBuilder builder1 = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects1 = builder1.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product1)
                .quantity(2)
                .add()
                .buildObjects();
        Invoice invoice1 = objects1.getCharge();
        checkEquals("3.30", invoice1.getTotal());

        Visit visit = objects1.getVisit(patient1).get();

        // verifies that the invoice is updated when charging more items to the customer
        InvoiceBuilder builder2 = new InvoiceBuilderImpl(builderServices);
        Invoice invoice2 = builder2.customer(customer)
                .newItem()
                .patient(patient1)
                .product(product2)
                .quantity(1)
                .add()
                .build();

        assertEquals(invoice1.getId(), invoice2.getId());
        checkEquals("14.30", invoice2.getTotal());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice2);
        assertEquals(2, items.getRelationships().size());
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.patient(patient1)
                .createdBy(serviceUser)
                .visit(visit);

        verifier.product(product1)
                .quantity(2)
                .fixedPrice("1.10")
                .unitPrice("1.10")
                .total("3.30").tax("0.30")
                .verify(items, 0);
        verifier.product(product2)
                .quantity(1)
                .unitPrice(0)
                .fixedPrice(11)
                .total(11).tax(1)
                .verify(items, 1);

        // now post the invoice
        post(invoice2);

        // items should now go to a new invoice
        InvoiceBuilder builder3 = new InvoiceBuilderImpl(builderServices);
        Invoice invoice3 = builder3.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product1)
                .quantity(1)
                .add()
                .build();

        assertNotEquals(invoice2.getId(), invoice3.getId());
        checkEquals("2.20", invoice3.getTotal());
    }

    /**
     * Verifies that when a template is added to an existing invoice, the template expansion group is incremented.
     */
    @Test
    public void testUpdateInvoiceWithTemplate() {
        Product product1 = productFactory.newService()
                .fixedPrice(1)
                .unitPrice(1)
                .build();
        Product product2 = productFactory.newMerchandise()
                .fixedPrice(10)
                .build();
        Product product3 = productFactory.newMerchandise()
                .fixedPrice(5)
                .build();
        Product templateA = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .newInclude().product(product2).quantity(1).add()
                .build();
        Product templateB = productFactory.newTemplate()
                .newInclude().product(product3).quantity(1).add()
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects1 = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(templateA)
                .quantity(1)
                .add()
                .buildObjects();
        Invoice invoice1 = objects1.getCharge();
        Visit visit = objects1.getVisit(patient1).get();

        InvoiceBuilder builder2 = new InvoiceBuilderImpl(builderServices);
        Invoice invoice2 = builder2.customer(customer)
                .newItem()
                .patient(patient1)
                .product(templateB)
                .quantity(1)
                .add()
                .build();

        // verify invoice1 has been updated
        assertEquals(invoice1.getId(), invoice2.getId());
        checkEquals("18.70", invoice2.getTotal());
        checkEquals("1.70", invoice2.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice2);
        assertEquals(3, items.getRelationships().size());
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.patient(patient1)
                .createdBy(serviceUser)
                .visit(visit);

        verifier.product(product1)
                .quantity(1).minQuantity(1)
                .fixedPrice("1.10")
                .unitPrice("1.10")
                .total("2.20").tax("0.20")
                .template(templateA)
                .group(0)
                .verify(items, 0);
        verifier.product(product2)
                .quantity(1).minQuantity(1)
                .unitPrice(0)
                .fixedPrice(11)
                .total(11).tax(1)
                .template(templateA)
                .group(0)
                .verify(items, 1);
        verifier.product(product3)
                .quantity(1).minQuantity(1)
                .unitPrice(0)
                .fixedPrice("5.50")
                .total("5.50").tax("0.5")
                .template(templateB)
                .group(1) // different template expansion group
                .verify(items, 2);
    }

    /**
     * Verifies templates can be expanded.
     */
    @Test
    public void testTemplateExpansion() {
        patientFactory.createWeight(patient1, new BigDecimal("4.2"));

        Product product1 = productFactory.newMedication()
                .unitPrice(1)
                .concentration(1)
                .newDose()
                .minWeight(0)
                .maxWeight(10)
                .rate(1)
                .quantity(1)
                .add()
                .build();
        Product product2 = productFactory.newMerchandise()
                .unitPrice(2)
                .build();
        Product product3 = productFactory.newService()
                .unitPrice(3)
                .build();
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .newInclude().product(product2).quantity(2).add()
                .newInclude().product(product3).quantity(3).add()
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(template)
                .quantity(1)
                .add()
                .buildObjects();
        Invoice invoice = objects.getCharge();

        assertEquals(customer, invoice.getCustomer());
        assertEquals(3, invoice.getItems().size());
        checkEquals(0, invoice.getDiscount());
        checkEquals(0, invoice.getDiscountTax());
        checkEquals("15.40", invoice.getTotal());
        checkEquals("1.40", invoice.getTotalTax());

        // verify the items match that expected
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(3, items.getRelationships().size());
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        Visit visit = objects.getVisit(patient1).get();
        verifier.patient(patient1)
                .createdBy(serviceUser)
                .visit(visit);
        verifier.product(product1).template(template).group(0).quantity(1).minQuantity(1)
                .unitPrice("1.10").total("1.10").tax("0.10")
                .verify(items, 0);
        verifier.product(product2).template(template).group(0).quantity(2).minQuantity(2)
                .unitPrice("2.20").total("4.40").tax("0.40")
                .verify(items, 1);
        verifier.product(product3).template(template).group(0).quantity(3).minQuantity(3)
                .unitPrice("3.30").total("9.90").tax("0.90")
                .verify(items, 2);
    }

    /**
     * Verifies that when a template expands, any visit notes are added to the associated patient's history.
     */
    @Test
    public void testTemplateVisitNotes() {
        Patient patient2 = domainService.create(patientFactory.createPatient(customer), Patient.class);

        Product productA = productFactory.newMedication().fixedPrice("0.91").build();
        Product productB = productFactory.newMedication().fixedPrice("0.91").build();
        Product template1 = productFactory.newTemplate()
                .visitNote("template 1 notes")
                .newInclude().product(productA).highQuantity(1).add()
                .newInclude().product(productB).highQuantity(1).add()
                .build();

        Product template2 = productFactory.newTemplate()
                .visitNote("template 2 notes")
                .newInclude().product(productA).highQuantity(1).add()
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem().patient(patient1).product(template1).quantity(1).add()
                .newItem().patient(patient2).product(template2).quantity(1).add()
                .buildObjects();
        Invoice invoice = objects.getCharge();
        Visit visit1 = objects.getVisit(patient1).get();
        Visit visit2 = objects.getVisit(patient2).get();
        assertEquals(2, objects.getVisits().size());

        // verify that there are 3 items in the invoice, but only two notes, one for each template
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(3, items.getRelationships().size());

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser);

        verifier.patient(patient1).product(productA).template(template1).group(0).quantity(1).minQuantity(1)
                .fixedPrice(1).tax("0.091").total(1)
                .visit(visit1)
                .verify(items, 0);
        verifier.patient(patient1).product(productB).template(template1).group(0).quantity(1).minQuantity(1)
                .fixedPrice(1).tax("0.091").total(1)
                .visit(visit1)
                .verify(items, 1);
        verifier.patient(patient2).product(productA).template(template2).group(1).quantity(1).minQuantity(1)
                .fixedPrice(1).tax("0.091").total(1)
                .visit(visit2)
                .verify(items, 2);

        checkEventNote(visit1, patient1, "template 1 notes");
        checkEventNote(visit2, patient2, "template 2 notes");
    }

    /**
     * Verifies that the currency min price is used.
     */
    @Test
    public void testMinPrice() {
        // Update the practice currency to have a minimum price. Fixed and unit prices will round to multiples of this.
        lookupFactory.newCurrency()
                .code("AUD")
                .minPrice(new BigDecimal("0.20"))  // set a minimum price for calculated prices
                .build();

        Product product1 = productFactory.newMedication()
                .fixedPrice(1) // 1.1 tax inc rounded up to 1.2 with minPrice
                .unitPrice(2)  // 2.2 tax inc
                .build();
        Product product2 = productFactory.newMerchandise()
                .fixedPrice("0.45") // 0.495 tax inc rounded down to 0.40 with minPrice
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem().patient(patient1).product(product1).quantity(1).add()
                .newItem().patient(patient1).product(product2).quantity(1).add()
                .buildObjects();
        Invoice invoice = objects.getCharge();

        assertEquals(customer, invoice.getCustomer());
        assertEquals(2, invoice.getItems().size());
        checkEquals(0, invoice.getDiscount());
        checkEquals(0, invoice.getDiscountTax());
        checkEquals("3.80", invoice.getTotal());
        checkEquals("0.35", invoice.getTotalTax());

        Visit visit = objects.getVisit(patient1).get();

        // verify the items match that expected
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(2, items.getRelationships().size());
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.patient(patient1)
                .createdBy(serviceUser)
                .visit(visit);
        verifier.product(product1).quantity(1).fixedPrice("1.20").unitPrice("2.20").total("3.40").tax("0.309")
                .verify(items, 0);
        verifier.product(product2).quantity(1).fixedPrice("0.40").unitPrice(0).total("0.40").tax("0.036")
                .verify(items, 1);
    }

    /**
     * Verifies service ratios are applied.
     */
    @Test
    public void testServiceRatio() {
        Entity productType = productFactory.createProductType();
        Product product = productFactory.newService()
                .type(productType)
                .fixedPrice(1)
                .unitPrice(1)
                .build();

        // invoice without service ratios
        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        Invoice invoice1 = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(2)
                .add()
                .build();

        checkEquals("3.30", invoice1.getTotal());
        checkEquals("0.30", invoice1.getTotalTax());

        post(invoice1);  // post the invoice to guarantee a new one is created.

        // now add a service ratio
        practiceFactory.updateLocation(location)
                .addServiceRatio(productType, 2)
                .build();
        InvoiceObjects objects = builder.location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(2)
                .add()
                .buildObjects();
        Invoice invoice2 = objects.getCharge();

        // verify the service ratio is applied
        checkEquals("6.60", invoice2.getTotal());
        checkEquals("0.60", invoice2.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice2);
        assertEquals(1, items.getRelationships().size());

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1).product(product).quantity(2).fixedPrice("2.20").unitPrice("2.20")
                .serviceRatio(2).total("6.60").tax("0.60")
                .visit(objects.getVisit(patient1).get())
                .verify(items, 0);
    }

    /**
     * Verifies taxes aren't applied if a customer has an exemption.
     */
    @Test
    public void testTaxExemption() {
        Entity productType = productFactory.createProductType();
        Product product = productFactory.newService()
                .type(productType)
                .fixedPrice(1)
                .unitPrice(1)
                .build();

        // invoice without tax exemptions
        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        Invoice invoice1 = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(2)
                .add()
                .build();

        checkEquals("3.30", invoice1.getTotal());
        checkEquals("0.30", invoice1.getTotalTax());

        post(invoice1);  // post the invoice to guarantee a new one is created.

        // now add a tax exemption
        customerFactory.updateCustomer(customer)
                .addTaxExemptions(taxType.getCode())
                .build();
        InvoiceObjects objects = builder.location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(2)
                .add()
                .buildObjects();
        Invoice invoice2 = objects.getCharge();

        // verify there is no tax
        checkEquals(3, invoice2.getTotal());
        checkEquals(0, invoice2.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice2);
        assertEquals(1, items.getRelationships().size());

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1).product(product).quantity(2).fixedPrice(1).unitPrice(1)
                .total(3).tax(0)
                .visit(objects.getVisit(patient1).get())
                .verify(items, 0);
    }

    /**
     * Tests charging a product with a 10% discount.
     */
    @Test
    public void testDiscounts() {
        Entity discount = productFactory.newDiscount().percentage(TEN).discountFixedPrice(true).build();
        customerFactory.updateCustomer(customer)
                .addDiscounts(discount)
                .build();
        Product product = productFactory.newService()
                .newFixedPrice().cost(1).price("1.82").add()
                .newUnitPrice().cost(5).price("9.09").add()
                .addDiscounts(discount)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(2)
                .add()
                .buildObjects();
        Invoice invoice1 = objects.getCharge();
        checkEquals("2.20", invoice1.getDiscount());
        checkEquals("0.20", invoice1.getDiscountTax());
        checkEquals("19.80", invoice1.getTotal());
        checkEquals("1.80", invoice1.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice1);
        assertEquals(1, items.getRelationships().size());

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1).product(product).quantity(2)
                .fixedCost(1).fixedPrice(2)
                .unitCost(5).unitPrice(10)
                .discount("2.20")
                .total("19.80").tax("1.80")
                .visit(objects.getVisit(patient1).get())
                .verify(items, 0);
    }

    /**
     * Tests reminder generation.
     */
    @Test
    public void testReminders() {
        Patient patient2 = domainService.create(patientFactory.createPatient(customer), Patient.class);
        Entity reminderType1 = reminderFactory.newReminderType()
                .defaultInterval(1, MONTHS)
                .newCount().count(0).interval(-2, DateUnits.WEEKS).add()
                .newCount().count(1).interval(-1, DateUnits.WEEKS).add()
                .build();
        Entity reminderType2 = reminderFactory.newReminderType()
                .defaultInterval(1, MONTHS)
                .newCount().count(0).interval(-2, DateUnits.WEEKS).add()
                .newCount().count(1).interval(-1, DateUnits.WEEKS).add()
                .build();
        Product product1 = productFactory.newService()
                .fixedPrice(10)
                .addProductReminder(reminderType1, 1, YEARS)
                .addProductReminder(reminderType2, 2, MONTHS)
                .build();
        Product product2 = productFactory.newService()
                .fixedPrice(10)
                .addProductReminder(reminderType1, 6, MONTHS)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product1)
                .quantity(1)
                .add()
                .newItem()
                .patient(patient2)
                .product(product1)
                .quantity(1)
                .add()
                .newItem()
                .patient(patient1)
                .product(product2)
                .quantity(1)
                .add()
                .buildObjects();

        Invoice invoice = objects.getCharge();
        checkEquals(0, invoice.getDiscount());
        checkEquals(0, invoice.getDiscountTax());
        checkEquals(33, invoice.getTotal());
        checkEquals(3, invoice.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(3, items.getRelationships().size());

        Date invoiceDate = DateUtils.truncate(DateRules.toDate(invoice.getDate()), Calendar.SECOND); // ms not stored
        Date product1ReminderType1Due = DateRules.getDate(invoiceDate, 1, YEARS);
        Date product1ReminderType1NextDue = DateRules.getDate(product1ReminderType1Due, -2, DateUnits.WEEKS);
        Date product1ReminderType2Due = DateRules.getDate(invoiceDate, 2, MONTHS);
        Date product1ReminderType2NextDue = DateRules.getDate(product1ReminderType2Due, -2, DateUnits.WEEKS);
        Date product2ReminderType1Due = DateRules.getDate(invoiceDate, 6, MONTHS);
        Date product2ReminderType1NextDue = DateRules.getDate(product2ReminderType1Due, -2, DateUnits.WEEKS);

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .quantity(1)
                .fixedPrice(11)
                .total(11).tax(1);

        // patient1, product1 should have reminderType2. No reminderType1 should be present as this is overridden
        // by product2 which has a shorter due date.
        verifier.patient(patient1).product(product1)
                .visit(objects.getVisit(patient1).get())
                .addReminder(reminderType2, product1ReminderType2Due, product1ReminderType2NextDue, ReminderStatus.IN_PROGRESS)
                .verify(items, 0);

        // patient2, product1 should have reminderType1 and reminderType2
        verifier.patient(patient2).product(product1)
                .visit(objects.getVisit(patient2).get())
                .resetReminders()
                .addReminder(reminderType1, product1ReminderType1Due, product1ReminderType1NextDue, ReminderStatus.IN_PROGRESS)
                .addReminder(reminderType2, product1ReminderType2Due, product1ReminderType2NextDue, ReminderStatus.IN_PROGRESS)
                .verify(items, 1);

        // patient1, product2 should have reminderType1
        verifier.patient(patient1).product(product2)
                .visit(objects.getVisit(patient1).get())
                .resetReminders()
                .addReminder(reminderType1, product2ReminderType1Due, product2ReminderType1NextDue, ReminderStatus.IN_PROGRESS)
                .verify(items, 2);
    }

    /**
     * Verifies that only reminders for the patient species are generated.
     */
    @Test
    public void testSpeciesReminders() {
        assertEquals("CANINE", patient1.getSpeciesCode());
        Entity reminderType1 = reminderFactory.newReminderType()
                .defaultInterval(1, MONTHS)
                .build();
        Entity reminderType2 = reminderFactory.newReminderType()
                .addSpecies("CANINE", "FELINE")
                .defaultInterval(1, MONTHS)
                .build();
        Entity reminderType3 = reminderFactory.newReminderType()
                .addSpecies("FELINE")
                .defaultInterval(1, MONTHS)
                .build();
        Product product = productFactory.newService()
                .fixedPrice(10)
                .addProductReminder(reminderType1, 1, MONTHS)
                .addProductReminder(reminderType2, 2, MONTHS)
                .addProductReminder(reminderType3, 3, MONTHS)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(1)
                .add()
                .buildObjects();

        Invoice invoice = objects.getCharge();
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(1, items.getRelationships().size());

        Date invoiceDate = DateUtils.truncate(DateRules.toDate(invoice.getDate()), Calendar.SECOND); // ms not stored
        Date reminderType1Due = DateRules.getDate(invoiceDate, 1, MONTHS);
        Date reminderType2Due = DateRules.getDate(invoiceDate, 2, MONTHS);

        // verify reminders generated for reminderType1 and reminderType2 but not reminderType3
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1)
                .product(product)
                .quantity(1)
                .fixedPrice(11)
                .total(11).tax(1)
                .visit(objects.getVisit(patient1).get())
                .addReminder(reminderType1, reminderType1Due, reminderType1Due, ReminderStatus.IN_PROGRESS)
                .addReminder(reminderType2, reminderType2Due, reminderType2Due, ReminderStatus.IN_PROGRESS)
                .verify(items, 0);
    }

    /**
     * Verifies that existing reminders are completed if a product is used with the same reminder type.
     */
    @Test
    public void testMarkMatchingRemindersCompleted() {
        Entity reminderType1 = reminderFactory.createReminderType();
        Entity reminderType2 = reminderFactory.createReminderType();
        Act reminder1 = reminderFactory.createReminder(patient1, reminderType1);
        Act reminder2 = reminderFactory.createReminder(patient1, reminderType2);
        assertEquals(ReminderStatus.IN_PROGRESS, reminder1.getStatus());
        assertEquals(ReminderStatus.IN_PROGRESS, reminder2.getStatus());

        Product product = productFactory.newService()
                .fixedPrice(10)
                .addProductReminder(reminderType1, 1, MONTHS)
                .addProductReminder(reminderType2, 1, MONTHS)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(1)
                .add()
                .buildObjects();

        Invoice invoice = objects.getCharge();
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(1, items.getRelationships().size());

        Date invoiceDate = DateUtils.truncate(DateRules.toDate(invoice.getDate()), Calendar.SECOND); // ms not stored
        Date dueDate = DateRules.getDate(invoiceDate, 1, MONTHS);

        // verify new reminders have been created, with IN_PROGRESS status
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1)
                .product(product)
                .quantity(1)
                .fixedPrice(11)
                .total(11).tax(1)
                .visit(objects.getVisit(patient1).get())
                .addReminder(reminderType1, dueDate, dueDate, ReminderStatus.IN_PROGRESS)
                .addReminder(reminderType2, dueDate, dueDate, ReminderStatus.IN_PROGRESS)
                .verify(items, 0);


        // verify the existing reminders have been marked COMPLETED
        assertEquals(ReminderStatus.COMPLETED, get(reminder1).getStatus());
        assertEquals(ReminderStatus.COMPLETED, get(reminder2).getStatus());
    }

    /**
     * Tests alert generation.
     */
    @Test
    public void testAlerts() {
        Patient patient2 = domainService.create(patientFactory.createPatient(customer), Patient.class);
        Entity alertTypeA = patientFactory.newAlertType()
                .reason("reason A")
                .duration(1, MONTHS)
                .build();
        Entity alertTypeB = patientFactory.newAlertType()
                .reason("reason B")
                .build();
        Product product1 = productFactory.newService()
                .fixedPrice(10)
                .addAlertTypes(alertTypeA, alertTypeB)
                .build();
        Product product2 = productFactory.newService()
                .fixedPrice(10)
                .addAlertTypes(alertTypeA)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product1)
                .quantity(1)
                .add()
                .newItem()
                .patient(patient2)
                .product(product2)
                .quantity(1)
                .add()
                .newItem()
                .patient(patient1)
                .product(product2)
                .quantity(1)
                .add()
                .buildObjects();

        Invoice invoice = objects.getCharge();
        checkEquals(0, invoice.getDiscount());
        checkEquals(0, invoice.getDiscountTax());
        checkEquals(33, invoice.getTotal());
        checkEquals(3, invoice.getTotalTax());

        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(3, items.getRelationships().size());

        Date invoiceDate = DateUtils.truncate(DateRules.toDate(invoice.getDate()), Calendar.SECOND); // ms not stored
        Date alertTypeAEndDate = DateRules.getDate(invoiceDate, 1, MONTHS);

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .quantity(1)
                .fixedPrice(11)
                .total(11).tax(1);

        // patient1, product1 should have alertTypeA, alertTypeB
        verifier.patient(patient1).product(product1)
                .visit(objects.getVisit(patient1).get())
                .addAlert(alertTypeA, alertTypeAEndDate, ActStatus.IN_PROGRESS)
                .addAlert(alertTypeB, null, ActStatus.IN_PROGRESS)
                .verify(items, 0);

        // patient2, product2 should have alertTypeA
        verifier.patient(patient2).product(product2)
                .visit(objects.getVisit(patient2).get())
                .resetAlerts()
                .addAlert(alertTypeA, alertTypeAEndDate, ActStatus.IN_PROGRESS)
                .verify(items, 1);

        // patient1, product2 should have no alert, as alertTypeA is associated with the first charge
        verifier.patient(patient1).product(product2)
                .visit(objects.getVisit(patient1).get())
                .resetAlerts()
                .verify(items, 2);
    }

    /**
     * Verifies that existing alerts are completed if a product is used with the same alert type.
     */
    @Test
    public void testMarkMatchingAlertsCompleted() {
        Entity alertType1 = patientFactory.createAlertType();
        Entity alertType2 = patientFactory.createAlertType();
        Act alert1 = patientFactory.createAlert(patient1, alertType1);
        Act alert2 = patientFactory.createAlert(patient1, alertType2);
        assertEquals(ActStatus.IN_PROGRESS, alert1.getStatus());
        assertEquals(ActStatus.IN_PROGRESS, alert2.getStatus());

        Product product = productFactory.newService()
                .fixedPrice(10)
                .addAlertTypes(alertType1, alertType2)
                .build();

        InvoiceBuilder builder = new InvoiceBuilderImpl(builderServices);
        InvoiceObjects objects = builder.customer(customer)
                .location(domainService.create(location, Location.class))
                .newItem()
                .patient(patient1)
                .product(product)
                .quantity(1)
                .add()
                .buildObjects();

        Invoice invoice = objects.getCharge();
        RelatedIMObjects<FinancialAct, ActRelationship> items = getItems(invoice);
        assertEquals(1, items.getRelationships().size());

        // verify new reminders have been created, with IN_PROGRESS status
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService());
        verifier.createdBy(serviceUser)
                .patient(patient1)
                .product(product)
                .quantity(1)
                .fixedPrice(11)
                .total(11).tax(1)
                .visit(objects.getVisit(patient1).get())
                .addAlert(alertType1, null, ActStatus.IN_PROGRESS)
                .addAlert(alertType2, null, ActStatus.IN_PROGRESS)
                .verify(items, 0);


        // verify the existing alerts have been marked COMPLETED
        assertEquals(ActStatus.COMPLETED, get(alert1).getStatus());
        assertEquals(ActStatus.COMPLETED, get(alert2).getStatus());
    }

    /**
     * Verifies that an event has one instance of the expected note.
     *
     * @param event   the event
     * @param patient the expected patient
     * @param note    the expected note
     */
    protected void checkEventNote(Act event, Party patient, String note) {
        int found = 0;
        IMObjectBean eventBean = getBean(event);
        assertEquals(patient, eventBean.getTarget("patient"));
        for (Act item : eventBean.getTargets("items", Act.class)) {
            if (item.isA(PatientArchetypes.CLINICAL_NOTE)) {
                IMObjectBean bean = getBean(item);
                assertEquals(patient, bean.getTarget("patient"));
                if (StringUtils.equals(note, bean.getString("note"))) {
                    found++;
                }
            }
        }
        assertEquals(1, found);
    }

    /**
     * Returns the item acts for an invoice
     *
     * @param invoice the invoice
     * @return the item acts
     */
    private RelatedIMObjects<FinancialAct, ActRelationship> getItems(Invoice invoice) {
        FinancialAct act = getArchetypeService().get(CustomerAccountArchetypes.INVOICE, invoice.getId(),
                                                     FinancialAct.class);
        return getBean(act).getRelated("items", FinancialAct.class, ActRelationship.class);
    }

    /**
     * Sets an invoice status to POSTED.
     *
     * @param invoice the invoice
     */
    private void post(Invoice invoice) {
        FinancialAct act = getArchetypeService().get(CustomerAccountArchetypes.INVOICE, invoice.getId(),
                                                     FinancialAct.class);
        assertNotNull(act);
        act.setStatus(FinancialActStatus.POSTED);
        save(act);
    }

    /**
     * Initialises stock quantities for a product at a stock location.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param quantity      the initial stock quantity
     */
    private void initStock(Product product, Party stockLocation, int quantity) {
        stockRules.updateStock(product, stockLocation, BigDecimal.valueOf(quantity));
    }

    /**
     * Checks stock for a product at a stock location.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param expected      the expected stock quantity
     */
    private void checkStock(Product product, Party stockLocation, int expected) {
        checkEquals(expected, stockRules.getStock(get(product), get(stockLocation)));
    }

}
