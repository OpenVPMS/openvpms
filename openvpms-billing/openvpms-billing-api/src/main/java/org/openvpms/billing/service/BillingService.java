/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.service;

import org.openvpms.billing.charge.InvoiceBuilder;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;

import java.math.BigDecimal;

/**
 * Customer billing service.
 *
 * @author Tim Anderson
 */
public interface BillingService {

    /**
     * Returns an invoice given its identifier.
     *
     * @param id the invoice id
     * @return the invoice, or {@code null} if none is found
     */
    Invoice getInvoice(long id);

    /**
     * Returns an invoice given an external identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.*</em> prefix.
     * @param id        the identifier
     * @return the invoice or {@code null} if none is found
     */
    Invoice getInvoice(String archetype, String id);

    /**
     * Returns the id of an invoice given an external identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.*</em> prefix.
     * @param id        the identifier
     * @return the corresponding invoice id, or {@code -1} if none is found
     */
    long getInvoiceId(String archetype, String id);

    /**
     * Invoices a product to a customer.
     * <p/>
     * If there is an existing invoice for the customer, the product will be added to that, else a new invoice will
     * be created.
     *
     * @param customer  the customer
     * @param patient   the patient
     * @param product   the product. If it is a template product, it will be expanded
     * @param quantity  the quantity to invoice
     * @param location  the practice location where the product is being invoiced
     * @param clinician the responsible clinician. Required for medication products, optional otherwise.
     * @return the invoice
     */
    Invoice invoice(Customer customer, Patient patient, Product product, BigDecimal quantity, Location location,
                    User clinician);

    /**
     * Returns a builder to invoice a customer.
     * <p/>
     * If there is an existing invoice for the customer, items will be added to that, else a new invoice will be
     * created.
     *
     * @return an invoice builder
     */
    InvoiceBuilder getInvoiceBuilder();
}