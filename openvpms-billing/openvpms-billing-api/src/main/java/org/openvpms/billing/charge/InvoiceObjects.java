/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;

import java.util.List;
import java.util.Optional;

/**
 * Tracks the objects created and updated by a {@link InvoiceBuilder}.
 *
 * @author Tim Anderson
 */
public interface InvoiceObjects extends ChargeObjects<Invoice, InvoiceItem> {

    /**
     * Returns the patient visits that were updated.
     *
     * @return the patient visits
     */
    List<Visit> getVisits();

    /**
     * Returns the most recent visit that was updated for a patient.
     *
     * @param patient the patient
     * @return an {@code Optional} containing the most recent visit, or an empty {@code Optional} if none is found
     */
    Optional<Visit> getVisit(Patient patient);
}
