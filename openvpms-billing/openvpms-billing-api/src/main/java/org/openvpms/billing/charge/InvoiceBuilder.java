/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.billing.charge;

import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.product.Template;

import java.math.BigDecimal;

/**
 * A builder for {@link Invoice} instances.
 *
 * @author Tim Anderson
 */
public interface InvoiceBuilder extends ChargeBuilder<Invoice, InvoiceItem, InvoiceBuilder, InvoiceItemBuilder> {

    /**
     * Expands a template.
     * <p/>
     * All items will be added to the charge.
     *
     * @param template  the product template
     * @param patient   the patient
     * @param quantity  the template quantity
     * @param clinician the clinician
     * @return the charge items generated from the template
     */
    @Override
    InvoiceItems expand(Template template, Patient patient, BigDecimal quantity, User clinician);

    /**
     * Returns the charge item builders for new and updated items.
     *
     * @return the charge item builders
     */
    @Override
    InvoiceItems getChangedItems();

    /**
     * Builds the charge, and returns the built objects.
     *
     * @return the built objects
     */
    @Override
    InvoiceObjects buildObjects();
}