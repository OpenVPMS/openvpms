/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.roster;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Deputy roster.
 * <p/>
 * This corresponds to an <em>act.rosterEvent</em> in OpenVPMS.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Roster {

    /**
     * The roster identifier.
     */
    @JsonProperty("Id")
    private long id;

    /**
     * The roster date.
     */
    @JsonProperty("Date")
    private String date;

    /**
     * The roster start time.
     */
    @JsonProperty("StartTime")
    private long startTime;

    /**
     * The roster end time.
     */
    @JsonProperty("EndTime")
    private long endTime;

    /**
     * The meal break.
     */
    @JsonProperty("Mealbreak")
    private String mealbreak;

    /**
     * The roster slots.
     */
    @JsonProperty("Slots")
    private ArrayList<Object> slots = new ArrayList<>();

    /**
     * The total time.
     */
    @JsonProperty("TotalTime")
    private BigDecimal totalTime;

    /**
     * The cost.
     */
    @JsonProperty("Cost")
    private BigDecimal cost;

    /**
     * The operational unit identifier.
     */
    @JsonProperty("OperationalUnit")
    private long operationalUnit;

    /**
     * The employee identifier.
     */
    @JsonProperty("Employee")
    private long employee;

    /**
     * The comment.
     */
    @JsonProperty("Comment")
    private String comment;

    /**
     * The warning.
     */
    @JsonProperty("Warning")
    private String warning;

    /**
     * The warning override comment.
     */
    @JsonProperty("WarningOverrideComment")
    private String warningOverrideComment;

    /**
     * Determines if the roster is published or not.
     */
    @JsonProperty("Published")
    private boolean published;

    /**
     * Matched by timesheet.
     */
    @JsonProperty("MatchedByTimesheet")
    private long matchedByTimesheet;

    /**
     * Determines if the roster is open.
     */
    @JsonProperty("Open")
    private boolean open;

    /**
     * The confirm status.
     */
    @JsonProperty("ConfirmStatus")
    private long confirmStatus;

    /**
     * The confirm comment.
     */
    @JsonProperty("ConfirmComment")
    private String confirmComment;

    /**
     * The confirm by time.
     */
    @JsonProperty("ConfirmBy")
    private long confirmBy;

    /**
     * The confirmation time.
     */
    @JsonProperty("ConfirmTime")
    private long confirmTime;

    /**
     * The swap status.
     */
    @JsonProperty("SwapStatus")
    private long SwapStatus;

    /**
     * The swap managed by.
     */
    @JsonProperty("SwapManageBy")
    private String swapManageBy;

    /**
     * The shift template id.
     */
    @JsonProperty("ShiftTemplate")
    private long shiftTemplate;

    /**
     * The connect status.
     */
    @JsonProperty("ConnectStatus")
    private String connectStatus = null;

    /**
     * The creator id.
     */
    @JsonProperty("Creator")
    private long creator;

    /**
     * The created timestamp.
     */
    @JsonProperty("Created")
    private Date created;

    /**
     * The modified timestamp.
     */
    @JsonProperty("Modified")
    private Date modified;

    /**
     * The on-cost.
     */
    @JsonProperty("OnCost")
    private BigDecimal onCost;

    /**
     * The localised start time.
     */
    @JsonProperty("StartTimeLocalized")
    private String startTimeLocalized;

    /**
     * The localised end time.
     */
    @JsonProperty("EndTimeLocalized")
    private String endTimeLocalized;

    /**
     * The external id.
     */
    @JsonProperty("ExternalId")
    private String externalId = null;

    /**
     * The connect creator.
     */
    @JsonProperty("ConnectCreator")
    private String connectCreator = null;

    /**
     * Returns the roster identifier.
     *
     * @return the roster identifier
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the roster identifier.
     *
     * @param id the roster identifier
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the roster date.
     *
     * @return the roster date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the roster date.
     *
     * @param date the roster date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the roster start time.
     *
     * @return the unix timestamp (GMT) when the roster starts
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Sets the roster start time.
     *
     * @param startTime the unix timestamp (GMT) when the roster starts
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * Returns the roster end time.
     *
     * @return the unix timestamp (GMT) when the roster ends
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * Sets the roster end time.
     *
     * @param endTime the unix timestamp (GMT) when the roster ends
     */
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    /**
     * Returns the meal break.
     *
     * @return the meal break
     */
    public String getMealbreak() {
        return mealbreak;
    }

    /**
     * Sets the meal break.
     *
     * @param mealbreak the meal break
     */
    public void setMealbreak(String mealbreak) {
        this.mealbreak = mealbreak;
    }

    /**
     * Returns the total time.
     *
     * @return the total time, in hours
     */
    public BigDecimal getTotalTime() {
        return totalTime;
    }

    /**
     * Sets the total time.
     *
     * @param totalTime the total time, in hours
     */
    public void setTotalTime(BigDecimal totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * Returns the cost.
     *
     * @return the cost
     */
    public BigDecimal getCost() {
        return cost;
    }

    /**
     * Returns the operational unit identifier.
     *
     * @return the operational unit identifier
     */
    public long getOperationalUnit() {
        return operationalUnit;
    }

    /**
     * Sets the operational unit identifier.
     *
     * @param operationalUnit the operational unit identifier
     */
    public void setOperationalUnit(long operationalUnit) {
        this.operationalUnit = operationalUnit;
    }

    /**
     * Returns the employee identifier.
     *
     * @return the employee identifier
     */
    public long getEmployee() {
        return employee;
    }

    /**
     * Sets the employee identifier.
     *
     * @param employee the employee identifier
     */
    public void setEmployee(long employee) {
        this.employee = employee;
    }

    /**
     * Returns the comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Returns the warning.
     *
     * @return the warning
     */
    public String getWarning() {
        return warning;
    }

    /**
     * Sets the warning.
     *
     * @param warning the warning
     */
    public void setWarning(String warning) {
        this.warning = warning;
    }

    /**
     * Returns the warning override comment.
     *
     * @return the warning override comment
     */
    public String getWarningOverrideComment() {
        return warningOverrideComment;
    }

    /**
     * Sets the warning override comment.
     *
     * @param warningOverrideComment the warning override comment
     */
    public void setWarningOverrideComment(String warningOverrideComment) {
        this.warningOverrideComment = warningOverrideComment;
    }

    /**
     * Determines if the roster is published.
     *
     * @return {@code true} if the roster is published
     */
    public boolean getPublished() {
        return published;
    }

    /**
     * Determines if the roster is published.
     *
     * @param published if {@code true} the roster is published
     */
    public void setPublished(boolean published) {
        this.published = published;
    }

    /**
     * Returns the matched by timesheet identifier.
     *
     * @return the matched by timesheet identifier
     */
    public long getMatchedByTimesheet() {
        return matchedByTimesheet;
    }

    /**
     * Sets the matched by timesheet identifier.
     *
     * @param matchedByTimesheet the matched by timesheet identifier
     */
    public void setMatchedByTimesheet(long matchedByTimesheet) {
        this.matchedByTimesheet = matchedByTimesheet;
    }

    /**
     * Determines if the roster is open.
     *
     * @return {@code true} if the roster is open
     */
    public boolean getOpen() {
        return open;
    }

    /**
     * Determines if the roster is open.
     *
     * @param open if {@code true} the roster is open
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * Returns the confirm status.
     *
     * @return the confirm status
     */
    public long getConfirmStatus() {
        return confirmStatus;
    }

    /**
     * Sets the confirm status.
     *
     * @param confirmStatus the confirm status
     */
    public void setConfirmStatus(long confirmStatus) {
        this.confirmStatus = confirmStatus;
    }

    /**
     * Returns the confirm comment.
     *
     * @return the confirm comment
     */
    public String getConfirmComment() {
        return confirmComment;
    }

    /**
     * Sets the confirm comment.
     *
     * @param confirmComment the confirm commment
     */
    public void setConfirmComment(String confirmComment) {
        this.confirmComment = confirmComment;
    }

    /**
     * Returns the confirm by time.
     *
     * @return the confirm by time
     */
    public long getConfirmBy() {
        return confirmBy;
    }

    /**
     * Sets the confirm by time.
     *
     * @param confirmBy the confirm by time
     */
    public void setConfirmBy(long confirmBy) {
        this.confirmBy = confirmBy;
    }

    /**
     * Returns the confirm time.
     *
     * @return the confirm time
     */
    public long getConfirmTime() {
        return confirmTime;
    }

    /**
     * Returns the confirm time.
     *
     * @param confirmTime the confirm time
     */
    public void setConfirmTime(long confirmTime) {
        this.confirmTime = confirmTime;
    }

    /**
     * Returns the swap status.
     *
     * @return the swap status
     */
    public long getSwapStatus() {
        return SwapStatus;
    }

    /**
     * Sets the swap status.
     *
     * @param swapStatus the swap status
     */
    public void setSwapStatus(long swapStatus) {
        this.SwapStatus = swapStatus;
    }

    /**
     * Returns the swap manage by.
     *
     * @return the swap manage by
     */
    public String getSwapManageBy() {
        return swapManageBy;
    }

    /**
     * Sets the swap manage by.
     *
     * @param swapManageBy the swap manage by
     */
    public void setSwapManageBy(String swapManageBy) {
        this.swapManageBy = swapManageBy;
    }

    /**
     * Returns the shift template identifier.
     *
     * @return the shift template identifier
     */
    public long getShiftTemplate() {
        return shiftTemplate;
    }

    /**
     * Sets the shift template identifier.
     *
     * @param shiftTemplate the shift template identifier
     */
    public void setShiftTemplate(long shiftTemplate) {
        this.shiftTemplate = shiftTemplate;
    }

    /**
     * Returns the connect status.
     *
     * @return the connect status
     */
    public String getConnectStatus() {
        return connectStatus;
    }

    /**
     * Sets the connect status.
     *
     * @param connectStatus the connect status
     */
    public void setConnectStatus(String connectStatus) {
        this.connectStatus = connectStatus;
    }

    /**
     * Returns the creator identifier.
     *
     * @return the creator identifier
     */
    public long getCreator() {
        return creator;
    }

    /**
     * Sets the creator identifier.
     *
     * @param creator the creator identifier
     */
    public void setCreator(long creator) {
        this.creator = creator;
    }

    /**
     * Returns the created timestamp.
     *
     * @return the created timestamp
     */
    public Date getCreated() {
        return created;
    }

    /**
     * Returns the created timestamp.
     *
     * @param created the created timestamp
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * Returns the modified timestamp.
     *
     * @return the modified timestamp
     */
    public Date getModified() {
        return modified;
    }

    /**
     * Sets the modified timestamp.
     *
     * @param modified the modified timestamp
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * Returns the on-cost.
     *
     * @return the on-cost
     */
    public BigDecimal getOnCost() {
        return onCost;
    }

    /**
     * Sets the on-cost.
     *
     * @param onCost the on-cost
     */
    public void setOnCost(BigDecimal onCost) {
        this.onCost = onCost;
    }

    /**
     * Returns the localised start time.
     *
     * @return the localised start time
     */
    public String getStartTimeLocalized() {
        return startTimeLocalized;
    }

    /**
     * Sets the localised start time.
     *
     * @param startTimeLocalized the localised start time
     */
    public void setStartTimeLocalized(String startTimeLocalized) {
        this.startTimeLocalized = startTimeLocalized;
    }

    /**
     * Returns the localised end time.
     *
     * @return the localised end time
     */
    public String getEndTimeLocalized() {
        return endTimeLocalized;
    }

    /**
     * Sets the localised end time.
     *
     * @param endTimeLocalized the localised end time
     */
    public void setEndTimeLocalized(String endTimeLocalized) {
        this.endTimeLocalized = endTimeLocalized;
    }

    /**
     * Returns the external id.
     *
     * @return the external id
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * Sets the external id.
     *
     * @param externalId the external id
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * Returns the connect creator.
     *
     * @return the connect creator
     */
    public String getConnectCreator() {
        return connectCreator;
    }

    /**
     * Sets the connect creator.
     *
     * @param connectCreator the connect creator
     */
    public void setConnectCreator(String connectCreator) {
        this.connectCreator = connectCreator;
    }
}