/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Tracks Deputy rosters.
 *
 * @author Tim Anderson
 */
class Rosters {

    /**
     * The Deputy service. May be {@code null} if rosters are pre-cached.
     */
    private final Deputy deputy;

    /**
     * The rosters, keyed on id.
     */
    private final Map<Long, Roster> rosters = new LinkedHashMap<>();

    /**
     * Determines if a roster has been processed.
     */
    private Set<Long> processed = new HashSet<>();

    /**
     * The log.
     */
    private static final Logger log = LoggerFactory.getLogger(Rosters.class);

    /**
     * Constructs a {@link Rosters}.
     * <p/>
     * Deputy will be queried to retrieve rosters.
     *
     * @param deputy the Deputy service
     */
    Rosters(Deputy deputy) {
        this.deputy = deputy;
    }

    /**
     * Constructs a {@link Rosters}.
     *
     * @param rosters pre-cached rosters
     * @param deputy  the Deputy service
     */
    Rosters(List<Roster> rosters, Deputy deputy) {
        this.deputy = deputy;
        for (Roster roster : rosters) {
            add(roster);
        }
    }

    /**
     * Registers a roster as being processed.
     * <p/>
     * The roster will be cached.
     *
     * @param roster the roster
     */
    void processed(Roster roster) {
        add(roster);
        processed.add(roster.getId());
    }

    /**
     * Returns a roster given its id.
     *
     * @param id the roster id
     * @return the corresponding roster, or {@code null} if none is found
     */
    Roster get(long id) {
        Roster result = null;
        if (id != -1) {
            result = rosters.get(id);
            if (result == null) {
                try {
                    result = deputy.getRoster(id);
                    if (result != null) {
                        add(result);
                    }
                } catch (NotFoundException exception) {
                    log.debug("Roster not found: " + id, exception);
                }
            }
        }
        return result;
    }

    /**
     * Returns the unprocessed rosters.
     *
     * @return the unprocessed rosters
     */
    List<Roster> getUnprocessed() {
        List<Roster> result = new ArrayList<>();
        for (Roster roster : rosters.values()) {
            if (!processed.contains(roster.getId())) {
                result.add(roster);
            }
        }
        return result;
    }

    private void add(Roster roster) {
        rosters.put(roster.getId(), roster);
    }

}
