/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.deputy.internal.model.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

/**
 * Helper to collects all results matching a query.
 *
 * @author Tim Anderson
 */
public class QueryHelper {

    /**
     * Default maximum number of results to retrieve. 500 is the Deputy limit.
     */
    public static final int MAX_RESULTS = 500;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(QueryHelper.class);

    /**
     * Query results.
     * <p/>
     * Deputy limits the no. of results per call. This pages through the results, and collects them. This should
     * only be called for relatively small result sets.
     *
     * @param query    the query
     * @param function the function to perform the query
     * @return the matching results
     */
    public static <T> List<T> query(Query query, Function<Query, List<T>> function) {
        return query(query, MAX_RESULTS, function, null);
    }

    /**
     * Query results.
     * <p/>
     * Deputy limits the no. of results per call. This pages through the results, and collects them. This should
     * only be called for relatively small result sets.
     *
     * @param query    the query
     * @param pageSize the maximum no. of results to request at a time
     * @param function the function to perform the query
     * @param stop     flag to terminate collection early. May be {@code null}
     * @return the matching results
     */
    public static <T> List<T> query(Query query, int pageSize, Function<Query, List<T>> function, AtomicBoolean stop) {
        List<T> results = new ArrayList<>();
        int firstResult = 0;
        query.setMaxResults(pageSize);
        boolean done = false;
        while (!done && (stop == null || !stop.get())) {
            query.setFirstResult(firstResult);
            List<T> matches = function.apply(query);
            if (matches.size() < pageSize) {
                done = true;
            } else {
                if (matches.size() > pageSize) {
                    log.error("Returned " + matches.size() + ", requested=" + pageSize);
                }
                firstResult += matches.size();
            }
            results.addAll(matches);
        }
        return results;
    }
}
