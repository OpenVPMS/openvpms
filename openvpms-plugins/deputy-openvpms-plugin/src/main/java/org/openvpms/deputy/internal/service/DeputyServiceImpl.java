/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.user.User;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.i18n.DeputyMessages;
import org.openvpms.deputy.internal.mapping.Employees;
import org.openvpms.deputy.internal.mapping.OperationalUnits;
import org.openvpms.deputy.internal.model.organisation.Company;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;
import org.openvpms.mapping.exception.MappingException;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.service.MappingProvider;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;
import org.openvpms.plugin.service.util.ContextClassLoaderHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Service to perform roster synchronisation with Deputy.
 *
 * @author Tim Anderson
 */
public class DeputyServiceImpl implements MappingProvider, Deputy {

    /**
     * The service name.
     */
    public static final String NAME = "Deputy Roster Synchronisation Service";

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The mapping service.
     */
    private final MappingService mappingService;

    /**
     * The encryptor.
     */
    private final PasswordEncryptor encryptor;

    /**
     * The query service.
     */
    private final QueryService queryService;

    /**
     * Used to schedule synchronisation.
     */
    private final ScheduledExecutorService executor;

    /**
     * Thread id name counter.
     */
    private final AtomicLong counter = new AtomicLong(0);

    /**
     * Flag used to determine if synchronisation is underway.
     */
    private volatile boolean syncInProgress = false;

    /**
     * The synchronisation manager.
     */
    private SynchronisationManager synchroniser;

    /**
     * The Deputy client.
     */
    private Deputy client;

    /**
     * The Deputy url.
     */
    private String url;

    /**
     * The access token.
     */
    private String accessToken;

    /**
     * The number of days to sync.
     */
    private int daysToSync;

    /**
     * The synchronisation frequency (minutes).
     */
    private int syncFrequency;

    /**
     * The mapping configuration.
     */
    private IMObject mapping;

    /**
     * Future used to cancel synchronisation.
     */
    private ScheduledFuture<?> future;

    /**
     * The log.
     */
    private static final Logger log = LoggerFactory.getLogger(DeputyServiceImpl.class);

    /**
     * Constructs a {@link DeputyServiceImpl}.
     *
     * @param service        the archetype service
     * @param installer      the archetype installer
     * @param mappingService the mapping service
     * @param encryptor      the encryptor
     */
    public DeputyServiceImpl(ArchetypeService service, ArchetypeInstaller installer, MappingService mappingService,
                             PasswordEncryptor encryptor) {
        install(installer, Archetypes.PLUGIN);
        install(installer, Archetypes.EVENT_ID);
        this.service = service;
        this.mappingService = mappingService;
        this.encryptor = encryptor;
        queryService = new QueryService(service);
        executor = Executors.newSingleThreadScheduledExecutor(
                runnable -> new Thread(runnable, "DeputySync" + counter.incrementAndGet()));
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return NAME;
    }

    /**
     * Returns the mappings required to support synchronisation.
     *
     * @return the mappings
     */
    @Override
    public List<Mappings<?>> getMappings() {
        return Arrays.asList(getRosterAreas(), getUsers());
    }

    /**
     * Invoked when the service is registered, and each time the configuration is updated.
     *
     * @param config may be {@code null}, if no configuration exists, or the configuration is deactivated or removed
     */
    public synchronized void setConfiguration(IMObject config) {
        try {
            stopSync();
            if (config != null) {
                IMObjectBean bean = service.getBean(config);
                url = bean.getString("url");
                accessToken = getAccessToken(bean);
                mapping = bean.getTarget("mapping");
                daysToSync = bean.getInt("daysToSync");
                if (daysToSync < 1) {
                    daysToSync = 30;
                }
                syncFrequency = bean.getInt("syncFrequency");
                if (syncFrequency < 5) {
                    syncFrequency = 5;
                }
                if (mapping == null) {
                    mapping = mappingService.createMappingConfiguration(Entity.class);
                    bean.addTarget("mapping", mapping);
                    bean.save(mapping);
                }
            } else {
                url = null;
                accessToken = null;
                mapping = null;
            }
            client = null;

            if (mapping != null && accessToken != null) {
                startSynchronisation();
            }
        } catch (Throwable exception) {
            log.error("Failed to update configuration", exception);
        }
    }

    /**
     * Disposes of this service.
     */
    public synchronized void dispose() {
        stopSync();
    }

    /**
     * Returns the companies.
     *
     * @return the companies
     */
    @Override
    public List<Company> getCompanies() {
        return getClient().getCompanies();
    }

    /**
     * Queries operational units.
     *
     * @param query the query
     * @return the matching operational units
     */
    @Override
    public List<OperationalUnit> getOperationalUnits(Query query) {
        return getClient().getOperationalUnits(query);
    }

    /**
     * Returns an operational unit given its identifier.
     *
     * @param id the operational unit identifier
     * @return the corresponding operational unit
     * @throws NotFoundException if the operational unit does not exist
     */
    @Override
    public OperationalUnit getOperationalUnit(long id) {
        return getClient().getOperationalUnit(id);
    }

    /**
     * Queries employees.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Employee> getEmployees(Query query) {
        return getClient().getEmployees(query);
    }

    /**
     * Returns an employee given its identifier.
     *
     * @param id the employee identifier
     * @return the corresponding employee, or {@code null} if none is found
     */
    @Override
    public Employee getEmployee(long id) {
        return getClient().getEmployee(id);
    }

    /**
     * Queries rosters.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Roster> getRosters(Query query) {
        return getClient().getRosters(query);
    }

    /**
     * Returns a roster given its identifier.
     *
     * @param id the roster identifier
     * @return the roster
     * @throws NotFoundException if the roster is not found
     */
    @Override
    public Roster getRoster(long id) {
        return getClient().getRoster(id);
    }

    /**
     * Deletes an event.
     *
     * @param id the event identifier
     * @return the deletion confirmation message
     * @throws NotFoundException if the roster does not exist
     */
    @Override
    public String removeRoster(long id) {
        return getClient().removeRoster(id);
    }

    /**
     * Creates a new roster, or updates an existing roster.
     * <p/>
     * To update an existing roster, the {@link RosterData#getId()} must be set.
     *
     * @param data the roster data
     * @return the new roster
     * @throws BadRequestException if an overlap is detected
     */
    @Override
    public Roster roster(RosterData data) {
        return getClient().roster(data);
    }

    /**
     * Invoked when a roster event is updated in OpenVPMS.
     *
     * @param event the event - an <em>act.rosterEvent</em>
     */
    public void eventUpdated(Act event) {
        if (!syncInProgress) {
            try {
                if (isCurrent(event)) {
                    IMObjectBean bean = service.getBean(event);
                    Identity identity = DeputyHelper.getSynchronisationId(bean);
                    if (identity == null || SyncStatus.PENDING.equals(service.getBean(identity).getValue("status"))) {
                        RosterSynchroniser updater = new RosterSynchroniser(getRosterAreas(), getUsers(), this,
                                                                            queryService, service);
                        updater.synchroniseFromEvent(bean);
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("Ignoring update of event=" + event.getObjectReference()
                              + ". Shift ended before today");
                }
            } catch (Throwable exception) {
                log.error("Failed to update event=" + event.getId() + ": " + exception.getMessage(), exception);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("Ignoring update of event=" + event.getObjectReference() + " during synchronisation");
        }
    }

    /**
     * Invoked when a roster event is removed in OpenVPMS.
     *
     * @param event the event - an <em>act.rosterEvent</em>
     */
    public void eventRemoved(Act event) {
        if (!syncInProgress) {
            try {
                if (isCurrent(event)) {
                    IMObjectBean bean = service.getBean(event);
                    Identity identity = DeputyHelper.getSynchronisationId(bean);
                    if (identity != null) {
                        RosterSynchroniser updater = new RosterSynchroniser(getRosterAreas(), getUsers(), this,
                                                                            queryService, service);
                        updater.remove(bean);
                    }
                } else if (log.isDebugEnabled()) {
                    log.debug("Ignoring removal of event=" + event.getObjectReference()
                              + ". Shift ended before today");
                }
            } catch (Throwable exception) {
                log.error("Failed to remove event=" + event.getId() + ": " + exception.getMessage(), exception);
            }
        } else if (log.isDebugEnabled()) {
            log.debug("Ignoring removal of event=" + event.getObjectReference() + " during synchronisation");
        }
    }

    /**
     * Returns the decrypted access token.
     *
     * @param bean the configuration bean
     * @return the access token, or {@code null} if it cannot be decrypted
     */
    protected String getAccessToken(IMObjectBean bean) {
        String result = null;
        String accessToken = bean.getString("accessToken");
        if (accessToken != null) {
            try {
                result = encryptor.decrypt(accessToken);
            } catch (Throwable exception) {
                log.error("Failed to decrypt accessToken. Calls to Deputy will not succeed: " + exception.getMessage(),
                          exception);
            }
        }
        return result;
    }

    /**
     * Determines if event is current.
     *
     * @param event the event
     * @return {@code true} if it ends on or after today
     */
    private boolean isCurrent(Act event) {
        LocalDate today = LocalDate.now();
        LocalDate date = event.getActivityEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return !date.isBefore(today);
    }

    /**
     * Returns the roster area mappings.
     *
     * @return the roster areas
     */
    private Mappings<Entity> getRosterAreas() {
        OperationalUnits targets = new OperationalUnits("Deputy Area", this);
        return mappingService.createMappings(getMapping(), Entity.class, Archetypes.ROSTER_AREA, "Roster Areas",
                                             targets);
    }

    /**
     * Returns the user mappings for users that may be rostered.
     *
     * @return the users that may be rostered at the location
     */
    private Mappings<User> getUsers() {
        Employees targets = new Employees("Deputy Employee", this);
        return mappingService.createMappings(getMapping(), User.class, Archetypes.USER, "Employees", targets);
    }

    /**
     * Starts synchronisation.
     */
    private void startSynchronisation() {
        stopSync();
        future = executor.scheduleAtFixedRate(this::sync, 0, syncFrequency, TimeUnit.MINUTES);
    }

    /**
     * Stops synchronisation.
     */
    private synchronized void stopSync() {
        if (synchroniser != null) {
            synchroniser.stop();
            synchroniser = null;
        }
        if (future != null) {
            future.cancel(true);
        }
    }

    /**
     * Performs synchronisation.
     */
    private void sync() {
        try {
            SynchronisationManager sync;
            synchronized (this) {
                int days = daysToSync;
                if (log.isInfoEnabled()) {
                    log.info("Synchronising with Deputy, daysToSync=" + days + ", syncFrequency=" + syncFrequency);
                }
                sync = new SynchronisationManager(days, getRosterAreas(), getUsers(), this, queryService, service);
                this.synchroniser = sync;
                syncInProgress = true;
            }
            sync.run();
        } catch (Throwable exception) {
            log.error("Synchronisation with Deputy failed", exception);
        } finally {
            syncInProgress = false;
        }
    }

    /**
     * Returns the mapping configuration.
     *
     * @return the mapping configuration
     * @throws MappingException if it is not configured
     */
    private synchronized IMObject getMapping() {
        if (mapping == null) {
            throw new MappingException(DeputyMessages.notConfigured());
        }
        return mapping;
    }

    /**
     * Installs an archetype.
     *
     * @param installer the installer
     * @param archetype the archetype to install
     */
    private void install(ArchetypeInstaller installer, String archetype) {
        installer.install(getClass(), "/org/openvpms/deputy/internal/archetype/" + archetype + ".adl");
    }

    /**
     * Returns the Deputy client.
     * <p/>
     * This proxies all calls to ensure the thread context class loader is set. This is to avoid problems with the
     * wrong javax.ws.rs.ext.RuntimeDelegate being used.
     *
     * @return the Deputy client
     */
    private synchronized Deputy getClient() {
        if (client == null) {
            try {
                client = ContextClassLoaderHelper.proxy(() -> new DeputyClient(url, accessToken));
            } catch (RuntimeException exception) {
                throw exception;
            } catch (Exception exception) {
                throw new IllegalStateException("Failed to create Deputy client: " + exception.getMessage(), exception);
            }
        }
        return client;
    }

}
