/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.roster;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Used to create and update rosters in Deputy.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RosterData {

    /**
     * Unix timestamp (GMT) when the shift starts.
     */
    @JsonProperty("intStartTimestamp")
    private long startTime;

    /**
     * Unix timestamp (GMT) when the shift finishes.
     */
    @JsonProperty("intEndTimestamp")
    private long endTime;

    /**
     * The employee. Leave as 0 for unallocated.
     */
    @JsonProperty("intRosterEmployee")
    private long employee;

    /**
     * Is the shift published? If 1 then employee can see. If 0, they can't.
     */
    @JsonProperty("blnPublish")
    private int publish;

    /**
     * Is the shift open? If 1 then other employee can claim it.
     */
    @JsonProperty("blnOpen")
    private boolean open;

    /**
     * Comment for the shift.
     */
    @JsonProperty("strComment")
    private String comment;

    /**
     * Ignore any warnings (e.g. training/leave/unavailability) and force save this roster.
     */
    @JsonProperty("blnForceOverwrite")
    private int forceOverwrite;

    /**
     * Number of minutes to have a meal break for.
     */
    @JsonProperty("intMealbreakMinute")
    private int mealbreakMinute;

    /**
     * The department/area ID.
     */
    @JsonProperty("intOpunitId")
    private long operationalUnit;

    /**
     * If you want to modify existing roster. (optional).
     */
    @JsonProperty("intRosterId")
    private long id;

    /**
     * Returns the roster start time.
     *
     * @return the unix timestamp (GMT) when the roster starts
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Sets the roster start time.
     *
     * @param startTime the unix timestamp (GMT) when the roster starts
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * Returns the roster end time.
     *
     * @return the unix timestamp (GMT) when the roster ends
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * Sets the roster end time.
     *
     * @param endTime the unix timestamp (GMT) when the roster ends
     */
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    /**
     * Returns the employee identifier.
     *
     * @return the employee identifier
     */
    public long getEmployee() {
        return employee;
    }

    /**
     * Sets the employee identifier.
     *
     * @param employee the employee identifier
     */
    public void setEmployee(long employee) {
        this.employee = employee;
    }

    /**
     * Determines if the shift is published or not.
     *
     * @return {@code 1} if the shift is published, {@code 0} if its not published
     */
    public int getPublish() {
        return publish;
    }

    /**
     * Determines if the shift is published or not.
     *
     * @param publish if {@code 1} the shift is published, if {@code 0} it's not published
     */
    public void setPublish(int publish) {
        this.publish = publish;
    }

    /**
     * Determines if the shift is open.
     *
     * @return {@code true} if the shift is open
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * Determines if the shift is open.
     *
     * @param open if {@code true} the shift is open
     */
    public void setOpen(boolean open) {
        this.open = open;
    }

    /**
     * Returns the shift comment.
     *
     * @return the shift comment. May be {@code null}
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the shift comment.
     *
     * @param comment the shift comment. May be {@code null}
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Determines if any warnings (e.g. training/leave/unavailability) should be ignored when saving the roster.
     *
     * @return {@code 1} to ignore warnings, otherwise {@code 0} to not ignore them
     */
    public int getForceOverwrite() {
        return forceOverwrite;
    }

    /**
     * Determines if any warnings (e.g. training/leave/unavailability) should be ignored when saving the roster.
     *
     * @param forceOverwrite if {@code 1} ignore warnings, otherwise use {@code 0} to not ignore them
     */
    public void setForceOverwrite(int forceOverwrite) {
        this.forceOverwrite = forceOverwrite;
    }

    /**
     * Returns the number of minutes to have a meal break for.
     *
     * @return the number of minutes to have a meal break for
     */
    public int getMealbreakMinute() {
        return mealbreakMinute;
    }

    /**
     * Sets the number of minutes to have a meal break for.
     *
     * @param minutes the number of minutes to have a meal break for
     */
    public void setMealbreakMinute(int minutes) {
        this.mealbreakMinute = minutes;
    }

    /**
     * Returns the operational unit the roster is for.
     *
     * @return the operational identifier
     */
    public long getOperationalUnit() {
        return operationalUnit;
    }

    /**
     * Sets the operational unit the roster is for
     *
     * @param operationalUnit the operational unit identifier
     */
    public void setOperationalUnit(long operationalUnit) {
        this.operationalUnit = operationalUnit;
    }

    /**
     * The roster identifier, if an existing roster is being updated.
     *
     * @return the roster identifier
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the roster identifier, if an existing roster is being updated.
     *
     * @param id the shift identifier
     */
    public void setId(long id) {
        this.id = id;
    }

}