/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.organisation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Deputy Employee.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee implements Resource {

    /**
     * The employee identifier.
     */
    @JsonProperty("Id")
    private long id;

    /**
     * The company identifier.
     */
    @JsonProperty("Company")
    private long company;

    /**
     * The employee's first name.
     */
    @JsonProperty("FirstName")
    private String firstName;

    /**
     * The employee's last name.
     */
    @JsonProperty("LastName")
    private String lastName;

    /**
     * The employee's display name.
     */
    @JsonProperty("DisplayName")
    private String displayName;

    /**
     * The employee's other name.
     */
    @JsonProperty("OtherName")
    private String otherName = null;

    /**
     * The employee's title.
     */
    @JsonProperty("Salutation")
    private String salutation = null;

    /**
     * The employee's main address.
     */
    @JsonProperty("MainAddress")
    private String mainAddress = null;

    /**
     * The employee's postal address.
     */
    @JsonProperty("PostalAddress")
    private String postalAddress = null;

    /**
     * The employee's contact identifier.
     */
    @JsonProperty("Contact")
    private long contact;

    /**
     * The employee's emergency address.
     */
    @JsonProperty("EmergencyAddress")
    private String emergencyAddress = null;

    /**
     * The employee's date of birth.
     */
    @JsonProperty("DateOfBirth")
    private String dateOfBirth = null;

    /**
     * The employee's gender.
     */
    @JsonProperty("Gender")
    private String gender = null;

    /**
     * The employee's photo.
     */
    @JsonProperty("Photo")
    private String photo = null;

    /**
     * The employee's user identifier.
     */
    @JsonProperty("UserId")
    private long userId;

    /**
     * The employee's job application identifier.
     */
    @JsonProperty("JobAppId")
    private String jobAppId = null;

    /**
     * Determines if the employee is active or not.
     */
    @JsonProperty("Active")
    private boolean active;

    /**
     * The employee's start date.
     */
    @JsonProperty("StartDate")
    private String startDate;

    /**
     * The employee's termination date.
     */
    @JsonProperty("TerminationDate")
    private String terminationDate = null;

    /**
     * The stress profile.
     */
    @JsonProperty("StressProfile")
    private long stressProfile;

    /**
     * The employee's position.
     */
    @JsonProperty("Position")
    private String position = null;

    /**
     * The higher duty.
     */
    @JsonProperty("HigherDuty")
    private String higherDuty = null;

    /**
     * The employee's role identifier.
     */
    @JsonProperty("Role")
    private long role;

    /**
     * Allow appraisal flag.
     */
    @JsonProperty("AllowAppraisal")
    private boolean allowAppraisal;

    /**
     * History identifier.
     */
    @JsonProperty("HistoryId")
    private long historyId;

    /**
     * Custom field data.
     */
    @JsonProperty("CustomFieldData")
    private Object customFieldData = null;

    /**
     * The creator identifier.
     */
    @JsonProperty("Creator")
    private long creator;

    /**
     * The created timestamp.
     */
    @JsonProperty("Created")
    private String created;

    /**
     * The modified timestamp.
     */
    @JsonProperty("Modified")
    private String modified;

    /**
     * Returns the object identifier.
     *
     * @return the object identifier
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets the employee identifier.
     *
     * @param Id the employee identifier
     */
    public void setId(long Id) {
        this.id = Id;
    }

    /**
     * Returns the object name.
     *
     * @return the object name
     */
    @Override
    public String getName() {
        return getDisplayName();
    }

    /**
     * Returns the company identifier.
     *
     * @return the company identifier
     */
    public long getCompany() {
        return company;
    }

    /**
     * Sets the company identifier.
     *
     * @param company the company identifier
     */
    public void setCompany(long company) {
        this.company = company;
    }

    /**
     * Returns the employee's first name.
     *
     * @return the employee's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the employee's first name.
     *
     * @param firstName the employee's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the employee's last name.
     *
     * @return the employee's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the employee's last name.
     *
     * @param lastName the employee's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the employee's display name.
     *
     * @return the employee's display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the employee's display name.
     *
     * @param displayName the employee's display name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Returns the employee's other name.
     *
     * @return the employee's other name
     */
    public String getOtherName() {
        return otherName;
    }

    /**
     * Sets the employee's other name.
     *
     * @param otherName the employee's other name
     */
    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    /**
     * Returns the employee's title.
     *
     * @return the employee's title
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Sets the employee's title.
     *
     * @param salutation the employee's title
     */
    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    /**
     * Returns the employee's main address.
     *
     * @return the employee's main address
     */
    public String getMainAddress() {
        return mainAddress;
    }

    /**
     * Sets the employee's main address.
     *
     * @param mainAddress the employee's main address
     */
    public void setMainAddress(String mainAddress) {
        this.mainAddress = mainAddress;
    }

    /**
     * Returns the employee's postal address.
     *
     * @return the employee's postal address
     */
    public String getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the employee's postal address.
     *
     * @param postalAddress the employee's postal address
     */
    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    /**
     * Returns the employee's contact identifier.
     *
     * @return the employee's contact identifier
     */
    public long getContact() {
        return contact;
    }

    /**
     * Sets the employee's contact identifier.
     *
     * @param contact the employee's contact identifier
     */
    public void setContact(long contact) {
        this.contact = contact;
    }

    /**
     * Returns the employee's emergency address.
     *
     * @return the employee's emergency address
     */
    public String getEmergencyAddress() {
        return emergencyAddress;
    }

    /**
     * Sets the employee's emergency address.
     *
     * @param emergencyAddress the employee's emergency address
     */
    public void setEmergencyAddress(String emergencyAddress) {
        this.emergencyAddress = emergencyAddress;
    }

    /**
     * Returns the employee's date of birth.
     *
     * @return the employee's date of birth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the employee's date of birth.
     *
     * @param dateOfBirth the employee's date of birth
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Returns the employee's gender.
     *
     * @return the employee's gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the employee's gender.
     *
     * @param gender the employee's gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Returns the employee's photo URL.
     *
     * @return the employee's photo URL. May be {@code null}
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * Sets the employee's photo URL.
     *
     * @param photo the employee's photo URL. May be {@code null}
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * Returns the employee's user identifier.
     *
     * @return the user identifier
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets the employee's user identifier.
     *
     * @param userId the employee's user identifier
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Returns the employee's job application identifier.
     *
     * @return the employee's job application identifier
     */
    public String getJobAppId() {
        return jobAppId;
    }

    /**
     * Sets the employee's job application identifier.
     *
     * @param jobAppId the employee's job application identifier
     */
    public void setJobAppId(String jobAppId) {
        this.jobAppId = jobAppId;
    }

    /**
     * Determines if the object is active or not.
     *
     * @return {@code true} if the object is active, {@code false} if it is inactive
     */
    @Override
    public boolean getActive() {
        return active;
    }

    /**
     * Determines if the object is active or not.
     *
     * @param active if {@code true} the object is active, {@code false} if it is inactive
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Returns the employee's start date.
     *
     * @return the employee's start date
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the employee's start date.
     *
     * @param startDate the employee's start date
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Returns the employee's termination date.
     *
     * @return the employee's termination date. May be {@code null}
     */
    public String getTerminationDate() {
        return terminationDate;
    }

    /**
     * Sets the employee's termination date.
     *
     * @param terminationDate the employee's termination date. May be {@code null}
     */
    public void setTerminationDate(String terminationDate) {
        this.terminationDate = terminationDate;
    }

    /**
     * Returns the stress profile identifier.
     *
     * @return the stress profile identifier
     */
    public long getStressProfile() {
        return stressProfile;
    }

    /**
     * Sets the stress profile identifier.
     *
     * @param stressProfile the stress profile identifier
     */
    public void setStressProfile(long stressProfile) {
        this.stressProfile = stressProfile;
    }

    /**
     * Returns the employee's position.
     *
     * @return the employee's position
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets the employee's position.
     *
     * @param position the employee's position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * Returns the higher duty.
     *
     * @return the higher duty
     */
    public String getHigherDuty() {
        return higherDuty;
    }

    /**
     * Sets the higher duty.
     *
     * @param higherDuty the higher duty
     */
    public void setHigherDuty(String higherDuty) {
        this.higherDuty = higherDuty;
    }

    /**
     * Returns the employee's role identifier.
     *
     * @return the employee's role identifier
     */
    public long getRole() {
        return role;
    }

    /**
     * Sets the employee's role identifier.
     *
     * @param role the employee's role identifier
     */
    public void setRole(long role) {
        this.role = role;
    }

    /**
     * Determines if appraisals are allowed.
     *
     * @return {@code true} if appraisals are allowed
     */
    public boolean getAllowAppraisal() {
        return allowAppraisal;
    }

    /**
     * Determines if appraisals are allowed.
     *
     * @param allowAppraisal if {@code true} appraisals are allowed
     */
    public void setAllowAppraisal(boolean allowAppraisal) {
        this.allowAppraisal = allowAppraisal;
    }

    /**
     * Returns the employee's history identifier.
     *
     * @return the employee's history identifier
     */
    public long getHistoryId() {
        return historyId;
    }

    /**
     * Sets the employee's history identifier.
     *
     * @param historyId the employee's history identifier
     */
    public void setHistoryId(long historyId) {
        this.historyId = historyId;
    }

    /**
     * Returns the custom field data.
     *
     * @return the custom field data. May be {@code null}
     */
    public Object getCustomFieldData() {
        return customFieldData;
    }

    /**
     * Sets the custom field data.
     *
     * @param customFieldData the custom field data. May be {@code null}
     */
    public void setCustomFieldData(Object customFieldData) {
        this.customFieldData = customFieldData;
    }

    /**
     * Returns the creator identifier.
     *
     * @return the creator identifier
     */
    public long getCreator() {
        return creator;
    }

    /**
     * Sets the creator identifier.
     *
     * @param creator the creator identifier
     */
    public void setCreator(long creator) {
        this.creator = creator;
    }

    /**
     * Returns the created timestamp.
     *
     * @return the created timestamp
     */
    public String getCreated() {
        return created;
    }

    /**
     * Sets the created timestamp.
     *
     * @param created the created timestamp
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Returns the modified timestamp.
     *
     * @return the modified timestamp
     */
    public String getModified() {
        return modified;
    }

    /**
     * Sets the modified timestamp.
     *
     * @param modified the modified timestamp
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

}
