/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.query;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Deputy Query.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Query {

    /**
     * The search criteria.
     */
    @JsonProperty("search")
    private Map<String, Field> search;

    /**
     * The sort criteria.
     */
    @JsonProperty("sort")
    private Map<String, String> sort;

    /**
     * The first result to retrieve.
     */
    @JsonProperty("start")
    private Integer firstResults;

    /**
     * The maximum number of results.
     */
    @JsonProperty("max")
    private Integer maxResults;

    /**
     * Default constructor.
     */
    public Query() {
        super();
    }

    /**
     * Returns the search criteria.
     *
     * @return the search criteria. May be {@code null}
     */
    public Map<String, Field> getSearch() {
        return search;
    }

    /**
     * Adds search criteria.
     *
     * @param name  criteria name
     * @param field the field
     */
    public void addSearch(String name, Field field) {
        if (search == null) {
            search = new LinkedHashMap<>();
        }
        search.put(name, field);
    }

    /**
     * Adds search criteria.
     *
     * @param name  the criteria name
     * @param field the search field
     * @param type  the field type
     * @param data  the search data
     */
    public void addSearch(String name, String field, String type, Object data) {
        addSearch(name, new Field(field, type, data));
    }

    /**
     * Returns the sort criteria.
     *
     * @return the sort criteria. May be {@code null}
     */
    public Map<String, String> getSort() {
        return sort;
    }

    /**
     * Adds a sort clause.
     *
     * @param name      the field to sort on
     * @param ascending if {@code true} sort ascending, else sort descending
     */
    public void orderBy(String name, boolean ascending) {
        addSort(name, ascending ? "asc" : "desc");
    }

    /**
     * Returns the first result to retrieve.
     *
     * @return the first result, or {@code null} to use the default of {@code 0}
     */
    public Integer getFirstResults() {
        return firstResults;
    }

    /**
     * Sets the first result to retrieve.
     *
     * @param firstResult the first result, or {@code null} to use the default of {@code 0}
     */
    public void setFirstResult(Integer firstResult) {
        this.firstResults = firstResult;
    }

    /**
     * Returns the maximum number of results to retrieve.
     *
     * @return the maximum number of results, or {@code null} to use the default
     */
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     * Sets the maximum number of results to retrieve.
     *
     * @param maxResults maximum number of results, or {@code null} to use the default
     */
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    /**
     * Adds sort criteria.
     *
     * @param name  the sort field
     * @param order the order. One of "asc" or "desc"
     */
    private void addSort(String name, String order) {
        if (sort == null) {
            sort = new LinkedHashMap<>();
        }
        sort.put(name, order);
    }

}
