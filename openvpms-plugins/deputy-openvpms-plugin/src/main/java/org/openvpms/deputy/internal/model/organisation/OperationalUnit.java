/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.organisation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Deputy Operational Unit.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationalUnit implements Resource {

    /**
     * The operational unit identifier.
     */
    @JsonProperty("Id")
    private long id;

    /**
     * The creator identifier.
     */
    @JsonProperty("Creator")
    private long creator;

    /**
     * The created timestamp.
     */
    @JsonProperty("Created")
    private String created;

    /**
     * The modified timestamp.
     */
    @JsonProperty("Modified")
    private String modified;

    /**
     * The company identifier.
     */
    @JsonProperty("Company")
    private long company;

    /**
     * The parent operational unit identifier.
     */
    @JsonProperty("ParentOperationalUnit")
    private long parentOperationalUnit;

    /**
     * The operational unit name.
     */
    @JsonProperty("OperationalUnitName")
    private String operationalUnitName;

    /**
     * Determines if the operational unit is active.
     */
    @JsonProperty("Active")
    private boolean active;

    /**
     * The payroll export name,
     */
    @JsonProperty("PayrollExportName")
    private String payrollExportName;

    /**
     * The address identifier.
     */
    @JsonProperty("Address")
    private long address;

    /**
     * The contact.
     */
    @JsonProperty("Contact")
    private String contact;

    /**
     * The roster sort order.
     */
    @JsonProperty("RosterSortOrder")
    private long rosterSortOrder;

    /**
     * Determines if the operational unit is shown on the roster.
     */
    @JsonProperty("ShowOnRoster")
    private boolean showOnRoster;

    /**
     * The operational unit colour.
     */
    @JsonProperty("Colour")
    private String colour;

    /**
     * The roster active hours schedule.
     */
    @JsonProperty("RosterActiveHoursSchedule")
    private String rosterActiveHoursSchedule = null;

    /**
     * The roster daily budget.
     */
    @JsonProperty("DailyRosterBudget")
    private String dailyRosterBudget = null;

    /**
     * The company code.
     */
    @JsonProperty("CompanyCode")
    private String companyCode;

    /**
     * The company name.
     */
    @JsonProperty("CompanyName")
    private String companyName;

    /**
     * Returns the object identifier.
     *
     * @return the object identifier
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets the operational unit identifier.
     *
     * @param id the operational unit identifier
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns the object name.
     *
     * @return the object name
     */
    @Override
    public String getName() {
        return getOperationalUnitName();
    }

    /**
     * Returns the creator identifier.
     *
     * @return the creator identifier
     */
    public long getCreator() {
        return creator;
    }

    /**
     * Sets the creator identifier.
     *
     * @param creator the creator identifier
     */
    public void setCreator(long creator) {
        this.creator = creator;
    }

    /**
     * Returns the created timestamp.
     *
     * @return the created timestamp
     */
    public String getCreated() {
        return created;
    }

    /**
     * Sets the created timestamp.
     *
     * @param created the created timestamp
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Returns the modified timestamp.
     *
     * @return the modified timestamp
     */
    public String getModified() {
        return modified;
    }

    /**
     * Sets the modified timestamp.
     *
     * @param modified the modified timestamp
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * Returns the company identifier.
     *
     * @return the company identifier
     */
    public long getCompany() {
        return company;
    }

    /**
     * Sets the company identifier.
     *
     * @param company the company identifier
     */
    public void setCompany(long company) {
        this.company = company;
    }

    /**
     * Returns the parent operational unit identifier.
     *
     * @return the parent operational unit identifier
     */
    public long getParentOperationalUnit() {
        return parentOperationalUnit;
    }

    /**
     * Sets the parent operational unit identifier.
     *
     * @param parentOperationalUnit the parent operational unit identifier
     */
    public void setParentOperationalUnit(long parentOperationalUnit) {
        this.parentOperationalUnit = parentOperationalUnit;
    }

    /**
     * Returns the operational unit name.
     *
     * @return the operational unit name
     */
    public String getOperationalUnitName() {
        return operationalUnitName;
    }

    /**
     * Sets the operational unit name.
     *
     * @param operationalUnitName the operational unit name
     */
    public void setOperationalUnitName(String operationalUnitName) {
        this.operationalUnitName = operationalUnitName;
    }

    /**
     * Determines if the object is active or not.
     *
     * @return {@code true} if the object is active, {@code false} if it is inactive
     */
    @Override
    public boolean getActive() {
        return active;
    }

    /**
     * Determines if the object is active or not.
     *
     * @param active if {@code true} the object is active, {@code false} if it is inactive
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Returns the payroll export name.
     *
     * @return the payroll export name
     */
    public String getPayrollExportName() {
        return payrollExportName;
    }

    /**
     * Sets the payroll export name.
     *
     * @param payrollExportName the payroll export name
     */
    public void setPayrollExportName(String payrollExportName) {
        this.payrollExportName = payrollExportName;
    }

    /**
     * Returns the address identifier.
     *
     * @return the address identifier
     */
    public long getAddress() {
        return address;
    }

    /**
     * Sets the address identifier.
     *
     * @param address the address identifier
     */
    public void setAddress(long address) {
        this.address = address;
    }

    /**
     * Returns the contact.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the contact.
     *
     * @param contact the contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Returns the roster sort order.
     *
     * @return the contact identifier
     */
    public long getRosterSortOrder() {
        return rosterSortOrder;
    }

    /**
     * Sets the roster sort order.
     *
     * @param rosterSortOrder the roster sort order
     */
    public void setRosterSortOrder(long rosterSortOrder) {
        this.rosterSortOrder = rosterSortOrder;
    }

    /**
     * Determines if the organisational unit is shown on the roster.
     *
     * @return {@code true} if it is shown on the roster
     */
    public boolean getShowOnRoster() {
        return showOnRoster;
    }

    /**
     * Determines if the organisational unit is shown on the roster.
     *
     * @param showOnRoster if {@code true} it is shown on the roster
     */
    public void setShowOnRoster(boolean showOnRoster) {
        this.showOnRoster = showOnRoster;
    }

    /**
     * Returns the organisational unit colour.
     *
     * @return the organisational unit colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * Sets the organisational unit colour.
     *
     * @param colour the organisational unit colour
     */
    public void setColour(String colour) {
        this.colour = colour;
    }

    /**
     * Returns the roster active hours schedule.
     *
     * @return the roster active hours schedule
     */
    public String getRosterActiveHoursSchedule() {
        return rosterActiveHoursSchedule;
    }

    /**
     * Sets the roster active hours schedule.
     *
     * @param rosterActiveHoursSchedule the roster active hours schedule
     */
    public void setRosterActiveHoursSchedule(String rosterActiveHoursSchedule) {
        this.rosterActiveHoursSchedule = rosterActiveHoursSchedule;
    }

    /**
     * Returns the daily roster budget.
     *
     * @return the daily roster budget
     */
    public String getDailyRosterBudget() {
        return dailyRosterBudget;
    }

    /**
     * Sets the daily roster budget.
     *
     * @param dailyRosterBudget the daily roster budget
     */
    public void setDailyRosterBudget(String dailyRosterBudget) {
        this.dailyRosterBudget = dailyRosterBudget;
    }

    /**
     * Returns the company code.
     *
     * @return the company code
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the company code.
     *
     * @param companyCode the company code
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * Returns the company name.
     *
     * @return the company name
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
