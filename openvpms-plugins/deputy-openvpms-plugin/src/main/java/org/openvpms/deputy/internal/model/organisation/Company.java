/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.model.organisation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Deputy Company.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company implements Resource {

    /**
     * The company identifier.
     */
    @JsonProperty("Id")
    private long id;

    /**
     * The portfolio.
     */
    @JsonProperty("Portfolio")
    private String portfolio = null;

    /**
     * The company code.
     */
    @JsonProperty("Code")
    private String code;

    /**
     * Determines if the company is active.
     */
    @JsonProperty("Active")
    private boolean active;

    /**
     * The parent company identifier.
     */
    @JsonProperty("ParentCompany")
    private long parentCompany;

    /**
     * The company name.
     */
    @JsonProperty("CompanyName")
    private String companyName;

    /**
     * The company trading name.
     */
    @JsonProperty("TradingName")
    private String tradingName;

    /**
     * The business number.
     */
    @JsonProperty("BusinessNumber")
    private String businessNumber;

    /**
     * The company number.
     */
    @JsonProperty("CompanyNumber")
    private String companyNumber;

    /**
     * Determines if the company is a workplace.
     */
    @JsonProperty("IsWorkplace")
    private boolean isWorkplace;

    /**
     * Determines if the company is a payroll entity.
     */
    @JsonProperty("IsPayrollEntity")
    private boolean isPayrollEntity;

    /**
     * The payroll export code.
     */
    @JsonProperty("PayrollExportCode")
    private String payrollExportCode;

    /**
     * The address identifier.
     */
    @JsonProperty("Address")
    private long address;

    /**
     * The contact identifier.
     */
    @JsonProperty("Contact")
    private long contact;

    /**
     * The creator identifier.
     */
    @JsonProperty("Creator")
    private long creator;

    /**
     * The created timestamp.
     */
    @JsonProperty("Created")
    private String created;

    /**
     * The modified timestamp.
     */
    @JsonProperty("Modified")
    private String modified;

    /**
     * Returns the object identifier.
     *
     * @return the object identifier
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets the company identifier.
     *
     * @param Id the company identifier
     */
    public void setId(long Id) {
        this.id = Id;
    }

    /**
     * Returns the object name.
     *
     * @return the object name
     */
    @Override
    public String getName() {
        return getCompanyName();
    }

    /**
     * Returns the portfolio.
     *
     * @return the portfolio. May be {@code null}
     */
    public String getPortfolio() {
        return portfolio;
    }

    /**
     * Sets the portfolio.
     *
     * @param portfolio the portfolio. May be {@code null}
     */
    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    /**
     * Returns the company code.
     *
     * @return the company code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the company code.
     *
     * @param code the company code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Determines if the object is active or not.
     *
     * @return {@code true} if the object is active, {@code false} if it is inactive
     */
    @Override
    public boolean getActive() {
        return active;
    }

    /**
     * Determines if the object is active or not.
     *
     * @param active if {@code true}, the object is active, {@code false} if it is inactive
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Returns the parent company identifier.
     *
     * @return the parent company identifier
     */
    public long getParentCompany() {
        return parentCompany;
    }

    /**
     * Sets the parent company identifier.
     *
     * @param parentCompany the parent company identifier
     */
    public void setParentCompany(long parentCompany) {
        this.parentCompany = parentCompany;
    }

    /**
     * Returns the company name.
     *
     * @return the company name
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Returns the company trading name.
     *
     * @return the company trading name
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * Sets the company trading name.
     *
     * @param tradingName the trading name
     */
    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    /**
     * Returns the business number.
     *
     * @return the business number
     */
    public String getBusinessNumber() {
        return businessNumber;
    }

    /**
     * Sets the business number.
     *
     * @param businessNumber the business number
     */
    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    /**
     * Returns the company number.
     *
     * @return the company number
     */
    public String getCompanyNumber() {
        return companyNumber;
    }

    /**
     * Sets the company number.
     *
     * @param companyNumber the company number
     */
    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    /**
     * Determines if the company is a workplace.
     *
     * @return {@code true} if the company is a workplace
     */
    public boolean getIsWorkplace() {
        return isWorkplace;
    }

    /**
     * Determines if the company is a workplace.
     *
     * @param isWorkplace if {@code true}, the company is a workplace
     */
    public void setIsWorkplace(boolean isWorkplace) {
        this.isWorkplace = isWorkplace;
    }

    /**
     * Determines if the company is a payroll entity.
     *
     * @return {@code true} if the company is a payroll entity
     */
    public boolean getIsPayrollEntity() {
        return isPayrollEntity;
    }

    /**
     * Determines if the company is a payroll entity.
     *
     * @param isPayrollEntity if {@code true}, the company is a payroll entity
     */
    public void setIsPayrollEntity(boolean isPayrollEntity) {
        this.isPayrollEntity = isPayrollEntity;
    }

    /**
     * Returns the payroll export code.
     *
     * @return the payroll export code
     */
    public String getPayrollExportCode() {
        return payrollExportCode;
    }

    /**
     * Sets the payroll export code.
     *
     * @param payrollExportCode the payroll export code
     */
    public void setPayrollExportCode(String payrollExportCode) {
        this.payrollExportCode = payrollExportCode;
    }

    /**
     * Returns the address identifier.
     *
     * @return the address identifier
     */
    public long getAddress() {
        return address;
    }

    /**
     * Sets the address identifier.
     *
     * @param address the address identifier
     */
    public void setAddress(long address) {
        this.address = address;
    }

    /**
     * Returns the contact identifier.
     *
     * @return the contact identifier
     */
    public long getContact() {
        return contact;
    }

    /**
     * Sets the contact identifier.
     *
     * @param contact the contact identifier
     */
    public void setContact(long contact) {
        this.contact = contact;
    }

    /**
     * Returns the creator identifier.
     *
     * @return the creator identifier
     */
    public long getCreator() {
        return creator;
    }

    /**
     * Sets the creator identifier.
     *
     * @param creator the creator identifier
     */
    public void setCreator(long creator) {
        this.creator = creator;
    }

    /**
     * Returns the created timestamp.
     *
     * @return the created timestamp
     */
    public String getCreated() {
        return created;
    }

    /**
     * Sets the created timestamp.
     *
     * @param created the created timestamp
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Returns the modified timestamp.
     *
     * @return the modified timestamp
     */
    public String getModified() {
        return modified;
    }

    /**
     * Sets the modified timestamp.
     *
     * @param modified the modified timestamp
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

}
