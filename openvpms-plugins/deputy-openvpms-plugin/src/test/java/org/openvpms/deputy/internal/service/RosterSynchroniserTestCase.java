/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.i18n.DeputyMessages;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;
import org.openvpms.mapping.model.DefaultTarget;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Targets;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.openvpms.plugin.internal.service.mapping.MappingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.deputy.internal.service.DeputyHelper.fromUnixtimestamp;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.createRoster;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.getUnixtimestamp;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.updateRoster;
import static org.openvpms.deputy.internal.service.SyncStatus.ERROR;
import static org.openvpms.deputy.internal.service.SyncStatus.SYNC;

/**
 * Tests the {@link RosterSynchroniser}.
 *
 * @author Tim Anderson
 */
public class RosterSynchroniserTestCase extends AbstractDeputyTest {

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The archetype service.
     */
    private ArchetypeService service;

    /**
     * The mapped areas.
     */
    private Mappings<Entity> areas;

    /**
     * The mapped users.
     */
    private Mappings<User> users;

    /**
     * Test location 1.
     */
    private Party location1;

    /**
     * Test area 1.
     */
    private Entity area1;

    /**
     * Test user 1.
     */
    private User user1;

    /**
     * Area 1 id.
     */
    private static final int AREA1 = 1001;

    /**
     * Area 2 id.
     */
    private static final int AREA2 = 1002;

    /**
     * Employee 1 id.
     */
    private static final int EMPLOYEE1 = 33;

    /**
     * Employee 2 id.
     */
    private static final int EMPLOYEE2 = 35;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        Party practice = practiceService.getPractice();
        if (practiceService.getServiceUser() == null) {
            IMObjectBean bean = getBean(practice);
            bean.setTarget("serviceUser", TestHelper.createUser());
        }
        service = new PluginArchetypeService((IArchetypeRuleService) getArchetypeService(), getLookupService(),
                                             practiceService);

        MappingService mappingService = new MappingServiceImpl(service);
        IMObject configuration = mappingService.createMappingConfiguration(Entity.class);
        areas = mappingService.createMappings(configuration, Entity.class, Archetypes.ROSTER_AREA,
                                              "Areas", Mockito.mock(Targets.class));
        users = mappingService.createMappings(configuration, User.class, Archetypes.USER,
                                              "Users", Mockito.mock(Targets.class));

        location1 = TestHelper.createLocation();
        area1 = ScheduleTestHelper.createRosterArea(location1);
        user1 = TestHelper.createUser();

        areas.add(area1, new DefaultTarget(Long.toString(AREA1), "Area 1", true));
        users.add(user1, new DefaultTarget(Long.toString(EMPLOYEE1), "Employee 1", true));
    }

    /**
     * Tests the {@link RosterSynchroniser#synchroniseFromEvent(Act)} method for a new event.
     */
    @Test
    public void testSynchroniseFromNewEvent() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Party location2 = ScheduleTestHelper.createLocation();
        Entity area2 = ScheduleTestHelper.createRosterArea(location2); // not synchronised
        User user2 = TestHelper.createUser("J", "Bloggs");  // not mapped

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);
        Act event2 = createEvent("2019-08-13 09:00:00", "2019-08-13 17:30:00", user1, area2, location2);
        Act event3 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event1);
        Roster roster2 = synchroniser.synchroniseFromEvent(event2);
        Roster roster3 = synchroniser.synchroniseFromEvent(event3);

        checkEvent(event1, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, roster1.getId(), SYNC);
        checkEvent(event2, "2019-08-13 09:00:00", "2019-08-13 17:30:00", user1, area2, location2, -1, null);
        checkEvent(event3, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1, location1, -1, ERROR,
                   "DEPUTY-0050: Cannot synchronise this shift with Deputy as J Bloggs is not mapped to an " +
                   "Employee in Deputy");

        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);
        assertNull(roster2);  // area2 not synchronised
        assertNull(roster3);  // user2 not synchronised
    }

    /**
     * Verifies that when an area changes on a synchronised event, this propagates to Deputy.
     */
    @Test
    public void testSynchroniseFromEventChangeArea() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Party location2 = ScheduleTestHelper.createLocation();
        Entity area2 = ScheduleTestHelper.createRosterArea(location2);
        areas.add(area2, new DefaultTarget(AREA2, "Area 2", true));

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event);
        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2);

        Roster updated = synchroniser.synchroniseFromEvent(event);
        checkRoster(updated, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA2);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2, location2, startId, SYNC);
    }

    /**
     * Verifies that when a user changes on a synchronised event, this propagates to Deputy.
     */
    @Test
    public void testSynchroniseFromEventChangeUser() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        User user2 = TestHelper.createUser();
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event);
        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1);

        Roster updated = synchroniser.synchroniseFromEvent(event);
        checkRoster(updated, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE2, AREA1);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1, location1, startId, SYNC);
    }

    /**
     * Verifies that if a synchronised event is changed to an area that is not synchronised, a sync error is raised.
     */
    @Test
    public void testSynchroniseFromChangedEventNoAreaMapping() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Party location2 = ScheduleTestHelper.createLocation();
        Entity area2 = ScheduleTestHelper.createRosterArea(location2); // not mapped

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event);
        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2);

        Roster updated = synchroniser.synchroniseFromEvent(event);
        assertNull(updated);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2, location2, startId, ERROR,
                   "DEPUTY-0053: Cannot synchronise this shift as the area 'XArea' does not exist in Deputy");
    }

    /**
     * Verifies that if a synchronised event is changed to a user that is not synchronised, a sync error is raised.
     */
    @Test
    public void testSynchroniseFromChangedEventNoUserMapping() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        User user2 = TestHelper.createUser("J", "Bloggs");  // not mapped

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event);
        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1);

        Roster updated = synchroniser.synchroniseFromEvent(event);
        assertNull(updated);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1, location1, startId, ERROR,
                   "DEPUTY-0050: Cannot synchronise this shift with Deputy as J Bloggs is not mapped to an " +
                   "Employee in Deputy");
    }

    /**
     * Verifies a user can be removed from an event and this propagates to Deputy.
     */
    @Test
    public void testSynchroniseFromEventNoUser() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event);
        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1);

        Roster updated = synchroniser.synchroniseFromEvent(event);
        checkRoster(updated, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", 0, AREA1);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, startId, SYNC);
    }

    /**
     * Tests synchronisation of an event created in OpenVPMS and then subsequently updated in Deputy.
     */
    @Test
    public void test2WaySyncFromEvent() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);
        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        Roster roster = synchroniser.synchroniseFromEvent(event1);
        checkRoster(roster, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        // now simulate an update from Deputy
        roster.setStartTime(getUnixtimestamp("2019-08-12T08:00:00+10:00"));
        roster.setEndTime(getUnixtimestamp("2019-08-12T16:30:00+10:00"));
        roster.setModified(new Date());

        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        checkEvent(updated, "2019-08-12 08:00:00", "2019-08-12 16:30:00", user1, area1, location1, roster.getId(), SYNC);
    }

    /**
     * Tests synchronisation of an event created and updated in Deputy.
     */
    @Test
    public void test2WaySyncFromRoster() {
        long id1 = RandomUtils.nextLong();
        Roster roster = createRoster(id1, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id1, SYNC);

        // change the roster and resync from the event
        updateRoster(roster, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE1, AREA1);
        Roster updated1 = synchroniser.synchroniseFromEvent(event);

        // verify updates
        checkEvent(event, "2019-08-12 10:00:00", "2019-08-12 18:30:00", user1, area1, location1, id1, SYNC);
        checkRoster(updated1, id1, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE1, AREA1);

        // change to an open roster
        updateRoster(roster, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", 0, AREA1);
        Roster updated2 = synchroniser.synchroniseFromEvent(event);

        // verify updates
        checkEvent(event, "2019-08-12 10:00:00", "2019-08-12 18:30:00", null, area1, location1, id1, SYNC);
        checkRoster(updated2, id1, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", 0, AREA1);

        // change to an unmapped user
        updateRoster(roster, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE2, AREA1);
        Roster update3 = synchroniser.synchroniseFromEvent(event);

        // verify updates
        checkEvent(event, "2019-08-12 10:00:00", "2019-08-12 18:30:00", null, area1, location1, id1, ERROR,
                   "DEPUTY-0051: Cannot synchronise this shift as the Deputy Employee 'Employee 35' is not mapped to " +
                   "an OpenVPMS User");
        checkRoster(update3, id1, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE2, AREA1);

        // change to an unmapped area
        updateRoster(roster, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE1, AREA2);
        Roster update4 = synchroniser.synchroniseFromEvent(event);

        // verify updates
        checkEvent(event, "2019-08-12 10:00:00", "2019-08-12 18:30:00", null, area1, location1, id1, ERROR,
                   "DEPUTY-0052: Cannot synchronise this shift as the Deputy Area 'Area 1002' is not mapped to an " +
                   "OpenVPMS Roster Area");
        checkRoster(update4, id1, "2019-08-12T10:00:00+10:00", "2019-08-12T18:30:00+10:00", EMPLOYEE1, AREA2);
    }

    /**
     * Verifies that when a synchronised event has been updated in Deputy, the event itself is updated.
     */
    @Test
    public void testSynchroniseEventUpdatedInDeputy() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00",
                                     EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        // now modify the roster in Deputy, but don't sync the change
        roster.setEndTime(getUnixtimestamp("2019-08-12T17:00:00+10:00"));
        roster.setModified(new Date());

        changeEvent(event, "2019-08-12 08:30:00", "2019-08-12 17:30:00", user1, area1);

        // synchronise the event, and verify the roster change replaces it. This is because there is
        // no modified timestamp on the act.rosterEvent to indicate which is newer.
        Roster updated = synchroniser.synchroniseFromEvent(event);
        checkRoster(updated, id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:00:00+10:00", EMPLOYEE1, AREA1);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:00:00", user1, area1, location1, id, SYNC);
    }

    /**
     * Verifies that when a synchronised event has been deleted in Deputy, the event itself will be removed if it
     * changes.
     */
    @Test
    public void testSynchroniseEventDeletedInDeputy() {
        long id = RandomUtils.nextLong();

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id);

        Deputy deputy = new TestDeputy();
        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        synchroniser.synchroniseFromEvent(event1);
        assertNull(get(event1));  // verify it has been deleted
    }

    /**
     * Tests the {@link RosterSynchroniser#synchroniseFromRoster(Roster, Rosters)} method for a new roster.
     */
    @Test
    public void testSynchroniseFromNewRoster() {
        long id1 = RandomUtils.nextLong();
        Roster roster1 = createRoster(id1, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy();
        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event1 = synchroniser.synchroniseFromRoster(roster1, new Rosters(deputy));
        assertNotNull(event1);
        checkEvent(event1, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id1, SYNC);

        // now sync a roster where there is no area mapping. No event should be created
        long id2 = RandomUtils.nextLong();
        Roster roster2 = createRoster(id2, "2019-08-13T09:00:00+10:00", "2019-08-13T17:30:00+10:00", EMPLOYEE1, AREA2);
        Act event2 = synchroniser.synchroniseFromRoster(roster2, new Rosters(deputy));
        assertNull(event2);

        // now sync a roster where there is no employee mapping. No event should be created
        long id3 = RandomUtils.nextLong();
        Roster roster3 = createRoster(id3, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE2, AREA1);
        Act event3 = synchroniser.synchroniseFromRoster(roster3, new Rosters(deputy));
        assertNull(event3);
    }

    /**
     * Verifies that when a roster is updated in Deputy to an area that is not mapped, the event is flagged as an error.
     * If the event is subsequently deleted in OpenVPMS, this won't propagate to Deputy.
     */
    @Test
    public void testSyncFromChangedRosterNoAreaMapping() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertNotNull(event);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        roster.setOperationalUnit(AREA2);
        roster.setModified(new Date());
        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        checkEvent(updated, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, ERROR,
                   "DEPUTY-0052: Cannot synchronise this shift as the Deputy Area 'Area 1002' is not mapped to an " +
                   "OpenVPMS Roster Area");

        // verify deleting the event does not propagate back to Deputy. The assumption is that Deputy has the correct
        // version.
        remove(updated);
        assertFalse(synchroniser.remove(event));
    }

    /**
     * Verifies that when a roster is updated in Deputy to a user that is not mapped, the event is flagged as an error.
     * If the event is subsequently deleted in OpenVPMS, this won't propagate to Deputy.
     */
    @Test
    public void testSyncFromChangedRosterNoUserMapping() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertNotNull(event);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        roster.setEmployee(EMPLOYEE2);
        roster.setModified(new Date());
        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        checkEvent(updated, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, ERROR,
                   "DEPUTY-0051: Cannot synchronise this shift as the Deputy Employee 'Employee 35' is not mapped to " +
                   "an OpenVPMS User");

        // verify deleting the event does not propagate back to Deputy. The assumption is that Deputy has the correct
        // version.
        remove(updated);
        assertFalse(synchroniser.remove(event));
        assertNotNull(deputy.getRoster(id));
    }

    /**
     * Verifies a user can be removed from a roster and this propagates to OpenVPMS.
     */
    @Test
    public void testSynchroniseFromRosterNoUser() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertNotNull(event);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        roster.setEmployee(0);
        roster.setModified(new Date());
        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertEquals(event, updated);
        checkEvent(updated, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, id, SYNC);
    }

    /**
     * Verifies roster area changes propagate to OpenVPMS.
     */
    @Test
    public void testSynchroniseFromRosterChangeArea() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Party location2 = ScheduleTestHelper.createLocation();
        Entity area2 = ScheduleTestHelper.createRosterArea(location2);
        areas.add(area2, new DefaultTarget(Long.toString(AREA2), "Area 2", true));

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertNotNull(event);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        // change the area and resync
        roster.setOperationalUnit(AREA2);
        roster.setModified(new Date());
        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertEquals(event, updated);
        checkEvent(updated, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2, location2, id, SYNC);
    }

    /**
     * Verifies roster employee changes propagate to OpenVPMS.
     */
    @Test
    public void testSynchroniseFromRosterChangeEmployee() {
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        User user2 = TestHelper.createUser();
        users.add(user2, new DefaultTarget(Long.toString(EMPLOYEE2), "Employee 2", true));

        Deputy deputy = new TestDeputy(roster);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act event = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertNotNull(event);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);

        // change the employee and resync
        roster.setEmployee(EMPLOYEE2);
        roster.setModified(new Date());
        Act updated = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertEquals(event, updated);
        checkEvent(updated, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user2, area1, location1, id, SYNC);
    }

    /**
     * Verifies that when an event is created that is the same as or overlaps one in Deputy, a synchronisation error
     * is raised.
     * <p/>
     * The error can be resolved by syncing from Deputy.
     */
    @Test
    public void testSyncFromEventWithOverlappingRoster() {
        Predicate<RosterData> predicate = data -> data.getEmployee() == EMPLOYEE1;
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId, predicate); // throws BadRequestException when EMPLOYEE1 used

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);
        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster response1 = synchroniser.synchroniseFromEvent(event);
        assertNull(response1);
        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, -1, ERROR,
                   "Overlap detected!");

        // now resync, but with no employee. The error should be cleared as it no longer overlaps
        changeEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1);
        Roster response2 = synchroniser.synchroniseFromEvent(event);
        assertNotNull(response2);

        checkEvent(event, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, response2.getId(), SYNC);
    }

    /**
     * Verifies that when a roster is created in Deputy that duplicates one in OpenVPMS, the event is linked to it.
     */
    @Test
    public void testSyncFromRosterToDuplicateEvent() {
        Deputy deputy = new TestDeputy(RandomUtils.nextLong());

        Act event = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1);
        long id = RandomUtils.nextLong();
        Roster roster = createRoster(id, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Act act = synchroniser.synchroniseFromRoster(roster, new Rosters(deputy));
        assertEquals(event, act);
        checkEvent(act, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, id, SYNC);
    }

    /**
     * Verifies that when a roster is created in Deputy that overlaps one in OpenVPMS with different details
     * a synchronisation error is raised.
     */
    @Test
    public void testSyncFromRosterToOverlappingEvent() {
        Deputy deputy = new TestDeputy(RandomUtils.nextLong());

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, 10); // synced
        Act event2 = createEvent("2019-08-13 09:00:00", "2019-08-13 17:30:00", user1, area1, location1);     // unsynced
        long id = RandomUtils.nextLong();
        Roster roster1 = createRoster(id, "2019-08-12T08:00:00+10:00", "2019-08-12T16:30:00+10:00", EMPLOYEE1, AREA1);
        Roster roster2 = createRoster(id, "2019-08-13T08:00:00+10:00", "2019-08-13T16:30:00+10:00", EMPLOYEE1, AREA1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        // try to synchronise a roster that overlaps a synchronised event
        Act act1 = synchroniser.synchroniseFromRoster(roster1, new Rosters(deputy));
        assertNull(act1);

        String message1 = DeputyMessages.syncedShiftOverlapsRoster(fromUnixtimestamp(roster1.getStartTime()),
                                                                   fromUnixtimestamp(roster1.getEndTime())).toString();
        checkEvent(event1, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, 10, ERROR, message1);

        // try to synchronise a roster that overlaps an unsynchronised event
        Act act2 = synchroniser.synchroniseFromRoster(roster2, new Rosters(deputy));
        assertNull(act2);

        String message2 = DeputyMessages.unsyncedShiftOverlapsRoster(
                fromUnixtimestamp(roster2.getStartTime()), fromUnixtimestamp(roster2.getEndTime())).toString();
        checkEvent(event2, "2019-08-13 09:00:00", "2019-08-13 17:30:00", user1, area1, location1, -1, ERROR, message2);
    }

    /**
     * Verifies that multiple open events (i.e. those not assigned to users) with overlapping times can be synced.
     */
    @Test
    public void testSyncFromOpenEvents() {
        long startId = RandomUtils.nextLong();
        Deputy deputy = new TestDeputy(startId);

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1);
        Act event2 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);
        Roster roster1 = synchroniser.synchroniseFromEvent(event1);
        Roster roster2 = synchroniser.synchroniseFromEvent(event2);

        checkRoster(roster1, startId, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", 0, AREA1);
        checkRoster(roster2, startId + 1, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", 0, AREA1);

        checkEvent(event1, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, roster1.getId(), SYNC);
        checkEvent(event2, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, roster2.getId(), SYNC);
    }

    /**
     * Verifies that multiple open rosters (i.e. those not assigned to employees) with overlapping times can be synced,
     * and will be linked to events that already exist.
     */
    @Test
    public void testSyncFromOpenRostersWithExistingEvents() {
        Deputy deputy = new TestDeputy(RandomUtils.nextLong());

        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1);
        Act event2 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1);
        Act event3 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1); // won't sync

        long id1 = RandomUtils.nextLong();
        long id2 = RandomUtils.nextLong();
        Roster roster1 = createRoster(id1, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", 0, AREA1);
        Roster roster2 = createRoster(id2, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", 0, AREA1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        Act act1 = synchroniser.synchroniseFromRoster(roster1, new Rosters(deputy));
        Act act2 = synchroniser.synchroniseFromRoster(roster2, new Rosters(deputy));

        assertEquals(event1, act1);
        assertEquals(event2, act2);

        checkEvent(event1, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, roster1.getId(), SYNC);
        checkEvent(event2, "2019-08-12 09:00:00", "2019-08-12 17:30:00", null, area1, location1, roster2.getId(), SYNC);
        checkEvent(event3, "2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, -1, null);
    }

    /**
     * Verifies that multiple open rosters (i.e. those not assigned to employees) with overlapping times can be synced.
     */
    @Test
    public void testSyncFromOpenRosters() {
        Deputy deputy = new TestDeputy(RandomUtils.nextLong());

        long id1 = RandomUtils.nextLong();
        long id2 = RandomUtils.nextLong();
        Roster roster1 = createRoster(id1, "2019-08-12T08:00:00+10:00", "2019-08-12T16:30:00+10:00", 0, AREA1);
        Roster roster2 = createRoster(id2, "2019-08-12T08:00:00+10:00", "2019-08-12T16:30:00+10:00", 0, AREA1);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        Act event1 = synchroniser.synchroniseFromRoster(roster1, new Rosters(deputy));
        Act event2 = synchroniser.synchroniseFromRoster(roster2, new Rosters(deputy));

        checkEvent(event1, "2019-08-12 08:00:00", "2019-08-12 16:30:00", null, area1, location1, roster1.getId(), SYNC);
        checkEvent(event2, "2019-08-12 08:00:00", "2019-08-12 16:30:00", null, area1, location1, roster2.getId(), SYNC);

    }

    /**
     * Tests the {@link RosterSynchroniser#remove(Act)} method.
     */
    @Test
    public void testRemove() {
        long id1 = RandomUtils.nextLong();
        long id2 = RandomUtils.nextLong();
        Roster roster1 = createRoster(id1, "2019-08-12T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);
        Roster roster2 = createRoster(id2, "2019-08-13T09:00:00+10:00", "2019-08-12T17:30:00+10:00", EMPLOYEE1, AREA1);

        Deputy deputy = new TestDeputy(roster1);

        Party location2 = ScheduleTestHelper.createLocation();
        Entity area2 = ScheduleTestHelper.createRosterArea(location2);

        RosterSynchroniser synchroniser = createRosterSynchroniser(deputy);

        // create an event that has been synced to Deputy
        Act event1 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area1, location1, roster1.getId());

        // create an event that has already been removed in Deputy
        Act event2 = createEvent("2019-08-13 09:00:00", "2019-08-13 17:30:00", user1, area1, location1, roster2.getId());

        // create an event that hasn't been synced (i.e. has no Deputy identity).
        Act event3 = createEvent("2019-08-14 09:00:00", "2019-08-14 17:30:00", user1, area1, location1);

        // create an event that hasn't been synced as there is no mapping for area2
        Act event4 = createEvent("2019-08-12 09:00:00", "2019-08-12 17:30:00", user1, area2, location2);

        remove(event1);
        remove(event2);
        remove(event3);
        remove(event4);

        assertTrue(synchroniser.remove(event1));
        assertFalse(synchroniser.remove(event2)); // threw NotFoundException
        assertFalse(synchroniser.remove(event3));
        assertFalse(synchroniser.remove(event4));
    }

    /**
     * Creates a new {@link RosterSynchroniser}.
     *
     * @param deputy the Deputy client.
     * @return a new synchroniser
     */
    private RosterSynchroniser createRosterSynchroniser(Deputy deputy) {
        return new RosterSynchroniser(areas, users, deputy, new QueryService(service), service);
    }

}
