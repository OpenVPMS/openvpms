/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.deputy.internal.model.roster.Roster;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openvpms.ws.util.ObjectMapperContextResolver.ISO_8601_SECOND_TIMEZONE;

/**
 * Deputy test helper methods.
 *
 * @author Tim Anderson
 */
public class DeputyTestHelper {

    /**
     * Converts an ISO date/time to a unix timestamp.
     *
     * @param isoDatetime the date/time
     * @return the unix timestamp
     */
    static long getUnixtimestamp(String isoDatetime) {
        Date date = getISODatetime(isoDatetime);
        return getUnixtimestamp(date);
    }

    /**
     * Converts a date to a unix timestamp.
     *
     * @param date the date
     * @return the unix timestamp
     */
    static long getUnixtimestamp(Date date) {
        return date.getTime() / 1000;
    }

    /**
     * Parses an ISO date/time to a {@code Date}.
     *
     * @param datetime the date/time
     * @return the parsed date
     */
    static Date getISODatetime(String datetime) {
        try {
            return (Date) new SimpleDateFormat(ISO_8601_SECOND_TIMEZONE).parseObject(datetime);
        } catch (ParseException exception) {
            throw new IllegalArgumentException("Invalid ISO date/time '" + datetime + "'", exception);
        }
    }

    /**
     * Creates a new {@link Roster}.
     *
     * @param id              the roster identifier
     * @param startTime       the start time
     * @param endTime         the end time
     * @param employee        the Deputy employee identifier
     * @param operationalUnit the Deputy operational unit identifier
     * @return a new {@code Roster}
     */
    static Roster createRoster(long id, String startTime, String endTime, long employee, long operationalUnit) {
        return createRoster(id, getISODatetime(startTime), getISODatetime(endTime), employee, operationalUnit);
    }

    /**
     * Creates a new {@link Roster}.
     *
     * @param id              the roster identifier
     * @param startTime       the start time
     * @param endTime         the end time
     * @param employee        the Deputy employee identifier
     * @param operationalUnit the Deputy operational unit identifier
     * @return a new {@code Roster}
     */
    static Roster createRoster(long id, Date startTime, Date endTime, long employee, long operationalUnit) {
        Roster roster = new Roster();
        roster.setId(id);
        updateRoster(roster, startTime, endTime, employee, operationalUnit);
        return roster;
    }

    /**
     * Updates a {@link Roster}.
     *
     * @param roster          the roster
     * @param startTime       the start time
     * @param endTime         the end time
     * @param employee        the Deputy employee identifier
     * @param operationalUnit the Deputy operational unit identifier
     */
    static void updateRoster(Roster roster, String startTime, String endTime, long employee, long operationalUnit) {
        updateRoster(roster, getISODatetime(startTime), getISODatetime(endTime), employee, operationalUnit);
    }

    /**
     * Updates a {@link Roster}.
     *
     * @param roster          the roster
     * @param startTime       the start time
     * @param endTime         the end time
     * @param employee        the Deputy employee identifier
     * @param operationalUnit the Deputy operational unit identifier
     */
    static void updateRoster(Roster roster, Date startTime, Date endTime, long employee, long operationalUnit) {
        roster.setStartTime(getUnixtimestamp(startTime));
        roster.setEndTime(getUnixtimestamp(endTime));
        roster.setEmployee(employee);
        roster.setOperationalUnit(operationalUnit);
        roster.setModified(new Date());
    }

}
