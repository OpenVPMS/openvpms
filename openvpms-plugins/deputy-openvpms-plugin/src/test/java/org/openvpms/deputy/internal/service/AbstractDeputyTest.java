/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.model.roster.Roster;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.createActIdentity;
import static org.openvpms.archetype.test.TestHelper.getDatetime;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.getUnixtimestamp;
import static org.openvpms.deputy.internal.service.SyncStatus.SYNC;

/**
 * Base class for Deputy tests.
 *
 * @author Tim Anderson
 */
abstract class AbstractDeputyTest extends ArchetypeServiceTest {

    /**
     * Verify an event matches that expected.
     *
     * @param event     the event
     * @param startTime the expected start time
     * @param endTime   the expected end time
     * @param user      the expected user
     * @param area      the expected area
     * @param location  the expected location
     * @param rosterId  the expected roster id, or {@code -1} if it is not synchronised
     * @param status    the expected sync status. May be {@code null}
     */
    protected void checkEvent(Act event, String startTime, String endTime, User user, Entity area, Party location,
                              long rosterId, String status) {
        checkEvent(event, startTime, endTime, user, area, location, rosterId, status, null);
    }

    /**
     * Verify an event matches that expected.
     *
     * @param event     the event
     * @param startTime the expected start time
     * @param endTime   the expected end time
     * @param user      the expected user
     * @param area      the expected area
     * @param location  the expected location
     * @param rosterId  the expected roster id
     * @param status    the expected sync status
     * @param error     the expected error. May be {@code null}
     */
    protected void checkEvent(Act event, String startTime, String endTime, User user, Entity area, Party location,
                              long rosterId, String status, String error) {
        checkEvent(event, getDatetime(startTime, true), getDatetime(endTime, true), user, area, location, rosterId,
                   status, error);
    }

    /**
     * Verify an event matches that expected.
     *
     * @param event     the event
     * @param startTime the expected start time
     * @param endTime   the expected end time
     * @param user      the expected user
     * @param area      the expected area
     * @param location  the expected location
     * @param rosterId  the expected roster id
     * @param status    the expected sync status
     */
    protected void checkEvent(Act event, Date startTime, Date endTime, User user, Entity area, Party location,
                              long rosterId, String status, String error) {
        event = get(event);
        assertNotNull(event);
        IMObjectBean bean = getBean(event);
        assertEquals(startTime, bean.getDate("startTime"));
        assertEquals(endTime, bean.getDate("endTime"));
        assertEquals(user, bean.getTarget("user"));
        assertEquals(area, bean.getTarget("schedule"));
        assertEquals(location, bean.getTarget("location"));
        checkSynchronisationId(event, rosterId, status, error);
    }

    /**
     * Checks the synchronisation id of an event.
     *
     * @param event    the event
     * @param rosterId the expected roster id
     * @param status   the expected status
     * @param error    the expected error message. May be {@code null}
     */
    protected void checkSynchronisationId(Act event, long rosterId, String status, String error) {
        long actualId = checkSynchronisationId(event, status, error);
        assertEquals(rosterId, actualId);
    }

    /**
     * Checks the synchronisation id of an event.
     *
     * @param event  the event
     * @param status the expected status
     * @param error  the expected error message. May be {@code null}
     * @return the roster id, or {@code -1} if there is none
     */
    protected long checkSynchronisationId(Act event, String status, String error) {
        ActIdentity identity = DeputyHelper.getSynchronisationId(getBean(event));
        long actualId = DeputyHelper.getId(identity);
        if (identity != null) {
            IMObjectBean idBean = getBean(identity);
            assertEquals(status, idBean.getString("status"));
            assertEquals(error, idBean.getString("error"));
        } else {
            assertNull(status);
            assertNull(error);
        }
        return actualId;
    }

    /**
     * Creates a new event.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param user      the user
     * @param area      the area
     * @param location  the location
     * @return a new event
     */
    protected Act createEvent(String startTime, String endTime, User user, Entity area, Party location) {
        return createEvent(startTime, endTime, user, area, location, -1);
    }

    /**
     * Creates a new event.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param user      the user
     * @param area      the area
     * @param location  the location
     * @param rosterId  the corresponding Deputy roster id, or {@code -1} if it has not been synchronised
     * @return a new event
     */
    protected Act createEvent(String startTime, String endTime, User user, Entity area, Party location, long rosterId) {
        return createEvent(getDatetime(startTime), getDatetime(endTime), user, area, location, rosterId);
    }

    /**
     * Creates a new event.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param user      the user
     * @param area      the area
     * @param location  the location
     * @param rosterId  the corresponding Deputy roster id, or {@code -1} if it has not been synchronised
     * @return a new event
     */
    protected Act createEvent(Date startTime, Date endTime, User user, Entity area, Party location, long rosterId) {
        Act event = ScheduleTestHelper.createRosterEvent(startTime, endTime, user, area, location);
        if (rosterId != -1) {
            ActIdentity identity = createActIdentity(Archetypes.EVENT_ID, Long.toString(rosterId));
            IMObjectBean bean = getBean(identity);
            bean.setValue("status", SYNC);
            event.addIdentity(identity);
        }
        save(event);
        return event;
    }

    /**
     * Changes an event.
     *
     * @param event     the event to change
     * @param startTime the new start time
     * @param endTime   the new end time
     * @param user      the new user
     * @param area      the new area
     */
    protected void changeEvent(Act event, String startTime, String endTime, User user, Entity area) {
        IMObjectBean bean = getBean(event);
        bean.setValue("startTime", getDatetime(startTime));
        bean.setValue("endTime", getDatetime(endTime));
        if (user == null) {
            bean.removeValues("user");
        } else {
            bean.setTarget("user", user);
        }
        bean.setTarget("schedule", area);
        bean.setTarget("location", getBean(area).getTargetRef("location"));
        bean.save();
    }

    /**
     * Verifies a roster matches that expected.,
     *
     * @param roster          the roster to check
     * @param id              the roster id
     * @param startTime       the expected start time
     * @param endTime         the expected end time
     * @param employee        the expected employee id
     * @param operationalUnit the expected operation unit id
     */
    protected void checkRoster(Roster roster, long id, String startTime, String endTime, long employee,
                               long operationalUnit) {
        assertNotNull(roster);
        assertEquals(id, roster.getId());
        assertEquals(getUnixtimestamp(startTime), roster.getStartTime());
        assertEquals(getUnixtimestamp(endTime), roster.getEndTime());
        assertEquals(employee, roster.getEmployee());
        assertEquals(operationalUnit, roster.getOperationalUnit());
    }
}
