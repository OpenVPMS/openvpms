/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugins.test.listener;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.plugin.service.archetype.IMObjectListener;
import org.osgi.service.component.annotations.Component;

/**
 * Test {@link IMObjectListener} implementation.
 *
 * @author Tim Anderson
 */
@Component(service = IMObjectListener.class, immediate = true)
public class TestListenerPlugin implements IMObjectListener {

    /**
     * The archetypes to receive notifications for.
     *
     * @return the archetypes
     */
    @Override
    public String[] getArchetypes() {
        return new String[]{"party.customerperson", "party.patientpet"};
    }

    /**
     * Invoked when an object is updated.
     *
     * @param object the object
     */
    @Override
    public void updated(IMObject object) {
        System.out.println("Updated: object=" + object.getId() + ", archetype=" + object.getArchetype()
                           + ", name=" + object.getName());
    }

    /**
     * invoked when an object is removed.
     *
     * @param object the object
     */
    @Override
    public void removed(IMObject object) {
        System.out.println("Removed: object=" + object.getId() + ", archetype=" + object.getArchetype()
                           + ", name=" + object.getName());
    }
}
