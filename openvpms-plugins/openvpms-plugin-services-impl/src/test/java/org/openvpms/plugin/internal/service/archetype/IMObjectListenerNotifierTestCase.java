/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.archetype;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.service.archetype.IMObjectListener;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link IMObjectListenerNotifier}.
 *
 * @author Tim Anderson
 */
public class IMObjectListenerNotifierTestCase extends ArchetypeServiceTest {

    /**
     * Tests notification.
     *
     * @throws Exception for any error
     */
    @Test
    public void testNotification() throws Exception {
        PluginManager manager = Mockito.mock(PluginManager.class);
        IMObjectListenerNotifier notifier = new IMObjectListenerNotifier(getArchetypeService(), manager);
        notifier.afterPropertiesSet();

        AtomicInteger updated = new AtomicInteger(0);
        AtomicInteger removed = new AtomicInteger();

        IMObjectListener service = new IMObjectListener() {
            @Override
            public String[] getArchetypes() {
                return new String[]{PatientArchetypes.PATIENT, CustomerArchetypes.PERSON};
            }

            @Override
            public void updated(IMObject object) {
                assertTrue(object.isA(PatientArchetypes.PATIENT, CustomerArchetypes.PERSON));
                updated.incrementAndGet();
            }

            @Override
            public void removed(IMObject object) {
                assertTrue(object.isA(PatientArchetypes.PATIENT, CustomerArchetypes.PERSON));
                removed.incrementAndGet();
            }
        };
        notifier.addService(service); // in practice, this is invoked via the ServiceTracker

        // these should generate notifications
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        Party patient3 = TestHelper.createPatient();
        Party customer1 = TestHelper.createCustomer();

        Product product1 = TestHelper.createProduct(); // no notifications

        // these should generate notifications
        remove(patient1);
        remove(patient2);
        remove(customer1);

        remove(product1); // no notifications

        Thread.sleep(1000);

        notifier.removeService(service); // in practice, this is invoked via the ServiceTracker

        // shouldn't generate notifications
        remove(patient3);
        TestHelper.createPatient(); // create a 4th patient.

        // cleanup
        notifier.destroy();

        assertEquals(4, updated.get()); // 3 patients, 1 customer
        assertEquals(3, removed.get()); // 2 patients, 1 customer
    }
}
