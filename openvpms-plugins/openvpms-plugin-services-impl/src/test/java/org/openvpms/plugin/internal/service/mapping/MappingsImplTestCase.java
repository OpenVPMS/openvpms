/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.mapping.exception.MappingException;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Target;
import org.openvpms.mapping.model.Targets;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link MappingsImpl} class.
 *
 * @author Tim Anderson
 */
public class MappingsImplTestCase extends ArchetypeServiceTest {

    /**
     * The archetype service.
     */
    @Autowired
    private IArchetypeRuleService service;

    /**
     * The plugin archetype service.
     */
    private PluginArchetypeService pluginArchetypeService;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getServiceUser()).thenReturn(
                new org.openvpms.component.business.domain.im.security.User());
        pluginArchetypeService = new PluginArchetypeService(service, getLookupService(), practiceService);
    }

    /**
     * Verifies that {@link MappingsImpl} can be used for mapping entities.
     */
    @Test
    public void testEntityMappings() {
        IMObject config1 = create(MappingArchetypes.ENTITY_MAPPINGS);
        SimpleTargets targets = new SimpleTargets("Employees", "EMP1", "EMP2", "EMP3", "EMP4");
        MappingsImpl<User> mappings1 = createEmployeeMapping(config1, targets);
        assertEquals(Cardinality.ONE_TO_ONE, mappings1.getCardinality());
        User user1 = TestHelper.createUser();
        User user2 = TestHelper.createUser();
        User user3 = TestHelper.createUser();
        Target emp1 = targets.getTarget("EMP1");
        Target emp2 = targets.getTarget("EMP2");
        Target emp3 = targets.getTarget("EMP3");
        Target emp4 = targets.getTarget("EMP4");

        mappings1.add(user1, emp1);
        mappings1.add(user2, emp2);
        mappings1.add(user3, emp3);
        mappings1.save();

        // now reload and verify the mappings exist
        IMObject config2 = get(config1);
        MappingsImpl<User> mappings2 = createEmployeeMapping(config2, targets);
        assertEquals(3, mappings2.getMappings().size());

        Mapping user1Mapping = mappings2.getMapping("EMP1");
        assertNotNull(user1Mapping);
        assertEquals(user1.getObjectReference(), user1Mapping.getSource());
        assertEquals(emp1, user1Mapping.getTarget());

        Mapping user4Mapping = mappings2.getMapping("EMP4");
        assertNull(user4Mapping);

        List<Target> unmapped = mappings2.getTargets(null, true, 0, -1);
        assertEquals(1, unmapped.size());
        assertEquals(emp4, unmapped.get(0));
    }

    /**
     * Verifies that {@link MappingsImpl} can be used for mapping lookups.
     */
    @Test
    public void testLookupMappings() {
        Lookup config1 = createLookMapping();

        SimpleTargets targets = new SimpleTargets("Species", "DOG", "CAT", "PIG", "COW");
        MappingsImpl<Lookup> mappings1 = createSpeciesMapping(config1, targets);
        assertEquals(Cardinality.ONE_TO_ONE, mappings1.getCardinality());
        Lookup canine = TestHelper.getLookup("lookup.species", "CANINE");
        Lookup feline = TestHelper.getLookup("lookup.species", "FELINE");
        Lookup bovine = TestHelper.getLookup("lookup.species", "BOVINE");
        Target dog = targets.getTarget("DOG");
        Target cat = targets.getTarget("CAT");
        Target cow = targets.getTarget("COW");
        Target pig = targets.getTarget("PIG");

        mappings1.add(canine, dog);
        mappings1.add(feline, cat);
        mappings1.add(bovine, cow);
        mappings1.save();

        // now reload and verify the mappings exist
        IMObject config2 = get(config1);
        MappingsImpl<Lookup> mappings2 = createSpeciesMapping(config2, targets);
        assertEquals(3, mappings2.getMappings().size());

        Mapping dogMapping = mappings2.getMapping("DOG");
        assertNotNull(dogMapping);
        assertEquals(canine.getObjectReference(), dogMapping.getSource());
        assertEquals(dog, dogMapping.getTarget());

        Mapping pigMapping = mappings2.getMapping("PIG");
        assertNull(pigMapping);

        List<Target> unmapped = mappings2.getTargets(null, true, 0, -1);
        assertEquals(1, unmapped.size());
        assertEquals(pig, unmapped.get(0));
    }

    /**
     * Verifies that a {@link MappingException} is thrown if a source is mapped twice for ONE_TO_ONE cardinality.
     */
    @Test
    public void testDuplicateSourceMapping() {
        Lookup config1 = createLookMapping();

        SimpleTargets targets = new SimpleTargets("Species", "DOG", "CAT", "OTHER");
        MappingsImpl<Lookup> mappings = createSpeciesMapping(config1, targets);
        assertEquals(Cardinality.ONE_TO_ONE, mappings.getCardinality());
        Lookup canine = TestHelper.getLookup("lookup.species", "CANINE");
        Target dog = targets.getTarget("DOG");
        Target other = targets.getTarget("OTHER");

        mappings.add(canine, dog);
        mappings.add(canine, other);
        try {
            mappings.save();
            fail("MappingException expected");
        } catch (MappingException exception) {
            assertEquals("MAP-0104: Multiple mappings exist for Canine (" + canine.getId() + ")",
                         exception.getMessage());
        }
    }

    /**
     * Verifies that a {@link MappingException} is thrown if a target is mapped twice for ONE_TO_ONE cardinality.
     */
    @Test
    public void testOneToOneMappingForDuplicateTargetException() {
        Lookup config1 = createLookMapping();

        SimpleTargets targets = new SimpleTargets("Species", "DOG", "CAT", "OTHER");
        MappingsImpl<Lookup> mappings = createSpeciesMapping(config1, targets);
        assertEquals(Cardinality.ONE_TO_ONE, mappings.getCardinality());
        Lookup canine = TestHelper.getLookup("lookup.species", "CANINE");
        Lookup feline = TestHelper.getLookup("lookup.species", "FELINE");
        Lookup bovine = TestHelper.getLookup("lookup.species", "BOVINE");
        Target dog = targets.getTarget("DOG");
        Target other = targets.getTarget("OTHER");

        mappings.add(canine, dog);
        mappings.add(feline, other);
        mappings.add(bovine, other);

        try {
            mappings.save();
            fail("Expected MappingException");
        } catch (MappingException exception) {
            assertEquals("MAP-0105: Multiple mappings exist for OTHER (OTHER)", exception.getMessage());
        }
    }

    /**
     * Verifies many to one mapping is supported.
     */
    @Test
    public void testManyToOne() {
        Lookup config1 = createLookMapping();
        IMObjectBean bean = service.getBean(config1);
        bean.setValue("cardinality", "N:1");

        SimpleTargets targets = new SimpleTargets("Species", "DOG", "CAT", "UNKNOWN");
        MappingsImpl<Lookup> mappings1 = createSpeciesMapping(config1, targets);
        assertEquals(Cardinality.MANY_TO_ONE, mappings1.getCardinality());
        Lookup canine = TestHelper.getLookup("lookup.species", "CANINE");
        Lookup feline = TestHelper.getLookup("lookup.species", "FELINE");
        Lookup bovine = TestHelper.getLookup("lookup.species", "BOVINE");
        Lookup other = TestHelper.getLookup("lookup.species", "OTHER");
        Target dog = targets.getTarget("DOG");
        Target cat = targets.getTarget("CAT");
        Target unknown = targets.getTarget("UNKNOWN");

        mappings1.add(canine, dog);
        mappings1.add(feline, cat);
        mappings1.add(bovine, unknown);
        mappings1.add(other, unknown);
        mappings1.save();

        // now reload and verify the mappings exist
        IMObject config2 = get(config1);
        MappingsImpl<Lookup> mappings2 = createSpeciesMapping(config2, targets);
        assertEquals(4, mappings2.getMappings().size());

        Mapping dogMapping = mappings2.getMapping("DOG");
        assertNotNull(dogMapping);
        assertEquals(canine.getObjectReference(), dogMapping.getSource());
        assertEquals(dog, dogMapping.getTarget());

        Mapping catMapping = mappings2.getMapping("CAT");
        assertNotNull(catMapping);
        assertEquals(feline.getObjectReference(), catMapping.getSource());
        assertEquals(cat, catMapping.getTarget());

        List<Mapping> unknownMappings = mappings2.getMappings("UNKNOWN");
        assertEquals(2, unknownMappings.size());

        Mapping unknownMapping1 = unknownMappings.get(0);
        Mapping unknownMapping2 = unknownMappings.get(1);
        assertEquals(unknown, unknownMapping1.getTarget());
        assertTrue(unknownMapping1.getSource().equals(bovine.getObjectReference())
                   || unknownMapping1.getSource().equals(other.getObjectReference()));
        assertEquals(unknown, unknownMapping2.getTarget());
        assertTrue(unknownMapping2.getSource().equals(bovine.getObjectReference())
                   || unknownMapping2.getSource().equals(other.getObjectReference()));
    }

    /**
     * Creates a lookup mapping.
     *
     * @return the lookup mapping
     */
    private Lookup createLookMapping() {
        Lookup config = create(MappingArchetypes.LOOKUP_MAPPINGS, Lookup.class);
        config.setCode(config.getLinkId().toUpperCase());
        return config;
    }

    /**
     * Creates a mapping of users to employees.
     *
     * @param config  the configuration
     * @param targets the employee targets
     * @return a new mapping
     */
    private MappingsImpl<User> createEmployeeMapping(IMObject config, Targets targets) {
        return new MappingsImpl<>(config, User.class, "security.user", "Employees",
                                  "Users", targets, pluginArchetypeService);
    }

    /**
     * Creates a species mapping.
     *
     * @param config  the configuration
     * @param targets the species targets
     * @return a new mapping
     */
    private MappingsImpl<Lookup> createSpeciesMapping(IMObject config, Targets targets) {
        return new MappingsImpl<>(config, Lookup.class, "lookup.species", "Species",
                                  "Species", targets, pluginArchetypeService);
    }

}
