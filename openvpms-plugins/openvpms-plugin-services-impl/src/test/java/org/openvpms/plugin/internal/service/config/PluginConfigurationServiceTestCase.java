/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.hl7.TestHL7Factory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.service.config.ConfigurableService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PluginConfigurationService}.
 *
 * @author Tim Anderson
 */
public class PluginConfigurationServiceTestCase extends ArchetypeServiceTest {

    /**
     * The HL7 factory.
     */
    @Autowired
    private TestHL7Factory hl7Factory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The configuration service.
     */
    private PluginConfigurationService configurationService;

    /**
     * The archetype to use.
     */
    private static final String ARCHETYPE = "entity.HL7ServicePharmacyGroup";

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // remove all instances of the test archetype
        CriteriaBuilder builder = getArchetypeService().getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, ARCHETYPE);
        query.select(root);
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : getArchetypeService().createQuery(query).getResultList()) {
            remove(entity);
        }

        PluginManager manager = Mockito.mock(PluginManager.class);
        configurationService = new PluginConfigurationService(getArchetypeService(), manager);
        configurationService.afterPropertiesSet();
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        configurationService.destroy();
    }

    /**
     * Verifies that when a {@link ConfigurableService} is registered, it will be populated with the active
     * configuration with the lowest id.
     */
    @Test
    public void testRegisterServiceWithActiveConfig() {
        createConfig("Z Group 1", false);
        IMObject config2 = createConfig("Z Group 2", true);
        createConfig("Z Group 3", true);

        TestService service = new TestService();
        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertEquals(config2, service.getConfiguration());
        assertEquals(1, service.getCalls());
    }

    /**
     * Verifies that if there is no active config, the setConfiguration() is invoked with a {@code null}.
     */
    @Test
    public void testRegisterServiceWithInactiveConfig() {
        TestService service = new TestService();
        assertEquals(0, service.getCalls());

        createConfig("Z Group 1", false);

        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertNull(service.getConfiguration());
        assertEquals(1, service.getCalls());
    }

    /**
     * Verifies that if there is no config, the setConfiguration() is invoked with a {@code null}.
     */
    @Test
    public void testRegisterServiceWithNoConfig() {
        TestService service = new TestService();
        assertEquals(0, service.getCalls());

        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertNull(service.getConfiguration());
        assertEquals(1, service.getCalls());
    }

    /**
     * Verifies that when a configuration is deactivated, null is passed to setConfiguration().
     *
     * @throws Exception for any error
     */
    @Test
    public void testDeactivateConfig() throws Exception {
        IMObject config1 = createConfig("Z Group 1", true);
        createConfig("Z Group 2", false);   // shouldn't be used

        TestService service = new TestService();
        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertEquals(config1, service.getConfiguration());
        assertEquals(1, service.getCalls());

        // verify that deactivating the config removes it from the service
        deactivate(config1);
        Thread.sleep(500);

        assertNull(service.getConfiguration());
        assertEquals(2, service.getCalls());
    }

    /**
     * Verifies that when a configuration is deactivated and another active configuration is available, it will be
     * supplied.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDeactivateConfigWithFallback() throws Exception {
        IMObject config1 = createConfig("Z Group 1", true);
        IMObject config2 = createConfig("Z Group 2", true);
        createConfig("Z Group 3", false);  // shouldn't be used

        TestService service = new TestService();
        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertEquals(config1, service.getConfiguration());
        assertEquals(1, service.getCalls());

        // deactivate config1, and verify config2 is used
        deactivate(config1);
        Thread.sleep(500);
        assertEquals(config2, service.getConfiguration());
        assertEquals(2, service.getCalls());

        // deactivate config2, and verify null is passed
        deactivate(config2);
        Thread.sleep(500);
        assertNull(service.getConfiguration());
    }

    /**
     * Verifies that when a configuration is removed, null is passed to setConfiguration().
     *
     * @throws Exception for any error
     */
    @Test
    public void testRemoveConfig() throws Exception {
        IMObject config1 = createConfig("Z Group 1", true);

        TestService service = new TestService();
        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertEquals(config1, service.getConfiguration());
        assertEquals(1, service.getCalls());

        // now remove and verify the config is set to null
        remove(config1);
        Thread.sleep(500);

        assertNull(service.getConfiguration());
        assertEquals(2, service.getCalls());
    }

    /**
     * Verifies that when a configuration is removed and another active configuration is available, it will be
     * supplied.
     *
     * @throws Exception for any error
     */
    @Test
    public void testRemoveConfigWithFallback() throws Exception {
        IMObject config1 = createConfig("Z Group 1", true);
        IMObject config2 = createConfig("Z Group 2", true);
        createConfig("Z Group 3", false);  // shouldn't be used

        TestService service = new TestService();
        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertEquals(config1, service.getConfiguration());
        assertEquals(1, service.getCalls());

        // remove config1, and verify config2 is used
        remove(config1);
        Thread.sleep(500);
        assertEquals(config2, service.getConfiguration());
        assertEquals(2, service.getCalls());

        // remove config2, and verify null is passed
        remove(config2);
        Thread.sleep(500);
        assertNull(service.getConfiguration());
    }

    /**
     * Verifies that a {@link ConfigurableService} receives configuration changes.
     *
     * @throws Exception for any error
     */
    @Test
    public void testChangeConfig() throws Exception {
        TestService service = new TestService();

        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker

        assertNull(service.getConfiguration());
        assertEquals(1, service.getCalls());

        IMObject config1 = createConfig("Z Group 1", true);
        Thread.sleep(500);
        assertEquals(config1, service.getConfiguration());
        assertEquals(2, service.getCalls());

        // verify that deactivating the config removes it from the service
        config1 = get(config1);      // need to reload otherwise it will change the instance held by the service
        config1.setActive(false);
        save(config1);
        Thread.sleep(500);
        assertNull(service.getConfiguration());
        assertEquals(3, service.getCalls());

        // verify that saving a new version updates the service
        IMObject config2 = createConfig("Z Group 2", true);
        Thread.sleep(500);
        assertEquals(config2, service.getConfiguration());
        assertEquals(4, service.getCalls());

        // verify that reactivating the original config doesn't update the service
        config1 = get(config1);
        config1.setActive(true);
        save(config1);
        Thread.sleep(500);
        assertEquals(config2, service.getConfiguration());
        assertEquals(4, service.getCalls());

        // now remove config2 and verify that it falls back to config1
        remove(config2);
        Thread.sleep(500);
        assertEquals(config1, service.getConfiguration());
        assertEquals(5, service.getCalls());

        // now remove config1, and remove the service has no config
        remove(config1);
        Thread.sleep(500);
        assertNull(service.getConfiguration());
        assertEquals(6, service.getCalls());

        // remove the service, and verify it no longer receives updates.
        configurationService.removeService(service);

        IMObject config3 = createConfig("Z Group 3", true);
        save(config3);
        Thread.sleep(500);
        assertEquals(6, service.getCalls());

        remove(config3);
        Thread.sleep(500);
        assertEquals(6, service.getCalls());
    }

    /**
     * Verifies that when a relationship in an object changes, the service is notified.
     *
     * @throws Exception for any error
     */
    @Test
    public void testUpdateRelationship() throws Exception {
        IMObject config = createConfig("Z Group", true);
        IMObject related = createRelated();
        IMObjectBean bean = getBean(config);
        Relationship relationship = bean.setTarget("services", related);
        bean.save();
        long version = config.getVersion();

        TestService service = new TestService();

        configurationService.addService(service); // in practice, this is invoked via the ServiceTracker
        Thread.sleep(500);
        assertEquals(config, service.getConfiguration());
        assertEquals(1, service.getCalls());

        relationship.setDescription("a change");
        save(config);
        assertEquals(version, config.getVersion());
        // verify the version didn't update, although the relationship changed

        // verify the service received notification of the change
        Thread.sleep(500);
        assertEquals(config, service.getConfiguration());
        assertEquals(2, service.getCalls());

        // in order to support notifications when a relationship updates, saving an object with no changes will
        // also trigger a notification
        save(config);
        assertEquals(version, config.getVersion());
        Thread.sleep(500);
        assertEquals(config, service.getConfiguration());
        assertEquals(3, service.getCalls());
    }

    /**
     * Deactivates a configuration.
     *
     * @param config the configuration
     */
    private void deactivate(IMObject config) {
        config = get(config);      // need to reload otherwise it will change the instance held by the service
        config.setActive(false);
        save(config);
    }

    /**
     * Creates and saves a new configuration.
     *
     * @param name   the configuration name
     * @param active determines if the configuration is active or not
     * @return the configuration
     */
    private IMObject createConfig(String name, boolean active) {
        IMObject config = create(ARCHETYPE);
        config.setName(name);
        config.setActive(active);
        save(config);
        return config;
    }

    private IMObject createRelated() {
        return hl7Factory.newHL7Pharmacy()
                .user(userFactory.createUser())
                .defaultSenderReceiver()
                .location(practiceFactory.createLocation())
                .build();
    }

    private static class TestService implements ConfigurableService {

        /**
         * Ccounts the number of calls to setConfiguration().
         */
        private final AtomicInteger calls = new AtomicInteger(0);

        private IMObject config;

        @Override
        public String getArchetype() {
            return ARCHETYPE;
        }

        @Override
        public synchronized void setConfiguration(IMObject config) {
            assertTrue(config == null || config.isA(ARCHETYPE));
            calls.incrementAndGet();
            this.config = config;
        }

        @Override
        public synchronized IMObject getConfiguration() {
            return config;
        }

        int getCalls() {
            return calls.get();
        }
    }
}
