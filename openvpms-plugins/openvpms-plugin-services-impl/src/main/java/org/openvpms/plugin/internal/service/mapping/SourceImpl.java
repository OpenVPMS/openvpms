/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Source;

/**
 * Default implementation of {@link Source}.
 *
 * @author Tim Anderson
 */
class SourceImpl implements Source {

    /**
     * The source object reference.
     */
    private final Reference reference;

    /**
     * The source object display name.
     */
    private final String name;

    /**
     * Constructs a {@link SourceImpl}.
     *
     * @param reference the source object reference
     * @param name      the source object display name
     */
    SourceImpl(Reference reference, String name) {
        this.reference = reference;
        this.name = name;
    }

    /**
     * Returns the source object identifier.
     *
     * @return the source object identifier
     */
    @Override
    public Reference getId() {
        return reference;
    }

    /**
     * Returns a display name for the source object.
     *
     * @return a display name for the source object
     */
    @Override
    public String getName() {
        return name;
    }

}
