/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Target;

/**
 * Default implementation of {@link Mapping}.
 *
 * @author Tim Anderson
 */
class IMObjectMapping implements Mapping {

    /**
     * The configuration.
     */
    private final IMObject config;

    /**
     * The OpenVPMS identifier for the object.
     */
    private Reference source;

    /**
     * The target object.
     */
    private Target target;

    /**
     * Constructs a {@link IMObjectMapping}.
     *
     * @param config the configuration
     * @param source the OpenVPMS identifier for the object
     * @param target the target
     */
    IMObjectMapping(IMObject config, Reference source, Target target) {
        this.config = config;
        this.source = source;
        this.target = target;
    }

    /**
     * Returns the configuration.
     *
     * @return the configuration
     */
    public IMObject getConfig() {
        return config;
    }

    /**
     * Returns the OpenVPMS identifier for this object.
     *
     * @return the OpenVPMS identifier
     */
    @Override
    public Reference getSource() {
        return source;
    }

    /**
     * Sets the OpenVPMS identifier for the object.
     *
     * @param source the source
     */
    public void setSource(Reference source) {
        this.source = source;
    }

    /**
     * Returns the target object.
     *
     * @return the target object
     */
    @Override
    public Target getTarget() {
        return target;
    }

    /**
     * Sets the target object.
     *
     * @param target the target object
     */
    public void setTarget(Target target) {
        this.target = target;
    }
}