/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.archetype;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeServiceListener;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.manager.PluginManagerListener;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Notifies listeners when objects are updated or removed.
 * <p/>
 * This processes notifications asynchronously so that plugins don't block user threads.
 *
 * @author Tim Anderson
 */
public abstract class IMObjectUpdateNotifier<T> implements InitializingBean, DisposableBean {

    /**
     * The plugin manager listener.
     */
    private final PluginManagerListener pluginManagerListener;

    /**
     * The listener type.
     */
    private final Class<T> type;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The plugin manager.
     */
    private final PluginManager manager;

    /**
     * Performs asynchronous notification.
     */
    private final ScheduledExecutorService executor;

    /**
     * Thread id name counter.
     */
    private final AtomicLong counter = new AtomicLong(0);

    /**
     * The listeners.
     */
    private final Map<T, Listener> listeners = Collections.synchronizedMap(new HashMap<>());

    /**
     * Tracks registration of listeners.
     */
    private volatile ServiceTracker<T, T> tracker;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectUpdateNotifier.class);

    /**
     * Constructs an {@link IMObjectUpdateNotifier}.
     *
     * @param type    the listener type
     * @param service the archetype service
     * @param manager the plugin manager
     */
    public IMObjectUpdateNotifier(Class<T> type, IArchetypeService service, PluginManager manager) {
        this.type = type;
        pluginManagerListener = new PluginManagerListener() {
            @Override
            public void started() {
                onStart();
            }

            @Override
            public void stopped() {

            }
        };
        this.service = service;
        this.manager = manager;
        executor = Executors.newSingleThreadScheduledExecutor(
                runnable -> new Thread(runnable, "IMObjectUpdateNotifier" + counter.incrementAndGet()));
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     */
    @Override
    public void afterPropertiesSet() {
        manager.addListener(pluginManagerListener);
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     */
    @Override
    public void destroy() {
        Listener[] listeners = Iterables.toArray(this.listeners.values(), Listener.class);
        for (Listener listener : listeners) {
            try {
                listener.unregister(service);
            } catch (Exception exception) {
                log.warn("Failed to unregister listener for archetypes=" + StringUtils.join(listener.archetypes, ','));
            }
        }

        try {
            executor.shutdown();
        } catch (Exception exception) {
            log.error("Failed to shutdown Executor: {}", exception.getMessage(), exception);
        }
        try {
            ServiceTracker<T, T> tracker = this.tracker;
            if (tracker != null) {
                tracker.close();
            }
        } catch (Exception exception) {
            log.error("Warning: failed to close ServiceTracker: {}", exception.getMessage(), exception);
        }
        manager.removeListener(pluginManagerListener);
    }

    /**
     * Creates a listener.
     *
     * @param service the service to register a listener for
     * @return the listener
     */
    protected abstract Listener createListener(T service);

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Adds a service.
     *
     * @param service the service to add
     */
    protected void addService(T service) {
        try {
            Listener listener = createListener(service);
            listeners.put(service, listener);
            listener.register(this.service);
        } catch (Exception exception) {
            log.warn("Failed to add {}", service.getClass(), exception);
        }
    }

    /**
     * Removes a service.
     *
     * @param service the service to remove
     */
    protected void removeService(T service) {
        Listener listener = listeners.remove(service);
        if (listener != null) {
            listener.unregister(this.service);
        }
    }

    /**
     * Invoked when the plugin manager starts.
     */
    private void onStart() {
        BundleContext bundleContext = manager.getBundleContext();
        if (bundleContext != null) {
            tracker = getServiceTracker(bundleContext);
            tracker.open();
        } else {
            log.error("Cannot start IMObjectUpdateNotifier for {}: no BundleContext", type);
        }
    }

    /**
     * Creates a service tracker.
     *
     * @param bundleContext the bundle context
     * @return a new service tracker
     */
    private ServiceTracker<T, T> getServiceTracker(BundleContext bundleContext) {
        return new ServiceTracker<T, T>(bundleContext, type, null) {
            /**
             * Invoked when a service is added.
             *
             * @param reference The reference to the service being added to this {@code ServiceTracker}.
             * @return The service object to be tracked for the service added to this {@code ServiceTracker}.
             */
            @Override
            public T addingService(ServiceReference<T> reference) {
                T service = super.addingService(reference);
                if (service != null) {
                    addService(service);
                }
                return service;
            }

            /**
             * Invoked when a service is removed.
             *
             * @param reference The reference to removed service.
             * @param service   The service object for the removed service.
             */
            @Override
            public void removedService(ServiceReference<T> reference, T service) {
                removeService(service);
                super.removedService(reference, service);
            }
        };
    }

    protected class Listener {
        private final String[] archetypes;

        /**
         * A listener that schedules notification to the actual listener. This prevents plugins from
         * blocking user threads.
         */
        private final IArchetypeServiceListener delegate;

        public Listener(String[] archetypes, IArchetypeServiceListener listener) {
            this.archetypes = archetypes;
            this.delegate = new IArchetypeServiceListener() {

                @Override
                public void save(IMObject object) {
                    executor.execute(() -> listener.save(object));
                }

                @Override
                public void remove(IMObject object) {
                    executor.execute(() -> listener.remove(object));
                }

                @Override
                public void saved(IMObject object) {
                    executor.execute(() -> listener.saved(object));
                }

                @Override
                public void removed(IMObject object) {
                    executor.execute(() -> listener.removed(object));
                }

                @Override
                public void rollback(IMObject object) {
                    executor.execute(() -> listener.rollback(object));
                }
            };
        }

        private void register(IArchetypeService service) {
            for (String archetype : archetypes) {
                service.addListener(archetype, delegate);
            }
        }

        private void unregister(IArchetypeService service) {
            for (String archetype : archetypes) {
                service.removeListener(archetype, delegate);
            }
        }
    }
}
