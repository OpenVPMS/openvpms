/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import org.apache.felix.framework.Logger;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Manifest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ExportPackages} class.
 *
 * @author Tim Anderson
 */
public class ExportPackagesTestCase {

    /**
     * Tests export packages.
     * <p/>
     * NOTE: this will fail if run within IntelliJ, as it doesn't put the OpenVPMS jars on the classpath.
     */
    @Test
    public void testExportPackages() {
        ExportPackages packages = ExportPackages.create(new Logger());
        Map<String, String> map = packages.getPackages();

        // verify OpenVPMS frameworks packages are included
        assertTrue(map.containsKey("org.openvpms.component.model.act"));

        // verify the most recent commons-io version is exported. The commons-io jar exports both a 1.4.9999 and 2.6.0
        // version
        assertEquals("2.6.0", map.get("org.apache.commons.io"));
    }

    /**
     * Verifies that if there are two jars exporting the same packages, the highest version is selected.
     */
    @Test
    public void testMultipleVersionsExported() {
        Map<URL, Manifest> manifests = new HashMap<>();
        addManifest(manifests, "commons-io-2.1-MANIFEST.MF", "repository/commons-io-2.1.jar");
        addManifest(manifests, "commons-io-2.6-MANIFEST.MF", "commons-io-2.6.jar");

        ExportPackages packages = new TestExportPackages(manifests);

        checkJarIncludes(packages, "commons-io-2.6.jar");
        checkJarExcludes(packages, "commons-io-2.1.jar");

        assertEquals("2.6.0", packages.getPackages().get("org.apache.commons.io"));
    }

    /**
     * Verifies that if a jar exports multiple versions, the highest version is selected.
     * <p/>
     * This tests using the commons-io MANIFEST.MF which exports both a 1.4.9999 and 2.6.0 version.
     */
    @Test
    public void testMultipleVersionsExportedFromOneJar() {
        Map<URL, Manifest> manifests = new HashMap<>();
        addManifest(manifests, "commons-io-2.6-MANIFEST.MF", "commons-io-2.6.jar");

        ExportPackages packages = new TestExportPackages(manifests);

        checkJarIncludes(packages, "commons-io-2.6.jar");
        checkJarExcludes(packages);

        assertEquals("2.6.0", packages.getPackages().get("org.apache.commons.io"));
    }

    /**
     * Verifies that if jsr305-*.jar is on the classpath, it is excluded as it erroneously exports javax.annotation.
     * See OVPMS-2622.
     */
    @Test
    public void testJSR305Excluded() {
        Map<URL, Manifest> manifests = new HashMap<>();

        // simulate r
        addManifest(manifests, "javax.annotation-api-MANIFEST.MF", "javax.annotation-api-1.3.2.jar");
        addManifest(manifests, "jsr305-MANIFEST.MF", "WEB-INF/lib/jsr305-3.0.2.jar");

        ExportPackages packages = new TestExportPackages(manifests);

        Map<String, String> map = packages.getPackages();
        assertEquals("1.3.2", map.get("javax.annotation"));
        assertNull(map.get("javax.annotation.meta"));  // included by jsr305

        checkJarIncludes(packages, "javax.annotation-api-1.3.2.jar");
        checkJarExcludes(packages, "jsr305-3.0.2.jar");
    }

    /**
     * Verifies that if a jar is included on the classpath twice, with different paths, it is only included once.
     * This can occur if running with maven overlays.
     */
    @Test
    public void testJarWithSameNameButDifferentPath() {
        Map<URL, Manifest> manifests = new HashMap<>();

        // javax.annotation-api-MANIFEST.MF corresponds to the javax.annotation API used by Jetty 9.4.x
        addManifest(manifests, "javax.annotation-api-MANIFEST.MF", "WEB/INF/lib/javax.annotation-api-1.3.2.jar");
        addManifest(manifests, "javax.annotation-api-MANIFEST.MF", "repository/javax.annotation-api-1.3.2.jar");

        ExportPackages packages = new TestExportPackages(manifests);

        checkJarIncludes(packages, "javax.annotation-api-1.3.2.jar");
        checkJarExcludes(packages);
    }

    /**
     * Verifies that if a servlet container doesn't provide headers, default exports will be added.
     */
    @Test
    public void testContainerJarWithoutOSGiHeaders() {
        Map<URL, Manifest> manifests = new HashMap<>();

        // annotations-api-MANIFEST.MF corresponds to the javax.annotation API used by Tomcat 8
        addManifest(manifests, "annotations-api-MANIFEST.MF", "annotations-api.jar");

        ExportPackages packages = new TestExportPackages(manifests);
        checkJarIncludes(packages, "annotations-api.jar");
        checkJarExcludes(packages);

        assertEquals("1.3.5", packages.getPackages().get("javax.annotation"));
    }

    /**
     * Verifies that the correct jars are included.
     *
     * @param packages the packages
     * @param jars     the expected jars
     */
    private void checkJarIncludes(ExportPackages packages, String... jars) {
        assertEquals(Arrays.asList(jars), packages.getJarIncludes());
    }

    /**
     * Verifies that the correct jars are excluded.
     *
     * @param packages the packages
     * @param jars     the expected jars
     */
    private void checkJarExcludes(ExportPackages packages, String... jars) {
        assertEquals(Arrays.asList(jars), packages.getJarExcludes());
    }

    /**
     * Adds a MANIFEST.MF resource.
     *  @param manifests the cache of manifests, keyed on a (dummy) jar URL
     * @param path      the MANIFEST.MF resource path
     * @param jar       the jar that the MANIFEST.MF represents
     */
    private void addManifest(Map<URL, Manifest> manifests, String path, String jar) {
        URL resource = getClass().getResource(path);
        assertNotNull(resource);
        try {
            Manifest manifest = new Manifest(resource.openStream());
            manifests.put(new URL("jar:file:/" + jar + "!/META-INF/MANIFEST.MF"), manifest);
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to add manifest", exception);
        }
    }

    private static class TestExportPackages extends ExportPackages {

        private final Map<URL, Manifest> manifests;

        public TestExportPackages(Map<URL, Manifest> manifests) {
            this.manifests = manifests;
            init(Collections.enumeration(manifests.keySet()), new Logger());
        }

        /**
         * Returns the manifest for the specified URL.
         *
         * @param url the URL
         * @return the manifest, or {@code null} if none can be read
         */
        @Override
        protected Manifest getManifest(URL url) {
            return manifests.get(url);
        }
    }

}
