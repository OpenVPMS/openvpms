/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.plugin.internal.manager.atlassian.servlet.ServletContainerContextImpl;
import org.openvpms.plugin.test.service.TestService;
import org.openvpms.plugin.test.service.impl.TestServiceImpl;
import org.openvpms.plugins.test.api.TestPlugin;
import org.osgi.framework.Bundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;

import javax.servlet.http.HttpServlet;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PluginManagerImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PluginManagerImplTestCase extends ArchetypeServiceTest {

    /**
     * Resource base path for the servlet context.
     */
    @Rule
    public TemporaryFolder dir = new TemporaryFolder();

    /**
     * The test service.
     */
    private TestServiceImpl testService;

    /**
     * The host component provider.
     */
    private HostComponentProvider provider;

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAO pluginDAO;

    /**
     * Sets up the test case.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        testService = new TestServiceImpl();
        provider = registrar -> {
            registrar.register(TestService.class).forInstance(testService);
            registrar.register(UserManager.class).forInstance(Mockito.mock(UserManager.class));
            registrar.register(ThreadLocalContextManager.class)
                    .forInstance(Mockito.mock(ThreadLocalContextManager.class));
        };
        PluginHelper.removeAll(pluginDAO);
        PluginHelper.deployAll(pluginDAO);
    }

    /**
     * Verifies that services can be provided to a plugin, and that the plugin can call them.
     * <p>
     * This provides a {@link TestService} to the {@code TestPluginImpl} plugin, which calls the {@link TestService}
     * with its next value.
     *
     * @throws Exception for any error
     */
    @Test
    public void testPluginManager() throws Exception {
        String pluginDir = PluginHelper.getPluginDir();

        testService.setValue(10);

        // start the plugin manager
        MockServletContext servletContext = new MockServletContext(PluginHelper.getServletRoot());
        ServletContainerContextImpl containerContext = new ServletContainerContextImpl();
        containerContext.setServletContext(servletContext);
        PluginManagerImpl manager = new PluginManagerImpl(provider, pluginDAO, containerContext,
                                                          practiceService);
        manager.start();

        // make sure directories have been packaged correctly
        Path system = Paths.get(pluginDir, "system");
        Path bundled = Paths.get(pluginDir, "bundled");
        Path deploy = Paths.get(pluginDir, "deploy");
        assertTrue(Files.isDirectory(system));
        assertTrue(Files.isDirectory(bundled));

        for (int i = 0; i < 20; ++i) {
            if (testService.getValue() > 10) {
                break;
            } else {
                Thread.sleep(1000);
            }
        }

        // verify the service was called by the plugin
        assertEquals(11, testService.getValue());

        // now verify the plugin was exported
        assertNotNull(manager.getService(TestPlugin.class));

        // verify the plugin can be uninstalled
        Bundle test = null;
        for (Bundle bundle : manager.getBundles()) {
            if (bundle.getSymbolicName().equals("openvpms-test-plugin")) {
                test = bundle;
                break;
            }
        }
        assertNotNull(test);
        String key = OsgiHeaderUtil.getPluginKey(test);
        assertNotNull(pluginDAO.getPlugin(key));

        // verify the plugin can be uninstalled
        assertTrue(manager.canUninstall(test));
        manager.uninstall(test);

        // verify it has been removed
        assertNull(pluginDAO.getPlugin(key));

        // verify the service is no longer exported
        assertNull(manager.getService(TestPlugin.class));

        manager.stop();

        // verify the deploy directory was created
        assertTrue(Files.isDirectory(deploy));
    }

    /**
     * Verifies that servlet plugins are supported.
     *
     * @throws Exception for any error
     */
    @Test
    public void testServletPlugin() throws Exception {
        // start the plugin manager
        MockServletContext servletContext = new MockServletContext(PluginHelper.getServletRoot());
        ServletContainerContextImpl containerContext = new ServletContainerContextImpl();
        containerContext.setServletContext(servletContext);
        PluginManagerImpl manager = new PluginManagerImpl(provider, pluginDAO, containerContext, practiceService);
        manager.start();

        ServletModuleManager servletModuleManager = manager.getServletModuleManager();
        assertNotNull(servletModuleManager);

        MockServletConfig servletConfig = new MockServletConfig(servletContext, "test-servlet");
        HttpServlet servlet = servletModuleManager.getServlet("/test-servlet", servletConfig);
        assertNotNull(servlet);

        MockHttpServletResponse response = new MockHttpServletResponse();
        servlet.service(new MockHttpServletRequest("GET", "/test-servlet"), response);

        String text = StringUtils.trim(response.getContentAsString());

        assertEquals(SC_OK, response.getStatus());
        assertEquals("Hello World", text);

        manager.stop();
    }

    /**
     * Verifies that rest plugins are supported.
     */
    @Test
    public void testRestPlugin() throws Exception {
        // start the plugin manager
        MockServletContext servletContext = new MockServletContext(PluginHelper.getServletRoot());
        ServletContainerContextImpl containerContext = new ServletContainerContextImpl();
        containerContext.setServletContext(servletContext);
        PluginManagerImpl manager = new PluginManagerImpl(provider, pluginDAO, containerContext,
                                                          practiceService);
        manager.start();

        ServletModuleManager servletModuleManager = manager.getServletModuleManager();
        assertNotNull(servletModuleManager);

        // need to use the servlet module manager to get access to the rest filter
        MockServletConfig servletConfig = new MockServletConfig(servletContext, "test-servlet");
        HttpServlet servlet = servletModuleManager.getServlet("/test-servlet", servletConfig);
        assertNotNull(servlet);

        MockHttpServletResponse response = new MockHttpServletResponse();
        servlet.service(new MockHttpServletRequest("GET", "/hello"), response);

        String text = StringUtils.trim(response.getContentAsString());

        assertEquals(SC_OK, response.getStatus());
        assertEquals("Hello World", text);

        manager.stop();
    }

}
