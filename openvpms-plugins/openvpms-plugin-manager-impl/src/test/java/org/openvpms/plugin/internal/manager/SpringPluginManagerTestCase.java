/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.plugin.internal.manager.atlassian.servlet.ServletContainerContextImpl;
import org.openvpms.plugin.test.service.impl.TestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link PluginManagerImpl} when the plugin services and {@link HostComponentProvider} are bootstrapped from
 * a Spring context.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("/applicationContext.xml")
public class SpringPluginManagerTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The plugin service provider.
     */
    @Autowired
    private HostComponentProvider provider;

    /**
     * The test service.
     */
    @Autowired
    private TestServiceImpl testService;

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAO pluginDAO;

    /**
     * Sets up the test case.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        PluginHelper.removeAll(pluginDAO);
        PluginHelper.deployAll(pluginDAO);
    }

    /**
     * Verifies that the {@link TestServiceImpl} defined in the Spring context is exposed to the {@code TestPluginImpl}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testPluginManager() throws Exception {
        assertEquals(0, testService.getValue());

        MockServletContext servletContext = new MockServletContext(PluginHelper.getServletRoot());
        ServletContainerContextImpl containerContext = new ServletContainerContextImpl();
        containerContext.setServletContext(servletContext);

        PluginManagerImpl pluginManager = new PluginManagerImpl(provider, pluginDAO, containerContext,
                                                                practiceService);
        pluginManager.start();
        Thread.sleep(7000);
        pluginManager.stop();
        long value = testService.getValue();

        assertEquals(1, value);
    }
}
