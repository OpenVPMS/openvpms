/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.hostcontainer.SimpleConstructorHostContainer;
import com.atlassian.plugin.main.PluginsConfiguration;
import com.atlassian.plugin.main.PluginsConfigurationBuilder;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.module.ClassPrefixModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.schema.impl.DefaultDescribedModuleDescriptorFactory;
import com.atlassian.plugin.scope.EverythingIsActiveScopeManager;
import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceManagerImpl;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.sal.core.message.SystemDefaultLocaleResolver;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.apache.felix.framework.Logger;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.plugin.internal.manager.atlassian.ApplicationPropertiesImpl;
import org.openvpms.plugin.internal.manager.atlassian.DarkFeatureManagerImpl;
import org.openvpms.plugin.internal.manager.atlassian.I18nResolverImpl;
import org.openvpms.plugin.internal.manager.atlassian.WebResourceIntegrationImpl;
import org.openvpms.plugin.internal.manager.atlassian.WebSudoManagerImpl;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.manager.PluginManagerListener;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.singleton;


/**
 * OpenVPMS Plugin manager.
 *
 * @author Tim Anderson
 */
public class PluginManagerImpl implements PluginManager {

    /**
     * The plugin service provider.
     */
    private final Provider provider;

    /**
     * The plugin DAO.
     */
    private final PluginDAO pluginDAO;

    /**
     * The servlet container context.
     */
    private final ServletContainerContext context;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The Felix logger.
     */
    private final Logger logger = new Logger();

    /**
     * The listeners.
     */
    private final Set<PluginManagerListener> listeners = Collections.synchronizedSet(new HashSet<>());

    /**
     * Atlassian plugin framework.
     */
    private AtlassianPlugins plugins;

    /**
     * The servlet module manager.
     */
    private ServletModuleManager servletModuleManager;

    /**
     * The logger.
     */
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(PluginManagerImpl.class);

    /**
     * Constructs a {@link PluginManagerImpl}.
     *
     * @param provider        the plugin service provider
     * @param pluginDAO       the plugin DAO
     * @param context         the servlet container context
     * @param practiceService the practice service
     */
    PluginManagerImpl(HostComponentProvider provider, PluginDAO pluginDAO, ServletContainerContext context,
                      PracticeService practiceService) {
        this.provider = new Provider(provider);
        this.pluginDAO = pluginDAO;
        this.context = context;
        this.practiceService = practiceService;
    }

    /**
     * Returns the first service implementing the specified interface.
     *
     * @param type the interface
     * @return the first service implementing the interface, or {@code null} if none was found
     */
    @Override
    public <T> T getService(Class<T> type) {
        T result = null;
        BundleContext context = getBundleContext();
        if (context != null) {
            ServiceReference<T> reference = context.getServiceReference(type);
            if (reference != null) {
                result = getService(reference, context);
            }
        }
        return result;
    }

    /**
     * Returns all services implementing the specified interface.
     *
     * @param type the interface
     * @return the services implementing the interface
     */
    @Override
    public <T> List<T> getServices(Class<T> type) {
        List<T> result = new ArrayList<>();
        BundleContext context = getBundleContext();
        if (context != null) {
            try {
                Collection<ServiceReference<T>> references = context.getServiceReferences(type, null);
                for (ServiceReference<T> reference : references) {
                    T service = getService(reference, context);
                    if (service != null) {
                        result.add(service);
                    }
                }
            } catch (InvalidSyntaxException ignore) {
                // do nothing
            }
        }
        return result;
    }

    /**
     * Returns the bundle context, or {@code null} if the manager is not running.
     *
     * @return the bundle context. May be {@code null}
     */
    public synchronized BundleContext getBundleContext() {
        if (plugins != null) {
            Bundle[] bundles = plugins.getOsgiContainerManager().getBundles();
            // system bundle is always the first
            return bundles[0].getBundleContext();
        }
        return null;
    }

    /**
     * Returns a list of all installed bundles.
     *
     * @return the installed bundles
     */
    @Override
    public Bundle[] getBundles() {
        BundleContext context = getBundleContext();
        return context != null ? context.getBundles() : new Bundle[0];
    }

    /**
     * Starts the plugin manager.
     */
    @Override
    public synchronized void start() {
        if (plugins == null) {
            File dir = getPluginDir("/WEB-INF/plugins");
            File system = getDir(dir, "system", false);
            File bundled = getDir(dir, "bundled", false);
            File deploy = getDir(dir, "deploy", true);     // deployment cache for db plugins
            File cache = getDir(dir, "cache", true);       // OSGi cache

            Bootstrapper bootstrapper = new Bootstrapper(system, bundled, deploy, cache);
            plugins = bootstrapper.getAlassianPlugins();
            servletModuleManager = bootstrapper.getServletModuleManager();
            plugins.afterPropertiesSet();
            plugins.getPluginSystemLifecycle().init();
            notifyStarted();
        }
    }

    /**
     * Determines if the plugin manager is started.
     *
     * @return {@code true} if the plugin manager is started
     */
    @Override
    public synchronized boolean isStarted() {
        return plugins != null;
    }

    /**
     * Stops the plugin manager.
     * <p>
     * This method will wait until the manager shuts down.
     */
    @Override
    public synchronized void stop() {
        if (plugins != null) {
            plugins.getPluginSystemLifecycle().shutdown();
            plugins.destroy();

            plugins = null;
            servletModuleManager = null;
            notifyStopped();
        }
    }

    /**
     * Installs a plugin from a file.
     * <p>
     * The plugin manager must be started for this operation to be successful.
     *
     * @param file the file
     * @throws BundleException if the plugin cannot be installed
     */
    @Override
    public synchronized void install(File file) throws BundleException {
        PluginArtifact artifact = new JarPluginArtifact(file);
        plugins.getPluginController().installPlugins(artifact);
    }

    /**
     * Determines if a bundle can be uninstalled.
     *
     * @param bundle the bundle
     * @return {@code true} if the bundle is a plugin that can be uninstalled
     */
    @Override
    public boolean canUninstall(Bundle bundle) {
        String key = getPluginKey(bundle);
        return pluginDAO.getPlugin(key) != null;
    }

    /**
     * Uninstalls a bundle.
     * <p>
     * The plugin manager must be started for this operation to be successful.
     *
     * @param bundle the bundle
     * @throws BundleException if the bundle cannot be uninstalled
     */
    @Override
    public synchronized void uninstall(Bundle bundle) throws BundleException {
        if (plugins == null) {
            throw new BundleException("PluginManager not running");
        }
        String key = getPluginKey(bundle);
        if (!pluginDAO.remove(key)) {
            throw new BundleException("Cannot uninstall plugin for key: " + key);
        }
        Plugin plugin = plugins.getPluginAccessor().getPlugin(key);
        if (plugin == null) {
            throw new BundleException("Plugin not found: " + key);
        }
        pluginDAO.remove(key);
        try {
            plugins.getPluginController().uninstall(plugin);
        } catch (PluginException exception) {
            throw new BundleException("Failed to uninstall bundle: " + exception.getMessage(), exception);
        }
    }

    /**
     * Determines if a bundle can be restarted.
     *
     * @param bundle the bundle
     * @return {@code true} if the bundle is a plugin that can be restarted
     */
    @Override
    public synchronized boolean canRestart(Bundle bundle) {
        return plugins != null && plugins.getPluginAccessor().getPlugin(getPluginKey(bundle)) != null;
    }

    /**
     * Start a bundle.
     *
     * @param bundle the bundle to start
     */
    @Override
    public synchronized void start(Bundle bundle) {
        if (canRestart(bundle)) {
            plugins.getPluginController().enablePlugins(getPluginKey(bundle));
        }
    }

    /**
     * Stop a bundle.
     *
     * @param bundle the bundle to stop
     */
    @Override
    public synchronized void stop(Bundle bundle) {
        if (canRestart(bundle)) {
            plugins.getPluginController().disablePlugin(getPluginKey(bundle));
        }
    }

    /**
     * Scan for and deploy new plugins.
     *
     * @return the number of new plugins found
     */
    @Override
    public synchronized int scanForNewPlugins() {
        int result = 0;
        if (plugins != null) {
            result = plugins.getPluginController().scanForNewPlugins();
        }
        return result;
    }

    /**
     * Adds a listener to be notified of plugin manager events.
     *
     * @param listener the listener to notify
     */
    @Override
    public void addListener(PluginManagerListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    @Override
    public void removeListener(PluginManagerListener listener) {
        listeners.remove(listener);
    }

    /**
     * Returns the servlet module manager.
     *
     * @return Returns the servlet module manager
     */
    ServletModuleManager getServletModuleManager() {
        return servletModuleManager;
    }

    /**
     * Returns the Atlassian plugin key for a bundle.
     *
     * @param bundle the bundle
     * @return the plugin key
     */
    private String getPluginKey(Bundle bundle) {
        return OsgiHeaderUtil.getPluginKey(bundle);
    }

    /**
     * Returns a service given its reference.
     *
     * @param reference the reference
     * @param context   the bundle context
     * @return the service, or {@code null}, if it doesn't exist
     */
    private <T> T getService(ServiceReference<T> reference, BundleContext context) {
        T result = null;
        try {
            result = context.getService(reference);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Returns the plugin directory.
     *
     * @param path the path relative to the servlet context root
     * @return the plugin directory
     */
    private File getPluginDir(String path) {
        String realPath = context.getServletContext().getRealPath(path);
        File dir = new File(realPath);
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IllegalStateException("Invalid plugin directory: " + path);
        }
        return dir;
    }

    /**
     * Returns a directory, optionally creating it if it doesn't exist.
     *
     * @param parent the parent directory
     * @param path   the child path
     * @param create if {@code true}, create the directory if it doesn't exist
     * @return the directory
     * @throws IllegalArgumentException if the directory is invalid or cannot be created
     */
    private File getDir(File parent, String path, boolean create) {
        File dir = new File(parent, path);
        if (!dir.exists()) {
            if (!create) {
                throw new IllegalArgumentException("Directory doesn't exist: " + dir);
            }
            if (!dir.mkdirs()) {
                throw new IllegalArgumentException("Failed to create directory: " + dir);
            }
        } else if (!dir.isDirectory()) {
            throw new IllegalArgumentException("Invalid directory: " + dir);
        }

        return dir;
    }

    /**
     * Notify listeners that the plugin manager has started.
     */
    private void notifyStarted() {
        PluginManagerListener[] list = listeners.toArray(new PluginManagerListener[0]);
        for (PluginManagerListener listener : list) {
            try {
                listener.started();
            } catch (Throwable exception) {
                log.error("PluginManagerListener threw exception", exception);
            }
        }
    }

    /**
     * Notify listeners that the plugin manager has stopped.
     */
    private void notifyStopped() {
        PluginManagerListener[] list = listeners.toArray(new PluginManagerListener[0]);
        for (PluginManagerListener listener : list) {
            try {
                listener.stopped();
            } catch (Throwable exception) {
                log.error("PluginManagerListener threw exception", exception);
            }
        }
    }

    private class Bootstrapper {

        private final AtlassianPlugins plugins;

        private final HostContainer hostContainer;

        private final ServletModuleManager servletModuleManager;

        /**
         * Constructs a {@link Bootstrapper}.
         *
         * @param system  the directory for system bundles
         * @param bundled the directory for pre-installed bundles
         * @param user    the directory for user-installed bundles
         * @param cache   the OSGi cache directory
         */
        Bootstrapper(File system, File bundled, File user, File cache) {
            ExportPackages exportPackages = ExportPackages.create(logger);

            // Use a delegating host container since the real one requires the created object map, which won't be
            // available until later
            HostContainer delegatingHostContainer = new HostContainer() {
                public <T> T create(final Class<T> moduleClass) throws IllegalArgumentException {
                    return hostContainer.create(moduleClass);
                }
            };
            DefaultModuleDescriptorFactory moduleDescriptorFactory
                    = new DefaultDescribedModuleDescriptorFactory(delegatingHostContainer);
            moduleDescriptorFactory.addModuleDescriptor("servlet", ServletModuleDescriptor.class);
            moduleDescriptorFactory.addModuleDescriptor("servlet-filter", ServletFilterModuleDescriptor.class);
            moduleDescriptorFactory.addModuleDescriptor("servlet-context-param",
                                                        ServletContextParamModuleDescriptor.class);
            moduleDescriptorFactory.addModuleDescriptor("servlet-context-listener",
                                                        ServletContextListenerModuleDescriptor.class);
            moduleDescriptorFactory.addModuleDescriptor("web-resource", WebResourceModuleDescriptor.class);

            // APF goes out of its way to use whatever is on the classpath, despite the package includes and versions
            // supplied. Need to exclude jars that don't export packages
            DefaultPackageScannerConfiguration scannerConfig = new DefaultPackageScannerConfiguration();
            scannerConfig.setServletContext(context.getServletContext());
            scannerConfig.setPackageVersions(exportPackages.getPackages());
            List<String> packageIncludes = new ArrayList<>(exportPackages.getPackageIncludes());
            packageIncludes.addAll(scannerConfig.getPackageIncludes());
            scannerConfig.setPackageIncludes(packageIncludes);
            scannerConfig.setJarIncludes(exportPackages.getJarIncludes());
            scannerConfig.setJarExcludes(exportPackages.getJarExcludes());

            URL bundledPluginUrl;
            try {
                bundledPluginUrl = bundled.toURI().toURL();
            } catch (MalformedURLException exception) {
                // shouldn't occur
                throw new IllegalStateException("Failed to get URL for bundled plugin directory: " + bundled,
                                                exception);
            }
            PluginsConfiguration config = new PluginsConfigurationBuilder()
                    .frameworkBundleDirectory(system)
                    .bundledPluginCacheDirectory(bundled)
                    .bundledPluginUrl(bundledPluginUrl)
                    .pluginDirectory(user)
                    .moduleDescriptorFactory(moduleDescriptorFactory)
                    .packageScannerConfiguration(scannerConfig)
                    .hostComponentProvider(provider)
                    .osgiPersistentCache(cache)
                    .pluginStateStore(new MemoryPluginPersistentStateStore()) // TODO
                    .build();

            plugins = new AtlassianPlugins(config, pluginDAO);

            PrefixDelegatingModuleFactory moduleFactory
                    = new PrefixDelegatingModuleFactory(singleton(new BeanPrefixModuleFactory()));
            PluginEventManager pluginEventManager = plugins.getPluginEventManager();

            servletModuleManager = new DefaultServletModuleManager(context.getServletContext(), pluginEventManager);
            PluginAccessor pluginAccessor = plugins.getPluginAccessor();

            I18nResolver i18nResolver = new I18nResolverImpl(pluginAccessor, pluginEventManager);

            WebResourceIntegration webResourceIntegration = new WebResourceIntegrationImpl(
                    pluginAccessor,
                    new SystemDefaultLocaleResolver(),
                    i18nResolver,
                    pluginEventManager, context);
            WebResourceUrlProvider webResourceUrlProvider = new WebResourceUrlProviderImpl(webResourceIntegration);

            ServletContextFactory servletContextFactory = context::getServletContext;
            PluginResourceLocator pluginResourceLocator = new PluginResourceLocatorImpl(
                    webResourceIntegration, servletContextFactory, webResourceUrlProvider, pluginEventManager);

            PrebakeWebResourceAssemblerFactory prebakeWebResourceAssemblerFactory
                    = new DefaultWebResourceAssemblerFactory(pluginResourceLocator);

            DefaultPageBuilderService pageBuilderService
                    = new DefaultPageBuilderService(webResourceIntegration, prebakeWebResourceAssemblerFactory);

            EventPublisher eventPublisher = plugins.getEventPublisher();

            WebResourceManager webResourceManager = new WebResourceManagerImpl(
                    pluginResourceLocator, webResourceIntegration, webResourceUrlProvider);

            // TODO - hack to workaround OVPMS-2317
            System.setProperty(FelixOsgiContainerManager.ATLASSIAN_BOOTDELEGATION_EXTRA, "META-INF");

            provider.register(eventPublisher, EventPublisher.class);
            provider.register(i18nResolver, I18nResolver.class);
            provider.register(context, HttpContext.class);
            provider.register(webResourceIntegration, WebResourceIntegration.class);
            // required by atlassian-plugins-webresource-plugin
            provider.register(webResourceUrlProvider, WebResourceUrlProvider.class);
            provider.register(webResourceManager, WebResourceManager.class);
            // required by atlassian-template-renderer-velocity-16-plugin
            provider.register(servletModuleManager, ServletModuleManager.class);
            XsrfTokenAccessor accessor = new IndependentXsrfTokenAccessor();
            provider.register(accessor, XsrfTokenAccessor.class);
            XsrfTokenValidator xsrfTokenValidator = new IndependentXsrfTokenValidator(accessor);
            provider.register(xsrfTokenValidator, XsrfTokenValidator.class);
            provider.register(new XsrfRequestValidatorImpl(xsrfTokenValidator), XsrfRequestValidator.class);
            provider.register(moduleFactory, ModuleFactory.class, DescribedModuleDescriptorFactory.class);
            provider.register(pageBuilderService, PageBuilderService.class);
            provider.register(pluginResourceLocator, PluginResourceLocator.class);
            provider.register(prebakeWebResourceAssemblerFactory, PrebakeWebResourceAssemblerFactory.class);
            provider.register(new HttpClientRequestFactory(), NonMarshallingRequestFactory.class);
            provider.register(new EverythingIsActiveScopeManager(), ScopeManager.class);
            provider.register(new ApplicationPropertiesImpl(webResourceIntegration, context, practiceService),
                              ApplicationProperties.class);
            provider.register(new DarkFeatureManagerImpl(), DarkFeatureManager.class);
            provider.register(new WebSudoManagerImpl(), WebSudoManager.class);


            // These components are required for the early startup phase where plugins are being deployed.
            // TODO - integrate with Provider
            Map<Class<?>, Object> publicContainer = new HashMap<>();
            publicContainer.put(DescribedModuleDescriptorFactory.class, moduleDescriptorFactory);
            publicContainer.put(ListableModuleDescriptorFactory.class, moduleDescriptorFactory);
            publicContainer.put(ModuleDescriptorFactory.class, moduleDescriptorFactory);
            publicContainer.put(ModuleFactory.class, moduleFactory);
            publicContainer.put(ServletContextFactory.class, servletContextFactory);
            publicContainer.put(ServletModuleManager.class, servletModuleManager);

            hostContainer = new SimpleConstructorHostContainer(publicContainer);
            moduleFactory.addPrefixModuleFactory(new ClassPrefixModuleFactory(hostContainer));
        }

        AtlassianPlugins getAlassianPlugins() {
            return plugins;
        }

        ServletModuleManager getServletModuleManager() {
            return servletModuleManager;
        }

    }

    /**
     * Helper to expose services to plugins, including those supplied at construction (typically configured
     * in the Spring application context), and additional services required by Atlassian plugins constructed internally.
     */
    private static class Provider implements HostComponentProvider {

        /**
         * The external service provider.
         */
        private final HostComponentProvider provider;

        /**
         * Services registered internally.
         */
        private final Map<Object, Class<?>[]> objects = new HashMap<>();

        /**
         * Constructs a {@link Provider}.
         *
         * @param provider the external service provider
         */
        Provider(HostComponentProvider provider) {
            this.provider = provider;
        }

        /**
         * Gives the object a chance to register its host components with the registrar
         *
         * @param registrar The host component registrar
         */
        @Override
        public void provide(ComponentRegistrar registrar) {
            provider.provide(registrar);
            for (Map.Entry<Object, Class<?>[]> entry : objects.entrySet()) {
                registrar.register(entry.getValue()).forInstance(entry.getKey());
            }
        }

        /**
         * Registers a service.
         *
         * @param service    the service
         * @param interfaces the interfaces that the service should expose
         */
        void register(Object service, Class<?>... interfaces) {
            objects.put(service, interfaces);
        }
    }

}
