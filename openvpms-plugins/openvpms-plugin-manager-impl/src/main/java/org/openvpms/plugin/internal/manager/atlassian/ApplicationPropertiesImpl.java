/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.plugin.internal.manager.ServletContainerContext;
import org.openvpms.version.Version;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Implementation of {@link ApplicationProperties} for OpenVPMS, largely lifted from
 * the Atlassian Reference Application.
 *
 * @author Tim Anderson
 */
public class ApplicationPropertiesImpl implements ApplicationProperties {

    /**
     * The web resource integration.
     */
    private final WebResourceIntegration webResourceIntegration;

    /**
     * The servlet container context.
     */
    private final ServletContainerContext context;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The base URL supplier.
     */
    private final Supplier<String> canonicalBaseUrlSupplier = this::getCanonicalBaseUrl;

    /**
     * The context path supplier.
     */
    private final Supplier<String> canonicalContextPathSupplier = this::getCanonicalContextPath;

    /**
     * Constructs an {@link ApplicationPropertiesImpl}.
     *
     * @param webResourceIntegration the web resource integration
     * @param context                the servlet container context
     * @param practiceService        the practice service, used to access the configured base URL
     */
    public ApplicationPropertiesImpl(WebResourceIntegration webResourceIntegration, ServletContainerContext context,
                                     PracticeService practiceService) {
        this.webResourceIntegration = webResourceIntegration;
        this.context = context;
        this.practiceService = practiceService;
    }

    /**
     * @deprecated
     */
    @Override
    public String getBaseUrl() {
        return webResourceIntegration.getBaseUrl();
    }

    /**
     * Returns the base URL for the specified URL mode.
     *
     * @param urlMode the URL mode
     * @return the base URL
     * @throws IllegalStateException if the URL cannot be determined
     */
    @Override
    public String getBaseUrl(UrlMode urlMode) {
        switch (urlMode) {
            case ABSOLUTE: {
                Optional<String> absoluteUrl = getAbsoluteBaseUrlFromRequest();
                return absoluteUrl.orElseGet(canonicalBaseUrlSupplier);
            }
            case CANONICAL:
                return getCanonicalBaseUrl();
            case RELATIVE: {
                Optional<String> contextPath = getContextPathFromRequest();
                return contextPath.orElseGet(canonicalContextPathSupplier);
            }
            case RELATIVE_CANONICAL:
                return getCanonicalContextPath();
            case AUTO: {
                Optional<String> contextPath = getContextPathFromRequest();
                return contextPath.orElseGet(canonicalBaseUrlSupplier);
            }
            default:
                throw new IllegalStateException("Unhandled UrlMode " + urlMode);
        }
    }

    @Override
    public String getDisplayName() {
        return "OpenVPMS";
    }

    @Override
    public String getPlatformId() {
        return "openvpms";
    }

    @Override
    public String getVersion() {
        return Version.VERSION;
    }

    @Override
    public Date getBuildDate() {
        return new Date(); // TODO
    }

    @Override
    public String getBuildNumber() {
        return Version.REVISION;
    }

    @Override
    public File getHomeDirectory() {
        return null;
    }

    /**
     * @deprecated
     */
    @Override
    public String getPropertyValue(String var1) {
        return null;
    }

    /**
     * Returns the base URL from the request.
     *
     * @return the base URL, or {@link Optional#empty()} if there is none
     */
    private Optional<String> getAbsoluteBaseUrlFromRequest() {
        HttpServletRequest request = context.getRequest();
        if (request != null) {
            return Optional.of(webResourceIntegration.getBaseUrl(com.atlassian.plugin.webresource.UrlMode.ABSOLUTE));
        }
        return Optional.empty();
    }

    /**
     * Returns the context path from the request.
     *
     * @return the context path, or {@link Optional#empty()} if there is none
     */
    private Optional<String> getContextPathFromRequest() {
        HttpServletRequest request = context.getRequest();
        if (request != null) {
            return Optional.of(request.getContextPath());
        }
        return Optional.empty();
    }

    /**
     * Returns the configured base URL.
     *
     * @return the base URL
     */
    private String getCanonicalBaseUrl() {
        String result = practiceService.getBaseURL();
        if (result == null) {
            throw new IllegalStateException("Practice Base URL is not configured");
        }
        return StringUtils.removeEnd(result, "/");
    }

    /**
     * Returns the configured context path.
     *
     * @return the context path
     */
    private String getCanonicalContextPath() {
        String baseUrl = getCanonicalBaseUrl();
        try {
            return new URL(baseUrl).getPath();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Base URL misconfigured", e);
        }
    }

}
