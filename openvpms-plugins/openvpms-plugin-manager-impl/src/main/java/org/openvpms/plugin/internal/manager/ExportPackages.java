/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.framework.Logger;
import org.apache.felix.framework.util.StringMap;
import org.apache.felix.framework.util.manifestparser.ManifestParser;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.osgi.framework.namespace.PackageNamespace;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRequirement;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.resource.Capability;
import org.osgi.resource.Requirement;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

/**
 * Parses OSGi manifest entries to determine the packages that need to be added to the
 * <em>org.osgi.framework.system.packages.extra</em> property to expose them to plugins.
 * <p/>
 * To avoid clashes with bundles, the following packages are not exported:
 * <ul>
 * <li>org.osg.*</li>
 * <li>java.*</li>
 * <li>javax.ws.* - conflicts with the atlassian-rest-module</li>
 * <li>org.springframework.*</li>
 * </ul>
 * In addition, any instance of jsr305 on the classpath is ignored. This is because it exports javax.annotation
 * which conflicts with that provided by the servlet container.
 * <p/>
 * Tomcat 8.x doesn't export the following packages, so these will be exported if they aren't found:
 * <ul>
 *     <li>javax.annotation - 1.3.5</li>
 *     <li>javax.el - 3.0.0</li>
 *     <li>javax.servlet - 3.1.0</li>
 * </ul>
 * Tomcat 9.x may export javax.el, but supply a MANIFEST.MF in el-api.jar that cannot be parsed by
 * {@link ManifestParser} (see OVPMS-2898). In this case, any jar named el-api.jar will be included.
 * <br>
 * Notes:
 * <ul>
 *     <li>Atlassian jars typically don't contain OSGi metadata in their manifests, and so the Atlassian plugin
 * bootstrapping exports all Atlassian packages.</li>
 * <li>A warning will be raised if a package appears twice. In most cases, this will be because
 * the servlet container has jars on its classpath that are also included in the webapp. This is usually benign
 * as the classes from the webapp will be used. However, the exported version may be incorrect as the highest
 * version is selected. A better approach would be to examine which classloader is used, and exclude those from
 * the parent. TODO</li>
 * </ul>
 *
 * @author Tim Anderson
 */
class ExportPackages {

    /**
     * The encountered jars.
     */
    private final List<Jar> jars = new ArrayList<>();

    /**
     * The packages to export.
     */
    private final Map<String, String> packages = new TreeMap<>();

    /**
     * Packages to the jars that export them.
     */
    private final Map<String, Jar> packageToJar = new HashMap<>();

    /**
     * The jars to include.
     */
    private final List<String> includes = new ArrayList<>();

    /**
     * The jars to exclude.
     */
    private final List<String> excludes = new ArrayList<>();

    /**
     * The logger.
     */
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ExportPackages.class);

    /**
     * Helper to supply to {@code ManifestParser}.
     */
    private static final BundleRevision BUNDLE_REVISION = new MockBundleRevision();

    /**
     * The annotation API version.
     */
    private static final String ANNOTATION_API = "javax.annotation";

    /**
     * The annotation API version.
     */
    private static final String ANNOTATION_API_VERSION = "1.3.5";

    /**
     * The base servlet API package.
     */
    private static final String SERVLET_API = "javax.servlet";

    /**
     * The required servlet API version.
     */
    private static final String SERVLET_API_VERSION = "3.1.0";

    /**
     * The EL API package.
     */
    private static final String EL_API = "javax.el";

    /**
     * The required EL API version.
     */
    private static final String EL_API_VERSION = "3.0.0";


    /**
     * Constructs an {@link ExportPackages}.
     *
     * @param logger the logger
     */
    private ExportPackages(Logger logger) {
        init(logger);
    }

    /**
     * Constructs a {@link ExportPackages}.
     */
    ExportPackages() {
        super();
    }

    /**
     * Creates a new instance.
     *
     * @param logger the logger
     * @return the packages to export
     */
    public static ExportPackages create(Logger logger) {
        return new ExportPackages(logger);
    }

    /**
     * Returns the manifest for the specified URL.
     *
     * @param url the URL
     * @return the manifest, or {@code null} if none can be read
     */
    protected Manifest getManifest(URL url) {
        Manifest manifest = null;
        try (InputStream stream = url.openStream()) {
            manifest = new Manifest(stream);
        } catch (Throwable exception) {
            log.warn("Failed to read manifest for {}: {}", url, exception.getMessage(), exception);
        }
        return manifest;
    }

    /**
     * Returns the packages to export, and their corresponding version.
     *
     * @return the packages
     */
    Map<String, String> getPackages() {
        return packages;
    }

    /**
     * Returns the packages to export.
     *
     * @return the packages
     */
    List<String> getPackageIncludes() {
        return new ArrayList<>(packages.keySet());
    }

    /**
     * Returns the names of the jars to include.
     *
     * @return the jars to include
     */
    List<String> getJarIncludes() {
        return includes;
    }

    /**
     * Returns the jars to exclude.
     *
     * @return the jars to exclude
     */
    List<String> getJarExcludes() {
        return excludes;
    }

    /**
     * Initialises the packages.
     *
     * @param manifests the manifest URLs
     * @param logger    the logger
     */
    void init(Enumeration<URL> manifests, Logger logger) {
        Set<String> seen = new HashSet<>(); // used to exclude jars included on the classpath more than once
        List<URL> urls = getSortedUrls(manifests);
        for (URL url : urls) {
            String lowercaseURL = url.toString().toLowerCase(); // needed for Windows case changes
            if (seen.add(lowercaseURL)) {
                processManifest(url, logger);
            }
        }
        determineIncludes();

        // add any missing packages. This is required when running under Tomcat
        addAnnotationPackages();
        addELPackages();
        addServletPackages();

        if (log.isDebugEnabled()) {
            log.debug("Included jars: {}", StringUtils.join(includes, ","));
            log.debug("Excluded jars: {}", StringUtils.join(excludes, ","));
            log.debug("Packages: {}", packages.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .collect(Collectors.joining(", ")));
        }
    }

    /**
     * Returns URLS from an enumeration as a sorted list, to ensure they are processed the same each time.
     *
     * @param urls the url enumeration
     * @return the urls as a sorted list
     */
    private List<URL> getSortedUrls(Enumeration<URL> urls) {
        List<URL> list = Collections.list(urls);
        list.sort(Comparator.comparing(URL::toString));
        return list;
    }

    /**
     * Determines the jars to include.
     */
    private void determineIncludes() {
        Set<String> include = new TreeSet<>();
        Set<String> exclude = new TreeSet<>();
        for (Jar jar : jars) {
            if (jar.include) {
                include.add(jar.name);
            } else {
                exclude.add(jar.name);
            }
        }

        // if a jar has a duplicate name, and one has been included, make sure it isn't also excluded
        exclude.removeAll(include);

        includes.addAll(include);
        excludes.addAll(exclude);
    }

    /**
     * Initialises this.
     *
     * @param logger the logger
     */
    private void init(Logger logger) {
        Enumeration<URL> manifests;
        try {
            manifests = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
        } catch (IOException exception) {
            throw new IllegalStateException("Failed to get manifests to determine exports: " + exception.getMessage(),
                                            exception);
        }
        init(manifests, logger);
    }

    /**
     * Processes a manifest to determine if a jar should be included or excluded.
     *
     * @param manifestURL the jar manifest URL
     * @param logger      the logger
     */
    private void processManifest(URL manifestURL, Logger logger) {
        Manifest manifest = getManifest(manifestURL);
        boolean include = false;
        Jar jar = addJar(manifestURL);
        if (jar != null && !excludeJar(jar)) {
            if (manifest != null) {
                Map<String, Object> attributes = getAttributes(manifest);
                if (attributes != null) {
                    if (addPackages(attributes, jar, logger)) {
                        include = true;
                    }
                } else {
                    // Not an OSGi bundle. See if it should be included anyway
                    include = includeNonOSGIJar(manifest);
                }
            }
            if (include || isAtlassianJar(jar) || isJavaEl(jar)) {
                jar.include = true;
            }
        }
    }

    /**
     * Determines if a jar is the Java EL API jar shipped by Tomcat.
     * <p/>
     * The {@code javax.el} API jar shipped with some versions of Tomcat 9 (e.g. Tomcat 9.0.88) has a MANIFEST.MF
     * that cannot be parsed by {@link ManifestParser}, as it includes java.* packages in its Import-Package statement.
     * <p/>
     * See OVPMS-2898 for more details.
     *
     * @param jar the jar
     * @return {@code true} if the jar is the {@code javax.el} API jar, otherwise {@code false}
     */
    private boolean isJavaEl(Jar jar) {
        return jar.name.equals("el-api.jar");
    }

    /**
     * Determines if a jar needs to be excluded.
     * <p/>
     * At present, this only applies to com.google.code.findbugs:jsr305 which exports javax.annotation.
     *
     * @param jar the jar
     * @return {@code true} if the jar should be excluded, otherwise {@code false}
     */
    private boolean excludeJar(Jar jar) {
        return jar.name.startsWith("jsr305");
    }

    /**
     * Determines if a non-OSGI jar should be included.
     * <p/>
     * It should be included if it provides javax.annotation, javax.el, or javax.servlet.
     * This is required when running under Tomcat, as Tomcat doesn't provide OSGi headers for these.
     * The jar needs to be included if its packages are going to be considered by org.twdata.pkgscanner.PackageScanner
     *
     * @param manifest the jar manifest
     * @return {@code true} if it should be included, otherwise {@code false}
     */
    private boolean includeNonOSGIJar(Manifest manifest) {
        boolean include = false;
        Map<String, Attributes> entries = manifest.getEntries();
        if (entries.get("javax/annotation/") != null || entries.get("javax/el/") != null
            || entries.get("javax/servlet/") != null) {
            include = true;
        }
        return include;
    }

    /**
     * Adds exports for javax.annotation packages if they aren't present.
     */
    private void addAnnotationPackages() {
        String version = packages.get(ANNOTATION_API);
        if (version == null) {
            log.info(ANNOTATION_API + " not exported, defaulting version to " + ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API, ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API + ".security", ANNOTATION_API_VERSION);
            packages.put(ANNOTATION_API + ".sql", ANNOTATION_API_VERSION);
        }
    }

    /**
     * Adds exports for javax.servlet packages if they aren't present.
     */
    private void addServletPackages() {
        String version = packages.get(SERVLET_API);
        if (version == null) {
            log.info(SERVLET_API + " not exported, defaulting version to " + SERVLET_API_VERSION);
            packages.put(SERVLET_API, SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".annotation", SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".descriptor", SERVLET_API_VERSION);
            packages.put(SERVLET_API + ".http", SERVLET_API_VERSION);
        }
    }

    /**
     * Adds exports for javax.el packages if they aren't present.
     */
    private void addELPackages() {
        String version = packages.get(EL_API);
        if (version == null) {
            log.info(EL_API + " not exported, defaulting version to " + EL_API_VERSION);
            packages.put(EL_API, EL_API_VERSION);
        }
    }

    /**
     * Hack to avoid excluding Atlassian jars from being exported on the system class path.
     * <p/>
     * These don't have OSGi meta-data, but are picked up by the APF packaging scanning.
     *
     * @param jar the jar
     * @return {@code true} if the jar is an atlassian jar
     */
    private boolean isAtlassianJar(Jar jar) {
        return jar.name.startsWith("atlassian-") || jar.name.startsWith("velocity-htmlsafe");
    }

    /**
     * Determines if a package is excluded.
     *
     * @param pkg the package
     * @return {@code true} if the package is excluded
     */
    private boolean exclude(String pkg) {
        return pkg.startsWith("org.osgi.") || pkg.startsWith("java.")
               || pkg.startsWith("javax.ws")
               || pkg.startsWith("com.fasterxml")
               || pkg.startsWith("org.openvpms.ws.util"); // because it imports com.fasterxml
    }


    /**
     * Adds the jar determined from its MANIFEST.MF url.
     * <p/>
     * By default, it is excluded from export.
     *
     * @param url the MANIFEST.MF url
     * @return the corresponding jar, or {@code null} if one cannot be determined
     */
    private Jar addJar(URL url) {
        Jar jar = null;
        String path = url.getPath();
        int index = path.lastIndexOf('!');
        if (index > 0) {
            path = path.substring(0, index);
            index = path.lastIndexOf('/');
            if (index >= 0) {
                String name = path.substring(index + 1);
                if (!name.isEmpty()) {
                    jar = new Jar(path, name);
                    jars.add(jar);
                }
            }
        }
        return jar;
    }

    /**
     * Adds the packages that a manifest exports, if any.
     *
     * @param manifest the manifest
     * @param jar      the jar
     * @param logger   the Felix logger
     * @return {@code true} if packages were added, {@code false} it nothing was exported
     */
    private boolean addPackages(Map<String, Object> manifest, Jar jar, Logger logger) {
        Map<String, String> pkgVersions = new HashMap<>();
        boolean exclude = false;
        try {
            Map<String, Object> configMap = Collections.emptyMap();
            ManifestParser parser = new ManifestParser(logger, configMap, BUNDLE_REVISION, manifest);
            for (BundleCapability capability : parser.getCapabilities()) {
                if (BundleRevision.PACKAGE_NAMESPACE.equals(capability.getNamespace())) {
                    Map<String, Object> attributes = capability.getAttributes();
                    String pkg = getString(PackageNamespace.PACKAGE_NAMESPACE, attributes);
                    if (pkg != null) {
                        if (exclude(pkg)) {
                            exclude = true;
                            break;
                        } else {
                            String version = getString(PackageNamespace.CAPABILITY_VERSION_ATTRIBUTE, attributes);
                            if (version != null) {
                                addPackage(jar, pkg, version, pkgVersions);
                            }
                        }
                    }
                }
            }
            if (!exclude) {
                packages.putAll(pkgVersions);
            }
        } catch (Throwable exception) {
            log.info("Failed to process {}: {}", jar.url, exception.getMessage(), exception);
        }
        return !exclude && !pkgVersions.isEmpty();
    }

    /**
     * Adds a package to the supplied package/version map, if:
     * <ul>
     *     <li>no other jar provides the package; or</li>
     *     <li>another jar provides the package but the supplied version is newer</li>
     * </ul>
     *
     * @param jar         the jar
     * @param pkg         the package
     * @param version     the package version
     * @param pkgVersions collects packages and their versions
     */
    private void addPackage(Jar jar, String pkg, String version, Map<String, String> pkgVersions) {
        Jar existing = packageToJar.get(pkg);
        if (existing != null) {
            String existingVersion = packages.get(pkg);
            if (!version.equals(existingVersion)) {
                if (existingVersion == null) {
                    addPackageVersion(jar, pkg, version, pkgVersions);
                } else if (!existing.sameUrl(jar)) {
                    // package is exported multiple times under different versions. Use the newest version.
                    if (isGreater(version, existingVersion)) {
                        log.warn("Found newer version of package {} with version {} from '{}'. " +
                                 "Replaces version {} from '{}'",
                                 pkg, version, jar.url, existingVersion, existing.url);
                        addPackageVersion(jar, pkg, version, pkgVersions);
                        existing.include = false;   // exclude the existing jar
                    } else {
                        log.warn("Excluding package {} with version {} from '{}'. Using version {} from '{}'",
                                 pkg, version, jar.url, existingVersion, existing.url);
                    }
                }
            } else if (!jar.sameUrl(existing)) {
                // same package, but from different jars
                log.warn("Package {} occurs in both '{}' ({}) and '{}' ({})",
                         pkg, existing.url, existingVersion, jar.url, version);
            }
        } else {
            addPackageVersion(jar, pkg, version, pkgVersions);
        }
    }

    /**
     * Adds a package and version to the supplied package/version map, and updates {@link #packageToJar}.
     *
     * @param jar         the jar
     * @param pkg         the package
     * @param version     the package version
     * @param pkgVersions collects packages and their versions
     */
    private void addPackageVersion(Jar jar, String pkg, String version, Map<String, String> pkgVersions) {
        pkgVersions.put(pkg, version);
        packageToJar.put(pkg, jar);
    }

    /**
     * Returns the value of an attribute.
     *
     * @param name       the attribute name
     * @param attributes the attributes
     * @return the corresponding value, or {@code null} if none is found
     */
    private String getString(String name, Map<String, Object> attributes) {
        Object result = attributes.get(name);
        return (result != null) ? result.toString() : null;
    }

    /**
     * Parses the manifest, returning the main attributes if it exports packages.
     *
     * @param manifest the manifest
     * @return the manifest's main attributes, or {@code null} if it doesn't export packages
     */
    private Map<String, Object> getAttributes(Manifest manifest) {
        Map<String, Object> result = null;
        if (manifest.getMainAttributes().getValue(Constants.EXPORT_PACKAGE) != null) {
            result = new StringMap(manifest.getMainAttributes());
        }
        return result;
    }

    /**
     * Determines if a version is greater than another
     *
     * @param version      the version
     * @param otherVersion the version to compare
     * @return {@code true} if {@code version} is greater than {@code otherVersion}
     */
    private boolean isGreater(String version, String otherVersion) {
        try {
            Version a = new Version(version);
            Version b = new Version(otherVersion);
            return a.compareTo(b) > 0;
        } catch (Throwable exception) {
            return false;
        }
    }

    private static class Jar {

        private final String url;

        private final String name;

        private boolean include = false;

        Jar(String url, String name) {
            this.url = url;
            this.name = name;
        }

        /**
         * Determines if the jar URL is the same as another.
         * <p/>
         * This does a case-insensitive comparison for Windows.
         *
         * @param other the other jar
         * @return {@code true} if the urls are the same; otherwise {@code false}
         */
        public boolean sameUrl(Jar other) {
            return url.equalsIgnoreCase(other.url);
        }
    }

    /**
     * Helper to enable the use of {@code ManifestParser}.
     */
    private static class MockBundleRevision implements BundleRevision {
        public String getSymbolicName() {
            return null;
        }

        public Version getVersion() {
            return null;
        }

        public List<BundleCapability> getDeclaredCapabilities(String namespace) {
            return null;
        }

        public List<BundleRequirement> getDeclaredRequirements(String namespace) {
            return null;
        }

        public int getTypes() {
            return 0;
        }

        public BundleWiring getWiring() {
            return null;
        }

        public List<Capability> getCapabilities(String namespace) {
            return null;
        }

        public List<Requirement> getRequirements(String namespace) {
            return null;
        }

        public Bundle getBundle() {
            return null;
        }
    }
}
