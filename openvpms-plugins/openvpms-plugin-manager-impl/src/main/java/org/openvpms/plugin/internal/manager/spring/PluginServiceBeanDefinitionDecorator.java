/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.spring;

import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.BeanDefinitionDecorator;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.util.StringUtils;
import org.springframework.util.xml.DomUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Called when Spring encounters a "plugin:service" element in a bean declaration.
 * <p/>
 * A bean is declared as a plugin service using:<br/>
 * <pre>{@code
 *  <bean id="someBean" class="....">
 *      <plugin:service>
 *          <plugin:interface>someInterfaceToExpose</plugin:interface>
 *          <plugin:interface>anotherInterfaceToExpose</plugin:interface>
 *      </plugin:service>
 * }
 * </pre>
 * If no interfaces are supplied, all interfaces that the bean implements will be exposed.
 *
 * @author Tim Anderson
 */
public class PluginServiceBeanDefinitionDecorator implements BeanDefinitionDecorator {

    /**
     * The services property name.
     */
    private static final String SERVICES = "services";

    /**
     * Parses the specified {@link Node} for a plugin:service element.
     *
     * @param node       the export node
     * @param definition the bean definition holder
     * @param context    the parser context
     * @return the bean definition holder
     */
    @Override
    public BeanDefinitionHolder decorate(Node node, BeanDefinitionHolder definition, ParserContext context) {
        addService(definition.getBeanName(), getInterfaces(node), context.getRegistry());
        return definition;
    }

    /**
     * Returns any interfaces exposed by the service.
     *
     * @param node the node to parse
     * @return the interfances
     */
    private List<String> getInterfaces(Node node) {
        List<String> result = new ArrayList<>();
        if (node instanceof Element) {
            for (Element element : DomUtils.getChildElementsByTagName((Element) node, "interface")) {
                String text = DomUtils.getTextValue(element);
                if (!StringUtils.isEmpty(text)) {
                    result.add(text);
                }
            }
        }
        return result;
    }

    /**
     * Registers a bean as a plugin service.
     *
     * @param beanName the bean name
     * @param registry the bean registry
     */
    @SuppressWarnings("unchecked")
    private void addService(String beanName, List<String> interfaces, BeanDefinitionRegistry registry) {
        BeanDefinition config = getConfig(registry);
        PropertyValue propertyValue = config.getPropertyValues().getPropertyValue(SERVICES);
        Map<String, List<String>> services = (propertyValue != null)
                                             ? (Map<String, List<String>>) propertyValue.getValue() : null;
        if (services == null) {
            services = new HashMap<>();
            config.getPropertyValues().addPropertyValue(SERVICES, services);
        }
        services.put(beanName, interfaces);
    }

    /**
     * Returns the {@link PluginServiceProviderConfig} bean definition, creating it if required.
     *
     * @param registry the bean definition registry
     * @return the configuration bean definition
     */
    private BeanDefinition getConfig(BeanDefinitionRegistry registry) {
        BeanDefinition config;
        if (!registry.containsBeanDefinition(PluginServiceProviderConfig.BEAN_NAME)) {
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(PluginServiceProviderConfig.class);
            config = builder.getBeanDefinition();
            registry.registerBeanDefinition(PluginServiceProviderConfig.BEAN_NAME, config);
        } else {
            config = registry.getBeanDefinition(PluginServiceProviderConfig.BEAN_NAME);
        }
        return config;
    }

}
