/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian.servlet;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.filter.CompositeFilter;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.Arrays;
import java.util.List;

/**
 * A composite filter that creates all of the filters required to support the Atlassian plugin framework.
 * <p/>
 * For background, refer to the web.xml of the Atlassian Reference Application.
 *
 * @author Tim Anderson
 */
public class AtlassianPluginFrameworkFilter extends CompositeFilter implements ServletContextAware, InitializingBean {

    /**
     * Filter to filter requests by IP.
     */
    private final Filter ipFilter;

    /**
     * The servlet context.
     */
    private ServletContext servletContext;

    /**
     * Constructs an {@link AtlassianPluginFrameworkFilter}.
     *
     * @param ipFilter a filter to restrict requests by IP address
     */
    public AtlassianPluginFrameworkFilter(Filter ipFilter) {
        this.ipFilter = ipFilter;
    }

    /**
     * Sets the servlet context.
     *
     * @param servletContext the servlet context
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Invoked by the containing {@code BeanFactory} after it has set all bean properties.
     */
    @Override
    public void afterPropertiesSet() {
        List<Filter> filters = Arrays.asList(new ServletContextFilter(),
                                             ipFilter,
                                             new APFServletFilterModuleContainerFilter("after-encoding", "REQUEST"),
                                             new APFServletFilterModuleContainerFilter("after-encoding", "FORWARD"),
                                             new APFServletFilterModuleContainerFilter("after-encoding", "INCLUDE"),
                                             new APFServletFilterModuleContainerFilter("after-encoding", "ERROR"),
                                             new APFServletFilterModuleContainerFilter("before-login", "REQUEST"),
                                             new APFServletFilterModuleContainerFilter("before-login", "FORWARD"),
                                             new APFServletFilterModuleContainerFilter("before-login", "INCLUDE"),
                                             new APFServletFilterModuleContainerFilter("before-login", "ERROR"),
                                             new APFServletFilterModuleContainerFilter("before-decoration", "REQUEST"),
                                             new APFServletFilterModuleContainerFilter("before-decoration", "FORWARD"),
                                             new APFServletFilterModuleContainerFilter("before-decoration", "INCLUDE"),
                                             new APFServletFilterModuleContainerFilter("before-decoration", "ERROR"),
                                             new APFServletFilterModuleContainerFilter("before-dispatch", "REQUEST"),
                                             new APFServletFilterModuleContainerFilter("before-dispatch", "FORWARD"),
                                             new APFServletFilterModuleContainerFilter("before-dispatch", "INCLUDE"),
                                             new APFServletFilterModuleContainerFilter("before-dispatch", "ERROR"));
        for (Filter filter : filters) {
            if (filter instanceof GenericFilterBean) {
                try {
                    GenericFilterBean bean = (GenericFilterBean) filter;
                    bean.setServletContext(servletContext);
                    bean.afterPropertiesSet();
                } catch (ServletException exception) {
                    throw new IllegalStateException("Failed to initialise filter", exception);
                }
            }
        }
        setFilters(filters);
    }
}
