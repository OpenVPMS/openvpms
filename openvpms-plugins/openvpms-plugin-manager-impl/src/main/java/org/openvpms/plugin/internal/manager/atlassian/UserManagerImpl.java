/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.seraph.auth.AuthenticationContext;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.security.Principal;
import java.util.Collection;

/**
 * Implementation of the Atlassian {@link UserManager} service.
 *
 * @author Tim Anderson
 */
public class UserManagerImpl implements UserManager {

    /**
     * The user service.
     */
    private final UserService service;

    /**
     * The authentication context.
     */
    private final AuthenticationContext authenticationContext;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The authentication manager.
     */
    private final AuthenticationManager authenticationManager;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(UserManagerImpl.class);

    /**
     * Constructs a {@link UserManagerImpl}.
     *
     * @param service               the user service
     * @param authenticationContext the authentication context
     * @param userRules             the user rules
     * @param authenticationManager the authentication manager
     */
    public UserManagerImpl(UserService service, AuthenticationContext authenticationContext,
                           UserRules userRules, AuthenticationManager authenticationManager) {
        this.service = service;
        this.authenticationContext = authenticationContext;
        this.userRules = userRules;
        this.authenticationManager = authenticationManager;
    }

    /**
     * Returns the name of the current authenticated user.
     *
     * @return the user name, or {@code null} if there is none
     * @deprecated
     */
    @Override
    public String getRemoteUsername() {
        Principal user = authenticationContext.getUser();
        return (user != null) ? user.getName() : null;
    }

    /**
     * Returns the profile of the current authenticated user.
     *
     * @return the profile, or {@code null} if there is none
     */
    @Override
    public UserProfile getRemoteUser() {
        Principal principal = authenticationContext.getUser();
        return (principal != null) ? getUserProfile(principal.getName()) : null;
    }

    /**
     * Returns the user key of the current authenticated user.
     *
     * @return the user key, or {@code null} if there is none
     */
    @Override
    public UserKey getRemoteUserKey() {
        Principal principal = authenticationContext.getUser();
        return (principal != null) ? new UserKey(principal.getName()) : null;
    }

    /**
     * Returns the name of the authenticated user associated with an HTTP request.
     *
     * @param request the HTTP request
     * @return the name, or {@code null} if there is none
     * @deprecated
     */
    @Override
    public String getRemoteUsername(HttpServletRequest request) {
        return request.getRemoteUser();
    }

    /**
     * Returns the profile of the authenticated user associated with a request.
     *
     * @return the profile, or {@code null} if there is none
     */
    @Override
    public UserProfile getRemoteUser(HttpServletRequest request) {
        return getUserProfile(request.getRemoteUser());
    }

    /**
     * Returns the user key of the authenticated user associated with a request.
     *
     * @return the user key, or {@code null} if there is none
     */
    @Override
    public UserKey getRemoteUserKey(HttpServletRequest request) {
        String username = request.getRemoteUser();
        return (username != null) ? new UserKey(username) : null;
    }

    /**
     * Returns the profile of the specified user.
     *
     * @param username the user name. May be {@code null}
     * @return the profile, or {@code null} if there is none
     */
    @Override
    public UserProfile getUserProfile(String username) {
        User user = getUser(username);
        return (user != null) ? new UserProfileImpl(user) : null;
    }

    /**
     * Returns the profile of the specified user.
     *
     * @param userKey the user key. May be {@code null}
     * @return the profile, or {@code null} if there is none
     */
    @Override
    public UserProfile getUserProfile(UserKey userKey) {
        return (userKey != null) ? getUserProfile(userKey.getStringValue()) : null;
    }

    /**
     * Determines if a user is in a group.
     *
     * @param username the user name. May be {@code null}
     * @param group    the group. May be {@code null
     * @return {@code false} as this is not supported
     * @deprecated
     */
    @Override
    public boolean isUserInGroup(String username, String group) {
        return false;
    }

    /**
     * Determines if a user is in a group.
     *
     * @param userKey the user key. May be {@code null}
     * @param group   the group. May be {@code null}
     * @return {@code false} as this is not supported
     */
    @Override
    public boolean isUserInGroup(UserKey userKey, String group) {
        return false;
    }

    /**
     * Determines if a user is a system administrator.
     *
     * @param userName the user name. May be {@code null}
     * @return {@code true} if the user is a system administrator, otherwise {@code false}
     * @deprecated
     */
    @Override
    public boolean isSystemAdmin(String userName) {
        return isAdministrator(userName);
    }

    /**
     * Determines if a user is a system administrator.
     *
     * @param userKey the user key. May be {@code null}
     * @return {@code true} if the user is a system administrator, otherwise {@code false}
     */
    @Override
    public boolean isSystemAdmin(UserKey userKey) {
        return (userKey != null) && isSystemAdmin(userKey.getStringValue());
    }

    /**
     * Determines if a user is an administrator.
     *
     * @param username the user name. May be {@code null}
     * @return {@code true} if the user is an administrator, otherwise {@code false}
     * @deprecated
     */
    @Override
    public boolean isAdmin(String username) {
        return isSystemAdmin(username);
    }

    /**
     * Determines if a user is an administrator.
     *
     * @param userKey the user key. May be {@code null}
     * @return {@code true} if the user is an administrator, otherwise {@code false}
     * @deprecated
     */
    @Override
    public boolean isAdmin(UserKey userKey) {
        return isSystemAdmin(userKey);
    }

    /**
     * Authenticates a user.
     *
     * @param username the user name
     * @param password the password
     * @return {@code true} if the user name and password are valid, otherwise {@code false}
     */
    @Override
    public boolean authenticate(String username, String password) {
        boolean result = false;
        try {
            Authentication authenticate = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
            result = authenticate != null;
        } catch (Throwable exception) {
            log.debug("Failed to authenticate " + username, exception);
        }
        return result;
    }

    /**
     * Resolves a user.
     *
     * @param username the user name
     * @return the principal, or {@code null if none is found}
     * @throws UserResolutionException if an error occurs resolving the user
     */
    @Override
    public Principal resolve(String username) throws UserResolutionException {
        Principal principal = null;
        try {
            if (getUser(username) != null) {
                principal = () -> username;
            }
        } catch (Throwable exception) {
            throw new UserResolutionException("Failed to resolve user: " + username, exception);
        }
        return principal;
    }

    @Override
    public Iterable<String> findGroupNamesByPrefix(String s, int i, int i1) {
        return null;
    }

    /**
     * Determines if a user is an administrator.
     *
     * @param userName the user name. May be {@code null}
     * @return {@code true} if the user is an administrator, otherwise {@code false}
     */
    private boolean isAdministrator(String userName) {
        User user = getUser(userName);
        return user != null && userRules.isAdministrator(user);
    }

    /**
     * Returns the user associated with a user name.
     *
     * @param username the user name. May be {@code null}
     * @return the corresponding user, or {@code null} if none is found
     */
    private User getUser(String username) {
        User result = null;
        if (username != null) {
            try {
                result = (User) service.loadUserByUsername(username);
            } catch (UsernameNotFoundException exception) {
                log.debug(exception.getMessage(), exception);
            } catch (Throwable exception) {
                log.warn("Failed to retrieve user: " + username, exception);
            }
        }
        return result;
    }

    /**
     * Adapts a {@link User} to a {@link UserProfile}.
     */
    private static class UserProfileImpl implements UserProfile, UserDetails {

        private final User user;

        UserProfileImpl(User user) {
            this.user = user;
        }

        @Override
        public UserKey getUserKey() {
            return new UserKey(user.getUsername());
        }

        @Override
        public String getUsername() {
            return user.getUsername();
        }

        @Override
        public String getFullName() {
            return user.getName();
        }

        @Override
        public String getEmail() {
            return null;
        }

        @Override
        public URI getProfilePictureUri(int i, int i1) {
            return null;
        }

        @Override
        public URI getProfilePictureUri() {
            return null;
        }

        @Override
        public URI getProfilePageUri() {
            return null;
        }

        /**
         * Returns the authorities granted to the user. Cannot return {@code null}.
         *
         * @return the authorities, sorted by natural key (never {@code null})
         */
        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return user.getAuthorities();
        }

        /**
         * Returns the password used to authenticate the user.
         *
         * @return the password
         */
        @Override
        public String getPassword() {
            return user.getPassword();
        }

        /**
         * Indicates whether the user's account has expired. An expired account cannot be
         * authenticated.
         *
         * @return {@code true} if the user's account is valid (ie non-expired),
         * {@code false} if no longer valid (ie expired)
         */
        @Override
        public boolean isAccountNonExpired() {
            return user.isAccountNonExpired();
        }

        /**
         * Indicates whether the user is locked or unlocked. A locked user cannot be
         * authenticated.
         *
         * @return {@code true} if the user is not locked, {@code false} otherwise
         */
        @Override
        public boolean isAccountNonLocked() {
            return user.isAccountNonLocked();
        }

        /**
         * Indicates whether the user's credentials (password) has expired. Expired
         * credentials prevent authentication.
         *
         * @return {@code true} if the user's credentials are valid (ie non-expired),
         * {@code false} if no longer valid (ie expired)
         */
        @Override
        public boolean isCredentialsNonExpired() {
            return user.isCredentialsNonExpired();
        }

        /**
         * Indicates whether the user is enabled or disabled. A disabled user cannot be
         * authenticated.
         *
         * @return {@code true} if the user is enabled, {@code false} otherwise
         */
        @Override
        public boolean isEnabled() {
            return user.isEnabled();
        }
    }
}
