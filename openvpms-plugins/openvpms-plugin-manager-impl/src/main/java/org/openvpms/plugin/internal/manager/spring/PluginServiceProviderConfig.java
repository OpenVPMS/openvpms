/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.spring;

import org.springframework.beans.factory.BeanFactory;

import java.util.List;
import java.util.Map;

/**
 * Collects plugin from a Spring context as its being parsed.
 *
 * @author Tim Anderson
 */
public class PluginServiceProviderConfig {

    /**
     * The services being exposed, and their corresponding interfaces.
     */
    private Map<String, List<String>> services;

    /**
     * The plugin service provider configuration bean name.
     */
    static final String BEAN_NAME = "_pluginServiceProviderConfig";

    /**
     * Sets the services being and their corresponding interfaces.
     *
     * @param services the services
     */
    public void setServices(Map<String, List<String>> services) {
        this.services = services;
    }

    /**
     * Returns the services being exposed.
     *
     * @return the services
     */
    public Map<String, List<String>> getServices() {
        return services;
    }

    /**
     * Helper to return an instance of {@link PluginServiceProviderConfig} registered in the supplied factory,
     * if it exists.
     *
     * @param factory the factory to use
     * @return the corresponding configuration, or {@code null} if none exists
     */
    static PluginServiceProviderConfig getConfig(BeanFactory factory) {
        if (factory.containsBean(BEAN_NAME)) {
            return factory.getBean(BEAN_NAME, PluginServiceProviderConfig.class);
        }
        return null;
    }

}
