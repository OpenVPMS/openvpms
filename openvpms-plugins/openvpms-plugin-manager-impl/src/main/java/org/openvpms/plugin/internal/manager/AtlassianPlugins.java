/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginRegistry;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.BundledPluginLoader;
import com.atlassian.plugin.loaders.ClassPathPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.ScanningPluginLoader;
import com.atlassian.plugin.main.HotDeployer;
import com.atlassian.plugin.main.PluginsConfiguration;
import com.atlassian.plugin.main.PluginsConfigurationBuilder;
import com.atlassian.plugin.manager.DefaultPluginManager;
import com.atlassian.plugin.manager.PluginRegistryImpl;
import com.atlassian.plugin.manager.ProductPluginAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager;
import com.atlassian.plugin.osgi.factory.OsgiBundleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPluginFactory;
import com.atlassian.plugin.osgi.factory.RemotablePluginFactory;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.google.common.collect.Sets;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.plugin.internal.manager.persistence.PluginInstallerImpl;
import org.openvpms.plugin.internal.manager.persistence.PluginScannerImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Adapted from the com.atlassian.plugin.main.AtlassianPlugins to enable plugins to be loaded from the database.
 */
public class AtlassianPlugins {

    /**
     * Suffix for temporary directories which will be removed on shutdown
     */
    public static final String TEMP_DIRECTORY_SUFFIX = ".tmp";

    private final OsgiContainerManager osgiContainerManager;

    private final DefaultPluginEventManager pluginEventManager;

    private final PluginAccessor pluginAccessor;

    private final PluginController pluginController;

    private final SplitStartupPluginSystemLifecycle pluginSystemLifecycle;

    private final EventPublisher eventPublisher;

    private HotDeployer hotDeployer;

    /**
     * Constructs an instance of the plugin framework with the specified config. No additional validation is performed on
     * the configuration, so it is recommended you use the {@link PluginsConfigurationBuilder} class to create a
     * configuration instance.
     *
     * @param config    the plugins configuration to use
     * @param pluginDAO the plugin DAO
     */
    public AtlassianPlugins(PluginsConfiguration config, PluginDAO pluginDAO) {
        pluginEventManager = new DefaultPluginEventManager(config.getScopeManager());
        eventPublisher = pluginEventManager.getEventPublisher();

        final AtomicReference<PluginAccessor> pluginAccessorRef = new AtomicReference<>();
        osgiContainerManager = new FelixOsgiContainerManager(
                config.getFrameworkBundleDirectory(),
                config.getOsgiPersistentCache(),
                config.getPackageScannerConfiguration(),
                new CriticalHostComponentProvider(config.getHostComponentProvider(), pluginEventManager,
                                                  pluginAccessorRef),
                pluginEventManager);

        final Set<Application> applications = config.getApplication() != null ?
                                              Sets.newHashSet(config.getApplication()) : Collections.emptySet();

        // plugin factories/deployers
        final OsgiPluginFactory osgiPluginDeployer = new OsgiPluginFactory(
                config.getPluginDescriptorFilename(),
                applications,
                config.getOsgiPersistentCache(),
                osgiContainerManager,
                pluginEventManager);

        final OsgiBundleFactory osgiBundleDeployer = new OsgiBundleFactory(osgiContainerManager);

        final RemotablePluginFactory remotablePluginFactory = new RemotablePluginFactory(
                config.getPluginDescriptorFilename(),
                applications,
                osgiContainerManager,
                pluginEventManager);

        final List<PluginFactory> pluginDeployers = new LinkedList<>(Arrays.asList(
                osgiPluginDeployer, osgiBundleDeployer, remotablePluginFactory));
        if (config.isUseLegacyDynamicPluginDeployer()) {
            pluginDeployers.add(new LegacyDynamicPluginFactory(config.getPluginDescriptorFilename()));
        }

        final List<PluginLoader> pluginLoaders = new ArrayList<>();

        // classpath plugins
        pluginLoaders.add(new ClassPathPluginLoader());

        // bundled plugins
        if (config.getBundledPluginUrl() != null) {
            pluginLoaders.add(new BundledPluginLoader(config.getBundledPluginUrl(),
                                                      config.getBundledPluginCacheDirectory(), pluginDeployers,
                                                      pluginEventManager));
        }

        // osgi/v2 and v3 plugins
        pluginLoaders.add(new ScanningPluginLoader(new PluginScannerImpl(config.getPluginDirectory(), pluginDAO),
                                                   pluginDeployers, pluginEventManager));

        PluginRegistry.ReadWrite pluginRegistry = new PluginRegistryImpl();
        pluginAccessor = new ProductPluginAccessor(pluginRegistry,
                                                   config.getPluginStateStore(),
                                                   config.getModuleDescriptorFactory(),
                                                   pluginEventManager,
                                                   config.getScopeManager());

        DefaultPluginManager defaultPluginManager = DefaultPluginManager.newBuilder()
                .withPluginRegistry(pluginRegistry)
                .withStore(config.getPluginStateStore())
                .withPluginLoaders(pluginLoaders)
                .withModuleDescriptorFactory(config.getModuleDescriptorFactory())
                .withPluginEventManager(pluginEventManager)
                .withPluginAccessor(pluginAccessor)
                .build();

        pluginController = defaultPluginManager;
        pluginSystemLifecycle = defaultPluginManager;

        pluginAccessorRef.set(pluginAccessor);

        defaultPluginManager.setPluginInstaller(new PluginInstallerImpl(pluginDAO));

        if (config.getHotDeployPollingPeriod() > 0) {
            hotDeployer = new HotDeployer(pluginController, config.getHotDeployPollingPeriod());
        }
    }

    public void afterPropertiesSet() {
        pluginEventManager.register(this);
    }

    public void destroy() {
        pluginEventManager.unregister(this);
    }

    /**
     * @return the underlying OSGi container manager
     */
    public OsgiContainerManager getOsgiContainerManager() {
        return osgiContainerManager;
    }

    /**
     * @return the plugin event manager
     */
    public PluginEventManager getPluginEventManager() {
        return pluginEventManager;
    }

    /**
     * @return the event publisher used by plugin event manager
     */
    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    /**
     * @return the plugin controller for manipulating plugins
     */
    public PluginController getPluginController() {
        return pluginController;
    }

    /**
     * @return the plugin accessor for accessing plugins
     */
    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    /**
     * @return the plugin system lifecycle for starting and stopping the plugin system
     */
    public SplitStartupPluginSystemLifecycle getPluginSystemLifecycle() {
        return pluginSystemLifecycle;
    }


    @PluginEventListener
    public void onPluginFrameworkStartedEvent(final PluginFrameworkStartedEvent event) {
        if (hotDeployer != null && !hotDeployer.isRunning()) {
            hotDeployer.start();
        }
    }

    @PluginEventListener
    public void onPluginFrameworkShutdownEvent(final PluginFrameworkShutdownEvent event) {
        if (hotDeployer != null && hotDeployer.isRunning()) {
            hotDeployer.stop();
        }
    }

    private static class CriticalHostComponentProvider implements HostComponentProvider {
        private final HostComponentProvider delegate;

        private final PluginEventManager pluginEventManager;

        private final AtomicReference<PluginAccessor> pluginManagerRef;

        CriticalHostComponentProvider(final HostComponentProvider delegate,
                                      final PluginEventManager pluginEventManager,
                                      final AtomicReference<PluginAccessor> pluginManagerRef) {
            this.delegate = delegate;
            this.pluginEventManager = pluginEventManager;
            this.pluginManagerRef = pluginManagerRef;
        }

        public void provide(final ComponentRegistrar registrar) {
            registrar.register(PluginEventManager.class).forInstance(pluginEventManager);
            registrar.register(PluginAccessor.class).forInstance(pluginManagerRef.get());
            delegate.provide(registrar);
        }
    }
}
