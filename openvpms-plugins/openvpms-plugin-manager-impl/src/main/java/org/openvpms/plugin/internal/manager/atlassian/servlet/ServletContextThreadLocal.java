/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Tracks the current HTTP request and response.
 *
 * @author Tim Anderson
 * @see ServletContextFilter
 */
class ServletContextThreadLocal {

    /**
     * The context.
     */
    private static final ThreadLocal<Context> context = new ThreadLocal<>();

    /**
     * Returns the request associated with the current thread.
     *
     * @return the request, or {@code null} if there is none
     */
    static HttpServletRequest getRequest() {
        Context result = context.get();
        return (result != null) ? result.request : null;
    }

    /**
     * Returns the response associated with the current thread.
     *
     * @return the response, or {@code null} if there is none
     */
    static HttpServletResponse getResponse() {
        Context result = context.get();
        return (result != null) ? result.response : null;
    }

    /**
     * Returns the request cache associated with the current thread
     *
     * @return the request cache
     */
    static Map<String, Object> getRequestCache() {
        Context result = context.get();
        return (result != null) ? result.requestCache : null;
    }

    /**
     * Sets the context.
     *
     * @param request  the request
     * @param response the response
     */
    static void setContext(HttpServletRequest request, HttpServletResponse response) {
        context.set(new Context(request, response));
    }

    /**
     * Clears the request and response.
     */
    static void clearContext() {
        context.remove();
    }

    private static class Context {

        final HttpServletRequest request;

        final HttpServletResponse response;

        final Map<String, Object> requestCache;

        public Context(HttpServletRequest request, HttpServletResponse response) {
            this.request = request;
            this.response = response;
            this.requestCache = new HashMap<>();
        }
    }

}
