/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.persistence;

import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.Scanner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Monitors the {@link PluginDAO} for deployed plugins.
 * <p/>
 * Note that due to the way APF manages plugins, plugins may only be disabled via a {@link PluginController},
 * so marking a plugin inactive via the archetype service is not sufficient to disable it.
 */
public class PluginScannerImpl implements Scanner {

    /**
     * The directory to store plugins in.
     */
    private final File dir;

    /**
     * The plugin DAO.
     */
    private final PluginDAO dao;

    /**
     * The deployment units and their corresponding plugin ids.
     */
    private final Map<DeploymentUnit, Long> deploymentUnits = new HashMap<>();

    /**
     * The plugin id to version map, used to determine if a plugin has been updated.
     */
    private final Map<Long, Long> pluginVersions = new HashMap<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PluginScannerImpl.class);

    /**
     * Constructs a {@link PluginInstallerImpl}.
     *
     * @param dir the directory to store plugins in
     */
    public PluginScannerImpl(File dir, PluginDAO dao) {
        this.dir = dir;
        this.dao = dao;
    }

    /**
     * Scan for new deployment units. On the first scan, all deployment units that the scanner can find will
     * be returned. Subsequent scans will only return deployment units that are new since the last scan (or
     * call to reset() or clear())
     *
     * @return all new deployment units since the last scan
     */
    @Override
    public synchronized Collection<DeploymentUnit> scan() {
        List<DeploymentUnit> result = new ArrayList<>();
        Iterator<Plugin> iterator = dao.getPlugins();
        while (iterator.hasNext()) {
            Plugin plugin = iterator.next();
            Long version = pluginVersions.get(plugin.getId());
            if (version == null || version != plugin.getVersion()) {
                DeploymentUnit unit = deploy(plugin);
                result.add(unit);
            }
        }
        return result;
    }

    /**
     * Gets all deployment units currently being tracked by the scanner. This <i>will not</i> trigger
     * a scan, meaning that plugins that have been added since the last scan will not be returned.
     *
     * @return a collection of all deployment units currently being tracked by the scanner.
     */
    @Override
    public synchronized Collection<DeploymentUnit> getDeploymentUnits() {
        return deploymentUnits.keySet();
    }

    /**
     * Reset the scanner. This causes it to forget all state about which plugins have (or haven't) been loaded.
     */
    @Override
    public synchronized void reset() {
        deploymentUnits.clear();
        pluginVersions.clear();
        try {
            FileUtils.deleteDirectory(dir);
            if (!dir.mkdirs()) {
                log.error("Failed to create directory=" + dir);

            }
        } catch (IOException exception) {
            log.error("Failed to delete directory=" + dir + ": " + exception.getMessage(), exception);
        }
    }

    /**
     * Remove the specified deployment unit in such a way as it will not be picked up by subsequent scans, even
     * if the system is restarted.
     *
     * @param unit the deployment unit to remove
     * @throws PluginException if the unit has not been properly removed: i.e. a restart would mean the unit would
     *                         be reloaded.
     */
    @Override
    public synchronized void remove(DeploymentUnit unit) {
        Long id = deploymentUnits.get(unit);
        if (id == null) {
            throw new PluginException("Deployment unit not found: " + unit);
        }
        deploymentUnits.remove(unit);
        pluginVersions.remove(id);
        if (!unit.getPath().delete()) {
            throw new PluginException("Failed to remove deployment unit: " + unit);
        }
    }

    /**
     * Deploys a plugin.
     *
     * @param plugin the plugin
     * @return the deployment unit
     */
    private DeploymentUnit deploy(Plugin plugin) {
        InputStream binary = dao.getBinary(plugin.getKey());
        File file = new File(dir, plugin.getVersion() + "-" + plugin.getName());
        try (OutputStream out = new FileOutputStream(file)) {
            IOUtils.copy(binary, out);
        } catch (IOException exception) {
            throw new PluginException("Failed to write plugin to " + file + ": " + exception.getMessage(), exception);
        }
        Date modified = plugin.getUpdated();
        if (modified == null) {
            modified = plugin.getCreated();
        }
        if (modified == null || !file.setLastModified(modified.getTime())) {
            log.warn("Failed to set modified timestamp for " + file);
        }
        DeploymentUnit unit = new DeploymentUnit(file);
        deploymentUnits.put(unit, plugin.getId());
        pluginVersions.put(plugin.getId(), plugin.getVersion());
        return unit;
    }
}