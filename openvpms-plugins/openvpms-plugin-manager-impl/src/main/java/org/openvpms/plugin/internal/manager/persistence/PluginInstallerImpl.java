/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.persistence;

import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInstaller;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Default implementation of the {@link PluginInstaller}, backed by {@link PluginDAO}.
 *
 * @author Tim Anderson
 */
public class PluginInstallerImpl implements PluginInstaller {

    /**
     * The plugin DAO.
     */
    private final PluginDAO dao;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PluginInstallerImpl.class);

    /**
     * Constructs a {@link PluginDAO}.
     *
     * @param dao the plugin DAO
     */
    public PluginInstallerImpl(PluginDAO dao) {
        this.dao = dao;
    }

    /**
     * Installs the plugin with the given key. If the plugin already exists, it is replaced silently.
     *
     * @param key            the plugin key
     * @param pluginArtifact the plugin artifact
     */
    @Override
    public void installPlugin(String key, PluginArtifact pluginArtifact) {
        try (InputStream stream = pluginArtifact.getInputStream()) {
            dao.save(key, pluginArtifact.getName(), stream);
        } catch (IOException exception) {
            log.error("Failed to close stream for " + pluginArtifact.getName() + ": " + exception.getMessage(),
                      exception);
        }
    }
}