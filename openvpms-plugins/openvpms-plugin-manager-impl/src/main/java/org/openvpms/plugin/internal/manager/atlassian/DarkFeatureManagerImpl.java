/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.user.UserKey;

import java.util.Optional;

/**
 * No-op implementation of {@link DarkFeatureManager} to satisfy plugin component wiring.
 *
 * @author Tim Anderson
 */
public class DarkFeatureManagerImpl implements DarkFeatureManager {

    @Override
    public Optional<Boolean> isEnabledForAllUsers(String s) {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> isEnabledForCurrentUser(String s) {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> isEnabledForUser(UserKey userKey, String s) {
        return Optional.empty();
    }

    @Override
    public boolean isFeatureEnabledForAllUsers(String s) {
        return false;
    }

    @Override
    public boolean isFeatureEnabledForCurrentUser(String s) {
        return false;
    }

    @Override
    public boolean isFeatureEnabledForUser(UserKey userKey, String s) {
        return false;
    }

    @Override
    public boolean canManageFeaturesForAllUsers() {
        return false;
    }

    @Override
    public void enableFeatureForAllUsers(String s) {

    }

    @Override
    public void disableFeatureForAllUsers(String s) {

    }

    @Override
    public void enableFeatureForCurrentUser(String s) {

    }

    @Override
    public void enableFeatureForUser(UserKey userKey, String s) {

    }

    @Override
    public void disableFeatureForCurrentUser(String s) {

    }

    @Override
    public void disableFeatureForUser(UserKey userKey, String s) {

    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForAllUsers() {
        return null;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForCurrentUser() {
        return null;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForUser(UserKey userKey) {
        return null;
    }
}
