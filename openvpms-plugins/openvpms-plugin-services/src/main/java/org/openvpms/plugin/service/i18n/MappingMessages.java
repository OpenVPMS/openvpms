/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.service.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.model.object.IMObject;

/**
 * Messages reported by the mapping service.
 *
 * @author Tim Anderson
 */
public class MappingMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("MAP", MappingMessages.class.getName());

    /**
     * Creates a message indicating that a type is not valid for mapping.
     *
     * @param type the unsupported type
     * @return a new message
     */
    public static Message unsupportedType(Class<?> type) {
        return messages.create(100, type.getName());
    }

    /**
     * Creates a message indicating that an archetype is not valid for mapping.
     *
     * @param archetype the archetype
     * @param expected  the expected archetype
     * @return a new message
     */
    public static Message invalidArchetype(String archetype, String expected) {
        return messages.create(101, archetype, expected);
    }

    /**
     * Creates a message indicating that an archetype is not found.
     *
     * @param archetype the archetype
     * @return a new message
     */
    public static Message archetypeNotFound(String archetype) {
        return messages.create(102, archetype);
    }

    /**
     * Creates a message indicating that an archetype is not backed by a particular type.
     *
     * @param archetype the archetype
     * @param type      the type
     * @return a new message
     */
    public static <T extends IMObject> Message archetypeNotType(String archetype, Class<T> type) {
        return messages.create(103, archetype, type.getName());
    }

    /**
     * Creates a message indicating that multiple mappings for a source exist, when only one should be present.
     *
     * @param id   the source identifier
     * @param name the source name
     * @return a new message
     */
    public static Message multipleMappingsExistForSource(long id, String name) {
        return messages.create(104, id, name);
    }

    /**
     * Creates a message indicating that multiple mappings for a target exist, when only one should be present.
     *
     * @param id   the target identifier
     * @param name the target name
     * @return a new message
     */
    public static Message multipleMappingsExistForTarget(String id, String name) {
        return messages.create(105, id, name);
    }

}
