/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.auth.internal;

import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.seraph.auth.SimpleAbstractRoleMapper;

import java.security.Principal;

/**
 * Implementation of the Seraph {@link RoleMapper}.
 *
 * @author Tim Anderson
 */
public class RoleMapperImpl extends SimpleAbstractRoleMapper {

    /**
     * Default constructor.
     */
    public RoleMapperImpl() {
        super();
    }

    /**
     * Determines if a user has a role.
     *
     * @param user    the user
     * @param request the request
     * @param role    the role
     * @return {@code false}
     */
    @Override
    public boolean hasRole(Principal user, javax.servlet.http.HttpServletRequest request, String role) {
        return false;
    }
}
