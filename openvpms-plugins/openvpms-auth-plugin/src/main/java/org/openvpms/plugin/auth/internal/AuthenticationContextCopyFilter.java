/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.auth.internal;

import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.config.SecurityConfig;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Authentication context copy filter, lifted from the Atlassian Reference Application.
 * <p/>
 * This populates the {@link AuthenticationContext} and clears it when the last filter completes.
 *
 * @author Tim Anderson
 */
public class AuthenticationContextCopyFilter implements Filter {

    /**
     * The authentication context.
     */
    private final AuthenticationContext authenticationContext;

    /**
     * Tracks the depth of the chain.
     */
    private final ThreadLocal<Integer> entryCount = new ThreadLocal<>();

    /**
     * The security config.
     */
    private SecurityConfig securityConfig;

    /**
     * Constructs an {@link AuthenticationContextCopyFilter}.
     *
     * @param authenticationContext the authentication context
     */
    public AuthenticationContextCopyFilter(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    /**
     * Initialises the filter.
     *
     * @param filterConfig the filter config
     * @throws ServletException for any servlet error
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        securityConfig = (SecurityConfig) filterConfig.getServletContext().getAttribute(SecurityConfig.STORAGE_KEY);
        if (securityConfig == null) {
            throw new ServletException("No SecurityConfig found in servlet context!");
        }
    }

    /**
     * Filters the chain.
     *
     * @param request  the servlet request
     * @param response the servlet response
     * @param chain    the filter chain
     * @throws IOException      for any I/O error
     * @throws ServletException for any servlet error
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        authenticationContext.setUser(securityConfig.getAuthenticationContext().getUser());
        entryCount.set(entryCount.get() == null ? 1 : entryCount.get() + 1);
        try {
            chain.doFilter(request, response);
        } finally {
            entryCount.set(entryCount.get() - 1);
            if (entryCount.get() == 0) {
                authenticationContext.clearUser();
                entryCount.remove();
            }
        }
    }

    /**
     * Destroys the filter.
     */
    public void destroy() {
    }

}
