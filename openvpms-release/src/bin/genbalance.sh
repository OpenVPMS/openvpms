#!/bin/sh

. ./setenv.sh

java -Xmx512m -classpath $CLASSPATH -Dlog4j.configurationFile=file:../conf/log4j2.xml org.openvpms.archetype.tools.account.AccountBalanceTool --context ../conf/applicationContext.xml $*
