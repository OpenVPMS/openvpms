<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Investigation" pageWidth="612" pageHeight="792" columnWidth="554" leftMargin="29" rightMargin="29" topMargin="26" bottomMargin="26" isSummaryWithPageHeaderAndFooter="true" resourceBundle="localisation.reports" uuid="8c42a3ea-6f37-49f5-803e-e1f46987b877">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="9"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="dataSource" class="org.openvpms.report.jasper.DataSource" isForPrompting="false"/>
	<field name="createdBy.name" class="java.lang.String"/>
	<field name="startTime" class="java.util.Date"/>
	<field name="id" class="java.lang.Long"/>
	<field name="notes" class="java.lang.String"/>
	<field name="status.code" class="java.lang.String"/>
	<field name="patient.entity" class="java.lang.Object"/>
	<field name="patient.entity.name" class="java.lang.String"/>
	<field name="patient.entity.id" class="java.lang.Long"/>
	<field name="patient.entity.species" class="java.lang.String"/>
	<field name="patient.entity.breed" class="java.lang.String"/>
	<field name="patient.entity.dateOfBirth" class="java.util.Date"/>
	<field name="patient.entity.sex" class="java.lang.String"/>
	<field name="patient.entity.desexed" class="java.lang.Boolean"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="investigationType.entity.name" class="java.lang.String"/>
	<field name="clinician.entity.name" class="java.lang.String"/>
	<field name="document" class="java.lang.Object"/>
	<variable name="age" class="java.lang.String">
		<variableExpression><![CDATA[EVALUATE("patient:age(patient:get(), openvpms:get(., 'startTime'))")]]></variableExpression>
	</variable>
	<variable name="microchip" class="java.lang.String">
		<variableExpression><![CDATA[EVALUATE($F{patient.entity}, "openvpms:get(patient:microchip(.), 'identity')")]]></variableExpression>
	</variable>
	<variable name="clinician" class="java.lang.String">
		<variableExpression><![CDATA[EVALUATE("user:format(openvpms:get(., 'clinician.entity'), 'long')")]]></variableExpression>
	</variable>
	<variable name="owner" class="java.lang.Object">
		<variableExpression><![CDATA[EVALUATE("patient:owner(patient:get(), openvpms:get(.,'startTime'))")]]></variableExpression>
	</variable>
	<variable name="ownerName" class="java.lang.String">
		<variableExpression><![CDATA[EVALUATE($V{owner}, "openvpms:get(.,'name')")]]></variableExpression>
	</variable>
	<variable name="resultsCount" class="java.lang.Integer"/>
	<title>
		<band height="32" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="74c0a259-4415-4de5-bf2c-8abecc75564c">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("patient")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</title>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="147" splitType="Stretch">
			<property name="local_mesure_unitheight" value="pixel"/>
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<rectangle>
				<reportElement x="0" y="1" width="554" height="134" backcolor="#E8E8E8" uuid="2442bd88-884e-431f-8200-6f3ef1ce0a83">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#FFFFFF"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement x="288" y="60" width="79" height="13" uuid="e8664dd1-4444-442f-8971-280c51152483">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<text><![CDATA[Microchip:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="60" width="175" height="13" uuid="d10d224e-d91f-404d-8eff-f07388c86864">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$V{microchip}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="42" width="103" height="13" uuid="9e85db4d-0c2e-484e-bb4e-8e8a10edfe95">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Pet:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="42" width="175" height="13" uuid="ee1821ea-0998-45e5-9323-8d8853d9f5ec">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.name}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="78" width="175" height="13" uuid="94461bc9-81ac-4d12-903e-7ac126156572">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.species}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="78" width="103" height="13" uuid="6e20a757-0191-4cb3-9978-c6bd37786717">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Species:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="78" width="175" height="13" uuid="2588e3b2-8aa6-4099-a379-261c83e1b7b4">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.breed}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="288" y="78" width="79" height="13" uuid="340824da-9545-4991-b2d8-aa430ef7f11e">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Breed:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="114" width="175" height="13" uuid="edc1bc20-d8ad-40c2-adf8-06ad142309f6">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.dateOfBirth} != null ? DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{patient.entity.dateOfBirth}) : null]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="114" width="103" height="13" uuid="29a066df-8f72-44ce-9431-e4d379ca3bdb">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Date Of birth:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="96" width="175" height="13" uuid="590b6bd7-84cb-4fc6-afe8-2389ee7e153b">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.sex}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="96" width="103" height="13" uuid="403dc290-4dd0-4d0e-a8d3-bfd5a264322f">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<text><![CDATA[Sex:]]></text>
			</staticText>
			<staticText>
				<reportElement x="288" y="96" width="79" height="13" uuid="4e852403-b67e-4746-bc44-c8134d2f4282">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<text><![CDATA[Desexed:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="96" width="175" height="13" uuid="9ef5ceaa-fdcf-4999-a65e-81f81e7e93ce">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.desexed} ? "Yes" : "No"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="288" y="114" width="79" height="13" uuid="89c01b0e-3e2b-470b-a2d5-89f94f8e4037">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<text><![CDATA[Age:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="114" width="175" height="13" uuid="6de68a05-9b44-410e-8036-66095587de95">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$V{age}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="7" width="103" height="13" uuid="004fc140-7cba-49a0-9671-2a1ca4ae94f3">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Investigation:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="7" width="175" height="13" uuid="ee431eeb-6826-499f-99ed-f4c039af351c">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{investigationType.entity.name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="288" y="7" width="79" height="13" uuid="137086c2-6a80-4fa4-875a-db3c8093aec0"/>
				<text><![CDATA[Date:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="7" width="175" height="13" uuid="047aaad5-6111-44f4-9569-17325633abc3">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{startTime})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="60" width="103" height="13" uuid="651dee9b-6b0d-4230-a255-4bb6dd5eb49e">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Owner:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="60" width="175" height="13" uuid="1da90644-284f-4f04-9b47-9fb261ff1195">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$V{ownerName}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="288" y="42" width="79" height="13" uuid="d26ebc4c-88a6-4353-bf38-609b10eb1066">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<text><![CDATA[Pet Id:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="42" width="175" height="13" uuid="f1d22564-3d18-4303-be37-f31b319e871e">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{patient.entity.id}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="5" y="24" width="103" height="14" uuid="cde0c9ad-4a80-4b15-8e12-5d651a301b44">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Investigation Id:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="108" y="24" width="175" height="14" uuid="d32a69ca-79c8-464d-ac53-e6a0f8492c9a">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="288" y="24" width="79" height="14" uuid="509204bd-8e4f-4681-8239-ecb71e09dc9b">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<text><![CDATA[Clinician:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="368" y="24" width="175" height="14" uuid="214aad56-65d7-4d1c-b0a8-1e8fe4b16d09">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box leftPadding="0"/>
				<textFieldExpression><![CDATA[$F{clinician.entity.name}]]></textFieldExpression>
			</textField>
		</band>
		<band height="34">
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" x="0" y="0" width="554" height="34" uuid="4fa180fe-04f2-4e95-b540-0c6b0358ca8c">
					<property name="local_mesure_unitwidth" value="pixel"/>
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("results.target", false, "results.sequence")]]></dataSourceExpression>
				<returnValue subreportVariable="REPORT_COUNT" toVariable="resultsCount"/>
				<subreportExpression><![CDATA["Investigation Results.jrxml"]]></subreportExpression>
			</subreport>
		</band>
		<band height="34">
			<printWhenExpression><![CDATA[$V{resultsCount} == 0 && $F{document} == null]]></printWhenExpression>
			<staticText>
				<reportElement x="0" y="0" width="554" height="34" uuid="4ac68828-2cf2-40f8-b882-e88ada3e2953"/>
				<box leftPadding="5">
					<pen lineWidth="1.0"/>
				</box>
				<textElement>
					<font isBold="false"/>
				</textElement>
				<text><![CDATA[No results have been received.]]></text>
			</staticText>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
</jasperReport>
