NOTE: due to a limitation of the template load, a number of templates are duplicates.

The following templates are duplicates of Patient Single Reminder.jrxml:
  . Patient Single Overdue Reminder.jrxml
  . Patient Desexing Reminder.jrxml

The following templates are duplicates of Patient Collection Reminder.jrxml:
  . Patient Collection Overdue Reminder.jrxml