<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Patient Grouped Reminder" pageWidth="612" pageHeight="792" columnWidth="554" leftMargin="29" rightMargin="29" topMargin="26" bottomMargin="26" uuid="774b9ad9-b9f2-4a91-8e52-56c94700044f">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="9"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="dataSource" class="org.openvpms.report.jasper.DataSource" isForPrompting="false"/>
	<field name="endTime" class="java.util.Date"/>
	<field name="customer.id" class="java.lang.Long"/>
	<field name="customer.initials" class="java.lang.String"/>
	<field name="customer.title" class="java.lang.String"/>
	<field name="customer.firstName" class="java.lang.String"/>
	<field name="customer.lastName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="customer.companyName" class="java.lang.String"/>
	<field name="patient.name" class="java.lang.String"/>
	<field name="[party:getPartyFullName(openvpms:get(.,&apos;customer&apos;))]" class="java.lang.String"/>
	<field name="[party:getBillingAddress(openvpms:get(.,&apos;customer&apos;))]" class="java.lang.String"/>
	<field name="reminderCount" class="java.lang.Integer"/>
	<field name="reminderType.name" class="java.lang.String"/>
	<field name="product.name" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.groupRemindMsg" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="reminderType.note" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="OpenVPMS.location.name" class="java.lang.String"/>
	<variable name="CustomerFullName" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[(($F{customer.title} == null) ? "": $F{customer.title}) + " " + (($F{customer.firstName} == null) ? "": $F{customer.firstName}) + " " + $F{customer.lastName}]]></variableExpression>
	</variable>
	<variable name="Salutation" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[($F{customer.firstName}.length() <= 2) ? ($F{customer.lastName} + " Family"):($F{customer.firstName})]]></variableExpression>
	</variable>
	<group name="customerIdGroup">
		<groupExpression><![CDATA[$F{customer.id}]]></groupExpression>
		<groupHeader>
			<band height="207" splitType="Stretch">
				<textField isBlankWhenNull="false">
					<reportElement key="textField-1" positionType="Float" x="0" y="141" width="336" height="18" uuid="85647cc4-1b1f-45ba-85f1-5a903e7b1645"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textFieldExpression><![CDATA["Dear "+ $V{Salutation} + ","]]></textFieldExpression>
				</textField>
				<subreport isUsingCache="false">
					<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="30cb3cb0-3c14-46c6-b422-5b6ec742523c">
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<subreportParameter name="IsEmail">
						<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="pageNo">
						<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
					</subreportParameter>
					<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
					<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
				</subreport>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-3" positionType="Float" x="441" y="61" width="82" height="14" uuid="be0b1c94-37df-44ce-9bf1-c67ec7914658">
						<property name="local_mesure_unitheight" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-3" positionType="Float" x="388" y="61" width="41" height="14" uuid="a3ce408e-96e4-473a-bcc4-f627c2236409">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Date]]></text>
				</staticText>
				<subreport>
					<reportElement positionType="Float" x="0" y="24" width="381" height="103" uuid="4d4d426b-2cb0-416e-aba1-439666209169">
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<subreportParameter name="Addr1">
						<subreportParameterExpression><![CDATA[$F{customer.companyName}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="Addr2">
						<subreportParameterExpression><![CDATA[$V{CustomerFullName}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter name="Addr3">
						<subreportParameterExpression><![CDATA[$F{[party:getBillingAddress(openvpms:get(.,'customer'))]}]]></subreportParameterExpression>
					</subreportParameter>
					<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("patient")]]></dataSourceExpression>
					<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+" AddressBlock"+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
				</subreport>
				<staticText>
					<reportElement key="staticText-1" positionType="Float" x="388" y="24" width="166" height="27" uuid="c99aa075-dd74-4ea2-a5f7-9a5c7d0439e7"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font size="16" isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Reminder]]></text>
				</staticText>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement positionType="Float" x="0" y="170" width="554" height="28" uuid="e2b7cff0-405e-4a09-b6b0-caeedec0a721"/>
					<textElement verticalAlignment="Top"/>
					<textFieldExpression><![CDATA[$F{patient.name} + "'s" + " health and wellness is important to us. Our records indicate that " + $F{patient.name} + " is due for the following important health services:"]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="53" splitType="Stretch">
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement x="21" y="24" width="504" height="22" isPrintWhenDetailOverflows="true" uuid="261a2124-ce85-4f3b-bd2d-1a1ed0fbf084"/>
				<textElement verticalAlignment="Top"/>
				<textFieldExpression><![CDATA[$F{reminderType.note}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" x="0" y="3" width="554" height="17" isPrintWhenDetailOverflows="true" uuid="63e41c13-d570-40c4-bed9-49fc394b1ba7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reminderType.name} + (($F{endTime}.after(new Date()))?" due by ":" overdue by ") + DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{endTime})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="145" splitType="Stretch">
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="staticText-1" x="0" y="6" width="554" height="107" uuid="55525503-29c6-46dd-b167-0498c8c00421">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<printWhenExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.groupRemindMsg}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.groupRemindMsg}.trim().length()>0)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font isBold="false" isItalic="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{OpenVPMS.location.letterhead.target.groupRemindMsg}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight">
				<reportElement positionType="Float" x="0" y="120" width="388" height="15" uuid="81a87914-3ad7-4e4e-84bf-11f19d0297e0"/>
				<textFieldExpression><![CDATA["From the Team at " + $F{OpenVPMS.location.name}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
