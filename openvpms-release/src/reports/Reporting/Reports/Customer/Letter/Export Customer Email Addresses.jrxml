<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Export Customer Email Addresses for Sales" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="792" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="a28dd50a-b02e-4be9-99cb-d2eae007144b">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="OpenVPMS"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" vTextAlign="Middle" fontSize="10"/>
	<parameter name="Species" class="java.lang.String">
		<parameterDescription><![CDATA[Species selection]]></parameterDescription>
	</parameter>
	<parameter name="Breed" class="java.lang.String">
		<parameterDescription><![CDATA[Breed selection]]></parameterDescription>
	</parameter>
	<parameter name="DOB Start" class="java.util.Date">
		<parameterDescription><![CDATA[Date of Birth from]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date("01/01/1980")]]></defaultValueExpression>
	</parameter>
	<parameter name="DOB End" class="java.util.Date">
		<parameterDescription><![CDATA[Date of Birth to]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Classification" class="java.lang.String">
		<parameterDescription><![CDATA[Acct Type or Category]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="PracticeLocation" class="java.lang.String">
		<parameterDescription><![CDATA[Practice Location]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="Explain" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Display explanation ?]]></parameterDescription>
		<defaultValueExpression><![CDATA[false]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[SELECT customer.entity_id      AS id,
       last_name.value         AS lastname,
       first_name.value        AS firstname,
       title_lookup.name       AS title,
       customer.description    AS description,
       c_email.name            AS emailName,
       email.value             AS email,
       IFNULL(contact_rank, 0) AS contact_rank
FROM entities customer
         LEFT JOIN entity_details last_name
                   ON last_name.entity_id = customer.entity_id
                       AND last_name.name = 'lastName'
         LEFT JOIN entity_details first_name
                   ON first_name.entity_id = customer.entity_id
                       AND first_name.name = 'firstName'
         LEFT JOIN entity_details title
                   ON title.entity_id = customer.entity_id AND title.name = 'title'
         LEFT JOIN lookups title_lookup
                   ON title_lookup.code = title.value
                       AND title_lookup.arch_short_name = 'lookup.personTitle'
         LEFT JOIN entity_links l_location
                   ON l_location.source_id = customer.entity_id
                       AND l_location.arch_short_name = 'entityLink.customerLocation'
         LEFT JOIN entities location
                   ON location.entity_id = l_location.target_id
         JOIN (SELECT c1.party_id,
                           SUBSTRING_INDEX(GROUP_CONCAT(c1.contact_id ORDER BY c1.contact_id DESC), ',', 1) AS contact_id,
                           SUBSTRING_INDEX(GROUP_CONCAT(c1.contact_rank ORDER BY c1.contact_rank DESC), ',', 1) AS contact_rank
                    FROM (SELECT c_contact.party_id,
                                 c_contact.contact_id,
                                 IF((preferred.value = 'true'), IF((purpose.code = 'CORRESPONDENCE'), 30, 10),
                                    IF((purpose.code = 'CORRESPONDENCE'), 20, 0)) AS contact_rank
                          FROM contacts c_contact
                                   LEFT JOIN contact_details preferred
                                             ON preferred.contact_id = c_contact.contact_id
                                                 AND preferred.name = 'preferred'
                                   LEFT JOIN contact_classifications c_classifications
                                             ON c_classifications.contact_id = c_contact.contact_id
                                   LEFT JOIN lookups purpose
                                             ON c_classifications.lookup_id = purpose.lookup_id
                                                 AND purpose.arch_short_name = 'lookup.contactPurpose'
                          WHERE c_contact.arch_short_name = 'contact.email') c1
                             LEFT JOIN (
                        SELECT c_contact.party_id,
                               c_contact.contact_id,
                               IF((preferred.value = 'true'), IF((purpose.code = 'CORRESPONDENCE'), 30, 10),
                                  IF((purpose.code = 'CORRESPONDENCE'), 20, 0)) AS contact_rank
                        FROM contacts c_contact
                                 LEFT JOIN contact_details preferred
                                           ON preferred.contact_id = c_contact.contact_id
                                               AND preferred.name = 'preferred'
                                 LEFT JOIN contact_classifications c_classifications
                                           ON c_classifications.contact_id = c_contact.contact_id
                                 LEFT JOIN lookups purpose
                                           ON c_classifications.lookup_id = purpose.lookup_id
                                               AND purpose.arch_short_name = 'lookup.contactPurpose'
                        WHERE c_contact.arch_short_name = 'contact.email') c2
                                       ON c1.party_id = c2.party_id
                                           AND ((c2.contact_rank > c1.contact_rank)
                                                OR c2.contact_rank = c1.contact_rank AND c2.contact_id < c1.contact_id)
                    WHERE c2.party_id IS NULL
                    GROUP BY c1.party_id) unique_contact 
                       ON unique_contact.party_id = customer.entity_id 
         LEFT JOIN contacts c_email 
                   ON c_email.contact_id = unique_contact.contact_id	 
         LEFT JOIN contact_details email
                   ON email.contact_id = unique_contact.contact_id
                       AND email.name = 'emailAddress'
WHERE customer.arch_short_name = 'party.customerperson'
  AND customer.active = 1
  AND EXISTS(SELECT 1
             FROM entity_relationships r_owner
                      JOIN entities patient
                           ON patient.entity_id = r_owner.target_id
                      LEFT JOIN entity_details birth_date
                           ON birth_date.entity_id = patient.entity_id
                                    AND birth_date.name = 'dateOfBirth'
                      LEFT JOIN entity_details species
                           ON species.entity_id = patient.entity_id
                                    AND species.name = 'species'
                      LEFT JOIN lookups species_lookup
                           ON species_lookup.code = species.value
                                    AND species_lookup.arch_short_name = 'lookup.species'
                      LEFT JOIN entity_details breed
                           ON breed.entity_id = patient.entity_id
                                    AND breed.name = 'breed'
                      LEFT JOIN lookups breed_lookup
                           ON breed_lookup.code = breed.value
                                    AND breed_lookup.arch_short_name = 'lookup.breed'
             WHERE r_owner.source_id = customer.entity_id
               AND r_owner.arch_short_name = 'entityRelationship.patientOwner'
               AND r_owner.active_end_time IS NULL
               AND patient.active = 1
               AND species_lookup.name LIKE CONCAT(IFNULL($P{Species}, ''), '%')
               AND ((breed_lookup.name IS NULL AND $P{Breed} IS NULL) OR
                    (breed_lookup.name LIKE CONCAT(IFNULL($P{Breed}, ''), '%')))
               AND ((birth_date.value >= $P{DOB Start} AND birth_date.value < DATE_ADD($P{DOB End}, INTERVAL 1 DAY))
                 OR birth_date.value IS NULL))
  AND (IFNULL($P{Classification}, 1) OR EXISTS (
		SELECT 1
        FROM entity_classifications c_classification
         JOIN lookups l_type
                   ON l_type.lookup_id = c_classification.lookup_id
                       AND l_type.arch_short_name IN ('lookup.customerAccountType', 'lookup.customerType')
                       AND l_type.name LIKE CONCAT($P{Classification}, '%')
		WHERE c_classification.entity_id = customer.entity_id))
  AND IFNULL(location.name, '-') LIKE CONCAT(IFNULL($P{PracticeLocation}, ''), '%')
ORDER BY customer.name, customer.entity_id]]>
	</queryString>
	<field name="id" class="java.lang.Long"/>
	<field name="lastname" class="java.lang.String"/>
	<field name="firstname" class="java.lang.String"/>
	<field name="title" class="java.lang.String"/>
	<field name="contact_rank" class="java.lang.Integer"/>
	<field name="description" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="emailName" class="java.lang.String"/>
	<field name="email" class="java.lang.String"/>
	<variable name="EmailAddr" class="java.lang.String" resetType="Group" resetGroup="id-group" calculation="First">
		<variableExpression><![CDATA[$F{email}]]></variableExpression>
	</variable>
	<variable name="AllEmailAddresses" class="java.lang.String" incrementType="Group" incrementGroup="id-group">
		<variableExpression><![CDATA[$V{id-group_COUNT}==1?$V{AllEmailAddresses}+$V{EmailAddr}+"; ":$V{AllEmailAddresses}]]></variableExpression>
		<initialValueExpression><![CDATA[""]]></initialValueExpression>
	</variable>
	<group name="id-group" minHeightToStartNewPage="18">
		<groupExpression><![CDATA[$F{id}]]></groupExpression>
		<groupHeader>
			<band height="19">
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="0" y="0" width="75" height="13" uuid="1f61084b-39f6-48f2-92b5-7008961f066c"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="75" y="0" width="75" height="13" uuid="59535b4c-62a3-456b-a068-f53f21963257"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{title}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="150" y="0" width="76" height="13" uuid="3ec4c1a4-73c3-4190-9327-0b7c8accedab"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{lastname}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="226" y="0" width="75" height="13" uuid="c41c48f2-2ef9-4125-ba9d-31b7a5c2bd65"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{firstname}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" pattern="" isBlankWhenNull="true">
					<reportElement key="textField" x="301" y="0" width="150" height="13" uuid="678f3e41-e5e2-4d8f-a6c5-d7078f8e8f5d"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{description}.replace( '\n', ' ' )]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="451" y="0" width="76" height="13" uuid="46a080bc-12c0-49e8-b3c6-b15145761cda"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{emailName}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="527" y="0" width="75" height="13" uuid="c152e13a-7d02-4ffb-8979-f028100c492b"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{email}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" pattern="" isBlankWhenNull="true">
					<reportElement key="textField-2" x="602" y="0" width="75" height="13" uuid="fe10b06e-2a4d-4893-8a84-2a437da30838">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{contact_rank}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="4" width="75" height="11" uuid="e9441f6b-432a-461d-913d-e53654a659b1"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[CLIENTID]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" x="75" y="4" width="75" height="11" uuid="469f3585-c90b-47a3-9e73-58ab1e0ea272"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[TITLE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="150" y="4" width="76" height="11" uuid="79ac29a1-5830-4a9d-80ff-a732b80a6b08"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[LASTNAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="226" y="4" width="75" height="11" uuid="89a3cdf8-8480-49cf-80de-e859e5a81d65"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[FIRSTNAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="301" y="4" width="150" height="11" uuid="102705fb-7670-41a5-91c7-02494ac52a97"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[ADDRESS]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="451" y="4" width="76" height="11" uuid="8e538207-efab-4db9-9d79-6df35dd7980e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[EMAIL-NAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="527" y="4" width="75" height="11" uuid="c173fcea-0594-4be7-898f-5e5d58dc72c4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[EMAIL-ADDRESS]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="602" y="4" width="75" height="11" uuid="794e978b-14f1-4e06-b35c-763d8bf5c5bb">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[RANK]]></text>
			</staticText>
		</band>
	</columnHeader>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="611" splitType="Prevent">
			<staticText>
				<reportElement x="0" y="1" width="308" height="21" isRemoveLineWhenBlank="true" uuid="6a93ce36-4663-4d52-8eab-d8373dbeb7dc">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()==0)]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No Data Found - Check Parameters]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="82" width="792" height="216" isRemoveLineWhenBlank="true" uuid="78b86826-9d3f-479a-a660-81f025032d10">
					<printWhenExpression><![CDATA[$P{Explain}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[This report is designed for exporting customer email contact data for use with mail merge facilities.
It finds the customers who match the selection conditions.
All the text selection fields entered have % added so that leaving them blank will find all, and entering say breed %terrier will find all species containing 'terrier' in their species name, and account type'c' will include all types starting C. The selection is case-insensitive.
The Patient DOB selection is inclusive (ie from <= DOB<= to), but where Patients have no DOB they will be included.
If there is no Practice Location selection then customers with no Practice Location set will be included. Using the selection '-' will select just those with no Practice Location set. The same applies for the Account Type/Category selection.
Only active customers with active patients are included.
Where the customer has multiple contacts, these are ranked as follows:
  If preferred and purpose Correspondence - score 30; If purpose Correspondence - score 20; If preferred - score 10; else score 0.
The highest rank contact is used, and if more than one has the highest rank, then the newest is used.
Note that prior to using in mail-merge or other programs, you must remove the information and explanation lines following the data lines.
The cell below this one contains all the addresses concatenated. It is in 1 point font so as to fit in as many as possible. You will not be able to read these in the printed or preview version, but in the exported CSV file they appear correctly. The field can contain over 10,000 email addresses.
See http://chandoo.org/wp/2014/01/13/combine-text-values-quick-tip/ if you need to rebuild this list after removing some data.]]></text>
			</staticText>
			<frame>
				<reportElement x="0" y="21" width="640" height="58" uuid="bd7289c7-a885-4a23-9928-733f955ceed7"/>
				<textField>
					<reportElement x="75" y="14" width="75" height="15" uuid="215a82ec-5ad9-4a77-81b0-8e9864de9fce">
						<property name="local_mesure_unitheight" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Species}==null)?"%":$P{Species}+"%"]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="226" y="14" width="75" height="15" uuid="1a04c237-ae70-4bea-831e-8c27df127bff">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Breed}==null)?"%":$P{Breed}+"%"]]></textFieldExpression>
				</textField>
				<textField pattern="MM/dd/yyyy">
					<reportElement x="75" y="28" width="75" height="15" uuid="9f999226-8982-470a-b545-71f039f4cda1">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{DOB Start})]]></textFieldExpression>
				</textField>
				<textField pattern="MM/dd/yyyy">
					<reportElement x="226" y="28" width="75" height="15" uuid="014fd923-013a-4310-a47f-752cf83c1a48">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{DOB End})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="0" width="74" height="14" uuid="2fe82852-700a-42b8-8f0a-c46390f1ce32">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Parameters]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="14" width="74" height="15" uuid="49b50123-f652-41f3-89b7-0d9857e04658">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Species]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="28" width="74" height="15" uuid="11e6ddc9-93e6-45af-bebf-b8285ec548f9">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[DOB From]]></text>
				</staticText>
				<staticText>
					<reportElement x="151" y="14" width="75" height="15" uuid="c6706af9-9c20-403c-b578-eea5c866d800">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Breed]]></text>
				</staticText>
				<staticText>
					<reportElement x="151" y="28" width="75" height="15" uuid="53321795-57cc-4940-86d1-c1831df0265c">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[DOB To]]></text>
				</staticText>
				<staticText>
					<reportElement x="452" y="14" width="75" height="15" uuid="5b1ad351-97c0-404f-aa7e-a62d96cb9e75">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Generated]]></text>
				</staticText>
				<textField pattern="dd/MM/yyyy h.mm a">
					<reportElement x="527" y="14" width="94" height="15" uuid="c5786561-aedd-4400-a255-b5064661adfe">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="43" width="74" height="14" uuid="6435ddd8-a435-447f-b5ca-df2a268b0992">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Acct.Type/Cat]]></text>
				</staticText>
				<textField>
					<reportElement x="75" y="43" width="75" height="14" uuid="3773a6da-7671-4fd5-9e5e-e9b525d02a73">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Classification}==null)?"%":$P{Classification}+"%"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="151" y="43" width="75" height="14" uuid="0f523ae7-0153-45b2-8d4a-0eab8e7778bf">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Practice Locn]]></text>
				</staticText>
				<textField>
					<reportElement x="226" y="43" width="75" height="14" uuid="52c96da9-2f18-44c5-9a89-3151546fa9ca">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{PracticeLocation}==null)?"%":$P{PracticeLocation}+"%"]]></textFieldExpression>
				</textField>
			</frame>
			<textField textAdjust="StretchHeight" evaluationTime="Report">
				<reportElement positionType="Float" stretchType="RelativeToBandHeight" x="0" y="298" width="792" height="313" isPrintWhenDetailOverflows="true" uuid="b78ec3a8-ebf0-46b0-88b1-beeec609c584"/>
				<textElement>
					<font size="1"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{AllEmailAddresses}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
