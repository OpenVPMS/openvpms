<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Customer No Show Report" pageWidth="612" pageHeight="792" whenNoDataType="AllSectionsNoDetail" columnWidth="550" leftMargin="31" rightMargin="31" topMargin="19" bottomMargin="19" isSummaryWithPageHeaderAndFooter="true" uuid="66954a41-1582-4de7-8e54-4893d0674585">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="OpenVPMS"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="202"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="791"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="9"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="fromDate" class="java.util.Date">
		<parameterDescription><![CDATA[From Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="toDate" class="java.util.Date">
		<parameterDescription><![CDATA[To Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="showDetails" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Show Details]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="explain" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Display Explanation]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[SELECT appointment.act_id appointment_id,
	   appointment.activity_start_time start_time,
	   appointment.activity_end_time end_time,
       customer.entity_id customer_id,
       customer.name customer_name,
       customer.description customer_description,
       patient.entity_id patient_id,
       patient.name patient_name,
       reason.name reason,
       appointment_type.entity_id appointent_type_id,
       appointment_type.name appointment_type_name
FROM acts appointment
LEFT JOIN participations pcustomer 
	ON appointment.act_id = pcustomer.act_id 
		AND pcustomer.arch_short_name = 'participation.customer'
LEFT JOIN entities customer
	ON customer.entity_id = pcustomer.entity_id
LEFT JOIN participations ppatient
	ON appointment.act_id = ppatient.act_id 
		AND ppatient.arch_short_name = 'participation.patient'
LEFT JOIN entities patient
	ON patient.entity_id = ppatient.entity_id
LEFT JOIN participations pappointment_type
	ON pappointment_type.act_id = appointment.act_id
		AND pappointment_type.arch_short_name = 'participation.appointmentType'
LEFT JOIN entities appointment_type 
	ON pappointment_type.entity_id = appointment_type.entity_id
LEFT JOIN lookups reason
	ON reason.code = appointment.reason
		AND reason.arch_short_name IN ('lookup.visitReason', 'lookup.visitReasonVeNom')
WHERE appointment.arch_short_name = 'act.customerAppointment'
	AND appointment.status = 'NO_SHOW'
	AND appointment.activity_start_time BETWEEN $P{fromDate} AND $P{toDate}
ORDER BY customer.name, customer_id, patient.name, patient_id, appointment.activity_start_time]]>
	</queryString>
	<field name="appointment_id" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.name" value="act_id"/>
		<property name="com.jaspersoft.studio.field.label" value="appointment_id"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="acts"/>
	</field>
	<field name="start_time" class="java.sql.Timestamp">
		<property name="com.jaspersoft.studio.field.name" value="activity_start_time"/>
		<property name="com.jaspersoft.studio.field.label" value="start_time"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="acts"/>
	</field>
	<field name="end_time" class="java.sql.Timestamp">
		<property name="com.jaspersoft.studio.field.name" value="activity_end_time"/>
		<property name="com.jaspersoft.studio.field.label" value="end_time"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="acts"/>
	</field>
	<field name="customer_id" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.name" value="entity_id"/>
		<property name="com.jaspersoft.studio.field.label" value="customer_id"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="customer_name" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.name" value="name"/>
		<property name="com.jaspersoft.studio.field.label" value="customer_name"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="customer_description" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.name" value="description"/>
		<property name="com.jaspersoft.studio.field.label" value="customer_description"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="patient_id" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.name" value="entity_id"/>
		<property name="com.jaspersoft.studio.field.label" value="patient_id"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="patient_name" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.name" value="name"/>
		<property name="com.jaspersoft.studio.field.label" value="patient_name"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="appointent_type_id" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.name" value="entity_id"/>
		<property name="com.jaspersoft.studio.field.label" value="appointent_type_id"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="appointment_type_name" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.name" value="name"/>
		<property name="com.jaspersoft.studio.field.label" value="appointment_type_name"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="entities"/>
	</field>
	<field name="reason" class="java.lang.String"/>
	<variable name="previousCustomerId" class="java.lang.Long">
		<variableExpression><![CDATA[$V{currentCustomerId}]]></variableExpression>
	</variable>
	<variable name="currentCustomerId" class="java.lang.Long">
		<variableExpression><![CDATA[$F{customer_id}]]></variableExpression>
	</variable>
	<group name="CustomerGroup">
		<groupExpression><![CDATA[$F{customer_id}]]></groupExpression>
		<groupHeader>
			<band height="1">
				<property name="com.jaspersoft.studio.unit.height" value="px"/>
				<printWhenExpression><![CDATA[$P{showDetails} && $V{REPORT_COUNT} != 0]]></printWhenExpression>
				<line>
					<reportElement key="line-1" x="0" y="0" width="550" height="1" isRemoveLineWhenBlank="true" forecolor="#CDCDCD" uuid="83b0ed1e-8035-4c6f-9855-5daa55387c5e">
						<property name="com.jaspersoft.studio.unit.width" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="13">
				<property name="com.jaspersoft.studio.unit.height" value="px"/>
				<printWhenExpression><![CDATA[!$P{showDetails}]]></printWhenExpression>
				<textField textAdjust="ScaleFont" isBlankWhenNull="true">
					<reportElement isPrintRepeatedValues="false" x="0" y="0" width="67" height="10" uuid="23bc50f7-26c0-4aab-9121-56f4c803345b">
						<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="5874cf54-db5e-4d0e-86bc-53b79432ead0"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="px"/>
					</reportElement>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_id}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement isPrintRepeatedValues="false" x="75" y="0" width="131" height="10" uuid="7345a891-b37c-4dec-9ba2-c1531f533054">
						<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="50814c4d-e2ce-4611-8253-f38525ea74e4"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_name}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement isPrintRepeatedValues="false" x="218" y="0" width="265" height="10" uuid="daf4e68a-a391-42fe-b74e-577e72ba9b0d">
						<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="b2f48c14-e2d9-47f9-b64a-a5c3f2cc1291"/>
					</reportElement>
					<textElement>
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{customer_description}]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont">
					<reportElement x="494" y="0" width="49" height="10" uuid="6b58d2c6-6099-4311-93d1-a021716903a4">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement textAlignment="Right">
						<font size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{CustomerGroup_COUNT}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="57" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement key="textField" x="284" y="37" width="62" height="16" uuid="f34d92f0-715c-4c7b-bd43-336ad3dca081"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{toDate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" x="218" y="37" width="62" height="16" uuid="f09560a2-8e61-4efd-b25d-a7e22ce4d213"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Date: ]]></text>
			</staticText>
			<textField evaluationTime="Master" isBlankWhenNull="false">
				<reportElement key="textField-1" x="436" y="36" width="114" height="17" uuid="f2af73eb-273b-4ad9-8b25-01e9d451ddbd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{MASTER_CURRENT_PAGE} +" of " + $V{MASTER_TOTAL_PAGES}]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement x="459" y="1" width="92" height="10" uuid="caa6bca0-04b3-47c1-bf8e-f79a2d03429c"/>
				<textElement textAlignment="Right">
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-1" x="122" y="1" width="305" height="23" uuid="459709cf-81cb-44c9-bcb6-29d6a246c38a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="16" isBold="true" isUnderline="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer No Show Report]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="0" y="37" width="62" height="16" uuid="d7339a3d-7e69-4b47-91dc-e1e440f464d2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[From Date: ]]></text>
			</staticText>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement key="textField" x="75" y="37" width="62" height="16" uuid="0d6fa52c-da67-40c9-8aec-f1c261848bb4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{fromDate})]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<pageHeader>
		<band height="22" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER} != 1]]></printWhenExpression>
			<textField evaluationTime="Master" isBlankWhenNull="false">
				<reportElement key="textField-1" x="436" y="0" width="114" height="17" uuid="98b786ec-67bf-44b9-ae71-05eaa96bb599"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{MASTER_CURRENT_PAGE} +" of " + $V{MASTER_TOTAL_PAGES}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<line>
				<reportElement key="line-1" x="0" y="16" width="550" height="1" uuid="66d2a644-94d7-4192-832a-048883d7509e">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
			</line>
			<staticText>
				<reportElement key="staticText-15" x="1" y="0" width="113" height="16" uuid="af84fcf0-5dbb-4681-8d98-3d68bf3bdd61"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer]]></text>
			</staticText>
			<staticText>
				<reportElement x="284" y="0" width="101" height="16" uuid="16810624-9ea1-47e6-b70c-8ba0e245dc00">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="ff3a0635-363f-4540-9ef2-39d8e998a836"/>
					<printWhenExpression><![CDATA[$P{showDetails}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Appointment]]></text>
			</staticText>
			<staticText>
				<reportElement x="483" y="0" width="60" height="16" uuid="ef036ea4-d6a7-4632-ad47-ead3084ee9db">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="ff3a0635-363f-4540-9ef2-39d8e998a836"/>
					<printWhenExpression><![CDATA[!$P{showDetails}]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[No Shows]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="34" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<printWhenExpression><![CDATA[$P{showDetails}]]></printWhenExpression>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement isPrintRepeatedValues="false" x="0" y="1" width="67" height="10" uuid="eb172b24-c08c-4f8b-95e7-5d325365c220">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="5874cf54-db5e-4d0e-86bc-53b79432ead0"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_id}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement isPrintRepeatedValues="false" x="75" y="1" width="206" height="10" uuid="e3d062ac-e150-49fd-b691-747cd26853e7">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="50814c4d-e2ce-4611-8253-f38525ea74e4"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_name}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement isPrintRepeatedValues="false" x="75" y="10" width="206" height="11" uuid="0532a385-ed2b-48d6-a804-36ecb066ee23">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="b2f48c14-e2d9-47f9-b64a-a5c3f2cc1291"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_description}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="284" y="1" width="78" height="10" uuid="05781d0e-17f7-43ba-b677-63089ea16c2a">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="ff3a0635-363f-4540-9ef2-39d8e998a836"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{start_time})]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="367" y="1" width="182" height="10" uuid="f682bfbc-eb8d-4e7b-9896-955948caf7bc">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="87a258c8-d416-4da7-9b71-eeca3df6e6e7"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patient_name} != null ? $F{patient_name} : "-- No Patient --"]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement x="367" y="11" width="182" height="11" uuid="fcf70a60-b227-483c-809a-043a9d4ef86e">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="7e3f340c-eef6-4a1d-bcbb-f9d682134f51"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reason}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="true">
				<reportElement positionType="Float" x="367" y="23" width="182" height="10" uuid="8029f744-a3ea-4560-a2cb-a2adf90cc0c5">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="7e3f340c-eef6-4a1d-bcbb-f9d682134f51"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<textElement>
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{appointment_type_name}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="35" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{REPORT_COUNT} == 0 || $P{explain}]]></printWhenExpression>
			<staticText>
				<reportElement stretchType="RelativeToBandHeight" x="123" y="9" width="304" height="19" isRemoveLineWhenBlank="true" uuid="7b085e40-bf99-44b9-8b37-f11d68ebac7f">
					<printWhenExpression><![CDATA[$V{REPORT_COUNT} ==0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="13" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No Data Found - Check Parameters]]></text>
			</staticText>
		</band>
	</lastPageFooter>
	<summary>
		<band height="82">
			<staticText>
				<reportElement x="0" y="28" width="549" height="48" isRemoveLineWhenBlank="true" uuid="11749dce-2ce7-4811-aeba-44602a6caf33">
					<printWhenExpression><![CDATA[$P{explain}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[This report shows No Show appointments between the 'From Date' and 'To Date', inclusive.
The records are sorted by customer.
Select 'Show Details' to show the appointment details. Deselect it to show a summary per customer.]]></text>
			</staticText>
			<staticText>
				<reportElement x="367" y="0" width="101" height="16" uuid="cb2abbc3-b2b2-4c63-882a-2a896eec8436">
					<property name="com.jaspersoft.studio.spreadsheet.connectionID" value="ff3a0635-363f-4540-9ef2-39d8e998a836"/>
				</reportElement>
				<textElement textAlignment="Left">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total No Shows]]></text>
			</staticText>
			<textField>
				<reportElement x="494" y="0" width="49" height="16" uuid="173aa00c-d917-47fe-8a30-0defe6cda4ec"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="0" width="550" height="1" uuid="3f81f405-03bb-4277-82ad-5acc2b49b758">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
			</line>
		</band>
	</summary>
</jasperReport>
