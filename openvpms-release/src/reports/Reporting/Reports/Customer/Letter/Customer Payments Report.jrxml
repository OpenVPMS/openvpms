<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Customer Payments Report" pageWidth="612" pageHeight="792" whenNoDataType="AllSectionsNoDetail" columnWidth="550" leftMargin="31" rightMargin="31" topMargin="19" bottomMargin="19" isSummaryWithPageHeaderAndFooter="true" uuid="6e62010b-564a-4441-afe6-523543d1e513">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="OpenVPMS"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" vTextAlign="Middle" fontSize="9"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="startDate" class="java.util.Date">
		<parameterDescription><![CDATA[From Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="endDate" class="java.util.Date">
		<parameterDescription><![CDATA[To Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="locationName" class="java.lang.String">
		<parameterDescription><![CDATA[Payment Location]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="printDetail" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Show Details]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="Explain" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Display explanation]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[SELECT SUBSTRING(a.arch_short_name, 20)      AS paymentRefund,
       a.activity_start_time                 AS date,
       IF(item.arch_short_name LIKE 'act.customerAccount%Credit', 'Credit Card',
          IF(item.arch_short_name LIKE 'act.customerAccountPayment%', SUBSTRING(item.arch_short_name, 27),
             IF(item.arch_short_name LIKE 'act.customerAccountRefund%', SUBSTRING(item.arch_short_name, 26),
                '???')))                     AS payType,
       IF(credit, fitem.total, -fitem.total) AS total,
       customer.name                         AS customer_name,
       customer.entity_id                    AS cid,
       IFNULL(location.name, '--NONE--')     AS location,
       IFNULL(lpayment_type.name,"")         AS otherType
FROM acts a
         JOIN act_relationships items
              ON items.source_id = a.act_id
                  AND (items.arch_short_name = 'actRelationship.customerAccountPaymentItem'
                      OR items.arch_short_name = 'actRelationship.customerAccountRefundItem')
         JOIN acts item
              ON item.act_id = items.target_id
         JOIN financial_acts fitem
              ON item.act_id = fitem.financial_act_id
         JOIN participations pcustomer
              ON a.act_id = pcustomer.act_id
                  AND pcustomer.arch_short_name = 'participation.customer'
         JOIN entities customer
              ON pcustomer.entity_id = customer.entity_id
         JOIN participations ptill
         	  ON a.act_id = ptill.act_id
         	  	AND ptill.arch_short_name = 'participation.till'
         JOIN entity_relationships rtill
              ON rtill.target_id = ptill.entity_id
              	AND rtill.arch_short_name = 'entityRelationship.locationTill'
         JOIN entities location
                   ON location.entity_id = rtill.source_id
         LEFT JOIN act_details payment_type
          	ON payment_type.act_id = item.act_id
          		AND payment_type.name = 'paymentType'
         LEFT JOIN lookups lpayment_type
          	ON lpayment_type.code = payment_type.value
          		AND lpayment_type.arch_short_name = 'lookup.customPaymentType'
WHERE (a.arch_short_name = 'act.customerAccountPayment' OR a.arch_short_name = 'act.customerAccountRefund')
  AND a.activity_start_time >= DATE($P{startDate})
  AND a.activity_start_time < DATE_ADD(DATE($P{endDate}), INTERVAL 1 DAY)
  AND a.status = 'POSTED'
  AND IFNULL(location.name, '-') LIKE CONCAT(IFNULL($P{locationName}, ''), '%')
ORDER BY location.name, a.arch_short_name, item.arch_short_name, lpayment_type.name, customer.name, customer.entity_id,
         a.activity_start_time;]]>
	</queryString>
	<field name="paymentRefund" class="java.lang.String"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="payType" class="java.lang.String"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="customer_name" class="java.lang.String"/>
	<field name="cid" class="java.lang.Long"/>
	<field name="location" class="java.lang.String"/>
	<field name="otherType" class="java.lang.String"/>
	<variable name="TOTAL_PAYMENT" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA["Payment".equals($F{paymentRefund})? $F{total} : new BigDecimal("0.00")]]></variableExpression>
	</variable>
	<variable name="TOTAL_REFUND" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA["Refund".equals($F{paymentRefund})? $F{total} : new BigDecimal("0.00")]]></variableExpression>
	</variable>
	<variable name="PAYREF_TOTAL" class="java.math.BigDecimal" resetType="Group" resetGroup="PayRef" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<variable name="LOCATION_TOTAL" class="java.math.BigDecimal" resetType="Group" resetGroup="Location" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<variable name="PAYTYPE_TOTAL" class="java.math.BigDecimal" resetType="Group" resetGroup="payType" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<group name="Dummy">
		<groupFooter>
			<band height="54">
				<staticText>
					<reportElement key="staticText-5" x="235" y="19" width="156" height="12" uuid="4219598d-f077-4cf9-8085-9dfd00f95088">
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Total Refunds :]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-17" x="235" y="34" width="156" height="12" uuid="2a7353fc-3c94-4e30-8ba5-84a7bf6976f5">
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Total Gross Payments :]]></text>
				</staticText>
				<textField textAdjust="ScaleFont" pattern="¤ #,##0.00" isBlankWhenNull="true">
					<reportElement key="textField" x="395" y="6" width="150" height="12" uuid="dd1474f8-907f-4c5d-b7e0-f204befe3a14"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TOTAL_PAYMENT}]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont" pattern="¤ #,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-3" x="395" y="19" width="150" height="12" uuid="4bcada6d-4291-444d-9791-1fae25bb109f"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TOTAL_REFUND}]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont" pattern="¤ #,##0.00" isBlankWhenNull="true">
					<reportElement key="textField-5" x="395" y="34" width="150" height="12" uuid="51d55d79-3cd8-4787-a05d-dddfb02d99c8">
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="true" isUnderline="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{TOTAL_PAYMENT}.add($V{TOTAL_REFUND})]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-4" x="233" y="50" width="314" height="1" uuid="83dc838f-6e5a-48f2-8477-48fe664701b7">
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
				</line>
				<line>
					<reportElement key="line-5" x="233" y="32" width="314" height="1" uuid="113cc545-5bdc-4705-a264-8d492be06f42">
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
				</line>
				<staticText>
					<reportElement key="staticText-2" x="235" y="6" width="156" height="12" uuid="6f3be980-cb8b-452b-a011-1e15628179b1">
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Total Payments :]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<group name="Location" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{location}]]></groupExpression>
		<groupHeader>
			<band height="16">
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement x="0" y="0" width="406" height="15" uuid="ec8df509-3acf-4475-b6e6-490995b269da">
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<font size="11" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{location}+(($V{LOCATION_TOTAL}==null)?"":" (cont)")]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="16">
				<textField>
					<reportElement x="5" y="2" width="406" height="13" uuid="cfc36387-9bce-4170-9c82-93a1bab488a5"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{location}+" Total"]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField" x="425" y="2" width="120" height="13" uuid="67e5e592-2135-4da4-97dd-189d2a4d1656"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{LOCATION_TOTAL}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="PayRef">
		<groupExpression><![CDATA[$F{paymentRefund}]]></groupExpression>
		<groupFooter>
			<band height="16">
				<textField>
					<reportElement x="154" y="2" width="257" height="13" uuid="a5ed29f7-41bf-4036-bf3b-500d7b1343f1">
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					</reportElement>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{paymentRefund}+" Total"]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField" x="425" y="2" width="120" height="13" uuid="8f71beaa-b70a-4099-90aa-ed20a93bb028"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{PAYREF_TOTAL}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="payType">
		<groupExpression><![CDATA[$F{payType}+$F{otherType}]]></groupExpression>
		<groupFooter>
			<band height="16">
				<textField isBlankWhenNull="true">
					<reportElement x="123" y="2" width="288" height="13" uuid="acc90107-c8e0-4d81-85ff-3bb85c4b3784">
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					</reportElement>
					<textElement textAlignment="Right">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{payType}+" "+ $F{otherType} + " " +$F{paymentRefund}+" Total"]]></textFieldExpression>
				</textField>
				<textField textAdjust="ScaleFont" pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement key="textField" x="425" y="2" width="120" height="13" uuid="10d8c086-90b5-4d17-9ebe-b877139d5ccd"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{PAYTYPE_TOTAL}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="71" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="123" y="0" width="303" height="23" uuid="2759eac9-e206-4acf-9c4b-dbd300c2a8b5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="16" isBold="true" isUnderline="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer Payments Report]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="114" y="33" width="103" height="16" uuid="4778eaaa-58ba-437d-8afa-b144e1bcec62"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{startDate})]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="280" y="34" width="103" height="16" uuid="1734f422-bb61-4de9-8f01-511daa2d4ddd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{endDate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" x="46" y="33" width="65" height="16" uuid="eaa1735b-646b-40bc-9de7-e1532cf1721e">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[From Date:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="219" y="34" width="58" height="16" uuid="99e91890-59de-491a-bca3-9656e8cfd67a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Date:]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="395" y="27" width="76" height="17" uuid="4804360d-ec77-4bda-b12d-d9cbdae60699"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="false">
				<reportElement key="textField-2" x="475" y="27" width="75" height="17" uuid="5c1a28b0-12d6-453a-8357-013be14e9c45"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement x="447" y="0" width="103" height="10" uuid="963cd7fc-a077-48c4-9cde-e5fe3a245fb3"/>
				<textElement textAlignment="Right">
					<font size="7"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="114" y="50" width="103" height="16" uuid="928a51a8-2b02-4309-a602-0f8c605f3e05"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[($P{locationName}==null)?"%":$P{locationName}+"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" x="0" y="49" width="111" height="16" uuid="4b6afd5e-4c2f-4650-bc4b-f5b8bf2dd9bc">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right"/>
				<text><![CDATA[Payment Location:]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="36" splitType="Stretch">
			<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
			<staticText>
				<reportElement key="staticText-12" x="1" y="3" width="71" height="25" uuid="c2c25c4d-8c43-4b53-a2dd-c4be34e5df09">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Payment/
Refund]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="488" y="3" width="57" height="16" uuid="a7fe0868-7bae-437c-a8d0-ecd4e7ef4c5f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="33" width="550" height="1" uuid="9ddb4535-1a63-4020-b87b-8edea6478f97">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
			</line>
			<staticText>
				<reportElement key="staticText-15" x="178" y="3" width="217" height="16" uuid="8453948b-f515-4358-9669-46b457107f15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer (ID)]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="78" y="4" width="93" height="16" uuid="99274466-5c07-402c-a2f4-9bad61fde885"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="406" y="3" width="65" height="16" uuid="b1d57bae-2d35-4c15-a3b2-d0ba7454bdb3">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="13" splitType="Stretch">
			<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" x="1" y="0" width="71" height="12" isPrintInFirstWholeBand="true" uuid="49feb355-0897-4ade-9ea5-18a7083bfc7d">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{paymentRefund}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="488" y="0" width="57" height="12" uuid="4b5994fb-1d7e-47dd-ab97-12b7101dbc19"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="textField" x="178" y="0" width="217" height="12" uuid="b58a2887-a3b6-4469-9a05-e02f1a095616"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_name}+" ("+$F{cid}.toString()+")"]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="textField" isPrintRepeatedValues="false" x="80" y="0" width="91" height="12" isPrintInFirstWholeBand="true" uuid="a299ed99-d2fb-42be-b4c3-359472c24e8c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{payType} + " " + $F{otherType}]]></textFieldExpression>
			</textField>
			<textField pattern="MM/dd/yyyy" isBlankWhenNull="false">
				<reportElement key="textField" x="406" y="0" width="65" height="12" uuid="10d300c4-e5b5-49dc-9de7-eb85ac6e6cbe"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{date})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="142" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="19" width="550" height="122" isRemoveLineWhenBlank="true" uuid="a63f08a9-c3aa-44b6-9828-dc57bb947016">
					<printWhenExpression><![CDATA[$P{Explain}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[This report shows, for the selected payment location(s) and date range, the payments and refunds for each payment/refund type.
The period is inclusive - ie from 00:00 on the start date through 23:59:59 on the end date.

The Payment Location field has % added. Leaving it blank will find all practice locations; entering %branch will find all locations containing 'branch' in their name; and entering E will find payments and refunds for all practice locations starting with E.

Only finalised payments and refunds are shown. All amounts are tax inclusive.
Unless Show Details is ticked, then only the totals are shown. 
The data is ordered by practice location, payment/refund, type, customer name, date.]]></text>
			</staticText>
			<staticText>
				<reportElement x="134" y="0" width="303" height="19" isRemoveLineWhenBlank="true" uuid="47cd04b7-e68d-4586-b2d5-6a2d67d54f56">
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()==0)]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="13" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No Data Found - Check Parameters]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
