<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Customer Product Sales Report" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="539" leftMargin="28" rightMargin="28" topMargin="20" bottomMargin="20" isFloatColumnFooter="true" uuid="77ee6d65-55d2-45ba-ad27-5d686d80eda6">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="Vetwest"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="232"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="762"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" vTextAlign="Middle" fontSize="10"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="startDate" class="java.util.Date">
		<parameterDescription><![CDATA[From Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="endDate" class="java.util.Date">
		<parameterDescription><![CDATA[To Date]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="locationName" class="java.lang.String">
		<parameterDescription><![CDATA[Location name]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="customerName" class="java.lang.String">
		<parameterDescription><![CDATA[Customer name]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="productName" class="java.lang.String">
		<parameterDescription><![CDATA[Product name]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="productType" class="java.lang.String">
		<parameterDescription><![CDATA[Product Type]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="classification" class="java.lang.String">
		<parameterDescription><![CDATA[Classification]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="printDetail" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Print detail ?]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="Explain" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Display explanation ?]]></parameterDescription>
		<defaultValueExpression><![CDATA[false]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[SELECT charge.act_id                                 AS charge_id,
       item.act_id                                   AS charge_item_id,
       product.name                                  AS product,
       customer.name                                 AS customer,
       customer.entity_id                            AS customer_id,
       patient.name                                  AS patient,
       patient.entity_id                             AS patient_id,
       item.activity_start_time                      AS date,
       IF(credit, -f_item.quantity, f_item.quantity) AS quantity,
       IF(credit, -f_item.total, f_item.total)       AS total
FROM acts charge
         JOIN act_relationships r_charge_item
              ON charge.act_id = r_charge_item.source_id
         JOIN acts item
              ON r_charge_item.target_id = item.act_id
                  AND item.arch_short_name LIKE 'act.customerAccount%Item'
         JOIN participations p_product
              ON item.act_id = p_product.act_id
                  AND p_product.arch_short_name = 'participation.product'
         JOIN entities product
              ON product.entity_id = p_product.entity_id
         LEFT JOIN participations p_patient
                   ON item.act_id = p_patient.act_id
                       AND p_patient.arch_short_name = 'participation.patient'
         LEFT JOIN entities patient
                   ON patient.entity_id = p_patient.entity_id
         JOIN participations p_customer
              ON charge.act_id = p_customer.act_id
                  AND p_customer.arch_short_name = 'participation.customer'
         JOIN entities customer
              ON customer.entity_id = p_customer.entity_id
         JOIN financial_acts f_item
              ON f_item.financial_act_id = item.act_id
         LEFT JOIN participations p_location
                   ON charge.act_id = p_location.act_id
                       AND p_location.arch_short_name = 'participation.location'
         LEFT JOIN entities location
                   ON p_location.entity_id = location.entity_id
         LEFT JOIN entity_links l_product_type
                   ON l_product_type.source_id = product.entity_id
                       AND l_product_type.arch_short_name = 'entityLink.productType'
         LEFT JOIN entities product_type
                   ON product_type.entity_id = l_product_type.target_id
WHERE charge.arch_short_name IN ('act.customerAccountChargesInvoice', 'act.customerAccountChargesCredit',
                                 'act.customerAccountChargesCounter')
  AND charge.activity_start_time >= $P{startDate}
  AND charge.activity_start_time < DATE_ADD($P{endDate}, INTERVAL 1 DAY)
  AND charge.status = 'POSTED'
  AND product.name LIKE CONCAT(IFNULL($P{productName}, ''), '%')
  AND customer.name LIKE CONCAT(IFNULL($P{customerName}, ''), '%')
  AND IFNULL(location.name, '-') LIKE CONCAT(IFNULL($P{locationName}, ''), '%')
  AND IFNULL(product_type.name, '-') LIKE CONCAT(IFNULL($P{productType}, ''), '%')
  AND ($P{classification} IS NULL OR
       EXISTS(SELECT 1
              FROM entity_classifications product_classification
                       JOIN lookups l_group
                            ON l_group.lookup_id = product_classification.lookup_id
                                AND l_group.arch_short_name IN ('lookup.productGroup', 'lookup.productIncomeGroup')
              WHERE product_classification.entity_id = product.entity_id
                AND l_group.name LIKE CONCAT($P{classification}, '%')))
ORDER BY product.name, product.entity_id, item.activity_start_time, item.act_id]]>
	</queryString>
	<field name="product" class="java.lang.String"/>
	<field name="customer" class="java.lang.String"/>
	<field name="customer_id" class="java.lang.Long"/>
	<field name="patient" class="java.lang.String"/>
	<field name="patient_id" class="java.lang.Long"/>
	<field name="date" class="java.sql.Timestamp"/>
	<field name="quantity" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<variable name="PRODUCT_TOTAL" class="java.math.BigDecimal" resetType="Group" resetGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
		<initialValueExpression><![CDATA[BigDecimal.ZERO]]></initialValueExpression>
	</variable>
	<variable name="PRODUCT_COUNT" class="java.math.BigDecimal" resetType="Group" resetGroup="product" calculation="Sum">
		<variableExpression><![CDATA[$F{quantity}]]></variableExpression>
		<initialValueExpression><![CDATA[BigDecimal.ZERO]]></initialValueExpression>
	</variable>
	<variable name="TOTAL_SALES" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
		<initialValueExpression><![CDATA[BigDecimal.ZERO]]></initialValueExpression>
	</variable>
	<variable name="TOTAL_COUNT" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{quantity}]]></variableExpression>
		<initialValueExpression><![CDATA[BigDecimal.ZERO]]></initialValueExpression>
	</variable>
	<group name="product" isReprintHeaderOnEachPage="true" keepTogether="true">
		<groupExpression><![CDATA[$F{product}]]></groupExpression>
		<groupHeader>
			<band height="16" splitType="Stretch">
				<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="5" y="2" width="393" height="14" uuid="b08ded96-6686-47b9-ae34-ac92553cdc1e"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="10" isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{product}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="18" splitType="Stretch">
				<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
					<reportElement key="textField" x="457" y="3" width="80" height="14" uuid="6593b6fc-86c8-43bd-ab39-dedca211ea05">
						<property name="com.jaspersoft.studio.unit.x" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{PRODUCT_TOTAL}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-28" x="284" y="2" width="96" height="14" uuid="0486c2e0-7574-4635-b700-82d81a5cdefd">
						<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font isBold="true"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Product Total]]></text>
				</staticText>
				<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
					<reportElement key="textField" x="380" y="3" width="66" height="14" uuid="bcae292b-ba12-47a4-ba06-ca82a1d9852d">
						<property name="com.jaspersoft.studio.unit.width" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{PRODUCT_COUNT}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement key="line-3" x="0" y="2" width="539" height="1" forecolor="#C0C0C0" uuid="cb51fec5-be84-45e1-b8cb-93a46e081c37">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="px"/>
						<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
					</reportElement>
				</line>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="5" y="3" width="273" height="14" uuid="de4e6b3a-41e2-4568-88a3-3816cdcf282a">
						<printWhenExpression><![CDATA[!$P{printDetail}]]></printWhenExpression>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="10" isBold="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{product}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="97" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="94" y="0" width="346" height="24" uuid="f5c604cb-9d92-4b7c-a4a8-1d857576a078"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" isUnderline="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer Product Sales Report]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="64" y="36" width="100" height="17" uuid="cf6c50e0-34c0-4c3f-826c-5156dca3d0d2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{startDate})]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="227" y="36" width="100" height="17" uuid="06f38383-2cc2-4d94-b9df-b2dad26a5a19"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{endDate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" x="5" y="36" width="56" height="17" uuid="4dc73faa-9ed4-4568-b044-60b8eac18271"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Start Date:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="172" y="36" width="56" height="17" uuid="edfa4cc5-0606-47b6-9002-a801a8714b82"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[End Date:]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="428" y="36" width="58" height="17" uuid="6538e8a4-f3bd-4763-b4e3-af46b73bdb72"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" isBlankWhenNull="false">
				<reportElement key="textField-2" x="490" y="36" width="42" height="17" uuid="8e8bb7d2-02c0-4c1c-8530-e28b1d1742ac"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="64" y="56" width="100" height="17" uuid="dfcae80d-76c1-43c2-8204-2b8b413ed0ca"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{locationName}==null)?"%":$P{locationName}+"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" x="5" y="56" width="56" height="17" uuid="a6700bb7-bc50-43cf-96a7-013151774f05"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Location:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="340" y="77" width="77" height="17" uuid="c7236628-7693-44e1-825c-d8056664f68d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Classification:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="417" y="77" width="100" height="17" uuid="5a8a3d4a-3f80-49ed-ac9c-739d741e1577"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{classification}==null)?"%":$P{classification}+"%"]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="227" y="77" width="100" height="17" uuid="59aa3db4-1b99-45f7-82ac-9a8050e0eaba"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{productType}==null)?"%":$P{productType}+"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" x="172" y="56" width="56" height="17" uuid="a8383820-62e8-4379-93f7-1bffa0c1d524"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="227" y="56" width="100" height="17" uuid="69070f01-7753-49fa-9868-80b969b5c7f0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{customerName}==null)?"%":$P{customerName}+"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" x="172" y="77" width="56" height="17" uuid="ebc7cbd2-ebad-4539-acc2-519d5ffea8fd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Prod.Type:]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="64" y="77" width="100" height="17" uuid="a28edcda-2c37-4b04-aafe-c7555cd7bc61"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[($P{productName}==null)?"%":$P{productName}+"%"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-10" x="5" y="77" width="56" height="17" uuid="e6f5f2c9-4246-47a2-95e1-279577e198d4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Product:]]></text>
			</staticText>
			<textField pattern="">
				<reportElement x="435" y="0" width="100" height="11" uuid="8e281a1a-5cd0-4e5e-b18d-0d3b9aa3ebe9"/>
				<textElement textAlignment="Right">
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="25" splitType="Stretch">
			<textField>
				<reportElement key="staticText-11" x="3" y="2" width="77" height="17" uuid="bb7981cd-0287-4681-8bb2-4dda8f4120a4"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{printDetail} ? "Product/Date" : "Product"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-13" x="396" y="2" width="50" height="17" uuid="ec2259a4-f85f-4516-9b41-544f546ca82c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Qty]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="457" y="2" width="80" height="17" uuid="4d4bf058-6f19-4427-8611-976a1b0d3e31">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="23" width="539" height="1" uuid="bc891e11-5d2b-4c7c-82d1-44c0fb125224">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
			</line>
			<staticText>
				<reportElement key="staticText-15" x="84" y="2" width="154" height="17" uuid="e192f3db-1fc4-4235-903b-b387d839fdfd">
					<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-27" x="243" y="2" width="47" height="17" uuid="8bb0ca10-6468-4045-bebe-c95fd869ef36">
					<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Patient]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<printWhenExpression><![CDATA[$P{printDetail}]]></printWhenExpression>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="10" y="2" width="63" height="13" uuid="a0227f04-077c-40af-b346-125d48321adc">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{date})]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField" x="457" y="2" width="80" height="13" uuid="60160bdd-6c06-4cf7-88fa-ec4ff1fb5cb2">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="false">
				<reportElement key="textField" x="124" y="2" width="114" height="13" uuid="76b13bc2-f12e-4d45-80d0-ae8e13061992"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" isBlankWhenNull="false">
				<reportElement key="textField" x="284" y="2" width="114" height="13" uuid="68b20f39-66ee-4d28-b60b-57b1896ae61c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patient}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField" x="396" y="2" width="50" height="13" uuid="e37b9a71-e3b8-478b-9f2b-02867e7de3b0">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{quantity}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont">
				<reportElement x="84" y="2" width="35" height="13" uuid="fed1efbe-1a01-4b87-94f9-fa99dacf7f4c"/>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer_id}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont">
				<reportElement x="243" y="2" width="35" height="13" uuid="23247137-90c1-48b8-9910-f099539f34ab"/>
				<textElement textAlignment="Right">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{patient_id}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="234" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-30" x="284" y="3" width="96" height="20" uuid="078d3c89-3688-414a-aad6-d9cabc31ea42"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total Sales]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField" x="380" y="3" width="67" height="20" uuid="c1d9fe41-3509-4aa9-9c3a-d17a4100fb0d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TOTAL_COUNT}]]></textFieldExpression>
			</textField>
			<textField textAdjust="ScaleFont" pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField" x="458" y="3" width="80" height="20" uuid="fb949bf7-74e2-418a-b25c-dc4dabb80899"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TOTAL_SALES}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-2" x="0" y="3" width="539" height="1" uuid="adaf6f5f-2604-4385-9199-da68615a93e6">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
			</line>
			<staticText>
				<reportElement positionType="Float" x="111" y="33" width="327" height="20" uuid="d88eaba6-8e53-41dd-8bc3-4515d2738e97">
					<printWhenExpression><![CDATA[$V{REPORT_COUNT} == 0]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No Data Found - Check Parameters]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="5" y="64" width="539" height="169" uuid="e883a12b-0ca3-4c43-9147-f439f4a29d4c">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<printWhenExpression><![CDATA[$P{Explain}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[This report shows which products were sold during the specified period (and if details are requested, then to which customers).
The period is inclusive - ie sales from 00:00 on the start date through 23:59:59 on the end date.
The totals are the total of all invoices less any credits but does not include adjustments.
All the selection fields entered have % added so that leaving them blank will find all, and entering say product %vacc will find all products contain 'vacc' in their product name, and customer fred will find Fred, Fredricks, Freddy, etc. Note that these are case insensitive. If you just want counter sales, set the Customer to %counter sale.
For Product Type and Classification (Product Group and Product Income Type) selection, omitting the selection will include products with no product type or classification respectively, and using the selection '-' will find only those with no product type or classification.
All amounts are tax inclusive.
Note that the total of the Qty column does not take into account the units of different products.]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
