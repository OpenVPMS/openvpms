<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Refund" pageWidth="612" pageHeight="792" columnWidth="554" leftMargin="29" rightMargin="29" topMargin="26" bottomMargin="26" isSummaryWithPageHeaderAndFooter="true" resourceBundle="localisation.reports" uuid="ef04735e-ea59-4c0f-ba5a-8ff80209e725">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<property name="net.sf.jasperreports.print.create.bookmarks" value="true"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="org.openvpms.archetype.function.party.PartyFunctions"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="9"/>
	<parameter name="dataSource" class="org.openvpms.report.jasper.IMObjectCollectionDataSource" isForPrompting="false"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="taxMode" class="java.lang.String" isForPrompting="false">
		<parameterDescription><![CDATA[set to 'ex-Tax' or 'inc-tax']]></parameterDescription>
		<defaultValueExpression><![CDATA["inc-Tax"]]></defaultValueExpression>
	</parameter>
	<field name="createdBy.name" class="java.lang.String"/>
	<field name="customer.entity.name" class="java.lang.String"/>
	<field name="customer.entity.companyName" class="java.lang.String"/>
	<field name="customer.entity.lastName" class="java.lang.String"/>
	<field name="customer.entity.firstName" class="java.lang.String"/>
	<field name="customer.entity.title" class="java.lang.String"/>
	<field name="customer.entity.initials" class="java.lang.String"/>
	<field name="customer.entity.id" class="java.lang.Long"/>
	<field name="startTime" class="java.util.Date"/>
	<field name="id" class="java.lang.Long"/>
	<field name="amount" class="java.math.BigDecimal"/>
	<field name="notes" class="java.lang.String"/>
	<field name="[party:getBillingAddress(.)]" class="java.lang.String"/>
	<field name="[party:getAccountBalance(.)]" class="java.math.BigDecimal"/>
	<field name="[party:getPartyFullName(.)]" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.lastPageFooter" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.generalMsg" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="reference" class="java.lang.String"/>
	<field name="status.code" class="java.lang.String"/>
	<field name="hide" class="java.lang.Boolean"/>
	<variable name="CustomerFullName" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[(($F{customer.entity.title} == null) ? "": $F{customer.entity.title}) + " " + (($F{customer.entity.firstName} == null) ? "": $F{customer.entity.firstName}) + " " + $F{customer.entity.lastName}]]></variableExpression>
	</variable>
	<variable name="isFinalised" class="java.lang.Boolean" calculation="First">
		<variableExpression><![CDATA["POSTED".equals($F{status.code})]]></variableExpression>
		<initialValueExpression><![CDATA[Boolean.FALSE]]></initialValueExpression>
	</variable>
	<background>
		<band height="565" splitType="Stretch">
			<staticText>
				<reportElement x="184" y="28" width="185" height="536" forecolor="#FF0000" uuid="24b065e3-a5d1-4c3c-b1ab-556ba4b3f05b">
					<printWhenExpression><![CDATA[$F{hide}]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Right">
					<font size="94" isBold="true"/>
				</textElement>
				<text><![CDATA[Cancelled]]></text>
			</staticText>
		</band>
	</background>
	<title>
		<band height="146" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" positionType="Float" x="473" y="74" width="81" height="13" uuid="24efd486-b026-479b-9e24-323fa7a36685">
					<property name="local_mesure_unitheight" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{startTime})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="399" y="87" width="72" height="14" uuid="ab4aece5-15a1-4304-bbfc-210774cd007f">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Reference]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="399" y="74" width="72" height="13" uuid="c8f716a8-c986-41d0-a42b-8d4351af26de">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-4" positionType="Float" x="473" y="87" width="81" height="14" uuid="8f10d72d-598b-4c22-bf1a-44b61b5f1c3e">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reference}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="399" y="61" width="72" height="13" uuid="025c6082-8b4b-4236-ae4b-04723d1937ca">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer ID]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" x="473" y="61" width="81" height="13" uuid="acdaada2-092e-495d-a85d-da222aaa9c58">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer.entity.id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Master">
				<reportElement positionType="Float" x="473" y="114" width="81" height="13" uuid="6b960f26-8f0e-48ee-9cb0-25edf75e65a4"/>
				<textElement textAlignment="Left"/>
				<textFieldExpression><![CDATA["Page 1 of "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight">
				<reportElement key="staticText-1" positionType="Float" x="388" y="24" width="166" height="37" uuid="b6623dcd-748a-48e4-b4ae-79ecb694aa6f">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="16" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.refundTitle}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="399" y="101" width="72" height="13" uuid="feb88a5c-aa40-4147-a16a-3709959eb6f7">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Document No]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" positionType="Float" x="473" y="101" width="81" height="13" uuid="639831bc-053b-4606-afb1-3f7016fd1e4e">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement key="" positionType="Float" mode="Opaque" x="388" y="114" width="83" height="32" forecolor="#FF0000" backcolor="#FFFFFF" uuid="849f84e6-da03-480d-9d62-ee42b38199fe">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineColor="#FFFFFF"/>
					<leftPen lineColor="#FFFFFF"/>
					<bottomPen lineColor="#FFFFFF"/>
					<rightPen lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" markup="html">
					<font size="18" isBold="true" isItalic="false" isUnderline="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{isFinalised} ? "":"DRAFT"]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement positionType="Float" x="0" y="24" width="381" height="103" uuid="98ae224e-3481-4c80-968b-539516989dba">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="Addr1">
					<subreportParameterExpression><![CDATA[$F{customer.entity.companyName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr2">
					<subreportParameterExpression><![CDATA[$V{CustomerFullName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr3">
					<subreportParameterExpression><![CDATA[$F{[party:getBillingAddress(.)]}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+" AddressBlock"+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="48824a00-b3e2-4354-8033-bb7bdccbc4aa">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</title>
	<pageHeader>
		<band height="66" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER}>1]]></printWhenExpression>
			<textField>
				<reportElement key="staticText-1" positionType="Float" x="153" y="24" width="247" height="27" uuid="7dec5aba-4493-442c-884c-cdb8887c1ed9"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="16" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.refundTitle}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="441" y="24" width="67" height="12" uuid="37161f84-c5c4-4c7d-b409-9a8f7856ae3d">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement positionType="Float" x="508" y="24" width="46" height="12" uuid="33a6f1cf-7c76-4449-a1f4-d6b9ba08ee82"/>
				<textElement textAlignment="Left">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[" of " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="83136018-ce9b-4b8d-a77f-b8499a1731db">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="34" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" x="0" y="0" width="554" height="34" uuid="66f7350a-a121-4c8f-87b7-dfd3a4a947f6">
					<property name="local_mesure_unitwidth" value="pixel"/>
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("items")]]></dataSourceExpression>
				<subreportExpression><![CDATA["Refund Items.jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="25">
			<textField textAdjust="StretchHeight" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="staticText-1" stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" uuid="a3b5d5e9-dc50-4d34-a9f3-e4869ec2a87b">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<printWhenExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.lastPageFooter}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.lastPageFooter}.trim().length()>0)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="9" isBold="true" isItalic="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{OpenVPMS.location.letterhead.target.lastPageFooter}]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band height="101" splitType="Immediate">
			<line>
				<reportElement key="line-2" x="0" y="1" width="554" height="1" uuid="01791b26-66b9-46e7-9de9-fa2a854ee6f6">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
			</line>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="staticText-1" positionType="Float" x="0" y="40" width="554" height="24" uuid="629abaaa-5df1-4daa-af75-0c21093aa5fe">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<printWhenExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.generalMsg}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.generalMsg}.trim().length()>0)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="9" isBold="true" isItalic="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{OpenVPMS.location.letterhead.target.generalMsg}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" positionType="Float" x="284" y="14" width="168" height="15" isRemoveLineWhenBlank="true" uuid="b40f6ebc-2b4a-458e-9753-f505aa78cc83">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<printWhenExpression><![CDATA[$V{isFinalised}&&($F{[party:getAccountBalance(.)]}!=BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font size="9" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Your account balance is]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="¤ #,##0.00" isBlankWhenNull="false">
				<reportElement key="textField-8" positionType="Float" x="465" y="14" width="82" height="15" isRemoveLineWhenBlank="true" uuid="3f57c1fd-e06f-4e10-ab20-845ce975caac">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<printWhenExpression><![CDATA[$V{isFinalised}&&($F{[party:getAccountBalance(.)]}!=BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font size="9" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{[party:getAccountBalance(.)]}]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement x="-29" y="76" width="612" height="24" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="c16c5e2e-24bb-40bc-bd0d-64114df4a95c"/>
				<subreportParameter name="isSubReport">
					<subreportParameterExpression><![CDATA[TRUE( )]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getExpressionDataSource("eftpos:printableReceipts()")]]></dataSourceExpression>
				<subreportExpression><![CDATA["EFTPOS Receipt.jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
