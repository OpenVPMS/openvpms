The samples here provide various patient, customer and supplier letters and
forms.

Most are Open Office (.odt) documents, but Sample Patient Form.jrxml and
Sample Rabies Certificate.jrxml are Jasper Reports templates.

The Sample Patient Document.odt contains more complex examples.  It can be used
as either a Patient Form or a Patient Letter - or in fact any other
Report Type.

Sample Patient Document.doc is a Microsoft Word document that mimics most of
Sample Patient Document.odt.
It does illustrate that although you can use Word to prepare patient etc
letters and forms, Open Office is more powerful. (This is because OpenVPMS
used Open Office to process .doc content, but does not implement all of the
mailmerge features available in Word.)

Sample Rabies Certificate.jrxml is designed to be used as a Patient Form
attached to a product - it picks up the Batch and Reminder information.

Both Sample Patient Form.jrxml and Sample Rabies Certificate.jrxml use the
Letterhead facility introduced in 1.9 and thus enable the one template to
be used by different Practice Locations each of which have their own logo
and contact details.

The A4 folder contains the A4 versions of the documents.
The A5 and Letter folders contain the A5 and US Letter versions of the jrxml
documents.

Vaccination Certificates
------------------------

A number of sample Vaccination certificates are included.

These are generic vaccination certificates designed to be used as a
Patient Form or Patient Letter, attached to a product and include batch and
manufacturer information.

If there is a legal requirement to store the document, use a Patient Letter
as Patient Forms are generated on demand.

. Sample Vaccination Certificate Next date in full.odt
  A vaccination certificate which includes the date of the next vaccination
  calculated 4 weeks from that of the certificate in the form '23 March 2021'.

. Sample Vaccination Certificate Next date month year.odt
  A vaccination certificate which includes the date of the next vaccination
  calculated 1 year from that of the certificate in the form 'February 2022'.

. Sample Vaccination Certificate.odt
  This is designed to be used with vaccination products where invoicing the
  product generates both the certificate and a followup reminder.
  E.g, a Puppy Vaccination 6-8 Weeks product could include this with a
  'Puppy - 12 week Booster' reminder.

  Note: if used with a Patient Form, the list of vaccination reminders shown
        will change over time. To avoid this, use a Patient Letter which saves
        the certificate as a PDF.

  A Report Macro is used to list the upcoming vaccination reminders. The
  reminders must be associated with products that have a Product Type named
  Vaccinations.

  This needs to be configured as follows:

  1. In Administration - Templates, create a Document Template:
     - Name:       Sample Vaccination Certificate
     - Type:       Patient Form
     - Upload:     A4/Sample Vaccination Certificate.odt

  2. In Administration - Templates, create a Document Template:
     - Name:       Report Macro - Vaccination Reminders
     - Upload:     A4/Report Macro - Vaccination Reminders On Date.jrxml

  3. In Administration - Templates, create a Document Template:
     - Name:       Report Macro - Vaccination Reminder Items
     - Upload:     A4/Report Macro - Vaccination Reminder Items.jrxml

  4. Go to Administration - Lookups and create a Report Macro:
     - Code:        @VaccinationRemindersOnDate
     - Name:        Vaccination Reminders On Date
     - Description: Lists reminders for products with a Vaccinations product
                    type created on the same date
     - Expression:  .

  The Report Macro can be tested by:

  1. creating a reminder linked to a product with a Vaccinations product type.
     For this example, the Reminder Type is 'Puppy - 12 Week Booster'
  2. creating a patient clinical Note and entering the Report Macro name:
     @VaccinationRemindersOnDate
  3. Press enter to get the macro to expand. It should be replaced with the
     reminder due date and reminder type e.g.
     23/02/22       Puppy - 12 Week Booster

  The template can be tested by creating a Patient Form with
     Form:          Sample Vaccination Certificate
     Product:       use the same product used to test the reminder
