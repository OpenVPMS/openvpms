/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util.filter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper to test {@link JAXBErrorMessageReader} and {@link JSONErrorMessageReader} classes.
 *
 * @author Tim Anderson
 */
@XmlRootElement(name = "error")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorMessage {

    /**
     * The message.
     */
    @XmlElement(required = true)
    @JsonProperty("message")
    private String message;

    /**
     * Sets the message.
     *
     * @param message the message. May be {@code null}
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    @Override
    public String toString() {
        return message;
    }
}
