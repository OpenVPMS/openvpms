/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openvpms.ws.util.filter.ErrorMessageReader;
import org.openvpms.ws.util.filter.JAXBErrorMessageReader;
import org.openvpms.ws.util.filter.JSONErrorMessageReader;
import org.openvpms.ws.util.filter.PlainTextErrorMessageReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED;

/**
 * A filter used to handle 40x and 50x series errors. These extract any error messages returned in the body of
 * responses, and includes them in the thrown exception.
 * <p/>
 * A {@link ErrorMessageReader} may be provided to deserialise error messages.
 *
 * @author Tim Anderson
 * @see JAXBErrorMessageReader
 * @see JSONErrorMessageReader
 */
public class ErrorResponseFilter implements ClientResponseFilter {

    /**
     * The error message reader. May be {@code null}.
     */
    private final ErrorMessageReader reader;

    /**
     * Default reader for text/plain content.
     */
    private static final ErrorMessageReader plainTextReader = new PlainTextErrorMessageReader();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ErrorResponseFilter.class);

    /**
     * Constructs an {@link ErrorResponseFilter}.
     */
    public ErrorResponseFilter() {
        this(null);
    }

    /**
     * Constructs an {@link ErrorResponseFilter}.
     *
     * @param mapper    the mapper, used to deserialise JSON responses
     * @param errorType the class responsible for deserialising errors, for JSON responses
     */
    public <T> ErrorResponseFilter(ObjectMapper mapper, Class<T> errorType) {
        this(new JSONErrorMessageReader<>(mapper, errorType));
    }

    /**
     * Constructs an {@link ErrorResponseFilter}.
     *
     * @param reader the reader, used to deserialise error messages from error responses
     */
    public ErrorResponseFilter(ErrorMessageReader reader) {
        this.reader = reader;
    }

    /**
     * Filter method called after a response has been provided for a request (either by a
     * {@link ClientRequestFilter request filter} or when the HTTP invocation returns.
     * <p>
     * Filters in the filter chain are ordered according to their {@code javax.annotation.Priority}
     * class-level annotation value.
     *
     * @param requestContext  request context.
     * @param responseContext response context.
     */
    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) {
        Response.StatusType statusInfo = responseContext.getStatusInfo();
        if (statusInfo.getFamily() == Response.Status.Family.CLIENT_ERROR
            || statusInfo.getFamily() == Response.Status.Family.SERVER_ERROR) {
            MediaType mediaType = getMediaType(responseContext);
            if (responseContext.hasEntity() && mediaType != null) {
                String message = getErrorMessage(responseContext);
                WebApplicationException exception;
                int status = responseContext.getStatus();
                switch (status) {
                    case HttpServletResponse.SC_BAD_REQUEST:
                        exception = (message == null) ? new BadRequestException() : new BadRequestException(message);
                        break;
                    case HttpServletResponse.SC_UNAUTHORIZED:
                        exception = createNotAuthorisedException(message, status);
                        break;
                    case HttpServletResponse.SC_FORBIDDEN:
                        exception = (message == null) ? new ForbiddenException() : new ForbiddenException(message);
                        break;
                    case HttpServletResponse.SC_NOT_FOUND:
                        exception = (message == null) ? new NotFoundException() : new NotFoundException(message);
                        break;
                    case SC_METHOD_NOT_ALLOWED:
                        exception = createNotAllowedException(message, responseContext);
                        break;
                    case HttpServletResponse.SC_NOT_ACCEPTABLE:
                        exception = (message == null) ? new NotAcceptableException()
                                                      : new NotAcceptableException(message);
                        break;
                    case HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE:
                        exception = (message == null) ? new NotSupportedException()
                                                      : new NotSupportedException(message);
                        break;
                    case HttpServletResponse.SC_INTERNAL_SERVER_ERROR:
                        exception = (message == null) ? new InternalServerErrorException()
                                                      : new InternalServerErrorException(message);
                        break;
                    case HttpServletResponse.SC_SERVICE_UNAVAILABLE:
                        exception = (message == null) ? new ServiceUnavailableException()
                                                      : new ServiceUnavailableException(message);
                        break;
                    default:
                        // For servers that uses custom status. E.g. SFS uses 465 Access to the Document Denied
                        exception = (message == null) ? new WebApplicationException(status)
                                                      : new WebApplicationException(message, status);
                }

                throw exception;
            }
        }
    }

    /**
     * Returns the media type from the response.
     *
     * @param response the response
     * @return the media type, or {@code null} if it is not present or malformed
     */
    protected MediaType getMediaType(ClientResponseContext response) {
        try {
            return response.getMediaType();
        } catch (Throwable exception) {
            return null;
        }
    }

    /**
     * Returns any error message from the response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     */
    private String getErrorMessage(ClientResponseContext response) {
        String message = null;
        try {
            if (reader != null && reader.canRead(response)) {
                message = reader.read(response);
            } else if (plainTextReader.canRead(response)) {
                message = plainTextReader.read(response);
            }
        } catch (Throwable exception) {
            log.error("Failed to deserialise message from response: " + exception.getMessage(), exception);
        }
        return message;
    }

    /**
     * Returns an exception for {@code SC_METHOD_NOT_AUTHORIZED}.
     *
     * @param message the error message. May be {@code null}
     * @return the exception
     */
    private WebApplicationException createNotAuthorisedException(String message, int status) {
        Response response = Response.status(status).build();
        return (message == null) ? new NotAuthorizedException(response)
                                 : new NotAuthorizedException(message, response);
    }

    /**
     * Returns an exception for {@code SC_METHOD_NOT_ALLOWED}.
     *
     * @param message         the error message. May be {@code null}
     * @param responseContext the response context
     * @return the exception
     */
    private WebApplicationException createNotAllowedException(String message, ClientResponseContext responseContext) {
        WebApplicationException result = null;
        try {
            String allowed = responseContext.getHeaderString(HttpHeaders.ALLOW);
            Response response = Response.status(SC_METHOD_NOT_ALLOWED).header(HttpHeaders.ALLOW, allowed).build();
            result = (message == null) ? new NotAllowedException(response) : new NotAllowedException(message, response);
        } catch (Throwable exception) {
            log.error("Failed to create NotAllowedException: " + exception.getMessage(), exception);
        }
        if (result == null) {
            // fall back to ClientErrorException
            return new ClientErrorException(message, SC_METHOD_NOT_ALLOWED);
        }
        return result;
    }

}