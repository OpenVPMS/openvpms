/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.ws.util.filter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientResponseContext;

/**
 * Reads an error message from a {@link ClientResponseContext} serialised as an object.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSerialisedErrorMessageReader<T> extends AbstractErrorMessageReader {

    /**
     * The error type.
     */
    private final Class<T> type;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractSerialisedErrorMessageReader.class);


    /**
     * Constructs an {@link AbstractErrorMessageReader}.
     *
     * @param type the type of the error to deserialise
     */
    public AbstractSerialisedErrorMessageReader(Class<T> type) {
        this.type = type;
    }

    /**
     * Reads an error message from a response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     * @throws Throwable for any error
     */
    @Override
    protected String readProtected(ClientResponseContext response) throws Throwable {
        String result = null;
        String value = super.readProtected(response);
        try {
            T object = deserialise(value, type);
            if (object != null) {
                result = StringUtils.trimToNull(toString(object));
            }
        } catch (Throwable exception) {
            log.error("Failed to deserialise error from response=\"" + response + "\": " + exception.getMessage(),
                      exception);
        }
        return result;
    }

    /**
     * Deserialises an instance of the specified type from a string.
     *
     * @param value the string to deserialise
     * @param type  the type
     * @return the deserialised object. May be {@code null}
     * @throws Throwable if the object cannot be deserialised
     */
    protected abstract T deserialise(String value, Class<T> type) throws Throwable;

    /**
     * Returns a string representation of the object.
     * <p/>
     * This implementation simply invokes {@code object.toString()} and trims leading and trailing whitespace.
     * <p/>
     * Subclasses should override this if the object doesn't provide a toString() method, or whitespace should be
     * preserved.
     *
     * @param object the object
     * @return a string representation of the object. May be {@code null}
     */
    protected String toString(T object) {
        return StringUtils.trimToNull(object.toString());
    }

}
