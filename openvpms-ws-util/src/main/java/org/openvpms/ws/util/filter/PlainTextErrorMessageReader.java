/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.ws.util.filter;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.ws.util.MediaTypes;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;

/**
 * Reads a plain text error message from a {@link ClientResponseContext}.
 * <p/>
 * This trims whitespace.
 *
 * @author Tim Anderson
 */
public class PlainTextErrorMessageReader extends AbstractErrorMessageReader {

    /**
     * Determines if this reader can read messages from a response.
     *
     * @param response the response
     * @return {@code true} if the response is supported, otherwise {@code false}
     */
    @Override
    public boolean canRead(ClientResponseContext response) {
        return MediaTypes.isA(getMediaType(response), MediaType.TEXT_PLAIN_TYPE);
    }

    /**
     * Reads an error message from a response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     * @throws Throwable for any error
     */
    @Override
    protected String readProtected(ClientResponseContext response) throws Throwable {
        return StringUtils.trimToNull(super.readProtected(response));
    }
}
