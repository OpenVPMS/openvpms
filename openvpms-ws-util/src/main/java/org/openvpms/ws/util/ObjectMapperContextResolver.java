/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.ws.rs.Produces;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Provides an {@link ObjectMapper} that ensures date/times are serialized in ISO-8601 format.
 *
 * @author Tim Anderson
 * @see <a href="http://jersey.java.net/documentation/latest/user-guide.html#jackson-registration">Jackson</a>
 */
@Provider
@Produces("application/json")
public class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

    /**
     * ISO 8601 date/time with millisecond precision and timezone.
     */
    public static final String ISO_8601_MILLSECOND_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    /**
     * ISO 8601 date/time with second precision and timezone.
     */
    public static final String ISO_8601_SECOND_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ssXXX";

    /**
     * The object mapper.
     */
    private final ObjectMapper mapper = new ObjectMapper();


    /**
     * Constructs {@link ObjectMapperContextResolver}.
     * <p/>
     * This generates and parses timestamps using the {@link #ISO_8601_MILLSECOND_TIMEZONE} date pattern.
     *
     * @param timeZone the time zone used to determine how date/times are serialized
     */
    public ObjectMapperContextResolver(TimeZone timeZone) {
        SimpleDateFormat format = new SimpleDateFormat(ISO_8601_MILLSECOND_TIMEZONE);
        format.setTimeZone(timeZone);
        mapper.setDateFormat(format);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    /**
     * Constructs a {@link ObjectMapperContextResolver}.
     *
     * @param timeZone the time zone used to determine how date/times are serialized
     * @param pattern  the date/time pattern
     */
    public ObjectMapperContextResolver(TimeZone timeZone, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(timeZone);
        mapper.setDateFormat(format);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    }

    /**
     * Get a context of type {@code T} that is applicable to the supplied type.
     *
     * @param type the class of object for which a context is desired
     * @return a context for the supplied type or {@code null} if a context for the supplied type is not available from
     * this provider.
     */
    @Override
    public ObjectMapper getContext(Class<?> type) {
        return mapper;
    }
}
