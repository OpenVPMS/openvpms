/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;

import org.openvpms.component.model.object.Reference;

/**
 * Represents a mapping between an OpenVPMS object (the source) and that held by another service (the target).
 *
 * @author Tim Anderson
 */
public interface Mapping {

    /**
     * Returns the identifier of the source object.
     *
     * @return the source object identifier
     */
    Reference getSource();

    /**
     * Returns the target object.
     *
     * @return the target object
     */
    Target getTarget();

}
