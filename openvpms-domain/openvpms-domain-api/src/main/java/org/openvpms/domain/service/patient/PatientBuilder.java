/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.patient;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.Patient.Sex;

import java.time.LocalDate;

/**
 * Patient builder.
 *
 * @author Tim Anderson
 */
public interface PatientBuilder {

    /**
     * Sets the patient name.
     *
     * @param name the patient name
     * @return this
     */
    PatientBuilder name(String name);

    /**
     * Sets the patient species.
     *
     * @param species the code for a <em>lookup.species</em>
     * @return this
     */
    PatientBuilder species(String species);

    /**
     * Sets the patient species.
     *
     * @param species a <em>lookup.species</em>
     * @return this
     */
    PatientBuilder species(Lookup species);

    /**
     * Sets the patient breed.
     *
     * @param breed the code for a <em>lookup.breed</em>
     * @return this
     */
    PatientBuilder breed(String breed);

    /**
     * Sets the patient breed.
     *
     * @param breed a <em>lookup.breed</em>
     * @return this
     */
    PatientBuilder breed(Lookup breed);

    /**
     * Sets the patient sex.
     *
     * @param sex the patient sex
     * @return this
     */
    PatientBuilder sex(Sex sex);

    /**
     * Determines if the patient has been desexed or not.
     *
     * @param desexed if {@code true}, the patient has been desexed
     * @return this
     */
    PatientBuilder desexed(boolean desexed);

    /**
     * Sets the patient date of birth.
     *
     * @param dateOfBirth the date of birth
     * @return this
     */
    PatientBuilder dateOfBirth(LocalDate dateOfBirth);

    /**
     * Determines if the patient is deceased.
     *
     * @param deceased if {@code true}, the patient is deceased
     * @return this
     */
    PatientBuilder deceased(boolean deceased);

    /**
     * Sets the patient date of death.
     *
     * @param dateOfDeath the date of death
     * @return this
     */
    PatientBuilder dateOfDeath(LocalDate dateOfDeath);

    /**
     * Sets the patient colour.
     *
     * @param colour the patient colour
     * @return this
     */
    PatientBuilder colour(String colour);

    /**
     * Sets the patient owner.
     * <p/>
     * If the patient already has an owner relationship, the existing relationship will be ended.
     *
     * @param owner the owner, or {@code null} to end the existing relationship
     * @return this
     */
    PatientBuilder owner(Customer owner);

    /**
     * Sets the microchip.
     *
     * @param microchip the microchip. If {@code null} removes the existing microchip
     * @return this
     */
    PatientBuilder microchip(String microchip);

    /**
     * Determines if the patient is active.
     *
     * @param active if {@code true}, the patient is active, {@code false} if it is inactive
     * @return this
     */
    PatientBuilder active(boolean active);

    /**
     * Builds the patient.
     *
     * @return the patient
     */
    Patient build();
}