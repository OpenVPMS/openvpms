/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.product;

import org.openvpms.component.math.Weight;

import java.util.List;

/**
 * Template product.
 *
 * @author Tim Anderson
 */
public interface Template extends BaseProduct {

    /**
     * Returns the note to be included as a note in patient history, when this template is expanded.
     *
     * @return the visit note. May be {@code null}
     */
    String getVisitNote();

    /**
     * Returns the items included by this template.
     * <p/>
     * Inactive products are excluded.
     *
     * @return the items
     */
    List<TemplateItem> getItems();

    /**
     * Returns the items included by this template, for a patient weight.
     *
     * @param weight the patient weight
     * @return the items
     */
    List<TemplateItem> getItems(Weight weight);

}
