/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.customer;

import org.openvpms.domain.customer.Customer;

/**
 * Manages customers.
 *
 * @author Tim Anderson
 */
public interface Customers {

    /**
     * Returns a customer given its identifier.
     *
     * @param id the customer identifier
     * @return the corresponding customer, or {@code null} if none is found
     */
    Customer getCustomer(long id);

    /**
     * Returns a customer query.
     *
     * @return a new query
     */
    CustomerQuery getQuery();

    /**
     * Returns a builder for a customer.
     *
     * @return a customer builder
     */
    CustomerBuilder getCustomerBuilder();

    /**
     * Returns a builder to update a customer.
     *
     * @param customer the customer to update
     * @return a customer builder
     */
    CustomerBuilder getCustomerBuilder(Customer customer);

}
