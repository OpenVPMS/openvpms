/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.sync;

/**
 * Records changes made when synchronising data.
 *
 * @author Tim Anderson
 */
public interface Changes<T> {

    /**
     * Records an object as being added.
     *
     * @param object the object
     * @return this
     */
    Changes<T> added(T object);

    /**
     * Records an object as being updated.
     *
     * @param object the object
     * @return this
     */
    Changes<T> updated(T object);

    /**
     * Records an object as being deactivated.
     *
     * @param object the object
     * @return this
     */
    Changes<T> deactivated(T object);

}
