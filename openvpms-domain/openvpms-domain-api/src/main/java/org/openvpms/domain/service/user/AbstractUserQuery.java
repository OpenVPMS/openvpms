/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.user;

import org.openvpms.component.model.user.User;
import org.openvpms.domain.query.DomainQuery;

/**
 * User query.
 *
 * @author Tim Anderson
 */
public interface AbstractUserQuery<D extends User, Q extends AbstractUserQuery<D, Q>> extends DomainQuery<D, Q> {

    /**
     * Filter users by username.
     *
     * @param username the username
     * @return this
     */
    Q username(String username);

    /**
     * Returns users that are employees.
     *
     * @return an employee query
     */
    EmployeeQuery employees();

    /**
     * Return users that are clinicians.
     *
     * @return this
     */
    Q clinicians();
}
