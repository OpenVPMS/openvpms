/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.practice.Location;

import java.util.List;

/**
 * Laboratory definition.
 *
 * @author Tim Anderson
 */
public interface Laboratory extends Entity {

    /**
     * The laboratory archetypes.
     */
    String ARCHETYPES = "entity.laboratoryService*";

    /**
     * Returns the practice locations that this laboratory may be used at.
     *
     * @return the locations, or an empty list if the laboratory is available at all locations
     */
    List<Location> getLocations();

    /**
     * Returns the devices that this laboratory manages.
     *
     * @param activeOnly if {@code true}, only return active devices
     * @return the devices supported by the laboratory
     */
    List<Device> getDevices(boolean activeOnly);

    /**
     * Returns all the tests supported by the laboratory.
     *
     * @param activeOnly if {@code true}, only return active tests
     * @return the tests supported by the laboratory.
     */
    List<Test> getTests(boolean activeOnly);

    /**
     * Returns the configuration for a practice location.
     * <p/>
     * Laboratory implementations can provide a custom <em>entityLink.laboratoryLocation&lt;provider&gt;</em>
     * in order to configure location specific details such as logins.
     *
     * @param location the location
     * @return the configuration, or {@code null} if none is found
     */
    IMObject getConfiguration(Location location);

}
