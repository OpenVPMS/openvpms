/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.customer.transaction;

import java.math.BigDecimal;

/**
 * Line item on a transaction.
 *
 * @author Tim Anderson
 */
public interface TransactionItem {

    /**
     * Returns the OpenVPMS identifier for this item.
     *
     * @return the identifier
     */
    long getId();

    /**
     * Returns the total amount, including tax.
     *
     * @return the total amount
     */
    BigDecimal getTotal();

}
