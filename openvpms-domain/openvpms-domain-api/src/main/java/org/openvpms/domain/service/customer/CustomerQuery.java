/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.customer;

import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.query.DomainQuery;
import org.openvpms.domain.query.Filter;

/**
 * Customer query.
 *
 * @author Tim Anderson
 */
public interface CustomerQuery extends DomainQuery<Customer, CustomerQuery> {

    /**
     * Filter customers by last and first name.
     *
     * @param lastName  the customer last name. May be {@code null} to reset the criteria
     * @param firstName the customer first name. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery name(String lastName, String firstName);

    /**
     * Filter customers by last and first name.
     * <p/>
     * Only 'equal' and 'like' filters are supported, and the operator must be the same for each filter.
     *
     * @param lastName  the customer last name criteria. May be {@code null} to reset the criteria
     * @param firstName the customer first name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery name(Filter<String> lastName, Filter<String> firstName);

    /**
     * Filter customers by address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery address(String address, String suburb, String postcode, String state);

    /**
     * Filter customers by address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery address(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                          Filter<String> state);

    /**
     * Filter customers by email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery email(String email);

    /**
     * Filter customers by email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery email(Filter<String> email);

    /**
     * Filter customers by phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery phone(String phone);

    /**
     * Filter customers by phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    CustomerQuery phone(Filter<String> phone);
}