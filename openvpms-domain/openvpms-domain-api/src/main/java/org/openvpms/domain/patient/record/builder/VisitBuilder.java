/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.record.builder;

import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;

/**
 * A builder for {@link Visit} instances.
 *
 * @author Tim Anderson
 */
public interface VisitBuilder extends RecordBuilder<Visit, VisitBuilder> {

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    Patient getPatient();

    /**
     * Sets the patient.
     *
     * @param patient the patient
     * @return this
     */
    VisitBuilder patient(Patient patient);

    /**
     * Returns a builder to add a note.
     *
     * @return a new note builder
     */
    VisitNoteBuilder newNote();

    /**
     * Returns a builder to add a weight.
     *
     * @return a new weight builder
     */
    VisitWeightBuilder newWeight();

    /**
     * Builds the visit.
     *
     * @return the visit
     */
    Visit build();

}
