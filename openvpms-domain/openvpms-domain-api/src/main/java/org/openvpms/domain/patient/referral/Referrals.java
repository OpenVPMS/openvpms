/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.referral;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Patient referrals.
 *
 * @author Tim Anderson
 */
public interface Referrals {

    /**
     * Returns the current referral.
     *
     * @return the current referral, or {@code null} if the patient has none
     */
    Referral getCurrent();

    /**
     * Returns where the patient was referred from.
     * <p/>
     * If the patient has been referred from other practices multiple times, this represents the most recent referral.
     *
     * @return the referral. May be {@code null}. If present, may be inactive.
     */
    Referral getReferredFrom();

    /**
     * Returns where the patient was referred from.
     * <p/>
     * If the patient has been referred from other practices multiple times, this represents the most recent referral.
     *
     * @param activeOnly if {@code true}, only return the referral if it is current
     * @return the referral. May be {@code null}
     */
    Referral getReferredFrom(boolean activeOnly);

    /**
     * Returns where the patient was referred to.
     * <p/>
     * If the patient has been referred to other practices multiple times, this represents the most recent referral.
     *
     * @return the referral. May be {@code null}. If present, may be inactive.
     */
    Referral getReferredTo();

    /**
     * Returns where the patient was referred to.
     * <p/>
     * If the patient has been referred to other practices multiple times, this represents the most recent referral.
     *
     * @param activeOnly if {@code true}, only return the referral if it is current
     * @return the referral. May be {@code null}
     */
    Referral getReferredTo(boolean activeOnly);

    /**
     * Returns the referral as at the specified date.
     *
     * @return the referral or {@code null} if the patient has no referral as at the date
     */
    Referral getReferral(OffsetDateTime dateTime);

    /**
     * Returns all referrals for the patient.
     *
     * @return all referrals for the patient, sorted most recent to oldest
     */
    List<Referral> getAll();
}