/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.patient;

import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.patient.record.builder.VisitBuilder;

/**
 * Manages patients.
 *
 * @author Tim Anderson
 */
public interface Patients {

    /**
     * Returns a patient given its identifier.
     *
     * @param id the patient identifier
     * @return the corresponding patient, or {@code null} if none is found
     */
    Patient getPatient(long id);

    /**
     * Returns a patient query.
     *
     * @return a new query
     */
    PatientQuery getQuery();

    /**
     * Returns a builder for a patient.
     *
     * @return a patient builder
     */
    PatientBuilder getPatientBuilder();

    /**
     * Returns a builder to update a patient.
     *
     * @param patient the patient to update
     * @return a patient builder
     */
    PatientBuilder getPatientBuilder(Patient patient);

    /**
     * Returns a builder for a patient visit.
     * <p/>
     * If there is a current vist for the patient, records will be added to that, else a new visit will be
     * created.
     *
     * @return a visit builder
     */
    VisitBuilder getVisitBuilder();

    /**
     * Returns a builder to update a patient visit.
     *
     * @param visit the visit to update
     * @return a visit builder
     */
    VisitBuilder getVisitBuilder(Visit visit);

    /**
     * Returns a record given its archetype and external identity.
     *
     * @param archetype   the record archetype
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return the corresponding record, or {@code null} if none is found
     */
    Record getRecord(String archetype, String idArchetype, String identity);

    /**
     * Determines if a record exists.
     *
     * @param archetype   the record archetype
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return {@code true} if the record exists, otherwise {@code false}
     */
    boolean exists(String archetype, String idArchetype, String identity);

}
