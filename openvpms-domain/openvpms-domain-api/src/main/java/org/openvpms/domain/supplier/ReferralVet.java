/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.supplier;

import java.time.OffsetDateTime;

/**
 * An external veterinarian, used for referrals.
 *
 * @author Tim Anderson
 */
public interface ReferralVet extends Supplier {

    /**
     * Returns the vet's title.
     *
     * @return the title. May be {@code null}
     */
    String getTitle();

    /**
     * Returns the vet's first name.
     *
     * @return the first name
     */
    String getFirstName();

    /**
     * Returns the vet's last name.
     *
     * @return the last name
     */
    String getLastName();

    /**
     * Returns the vet's current practice.
     *
     * @return the practice. May be {@code null}. If present, the practice may be inactive.
     */
    ReferralPractice getPractice();

    /**
     * Returns the vet's practice, as at the specified date.
     *
     * @param date the date
     * @return the practice. May be {@code null}. If present, the practice may be inactive.
     */
    ReferralPractice getPractice(OffsetDateTime date);
}