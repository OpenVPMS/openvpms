/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.referral;

import org.openvpms.domain.supplier.ReferralPractice;
import org.openvpms.domain.supplier.ReferralVet;

import java.time.OffsetDateTime;

/**
 * Patient referral.
 *
 * @author Tim Anderson
 */
public interface Referral {

    /**
     * Returns the practice the patient was referred from/to.
     *
     * @return the practice. The practice may be inactive. May be {@code null}
     */
    ReferralPractice getPractice();

    /**
     * Returns the vet the patient was referred from/to.
     *
     * @return the vet. The vet may be inactive
     */
    ReferralVet getVet();

    /**
     * Returns the date when the referral was made.
     *
     * @return the date
     */
    OffsetDateTime getDate();

    /**
     * Determines if this referral is active.
     * <p/>
     * A referral is considered active if it is the most recent referral.
     *
     * @return {@code true} if the referral is active, {@code false} if there is a more recent referral
     */
    boolean isActive();

    /**
     * Determines if the patient was referred from the practice/vet.
     *
     * @return {@code true} if the patient was referred from the practice/vet, {@code false} if they were referred to
     * it
     */
    boolean referredFrom();

    /**
     * Determines if the patient was referred to the practice/vet.
     *
     * @return {@code true} if the patient was referred to the practice/vet, {@code false} if they were referred from
     * it
     */
    boolean referredTo();

    /**
     * Returns the reason for the referral.
     *
     * @return the reason. May be {@code null}
     */
    String getReason();
}