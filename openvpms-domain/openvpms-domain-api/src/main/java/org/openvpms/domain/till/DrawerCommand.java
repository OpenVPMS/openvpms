/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.till;

/**
 * The command to open a till drawer, when it is opened by sending bytes to a printer.
 *
 * @author Tim Anderson
 */
public interface DrawerCommand {

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    String getPrinter();

    /**
     * Returns the archetype of the document printer service that manages the printer.
     *
     * @return the archetype, or {@code null} for the Java Print API
     */
    String getPrinterServiceArchetype();

    /**
     * Returns the command.
     *
     * @return the command
     */
    byte[] getCommand();
}