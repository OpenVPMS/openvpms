/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.patient;

import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Active;
import org.openvpms.domain.query.DomainQuery;
import org.openvpms.domain.query.Filter;

/**
 * Patient query.
 *
 * @author Tim Anderson
 */
public interface PatientQuery extends DomainQuery<Patient, PatientQuery> {

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery microchip(String microchip);

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery microchip(Filter<String> microchip);

    /**
     * Filter patients by owner.
     *
     * @param owner the owner. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery owner(Customer owner);

    /**
     * Filter patients by owner identifier.
     *
     * @param ownerId the owner identifier
     * @return this
     */
    PatientQuery owner(long ownerId);

    /**
     * Filter patients by owner name.
     *
     * @param lastName the owner's last name. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerName(String lastName);

    /**
     * Filter patients by owner name.
     *
     * @param lastName  the owner's last name. May be {@code null} to reset the criteria
     * @param firstName the owner's first name. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerName(String lastName, String firstName);

    /**
     * Filter patients by owner name.
     * <p/>
     * Only 'equal' and 'like' filters are supported.
     *
     * @param lastName the owner's last name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerName(Filter<String> lastName);

    /**
     * Filter patients by owner name.
     * <p/>
     * Only 'equal' and 'like' filters are supported.
     *
     * @param lastName  the owner's last name criteria. May be {@code null} to reset the criteria
     * @param firstName the owner's first name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerName(Filter<String> lastName, Filter<String> firstName);

    /**
     * Only query patients with active owners.
     *
     * @return this
     */
    PatientQuery activeOwner();

    /**
     * Only query patients with inactive owners.
     *
     * @return this
     */
    PatientQuery inactiveOwner();

    /**
     * Determines if patients with active, inactive, or both active and inactive owners are returned.
     *
     * @param active the active state
     * @return this
     */
    PatientQuery activeOwner(Active active);

    /**
     * Filter patients by owner address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerAddress(String address, String suburb, String postcode, String state);

    /**
     * Filter patients by owner address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerAddress(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                              Filter<String> state);

    /**
     * Filter patients by owner email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerEmail(String email);

    /**
     * Filter patients by owner email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerEmail(Filter<String> email);

    /**
     * Filter patients by owner phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerPhone(String phone);

    /**
     * Filter patients by owner phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    PatientQuery ownerPhone(Filter<String> phone);

}
