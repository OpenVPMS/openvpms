/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.user;

import org.openvpms.component.model.user.User;

/**
 * Manages users.
 *
 * @author Tim Anderson
 */
public interface Users {

    /**
     * Returns a user given its identifier.
     *
     * @param id the user identifier
     * @return the corresponding user, or {@code null} if none is found
     */
    User getUser(long id);

    /**
     * Returns a user given its username.
     *
     * @param username the username
     * @return the corresponding user, or {@code null} if none is found
     */
    User getUser(String username);

    /**
     * Returns a user query.
     *
     * @return a new query
     */
    UserQuery getQuery();
}
