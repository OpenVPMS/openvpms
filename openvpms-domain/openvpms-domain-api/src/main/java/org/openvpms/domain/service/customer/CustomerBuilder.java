/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.service.customer;

import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.practice.Location;

/**
 * Customer builder.
 *
 * @author Tim Anderson
 */
public interface CustomerBuilder {

    /**
     * Sets the customer title.
     *
     * @param title the code for a <em>lookup.personTitle</em>
     * @return this
     */
    CustomerBuilder title(String title);

    /**
     * Sets the customer first name.
     *
     * @param firstName the first name
     * @return this
     */
    CustomerBuilder firstName(String firstName);

    /**
     * Sets the customer last name.
     *
     * @param lastName the last name
     * @return this
     */
    CustomerBuilder lastName(String lastName);

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     * @return this
     */
    CustomerBuilder companyName(String companyName);

    /**
     * Sets the customer address.
     *
     * @param address  the address
     * @param suburb   the code for a <em>lookup.suburb</em>
     * @param postcode the postcode
     * @param state    the code for a <em>lookup.state</em>
     * @return this
     */
    CustomerBuilder address(String address, String suburb, String postcode, String state);

    /**
     * Sets the customer home phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing home phone
     * @return this
     */
    CustomerBuilder homePhone(String phone);

    /**
     * Sets the customer work phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing work phone
     * @return this
     */
    CustomerBuilder workPhone(String phone);

    /**
     * Sets the customer mobile phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing mobile phone
     * @return this
     */
    CustomerBuilder mobilePhone(String phone);

    /**
     * Sets the customer email.
     *
     * @param email the email, or {@code null} to remove the existing email
     * @return this
     */
    CustomerBuilder email(String email);

    /**
     * Sets the preferred practice location for the customer.
     *
     * @param location the preferred practice location
     * @return this
     */
    CustomerBuilder practice(Location location);

    /**
     * Determines if the customer is active.
     *
     * @param active if {@code true}, the customer is active, {@code false} if it is inactive
     * @return this
     */
    CustomerBuilder active(boolean active);

    /**
     * Builds the customer.
     *
     * @return the customer
     */
    Customer build();
}