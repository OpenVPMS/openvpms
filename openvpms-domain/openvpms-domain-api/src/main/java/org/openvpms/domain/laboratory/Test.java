/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;

import java.math.BigDecimal;
import java.util.List;

/**
 * A test that can be performed by a laboratory.
 *
 * @author Tim Anderson
 */
public interface Test extends Entity {

    /**
     * The laboratory test archetype.
     */
    String ARCHETYPE = "entity.laboratoryTest";

    enum UseDevice {
        YES,          // test requires a device to be specified when ordering
        NO,           // test does not use a device
        OPTIONAL      // test can have a device specified when ordering
    }

    /**
     * Returns the laboratory assigned test code.
     * <p/>
     * This is short for: {@code getCodeIdentity().getIdentity()}
     *
     * @return the test code
     */
    String getCode();

    /**
     * Returns the laboratory assigned identifier for this test.
     *
     * @return the test identifier
     */
    Identity getCodeIdentity();

    /**
     * Returns the investigation type.
     *
     * @return the investigation type
     */
    InvestigationType getInvestigationType();

    /**
     * Determines if the test can be grouped with other tests in an order.
     *
     * @return {@code true} if the test can be grouped with other tests; otherwise it must be submitted individually
     */
    boolean getGroup();

    /**
     * Returns the turnaround notes for this test.
     *
     * @return turnaround notes. May be {@code null}
     */
    String getTurnaround();

    /**
     * Returns the specimen collection notes for this test.
     *
     * @return the specimen collection notes. May be {@code null}
     */
    String getSpecimen();

    /**
     * Returns the test price.
     *
     * @return the test price, excluding tax
     */
    BigDecimal getPrice();

    /**
     * Determines if the laboratory device must be specified when this test is ordered.
     *
     * @return {@link UseDevice#YES YES} if a device must be specified, {@link UseDevice#NO NO} if no device should be
     * specified and {@link UseDevice#OPTIONAL OPTIONAL} if a device may be specified
     */
    UseDevice getUseDevice();

    /**
     * Returns the devices that can perform this test.
     *
     * @return the devices
     */
    List<Device> getDevices();

}
