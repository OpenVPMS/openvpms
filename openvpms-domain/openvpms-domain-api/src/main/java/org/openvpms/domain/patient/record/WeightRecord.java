/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.patient.record;

import org.openvpms.component.math.Weight;

/**
 * Represents a patient weight.
 *
 * @author Tim Anderson
 */
public interface WeightRecord extends Record {

    /**
     * The weight archetype.
     */
    String ARCHETYPE = "act.patientWeight";

    /**
     * Returns the patient weight.
     *
     * @return the patient weight
     */
    Weight getWeight();

}
