/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.customer.transaction;

import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.practice.Location;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

/**
 * Customer transaction.
 *
 * @author Tim Anderson
 */
public interface Transaction<T extends TransactionItem> {

    /**
     * Returns the OpenVPMS identifier for this transaction.
     *
     * @return the identifier
     */
    long getId();

    /**
     * Returns the transaction date.
     *
     * @return the transaction date
     */
    OffsetDateTime getDate();

    /**
     * Returns the customer the transaction is for.
     *
     * @return the customer. May be {@code null} for over-the-counter charges
     */
    Customer getCustomer();

    /**
     * Returns the line items.
     *
     * @return the line items
     */
    List<T> getItems();

    /**
     * Returns the transaction total, including tax.
     *
     * @return the transaction total
     */
    BigDecimal getTotal();

    /**
     * Determines if this transaction has been finalised.
     * <p/>
     * Finalised transactions cannot be modified.
     *
     * @return {@code true} if this transaction has been finalised, otherwise {@code false}
     */
    boolean isFinalised();

    /**
     * Returns the location where this transaction was performed.
     *
     * @return the location. May be {@code null}
     */
    Location getLocation();

}
