/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.customer.CustomerQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link CustomerQueryImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CustomerQueryImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Tests the {@link CustomerQueryImpl#id(long)} method.
     */
    @Test
    public void testQueryById() {
        Customer customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        CustomerQuery query = getQuery()
                .id(customer.getId());

        checkQuery(query, customer);
        assertEquals(customer, query.findFirst());
    }

    /**
     * Tests the {@link CustomerQueryImpl#name(String)} method.
     */
    @Test
    public void testQueryByName() {
        String name = ValueStrategy.randomString();
        Customer customerA = createCustomer(name);
        Customer customerB = createCustomer(name);

        CustomerQuery query = getQuery()
                .name(name);
        checkQuery(query, customerA, customerB);
    }

    /**
     * Tests the {@link CustomerQueryImpl#name(Filter)} method.
     */
    @Test
    public void testQueryByNameFilter() {
        String name = ValueStrategy.randomString();
        Customer customerA = createCustomer(name + "A");
        Customer customerB = createCustomer(name + "B");

        CustomerQuery query1 = getQuery()
                .name(Filter.like(name + "%"));
        checkQuery(query1, customerA, customerB);
    }

    /**
     * Tests the {@link CustomerQueryImpl#active()} and {@link CustomerQueryImpl#inactive()}.
     */
    @Test
    public void testQueryByActiveInactive() {
        String name = ValueStrategy.randomString();
        Customer customerA = createCustomer(name + "A", true);
        Customer customerB = createCustomer(name + "B", false);
        Customer customerC = createCustomer(name + "C", true);

        // verify all customers are returned by default
        CustomerQuery query1 = getQuery()
                .name(Filter.like(name + "%"));
        checkQuery(query1, customerA, customerB, customerC);

        // check active customers
        CustomerQuery query2 = getQuery()
                .name(Filter.like(name + "%"))
                .active();
        checkQuery(query2, customerA, customerC);

        // check inactive customers
        CustomerQuery query3 = getQuery()
                .name(Filter.like(name + "%"))
                .inactive();
        checkQuery(query3, customerB);
    }

    /**
     * Tests the {@link CustomerQueryImpl#orderById()} and {@link CustomerQueryImpl#orderById(boolean)} methods.
     */
    @Test
    public void testOrderById() {
        String name = ValueStrategy.randomString();
        Customer customerA = createCustomer(name + "A", true);
        Customer customerB = createCustomer(name + "B", false);
        Customer customerC = createCustomer(name + "C", true);

        // default should be to order by ascending id, if no order specified.
        CustomerQuery query1 = getQuery()
                .name(Filter.like(name + "%"));
        checkQuery(query1, customerA, customerB, customerC);

        CustomerQuery query2 = getQuery()
                .name(Filter.like(name + "%"))
                .orderById();
        checkQuery(query2, customerA, customerB, customerC);

        CustomerQuery query3 = getQuery()
                .name(Filter.like(name + "%"))
                .orderById(true);
        checkQuery(query3, customerA, customerB, customerC);

        CustomerQuery query4 = getQuery()
                .name(Filter.like(name + "%"))
                .orderById(false);
        checkQuery(query4, customerC, customerB, customerA);
    }

    /**
     * Tests the {@link CustomerQueryImpl#orderByName()} and {@link CustomerQueryImpl#orderByName(boolean)} methods.
     */
    @Test
    public void testOrderByName() {
        String name = ValueStrategy.randomString();
        Customer customerA = createCustomer(name + "Z");
        Customer customerB = createCustomer(name + "A");
        Customer customerC = createCustomer(name + "A");
        // by default, customerB should always appear before customerC as it has a lower id
        assertTrue(customerB.getId() < customerC.getId());

        CustomerQuery query1 = getQuery()
                .name(Filter.like(name + "%"))
                .orderByName();
        checkQuery(query1, customerB, customerC, customerA);

        CustomerQuery query2 = getQuery()
                .name(Filter.like(name + "%"))
                .orderByName(true);
        checkQuery(query2, customerB, customerC, customerA);

        CustomerQuery query3 = getQuery()
                .name(Filter.like(name + "%"))
                .orderByName(false);
        checkQuery(query3, customerA, customerB, customerC);

        // verify can order by name and descending id
        CustomerQuery query4 = getQuery()
                .name(Filter.like(name + "%"))
                .orderByName(true)
                .orderById(false);
        checkQuery(query4, customerC, customerB, customerA);
    }

    /**
     * Tests the {@link CustomerQueryImpl#name(String, String)} and {@link CustomerQueryImpl#name(Filter, Filter)}
     * methods.
     */
    @Test
    public void testQueryByLastFirstName() {
        String suffix = ValueStrategy.randomString();
        String lastNameA = "Smith" + suffix;
        String lastNameB = "Smyth" + suffix;
        String lastNameC = "Smithers" + suffix;
        Customer customerA = domainService.create(customerFactory.createCustomer("Jo", lastNameA), Customer.class);
        Customer customerB = domainService.create(customerFactory.createCustomer("Jo", lastNameB), Customer.class);
        Customer customerC = domainService.create(customerFactory.createCustomer("Joanna", lastNameC), Customer.class);

        CustomerQuery query1 = getQuery()
                .name(lastNameA);
        checkQuery(query1, customerA);

        CustomerQuery query2 = getQuery()
                .name(lastNameA, "Jo");
        checkQuery(query2, customerA);

        CustomerQuery query3 = getQuery()
                .name(lastNameA, "Joanna");
        checkQuery(query3);

        CustomerQuery query4 = getQuery()
                .name(Filter.equal(lastNameA));
        checkQuery(query4, customerA);

        CustomerQuery query5 = getQuery()
                .name(Filter.equal(lastNameB), Filter.equal("Jo"));
        checkQuery(query5, customerB);

        CustomerQuery query6 = getQuery()
                .name(lastNameA);
        checkQuery(query6, customerA);

        CustomerQuery query7 = getQuery()
                .name(Filter.like("Smith%" + suffix));
        checkQuery(query7, customerA, customerC);

        CustomerQuery query8 = getQuery()
                .name(Filter.like("S%" + suffix), Filter.like("Joan%"));
        checkQuery(query8, customerC);

        CustomerQuery query9 = getQuery()
                .name(Filter.like("S%" + suffix), Filter.like("J%"));
        checkQuery(query9, customerA, customerB, customerC);
    }

    /**
     * Tests the {@link CustomerQueryImpl#address(String, String, String, String)}
     * and {@link CustomerQueryImpl#address(Filter, Filter, Filter, Filter)} methods.
     */
    @Test
    public void testQueryByAddress() {
        String name = ValueStrategy.randomString(); // use the same name for all customers to limit results
        Lookup suburb1 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup suburb2 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup vic = lookupFactory.getLookup(ContactArchetypes.STATE, "VIC", "Victoria");
        Lookup nsw = lookupFactory.getLookup(ContactArchetypes.STATE, "NSW", "New South Wales");
        Customer customerA = domainService.create(
                customerFactory.newCustomer()
                        .lastName(name)
                        .addAddress("1024 Broadwater Avenue", suburb1.getCode(), vic.getCode(), "3925")
                        .build(), Customer.class);
        Customer customerB = domainService.create(
                customerFactory.newCustomer()
                        .lastName(name)
                        .addAddress("1024 Broadwater St", suburb1.getCode(), vic.getCode(), "3925")
                        .build(), Customer.class);
        Customer customerC = domainService.create(
                customerFactory.newCustomer()
                        .lastName(name)
                        .addAddress("1024 Broadwater St", suburb2.getCode(), nsw.getCode(), "2360")
                        .build(), Customer.class);

        CustomerQuery query1 = getQuery().name(name)
                .address("1024 Broadwater Avenue", suburb1.getName(), "3925", vic.getName());
        checkQuery(query1, customerA);

        CustomerQuery query2 = getQuery().name(name)
                .address("1024 Broadwater St", null, null, null);
        checkQuery(query2, customerB, customerC);

        CustomerQuery query3 = getQuery().name(name)
                .address(null, suburb1.getName(), null, null);
        checkQuery(query3, customerA, customerB);

        CustomerQuery query4 = getQuery().name(name)
                .address(null, null, "3925", null);
        checkQuery(query4, customerA, customerB);

        CustomerQuery query5 = getQuery().name(name)
                .address(null, null, null, vic.getName());
        checkQuery(query5, customerA, customerB);

        CustomerQuery query6 = getQuery().name(name)
                .address(Filter.like("1024 Broadwater%"), null, null, null);
        checkQuery(query6, customerA, customerB, customerC);

        CustomerQuery query7 = getQuery().name(name)
                .address(null, Filter.like(suburb1.getName() + "%"), null, null);
        checkQuery(query7, customerA, customerB);

        CustomerQuery query9 = getQuery().name(name)
                .address(null, null, Filter.like("236%"), null);
        checkQuery(query9, customerC);

        CustomerQuery query10 = getQuery().name(name)
                .address(null, null, null, Filter.like("Vic%"));
        checkQuery(query10, customerA, customerB);
    }

    /**
     * Tests the {@link CustomerQuery#email(String)} and {@link CustomerQuery#email(Filter)} methods.
     */
    @Test
    public void testQueryByEmail() {
        String prefix = ValueStrategy.randomString();
        String emailA = prefix + "A@gmail.com";
        String emailB = prefix + "B@gmail.com";
        String emailC = prefix + "C@gmail.com";
        Customer customerA = domainService.create(customerFactory.newCustomer().addEmail(emailA).build(),
                                                  Customer.class);
        Customer customerB = domainService.create(customerFactory.newCustomer().addEmail(emailB).build(),
                                                  Customer.class);
        Customer customerC = domainService.create(customerFactory.newCustomer().addEmail(emailC).build(),
                                                  Customer.class);
        CustomerQuery query1 = getQuery()
                .email(emailA);
        checkQuery(query1, customerA);

        CustomerQuery query2 = getQuery()
                .email(Filter.equal(emailB));
        checkQuery(query2, customerB);

        CustomerQuery query3 = getQuery()
                .email(Filter.like(prefix + "%"));
        checkQuery(query3, customerA, customerB, customerC);
    }

    /**
     * Tests the {@link CustomerQuery#phone(String)} and {@link CustomerQuery#phone(Filter)} methods.
     */
    @Test
    public void testQueryByPhone() {
        String prefix = ValueStrategy.randomString();
        String phoneA = prefix + "1";
        String phoneB = prefix + "2";
        String phoneC = prefix + "3";
        Customer customerA = domainService.create(customerFactory.newCustomer().addPhone(phoneA).build(),
                                                  Customer.class);
        Customer customerB = domainService.create(customerFactory.newCustomer().addPhone(phoneB).build(),
                                                  Customer.class);
        Customer customerC = domainService.create(customerFactory.newCustomer().addPhone(phoneC).build(),
                                                  Customer.class);
        CustomerQuery query1 = getQuery().phone(phoneA);
        checkQuery(query1, customerA);

        CustomerQuery query2 = getQuery().phone(Filter.equal(phoneB));
        checkQuery(query2, customerB);

        CustomerQuery query3 = getQuery().phone(Filter.like(prefix + "%"));
        checkQuery(query3, customerA, customerB, customerC);
    }

    /**
     * Returns a new query.
     *
     * @return a new query
     */
    private CustomerQueryImpl getQuery() {
        return new CustomerQueryImpl(getArchetypeService(), domainService);
    }

    /**
     * Verifies a query returns the expected result, in the order given.
     *
     * @param query    the query
     * @param expected the expected results
     */
    private void checkQuery(CustomerQuery query, Customer... expected) {
        List<Customer> matches = IterableUtils.toList(query.query());
        assertEquals(Arrays.asList(expected), matches);
    }

    /**
     * Creates a customer with the specified name.
     *
     * @param name the customer name
     * @return a new customer
     */
    private Customer createCustomer(String name) {
        Party customer = customerFactory.createCustomer("Jo", name);
        return domainService.create(customer, Customer.class);
    }

    /**
     * Create a customer with the specified last name and active flag
     *
     * @param lastName the last name
     * @param active   if {@code true} the customer is active, else it is inactive
     * @return a new customer
     */
    private Customer createCustomer(String lastName, boolean active) {
        Party customer = customerFactory.newCustomer()
                .firstName("Jo")
                .lastName(lastName)
                .active(active)
                .build();
        return domainService.create(customer, Customer.class);
    }
}