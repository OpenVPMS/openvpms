/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.factory;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.CustomerPatients;
import org.openvpms.domain.internal.customer.CustomerImpl;
import org.openvpms.domain.internal.customer.CustomerPatientsImpl;
import org.openvpms.domain.internal.object.RelatedDomainObjects;
import org.openvpms.domain.internal.patient.record.DefaultRecordImpl;
import org.openvpms.domain.internal.patient.record.DocumentRecordDocumentBuilderImpl;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link DomainServiceImpl} class.
 *
 * @author Tim Anderson
 */
public class DomainServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;


    /**
     * Tests the {@link DomainServiceImpl#create(IMObject, Class)} and
     * {@link DomainServiceImpl#create(IMObjectBean, Class)} methods.
     */
    @Test
    public void testCreate() {
        DomainServiceImpl domainService = getDomainService();
        Party party = customerFactory.newCustomer().build(false);
        Customer customer1 = domainService.create(party, Customer.class);
        assertTrue(customer1 instanceof CustomerImpl);

        IMObjectBean bean = getBean(party);
        Customer customer2 = domainService.create(bean, Customer.class);
        assertTrue(customer2 instanceof CustomerImpl);
    }

    /**
     * Tests the {@link DomainServiceImpl#create(IMObject, Class, Class)} method.
     */
    @Test
    public void testCreateWithFallback() {
        DomainServiceImpl domainService = getDomainService();
        Act act = create(PatientArchetypes.CLINICAL_ADDENDUM, Act.class);
        try {
            // when there is no specific class registered, should fail
            domainService.create(act, Record.class);
            fail("Expected create to fail");
        } catch (IllegalArgumentException exception) {
            assertEquals("No conversion from act.patientClinicalAddendum to org.openvpms.domain.patient.record.Record",
                         exception.getMessage());
            // no-op
        }

        // verify the fallback is created
        Record record = domainService.create(act, Record.class, DefaultRecordImpl.class);
        assertTrue(record instanceof DefaultRecordImpl);
    }

    /**
     * Tests the {@link DomainServiceImpl#getBean(IMObject)} method.
     */
    @Test
    public void testBean() {
        DomainServiceImpl domainService = getDomainService();
        Party party = customerFactory.newCustomer().build(false);

        // verify a bean can be created and used to save the party.
        IMObjectBean bean = domainService.getBean(party);
        bean.save();
        assertNotNull(get(party));
    }

    /**
     * Tests the {@link DomainServiceImpl#createBuilder(IMObject, Class)} method.
     */
    @Test
    public void testCreateBuilder() {
        DomainServiceImpl domainService = getDomainService();
        DocumentAct act = patientFactory.newInvestigation().build(false);
        DocumentBuilder builder = domainService.createBuilder(act, DocumentRecordDocumentBuilderImpl.class);
        assertNotNull(builder);
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelated(List, Class)} method.
     */
    @Test
    public void testCreateRelated() {
        // set up an application context with dummy beans that conflict with the relationships argument to the
        // CustomerPatientsImpl constructor. These should not be autowired.
        List<EntityRelationship> conflictingBean1 = new ArrayList<>();
        List<Object> conflictingBean2 = new ArrayList<>();
        DomainServiceImpl domainService = createDomainServiceWithConflictingBeans("conflictingBean1", conflictingBean1,
                                                                                  "conflictingBean2", conflictingBean2);

        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        List<EntityRelationship> relationships = new ArrayList<>(patient.getTargetEntityRelationships());

        CustomerPatients related = domainService.createRelated(relationships, CustomerPatientsImpl.class);
        assertNotNull(related);
        List<EntityRelationship> actual = related.getRelationships();
        assertEquals(1, actual.size());
        assertEquals(relationships.get(0), actual.get(0));

        // verify the parent context hasn't been changed
        try {
            applicationContext.getBean("conflictingBean1");
            fail("Parent context should not have test bean definition");
        } catch (NoSuchBeanDefinitionException expected) {
        }
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelatedObjects(List, Class)} method.
     */
    @Test
    public void testCreateRelatedObjects() {
        // set up an application context with dummy beans that conflict with the relationships argument to the
        // RelatedDomainObjects constructor. These should not be autowired.
        List<EntityRelationship> conflictingBean1 = new ArrayList<>();
        List<Object> conflictingBean2 = new ArrayList<>();
        DomainServiceImpl domainService = createDomainServiceWithConflictingBeans("conflictingBean1", conflictingBean1,
                                                                                  "conflictingBean2", conflictingBean2);
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        List<EntityRelationship> relationships = new ArrayList<>(patient.getTargetEntityRelationships());

        RelatedDomainObjects<Patient, EntityRelationship> related
                = domainService.createRelatedObjects(relationships, Patient.class);
        assertNotNull(related);

        List<EntityRelationship> actual = related.getRelationships();
        assertEquals(1, actual.size());
        assertEquals(relationships.get(0), actual.get(0));

        Patient object = related.getObject();
        assertNotNull(object);
        assertEquals(patient.getId(), object.getId());
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelatedObjects(List, Class, Class)} method.
     */
    @Test
    public void testCreateRelatedObjectsWithFallback() {
        DomainServiceImpl domainService = getDomainService();
        List<ActRelationship> relationships = new ArrayList<>();
        RelatedDomainObjects<Record, ActRelationship> related
                = domainService.createRelatedObjects(relationships, Record.class, DefaultRecordImpl.class);
        assertNotNull(related);
    }

    /**
     * Tests the {@link DomainServiceImpl#get(Reference, Class)} method.
     */
    @Test
    public void testGet() {
        DomainServiceImpl domainService = getDomainService();
        Party active = customerFactory.createCustomer();
        Party inactive = customerFactory.newCustomer().active(false).build();

        // test active retrieval
        assertTrue(domainService.get(active.getObjectReference(), Customer.class) instanceof CustomerImpl);
        assertTrue(domainService.get(active.getObjectReference(), Customer.class, true) instanceof CustomerImpl);
        assertNull(domainService.get(active.getObjectReference(), Customer.class, false));
        assertTrue(domainService.get(active.getArchetype(), active.getId(), Customer.class) instanceof CustomerImpl);
        assertTrue(domainService.get(active.getArchetype(), active.getId(), Customer.class, true)
                           instanceof CustomerImpl);
        assertNull(domainService.get(active.getArchetype(), active.getId(), Customer.class, false));

        // test inactive retrieval
        assertTrue(domainService.get(inactive.getObjectReference(), Customer.class) instanceof CustomerImpl);
        assertNull(domainService.get(inactive.getObjectReference(), Customer.class, true));
        assertTrue(domainService.get(inactive.getObjectReference(), Customer.class, false) instanceof CustomerImpl);
        assertTrue(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class)
                           instanceof CustomerImpl);
        assertNull(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class, true));
        assertTrue(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class, false)
                           instanceof CustomerImpl);
    }

    /**
     * Verifies an {@link IllegalArgumentException} is thrown if no conversion exists when invoking
     * {@link DomainService#create(IMObject, Class)} and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Test
    public void testCreateWithNoConversion() {
        DomainServiceImpl domainService = getDomainService();
        Entity test = laboratoryFactory.createHL7Test(laboratoryFactory.createInvestigationType());
        try {
            domainService.create(test, org.openvpms.domain.laboratory.Test.class);
        } catch (IllegalArgumentException exception) {
            assertEquals("No conversion from entity.laboratoryTestHL7 to org.openvpms.domain.laboratory.Test",
                         exception.getMessage());
        }

        try {
            domainService.create(getBean(test), org.openvpms.domain.laboratory.Test.class);
        } catch (IllegalArgumentException exception) {
            assertEquals("No conversion from entity.laboratoryTestHL7 to org.openvpms.domain.laboratory.Test",
                         exception.getMessage());
        }
    }

    /**
     * Verifies an {@link IllegalArgumentException} is thrown if no conversion exists when invoking
     * {@link DomainService#get(Reference, Class)}.
     */
    @Test
    public void testGetWithNoConversion() {
        DomainServiceImpl domainService = getDomainService();
        Entity test = laboratoryFactory.createHL7Test(laboratoryFactory.createInvestigationType());
        try {
            domainService.get(test.getObjectReference(), org.openvpms.domain.laboratory.Test.class);
        } catch (IllegalArgumentException exception) {
            assertEquals("No conversion from entity.laboratoryTestHL7 to org.openvpms.domain.laboratory.Test",
                         exception.getMessage());
        }
    }

    /**
     * Creates a {@link DomainServiceImpl} that uses an application context containing beans that conflict with the
     * argument to be passed to the domain object constructor. These should not be used.
     *
     * @return a new {@link DomainServiceImpl}
     */
    private DomainServiceImpl createDomainServiceWithConflictingBeans(String bean1Name, Object bean1,
                                                                      String bean2Name, Object bean2) {
        GenericApplicationContext testApplicationContext = new GenericApplicationContext(applicationContext);
        ConfigurableListableBeanFactory beanFactory
                = ((ConfigurableApplicationContext) testApplicationContext).getBeanFactory();
        beanFactory.registerSingleton(bean1Name, bean1);
        beanFactory.registerSingleton(bean2Name, bean2);
        testApplicationContext.refresh();

        assertNotNull(testApplicationContext.getBean(bean1Name));
        assertNotNull(testApplicationContext.getBean(bean2Name));

        return getDomainService(testApplicationContext);
    }

    /**
     * Creates a {@link DomainServiceImpl} using the default application context.
     *
     * @return a new domain service
     */
    private DomainServiceImpl getDomainService() {
        return getDomainService(applicationContext);
    }

    /**
     * Creates a {@link DomainServiceImpl} using the specified application context.
     *
     * @return a new domain service
     */
    private DomainServiceImpl getDomainService(ApplicationContext context) {
        DomainServiceImpl result = new DomainServiceImpl(getArchetypeService());
        result.setApplicationContext(context);
        return result;
    }
}