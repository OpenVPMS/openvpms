/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.Patient.Sex;
import org.openvpms.domain.service.patient.PatientBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientBuilderImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The patient services.
     */
    @Autowired
    private PatientServices patientServices;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Tests creating a new patient.
     */
    @Test
    public void testNewPatient() {
        Lookup species = lookupFactory.getSpecies("CANINE");
        Lookup breed = lookupFactory.getBreed("KELPIE");
        LocalDate dateOfBirth = DateRules.toLocalDate(TestHelper.getDate("2020-01-01"));
        LocalDate dateOfDeath = DateRules.toLocalDate(TestHelper.getDate("2023-01-01"));
        Customer customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient = getBuilder().name("Fido")
                .species(species)
                .breed(breed)
                .sex(Sex.FEMALE)
                .desexed(true)
                .dateOfBirth(dateOfBirth)
                .deceased(true)
                .dateOfDeath(dateOfDeath)
                .colour("Black")
                .owner(customer)
                .microchip("123456")
                .active(false)
                .build();

        assertEquals("Fido", patient.getName());
        assertEquals(species.getCode(), patient.getSpeciesCode());
        assertEquals(breed.getCode(), patient.getBreedCode());
        assertEquals(Sex.FEMALE, patient.getSex());
        assertTrue(patient.isDesexed());
        assertEquals(dateOfBirth, patient.getDateOfBirth());
        assertTrue(patient.isDeceased());
        assertEquals(dateOfDeath, patient.getDateOfDeath());
        assertEquals("Black", patient.getColourName());
        assertEquals(customer, patient.getOwner());
        assertEquals("123456", patient.getMicrochip().getIdentity());
        assertFalse(patient.isActive());
    }

    /**
     * Tests updating a new patient.
     */
    @Test
    public void testUpdatePatient() {
        Lookup species1 = lookupFactory.getSpecies("CANINE");
        Lookup breed1 = lookupFactory.getBreed("KELPIE");
        Lookup species2 = lookupFactory.getSpecies("FELINE");
        Lookup breed2 = lookupFactory.getBreed("BIRMAN");
        LocalDate dateOfBirth1 = DateRules.toLocalDate(TestHelper.getDate("2020-01-01"));
        LocalDate dateOfDeath1 = DateRules.toLocalDate(TestHelper.getDate("2023-01-01"));
        LocalDate dateOfBirth2 = DateRules.toLocalDate(TestHelper.getDate("2020-01-02"));
        Customer customer1 = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customer2 = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient1 = getBuilder().name("Fido")
                .species(species1)
                .breed(breed1)
                .sex(Sex.FEMALE)
                .desexed(true)
                .dateOfBirth(dateOfBirth1)
                .deceased(true)
                .dateOfDeath(dateOfDeath1)
                .colour("Black")
                .owner(customer1)
                .microchip("123456")
                .active(false)
                .build();

        getBuilder(patient1).name("Milo")
                .species(species2)
                .breed(breed2)
                .sex(Sex.MALE)
                .desexed(false)
                .dateOfBirth(dateOfBirth2)
                .deceased(false)
                .dateOfDeath(null)
                .colour("Brown")
                .owner(customer2)
                .microchip("987654")
                .active(true)
                .build();

        Patient patient2 = reload(patient1);
        assertEquals("Milo", patient2.getName());
        assertEquals(species2.getCode(), patient2.getSpeciesCode());
        assertEquals(breed2.getCode(), patient2.getBreedCode());
        assertEquals(Sex.MALE, patient2.getSex());
        assertFalse(patient2.isDesexed());
        assertEquals(dateOfBirth2, patient2.getDateOfBirth());
        assertFalse(patient2.isDeceased());
        assertEquals("Brown", patient2.getColourName());
        assertEquals(customer2, patient2.getOwner());
        assertEquals("987654", patient2.getMicrochip().getIdentity());
        assertTrue(patient2.isActive());
    }

    /**
     * Tests patient ownership.
     */
    @Test
    public void testChangeOwnership() {
        Lookup species = lookupFactory.getSpecies("CANINE");
        Lookup breed = lookupFactory.getBreed("KELPIE");
        Customer customer1 = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customer2 = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient1 = getBuilder().name("Fido")
                .species(species)
                .breed(breed)
                .build();
        assertNull(patient1.getOwner());

        getBuilder(patient1)
                .owner(customer1)
                .build();
        assertEquals(customer1, patient1.getOwner());

        // don't update the owner and verify it is unchanged
        getBuilder(patient1)
                .build();
        assertEquals(customer1, patient1.getOwner());

        getBuilder(patient1)
                .owner(customer2)
                .build();
        assertEquals(customer2, patient1.getOwner());

        getBuilder(patient1)
                .owner(null)
                .build();
        assertNull(patient1.getOwner());

        Patient patient2 = reload(patient1);
        assertNull(patient2.getOwner());
        List<EntityRelationship> relationships = patient2.getEntityRelationships()
                .stream()
                .filter(relationship -> relationship.isA(PatientArchetypes.PATIENT_OWNER))
                .collect(Collectors.toList());
        assertEquals(2, relationships.size());
    }

    /**
     * Verifies the microchip can be changed.
     */
    @Test
    public void testChangeMicrochip() {
        Lookup species = lookupFactory.getSpecies("CANINE");
        Lookup breed = lookupFactory.getBreed("KELPIE");
        Patient patient1 = getBuilder().name("Fido")
                .species(species)
                .breed(breed)
                .build();
        assertNull(patient1.getMicrochip());

        getBuilder(patient1)
                .microchip("1")
                .build();
        assertEquals("1", patient1.getMicrochip().getIdentity());

        // don't update the microchip and verify it is unchanged
        getBuilder(patient1)
                .build();
        assertEquals("1", patient1.getMicrochip().getIdentity());

        getBuilder(patient1)
                .microchip("2")
                .build();
        assertEquals("2", patient1.getMicrochip().getIdentity());

        getBuilder(patient1)
                .microchip(null)
                .build();
        assertNull(patient1.getMicrochip());

        Patient patient2 = reload(patient1);
        assertNull(patient2.getMicrochip());
    }

    /**
     * Returns a builder to create new patients.
     *
     * @return a builder
     */
    private PatientBuilder getBuilder() {
        return new PatientBuilderImpl(patientServices);
    }

    /**
     * Returns a builder to update a patient.
     *
     * @param patient the patient
     * @return a builder
     */
    private PatientBuilder getBuilder(Party patient) {
        return new PatientBuilderImpl(patient, patientServices);
    }

    /**
     * Reloads a patient.
     *
     * @param patient the patient
     * @return the reloaded patient
     */
    private Patient reload(Patient patient) {
        return domainService.create(get(patient.getObjectReference()), Patient.class);
    }
}