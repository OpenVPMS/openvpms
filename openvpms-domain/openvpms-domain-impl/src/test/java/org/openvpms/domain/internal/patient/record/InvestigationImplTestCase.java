/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.DocumentRecord;
import org.openvpms.domain.patient.record.DocumentVersion;
import org.openvpms.domain.patient.record.Investigation;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link InvestigationImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class InvestigationImplTestCase extends ArchetypeServiceTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The test investigation.
     */
    private Investigation investigation;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party patient = patientFactory.createPatient();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct act = patientFactory.newInvestigation().patient(patient).investigationType(investigationType)
                .build();
        investigation = domainService.create(act, Investigation.class);
    }

    /**
     * Tests adding of a single document to an investigation.
     *
     * @throws IOException for any error
     */
    @Test
    public void testAddDocument() throws IOException {
        assertNull(investigation.getDocument());
        investigation.getDocumentBuilder().fileName("some.txt").mimeType("text/plain").content(
                getStream("some text")).build();
        checkDocument("some.txt", "some text", investigation);
    }

    /**
     * Verifies that by default, documents are versioned.
     *
     * @throws IOException for any error
     */
    @Test
    public void testDefaultVersioning() throws IOException {
        assertNull(investigation.getDocument());
        assertEquals(0, investigation.getVersions().getRelationships().size());

        Document document1 = investigation.getDocumentBuilder().fileName("doc1.txt").mimeType("text/plain").content(
                getStream("doc 1")).build();

        Document document2 = investigation.getDocumentBuilder().fileName("doc2.txt").mimeType("text/plain").content(
                getStream("doc 2")).build();

        Document document3 = investigation.getDocumentBuilder().fileName("doc3.txt").mimeType("text/plain").content(
                getStream("doc 3")).build();

        checkDocument(document3, "doc3.txt", "doc 3", investigation);

        List<DocumentVersion> versions = new ArrayList<>();
        investigation.getVersions().getObjects().forEach(versions::add);
        assertEquals(2, versions.size());
        versions.sort(Comparator.comparingLong(IMObject::getId));
        checkDocument(document1, "doc1.txt", "doc 1", versions.get(0));
        checkDocument(document2, "doc2.txt", "doc 2", versions.get(1));
    }

    /**
     * Verifies that when a document is added with versioning disabled, it replaces the existing document.
     *
     * @throws IOException for any I/O error
     */
    @Test
    public void testVersioningDisabled() throws IOException {
        assertNull(investigation.getDocument());
        assertEquals(0, investigation.getVersions().getRelationships().size());

        Document document1 = investigation.getDocumentBuilder().fileName("doc1.txt").mimeType("text/plain").content(
                getStream("doc 1")).build();
        Document document2 = investigation.getDocumentBuilder().fileName("doc2.txt").mimeType("text/plain").content(
                        getStream("doc 2"))
                .version(false).build();

        checkDocument(document2, "doc2.txt", "doc 2", investigation);

        assertEquals(0, investigation.getVersions().getRelationships().size()); // verify there are no versions
        assertNull(get(document1)); // verify document1 has been removed
    }

    /**
     * Verifies that if versioning is turned off, but an existing document is protected, it is versioned.
     */
    @Test
    public void testVersionProtectedDocument() throws IOException {
        Document document1 = investigation.getDocumentBuilder().fileName("doc1.txt").mimeType("text/plain").content(
                getStream("doc 1")).build();
        IMObjectBean bean = getBean(investigation);

        // mark the current document as protected
        bean.setValue(DocumentRecordDocumentBuilderImpl.PROTECTED_DOCUMENT, true);
        bean.save();

        Document document2 = investigation.getDocumentBuilder().fileName("doc2.txt").mimeType("text/plain").content(
                getStream("doc 2")).version(false).build();

        checkDocument(document2, "doc2.txt", "doc 2", investigation);

        // verify document1 has been preserved
        List<DocumentVersion> versions = new ArrayList<>();
        investigation.getVersions().getObjects().forEach(versions::add);
        assertEquals(1, versions.size());
        checkDocument(document1, "doc1.txt", "doc 1", versions.get(0));

        // protectedDocument flag should be reset
        assertFalse(bean.getBoolean(DocumentRecordDocumentBuilderImpl.PROTECTED_DOCUMENT));
    }

    /**
     * Verifies a document matches that expected.
     *
     * @param fileName the document file name
     * @param content  the content
     * @param actual   the actual document record
     * @throws IOException for any I/O error
     */
    private void checkDocument(String fileName, String content, DocumentRecord actual) throws IOException {
        checkDocument(null, fileName, content, actual);
    }

    /**
     * Verifies a document matches that expected, and the file name and mime type have been duplicated on the act.
     *
     * @param fileName the document file name
     * @param content  the content
     * @param actual   the actual document record
     * @throws IOException for any I/O error
     */
    private void checkDocument(Document expected, String fileName, String content, DocumentRecord actual)
            throws IOException {
        Document document = actual.getDocument();
        if (expected != null) {
            assertEquals(expected, document);
        } else {
            assertNotNull(document);
        }
        assertEquals(fileName, document.getName());
        assertEquals("text/plain", document.getMimeType());
        assertEquals(content, IOUtils.toString(document.getContent(), StandardCharsets.UTF_8));

        // verify the file name and mime type are duplicated on the record. This is for performance reasons.
        IMObjectBean bean = getBean(actual);
        assertEquals(fileName, bean.getString("fileName"));
        assertEquals("text/plain", bean.getString("mimeType"));
    }

    /**
     * Returns a stream from some text.
     *
     * @param text the text
     * @return a stream to read the text
     */
    private InputStream getStream(String text) {
        return IOUtils.toInputStream(text, StandardCharsets.UTF_8);
    }
}
