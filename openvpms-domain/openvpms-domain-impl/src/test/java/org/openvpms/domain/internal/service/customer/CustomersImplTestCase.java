/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link CustomersImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CustomersImplTestCase extends ArchetypeServiceTest {

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules rules;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainObjectService;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The Customers implementation.
     */
    private CustomersImpl customers;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        customers = new CustomersImpl(rules, getArchetypeService(), domainObjectService, getLookupService());
    }

    /**
     * Tests the {@link CustomersImpl#getCustomer(long)} method.
     */
    @Test
    public void testGetCustomer() {
        Party party = customerFactory.createCustomer();

        Customer customer1 = customers.getCustomer(party.getId());
        assertNotNull(customer1);
        assertEquals(party, customer1);

        // now check a customer that doesn't exist
        Customer customer2 = customers.getCustomer(0);
        assertNull(customer2);
    }

    /**
     * Tests the {@link CustomersImpl#getCustomerBuilder()} method.
     */
    @Test
    public void testBuildNewCustomer() {
        Customer customer = customers.getCustomerBuilder()
                .firstName("J")
                .lastName("Smith")
                .build();
        assertEquals("J", customer.getFirstName());
        assertEquals("Smith", customer.getLastName());
    }

    /**
     * Tests the {@link CustomersImpl#getCustomerBuilder(Customer)} method.
     */
    @Test
    public void testUpdateCustomer() {
        Customer customer = customers.getCustomerBuilder()
                .firstName("J")
                .lastName("Smith")
                .build();

        customers.getCustomerBuilder(customer)
                .firstName("Jo")
                .lastName("Smyth")
                .build();
        assertEquals("Jo", customer.getFirstName());
        assertEquals("Smyth", customer.getLastName());
    }
}
