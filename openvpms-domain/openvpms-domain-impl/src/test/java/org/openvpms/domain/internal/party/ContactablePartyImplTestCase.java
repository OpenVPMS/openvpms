/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.party;

import org.junit.Test;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.party.TestEmailContactBuilder;
import org.openvpms.archetype.test.builder.party.TestLocationContactBuilder;
import org.openvpms.archetype.test.builder.party.TestPhoneContactBuilder;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.ContactableParty;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link ContactablePartyImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ContactablePartyImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules rules;


    /**
     * Verifies that the {@link ContactableParty#getContacts()} method returns {@link Address},{@link Email} and
     * {@link Phone} contacts
     */
    @Test
    public void testGetContacts() {
        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addPhone("12345678")
                .addMobilePhone("0412345678")
                .addWebsite("http://foo.com")
                .build(false);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);

        Set<Contact> contacts = party.getContacts();
        assertEquals(5, contacts.size());
        for (Contact contact : contacts) {
            if (contact.isA(Email.ARCHETYPE)) {
                assertTrue(contact instanceof Email);
            } else if (contact.isA(Address.ARCHETYPE)) {
                assertTrue(contact instanceof Address);
            } else if (contact.isA(Phone.ARCHETYPE)) {
                assertTrue(contact instanceof Phone);
            } else if (contact.isA(ContactArchetypes.WEBSITE)) {
                assertTrue(contact instanceof org.openvpms.component.business.domain.im.party.Contact);
                // no wrapper yet
            } else {
                fail("Unknown contact archetype " + contact.getArchetype());
            }
        }
    }

    /**
     * Tests the {@link ContactableParty#getAddress()} method.
     */
    @Test
    public void testGetAddress() {
        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addPhone("12345678")
                .addMobilePhone("0412345678")
                .addWebsite("http://foo.com")
                .build(false);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);
        Address address = party.getAddress();
        assertNotNull(address);

        assertEquals("1 Smith St", address.getAddress());
        assertEquals("SALE", address.getSuburbCode());
        assertEquals("VIC", address.getStateCode());
        assertEquals("3085", address.getPostcode());
    }

    /**
     * Tests the {@link ContactableParty#getAddresses()} method.
     */
    @Test
    public void testGetAddresses() {
        TestLocationContactBuilder<?, ?> builder = new TestLocationContactBuilder<>(getArchetypeService());
        Contact contact1 = builder.address("Address 1").suburbCode("SALE").stateCode("VIC").postcode("3085").build();
        Contact contact2 = builder.address("Address 2").build();

        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addContact(contact1)
                .build();
        customer.addContact(contact2);  // to ensure it gets a higher id
        save(customer);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);

        // addresses are sorted on increasing identifier
        List<Address> addresses = party.getAddresses();
        assertEquals(2, addresses.size());
        assertEquals("Address 1", addresses.get(0).getAddress());
        assertEquals("Address 2", addresses.get(1).getAddress());
    }

    /**
     * Tests the {@link ContactableParty#getPhone()} method.
     */
    @Test
    public void testGetPhone() {
        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addPhone("12345678")
                .addWebsite("http://foo.com")
                .build(false);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);
        Phone phone = party.getPhone();
        assertNotNull(phone);

        assertEquals("12345678", phone.getPhoneNumber());
    }

    /**
     * Tests the {@link ContactableParty#getPhones()} method.
     */
    @Test
    public void testGetPhones() {
        TestPhoneContactBuilder<?, ?> builder = new TestPhoneContactBuilder<>(getArchetypeService());
        Contact contact1 = builder.phone("12345678").build();
        Contact contact2 = builder.phone("04987654").purposes(ContactArchetypes.MOBILE_PURPOSE).build();
        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addContact(contact1)
                .addWebsite("http://foo.com")
                .build();
        customer.addContact(contact2); // to ensure it gets assigned a higher id
        save(customer);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);

        List<Phone> phones = party.getPhones();
        assertEquals(2, phones.size());

        Phone phone1 = phones.get(0);
        assertEquals("12345678", phone1.getPhoneNumber());
        assertFalse(phone1.isMobile());
        assertFalse(phone1.isFax());

        Phone phone2 = phones.get(1);
        assertEquals("04987654", phone2.getPhoneNumber());
        assertTrue(phone2.isMobile());
        assertFalse(phone2.isFax());
    }

    /**
     * Tests the {@link ContactableParty#getEmail()} method.
     */
    @Test
    public void testGetEmail() {
        Party customer = customerFactory.newCustomer()
                .addEmail("foo@bar.com")
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addPhone("12345678")
                .addMobilePhone("0412345678")
                .addWebsite("http://foo.com")
                .build(false);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);
        Email email = party.getEmail();
        assertNotNull(email);

        assertEquals("foo@bar.com", email.getEmailAddress());
    }

    /**
     * Tests the {@link ContactableParty#getEmails()}.
     */
    @Test
    public void testGetEmails() {
        TestEmailContactBuilder<?, ?> builder = new TestEmailContactBuilder<>(getArchetypeService());
        Contact contact1 = builder.email("foo@bar.com").build();
        Contact contact2 = builder.email("foo@gmail.com").build();

        Party customer = customerFactory.newCustomer()
                .addContact(contact1)
                .addAddress("1 Smith St", "SALE", "VIC", "3085")
                .addPhone("12345678")
                .addMobilePhone("0412345678")
                .addWebsite("http://foo.com")
                .build();
        customer.addContact(contact2);  // to ensure it gets a higher id
        save(customer);
        ContactableParty party = new ContactablePartyImpl(customer, rules, domainService);

        // contacts are sorted on increasing identifier
        List<Email> emails = party.getEmails();
        assertEquals(2, emails.size());
        assertEquals("foo@bar.com", emails.get(0).getEmailAddress());
        assertEquals("foo@gmail.com", emails.get(1).getEmailAddress());
    }

}
