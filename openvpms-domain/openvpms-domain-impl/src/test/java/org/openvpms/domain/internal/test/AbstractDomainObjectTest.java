/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.test;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.object.DomainObjectService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base class for domain object tests.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractDomainObjectTest extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Verifies the domain implementation of an object has provided equals and hashCode methods.
     */
    @Test
    public abstract void testEquality();

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Test
    public abstract void testFactory();

    /**
     * Verifies that a domain implementation provides equals() and hashCode() methods.
     *
     * @param object1         the first object
     * @param object2         the second object
     * @param domainInterface the domain interface
     */
    protected void checkEquality(IMObject object1, IMObject object2, Class<?> domainInterface) {
        Object domain1A = domainService.create(object1, domainInterface);
        Object domain1B = domainService.create(object1, domainInterface);

        Object domain2 = domainService.create(object2, domainInterface);

        assertEquals(domain1A, domain1B);
        assertEquals(domain1A.hashCode(), domain1B.hashCode());

        assertNotEquals(domain1A, domain2);
        assertNotEquals(domain1A.hashCode(), domain2.hashCode());
    }

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     *
     * @param object          the model object
     * @param domainInterface the domain interface
     * @param domainImpl      the domain implementation
     */
    protected <T, I extends T> void checkFactory(IMObject object, Class<T> domainInterface, Class<I> domainImpl) {
        Object object1 = domainService.create(object, domainInterface);
        assertTrue(domainInterface.isAssignableFrom(object1.getClass()));
        assertEquals(domainImpl, object1.getClass());

        IMObjectBean bean = getBean(object);
        Object object2 = domainService.create(bean, domainInterface);
        assertTrue(domainInterface.isAssignableFrom(object2.getClass()));
        assertEquals(domainImpl, object2.getClass());
    }

    /**
     * Returns the domain service.
     *
     * @return the domain service
     */
    protected DomainService getDomainService() {
        return domainService;
    }

}
