/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.practice;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.practice.Practice;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PracticeImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PracticeImplTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Verifies that the {@link DomainService} can create practices.
     */
    @Test
    public void testFactory() {
        Party party = practiceFactory.newPractice().build(false);
        Practice practice = domainService.create(party, Practice.class);
        assertTrue(practice instanceof PracticeImpl);
    }

    /**
     * Verifies that {@link Practice} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party location1Party = practiceFactory.newLocation()
                .name("Location 1")
                .build();
        Party location2Party = practiceFactory.newLocation()
                .name("Location 2")
                .build();
        Party party = practiceFactory.newPractice()
                .name("Vets R Us")
                .locations(location1Party, location2Party)
                .addAddress("101 Broadwater Ave", "CAPE_WOOLAMAI", "VIC", "1111")
                .addPhone("123456")
                .addEmail("foo@bar.com")
                .build(false);
        Practice practice = domainService.create(party, Practice.class);
        assertEquals("Vets R Us", practice.getName());
        Address address = practice.getAddress();
        assertNotNull(address);
        assertEquals("101 Broadwater Ave", address.getAddress());
        assertEquals("Cape Woolamai", address.getSuburbName());
        assertEquals("VIC", address.getStateCode());
        Phone phone = practice.getPhone();
        assertNotNull(phone);
        assertEquals("123456", phone.getPhoneNumber());
        Email email = practice.getEmail();
        assertNotNull(email);
        ;
        assertEquals("foo@bar.com", email.getEmailAddress());

        List<Location> locations = practice.getLocations();
        assertEquals(2, locations.size());
    }
}
