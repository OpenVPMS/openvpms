/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.alert.Allergy;
import org.openvpms.domain.patient.referral.Referral;
import org.openvpms.domain.patient.referral.Referrals;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link PatientImpl} class.
 *
 * @author Tim Anderson
 */
public class PatientImplTestCase extends AbstractDomainObjectTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    /**
     * Verifies that the {@link DomainService} can create patients.
     */
    @Test
    public void testFactory() {
        Party party = patientFactory.newPatient().build(false);
        checkFactory(party, Patient.class, PatientImpl.class);
    }

    /**
     * Verifies a domain implementation of a product has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Party party1 = patientFactory.newPatient().build(false);
        Party party2 = patientFactory.newPatient().build(false);
        checkEquality(party1, party2, Patient.class);
    }

    /**
     * Verifies that the {@link PatientImpl} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party customer = customerFactory.createCustomer();
        Date dateOfBirth = getDate("2013-01-01");
        Date dateOfDeath = getDate("2019-01-01");
        Party party = patientFactory.newPatient()
                .dateOfBirth(dateOfBirth)
                .deceased(true)
                .dateOfDeath(dateOfDeath)
                .species("CANINE")
                .breed("DINGO")
                .female()
                .desexed(true)
                .colour("Tan")
                .addMicrochip("123456789")
                .owner(customer)
                .build();
        patientFactory.createWeight(party, BigDecimal.TEN);
        Patient patient = getDomainService().create(party, Patient.class);
        assertEquals(dateOfBirth, DateRules.toDate(patient.getDateOfBirth()));
        assertEquals(dateOfDeath, DateRules.toDate(patient.getDateOfDeath()));
        assertTrue(patient.isDeceased());
        Microchip microchip = patient.getMicrochip();
        assertNotNull(microchip);
        assertEquals("123456789", microchip.getIdentity());
        assertEquals("CANINE", patient.getSpeciesCode());
        assertEquals("DINGO", patient.getBreedCode());
        assertEquals(Patient.Sex.FEMALE, patient.getSex());
        assertTrue(patient.isDesexed());
        assertEquals("Tan", patient.getColourName());
        assertEquals(customer.getId(), patient.getOwner().getId());
        Weight weight = patient.getWeight();
        assertNotNull(weight);
        assertEquals(BigDecimal.TEN, weight.getWeight());
        assertEquals(WeightUnits.KILOGRAMS, weight.getUnits());
    }

    /**
     * Tests the {@link PatientImpl#getBreedName()}, {@link PatientImpl#getBreedCode()} and
     * {@link PatientImpl#getBreedLookup()} methods.
     */
    @Test
    public void testBreed() {
        Party party1 = patientFactory.newPatient()
                .species("CANINE")
                .breed("KELPIE")
                .build();
        Patient patient1 = getDomainService().create(party1, Patient.class);
        assertEquals("Kelpie", patient1.getBreedName());
        assertEquals("KELPIE", patient1.getBreedCode());
        assertNotNull(patient1.getBreedLookup());
        assertEquals("KELPIE", patient1.getBreedLookup().getCode());
        assertEquals("Kelpie", patient1.getBreedLookup().getName());

        Party party2 = patientFactory.newPatient()
                .species("CANINE")
                .build();
        Patient patient2 = getDomainService().create(party2, Patient.class);
        assertNull(patient2.getBreedName());
        assertNull(patient2.getBreedCode());
        assertNull(patient2.getBreedLookup());


        // no breed - the newBreed name should be returned by getBreedName()
        Party party3 = patientFactory.newPatient()
                .species("CANINE")
                .newBreed("Bitzer")
                .build();
        Patient patient3 = getDomainService().create(party3, Patient.class);
        assertEquals("Bitzer", patient3.getBreedName());
        assertNull(patient3.getBreedCode());
        assertNull(patient3.getBreedLookup());


        // both breed and newBreed specified - newBreed name should be ignored
        Party party4 = patientFactory.newPatient()
                .species("CANINE")
                .breed("BOXER")
                .newBreed("Boxa")
                .build();
        Patient patient4 = getDomainService().create(party4, Patient.class);
        assertEquals("Boxer", patient4.getBreedName());
        assertEquals("BOXER", patient4.getBreedCode());
        assertNotNull(patient4.getBreedLookup());
        assertEquals("BOXER", patient4.getBreedLookup().getCode());
        assertEquals("Boxer", patient4.getBreedLookup().getName());
    }

    /**
     * Tests the {@link PatientImpl#getAge()} method and {@link PatientImpl#getAge(LocalDate)} methods.
     */
    @Test
    public void testGetAge() {
        // patient with no date of birth recorded
        Party party1 = patientFactory.newPatient().build(false);
        Patient patient1 = getDomainService().create(party1, Patient.class);
        assertNull(patient1.getAge());
        assertNull(patient1.getAge(LocalDate.now()));

        // patient with date of birth
        Party party2 = patientFactory.newPatient().dateOfBirth(getDate("2013-01-01")).build(false);
        Patient patient2 = getDomainService().create(party2, Patient.class);
        long years = patient2.getDateOfBirth().until(LocalDate.now(), ChronoUnit.YEARS);
        assertTrue(patient2.getAge().startsWith(years + " Years"));
        assertEquals("5 Years", patient2.getAge(LocalDate.of(2018, 1, 1)));

        // patient with date of birth, date of death
        Party party3 = patientFactory.newPatient().dateOfBirth(getDate("2013-01-01"))
                .deceased(true).dateOfDeath(getDate("2017-01-01")).build(false);
        Patient patient3 = getDomainService().create(party3, Patient.class);
        assertEquals("4 Years", patient3.getAge());
        assertEquals("3 Years", patient2.getAge(LocalDate.of(2016, 1, 1)));
        assertEquals("4 Years", patient3.getAge(LocalDate.now()));

        // patient with date of death
        Party party4 = patientFactory.newPatient()
                .deceased(true).dateOfDeath(getDate("2017-01-01")).build(false);
        Patient patient4 = getDomainService().create(party4, Patient.class);
        assertNull(patient4.getAge());
        assertNull(patient4.getAge(LocalDate.now()));
    }

    /**
     * Tests the {@link Patient#isAggressive()} method.
     */
    @Test
    public void testIsAggressive() {
        Party party = patientFactory.createPatient();
        Patient patient = getDomainService().create(party, Patient.class);
        assertFalse(patient.isAggressive());

        Entity type = patientFactory.newAlertType()
                .alertType(PatientRules.AGGRESSION_ALERT_TYPE)
                .build();
        patientFactory.newAlert()
                .patient(patient)
                .alertType(type)
                .build();

        assertTrue(patient.isAggressive());
    }

    /**
     * Tests the {@link Patient#getAllergies()} method.
     */
    @Test
    public void testAllergies() {
        Party party = patientFactory.createPatient();
        Patient patient = getDomainService().create(party, Patient.class);
        User clinician1 = userFactory.createClinician();
        User clinician2 = userFactory.createClinician();
        assertFalse(patient.getAllergies().getCurrent().iterator().hasNext());

        Entity type = patientFactory.newAlertType()
                .alertType(PatientRules.ALLERGY_ALERT_TYPE)
                .build();
        patientFactory.newAlert()
                .startTime("2020-01-01")
                .patient(patient)
                .alertType(type)
                .reason("allergy 1")
                .notes("notes 1")
                .clinician(clinician1)
                .build();
        patientFactory.newAlert()
                .startTime("2021-06-1")
                .patient(patient)
                .alertType(type)
                .reason("allergy 2")
                .notes("notes 2")
                .clinician(clinician2)
                .build();

        List<Allergy> allergies = new ArrayList<>();
        CollectionUtils.addAll(allergies, patient.getAllergies().getCurrent().iterator());
        assertEquals(2, allergies.size());
        checkAllergy(allergies.get(0), "2020-01-01", "allergy 1", "notes 1", clinician1);
        checkAllergy(allergies.get(1), "2021-06-01", "allergy 2", "notes 2", clinician2);
    }

    /**
     * Tests the {@link Patient#getReferrals()} methods.
     */
    @Test
    public void testReferrals() {
        Party party = patientFactory.createPatient();
        Patient patient = getDomainService().create(party, Patient.class);

        Referrals referrals = patient.getReferrals();
        assertNull(referrals.getCurrent());
        assertNull(referrals.getReferredFrom());
        assertNull(referrals.getReferredTo());
        assertEquals(0, referrals.getAll().size());

        Party practice1 = supplierFactory.createVetPractice();
        Party practice2 = supplierFactory.createVetPractice();
        Party practice3 = supplierFactory.createVetPractice();
        Party vet1 = supplierFactory.newVet().addPractice(practice1).build();
        Party vet2 = supplierFactory.newVet().addPractice(practice2).build();
        Party vet3 = supplierFactory.newVet().addPractice(practice3).build();

        Date third = DateRules.getToday();
        Date first = DateRules.getDate(third, -2, DateUnits.MONTHS);
        Date second = DateRules.getDate(third, -1, DateUnits.DAYS);
        patientFactory.updatePatient(patient)
                .addReferredFrom(vet1, first, "reason 1")
                .addReferredFrom(vet2, second, "reason 2")
                .addReferredTo(vet3, third, "reason 3")
                .build();

        checkReferral(referrals.getCurrent(), practice3, vet3, third, true, false, "reason 3");

        checkReferral(referrals.getReferredFrom(), practice2, vet2, second, false, true, "reason 2");
        assertNull(referrals.getReferredFrom(true));
        checkReferral(referrals.getReferredFrom(false), practice2, vet2, second, false, true, "reason 2");

        checkReferral(referrals.getReferredTo(), practice3, vet3, third, true, false, "reason 3");
        checkReferral(referrals.getReferredTo(true), practice3, vet3, third, true, false, "reason 3");
        checkReferral(referrals.getReferredTo(false), practice3, vet3, third, true, false, "reason 3");

        assertNull(referrals.getReferral(DateRules.toOffsetDateTime(first).minusDays(1)));
        checkReferral(referrals.getReferral(DateRules.toOffsetDateTime(first)), practice1, vet1, first, false, true,
                      "reason 1");
        checkReferral(referrals.getReferral(DateRules.toOffsetDateTime(second)), practice2, vet2, second, false, true,
                      "reason 2");
        checkReferral(referrals.getReferral(DateRules.toOffsetDateTime(third)), practice3, vet3, third, true, false,
                      "reason 3");
        checkReferral(referrals.getReferral(DateRules.toOffsetDateTime(third).plusDays(1)), practice3, vet3, third,
                      true, false, "reason 3");

        assertEquals(3, referrals.getAll().size());
        checkReferral(referrals.getAll().get(0), practice3, vet3, third, true, false, "reason 3");
        checkReferral(referrals.getAll().get(1), practice2, vet2, second, false, true, "reason 2");
        checkReferral(referrals.getAll().get(2), practice1, vet1, first, false, true, "reason 1");
    }

    /**
     * Verifies an allergy matches that expected.
     *
     * @param actual    the actual allergy
     * @param date      the expected date
     * @param allergy   the expected allergy
     * @param notes     the expected notes
     * @param clinician the expected clinician
     */
    private void checkAllergy(Allergy actual, String date, String allergy, String notes, User clinician) {
        assertEquals(DateRules.toOffsetDateTime(TestHelper.getDate(date)), actual.getDate());
        assertEquals(allergy, actual.getAllergy());
        assertEquals(notes, actual.getNotes());
        assertEquals(clinician, actual.getClinician());
    }

    /**
     * Verifies a referral matches that expected.
     *
     * @param referral the referral
     * @param practice the expected practice
     * @param vet      the expected vet
     * @param date     the expected date
     * @param active   the expected active flag
     * @param from     if {@code true}, the patient has been referred from the practice, else it has been referred to it
     * @param reason   the expected reason
     */
    private void checkReferral(Referral referral, Party practice, Party vet, Date date, boolean active, boolean from,
                               String reason) {
        assertNotNull(referral);
        assertEquals(practice, referral.getPractice());
        assertEquals(vet, referral.getVet());
        assertEquals(date, DateRules.toDate(referral.getDate()));
        assertEquals(active, referral.isActive());
        assertEquals(from, referral.referredFrom());
        assertEquals(!from, referral.referredTo());
        assertEquals(reason, referral.getReason());
    }

}
