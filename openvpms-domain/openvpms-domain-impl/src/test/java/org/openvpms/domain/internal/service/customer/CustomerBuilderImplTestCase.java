/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.service.customer.CustomerBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link CustomerBuilderImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class CustomerBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules rules;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainObjectService;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests creating a new customer.
     */
    @Test
    public void testNewCustomer() {
        lookupFactory.getLookup(CustomerArchetypes.TITLE, "MS");
        Lookup suburb = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup vic = lookupFactory.getLookup(ContactArchetypes.STATE, "VIC", "Victoria");
        Location location = domainObjectService.create(practiceFactory.createLocation(), Location.class);
        Customer customer = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .address("123 Smith St", suburb.getCode(), "3070", vic.getCode())
                .homePhone("11111111")
                .workPhone("22222222")
                .mobilePhone("33333333")
                .email("foo@bar.com")
                .practice(location)
                .active(true)
                .build();

        assertNotEquals(customer.getId(), -1); // must be saved
        assertEquals("MS", customer.getTitleCode());
        assertEquals("J", customer.getFirstName());
        assertEquals("Smith", customer.getLastName());
        assertEquals(1, customer.getAddresses().size());
        Address address = customer.getAddress();
        assertNotNull(address);
        assertEquals("123 Smith St", address.getAddress());
        assertEquals(suburb.getCode(), address.getSuburbCode());
        assertEquals("3070", address.getPostcode());
        assertEquals(vic.getCode(), address.getStateCode());
        assertEquals(3, customer.getPhones().size());
        assertEquals("11111111", customer.getHomePhone().getPhoneNumber());
        assertEquals("22222222", customer.getWorkPhone().getPhoneNumber());
        assertEquals("33333333", customer.getMobilePhone().getPhoneNumber());
        assertEquals("foo@bar.com", customer.getEmail().getEmailAddress());
        assertTrue(customer.isActive());
        assertEquals(location, customer.getPractice());
    }

    /**
     * Verifies a customer can be updated.
     */
    @Test
    public void testUpdateCustomer() {
        lookupFactory.getLookup(CustomerArchetypes.TITLE, "MS");
        lookupFactory.getLookup(CustomerArchetypes.TITLE, "DR");
        Lookup suburb1 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup suburb2 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup state1 = lookupFactory.getLookup(ContactArchetypes.STATE, "VIC");
        Lookup state2 = lookupFactory.getLookup(ContactArchetypes.STATE, "NSW");
        Location location1 = domainObjectService.create(practiceFactory.createLocation(), Location.class);
        Location location2 = domainObjectService.create(practiceFactory.createLocation(), Location.class);
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .address("123 Smith St", suburb1.getCode(), "3070", state1.getCode())
                .homePhone("11111111")
                .workPhone("22222222")
                .mobilePhone("33333333")
                .email("foo@bar.com")
                .practice(location1)
                .active(true)
                .build();

        getBuilder(customer1).title("DR")
                .firstName("Jo")
                .lastName("Smyth")
                .address("234 Main St", suburb2.getCode(), "2000", state2.getCode())
                .homePhone("22222222")
                .workPhone("33333333")
                .mobilePhone("44444444")
                .email("foo@bar.org")
                .practice(location2)
                .active(false)
                .build();

        Customer customer2 = reload(customer1);
        assertEquals("DR", customer2.getTitleCode());
        assertEquals("Jo", customer2.getFirstName());
        assertEquals("Smyth", customer2.getLastName());
        assertEquals(1, customer2.getAddresses().size());
        Address address = customer2.getAddress();
        assertNotNull(address);
        assertEquals("234 Main St", address.getAddress());
        assertEquals(suburb2.getCode(), address.getSuburbCode());
        assertEquals("2000", address.getPostcode());
        assertEquals(state2.getCode(), address.getStateCode());
        assertEquals(3, customer2.getPhones().size());
        assertEquals("22222222", customer2.getHomePhone().getPhoneNumber());
        assertEquals("33333333", customer2.getWorkPhone().getPhoneNumber());
        assertEquals("44444444", customer2.getMobilePhone().getPhoneNumber());
        assertEquals("foo@bar.org", customer2.getEmail().getEmailAddress());
        assertFalse(customer2.isActive());
        assertEquals(location2, customer2.getPractice());
    }

    /**
     * Verifies that a single contact is used if phone numbers are the same.
     */
    @Test
    public void testIdenticalNumbersMappedToSamePhoneContact() {
        Customer customer = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .homePhone("11111111")
                .workPhone("11111111")
                .mobilePhone("11111111")
                .build();

        assertEquals(1, customer.getContacts().size());
        Phone homePhone = customer.getHomePhone();
        Phone workPhone = customer.getWorkPhone();
        Phone mobilePhone = customer.getMobilePhone();
        assertEquals("11111111", homePhone.getPhoneNumber());
        assertEquals(homePhone, workPhone);
        assertEquals(homePhone, mobilePhone);
    }

    /**
     * Verifies the home phone can be updated.
     */
    @Test
    public void testUpdateHomePhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .homePhone("11111111")
                .build();

        getBuilder(customer1)
                .homePhone("22222222")
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(1, customer2.getContacts().size());
        assertEquals("22222222", customer2.getHomePhone().getPhoneNumber());
        assertNull(customer2.getWorkPhone());
        assertNull(customer2.getMobilePhone());
    }

    /**
     * Verifies the work phone can be updated.
     */
    @Test
    public void testUpdateWorkPhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .workPhone("11111111")
                .build();

        getBuilder(customer1)
                .workPhone("22222222")
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(1, customer2.getContacts().size());
        assertEquals("22222222", customer2.getWorkPhone().getPhoneNumber());
        assertNull(customer2.getHomePhone());
        assertNull(customer2.getMobilePhone());
    }

    /**
     * Verifies the work phone can be updated.
     */
    @Test
    public void testUpdateMobilePhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .mobilePhone("11111111")
                .build();

        getBuilder(customer1)
                .mobilePhone("22222222")
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(1, customer2.getContacts().size());
        assertEquals("22222222", customer2.getMobilePhone().getPhoneNumber());
        assertNull(customer2.getHomePhone());
        assertNull(customer2.getWorkPhone());
    }

    /**
     * Verifies the home phone can be removed.
     */
    @Test
    public void testRemoveHomePhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .homePhone("11111111")
                .workPhone("22222222")
                .mobilePhone("33333333")
                .build();

        getBuilder(customer1)
                .homePhone(null)
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(2, customer2.getContacts().size());
        assertNull(customer2.getHomePhone());
        assertEquals("22222222", customer2.getWorkPhone().getPhoneNumber());
        assertEquals("33333333", customer2.getMobilePhone().getPhoneNumber());
    }

    /**
     * Verifies the work phone can be removed.
     */
    @Test
    public void testRemoveWorkPhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .homePhone("11111111")
                .workPhone("22222222")
                .mobilePhone("33333333")
                .build();

        getBuilder(customer1)
                .workPhone(null)
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(2, customer2.getContacts().size());
        assertNull(customer2.getWorkPhone());
        assertEquals("11111111", customer2.getHomePhone().getPhoneNumber());
        assertEquals("33333333", customer2.getMobilePhone().getPhoneNumber());
    }

    /**
     * Verifies the mobile phone can be removed.
     */
    @Test
    public void testRemoveMobilePhone() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .homePhone("11111111")
                .workPhone("22222222")
                .mobilePhone("33333333")
                .build();

        getBuilder(customer1)
                .mobilePhone(null)
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(2, customer2.getContacts().size());
        assertNull(customer2.getMobilePhone());
        assertEquals("11111111", customer2.getHomePhone().getPhoneNumber());
        assertEquals("22222222", customer2.getWorkPhone().getPhoneNumber());
    }

    /**
     * Verifies the email can be updated.
     */
    @Test
    public void testUpdateEmail() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .email("foo@bar.com")
                .build();

        getBuilder(customer1)
                .email("foo@bar.org")
                .build();

        Customer customer2 = reload(customer1);
        assertEquals(1, customer2.getContacts().size());
        assertEquals("foo@bar.org", customer2.getEmail().getEmailAddress());
    }

    /**
     * Verifies the email address can be removed.
     */
    @Test
    public void testRemoveEmail() {
        Customer customer1 = getBuilder().title("MS")
                .firstName("J")
                .lastName("Smith")
                .email("foo@bar.com")
                .build();

        getBuilder(customer1)
                .email(null)
                .build();

        Customer customer2 = reload(customer1);
        assertTrue(customer2.getContacts().isEmpty());
        assertNull(customer2.getEmail());
    }

    /**
     * Returns a builder to create new customers.
     *
     * @return a builder
     */
    private CustomerBuilder getBuilder() {
        return new CustomerBuilderImpl(rules, getArchetypeService(), domainObjectService, getLookupService());
    }

    /**
     * Returns a builder to update a customer.
     *
     * @param customer the customer
     * @return a builder
     */
    private CustomerBuilder getBuilder(Party customer) {
        return new CustomerBuilderImpl(customer, rules, getArchetypeService(), domainObjectService, getLookupService());
    }

    /**
     * Reloads a customer.
     *
     * @param customer the customer
     * @return the reloaded customer
     */
    private Customer reload(Customer customer) {
        return domainObjectService.create(get(customer.getObjectReference()), Customer.class);
    }
}