/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.till;

import org.junit.Test;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.domain.till.DrawerCommand;
import org.openvpms.domain.till.Till;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link TillImpl} class.
 *
 * @author Tim Anderson
 */
public class TillImplTestCase extends AbstractDomainObjectTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies the implementation has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Entity till1 = practiceFactory.newTill().build(false);
        Entity till2 = practiceFactory.newTill().build(false);
        checkEquality(till1, till2, Till.class);
    }

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Override
    public void testFactory() {
        Entity till = practiceFactory.newTill().build(false);
        checkFactory(till, Till.class, TillImpl.class);
    }

    /**
     * Tests the {@link TillImpl#getDrawerCommand()} method.
     */
    @Test
    public void testGetDrawerCommand() {
        Till till1 = createTill(null, null);
        assertNull(till1.getDrawerCommand());

        Till till2 = createTill("printer", null);
        assertNull(till2.getDrawerCommand());

        Till till3 = createTill(null, "1,2,3");
        assertNull(till3.getDrawerCommand());

        Till till4 = createTill("printer", "invalid command");
        assertNull(till4.getDrawerCommand());

        Till till5 = createTill("printer", "27,112,0,50,250");
        DrawerCommand drawerCommand5 = till5.getDrawerCommand();
        assertNotNull(drawerCommand5);
        assertEquals("printer", drawerCommand5.getPrinter());
        assertNull(drawerCommand5.getPrinterServiceArchetype());
        assertArrayEquals(new byte[]{27, 112, 0, 50, (byte)250}, drawerCommand5.getCommand());

        Till till6 = createTill("entity.printerServiceTest:printer", "27,112,0,50,250");
        DrawerCommand drawerCommand6 = till6.getDrawerCommand();
        assertEquals("printer", drawerCommand6.getPrinter());
        assertEquals("entity.printerServiceTest", drawerCommand6.getPrinterServiceArchetype());
        assertArrayEquals(new byte[]{27, 112, 0, 50, (byte)250}, drawerCommand6.getCommand());
    }

    /**
     * Creates a new till.
     *
     * @param printer       the printer. May be {@code null}
     * @param drawerCommand the drawer command. May be {@code null}
     * @return a new till
     */
    private Till createTill(String printer, String drawerCommand) {
        Entity till = practiceFactory.newTill()
                .printer(printer)
                .drawerCommand(drawerCommand)
                .build(false);
        return getDomainService().create(till, Till.class);
    }
}
