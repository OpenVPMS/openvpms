/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.junit.Test;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.product.Service;
import org.openvpms.domain.service.object.DomainObjectService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link ServiceImpl} class.
 *
 * @author Tim Anderson
 */
public class ServiceImplTestCase extends AbstractDomainObjectTest {

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Verifies the implementation has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Product product1 = productFactory.createService();
        Product product2 = productFactory.createService();
        checkEquality(product1, product2, Service.class);
    }

    /**
     * Verifies a {@link ServiceImpl} can be created via {@link DomainObjectService#create(IMObject, Class)}.
     */
    @Test
    public void testFactory() {
        Product product = productFactory.createService();
        checkFactory(product, Service.class, ServiceImpl.class);
    }
}
