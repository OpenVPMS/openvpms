/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Active;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.patient.PatientQuery;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientQueryImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientQueryImplTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Tests the {@link PatientQueryImpl#id(long)} method.
     */
    @Test
    public void testQueryById() {
        Patient patient = domainService.create(patientFactory.createPatient(), Patient.class);
        PatientQuery query = getQuery()
                .id(patient.getId());

        checkQuery(query, patient);
        assertEquals(patient, query.findFirst());
    }

    /**
     * Tests the {@link PatientQueryImpl#name(String)} method.
     */
    @Test
    public void testQueryByName() {
        String name = ValueStrategy.randomString();
        Patient patientA = createPatient(name);
        Patient patientB = createPatient(name);

        PatientQuery query = getQuery()
                .name(name);
        checkQuery(query, patientA, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#name(Filter)} method.
     */
    @Test
    public void testQueryByNameFilter() {
        String name = ValueStrategy.randomString();
        Patient patientA = createPatient(name + "A");
        Patient patientB = createPatient(name + "B");

        PatientQuery query1 = getQuery()
                .name(Filter.like(name + "%"));
        checkQuery(query1, patientA, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#active()} and {@link PatientQueryImpl#inactive()}.
     */
    @Test
    public void testQueryByActiveInactive() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = ValueStrategy.randomString();
        Patient patientA = createPatient("A", microchip, true);
        Patient patientB = createPatient("B", microchip, false);
        Patient patientC = createPatient("C", microchip, true);

        // verify all patients are returned by default
        PatientQuery query1 = getQuery()
                .microchip(microchip);
        checkQuery(query1, patientA, patientB, patientC);

        // check active patients
        PatientQuery query2 = getQuery()
                .microchip(microchip).active();
        checkQuery(query2, patientA, patientC);

        // check inactive patients
        PatientQuery query3 = getQuery()
                .microchip(microchip).inactive();
        checkQuery(query3, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#microchip(String)} method.
     */
    @Test
    public void testQueryByMicrochip() {
        String microchip1 = ValueStrategy.randomString();
        String microchip2 = ValueStrategy.randomString();
        Patient patientA = createPatient("A", microchip1);
        Patient patientB = createPatient("B", microchip1);
        Patient patientC = createPatient("C", microchip2);

        PatientQuery query1 = getQuery()
                .microchip(microchip1);
        checkQuery(query1, patientA, patientB);

        PatientQuery query2 = getQuery()
                .microchip(microchip2);
        assertEquals(patientC, query2.findFirst());
        checkQuery(query2, patientC);
    }

    /**
     * Tests the {@link PatientQueryImpl#microchip(Filter)} method.
     */
    @Test
    public void testQueryByMicrochipFilter() {
        String microchip = ValueStrategy.randomString();
        Patient patientA = createPatient("Spot", microchip + "A");
        Patient patientB = createPatient("Fido", microchip + "B");

        PatientQuery query1 = getQuery()
                .microchip(Filter.like(microchip + "%"));
        checkQuery(query1, patientA, patientB);
    }

    /**
     * Tests the {@link PatientQueryImpl#orderById()} and {@link PatientQueryImpl#orderById(boolean)} methods.
     */
    @Test
    public void testOrderById() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = ValueStrategy.randomString();
        Patient patientA = createPatient("A", microchip);
        Patient patientB = createPatient("B", microchip);
        Patient patientC = createPatient("C", microchip);

        // default should be to order by ascending id, if no order specified.
        PatientQuery query1 = getQuery()
                .microchip(microchip);
        checkQuery(query1, patientA, patientB, patientC);

        PatientQuery query2 = getQuery()
                .microchip(microchip)
                .orderById();
        checkQuery(query2, patientA, patientB, patientC);

        PatientQuery query3 = getQuery()
                .microchip(microchip)
                .orderById(true);
        checkQuery(query3, patientA, patientB, patientC);

        PatientQuery query4 = getQuery()
                .microchip(microchip)
                .orderById(false);
        checkQuery(query4, patientC, patientB, patientA);
    }

    /**
     * Tests the {@link PatientQueryImpl#orderByName()} and {@link PatientQueryImpl#orderByName(boolean)} methods.
     */
    @Test
    public void testOrderByName() {
        // create 3 patients, each with the same microchip to limit the amount of data returned
        String microchip = ValueStrategy.randomString();
        Patient patientA = createPatient("Spot", microchip);
        Patient patientB = createPatient("Fido", microchip);
        Patient patientC = createPatient("Fido", microchip);
        // by default, patient2 should always appear before patient3 as it has a lower id
        assertTrue(patientB.getId() < patientC.getId());

        PatientQuery query1 = getQuery()
                .microchip(microchip)
                .orderByName();

        checkQuery(query1, patientB, patientC, patientA);

        PatientQuery query2 = getQuery()
                .microchip(microchip)
                .orderByName(true);
        checkQuery(query2, patientB, patientC, patientA);

        PatientQuery query3 = getQuery()
                .microchip(microchip)
                .orderByName(false);
        checkQuery(query3, patientA, patientB, patientC);

        // verify can order by name and descending id
        PatientQuery query4 = getQuery()
                .microchip(microchip)
                .orderByName(true)
                .orderById(false);
        checkQuery(query4, patientC, patientB, patientA);
    }

    /**
     * Tests the {@link PatientQueryImpl#owner(Customer)} and {@link PatientQueryImpl#owner(long)} methods.
     */
    @Test
    public void testQueryByOwner() {
        Customer customer = domainService.create(customerFactory.createCustomer(), Customer.class);
        Patient patient1 = createPatient(customer);
        Patient patient2 = createPatient(customer);
        PatientQuery query1 = getQuery().owner(customer);
        checkQuery(query1, patient1, patient2);

        PatientQuery query2 = getQuery().owner(customer.getId());
        checkQuery(query2, patient1, patient2);
    }

    /**
     * Tests the {@link PatientQueryImpl#activeOwner()}, {@link PatientQuery#inactiveOwner()} and
     * {@link PatientQueryImpl#activeOwner(Active)} methods.
     */
    @Test
    public void testQueryByOwnerActiveInactive() {
        Customer customerA = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customerB = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customerC = domainService.create(customerFactory.newCustomer().active(false).build(), Customer.class);

        // create 4 patients, each with the same microchip to limit the amount of data returned
        String microchip = ValueStrategy.randomString();

        Patient patient1 = createPatient(customerA, microchip);
        Patient patient2 = createPatient(customerA, microchip);
        Patient patient3 = createPatient(customerB, microchip);
        Patient patient4 = createPatient(customerC, microchip);

        // default behaviour picks up active and inactive owners
        PatientQuery query1 = getQuery()
                .microchip(microchip)
                .orderById();
        checkQuery(query1, patient1, patient2, patient3, patient4);

        PatientQuery query2 = getQuery()
                .activeOwner()
                .microchip(microchip)
                .orderById();
        checkQuery(query2, patient1, patient2, patient3);

        PatientQuery query3 = getQuery()
                .inactiveOwner()
                .microchip(microchip)
                .orderById();
        checkQuery(query3, patient4);

        PatientQuery query4 = getQuery()
                .activeOwner(Active.ALL)
                .microchip(microchip)
                .orderById();
        checkQuery(query4, patient1, patient2, patient3, patient4);
    }

    /**
     * Tests the {@link PatientQueryImpl#ownerName(String, String)} and {@link PatientQueryImpl#ownerName(Filter, Filter)}
     * methods.
     */
    @Test
    public void testQueryByOwnerName() {
        String suffix = ValueStrategy.randomString();
        String lastNameA = "Smith" + suffix;
        String lastNameB = "Smyth" + suffix;
        String lastNameC = "Smithers" + suffix;
        Customer customerA = domainService.create(customerFactory.createCustomer("Jo", lastNameA), Customer.class);
        Customer customerB = domainService.create(customerFactory.createCustomer("Jo", lastNameB), Customer.class);
        Customer customerC = domainService.create(customerFactory.createCustomer("Joanna", lastNameC), Customer.class);

        Patient patient1 = createPatient(customerA);
        Patient patient2 = createPatient(customerB);
        Patient patient3 = createPatient(customerC);

        PatientQuery query1 = getQuery().ownerName(lastNameA);
        checkQuery(query1, patient1);

        PatientQuery query2 = getQuery().ownerName(lastNameA, "Jo");
        checkQuery(query2, patient1);

        PatientQuery query3 = getQuery().ownerName(lastNameA, "Joanna");
        checkQuery(query3);

        PatientQuery query4 = getQuery().ownerName(Filter.equal(lastNameA));
        checkQuery(query4, patient1);

        PatientQuery query5 = getQuery().ownerName(Filter.equal(lastNameB), Filter.equal("Jo"));
        checkQuery(query5, patient2);

        PatientQuery query6 = getQuery().ownerName(lastNameA);
        checkQuery(query6, patient1);

        PatientQuery query7 = getQuery().ownerName(Filter.like("Smith%" + suffix));
        checkQuery(query7, patient1, patient3);

        PatientQuery query8 = getQuery().ownerName(Filter.like("S%" + suffix), Filter.like("Joan%"));
        checkQuery(query8, patient3);

        PatientQuery query9 = getQuery().ownerName(Filter.like("S%" + suffix), Filter.like("J%"));
        checkQuery(query9, patient1, patient2, patient3);
    }

    /**
     * Tests the {@link PatientQueryImpl#ownerAddress(String, String, String, String)}
     * and {@link PatientQueryImpl#ownerAddress(String, String, String, String)} methods.
     */
    @Test
    public void testQueryByOwnerAddress() {
        String name = ValueStrategy.randomString(); // use the same name for all patients to limit results
        Lookup suburb1 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup suburb2 = lookupFactory.newLookup(ContactArchetypes.SUBURB)
                .uniqueCode()
                .build();
        Lookup vic = lookupFactory.getLookup(ContactArchetypes.STATE, "VIC", "Victoria");
        Lookup nsw = lookupFactory.getLookup(ContactArchetypes.STATE, "NSW", "New South Wales");
        Customer customerA = domainService.create(
                customerFactory.newCustomer()
                        .addAddress("1024 Broadwater Avenue", suburb1.getCode(), vic.getCode(), "3925")
                        .build(), Customer.class);
        Customer customerB = domainService.create(
                customerFactory.newCustomer()
                        .addAddress("1024 Broadwater St", suburb1.getCode(), vic.getCode(), "3925")
                        .build(), Customer.class);
        Customer customerC = domainService.create(
                customerFactory.newCustomer()
                        .addAddress("1024 Broadwater St", suburb2.getCode(), nsw.getCode(), "2360")
                        .build(), Customer.class);

        Patient patient1 = createPatient(name, customerA);
        Patient patient2 = createPatient(name, customerB);
        Patient patient3 = createPatient(name, customerC);

        PatientQuery query1 = getQuery().name(name)
                .ownerAddress("1024 Broadwater Avenue", suburb1.getName(), "3925", vic.getName());
        checkQuery(query1, patient1);

        PatientQuery query2 = getQuery().name(name)
                .ownerAddress("1024 Broadwater St", null, null, null);
        checkQuery(query2, patient2, patient3);

        PatientQuery query3 = getQuery().name(name)
                .ownerAddress(null, suburb1.getName(), null, null);
        checkQuery(query3, patient1, patient2);

        PatientQuery query4 = getQuery().name(name)
                .ownerAddress(null, null, "3925", null);
        checkQuery(query4, patient1, patient2);

        PatientQuery query5 = getQuery().name(name)
                .ownerAddress(null, null, null, vic.getName());
        checkQuery(query5, patient1, patient2);

        PatientQuery query6 = getQuery().name(name)
                .ownerAddress(Filter.like("1024 Broadwater%"), null, null, null);
        checkQuery(query6, patient1, patient2, patient3);

        PatientQuery query7 = getQuery().name(name)
                .ownerAddress(null, Filter.like(suburb1.getName() + "%"), null, null);
        checkQuery(query7, patient1, patient2);

        PatientQuery query9 = getQuery().name(name)
                .ownerAddress(null, null, Filter.like("236%"), null);
        checkQuery(query9, patient3);

        PatientQuery query10 = getQuery().name(name)
                .ownerAddress(null, null, null, Filter.like("Vic%"));
        checkQuery(query10, patient1, patient2);
    }

    /**
     * Tests the {@link PatientQueryImpl#ownerEmail(String)} and {@link PatientQueryImpl#ownerEmail(String)} methods.
     */
    @Test
    public void testQueryByOwnerEmail() {
        String prefix = ValueStrategy.randomString();
        String emailA = prefix + "A@gmail.com";
        String emailB = prefix + "B@gmail.com";
        String emailC = prefix + "C@gmail.com";

        Customer customerA = domainService.create(customerFactory.newCustomer().addEmail(emailA).build(),
                                                  Customer.class);
        Customer customerB = domainService.create(customerFactory.newCustomer().addEmail(emailB).build(),
                                                  Customer.class);
        Customer customerC = domainService.create(customerFactory.newCustomer().addEmail(emailC).build(),
                                                  Customer.class);
        Patient patient1 = createPatient(customerA);
        Patient patient2 = createPatient(customerB);
        Patient patient3 = createPatient(customerC);

        PatientQuery query1 = getQuery().ownerEmail(emailA);
        checkQuery(query1, patient1);

        PatientQuery query2 = getQuery().ownerEmail(Filter.equal(emailB));
        checkQuery(query2, patient2);

        PatientQuery query3 = getQuery().ownerEmail(Filter.like(prefix + "%"));
        checkQuery(query3, patient1, patient2, patient3);
    }

    /**
     * Tests the {@link PatientQueryImpl#ownerPhone(String)} and {@link PatientQueryImpl#ownerPhone(Filter)} methods.
     */
    @Test
    public void testQueryByOwnerPhone() {
        String prefix = ValueStrategy.randomString();
        String phoneA = prefix + "1";
        String phoneB = prefix + "2";
        String phoneC = prefix + "3";
        Customer customerA = domainService.create(customerFactory.newCustomer().addPhone(phoneA).build(),
                                                  Customer.class);
        Customer customerB = domainService.create(customerFactory.newCustomer().addPhone(phoneB).build(),
                                                  Customer.class);
        Customer customerC = domainService.create(customerFactory.newCustomer().addPhone(phoneC).build(),
                                                  Customer.class);
        Patient patient1 = createPatient(customerA);
        Patient patient2 = createPatient(customerB);
        Patient patient3 = createPatient(customerC);
        PatientQuery query1 = getQuery().ownerPhone(phoneA);
        checkQuery(query1, patient1);

        PatientQuery query2 = getQuery().ownerPhone(Filter.equal(phoneB));
        checkQuery(query2, patient2);

        PatientQuery query3 = getQuery().ownerPhone(Filter.like(prefix + "%"));
        checkQuery(query3, patient1, patient2, patient3);
    }

    /**
     * Verifies that prior owner relationships are excluded.
     */
    @Test
    public void testPriorOwnerRelationshipExcluded() {
        Customer customerA = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customerB = domainService.create(customerFactory.createCustomer(), Customer.class);
        Customer customerC = domainService.create(customerFactory.createCustomer(), Customer.class);

        Patient patient1 = domainService.create(
                patientFactory.newPatient()
                        .addOwner(customerA, DateRules.getToday(), null)
                        .addOwner(customerB, DateRules.getYesterday(), DateRules.getToday())
                        .build(), Patient.class);
        Patient patient2 = domainService.create(
                patientFactory.newPatient()
                        .addOwner(customerB, DateRules.getToday(), null)
                        .addOwner(customerC, DateRules.getYesterday(), DateRules.getToday())
                        .build(), Patient.class);

        checkQuery(getQuery().owner(customerA), patient1);
        checkQuery(getQuery().owner(customerB), patient2);
        checkQuery(getQuery().owner(customerC));
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    private PatientQueryImpl getQuery() {
        return new PatientQueryImpl(getArchetypeService(), domainService);
    }

    /**
     * Verifies a query returns the expected result, in the order given.
     *
     * @param query    the query
     * @param expected the expected results
     */
    private void checkQuery(PatientQuery query, Patient... expected) {
        List<Patient> matches = IterableUtils.toList(query.query());
        assertEquals(Arrays.asList(expected), matches);
    }

    /**
     * Creates a patient with the specified name.
     *
     * @param name the patient name
     * @return a new patient
     */
    private Patient createPatient(String name) {
        Party patient = patientFactory.newPatient()
                .name(name)
                .build();
        return domainService.create(patient, Patient.class);
    }

    /**
     * Creates a patient with the specified name and microchip.
     *
     * @param name      the patient name
     * @param microchip the microchip
     * @return a new patient
     */
    private Patient createPatient(String name, String microchip) {
        return createPatient(name, microchip, true);
    }

    /**
     * Creates a patient with the specified name, microchip and active flag
     *
     * @param name      the patient name
     * @param microchip the microchip
     * @param active    if {@code true} the patient is active, else it is inactive
     * @return a new patient
     */
    private Patient createPatient(String name, String microchip, boolean active) {
        Party patient = patientFactory.newPatient()
                .name(name)
                .addMicrochip(microchip)
                .active(active)
                .build();
        return domainService.create(patient, Patient.class);
    }

    /**
     * Creates a patient with an owner.
     *
     * @param owner the owner
     * @return the corresponding patient
     */
    private Patient createPatient(String name, Customer owner) {
        Party patient = patientFactory.newPatient().name(name).owner(owner).build();
        return domainService.create(patient, Patient.class);
    }

    /**
     * Creates a patient with an owner.
     *
     * @param owner the owner
     * @return the corresponding patient
     */
    private Patient createPatient(Customer owner) {
        Party patient = patientFactory.createPatient(owner);
        return domainService.create(patient, Patient.class);
    }

    /**
     * Creates a patient with an owner and microchip.
     *
     * @param owner     the owner
     * @param microchip the microchip
     * @return the corresponding patient
     */
    private Patient createPatient(Customer owner, String microchip) {
        Party patient = patientFactory.newPatient()
                .owner(owner)
                .addMicrochip(microchip)
                .build();
        return domainService.create(patient, Patient.class);
    }
}