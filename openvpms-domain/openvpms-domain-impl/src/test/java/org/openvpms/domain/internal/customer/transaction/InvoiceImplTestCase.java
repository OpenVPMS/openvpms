/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.service.object.DomainObjectService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link InvoiceImpl} class.
 *
 * @author Tim Anderson
 */
public class InvoiceImplTestCase extends AbstractDomainObjectTest {

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * Tests the {@link InvoiceImpl#equals(Object)} and {@link InvoiceImpl#hashCode()} methods.
     */
    @Test
    public void testEquality() {
        FinancialAct act1 = accountFactory.newInvoice().customer(customerFactory.createCustomer()).build();
        FinancialAct act2 = accountFactory.newInvoice().customer(customerFactory.createCustomer()).build();
        checkEquality(act1, act2, Invoice.class);
    }

    /**
     * Verifies an {@link InvoiceImpl} can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Test
    public void testFactory() {
        FinancialAct act = accountFactory.newInvoice().customer(customerFactory.createCustomer()).build();
        checkFactory(act, Invoice.class, InvoiceImpl.class);
    }
}
