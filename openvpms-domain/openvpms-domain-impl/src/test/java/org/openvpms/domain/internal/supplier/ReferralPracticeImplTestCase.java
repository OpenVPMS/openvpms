/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.supplier;

import org.junit.Test;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.domain.supplier.ReferralPractice;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ReferralPracticeImpl} class.
 *
 * @author Tim Anderson
 */
public class ReferralPracticeImplTestCase extends AbstractDomainObjectTest {

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Verifies the domain implementation of an object has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Party party1 = supplierFactory.newVetPractice().build(false);
        Party party2 = supplierFactory.newVetPractice().build(false);
        checkEquality(party1, party2, ReferralPractice.class);
    }

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Override
    public void testFactory() {
        Party party = supplierFactory.newVetPractice().build(false);
        checkFactory(party, ReferralPractice.class, ReferralPracticeImpl.class);
    }

    /**
     * Verifies that {@link ReferralPractice} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party party = supplierFactory.createVetPractice();
        ReferralPractice practice = getDomainService().create(party, ReferralPractice.class);
        assertTrue(practice instanceof ReferralPracticeImpl);
        assertEquals(party.getName(), practice.getName());
    }
}
