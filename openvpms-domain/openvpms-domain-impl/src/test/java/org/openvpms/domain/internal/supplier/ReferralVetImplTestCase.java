/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.supplier;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.test.AbstractDomainObjectTest;
import org.openvpms.domain.service.object.DomainObjectService;
import org.openvpms.domain.supplier.ReferralVet;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ReferralVetImpl} class.
 *
 * @author Tim Anderson
 */
public class ReferralVetImplTestCase extends AbstractDomainObjectTest {

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Verifies the domain implementation of an object has provided equals and hashCode methods.
     */
    @Override
    public void testEquality() {
        Party party1 = supplierFactory.newVet().build(false);
        Party party2 = supplierFactory.newVet().build(false);
        checkEquality(party1, party2, ReferralVet.class);
    }

    /**
     * Verifies a domain object can be created via {@link DomainObjectService#create(IMObject, Class)}
     * and {@link DomainService#create(IMObjectBean, Class)}.
     */
    @Override
    public void testFactory() {
        Party party = supplierFactory.newVet().build(false);
        checkFactory(party, ReferralVet.class, ReferralVetImpl.class);
    }

    /**
     * Verifies that {@link ReferralVet} methods return the expected results.
     */
    @Test
    public void testAccessors() {
        Party practice1 = supplierFactory.createVetPractice();
        Party practice2 = supplierFactory.createVetPractice();
        Party party = supplierFactory.newVet().title("DR").firstName("J").lastName("Smith")
                .addPractice(practice1, DateRules.getYesterday())
                .addPractice(practice2, DateRules.getToday())
                .build();
        ReferralVet vet = getDomainService().create(party, ReferralVet.class);
        assertTrue(vet instanceof ReferralVetImpl);
        assertEquals("Dr", vet.getTitle());
        assertEquals("J", vet.getFirstName());
        assertEquals("Smith", vet.getLastName());
        assertEquals(practice2, vet.getPractice());
        assertEquals(practice1, vet.getPractice(OffsetDateTime.now().minusDays(1)));
    }
}
