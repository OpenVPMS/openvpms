/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.party.BeanPartyDecorator;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.patient.alert.AllergyImpl;
import org.openvpms.domain.internal.patient.referral.ReferralsImpl;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.alert.Allergies;
import org.openvpms.domain.patient.alert.Allergy;
import org.openvpms.domain.patient.referral.Referrals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Default implementation of {@link Patient}.
 *
 * @author Tim Anderson
 */
public class PatientImpl extends BeanPartyDecorator implements Patient {

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Date of birth node name.
     */
    private static final String DATE_OF_BIRTH = "dateOfBirth";

    /**
     * The species node name.
     */
    private static final String SPECIES = "species";

    /**
     * The breed node name.
     */
    private static final String BREED = "breed";

    /**
     * The deceased node name.
     */
    private static final String DECEASED = "deceased";

    /**
     * The deceased date node name.
     */
    private static final String DECEASED_DATE = "deceasedDate";

    /**
     * The colour node name.
     */
    private static final String COLOUR = "colour";

    /**
     * Constructs a {@link PatientImpl}.
     *
     * @param peer         the peer to delegate to
     * @param patientRules the patient rules
     * @param service      the domain object service
     */
    public PatientImpl(Party peer, PatientRules patientRules, DomainService service) {
        super(peer, service);
        this.patientRules = patientRules;
        this.domainService = service;
    }

    /**
     * Constructs a {@link PatientImpl}.
     *
     * @param bean         the bean wrapping the party
     * @param patientRules the patient rules
     * @param service      the domain object service
     */
    public PatientImpl(IMObjectBean bean, PatientRules patientRules, DomainService service) {
        super(bean);
        this.patientRules = patientRules;
        this.domainService = service;
    }

    /**
     * Returns the age of the patient.
     * <p>
     * If the patient is deceased, the age of the patient when they died will be returned.
     *
     * @return the age as a formatted string, or {@code null} if the patient has no recorded date of birth
     */
    @Override
    public String getAge() {
        return hasDateOfBirth() ? patientRules.getPatientAge(getPeer()) : null;
    }

    /**
     * Returns the age of the patient as at the specified date.
     * <p>
     * If the patient died prior to the date, the age of the patient when they died will be returned.
     *
     * @param date the date to base the age upon
     * @return the age as a formatted string, or {@code null} if the patient has no recorded date of birth
     */
    @Override
    public String getAge(LocalDate date) {
        return hasDateOfBirth() ? patientRules.getPatientAge(getPeer(), DateRules.toDate(date)) : null;
    }

    /**
     * Returns the patient's date of birth.
     *
     * @return the patient's date of birth. May be {@code null}
     */
    @Override
    public LocalDate getDateOfBirth() {
        Date date = getBean().getDate(DATE_OF_BIRTH);
        return DateRules.toLocalDate(date);
    }

    /**
     * Determines if the patient is deceased.
     *
     * @return {@code true} if the patient is deceased
     */
    @Override
    public boolean isDeceased() {
        return getBean().getBoolean(DECEASED);
    }

    /**
     * Returns the patient's date of death.
     *
     * @return the patient's date of death. May be {@code null}
     */
    @Override
    public LocalDate getDateOfDeath() {
        Date date = getBean().getDate(DECEASED_DATE);
        return date != null ? DateRules.toLocalDate(date) : null;
    }

    /**
     * Returns the species name.
     *
     * @return the species name
     */
    @Override
    public String getSpeciesName() {
        Lookup lookup = getSpeciesLookup();
        return (lookup != null) ? lookup.getName() : null;
    }

    /**
     * Returns the species code.
     *
     * @return the species code. May be {@code null}
     */
    @Override
    public String getSpeciesCode() {
        return getBean().getString(SPECIES);
    }

    /**
     * Returns the species.
     *
     * @return the species.
     */
    @Override
    public Lookup getSpeciesLookup() {
        return getBean().getLookup(SPECIES);
    }

    /**
     * Returns the breed name.
     *
     * @return the breed name. May be {@code null}
     */
    @Override
    public String getBreedName() {
        String result;
        Lookup lookup = getBreedLookup();
        if (lookup != null) {
            result = lookup.getName();
        } else {
            result = getBean().getString("newBreed");
        }
        return result;
    }

    /**
     * Returns the breed code.
     *
     * @return the breed code. May be {@code null}
     */
    @Override
    public String getBreedCode() {
        return getBean().getString(BREED);
    }

    /**
     * Returns the breed lookup.
     *
     * @return the breed lookup. May be {@code null}
     */
    @Override
    public Lookup getBreedLookup() {
        return getBean().getLookup(BREED);
    }

    /**
     * Returns the patient's sex.
     *
     * @return the sex
     */
    @Override
    public Sex getSex() {
        String code = getBean().getString("sex");
        return (code != null) ? Sex.valueOf(code) : Sex.UNSPECIFIED;
    }

    /**
     * Determines if the patient is desexed.
     *
     * @return {@code true} if the patient is desexed
     */
    @Override
    public boolean isDesexed() {
        return getBean().getBoolean("desexed");
    }

    /**
     * Returns the patient's colour.
     * <p>
     * Some implementations define this as a lookup.
     *
     * @return the patient's colour. May be {@code null}
     */
    @Override
    public String getColourName() {
        Lookup lookup = getColourLookup();
        return (lookup != null) ? lookup.getName() : getColourCode();
    }

    /**
     * Returns the colour code.
     *
     * @return the colour code. May be {@code null}
     */
    @Override
    public String getColourCode() {
        return getBean().getString(COLOUR);
    }

    /**
     * Returns the colour lookup.
     *
     * @return the colour lookup. May be {@code null}
     */
    @Override
    public Lookup getColourLookup() {
        IMObjectBean bean = getBean();
        NodeDescriptor node = bean.getNode(COLOUR);
        return node.isLookup() ? bean.getLookup(COLOUR) : null;
    }

    /**
     * Returns the patient's microchip.
     *
     * @return the patient's microchip. May be {@code null}
     */
    @Override
    public Microchip getMicrochip() {
        EntityIdentity identity = patientRules.getMicrochip(getPeer());
        return identity != null ? new MicrochipImpl(identity) : null;
    }

    /**
     * Returns the current owner of the patient.
     *
     * @return the current owner. May be {@code null}
     */
    @Override
    public Customer getOwner() {
        Party owner = patientRules.getOwner(this);
        return owner != null ? domainService.create(owner, Customer.class) : null;
    }

    /**
     * Returns the most recent weight for the patient.
     *
     * @return the patient's weight, or {@code 0} if its weight is not known
     */
    @Override
    public Weight getWeight() {
        return patientRules.getWeight(this);
    }

    /**
     * Determines if the patient is aggressive.
     *
     * @return {@code true} if the patient is aggressive
     */
    @Override
    public boolean isAggressive() {
        return patientRules.isAggressive(this);
    }

    /**
     * Returns any allergies this patient may have.
     *
     * @return the allergies
     */
    @Override
    public Allergies getAllergies() {
        return new AllergiesImpl();
    }

    /**
     * Determines if the patient has a date of birth recorded.
     *
     * @return {@code true} if the patient has a date of birth, otherwise {@code false}
     */
    private boolean hasDateOfBirth() {
        return getBean().getDate(DATE_OF_BIRTH) != null;
    }

    private class AllergiesImpl implements Allergies {

        /**
         * Returns the patient's active allergies.
         * <p/>
         * NOTE: this is {@link Iterable} to support future lazy query and construction.
         *
         * @return the current allergies
         */
        @Override
        public Iterable<Allergy> getCurrent() {
            List<Allergy> result = new ArrayList<>();
            List<Act> allergies = patientRules.getAllergies(PatientImpl.this, new Date());
            for (Act allergy : allergies) {
                result.add(new AllergyImpl(domainService.getBean(allergy)));
            }
            return result;
        }
    }

    /**
     * Returns any referrals the patient may have.
     *
     * @return the referrals
     */
    @Override
    public Referrals getReferrals() {
        return new ReferralsImpl(getBean(), domainService);
    }
}
