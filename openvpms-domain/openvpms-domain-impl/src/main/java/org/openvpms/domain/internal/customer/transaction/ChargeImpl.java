/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.customer.transaction.Charge;
import org.openvpms.domain.customer.transaction.ChargeItem;
import org.openvpms.domain.internal.factory.DomainService;

import java.math.BigDecimal;

/**
 * Default implementation of {@link Charge}.
 *
 * @author Tim Anderson
 */
public class ChargeImpl<T extends ChargeItem> extends TransactionImpl<T> implements Charge<T> {

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * Constructs a {@link ChargeImpl}.
     *
     * @param charge        the charge
     * @param itemType      the line item type, or {@code null} if the charge has no line items
     * @param domainService the domain service
     */
    public ChargeImpl(FinancialAct charge, Class<T> itemType, DomainService domainService) {
        super(charge, itemType, domainService);
    }

    /**
     * Constructs a {@link ChargeImpl}.
     *
     * @param bean          a bean wrapping the charge
     * @param itemType      the line item type, or {@code null} if the charge has no line items
     * @param domainService the domain service
     */
    protected ChargeImpl(IMObjectBean bean, Class<T> itemType, DomainService domainService) {
        super(bean, itemType, domainService);
    }

    /**
     * Returns the clinician responsible for this charge.
     *
     * @return the clinician. May be {@code null}
     */
    public User getClinician() {
        if (clinician == null) {
            clinician = getBean().getTarget("clinician", User.class);
        }
        return clinician;
    }

    /**
     * Returns the discount amount, including tax.
     *
     * @return the discount amount
     */
    @Override
    public BigDecimal getDiscount() {
        BigDecimal result = BigDecimal.ZERO;
        for (ChargeItem item : getItems()) {
            result = result.add(item.getDiscount());
        }
        return result;
    }

    /**
     * Returns the discount tax amount.
     *
     * @return the discount tax amount
     */
    @Override
    public BigDecimal getDiscountTax() {
        BigDecimal result = BigDecimal.ZERO;
        for (ChargeItem item : getItems()) {
            result = result.add(item.getDiscountTax());
        }
        return result;
    }

    /**
     * Returns the total tax amount.
     *
     * @return the tax amount
     */
    @Override
    public BigDecimal getTotalTax() {
        return getTransaction().getTaxAmount();
    }
}
