/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.referral;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.referral.Referral;
import org.openvpms.domain.supplier.ReferralPractice;
import org.openvpms.domain.supplier.ReferralVet;

import java.time.OffsetDateTime;
import java.util.Date;

/**
 * Default implementation of {@link Referral}.
 *
 * @author Tim Anderson
 */
public class ReferralImpl implements Referral {

    /**
     * The relationship.
     */
    private final PeriodRelationship relationship;

    /**
     * A bean wrapping the relationship.
     */
    private final IMObjectBean bean;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link ReferralImpl}.
     *
     * @param relationship  the relationship
     * @param domainService the domain object service
     */
    public ReferralImpl(PeriodRelationship relationship, DomainService domainService) {
        this.relationship = relationship;
        this.domainService = domainService;
        this.bean = domainService.getBean(relationship);
    }

    /**
     * Returns the practice the patient was referred from/to.
     *
     * @return the practice. May be {@code null}
     */
    @Override
    public ReferralPractice getPractice() {
        ReferralVet vet = getVet();
        return vet != null ? vet.getPractice() : null;
    }

    /**
     * Returns the vet the patient was referred from/to.
     *
     * @return the vet
     */
    @Override
    public ReferralVet getVet() {
        return domainService.get(relationship.getTarget(), ReferralVet.class);
    }

    /**
     * Returns the date when the referral was made.
     *
     * @return the date
     */
    @Override
    public OffsetDateTime getDate() {
        Date date = relationship.getActiveStartTime();
        return (date != null) ? DateRules.toOffsetDateTime(date) : null;
    }

    /**
     * Determines if this referral is active.
     * <p/>
     * A referral is considered active if it is the most recent referral.
     *
     * @return {@code true} if the referral is active, {@code false} if there is a more recent referral
     */
    @Override
    public boolean isActive() {
        return relationship.isActive(new Date());
    }

    /**
     * Determines if the patient was referred from the practice/vet.
     *
     * @return {@code true} if the patient was referred from the practice/vet, {@code false} if they were referred to
     * it
     */
    @Override
    public boolean referredFrom() {
        return relationship.isA(PatientArchetypes.REFERRED_FROM);
    }

    /**
     * Determines if the patient was referred to the practice/vet.
     *
     * @return {@code true} if the patient was referred to the practice/vet, {@code false} if they were referred from
     * it
     */
    @Override
    public boolean referredTo() {
        return !referredFrom();
    }

    /**
     * Returns the reason for the referral.
     *
     * @return the reason. May be {@code null}
     */
    @Override
    public String getReason() {
        return bean.getString("reason");
    }
}