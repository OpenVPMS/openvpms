/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.domain.internal.service.patient.PatientServices;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.builder.RecordBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder for records that have children.
 *
 * @author Tim Anderson
 */
public abstract class ParentRecordBuilderImpl<R extends Record, RB extends RecordBuilder<R, RB>>
        extends RecordBuilderImpl<R, RB> {

    /**
     * The collected record builders.
     */
    private final List<RecordBuilderImpl<?, ?>> builders = new ArrayList<>();

    /**
     * Constructs a {@link RecordBuilderImpl}.
     *
     * @param archetype the archetype being built
     * @param services  the builder services
     */
    public ParentRecordBuilderImpl(String archetype, PatientServices services) {
        super(archetype, services);
    }

    /**
     * Adds a builder.
     *
     * @param builder the builder
     */
    public void add(RecordBuilderImpl<?, ?> builder) {
        builders.add(builder);
    }

    /**
     * Returns the builders.
     *
     * @return the builders
     */
    protected List<RecordBuilderImpl<?, ?>> getBuilders() {
        return builders;
    }

}
