/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Implementation of {@link DocumentBuilder} for {@link AbstractVersioningDocumentRecordImpl}.
 *
 * @author Tim Anderson
 */
public class DocumentRecordDocumentBuilderImpl extends AbstractDocumentBuilder<DocumentRecordDocumentBuilderImpl> {

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link DocumentRecordDocumentBuilderImpl}.
     *
     * @param act      the parent act
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    public DocumentRecordDocumentBuilderImpl(DocumentAct act, ArchetypeService service, DocumentHandlers handlers,
                                             DocumentRules rules, PlatformTransactionManager transactionManager) {
        super(act, service, handlers, rules);
        this.transactionManager = transactionManager;
    }

    /**
     * Build the document.
     *
     * @return the document
     */
    @Override
    public Document build() {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        return template.execute(transactionStatus -> buildAndCommit());
    }

}
