/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.builder.AbstractDomainObjectBuilder;
import org.openvpms.domain.internal.builder.NodeValue;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.Patient.Sex;
import org.openvpms.domain.service.patient.PatientBuilder;

import java.time.LocalDate;
import java.util.Date;
import java.util.function.Predicate;

import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_OWNER;

/**
 * Default implementation of {@link PatientBuilder}.
 *
 * @author Tim Anderson
 */
public class PatientBuilderImpl extends AbstractDomainObjectBuilder<Patient, Party, PatientBuilderImpl>
        implements PatientBuilder {

    /**
     * The patient rules.
     */
    private final PatientRules rules;

    /**
     * The patient name.
     */
    private final NodeValue name = new NodeValue("name");

    /**
     * The species code.
     */
    private final NodeValue species = new NodeValue("species");

    /**
     * The breed code.
     */
    private final NodeValue breed = new NodeValue("breed");

    /**
     * The patient sex.
     */
    private final NodeValue sex = new NodeValue("sex");

    /**
     * Determines if the patient is desexed.
     */
    private final NodeValue desexed = new NodeValue("desexed");

    /**
     * The patient date of birth.
     */
    private final NodeValue dateOfBirth = new NodeValue("dateOfBirth");

    /**
     * Determines if the patient is deceased.
     */
    private final NodeValue deceased = new NodeValue("deceased");

    /**
     * The patient date of death.
     */
    private final NodeValue deceasedDate = new NodeValue("deceasedDate");

    /**
     * The patient colour.
     */
    private final NodeValue colour = new NodeValue("colour");

    /**
     * The microchip.
     */
    private final NodeValue microchip = new NodeValue("identity");

    /**
     * The active flag.
     */
    private final NodeValue active = new NodeValue("active");

    /**
     * The owner.
     */
    private MutableObject<Customer> owner;

    /**
     * Constructs a {@link PatientBuilder}.
     *
     * @param patientServices the patient services
     */
    public PatientBuilderImpl(PatientServices patientServices) {
        super(Patient.ARCHETYPE, Party.class, Patient.class, patientServices.getArchetypeService(),
              patientServices.getDomainService());
        rules = patientServices.getPatientRules();
    }

    /**
     * Constructs a {@link PatientBuilder}.
     *
     * @param patient         the patient to update
     * @param patientServices the patient services
     */
    public PatientBuilderImpl(Party patient, PatientServices patientServices) {
        super(patient, Patient.class, patientServices.getArchetypeService(), patientServices.getDomainService());
        rules = patientServices.getPatientRules();
    }

    /**
     * Sets the patient name.
     *
     * @param name the patient name
     * @return this
     */
    @Override
    public PatientBuilder name(String name) {
        return setValue(this.name, name);
    }

    /**
     * Sets the patient species.
     *
     * @param species the code for a <em>lookup.species</em>
     * @return this
     */
    @Override
    public PatientBuilder species(String species) {
        return setValue(this.species, species);
    }

    /**
     * Sets the patient species.
     *
     * @param species a <em>lookup.species</em>
     * @return this
     */
    @Override
    public PatientBuilder species(Lookup species) {
        return species(species != null ? species.getCode() : null);
    }

    /**
     * Sets the patient breed.
     *
     * @param breed the code for a <em>lookup.breed</em>
     * @return this
     */
    @Override
    public PatientBuilder breed(String breed) {
        return setValue(this.breed, breed);
    }

    /**
     * Sets the patient breed.
     *
     * @param breed a <em>lookup.breed</em>
     * @return this
     */
    @Override
    public PatientBuilder breed(Lookup breed) {
        return breed(breed != null ? breed.getCode() : null);
    }

    /**
     * Sets the patient sex.
     *
     * @param sex the patient sex
     * @return this
     */
    @Override
    public PatientBuilder sex(Sex sex) {
        return setValue(this.sex, sex != null ? sex.toString() : null);
    }

    /**
     * Determines if the patient has been desexed or not.
     *
     * @param desexed if {@code true}, the patient has been desexed
     * @return this
     */
    @Override
    public PatientBuilder desexed(boolean desexed) {
        return setValue(this.desexed, desexed);
    }

    /**
     * Sets the patient date of birth.
     *
     * @param dateOfBirth the date of birth
     * @return this
     */
    @Override
    public PatientBuilder dateOfBirth(LocalDate dateOfBirth) {
        return setValue(this.dateOfBirth, DateRules.toDate(dateOfBirth));
    }

    /**
     * Determines if the patient is deceased.
     *
     * @param deceased if {@code true}, the patient is deceased
     * @return this
     */
    @Override
    public PatientBuilder deceased(boolean deceased) {
        return setValue(this.deceased, deceased);
    }

    /**
     * Sets the patient date of death.
     *
     * @param dateOfDeath the date of death
     * @return this
     */
    @Override
    public PatientBuilder dateOfDeath(LocalDate dateOfDeath) {
        return setValue(deceasedDate, DateRules.toDate(dateOfDeath));
    }

    /**
     * Sets the patient colour.
     *
     * @param colour the patient colour
     * @return this
     */
    @Override
    public PatientBuilder colour(String colour) {
        return setValue(this.colour, colour);
    }

    /**
     * Sets the patient owner.
     * <p/>
     * If the patient already has an owner relationship, the existing relationship will be ended.
     *
     * @param owner the owner, or {@code null} to end the existing relationship
     * @return this
     */
    @Override
    public PatientBuilder owner(Customer owner) {
        this.owner = new MutableObject<>(owner);
        return this;
    }

    /**
     * Sets the microchip.
     *
     * @param microchip the microchip. If {@code null} removes the existing microchip
     * @return this
     */
    @Override
    public PatientBuilder microchip(String microchip) {
        return setValue(this.microchip, microchip);
    }

    /**
     * Determines if the patient is active.
     *
     * @param active if {@code true}, the patient is active, {@code false} if it is inactive
     * @return this
     */
    @Override
    public PatientBuilder active(boolean active) {
        return setValue(this.active, active);
    }

    /**
     * Builds the object.
     *
     * @param state the object state
     */
    @Override
    protected void build(State state) {
        Party object = state.getObject();
        IMObjectBean bean = state.getBean();
        name.update(bean);
        species.update(bean);
        breed.update(bean);
        breed.update(bean);
        sex.update(bean);
        desexed.update(bean);
        dateOfBirth.update(bean);
        deceased.update(bean);
        if (deceased.getBoolean(bean)) {
            deceasedDate.update(bean);
        } else {
            deceasedDate.update(bean, null);
        }
        colour.update(bean);
        buildMicrochip(object);
        buildOwner(object, bean, state);
        active.update(bean);
    }

    /**
     * Builds a microchip.
     *
     * @param object the patient to update
     */
    private void buildMicrochip(Party object) {
        if (microchip.isSet()) {
            EntityIdentity identity = rules.getMicrochip(object);
            if (microchip.getValue() != null) {
                if (identity == null) {
                    identity = create(PatientArchetypes.MICROCHIP, EntityIdentity.class);
                    object.addIdentity(identity);
                }
                IMObjectBean microchipBean = getBean(identity);
                microchip.update(microchipBean);
            } else if (identity != null) {
                object.removeIdentity(identity);
            }
        }
    }

    /**
     * Builds the owner relationship.
     *
     * @param object the patient
     * @param bean   the bean wrapping the patient
     * @param state  the state
     */
    private void buildOwner(Party object, IMObjectBean bean, State state) {
        if (owner != null) {
            Customer customer = owner.getValue();
            Date now = new Date();
            Predicate<EntityRelationship> predicate = Predicates.<EntityRelationship>isA(PATIENT_OWNER)
                    .and(Predicates.activeAt(now));
            EntityRelationship relationship = bean.getValue("customers", EntityRelationship.class, predicate);
            if (customer != null) {
                if (relationship != null) {
                    relationship.setActive(false);
                }
                rules.addPatientOwnerRelationship(customer, object);
                state.addChanged(customer);
            } else if (relationship != null) {
                relationship.setActive(false);
            }
        }
    }
}