/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.ContactMatcher;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.party.PurposeMatcher;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.CustomerPatients;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.party.ContactablePartyImpl;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.practice.Location;

import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link Customer}.
 *
 * @author Tim Anderson
 */
public class CustomerImpl extends ContactablePartyImpl implements Customer {

    /**
     * The title node.
     */
    private static final String TITLE = "title";

    /**
     * The first name node.
     */
    private static final String FIRST_NAME = "firstName";

    /**
     * The last name node.
     */
    private static final String LAST_NAME = "lastName";

    /**
     * The company name node.
     */
    private static final String COMPANY_NAME = "companyName";

    /**
     * The patients node.
     */
    private static final String PATIENTS = "patients";

    /**
     * The practice node.
     */
    private static final String PRACTICE = "practice";

    /**
     * Constructs a {@link CustomerImpl}.
     *
     * @param peer    the peer to delegate to
     * @param rules   the customer rules
     * @param service the domain object service
     */
    public CustomerImpl(Party peer, CustomerRules rules, DomainService service) {
        super(peer, rules, service);
    }

    /**
     * Constructs a {@link CustomerImpl}.
     *
     * @param bean    the bean wrapping the party
     * @param rules   the party rules
     * @param service the domain object service
     */
    public CustomerImpl(IMObjectBean bean, PartyRules rules, DomainService service) {
        super(bean, rules, service);
    }

    /**
     * Returns the person's title.
     *
     * @return the title. May be {@code null}
     */
    @Override
    public Lookup getTitleLookup() {
        return getBean().getLookup(TITLE);
    }

    /**
     * Returns the person's title.
     */
    @Override
    public String getTitle() {
        Lookup lookup = getTitleLookup();
        return (lookup != null) ? lookup.getName() : null;
    }

    /**
     * Returns the person's title name.
     *
     * @return the title name. May be {@code null}
     * @deprecated use {@link #getTitle()}
     */
    @Deprecated
    @Override
    public String getTitleName() {
        return getTitle();
    }

    /**
     * Returns the person's title code.
     *
     * @return the title code. May be {@code null}
     */
    @Override
    public String getTitleCode() {
        return getBean().getString(TITLE);
    }

    /**
     * Returns the person's first name.
     *
     * @return the first name
     */
    @Override
    public String getFirstName() {
        return getBean().getString(FIRST_NAME);
    }

    /**
     * Returns the person's last name.
     *
     * @return the last name
     */
    @Override
    public String getLastName() {
        return getBean().getString(LAST_NAME);
    }

    /**
     * Returns the person's full name.
     *
     * @return the full name. May be {@code null}
     */
    @Override
    public String getFullName() {
        StringBuilder result = new StringBuilder();
        String title = getTitle();
        String firstName = getFirstName();
        String lastName = getLastName();
        if (title != null) {
            result.append(title);
        }
        if (firstName != null) {
            if (result.length() != 0) {
                result.append(" ");
            }
            result.append(firstName);
        }
        if (lastName != null) {
            if (result.length() != 0) {
                result.append(" ");
            }
            result.append(lastName);
        }
        return result.length() != 0 ? result.toString() : null;
    }

    /**
     * Returns the company name, if this customer is a representative of a company.
     *
     * @return the company name. May be {@code null}
     */
    @Override
    public String getCompanyName() {
        return getBean().hasNode(COMPANY_NAME) ? getBean().getString(COMPANY_NAME) : null;
    }

    /**
     * Returns the person's address.
     *
     * @return the person's address. May be {@code null}
     */
    @Override
    public Address getAddress() {
        return getAddress(ContactArchetypes.BILLING_PURPOSE);
    }

    /**
     * Returns the mailing address.
     * <p/>
     * If a customer has only one address, this will be the same as {@link #getAddress()}.
     *
     * @return the mailing address. May be {@code null}
     */
    @Override
    public Address getMailingAddress() {
        return getAddress(ContactArchetypes.CORRESPONDENCE_PURPOSE);
    }

    /**
     * Returns the work telephone.
     *
     * @return the work telephone. May be {@code null}
     */
    @Override
    public Phone getWorkPhone() {
        return getPhone(ContactArchetypes.WORK_PURPOSE);
    }

    /**
     * Returns the home telephone.
     *
     * @return the home telephone. May be {@code null}
     */
    @Override
    public Phone getHomePhone() {
        Phone phone = getPhone(ContactArchetypes.HOME_PURPOSE);
        if (phone == null) {
            // no phone with a HOME purpose, so use the contact with no purpose if present
            DomainService service = getService();
            Collection<Contact> contacts = Contacts.sort(getPeer().getContacts());
            if (!contacts.isEmpty()) {
                Contact contact = Contacts.find(contacts, new ContactMatcher(Phone.ARCHETYPE, service) {
                    @Override
                    public boolean matches(Contact contact) {
                        boolean result = false;
                        if (super.matches(contact) && PurposeMatcher.hasNoContactPurpose(contact)) {
                            if (isPreferred(contact)) {
                                setMatch(1, contact);
                                result = true;  // exact match
                            } else {
                                setMatch(2, contact);
                            }
                        }
                        return result;
                    }
                });
                if (contact != null) {
                    phone = service.create(contact, Phone.class);
                }
            }
        }
        return phone;
    }

    /**
     * Returns the mobile telephone.
     *
     * @return the mobile telephone. May be {@code null}
     */
    @Override
    public Phone getMobilePhone() {
        return getPhone(ContactArchetypes.MOBILE_PURPOSE);
    }

    /**
     * Returns the customer's patients.
     *
     * @return the patients
     */
    @Override
    public CustomerPatients getPatients() {
        List<EntityRelationship> patients = getBean().getValues(PATIENTS, EntityRelationship.class);
        return getService().createRelated(patients, CustomerPatientsImpl.class);
    }

    /**
     * Returns the customer's preferred practice location.
     *
     * @return the customer's preferred practice location. May be {@code null}
     */
    @Override
    public Location getPractice() {
        Party location = getBean().getTarget(PRACTICE, Party.class);
        return location != null ? getService().create(location, Location.class) : null;
    }
}
