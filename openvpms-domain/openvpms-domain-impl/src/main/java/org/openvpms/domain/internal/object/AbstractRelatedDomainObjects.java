/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.object;

import org.apache.commons.collections4.iterators.AbstractUntypedIteratorDecorator;
import org.openvpms.component.business.service.archetype.helper.DefaultRelatedObjectPolicyBuilder;
import org.openvpms.component.business.service.archetype.helper.ObjectRelationshipImpl;
import org.openvpms.component.business.service.archetype.helper.RelatedIMObjectsImpl;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Order;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.RelatedObjectPolicyBuilder;
import org.openvpms.component.model.bean.RelatedObjects;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;

import java.util.Date;
import java.util.List;

/**
 * Queries related domain objects.
 *
 * @author Tim Anderson
 */
public abstract class AbstractRelatedDomainObjects<T, R extends Relationship,
        P extends RelatedObjects<T, R, P>> implements RelatedObjects<T, R, P> {

    /**
     * The shareable state.
     */
    private final State<T, R> state;

    /**
     * The related objects to adapt.
     */
    private final RelatedIMObjectsImpl<IMObject, R> related;

    /**
     * Constructs a {@link AbstractRelatedDomainObjects}.
     *
     * @param relationships the relationships to adapt
     * @param type          the domain object type
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public AbstractRelatedDomainObjects(List<R> relationships, Class<T> type, boolean source,
                                        DomainService domainService, ArchetypeService service) {
        this(relationships, type, null, source, domainService, service);
    }

    /**
     * Constructs a {@link AbstractRelatedDomainObjects}.
     *
     * @param relationships the relationships to adapt
     * @param type          the domain object type
     * @param fallbackType  the fallback domain object type, if there is no specific domain object type for an IMObject
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param domainService the domain object service
     * @param service       the archetype service
     */
    public AbstractRelatedDomainObjects(List<R> relationships, Class<T> type, Class<? extends T> fallbackType,
                                        boolean source, DomainService domainService, ArchetypeService service) {
        this(new State<>(relationships, type, fallbackType, source, domainService, service), null);
    }

    /**
     * Constructs an {@link AbstractRelatedDomainObjects}.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     */
    protected AbstractRelatedDomainObjects(State<T, R> state, Policy<R> policy) {
        this.state = state;
        this.related = new RelatedIMObjectsImpl<>(state.relationships, IMObject.class, state.source, state.service,
                                                  policy);
    }

    /**
     * Selects all related objects for retrieval.
     * <p/>
     * This is the default.
     *
     * @return an instance that selects all related objects
     */
    @Override
    public P all() {
        return policy(null);
    }

    /**
     * Selects active relationships and objects for retrieval.
     *
     * @return an instance that selects active relationships and objects
     */
    @SuppressWarnings("unchecked")
    @Override
    public P active() {
        return policy((Policy<R>) Policies.active());
    }

    /**
     * Selects relationships active at the specified time and returns active objects.
     *
     * @param time the time
     * @return an instance that selects active relationships at {@code time}, and active objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public P active(Date time) {
        return policy((Policy<R>) Policies.active());
    }

    /**
     * Returns a policy builder to select objects.
     *
     * @return a new policy builder
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedObjectPolicyBuilder<T, R, P> newPolicy() {
        return new DefaultRelatedObjectPolicyBuilder<>((P) this, (Class<R>) Relationship.class);
    }

    /**
     * Selects objects based on the specified policy.
     *
     * @param policy the policy
     * @return an instance that selects objects according to {@code policy}
     */
    @Override
    @SuppressWarnings("unchecked")
    public P policy(Policy<R> policy) {
        return related.hasPolicy(policy) ? (P) this: newInstance(state, policy);
    }

    /**
     * Returns the first object to match the {@link #getPolicy() policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object, or {@code null} if none is found
     */
    @Override
    public T getObject() {
        IMObject object = related.getObject();
        return object != null ? state.create(object) : null;
    }

    /**
     * Returns the first object and relationship to match the {@link #getPolicy() policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object and relationship, or {@code null} if none is found
     */
    @Override
    public ObjectRelationship<T, R> getObjectRelationship() {
        ObjectRelationship<IMObject, R> pair = related.getObjectRelationship();
        if (pair != null) {
            T object = state.create(pair.getObject());
            return new ObjectRelationshipImpl<>(object, pair.getRelationship());
        }
        return null;
    }

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}.
     *
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects() {
        Iterable<IMObject> iterable = related.getObjects();
        return () -> new LazyObjectIterator(iterable);
    }

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects(int firstResult, int maxResults) {
        Iterable<IMObject> iterable = related.getObjects(firstResult, maxResults);
        return () -> new LazyObjectIterator(iterable);
    }

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @param order       the order criteria
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects(int firstResult, int maxResults, Order... order) {
        Iterable<IMObject> iterable = related.getObjects(firstResult, maxResults, order);
        return () -> new LazyObjectIterator(iterable);
    }

    /**
     * Returns the objects and their corresponding relationships matching the {@link #getPolicy() policy}.
     *
     * @return the objects and their corresponding relationships
     */
    @Override
    public Iterable<ObjectRelationship<T, R>> getObjectRelationships() {
        Iterable<ObjectRelationship<IMObject, R>> iterable = related.getObjectRelationships();
        return () -> new LazyObjectRelationshipIterator(iterable);
    }

    /**
     * Returns the objects and their corresponding relationships matching the {@link #getPolicy() policy},
     * supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the objects and their corresponding relationships
     */
    @Override
    public Iterable<ObjectRelationship<T, R>> getObjectRelationships(int firstResult, int maxResults) {
        Iterable<ObjectRelationship<IMObject, R>> iterable = related.getObjectRelationships(firstResult, maxResults);
        return () -> new LazyObjectRelationshipIterator(iterable);
    }

    /**
     * Returns the relationships matching the {@link #getPolicy() policy}.
     *
     * @return the relationships matching the criteria
     */
    @Override
    public List<R> getRelationships() {
        return related.getRelationships();
    }

    /**
     * Returns the relationship references matching the {@link #getPolicy() policy}.
     * <p/>
     * NOTE: this does not determine if the objects are active or inactive
     *
     * @return the related object references
     */
    @Override
    public List<Reference> getReferences() {
        return related.getReferences();
    }

    /**
     * Returns the policy being used to select objects.
     *
     * @return the policy. May be {@code null} to indicate that all objects are returned
     */
    @Override
    public Policy<R> getPolicy() {
        return related.getPolicy();
    }

    /**
     * Creates a new instance with the specified state and policy.
     *
     * @param state  the state
     * @param policy the policy. May be {@code null}
     * @return a new instance
     */
    protected abstract P newInstance(State<T, R> state, Policy<R> policy);

    /**
     * Maintains state that is shareable amongst {@link RelatedDomainObjects} instances.
     */
    protected static class State<T, R extends Relationship> {

        /**
         * The relationships.
         */
        private final List<R> relationships;

        /**
         * The domain object type.
         */
        private final Class<T> type;

        /**
         * The fallback domain object type, if there is no specific domain object type for an IMObject.
         */
        private final Class<? extends T> fallbackType;

        /**
         * If {@code true}, return the source of the relationships, else return the target.
         */
        private final boolean source;

        /**
         * The domain object service.
         */
        private final DomainService domainService;

        /**
         * The archetype service.
         */
        private final ArchetypeService service;


        State(List<R> relationships, Class<T> type, Class<? extends T> fallbackType, boolean source,
              DomainService domainService, ArchetypeService service) {
            this.relationships = relationships;
            this.type = type;
            this.fallbackType = fallbackType;
            this.source = source;
            this.domainService = domainService;
            this.service = service;
        }

        T create(IMObject object) {
            return domainService.create(object, type, fallbackType);
        }
    }

    /**
     * An iterator that adapts {@link IMObject} instances to the domain object type.
     */
    private class LazyObjectIterator extends AbstractUntypedIteratorDecorator<IMObject, T> {

        /**
         * Constructs a {@link RelatedDomainObjects.LazyObjectIterator}.
         *
         * @param iterable the iterable over the {@link IMObject} instances to adapt
         */
        public LazyObjectIterator(Iterable<IMObject> iterable) {
            super(iterable.iterator());
        }

        @Override
        public T next() {
            return state.create(getIterator().next());
        }
    }

    /**
     * An iterator that adapts {@link ObjectRelationship} instances to relationships containing the domain object type.
     */
    private class LazyObjectRelationshipIterator
            extends AbstractUntypedIteratorDecorator<ObjectRelationship<IMObject, R>, ObjectRelationship<T, R>> {

        /**
         * Constructs a {@link RelatedDomainObjects.LazyObjectRelationshipIterator}.
         *
         * @param iterable the iterable over the relationships to adapt
         */
        public LazyObjectRelationshipIterator(Iterable<ObjectRelationship<IMObject, R>> iterable) {
            super(iterable.iterator());
        }

        @Override
        public ObjectRelationship<T, R> next() {
            ObjectRelationship<IMObject, R> relationship = getIterator().next();
            T object = null;
            if (relationship.getObject() != null) {
                object = state.create(relationship.getObject());
            }
            return new ObjectRelationshipImpl<>(object, relationship.getRelationship());
        }
    }
}