/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.product.BaseProduct;
import org.openvpms.domain.product.Batch;
import org.openvpms.domain.service.product.ProductQuery;
import org.openvpms.domain.service.product.Products;

/**
 * Default implementation of {@link Products}
 *
 * @author Tim Anderson
 */
public class ProductsImpl implements Products {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link ProductsImpl}.
     *
     * @param domainService the domain object service
     */
    public ProductsImpl(ArchetypeService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns a product given its identifier.
     *
     * @param id the product identifier
     * @return the corresponding product, or {@code null} if none is found
     */
    @Override
    public Product getProduct(long id) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Product> query = builder.createQuery(Product.class);
        Root<Product> from = query.from(Product.class, ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE,
                                        ProductArchetypes.SERVICE, ProductArchetypes.TEMPLATE);
        // exclude price templates, as these are scheduled to be removed
        query.where(builder.equal(from.get("id"), id));
        Product result = service.createQuery(query).getFirstResult();
        return result != null ? domainService.create(result, BaseProduct.class) : null;
    }

    /**
     * Returns a product query.
     *
     * @return a new query
     */
    @Override
    public ProductQuery getQuery() {
        return new ProductQueryImpl(service, domainService);
    }

    /**
     * Returns a batch for a product.
     *
     * @param product     the product
     * @param batchNumber the batch number
     * @return the corresponding batch, or {@code null} if none is found
     */
    @Override
    public Batch getBatch(Product product, String batchNumber) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> from = query.from(Entity.class, ProductArchetypes.PRODUCT_BATCH);
        Join<Entity, IMObject> join = from.join("product");
        join.on(builder.equal(join.get("target"), product.getObjectReference()));
        query.where(builder.equal(from.get("name"), batchNumber));
        query.orderBy(builder.asc(from.get("id")));
        Entity result = service.createQuery(query).getFirstResult();
        return result != null ? domainService.create(result, Batch.class) : null;
    }
}
