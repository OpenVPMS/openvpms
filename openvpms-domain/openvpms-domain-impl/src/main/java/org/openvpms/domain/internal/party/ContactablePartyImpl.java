/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.party;

import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.component.business.domain.im.party.BeanPartyDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.ContactableParty;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of {@link ContactableParty}.
 *
 * @author Tim Anderson
 */
public class ContactablePartyImpl extends BeanPartyDecorator implements ContactableParty {

    /**
     * The party rules.
     */
    private final PartyRules rules;

    /**
     * The domain object service.
     */
    private final DomainService service;

    /**
     * Constructs a {@link ContactablePartyImpl}.
     *
     * @param peer    the peer to delegate to
     * @param rules   the party rules
     * @param service the domain object service
     */
    public ContactablePartyImpl(Party peer, PartyRules rules, DomainService service) {
        super(peer, service);
        this.rules = rules;
        this.service = service;
    }

    /**
     * Constructs a {@link ContactablePartyImpl}.
     *
     * @param bean    the bean wrapping the party
     * @param rules   the party rules
     * @param service the domain object service
     */
    public ContactablePartyImpl(IMObjectBean bean, PartyRules rules, DomainService service) {
        super(bean);
        this.rules = rules;
        this.service = service;
    }

    /**
     * Returns the contacts.
     *
     * @return the contacts
     */
    @Override
    public Set<Contact> getContacts() {
        Set<Contact> result = new HashSet<>();
        for (Contact contact : super.getContacts()) {
            result.add(service.create(contact, Contact.class));
        }
        return result;
    }

    /**
     * Returns the address.
     *
     * @return the address. May be {@code null}
     */
    @Override
    public Address getAddress() {
        Contact contact = rules.getAddressContact(getPeer(), null);
        return contact != null ? service.create(contact, Address.class) : null;
    }

    /**
     * Returns all of the party's addresses.
     *
     * @return the addresses
     */
    @Override
    public List<Address> getAddresses() {
        return getContacts(Address.ARCHETYPE, Address.class);
    }

    /**
     * Returns the preferred telephone.
     *
     * @return the phone. May be {@code null}
     */
    @Override
    public Phone getPhone() {
        Contact contact = rules.getTelephoneContact(getPeer());
        return contact != null ? service.create(contact, Phone.class) : null;
    }

    /**
     * Returns all of the party's phone numbers.
     *
     * @return the phone numbers
     */
    @Override
    public List<Phone> getPhones() {
        return getContacts(Phone.ARCHETYPE, Phone.class);
    }

    /**
     * Returns the preferred email address.
     *
     * @return the email address. May be {@code null}
     */
    @Override
    public Email getEmail() {
        Contact contact = rules.getEmailContact(getPeer());
        return contact != null ? service.create(contact, Email.class) : null;
    }

    /**
     * Returns all of the party's email addresses.
     *
     * @return the email addresses
     */
    @Override
    public List<Email> getEmails() {
        return getContacts(Email.ARCHETYPE, Email.class);
    }

    /**
     * Returns an address with the given purpose.
     * <p>
     * If it cannot find the specified purpose, it uses the preferred location contact or any location contact if there
     * is no preferred.
     *
     * @param purpose the purpose
     * @return the address. May be {@code null}
     */
    protected Address getAddress(String purpose) {
        Contact contact = rules.getAddressContact(getPeer(), purpose);
        return contact != null ? service.create(contact, Address.class) : null;
    }

    /**
     * Returns a phone with the given purpose.
     *
     * @param purpose the purpose
     * @return the phone. May be {@code null}
     */
    protected Phone getPhone(String purpose) {
        Contact contact = rules.getTelephoneContact(getPeer(), true, purpose);
        return contact != null ? service.create(contact, Phone.class) : null;
    }

    /**
     * Returns the rules.
     *
     * @return the rules
     */
    protected PartyRules getRules() {
        return rules;
    }

    /**
     * Returns the domain object service.
     *
     * @return the domain object service
     */
    protected DomainService getService() {
        return service;
    }

    /**
     * Returns all contacts of the specified archetype and purpose.
     *
     * @param archetype the contact archetype
     * @param purpose   the contact purpose
     * @param type      the contact type
     * @return the matching contacts, sorted on ascending identifier
     */
    protected <T extends Contact> List<T> getContacts(String archetype, String purpose, Class<T> type) {
        List<T> result = new ArrayList<>();
        List<Contact> contacts = rules.getContacts(getPeer(), archetype, true, null, purpose);
        for (Contact contact : contacts) {
            result.add(service.create(contact, type));
        }
        return result;
    }

    /**
     * Returns contacts of the specified archetype.
     *
     * @param archetype the contact archetype
     * @param type      the domain object type
     * @return the corresponding contacts
     */
    private <T extends Contact> List<T> getContacts(String archetype, Class<T> type) {
        List<T> result = new ArrayList<>();
        for (Contact contact : getPeer().getContacts()) {
            if (contact.isA(archetype)) {
                result.add(service.create(contact, type));
            }
        }
        return Contacts.sort(result);
    }

}
