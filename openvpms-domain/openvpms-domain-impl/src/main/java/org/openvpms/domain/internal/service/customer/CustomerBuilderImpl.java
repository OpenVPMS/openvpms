/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.component.business.domain.im.common.Beanable;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.builder.AbstractDomainObjectBuilder;
import org.openvpms.domain.internal.builder.NodeValue;
import org.openvpms.domain.internal.builder.TargetNodeValue;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.service.customer.CustomerBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.openvpms.archetype.rules.party.ContactArchetypes.HOME_PURPOSE;
import static org.openvpms.archetype.rules.party.ContactArchetypes.MOBILE_PURPOSE;
import static org.openvpms.archetype.rules.party.ContactArchetypes.WORK_PURPOSE;

/**
 * Default implementation of {@link CustomerBuilderImpl}.
 *
 * @author Tim Anderson
 */
public class CustomerBuilderImpl extends AbstractDomainObjectBuilder<Customer, Party, CustomerBuilderImpl>
        implements CustomerBuilder {

    /**
     * The customer rules.
     */
    private final CustomerRules rules;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The title node.
     */
    private final NodeValue title = new NodeValue("title");

    /**
     * The first name node.
     */
    private final NodeValue firstName = new NodeValue("firstName");

    /**
     * The last name node.
     */
    private final NodeValue lastName = new NodeValue("lastName");

    /**
     * The company name node.
     */
    private final NodeValue companyName = new NodeValue("companyName");

    /**
     * The address node.
     */
    private final NodeValue address = new NodeValue("address");

    /**
     * The suburb node.
     */
    private final NodeValue suburb = new NodeValue("suburb");

    /**
     * The postcode node.
     */
    private final NodeValue postcode = new NodeValue("postcode");

    /**
     * The state node.
     */
    private final NodeValue state = new NodeValue("state");

    /**
     * The home phone node.
     */
    private final NodeValue homePhone = new NodeValue("telephoneNumber");

    /**
     * The work phone node.
     */
    private final NodeValue workPhone = new NodeValue("telephoneNumber");

    /**
     * The mobile phone node.
     */
    private final NodeValue mobilePhone = new NodeValue("telephoneNumber");

    /**
     * The email node.
     */
    private final NodeValue email = new NodeValue("emailAddress");

    /**
     * The practice node.
     */
    private final TargetNodeValue practice = new TargetNodeValue("practice");

    /**
     * The active node.
     */
    private final NodeValue active = new NodeValue("active");

    /**
     * Constructs a {@link CustomerBuilderImpl}.
     *
     * @param rules         the customer rules
     * @param service       the archetype service
     * @param domainService the domain object service
     * @param lookups       the lookup service
     */
    public CustomerBuilderImpl(CustomerRules rules, ArchetypeService service, DomainService domainService,
                               LookupService lookups) {
        super(CustomerArchetypes.PERSON, Party.class, Customer.class, service, domainService);
        this.rules = rules;
        this.lookups = lookups;
    }

    /**
     * Constructs a {@link CustomerBuilderImpl}.
     *
     * @param object        the object to update
     * @param rules         the customer rules
     * @param service       the archetype service
     * @param domainService the domain object service
     * @param lookups       the lookup service
     */
    public CustomerBuilderImpl(Party object, CustomerRules rules, ArchetypeService service, DomainService domainService,
                               LookupService lookups) {
        super(object, Customer.class, service, domainService);
        this.rules = rules;
        this.lookups = lookups;
    }

    /**
     * Sets the customer title.
     *
     * @param title the code for a <em>lookup.personTitle</em>
     * @return this
     */
    @Override
    public CustomerBuilder title(String title) {
        return setValue(this.title, title);
    }

    /**
     * Sets the customer first name.
     *
     * @param firstName the first name
     * @return this
     */
    @Override
    public CustomerBuilder firstName(String firstName) {
        return setValue(this.firstName, firstName);
    }

    /**
     * Sets the customer last name.
     *
     * @param lastName the last name
     * @return this
     */
    @Override
    public CustomerBuilder lastName(String lastName) {
        return setValue(this.lastName, lastName);
    }

    /**
     * Sets the company name.
     *
     * @param companyName the company name
     * @return this
     */
    @Override
    public CustomerBuilder companyName(String companyName) {
        return setValue(this.companyName, companyName);
    }

    /**
     * Sets the customer address.
     *
     * @param address  the address
     * @param suburb   the code for a <em>lookup.suburb</em>
     * @param postcode the postcode
     * @param state    the code for a <em>lookup.state</em>
     * @return this
     */
    @Override
    public CustomerBuilder address(String address, String suburb, String postcode, String state) {
        setValue(this.address, address);
        setValue(this.suburb, suburb);
        setValue(this.postcode, postcode);
        return setValue(this.state, state);
    }

    /**
     * Sets the customer home phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing home phone
     * @return this
     */
    @Override
    public CustomerBuilder homePhone(String phone) {
        return setValue(homePhone, phone);
    }

    /**
     * Sets the customer work phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing work phone
     * @return this
     */
    @Override
    public CustomerBuilder workPhone(String phone) {
        return setValue(workPhone, phone);
    }

    /**
     * Sets the customer mobile phone.
     *
     * @param phone the phone number, or {@code null} to remove the existing mobile phone
     * @return this
     */
    @Override
    public CustomerBuilder mobilePhone(String phone) {
        return setValue(mobilePhone, phone);
    }

    /**
     * Sets the customer email.
     *
     * @param email the email, or {@code null} to remove the existing email
     * @return this
     */
    @Override
    public CustomerBuilder email(String email) {
        return setValue(this.email, email);
    }

    /**
     * Sets the preferred practice location for the customer.
     *
     * @param location the preferred practice location
     * @return this
     */
    @Override
    public CustomerBuilder practice(Location location) {
        return setValue(practice, location);
    }

    /**
     * Determines if the customer is active.
     *
     * @param active if {@code true}, the customer is active, {@code false} if it is inactive
     * @return this
     */
    @Override
    public CustomerBuilder active(boolean active) {
        return setValue(this.active, active);
    }

    /**
     * Builds the object.
     *
     * @param save if {@code true}, save the object, and any related objects
     * @return the object
     */
    @Override
    public Customer build(boolean save) {
        return super.build(save);
    }

    /**
     * Builds the object.
     *
     * @param state the build state
     */
    @Override
    protected void build(State state) {
        Party object = state.getObject();
        if (object instanceof Beanable) {
            // need to unwrap to avoid domain objects
            object = (Party) ((Beanable) object).getObject();
        }
        IMObjectBean bean = state.getBean();
        title.update(bean);
        firstName.update(bean);
        lastName.update(bean);

        Customer customer = state.getDomainObject();
        updateAddress(customer);
        if (homePhone.isSet() || workPhone.isSet() || mobilePhone.isSet()) {
            Map<String, Contact> inuse = new HashMap<>();
            HashSet<Contact> toRemove = new HashSet<>();
            updatePhone(object, homePhone, HOME_PURPOSE, inuse, toRemove);
            updatePhone(object, workPhone, WORK_PURPOSE, inuse, toRemove);
            updatePhone(object, mobilePhone, MOBILE_PURPOSE, inuse, toRemove);
            toRemove.removeAll(inuse.values());
            toRemove.forEach(object::removeContact); // TODO - should expire these, but currently not supported in UI
        }
        updateEmail(customer);
        practice.update(bean);
        active.update(bean);
    }

    /**
     * Updates a customer's email.
     *
     * @param customer the customer to update
     */
    private void updateEmail(Customer customer) {
        if (email.isSet()) {
            Contact contact = customer.getEmail();
            if (email.getValue() == null) {
                if (contact != null) {
                    customer.removeContact(contact);
                }
            } else {
                if (contact == null) {
                    contact = create(Email.ARCHETYPE, Contact.class);
                    customer.addContact(contact);
                }
                IMObjectBean bean = getBean(contact);
                email.update(bean);
            }
        }
    }

    /**
     * Updates a phone number.
     *
     * @param customer    the customer to update
     * @param value       the phone number value
     * @param purpose     the purpose. One of {@link ContactArchetypes#HOME_PURPOSE},
     *                    {@link ContactArchetypes#WORK_PURPOSE WORK} or {@link ContactArchetypes#MOBILE_PURPOSE}
     * @param populated   phone contacts already populated, keyed on number
     * @param maybeRemove contacts that can be removed, if they aren't used again
     */
    private void updatePhone(Party customer, NodeValue value, String purpose, Map<String, Contact> populated,
                             Set<Contact> maybeRemove) {
        if (value.isSet()) {
            Contact contact = rules.getTelephoneContact(customer, true, purpose);
            String phone = value.toString();
            if (phone == null) {
                if (contact != null) {
                    maybeRemove.add(contact);
                }
            } else {
                Contact match = populated.get(phone);
                if (match == null) {
                    if (contact == null) {
                        contact = create(Phone.ARCHETYPE, Contact.class);
                        customer.addContact(contact);
                    } else {
                        removePurposesExcept(contact, purpose);
                    }
                    IMObjectBean bean = getBean(contact);
                    value.update(bean);
                    populated.put(phone, contact);
                } else {
                    contact = match;
                }
                if (missingPurpose(contact, purpose)) {
                    Lookup lookup = lookups.getLookup(ContactArchetypes.PURPOSE, purpose);
                    if (lookup != null) {
                        contact.addClassification(lookup);
                    }
                }
            }
        }
    }

    /**
     * Determines if a contact is missing a contact purpose.
     *
     * @param contact the contact
     * @param purpose the purpose
     * @return {@code true} if the contact doesn't have the purpose, otherwise {@code false}
     */
    private boolean missingPurpose(Contact contact, String purpose) {
        return contact.getClassifications().stream()
                .noneMatch(lookup -> lookup.getArchetype().equals(ContactArchetypes.PURPOSE)
                                     && lookup.getCode().equals(purpose));
    }

    /**
     * Removes all purposes from a contact, except that given.
     *
     * @param contact the contact
     * @param purpose the purpose to preserve
     */
    private void removePurposesExcept(Contact contact, String purpose) {
        new HashSet<>(contact.getClassifications())
                .stream()
                .filter(l -> l.getArchetype().equals(ContactArchetypes.PURPOSE) && !l.getCode().equals(purpose))
                .forEach(contact::removeClassification);
    }

    /**
     * Updates a customer's address.
     *
     * @param customer the customer to update
     */
    private void updateAddress(Customer customer) {
        if (address.isSet()) {
            Contact contact = customer.getAddress();
            if (contact == null) {
                contact = create(Address.ARCHETYPE, Contact.class);
                customer.addContact(contact);
            }
            IMObjectBean bean = getBean(contact);
            address.update(bean);
            suburb.update(bean);
            postcode.update(bean);
            state.update(bean);
        }
    }
}