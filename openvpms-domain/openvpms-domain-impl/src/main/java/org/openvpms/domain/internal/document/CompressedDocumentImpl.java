/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.document;

import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.document.DocumentDecorator;
import org.openvpms.component.model.document.Document;

import java.io.IOException;
import java.io.InputStream;

/**
 * A {@link Document} that supports compressed content.
 *
 * @author Tim Anderson
 */
public class CompressedDocumentImpl extends DocumentDecorator {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs a {@link CompressedDocumentImpl}.
     *
     * @param peer the peer to delegate to
     */
    public CompressedDocumentImpl(Document peer, DocumentHandlers handlers) {
        super(peer);
        this.handlers = handlers;
    }

    /**
     * Returns the document content.
     *
     * @return the content
     * @throws IOException for any I/O error
     */
    @Override
    public InputStream getContent() throws IOException {
        // return the decompressed content
        DocumentHandler handler = handlers.get(getPeer());
        return handler.getContent(getPeer());
    }

    /**
     * Sets the document content.
     *
     * @param stream a stream of the content
     * @throws IOException for any I/O error
     */
    @Override
    public void setContent(InputStream stream) throws IOException {
        throw new IOException("The content of this document cannot be updated directly");
    }
}
