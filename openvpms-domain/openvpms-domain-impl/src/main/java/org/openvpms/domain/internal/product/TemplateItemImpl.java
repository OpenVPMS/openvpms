/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.domain.product.TemplateItem;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

/**
 * Default implementation of {@link TemplateItem}.
 *
 * @author Tim Anderson
 */
public class TemplateItemImpl implements TemplateItem {

    /**
     * The product.
     */
    private final Product product;

    /**
     * The relationship bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link TemplateItemImpl}.
     *
     * @param product the product
     * @param relationship the template-product relationship
     */
    public TemplateItemImpl(Product product, IMObjectBean relationship) {
        this.product = product;
        this.bean = relationship;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    @Override
    public Product getProduct() {
        return product;
    }

    /**
     * The minimum quantity.
     *
     * @return the minimum quantity
     */
    @Override
    public BigDecimal getLowQuantity() {
        return bean.getBigDecimal("lowQuantity", BigDecimal.ZERO);
    }

    /**
     * The minimum quantity.
     *
     * @return the minimum quantity
     */
    @Override
    public BigDecimal getHighQuantity() {
        return bean.getBigDecimal("highQuantity", BigDecimal.ZERO);
    }

    /**
     * The minimum patient weight that this item applies to.
     *
     * @return the minimum weight, inclusive
     */
    @Override
    public BigDecimal getMinWeight() {
        return bean.getBigDecimal("minWeight", BigDecimal.ZERO);
    }

    /**
     * The maximum patient weight that this item applies to.
     *
     * @return the maximum weight, exclusive
     */
    @Override
    public BigDecimal getMaxWeight() {
        return bean.getBigDecimal("maxWeight", BigDecimal.ZERO);
    }

    /**
     * The weight units.
     *
     * @return the weight units
     */
    @Override
    public WeightUnits getWeightUnits() {
        return WeightUnits.fromString(bean.getString("weightUnits"), WeightUnits.KILOGRAMS);
    }

    /**
     * Determines if inclusion of the item requires a patient weight.
     *
     * @return {@code true} if a patient weight is required, otherwise {@code false}
     */
    @Override
    public boolean requiresWeight() {
        return getMinWeight().compareTo(ZERO) != 0 || getMaxWeight().compareTo(ZERO) != 0;
    }

    /**
     * Determines if a patient weight is in the weight range for this item.
     *
     * @param weight the patient weight
     * @return {@code true} if the patient weight is within the weight range
     */
    @Override
    public boolean inWeightRange(Weight weight) {
        return weight.between(getMinWeight(), getMaxWeight(), getWeightUnits());
    }

    /**
     * Determines if this item should be skipped if the product is not available.
     *
     * @return {@code true} if the item should be skipped, otherwise {@code false}
     */
    @Override
    public boolean getSkipIfMissing() {
        return bean.getBoolean("skipIfMissing");
    }

    /**
     * Determines if this item should have a zero price when charged.
     *
     * @return {@code true} if the item should have a zero price, otherwise {@code false}
     */
    @Override
    public boolean getZeroPrice() {
        return bean.getBoolean("zeroPrice");
    }

    /**
     * Determines if this item should be printed.
     *
     * @return {@code true} if the item should be printed, {@code false} if printing should be suppressed
     */
    @Override
    public boolean getPrint() {
        return bean.getBoolean("print");
    }
}
