/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.component.business.domain.im.product.BeanProductDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.product.BaseProduct;
import org.openvpms.domain.product.ProductType;

/**
 * Default implementation of {@link BaseProduct}.
 *
 * @author Tim Anderson
 */
public class BaseProductImpl extends BeanProductDecorator implements BaseProduct {

    /**
     * The product type.
     */
    private ProductType type;

    /**
     * Constructs a {@link BaseProductImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public BaseProductImpl(Product peer, ArchetypeService service) {
        super(peer, service);
    }

    /**
     * Constructs a {@link BaseProductImpl}.
     *
     * @param bean the bean wrapping the product
     */
    public BaseProductImpl(IMObjectBean bean) {
        super(bean);
    }

    /**
     * Returns the product type.
     *
     * @return the product type. May be {@code null}
     */
    @Override
    public ProductType getType() {
        if (type == null) {
            IMObjectBean bean = getBean();
            Entity entity = bean.getTarget("type", Entity.class);
            if (entity != null) {
                type = new ProductTypeImpl(bean.getBean(entity));
            }
        }
        return type;
    }

    /**
     * Returns the product's printed name.
     *
     * @return the printed name. May be {@code null}
     */
    @Override
    public String getPrintedName() {
        return getBean().getString("printedName");
    }
}
