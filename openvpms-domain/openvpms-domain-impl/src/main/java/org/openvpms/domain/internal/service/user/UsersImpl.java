/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.user.UserQuery;
import org.openvpms.domain.service.user.Users;

/**
 * Default implementation of {@link Users}.
 *
 * @author Tim Anderson
 */
public class UsersImpl implements Users {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link UsersImpl}.
     *
     * @param domainService the domain object service
     */
    public UsersImpl(ArchetypeService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns a user given its identifier.
     *
     * @param id the user identifier
     * @return the corresponding user, or {@code null} if none is found
     */
    @Override
    public User getUser(long id) {
        User user = service.get(UserArchetypes.USER, id, User.class);
        return user != null ? UserFactory.create(user, domainService) : null;
    }

    /**
     * Returns a user given its username.
     *
     * @param username the username
     * @return the corresponding user, or {@code null} if none is found
     */
    @Override
    public User getUser(String username) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> from = query.from(User.class, UserArchetypes.USER);
        query.where(builder.equal(from.get("username"), username));
        User user = service.createQuery(query).getFirstResult();
        return user != null ? UserFactory.create(user, domainService) : null;
    }

    /**
     * Creates a new user query.
     *
     * @return a new query
     */
    @Override
    public UserQuery getQuery() {
        return new UserQueryImpl(service, domainService);
    }
}
