/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.domain.customer.transaction.TransactionItem;
import org.openvpms.domain.internal.factory.DomainService;

import java.math.BigDecimal;

/**
 * Default implementation of {@link TransactionImpl}.
 *
 * @author Tim Anderson
 */
public abstract class TransactionItemImpl implements TransactionItem {

    /**
     * The item.
     */
    private final FinancialAct item;

    /**
     * The bean wrapping the transaction.
     */
    private final IMObjectBean bean;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link TransactionItemImpl}.
     *
     * @param item          the item
     * @param domainService the domain service
     */
    public TransactionItemImpl(FinancialAct item, DomainService domainService) {
        this.item = item;
        this.bean = domainService.getBean(item);
        this.domainService = domainService;
    }

    /**
     * Constructs a {@link TransactionItemImpl}.
     *
     * @param bean          a bean wrapping the item
     * @param domainService the domain service
     */
    public TransactionItemImpl(IMObjectBean bean, DomainService domainService) {
        this.item = bean.getObject(FinancialAct.class);
        this.bean = bean;
        this.domainService = domainService;
    }

    /**
     * Returns the OpenVPMS identifier for this item.
     *
     * @return the identifier
     */
    @Override
    public long getId() {
        return item.getId();
    }

    /**
     * Returns the total amount, including tax.
     *
     * @return the total amount
     */
    @Override
    public BigDecimal getTotal() {
        return item.getTotal();
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TransactionItemImpl) {
            return item.equals(((TransactionItemImpl) obj).item);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return item.hashCode();
    }

    /**
     * Returns the underlying act.
     *
     * @return the act
     */
    protected FinancialAct getAct() {
        return item;
    }

    /**
     * Returns the bean wrapping the item.
     *
     * @return the bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }

    /**
     * Returns the domain object service.
     *
     * @return the domain object service
     */
    protected DomainService getDomainService() {
        return domainService;
    }
}
