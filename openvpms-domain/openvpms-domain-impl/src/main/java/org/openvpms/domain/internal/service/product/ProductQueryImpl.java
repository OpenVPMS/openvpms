/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.product.BaseProduct;
import org.openvpms.domain.service.product.ProductQuery;

/**
 * Default implementation of {@link ProductQueryImpl}.
 *
 * @author Tim Anderson
 */
public class ProductQueryImpl extends AbstractProductQueryImpl<BaseProduct, ProductQuery> implements ProductQuery {

    /**
     * Constructs a {@link AbstractProductQueryImpl}.
     *
     * @param service       the service
     * @param domainService the domain service
     */
    public ProductQueryImpl(ArchetypeService service, DomainService domainService) {
        super(new ProductQueryState<>(BaseProduct.class, service, domainService));
    }

}
