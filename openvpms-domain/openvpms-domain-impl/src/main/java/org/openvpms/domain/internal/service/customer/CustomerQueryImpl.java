/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.AddressFilter;
import org.openvpms.domain.internal.query.PartyQueryImpl;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.customer.CustomerQuery;

import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 * Default implementation of {@link CustomerQuery}.
 * <p/>
 * NOTE: any contact-based queries are likely to have poor performance due to lack of indexes. TODO
 *
 * @author Tim Anderson
 */
public class CustomerQueryImpl extends PartyQueryImpl<Customer, Party, CustomerQuery> implements CustomerQuery {

    /**
     * The address filter.
     */
    private AddressFilter address;

    /**
     * The email filter.
     */
    private Filter<String> email;

    /**
     * The phone filter.
     */
    private Filter<String> phone;

    /**
     * Constructs  a {@link CustomerQueryImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public CustomerQueryImpl(ArchetypeService service, DomainService domainService) {
        super(Customer.ARCHETYPE, Customer.class, Party.class, service, domainService);
    }

    /**
     * Filter by name.
     *
     * @param name the name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery name(Filter<String> name) {
        return name(name, null);
    }

    /**
     * Filter customers by last and first name.
     *
     * @param lastName  the customer last name. May be {@code null} to reset the criteria
     * @param firstName the customer first name. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery name(String lastName, String firstName) {
        return name(lastName != null ? Filter.equal(lastName) : null,
                    (firstName != null) ? Filter.equal(firstName) : null);
    }

    /**
     * Filter customers by last and first name.
     * <p/>
     * Only 'equal' and 'like' filters are supported, and the operator must be the same for each filter.
     *
     * @param lastName  the customer last name criteria. May be {@code null} to reset the criteria
     * @param firstName the customer first name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery name(Filter<String> lastName, Filter<String> firstName) {
        Filter<String> name = (lastName != null) ? createNameFilter(lastName, firstName) : null;
        return super.name(name);
    }

    /**
     * Filter customers by address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery address(String address, String suburb, String postcode, String state) {
        this.address = AddressFilter.create(address, suburb, postcode, state);
        return this;
    }

    /**
     * Filter customers by address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery address(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                                 Filter<String> state) {
        this.address = AddressFilter.create(address, suburb, postcode, state);
        return this;
    }

    /**
     * Filter customers by email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery email(String email) {
        return email((email != null) ? Filter.equal(email) : null);
    }

    /**
     * Filter customers by email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery email(Filter<String> email) {
        this.email = email;
        return this;
    }

    /**
     * Filter customers by phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery phone(String phone) {
        return phone((phone != null) ? Filter.equal(phone) : null);
    }

    /**
     * Filter customers by phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public CustomerQuery phone(Filter<String> phone) {
        this.phone = phone;
        return this;
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param query      the query
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    @Override
    protected void addPredicates(List<Predicate> predicates, CriteriaQuery<Party> query, Root<Party> from,
                                 CriteriaBuilder builder) {
        super.addPredicates(predicates, query, from, builder);
        if (address != null) {
            addAddressPredicate(address, predicates, query, from, builder);
        }
        if (email != null) {
            addEmailPredicate(email, predicates, from, builder);
        }
        if (phone != null) {
            addPhonePredicate(phone, predicates, from, builder);
        }
    }
}