/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.internal.document.CompressedDocumentImpl;
import org.openvpms.domain.internal.factory.DomainService;

/**
 * Base class for {@link DocumentAct}-based patient records that need to access the document.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDocumentActRecordImpl extends AbstractRecordImpl {

    /**
     * Constructs an {@link AbstractDocumentActRecordImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    protected AbstractDocumentActRecordImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the document.
     *
     * @return the document. May be {@code null}
     */
    public Document getDocument() {
        Document result = null;
        Reference reference = getPeer().getDocument();
        if (reference != null) {
            DomainService service = getService();
            result = service.get(reference, CompressedDocumentImpl.class);
        }
        return result;
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected DocumentAct getPeer() {
        return (DocumentAct) super.getPeer();
    }
}
