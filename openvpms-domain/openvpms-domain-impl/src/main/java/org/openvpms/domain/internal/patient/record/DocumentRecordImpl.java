/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.VersioningDocumentRecord;

/**
 * Default implementation of {@link VersioningDocumentRecord}.
 *
 * @author Tim Anderson
 */
public class DocumentRecordImpl extends AbstractVersioningDocumentRecordImpl {

    /**
     * Constructs a {@link DocumentRecordImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public DocumentRecordImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }
}
