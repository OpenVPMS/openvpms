/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record.builder;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashSet;
import java.util.Set;

/**
 * Build state to be shared among builders.
 *
 * @author Tim Anderson
 */
public class BuildContext {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Objects that need to be saved.
     */
    private final Set<IMObject> changes = new HashSet<>();

    /**
     * Objects that need to be removed.
     */
    private final Set<Reference> remove = new HashSet<>();

    /**
     * Constructs a {@link BuildContext}.
     *
     * @param service the archetype service
     */
    public BuildContext(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Queues a new or updated object for saving.
     *
     * @param object the changed object
     */
    public void addChange(IMObject object) {
        changes.add(object);
    }

    /**
     * Queues an object for removal.
     *
     * @param reference the object reference
     */
    public void remove(Reference reference) {
        remove.add(reference);
    }

    /**
     * Saves changes.
     * <p/>
     * This should be invoked in a transaction.
     */
    public void save() {
        service.save(changes);
        for (Reference reference : remove) {
            service.remove(reference);
        }
    }
}
