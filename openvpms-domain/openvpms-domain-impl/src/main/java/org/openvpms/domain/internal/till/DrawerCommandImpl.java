/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.till;

import org.openvpms.domain.till.DrawerCommand;

/**
 * Default implementation of {@link DrawerCommand}.
 *
 * @author Tim Anderson
 */
class DrawerCommandImpl implements DrawerCommand {

    /**
     * The printer identifier.
     */
    private final String printer;

    /**
     * The print service archetype.
     */
    private final String archetype;

    /**
     * The command to open the till.
     */
    private final byte[] command;

    /**
     * Constructs a {@link DrawerCommand}.
     *
     * @param printer   the printer identifier
     * @param archetype the print service archetype. May be {@code null}
     * @param command   the command to open the till
     */
    public DrawerCommandImpl(String printer, String archetype, byte[] command) {
        this.printer = printer;
        this.archetype = archetype;
        this.command = command;
    }

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    @Override
    public String getPrinter() {
        return printer;
    }

    /**
     * Returns the archetype of the document printer service that manages the printer.
     *
     * @return the archetype, or {@code null} for the Java Print API
     */
    @Override
    public String getPrinterServiceArchetype() {
        return archetype;
    }

    /**
     * Returns the command.
     *
     * @return the command
     */
    @Override
    public byte[] getCommand() {
        return command;
    }
}
