/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.referral;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.referral.Referral;
import org.openvpms.domain.patient.referral.Referrals;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link Referrals}.
 *
 * @author Tim Anderson
 */
public class ReferralsImpl implements Referrals {

    /**
     * The patient bean.
     */
    private final IMObjectBean bean;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link ReferralsImpl}.
     *
     * @param bean          the patient bean
     * @param domainService the domain service
     */
    public ReferralsImpl(IMObjectBean bean, DomainService domainService) {
        this.bean = bean;
        this.domainService = domainService;
    }

    /**
     * Returns the current referral.
     *
     * @return the current referral, or {@code null} if the patient has none
     */
    @Override
    public Referral getCurrent() {
        return getReferral(new Date());
    }

    /**
     * Returns where the patient was referred from.
     * <p/>
     * If the patient has been referred from other practices multiple times, this represents the most recent referral.
     *
     * @return the referral. May be {@code null}. If present, may be inactive.
     */
    @Override
    public Referral getReferredFrom() {
        return getReferredFrom(false);
    }

    /**
     * Returns where the patient was referred from.
     * <p/>
     * If the patient has been referred from other practices multiple times, this represents the most recent referral.
     *
     * @param activeOnly if {@code true}, only return the referral if it is current
     * @return the referral. May be {@code null}
     */
    @Override
    public Referral getReferredFrom(boolean activeOnly) {
        return getMostRecent(PatientArchetypes.REFERRED_FROM, activeOnly);
    }

    /**
     * Returns where the patient was referred to.
     * <p/>
     * If the patient has been referred to other practices multiple times, this represents the most recent referral.
     *
     * @return the referral. May be {@code null}. If present, may be inactive.
     */
    @Override
    public Referral getReferredTo() {
        return getReferredTo(false);
    }

    /**
     * Returns where the patient was referred to.
     * <p/>
     * If the patient has been referred to other practices multiple times, this represents the most recent referral.
     *
     * @param activeOnly if {@code true}, only return the referral if it is current
     * @return the referral. May be {@code null}
     */
    @Override
    public Referral getReferredTo(boolean activeOnly) {
        return getMostRecent(PatientArchetypes.REFERRED_TO, activeOnly);
    }

    /**
     * Returns the referral as at the specified date.
     *
     * @param date the referral date
     * @return the referral or {@code null} if the patient has no referral as at the date
     */
    @Override
    public Referral getReferral(OffsetDateTime date) {
        return getReferral(DateRules.toDate(date));
    }

    /**
     * Returns all referrals for the patient
     *
     * @return all referrals for the patient, sorted most recent to oldest
     */
    @Override
    public List<Referral> getAll() {
        List<Referral> result = Collections.emptyList();
        List<PeriodRelationship> relationships = bean.getValues("referrals", PeriodRelationship.class);
        if (!relationships.isEmpty()) {
            sort(relationships);
            result = relationships.stream().map(relationship -> new ReferralImpl(relationship, domainService))
                    .collect(Collectors.toList());
        }
        return result;
    }

    /**
     * Returns the most recent referral of the specified archetype.
     *
     * @param archetype the archetype
     * @param activeOnly if {@code true}, only return the referral if it is active now
     * @return the most recent referral. May be {@code null}
     */
    private Referral getMostRecent(String archetype, boolean activeOnly) {
        Referral result = null;
        Predicate<PeriodRelationship> predicate;
        if (activeOnly) {
            predicate = Predicates.<PeriodRelationship>activeNow().and(Predicates.isA(archetype));
        } else {
            predicate = Predicates.isA(archetype);
        }
        List<PeriodRelationship> relationships
                = bean.getValues("referrals", PeriodRelationship.class, predicate);
        if (!relationships.isEmpty()) {
            sort(relationships);
            result = new ReferralImpl(relationships.get(0), domainService);
        }
        return result;
    }

    /**
     * Sorts relationships, most recent relationship first.
     *
     * @param relationships the relationships
     */
    private void sort(List<PeriodRelationship> relationships) {
        if (relationships.size() > 1) {
            // sort on decreasing date
            relationships.sort(((Comparator<PeriodRelationship>) (o1, o2)
                    -> DateRules.compareTo(o1.getActiveStartTime(), o2.getActiveStartTime()))
                    .thenComparingLong(IMObject::getId)
                    .reversed());
            // dates should never match, but sort on id for reproducibility
        }
    }

    /**
     * Returns the referral active at the specified date.
     *
     * @param date the date
     * @return the corresponding referral, or {@code null} if there is none
     */
    private Referral getReferral(Date date) {
        PeriodRelationship relationship
                = bean.getValue("referrals", PeriodRelationship.class, Predicates.activeAt(date));
        return (relationship != null) ? new ReferralImpl(relationship, domainService) : null;
    }
}