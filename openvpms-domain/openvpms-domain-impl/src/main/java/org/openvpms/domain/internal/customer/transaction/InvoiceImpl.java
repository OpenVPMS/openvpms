/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.customer.transaction;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.domain.customer.transaction.Invoice;
import org.openvpms.domain.customer.transaction.InvoiceItem;
import org.openvpms.domain.internal.factory.DomainService;

/**
 * Default implementation of {@link Invoice}.
 *
 * @author Tim Anderson
 */
public class InvoiceImpl extends ChargeImpl<InvoiceItem> implements Invoice {

    /**
     * Constructs an {@link InvoiceImpl}.
     *
     * @param charge        the charge
     * @param domainService the domain service
     */
    public InvoiceImpl(IMObjectBean charge, DomainService domainService) {
        super(charge, InvoiceItem.class, domainService);
    }

    /**
     * Constructs an {@link InvoiceImpl}.
     *
     * @param charge        the charge
     * @param domainService the domain service
     */
    public InvoiceImpl(FinancialAct charge, DomainService domainService) {
        super(charge, InvoiceItem.class, domainService);
    }
}
