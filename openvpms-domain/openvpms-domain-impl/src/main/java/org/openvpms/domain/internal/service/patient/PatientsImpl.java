/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.domain.internal.patient.record.builder.VisitBuilderImpl;
import org.openvpms.domain.internal.query.IdQuery;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.patient.record.builder.VisitBuilder;
import org.openvpms.domain.service.patient.PatientBuilder;
import org.openvpms.domain.service.patient.PatientQuery;
import org.openvpms.domain.service.patient.Patients;

/**
 * Default implementation of {@link Patients}.
 *
 * @author Tim Anderson
 */
public class PatientsImpl implements Patients {

    /**
     * The patient services.
     */
    private final PatientServices services;

    /**
     * Helper to query records by external id.
     */
    private final IdQuery query;

    /**
     * Constructs a {@link PatientsImpl}.
     *
     * @param services the patient services
     */
    public PatientsImpl(PatientServices services) {
        this.services = services;
        query = new IdQuery(services.getArchetypeService());
    }

    /**
     * Returns a patient given its identifier.
     *
     * @param id the patient identifier
     * @return the corresponding patient, or {@code null} if none is found
     */
    @Override
    public Patient getPatient(long id) {
        return services.getDomainService().get(PatientArchetypes.PATIENT, id, Patient.class);
    }

    /**
     * Returns a patient query.
     *
     * @return a new query
     */
    @Override
    public PatientQuery getQuery() {
        return new PatientQueryImpl(services.getArchetypeService(), services.getDomainService());
    }

    /**
     * Returns a builder for a patient.
     *
     * @return a patient builder
     */
    @Override
    public PatientBuilder getPatientBuilder() {
        return new PatientBuilderImpl(services);
    }

    /**
     * Returns a builder to update a patient.
     *
     * @param patient the patient to update
     * @return a patient builder
     */
    @Override
    public PatientBuilder getPatientBuilder(Patient patient) {
        return new PatientBuilderImpl(patient, services);
    }

    /**
     * Returns a builder for a patient visit.
     * <p/>
     * If there is an existing vist for the patient that may have records added, records will be added to it, else a
     * new visit will be created.
     *
     * @return a visit builder
     */
    @Override
    public VisitBuilder getVisitBuilder() {
        return new VisitBuilderImpl(services);
    }

    /**
     * Returns a builder to update a patient visit.
     *
     * @param visit the visit to update
     * @return a visit builder
     */
    @Override
    public VisitBuilder getVisitBuilder(Visit visit) {
        return new VisitBuilderImpl(visit, services);
    }

    /**
     * Returns a record given its archetype and external identity.
     *
     * @param archetype   the record archetype
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return the corresponding record, or {@code null} if none is found
     */
    @Override
    public Record getRecord(String archetype, String idArchetype, String identity) {
        Act act = query.getObject(archetype, Act.class, idArchetype, identity);
        return (act != null) ? services.getDomainService().create(act, Record.class) : null;
    }

    /**
     * Determines if a record exists.
     *
     * @param archetype   the record archetype
     * @param idArchetype the identity archetype
     * @param identity    the identity
     * @return {@code true} if the record exists, otherwise {@code false}
     */
    @Override
    public boolean exists(String archetype, String idArchetype, String identity) {
        return query.exists(archetype, Act.class, idArchetype, identity);
    }
}
