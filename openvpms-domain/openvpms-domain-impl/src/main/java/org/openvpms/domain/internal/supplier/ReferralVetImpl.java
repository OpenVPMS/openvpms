/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.supplier;

import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.party.ContactablePartyImpl;
import org.openvpms.domain.supplier.ReferralPractice;
import org.openvpms.domain.supplier.ReferralVet;

import java.time.OffsetDateTime;
import java.util.Date;

/**
 * Default implementation of {@link ReferralVet}.
 *
 * @author Tim Anderson
 */
public class ReferralVetImpl extends ContactablePartyImpl implements ReferralVet {

    /**
     * Constructs a {@link ReferralVetImpl}.
     *
     * @param peer    the peer to delegate to
     * @param rules   the party rules
     * @param service the domain object service
     */
    public ReferralVetImpl(Party peer, PartyRules rules, DomainService service) {
        super(peer, rules, service);
    }

    /**
     * Constructs a {@link ReferralVetImpl}.
     *
     * @param bean    the bean wrapping the party
     * @param rules   the party rules
     * @param service the domain object service
     */
    public ReferralVetImpl(IMObjectBean bean, PartyRules rules, DomainService service) {
        super(bean, rules, service);
    }

    /**
     * Returns the vet's title.
     *
     * @return the title. May be {@code null}
     */
    @Override
    public String getTitle() {
        Lookup title = getBean().getLookup("title");
        return title != null ? title.getName() : null;
    }

    /**
     * Returns the vet's first name.
     *
     * @return the first name
     */
    @Override
    public String getFirstName() {
        return getBean().getString("firstName");
    }

    /**
     * Returns the vet's last name.
     *
     * @return the last name
     */
    @Override
    public String getLastName() {
        return getBean().getString("lastName");
    }

    /**
     * Returns the vet's current practice.
     *
     * @return the practice. May be {@code null}. If present, the practice may be inactive.
     */
    @Override
    public ReferralPractice getPractice() {
        return getPractice(new Date());
    }

    /**
     * Returns the vet's practice, as at the specified date.
     *
     * @param date the date
     * @return the practice. May be {@code null}. If present, the practice may be inactive.
     */
    @Override
    public ReferralPractice getPractice(OffsetDateTime date) {
        return getPractice(DateRules.toDate(date));
    }

    /**
     * Returns the vet's practice, as at the specified date.
     *
     * @param date the date
     * @return the practice. May be {@code null}. If present, the practice may be inactive.
     */
    private ReferralPractice getPractice(Date date) {
        Party practice = getBean().getSource("practices", Party.class, Policies.any(Predicates.activeAt(date)));
        return practice != null ? getService().create(practice, ReferralPractice.class) : null;
    }
}
