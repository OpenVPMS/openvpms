/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.DomainQueryState;
import org.openvpms.domain.query.Filter;

/**
 * Product query state.
 *
 * @author Tim Anderson
 */
class ProductQueryState<D> extends DomainQueryState<D, Product> {

    /**
     * Product type name filter.
     */
    private Filter<String> productTypeName;

    /**
     * Product type id filter.
     */
    private Filter<Long> productTypeId;

    /**
     * Constructs a {@link ProductQueryState}.
     *
     * @param domainType    the domain type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public ProductQueryState(Class<D> domainType, ArchetypeService service, DomainService domainService) {
        super(domainType, Product.class, service, domainService);
    }

    /**
     * Returns the product type name filter.
     *
     * @return the filter. May be {@code null}
     */
    public Filter<String> getProductTypeName() {
        return productTypeName;
    }

    /**
     * Sets the product type name filter.
     *
     * @param productTypeName the filter
     */
    public void setProductTypeName(Filter<String> productTypeName) {
        this.productTypeName = productTypeName;
    }

    /**
     * Returns the product type id filter.
     *
     * @return the filter. May be {@code null}
     */
    public Filter<Long> getProductTypeId() {
        return productTypeId;
    }

    /**
     * Sets the product type id filter.
     *
     * @param productTypeId the filter}
     */
    public void setProductTypeId(Filter<Long> productTypeId) {
        this.productTypeId = productTypeId;
    }

    /**
     * Creates a new state object, with details copied from this.
     *
     * @param domainType the domain type
     * @return a new state object
     */
    @Override
    public <D2 extends IMObject> ProductQueryState<D2> newState(Class<D2> domainType) {
        ProductQueryState<D2> result = new ProductQueryState<>(domainType, getService(), getDomainService());
        populate(result);
        return result;
    }

    /**
     * Populates the supplied state.
     *
     * @param state the state to populate
     */
    @Override
    protected void populate(DomainQueryState<?, ?> state) {
        super.populate(state);
        ProductQueryState<?> other = (ProductQueryState<?>) state;
        other.setProductTypeName(productTypeName);
        other.setProductTypeId(productTypeId);
    }
}
