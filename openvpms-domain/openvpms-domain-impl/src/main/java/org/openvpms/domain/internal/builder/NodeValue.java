/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.builder;

import org.apache.commons.lang.ObjectUtils;
import org.openvpms.component.model.bean.IMObjectBean;

/**
 * Helper to defer updates to a node on a {@link IMObjectBean}.
 *
 * @author Tim Anderson
 */
public class NodeValue {

    /**
     * The node name.
     */
    private final String name;

    /**
     * The value.
     */
    private Object value;

    /**
     * Determines if the value has been set.
     */
    private boolean set;

    /**
     * Constructs a {@link NodeValue}.
     *
     * @param name the node name
     */
    public NodeValue(String name) {
        this.name = name;
    }

    /**
     * Returns the node name.
     *
     * @return the node name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value.
     *
     * @param value the value. May be {@code null}
     */
    public void setValue(Object value) {
        this.value = value;
        set = true;
    }

    /**
     * Returns the value.
     *
     * @return the value. May be {@code null}
     */
    public Object getValue() {
        return value;
    }

    /**
     * Returns the stringified version of {@link #getValue()}.
     *
     * @return the string value. May be {@code null}
     */
    @Override
    public String toString() {
        return value != null ? value.toString() : null;
    }

    /**
     * Determines if a value has been set.
     *
     * @return {@code true} if a value has been set, otherwise {@code false}
     */
    public boolean isSet() {
        return set;
    }

    /**
     * Resets this.
     * <br/>
     * This sets the {@link #getValue() value} to {@code null}, and {@link #isSet()} will now return {@code false}
     */
    public void reset() {
        this.value = null;
        set = false;
    }

    /**
     * Populates the node with the value, if one has been set via {@link #setValue(Object)}, else leaves it unchanged.
     *
     * @param bean the bean to populate
     * @return {@code true} if the node was updated, otherwise {@code false}
     */
    public boolean update(IMObjectBean bean) {
        boolean result = false;
        if (set) {
            result = update(bean, value);
        }
        return result;
    }

    /**
     * Populates the node with the specified value.
     *
     * @param bean  the bean to populate
     * @param value the value to set. May be {@code null}
     * @return {@code true} if the value was updated, otherwise {@code false}
     */
    public boolean update(IMObjectBean bean, Object value) {
        boolean result = false;
        Object existing = bean.getValue(name);
        if (!ObjectUtils.equals(existing, value)) {
            bean.setValue(name, value);
            result = true;
        }
        return result;
    }

    /**
     * Helper to return the boolean value of a node.
     *
     * @param bean the bean
     * @return the value of the node, or {@code false} if the node is null
     */
    public boolean getBoolean(IMObjectBean bean) {
        return bean.getBoolean(name);
    }
}