/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.product;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.product.Merchandise;

/**
 * Default implementation of {@link Merchandise}.
 *
 * @author Tim Anderson
 */
public class MerchandiseImpl extends BaseProductImpl implements Merchandise {

    /**
     * Constructs a {@link MerchandiseImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public MerchandiseImpl(Product peer, ArchetypeService service) {
        super(peer, service);
    }

    /**
     * Constructs a {@link MerchandiseImpl}.
     *
     * @param bean the bean wrapping the product
     */
    public MerchandiseImpl(IMObjectBean bean) {
        super(bean);
    }
}
