/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.party;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.PurposeMatcher;
import org.openvpms.component.business.domain.im.party.BeanContactDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Phone;

/**
 * Default implementation of {@link Phone}.
 *
 * @author Tim Anderson
 */
public class PhoneImpl extends BeanContactDecorator implements Phone {

    /**
     * Constructs a {@link PhoneImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public PhoneImpl(Contact peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the phone number.
     *
     * @return the phone number. May be {@code null}
     */
    @Override
    public String getPhoneNumber() {
        IMObjectBean bean = getBean();
        String areaCode = bean.getString("areaCode");
        String phone = bean.getString("telephoneNumber");
        return Contacts.getPhone(areaCode, phone);
    }

    /**
     * Determines if this is the preferred phone contact.
     *
     * @return {@code true} if this is the preferred phone contact
     */
    @Override
    public boolean isPreferred() {
        return getBean().getBoolean("preferred");
    }

    /**
     * Determines if this a mobile phone.
     *
     * @return {@code true} if this is a mobile phone
     */
    @Override
    public boolean isMobile() {
        return PurposeMatcher.hasContactPurpose(getPeer(), ContactArchetypes.MOBILE_PURPOSE);
    }

    /**
     * Determines if this a fax.
     *
     * @return {@code true} if this is a fax
     */
    @Override
    public boolean isFax() {
        return PurposeMatcher.hasContactPurpose(getPeer(), ContactArchetypes.FAX_PURPOSE);
    }
}
