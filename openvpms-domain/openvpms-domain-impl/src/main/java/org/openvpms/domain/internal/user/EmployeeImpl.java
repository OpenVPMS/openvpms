/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.user;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.domain.im.security.BeanUserDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.user.Employee;

/**
 * Default implementation of {@link EmployeeImpl}.
 *
 * @author Tim Anderson
 */
public class EmployeeImpl extends BeanUserDecorator implements Employee {

    /**
     * First name node.
     */
    public static final String FIRST_NAME = "firstName";

    /**
     * Last name node.
     */
    public static final String LAST_NAME = "lastName";

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * Constructs an {@link EmployeeImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public EmployeeImpl(User peer, ArchetypeService service, UserRules userRules) {
        super(peer, service);
        this.userRules = userRules;
    }

    /**
     * Constructs a {@link EmployeeImpl}.
     *
     * @param bean the bean wrapping the user
     */
    public EmployeeImpl(IMObjectBean bean, UserRules userRules) {
        super(bean);
        this.userRules = userRules;
    }

    /**
     * Returns the employee's title.
     *
     * @return the title. May be {@code null}
     */
    @Override
    public String getTitle() {
        Lookup title = getBean().getLookup("title");
        return title != null ? title.getName() : null;
    }

    /**
     * Returns the employee's first name.
     *
     * @return the first name
     */
    @Override
    public String getFirstName() {
        return getBean().getString(FIRST_NAME);
    }

    /**
     * Returns the employee's last name.
     *
     * @return the last name
     */
    @Override
    public String getLastName() {
        return getBean().getString(LAST_NAME);
    }

    /**
     * Returns the employee's qualifications.
     *
     * @return the qualifications. May be {@code null}
     */
    @Override
    public String getQualifications() {
        return getBean().getString("qualifications");
    }

    /**
     * Determines if the employee is a clinician.
     *
     * @return {@code true} if the employee is a clinician, otherwise {@code false}
     */
    @Override
    public boolean isClinician() {
        return userRules.isClinician(this);
    }

    /**
     * Determines if a user is an employee.
     * <p/>
     * This assumes that users with a firstName and lastName specified are employees, rather than system users.
     * <p/>
     * TODO - need a better way to distinguish between employees and system users.
     *
     * @param bean a bean wrapping the user
     * @return {@code true} if the user is an employee, otherwise {@code false}
     */
    public static boolean isEmployee(IMObjectBean bean) {
        return bean.getString(FIRST_NAME) != null && bean.getString(LAST_NAME) != null;
    }
}
