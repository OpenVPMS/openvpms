/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.till;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.till.DrawerCommand;
import org.openvpms.domain.till.Till;

/**
 * Default implementation of {@link Till}.
 *
 * @author Tim Anderson
 */
public class TillImpl extends BeanEntityDecorator implements Till {

    /**
     * Constructs a {@link TillImpl}.
     *
     * @param peer    the peer to delegate to
     * @param factory the domain object factory
     */
    public TillImpl(Entity peer, DomainService factory) {
        super(peer, factory);
    }

    /**
     * Creates a {@link TillImpl}.
     *
     * @param bean the bean wrapping the entity
     */
    public TillImpl(IMObjectBean bean) {
        super(bean);
    }

    /**
     * Returns the cash drawer command.
     * <p/>
     * This is only applicable where the till can be opened by sending a command to a printer.
     *
     * @return the command to open the cash drawer. May be {@code null}
     */
    @Override
    public DrawerCommand getDrawerCommand() {
        DrawerCommand result = null;
        IMObjectBean bean = getBean();
        PrinterReference printer = PrinterReference.fromString(bean.getString("printerName"));
        if (printer != null) {
            String printerCommand = bean.getString("drawerCommand");
            byte[] command = null;
            if (!StringUtils.isEmpty(printerCommand)) {
                command = getCommand(printerCommand);
            }
            if (command != null) {
                result = new DrawerCommandImpl(printer.getId(), printer.getArchetype(), command);
            }
        }
        return result;
    }

    /**
     * Parses the open drawer command string.
     *
     * @param command the open drawer command
     * @return the control codes, or {@code null} if there are none, or they are invalid
     */
    private byte[] getCommand(String command) {
        byte[] result = null;
        String[] codes = command.split(",");
        if (codes.length != 0) {
            result = new byte[codes.length];
            try {
                for (int i = 0; i < codes.length; ++i) {
                    int value = Integer.parseInt(codes[i].trim());
                    result[i] = (byte) value;
                }
            } catch (NumberFormatException exception) {
                result = null;
            }
        }
        return result;
    }

}
