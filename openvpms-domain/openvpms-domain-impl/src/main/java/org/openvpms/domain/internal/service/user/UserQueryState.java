/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.DomainQueryState;
import org.openvpms.domain.query.Filter;

/**
 * User query state.
 *
 * @author Tim Anderson
 */
class UserQueryState<D> extends DomainQueryState<D, User> {

    /**
     * Username filter.
     */
    private Filter<String> username;

    /**
     * If {@code true}, limit results to employees.
     */
    private boolean employees;

    /**
     * If {@code true}, limit results to clinicians.
     */
    private boolean clinicians;

    /**
     * Constructs a {@link UserQueryState}.
     *
     * @param domainType    the domain type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public UserQueryState(Class<D> domainType, ArchetypeService service, DomainService domainService) {
        super(domainType, User.class, service, domainService);
        setArchetypes(UserArchetypes.USER);
    }

    /**
     * Returns the username filter.
     *
     * @return the username filter. May be {@code null}
     */
    public Filter<String> getUsername() {
        return username;
    }

    /**
     * Sets the username filter.
     *
     * @param username the username filter
     */
    public void setUsername(Filter<String> username) {
        this.username = username;
    }

    /**
     * Determines if results should be limited to employees.
     *
     * @return {@code true} if results should be limited to employees
     */
    public boolean getEmployees() {
        return employees;
    }

    /**
     * Determines if results should be limited to employees.
     *
     * @param employees if {@code true}, results should be limited to employees
     */
    public void setEmployees(boolean employees) {
        this.employees = employees;
    }

    /**
     * Determines if results should be limited to clinicians.
     *
     * @return {@code true} if results should be limited to clinicians
     */
    public boolean getClinicians() {
        return clinicians;
    }

    /**
     * Determines if results should be limited to clinicians.
     *
     * @param clinicians if {@code true}, results should be limited to clinicians
     */
    public void setClinicians(boolean clinicians) {
        this.clinicians = clinicians;
    }

    /**
     * Creates a new state object, with details copied from this.
     *
     * @param domainType the domain type
     * @return a new state object
     */
    @Override
    public <D2 extends IMObject> DomainQueryState<D2, User> newState(Class<D2> domainType) {
        UserQueryState<D2> result = new UserQueryState<>(domainType, getService(), getDomainService());
        populate(result);
        return result;
    }

    /**
     * Populates the supplied state.
     *
     * @param state the state to populate
     */
    @Override
    protected void populate(DomainQueryState<?, ?> state) {
        super.populate(state);
        UserQueryState<?> other = (UserQueryState<?>) state;
        other.setUsername(username);
        other.setEmployees(employees);
        other.setClinicians(clinicians);
    }
}
