/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.supplier;

import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.party.ContactablePartyImpl;
import org.openvpms.domain.supplier.Supplier;

/**
 * Default implementation of {@link Supplier}.
 *
 * @author Tim Anderson
 */
public class SupplierImpl extends ContactablePartyImpl implements Supplier {

    /**
     * Constructs a {@link SupplierImpl}.
     *
     * @param peer    the peer to delegate to
     * @param rules   the party rules
     * @param service the domain object service
     */
    public SupplierImpl(Party peer, PartyRules rules, DomainService service) {
        super(peer, rules, service);
    }

    /**
     * Constructs a {@link SupplierImpl}.
     *
     * @param bean    the bean wrapping the party
     * @param rules   the party rules
     * @param service the domain object service
     */
    public SupplierImpl(IMObjectBean bean, PartyRules rules, DomainService service) {
        super(bean, rules, service);
    }
}
