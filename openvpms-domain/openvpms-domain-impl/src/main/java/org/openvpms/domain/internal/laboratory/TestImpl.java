/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.laboratory;

import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Test;

import java.math.BigDecimal;
import java.util.List;

/**
 * Default implementation of {@link Test}.
 *
 * @author Tim Anderson
 */
public class TestImpl extends BeanEntityDecorator implements Test {

    /**
     * The domain object service.
     */
    private final DomainService service;

    /**
     * Constructs a {@link TestImpl}.
     *
     * @param entity  the laboratory test
     * @param service the domain object service
     */
    public TestImpl(Entity entity, DomainService service) {
        super(entity, service);
        this.service = service;
    }

    /**
     * Returns the laboratory assigned test code.
     * <p/>
     * This is short for: {@code getCodeIdentity().getIdentity()}
     *
     * @return the test code
     */
    @Override
    public String getCode() {
        return getCodeIdentity().getIdentity();
    }

    /**
     * Returns the laboratory assigned identifier for this test.
     *
     * @return the test identifier
     */
    @Override
    public Identity getCodeIdentity() {
        return getBean().getObject("code", Identity.class);
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type
     */
    @Override
    public InvestigationType getInvestigationType() {
        IMObjectBean bean = getBean();
        Entity investigationType = bean.getTarget("investigationType", Entity.class);
        if (investigationType == null) {
            throw new IllegalStateException("Test=" + getId() + " has no investigation type");
        }
        return service.create(investigationType, InvestigationType.class);
    }

    /**
     * Determines if the test can be grouped with other tests in an order.
     *
     * @return {@code true} if the test can be grouped with other tests; otherwise it must be submitted individually
     */
    @Override
    public boolean getGroup() {
        return getBean().getBoolean("group");
    }

    /**
     * Returns the turnaround notes for this test.
     *
     * @return turnaround notes. May be {@code null}
     */
    @Override
    public String getTurnaround() {
        return getBean().getString("turnaround");
    }

    /**
     * Returns the specimen collection notes for this test.
     *
     * @return the specimen collection notes. May be {@code null}
     */
    @Override
    public String getSpecimen() {
        return getBean().getString("specimen");
    }

    /**
     * Returns the test price.
     *
     * @return the test price, excluding tax
     */
    @Override
    public BigDecimal getPrice() {
        return getBean().getBigDecimal("price", BigDecimal.ZERO);
    }

    /**
     * Determines if the laboratory device must be specified when this test is ordered.
     *
     * @return {@link UseDevice#YES YES} if a device must be specified, {@link UseDevice#NO NO} if no device should be
     * specified and {@link UseDevice#OPTIONAL OPTIONAL} if a device may be specified
     */
    @Override
    public UseDevice getUseDevice() {
        return UseDevice.valueOf(getBean().getString("useDevice"));
    }

    /**
     * Returns the devices that can perform this test.
     *
     * @return the devices
     */
    @Override
    public List<Device> getDevices() {
        return getInvestigationType().getDevices();
    }
}
