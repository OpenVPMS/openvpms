/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.user;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.user.EmployeeImpl;

/**
 * User factory.
 *
 * @author Tim Anderson
 */
public class UserFactory {

    /**
     * Creates a domain object based on the supplied user, or returns it unchanged if it doesn't correspond to a
     * domain object.
     *
     * @param user          the user
     * @param domainService the domain service
     * @return the domain object corresponding to the user, or {@code user} if there is none
     */
    public static User create(User user, DomainService domainService) {
        User result;
        IMObjectBean bean = domainService.getBean(user);
        if (EmployeeImpl.isEmployee(bean)) {
            // TODO - need a better way to differentiate between employees and system users
            result = domainService.create(bean, EmployeeImpl.class);
        } else {
            result = user;
        }
        return result;
    }

}
