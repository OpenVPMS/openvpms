/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.record.Note;

/**
 * Default implementation of {@link Note}.
 *
 * @author Tim Anderson
 */
public class NoteImpl extends AbstractDocumentActRecordImpl implements Note {

    /**
     * Constructs a {@link NoteImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public NoteImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the note.
     *
     * @return the note. May be {@code null}
     */
    @Override
    public String getNote() {
        IMObjectBean bean = getBean();
        String result = bean.getString("note");
        if (result == null) {
            Reference reference = getPeer().getDocument();
            if (reference != null) {
                Document document = (Document) bean.getObject(reference);
                if (document != null && TextDocumentHandler.TEXT_PLAIN.equals(document.getMimeType())) {
                    result = TextDocumentHandler.asString(document);
                }
            }
        }
        return result;
    }
}
