/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.query;

import org.openvpms.domain.query.Filter;

/**
 * Address filter.
 *
 * @author Tim Anderson
 */
public class AddressFilter {

    /**
     * The address filter.
     */
    private final Filter<String> address;

    /**
     * The suburb filter.
     */
    private final Filter<String> suburb;

    /**
     * The postcode filter.
     */
    private final Filter<String> postcode;

    /**
     * The state filter.
     */
    private final Filter<String> state;

    /**
     * Constructs an {@link AddressFilter}.
     *
     * @param address  the address. May be {@code null}
     * @param suburb   the suburb. May be {@code null}
     * @param postcode the postcode. May be {@code null}
     * @param state    the state. May be {@code null}
     */
    private AddressFilter(String address, String suburb, String postcode, String state) {
        this(address != null ? Filter.equal(address) : null,
             suburb != null ? Filter.equal(suburb) : null,
             postcode != null ? Filter.equal(postcode) : null,
             state != null ? Filter.equal(state) : null);
    }

    /**
     * Filter customers by address.
     *
     * @param address  the address. May be {@code null}
     * @param suburb   the suburb. May be {@code null}
     * @param postcode the postcode. May be {@code null}
     * @param state    the state. May be {@code null}¶
     */
    private AddressFilter(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                          Filter<String> state) {
        this.address = address;
        this.suburb = suburb;
        this.postcode = postcode;
        this.state = state;
    }

    /**
     * Returns the address filter.
     *
     * @return the address filter. May be {@code null}
     */
    public Filter<String> getAddress() {
        return address;
    }

    /**
     * Returns the suburb filter.
     *
     * @return the suburb filter. May be {@code null}
     */
    public Filter<String> getSuburb() {
        return suburb;
    }

    /**
     * Returns the postcode filter.
     *
     * @return the postcode filter. May be {@code null}
     */
    public Filter<String> getPostcode() {
        return postcode;
    }

    /**
     * Returns the state filter.
     *
     * @return the state filter. May be {@code null}
     */
    public Filter<String> getState() {
        return state;
    }

    /**
     * Creates a new filter.
     *
     * @param address  the address. May be {@code null}
     * @param suburb   the suburb. May be {@code null}
     * @param postcode the postcode. May be {@code null}
     * @param state    the state. May be {@code null}
     * @return the filter, or {@code null} if all of the parameters are {@code null}
     */
    public static AddressFilter create(String address, String suburb, String postcode, String state) {
        return (address != null || suburb != null || postcode != null || state != null)
               ? new AddressFilter(address, suburb, postcode, state) : null;
    }

    /**
     * Creates a new filter.
     *
     * @param address  the address. May be {@code null}
     * @param suburb   the suburb. May be {@code null}
     * @param postcode the postcode. May be {@code null}
     * @param state    the state. May be {@code null}
     * @return the filter, or {@code null} if all of the parameters are {@code null}
     */
    public static AddressFilter create(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                                       Filter<String> state) {
        return (address != null || suburb != null || postcode != null || state != null)
               ? new AddressFilter(address, suburb, postcode, state) : null;
    }
}