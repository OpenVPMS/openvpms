/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.practice;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.practice.Practice;
import org.openvpms.domain.service.practice.PracticeService;

import java.util.Collections;
import java.util.List;

/**
 * Default implementation of {@link PracticeService}.
 *
 * @author Tim Anderson
 */
public class PracticeServiceImpl implements PracticeService {

    /**
     * The underlying practice service.
     */
    private final org.openvpms.archetype.rules.practice.PracticeService practiceService;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link PracticeService}.
     *
     * @param practiceService the service to delegate to
     * @param domainService   the domain service
     */
    public PracticeServiceImpl(org.openvpms.archetype.rules.practice.PracticeService practiceService,
                               DomainService domainService) {
        this.practiceService = practiceService;
        this.domainService = domainService;
    }

    /**
     * Returns the practice.
     *
     * @return the practice, or {@code null} if none is available
     */
    @Override
    public Practice getPractice() {
        return getPractice(true);
    }

    /**
     * Returns the active practice locations.
     *
     * @return the practice locations
     */
    @Override
    public List<Location> getLocations() {
        Practice practice = getPractice(false);
        return (practice != null) ? practice.getLocations() : Collections.emptyList();
    }

    /**
     * Returns the practice location given its identifier.
     *
     * @param id the practice location identifier
     * @return the corresponding location, or {@code null} if none is found
     */
    @Override
    public Location getLocation(long id) {
        Location result = null;
        Party practice = practiceService.getPractice();
        if (practice != null) {
            // only return the location if it has a relationship to the practice
            IMObjectBean bean = domainService.getBean(practice);
            Policy<Relationship> policy = Policies.any(relationship -> relationship.getTarget().getId() == id);
            Party location = bean.getTarget("locations", Party.class, policy);
            if (location != null) {
                result = domainService.create(location, Location.class);
            }
        }
        return result;
    }

    /**
     * Returns the practice.
     *
     * @param copy if {@code true}, return a copy, to prevent plugins modifying the shared instance
     * @return the practice, or {@code null} if none is available
     */
    private Practice getPractice(boolean copy) {
        Practice result = null;
        Party practice = practiceService.getPractice();
        if (practice != null) {
            if (copy) {
                // re-retrieve to prevent plugins from modifying a shared instance
                result = domainService.get(practice.getObjectReference(), Practice.class);
            } else {
                result = domainService.create(practice, Practice.class);
            }
        }
        return result;
    }
}
