/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.domain.internal.query.DomainQueryImpl;
import org.openvpms.domain.product.Medication;
import org.openvpms.domain.product.Merchandise;
import org.openvpms.domain.product.Service;
import org.openvpms.domain.product.Template;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.product.AbstractProductQuery;
import org.openvpms.domain.service.product.MedicationQuery;
import org.openvpms.domain.service.product.MerchandiseQuery;
import org.openvpms.domain.service.product.ServiceQuery;
import org.openvpms.domain.service.product.TemplateQuery;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link AbstractProductQuery}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractProductQueryImpl<D extends Product, Q extends AbstractProductQuery<D, Q>>
        extends DomainQueryImpl<D, Product, Q>
        implements AbstractProductQuery<D, Q> {

    /**
     * Constructs a {@link AbstractProductQueryImpl}.
     *
     * @param state the query state
     */
    protected AbstractProductQueryImpl(ProductQueryState<D> state) {
        super(state);
    }

    /**
     * Filter products by product type name.
     *
     * @param name the product type name
     * @return this
     */
    @Override
    public Q productType(String name) {
        getState().setProductTypeName(Filter.equal(name));
        return getThis();
    }

    /**
     * Return medication products.
     *
     * @return a medication query
     */
    @Override
    public MedicationQuery medications() {
        return new MedicationQueryImpl(getState().newState(Medication.class));
    }

    /**
     * Return merchandise products.
     *
     * @return a merchandise query
     */
    @Override
    public MerchandiseQuery merchandise() {
        return new MerchandiseQueryImpl(getState().newState(Merchandise.class));
    }

    /**
     * Return service products.
     *
     * @return a service query
     */
    @Override
    public ServiceQuery services() {
        return new ServiceQueryImpl(getState().newState(Service.class));
    }

    /**
     * Return product templates.
     *
     * @return a template query
     */
    @Override
    public TemplateQuery templates() {
        return new TemplateQueryImpl(getState().newState(Template.class));
    }

    /**
     * Returns the query state.
     *
     * @return the query state
     */
    @Override
    protected ProductQueryState<D> getState() {
        return (ProductQueryState<D>) super.getState();
    }

    /**
     * Creates the query.
     *
     * @return the query
     */
    @Override
    protected TypedQuery<Product> createQuery() {
        List<String> archetypes = getState().getArchetypes();
        if (archetypes == null || archetypes.isEmpty()) {
            archetypes(ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE, ProductArchetypes.SERVICE,
                       ProductArchetypes.TEMPLATE);
            // NOTE: price templates are excluded, as they are going to be removed
        }
        return super.createQuery();
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param query      the query
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    @Override
    protected void addPredicates(List<Predicate> predicates, CriteriaQuery<Product> query, Root<Product> from, CriteriaBuilder builder) {
        addProductTypeFilters(from, builder);
        super.addPredicates(predicates, query, from, builder);
    }

    /**
     * Adds any filters on product types.
     *
     * @param from    the from clause
     * @param builder the criteria builder
     */
    private void addProductTypeFilters(Root<Product> from, CriteriaBuilder builder) {
        Filter<Long> productTypeId = getState().getProductTypeId();
        Filter<String> productTypeName = getState().getProductTypeName();
        if (productTypeId != null || productTypeName != null) {
            Join<IMObject, IMObject> join = from.join("type").join("target");
            List<Predicate> joinPredicates = new ArrayList<>();
            if (productTypeId != null) {
                joinPredicates.add(createPredicate(join.get("id"), productTypeId, builder));
            }
            if (productTypeName != null) {
                joinPredicates.add(createPredicate(join.get("name"), productTypeName, builder));
            }
            join.on(joinPredicates.toArray(new Predicate[0]));
        }
    }
}
