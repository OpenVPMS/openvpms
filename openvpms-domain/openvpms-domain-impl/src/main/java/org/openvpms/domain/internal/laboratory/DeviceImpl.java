/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.laboratory;

import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.practice.Location;

import java.util.List;

/**
 * Default implementation of {@link Device}.
 *
 * @author Tim Anderson
 */
public class DeviceImpl extends BeanEntityDecorator implements Device {

    /**
     * The domain object service.
     */
    private final DomainService service;

    /**
     * Constructs a {@link DeviceImpl}.
     *
     * @param entity  the laboratory test
     * @param service the domain object service
     */
    public DeviceImpl(Entity entity, DomainService service) {
        super(entity, service);
        this.service = service;
    }

    /**
     * Returns the laboratory assigned device identifier.
     * <p/>
     * This is short for: {@code getDeviceIdentity().getIdentity()}
     *
     * @return the device identifier
     */
    @Override
    public String getDeviceId() {
        return getDeviceIdentity().getIdentity();
    }

    /**
     * Returns the laboratory assigned identifier for this device.
     *
     * @return the device identifier
     */
    @Override
    public Identity getDeviceIdentity() {
        return getBean().getObject("deviceId", Identity.class);
    }

    /**
     * Returns the laboratory that manages tests for this device.
     *
     * @return the laboratory
     */
    @Override
    public Laboratory getLaboratory() {
        return service.create(getBean().getTarget("laboratory"), Laboratory.class);
    }

    /**
     * Returns the practice locations that this device may be used at.
     *
     * @return the locations, or an empty list if the device is available at all locations
     */
    @Override
    public List<Location> getLocations() {
        return getLaboratory().getLocations();
    }

}
