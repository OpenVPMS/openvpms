/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.builder;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Base class for domain object builders.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDomainObjectBuilder<D, T extends IMObject,
        B extends AbstractDomainObjectBuilder<D, T, B>> {

    /**
     * The object to update.
     */
    private final T existing;

    /**
     * The archetype to build.
     */
    private final String archetype;

    /**
     * The domain type.
     */
    private final Class<D> domainType;

    /**
     * The model type.
     */
    private final Class<T> modelType;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs an {@link AbstractDomainObjectBuilder}.
     *
     * @param modelType     the model object type
     * @param domainType    the domain object type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public AbstractDomainObjectBuilder(Class<T> modelType, Class<D> domainType, ArchetypeService service,
                                       DomainService domainService) {
        this(null, null, modelType, domainType, service, domainService);
    }

    /**
     * Constructs an {@link AbstractDomainObjectBuilder}.
     *
     * @param archetype     the archetype to build
     * @param modelType     the model object type
     * @param domainType    the domain object type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public AbstractDomainObjectBuilder(String archetype, Class<T> modelType, Class<D> domainType,
                                       ArchetypeService service, DomainService domainService) {
        this(null, archetype, modelType, domainType, service, domainService);
    }

    /**
     * Constructs an {@link AbstractDomainObjectBuilder}.
     *
     * @param object        the object to update
     * @param domainType    the domain object type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    @SuppressWarnings("unchecked")
    public AbstractDomainObjectBuilder(T object, Class<D> domainType, ArchetypeService service,
                                       DomainService domainService) {
        this(object, object.getArchetype(), (Class<T>) object.getClass(), domainType, service, domainService);
    }

    /**
     * Constructs an {@link AbstractDomainObjectBuilder}.
     *
     * @param object        the object to update. May be {@code null}
     * @param archetype     the archetype. May be {@code null}
     * @param modelType     the model object type
     * @param domainType    the domain object type
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    private AbstractDomainObjectBuilder(T object, String archetype, Class<T> modelType, Class<D> domainType,
                                        ArchetypeService service, DomainService domainService) {
        this.existing = object;
        this.archetype = archetype;
        this.modelType = modelType;
        this.domainType = domainType;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Builds the object.
     * <p/>
     * This implementation saves it.
     *
     * @return the object
     */
    public D build() {
        return build(true);
    }

    /**
     * Builds the object.
     *
     * @param save if {@code true}, save the object, and any related objects
     * @return the object
     */
    public D build(boolean save) {
        T object = getObject(archetype);
        State state = new State(object);
        build(state);
        if (save) {
            service.save(state.getChanged());
        }
        return state.getDomainObject();
    }

    /**
     * Builds the object.
     *
     * @param state the build state
     */
    protected abstract void build(State state);

    /**
     * Creates a domain object from a model object.
     *
     * @param object the model object
     * @param type   the domain object type
     * @return a new domain object
     */
    protected <A extends IMObject, C> C create(A object, Class<C> type) {
        return domainService.create(object, type);
    }

    /**
     * Returns the object to build.
     * <p/>
     * This implementation returns the existing instance, if one was supplied at construction and matches
     * the specified archetype else it returns a new instance.
     * <p/>
     * For builders that require unique instances, it may return an existing instance.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    protected T getObject(String archetype) {
        T result;
        if (existing != null && existing.isA(archetype)) {
            result = existing;
        } else {
            result = service.create(archetype, modelType);
        }
        return result;
    }

    /**
     * Returns the existing object, if it is being updated.
     *
     * @return the existing object, or {@code null} if a new object is being created
     */
    protected T getExisting() {
        return existing;
    }

    /**
     * Helper to set a value on a {@link NodeValue}, returning this.
     *
     * @param node  the node to update
     * @param value the new value
     * @return this
     */
    protected B setValue(NodeValue node, Object value) {
        node.setValue(value);
        return getThis();
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected B getThis() {
        return (B) this;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the domain object service.
     *
     * @return the domain object service
     */
    protected DomainService getDomainService() {
        return domainService;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Helper to create an object.
     *
     * @param archetype the archetype
     * @param type      the expected type of the object
     */
    protected <O extends IMObject> O create(String archetype, Class<O> type) {
        return service.create(archetype, type);
    }

    /**
     * Build state.
     */
    protected class State {

        /**
         * The object being built.
         */
        private final T object;

        /**
         * Bean wrapping the object being built.
         */
        private final IMObjectBean bean;

        /**
         * The set of changed objects.
         */
        private final Set<IMObject> changed = new LinkedHashSet<>();

        /**
         * The domain object.
         */
        private D domainObject;

        /**
         * Constructs a {@link State}.
         *
         * @param object the object being built
         */
        public State(T object) {
            this.object = object;
            this.bean = service.getBean(object);
            addChanged(object);
            if (domainType.isAssignableFrom(object.getClass())) {
                // if an existing domain object is being updated, don't lazily create it
                domainObject = domainType.cast(object);
            }
        }

        /**
         * Returns the object being built.
         *
         * @return the object being built
         */
        public T getObject() {
            return object;
        }

        /**
         * Returns the bean wrapping the object being built.
         *
         * @return the bean
         */
        public IMObjectBean getBean() {
            return bean;
        }

        /**
         * Adds a changed object.
         *
         * @param object the object
         */
        public void addChanged(IMObject object) {
            changed.add(object);
        }

        /**
         * Returns the changed objects.
         *
         * @return the changed objects
         */
        public Set<IMObject> getChanged() {
            return changed;
        }

        /**
         * Returns the domain object.
         *
         * @return the domain object
         */
        public D getDomainObject() {
            if (domainObject == null) {
                domainObject = create(object, domainType);
            }
            return domainObject;
        }
    }
}