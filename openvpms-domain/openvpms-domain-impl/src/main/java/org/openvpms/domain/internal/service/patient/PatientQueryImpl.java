/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.query.AddressFilter;
import org.openvpms.domain.internal.query.PartyQueryImpl;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.query.Active;
import org.openvpms.domain.query.Filter;
import org.openvpms.domain.service.patient.PatientQuery;

import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

/**
 * Default implementation of {@link PatientQueryImpl}.
 *
 * @author Tim Anderson
 */
public class PatientQueryImpl extends PartyQueryImpl<Patient, Party, PatientQuery> implements PatientQuery {

    /**
     * The microchip to filter on.
     */
    private Filter<String> microchip;

    /**
     * The owner identifier.
     */
    private Long ownerId;

    /**
     * The owner name.
     */
    private Filter<String> ownerName;

    /**
     * The owner active filter.
     */
    private Filter<Boolean> activeOwner;

    /**
     * The owner address filter.
     */
    private AddressFilter address;

    /**
     * The owner email filter.
     */
    private Filter<String> email;

    /**
     * The owner phone filter.
     */
    private Filter<String> phone;

    /**
     * Constructs  a {@link PatientQueryImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public PatientQueryImpl(ArchetypeService service, DomainService domainService) {
        super(Patient.ARCHETYPE, Patient.class, Party.class, service, domainService);
    }

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery microchip(String microchip) {
        return microchip((microchip != null) ? Filter.equal(microchip) : null);
    }

    /**
     * Filter patients by microchip.
     *
     * @param microchip the microchip. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery microchip(Filter<String> microchip) {
        this.microchip = microchip;
        return this;
    }

    /**
     * Filter patients by owner.
     *
     * @param owner the owner. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery owner(Customer owner) {
        if (owner != null) {
            owner(owner.getId());
        } else {
            ownerId = null;
        }
        return this;
    }

    /**
     * Filter patients by owner identifier.
     *
     * @param ownerId the owner identifier
     * @return this
     */
    @Override
    public PatientQuery owner(long ownerId) {
        this.ownerId = ownerId;
        return this;
    }

    /**
     * Filter patients by owner name.
     *
     * @param lastName the owner's last name. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerName(String lastName) {
        return ownerName(lastName, null);
    }

    /**
     * Filter patients by owner name.
     *
     * @param lastName  the owner's last name. May be {@code null} to reset the criteria
     * @param firstName the owner's first name. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerName(String lastName, String firstName) {
        return ownerName((lastName != null) ? Filter.equal(lastName) : null,
                         (firstName != null) ? Filter.equal(firstName) : null);
    }

    /**
     * Filter patients by owner name.
     * <p/>
     * Only 'equal' and 'like' filters are supported.
     *
     * @param lastName the owner's last name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerName(Filter<String> lastName) {
        return ownerName(lastName, null);
    }

    /**
     * Filter patients by owner name.
     * <p/>
     * Only 'equal' and 'like' filters are supported.
     *
     * @param lastName  the owner's last name criteria. May be {@code null} to reset the criteria
     * @param firstName the owner's first name criteria. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerName(Filter<String> lastName, Filter<String> firstName) {
        ownerName = (lastName != null) ? createNameFilter(lastName, firstName) : null;
        return this;
    }

    /**
     * Only query patients with active owners.
     *
     * @return this
     */
    @Override
    public PatientQuery activeOwner() {
        return activeOwner(Active.ACTIVE);
    }

    /**
     * Only query patients with inactive owners.
     *
     * @return this
     */
    @Override
    public PatientQuery inactiveOwner() {
        return activeOwner(Active.INACTIVE);
    }

    /**
     * Determines if patients with active, inactive, or both active and inactive owners are returned.
     *
     * @param active the active state
     * @return this
     */
    @Override
    public PatientQuery activeOwner(Active active) {
        activeOwner = getActiveFilter(active);
        return this;
    }

    /**
     * Filter patients by owner address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerAddress(String address, String suburb, String postcode, String state) {
        this.address = AddressFilter.create(address, suburb, postcode, state);
        return this;
    }

    /**
     * Filter patients by owner address.
     *
     * @param address  the address. May be {@code null} to reset the criteria
     * @param suburb   the suburb. May be {@code null} to reset the criteria
     * @param postcode the postcode. May be {@code null} to reset the criteria
     * @param state    the state. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerAddress(Filter<String> address, Filter<String> suburb, Filter<String> postcode,
                                     Filter<String> state) {
        this.address = AddressFilter.create(address, suburb, postcode, state);
        return this;
    }

    /**
     * Filter patients by owner email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerEmail(String email) {
        return ownerEmail((email != null) ? Filter.equal(email) : null);
    }

    /**
     * Filter patients by owner email address.
     *
     * @param email the email address. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerEmail(Filter<String> email) {
        this.email = email;
        return this;
    }

    /**
     * Filter patients by owner phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerPhone(String phone) {
        return ownerPhone((phone != null) ? Filter.equal(phone) : null);
    }

    /**
     * Filter patients by owner phone number.
     *
     * @param phone the phone number. May be {@code null} to reset the criteria
     * @return this
     */
    @Override
    public PatientQuery ownerPhone(Filter<String> phone) {
        this.phone = phone;
        return this;
    }

    /**
     * Adds predicates.
     *
     * @param predicates collects the predicates
     * @param query      the query
     * @param from       the from clause
     * @param builder    the criteria builder
     */
    @Override
    protected void addPredicates(List<Predicate> predicates, CriteriaQuery<Party> query, Root<Party> from,
                                 CriteriaBuilder builder) {
        super.addPredicates(predicates, query, from, builder);
        if (microchip != null) {
            Join<?, ?> identity = from.join("identities", Microchip.ARCHETYPE);
            identity.on(createPredicate(identity.get("identity"), microchip, builder));
        }
        if (ownerId != null || ownerName != null || activeOwner != null || address != null || email != null
            || phone != null) {
            Join<Party, EntityRelationship> customers = from.join("customers", PatientArchetypes.PATIENT_OWNER);
            Join<EntityRelationship, Party> owner = customers.join("source", CustomerArchetypes.PERSON);
            owner.alias("owner");
            Date now = new Date();
            predicates.add(builder.lessThanOrEqualTo(customers.get("activeStartTime"), now));
            predicates.add(builder.or(builder.greaterThanOrEqualTo(customers.get("activeEndTime"), now),
                                      builder.isNull(customers.get("activeEndTime"))));
            if (ownerId != null) {
                predicates.add(createPredicate(owner.get("id"), Filter.equal(ownerId), builder));
            }
            if (ownerName != null) {
                predicates.add(createPredicate(owner.get("name"), ownerName, builder));
            }
            if (activeOwner != null) {
                predicates.add(createPredicate(owner.get("active"), activeOwner, builder));
            }
            if (address != null) {
                addAddressPredicate(address, predicates, query, owner, builder);
            }
            if (email != null) {
                addEmailPredicate(email, predicates, owner, builder);
            }
            if (phone != null) {
                addPhonePredicate(phone, predicates, owner, builder);
            }
        }
    }
}
