/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.query;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.From;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.query.DomainQuery;
import org.openvpms.domain.query.Filter;

import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 * Party query.
 *
 * @author Tim Anderson
 */
public class PartyQueryImpl<D, T extends Party, Q extends DomainQuery<D, Q>> extends DomainQueryImpl<D, T, Q> {

    /**
     * Constructs a {@link PartyQueryImpl}.
     *
     * @param domainTpe     the domain type
     * @param type          the {@link IMObject} type
     * @param service       the service
     * @param domainService the domain service
     */
    protected PartyQueryImpl(Class<D> domainTpe, Class<T> type, ArchetypeService service, DomainService domainService) {
        super(domainTpe, type, service, domainService);
    }

    /**
     * Constructs a {@link PartyQueryImpl}.
     *
     * @param archetype     the archetype. If {@code null}, {@link #archetypes(String...)} must be invoked
     * @param domainTpe     the domain type
     * @param type          the {@link IMObject} type
     * @param service       the service
     * @param domainService the domain service
     */
    protected PartyQueryImpl(String archetype, Class<D> domainTpe, Class<T> type, ArchetypeService service,
                             DomainService domainService) {
        super(archetype, domainTpe, type, service, domainService);
    }

    /**
     * Constructs a {@link PartyQueryImpl}.
     *
     * @param state the query state
     */
    protected PartyQueryImpl(DomainQueryState<D, T> state) {
        super(state);
    }

    /**
     * Filter by first and last name.
     * <p/>
     * Only 'equal' and 'like' filters are supported.
     *
     * @param lastName  the last name criteria
     * @param firstName the first name criteria. May be {@code null}
     * @return the name filter
     */
    protected Filter<String> createNameFilter(Filter<String> lastName, Filter<String> firstName) {
        Filter<String> result;
        if (lastName == null) {
            throw new IllegalArgumentException("Argument 'lastName' cannot be null");
        }
        if (lastName.getOperator() != Filter.Operator.EQUAL && lastName.getOperator() != Filter.Operator.LIKE) {
            throw new IllegalArgumentException("Operator must be EQUAL or LIKE");
        }
        if (firstName != null) {
            if (firstName.getOperator() != lastName.getOperator()) {
                throw new IllegalArgumentException("Both 'firstName' and 'lastName' must have the same operator");
            }
        }
        String name = lastName.getValue();
        name += ",";
        if (firstName != null) {
            name += firstName.getValue();
        }
        if (lastName.getOperator() == Filter.Operator.EQUAL) {
            if (firstName != null) {
                result = Filter.equal(name);
            } else {
                result = Filter.like(name + "%");
            }
        } else {
            if (firstName == null) {
                name += "%";
            }
            result = Filter.like(name);
        }
        return result;
    }

    /**
     * Adds an address predicate.
     *
     * @param filter     the address filter
     * @param predicates the predicates to add to
     * @param query      the query
     * @param from       the from clause to join on. Must have a 'çontacts' node
     * @param builder    the criteria builder
     */
    protected void addAddressPredicate(AddressFilter filter, List<Predicate> predicates, CriteriaQuery<Party> query,
                                       From<?, Party> from, CriteriaBuilder builder) {
        Join<Party, IMObject> addresses = from.join("contacts", Address.ARCHETYPE).alias("addressContact");
        if (filter.getAddress() != null) {
            predicates.add(createPredicate(addresses.get("address"), filter.getAddress(), builder));
        }
        if (filter.getSuburb() != null) {
            Root<Lookup> suburbLookups = query.from(Lookup.class, ContactArchetypes.SUBURB).alias("suburb");
            predicates.add(builder.equal(addresses.get("suburb"), suburbLookups.get("code")));
            predicates.add(createPredicate(suburbLookups.get("name"), filter.getSuburb(), builder));
        }
        if (filter.getPostcode() != null) {
            predicates.add(createPredicate(addresses.get("postcode"), filter.getPostcode(), builder));
        }
        if (filter.getState() != null) {
            Root<Lookup> stateLookups = query.from(Lookup.class, ContactArchetypes.STATE).alias("state");
            predicates.add(builder.equal(addresses.get("state"), stateLookups.get("code")));
            predicates.add(createPredicate(stateLookups.get("name"), filter.getState(), builder));
        }
    }

    /**
     * Adds an email predicate.
     *
     * @param filter     the email filter
     * @param predicates the predicates to add to
     * @param from       the from clause to join on. Must have a 'çontacts' node
     * @param builder    the criteria builder
     */
    protected void addEmailPredicate(Filter<String> filter, List<Predicate> predicates, From<?, Party> from,
                                     CriteriaBuilder builder) {
        Join<Party, IMObject> emails = from.join("contacts", Email.ARCHETYPE);
        predicates.add(createPredicate(emails.get("emailAddress"), filter, builder));
    }

    /**
     * Adds a phone predicate.
     *
     * @param filter     the email filter
     * @param predicates the predicates to add to
     * @param from       the from clause to join on. Must have a 'çontacts' node
     * @param builder    the criteria builder
     */
    protected void addPhonePredicate(Filter<String> filter, List<Predicate> predicates, From<?, Party> from,
                                     CriteriaBuilder builder) {
        Join<Party, IMObject> phones = from.join("contacts", Phone.ARCHETYPE);
        predicates.add(createPredicate(phones.get("telephoneNumber"), filter, builder));
    }
}