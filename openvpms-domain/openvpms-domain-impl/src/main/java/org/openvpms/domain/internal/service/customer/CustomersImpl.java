/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.customer;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.service.customer.CustomerBuilder;
import org.openvpms.domain.service.customer.CustomerQuery;
import org.openvpms.domain.service.customer.Customers;

/**
 * Default implementation of {@link Customers}.
 *
 * @author Tim Anderson
 */
public class CustomersImpl implements Customers {

    /**
     * The customer rules.
     */
    private final CustomerRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * Constructs a {@link CustomersImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain object service
     * @param lookups       the lookup service
     */
    public CustomersImpl(CustomerRules rules, ArchetypeService service, DomainService domainService,
                         LookupService lookups) {
        this.rules = rules;
        this.service = service;
        this.domainService = domainService;
        this.lookups = lookups;
    }

    /**
     * Returns a customer given its identifier.
     *
     * @param id the customer identifier
     * @return the corresponding customer, or {@code null} if none is found
     */
    @Override
    public Customer getCustomer(long id) {
        return domainService.get(CustomerArchetypes.PERSON, id, Customer.class);
    }

    /**
     * Returns a customer query.
     *
     * @return a new query
     */
    @Override
    public CustomerQuery getQuery() {
        return new CustomerQueryImpl(service, domainService);
    }

    /**
     * Returns a builder for a customer.
     *
     * @return a customer builder
     */
    @Override
    public CustomerBuilder getCustomerBuilder() {
        return new CustomerBuilderImpl(rules, service, domainService, lookups);
    }

    /**
     * Returns a builder to update a customer.
     *
     * @param customer the customer to update
     * @return a customer builder
     */
    @Override
    public CustomerBuilder getCustomerBuilder(Customer customer) {
        return new CustomerBuilderImpl(customer, rules, service, domainService, lookups);
    }
}
