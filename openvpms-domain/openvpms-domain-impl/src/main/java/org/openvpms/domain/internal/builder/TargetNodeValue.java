/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.builder;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;

import java.util.List;
import java.util.Objects;

import static org.openvpms.component.model.bean.Predicates.targetEquals;

/**
 * Helper to defer updates to the target of a relationship node on a {@link IMObjectBean}.
 *
 * @author Tim Anderson
 */
public class TargetNodeValue extends NodeValue {

    /**
     * The relationship archetype.
     */
    private final String archetype;

    /**
     * Constructs a {@link TargetNodeValue}.
     *
     * @param name the collection node name
     */
    public TargetNodeValue(String name) {
        this(name, null);
    }

    /**
     * Constructs a {@link TargetNodeValue}.
     *
     * @param name      the collection node name
     * @param archetype the relationship archetype
     */
    public TargetNodeValue(String name, String archetype) {
        super(name);
        this.archetype = archetype;
    }

    /**
     * Populates the node with the specified value.
     *
     * @param bean  the bean to populate
     * @param value the value to set. May be {@code null}
     * @return {@code true} if the value was updated, otherwise {@code false}
     */
    @Override
    public boolean update(IMObjectBean bean, Object value) {
        Reference reference;
        if (value instanceof Reference) {
            reference = (Reference) value;
        } else if (value instanceof IMObject) {
            reference = ((IMObject) value).getObjectReference();
        } else {
            throw new IllegalArgumentException("Argument 'value' must be a Reference or IMObject");
        }
        return update(bean, reference);
    }

    /**
     * Populates the relationship target node with the specified value.
     * <p/>
     * This should only be used for uni-directional relationships.
     *
     * @param bean      the bean
     * @param reference the reference. May be {@code null}
     * @return {@code true} if the value was updated, otherwise {@code false}
     */
    public boolean update(IMObjectBean bean, Reference reference) {
        String name = getName();
        boolean result = false;
        Relationship relationship = bean.getObject(name, Relationship.class);
        if (reference != null) {
            if (relationship == null || !Objects.equals(relationship.getTarget(), reference)) {
                bean.setTarget(name, reference);
                result = true;
            }
        } else if (relationship != null) {
            bean.removeValue(name, relationship);
            result = true;
        }
        return result;
    }

    /**
     * Populates the relationship target node with the specified value.
     * <p/>
     * This should be used for bidirectional relationships.
     *
     * @param bean   the bean
     * @param target the target. Never {@code null}
     * @return {@code true} if the value was updated, otherwise {@code false}
     */
    public boolean update(IMObjectBean bean, IMObject target, String targetName) {
        boolean result = false;
        String name = getName();
        Relationship relationship = bean.getValue(name, Relationship.class, Predicates.<Relationship>isA(archetype)
                .and(targetEquals(target)));
        if (relationship == null) {
            bean.addTarget(name, target, targetName);
            result = true;
        }
        return result;
    }

    /**
     * Removes relationships between two objects.
     *
     * @param bean       the source object
     * @param target     the target object
     * @param targetName the target relationship node name
     * @return {@code true} if any relationships were removed
     */
    public boolean remove(IMObjectBean bean, IMObject target, String targetName) {
        boolean result = false;
        String name = getName();
        List<Relationship> relationships = bean.getValues(name, Relationship.class, targetEquals(target));
        if (relationships.isEmpty()) {
            bean.removeTargets(getName(), target, targetName);
            result = true;
        }
        return result;
    }
}