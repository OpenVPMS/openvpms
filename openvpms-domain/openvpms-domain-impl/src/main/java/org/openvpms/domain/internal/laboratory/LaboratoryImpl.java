/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.practice.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link Laboratory}.
 *
 * @author Tim Anderson
 */
public class LaboratoryImpl extends BeanEntityDecorator implements Laboratory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link LaboratoryImpl}.
     *
     * @param object        the peer to delegate to
     * @param service       the archetype service
     * @param domainService the domain object factory
     */
    public LaboratoryImpl(Entity object, ArchetypeService service, DomainService domainService) {
        super(object, service);
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns the locations that this laboratory may be used at.
     *
     * @return the locations, or an empty list if the device is available at all locations
     */
    @Override
    public List<Location> getLocations() {
        List<Location> result = new ArrayList<>();
        IMObjectBean bean = getBean();
        for (Entity entity : bean.getTargets("locations", Entity.class)) {
            result.add(domainService.create(entity, Location.class));
        }
        return result;
    }

    /**
     * Returns the devices that this laboratory manages.
     *
     * @param activeOnly if {@code true}, only return active devices
     * @return the devices supported by the laboratory
     */
    @Override
    public List<Device> getDevices(boolean activeOnly) {
        List<Device> result = new ArrayList<>();
        for (Entity entity : query(LaboratoryArchetypes.DEVICE, activeOnly, "laboratory")) {
            result.add(domainService.create(entity, Device.class));
        }
        return result;
    }

    /**
     * Returns all tests supported by a laboratory.
     *
     * @param activeOnly if {@code true}, only return active tests
     * @return the tests supported by the laboratory.
     */
    @Override
    public List<Test> getTests(boolean activeOnly) {
        List<Test> result = new ArrayList<>();
        for (Entity entity : query(LaboratoryArchetypes.TEST, activeOnly, "laboratories")) {
            result.add(domainService.create(entity, Test.class));
        }
        return result;
    }

    /**
     * Returns the configuration for a practice location.
     * <p/>
     * Laboratory implementations can provide a custom <em>entityLink.laboratoryLocation&lt;provider&gt;</em>
     * in order to configure location specific details such as logins.
     *
     * @param location the location
     * @return the configuration, or {@code null} if none is found
     */
    @Override
    public IMObject getConfiguration(Location location) {
        return getBean().getValue("locations", Relationship.class, Predicates.targetEquals(location));
    }

    /**
     * Creates a query for entities linked to the laboratory.
     *
     * @param archetype  the entity archetype
     * @param activeOnly if {@code true}, only return active entities
     * @param node       the node to join on
     * @return a new query
     */
    private List<Entity> query(String archetype, boolean activeOnly, String node) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, archetype);
        Join<Entity, IMObject> laboratories = root.join(node);
        laboratories.on(builder.equal(laboratories.get("target"), getId()));
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        return service.createQuery(query).getResultList();
    }

}
