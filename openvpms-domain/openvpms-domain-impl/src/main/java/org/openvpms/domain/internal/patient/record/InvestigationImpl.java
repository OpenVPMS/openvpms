/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.patient.record.Investigation;
import org.openvpms.domain.practice.Location;

/**
 * Default implementation of {@link Investigation}.
 *
 * @author Tim Anderson
 */
public class InvestigationImpl extends AbstractVersioningDocumentRecordImpl implements Investigation {

    /**
     * Constructs an {@link InvestigationImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public InvestigationImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type
     */
    @Override
    public InvestigationType getInvestigationType() {
        Entity investigationType = getBean().getTarget("investigationType", Entity.class);
        if (investigationType == null) {
            throw new IllegalStateException("Investigation has no InvestigationType");
        }
        return getService().create(investigationType, InvestigationType.class);
    }

    /**
     * The location where the investigation was performed.
     *
     * @return the location. May be {@code null}
     */
    @Override
    public Location getLocation() {
        return getService().get(getBean().getTargetRef("location"), Location.class);
    }
}
