/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Services required by patient domain object implementations.
 *
 * @author Tim Anderson
 */
public class PatientServices {

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The medical record rules.
     */
    private final MedicalRecordRules medicalRecordRules;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The archetype service.
     */
    private final ArchetypeService archetypeService;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link PatientServices}.
     *
     * @param patientRules       the patient rules
     * @param medicalRecordRules the medical record rules
     * @param archetypeService   the archetype service
     * @param practiceService    the practice service
     * @param domainService      the domain service
     * @param transactionManager the transaction manager
     */
    public PatientServices(PatientRules patientRules, MedicalRecordRules medicalRecordRules,
                           ArchetypeService archetypeService, PracticeService practiceService,
                           DomainService domainService, PlatformTransactionManager transactionManager) {
        this.patientRules = patientRules;
        this.medicalRecordRules = medicalRecordRules;
        this.archetypeService = archetypeService;
        this.practiceService = practiceService;
        this.domainService = domainService;
        this.transactionManager = transactionManager;
    }

    /**
     * Returns the patient rules.
     *
     * @return the patient rules
     */
    public PatientRules getPatientRules() {
        return patientRules;
    }

    /**
     * Returns the practice service.
     *
     * @return the practice service
     */
    public PracticeService getPracticeService() {
        return practiceService;
    }

    /**
     * Returns the medical record rules.
     *
     * @return the medical record rules
     */
    public MedicalRecordRules getMedicalRecordRules() {
        return medicalRecordRules;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    public ArchetypeService getArchetypeService() {
        return archetypeService;
    }

    /**
     * Returns the domain service
     *
     * @return the domain service
     */
    public DomainService getDomainService() {
        return domainService;
    }

    /**
     * Returns the transaction manager.
     *
     * @return the transaction manager
     */
    public PlatformTransactionManager getTransactionManager() {
        return transactionManager;
    }
}