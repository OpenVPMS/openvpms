/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EFTPOSTransactionFactory}.
 *
 * @author Tim Anderson
 */
public class EFTPOSTransactionFactoryTestCase extends ArchetypeServiceTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The factory.
     */
    private EFTPOSTransactionFactory factory;

    /**
     * The location.
     */
    private Party location;

    /**
     * The till.
     */
    private Entity till;

    /**
     * The terminal.
     */
    private Entity terminal;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        factory = new EFTPOSTransactionFactory(getArchetypeService());
        location = practiceFactory.createLocation();
        till = practiceFactory.createTill();
        terminal = practiceFactory.createEFTPOSTerminal();
        customer = customerFactory.createCustomer();
    }

    /**
     * Tests the {@link EFTPOSTransactionFactory#createPayment(FinancialAct, BigDecimal, BigDecimal, Entity, Party)}
     * method.
     */
    @Test
    public void testCreatePayment() {
        FinancialAct act = accountFactory.newPayment()
                .customer(customer)
                .till(till)
                .eft().amount(10)
                .cashout(5)
                .add()
                .build();
        FinancialAct transaction = factory.createPayment(act, TEN, BigDecimal.valueOf(5), terminal, location);
        assertEquals(EFTPOSArchetypes.PAYMENT, transaction.getArchetype());
        assertTrue(transaction.isNew());
        assertEquals(EFTPOSTransactionStatus.PENDING, transaction.getStatus());
        IMObjectBean bean = getBean(transaction);
        assertEquals(Long.toString(act.getId()), bean.getObject("parentId", ActIdentity.class).getIdentity());
        checkEquals(10, bean.getBigDecimal("amount"));
        checkEquals(5, bean.getBigDecimal("cashout"));
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(terminal.getObjectReference(), bean.getTargetRef("terminal"));
        assertEquals(location.getObjectReference(), bean.getTargetRef("location"));
    }

    /**
     * Tests the {@link EFTPOSTransactionFactory#createRefund(FinancialAct, BigDecimal, Entity, Party)} method.
     */
    @Test
    public void testCreateRefund() {
        FinancialAct act = accountFactory.newRefund()
                .customer(customer)
                .till(till)
                .eft().amount(10)
                .add()
                .build();
        FinancialAct transaction = factory.createRefund(act, TEN, terminal, location);
        assertEquals(EFTPOSArchetypes.REFUND, transaction.getArchetype());
        assertTrue(transaction.isNew());
        assertEquals(EFTPOSTransactionStatus.PENDING, transaction.getStatus());
        IMObjectBean bean = getBean(transaction);
        assertEquals(Long.toString(act.getId()), bean.getObject("parentId", ActIdentity.class).getIdentity());
        checkEquals(10, bean.getBigDecimal("amount"));
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(terminal.getObjectReference(), bean.getTargetRef("terminal"));
        assertEquals(location.getObjectReference(), bean.getTargetRef("location"));
    }

    /**
     * Tests the {@link EFTPOSTransactionFactory#createNoTerminalPayment(FinancialAct, BigDecimal, BigDecimal, Party)}
     * method.
     */
    @Test
    public void testCreateNoTerminalPayment() {
        FinancialAct act = accountFactory.newPayment()
                .customer(customer)
                .till(till)
                .eft().amount(10)
                .cashout(5)
                .add()
                .build();
        FinancialAct transaction = factory.createNoTerminalPayment(act, TEN, BigDecimal.valueOf(5), location);
        assertEquals(EFTPOSArchetypes.PAYMENT, transaction.getArchetype());
        assertTrue(transaction.isNew());
        assertEquals(EFTPOSTransactionStatus.NO_TERMINAL, transaction.getStatus());
        IMObjectBean bean = getBean(transaction);
        assertEquals(Long.toString(act.getId()), bean.getObject("parentId", ActIdentity.class).getIdentity());
        checkEquals(10, bean.getBigDecimal("amount"));
        checkEquals(5, bean.getBigDecimal("cashout"));
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertNull(bean.getTargetRef("terminal"));
        assertEquals(location.getObjectReference(), bean.getTargetRef("location"));
    }

    /**
     * Tests the {@link EFTPOSTransactionFactory#createRefund(FinancialAct, BigDecimal, Entity, Party)} method.
     */
    @Test
    public void testCreateNoTerminalRefund() {
        FinancialAct act = accountFactory.newRefund()
                .customer(customer)
                .till(till)
                .eft().amount(10)
                .add()
                .build();
        FinancialAct transaction = factory.createNoTerminalRefund(act, TEN, location);
        assertEquals(EFTPOSArchetypes.REFUND, transaction.getArchetype());
        assertTrue(transaction.isNew());
        assertEquals(EFTPOSTransactionStatus.NO_TERMINAL, transaction.getStatus());
        IMObjectBean bean = getBean(transaction);
        assertEquals(Long.toString(act.getId()), bean.getObject("parentId", ActIdentity.class).getIdentity());
        checkEquals(10, bean.getBigDecimal("amount"));
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertNull(bean.getTargetRef("terminal"));
        assertEquals(location.getObjectReference(), bean.getTargetRef("location"));
    }
}
