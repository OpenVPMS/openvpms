/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.i18n;

import org.junit.Test;
import org.openvpms.component.i18n.Message;
import org.openvpms.eftpos.transaction.Transaction;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link EFTPOSMessages} class.
 *
 * @author Tim Anderson
 */
public class EFTPOSMessagesTestCase {

    /**
     * Tests the {@link EFTPOSMessages#serviceUnavailable(String)} method.
     */
    @Test
    public void testServiceUnavailable() {
        check("EFTPOS-0001: EFTPOS service unavailable for Terminal: foo", EFTPOSMessages.serviceUnavailable("foo"));
    }

    /**
     * Tests the {@link EFTPOSMessages#cannotChangeStatus(Transaction.Status, Transaction.Status)} method.
     */
    @Test
    public void testCannotChangeStatus() {
        check("EFTPOS-0100: Cannot change transaction status from APPROVED to ERROR",
              EFTPOSMessages.cannotChangeStatus(Transaction.Status.APPROVED, Transaction.Status.ERROR));
    }

    /**
     * Tests the {@link EFTPOSMessages#differentTxnIdentifierArchetype(String, String)} method.
     */
    @Test
    public void testDifferentTxnIdentifierArchetype() {
        check("EFTPOS-0101: Cannot change transaction identifier archetypes from a to b",
              EFTPOSMessages.differentTxnIdentifierArchetype("a", "b"));
    }

    /**
     * Verifies a message matches that expected.
     *
     * @param expected the expected message
     * @param actual   the actual message
     */
    private void check(String expected, Message actual) {
        assertEquals(expected, actual.toString());
    }
}
