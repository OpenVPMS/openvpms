/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;

/**
 * Factory for EFTPOS transactions.
 *
 * @author Tim Anderson
 */
public class EFTPOSTransactionFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link EFTPOSTransactionFactory}.
     *
     * @param service the archetype service
     */
    public EFTPOSTransactionFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new <em>act.EFTPOSPayment</em>.
     *
     * @param parentPayment the parent customer payment, used for reconciliation purposes
     * @param amount        the payment amount
     * @param cashout       the cashout amount
     * @param terminal      the EFTPOS terminal
     * @param location      the practice location
     * @return a new payment
     */
    public FinancialAct createPayment(FinancialAct parentPayment, BigDecimal amount, BigDecimal cashout,
                                      Entity terminal, Party location) {
        return createTransaction(parentPayment, EFTPOSArchetypes.PAYMENT, amount, cashout, null, terminal,
                                 location);
    }

    /**
     * Creates a new <em>act.EFTPOSRefund</em>.
     *
     * @param parentRefund the parent customer refund, used for reconciliation purposes
     * @param amount       the refund amount
     * @param terminal     the EFTPOS terminal
     * @param location     the practice location
     * @return a new refund
     */
    public FinancialAct createRefund(FinancialAct parentRefund, BigDecimal amount, Entity terminal, Party location) {
        return createTransaction(parentRefund, EFTPOSArchetypes.REFUND, amount, null, null, terminal, location);
    }

    /**
     * Creates a payment used to record that an EFTPOS transaction was performed, but the terminal or EFTPOS service
     * wasn't available.
     *
     * @param parentPayment the parent customer payment, used for reconciliation purposes
     * @param amount        the payment amount
     * @param cashout       the cashout amount
     * @param location      the practice location
     * @return a new payment
     */
    public FinancialAct createNoTerminalPayment(FinancialAct parentPayment, BigDecimal amount, BigDecimal cashout,
                                                Party location) {
        return createTransaction(parentPayment, EFTPOSArchetypes.PAYMENT, amount, cashout,
                                 EFTPOSTransactionStatus.NO_TERMINAL, null, location);
    }

    /**
     * Creates a refund used to record that an EFTPOS transaction was performed, but the terminal or EFTPOS service
     * wasn't available.
     *
     * @param parentRefund the parent customer refund, used for reconciliation purposes
     * @param amount       the refund amount
     * @param location     the practice location
     * @return a new refund
     */
    public FinancialAct createNoTerminalRefund(FinancialAct parentRefund, BigDecimal amount, Party location) {
        return createTransaction(parentRefund, EFTPOSArchetypes.REFUND, amount, null,
                                 EFTPOSTransactionStatus.NO_TERMINAL, null, location);
    }

    /**
     * Creates a new EFTPOS transaction.
     *
     * @param parent    the parent customer act, used for reconciliation purposes
     * @param archetype the transaction archetype
     * @param amount    the transaction amount
     * @param cashout   the cashout amount, or {@code null} to leave unset
     * @param status    the status, or {@code null} to use the default
     * @param terminal  the EFTPOS terminal. May be {@code null}
     * @param location  the practice location
     * @return a new transaction
     */
    private FinancialAct createTransaction(FinancialAct parent, String archetype, BigDecimal amount,
                                           BigDecimal cashout, String status, Entity terminal, Party location) {
        if (parent.isNew()) {
            throw new IllegalStateException("Parent act must be saved");
        }
        IMObjectBean parentBean = service.getBean(parent);
        Reference customer = parentBean.getTargetRef("customer");
        if (customer == null) {
            throw new IllegalStateException("Parent act has no customer");
        }
        FinancialAct act = service.create(archetype, FinancialAct.class);
        ActIdentity identity = service.create(EFTPOSArchetypes.PARENT_ID, ActIdentity.class);
        identity.setIdentity(Long.toString(parent.getId()));
        act.addIdentity(identity);

        IMObjectBean bean = service.getBean(act);
        bean.setValue("amount", amount);
        if (cashout != null) {
            bean.setValue("cashout", cashout);
        }
        if (status != null) {
            act.setStatus(status);
        }
        bean.setTarget("customer", customer);
        if (terminal != null) {
            bean.setTarget("terminal", terminal);
        }
        bean.setTarget("location", location);
        return act;
    }

}
