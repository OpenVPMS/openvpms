/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.transaction;

import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.math.Currency;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.practice.Location;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.eftpos.transaction.TransactionUpdater;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.openvpms.component.model.bean.Predicates.targetIsA;

/**
 * Default implementation of {@link Transaction}.
 *
 * @author Tim Anderson
 */
public class TransactionImpl implements Transaction {

    /**
     * The transaction act.
     */
    private final Act act;

    /**
     * Bean wrapping the act.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * Transaction id node.
     */
    static final String TRANSACTION_ID = "transactionId";

    /**
     * Authorisation code node.
     */
    static final String AUTHORISATION_CODE = "authorisationCode";

    /**
     * Retrieval reference number node.
     */
    static final String RETRIEVAL_REFERENCE_NUMBER = "retrievalReferenceNumber";

    /**
     * System trace audit number node.
     */
    static final String SYSTEM_TRACE_AUDIT_NUMBER = "systemTraceAuditNumber";

    /**
     * Constructs a {@link TransactionImpl}.
     *
     * @param act             the act
     * @param service         the archetype service
     * @param domainService   the domain service
     * @param practiceService the practice service
     */
    public TransactionImpl(Act act, ArchetypeService service, DomainService domainService,
                           PracticeService practiceService) {
        this.act = act;
        this.service = service;
        this.bean = service.getBean(act);
        this.domainService = domainService;
        this.practiceService = practiceService;
    }

    /**
     * Returns the OpenVPMS identifier for the transaction.
     *
     * @return the identifier
     */
    @Override
    public long getId() {
        return act.getId();
    }

    /**
     * Returns the OpenVPMS identifier for the parent payment or refund.
     *
     * @return the parent payment or refund id
     */
    @Override
    public long getParentId() {
        ActIdentity identity = bean.getObject("parentId", ActIdentity.class);
        return identity != null ? Long.parseLong(identity.getIdentity()) : -1;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof TransactionImpl) {
            return act.equals(((TransactionImpl) obj).act);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return act.hashCode();
    }

    /**
     * Returns the transaction identifier, issued by the provider.
     *
     * @return the transaction identifier, or {@code null} if none has been issued
     */
    @Override
    public String getTransactionId() {
        ActIdentity identity = getTransactionIdentity();
        return identity != null ? identity.getIdentity() : null;
    }


    /**
     * Returns the identity for the order, issued by the provider.
     *
     * @return the transaction identifier, or {@code null} if none has been issued
     */
    public ActIdentity getTransactionIdentity() {
        return getActIdentity(TRANSACTION_ID);
    }

    /**
     * Returns the transaction status.
     *
     * @return the transaction status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(act.getStatus());
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    @Override
    public String getMessage() {
        return bean.getString("message");
    }

    /**
     * Determines if the transaction is complete.
     * <p/>
     * A transaction is complete if it has {@link Status#APPROVED}, {@link Status#DECLINED}, {@link Status#ERROR}
     * or {@link Status#NO_TERMINAL} status.
     *
     * @return {@code true} if the transaction is complete, otherwise {@code false}
     */
    @Override
    public boolean isComplete() {
        Status status = getStatus();
        return status == Status.APPROVED || status == Status.DECLINED || status == Status.ERROR
               || status == Status.NO_TERMINAL;
    }

    /**
     * Returns the customer the transaction is for.
     *
     * @return the customer
     */
    @Override
    public Party getCustomer() {
        return bean.getTarget("customer", Party.class);
    }

    /**
     * Returns the terminal to use.
     *
     * @return the terminal, or {@code null} if the status is {@link Status#NO_TERMINAL}
     */
    @Override
    public Terminal getTerminal() {
        Terminal result;
        if (getStatus() == Status.NO_TERMINAL) {
            result = null;
        } else {
            Entity terminal = bean.getTarget("terminal", Entity.class);
            if (terminal == null) {
                throw new IllegalStateException("Transaction has no terminal");
            }
            result = domainService.create(terminal, Terminal.class);
        }
        return result;
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    @Override
    public Location getLocation() {
        Party location = bean.getTarget("location", Party.class);
        if (location == null) {
            throw new IllegalStateException("Transaction has no location");
        }
        return domainService.create(location, Location.class);
    }

    /**
     * Returns the transaction amount.
     *
     * @return the transaction amount
     */
    @Override
    public BigDecimal getAmount() {
        return bean.getBigDecimal("amount", BigDecimal.ZERO);
    }

    /**
     * Returns the transaction currency code.
     * <p/>
     * This is a 3 character code as specified by ISO 4217.
     *
     * @return the transaction currency code
     */
    @Override
    public String getCurrencyCode() {
        Currency currency = practiceService.getCurrency();
        if (currency == null) {
            throw new IllegalStateException("No practice currency");
        }
        return currency.getCode();
    }

    /**
     * Returns the card type.
     *
     * @return the card type. May be {@code null}
     */
    @Override
    public String getCardType() {
        return bean.getString("cardType");
    }

    /**
     * Returns the authorisation code.
     *
     * @return the authorisation code. May be {@code null}
     */
    @Override
    public String getAuthorisationCode() {
        return getIdentity(AUTHORISATION_CODE);
    }

    /**
     * Returns the response code.
     *
     * @return the response code. May be {@code null}
     */
    @Override
    public String getResponseCode() {
        return bean.getString("responseCode");
    }

    /**
     * Returns the response text.
     *
     * @return the response text. May be {@code null}
     */
    @Override
    public String getResponse() {
        return bean.getString("response");
    }

    /**
     * Returns the masked card number.
     *
     * @return the masked card number. May be {@code null}
     */
    @Override
    public String getMaskedCardNumber() {
        return bean.getString("maskedCardNumber");
    }

    /**
     * Returns the retrieval reference number (RRN).
     *
     * @return the retrieval reference number
     */
    @Override
    public String getRetrievalReferenceNumber() {
        return getIdentity(RETRIEVAL_REFERENCE_NUMBER);
    }

    /**
     * Returns the system trace audit number (STAN).
     *
     * @return the system trace audit number. May be {@code null}
     */
    @Override
    public String getSystemTraceAuditNumber() {
        return getIdentity(SYSTEM_TRACE_AUDIT_NUMBER);
    }

    /**
     * Returns the merchant transaction receipts.
     *
     * @return the merchant transaction receipts
     */
    @Override
    public List<Receipt> getMerchantReceipts() {
        return getReceipts(EFTPOSArchetypes.MERCHANT_RECEIPT);
    }

    /**
     * Returns the customer transaction receipts.
     *
     * @return the customer transaction receipts
     */
    @Override
    public List<Receipt> getCustomerReceipts() {
        return getReceipts(EFTPOSArchetypes.CUSTOMER_RECEIPT);
    }

    /**
     * Returns an updater to change the state of the transaction.
     *
     * @return the updater
     */
    @Override
    public TransactionUpdater state() {
        return new TransactionUpdaterImpl(this, bean, service);
    }

    /**
     * Returns the bean wrapping the act.
     *
     * @return the bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }

    /**
     * Returns an identity.
     *
     * @param node the identity node
     * @return the identity, or {@code null} if none is present
     */
    private String getIdentity(String node) {
        ActIdentity identity = getActIdentity(node);
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Returns an identity.
     *
     * @param node the identity node
     * @return the identity, or {@code null} if none is present
     */
    private ActIdentity getActIdentity(String node) {
        return bean.getObject(node, ActIdentity.class);
    }

    /**
     * Returns the receipts of the specified archetype.
     *
     * @param archetype the receipt archetype
     * @return the corresponding receipts
     */
    private List<Receipt> getReceipts(String archetype) {
        List<Receipt> result = new ArrayList<>();
        Policy<SequencedRelationship> policy = Policies.newPolicy(SequencedRelationship.class)
                .anyObject()
                .predicate(targetIsA(archetype))
                .orderBySequence()
                .build();
        for (DocumentAct receipt : bean.getTargets("receipts", DocumentAct.class, policy)) {
            result.add(new ReceiptImpl(receipt, service));
        }
        return result;
    }

}
