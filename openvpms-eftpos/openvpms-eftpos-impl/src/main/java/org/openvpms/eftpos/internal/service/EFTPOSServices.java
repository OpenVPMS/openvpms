/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.eftpos.exception.EFTPOSException;
import org.openvpms.eftpos.internal.i18n.EFTPOSMessages;
import org.openvpms.eftpos.service.EFTPOSService;
import org.openvpms.plugin.manager.PluginManager;

import java.util.List;

/**
 * EFTPOS service locator.
 *
 * @author Tim Anderson
 */
public class EFTPOSServices {

    /**
     * The plugin manager.
     */
    private final PluginManager manager;

    /**
     * Constructs an {@link EFTPOSServices}.
     *
     * @param manager the plugin manager
     */
    public EFTPOSServices(PluginManager manager) {
        this.manager = manager;
    }

    /**
     * Returns the EFTPOS service for a terminal.
     *
     * @param terminal the terminal
     * @return the corresponding EFTPOS service
     * @throws EFTPOSException if the service is unavailable
     */
    public EFTPOSService getService(Entity terminal) {
        EFTPOSService result = null;
        List<EFTPOSService> services = manager.getServices(EFTPOSService.class);
        String archetype = terminal.getArchetype();
        for (EFTPOSService service : services) {
            if (archetype.equals(service.getTerminalArchetype())) {
                result = service;
                break;
            }
        }
        if (result == null) {
            throw new EFTPOSException(EFTPOSMessages.serviceUnavailable(terminal.getName()));
        }
        return result;
    }
}
