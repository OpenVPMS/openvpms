/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.terminal;

import org.openvpms.component.business.domain.im.common.BeanEntityDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.terminal.Terminal;

/**
 * Default implementation of {@link Terminal}.
 *
 * @author Tim Anderson
 */
public class TerminalImpl extends BeanEntityDecorator implements Terminal {

    /**
     * The printer node.
     */
    private static final String PRINTER = "printer";

    /**
     * Constructs a {@link TerminalImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public TerminalImpl(Entity peer, ArchetypeService service) {
        super(peer, service);
    }

    /**
     * Determines if the terminal prints receipts.
     *
     * @return {@code true} if the terminal is responsible for receipt printing, otherwise {@code false}
     */
    @Override
    public boolean isReceiptPrinter() {
        IMObjectBean bean = getBean();
        return bean.hasNode(PRINTER) && bean.getBoolean(PRINTER);
    }
}
