/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.sample.internal.service;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.exception.EFTPOSException;
import org.openvpms.eftpos.service.ManagedTransactionDisplay;
import org.openvpms.eftpos.service.Prompt;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Transaction;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.LockSupport;

/**
 * Sample implementation of the {@link ManagedTransactionDisplay} interface.
 *
 * @author Tim Anderson
 */
public class TransactionDisplayImpl implements ManagedTransactionDisplay {

    /**
     * The transaction.
     */
    private final Transaction transaction;

    /**
     * The delay between state changes of a transaction.
     */
    private final long delay;

    /**
     * The time the transaction was last updated.
     */
    private long lastUpdate;

    /**
     * The terminal messages.
     */
    private final List<String> messages = new ArrayList<>();

    /**
     * The current state.
     */
    private State state = State.PRESENT_CARD;

    /**
     * Used to generate random strings.
     */
    private final Random random = new Random();

    private static final String PRESENT_CARD_MSG = "PRESENT/INSERT\nSWIPE CARD";

    private static final String PROCESSING_MSG = "PROCESSING";

    private static final String SELECT_ACCOUNT_MSG = "SELECT ACCOUNT\nCHQ SAV CRD";

    private static final String SIGNATURE_MSG = "SIGNATURE OK?\nYES/NO";

    private static final String PIN_MSG = "AWAITING PIN";

    private static final String APPROVED_MSG = "APPROVED";

    private static final String DECLINED_MSG = "DECLINED";

    private static final String CANCEL = "CANCEL";

    /**
     * Constructs a {@link TransactionDisplayImpl}.
     *
     * @param transaction the transaction
     * @param service     the archetype service
     */
    public TransactionDisplayImpl(Transaction transaction, ArchetypeService service) {
        this.transaction = transaction;
        IMObjectBean bean = service.getBean(transaction.getTerminal());

        lastUpdate = System.currentTimeMillis();
        delay = bean.getLong("delay", 5) * 1000;
    }

    /**
     * Returns the prompt.
     *
     * @return the prompt, or {@code null} if no prompt is required
     */
    @Override
    public synchronized Prompt getPrompt() {
        if (state == State.PRESENT_CARD) {
            return new PromptImpl(PRESENT_CARD_MSG, null, CANCEL);
        } else if (state == State.SELECT_ACCOUNT) {
            return new PromptImpl(SELECT_ACCOUNT_MSG, null, CANCEL);
        } else if (state == State.CONFIRM_SIGNATURE) {
            return new PromptImpl(SIGNATURE_MSG, "YES", "NO");
        }
        return null;
    }

    /**
     * Returns the messages.
     *
     * @return the messages
     */
    @Override
    public synchronized List<String> getMessages() {
        return messages;
    }

    /**
     * Updates the display.
     *
     * @return {@code true} if the display changed, otherwise {@code false}
     * @throws EFTPOSException for any error
     */
    @Override
    public synchronized boolean update() {
        boolean updated = false;
        delay();
        if (!transaction.isComplete()) {
            switch (state) {
                case PRESENT_CARD:
                    messages.add(PRESENT_CARD_MSG);
                    state = State.PROCESSING;
                    break;
                case PROCESSING:
                    messages.add(PROCESSING_MSG);
                    state = State.SELECT_ACCOUNT;
                    break;
                case SELECT_ACCOUNT:
                    messages.add(SELECT_ACCOUNT_MSG);
                    if (usePin()) {
                        state = State.ENTER_PIN;
                    } else {
                        state = State.SIGNATURE;
                    }
                    break;
                case ENTER_PIN:
                    messages.add(PIN_MSG);
                    state = State.RESULT;
                    break;
                case SIGNATURE:
                    transaction.state()
                            .addMerchantReceipt(createMerchantReceipt(transaction, new Date()), true)
                            .update();
                    state = State.CONFIRM_SIGNATURE;
                    break;
                case CONFIRM_SIGNATURE:
                    // no-op
                    break;
                case RESULT:
                    if (transaction.getAmount().toString().endsWith("1")) {
                        declined();
                    } else {
                        approved();
                    }
                    break;
            }
            updated = true;
        }
        return updated;
    }

    /**
     * Returns the transaction.
     *
     * @return the transaction
     */
    @Override
    public synchronized Transaction getTransaction() {
        return transaction;
    }

    /**
     * Determines if the transaction is complete.
     *
     * @return {@code true} if the transaction is complete, otherwise {@code false}
     */
    @Override
    public synchronized boolean isComplete() {
        return transaction.isComplete();
    }

    /**
     * Cancels the transaction.
     *
     * @throws EFTPOSException for any error
     */
    @Override
    public synchronized void cancel() {
        transaction.state()
                .status(Transaction.Status.ERROR, "Cancelled by user")
                .update();
    }

    private void approved() {
        messages.add(APPROVED_MSG);
        completed(Transaction.Status.APPROVED, "00");
    }

    private void declined() {
        messages.add(DECLINED_MSG);
        completed(Transaction.Status.DECLINED, null);
    }

    private void completed(Transaction.Status status, String authorisationCode) {
        String cardType = getCardType();
        String cardNumber = getCard();
        String reference = getReference();
        boolean payment = transaction instanceof Payment;
        transaction.state()
                .status(status)
                .cardType(getCardType())
                .authorisationCode(authorisationCode)
                .maskedCardNumber(cardNumber)
                .addCustomerReceipt(createCustomerReceipt(status == Transaction.Status.APPROVED,
                                                          transaction.getAmount(), payment,
                                                          new Date(), cardType, cardNumber,
                                                          authorisationCode + random(4),
                                                          reference))
                .update();
    }

    private boolean usePin() {
        return !"MasterCard".equals(getCardType());
    }

    /**
     * Add a delay between transaction updates.
     */
    private void delay() {
        long now = System.currentTimeMillis();
        long diff = now - lastUpdate;
        if (diff < delay) {
            LockSupport.parkUntil(now + (delay - diff));
        }
        lastUpdate = System.currentTimeMillis();
    }

    private String getCardType() {
        long amount = transaction.getAmount().toBigInteger().longValue();
        if (amount % 3 == 0) {
            return "EFTPOS";
        } else if (amount % 2 == 0) {
            return "MasterCard";
        } else {
            return "VISA";
        }
    }

    /**
     * Generates the masked card number.
     *
     * @return the masked card number
     */
    private String getCard() {
        // use the transaction id to generate the card number
        return "543111******" + format(transaction.getId(), 4);
    }

    /**
     * Generates the authorisation number from the transaction.
     *
     * @return the masked card number
     */
    private String getAuthorisation() {
        // use the parent transaction id to generate the authorisation
        return format(transaction.getParentId(), 6);
    }

    private String getReference() {
        // use the transaction id to generate the reference
        return StringUtils.reverse(format(transaction.getId(), 6));
    }

    private String format(long value, int length) {
        return String.format("%0" + length + "d", value);
    }

    private String createMerchantReceipt(Transaction transaction, Date date) {
        String cardType = getCardType();
        String card = getCard();
        BigDecimal amount = transaction.getAmount();
        return "OpenVPMS\n12 Wherever St\n\n" +
               "*---------------EFTPOS--------------*\n\n" +
               padLine(DateFormat.getDateTimeInstance().format(date), "CREDIT") +
               padLine(cardType, "SWIPE") +
               padLine("CARD", card) +
               padLine("AUHORISATION", getAuthorisation()) +
               padLine("REFERENCE", getReference()) +
               padLine("PURCHASE", format(amount)) +
               padLine("TOTAL", format(amount)) +
               "\n" +
               "              APPROVED\n\n" +
               "            PLEASE SIGN BELOW\n\n\n" +
               "*-----------------------------------*\n\n" +
               "             MERCHANT COPY\n" +
               "             PLEASE RETAIN\n" +
               "            FOR YOUR RECORDS\n";
    }

    private String createCustomerReceipt(boolean approved, BigDecimal amount, boolean payment, Date date,
                                         String cardType, String card,
                                         String authorisation, String reference) {
        StringBuilder result = new StringBuilder();
        result.append("OpenVPMS\n12 Wherever St\n\n" +
                      "*---------------EFTPOS--------------*\n\n");
        result.append(padLine(DateFormat.getDateTimeInstance().format(date), "CREDIT"));
        result.append(padLine(cardType, "SWIPE"));
        result.append(padLine("CARD", card));
        if (approved) {
            result.append(padLine("AUTHORISATION", authorisation));
        }
        result.append(padLine("REFERENCE", reference));
        result.append(padLine((payment) ? "PURCHASE" : "REFUND", format(amount)));
        result.append(padLine("TOTAL", format(amount)));
        result.append("\n");
        if (approved) {
            result.append("              APPROVED\n\n" +
                          "            PIN VERIFIED\n\n");
        } else {
            result.append("              DECLINED\n\n");
        }
        result.append("*-----------------------------------*\n\n" +
                      "             CUSTOMER COPY\n" +
                      "             PLEASE RETAIN\n" +
                      "            FOR YOUR RECORDS\n");

        return result.toString();
    }

    /**
     * Pads the left and right portion of a string to 37 characters, representing the width of the EFTPOS receipt.
     *
     * @param left  the left part of the string
     * @param right the right of the string
     * @return the padded string
     */
    private String padLine(String left, String right) {
        return left + String.format("%" + (37 - left.length()) + "s", right) + "\n";
    }

    /**
     * Formats a currency amount.
     *
     * @param amount the amount
     * @return the formatted amount
     */
    private String format(BigDecimal amount) {
        DecimalFormat format = new DecimalFormat();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        return "AUD" + format.format(amount); // should use practice currency code
    }

    /**
     * Creates a random number of the specified length.
     *
     * @param length the number length
     * @return the stringified number
     */
    private String random(int length) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            result.append((char) ('0' + random.nextInt(10)));
        }
        return result.toString();
    }

    private class PromptImpl implements Prompt {

        private final String message;

        private final List<Option> options = new ArrayList<>();

        public PromptImpl(String message, String option1, String option2) {
            this.message = message;
            if (option1 != null) {
                options.add(new OptionImpl(option1));
            }
            if (option2 != null) {
                options.add(new OptionImpl(option2));
            }
        }

        /**
         * Returns the prompt message.
         *
         * @return the prompt message
         */
        @Override
        public String getMessage() {
            return message;
        }

        /**
         * Returns the prompt options.
         *
         * @return the prompt options
         */
        @Override
        public List<Option> getOptions() {
            return options;
        }

        /**
         * Sends the selected option.
         *
         * @param option the option
         * @throws EFTPOSException for any POS error
         */
        @Override
        public void send(Option option) {
            OptionImpl opt = (OptionImpl) option;
            synchronized (TransactionDisplayImpl.this) {
                if (opt.isCancel()) {
                    if (!transaction.isComplete()) {
                        transaction.state()
                                .status(Transaction.Status.ERROR, "Cancelled by user")
                                .update();
                    }
                } else if (opt.isYes()) {
                    if (state == State.CONFIRM_SIGNATURE) {
                        approved();
                    }
                } else if (opt.isNo() && state == State.CONFIRM_SIGNATURE) {
                    declined();
                }
            }
        }
    }

    private static class OptionImpl implements Prompt.Option {

        private final String option;

        private OptionImpl(String option) {
            this.option = option;
        }

        /**
         * Returns the display name for the option.
         *
         * @return the display name
         */
        @Override
        public String getText() {
            return option;
        }

        public boolean isCancel() {
            return CANCEL.equals(option);
        }

        public boolean isYes() {
            return "YES".equals(option);
        }

        public boolean isNo() {
            return "NO".equals(option);
        }
    }

    enum State {
        PRESENT_CARD,
        PROCESSING,
        SELECT_ACCOUNT,
        ENTER_PIN,
        SIGNATURE,
        CONFIRM_SIGNATURE,
        RESULT
    }
}
