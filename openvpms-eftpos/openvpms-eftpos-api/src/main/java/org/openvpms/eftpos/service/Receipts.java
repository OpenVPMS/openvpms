/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

import org.openvpms.eftpos.exception.EFTPOSException;

/**
 * Transaction receipts.
 *
 * @author Tim Anderson
 */
public interface Receipts {

    /**
     * Returns the merchant receipt, if available.
     *
     * @param signature if {@code true}, want the receipt with a signature placeholder
     * @return the merchant receipt, or {@code null} if there is none
     * @throws EFTPOSException for any error
     */
    String getMerchantReceipt(boolean signature);

    /**
     * Returns the customer receipt, if available.
     *
     * @return the customer receipt, or {@code null} if there is none
     * @throws EFTPOSException for any error
     */
    String getCustomerReceipt();

}