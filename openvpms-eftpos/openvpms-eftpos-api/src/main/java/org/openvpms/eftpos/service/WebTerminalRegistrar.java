/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

/**
 * An {@link TerminalRegistrar} where the terminal registration is performed by the the EFTPOS provider in the browser,
 * via a url.
 * <p/>
 * This will be displayed in an iframe, in a window with a fixed width and height.
 * <p/>
 * On completion of registration, the supplied callback should be invoked, indicating success or failure.
 *
 * @author Tim Anderson
 */
public interface WebTerminalRegistrar extends TerminalRegistrar {

    /**
     * Returns the terminal registration URL.
     *
     * @return the terminal registration URL
     */
    String getUrl();

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels
     */
    int getWidth();

    /**
     * Returns the window height.
     *
     * @return the window height, in pixels
     */
    int getHeight();

}
