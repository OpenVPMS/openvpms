package org.openvpms.component.system.common.crypto;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.security.crypto.encrypt.Encryptors;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Tests the {@link Base64EncodingPasswordEncryptor}.
 *
 * @author Tim Anderson
 */
public class Base64EncodingPasswordEncryptorTestCase {

    /**
     * Tests encryption and decryption.
     */
    @Test
    public void testEncryptDecrypt() {
        Base64EncodingPasswordEncryptor encryptorA = new Base64EncodingPasswordEncryptor(
                Encryptors.stronger("test", "baddecaf"));

        for (int i = 1; i < 16; ++i) {
            String password = StringUtils.repeat("x", i);  // make sure passwords of different lengths are supported

            String encrypted1 = encryptorA.encrypt(password);
            assertEquals(password, encryptorA.decrypt(encrypted1));

            // verify that encrypting the same password again yields a different encryption
            String encrypted2 = encryptorA.encrypt(password);
            assertNotEquals(encrypted1, encrypted2);
            assertEquals(password, encryptorA.decrypt(encrypted2));

            // verify that using another encryptor with the same parameters can decrypt prior encrypted strings
            Base64EncodingPasswordEncryptor encryptorB = new Base64EncodingPasswordEncryptor(
                    Encryptors.stronger("test", "baddecaf"));

            assertEquals(password, encryptorB.decrypt(encrypted1));
            assertEquals(password, encryptorB.decrypt(encrypted2));
        }
    }

    /**
     * Verifies that multi-byte characters are supported.
     */
    @Test
    public void testEncoding() {
        String multibyte = "Ç";
        assertEquals(2, multibyte.getBytes(StandardCharsets.UTF_8).length);
        Base64EncodingPasswordEncryptor encryptor = new Base64EncodingPasswordEncryptor(
                Encryptors.stronger("test", "baddecaf"));

        String encrypted = encryptor.encrypt(multibyte);
        assertEquals(multibyte, encryptor.decrypt(encrypted));
    }
}
