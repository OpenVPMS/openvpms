/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.cache;

import org.junit.Test;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * Tests the {@link MapIMObjectCacheTestCase}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("/org/openvpms/component/business/service/archetype/archetype-service-appcontext.xml")
public class MapIMObjectCacheTestCase extends AbstractArchetypeServiceTest {

    /**
     * Tests the {@link MapIMObjectCache#get(Reference)} method.
     */
    @Test
    public void testGet() {
        User userA = createUser();
        User userB = createUser();
        userB.setActive(false);
        User userC = createUser();
        save(userA, userB);

        // create a cache backed by the archetype service
        MapIMObjectCache cache = new MapIMObjectCache(getArchetypeService());
        IMObject object1 = cache.get(userA.getObjectReference());
        assertEquals(object1, userA);
        assertNotSame(object1, userA);

        IMObject object2 = cache.get(userA.getObjectReference());
        assertSame(object1, object2);

        // verify can retrieve by archetype and id
        IMObject object3 = cache.get(new IMObjectReference(userA.getArchetype(), userA.getId()));
        assertSame(object1, object3);

        // verify objects can be retrieved by archetype and id if they aren't cached already
        IMObject object4 = cache.get(new IMObjectReference(userB.getArchetype(), userB.getId()));
        assertEquals(userB, object4);
        assertNotSame(userB, object4);
        assertSame(object4, cache.get(new IMObjectReference(userB.getArchetype(), userB.getId())));
        assertSame(object4, cache.get(userB.getObjectReference()));

        // now remove the object and verify a different instance is returned on next retrieve
        cache.remove(userA);
        cache.remove(userB);

        IMObject object5 = cache.get(userA.getObjectReference());
        assertEquals(userA, object5);
        assertNotSame(userA, object5);

        IMObject object6 = cache.get(new IMObjectReference(userA.getArchetype(), userA.getId()));
        assertEquals(userA, object6);
        assertSame(object5, object6);

        // verify unsaved objects cannot be retrieved
        assertNull(cache.get(userC.getObjectReference()));
        assertNull(cache.get(new IMObjectReference(userC.getArchetype(), userC.getId())));
    }

    /**
     * Tests the {@link MapIMObjectCache#get(Reference, boolean)} method.
     */
    @Test
    public void testGetActiveInactive() {
        User userA = createUser();
        User userB = createUser();
        User userC = createUser();
        User userD = createUser();

        userB.setActive(false);
        save(userA, userB);

        userD.setActive(false);

        MapIMObjectCache cache = new MapIMObjectCache(getArchetypeService());
        IMObject object1 = cache.get(userA.getObjectReference(), true);
        assertEquals(userA, object1);
        assertNotSame(userA, object1);

        // verify objects can be retrieved by archetype and id
        IMObject object2 = cache.get(new IMObjectReference(userA.getArchetype(), userA.getId()), true);
        assertSame(object2, object1);

        // verify active objects not retrieved when active = false
        assertNull(cache.get(userA.getObjectReference(), false));
        assertNull(cache.get(new IMObjectReference(userA.getArchetype(), userA.getId()), false));

        // remove the object from the cache and verify a different instance is returned on next get
        cache.remove(object1);
        IMObject object3 = cache.get(userA.getObjectReference(), true);
        assertEquals(userA, object3);
        assertNotSame(object1, object3);

        // verify inactive not retrieved when active = true
        assertNull(cache.get(userB.getObjectReference(), true));
        assertNull(cache.get(new IMObjectReference(userB.getArchetype(), userB.getId()), true));

        // verify inactive retrieved when active = false
        IMObject object4 = cache.get(new IMObjectReference(userB.getArchetype(), userB.getId()), false);
        assertEquals(userB, object4);
        assertNotSame(userB, object4);
        assertSame(object4, cache.get(userB.getObjectReference(), false));

        // now check unsaved objects
        assertNull(cache.get(userC.getObjectReference(), true));
        assertNull(cache.get(userD.getObjectReference(), false));

        cache.add(userC);
        cache.add(userD);
        assertSame(userC, cache.get(userC.getObjectReference(), true));
        assertNull(cache.get(new IMObjectReference(userC.getArchetype(), userC.getId()), true)); // can't retrieve by id
        assertNull(cache.get(userC.getObjectReference(), false));
        assertNull(cache.get(new IMObjectReference(userC.getArchetype(), userC.getId()), false));

        assertSame(userD, cache.get(userD.getObjectReference(), false));
        assertNull(cache.get(new IMObjectReference(userD.getArchetype(), userD.getId()), false));// can't retrieve by id
        assertNull(cache.get(userD.getObjectReference(), true));
        assertNull(cache.get(new IMObjectReference(userD.getArchetype(), userD.getId()), true));

        cache.remove(userC);
        cache.remove(userD);
        assertNull(cache.get(userC.getObjectReference(), true));
        assertNull(cache.get(userD.getObjectReference(), false));
    }

    /**
     * Tests the {@link MapIMObjectCache#getCached(Reference)} method.
     */
    @Test
    public void testGetCached() {
        User userA = createUser();
        save(userA);

        MapIMObjectCache cache = new MapIMObjectCache(getArchetypeService());
        assertNull(cache.getCached(userA.getObjectReference()));

        IMObject object1 = cache.get(userA.getObjectReference());
        assertEquals(userA, object1);

        IMObject object2 = cache.getCached(userA.getObjectReference());
        assertSame(object1, object2);

        // verify it can be retrieved by archetype and id
        Reference secondaryA = new IMObjectReference("security.user", userA.getId());
        IMObject object3 = cache.getCached(secondaryA);
        assertSame(object1, object3);

        // now remove the user from the cache and verify it can no longer be retrieved
        cache.remove(userA);
        assertNull(cache.getCached(userA.getObjectReference()));
        assertNull(cache.getCached(secondaryA));

        // now test caching of unsaved objects
        User userB = createUser();
        assertNull(cache.getCached(userB.getObjectReference()));
        Reference secondaryB = new IMObjectReference("security.user", userB.getId());
        assertNull(cache.getCached(secondaryB));

        // add the object to the cache
        cache.add(userB);
        assertSame(userB, cache.getCached(userB.getObjectReference()));
        assertNull(cache.getCached(secondaryB));  // can't retrieve by id for unsaved objects

        // now save it. Should still be able to retrieve by reference but not secondary key
        save(userB);
        assertSame(userB, cache.getCached(userB.getObjectReference()));
        assertNull(cache.getCached(secondaryB));
    }
}
