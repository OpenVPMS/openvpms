/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link StringUtilities} class.
 *
 * @author Tim Anderson
 */
public class StringUtilitiesTestCase {

    /**
     * Tests the {@link StringUtilities#hasControlChars(String)} method.
     */
    @Test
    public void testHasControlChars() {
        // check null, empty handling
        assertFalse(StringUtilities.hasControlChars(null));
        assertFalse(StringUtilities.hasControlChars(""));

        // check control chars <= 31. \r, \n, and \t should be false
        for (char ch = 0; ch < 32; ++ch) {
            boolean isControl = ch != '\r' && ch != '\n' && ch != '\t';
            checkHasControlChars(ch, isControl);
        }
        // check remaining chars up to 255. 0x7F (delete) should be true
        for (char ch = 32; ch <= 255; ++ch) {
            boolean isControl = ch == '\u007F';  // delete char
            checkHasControlChars(ch, isControl);
        }

        // check multiple control chars
        assertTrue(StringUtilities.hasControlChars("abc\u0000def\u007F"));
        assertTrue(StringUtilities.hasControlChars("\nabc\u0000def\u007F"));
        assertTrue(StringUtilities.hasControlChars("\nabc\u0000\u007Fdef"));
    }

    /**
     * Tests the {@link StringUtilities#replaceControlChars(String, String)} method.
     */
    @Test
    public void testReplaceControlChars() {
        // check null, empty handling
        assertNull(StringUtilities.replaceControlChars(null, " "));
        assertEquals("", StringUtilities.replaceControlChars("", " "));

        // check control chars <= 31. \r, \n, and \t should be false
        for (char ch = 0; ch < 32; ++ch) {
            boolean isControl = ch != '\r' && ch != '\n' && ch != '\t';
            checkReplaceControlChars(ch, isControl);
        }
        // check remaining chars up to 255. 0x7F (delete) should be true
        for (char ch = 32; ch <= 255; ++ch) {
            boolean isControl = ch == '\u007F';  // delete char
            checkReplaceControlChars(ch, isControl);
        }

        // check multiple control chars
        assertEquals("abc def ", StringUtilities.replaceControlChars("abc\u0000def\u007F", " "));
        assertEquals("\nabc def ", StringUtilities.replaceControlChars("\nabc\u0000def\u007F", " "));
        assertEquals("\nabc  def", StringUtilities.replaceControlChars("\nabc\u0000\u007Fdef", " "));
        assertTrue(StringUtilities.hasControlChars("abc\u0000def\u007F"));
        assertTrue(StringUtilities.hasControlChars("\nabc\u0000def\u007F"));
    }

    /**
     * Verifies that {@link StringUtilities#hasControlChars(String)} returns the expected result for a character.
     * <p/>
     * This checks single and multi-line strings.
     *
     * @param ch        the character to check
     * @param isControl if {@code true}, the character is a control character, else it is a normal character
     */
    private void checkHasControlChars(char ch, boolean isControl) {
        checkHasControlChars(ch + "abc", ch, isControl);             // start of string, no whitespace
        checkHasControlChars(ch + " abc", ch, isControl);            // start of string, whitespace
        checkHasControlChars(ch + "\tabc", ch, isControl);           // start of string, whitespace
        checkHasControlChars("abc" + ch + "def", ch, isControl);     // in string, no whitespace
        checkHasControlChars("abc " + ch + " def", ch, isControl);   // in string, whitespace
        checkHasControlChars("abc\t" + ch + "\tdef", ch, isControl); // in string, whitespace
        checkHasControlChars("abc" + ch, ch, isControl);             // end of string, no whitespace
        checkHasControlChars("abc " + ch, ch, isControl);            // end of string, whitespace
        checkHasControlChars("abc\t" + ch, ch, isControl);           // end of string, whitespace

        checkHasControlChars(ch + "abc\ndef", ch, isControl);                // start of multiline string
        checkHasControlChars(ch + "abc\r\ndef", ch, isControl);              // start of multiline string
        checkHasControlChars("abc\ndef" + ch + "\nghi", ch, isControl);      // in multiline string
        checkHasControlChars("abc\r\ndef" + ch + "\r\nghi", ch, isControl);  // in multiline string
        checkHasControlChars("abc\n" + ch, ch, isControl);                   // end of multiline string
        checkHasControlChars("abc\r\n " + ch, ch, isControl);                // end of multiline string
    }

    /**
     * Verifies that {@link StringUtilities#hasControlChars(String)} returns the expected result for a string.
     *
     * @param string    the string to check
     * @param ch        the character in the string to check, for error reporting purposes
     * @param isControl if {@code true}, the character is a control character, else it is a normal character
     */
    private void checkHasControlChars(String string, char ch, boolean isControl) {
        assertEquals("Failure for char 0x" + Integer.toHexString(ch) + " in '" + string + "'", isControl,
                     StringUtilities.hasControlChars(string));
    }


    /**
     * Verifies that {@link StringUtilities#replaceControlChars(String, String)} replaces control characters
     *
     * @param ch        the character to check
     * @param isControl if {@code true}, the character is a control character
     */
    private void checkReplaceControlChars(char ch, boolean isControl) {
        char rep = (isControl) ? ' ' : ch;
        checkReplaceControlChars(ch + "abc", rep + "abc", ch);                   // start of string, no whitespace
        checkReplaceControlChars(ch + " abc", rep + " abc", ch);                 // start of string, whitespace
        checkReplaceControlChars(ch + "\tabc", rep + "\tabc", ch);               // start of string, whitespace
        checkReplaceControlChars("abc" + ch + "def", "abc" + rep + "def", ch);         // in string, no whitespace
        checkReplaceControlChars("abc " + ch + " def", "abc " + rep + " def", ch);     // in string, whitespace
        checkReplaceControlChars("abc\t" + ch + "\tdef", "abc\t" + rep + "\tdef", ch); // in string, whitespace
        checkReplaceControlChars("abc" + ch, "abc" + rep, ch);                   // end of string, no whitespace
        checkReplaceControlChars("abc " + ch, "abc " + rep, ch);                 // end of string, whitespace
        checkReplaceControlChars("abc\t" + ch, "abc\t" + rep, ch);               // end of string, whitespace

        checkReplaceControlChars(ch + "abc\ndef", rep + "abc\ndef", ch);         // start of multiline string
        checkReplaceControlChars(ch + "abc\r\ndef", rep + "abc\r\ndef", ch);     // start of multiline string
        checkReplaceControlChars("abc\ndef" + ch + "\nghi", "abc\ndef" + rep + "\nghi", ch);  // in multiline string
        checkReplaceControlChars("abc\r\ndef" + ch + "\r\nghi", "abc\r\ndef" + rep + "\r\nghi", ch);
        // in multiline string

        checkReplaceControlChars("abc\n" + ch, "abc\n" + rep, ch);               // end of multiline string
        checkReplaceControlChars("abc\r\n" + ch, "abc\r\n" + rep, ch);           // end of multiline string
    }

    /**
     * Verifies that {@link StringUtilities#replaceControlChars(String, String)} returns the expected result
     * when replace control characters with spaces.
     *
     * @param string the string to check
     * @param ch     the character in the string to check, for error reporting purposes
     */
    private void checkReplaceControlChars(String string, String expected, char ch) {
        String actual = StringUtilities.replaceControlChars(string, " ");
        assertEquals("Failure for char 0x" + Integer.toHexString(ch) + " in '" + string + "'", expected, actual);
    }
}
