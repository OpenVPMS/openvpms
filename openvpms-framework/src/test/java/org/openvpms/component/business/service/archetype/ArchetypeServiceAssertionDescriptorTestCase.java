/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyList;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyMap;
import org.openvpms.component.business.service.archetype.descriptor.cache.ArchetypeDescriptorCacheFS;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.springframework.transaction.PlatformTransactionManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test the management of assertions through the archetype service.
 *
 * @author <a href="mailto:support@openvpms.org>OpenVPMS Team</a>
 */
public class ArchetypeServiceAssertionDescriptorTestCase {

    /**
     * Reference to the archetype service
     */
    private ArchetypeService service;

    /**
     * Test that we can successfully create all the archetypes loaded by the service.
     */
    @Test
    public void testCreateDefaultObject() {
        for (ArchetypeDescriptor descriptor : service.getArchetypeDescriptors()) {
            assertNotNull("Creating " + descriptor.getName(), service.create(descriptor.getArchetypeType()));
        }
    }

    /**
     * Test the creation of an archetypeRange assertion.
     *
     * @throws Exception for any error
     */
    @Test
    public void testCreateArchetypeRange() throws Exception {
        AssertionDescriptor adesc = service.create("assertion.archetypeRange", AssertionDescriptor.class);
        assertNotNull(adesc);
        PropertyMap pdesc = service.create("assertion.archetypeRangeProperties", PropertyMap.class);
        assertEquals(3, pdesc.getProperties().size());
        assertNotNull(pdesc.getProperties().get("shortName"));

        ArchetypeDescriptor desc = service.getArchetypeDescriptor(adesc.getArchetype());
        assertNotNull(desc);

        NodeDescriptor ndesc = (NodeDescriptor) desc.getNodeDescriptor("archetypes");
        assertNotNull(ndesc);

        ndesc.addChildToCollection(adesc, pdesc);
        assertNotNull(adesc.getProperty("archetypes"));
        assertTrue(adesc.getProperty("archetypes") instanceof PropertyList);


        PropertyList archetypes = (PropertyList) adesc.getProperty(
                "archetypes");
        assertEquals(1, archetypes.getProperties().size());
        for (NamedProperty archetype : archetypes.getProperties()) {
            assertTrue(archetype instanceof PropertyMap);
            PropertyMap map = (PropertyMap) archetype;
            assertEquals(3, map.getProperties().size());
            assertNotNull(map.getProperties().get("shortName"));
            assertNotNull(map.getProperties().get("minCardinality"));
            assertNotNull(map.getProperties().get("maxCardinality"));
        }
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        String assertionFile = "org/openvpms/archetype/assertionTypes.xml";
        String archFile = "org/openvpms/archetype/system/assertion/assertion.archetypeRange.adl";

        IArchetypeDescriptorCache cache = new ArchetypeDescriptorCacheFS(
                archFile, assertionFile);
        service = new ArchetypeService(cache, Mockito.mock(PlatformTransactionManager.class));
    }
}
