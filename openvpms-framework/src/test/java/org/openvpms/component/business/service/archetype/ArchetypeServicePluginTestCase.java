/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link Plugin} class in conjunction with the {@link IArchetypeService}.
 * <p/>
 * NOTE: no facility is provided to create plugins via the {@link IArchetypeService}; they must be created via
 * the {@link PluginDAO}. The {@link IArchetypeService} integration is intended simply for query purposes.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("archetype-service-appcontext.xml")
public class ArchetypeServicePluginTestCase extends AbstractArchetypeServiceTest {

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAO dao;

    /**
     * Verifies plugins can be queried and updated.
     */
    @Test
    public void test() {
        byte[] binary = RandomUtils.nextBytes(10);
        String key = UUID.randomUUID().toString();
        String name = key + ".jar";
        dao.save(key, name, new ByteArrayInputStream(binary));

        IArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Plugin> query = builder.createQuery(Plugin.class);
        Root<Plugin> root = query.from(Plugin.class, "plugin.default");
        query.where(builder.equal(root.get("key"), key));

        List<Plugin> list = service.createQuery(query).getResultList();
        assertEquals(1, list.size());
        Plugin plugin1 = list.get(0);

        checkPlugin(plugin1, key, name, null, true);

        plugin1.setDescription("new description");
        plugin1.setActive(false);
        service.save(plugin1);

        Plugin plugin2 = service.get(plugin1.getObjectReference(), Plugin.class);
        assertNotNull(plugin2);
        checkPlugin(plugin2, key, name, "new description", false);
    }

    /**
     * Verifies a plugin matches that expected.
     *
     * @param plugin      the plugin
     * @param key         the expected key
     * @param name        the expected name
     * @param description the expected description
     * @param active      the expected active flag
     */
    private void checkPlugin(Plugin plugin, String key, String name, String description, boolean active) {
        assertEquals(key, plugin.getKey());
        assertEquals(name, plugin.getName());
        assertEquals("plugin.default", plugin.getArchetype());
        assertEquals(description, plugin.getDescription());
        assertEquals(active, plugin.isActive());
        assertNotNull(plugin.getCreated());
    }
}
