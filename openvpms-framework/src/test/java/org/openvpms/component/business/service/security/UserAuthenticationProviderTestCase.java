/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.junit.Test;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.fail;

/**
 * Tests the {@link UserAuthenticationProvider}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("hibernate-base-appcontext.xml")
public class UserAuthenticationProviderTestCase extends AbstractArchetypeServiceTest {

    /**
     * The password encoder.
     */
    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    /**
     * The user service.
     */
    @Autowired
    private UserService userService;

    /**
     * Verifies that when {@code expireOnChangePassword = true}, {@link CredentialsExpiredException} is thrown
     * when a user needs to change their password.
     */
    @Test
    public void testCredentialsExpiredThrownOnChangePassword() {
        User userA = createUser("abc123", true);  // changePassword = true
        User userB = createUser("xyz789", false); // changePassword = false

        // verify userA doesn't authenticate, but userB does
        UserAuthenticationProvider provider1 = new UserAuthenticationProvider(userService, encoder, true);
        try {
            authenticate(provider1, userA, "abc123");
        } catch (CredentialsExpiredException expected) {
            // no-op
        }
        checkCanAuthenticate(provider1, userB, "xyz789");

        // now ignore changePassword. Both users should authenticate
        UserAuthenticationProvider provider2 = new UserAuthenticationProvider(userService, encoder, false);
        checkCanAuthenticate(provider2, userA, "abc123");
        checkCanAuthenticate(provider2, userB, "xyz789");
    }

    /**
     * Creates and saves a user.
     *
     * @param password       the password
     * @param changePassword determines if the user needs to change their password
     * @return a new user
     */
    private User createUser(String password, boolean changePassword) {
        User user = createUser();
        user.setPassword(encoder.encode(password));
        user.setChangePassword(changePassword);
        save(user);
        return user;
    }

    /**
     * Authenticates a user using the specified provider.
     *
     * @param provider the provider
     * @param user     the user
     * @param password the user's password
     * @throws AuthenticationException if authentication fails
     */
    private void authenticate(AuthenticationProvider provider, User user, String password) {
        Authentication token = new UsernamePasswordAuthenticationToken(user.getUsername(), password);
        provider.authenticate(token);
    }

    /**
     * Verifies a user can authenticate.
     *
     * @param provider the provider
     * @param user     the user
     * @param password the password
     */
    private void checkCanAuthenticate(UserAuthenticationProvider provider, User user, String password) {
        try {
            authenticate(provider, user, password);
        } catch (Exception exception) {
            fail("Exception not expected: " + exception.getMessage());
        }
    }
}
