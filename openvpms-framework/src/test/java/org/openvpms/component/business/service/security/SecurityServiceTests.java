/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.junit.After;
import org.junit.Test;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.object.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * This base class contains all the security test cases.
 *
 * @author <a href="mailto:support@openvpms.org>OpenVPMS Team</a>
 */
public abstract class SecurityServiceTests extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service.
     */
    @Autowired
    protected IArchetypeService archetype;

    /**
     * User name seed.
     */
    private int seed;

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    /**
     * Verifies that authentication is checked when creating objects.
     */
    @Test
    public void testCreate() {
        final String shortName = "party.person";
        Runnable createByShortName = () -> archetype.create(shortName);
        Runnable createByArchetypeId = () -> {
            ArchetypeId id = new ArchetypeId(shortName);
            assertNotNull(archetype.create(id));
        };

        // createSecurityContext("jima", "jima", "archetype:archetypeService.create:party.noauth");
        checkOperation(createByShortName, true);
        checkOperation(createByArchetypeId, true);

        String auth = "archetype:archetypeService.create:party.person";
        checkOperation(createByShortName, false, auth);
        checkOperation(createByArchetypeId, false, auth);
    }

    /**
     * Verifies that authentication is checked when saving objects.
     */
    @Test
    public void testSave() {
        final Party person = createPerson("MR", "Jim", "Alateras");
        Runnable save = () -> archetype.save(person);

        // verify that the save fails when no save authority is granted
        checkOperation(save, true);
        checkOperation(save, true, "archetype:archetypeService.save:party.invalid");

        // ... and succeeds when it is
        checkOperation(save, false, "archetype:archetypeService.save:party.person");

        // now check with authorities with wildcarded archetypes
        checkOperation(save, true, "archetype:archetypeService.save:person.*");
        checkOperation(save, true, "archetype:archetypeService.*:person.*");
        checkOperation(save, false, "archetype:archetypeService.save:party.*");
        checkOperation(save, false, "archetype:archetypeService.save:*.person");
        checkOperation(save, false, "archetype:archetypeService.save:party.per*");
        checkOperation(save, false, "archetype:archetypeService.save:part*.person");
        checkOperation(save, false, "archetype:archetypeService.save:party.*erson");
        checkOperation(save, false, "archetype:archetypeService.save:*rty.person");
        checkOperation(save, false, "archetype:archetypeService.save:*rty.per*");
        checkOperation(save, false, "archetype:archetypeService.save:par*.*son*");
        checkOperation(save, false, "archetype:archetypeService.save:*.*");

        // now check with authorities with wildcarded methods
        checkOperation(save, true, "archetype:archetypeService.*:party.invalid");
        checkOperation(save, true, "archetype:archetypeService.s*:party.invalid");
        checkOperation(save, false, "archetype:archetypeService.s*:party.person");
        checkOperation(save, false, "archetype:archetypeService.*ave:party.person");
        checkOperation(save, false, "archetype:archetypeService.*:party.person");

        // now check with authorities with both wildcarded methods and archetypes
        checkOperation(save, true, "archetype:archetypeService.s*:part*.invalid");
        checkOperation(save, false, "archetype:archetypeService.s*:part*.*son");
        checkOperation(save, false, "archetype:archetypeService.*ave:*.*");
        checkOperation(save, false, "archetype:archetypeService.*:party.*");
        checkOperation(save, false, "archetype:archetypeService.*:*.*");
    }

    /**
     * Verifies that authorities are checked when collections of objects are saved via
     * {@link IArchetypeService#save(Collection)}.
     */
    @Test
    public void testSaveCollection() {
        Party party1 = createPerson("MR", "Jim", "Alateras");
        Party party2 = createPet("Fido");
        List<IMObject> objects = new ArrayList<>();
        objects.add(party1);
        objects.add(party2);

        Runnable save = () -> archetype.save(objects);
        checkOperation(save, false, "archetype:archetypeService.save:party.*");
        checkOperation(save, false, "archetype:archetypeService.save:party.person",
                       "archetype:archetypeService.save:party.animalpet");
        checkOperation(save, false, "archetype:archetypeService.save:*.*");
        checkOperation(save, true, "archetype:archetypeService.save:party.person");
    }

    /**
     * Verifies that authentication is checked when removing objects using
     * {@link IArchetypeService#remove(org.openvpms.component.model.object.IMObject)} and
     * {@link IArchetypeService#remove(Reference)}.
     */
    @Test
    public void testRemove() {
        Party person1 = createPerson("MR", "Jim", "Alateras");
        Party person2 = createPerson("MR", "Foo", "Bar");
        createSecurityContext("jima", "jima", "archetype:archetypeService.save:*");
        archetype.save(person1);
        archetype.save(person2);

        Runnable remove1 = () -> archetype.remove(person1);
        Runnable remove2 = () -> archetype.remove(person2.getObjectReference());

        checkOperation(remove1, true);
        checkOperation(remove1, true, "archetype:archetypeService.*:party.invalid");
        checkOperation(remove1, false, "archetype:archetypeService.remove:party.person");

        checkOperation(remove2, true);
        checkOperation(remove2, true, "archetype:archetypeService.*:party.invalid");
        checkOperation(remove2, false, "archetype:archetypeService.remove:party.person");
    }

    /**
     * Create a secure context for authorization testing.
     *
     * @param user        the user name
     * @param password    the password
     * @param authorities the authorities of the person
     */
    protected abstract void createSecurityContext(String user, String password, String... authorities);

    /**
     * Creates a person.
     * <p/>
     * Note that this method grants authorities in order to perform the creation.
     *
     * @param title     the person's title
     * @param firstName the person's first name
     * @param lastName  the person's last name
     * @return Person
     */
    private Party createPerson(String title, String firstName, String lastName) {
        createSecurityContext("jima", "jima", "archetype:archetypeService.create:*");
        Party person = archetype.create("party.person", Party.class);
        person.getDetails().put("lastName", lastName);
        person.getDetails().put("firstName", firstName);
        person.getDetails().put("title", title);
        person.addContact(createPhoneContact());

        return person;
    }

    /**
     * Creates a pet.
     * <p/>
     * Note that this method grants authorities in order to perform the creation.
     *
     * @param name the pet's name
     * @return a new pet
     */
    private Party createPet(String name) {
        createSecurityContext("jima", "jima", "archetype:archetypeService.create:*");
        Party pet = archetype.create("party.animalpet", Party.class);
        pet.setName(name);
        pet.getDetails().put("species", "CANINE");
        return pet;
    }

    /**
     * Create a phone contact
     *
     * @return Contact
     */
    private Contact createPhoneContact() {
        Contact contact = archetype.create("contact.phoneNumber", Contact.class);
        contact.getDetails().put("areaCode", "03");
        contact.getDetails().put("telephoneNumber", "1234567");
        contact.getDetails().put("preferred", true);

        return contact;
    }

    /**
     * Executes an operation by a user with the specified authorities.
     *
     * @param operation   the operation to execute
     * @param fail        if {@code true}, expect the operation to fail, otherwise expect it to succeed
     * @param authorities the user's authorities
     */
    private void checkOperation(Runnable operation, boolean fail, String... authorities) {
        createSecurityContext("jima" + seed, "jima", authorities);
        ++seed;
        try {
            operation.run();
            if (fail) {
                fail("Expected operation to fail");
            }
        } catch (OpenVPMSAccessDeniedException exception) {
            if (!fail) {
                fail("Didn't expect operation to fail");
            }
            if (exception.getErrorCode() != OpenVPMSAccessDeniedException.ErrorCode.AccessDenied) {
                fail("Incorrect error code was specified during the exception");
            }
        }
    }
}
