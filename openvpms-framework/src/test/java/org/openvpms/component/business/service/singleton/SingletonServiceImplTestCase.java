/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.singleton;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link SingletonServiceImpl}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("../archetype/archetype-service-appcontext.xml")
public class SingletonServiceImplTestCase extends AbstractArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The singleton service.
     */
    private SingletonService singletonService;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // clean up any existing instance
        IArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        query.from(Party.class, "party.singleton");
        List<Party> parties = service.createQuery(query)
                .setMaxResults(Integer.MAX_VALUE)
                .getResultList();
        for (Party party : parties) {
            service.remove(party);
        }

        singletonService = new SingletonServiceImpl(getArchetypeService(), transactionManager);
    }

    /**
     * Tests the {@link SingletonService#exists(String)} and {@link SingletonService#exists(String, Reference)} methods.
     */
    @Test
    public void testExists() {
        assertFalse(singletonService.exists("party.singleton"));

        Party first = createSingleton("first", false);
        assertFalse(singletonService.exists("party.singleton"));
        assertFalse(singletonService.exists("party.singleton", first.getObjectReference()));
        save(first);

        assertTrue(singletonService.exists("party.singleton"));
        assertFalse(singletonService.exists("party.singleton", first.getObjectReference()));


        first.setActive(false);
        save(first);
        assertFalse(singletonService.exists("party.singleton"));
        assertFalse(singletonService.exists("party.singleton", first.getObjectReference()));

        Party second = createSingleton("second");
        assertTrue(singletonService.exists("party.singleton"));
        assertFalse(singletonService.exists("party.singleton", second.getObjectReference()));
    }

    /**
     * Tests the {@link SingletonService#get(String, Class)} method.
     */
    @Test
    public void testGet() {
        assertNull(singletonService.get("party.singleton", Party.class));
        Party test1 = createSingleton("test1");
        assertEquals(test1, singletonService.get("party.singleton", Party.class));
        test1.setActive(false);
        save(test1);
        assertNull(singletonService.get("party.singleton", Party.class));

        Party test2 = createSingleton("test2");
        assertEquals(test2, singletonService.get("party.singleton", Party.class));
    }

    /**
     * Tests the {@link SingletonService#get(String, Class, boolean)} method.
     */
    @Test
    public void testGetOrCreate() {
        // no singleton available
        assertNull(singletonService.get("party.singleton", Party.class, false));

        // verify the singleton is created
        Party test1 = singletonService.get("party.singleton", Party.class, true);
        assertNotNull(test1);

        // verify inactive instance not returned
        test1.setActive(false);
        save(test1);
        assertNull(singletonService.get("party.singleton", Party.class, false));

        // verify a new instance is created
        Party test2 = singletonService.get("party.singleton", Party.class, true);
        assertTrue(test2.isActive());
        assertNotNull(test2);
        assertNotEquals(test1, test2);
    }

    /**
     * Tests the {@link SingletonService#get(String, Class, Consumer)} method.
     */
    @Test
    public void testGetOrCreateWithPopulator() {
        // no singleton available
        assertNull(singletonService.get("party.singleton", Party.class));

        // verify the singleton is created
        Party test1 = singletonService.get("party.singleton", Party.class, party -> party.setName("test1"));
        assertNotNull(test1);
        assertEquals("test1", test1.getName());

        // verify inactive instance not returned
        test1.setActive(false);
        save(test1);
        assertNull(singletonService.get("party.singleton", Party.class, false));

        // verify a new instance is created
        Party test2 = singletonService.get("party.singleton", Party.class, party -> party.setName("test2"));
        assertEquals("test2", test2.getName());
        assertTrue(test2.isActive());
        assertNotNull(test2);
        assertNotEquals(test1, test2);
    }

    /**
     * Creates and saves a singleton.
     *
     * @param name the singleton name
     * @return a new singleton
     */
    private Party createSingleton(String name) {
        return createSingleton(name, true);
    }

    /**
     * Creates a singleton.
     *
     * @param name the singleton name
     * @param save if {@code true}, save it
     * @return a new singleton
     */
    private Party createSingleton(String name, boolean save) {
        Party party = (Party) create("party.singleton");
        party.setName(name);
        if (save) {
            save(party);
        }
        return party;
    }

}
