/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.service.archetype.descriptor;

import org.junit.Test;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeList;
import org.openvpms.component.business.service.archetype.ArchetypeService;
import org.openvpms.component.model.archetype.ArchetypeRange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ArchetypeList} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("descriptor-test-appcontext.xml")
public class ArchetypeListTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service.
     */
    @Autowired
    private ArchetypeService service;

    /**
     * Tests the {@link ArchetypeList#expand(ArchetypeRange, org.openvpms.component.service.archetype.ArchetypeService)}
     * method.
     */
    @Test
    public void testExpand() {
        ArchetypeList list1 = new ArchetypeList("contact.*", "contact.phoneNumber");
        ArchetypeRange expanded1 = ArchetypeList.expand(list1, service);
        List<String> archetypes1 = expanded1.getArchetypes();
        assertEquals(2, archetypes1.size());
        assertTrue(archetypes1.contains("contact.phoneNumber"));
        assertTrue(archetypes1.contains("contact.location"));
        assertEquals("contact.phoneNumber", expanded1.getDefaultArchetype());

        // test expand() when the archetypes are not present
        ArchetypeList list2 = new ArchetypeList("doesnotexist.*", "doesnotexist.default");
        ArchetypeRange expanded2 = ArchetypeList.expand(list2, service);
        List<String> archetypes2 = expanded2.getArchetypes();
        assertTrue(archetypes2.isEmpty());
        assertEquals("doesnotexist.default", expanded2.getDefaultArchetype());

        // verifies that ArchetypeList.EMPTY is used if the archetypes don't exist and there is no default
        ArchetypeList list3 = new ArchetypeList("doesnotexist.*");
        assertSame(ArchetypeList.EMPTY, ArchetypeList.expand(list3, service));
    }
}