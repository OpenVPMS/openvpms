/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service;

import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.archetype.helper.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertNotNull;


/**
 * Abstract base class for tests using the archetype service.
 *
 * @author Tim Anderson
 */
public abstract class AbstractArchetypeServiceTest extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service.
     */
    @Autowired
    @Qualifier("archetypeService")
    private IArchetypeService service;

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getArchetypeService() {
        return service;
    }

    /**
     * Helper to create a new object.
     *
     * @param shortName the archetype short name
     * @return the new object
     */
    protected IMObject create(String shortName) {
        IMObject object = service.create(shortName);
        assertNotNull(object);
        return object;
    }

    /**
     * Helper to create an object and wrap it in bean.
     *
     * @param shortName the archetype short name
     * @return the bean wrapping an instance of {@code shortName}.
     */
    protected IMObjectBean createBean(String shortName) {
        IMObject object = create(shortName);
        return getBean(object);
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean for the object
     */
    protected IMObjectBean getBean(org.openvpms.component.model.object.IMObject object) {
        return new IMObjectBean(object, service);
    }

    /**
     * Helper to save an object.
     *
     * @param object the object to save
     */
    protected void save(org.openvpms.component.model.object.IMObject object) {
        service.save(object);
    }

    /**
     * Helper to save a collection of objects.
     *
     * @param objects the object to save
     */
    protected <T extends org.openvpms.component.model.object.IMObject> void save(T... objects) {
        save(Arrays.asList(objects));
    }

    /**
     * Helper to save a collection of objects.
     *
     * @param objects the object to save
     */
    protected <T extends org.openvpms.component.model.object.IMObject> void save(Collection<T> objects) {
        service.save(objects);
    }

    /**
     * Helper to reload an object from the archetype service.
     *
     * @param object the object to reload
     * @return the corresponding object or {@code null} if no object is found
     */
    @SuppressWarnings("unchecked")
    protected <T extends org.openvpms.component.model.object.IMObject> T get(T object) {
        return (T) get(object.getObjectReference());
    }

    /**
     * Helper to retrieve an object from the archetype service.
     *
     * @param ref the object reference
     * @return the corresponding object or {@code null} if no object is found
     */
    protected IMObject get(Reference ref) {
        return service.get(ref);
    }

    /**
     * Helper to get the results from a query.
     *
     * @param query the query
     * @return the query results
     */
    protected List<IMObject> get(ArchetypeQuery query) {
        return service.get(query).getResults();
    }

    /**
     * Helper to remove an object.
     *
     * @param object the object to remove
     */
    protected void remove(org.openvpms.component.model.object.IMObject object) {
        remove(object, false);
    }

    /**
     * Helper to remove an object.
     *
     * @param object      the object to remove
     * @param byReference if {@code true}, use {@link IArchetypeService#remove(Reference)} else use
     *                    {@link IArchetypeService#remove(org.openvpms.component.model.object.IMObject)}
     */
    protected void remove(org.openvpms.component.model.object.IMObject object, boolean byReference) {
        if (byReference) {
            service.remove(object.getObjectReference());
        } else {
            service.remove(object);
        }
    }

    /**
     * Validates an object.
     *
     * @param object the object to validate
     * @throws ValidationException if there are validation errors
     */
    protected void validateObject(IMObject object) {
        service.validateObject(object);
    }

    /**
     * Returns the {@link ArchetypeDescriptor} with the specified short name.
     *
     * @param shortName the short name
     * @return the descriptor corresponding to the short name, or {@code null} if none is found
     */
    protected ArchetypeDescriptor getArchetypeDescriptor(String shortName) {
        return service.getArchetypeDescriptor(shortName);
    }

    /**
     * Creates a user.
     *
     * @return a new user
     */
    protected User createUser() {
        User user = (User) create("security.user");
        user.setUsername("u" + Math.abs(new Random().nextInt()));
        user.setName(user.getUsername());
        user.setPassword("x");
        return user;
    }

}
