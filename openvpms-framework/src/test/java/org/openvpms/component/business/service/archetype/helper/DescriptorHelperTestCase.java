/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.junit.Test;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link DescriptorHelper} class.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
@ContextConfiguration("../archetype-service-appcontext.xml")
public class DescriptorHelperTestCase extends AbstractArchetypeServiceTest {

    /**
     * Tests the {@link DescriptorHelper#getArchetypeDescriptor(String, ArchetypeService)}} method.
     */
    @Test
    public void testGetArchetypeDescriptor() {
        ArchetypeDescriptor animal = DescriptorHelper.getArchetypeDescriptor("party.animalpet", getArchetypeService());
        assertNotNull(animal);
        assertEquals("party.animalpet", animal.getArchetypeType());

        ArchetypeDescriptor noExist = DescriptorHelper.getArchetypeDescriptor("fox.pet", getArchetypeService());
        assertNull(noExist);

        // verify wildcards not expanded
        ArchetypeDescriptor noWild = DescriptorHelper.getArchetypeDescriptor("party.animal*", getArchetypeService());
        assertNull(noWild);
    }

    /**
     * Tests the {@link DescriptorHelper#getArchetypeDescriptor(Reference, ArchetypeService)} method.
     */
    @Test
    public void testGetArchetypeDescriptorFromRef() {
        IMObject pet = create("party.animalpet");

        ArchetypeDescriptor expected;
        ArchetypeDescriptor actual;

        Reference ref = pet.getObjectReference();

        expected = DescriptorHelper.getArchetypeDescriptor("party.animalpet", getArchetypeService());
        actual = DescriptorHelper.getArchetypeDescriptor(ref, getArchetypeService());
        assertNotNull(expected);
        assertNotNull(actual);

        assertEquals(expected, actual);
    }

    /**
     * Tests the {@link DescriptorHelper#getArchetypeDescriptor(IMObject, ArchetypeService)} method.
     */
    @Test
    public void testGetArchetypeDescriptorFromObject() {
        IMObject person = create("party.customerperson");

        ArchetypeDescriptor expected;
        ArchetypeDescriptor actual;

        expected = DescriptorHelper.getArchetypeDescriptor("party.customerperson", getArchetypeService());
        actual = DescriptorHelper.getArchetypeDescriptor(person, getArchetypeService());
        assertNotNull(expected);
        assertNotNull(actual);

        assertEquals(expected, actual);
    }

    /**
     * Tests the {@link DescriptorHelper#getArchetypeDescriptors(String[], ArchetypeService)} )} method.
     */
    @Test
    public void testGetArchetypeDescriptorFromRange() {
        String[] range = {"act.customerAccountPayment",
                          "act.customerEstimation",
                          "act.customerEstimationItem"};
        List<ArchetypeDescriptor> matches = DescriptorHelper.getArchetypeDescriptors(range, getArchetypeService());
        checkEquals(range, matches);
    }

    /**
     * Tests the {@link DescriptorHelper#getArchetypeDescriptors(String[], ArchetypeService)}
     * method, with wildcards.
     */
    @Test
    public void testGetAfrchetypeDescriptorFromWildcardRange() {
        String[] range = {"entityRelationship.animal*",
                          "entityRelationship.family*"};
        String[] expected = {"entityRelationship.animalCarer",
                             "entityRelationship.animalOwner",
                             "entityRelationship.familyMember"};
        List<ArchetypeDescriptor> matches = DescriptorHelper.getArchetypeDescriptors(range, getArchetypeService());
        checkEquals(expected, matches);
    }

    /**
     * Tests the {@link DescriptorHelper#getShortNames(NodeDescriptor, ArchetypeService)} method.
     */
    @Test
    public void testGetShortNamesFromNodeDescriptor() {
        ArchetypeDescriptor person = DescriptorHelper.getArchetypeDescriptor("party.person", getArchetypeService());
        assertNotNull(person);

        // get a range from a collection node
        NodeDescriptor contacts = person.getNodeDescriptor("contacts");
        assertNotNull(contacts);
        String[] range = DescriptorHelper.getShortNames(contacts, getArchetypeService());
        checkEquals(range, "contact.location", "contact.phoneNumber");

        // now check a node with a filter
        ArchetypeDescriptor personFilter = DescriptorHelper.getArchetypeDescriptor("party.personfilter",
                                                                                   getArchetypeService());
        assertNotNull(personFilter);
        NodeDescriptor staff = personFilter.getNodeDescriptor("staffClassifications");
        assertNotNull(staff);
        assertNotNull(staff.getFilter());
        String[] staffShortNames = DescriptorHelper.getShortNames(staff, getArchetypeService());
        checkEquals(staffShortNames, "lookup.staff");

        // now check a node with a filter and multiple archetypes, and verify
        // order is preserved
        NodeDescriptor patients = person.getNodeDescriptor("patients");
        assertNotNull(patients);
        assertNotNull(patients.getFilter());
        String[] relShortNames = DescriptorHelper.getShortNames(patients, getArchetypeService());
        checkEquals(relShortNames, true, "entityRelationship.animalOwner", "entityRelationship.animalCarer");
    }

    /**
     * Tests the {@link DescriptorHelper#getShortNames(String, ArchetypeService)} method.
     */
    @Test
    public void testGetShortNamesFromSingleShortName() {
        String[] actual;

        actual = DescriptorHelper.getShortNames("party.animalpet", getArchetypeService());
        checkEquals(actual, "party.animalpet");

        // now check wildcards
        actual = DescriptorHelper.getShortNames("*pet", getArchetypeService());
        checkEquals(actual, "party.patientpet", "party.animalpet", "party.horsepet");
    }

    /**
     * Tests the {@link DescriptorHelper#getShortNames(String, boolean, ArchetypeService)} method.
     */
    @Test
    public void testGetShortNamesNoPrimary() {
        String[] actual;

        // verify non-primary archetypes aren't returned when primaryOnly==true
        actual = DescriptorHelper.getShortNames("contact.*", true, getArchetypeService());
        assertEquals(0, actual.length);

        // verify they are returned when primaryOnly==false
        actual = DescriptorHelper.getShortNames("contact.*", false, getArchetypeService());
        checkEquals(actual, "contact.location", "contact.phoneNumber");
    }

    /**
     * Tests the {@link DescriptorHelper#getShortNames(String[], ArchetypeService)} method.
     */
    @Test
    public void testGetShortNamesFromRange() {
        String[] expected = {"party.animalpet", "party.horsepet"};
        String[] actual = DescriptorHelper.getShortNames(expected, getArchetypeService());
        checkEquals(actual, expected);

        String[] wildcards = {"party.animal*", "party.horse*"};
        actual = DescriptorHelper.getShortNames(wildcards, getArchetypeService());
        checkEquals(actual, expected);
    }

    /**
     * Tests the {@link DescriptorHelper#getShortNames(String[], boolean, ArchetypeService)} method.
     */
    @Test
    public void testGetShortNamesFromRangeNoPrimary() {
        // verify non-primary archetypes aren't returned when primaryOnly==true
        String[] expected = {"contact.location", "contact.phoneNumber"};
        String[] actual = DescriptorHelper.getShortNames(expected, true, getArchetypeService());
        assertEquals(0, actual.length);

        // verify they are returned when primaryOnly==false
        actual = DescriptorHelper.getShortNames(expected, false, getArchetypeService());
        checkEquals(expected, actual);
    }

    /**
     * Tests the {@link DescriptorHelper#getDisplayName(String, ArchetypeService)} method.
     */
    @Test
    public void testGetDisplayName() {
        String name = DescriptorHelper.getDisplayName("party.animalpet", getArchetypeService());
        assertEquals("Patient(Pet)", name);

        // now check non-existent archetype
        String noExist = DescriptorHelper.getDisplayName("badshortname", getArchetypeService());
        assertNull(noExist);
    }

    /**
     * Tests the {@link DescriptorHelper#getDisplayName(org.openvpms.component.model.object.IMObject,
     * ArchetypeService)} method.
     */
    @Test
    public void testGetDisplayNameForObject() {
        IMObject object = create("party.animalpet");
        assertNotNull(object);
        String name = DescriptorHelper.getDisplayName(object, getArchetypeService());
        assertEquals("Patient(Pet)", name);
    }

    /**
     * Tests the {@link DescriptorHelper#getDisplayName(IMObject, String, ArchetypeService)} method.
     */
    @Test
    public void testGetDisplayNameForObjectNode() {
        IMObject object = create("act.customerAccountPayment");
        assertNotNull(object);
        String name = DescriptorHelper.getDisplayName(object, "startTime", getArchetypeService());
        assertEquals("Date", name);

        // check a non existent node
        assertNull(DescriptorHelper.getDisplayName(object, "foo", getArchetypeService()));
    }

    /**
     * Tests the {@link DescriptorHelper#getDisplayName(String, String, ArchetypeService)} method.
     */
    @Test
    public void testGetDisplayNameForArchetypeNode() {
        String name = DescriptorHelper.getDisplayName("act.customerAccountPayment", "startTime", getArchetypeService());
        assertEquals("Date", name);

        // check a non existent node
        assertNull(DescriptorHelper.getDisplayName("act.customerAccountPayment", "foo", getArchetypeService()));

        // check a non-existent archetype
        assertNull(DescriptorHelper.getDisplayName("foo", "foo", getArchetypeService()));
    }

    /**
     * Tests the {@link DescriptorHelper#getNodeShortNames(String[], String, ArchetypeService)}
     * method.
     */
    @Test
    public void testGetNodeShortNames() {
        String[] shortNames = {"entityRelationship.animal*"};
        String[] nodeShortNames = DescriptorHelper.getNodeShortNames(shortNames, "target", getArchetypeService());
        assertEquals(1, nodeShortNames.length);
        assertEquals("party.animalpet", nodeShortNames[0]);

        nodeShortNames = DescriptorHelper.getNodeShortNames(shortNames, "source", getArchetypeService());
        assertEquals(2, nodeShortNames.length);
        List<String> list = Arrays.asList(nodeShortNames);
        assertTrue(list.contains("party.person"));
        assertTrue(list.contains("organization.organization"));
    }

    /**
     * Tests the {@link DescriptorHelper#getCommonNodeNames(String[], ArchetypeService)} method.
     */
    @Test
    public void testGetCommonNodeNames() {
        String[] shortNames = {"party.customerperson", "party.patientpet"};
        String[] names = DescriptorHelper.getCommonNodeNames(shortNames, getArchetypeService());
        assertEquals(5, names.length);
        assertArrayEquals(new String[]{"id", "name", "description", "active", "identities"}, names);
    }

    /**
     * Tests the {@link DescriptorHelper#getCommonNodeNames(String[], String[], ArchetypeService)} method.
     */
    @Test
    public void testGetCommonNodeNamesForNodes() {
        String[] shortNames = {"party.customerperson", "party.patientpet"};
        String[] nodes = {"id", "name", "species"};
        String[] names = DescriptorHelper.getCommonNodeNames(shortNames, nodes, getArchetypeService());
        assertEquals(2, names.length);
        assertArrayEquals(new String[]{"id", "name",}, names);
    }

    /**
     * Tests the {@link DescriptorHelper#getNode(String, String, ArchetypeService)} method.
     */
    @Test
    public void testGetNode() {
        NodeDescriptor node1 = DescriptorHelper.getNode("party.customerperson", "title", getArchetypeService());
        assertNotNull(node1);
        assertEquals("title", node1.getName());

        NodeDescriptor node2 = DescriptorHelper.getNode("bad.archetype", "name", getArchetypeService());
        assertNull(node2);

        NodeDescriptor node3 = DescriptorHelper.getNode("party.customerperson", "badName", getArchetypeService());
        assertNull(node3);
    }

    /**
     * Verifies that two lists of short names match.
     *
     * @param actualShortNames   the actual short names
     * @param expectedShortNames the expected short names
     */
    private void checkEquals(String[] actualShortNames, String... expectedShortNames) {
        assertEquals(expectedShortNames.length, actualShortNames.length);
        for (String expected : expectedShortNames) {
            boolean found = false;
            for (String actual : actualShortNames) {
                if (expected.equals(actual)) {
                    found = true;
                    break;
                }
            }
            assertTrue("Shortname not found: " + expected, found);
        }
    }

    /**
     * Verifies that two lists of short names match.
     *
     * @param actualShortNames   the actual short names
     * @param preserveOrder      if <tt>true</tt> the actual and expected shortnames must be in the same order
     * @param expectedShortNames the expected short names
     */
    private void checkEquals(String[] actualShortNames, boolean preserveOrder,
                             String... expectedShortNames) {
        if (preserveOrder) {
            assertEquals(expectedShortNames.length, actualShortNames.length);
            for (int i = 0; i < actualShortNames.length; ++i) {
                assertEquals(expectedShortNames[i], actualShortNames[i]);
            }
        } else {
            checkEquals(actualShortNames, expectedShortNames);
        }
    }

    /**
     * Verifies that a list of short names are all present in a list of
     * archetype descriptors
     *
     * @param shortNames the short names to check
     * @param archetypes the archetype descriptors
     */
    private void checkEquals(String[] shortNames, List<ArchetypeDescriptor> archetypes) {
        assertEquals(shortNames.length, archetypes.size());
        for (String shortName : shortNames) {
            boolean found = false;
            for (ArchetypeDescriptor archetype : archetypes) {
                if (archetype.getArchetypeType().equals(shortName)) {
                    found = true;
                    break;
                }
            }
            assertTrue("Archetype not found for shortname: " + shortName, found);
        }
    }

}
