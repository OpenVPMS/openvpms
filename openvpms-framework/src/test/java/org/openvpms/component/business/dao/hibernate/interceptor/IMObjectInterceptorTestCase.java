/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.interceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.component.business.dao.hibernate.im.common.IMObjectAssembler;
import org.openvpms.component.business.dao.hibernate.im.entity.EntityLinkDOImpl;
import org.openvpms.component.business.dao.hibernate.im.party.PartyDO;
import org.openvpms.component.business.dao.hibernate.im.party.PartyDOImpl;
import org.openvpms.component.business.dao.hibernate.im.security.UserDO;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.model.user.User;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link IMObjectInterceptor}.
 *
 * @author Tim Anderson
 */
public class IMObjectInterceptorTestCase {

    private IMObjectInterceptor interceptor;

    private AuthenticationContext context;

    private User user1;

    private User user2;

    private UserDO userDO1;

    private UserDO userDO2;

    private static String[] PROPERTY_NAMES = {IMObjectInterceptor.CREATED, IMObjectInterceptor.CREATED_BY,
                                              IMObjectInterceptor.UPDATED, IMObjectInterceptor.UPDATED_BY};


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        user1 = Mockito.mock(User.class);
        user2 = Mockito.mock(User.class);
        userDO1 = Mockito.mock(UserDO.class);
        userDO2 = Mockito.mock(UserDO.class);

        context = new TestAuthenticationContext();

        interceptor = new IMObjectInterceptor(context) {
            @Override
            protected UserDO resolve(User user) {
                return (user == user1) ? userDO1 : userDO2;
            }
        };
    }

    /**
     * Tests the {@link IMObjectInterceptor#onSave} method.
     */
    @Test
    public void testSave() {
        PartyDO party = new PartyDOImpl();
        context.setUser(user1);

        Object[] state1 = {null, null, null, null}; // corresponds to PROPERTY_NAMES
        Date preSave1 = new Date();
        assertTrue(interceptor.onSave(party, -1, state1, PROPERTY_NAMES, null));
        Date created1 = (Date) state1[0];
        checkDate(created1, preSave1);
        assertEquals(userDO1, state1[1]);
        assertNull(state1[2]);  // updated
        assertNull(state1[3]);  // updatedBy

        // verify that if values are pre-set. they are overwritten. For updated and updatedBy, the values should
        // be set to null
        Object[] state2 = {new Date(0), userDO1, new Date(1), userDO2};
        Date preSave2 = new Date();
        context.setUser(user2);
        assertTrue(interceptor.onSave(party, -1, state2, PROPERTY_NAMES, null));

        Date created2 = (Date) state2[0];
        checkDate(created2, preSave2);
        assertEquals(userDO2, state2[1]);

        assertNull(state2[2]); // updated
        assertNull(state2[3]); // updatedBy

        // verify that when no AuditableIMObject is passed, nothing is populated
        Object[] state3 = {null, null, null, null};
        assertFalse(interceptor.onSave(new EntityLinkDOImpl(), -1, state3, PROPERTY_NAMES, null));
    }

    /**
     * Tests the {@link IMObjectInterceptor#onFlushDirty} method.
     */
    @Test
    public void testFlushDirty() {
        PartyDO party = new PartyDOImpl();

        context.setUser(user2);
        Date created1 = new Date(0);
        Object[] currentState1 = {created1, userDO1, null, null}; // corresponds to PROPERTY_NAMES
        Date preSave1 = new Date();
        assertTrue(interceptor.onFlushDirty(party, 1, currentState1, null, PROPERTY_NAMES, null));

        assertEquals(created1, currentState1[0]);
        assertEquals(userDO1, currentState1[1]);
        Date updated1 = (Date) currentState1[2];
        checkDate(updated1, preSave1);
        assertEquals(userDO2, currentState1[3]);

        Date updated2 = new Date(1);
        context.setUser(user1);
        Object[] currentState2 = {created1, userDO1, updated2, userDO2};
        Date preSave2 = new Date();
        assertTrue(interceptor.onFlushDirty(party, 1, currentState2, null, PROPERTY_NAMES, null));

        assertEquals(created1, currentState2[0]);
        assertEquals(userDO1, currentState2[1]);
        Date updated3 = (Date) currentState2[2];
        assertNotEquals(updated2, updated3);
        checkDate(updated3, preSave2);
        assertEquals(userDO1, currentState2[3]);

        // verify that when there is no logged in user, the updated field updates, but updatedBy remains unchanged.
        Object[] currentState3 = {created1, userDO1, updated2, userDO2};
        context.setUser(null);
        Date preSave3 = new Date();
        assertTrue(interceptor.onFlushDirty(party, 1, currentState3, null, PROPERTY_NAMES, null));
        assertEquals(created1, currentState3[0]);
        assertEquals(userDO1, currentState3[1]);
        Date updated4 = (Date) currentState3[2];
        assertNotEquals(updated2, updated4);
        checkDate(updated4, preSave3);
        assertEquals(userDO2, currentState1[3]);

        // verify that when no AuditableIMObject is passed, nothing is populated
        Object[] currentState4 = {null, null, null, null};
        assertFalse(interceptor.onFlushDirty(new EntityLinkDOImpl(), 2, currentState4, null, PROPERTY_NAMES, null));
        assertNull(currentState4[0]);
        assertNull(currentState4[1]);
        assertNull(currentState4[2]);
        assertNull(currentState4[3]);
    }

    /**
     * Verifies that if an unsaved user is in the {@link AuthenticationContext}, then no author information is saved.
     */
    @Test
    public void testUnsavedUser() {
        Mockito.when(user1.isNew()).thenReturn(true);
        PartyDO party = new PartyDOImpl();
        context.setUser(user1);

        Object[] state1 = {null, null, null, null}; // corresponds to PROPERTY_NAMES
        Date preSave1 = new Date();
        assertTrue(interceptor.onSave(party, -1, state1, PROPERTY_NAMES, null));
        Date created1 = (Date) state1[0];
        checkDate(created1, preSave1);
        assertNull(state1[1]);  // createdBy
        assertNull(state1[2]);  // updated
        assertNull(state1[3]);  // updatedBy

        Object[] state2 = {created1, null, null, null};
        Date presave2 = new Date();
        assertTrue(interceptor.onFlushDirty(party, 1, state2, null, PROPERTY_NAMES, null));
        assertEquals(created1, state2[0]);
        assertNull(state2[1]);
        Date updated2 = (Date) state2[2];
        checkDate(updated2, presave2);
        assertNull(state1[3]);  // updatedBy
    }

    /**
     * Verifies a date has second precision and is >= the specified date.
     * <p/>
     * See {@link IMObjectAssembler#getTimestamp(Date)} for more details.
     *
     * @param date the date
     */
    private void checkDate(Date date, Date preSave) {
        assertNotNull(date);

        // verify the date has second precision
        assertEquals(0, date.getTime() % 1000);
        ;
        assertTrue(DateUtils.truncatedCompareTo(date, preSave, Calendar.SECOND) >= 0);
    }


    private static class TestAuthenticationContext implements AuthenticationContext {

        private User user;

        @Override
        public User getUser() {
            return user;
        }

        @Override
        public void setUser(User user) {
            this.user = user;
        }
    }
}
