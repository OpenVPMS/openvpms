/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.object.Reference;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Helper to verify that {@link AuditableIMObject} create/update info changes when a modified object is saved.
 *
 * @author Tim Anderson
 */
public class AuditableTestHelper {

    public static class AuditableState {

        /**
         * Created time.
         */
        private final Date created;

        /**
         * Created-by user.
         */
        private final Reference createdBy;

        /**
         * Updated time.
         */
        private final Date updated;

        /**
         * Updated by user.
         */
        private final Reference updatedBy;

        /**
         * Constructs a {@link AuditableState}.
         *
         * @param object the object to copy state from
         */
        public AuditableState(AuditableIMObject object) {
            this.created = object.getCreated();
            this.createdBy = object.getCreatedBy();
            this.updated = object.getUpdated();
            this.updatedBy = object.getUpdatedBy();
        }

        /**
         * Verifies that the creation details have been set for an object on first save.
         *
         * @param object  the object to check
         * @param preSave a timestamp prior to the save. The created time should be >= this
         * @param user    the expected user. May be {@code null}
         * @return the new state of the object
         */
        public AuditableState checkCreate(AuditableIMObject object, Date preSave, User user) {
            assertNotNull(object.getCreated());

            // created should be  >= pre-save time
            assertTrue(DateUtils.truncatedCompareTo(preSave, object.getCreated(), Calendar.SECOND) <= 0);
            if (user == null) {
                assertNull(object.getCreatedBy());
            } else {
                assertEquals(object.getCreatedBy(), user.getObjectReference());
            }

            // update details not set
            assertNull(object.getUpdated());
            assertNull(object.getUpdatedBy());
            return new AuditableState(object);
        }

        /**
         * Verifies that the update details have been set for an object on subsequent save.
         *
         * @param object  the object to check
         * @param preSave a timestamp prior to the save. The updated time should be >= this
         * @param user    the expected user. May be {@code null}
         */
        public void checkUpdate(AuditableIMObject object, Date preSave, User user) {
            // created details shouldn't change
            assertEquals(created, object.getCreated());
            assertEquals(createdBy, object.getCreatedBy());

            // update should be set
            assertNotNull(object.getUpdated());

            // updated should be  >= pre-save time
            assertTrue(DateUtils.truncatedCompareTo(preSave, object.getUpdated(), Calendar.SECOND) <= 0);

            if (user == null) {
                assertNull(object.getUpdatedBy());
            } else {
                assertEquals(object.getUpdatedBy(), user.getObjectReference());
            }
        }

        /**
         * Verifies that the saved state and the object are the same.
         *
         * @param object the object
         */
        public void checkSame(AuditableIMObject object) {
            assertEquals(created, object.getCreated());
            assertEquals(createdBy, object.getCreatedBy());
            assertEquals(updated, object.getUpdated());
            assertEquals(updatedBy, object.getUpdatedBy());
        }
    }

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs  an {@link AuditableTestHelper}.
     *
     * @param service the archetype service
     */
    public AuditableTestHelper(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Saves an object and verifies that the create/update information is set.
     *
     * @param object the object to save
     * @param user   the current user
     * @return the persistent object
     */
    public <T extends AuditableIMObject> T checkSave(T object, User user) {
        return checkSave(object, user, user, () -> service.save(object));
    }

    /**
     * Saves an object and verifies that the create/update information is set.
     *
     * @param object   the object to save
     * @param user     the current user. May be {@code null}
     * @param expected the expected user
     * @return the persistent object
     */
    public <T extends AuditableIMObject> T checkSave(T object, User user, User expected) {
        return checkSave(object, user, expected, () -> service.save(object));
    }

    /**
     * Saves an object and verifies that the create/update information is set.
     *
     * @param object   the object to save
     * @param user     the current user. May be {@code null}
     * @param expected the expected user
     * @param saver    a callback used to save the object(s)
     * @return the persistent object
     */
    public <T extends AuditableIMObject> T checkSave(T object, User user, User expected, Runnable saver) {
        AuthenticationContext context = new AuthenticationContextImpl();
        Date preSave = new Date();
        boolean first = false;
        AuditableState state = new AuditableState(object);

        if (object.isNew()) {
            checkNull(object);
            first = true;
        }
        context.setUser(user);
        saver.run();

        // verify the audit details pass back to the object
        if (first) {
            state.checkCreate(object, preSave, expected);
        } else {
            // created details shouldn't change
            state.checkUpdate(object, preSave, expected);
        }

        // now verify the audit details are the same in the persistent object
        return checkSame(object);
    }

    /**
     * Verifies that the create/update information doesn't change when an object is saved that has been modified.
     *
     * @param object the object to save
     * @param user   the current user
     */
    public void checkSaveUnchanged(AuditableIMObject object, User user) {
        checkSaveUnchanged(object, user, () -> service.save(object));
    }

    /**
     * Verifies that the create/update information doesn't change when an object is saved that has been modified.
     *
     * @param object the object to save
     * @param user   the current user
     */
    public void checkSaveUnchanged(AuditableIMObject object, User user, Runnable saver) {
        AuthenticationContext context = new AuthenticationContextImpl();
        context.setUser(user);

        AuditableState state = new AuditableState(object);
        saver.run();
        state.checkSame(object);
        checkSame(object);
    }

    /**
     * Verifies that the create/update details are not set.
     *
     * @param object the object to check
     */
    public void checkNull(AuditableIMObject object) {
        assertNull(object.getCreated());
        assertNull(object.getUpdated());
        assertNull(object.getCreatedBy());
        assertNull(object.getUpdatedBy());
    }

    /**
     * Verifies that an {@link AuditableIMObject} and its persistent version have the same audit information.
     *
     * @param object the object
     * @return the saved instance
     */
    @SuppressWarnings("unchecked")
    public <T extends AuditableIMObject> T checkSame(T object) {
        T saved = (T) service.get(object.getObjectReference());
        assertEquals(object.getCreated(), saved.getCreated());
        assertEquals(object.getUpdated(), saved.getUpdated());
        assertEquals(object.getCreatedBy(), saved.getCreatedBy());
        assertEquals(object.getUpdatedBy(), saved.getUpdatedBy());
        return saved;
    }

}
