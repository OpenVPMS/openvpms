/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Order;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link RelatedIMObjectsImpl} class.
 *
 * @author Tim Anderson
 */
public class RelatedIMObjectsImplTestCase extends AbstractIMObjectBeanTestCase {

    /**
     * Map of patients to their corresponding relationships.
     */
    private final Map<Party, EntityRelationship> relationshipMap = new LinkedHashMap<>();

    /**
     * Test patient 1.
     */
    private Party patient1;

    /**
     * Test patient 2.
     */
    private Party patient2;

    /**
     * Test patient 3.
     */
    private Party patient3;

    /**
     * Customer-patient relationship for patient1.
     */
    private EntityRelationship relationship1;

    /**
     * Customer-patient relationship for patient2.
     */
    private EntityRelationship relationship2;

    /**
     * The related objects.
     */
    private RelatedIMObjects<Party, EntityRelationship> objects;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party customer = createCustomer();
        patient1 = createPatient("C patient 1");
        patient2 = createPatient("A patient 2");
        patient3 = createPatient("B patient 3");

        // add relationships and sequence them
        relationship1 = addOwnerRelationship(customer, patient1);
        relationship1.setSequence(0);
        relationship2 = addOwnerRelationship(customer, patient2);
        relationship2.setSequence(1);
        EntityRelationship relationship3 = addOwnerRelationship(customer, patient3);
        relationship3.setSequence(2);

        objects = new RelatedIMObjectsImpl<>(customer, customer.getSourceEntityRelationships(), Party.class,
                                             getArchetypeService());

        // make a map of the patient relationships
        relationshipMap.put(patient1, relationship1);
        relationshipMap.put(patient2, relationship2);
        relationshipMap.put(patient3, relationship3);
    }

    /**
     * Tests the default behaviour. This should return the same values as {@link RelatedIMObjects#all()}
     */
    @Test
    public void testDefault() {
        checkAll(objects, null, false);
    }

    /**
     * Tests the behaviour of {@link RelatedIMObjectsImpl#all()}.
     */
    @Test
    public void testAll() {
        RelatedIMObjects<Party, EntityRelationship> all = objects.all();
        assertSame(all, objects);  // no policy change, so should return the same instance
        checkAll(all, null, false);
    }

    /**
     * Tests the behaviour of {@link RelatedIMObjectsImpl#active()}.
     */
    @Test
    public void testActive() {
        RelatedIMObjects<Party, EntityRelationship> active = objects.active();
        assertNotSame(active, objects);  // shouldn't mutate the original instance
        checkCollections(active, false, patient1, patient2, patient3);

        patient3.setActive(false);
        save(patient3);
        checkGetObjects(active, false, patient1, patient2);

        checkGetReferences(active, false, patient1, patient2, patient3);
        checkGetRelationships(active, false, patient1, patient2, patient3);
        // patient3 reference returned as getReferences()/getRelationships() ignores active/inactive

        checkGetObjectRelationships(active, false, patient1, patient2);

        relationship2.setActiveEndTime(DateUtils.addDays(new Date(), -1));
        checkGetObjects(active, false, patient1);

        checkGetReferences(active, false, patient1, patient3);
        checkGetRelationships(active, false, patient1, patient3);
        // patient3 reference returned as getReferences()/getRelationships() ignores active/inactive

        checkGetObjectRelationships(active, false, patient1);
    }

    /**
     * Tests the behaviour of {@link RelatedIMObjectsImpl#active(Date)}.
     */
    @Test
    public void testActiveAt() {
        Date now = new Date();

        RelatedIMObjects<Party, EntityRelationship> active = objects.active(now);
        assertNotSame(active, objects);  // shouldn't mutate the original instance
        checkCollections(active, false, patient1, patient2, patient3);

        patient3.setActive(false);
        save(patient3);
        checkGetObjects(active, false, patient1, patient2);

        checkGetReferences(active, false, patient1, patient2, patient3);
        checkGetRelationships(active, false, patient1, patient2, patient3);
        // patient3 reference returned as getReferences()/getRelationships() ignores active/inactive

        checkGetObjectRelationships(active, false, patient1, patient2);

        relationship2.setActiveEndTime(DateUtils.addDays(new Date(), -1));
        checkGetObjects(active, false, patient1);

        checkGetReferences(active, false, patient1, patient3);
        checkGetRelationships(active, false, patient1, patient3);
        // patient3 reference returned as getReferences()/getRelationships() ignores active/inactive

        checkGetObjectRelationships(active, false, patient1);
    }

    /**
     * Tests the {@link RelatedIMObjectsImpl#policy(Policy)} method.
     */
    @Test
    public void testPolicy() {
        // create a policy to return all objects and order relationships on sequence.
        Policy<EntityRelationship> policy1 = Policies.newPolicy(EntityRelationship.class)
                .anyObject()
                .orderBySequence()
                .build();
        RelatedIMObjects<Party, EntityRelationship> all = objects.policy(policy1);
        assertNotSame(all, objects);
        checkAll(all, policy1, true);
    }

    /**
     * Tests the {@link RelatedIMObjects#newPolicy()} method.
     */
    @Test
    public void testNewPolicy() {
        RelatedIMObjects<Party, EntityRelationship> inactive = objects.newPolicy().inactiveObjects().build();
        assertNotSame(inactive, objects);  // shouldn't mutate the original

        checkGetObject(inactive, null);
        checkGetObjectRelationship(inactive, null, null);
        checkGetObjects(inactive, false);  // all patients are active
        checkGetReferences(inactive, false, patient1, patient2, patient3);
        checkGetRelationships(inactive, false, patient1, patient2, patient3);
        checkGetObjectRelationships(inactive, false);

        patient2.setActive(false);
        save(patient2);
        checkGetObject(inactive, patient2);
        checkGetObjectRelationship(inactive, patient2, relationship2);
        checkGetObjects(inactive, false, patient2);
        checkGetReferences(inactive, false, patient1, patient2, patient3);
        checkGetRelationships(inactive, false, patient1, patient2, patient3);
        checkGetObjectRelationships(inactive, false, patient2);

        // now create a policy that selects active objects.
        RelatedIMObjects<Party, EntityRelationship> active = inactive.newPolicy().activeObjects().build();

        checkGetObjects(active, false, patient1, patient3);
        checkGetReferences(active, false, patient1, patient2, patient3);
        checkGetRelationships(active, false, patient1, patient2, patient3);
        checkGetObjectRelationships(active, false, patient1, patient3);
    }

    /**
     * Tests the {@link RelatedIMObjectsImpl#getObjects(int, int)} method.
     */
    @Test
    public void testGetObjectsPaged() {
        checkEquals(objects.getObjects(0, 3), patient1, patient2, patient3);  // maxResults same as available
        checkEquals(objects.getObjects(0, 10), patient1, patient2, patient3); // maxResults > available
        checkEquals(objects.getObjects(1, 2), patient2, patient3);
        checkEquals(objects.getObjects(2, 10), patient3);
        checkEquals(objects.getObjects(3, 10));                               // firstResult > available

        patient2.setActive(false);
        save(patient2);
        checkEquals(objects.active().getObjects(0, 2), patient1, patient3);   // should trigger 2 queries
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10), patient2);
        checkEquals(objects.getObjects(0, 10), patient1, patient2, patient3);

        patient1.setActive(false);
        save(patient1);
        checkEquals(objects.active().getObjects(0, 2), patient3);             // should trigger 2 queries
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10), patient1, patient2);
        checkEquals(objects.getObjects(0, 10), patient1, patient2, patient3);

        patient3.setActive(false);
        save(patient3);
        checkEquals(objects.active().getObjects(0, 2));                       // should trigger 2 queries
        checkEquals(objects.active().getObjects(0, 1));                       // should trigger 3 queries
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10), patient1, patient2, patient3);
        checkEquals(objects.getObjects(0, 10), patient1, patient2, patient3);

        // verify maxResults must be > 0
        try {
            objects.getObjects(0, 0);
            fail();
        } catch (IllegalArgumentException expected) {
            assertEquals("Argument 'maxResults' must be positive", expected.getMessage());
        }
    }

    /**
     * Tests the {@link RelatedIMObjectsImpl#getObjects(int, int, Order...)} method.
     */
    @Test
    public void testGetObjectsPagedSortable() {
        // patient1 = C.., patient2 = A.., patient3 = B..
        // in ascending order, expect patient2, patient3, patient1

        Order asc = Order.ascending("name");
        Order desc = asc.reverse();

        // ascending
        checkEquals(objects.getObjects(0, 3, asc), patient2, patient3, patient1);  // maxResults same as available
        checkEquals(objects.getObjects(0, 10, asc), patient2, patient3, patient1); // maxResults > available
        checkEquals(objects.getObjects(1, 2, asc), patient3, patient1);
        checkEquals(objects.getObjects(2, 10, asc), patient1);
        checkEquals(objects.getObjects(3, 10, asc));                               // firstResult > available

        /// descending
        checkEquals(objects.getObjects(0, 3, desc), patient1, patient3, patient2);  // maxResults same as available
        checkEquals(objects.getObjects(0, 10, desc), patient1, patient3, patient2); // maxResults > available
        checkEquals(objects.getObjects(1, 2, desc), patient3, patient2);
        checkEquals(objects.getObjects(2, 10, desc), patient2);
        checkEquals(objects.getObjects(3, 10, desc));                               // firstResult > available

        patient2.setActive(false);
        save(patient2);

        // ascending
        checkEquals(objects.active().getObjects(0, 2, asc), patient3, patient1);
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, asc), patient2);
        checkEquals(objects.getObjects(0, 10, asc), patient2, patient3, patient1);

        // descending
        checkEquals(objects.active().getObjects(0, 2, desc), patient1, patient3);
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, desc), patient2);
        checkEquals(objects.getObjects(0, 10, desc), patient1, patient3, patient2);

        patient1.setActive(false);
        save(patient1);

        // ascending
        checkEquals(objects.active().getObjects(0, 2, asc), patient3);
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, asc), patient2, patient1);
        checkEquals(objects.getObjects(0, 10, asc), patient2, patient3, patient1);

        // descending
        checkEquals(objects.active().getObjects(0, 2, desc), patient3);
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, desc), patient1, patient2);
        checkEquals(objects.getObjects(0, 10, desc), patient1, patient3, patient2);

        patient3.setActive(false);
        save(patient3);

        // ascending
        checkEquals(objects.active().getObjects(0, 2, asc));
        checkEquals(objects.active().getObjects(0, 1, asc));
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, asc),
                    patient2, patient3, patient1);
        checkEquals(objects.getObjects(0, 10, asc), patient2, patient3, patient1);

        // descending
        checkEquals(objects.active().getObjects(0, 2, desc));
        checkEquals(objects.active().getObjects(0, 1, desc));
        checkEquals(objects.newPolicy().inactiveObjects().build().getObjects(0, 10, desc),
                    patient1, patient3, patient2);
        checkEquals(objects.getObjects(0, 10, desc), patient1, patient3, patient2);

        // verify maxResults must be > 0
        try {
            objects.getObjects(0, 0, asc);
            fail();
        } catch (IllegalArgumentException expected) {
            assertEquals("Argument 'maxResults' must be positive", expected.getMessage());
        }
    }

    /**
     * Tests the {@link RelatedIMObjectsImpl#getObjectRelationships(int, int)} method.
     */
    @Test
    public void testGetRelationshipsPaged() {
        checkObjectRelationships(objects.getObjectRelationships(0, 3), patient1, patient2, patient3);
        // maxResults same as available

        checkObjectRelationships(objects.getObjectRelationships(0, 10), patient1, patient2, patient3);
        // maxResults > available

        checkObjectRelationships(objects.getObjectRelationships(1, 2), patient2, patient3);
        checkObjectRelationships(objects.getObjectRelationships(2, 10), patient3);
        checkObjectRelationships(objects.getObjectRelationships(3, 10));                  // firstResult > available

        patient2.setActive(false);
        save(patient2);
        checkObjectRelationships(objects.active().getObjectRelationships(0, 2), patient1, patient3);
        // should trigger 2 queries

        patient1.setActive(false);
        save(patient1);
        checkObjectRelationships(objects.active().getObjectRelationships(0, 2), patient3);
        // should trigger 2 queries

        patient3.setActive(false);
        save(patient3);
        checkObjectRelationships(objects.active().getObjectRelationships(0, 2));           // should trigger 2 queries
        checkObjectRelationships(objects.active().getObjectRelationships(0, 1));           // should trigger 3 queries

        // verify maxResults must be > 0
        try {
            objects.getObjectRelationships(0, 0);
            fail();
        } catch (IllegalArgumentException expected) {
            assertEquals("Argument 'maxResults' must be positive", expected.getMessage());
        }
    }

    /**
     * Verifies that two lists of objects match.
     *
     * @param actual   the actual result
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param expected the expected result
     */
    @SafeVarargs
    protected final <T> void checkEquals(Iterable<T> actual, boolean ordered, T... expected) {
        List<T> list = IterableUtils.toList(actual);
        if (ordered) {
            assertEquals(Arrays.asList(expected), list);
        } else {
            checkEquals(list, expected);
        }
    }

    /**
     * Tests the behaviour when all objects are being returned.
     *
     * @param related        the related objects
     * @param expectedPolicy the expected policy. May be {@code null}
     * @param ordered        expect the list to be ordered, else elements can be in any order
     */
    private void checkAll(RelatedIMObjects<Party, EntityRelationship> related,
                          Policy<EntityRelationship> expectedPolicy, boolean ordered) {
        assertEquals(expectedPolicy, related.getPolicy());

        // check getObject()
        if (ordered) {
            checkGetObject(related, patient1);
        } else {
            Party first = related.getObject();
            assertNotNull(first);
            assertTrue(first.isA("party.patientpet"));
        }

        // check getObjectRelationship()
        if (ordered) {
            checkGetObjectRelationship(related, patient1, relationship1);
        } else {
            ObjectRelationship<Party, EntityRelationship> firstRelationship = related.getObjectRelationship();
            assertNotNull(firstRelationship);
            assertTrue(firstRelationship.getObject().isA("party.patientpet"));
            assertEquals(relationshipMap.get(firstRelationship.getObject()), firstRelationship.getRelationship());
        }

        // verify the collection methods return the expected results
        checkCollections(related, ordered, patient1, patient2, patient3);

        // now deactivate patient3 and verify it is still returned
        patient3.setActive(false);
        save(patient3);
        checkCollections(related, ordered, patient1, patient2, patient3);
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getObject()} method returns the expected results.
     *
     * @param related the related objects
     * @param patient the expected patient
     */
    private void checkGetObject(RelatedIMObjects<Party, EntityRelationship> related, Party patient) {
        assertEquals(related.getObject(), patient);
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getObjectRelationship()} method returns the expected results.
     *
     * @param related      the related objects
     * @param patient      the expected patient
     * @param relationship the expected relationship
     */
    private void checkGetObjectRelationship(RelatedIMObjects<Party, EntityRelationship> related, Party patient,
                                            EntityRelationship relationship) {
        ObjectRelationship<Party, EntityRelationship> objectRelationship = related.getObjectRelationship();
        if (patient == null) {
            assertNull(objectRelationship);
        } else {
            assertNotNull(objectRelationship);
            assertEquals(patient, objectRelationship.getObject());
            assertEquals(relationship, objectRelationship.getRelationship());
        }
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getReferences()} method returns the expected results.
     *
     * @param related  the related objects
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients
     */
    private void checkGetReferences(RelatedIMObjects<Party, EntityRelationship> related, boolean ordered,
                                    Party... patients) {
        List<Reference> references = new ArrayList<>();
        for (Party patient : patients) {
            references.add(patient.getObjectReference());
        }
        if (ordered) {
            assertEquals(references, related.getReferences());
        } else {
            checkEquals(related.getReferences(), references.toArray(new Reference[0]));
        }
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getObjects()}, {@link RelatedIMObjectsImpl#getReferences()},
     * {@link RelatedIMObjectsImpl#getRelationships()} and {@link RelatedIMObjectsImpl#getObjectRelationships()}
     * methods return the expected results.
     *
     * @param related  the related objects
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients
     */
    private void checkCollections(RelatedIMObjects<Party, EntityRelationship> related, boolean ordered,
                                  Party... patients) {
        checkGetObjects(related, ordered, patients);
        checkGetReferences(related, ordered, patients);
        checkGetRelationships(related, ordered, patients);
        checkGetObjectRelationships(related, ordered, patients);
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getObjects()} method returns the expected results.
     *
     * @param related  the related objects
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients
     */
    private void checkGetObjects(RelatedIMObjects<Party, EntityRelationship> related, boolean ordered,
                                 Party... patients) {
        checkEquals(related.getObjects(), ordered, patients);
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getRelationships()} method returns the expected results.
     *
     * @param related  the related objects
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients
     */
    private void checkGetRelationships(RelatedIMObjects<Party, EntityRelationship> related, boolean ordered,
                                       Party... patients) {
        List<EntityRelationship> relationships = new ArrayList<>();
        for (Party patient : patients) {
            relationships.add(relationshipMap.get(patient));
        }
        if (ordered) {
            assertEquals(relationships, related.getRelationships());
        } else {
            checkEquals(related.getRelationships(), relationships.toArray(new EntityRelationship[0]));
        }
    }

    /**
     * Verifies the {@link RelatedIMObjectsImpl#getObjectRelationships()} method returns the expected results.
     *
     * @param related  the related objects
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients
     */
    private void checkGetObjectRelationships(RelatedIMObjects<Party, EntityRelationship> related, boolean ordered,
                                             Party... patients) {
        checkObjectRelationships(related.getObjectRelationships(), ordered, patients);
    }

    /**
     * Verifies an {@link Iterable} of {@link ObjectRelationship} matches that expected.
     *
     * @param iterable the iterable
     * @param patients the expected patients referred to by the relationships
     */
    private void checkObjectRelationships(Iterable<ObjectRelationship<Party, EntityRelationship>> iterable,
                                          Party... patients) {
        checkObjectRelationships(iterable, true, patients);
    }

    /**
     * Verifies an {@link Iterable} of {@link ObjectRelationship} matches that expected.
     *
     * @param iterable the iterable
     * @param ordered  expect the list to be ordered, else elements can be in any order
     * @param patients the expected patients referred to by the relationships
     */
    private void checkObjectRelationships(Iterable<ObjectRelationship<Party, EntityRelationship>> iterable,
                                          boolean ordered, Party... patients) {
        int count = 0;
        if (ordered) {
            for (ObjectRelationship<Party, EntityRelationship> relationship : iterable) {
                Party patient = patients[count];
                assertEquals(patient, relationship.getObject());
                assertEquals(relationshipMap.get(patient), relationship.getRelationship());
                count++;
            }
        } else {
            for (ObjectRelationship<Party, EntityRelationship> relationship : iterable) {
                boolean found = false;
                for (Party patient : patients) {
                    if (relationship.getObject().equals(patient)) {
                        assertEquals(relationshipMap.get(patient), relationship.getRelationship());
                        found = true;
                        break;
                    }
                }
                assertTrue(found);
                count++;
            }
        }
        assertEquals(count, patients.length);
    }
}