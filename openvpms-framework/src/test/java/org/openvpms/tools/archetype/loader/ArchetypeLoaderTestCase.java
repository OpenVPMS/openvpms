/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.loader;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.datatypes.property.AssertionProperty;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyList;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyMap;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.archetype.ActionTypeDescriptor;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.system.common.util.StringUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ArchetypeLoader}.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("/org/openvpms/tools/data/loader/archetype-data-loader-appcontext.xml")
public class ArchetypeLoaderTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service.
     */
    @Autowired
    private IArchetypeService service;

    /**
     * The archetype loader.
     */
    private ArchetypeLoader loader;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        loader = ArchetypeLoader.newBootstrapLoader(service);
    }

    /**
     * Tests the {@link ArchetypeLoader#loadAssertion(AssertionTypeDescriptor)} method for a new assertion.
     */
    @Test
    public void testAddAssertion() {
        AssertionTypeDescriptor descriptor = createAssertionType();
        assertNull(service.getAssertionTypeDescriptor(descriptor.getName()));

        assertTrue(loader.loadAssertion(descriptor));
        IMObject loaded = service.get(descriptor.getObjectReference());
        assertNotNull(loaded);
        assertEquals(descriptor, service.getAssertionTypeDescriptor(descriptor.getName()));
        assertEquals(descriptor, loaded);
    }

    /**
     * Tests the {@link ArchetypeLoader#loadAssertion(AssertionTypeDescriptor)} method can update assertions.
     */
    @Test
    public void testUpdateAssertion() {
        AssertionTypeDescriptor descriptor1 = createAssertionType();
        assertNull(service.getAssertionTypeDescriptor(descriptor1.getName()));

        assertTrue(loader.loadAssertion(descriptor1));
        assertFalse(descriptor1.isNew());  // has been saved

        AssertionTypeDescriptor descriptor2 = createAssertionType(descriptor1.getName());
        assertFalse(loader.loadAssertion(descriptor2)); // no changes
        assertTrue(descriptor2.isNew());  // won't be saved as it should be updating an existing assertion

        // now add some action types
        AssertionTypeDescriptor descriptor3 = createAssertionType(descriptor2.getName());
        descriptor3.addActionType(createActionType("create", "DummyClass", "method1"));
        descriptor3.addActionType(createActionType("set", "DummyClass", "method2"));
        descriptor3.addActionType(createActionType("validate", "DummyClass", "method3"));
        assertTrue(loader.loadAssertion(descriptor3));
        assertTrue(descriptor3.isNew());     // won't be saved as it should be updating an existing assertion

        // update the action types.
        AssertionTypeDescriptor descriptor4 = createAssertionType(descriptor3.getName());
        descriptor4.addActionType(createActionType("create", "DummyClass", "method1"));
        descriptor4.addActionType(createActionType("validate", "DummyClass2", "method2"));
        assertTrue(loader.loadAssertion(descriptor4));
        assertTrue(descriptor4.isNew());     // won't be saved as it should be updating an existing assertion

        AssertionTypeDescriptor saved = service.get(descriptor1.getObjectReference(), AssertionTypeDescriptor.class);
        assertEquals(descriptor1.getName(), saved.getName());
        assertEquals(2, saved.getActionTypes().size());
        checkActionType(saved, "create", "DummyClass", "method1");
        checkActionType(saved, "validate", "DummyClass2", "method2");

        // verify that an unchanged assertion has no effect
        AssertionTypeDescriptor descriptor5 = createAssertionType(descriptor4.getName());
        descriptor5.addActionType(createActionType("create", "DummyClass", "method1"));
        descriptor5.addActionType(createActionType("validate", "DummyClass2", "method2"));
        assertFalse(loader.loadAssertion(descriptor5));
        assertTrue(descriptor5.isNew());     // won't be saved as it should be updating an existing assertion
    }

    /**
     * Tests the {@link ArchetypeLoader#loadArchetype(ArchetypeDescriptor)} method for a new archetype.
     */
    @Test
    public void testAddArchetype() {
        String archetype = "act." + RandomUtils.nextInt();
        ArchetypeDescriptor descriptor = createArchetype(archetype);
        assertNull(service.getArchetypeDescriptor(archetype));

        descriptor.addNodeDescriptor(createNodeDescriptor("a", 0));
        descriptor.addNodeDescriptor(createNodeDescriptor("b", 1));

        assertTrue(loader.loadArchetype(descriptor));
        assertFalse(descriptor.isNew());

        assertEquals(descriptor, service.getArchetypeDescriptor(archetype));
    }

    /**
     * Tests the {@link ArchetypeLoader#loadArchetype(ArchetypeDescriptor)} method can update archetypes.
     */
    @Test
    public void testUpdateArchetype() {
        String archetype = "act." + RandomUtils.nextInt();
        ArchetypeDescriptor descriptor1 = createArchetype(archetype);
        assertNull(service.getArchetypeDescriptor(archetype));

        descriptor1.addNodeDescriptor(createNodeDescriptor("a", 0));
        descriptor1.addNodeDescriptor(createNodeDescriptor("b", 1));

        assertTrue(loader.loadArchetype(descriptor1));
        assertFalse(descriptor1.isNew());

        ArchetypeDescriptor descriptor2 = createArchetype(archetype);
        descriptor2.addNodeDescriptor(createNodeDescriptor("b", 0));
        descriptor2.addNodeDescriptor(createNodeDescriptor("a", 1));
        descriptor2.addNodeDescriptor(createNodeDescriptor("c", 2));
        assertTrue(loader.loadArchetype(descriptor2));
        assertTrue(descriptor2.isNew());

        ArchetypeDescriptor saved2 = service.getArchetypeDescriptor(archetype);
        assertNotNull(saved2);
        assertEquals(saved2.getObjectReference(), descriptor1.getObjectReference());
        assertEquals(3, saved2.getNodeDescriptors().size());
        checkNode(saved2, "b", 0);
        checkNode(saved2, "a", 1);
        checkNode(saved2, "c", 2);

        ArchetypeDescriptor descriptor3 = createArchetype(archetype);
        descriptor3.addNodeDescriptor(createNodeDescriptor("a", 0));
        descriptor3.addNodeDescriptor(createNodeDescriptor("c", 1));
        assertTrue(loader.loadArchetype(descriptor3));
        assertTrue(descriptor3.isNew());

        ArchetypeDescriptor saved3 = service.getArchetypeDescriptor(archetype);
        assertEquals(saved3.getObjectReference(), descriptor1.getObjectReference());
        assertEquals(2, saved3.getNodeDescriptors().size());
        checkNode(saved3, "a", 0);
        checkNode(saved3, "c", 1);

        ArchetypeDescriptor descriptor4 = createArchetype(archetype);
        descriptor4.addNodeDescriptor(createNodeDescriptor("a", 0));
        descriptor4.addNodeDescriptor(createNodeDescriptor("c", 1));
        assertFalse(loader.loadArchetype(descriptor4));
        assertTrue(descriptor4.isNew());
    }

    /**
     * Verifies that a node can be updated.
     */
    @Test
    public void testUpdateNode() {
        String archetype = "act." + RandomUtils.nextInt();
        ArchetypeDescriptor descriptor1 = createArchetype(archetype);
        assertNull(service.getArchetypeDescriptor(archetype));

        NodeDescriptor node1 = createNodeDescriptor("a", 0);
        descriptor1.addNodeDescriptor(node1);

        assertTrue(loader.loadArchetype(descriptor1));
        assertFalse(descriptor1.isNew());

        ArchetypeDescriptor descriptor2 = createArchetype(archetype);
        NodeDescriptor node2 = createNodeDescriptor("a", 0);
        node2.setDescription("a updated");
        node2.setDisplayName("A node");
        node2.setDerived(true);
        node2.setDerivedValue("1 + 1");
        node2.setBaseName("some base name");
        node2.setHidden(true);
        node2.setReadOnly(true);
        node2.setPath("/details/updated");
        node2.setIndex(1);
        node2.setType(List.class.getName());
        node2.setDefaultValue("'another default'");
        node2.setFilter("some filter");
        node2.setMinCardinality(1);
        node2.setMaxCardinality(2);
        node2.setMinLength(2);
        node2.setMaxLength(21);
        node2.setParentChild(true);
        descriptor2.addNodeDescriptor(node2);

        assertTrue(loader.loadArchetype(descriptor2));
        assertTrue(descriptor2.isNew());  // updated existing archetype

        ArchetypeDescriptor saved = service.get(descriptor1.getObjectReference(), ArchetypeDescriptor.class);
        NodeDescriptor savedNode = saved.getNodeDescriptor("a");
        assertNotNull(savedNode);
        assertEquals("a", savedNode.getName());
        assertEquals("a updated", savedNode.getDescription());
        assertEquals("A node", savedNode.getDisplayName());
        assertTrue(savedNode.isDerived());
        assertEquals("1 + 1", savedNode.getDerivedValue());
        assertEquals("some base name", savedNode.getBaseName());
        assertTrue(savedNode.isHidden());
        assertTrue(savedNode.isReadOnly());
        assertEquals("/details/updated", savedNode.getPath());
        assertEquals(1, savedNode.getIndex());
        assertEquals(List.class.getName(), savedNode.getType());
        assertEquals("'another default'", savedNode.getDefaultValue());
        assertEquals("some filter", savedNode.getFilter());
        assertEquals(1, savedNode.getMinCardinality());
        assertEquals(2, savedNode.getMaxCardinality());
        assertEquals(2, savedNode.getMinLength());
        assertEquals(21, savedNode.getMaxLength());
        assertTrue(savedNode.isParentChild());
    }

    /**
     * Verifies that assertions on a node can be updated.
     */
    @Test
    public void testUpdateNodeAssertion() {
        String archetype = "act." + RandomUtils.nextInt();
        ArchetypeDescriptor descriptor1 = createArchetype(archetype);
        assertNull(service.getArchetypeDescriptor(archetype));

        NodeDescriptor node1 = createNodeDescriptor("a", 0);
        node1.addAssertionDescriptor(createLocalLookupAssertion("error1", "IN_PROGRESS", "COMPLETED"));
        descriptor1.addNodeDescriptor(node1);
        assertTrue(loader.loadArchetype(descriptor1));

        ArchetypeDescriptor descriptor2 = createArchetype(archetype);
        NodeDescriptor node2 = createNodeDescriptor("a", 0);
        node2.addAssertionDescriptor(createLocalLookupAssertion("error1", "IN_PROGRESS", "COMPLETED", "POSTED"));
        descriptor2.addNodeDescriptor(node2);
        assertTrue(loader.loadArchetype(descriptor2));
        assertTrue(descriptor2.isNew());

        ArchetypeDescriptor descriptor3 = createArchetype(archetype);
        NodeDescriptor node3 = createNodeDescriptor("a", 0);
        node3.addAssertionDescriptor(createLocalLookupAssertion("error1", "PENDING", "IN_PROGRESS", "POSTED"));
        descriptor3.addNodeDescriptor(node3);
        assertTrue(loader.loadArchetype(descriptor3));
        assertTrue(descriptor3.isNew());

        ArchetypeDescriptor saved = service.get(descriptor1.getObjectReference(), ArchetypeDescriptor.class);
        assertNotNull(saved);
        NodeDescriptor savedNode = saved.getNodeDescriptor("a");
        assertNotNull(savedNode);
        assertEquals(1, savedNode.getAssertionDescriptors().size());
        AssertionDescriptor savedAssertion = savedNode.getAssertionDescriptor("lookup.local");
        assertNotNull(savedAssertion);
        assertEquals("error1", savedAssertion.getErrorMessage());
        PropertyList entries = (PropertyList) savedAssertion.getProperty("entries");
        assertEquals(3, entries.getProperties().size());
        NamedProperty[] properties = entries.getPropertiesAsArray();
        assertEquals("PENDING", properties[0].getName());
        assertEquals("IN_PROGRESS", properties[1].getName());
        assertEquals("POSTED", properties[2].getName());
    }

    /**
     * Verifies the order of items in an assertion can change.
     */
    @Test
    public void testChangeAssertionOrder() {
        String[] range1 = {"entityIdentity.alias", "entityIdentity.microchip", "entityIdentity.petTag"};
        String archetype = "party." + RandomUtils.nextInt();
        ArchetypeDescriptor descriptor1 = createArchetype(archetype, Party.class);
        NodeDescriptor node1 = createNodeDescriptor("identities", 0);
        node1.addAssertionDescriptor(createArchetypeRangeAssertion(range1));
        descriptor1.addNodeDescriptor(node1);
        assertTrue(loader.loadArchetype(descriptor1));
        assertArrayEquals(range1, node1.getArchetypeRange());

        // change order of the range
        String[] range2 = {"entityIdentity.microchip", "entityIdentity.petTag", "entityIdentity.alias"};
        ArchetypeDescriptor descriptor2 = createArchetype(archetype, Party.class);
        NodeDescriptor node2 = createNodeDescriptor("identities", 0);
        node2.addAssertionDescriptor(createArchetypeRangeAssertion(range2));
        descriptor2.addNodeDescriptor(node2);
        assertTrue(loader.loadArchetype(descriptor2));

        // verify the range has updated
        ArchetypeDescriptor saved = service.get(descriptor1.getObjectReference(), ArchetypeDescriptor.class);
        assertNotNull(saved);
        NodeDescriptor savedNode = saved.getNodeDescriptor("identities");
        assertNotNull(savedNode);
        assertArrayEquals(range2, savedNode.getArchetypeRange());
    }

    /**
     * Verifies a node descriptor matches that expected.
     *
     * @param descriptor the archetype descriptor
     * @param name       the node name
     * @param index      the expected index
     */
    private void checkNode(ArchetypeDescriptor descriptor, String name, int index) {
        NodeDescriptor node = descriptor.getNodeDescriptor(name);
        assertNotNull(node);
        assertEquals(index, node.getIndex());
    }

    /**
     * Verifies an action type matches that expected.
     *
     * @param descriptor the assertion type descriptor
     * @param name       the action type name
     * @param className  the expected class name
     * @param methodName the expected method name
     */
    private void checkActionType(AssertionTypeDescriptor descriptor, String name, String className, String methodName) {
        ActionTypeDescriptor actionType = descriptor.getActionType(name);
        assertNotNull(actionType);
        assertEquals(name, actionType.getName());
        assertEquals(className, actionType.getClassName());
        assertEquals(methodName, actionType.getMethodName());
    }

    /**
     * Creates an assertion type descriptor.
     *
     * @return a new assertion type descriptor
     */
    private AssertionTypeDescriptor createAssertionType() {
        String name = "a" + RandomUtils.nextInt();
        return createAssertionType(name);
    }

    /**
     * Creates an assertion type descriptor.
     *
     * @param name the name
     * @return a new assertion type descriptor
     */
    private AssertionTypeDescriptor createAssertionType(String name) {
        AssertionTypeDescriptor descriptor = new AssertionTypeDescriptor();
        descriptor.setName(name);
        descriptor.setPropertyArchetype("assertion." + name + ".1.0");
        return descriptor;
    }

    /**
     * Creates an action type descriptor.
     *
     * @param name       the name
     * @param className  the class name
     * @param methodName the method name
     * @return a new action type descriptor
     */
    private ActionTypeDescriptor createActionType(String name, String className, String methodName) {
        ActionTypeDescriptor descriptor
                = new org.openvpms.component.business.domain.im.archetype.descriptor.ActionTypeDescriptor();
        descriptor.setName(name);
        descriptor.setClassName(className);
        descriptor.setMethodName(methodName);
        return descriptor;
    }

    /**
     * Creates an archetype descriptor.
     *
     * @param archetype the archetype
     * @return a new archetype descriptor
     */
    private ArchetypeDescriptor createArchetype(String archetype) {
        return createArchetype(archetype, Act.class);
    }

    /**
     * Creates an archetype descriptor.
     *
     * @param archetype the archetype
     * @param type      the implementation class
     * @return a new archetype descriptor
     */
    private ArchetypeDescriptor createArchetype(String archetype, Class<?> type) {
        ArchetypeDescriptor descriptor
                = new org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor();
        descriptor.setName(archetype + ".1.0");
        descriptor.setClassName(type.getName());
        descriptor.setLatest(true);
        descriptor.setPrimary(true);
        return descriptor;
    }

    /**
     * Creates a node descriptor.
     *
     * @param name  the node name
     * @param index the node index
     * @return a new node descriptor
     */
    private NodeDescriptor createNodeDescriptor(String name, int index) {
        NodeDescriptor descriptor = new org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor();
        descriptor.setName(name);
        descriptor.setDisplayName(name + " node");
        descriptor.setDerived(false);
        descriptor.setHidden(false);
        descriptor.setReadOnly(false);
        descriptor.setPath("/details/" + name);
        descriptor.setIndex(index);
        descriptor.setType(String.class.getName());
        descriptor.setDefaultValue("'" + name + " default'");
        descriptor.setFilter(null);
        descriptor.setMinCardinality(0);
        descriptor.setMinCardinality(1);
        descriptor.setMinLength(1);
        descriptor.setMaxLength(20);
        descriptor.setParentChild(false);
        descriptor.setDescription(name + " description");
        return descriptor;
    }

    /**
     * Creates a lookup.local assertion.
     *
     * @param errorMessage the error message
     * @param entries      the assertion entries
     * @return a new assertion descriptor
     */
    private AssertionDescriptor createLocalLookupAssertion(String errorMessage, String... entries) {
        org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor descriptor
                = new org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor();
        descriptor.setName("lookup.local");
        PropertyList list = new PropertyList();
        list.setName("entries");
        for (String entry : entries) {
            list.addProperty(createAssertionProperty(entry, StringUtilities.unCamelCase(entry)));
        }
        descriptor.addProperty(list);
        descriptor.setErrorMessage(errorMessage);
        return descriptor;
    }

    /**
     * Creates an archetypeRange assertion.
     *
     * @param archetypes the assertion entries
     * @return a new assertion descriptor
     */
    private AssertionDescriptor createArchetypeRangeAssertion(String... archetypes) {
        org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor descriptor
                = new org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor();
        descriptor.setName("archetypeRange");
        PropertyList list = new PropertyList();
        list.setName("archetypes");
        for (String archetype : archetypes) {
            PropertyMap map = new PropertyMap("archetype");
            map.addProperty(createAssertionProperty("shortName", archetype));
            list.addProperty(map);
        }
        descriptor.addProperty(list);
        return descriptor;
    }

    /**
     * Creates an assertion property.
     *
     * @param name  the property name
     * @param value the property value
     * @return a new assertion property
     */
    private AssertionProperty createAssertionProperty(String name, String value) {
        AssertionProperty property = new AssertionProperty();
        property.setName(name);
        property.setValue(value);
        return property;
    }

}
