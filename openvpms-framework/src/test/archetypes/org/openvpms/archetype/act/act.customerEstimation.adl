<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="act.customerEstimation.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.act.Act" displayName="Estimation">
        <node name="id" path="/id" displayName="id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="name" type="java.lang.String" path="/name"
              hidden="true" minCardinality="1" derived="true"
              derivedValue="concat(/title, '')"/>
        <node name="description" type="java.lang.String"
              path="/description" hidden="true" derived="true"
              derivedValue="concat(/title, ' Low Estimate: ',/lowTotal,' High Estimate: ', /highTotal)"/>
        <node name="startTime" path="/activityStartTime"
              type="java.util.Date" minCardinality="1" defaultValue="java.util.Date.new()"/>
        <node name="title" path="/title" type="java.lang.String" minCardinality="1" maxLength="30"
              defaultValue="'Enter a title ...'"/>
        <node name="lowTotal" path="/details/lowTotal"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money"
              derivedValue="sum(openvpms:resolveRefs(/sourceActRelationships/target)/details/lowTotal)"
              derived="true" defaultValue="'0.0'" minCardinality="1"/>
        <node name="highTotal" path="/details/highTotal"
              type="org.openvpms.component.business.domain.im.datatypes.quantity.Money"
              derivedValue="sum(openvpms:resolveRefs(/sourceActRelationships/target)/details/highTotal)"
              derived="true" defaultValue="'0.0'" minCardinality="1"/>
        <node name="printed" path="/details/printed" type="java.lang.Boolean" defaultValue="false()"
              minCardinality="1"/>
        <node name="status" path="/status" type="java.lang.String"
              minCardinality="1">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="COMPLETED" value="Completed"/>
                    <property name="CANCELLED" value="Cancelled"/>
                    <property name="POSTED" value="Posted"/>
                </propertyList>
                <errorMessage>An error message</errorMessage>
            </assertion>
        </node>
        <node name="items" path="/sourceActRelationships"
              type="java.util.HashSet" baseName="SourceActRelationship" minCardinality="1" maxCardinality="*">
            <assertion name="archetypeRange">
                <propertyList name="archetypes">
                    <propertyMap name="archetype">
                        <property name="shortName"
                                  value="actRelationship.customerEstimationItem"/>
                    </propertyMap>
                </propertyList>
                <errorMessage>errorMessage</errorMessage>
            </assertion>
        </node>
        <node name="customer" path="/participations" type="java.util.HashSet"
              minCardinality="0" maxCardinality="1"
              filter="participation.customer" hidden="true"/>
        <node name="author" path="/participations" type="java.util.HashSet"
              minCardinality="0" maxCardinality="1"
              filter="participation.author" hidden="true"/>
        <node name="participants" path="/participations" parentChild="true"
              type="java.util.HashSet" baseName="Participation" minCardinality="0" maxCardinality="*">
            <assertion name="archetypeRange">
                <propertyList name="archetypes">
                    <propertyMap name="archetype">
                        <property name="shortName"
                                  value="participation.customer"/>
                    </propertyMap>
                    <propertyMap name="archetype">
                        <property name="shortName"
                                  value="participation.author"/>
                    </propertyMap>
                </propertyList>
                <errorMessage>errorMessage</errorMessage>
            </assertion>
        </node>
    </archetype>
</archetypes>
