/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.comparator;

import org.openvpms.component.model.archetype.AssertionTypeDescriptor;

import java.util.List;

/**
 * Tracks changes to an {@link AssertionTypeDescriptor}.
 *
 * @author Tim Anderson
 */
public class AssertionTypeChange extends DescriptorChange<AssertionTypeDescriptor> {

    /**
     * The field changes.
     */
    private final List<AssertionTypeFieldChange> fieldChanges;

    /**
     * The action type changes.
     */
    private final List<ActionTypeChange> actionTypeChanges;

    /**
     * Constructs an {@link AssertionTypeChange}.
     * <p/>
     * Note: at least one of the arguments must be non-null;
     *
     * @param oldVersion        the old version. May be {@code null}
     * @param newVersion        the new version. May be {@code null}
     * @param fieldChanges      the field changes
     * @param actionTypeChanges the action type changes
     */
    public AssertionTypeChange(AssertionTypeDescriptor oldVersion, AssertionTypeDescriptor newVersion,
                               List<AssertionTypeFieldChange> fieldChanges, List<ActionTypeChange> actionTypeChanges) {
        super(oldVersion, newVersion);
        this.fieldChanges = fieldChanges;
        this.actionTypeChanges = actionTypeChanges;
    }

    /**
     * Returns the field changes.
     *
     * @return the field changes
     */
    public List<AssertionTypeFieldChange> getFieldChanges() {
        return fieldChanges;
    }

    /**
     * Returns the action type changes.
     *
     * @return the action type changes
     */
    public List<ActionTypeChange> getActionTypeChanges() {
        return actionTypeChanges;
    }
}