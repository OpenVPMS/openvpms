/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.comparator;


import org.openvpms.component.model.archetype.ActionTypeDescriptor;

/**
 * Tracks changes between ActionTypeDescriptors.
 *
 * @author Tim Anderson
 */
public class ActionTypeChange extends DescriptorChange<ActionTypeDescriptor> {

    /**
     * Constructs an {@link ActionTypeChange}.
     * <p/>
     * Note: at least one of the arguments must be non-null;
     *
     * @param oldVersion the old version. May be {@code null}
     * @param newVersion the new version. May be {@code null}
     */
    public ActionTypeChange(ActionTypeDescriptor oldVersion, ActionTypeDescriptor newVersion) {
        super(oldVersion, newVersion);
    }
}
