/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.comparator;

import org.openvpms.component.model.archetype.ActionTypeDescriptor;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Compares {@link AssertionTypeComparator}s for differences.
 *
 * @author Tim Anderson
 */
public class AssertionTypeComparator extends AbstractComparator {

    /**
     * Default constructor.
     */
    public AssertionTypeComparator() {
        super();
    }

    /**
     * Compares two assertion type descriptors.
     *
     * @param oldVersion the old version. May be {@code null}
     * @param newVersion the new version. May be {@code null}
     * @return the changes, or {@code null} if they are the same
     */
    public AssertionTypeChange compare(AssertionTypeDescriptor oldVersion, AssertionTypeDescriptor newVersion) {
        AssertionTypeChange result;
        if (oldVersion != null && newVersion != null) {
            List<AssertionTypeFieldChange> fieldChanges = new ArrayList<>();
            if (!Objects.equals(oldVersion.getName(), newVersion.getName())) {
                fieldChanges.add(new AssertionTypeFieldChange(AssertionTypeFieldChange.Field.NAME,
                                                              oldVersion.getName(), newVersion.getName()));
            } else if (!Objects.equals(oldVersion.getPropertyArchetype(), newVersion.getPropertyArchetype())) {
                fieldChanges.add(new AssertionTypeFieldChange(AssertionTypeFieldChange.Field.PROPERTY_ARCHETYPE,
                                                              oldVersion.getPropertyArchetype(),
                                                              newVersion.getPropertyArchetype()));
            }
            List<ActionTypeChange> actionTypeChanges = getActionTypeChanges(oldVersion, newVersion);
            result = fieldChanges.isEmpty() && actionTypeChanges.isEmpty()
                     ? null : new AssertionTypeChange(oldVersion, newVersion, fieldChanges, actionTypeChanges);

        } else if (oldVersion == null && newVersion == null) {
            result = null;
        } else {
            result = new AssertionTypeChange(oldVersion, newVersion, Collections.emptyList(),
                                             Collections.emptyList());
        }
        return result;
    }

    /**
     * Returns the action type changes between two versions of an assertion type descriptor.
     *
     * @param oldVersion the old version of the descriptor
     * @param newVersion the new version of the descriptor
     * @return the changes
     */
    private List<ActionTypeChange> getActionTypeChanges(AssertionTypeDescriptor oldVersion,
                                                        AssertionTypeDescriptor newVersion) {
        List<ActionTypeChange> result = new ArrayList<>();
        Map<String, ActionTypeDescriptor> oldActionTypes = toMap(oldVersion.getActionTypes());
        Map<String, ActionTypeDescriptor> newActionTypes = toMap(newVersion.getActionTypes());
        Set<String> added = getAdded(oldActionTypes, newActionTypes);
        Set<String> deleted = getDeleted(oldActionTypes, newActionTypes);
        Set<String> retained = getRetained(oldActionTypes, newActionTypes);
        for (String addedType : added) {
            result.add(new ActionTypeChange(null, newActionTypes.get(addedType)));
        }
        for (String deletedType : deleted) {
            result.add(new ActionTypeChange(oldActionTypes.get(deletedType), null));
        }
        for (String retainedType : retained) {
            ActionTypeDescriptor oldType = oldActionTypes.get(retainedType);
            ActionTypeDescriptor newType = newActionTypes.get(retainedType);
            if (!same(oldType, newType)) {
                result.add(new ActionTypeChange(oldType, newType));
            }
        }
        return result;
    }

    /**
     * Determines if two action type descriptors are the same.
     *
     * @param oldVersion the old version
     * @param newVersion the new version
     * @return {@code true} if they are the same, otherwise {@code false}
     */
    private boolean same(ActionTypeDescriptor oldVersion, ActionTypeDescriptor newVersion) {
        return (Objects.equals(oldVersion.getClassName(), newVersion.getClassName())
                || Objects.equals(oldVersion.getMethodName(), newVersion.getMethodName()));
    }
}
