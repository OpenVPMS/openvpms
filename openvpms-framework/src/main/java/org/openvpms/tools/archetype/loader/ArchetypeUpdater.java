/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.loader;

import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.tools.archetype.comparator.ArchetypeChange;
import org.openvpms.tools.archetype.comparator.ArchetypeComparator;
import org.openvpms.tools.archetype.comparator.FieldChange;
import org.openvpms.tools.archetype.comparator.NodeChange;
import org.openvpms.tools.archetype.comparator.NodeFieldChange;

import java.util.ArrayList;

/**
 * Updates an {@link ArchetypeDescriptor}.
 *
 * @author Tim Anderson
 */
public class ArchetypeUpdater extends DescriptorUpdater<ArchetypeDescriptor> {

    /**
     * Default constructor.
     */
    public ArchetypeUpdater() {
        super();
    }

    /**
     * Updates an archetype.
     *
     * @param target the descriptor to update
     * @param source the descriptor to update from
     * @return {@code true} if the descriptor was updated, {@code false} if no changes were applied
     */
    public boolean update(ArchetypeDescriptor target, ArchetypeDescriptor source) {
        boolean updated = false;
        ArchetypeComparator comparator = new ArchetypeComparator();
        ArchetypeChange change = comparator.compare(target, source);
        if (change != null) {
            for (FieldChange fieldChange : change.getFieldChanges()) {
                updateArchetypeDescriptorField(target, fieldChange);
                updated = true;
            }
            for (NodeChange nodeChange : change.getNodeChanges()) {
                if (nodeChange.isAdd()) {
                    target.addNodeDescriptor(nodeChange.getNewVersion());
                } else if (nodeChange.isUpdate()) {
                    for (NodeFieldChange nodeFieldChange : nodeChange.getChanges()) {
                        NodeDescriptor nodeDescriptor = target.getNodeDescriptor(nodeChange.getName());
                        updateNodeDescriptorField(nodeDescriptor, nodeFieldChange);
                    }
                } else {
                    NodeDescriptor deleted = target.getNodeDescriptor(nodeChange.getName());
                    if (deleted != null) {
                        target.removeNodeDescriptor(deleted);
                    }
                }
                updated = true;
            }
        }
        return updated;
    }

    /**
     * Updates an archetype descriptor.
     *
     * @param descriptor the archetype descriptor to update
     * @param change     the change to apply
     */
    private void updateArchetypeDescriptorField(ArchetypeDescriptor descriptor, FieldChange change) {
        switch (change.getField()) {
            case NAME:
                descriptor.setName(toString(change));
                break;
            case DISPLAY_NAME:
                descriptor.setDisplayName(toString(change));
                break;
            case CLASS_NAME:
                descriptor.setClassName(toString(change));
                break;
            case PRIMARY:
                descriptor.setPrimary(toBoolean(change));
                break;
            case SINGLETON:
                descriptor.setSingleton(toBoolean(change));
                break;
            case ACTIVE:
                descriptor.setActive(toBoolean(change));
                break;
            default:
                throw new IllegalStateException("Cannot update " + descriptor.getArchetypeType() +
                                                ": unsupported field " + change.getField());
        }
    }

    /**
     * Updates a node descriptor.
     *
     * @param descriptor the node descriptor to update
     * @param change     the change to apply
     */
    private void updateNodeDescriptorField(NodeDescriptor descriptor, NodeFieldChange change) {
        switch (change.getField()) {
            case DESCRIPTION:
                descriptor.setDescription(toString(change));
                break;
            case DISPLAY_NAME:
                descriptor.setDisplayName(toString(change));
                break;
            case TYPE:
                descriptor.setType(toString(change));
                break;
            case BASE_NAME:
                descriptor.setBaseName(toString(change));
                break;
            case PATH:
                descriptor.setPath(toString(change));
                break;
            case PARENT_CHILD:
                descriptor.setParentChild(toBoolean(change));
                break;
            case MIN_LENGTH:
                descriptor.setMinLength(toInt(change));
                break;
            case MAX_LENGTH:
                descriptor.setMaxLength(toInt(change));
                break;
            case MIN_CARDINALITY:
                descriptor.setMinCardinality(toInt(change));
                break;
            case MAX_CARDINALITY:
                descriptor.setMaxCardinality(toInt(change, 1));
                break;
            case FILTER:
                descriptor.setFilter(toString(change));
                break;
            case INDEX:
                descriptor.setIndex(toInt(change));
                break;
            case DEFAULT_VALUE:
                descriptor.setDefaultValue(toString(change));
                break;
            case READ_ONLY:
                descriptor.setReadOnly(toBoolean(change));
                break;
            case HIDDEN:
                descriptor.setHidden(toBoolean(change));
                break;
            case DERIVED:
                descriptor.setDerived(toBoolean(change));
                break;
            case DERIVED_VALUE:
                descriptor.setDerivedValue(toString(change));
                break;
            case ASSERTION:
                updateAssertionDescriptor(descriptor, change);
                break;
            default:
                throw new IllegalStateException("Cannot update " + descriptor.getName() +
                                                ": unsupported field " + change.getField());
        }
    }

    /**
     * Updates an assertion descriptor on a node descriptor.
     *
     * @param descriptor the node descriptor to update
     * @param change     the assertion descriptor change to apply
     */
    private void updateAssertionDescriptor(NodeDescriptor descriptor, NodeFieldChange change) {
        if (change.isAdd()) {
            descriptor.addAssertionDescriptor((AssertionDescriptor) change.getNewVersion());
        } else if (change.isUpdate()) {
            AssertionDescriptor existing = (AssertionDescriptor) change.getOldVersion();
            AssertionDescriptor newVersion = (AssertionDescriptor) change.getNewVersion();
            existing.setIndex(newVersion.getIndex());
            existing.setErrorMessage(newVersion.getErrorMessage());
            for (NamedProperty property : new ArrayList<>(existing.getPropertyMap().getProperties().values())) {
                existing.removeProperty(property);
            }
            for (NamedProperty property : newVersion.getPropertyMap().getProperties().values()) {
                existing.addProperty(property);
            }
        } else {
            descriptor.removeAssertionDescriptor((AssertionDescriptor) change.getOldVersion());
        }
    }
}