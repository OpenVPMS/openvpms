/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.tools.archetype.loader;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptors;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptors;
import org.openvpms.component.business.domain.im.archetype.descriptor.DescriptorException;
import org.openvpms.component.business.domain.im.archetype.descriptor.DescriptorValidationError;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.DelegatingArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.tools.archetype.comparator.ArchetypeComparator;
import org.openvpms.tools.archetype.io.ArchetypeIOHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.tools.archetype.loader.ArchetypeLoaderException.ErrorCode.DirNotFound;
import static org.openvpms.tools.archetype.loader.ArchetypeLoaderException.ErrorCode.FileNotFound;
import static org.openvpms.tools.archetype.loader.ArchetypeLoaderException.ErrorCode.ValidationError;


/**
 * This utility will read all the archetypes from the specified directory
 * or file and load them in to the archetype service.
 * <p/>
 * When loading from a directory, all files with a <em>.adl</em> extension
 * will be processed.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
public class ArchetypeLoader {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The archetype comparator.
     */
    private final ArchetypeComparator comparator = new ArchetypeComparator();

    /**
     * Determines if verbose logging will be performed.
     */
    private boolean verbose;

    /**
     * The default name of the application context file.
     */
    private final static String APPLICATION_CONTEXT = "archetype-loader-context.xml";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ArchetypeLoader.class);

    /**
     * The archetype descriptor archetype.
     */
    private static final String ARCHETYPE_DESCRIPTOR = "descriptor.archetype";

    /**
     * All descriptor archetypes.
     */
    private static final String DESCRIPTORS = "descriptor.*";

    /**
     * All assertion archetypes.
     */
    private static final String ASSERTIONS = "assertion.*";

    /**
     * Constructs an {@link ArchetypeLoader}.
     *
     * @param service the archetype service
     */
    public ArchetypeLoader(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Determines if logging will be verbose.
     * <p/>
     * NOTE: logging will be performed via the <em>org.openvpms.tools.archetype.loader.ArchetypeLoader</em>
     * logging category, with INFO level.
     *
     * @param verbose if {@code true} perform verbose logging.
     */
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * Deletes all the archetype and assertion type descriptors.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    public void clean() {
        removeAssertionTypeDescriptors();
        removeArchetypeDescriptors();
    }

    /**
     * Loads assertions from the specified file.
     *
     * @param fileName the file name
     * @throws ArchetypeLoaderException if the assertions cannot be loaded
     */
    public void loadAssertions(String fileName) {
        if (verbose) {
            log.info("Processing assertion type descriptors from: {}", fileName);
        }
        try {
            loadAssertions(new FileInputStream(fileName));
        } catch (FileNotFoundException exception) {
            throw new ArchetypeLoaderException(FileNotFound, exception, fileName);
        }
    }

    /**
     * Loads all ADL files from the specified directory, optionally recursing sub-directories.
     *
     * @param dirName the directory
     * @param recurse if {@code true}, scan sub-directories
     * @throws ArchetypeLoaderException  if the directory cannot be found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DescriptorException       if a descriptor cannot be read
     */
    public void loadArchetypes(String dirName, boolean recurse) {
        File dir = new File(dirName);
        if (!dir.exists()) {
            throw new ArchetypeLoaderException(DirNotFound, dirName);
        }
        Collection<File> files = ArchetypeIOHelper.getArchetypeFiles(dir, recurse);
        Map<String, ArchetypeDescriptor> descriptors = new LinkedHashMap<>();
        for (File file : files) {
            readArchetypes(file, descriptors);
        }
        loadArchetypes(descriptors);
    }

    /**
     * Loads archetypes from a file.
     *
     * @param fileName the file name
     */
    public void loadArchetypes(String fileName) {
        Map<String, ArchetypeDescriptor> descriptors = new LinkedHashMap<>();
        readArchetypes(new File(fileName), descriptors);
        loadArchetypes(descriptors);
    }

    /**
     * Loads an assertion type descriptor.
     *
     * @param descriptor the descriptor to load
     * @return {@code true} if changes were made, otherwise {@code false}
     * @throws ArchetypeServiceException for any archetype service exception
     */
    public boolean loadAssertion(AssertionTypeDescriptor descriptor) {
        boolean result = false;
        if (verbose) {
            log.info("Processing assertion type descriptor: {}", descriptor.getName());
        }

        AssertionTypeDescriptor existing = (AssertionTypeDescriptor) getLatest(
                service.getAssertionTypeDescriptor(descriptor.getName()));
        if (existing != null) {
            AssertionTypeUpdater updater = new AssertionTypeUpdater();
            if (updater.update(existing, descriptor)) {
                save(existing);
                result = true;
            }
        } else {
            save(descriptor);
            result = true;
        }
        return result;
    }

    /**
     * Loads an archetype descriptor.
     *
     * @param descriptor the archetype descriptor to load
     * @return {@code true} if changes were made, otherwise {@code false}
     * @throws ArchetypeServiceException for any archetype service exception
     * @throws ArchetypeLoaderException  if the descriptor is invalid
     */
    public boolean loadArchetype(ArchetypeDescriptor descriptor) {
        validate(descriptor, null);
        return loadIfChanged(descriptor);
    }

    /**
     * Creates an {@link ArchetypeLoader} used to bootstrap archetypes for the first time.
     * <p/>
     * This relaxes validation for <em>descriptor.*</em> and <em>assertion.*</em> archetypes, which must exist
     * in order for validation to succeed.
     *
     * @param service the archetype service
     * @return a new {@link ArchetypeLoader}
     */
    public static ArchetypeLoader newBootstrapLoader(IArchetypeService service) {
        service = new DelegatingArchetypeService(service) {

            /**
             * Saves an object, executing any <em>save</em> rules associated with its archetype.
             *
             * @param object the object to save
             * @throws ArchetypeServiceException if the service cannot save the specified object
             * @throws ValidationException       if the object cannot be validated
             */
            @Override
            public void save(IMObject object) {
                // Disable validation for descriptor.* and assertion.* as there is a chicken and egg problem in that
                // some descriptors must exist in order to validate those same descriptors.
                if (!object.isA(DESCRIPTORS, ASSERTIONS)) {
                    super.save(object);
                } else {
                    // ideally, would register a custom validator to avoid using deprecated save. TODO
                    super.save(object, false);
                }
            }
        };
        return new ArchetypeLoader(service);
    }

    /**
     * Main line.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            JSAP parser = createParser();
            JSAPResult config = parser.parse(args);
            if (!config.success()) {
                displayUsage(parser, config);
            } else {
                String contextPath = config.getString("context");

                ApplicationContext context;
                if (!new File(contextPath).exists()) {
                    context = new ClassPathXmlApplicationContext(contextPath);
                } else {
                    context = new FileSystemXmlApplicationContext(contextPath);
                }

                IArchetypeService service = (IArchetypeService) context.getBean("archetypeService");
                ArchetypeLoader loader = ArchetypeLoader.newBootstrapLoader(service);
                String file = config.getString("file");
                String dir = config.getString("dir");
                boolean recurse = config.getBoolean("subdir");
                loader.setVerbose(config.getBoolean("verbose"));
                boolean clean = config.getBoolean("clean");
                String mappingFile = config.getString("mappingFile");
                int processed = 0;

                PlatformTransactionManager mgr = context.getBean(PlatformTransactionManager.class);
                TransactionStatus status = mgr.getTransaction(new DefaultTransactionDefinition());
                try {
                    if (clean) {
                        loader.clean();
                        ++processed;
                    }
                    if (mappingFile != null) {
                        loader.loadAssertions(mappingFile);
                        ++processed;
                    }
                    if (file != null) {
                        loader.loadArchetypes(file);
                        ++processed;
                    } else if (dir != null) {
                        loader.loadArchetypes(dir, recurse);
                        ++processed;
                    }
                    mgr.commit(status);
                    if (processed == 0) {
                        displayUsage(parser, config);
                    }
                } catch (Throwable throwable) {
                    log.error(throwable.getMessage(), throwable);
                    log.error("Rolling back changes");
                    mgr.rollback(status);
                }
            }
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
        }
    }

    /**
     * Saves a descriptor.
     *
     * @param descriptor the descriptor to save
     */
    private void save(IMObject descriptor) {
        if (verbose) {
            log.info("Saving {}", descriptor.getName());
        }
        service.save(descriptor);
    }

    /**
     * Loads archetypes descriptors.
     *
     * @param descriptors the descriptors to load, keyed on archetype
     */
    private void loadArchetypes(Map<String, ArchetypeDescriptor> descriptors) {
        List<String> archetypes = new ArrayList<>(descriptors.keySet());

        // process descriptor.* descriptors first, as these are required for subsequent validation
        for (String archetype : archetypes) {
            if (TypeHelper.isA(archetype, DESCRIPTORS)) {
                loadIfChanged(descriptors.remove(archetype));
            }
        }
        for (ArchetypeDescriptor descriptor : descriptors.values()) {
            loadIfChanged(descriptor);
        }
    }

    /**
     * Loads assertions from the specified stream.
     *
     * @param stream the stream to read
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DescriptorException       if the assertions cannot be read
     */
    private void loadAssertions(InputStream stream) {
        AssertionTypeDescriptors descriptors = AssertionTypeDescriptors.read(stream);
        for (AssertionTypeDescriptor descriptor : descriptors.getAssertionTypeDescriptors().values()) {
            loadAssertion(descriptor);
        }
    }

    /**
     * Loads the latest version of a descriptor to ensure the cached version is not being used.
     *
     * @param descriptor the descriptor. May be {@code null}
     * @return the descriptor. May be {@code null}
     */
    @SuppressWarnings("unchecked")
    private <T extends IMObject> T getLatest(T descriptor) {
        if (descriptor != null) {
            descriptor = (T) service.get(descriptor.getObjectReference());
        }
        return descriptor;
    }

    /**
     * Read archetype descriptors from a file.
     *
     * @param file          the file
     * @param descriptorMap collects the descriptors
     * @throws ArchetypeLoaderException  if the file doesn't exist or a descriptor is invalid
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DescriptorException       if the file cannot be read
     */
    private void readArchetypes(File file, Map<String, ArchetypeDescriptor> descriptorMap) {
        String name = file.getName();
        if (verbose) {
            log.info("Processing file: {}", file.getPath());
        }

        ArchetypeDescriptors descriptors;
        try {
            descriptors = ArchetypeDescriptors.read(new FileInputStream(file));
        } catch (FileNotFoundException exception) {
            throw new ArchetypeLoaderException(FileNotFound, exception, name);
        }
        for (ArchetypeDescriptor descriptor : descriptors.getArchetypeDescriptorsAsArray()) {
            validate(descriptor, name);
            if (descriptorMap.get(descriptor.getArchetypeType()) != null) {
                throw new ArchetypeLoaderException(ValidationError,
                                                   "Duplicate descriptor: " + descriptor.getArchetypeType()
                                                   + " loaded from " + file.getPath());
            } else {
                descriptorMap.put(descriptor.getArchetypeType(), descriptor);
            }
        }
    }

    /**
     * Performs a preliminary validation of a descriptor.
     *
     * @param descriptor the descriptor
     * @param fileName   the filename  where it came from. May be {@code null}
     */
    private void validate(ArchetypeDescriptor descriptor, String fileName) {
        // attempt to validate the archetype descriptor.
        List<DescriptorValidationError> validation
                = ((org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor) descriptor)
                .validate();
        if (!validation.isEmpty()) {
            StringBuilder builder = new StringBuilder("[Validation Error] ");
            if (fileName != null) {
                builder.append("[").append(fileName).append("]");
            }
            builder.append(" archetype ")
                    .append(descriptor.getName())
                    .append(" had ")
                    .append(validation.size())
                    .append(" errors.\n");
            for (DescriptorValidationError error : validation) {
                builder.append("\ttype:")
                        .append(error.getDescriptorType())
                        .append(" instance:")
                        .append(error.getInstanceName())
                        .append(" attribute:")
                        .append(error.getAttributeName())
                        .append(" error:")
                        .append(error.getError());
            }
            log.error(builder.toString());
            throw new ArchetypeLoaderException(ValidationError, builder.toString());
        }
    }

    /**
     * Loads an archetype descriptor if it is different that already stored, or if it doesn't yet exist.
     *
     * @param descriptor the archetype descriptor
     * @return {@code true} if changes were made, otherwise {@code false}
     * @throws ArchetypeLoaderException  if the descriptor is invalid
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DescriptorException       if the file cannot be read
     */
    private boolean loadIfChanged(ArchetypeDescriptor descriptor) {
        boolean result = false;
        if (verbose) {
            log.info("Processing archetype descriptor: {}", descriptor.getName());
        }

        // Compare the archetype with the both the cached and persistent instance.
        // If the database has been updated outside OpenVPMS the persistent instance may be different to that
        // cached.
        String archetype = descriptor.getArchetypeType();
        ArchetypeDescriptor persistent = getPersistent(descriptor);
        ArchetypeDescriptor cached = service.getArchetypeDescriptor(archetype);
        if (persistent != null && cached != null
            && (persistent.getId() != cached.getId() || comparator.compare(persistent, cached) != null)) {
            // the database has been updated outside of OpenVPMS, so force the cached instance to refresh.
            // NOTE that this only occurs on transaction commit.
            service.save(persistent);
            cached = null;
        }
        if (persistent != null || cached != null) {
            // only replace the archetype if it hasn't changed
            ArchetypeUpdater updater = new ArchetypeUpdater();
            if (persistent != null && updater.update(persistent, descriptor)) {
                save(persistent);
                result = true;
            } else if (cached != null && updater.update(cached, descriptor)) {
                save(cached);
                result = true;
            }
        } else {
            // archetype doesn't exist
            save(descriptor);
            result = true;
        }
        if (verbose && !result) {
            log.info("Unchanged: {}", descriptor.getName());
        }
        return result;
    }

    /**
     * Returns the persistent archetype descriptor corresponding to that supplied.
     *
     * @param descriptor the archetype descriptor
     * @return the persistent version, or {@code null} if none is found
     */
    private ArchetypeDescriptor getPersistent(ArchetypeDescriptor descriptor) {
        ArchetypeDescriptor result = null;
        if (service.getArchetypeDescriptor(ARCHETYPE_DESCRIPTOR) != null) {
            // only proceed if the descriptor.archetype has been loaded, else the query will fail
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<ArchetypeDescriptor> query = builder.createQuery(ArchetypeDescriptor.class);
            Root<ArchetypeDescriptor> root = query.from(ArchetypeDescriptor.class, ARCHETYPE_DESCRIPTOR);
            query.where(builder.equal(root.get("name"), descriptor.getName()));
            result = service.createQuery(query).getFirstResult();
        }
        return result;
    }

    /**
     * Creates a new command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("dir")
                                         .setShortFlag('d')
                                         .setLongFlag("dir")
                                         .setHelp("Directory where ADL files reside."));
        parser.registerParameter(new Switch("subdir")
                                         .setShortFlag('s')
                                         .setLongFlag("subdir")
                                         .setDefault("false")
                                         .setHelp("Search the subdirectories as well."));
        parser.registerParameter(new FlaggedOption("file")
                                         .setShortFlag('f')
                                         .setLongFlag("file")
                                         .setHelp("Name of file containing archetypes"));
        parser.registerParameter(new Switch("verbose")
                                         .setShortFlag('v')
                                         .setLongFlag("verbose")
                                         .setDefault("false")
                                         .setHelp("Displays verbose info to the console."));
        parser.registerParameter(new Switch("overwrite")
                                         .setShortFlag('o')
                                         .setLongFlag("overwrite")
                                         .setDefault("false")
                                         .setHelp("Overwrite archetype if it already exists"));
        parser.registerParameter(new Switch("clean")
                                         .setShortFlag('c')
                                         .setLongFlag("clean")
                                         .setDefault("false")
                                         .setHelp("Clean all the archetypes before loading"));
        parser.registerParameter(new FlaggedOption("context")
                                         .setLongFlag("context")
                                         .setDefault(APPLICATION_CONTEXT)
                                         .setHelp("The application context path"));
        parser.registerParameter(new FlaggedOption("mappingFile")
                                         .setShortFlag('m')
                                         .setLongFlag("mappingFile")
                                         .setHelp("A location of the assertion type mapping file"));
        return parser;
    }

    /**
     * Prints usage information and exits.
     *
     * @param parser the parser
     * @param result the parse result
     */
    private static void displayUsage(JSAP parser, JSAPResult result) {
        Iterator<?> iter = result.getErrorMessageIterator();
        while (iter.hasNext()) {
            System.err.println(iter.next());
        }
        System.err.println();
        System.err.println("Usage: java " + ArchetypeLoader.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.exit(1);
    }

    /**
     * Removes all archetype descriptors.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void removeArchetypeDescriptors() {
        List<org.openvpms.component.model.archetype.ArchetypeDescriptor> descriptors
                = service.getArchetypeDescriptors();
        for (org.openvpms.component.model.archetype.ArchetypeDescriptor descriptor : descriptors) {
            if (verbose) {
                log.info("Deleting {}", descriptor.getName());
            }
            service.remove(descriptor);
        }
    }

    /**
     * Removes all assertion type descriptors.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void removeAssertionTypeDescriptors() {
        List<org.openvpms.component.model.archetype.AssertionTypeDescriptor> descriptors
                = service.getAssertionTypeDescriptors();
        for (org.openvpms.component.model.archetype.AssertionTypeDescriptor descriptor : descriptors) {
            if (verbose) {
                log.info("Deleting {}", descriptor.getName());
            }
            service.remove(descriptor);
        }
    }
}
