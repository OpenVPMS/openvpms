/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.common;

import org.openvpms.component.business.dao.hibernate.interceptor.IMObjectInterceptor;
import org.openvpms.component.business.domain.im.common.AuditableIMObject;

/**
 * An {@link Assembler} responsible for assembling {@link AuditableIMObjectDO} instances from {@link AuditableIMObject}s
 * and vice-versa.
 * <p/>
 * Note that this does not populate the create/update information on an {@link AuditableIMObjectDO} from an
 * {@link AuditableIMObject}. This is handled by {@link IMObjectInterceptor}.
 *
 * @author Tim Anderson
 */
public abstract class AuditableIMObjectAssembler<T extends AuditableIMObject, DO extends AuditableIMObjectDO>
        extends IMObjectAssembler<T, DO> {

    /**
     * Constructs an {@link AuditableIMObjectAssembler}.
     *
     * @param type     the object type, or {@code null} if the implementation type has no corresponding interface
     * @param typeImpl the object type implementation
     * @param typeDO   the data object interface type
     * @param implDO   the data object implementation type
     */
    protected AuditableIMObjectAssembler(Class<? extends org.openvpms.component.model.object.IMObject> type,
                                         Class<T> typeImpl, Class<DO> typeDO, Class<? extends IMObjectDOImpl> implDO) {
        super(type, typeImpl, typeDO, implDO);
    }

    /**
     * Assembles an object from a data object.
     *
     * @param target  the object to assemble
     * @param source  the object to assemble from
     * @param context the assembly context
     */
    @Override
    protected void assembleObject(T target, DO source, Context context) {
        super.assembleObject(target, source, context);
        target.setCreated(source.getCreated());
        assembleCreatedByRef(target, source, context);
        target.setUpdated(source.getUpdated());
        assembleUpdatedByRef(target, source, context);
    }

}
