/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.party;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.party.Party;

/**
 * A {@link PartyDecorator} that wraps the entity in an {@link IMObjectBean}.
 *
 * @author Tim Anderson
 */
public class BeanPartyDecorator extends PartyDecorator {

    /**
     * The bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link BeanPartyDecorator}.
     *
     * @param peer    the peer to delegate to
     * @param factory the bean factory
     */
    public BeanPartyDecorator(Party peer, IMObjectBeanFactory factory) {
        super(peer);
        bean = factory.getBean(peer);
    }

    /**
     * Constructs a {@link BeanPartyDecorator}.
     *
     * @param bean the bean wrapping the party
     */
    public BeanPartyDecorator(IMObjectBean bean) {
        super(bean.getObject(Party.class));
        this.bean = bean;
    }

    /**
     * Returns the bean.
     *
     * @return the bean
     */
    public IMObjectBean getBean() {
        return bean;
    }
}