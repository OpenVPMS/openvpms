/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.util.StringUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * An {@code AccessDecisionManager} used to determine if a user can create, save or remove instances of an archetype.
 *
 * @author Tim Anderson
 * @see ArchetypeAwareGrantedAuthority
 * @see OpenVPMSMethodSecurityInterceptor
 */
public class ArchetypeAccessDecisionManager implements AccessDecisionManager {

    /**
     * Archetype service name prefix.
     */
    private final static String ARCHETYPE_PREFIX = "archetypeService";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ArchetypeAccessDecisionManager.class);

    /**
     * Resolves an access control decision for the passed parameters.
     *
     * @param authentication the caller invoking the method (not null)
     * @param object         the secured object being called
     * @param attributes     the configuration attributes associated with the secured
     *                       object being invoked
     * @throws AccessDeniedException               if access is denied as the authentication does not
     *                                             hold a required authority or ACL privilege
     * @throws InsufficientAuthenticationException if access is denied as the
     *                                             authentication does not provide a sufficient level of trust
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> attributes)
            throws AccessDeniedException, InsufficientAuthenticationException {
        boolean checked = false;

        String[] archetypes = getArchetypes((MethodInvocation) object);
        for (ConfigAttribute attribute : attributes) {
            if (supports(attribute)) {
                StringTokenizer tokens = new StringTokenizer(attribute.getAttribute(), ".");
                String serviceName = tokens.hasMoreTokens() ? tokens.nextToken() : null;
                String method = tokens.hasMoreTokens() ? tokens.nextToken() : null;
                if (serviceName != null && method != null) {
                    check(archetypes, authentication, serviceName, method);
                    checked = true;
                } else {
                    throw new AccessDeniedException("Unsupported attribute: " + attribute);
                }
            } else {
                throw new AccessDeniedException("Unsupported attribute: " + attribute);
            }
        }
        if (!checked) {
            throw new AccessDeniedException("Access is denied");
        }
    }

    /**
     * Indicates whether this {@code AccessDecisionManager} is able to process
     * authorization requests presented with the passed {@code ConfigAttribute}.
     * <p>
     * This allows the {@code AbstractSecurityInterceptor} to check every
     * configuration attribute can be consumed by the configured
     * {@code AccessDecisionManager} and/or {@code RunAsManager} and/or
     * {@code AfterInvocationManager}.
     * </p>
     *
     * @param attribute a configuration attribute that has been configured against the
     *                  {@code AbstractSecurityInterceptor}
     * @return true if this {@code AccessDecisionManager} can support the passed
     * configuration attribute
     */
    @Override
    public boolean supports(ConfigAttribute attribute) {
        String a = attribute.getAttribute();
        return (a != null) && a.startsWith(ARCHETYPE_PREFIX);
    }

    /**
     * Indicates whether the {@code AccessDecisionManager} implementation is able to
     * provide access control decisions for the indicated secured object type.
     *
     * @param clazz the class that is being queried
     * @return {@code true} if the implementation can process the indicated class
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == MethodInvocation.class;
    }

    /**
     * Verifies that access is granted to each archetype.
     *
     * @param archetypes     the archetype short names
     * @param authentication the authentication
     * @param serviceName    the service name
     * @param method         the method
     * @throws AccessDeniedException if access is denied
     */
    private void check(String[] archetypes, Authentication authentication, String serviceName, String method) {
        for (String archetype : archetypes) {
            boolean granted = false;
            // Attempt to find a matching granted authority
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                if (authority instanceof ArchetypeAwareGrantedAuthority
                    && isAccessGranted((ArchetypeAwareGrantedAuthority) authority, archetype, serviceName, method)) {
                    granted = true;
                    break;
                }
            }
            if (!granted) {
                if (log.isWarnEnabled()) {
                    log.warn("Access denied to principal=" + authentication.getPrincipal() + ", operation="
                             + method + ", archetype=" + archetype);
                }
                throw new ArchetypeAccessDeniedException(archetype, method);
            }
        }
    }

    /**
     * Determine if the access will be granted for the specified service on the
     * specifoed archetype given an authority
     *
     * @param authority   the authority to test against
     * @param archetype   the archetype
     * @param serviceName the service name
     * @param method      the method
     * @return {@code true} if access is granted
     */
    private boolean isAccessGranted(ArchetypeAwareGrantedAuthority authority, String archetype, String serviceName,
                                    String method) {
        if (!serviceName.equals(authority.getServiceName())) {
            return false;
        }
        String authMethod = authority.getMethod();
        String authShortName = authority.getShortName();
        return !StringUtils.isEmpty(authMethod) &&
               !StringUtils.isEmpty(authShortName) &&
               StringUtilities.matches(method, authMethod) &&
               StringUtilities.matches(archetype, authShortName);
    }

    /**
     * Examine the method and return the archetypes.
     *
     * @param method the method to examine
     * @return the archetypes
     */
    @SuppressWarnings("unchecked")
    private String[] getArchetypes(MethodInvocation method) {
        String[] result = {};
        if (method != null) {
            String methodName = method.getMethod().getName();
            Class<?> declaring = method.getMethod().getDeclaringClass();
            if (declaring.equals(IArchetypeService.class)) {
                Object arg = method.getArguments()[0];
                switch (methodName) {
                    case "create":
                        if (arg instanceof String) {
                            result = new String[]{(String) arg};
                        } else {
                            result = new String[]{((ArchetypeId) arg).getShortName()};
                        }
                        break;
                    case "save":
                        if (arg instanceof IMObject) {
                            IMObject object = (IMObject) arg;
                            result = new String[]{object.getArchetype()};
                        } else {
                            Collection<IMObject> objects = (Collection<IMObject>) arg;
                            Set<String> archetypes = new HashSet<>();
                            for (IMObject object : objects) {
                                archetypes.add(object.getArchetype());
                            }
                            result = archetypes.toArray(new String[0]);
                        }
                        break;
                    case "remove":
                        if (arg instanceof IMObject) {
                            result = new String[]{((IMObject) arg).getArchetype()};
                        } else {
                            result = new String[]{((Reference) arg).getArchetype()};
                        }
                        break;
                    default:
                        // we need to handle the other methods here
                        break;
                }
            }
        }
        return result;
    }

}
