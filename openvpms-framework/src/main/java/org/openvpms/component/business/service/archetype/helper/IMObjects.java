/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

/**
 * Provides access to {@link IMObject} instances, by reference.
 *
 * @author Tim Anderson
 */
public interface IMObjects {

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @return the corresponding object, or {@code null} if none is found
     */
    IMObject get(Reference reference);

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    <T extends IMObject> T get(Reference reference, Class<T> type);
}