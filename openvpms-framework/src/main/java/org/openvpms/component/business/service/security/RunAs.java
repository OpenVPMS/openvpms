/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.openvpms.component.model.user.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Helper methods to invoke functions within a spring {@code SecurityContext} for a particular user.
 *
 * @author Tim Anderson
 */
public class RunAs {

    /**
     * Executes a {@code Runnable} with the spring {@code SecurityContext} set to that of the supplied user.
     *
     * @param user     the user
     * @param runnable the runnable to execute
     */
    public static void run(User user, Runnable runnable) {
        SecurityContext context = createContext(user);
        DelegatingRunnable delegator = new DelegatingRunnable(context, runnable);
        delegator.run();
    }

    /**
     * Executes a {@code Callable} with the spring {@code SecurityContext} set to that of the supplied user.
     *
     * @param user     the user
     * @param callable the callable to execute
     */
    public static <T> T run(User user, Callable<T> callable) throws Exception {
        SecurityContext context = createContext(user);
        DelegatingCallable<T> delegator = new DelegatingCallable<>(context, callable);
        return delegator.call();
    }

    /**
     * Executes a {@code Supplier} with the spring {@code SecurityContext} set to that of the supplied user.
     *
     * @param user     the user
     * @param supplier the supplier to execute
     */
    public static <T> T call(User user, Supplier<T> supplier) {
        SecurityContext context = createContext(user);
        DelegatingSupplier<T> delegator = new DelegatingSupplier<>(context, supplier);
        return delegator.get();
    }

    /**
     * Creates a {@code Supplier} that inherits the spring {@code SecurityContext} of the current thread, for execution
     * in a different thread.
     * <p/>
     * This restores the security context after execution.
     *
     * @param supplier the supplier to delegate to
     * @return a new supplier
     */
    public static <T> Supplier<T> inheritSecurityContext(Supplier<T> supplier) {
        return new DelegatingSupplier<>(SecurityContextHolder.getContext(), supplier);
    }

    /**
     * Creates a {@code Consumer} that inherits the spring {@code SecurityContext} of the current thread, for execution
     * in a different thread.
     * <p/>
     * This restores the security context after execution.
     *
     * @param consumer the consumer to delegate to
     * @return a new consumer
     */
    public static <T> Consumer<T> inheritSecurityContext(Consumer<T> consumer) {
        return new DelegatingConsumer<>(SecurityContextHolder.getContext(), consumer);
    }

    /**
     * Creates a {@code Runnable} that inherits the spring {@code SecurityContext} of the current thread, for execution
     * in a different thread.
     * <p/>
     * This restores the security context after execution.
     *
     * @param runnable the runnable to delegate to
     * @return a new runnable
     */
    public static Runnable inheritSecurityContext(Runnable runnable) {
        return new DelegatingRunnable(SecurityContextHolder.getContext(), runnable);
    }

    /**
     * Creates a security context for the specified user.
     *
     * @param user the user
     * @return a new security context
     */
    private static SecurityContext createContext(User user) {
        org.openvpms.component.business.domain.im.security.User u
                = (org.openvpms.component.business.domain.im.security.User) user;
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        UsernamePasswordAuthenticationToken authentication
                = new UsernamePasswordAuthenticationToken(user, u.getPassword(), u.getAuthorities());
        context.setAuthentication(authentication);
        return context;
    }

    /**
     * Helper to run statements with the appropriate security context.
     */
    private abstract static class Runner {

        /**
         * The security context to use.
         */
        private final SecurityContext context;

        /**
         * Constructs a {@link Runner}
         *
         * @param context the security context. May be {@code null}
         */
        public Runner(SecurityContext context) {
            this.context = context;
        }

        /**
         * Initialises the security context.
         *
         * @return the original security context
         */
        protected SecurityContext init() {
            SecurityContext current = SecurityContextHolder.getContext();
            if (context != null) {
                SecurityContextHolder.setContext(context);
            } else {
                SecurityContextHolder.clearContext();
            }
            return current;
        }

        /**
         * Restores the security context.
         *
         * @param original the security context
         */
        protected void restore(SecurityContext original) {
            SecurityContextHolder.setContext(original);
        }
    }

    /**
     * A {@link Supplier} that sets the security context before delegating to the supplied {@code Supplier}.
     * <p/>
     * The security context is restored on completion.
     */
    private static class DelegatingSupplier<T> extends Runner implements Supplier<T> {

        private final Supplier<T> supplier;

        public DelegatingSupplier(SecurityContext context, Supplier<T> supplier) {
            super(context);
            this.supplier = supplier;
        }

        @Override
        public T get() {
            SecurityContext original = init();
            try {
                return supplier.get();
            } finally {
                restore(original);
            }
        }
    }

    /**
     * A {@link Consumer} that sets the security context before delegating to the supplied {@code Consumer}.
     * <p/>
     * The security context is restored on completion.
     */
    private static class DelegatingConsumer<T> extends Runner implements Consumer<T> {

        /**
         * The consumer to run.
         */
        private final Consumer<T> consumer;

        /**
         * Constructs a {@link DelegatingConsumer}.
         *
         * @param context  the security context to run the consumer in
         * @param consumer the consumer to run
         */
        public DelegatingConsumer(SecurityContext context, Consumer<T> consumer) {
            super(context);
            this.consumer = consumer;
        }

        /**
         * Performs this operation on the given argument.
         *
         * @param t the input argument
         */
        @Override
        public void accept(T t) {
            SecurityContext original = init();
            try {
                consumer.accept(t);
            } finally {
                restore(original);
            }
        }
    }

    /**
     * A {@link Runnable} that sets the security context before delegating to the supplied {@code Runnable}.
     * <p/>
     * The security context is restored on completion.
     */
    private static class DelegatingRunnable implements Runnable {

        /**
         * The supplier to delegate to.
         */
        private final Supplier<Object> supplier;

        /**
         * Constructs a {@link DelegatingRunnable}.
         *
         * @param context  the security context to run the consumer in
         * @param runnable the runnable to run
         */
        public DelegatingRunnable(SecurityContext context, Runnable runnable) {
            supplier = new DelegatingSupplier<>(context, () -> {
                runnable.run();
                return null;
            });
        }

        @Override
        public void run() {
            supplier.get();
        }
    }

    /**
     * A {@link Callable} that sets the security context before delegating to the supplied {@code Callable}.
     * <p/>
     * The security context is restored on completion.
     */
    private static class DelegatingCallable<T> extends Runner implements Callable<T> {

        /**
         * The callable to delegate to.
         */
        private final Callable<T> callable;

        /**
         * Constructs a {@link DelegatingCallable}.
         *
         * @param context  the security context to run the callable in
         * @param callable the callable to run
         */
        public DelegatingCallable(SecurityContext context, Callable<T> callable) {
            super(context);
            ;
            this.callable = callable;
        }

        @Override
        public T call() throws Exception {
            SecurityContext original = init();
            try {
                return callable.call();
            } finally {
                restore(original);
            }
        }
    }
}
