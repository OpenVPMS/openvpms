/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;


/**
 * Abstract implementation of the {@link IMObjectCopyHandler} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectCopyHandler implements IMObjectCopyHandler {

    /**
     * Determines how {@link IMObjectCopier} should treat an object. This
     * implementation always returns a new instance, of the same archetype as
     * {@code object}.
     *
     * @param object  the source object
     * @param service the archetype service
     * @return {@code object} if the object shouldn't be copied,
     * {@code null} if it should be replaced with {@code null},
     * or a new instance if the object should be copied
     */
    public IMObject getObject(IMObject object, ArchetypeService service) {
        return service.create(object.getArchetype(), IMObject.class);
    }

    /**
     * Determines how a node should be copied.
     *
     * @param source     the source archetype
     * @param sourceNode the source node
     * @param target     the target archetype
     * @return a node to copy sourceNode to, or {@code null} if the node shouldn't be copied
     */
    public NodeDescriptor getNode(ArchetypeDescriptor source,
                                  NodeDescriptor sourceNode,
                                  ArchetypeDescriptor target) {
        NodeDescriptor result = null;
        if (isCopyable(source, sourceNode, true)) {
            result = getTargetNode(source, sourceNode, target);
        }
        return result;
    }

    /**
     * Returns a target node for a given source node.
     *
     * @param source     the source archetype
     * @param sourceNode the source node
     * @param target     the target archetype
     * @return a node to copy source to, or {@code null} if the node
     * shouldn't be copied
     */
    protected NodeDescriptor getTargetNode(ArchetypeDescriptor source,
                                           NodeDescriptor sourceNode,
                                           ArchetypeDescriptor target) {
        NodeDescriptor result = null;
        NodeDescriptor desc = target.getNodeDescriptor(sourceNode.getName());
        if (desc != null && isCopyable(target, desc, false)) {
            result = desc;
        }
        return result;
    }

    /**
     * Helper to determine if a node is copyable.
     * </p>
     * This implementation excludes <em>id</em> nodes, and derived nodes
     * where the node is the target.
     *
     * @param archetype the archetype descriptor
     * @param node      the node descriptor
     * @param source    if {@code true} the node is the source; otherwise its
     *                  the target
     * @return {@code true} if the node is copyable; otherwise {@code false}
     */
    protected boolean isCopyable(ArchetypeDescriptor archetype, NodeDescriptor node, boolean source) {
        String name = node.getName();
        return !"id".equals(name) && !"created".equals(name) && !"updated".equals(name) && !"createdBy".equals(name)
               && !"updatedBy".equals(name) && !node.isDerived();
    }
}
