/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.interceptor;

import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Interceptor;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;
import org.openvpms.component.business.dao.hibernate.cache.ArchetypeIdCache;
import org.openvpms.component.business.dao.hibernate.im.common.AuditableIMObjectDO;
import org.openvpms.component.business.dao.hibernate.im.common.IMObjectAssembler;
import org.openvpms.component.business.dao.hibernate.im.security.UserDO;
import org.openvpms.component.business.dao.hibernate.im.security.UserDOImpl;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.user.User;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Objects;

/**
 * An {@link Interceptor} that:
 * <ul>
 *     <li>populates {@link AuditableIMObject}s</li>
 *     <li>replaces {@link ArchetypeId} instances with those cached by an {@link ArchetypeIdCache},
 *  to avoid proliferation of {@link ArchetypeId} instances.</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class IMObjectInterceptor extends EmptyInterceptor {

    /**
     * The authentication context.
     */
    private final transient AuthenticationContext context;

    /**
     * The ArchetypeId cache.
     */
    private final transient ArchetypeIdCache cache;

    /**
     * The session factory.
     */
    private SessionFactory factory;

    /**
     * Created field.
     */
    static final String CREATED = "created";

    /**
     * Created-by field.
     */
    static final String CREATED_BY = "createdBy";

    /**
     * Updated field.
     */
    static final String UPDATED = "updated";

    /**
     * Updated-by field.
     */
    static final String UPDATED_BY = "updatedBy";

    /**
     * Constructs an {@link IMObjectInterceptor}.
     *
     * @param context the authentication context
     */
    public IMObjectInterceptor(AuthenticationContext context) {
        this(context, new ArchetypeIdCache());
    }

    /**
     * Constructs an {@link IMObjectInterceptor}.
     *
     * @param context the authentication context
     * @param cache   the ArchetypeId cache to use
     */
    public IMObjectInterceptor(AuthenticationContext context, ArchetypeIdCache cache) {
        this.context = context;
        this.cache = cache;
    }

    /**
     * Sets the session factory.
     *
     * @param factory the session factory
     */
    @Resource
    public void setSessionFactory(SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Called just before an object is initialized. The interceptor may change the {@code state}, which will
     * be propagated to the persistent object. Note that when this method is called, {@code entity} will be
     * an empty uninitialized instance of the class.
     *
     * @param entity        The entity instance being loaded
     * @param id            The identifier value being loaded
     * @param state         The entity state (which will be pushed into the entity instance)
     * @param propertyNames The names of the entity properties, corresponding to the {@code state}.
     * @param types         The types of the entity properties, corresponding to the {@code state}.
     * @return {@code true} if the user modified the {@code state} in any way.
     */
    @Override
    public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        boolean modified = false;
        for (int i = 0; i < state.length; ++i) {
            Object object = state[i];
            if (object instanceof ArchetypeId) {
                state[i] = cache.get((ArchetypeId) object);
                modified = true;
            }
        }
        return modified;
    }

    /**
     * Called when an object is detected to be dirty, during a flush. The interceptor may modify the detected
     * {@code currentState}, which will be propagated to both the database and the persistent object.
     * Note that not all flushes end in actual synchronization with the database, in which case the
     * new {@code currentState} will be propagated to the object, but not necessarily (immediately) to
     * the database. It is strongly recommended that the interceptor <b>not</b> modify the {@code previousState}.
     * <p/>
     * NOTE: The indexes across the {@code currentState}, {@code previousState}, {@code propertyNames} and
     * {@code types} arrays match.
     *
     * @param entity        The entity instance detected as being dirty and being flushed
     * @param id            The identifier of the entity
     * @param currentState  The entity's current state
     * @param previousState The entity's previous (load time) state.
     * @param propertyNames The names of the entity properties
     * @param types         The types of the entity properties
     * @return {@code true} if the user modified the {@code currentState} in any way.
     */
    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
                                String[] propertyNames, Type[] types) {
        boolean modified = false;
        if (entity instanceof AuditableIMObjectDO) {
            modified = populate(UPDATED, currentState, propertyNames, IMObjectAssembler.getTimestampNow(), true);
            modified |= populate(UPDATED_BY, currentState, propertyNames, getUser(), false);
            // if there is no current user, don't remove any existing one
        }
        return modified;
    }

    /**
     * Called before an object is saved. The interceptor may modify the {@code state}, which will be used for
     * the SQL {@code INSERT} and propagated to the persistent object.
     *
     * @param entity        The entity instance whose state is being inserted
     * @param id            The identifier of the entity
     * @param state         The state of the entity which will be inserted
     * @param propertyNames The names of the entity properties.
     * @param types         The types of the entity properties
     * @return {@code true} if the user modified the {@code state} in any way.
     */
    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        boolean modified = false;
        if (entity instanceof AuditableIMObjectDO) {
            modified = populate(CREATED, state, propertyNames, IMObjectAssembler.getTimestampNow(), true);
            modified |= populate(CREATED_BY, state, propertyNames, getUser(), true);
            modified |= populate(UPDATED, state, propertyNames, null, true);
            modified |= populate(UPDATED_BY, state, propertyNames, null, true);
        }
        return modified;
    }

    /**
     * Returns the persistent instance of a user.
     *
     * @return the instance
     */
    protected UserDO resolve(User user) {
        return factory.getCurrentSession().load(UserDOImpl.class, user.getId());
    }

    /**
     * Populates a property.
     *
     * @param name          the property name
     * @param state         The state of the entity which will be inserted/updated
     * @param propertyNames The names of the entity properties.
     * @param value         the value. May be {@code null}
     * @param useNull       if {@code true}, and the value is null, use it, otherwise leave the existing value
     * @return {@code true} if the property was updated
     */
    private boolean populate(String name, Object[] state, String[] propertyNames, Object value, boolean useNull) {
        int index = ArrayUtils.indexOf(propertyNames, name);
        if (index != -1 && ((value != null || useNull) && !Objects.equals(value, state[index]))) {
            state[index] = value;
            return true;
        }
        return false;
    }

    /**
     * Returns the current user.
     * <p/>
     * This ignores any non-persistent user.
     *
     * @return the current user, or {@code null} if there is none
     */
    private UserDO getUser() {
        UserDO result = null;
        User user = context.getUser();
        if (user != null && !user.isNew()) {
            result = resolve(user);
        }
        return result;
    }

}
