/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.resources.Messages;
import org.openvpms.component.exception.OpenVPMSException;


/**
 * Exception class for exceptions raised by {@link IMObjectCopier}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class IMObjectCopierException extends OpenVPMSException {

    /**
     * An enumeration of error codes.
     */
    public enum ErrorCode {

        ObjectNotFound
    }

    /**
     * The error code.
     */
    private final ErrorCode errorCode;

    /**
     * Serialization version identifier.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The messages.
     */
    private static final Messages messages = Messages.getMessages(
            "org.openvpms.component.business.service.archetype.helper.errmessages");

    /**
     * Constructs an {@link IMObjectCopierException}.
     *
     * @param errorCode the error code
     * @param args      message arguments
     */
    public IMObjectCopierException(ErrorCode errorCode, Object... args) {
        super(org.openvpms.component.i18n.Messages.create(messages.getMessage(errorCode.toString(), args)));
        this.errorCode = errorCode;
    }

    /**
     * Returns the error code.
     *
     * @return the error code
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }

}