/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.im.plugin;

import org.openvpms.component.business.domain.im.plugin.Plugin;

import java.io.InputStream;
import java.util.Iterator;

/**
 * Plugin data access object.
 * <p/>
 * Note that will {@link Plugin} instances are also available via the ArchetypeService, this must be used to manage
 * the binary associated with a plugin.
 *
 * @author Tim Anderson
 */
public interface PluginDAO {

    /**
     * Returns the plugin with the given key.
     *
     * @param key the plugin key
     * @return the corresponding plugin, or {@code null} if none is found
     */
    Plugin getPlugin(String key);

    /**
     * Returns all active plugins.
     *
     * @return the active plugins
     */
    Iterator<Plugin> getPlugins();

    /**
     * Returns a stream to the binary for a plugin.
     *
     * @param key the plugin key
     * @return the stream, or {@code null} if the plugin was not found
     */
    InputStream getBinary(String key);

    /**
     * Saves a plugin.
     *
     * @param key    the plugin key
     * @param name   the plugin name
     * @param stream stream to the plugin binary
     */
    void save(String key, String name, InputStream stream);

    /**
     * Removes the plugin with the given key.
     *
     * @param key the plugin key
     * @return {@code true} if the plugin was removed, {@code false} if it doesn't exist
     */
    boolean remove(String key);

}
