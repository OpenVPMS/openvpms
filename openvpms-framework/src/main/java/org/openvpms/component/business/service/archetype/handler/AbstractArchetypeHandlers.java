/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.handler;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Collection;


/**
 * Provides a mapping between archetype short names and the classes that can
 * handle them.
 *
 * @author Tim Anderson
 */
public abstract class AbstractArchetypeHandlers<T> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link AbstractArchetypeHandlers}.
     *
     * @param service the archetype service
     */
    protected AbstractArchetypeHandlers(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns a handler that can handle an archetype.
     *
     * @param shortName the archetype short name
     * @return an implementation that supports {@code shortName} or {@code null} if there is no match
     */
    public abstract ArchetypeHandler<T> getHandler(String shortName);

    /**
     * Finds the most specific wildcarded short name from a collection of wildcarded short names that matches the
     * supplied short name.
     *
     * @param shortName the shortName
     * @param wildcards the wildcarded short names
     * @return the most specific short name, or {@code null} if none can be found
     */
    protected String getShortName(String shortName, Collection<String> wildcards) {
        return getShortName(new String[]{shortName}, wildcards);
    }

    /**
     * Finds the most specific wildcarded short name from a collection of
     * wildcarded short names that matches the supplied short names.
     *
     * @param shortNames the short names
     * @param wildcards  the wildcarded short names
     * @return the most specific short name, or {@code null} if none can be found
     */
    protected String getShortName(String[] shortNames, Collection<String> wildcards) {
        String match = null;
        int bestDotCount = -1;        // more dots in a short name, the more specific
        int bestWildCardCount = -1;   // less wildcards, the more specific
        int bestMatchCount = -1;      // fewer matches, the more specific
        for (String wildcard : wildcards) {
            if (TypeHelper.matches(shortNames, wildcard)) {
                if (match == null) {
                    match = wildcard;
                    bestDotCount = StringUtils.countMatches(wildcard, ".");
                    bestWildCardCount = StringUtils.countMatches(wildcard, "*");
                    String[] matches = getShortNames(wildcard, false);
                    bestMatchCount = matches.length;
                } else {
                    int dotCount = StringUtils.countMatches(wildcard, ".");
                    int wildcardCount = StringUtils.countMatches(wildcard, "*");
                    if (dotCount > bestDotCount || (dotCount == bestDotCount && wildcardCount < bestWildCardCount)) {
                        bestDotCount = dotCount;
                        bestWildCardCount = wildcardCount;
                        match = wildcard;
                    } else {
                        String[] wildcardMatches = getShortNames(wildcard, false);
                        int matchCount = wildcardMatches.length;
                        if (matchCount < bestMatchCount) {
                            bestMatchCount = matchCount;
                            match = wildcard;
                        }
                    }
                }
            }
        }
        return match;
    }

    /**
     * Returns primary archetype short names matching the specified criteria.
     *
     * @param shortName the short name. May contain wildcards
     * @return a list of short names matching the criteria
     */
    protected String[] getShortNames(String shortName) {
        return DescriptorHelper.getShortNames(shortName, service);
    }

    /**
     * Returns archetype short names matching the specified criteria.
     *
     * @param shortName   the short name. May contain wildcards
     * @param primaryOnly if {@code true} only include primary archetypes
     * @return a list of short names matching the criteria
     */
    protected String[] getShortNames(String shortName, boolean primaryOnly) {
        return DescriptorHelper.getShortNames(shortName, primaryOnly, service);
    }


}
