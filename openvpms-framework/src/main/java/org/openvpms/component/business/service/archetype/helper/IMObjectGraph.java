/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.model.object.IMObject;

import java.util.Set;

/**
 * Provides access to an {@link IMObject} and its related objects.
 *
 * @author Tim Anderson
 */
public interface IMObjectGraph extends IMObjects {

    /**
     * Returns the primary object.
     *
     * @return the primary object
     */
    IMObject getPrimary();

    /**
     * Returns the primary object.
     *
     * @param type the expected type of the object
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    <T> T getPrimary(Class<T> type);

    /**
     * Returns the objects in the graph.
     *
     * @return the objects
     */
    Set<IMObject> getObjects();

}
