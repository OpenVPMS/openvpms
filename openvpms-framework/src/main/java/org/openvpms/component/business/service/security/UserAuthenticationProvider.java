/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.openvpms.component.business.domain.im.security.User;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * An {@link AuthenticationProvider} that can be configured to allow or reject logins by users
 * whose {@link User#getChangePassword()} is {@code true}.
 * <p/>
 * An instance should be configured for UI authentication that allows users to log in even if their password needs
 * changing, but only to change their password. All other access should be restricted until this occurs.<br/>
 * Another instance should be configured for web service authentication that rejects authentication if the user
 * needs their password changed.
 *
 * @author Tim Anderson
 */
public class UserAuthenticationProvider extends DaoAuthenticationProvider {

    /**
     * Determines if an exception should be thrown if the user is required to change their password.
     */
    private final boolean expireOnChangePassword;

    /**
     * Constructs a {@link UserAuthenticationProvider}.
     *
     * @param userService            the user service
     * @param passwordEncoder        the password encoder
     * @param expireOnChangePassword if {@code true}, an exception will be thrown if the user is required to change
     *                               their password
     */
    public UserAuthenticationProvider(UserDetailsService userService, PasswordEncoder passwordEncoder,
                                      boolean expireOnChangePassword) {
        setUserDetailsService(userService);
        setPasswordEncoder(passwordEncoder);
        this.expireOnChangePassword = expireOnChangePassword;
    }

    /**
     * Performs authentication checks of a user.
     *
     * @param userDetails    the user details
     * @param authentication the authentication
     * @throws AuthenticationException     if the user cannot be authenticated
     * @throws CredentialsExpiredException if {@code expireOnChangePassword == true} and the user needs to change
     *                                     their password
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        super.additionalAuthenticationChecks(userDetails, authentication);
        checkAuthentication(userDetails);
    }

    /**
     * Checks a user's authentication.
     *
     * @param userDetails the user details
     * @throws AuthenticationException if the user cannot be authenticated
     */
    protected void checkAuthentication(UserDetails userDetails) {
        if (expireOnChangePassword) {
            User user = getUser(userDetails);
            if (user != null && user.getChangePassword()) {
                throw new CredentialsExpiredException(
                        messages.getMessage("AbstractUserDetailsAuthenticationProvider.credentialsExpired",
                                            "User credentials have expired"));
            }
        }
    }

    /**
     * Returns the user associated with a {@link UserDetails}.
     *
     * @param userDetails the user details
     * @return the associated user, or {@code null} if none exists
     */
    protected User getUser(UserDetails userDetails) {
        return (userDetails instanceof User) ? (User)  userDetails : null;
    }
}