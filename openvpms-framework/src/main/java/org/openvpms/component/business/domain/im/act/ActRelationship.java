/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.business.domain.im.common.IMObjectRelationship;


/**
 * A class that represents the directed association between two {@link Act}s.
 * In parent/child act relationships, the source act is the parent, the target
 * act is the child.
 *
 * @author Jim Alateras
 */
public class ActRelationship extends IMObjectRelationship
        implements org.openvpms.component.model.act.ActRelationship {

    /**
     * An integer representing the relative order of the relationship among
     * other like typed relationships.
     */
    private int sequence;

    /**
     * Indicates whether the relationship is one of parent-child. This means
     * that the parent is the owner of the relationship and is responsible for
     * managing its lifecycle. When the parent is deleted then it will also
     * delete the child
     */
    private boolean parentChildRelationship;

    /**
     * Serialisation version identifier.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Default constructor.
     */
    public ActRelationship() {
        // do nothing
    }

    /**
     * Returns the relationship sequence.
     * <p/>
     * This may be used to order relationships.
     *
     * @return the sequence
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * Sets the relationship sequence.
     * <p/>
     * This may be used to order relationships.
     *
     * @param sequence the sequence
     */
    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    /**
     * Determines if this is a parent/child relationship between two acts.
     *
     * @param parentChildRelationship if {@code true} it is a parent/child relationship
     */
    public void setParentChildRelationship(boolean parentChildRelationship) {
        this.parentChildRelationship = parentChildRelationship;
    }

    /**
     * Determines if this is a parent/child relationship between two acts.
     * If {@code true} it indicates that the parent act is the owner of the
     * relationship and is responsible for managing its lifecycle. When the
     * parent act is deleted, then the child act must also be deleted.
     *
     * @return {@code true} if this is a parent/child relationship
     */
    public boolean isParentChildRelationship() {
        return parentChildRelationship;
    }

}
