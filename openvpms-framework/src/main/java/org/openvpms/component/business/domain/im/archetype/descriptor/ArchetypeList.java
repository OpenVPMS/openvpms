/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.archetype.descriptor;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.archetype.ArchetypeRange;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of {@link ArchetypeRange} backed by a {@link List}.
 *
 * @author Tim Anderson
 */
public class ArchetypeList implements ArchetypeRange {

    /**
     * Empty archetype range.
     */
    public static final ArchetypeRange EMPTY = new ArchetypeList(Collections.emptyList());

    /**
     * The archetypes.
     */
    private final List<String> archetypes;

    /**
     * The default archetype.
     */
    private final String defaultArchetype;

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetype the archetype. May contain wildcards.
     */
    public ArchetypeList(String archetype) {
        this(archetype, null);
    }

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetype         the archetype. May contain wildcards.
     * @param defaultArchetype The default archetype. May be {@code null}
     */
    public ArchetypeList(String archetype, String defaultArchetype) {
        this(Collections.singletonList(archetype), defaultArchetype);
    }

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetypes the archetype. May contain wildcards.
     */
    public ArchetypeList(String[] archetypes) {
        this(archetypes, null);
    }

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetypes the archetype. May contain wildcards.
     */
    public ArchetypeList(String[] archetypes, String defaultArchetype) {
        this(Arrays.asList(archetypes), defaultArchetype);
    }

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetypes the archetypes. May contain wildcards.
     */
    public ArchetypeList(List<String> archetypes) {
        this(archetypes, null);
    }

    /**
     * Constructs a {@link ArchetypeList}.
     *
     * @param archetypes       the archetypes. May contain wildcards.
     * @param defaultArchetype the default archetype. May be {@code null}
     */
    public ArchetypeList(List<String> archetypes, String defaultArchetype) {
        this.archetypes = archetypes;
        this.defaultArchetype = defaultArchetype;
    }

    /**
     * Returns the range of archetypes.
     *
     * @return the range of archetypes. May contain wildcards.
     */
    @Override
    public List<String> getArchetypes() {
        return archetypes;
    }

    /**
     * Returns the default archetype.
     *
     * @return the default archetype. May be {@code null}
     */
    @Override
    public String getDefaultArchetype() {
        return defaultArchetype;
    }

    /**
     * Expands any wildcards in an {@link ArchetypeRange}.
     *
     * @param range   the archetype range
     * @param service the archetype service
     * @return the range with wildcards expanded
     */
    public static ArchetypeRange expand(ArchetypeRange range, ArchetypeService service) {
        String[] archetypes = DescriptorHelper.getShortNames(range.getArchetypes().toArray(new String[0]), false,
                                                             service);
        return (archetypes.length != 0 || range.getDefaultArchetype() != null)
               ? new ArchetypeList(archetypes, range.getDefaultArchetype())
               : EMPTY;
    }
}