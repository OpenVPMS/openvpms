/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.common;

import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.object.Reference;

import java.util.Date;

/**
 * Decorator for an {@link AuditableIMObject}.
 *
 * @author Tim Anderson
 */
public class AuditableIMObjectDecorator extends IMObjectDecorator implements AuditableIMObject {

    /**
     * Constructs an {@link AuditableIMObjectDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public AuditableIMObjectDecorator(AuditableIMObject peer) {
        super(peer);
    }

    /**
     * Returns the time the object was created.
     *
     * @return the create time. May be {@code null}
     */
    @Override
    public Date getCreated() {
        return getPeer().getCreated();
    }

    /**
     * Sets the time the object was created.
     *
     * @param created the create time. May be {@code null}
     */
    @Override
    public void setCreated(Date created) {
        getPeer().setCreated(created);
    }

    /**
     * Returns a reference to the user that created the object.
     *
     * @return the user reference. May be {@code null}
     */
    @Override
    public Reference getCreatedBy() {
        return getPeer().getCreatedBy();
    }

    /**
     * Sets the user that created the object.
     *
     * @param createdBy the user reference. May be {@code null}
     */
    @Override
    public void setCreatedBy(Reference createdBy) {
        getPeer().setCreatedBy(createdBy);
    }

    /**
     * Returns the time when the object was updated.
     *
     * @return the update time. May be {@code null}
     */
    @Override
    public Date getUpdated() {
        return getPeer().getUpdated();
    }

    /**
     * Sets the time when the object was updated.
     *
     * @param updated the update time. May be {@code null}
     */
    @Override
    public void setUpdated(Date updated) {
        getPeer().setUpdated(updated);
    }

    /**
     * Returns the user that updated the object.
     *
     * @return the user reference. May be {@code null}
     */
    @Override
    public Reference getUpdatedBy() {
        return getPeer().getUpdatedBy();
    }

    /**
     * Sets the user that updated the object.
     *
     * @param updatedBy the user reference. May be {@code null}
     */
    @Override
    public void setUpdatedBy(Reference updatedBy) {
        getPeer().setUpdatedBy(updatedBy);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected AuditableIMObject getPeer() {
        return (AuditableIMObject) super.getPeer();
    }
}
