/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.lookup;

import org.springframework.beans.factory.DisposableBean;

/**
 * This is a helper class for the lookup service.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class LookupServiceHelper implements DisposableBean {

    /**
     * A reference to the lookup service.
     */
    private static ILookupService lookupService;


    /**
     * Register the lookup service.
     *
     * @param service the service to register
     */
    public LookupServiceHelper(ILookupService service) {
        lookupService = service;
    }

    /**
     * Return a reference to the {@link LookupService}.
     *
     * @return the lookup service
     * @throws LookupServiceException if the look service is not registered
     */
    public static ILookupService getLookupService() {
        if (lookupService == null) {
            throw new LookupServiceException(LookupServiceException.ErrorCode.LookupServiceNotSet);
        }
        return lookupService;
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        lookupService = null;
    }
}
