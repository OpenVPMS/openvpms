/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.dao.hibernate.im.security;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.openvpms.component.business.dao.hibernate.cache.ArchetypeIdCache;
import org.openvpms.component.business.dao.hibernate.im.common.CompoundAssembler;
import org.openvpms.component.business.dao.hibernate.im.common.Context;
import org.openvpms.component.business.dao.hibernate.im.entity.EntityLinkAssembler;
import org.openvpms.component.business.dao.hibernate.im.entity.EntityRelationshipAssembler;
import org.openvpms.component.business.dao.hibernate.im.entity.IMObjectResultCollector;
import org.openvpms.component.business.dao.hibernate.im.lookup.LookupAssembler;
import org.openvpms.component.business.dao.hibernate.im.lookup.LookupLinkAssembler;
import org.openvpms.component.business.dao.hibernate.im.lookup.LookupRelationshipAssembler;
import org.openvpms.component.business.dao.hibernate.im.party.ContactAssembler;
import org.openvpms.component.business.dao.im.security.IUserDAO;
import org.openvpms.component.business.dao.im.security.UserDAOException;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;


/**
 * Implementation of {@link IUserDAO} using Hibernate.
 *
 * @author Jim Alateras
 */
public class UserDAOHibernate implements IUserDAO {

    /**
     * The session factory.
     */
    private final SessionFactory factory;

    /**
     * The archetype id cache.
     */
    private final ArchetypeIdCache archetypeIds;

    /**
     * The assembler.
     */
    private final CompoundAssembler assembler;

    /**
     * The archetype to filter on.
     */
    private List<String> archetypes;

    /**
     * Default user query.
     */
    private static final String DEFAULT_QUERY;

    /**
     * User query constrained by archetype.
     */
    private static final String QUERY_BY_ARCHETYPE;


    static {
        String prefix = "from " + UserDOImpl.class.getName() + " as user where user.username = :username";
        String suffix = " order by user.id";
        DEFAULT_QUERY = prefix + suffix;
        QUERY_BY_ARCHETYPE = prefix + " and user.archetypeId.shortName in (:archetypes)" + suffix;
    }

    /**
     * Constructs a {@link UserDAOHibernate}.
     *
     * @param factory    the session factory
     * @param archetypes the archetype descriptor cache
     */
    public UserDAOHibernate(SessionFactory factory, IArchetypeDescriptorCache archetypes) {
        this(factory, archetypes, new ArchetypeIdCache());
    }

    /**
     * Constructs a {@link UserDAOHibernate}.
     *
     * @param factory      the session factory
     * @param archetypes   the archetype descriptor cache
     * @param archetypeIds the archetype id cache
     */
    public UserDAOHibernate(SessionFactory factory, IArchetypeDescriptorCache archetypes,
                            ArchetypeIdCache archetypeIds) {
        this.factory = factory;
        this.archetypeIds = archetypeIds;
        assembler = new Assembler(archetypes);
    }

    /**
     * Constrains users to the specified archetypes.
     *
     * @param archetypes the user archetypes
     */
    public void setUserArchetypes(String... archetypes) {
        this.archetypes = archetypes != null && archetypes.length > 0 ? Arrays.asList(archetypes) : null;
    }

    /**
     * Retrieve the {@link User} with the specified name.
     *
     * @param username the username
     * @return the corresponding user, or {@code null} if none is found
     */
    @Override
    @Transactional(readOnly = true)
    public User getUser(String username) {
        List<User> results;
        Session session = factory.getCurrentSession();
        try {
            boolean byArchetype = archetypes != null && !archetypes.isEmpty();
            String userQuery = byArchetype ? QUERY_BY_ARCHETYPE : DEFAULT_QUERY;
            Query<UserDO> query = session.createQuery(userQuery, UserDO.class);
            query.setParameter("username", username);
            if (byArchetype) {
                query.setParameter("archetypes", archetypes);
            }
            Context context = Context.getContext(session, assembler, archetypeIds);
            IMObjectResultCollector collector = new IMObjectResultCollector();
            collector.setContext(context);
            query.stream().findFirst().ifPresent(collector::collect);
            context.resolveDeferredReferences();
            results = (List<User>) (List<?>) collector.getPage().getResults();
        } catch (Exception exception) {
            throw new UserDAOException(UserDAOException.ErrorCode.FailedToFindUserRecordByName,
                                       new Object[]{username}, exception);
        }
        return !results.isEmpty() ? results.get(0) : null;
    }

    private static class Assembler extends CompoundAssembler {

        Assembler(IArchetypeDescriptorCache archetypes) {
            super(archetypes);
            addAssembler(new ArchetypeAuthorityAssembler());
            addAssembler(new EntityLinkAssembler());
            addAssembler(new EntityRelationshipAssembler());
            addAssembler(new LookupAssembler());
            addAssembler(new LookupRelationshipAssembler());
            addAssembler(new LookupLinkAssembler());
            addAssembler(new SecurityRoleAssembler());
            addAssembler(new UserAssembler());
            addAssembler(new ContactAssembler());
        }
    }
}
