/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.component.system.common.cache.LRUIMObjectCache;

/**
 * A read-only {@link IArchetypeService} that supports caching of retrieved objects.
 * <p/>
 * This is intended to be used for short-lived purposes such as reporting. The cache will not necessarily reflect
 * the persistent state of objects.
 *
 * @author Tim Anderson
 */
public class CachingReadOnlyArchetypeService extends ReadOnlyArchetypeService {

    /**
     * The cache.
     */
    private final IMObjectCache cache;

    /**
     * Constructs a {@link ReadOnlyArchetypeService} that uses an {@link LRUIMObjectCache}.
     *
     * @param maxSize the maximum size of the cache
     * @param service the service to delegate to
     */
    public CachingReadOnlyArchetypeService(int maxSize, IArchetypeService service) {
        this(new LRUIMObjectCache(maxSize, service), service);
    }

    /**
     * Constructs a {@link ReadOnlyArchetypeService}.
     *
     * @param cache   the cache
     * @param service the service to delegate to
     */
    public CachingReadOnlyArchetypeService(IMObjectCache cache, IArchetypeService service) {
        super(service);
        this.cache = cache;
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @return the corresponding object, or {@code null} if none is found
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IMObject get(Reference reference) {
        return (IMObject) cache.get(reference);
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @param active    if {@code true}, only return the object if it is active
     * @return the corresponding object, or {@code null} if none is found
     * @throws ArchetypeServiceException if the query fails
     */
    @Override
    public IMObject get(Reference reference, boolean active) {
        IMObject result = null;
        if (!active) {
            result = get(reference);
        } else {
            IMObject cached = (IMObject) cache.getCached(reference);
            if (cached != null) {
                if (cached.isActive()) {
                    result = cached;
                }
            } else {
                IMObject object = super.get(reference, true);
                if (object != null) {
                    cache.add(object);
                    result = object;
                }
            }
        }
        return result;
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends org.openvpms.component.model.object.IMObject> T get(Reference reference, Class<T> type) {
        return type.cast(get(reference));
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.model.object.IMObject get(String archetype, long id) {
        return get(new IMObjectReference(archetype, id));
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the object, or {@code null} if none is found
     * @throws OpenVPMSException for any error
     */
    @Override
    public org.openvpms.component.model.object.IMObject get(String archetype, long id, boolean active) {
        return get(new IMObjectReference(archetype, id), active);
    }

    /**
     * Returns an object given its archetype and identifier.
     *
     * @param archetype the object's archetype
     * @param id        the object's identifier
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     * @throws OpenVPMSException  for any other error
     */
    @Override
    public <T extends org.openvpms.component.model.object.IMObject> T get(String archetype, long id, Class<T> type) {
        return type.cast(get(archetype, id));
    }

    /**
     * Clears the cache.
     */
    public void clear() {
        cache.clear();
    }
}
