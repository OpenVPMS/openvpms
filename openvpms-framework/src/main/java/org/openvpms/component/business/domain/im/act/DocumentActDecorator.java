/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.Reference;

/**
 * Decorator for {@link DocumentAct}.
 *
 * @author Tim Anderson
 */
public class DocumentActDecorator extends ActDecorator implements DocumentAct {

    /**
     * Constructs a {@link DocumentActDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public DocumentActDecorator(DocumentAct peer) {
        super(peer);
    }

    /**
     * Returns the document reference.
     *
     * @return the document reference. May be {@code null}
     */
    @Override
    public Reference getDocument() {
        return getPeer().getDocument();
    }

    /**
     * Sets the document reference.
     *
     * @param reference the document reference. May be {@code null}
     */
    @Override
    public void setDocument(Reference reference) {
        getPeer().setDocument(reference);
    }

    /**
     * Returns the document file name.
     *
     * @return the file name. May be {@code null}
     */
    @Override
    public String getFileName() {
        return getPeer().getFileName();
    }

    /**
     * Sets the document file name.
     *
     * @param fileName the file name. May be {@code null}
     */
    @Override
    public void setFileName(String fileName) {
        getPeer().setFileName(fileName);
    }

    /**
     * Returns the mime type of the document.
     *
     * @return the mime type. May be {@code null}
     */
    @Override
    public String getMimeType() {
        return getPeer().getMimeType();
    }

    /**
     * Sets the mime type of the document.
     *
     * @param mimeType the mime type. May be {@code null}
     */
    @Override
    public void setMimeType(String mimeType) {
        getPeer().setMimeType(mimeType);
    }

    /**
     * Determines if the document has been printed.
     *
     * @return {@code true} if the document has been printed
     */
    @Override
    public boolean isPrinted() {
        return getPeer().isPrinted();
    }

    /**
     * Determines if the document has been printed.
     *
     * @param printed ifi {@code true}, indicates the document has been printed
     */
    @Override
    public void setPrinted(boolean printed) {
        getPeer().setPrinted(printed);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected DocumentAct getPeer() {
        return (DocumentAct) super.getPeer();
    }
}
