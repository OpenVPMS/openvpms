/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.security;

import org.openvpms.component.model.user.User;

/**
 * Associates a user with the current thread of execution.
 *
 * @author Tim Anderson
 */
public interface AuthenticationContext {

    /**
     * Returns the user.
     *
     * @return the user, or {@code null} if no user has been set
     */
    User getUser();

    /**
     * Sets the user.
     *
     * @param user the user. May be {@code null}
     */
    void setUser(User user);
}
