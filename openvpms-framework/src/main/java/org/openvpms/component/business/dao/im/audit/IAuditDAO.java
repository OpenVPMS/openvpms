/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.im.audit;

import org.openvpms.component.business.domain.im.audit.AuditRecord;

import java.util.List;

/**
 * This interface provides data access object (DAO) support for objects of
 * type {@link AuditRecord}. The class includes the capability to perform save
 * and query data. Audit Records are immutable and cannot be modified.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public interface IAuditDAO {
    /**
     * Insert the specified {@link AuditRecord}.
     *
     * @param audit the audit record to insert
     * @throws AuditDAOException a runtime exception if the request cannot complete
     */
    public void insert(AuditRecord audit);

    /**
     * Retrieve the {@link AuditRecord} with the specified id
     *
     * @param id the id of the audit record
     * @return AuditRecord
     * @throws AuditDAOException
     */
    public AuditRecord getById(long id);

    /**
     * Retrieve all the {@link AuditRecord} instance with the specified
     * fully qualified archetypeId and objectId
     *
     * @param archetypeId the archetype of the object
     * @param objectId    the identity of the object
     * @return List<AuditRecord>
     * @throws AuditDAOException
     */
    public List<AuditRecord> getByObjectId(String archetypeId, long uid);
}
