/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper.sort;

import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.collections4.FunctorException;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.comparators.ComparatorChain;
import org.apache.commons.collections4.comparators.ReverseComparator;
import org.apache.commons.collections4.comparators.TransformingComparator;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.DescriptorException;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Sorts {@link IMObject}s.
 *
 * @author Tim Anderson
 */
public class IMObjectSorter {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * Default id comparator.
     */
    private static final Comparator<IMObject> ID_COMPARATOR = new IdComparator<>();

    /**
     * Default name comparator.
     */
    private static final Comparator<IMObject> NAME_COMPARATOR = new NameComparator<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectSorter.class);


    /**
     * Constructs an {@link IMObjectSorter}.
     *
     * @param service the archetype service
     * @param lookups the lookup service
     */
    public IMObjectSorter(IArchetypeService service, LookupService lookups) {
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Sorts objects on a node name, in ascending order.
     *
     * @param list the list to sort. This list is modified
     * @param name the name of the node to sort on
     * @return {@code list}
     */
    public <T extends IMObject> List<T> sort(List<T> list, String name) {
        return sort(list, name, true);
    }

    /**
     * Sorts objects on a node name.
     *
     * @param list      the list to sort. This list is modified
     * @param ascending if {@code true}, sort in ascending order, else sort in descending order
     * @param name      the name of the node to sort on
     * @return {@code list}
     */
    public <T extends IMObject> List<T> sort(List<T> list, String name, boolean ascending) {
        Comparator<T> comparator = getComparator(name);
        sort(list, ascending, comparator);
        return list;
    }

    /**
     * Sorts objects on multiple node names.
     *
     * @param list      the list to sort. This list is modified
     * @param ascending if {@code true}, sort in ascending order, else sort in descending order
     * @param names     the names of the nodes to sort on
     * @return {@code list}
     */
    public <T extends IMObject> List<T> sort(List<T> list, boolean ascending, String... names) {
        if (names.length >= 1) {
            ComparatorChain<T> chain = new ComparatorChain<>();
            for (String name : names) {
                chain.addComparator(getComparator(name));
            }
            sort(list, ascending, chain);
        }
        return list;
    }

    /**
     * Returns a comparator to sort {@link IMObject}s on {@link IMObject#getId()}.
     *
     * @return the comparator
     */
    @SuppressWarnings("unchecked")
    public static <T extends IMObject> Comparator<T> getIdComparator() {
        return (Comparator<T>) ID_COMPARATOR;
    }

    /**
     * Returns a comparator to sort {@link IMObject}s on {@link IMObject#getName()}.
     * <p/>
     * This treats nulls as low.
     *
     * @return the comparator
     */
    @SuppressWarnings("unchecked")
    public static <T extends IMObject> Comparator<T> getNameComparator() {
        return (Comparator<T>) NAME_COMPARATOR;
    }

    /**
     * Returns a comparator to sort on a node.
     *
     * @param name the node name
     * @return the comparator
     */
    private <T extends IMObject> Comparator<T> getComparator(String name) {
        Comparator<T> comparator;
        if ("id".equals(name)) {
            comparator = getIdComparator();
        } else if ("name".equals(name)) {
            comparator = getNameComparator();
        } else {
            comparator = new TransformingComparator<>(new NodeTransformer<>(name),
                                                      ComparatorUtils.nullLowComparator(null));
        }
        return comparator;
    }

    /**
     * Sorts a list.
     *
     * @param list       the list to sort
     * @param ascending  if {@code true}, sort in ascending order, else sort in descending order
     * @param comparator the comparator to use
     */
    private <T extends IMObject> void sort(List<T> list, boolean ascending, Comparator<T> comparator) {
        if (!ascending) {
            comparator = new ReverseComparator<>(comparator);
        }
        list.sort(comparator);
    }

    private class NodeTransformer<T extends IMObject> implements Transformer<T, Object> {

        /**
         * The node name.
         */
        private final String node;

        /**
         * Cached archetype descriptor.
         */
        private ArchetypeDescriptor archetype;

        /**
         * Cached node descriptor.
         */
        private NodeDescriptor descriptor;


        /**
         * Constructs a {@link NodeTransformer}.
         *
         * @param node the node name
         */
        public NodeTransformer(String node) {
            this.node = node;
        }

        /**
         * Transforms the input object (leaving it unchanged) into some output object.
         *
         * @param input the object to be transformed, should be left unchanged
         * @return a transformed object
         * @throws FunctorException (runtime) if the transform cannot be completed
         */
        public Object transform(T input) {
            Object result = null;
            if (input != null) {
                NodeDescriptor descriptor = getDescriptor(input);
                if (descriptor != null) {
                    try {
                        if (descriptor.isCollection()) {
                            List<IMObject> objects = descriptor.getValues(input);
                            if (objects.size() == 1) {
                                result = objects.get(0);
                            }
                        } else if (descriptor.isLookup()) {
                            result = lookups.getName(input, node);
                        } else {
                            result = descriptor.getValue(input);
                        }
                        if (result instanceof Participation) {
                            // sort on participation entity name
                            Participation p = (Participation) result;
                            result = getName(p.getEntity());
                        } else if (!(result instanceof Comparable)) {
                            // not comparable so null to avoid class cast exceptions
                            result = null;
                        } else if (result instanceof Timestamp) {
                            // convert all Timestamps to Dates to avoid class
                            // cast exceptions comparing dates and timestamps
                            result = new Date(((Timestamp) result).getTime());
                        }
                    } catch (DescriptorException exception) {
                        log.error(exception.getMessage(), exception);
                    }
                }
            }
            return result;
        }

        private NodeDescriptor getDescriptor(IMObject object) {
            if (archetype == null || !archetype.getArchetypeType().equals(object.getArchetype())) {
                archetype = DescriptorHelper.getArchetypeDescriptor(object, service);
                if (archetype == null) {
                    throw new FunctorException(
                            "No archetype descriptor found for object, id=" + object.getId() + ", archetype="
                            + object.getArchetype());
                }
                descriptor = archetype.getNodeDescriptor(node);
            }
            return descriptor;
        }

        /**
         * Returns the name of an object, given its reference.
         *
         * @param reference the object reference. May be {@code null}
         * @return the name or {@code null} if none exists
         */
        private String getName(Reference reference) {
            if (reference != null) {
                ArchetypeQuery query = new ArchetypeQuery(new ObjectRefConstraint("o", reference));
                query.add(new NodeSelectConstraint("o.name"));
                query.setMaxResults(1);
                Iterator<ObjectSet> iter = new ObjectSetQueryIterator(service, query);
                if (iter.hasNext()) {
                    ObjectSet set = iter.next();
                    return set.getString("o.name");
                }
            }
            return null;
        }
    }

    private static class IdComparator<T extends IMObject> implements Comparator<T> {

        /**
         * Compares its two arguments for order.  Returns a negative integer, zero, or a positive integer as the first
         * argument is less than, equal to, or greater than the second.
         *
         * @param o1 the first object to be compared.
         * @param o2 the second object to be compared.
         * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or
         * greater than the second.
         * @throws NullPointerException if an argument is null
         */
        @Override
        public int compare(T o1, T o2) {
            return Long.compare(o1.getId(), o2.getId());
        }
    }

    private static class NameComparator<T extends IMObject> implements Comparator<T> {

        private final Comparator<String> comparator = ComparatorUtils.nullLowComparator(
                ComparatorUtils.<String>naturalComparator());

        /**
         * Compares its two arguments for order.  Returns a negative integer,
         * zero, or a positive integer as the first argument is less than, equal
         * to, or greater than the second.<p>
         *
         * @param o1 the first object to be compared.
         * @param o2 the second object to be compared.
         * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or
         * greater than the second.
         * @throws NullPointerException if an argument is null
         */
        @Override
        public int compare(T o1, T o2) {
            return comparator.compare(o1.getName(), o2.getName());
        }
    }
}

