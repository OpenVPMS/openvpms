/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.lookup;

import org.openvpms.component.business.dao.hibernate.im.common.IMObjectRelationshipDOImpl;
import org.openvpms.component.business.domain.archetype.ArchetypeId;

/**
 * Implementation of the {@link LookupLinkDO} interface.
 *
 * @author Tim Anderson
 */
public class LookupLinkDOImpl extends IMObjectRelationshipDOImpl implements LookupLinkDO {

    /**
     * Default constructor.
     */
    public LookupLinkDOImpl() {
        super();
    }

    /**
     * Constructs an {@link LookupLinkDOImpl}.
     *
     * @param archetypeId the archetype identifier
     */
    public LookupLinkDOImpl(ArchetypeId archetypeId) {
        super(archetypeId);
    }

}
