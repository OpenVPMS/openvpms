/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.archetype.descriptor;

import org.openvpms.component.business.domain.im.datatypes.property.AssertionProperty;
import org.openvpms.component.business.domain.im.datatypes.property.PropertyList;
import org.openvpms.component.model.archetype.ArchetypeRange;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.archetype.PropertyMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * An {@link ArchetypeRange} backed by an {@link AssertionDescriptor}.
 * <br/>
 * Sample .adl definition:
 * <pre>
 * {@code
 * <node name="prices" path="/productPrices" parentChild="true" type="java.util.HashSet" baseName="ProductPrice"
 *               minCardinality="0" maxCardinality="*">
 *   <assertion name="archetypeRange">
 *     <propertyList name="archetypes">
 *       <propertyMap name="archetype">
 *         <property name="shortName" value="productPrice.fixedPrice"/>
 *       </propertyMap>
 *       <propertyMap name="archetype">
 *         <property name="shortName" value="productPrice.unitPrice"/>
 *       </propertyMap>
 *     </propertyList>
 *     <property name ="default" value="productPrice.unitPrice"/>
 *   </assertion>
 * </node>
 * }
 * </pre>
 *
 * @author Tim Anderson
 */
class ArchetypeRangeAssertion implements ArchetypeRange {

    /**
     * The assertion descriptor.
     */
    private final AssertionDescriptor descriptor;

    /**
     * Constructs a {@link ArchetypeRangeAssertion}.
     *
     * @param descriptor the assertion descriptor
     */
    public ArchetypeRangeAssertion(AssertionDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    /**
     * Returns the range of archetypes.
     *
     * @return the range of archetypes. May contain wildcards.
     */
    @Override
    public List<String> getArchetypes() {
        List<String> result = new ArrayList<>();
        PropertyList archetypes = (PropertyList) descriptor.getProperty("archetypes");
        for (NamedProperty property : archetypes.getProperties()) {
            Map<String, NamedProperty> properties = ((PropertyMap) property).getProperties();
            AssertionProperty shortName = (AssertionProperty) properties.get("shortName");
            result.add(shortName.getValue());
        }
        return result;
    }

    /**
     * Returns the default archetype.
     *
     * @return the default archetype. May be {@code null}
     */
    @Override
    public String getDefaultArchetype() {
        NamedProperty property = descriptor.getProperty("default");
        return (property instanceof AssertionProperty) ? ((AssertionProperty) property).getValue() : null;
    }
}