/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.lookup;

import org.openvpms.component.business.dao.hibernate.im.common.IMObjectRelationshipAssembler;
import org.openvpms.component.business.domain.im.lookup.LookupLink;


/**
 * Assembles {@link LookupLink}s from {@link LookupLinkDO}s and vice-versa.
 *
 * @author Tim Anderson
 */
public class LookupLinkAssembler extends IMObjectRelationshipAssembler<LookupLink, LookupLinkDO> {

    /**
     * Constructs an {@link LookupLinkAssembler}.
     */
    public LookupLinkAssembler() {
        super(org.openvpms.component.model.lookup.LookupLink.class, LookupLink.class, LookupLinkDO.class,
              LookupLinkDOImpl.class, LookupDO.class, LookupDOImpl.class);
    }

    /**
     * Creates a new object.
     *
     * @param object the source data object
     * @return a new object corresponding to the supplied data object
     */
    protected LookupLink create(LookupLinkDO object) {
        return new LookupLink();
    }

    /**
     * Creates a new data object.
     *
     * @param object the source object
     * @return a new data object corresponding to the supplied object
     */
    protected LookupLinkDO create(LookupLink object) {
        return new LookupLinkDOImpl();
    }
}
