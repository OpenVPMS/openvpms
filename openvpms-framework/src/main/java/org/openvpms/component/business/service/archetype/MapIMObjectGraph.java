/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implemenation of {@link IMObjectGraph} backed by a map.
 *
 * @author Tim Anderson
 */
public class MapIMObjectGraph implements IMObjectGraph {

    /**
     * The primary object.
     */
    private final IMObject primary;

    /**
     * The objects, keyed on reference.
     */
    private final Map<Reference, IMObject> objects = new HashMap<>();

    /**
     * Constructs a {@link MapIMObjectGraph}.
     *
     * @param primary the primary object
     * @param related the related objects
     */
    public MapIMObjectGraph(IMObject primary, List<IMObject> related) {
        this.primary = primary;
        objects.put(primary.getObjectReference(), primary);
        related.forEach(object -> objects.putIfAbsent(object.getObjectReference(), object));
    }

    /**
     * Returns the primary object.
     *
     * @return the primary object
     */
    @Override
    public IMObject getPrimary() {
        return primary;
    }

    /**
     * Returns the primary object.
     *
     * @param type the expected type of the object
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    @Override
    public <T> T getPrimary(Class<T> type) {
        return type.cast(primary);
    }

    /**
     * Retrieves an object given its reference.
     *
     * @param reference the object reference
     * @return the corresponding object, or {@code null} if none is found
     */
    @Override
    public IMObject get(Reference reference) {
        return objects.get(reference);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    @Override
    public <T extends IMObject> T get(Reference reference, Class<T> type) {
        return type.cast(get(reference));
    }

    /**
     * Returns the objects in the graph.
     *
     * @return the objects
     */
    @Override
    public Set<IMObject> getObjects() {
        return new HashSet<>(objects.values());
    }
}