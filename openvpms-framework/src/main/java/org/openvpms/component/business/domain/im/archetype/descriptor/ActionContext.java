/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.archetype.descriptor;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.archetype.NamedProperty;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;


/**
 * Context information passed to an assertion action.
 *
 * @author Tim Anderson
 * @author Jim Alateras
 */
public class ActionContext {

    /**
     * The assertion.
     */
    private final AssertionDescriptor assertion;

    /**
     * The parent object. May be {@code null}
     */
    private final IMObject parent;

    /**
     * The node descriptor.
     */
    private final NodeDescriptor node;

    /**
     * The value to that is the subject of the action.
     */
    private final Object value;

    /**
     * Beans that may be used by the assertion.
     */
    private final BeanFactory beans;


    /**
     * Constructs an {@link ActionContext}.
     *
     * @param assertion the assertion descriptor
     * @param parent    the parent object. May be {@code null}
     * @param node      the node descriptor
     * @param value     the value that is the subject of the action.
     */
    public ActionContext(AssertionDescriptor assertion, IMObject parent, NodeDescriptor node, Object value) {
        this(assertion, parent, node, value, null);
    }

    /**
     * Constructs an {@link ActionContext}.
     *
     * @param assertion the assertion descriptor
     * @param parent    the parent object. May be {@code null}
     * @param node      the node descriptor
     * @param value     the value that is the subject of the action.
     * @param beans     beans that may be used by the assertion
     */
    public ActionContext(AssertionDescriptor assertion, IMObject parent, NodeDescriptor node, Object value,
                         BeanFactory beans) {
        this.assertion = assertion;
        this.parent = parent;
        this.value = value;
        this.node = node;
        this.beans = beans;
    }

    /**
     * Returns the assertion descriptor.
     *
     * @return the assertion descriptor
     */
    public AssertionDescriptor getAssertion() {
        return assertion;
    }

    /**
     * Returns the parent object.
     *
     * @return the parent object, or {@code null} if there is no parent
     */
    public IMObject getParent() {
        return parent;
    }

    /**
     * Returns the node descriptor.
     *
     * @return the node descriptor
     */
    public NodeDescriptor getNode() {
        return node;
    }

    /**
     * Returns the value that is the subject of the action.
     *
     * @return the value. May be {@code null}
     */
    public Object getValue() {
        return value;
    }

    /**
     * Helper to return an assertion property.
     *
     * @param name the property name
     * @return the property, or {@code null} if none is found
     */
    public NamedProperty getProperty(String name) {
        return assertion.getProperty(name);
    }

    /**
     * Returns a bean of the specified type.
     *
     * @param type the type the bean
     * @return the corresponding bean
     * @throws BeansException        if the bean could not be created
     * @throws IllegalStateException if no beans are available
     */
    public <T> T getBean(Class<T> type) {
        if (beans == null) {
            throw new IllegalStateException("No beans are available");
        }
        return beans.getBean(type);
    }
}
