/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.util.StringUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * Validates {@link IMObject}s.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
class IMObjectValidator {

    /**
     * The archetype descriptor cache.
     */
    private final IArchetypeDescriptorCache cache;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectValidator.class);

    /**
     * Constructs an {@link IMObjectValidator}.
     *
     * @param cache the archetype descriptor cache
     */
    public IMObjectValidator(IArchetypeDescriptorCache cache) {
        this.cache = cache;
    }

    /**
     * Validates an object.
     *
     * @param object the object to validate
     * @return a list of validation errors encountered. Empty if no errors were found
     */
    public List<org.openvpms.component.service.archetype.ValidationError> validate(IMObject object) {
        List<org.openvpms.component.service.archetype.ValidationError> errors = new ArrayList<>();
        validate(object, errors);
        return errors;
    }

    /**
     * Validates an object.
     *
     * @param object the object to validate
     * @param errors the list to add validation errors to
     */
    @SuppressWarnings("unchecked")
    protected void validate(IMObject object, List<org.openvpms.component.service.archetype.ValidationError> errors) {
        String archetype = object.getArchetype();
        log.debug("Validating object of type {} with id {} and version {}", archetype, object.getId(),
                  object.getVersion());

        ArchetypeDescriptor descriptor = cache.getArchetypeDescriptor(archetype);
        if (descriptor == null) {
            addError(errors, object, null, "No archetype definition for " + archetype);
            log.error("No archetype definition for {}", archetype);
        } else {
            // if there are nodes attached to the archetype then validate the
            // associated assertions
            Map<String, NodeDescriptor> nodeDescriptors
                    = (Map<String, NodeDescriptor>) (Map<String, ?>) descriptor.getNodeDescriptorMap();
            if (!nodeDescriptors.isEmpty()) {
                JXPathContext context = JXPathHelper.newContext(object);
                validateNodes(object, context, descriptor, nodeDescriptors, errors);
            }
        }
    }

    /**
     * Validates each node of the parent object.
     *
     * @param parent    the parent object
     * @param context   holds the object to be validated
     * @param archetype the archetype descriptor
     * @param nodes     the nodes to validate
     * @param errors    the list to add validation errors to
     */
    protected void validateNodes(IMObject parent, JXPathContext context, ArchetypeDescriptor archetype,
                                 Map<String, NodeDescriptor> nodes,
                                 List<org.openvpms.component.service.archetype.ValidationError> errors) {
        for (NodeDescriptor node : nodes.values()) {
            validateNode(parent, context, archetype, node, errors);
        }
    }

    /**
     * Validates a node.
     *
     * @param parent    the parent object
     * @param context   holds the object to be validated
     * @param archetype the archetype descriptor
     * @param node      the node to validate
     * @param errors    the list to add validation errors to
     */
    protected void validateNode(IMObject parent, JXPathContext context, ArchetypeDescriptor archetype,
                                NodeDescriptor node,
                                List<org.openvpms.component.service.archetype.ValidationError> errors) {
        Object value;
        try {
            value = node.getValue(context);
        } catch (Exception exception) {
            addError(errors, parent, node, "Failed to get value");
            log.error("Failed to get value for {}", node.getName(), exception);
            return;
        }

        // first check whether the value for this node is derived and if it
        // is then set the derived value
        if (node.isDerived()) {
            try {
                context.getPointer(node.getPath()).setValue(value);
            } catch (Exception exception) {
                addError(errors, parent, node, "Cannot derive value");
                log.error("Failed to derive value for {}", node.getName(), exception);
                return;
            }
        }

        if (node.isCollection()) {
            checkCollection(parent, node, value, errors);
        } else {
            checkSimpleValue(parent, node, value, errors);
        }

        if (value != null) {
            // only check the assertions for non-null values. Null values are handled by minCardinality checks.
            checkAssertions(parent, node, value, errors);
        }

        // validate any child nodes
        if (!node.getNodeDescriptors().isEmpty()) {
            validateNodes(parent, context, archetype, node.getNodeDescriptors(), errors);
        }
    }

    /**
     * Checks that a simple value meets minCardinality requirements, and for strings, doesn't contain control
     * characters.
     *
     * @param parent the parent object
     * @param node   the node to validate
     * @param value  the node value to check
     * @param errors the list to add validation errors to
     */
    protected void checkSimpleValue(IMObject parent, NodeDescriptor node, Object value,
                                    List<org.openvpms.component.service.archetype.ValidationError> errors) {
        int min = node.getMinCardinality();
        if (min == 1 && value == null) {
            addError(errors, parent, node, "value is required");
        } else if (value instanceof String) {
            String str = (String) value;
            if (min == 1 && StringUtils.isEmpty(str)) {
                addError(errors, parent, node, "value is required");
            } else {
                int length = str.length();
                int minLength = node.getMinLength();
                int maxLength = node.getMaxLength();
                if (length != 0 && length < minLength) {
                    addError(errors, parent, node, "String too short. At least " + minLength
                                                   + " characters are required but only " + length + " were provided");
                } else if (maxLength > 0 && length > maxLength) {
                    addError(errors, parent, node, "String too long. Maximum length is " + maxLength
                                                   + " characters but " + length + " were provided");
                } else {
                    checkControlChars(parent, node, str, errors);
                }
            }
        }
    }

    /**
     * Checks a collection.
     *
     * @param parent the parent object
     * @param node   the node to validate
     * @param value  the node value to check
     * @param errors the list to add validation errors to
     */
    protected void checkCollection(IMObject parent, NodeDescriptor node, Object value,
                                   List<org.openvpms.component.service.archetype.ValidationError> errors) {
        Collection<?> collection = node.toCollection(value);

        checkMinCardinality(parent, node, collection, errors);

        // check the max cardinality if specified
        if (collection != null) {
            checkMaxCardinality(parent, node, collection, errors);

            // if it's a parent-child relationship then validate the children
            if (node.isParentChild()) {
                for (Object object : collection) {
                    if (object instanceof IMObject) {
                        validate((IMObject) object, errors);
                    }
                }
            }
        }
    }

    /**
     * Checks a nodes validation assertions.
     *
     * @param parent the parent object
     * @param node   the node to validate
     * @param value  the node value to check
     * @param errors the list to add validation errors to
     */
    protected void checkAssertions(IMObject parent, NodeDescriptor node, Object value,
                                   List<org.openvpms.component.service.archetype.ValidationError> errors) {
        for (AssertionDescriptor assertion : node.getAssertionDescriptorsAsArray()) {
            checkAssertion(parent, node, value, assertion, errors);
        }
    }

    /**
     * Checks an assertion for a node.
     *
     * @param parent    the parent object
     * @param node      the node to validate
     * @param value     the node value to check
     * @param assertion the assertion
     * @param errors    the list to add validation errors to
     */
    protected void checkAssertion(IMObject parent, NodeDescriptor node, Object value, AssertionDescriptor assertion,
                                  List<org.openvpms.component.service.archetype.ValidationError> errors) {
        try {
            if (!assertion.validate(value, parent, node)) {
                String message = assertion.getErrorMessage();
                if (message == null) {
                    message = "Validation failed for assertion " + assertion.getName();
                }
                addError(errors, parent, node, message);
            }
        } catch (Exception exception) {
            log.error("Assertion {} failed for node {}", assertion.getName(), node, exception);
            addError(errors, parent, node, exception.getMessage());
        }
    }

    /**
     * Checks a string for invalid control characters.
     *
     * @param parent the parent object.
     * @param node   the node descriptor
     * @param value  the string to check
     * @param errors the errors to add to if the string is invalid
     */
    protected void checkControlChars(IMObject parent, NodeDescriptor node, String value,
                                     List<org.openvpms.component.service.archetype.ValidationError> errors) {
        if (StringUtilities.hasControlChars(value)) {
            addError(errors, parent, node, "contains invalid characters");
        }
    }

    /**
     * Checks the minimum cardinality of a collection.
     *
     * @param parent     the parent object
     * @param node       the node
     * @param collection the collection. May be {@code null}
     * @param errors     the errors to add to if the collection is invalid
     */
    private void checkMinCardinality(IMObject parent, NodeDescriptor node, Collection<?> collection,
                                     List<org.openvpms.component.service.archetype.ValidationError> errors) {
        int min = node.getMinCardinality();
        if (min > 0 && (collection == null || collection.size() < min)) {
            String name = node.getBaseName() != null ? node.getBaseName() : "item";
            addError(errors, parent, node, "must supply at least " + min + " " + name);
        }
    }

    /**
     * Checks the maximum cardinality of a collection.
     *
     * @param parent     the parent object
     * @param node       the node
     * @param collection the collection
     * @param errors     the errors to add to if the collection is invalid
     */
    private void checkMaxCardinality(IMObject parent, NodeDescriptor node, Collection<?> collection,
                                     List<org.openvpms.component.service.archetype.ValidationError> errors) {
        int max = node.getMaxCardinality();
        if (max > 0 && collection.size() > max) {
            String name = node.getBaseName() != null ? node.getBaseName() : "item";
            addError(errors, parent, node, "cannot supply more than " + max + " " + name);
        }
    }


    /**
     * Adds a validation error.
     *
     * @param errors  the errors to add to if the string is invalid
     * @param object  the object that the error relates to
     * @param node    the node that validation failed for. May be {@code null}
     * @param message the error message
     */
    private void addError(List<org.openvpms.component.service.archetype.ValidationError> errors, IMObject object,
                          NodeDescriptor node, String message) {
        String archetype = object.getArchetype();
        String nodeName = (node != null) ? node.getName() : null;
        errors.add(new ValidationError(object.getObjectReference(), nodeName, message));
        log.debug("Validation failed: archetype={}, node={}, message={}", archetype, nodeName, message);
    }

}
