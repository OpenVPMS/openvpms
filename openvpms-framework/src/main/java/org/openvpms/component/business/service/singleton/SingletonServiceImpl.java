/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.singleton;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Consumer;

/**
 * Default implementation of {@link SingletonService}.
 *
 * @author Tim Anderson
 */
public class SingletonServiceImpl implements SingletonService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The query service.
     */
    private final SingletonQuery query;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs a {@link SingletonServiceImpl}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public SingletonServiceImpl(ArchetypeService service, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.transactionManager = transactionManager;
        query = new SingletonQuery(service);
    }

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    @Override
    public boolean exists(String archetype) {
        return query.exists(archetype);
    }

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @param exclude   exclude the instance with this reference
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    @Override
    public boolean exists(String archetype, Reference exclude) {
        return query.exists(archetype, exclude);
    }

    /**
     * Returns the single instance of an archetype.
     *
     * @param archetype the archetype
     * @param type      the type
     * @return the active instance, or {@code null} if none is found
     */
    @Override
    public <T extends IMObject> T get(String archetype, Class<T> type) {
        return query.get(archetype, type);
    }

    /**
     * Returns the instance of an archetype.
     * <p/>
     * This supports creation of the instance if none exists. The new instance will be saved, so the archetype must
     * provide default values for nodes with {@code minCardinality > 0}.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param create    if {@code true} create and save the instance if it doesn't exist
     * @return the singleton instance, or {@code null} if none is found
     */
    @Override
    public <T extends IMObject> T get(String archetype, Class<T> type, boolean create) {
        T result;
        if (create) {
            result = getOrCreate(archetype, type, null);
        } else {
            result = get(archetype, type);
        }
        return result;
    }

    /**
     * Returns the instance of an archetype, creating it if it doesn't exist.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param populator invoked to populate any new instance, prior to saving it
     * @return the singleton instance, or {@code null} if none is found
     */
    @Override
    public <T extends IMObject> T get(String archetype, Class<T> type, Consumer<T> populator) {
        return getOrCreate(archetype, type, populator);
    }

    /**
     * Retrieves the active instance of an archetype, or creates a new one if none exists.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param populator invoked to populate any new instance, prior to saving it. May be {@code null}
     * @return the active instance
     */
    private <T extends IMObject> T getOrCreate(String archetype, Class<T> type, Consumer<T> populator) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        return template.execute(status -> {
            T object = get(archetype, type);
            if (object == null) {
                object = service.create(archetype, type);
                if (populator != null) {
                    populator.accept(object);
                }
                service.save(object);
            }
            return object;
        });
    }

}
