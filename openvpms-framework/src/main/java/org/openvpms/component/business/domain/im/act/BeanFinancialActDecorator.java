/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.act;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A {@link FinancialActDecorator} that wraps the act in an {@link IMObjectBean}..
 *
 * @author Tim Anderson
 */
public class BeanFinancialActDecorator extends FinancialActDecorator {

    /**
     * The bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link BeanFinancialActDecorator}.
     *
     * @param peer    the peer to delegate to
     * @param service the archetype service
     */
    public BeanFinancialActDecorator(FinancialAct peer, ArchetypeService service) {
        this(service.getBean(peer));
    }

    /**
     * Creates a {@link BeanFinancialActDecorator}.
     *
     * @param bean the bean wrapping the act
     */
    public BeanFinancialActDecorator(IMObjectBean bean) {
        super(bean.getObject(FinancialAct.class));
        this.bean = bean;
    }

    /**
     * Returns the bean.
     *
     * @return the bean
     */
    public IMObjectBean getBean() {
        return bean;
    }
}
