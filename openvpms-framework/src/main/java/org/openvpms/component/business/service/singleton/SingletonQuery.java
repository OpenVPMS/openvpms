/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.singleton;

import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper to query singleton archetypes.
 * <p/>
 * These are archetypes where only a single active instance should be present.
 *
 * @author Tim Anderson
 */
public class SingletonQuery {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link SingletonQuery}.
     *
     * @param service the archetype service
     */
    public SingletonQuery(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    public boolean exists(String archetype) {
        return exists(archetype, null);
    }

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @param exclude   exclude the instance with this reference
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    public boolean exists(String archetype, Reference exclude) {
        Class<IMObject> type = getType(archetype);
        Query<IMObject, Long> query = new Query<>(archetype, type, Long.class, exclude);
        query.select("id");
        return query.get() != null;
    }

    /**
     * Returns the single instance of an archetype.
     *
     * @param archetype the archetype
     * @param type      the type
     * @return the active instance, or {@code null} if none is found
     */
    public <T extends IMObject> T get(String archetype, Class<T> type) {
        Query<T, T> query = new Query<>(archetype, type, type);
        return query.get();
    }

    /**
     * Determines the type of an archetype.
     *
     * @param archetype the archetype
     * @return the type
     */
    @SuppressWarnings("unchecked")
    private <T extends IMObject> Class<T> getType(String archetype) {
        ArchetypeDescriptor descriptor = service.getArchetypeDescriptor(archetype);
        if (descriptor == null) {
            throw new IllegalStateException("Unable to determine type of archetype: " + archetype);
        }
        return (Class<T>) descriptor.getClassType();
    }

    class Query<T extends IMObject, R> {

        private final Root<T> root;

        private final CriteriaQuery<R> query;

        public Query(String archetype, Class<T> type, Class<R> resultType) {
            this(archetype, type, resultType, null);
        }

        public Query(String archetype, Class<T> type, Class<R> resultType, Reference exclude) {
            CriteriaBuilder builder = service.getCriteriaBuilder();
            query = builder.createQuery(resultType);
            root = query.from(type, archetype);

            List<Predicate> predicates = new ArrayList<>();
            Predicate active = builder.equal(root.get("active"), true);
            predicates.add(active);
            if (exclude != null && !exclude.isNew()) {
                predicates.add(builder.equal(root.reference(), exclude).not());
            }

            query.where(predicates);
            query.orderBy(builder.asc(root.get("id")));
        }

        public void select(String node) {
            query.select(root.get(node));
        }

        public R get() {
            return service.createQuery(query).getFirstResult();
        }
    }
}