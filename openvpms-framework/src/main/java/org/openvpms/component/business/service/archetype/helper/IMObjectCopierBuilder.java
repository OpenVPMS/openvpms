/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.lang3.tuple.Pair;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Builds an {@link IMObjectCopier}.
 * <p/>
 * How an object is handled is determined by the following criteria:
 * <ol>
 *     <li>if it matches {@link #exclude(IMObject...)} or {@link #exclude(Reference...)}, it is skipped; else</li>
 *     <li>if it is mapped via one of the {@link #map} methods, it is mapped; else</li>
 *     <li>if it matches {@link #reference(IMObject...)} or {@link #reference(Reference...)}, it is referenced;
 *     else</li>
 *     <li>if it matches {@link #copy(IMObject...)} or {@link #copy(Reference...)}, it is copied; else</li>
 *     <li>if it matches {@link #exclude(String...)}, it is skipped; else</li>
 *     <li>if it matches {@link #reference(String...)}, it is referenced; else</li>
 *     <li>if it matches {@link #copy(String...)}, it is copied; else</li>
 *     <li>if it matches {@link #exclude(Class...)}, it is skipped; else</li>
 *     <li>if it matches {@link #reference(Class...)}, it is referenced; else</li>
 *     <li>if it matches {@link #copy(Class...)}, it is copied; else</li>
 *     <li>the default behaviour is invoked (see {@link #referenceByDefault()} or {@link #excludeByDefault()}
 *     </li>
 * </li>
 * In other words, an object is handled in order of the criteria:
 * <ol>
 *     <li>object: exclude, reference, copy; else</li>
 *     <li>archetype: exclude, reference, copy; else</li>
 *     <li>type: exclude, reference, copy; otherwise</li>
 *     <li>use the default behaviour</li>
 * </ol>
 *
 * @author Tim Anderson
 */
public class IMObjectCopierBuilder {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The state.
     */
    private final State state = new State();

    /**
     * Constructs an {@link IMObjectCopierBuilder}.
     *
     * @param service the archetype service
     */
    public IMObjectCopierBuilder(ArchetypeService service) {
        this.service = service;
        excludeByDefault();
    }

    /**
     * Exclude specific objects from being copied.
     * <p/>
     * Object exclusion has the highest precedence.
     *
     * @param objects the objects
     * @return this
     */
    public IMObjectCopierBuilder exclude(IMObject... objects) {
        return exclude(getReferences(objects));
    }

    /**
     * Exclude specific objects from being copied.
     * <p/>
     * Object exclusion has the highest precedence.
     *
     * @param references the references to the objects
     * @return this
     */
    public IMObjectCopierBuilder exclude(Reference... references) {
        return add(state.objectsToExclude, references);
    }

    /**
     * Indicates to map from one archetype to another.
     *
     * @param archetypeFrom the archetype to map from
     * @param archetypeTo   the archetype to map to. If {@code null}, any instance of {@code archetypeFrom} is ignored
     * @return this
     */
    public IMObjectCopierBuilder map(String archetypeFrom, String archetypeTo) {
        checkWildcard(archetypeFrom, "archetypeFrom");
        checkWildcard(archetypeTo, "archetypeTo");
        state.map.add(Pair.of(archetypeFrom, archetypeTo));
        return this;
    }

    /**
     * Indicates to map from one archetype to another.
     * <p/>
     * Each element is a pair of archetypes indicating the type to map from and to. If the 'to' archetype is
     * {@code null}, If {@code null}, any instance of the 'from' archetype is ignored.
     *
     * @param archetypes the archetypes to map
     * @return this
     */
    public IMObjectCopierBuilder map(String[][] archetypes) {
        return map(archetypes, false);
    }

    /**
     * Indicates to map from one archetype to another.
     * <p/>
     * Each element is a pair of archetypes indicating the type to map from and to. If the 'to' archetype is
     * {@code null}, any instance of the 'from' archetype is ignored.
     *
     * @param archetypes the archetypes to map
     * @param reverse    if {@code true}, treat the first element as the 'to' archetype, and the second as the 'from'
     *                   element ({@code null} 'from' archetypes are ignored)
     * @return this
     */
    public IMObjectCopierBuilder map(String[][] archetypes, boolean reverse) {
        for (String[] pair : archetypes) {
            String from;
            String to;
            if (reverse) {
                from = pair[1];
                to = pair[0];
            } else {
                from = pair[0];
                to = pair[1];
            }
            if (from != null) {
                map(from, to);
            }
        }
        return this;
    }

    /**
     * Indicates to reference objects rather than copy them.
     * <p/>
     * This has lower precedence than {@link #exclude(Reference...)}.
     *
     * @param objects the objects to reference
     * @return this
     */
    public IMObjectCopierBuilder reference(IMObject... objects) {
        return reference(getReferences(objects));
    }

    /**
     * Indicates to reference objects rather than copy them.
     * <p/>
     * This has lower precedence than {@link #exclude(Reference...)}.
     *
     * @param references the references to the objects
     * @return this
     */
    public IMObjectCopierBuilder reference(Reference... references) {
        return add(state.objectsToReference, references);
    }

    /**
     * Indicates to copy the specified objects.
     * <p/>
     * This has lower precedence than {@link #reference(IMObject...)} and {@link #reference(Reference...)}.
     *
     * @param objects the objects to copy
     * @return this
     */
    public IMObjectCopierBuilder copy(IMObject... objects) {
        return copy(getReferences(objects));
    }

    /**
     * Indicates to copy the specified objects.
     * <p/>
     * This has lower precedence than {@link #reference(IMObject...)} and {@link #reference(Reference...)}.
     *
     * @param references the references to the objects
     * @return this
     */
    public IMObjectCopierBuilder copy(Reference... references) {
        return add(state.objectsToCopy, references);
    }

    /**
     * Indicates to exclude objects with the specified archetypes.
     * <p/>
     * This has lower precedence than {@link #reference(IMObject...)}, {@link #reference(Reference...)},
     * {@link #copy(IMObject...)} and {@link #copy(Reference...)}.
     *
     * @param archetypes the archetypes to exclude. Cannot contain wildcards.
     * @return this
     */
    public IMObjectCopierBuilder exclude(String... archetypes) {
        return add(state.archetypesToExclude, archetypes);
    }

    /**
     * Indicates to reference objects with the specified archetypes.
     * <p/>
     * This has lower precedence than {@link #exclude(String...)}.
     *
     * @param archetypes the archetypes to reference. Cannot contain wildcards.
     * @return this
     */
    public IMObjectCopierBuilder reference(String... archetypes) {
        return add(state.archetypesToReference, archetypes);
    }

    /**
     * Indicates to copy objects with the specified archetypes.
     * <p/>
     * This has lower precedence than {@link #reference(String...)}.
     *
     * @param archetypes the archetypes to exclude. Cannot contain wildcards.
     * @return this
     */
    public IMObjectCopierBuilder copy(String... archetypes) {
        return add(state.archetypesToCopy, archetypes);
    }

    /**
     * Indicates to exclude objects with the specified types.
     * <p/>
     * This has lower precedence than {@link #exclude(String...)}, {@link #reference(String...)} and
     * {@link #copy(String...)}.
     *
     * @param types the types
     * @return this
     */
    @SafeVarargs
    public final IMObjectCopierBuilder exclude(Class<? extends IMObject>... types) {
        state.typesToExclude.add(types);
        return this;
    }

    /**
     * Indicates to reference objects with the specified types.
     * <p/>
     * This has lower precedence than {@link #exclude(Class...)}.
     *
     * @param types the types
     * @return this
     */
    @SafeVarargs
    public final IMObjectCopierBuilder reference(Class<? extends IMObject>... types) {
        state.typesToReference.add(types);
        return this;
    }

    /**
     * Indicates to copy objects with the specified types.
     * <p/>
     * This has lower precedence than {@link #reference(Class...)}.
     *
     * @param types the types
     * @return this
     */
    @SafeVarargs
    public final IMObjectCopierBuilder copy(Class<? extends IMObject>... types) {
        state.typesToCopy.add(types);
        return this;
    }

    /**
     * Indicates to reference all other objects, if no criteria is met.
     *
     * @return this
     */
    public IMObjectCopierBuilder referenceByDefault() {
        state.defaultTreatment = Treatment.REFERENCE;
        return this;
    }

    /**
     * Indicates to exclude all other objects, if no criteria is met.
     * <p/>
     * This is the default behaviour.
     *
     * @return this
     */
    public IMObjectCopierBuilder excludeByDefault() {
        state.defaultTreatment = Treatment.EXCLUDE;
        return this;
    }

    /**
     * Builds the copier.
     *
     * @return the copier
     */
    public IMObjectCopier build() {
        return new IMObjectCopier(new Handler(state), service);
    }

    /**
     * Adds references to a set.
     *
     * @param set        the set
     * @param references the references to add
     * @return this
     */
    private IMObjectCopierBuilder add(Set<Reference> set, Reference... references) {
        set.addAll(Arrays.asList(references));
        return this;
    }

    /**
     * Returns the references of an array of objects.
     *
     * @param objects the objects
     * @return the corresponding references
     */
    private Reference[] getReferences(IMObject... objects) {
        Reference[] result = new Reference[objects.length];
        for (int i = 0; i < objects.length; ++i) {
            result[i] = objects[i].getObjectReference();
        }
        return result;
    }

    /**
     * Adds archetypes to a set
     *
     * @param set        the set
     * @param archetypes the archetypes to add
     * @return this
     */
    private IMObjectCopierBuilder add(Set<String> set, String... archetypes) {
        for (String archetype : archetypes) {
            checkWildcard(archetype, "archetype");
            set.add(archetype);
        }
        return this;
    }

    /**
     * Ensures an archetype doesn't contain any wildcards.
     *
     * @param archetype the archetype
     * @param name      the argument name
     * @throws IllegalArgumentException if the archetype contains wildcards
     */
    private void checkWildcard(String archetype, String name) {
        if (archetype != null && archetype.contains("*")) {
            throw new IllegalArgumentException("Argument '" + name + "' cannot contain wildcards");
        }
    }

    private static class Handler extends AbstractIMObjectCopyHandler {

        /**
         * The state.
         */
        private final State state;

        /**
         * Returned by {@link #mapTo} to indicate that an object is to be replaced with {@code null}.
         */
        private static final String MAP_TO_NULL = "__MAP_TO_NULL";

        /**
         * Constructs a {@link Handler}.
         */
        protected Handler(State state) {
            this.state = state;
        }

        /**
         * Determines how {@link IMObjectCopier} should treat an object.
         * <p/>The behaviour is as follows:
         * <ol>
         * <li>if the the object is mapped to a different archetype. the map-to type is returned; else</li>
         * <li>if the object is mapped to null via the above, then {@code null</null>
         * is returned; else</li>
         * <li>the treatment is determined via {@link #getTreatment}</li>
         *
         * @param object  the source object
         * @param service the archetype service
         * @return {@code object} if the object shouldn't be copied, {@code null} if it should be replaced with
         * {@code null}, or a new instance if the object should be copied
         */
        @Override
        public IMObject getObject(IMObject object, ArchetypeService service) {
            IMObject result;
            if (state.objectsToExclude.contains(object.getObjectReference())) {
                result = null;
            } else {
                String from = object.getArchetype();
                String to = mapTo(from);
                if (MAP_TO_NULL.equals(to)) {
                    result = null;
                } else if (to != null) {
                    result = service.create(to, IMObject.class);
                } else {
                    switch (getTreatment(object)) {
                        case REFERENCE:
                            result = object;
                            break;
                        case COPY:
                            result = service.create(object.getArchetype(), IMObject.class);
                            break;
                        default:
                            result = null;
                    }
                }
            }
            return result;
        }

        /**
         * Returns the archetype to map the archetype name to.
         *
         * @param archetype the archetype to map from
         * @return the archetype to map to, or {@link #MAP_TO_NULL} if the object should be replaced with {@code null},
         * or {@code null} if there is no mapping for the archetype
         */
        protected String mapTo(String archetype) {
            String result = null;
            for (Pair<String, String> pair : state.map) {
                if (archetype.equals(pair.getLeft())) {
                    String to = pair.getRight();
                    result = (to == null) ? MAP_TO_NULL : to;
                    break;
                }
            }
            return result;
        }

        /**
         * Determines how an object should be handled.
         * <p/>
         * This returns:
         * <ul>
         * <li>{@link Treatment#EXCLUDE EXCLUDE} if the object should be replaced with {@code null}</li>
         * <li>{@link Treatment#REFERENCE REFERENCE} if the object should be referenced</li>
         * <li>{@link Treatment#COPY COPY} if a new instance of the object should be returned so it may be copied</li>
         * </ul>
         * How the object is handled is based on the criteria being matched, in order of precedence:
         * <ul>
         *     <li>object: exclude, reference or copy; else </li>
         *     <li>archetype: exclude, reference or copy; else</li>
         *     <li>type: exclude, reference, or copy; otherwise</li>
         *     <li>use the default treatment</li>
         * </ul>
         *
         * @param object the object
         * @return the type of behaviour to apply to the object
         */
        private Treatment getTreatment(IMObject object) {
            Treatment result;
            Reference reference = object.getObjectReference();
            if (state.objectsToExclude.contains(reference)) {
                result = Treatment.EXCLUDE;
            } else if (state.objectsToReference.contains(reference)) {
                result = Treatment.REFERENCE;
            } else if (state.objectsToCopy.contains(reference)) {
                result = Treatment.COPY;
            } else if (state.archetypesToExclude.contains(reference.getArchetype())) {
                result = Treatment.EXCLUDE;
            } else if (state.archetypesToReference.contains(reference.getArchetype())) {
                result = Treatment.REFERENCE;
            } else if (state.archetypesToCopy.contains(reference.getArchetype())) {
                result = Treatment.COPY;
            } else if (state.typesToExclude.matches(object)) {
                result = Treatment.EXCLUDE;
            } else if (state.typesToReference.matches(object)) {
                result = Treatment.REFERENCE;
            } else if (state.typesToCopy.matches(object)) {
                result = Treatment.COPY;
            } else {
                result = state.defaultTreatment;
            }
            return result;
        }
    }

    private static class State {

        private final Set<Reference> objectsToExclude = new HashSet<>();

        private final Set<Reference> objectsToReference = new HashSet<>();

        private final Set<Reference> objectsToCopy = new HashSet<>();

        private final Set<String> archetypesToExclude = new HashSet<>();

        private final Set<String> archetypesToReference = new HashSet<>();

        private final Set<String> archetypesToCopy = new HashSet<>();

        private final Types typesToExclude = new Types();

        private final Types typesToReference = new Types();

        private final Types typesToCopy = new Types();

        private final List<Pair<String, String>> map = new ArrayList<>();

        /**
         * The default treatment, if no other criteria is met.
         */
        private Treatment defaultTreatment = Treatment.EXCLUDE;

        private static class Types {

            /**
             * The types to match on.
             */
            private final Set<Class<?>> types = new HashSet<>();

            /**
             * Adds the types to match on.
             *
             * @param types the types
             */
            public void add(Class<?>... types) {
                this.types.addAll(Arrays.asList(types));
            }

            /**
             * Determines if an object matches the types.
             *
             * @param object the object
             * @return {@code true} if the object matches, otherwise {@code false}
             */
            public boolean matches(IMObject object) {
                for (Class<?> type : types) {
                    if (type.isAssignableFrom(object.getClass())) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    /**
     * Determines how objects should be treated.
     */
    private enum Treatment {
        REFERENCE,  // indicates that the object should be returned unchanged
        COPY,       // indicates to return a new instance of the object
        EXCLUDE     // indicates to return {@code null}
    }
}