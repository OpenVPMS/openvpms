/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.rule;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.DelegatingArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.ruleengine.IRuleEngine;
import org.openvpms.component.business.service.ruleengine.RuleSetUriHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.object.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Implementation of {@link IArchetypeRuleService} that uses Spring's
 * {@link PlatformTransactionManager} to provide transaction support.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
public class ArchetypeRuleService extends DelegatingArchetypeService implements IArchetypeRuleService {

    /**
     * The rule service.
     */
    private final IRuleEngine rules;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager txnManager;

    /**
     * Transaction helper.
     */
    private final TransactionTemplate template;

    /**
     * The facts to supply to all rules.
     */
    private List<Object> facts;

    /**
     * Save operation name.
     */
    private static final String SAVE = "save";

    /**
     * Remove operation name.
     */
    private static final String REMOVE = "remove";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ArchetypeRuleService.class);


    /**
     * Creates an {@link ArchetypeRuleService}.
     *
     * @param service    the archetype service to delegate requests to
     * @param rules      the rule engine
     * @param txnManager the transaction manager
     */
    public ArchetypeRuleService(IArchetypeService service, IRuleEngine rules, PlatformTransactionManager txnManager) {
        super(service);
        this.rules = rules;
        this.txnManager = txnManager;
        this.template = new TransactionTemplate(txnManager);
    }

    /**
     * Sets a list of facts to pass to all rules.
     * <p/>
     * These are supplied in addition to the underlying archetype service
     * and transaction manager.
     * <p/>
     * NOTE: this method is not thread safe. It is not intended to be used
     * beyond the initialisation of the service
     *
     * @param facts the rule facts. May be {@code null}
     */
    public void setFacts(List<Object> facts) {
        this.facts = facts;
    }

    /**
     * Saves an object, executing any <em>save</em> rules associated with its
     * archetype.
     *
     * @param object   the object to save
     * @param validate if {@code true} validate the object prior to saving it
     * @throws ArchetypeServiceException if the service cannot save the specified object
     * @throws ValidationException       if the specified object cannot be validated
     */
    @Deprecated
    @Override
    public void save(org.openvpms.component.model.object.IMObject object, boolean validate) {
        execute(SAVE, object, () -> getService().save(object, validate));
    }

    /**
     * Save a collection of {@link IMObject} instances, executing any
     * <em>save</em> rules associated with their archetypes.
     *
     * @param objects  the objects to insert or update
     * @param validate whether to validate or not
     * @throws ArchetypeServiceException if an object can't be saved
     * @throws ValidationException       if an object can't be validated
     */
    @Deprecated
    @Override
    public void save(Collection<? extends org.openvpms.component.model.object.IMObject> objects, boolean validate) {
        execute(SAVE, objects, () -> getService().save(objects, validate));
    }

    /**
     * Removes an object, executing any <em>remove</em> rules associated with its archetype.
     *
     * @param object the object to remove
     * @throws ArchetypeServiceException if the object cannot be removed
     */
    @Override
    public void remove(final org.openvpms.component.model.object.IMObject object) {
        execute(REMOVE, object, () -> getService().remove(object));
    }

    /**
     * Removes an object given its reference.
     *
     * @param reference the object reference
     * @throws OpenVPMSException for any error
     */
    @Override
    public void remove(Reference reference) {
        String archetype = reference.getArchetype();
        if (rules.hasRules(getRuleSetURI(REMOVE, archetype, true))
            || rules.hasRules(getRuleSetURI(REMOVE, archetype, false))) {
            // if there is a before/after remove rule for the archetype, need to use the remove(IMObject) method
            // to invoke them
            org.openvpms.component.model.object.IMObject object = get(reference);
            if (object == null) {
                throw new ArchetypeServiceException(ArchetypeServiceException.ErrorCode.FailedToDeleteObject,
                                                    reference);
            }
            remove(object);
        } else {
            getService().remove(reference);
        }
    }

    /**
     * Executes an operation in a transaction, as follows:
     * <ol>
     * <li>begin transaction
     * <li>execute <em>before</em> rules for {@code object}
     * <li>execute {@code operation}
     * <li>execute <em>after</em> rules for {@code object}
     * <li>commit on success/rollback on failure
     * </ol>
     *
     * @param name      the name of the operation
     * @param object    the object that will be supplied to before and after rules
     * @param operation the operation to execute
     */
    private void execute(String name, org.openvpms.component.model.object.IMObject object, Runnable operation) {
        template.execute(status -> {
            executeRules(name, object, true);
            operation.run();
            executeRules(name, object, false);
            return null;
        });
    }

    /**
     * Executes an operation in a transaction, with before and after rules.
     * <ol>
     * <li>begin transaction
     * <li>execute <em>before</em> rules for each object
     * <li>execute {@code operation}
     * <li>execute <em>after</em> rules for each object
     * <li>commit on success/rollback on failure
     * </ol>
     *
     * @param name    the name of the operation
     * @param objects the object that will be supplied to before and after rules
     * @param op      the operation to execute
     */
    private void execute(String name, Collection<? extends org.openvpms.component.model.object.IMObject> objects,
                         Runnable op) {
        template.execute(status -> {
            for (org.openvpms.component.model.object.IMObject object : objects) {
                executeRules(name, object, true);
            }
            op.run();
            for (org.openvpms.component.model.object.IMObject object : objects) {
                executeRules(name, object, false);
            }
            return null;
        });
    }

    /**
     * Executes rules for an object.
     *
     * @param name   the operation name
     * @param object the object to  execute rules for
     * @param before if {@code true} execute <em>before</em> rules, otherwise execute <em>after</em> rules
     */
    private void executeRules(String name, org.openvpms.component.model.object.IMObject object, boolean before) {
        String uri = getRuleSetURI(name, object.getArchetype(), before);
        if (rules.hasRules(uri)) {
            log.debug("Executing rules for uri={}", uri);
            List<Object> localFacts = new ArrayList<>();
            localFacts.add(object);
            localFacts.add(getService());
            localFacts.add(txnManager);
            if (facts != null) {
                localFacts.addAll(facts);
            }
            rules.executeRules(uri, localFacts);
        }
    }

    /**
     * Returns the rule set URI for an operation on an archetype.
     *
     * @param name      the operation name
     * @param archetype the archetype
     * @param before    if {@code true} create a <em>before</em> rule set URI, otherwise create an <em>after</em> URI
     * @return the URI
     */
    private String getRuleSetURI(String name, String archetype, boolean before) {
        return RuleSetUriHelper.getRuleSetURI("archetypeService", name, before, archetype);
    }

}
