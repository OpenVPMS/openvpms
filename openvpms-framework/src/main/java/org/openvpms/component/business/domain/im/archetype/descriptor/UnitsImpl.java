/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.archetype.descriptor;

import org.openvpms.component.model.archetype.Units;

/**
 * Default implementation of {@link Units}.
 *
 * @author Tim Anderson
 */
public class UnitsImpl implements Units {

    /**
     * The node name, if the units are held in another node.
     */
    private final String node;

    /**
     * The display name, if the units are fixed.
     */
    private final String displayName;

    /**
     * Constructs a {@link UnitsImpl}.
     *
     * @param node        the node name, if the units are held in another node.
     * @param displayName the display name, if the units are fixed.
     */
    public UnitsImpl(String node, String displayName) {
        this.node = node;
        this.displayName = displayName;
    }

    /**
     * Returns the name of the node holding the units.
     *
     * @return the node name, or {@code null} if the units are fixed
     */
    @Override
    public String getNode() {
        return node;
    }

    /**
     * Returns the units display name.
     *
     * @return the units display name, or {@code null} if the units are held by a node.
     */
    @Override
    public String getDisplayName() {
        return displayName;
    }
}