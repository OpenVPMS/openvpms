/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;


/**
 * Handler to determine how {@link IMObjectCopier} should copy objects.
 *
 * @author Tim Anderson
 */
public interface IMObjectCopyHandler {

    /**
     * Determines how {@link IMObjectCopier} should treat an object.
     *
     * @param object  the source object
     * @param service the archetype service
     * @return {@code object} if the object shouldn't be copied,
     * {@code null} if it should be replaced with
     * {@code null}, or a new instance if the object should be copied
     */
    IMObject getObject(IMObject object, ArchetypeService service);

    /**
     * Determines how a node should be copied.
     *
     * @param source     the source archetype
     * @param sourceNode the source node
     * @param target     the target archetype
     * @return a node to copy sourceNode to, or {@code null} if the node
     * shouldn't be copied
     */
    NodeDescriptor getNode(ArchetypeDescriptor source, NodeDescriptor sourceNode, ArchetypeDescriptor target);

}
