/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.entity;

import org.hibernate.Session;
import org.openvpms.component.business.dao.hibernate.im.common.AbstractDeleteHandler;
import org.openvpms.component.business.dao.hibernate.im.common.CompoundAssembler;
import org.openvpms.component.business.dao.hibernate.im.common.Context;
import org.openvpms.component.business.dao.hibernate.im.common.DeleteHandler;
import org.openvpms.component.business.dao.hibernate.im.common.IMObjectDO;
import org.openvpms.component.business.domain.im.common.Entity;


/**
 * Implementation of {@link DeleteHandler} for {@link Entity} instances.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class EntityDeleteHandler extends AbstractDeleteHandler {

    /**
     * Constructs an {@link EntityDeleteHandler}.
     *
     * @param assembler the assembler
     */
    public EntityDeleteHandler(CompoundAssembler assembler) {
        super(assembler);
    }

    /**
     * Deletes an object.
     * <p/>
     * This implementation removes relationships associated with the entity prior to its deletion.
     *
     * @param object  the object to delete
     * @param session the session
     * @param context the assembly context
     */
    @Override
    protected void delete(IMObjectDO object, Session session, Context context) {
        EntityDO entity = (EntityDO) object;
        // remove relationships where the entity is the source.
        EntityRelationshipDO[] sources = entity.getSourceEntityRelationships().toArray(new EntityRelationshipDO[0]);
        for (EntityRelationshipDO relationship : sources) {
            entity.removeSourceEntityRelationship(relationship);
            EntityDO target = (EntityDO) relationship.getTarget();
            if (target != null) {
                target.removeTargetEntityRelationship(relationship);
            }
        }

        // now remove relationships where the entity is the target
        EntityRelationshipDO[] targets = entity.getTargetEntityRelationships().toArray(new EntityRelationshipDO[0]);
        for (EntityRelationshipDO relationship : targets) {
            entity.removeTargetEntityRelationship(relationship);
            EntityDO source = (EntityDO) relationship.getSource();
            if (source != null) {
                source.removeSourceEntityRelationship(relationship);
            }
        }
        context.remove(entity);
    }
}
