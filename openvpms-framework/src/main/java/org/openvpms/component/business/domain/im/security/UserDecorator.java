/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.security;

import org.openvpms.component.business.domain.im.party.PartyDecorator;
import org.openvpms.component.model.user.User;

/**
 * Decorator for {@link User}.
 *
 * @author Tim Anderson
 */
public class UserDecorator extends PartyDecorator implements User {

    /**
     * Constructs a {@link UserDecorator}.
     *
     * @param peer the peer to delegate to
     */
    public UserDecorator(User peer) {
        super(peer);
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return the username
     */
    @Override
    public String getUsername() {
        return getPeer().getUsername();
    }

    /**
     * Sets the user's login name.
     *
     * @param userName the user's login name
     */
    @Override
    public void setUsername(String userName) {
        getPeer().setUsername(userName);
    }

    /**
     * Returns the peer.
     *
     * @return the peer
     */
    @Override
    protected User getPeer() {
        return (User) super.getPeer();
    }
}
