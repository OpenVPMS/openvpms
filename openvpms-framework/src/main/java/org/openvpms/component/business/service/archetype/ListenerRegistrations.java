/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.openvpms.component.business.service.archetype.helper.TypeHelper;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Manages {@link IArchetypeServiceListener} registrations.
 *
 * @author Tim Anderson
 */
class ListenerRegistrations {

    /**
     * The listener registrations.
     */
    private final Map<Key, Registration> listenerRegistrations = new ConcurrentHashMap<>();

    /**
     * The listeners for each archetype.
     */
    private final Map<String, Listeners> listeners = new ConcurrentHashMap<>();

    /**
     * Adds a listener to receive notification of changes.
     *
     * @param archetype the archetype short to receive events for. May contain wildcards.
     * @param matches   the archetypes that currently match {@code archetype}
     * @param listener  the listener to add
     */
    public void add(String archetype, List<String> matches, IArchetypeServiceListener listener) {
        Registration registration = listenerRegistrations.computeIfAbsent(
                new Key(archetype, listener), key -> new Registration(archetype, listener));
        registration.register(new LinkedHashSet<>(matches), listeners);
    }

    /**
     * Removes a listener.
     * <p/>
     * The listener must be removed with the same archetype it was registered with.
     *
     * @param archetype the archetype to stop receiving events for. May contain wildcards.
     * @param listener  the listener to remove
     */
    public void remove(String archetype, IArchetypeServiceListener listener) {
        Registration registration = listenerRegistrations.remove(new Key(archetype, listener));
        if (registration != null) {
            registration.deregister(listeners);
        }
    }

    /**
     * Notifies that an archetype has been added.
     * <p/>
     * All wildcard registrations that match the archetype will be updated to listen for updates.
     *
     * @param archetype the archetype
     */
    public void archetypeAdded(String archetype) {
        for (Registration registration : listenerRegistrations.values()) {
            registration.archetypeAdded(archetype, listeners);
        }
    }

    /**
     * Returns the listeners for an archetype.
     *
     * @param archetype the archetype
     * @return the listeners, or {@code null} if there aren't any
     */
    public IArchetypeServiceListener[] get(String archetype) {
        Listeners result = listeners.get(archetype);
        return result != null ? result.get() : null;
    }

    /**
     * Returns the count of registrations.
     *
     * @return the no. of registrations
     */
    public int getRegistrationCount() {
        return listenerRegistrations.size();
    }

    /**
     * Returns the count of listeners.
     * <p/>
     * This is the no. of listeners for individual archetypes.
     *
     * @return the no. of listeners
     */
    public int getListenerCount() {
        int result = 0;
        synchronized (listeners) {
            for (Listeners l : listeners.values()) {
                result += l.listeners.size();
            }
        }
        return result;
    }

    /**
     * Registration key.
     */
    static class Key {

        /**
         * The registration key.
         */
        private final String archetype;

        /**
         * The listener.
         */
        private final IArchetypeServiceListener listener;

        /**
         * Constructs a {@link Key}.
         *
         * @param archetype the registration key
         * @param listener  the listener
         */
        public Key(String archetype, IArchetypeServiceListener listener) {
            this.listener = listener;
            this.archetype = archetype;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode() {
            return Objects.hash(listener, archetype);
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Key) {
                Key other = (Key) obj;
                return listener == other.listener && archetype.equals(other.archetype);
            }
            return false;
        }
    }

    /**
     * Manages the registration of an {@link IArchetypeServiceListener}.
     *
     * @author Tim Anderson
     */
    static class Registration {

        /**
         * The archetype the listener was registered with. This may contain wildcards.
         */
        private final String registrationArchetype;

        /**
         * Determines if the registration archetype is a wildcard.
         */
        private final boolean wildcard;

        /**
         * The listener.
         */
        private final IArchetypeServiceListener listener;

        /**
         * The archetypes the registration matches.
         */
        private Set<String> matches = new HashSet<>();

        /**
         * Constructs a {@link Registration}.
         *
         * @param archetype the archetype(s) the listener is for. This may be a single archetype or contain wildcards.
         * @param listener  the listener
         */
        public Registration(String archetype, IArchetypeServiceListener listener) {
            this.registrationArchetype = archetype;
            this.wildcard = archetype.contains("*");
            this.listener = listener;
        }

        /**
         * Registers this listener for each archetype it matches.
         *
         * @param matches   the matching archetypes
         * @param listeners the listeners by archetype
         */
        public synchronized void register(Set<String> matches, Map<String, Listeners> listeners) {
            for (String match : matches) {
                register(match, listeners);
            }
            this.matches = matches;
        }

        /**
         * Deregister this listener.
         *
         * @param listeners the listeners
         */
        public synchronized void deregister(Map<String, Listeners> listeners) {
            for (String match : matches) {
                Listeners l = listeners.get(match);
                if (l != null) {
                    l.remove(listener);
                }
            }
            matches.clear();
        }

        /**
         * Notifies of a new archetype.
         * <p/>
         * If this registration matches, its listener will be added.
         *
         * @param archetype the archetype
         * @param listeners the listeners
         */
        public synchronized void archetypeAdded(String archetype, Map<String, Listeners> listeners) {
            if (matches(archetype)) {
                register(archetype, listeners);
                matches.add(archetype);
            }
        }

        /**
         * Determines if this registration matches an archetype.
         *
         * @param archetype the archetype
         * @return {@code true} if the registration matches, otherwise {@code false}
         */
        private boolean matches(String archetype) {
            return wildcard ? TypeHelper.isA(archetype, registrationArchetype)
                            : registrationArchetype.equals(archetype);
        }

        /**
         * Registers this listener for an archetype.
         *
         * @param archetype the archetype
         * @param listeners the listeners to add to
         */
        private void register(String archetype, Map<String, Listeners> listeners) {
            Listeners l = listeners.computeIfAbsent(archetype, k -> new Listeners());
            l.add(listener);
        }
    }

    /**
     * Helper to manage listeners for an archetype.
     */
    private static class Listeners {

        /**
         * The listeners.
         */
        private final Set<IArchetypeServiceListener> listeners = new LinkedHashSet<>();

        /**
         * Add a listener.
         *
         * @param listener the listener to add
         */
        synchronized void add(IArchetypeServiceListener listener) {
            listeners.add(listener);
        }

        /**
         * Removes a listener.
         *
         * @param listener the listener to remove
         */
        synchronized void remove(IArchetypeServiceListener listener) {
            listeners.remove(listener);
        }

        /**
         * Returns the listeners.
         *
         * @return the listeners, or {@code null} if there aren't any
         */
        synchronized IArchetypeServiceListener[] get() {
            IArchetypeServiceListener[] result = listeners.toArray(new IArchetypeServiceListener[0]);
            return result.length != 0 ? result : null;
        }
    }
}
