/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.singleton;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;

import java.util.function.Consumer;

/**
 * Manages archetypes where only a single active instance should exist.
 *
 * @author Tim Anderson
 */
public interface SingletonService {

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    boolean exists(String archetype);

    /**
     * Determines if there is a single instance of an archetype.
     *
     * @param archetype the archetype
     * @param exclude   exclude the instance with this reference
     * @return {@code true} if an active instance exists, otherwise {@code false}
     */
    boolean exists(String archetype, Reference exclude);

    /**
     * Returns the single instance of an archetype.
     *
     * @param archetype the archetype
     * @param type      the type
     * @return the active instance, or {@code null} if none is found
     */
    <T extends IMObject> T get(String archetype, Class<T> type);

    /**
     * Returns the instance of an archetype.
     * <p/>
     * This supports creation of the instance if none exists. The new instance will be saved, so the archetype must
     * provide default values for nodes with {@code minCardinality > 0}.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param create    if {@code true} create and save the instance if it doesn't exist
     * @return the singleton instance, or {@code null} if none is found
     */
    <T extends IMObject> T get(String archetype, Class<T> type, boolean create);

    /**
     * Returns the instance of an archetype, creating it if it doesn't exist.
     * <p/>
     * Use this to populate newly-created instances prior to save.
     *
     * @param archetype the archetype
     * @param type      the type
     * @param populator invoked to populate any new instance, prior to saving it
     * @return the singleton instance, or {@code null} if none is found
     */
    <T extends IMObject> T get(String archetype, Class<T> type, Consumer<T> populator);

}
