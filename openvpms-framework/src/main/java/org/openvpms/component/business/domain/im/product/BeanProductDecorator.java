/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.product;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.product.Product;

/**
 * A {@link ProductDecorator} that wraps the product in an {@link IMObjectBean}.
 *
 * @author Tim Anderson
 */
public class BeanProductDecorator extends ProductDecorator {

    /**
     * The bean.
     */
    private final IMObjectBean bean;


    /**
     * Constructs a {@link BeanProductDecorator}.
     *
     * @param peer    the peer to delegate to
     * @param factory the bean factory
     */
    public BeanProductDecorator(Product peer, IMObjectBeanFactory factory) {
        super(peer);
        bean = factory.getBean(peer);
    }

    /**
     * Constructs a {@link BeanProductDecorator}.
     *
     * @param bean the bean wrapping the product
     */
    public BeanProductDecorator(IMObjectBean bean) {
        super(bean.getObject(Product.class));
        this.bean = bean;
    }

    /**
     * Returns the bean.
     *
     * @return the bean
     */
    public IMObjectBean getBean() {
        return bean;
    }

}
