/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.dao.hibernate.im.query.TypeSet;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Order;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.Policy.State;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.bean.RelatedObjectPolicyBuilder;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link RelatedIMObjects}.
 *
 * @author Tim Anderson
 */
public class RelatedIMObjectsImpl<T extends IMObject, R extends Relationship> implements RelatedIMObjects<T, R> {

    /**
     * The relationships.
     */
    private final Collection<R> relationships;

    /**
     * Returns the reference from a relationship.
     */
    private final Function<R, Reference> accessor;

    /**
     * Used to resolve an object given its reference.
     */
    private final BiFunction<Reference, State, T> resolver;

    /**
     * The object retrieval policy.
     */
    private final Policy<R> policy;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     * <p/>
     * The object determines if the source or target of the relationships are returned.
     *
     * @param object        the object
     * @param relationships the object's relationships
     * @param type          the type of the related objects
     * @param service       the archetype service
     */
    public RelatedIMObjectsImpl(IMObject object, Collection<R> relationships, Class<T> type, ArchetypeService service) {
        this(relationships, getAccessor(object.getObjectReference()), getResolver(service, type), service);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param type          the type of the related objects
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param service       the archetype service
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Class<T> type, boolean source, ArchetypeService service,
                                Policy<R> policy) {
        this(relationships, getAccessor(source), getResolver(service, type), policy, service);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     * @param service       the archetype service
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Function<R, Reference> accessor,
                                BiFunction<Reference, State, T> resolver, ArchetypeService service) {
        this(relationships, accessor, resolver, null, service);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     * @param service       the archetype service
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Function<R, Reference> accessor,
                                BiFunction<Reference, State, T> resolver, Policy<R> policy, ArchetypeService service) {
        this.relationships = relationships;
        this.accessor = accessor;
        this.resolver = resolver;
        this.policy = policy;
        this.service = service;
    }

    /**
     * Selects all related objects for retrieval.
     * <p/>
     * This is the default.
     *
     * @return an instance that selects all related objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> all() {
        return policy((Policy<R>) Policies.all());
    }

    /**
     * Selects active relationships and objects for retrieval.
     *
     * @return an instance that selects active relationships and objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> active() {
        return policy((Policy<R>) Policies.active());
    }

    /**
     * Selects relationships active at the specified time and returns active objects.
     *
     * @param time the time
     * @return an instance that selects active relationships at {@code time}, and active objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> active(Date time) {
        return policy((Policy<R>) Policies.active(time));
    }

    /**
     * Selects objects based on the specified policy.
     *
     * @param policy the policy
     * @return an instance that selects objects according to {@code policy}
     */
    @Override
    public RelatedIMObjects<T, R> policy(Policy<R> policy) {
        return hasPolicy(policy) ? this : newInstance(relationships, accessor, resolver, policy, service);
    }

    /**
     * Returns a policy builder to select objects.
     *
     * @return a new policy builder
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedObjectPolicyBuilder<T, R, RelatedIMObjects<T, R>> newPolicy() {
        return new DefaultRelatedObjectPolicyBuilder<>(this, (Class<R>) Relationship.class);
    }

    /**
     * Returns the first object to match the criteria, after applying any comparator.
     * <br/>
     * If {@code state == State.ACTIVE} the object must be active in order to be returned, otherwise an active object
     * will be returned in preference to an inactive one.
     */
    @Override
    public T getObject() {
        ObjectRelationship<T, R> result = getObjectRelationship();
        return (result != null) ? result.getObject() : null;
    }

    /**
     * Returns the first object and relationship to match the {@link #getPolicy() policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object and relationship, or {@code null} if none is found
     */
    @Override
    public ObjectRelationship<T, R> getObjectRelationship() {
        ObjectRelationship<T, R> result = null;
        State state = getState();
        for (R relationship : getRelationships()) {
            Reference reference = accessor.apply(relationship);
            if (reference != null) {
                T object = resolver.apply(reference, state);
                if (object != null) {
                    if (object.isActive() || state == Policy.State.INACTIVE) {
                        // found a match, so return it
                        result = new ObjectRelationshipImpl<>(object, relationship);
                        break;
                    } else if (result == null) {
                        // can return inactive, but keep looking for an active match
                        result = new ObjectRelationshipImpl<>(object, relationship);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the related objects.
     *
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects() {
        List<Reference> references = getReferences();
        return () -> new LazyObjectIterator(references.iterator());
    }

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects(int firstResult, int maxResults) {
        List<Reference> references = getReferences();
        Comparator<R> comparator = (policy != null) ? policy.getComparator() : null;
        if (comparator == null) {
            // if there was no comparator, sort references to make paging deterministic
            references.sort((o1, o2) -> {
                int result = Long.compare(o1.getId(), o2.getId());
                if (result == 0) {
                    result = StringUtils.compare(o1.getLinkId(), o2.getLinkId());
                }
                return result;
            });
        }
        return new PagingReferenceIterable(references, getState(), firstResult, maxResults);
    }

    /**
     * Returns the related objects matching the {@link #getPolicy() policy}, supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @param order       the order criteria
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects(int firstResult, int maxResults, Order... order) {
        Iterable<T> result;
        if (order.length == 0) {
            result = getObjects(firstResult, maxResults);
        } else {
            result = new SortableIterable(getReferences(), getState(), firstResult, maxResults, order);
        }
        return result;
    }

    /**
     * Returns the objects and their corresponding relationships matching the criteria.
     *
     * @return the objects and their corresponding relationships
     */
    @Override
    public Iterable<ObjectRelationship<T, R>> getObjectRelationships() {
        List<R> relationships = getRelationships();
        return () -> new LazyRelationshipIterator(relationships.iterator());
    }

    /**
     * Returns the objects and their corresponding relationships matching the {@link #getPolicy() policy},
     * supporting paging.
     * <p/>
     * This is an {@code Iterable} to allow implementations to lazily load objects. Retrieval of large numbers
     * of objects may be resource intensive.
     *
     * @param firstResult the first result position
     * @param maxResults  the maximum number of results to return
     * @return the objects and their corresponding relationships
     */
    @Override
    public Iterable<ObjectRelationship<T, R>> getObjectRelationships(int firstResult, int maxResults) {
        List<R> relationships = getRelationships();
        Comparator<R> comparator = (policy != null) ? policy.getComparator() : null;
        if (comparator == null) {
            // if there was no comparator, sort relationships to make paging deterministic
            relationships.sort((o1, o2) -> {
                int result = Long.compare(o1.getId(), o2.getId());
                if (result == 0) {
                    result = StringUtils.compare(o1.getLinkId(), o2.getLinkId());
                }
                return result;
            });
        }
        return new PagingObjectRelationIterable(relationships, firstResult, maxResults, getState());
    }

    /**
     * Returns the relationships matching the criteria.
     *
     * @return the relationships matching the criteria
     */
    @Override
    public List<R> getRelationships() {
        List<R> result = new ArrayList<>();
        Predicate<R> predicate = (policy != null) ? policy.getPredicate() : null;
        if (predicate == null) {
            result = new ArrayList<>(relationships);
        } else {
            for (R relationship : relationships) {
                if (predicate.test(relationship)) {
                    result.add(relationship);
                }
            }
        }
        if (!result.isEmpty()) {
            Comparator<R> comparator = (policy != null) ? policy.getComparator() : null;
            if (comparator != null) {
                result.sort(comparator);
            }
        }
        return result;
    }

    /**
     * Returns the relationship references matching the criteria.
     * <p/>
     * NOTE: this does not determine if the objects are active or inactive
     *
     * @return the related object references
     */
    @Override
    public List<Reference> getReferences() {
        List<Reference> result = new ArrayList<>();
        for (R relationship : getRelationships()) {
            Reference reference = accessor.apply(relationship);
            if (reference != null) {
                result.add(reference);
            }
        }
        return result;
    }

    /**
     * Returns the policy being used to select objects.
     *
     * @return the policy. May be {@code null} to indicate that all objects are returned
     */
    @Override
    public Policy<R> getPolicy() {
        return policy;
    }

    /**
     * Determines if the policy specified is the same as that being used.
     *
     * @param policy the policy. May be {@code null}
     * @return {@code true} if the policies are the same, otherwise {@code false}
     */
    public boolean hasPolicy(Policy<R> policy) {
        return Objects.equals(this.policy, policy) || (isAll(this.policy) && isAll(policy));
    }

    /**
     * Creates a new instance.
     *
     * @param relationships the relationships.
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     * @param service       the archetype service
     * @return a new instance
     */
    protected RelatedIMObjectsImpl<T, R> newInstance(Collection<R> relationships, Function<R, Reference> accessor,
                                                     BiFunction<Reference, State, T> resolver, Policy<R> policy,
                                                     ArchetypeService service) {
        return new RelatedIMObjectsImpl<>(relationships, accessor, resolver, policy, service);
    }

    /**
     * Determines if a policy selects all objects.
     *
     * @param policy the policy. A {@code null} policy indicates it selects all objects
     * @return {@code true} if the policy selects all objects, otherwise {@code false}
     */
    private boolean isAll(Policy<R> policy) {
        return (policy == null || Objects.equals(policy, Policies.all()));
    }

    /**
     * Returns a function to resolve objects by reference from an {@link ArchetypeService}.
     *
     * @param service the archetype service
     * @param type    the type of the objects
     * @return a function to resolve objects by reference
     */
    private static <T extends IMObject> BiFunction<Reference, State, T> getResolver(ArchetypeService service,
                                                                                    Class<T> type) {
        return (reference, state) -> type.cast(resolve(reference, state, service));
    }

    /**
     * Returns the active state of the objects to return.
     *
     * @return the active state
     */
    private State getState() {
        return (policy != null) ? policy.getState() : State.ANY;
    }

    /**
     * Resolves an object given its reference.
     *
     * @param reference the object reference
     * @param state     the active state
     * @param service   the archetype service
     * @return the corresponding object, or {@code null} if none is found or doesn't match the state
     */
    private static IMObject resolve(Reference reference, State state, ArchetypeService service) {
        IMObject result = null;
        if (reference != null) {
            if (state == Policy.State.ANY) {
                result = service.get(reference);
            } else {
                boolean active = state == Policy.State.ACTIVE;
                result = service.get(reference, active);
            }
        }
        return result;
    }

    /**
     * Returns a function that returns the source or target of a relationship.
     *
     * @param source if {@code true}, return the source of the relationships, else return the target
     * @return a new function
     */
    private static <R extends Relationship> Function<R, Reference> getAccessor(boolean source) {
        return (source) ? R::getSource : R::getTarget;
    }

    /**
     * Returns a function that returns the related reference from a relationship.
     *
     * @param reference the reference to the object
     * @return a function that returns the reference related to {@code reference}
     */
    private static <R extends Relationship> Function<R, Reference> getAccessor(Reference reference) {
        return relationship -> {
            Reference target = relationship.getTarget();
            Reference related = null;
            if (target != null && !target.equals(reference)) {
                related = target;
            } else {
                Reference source = relationship.getSource();
                if (source != null && !source.equals(reference)) {
                    related = source;
                }
            }
            return related;
        };
    }

    /**
     * Fetches objects corresponding to a list of references.
     *
     * @param references the references
     * @param state      the active state
     * @return the objects
     */
    @SuppressWarnings("unchecked")
    private List<T> fetch(Collection<Reference> references, State state) {
        TypedQuery<? extends IMObject> query = newQuery(references, state).build();
        return new ArrayList<>((List<T>) query.getResultList());
    }

    /**
     * Returns a query builder that queries the specified references.
     *
     * @param references the references
     * @param state      the active state
     * @return a new query builder
     */
    @SuppressWarnings("unchecked")
    private QueryBuilder newQuery(Collection<Reference> references, State state) {
        Set<String> archetypes = references.stream().map(Reference::getArchetype).collect(Collectors.toSet());
        Set<ArchetypeDescriptor> descriptors = archetypes.stream().map(service::getArchetypeDescriptor)
                .collect(Collectors.toSet());
        Class<T> type = (Class<T>) TypeSet.getClass(descriptors);
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<? extends IMObject> query = builder.createQuery(type);
        Root<? extends IMObject> from = query.from(type, archetypes);
        List<javax.persistence.criteria.Predicate> predicates = new ArrayList<>();

        List<Long> ids = references.stream().map(Reference::getId).collect(Collectors.toList());
        predicates.add(from.get("id").in(ids));
        // TODO these aren't constrained by archetype. Can be fixed when OBF-289 is implemented.

        if (state != State.ANY) {
            boolean active = state == State.ACTIVE;
            predicates.add(builder.equal(from.get("active"), active));
        }
        query.where(predicates);
        return new QueryBuilder(query, from, builder);
    }

    /**
     * Query builder.
     */
    private class QueryBuilder {

        /**
         * The query.
         */
        private final CriteriaQuery<? extends IMObject> query;

        /**
         * The from-clause.
         */
        private final Root<? extends IMObject> from;

        /**
         * The builder.
         */
        private final CriteriaBuilder builder;

        /**
         * Constructs a {@link QueryBuilder}.
         *
         * @param query   the query
         * @param from    the from-clause
         * @param builder the builder
         */
        public QueryBuilder(CriteriaQuery<? extends IMObject> query, Root<? extends IMObject> from,
                            CriteriaBuilder builder) {
            this.query = query;
            this.from = from;
            this.builder = builder;
        }

        /**
         * Adds order by clauses.
         *
         * @param orderBy the order criteria
         * @return this
         */
        public QueryBuilder orderBy(Order... orderBy) {
            List<Order> list = new ArrayList<>(Arrays.asList(orderBy));
            if (list.stream().noneMatch(order -> "id".equals(order.getName()))) {
                list.add(Order.ascending("id"));  // make the query deterministic
            }
            List<javax.persistence.criteria.Order> criteria = new ArrayList<>();
            for (Order order : list) {
                Path<Object> expression = from.get(order.getName());
                criteria.add(order.isAscending() ? builder.asc(expression) : builder.desc(expression));
            }
            query.orderBy(criteria);
            return this;
        }

        /**
         * Builds the query.
         *
         * @return the query
         */
        public TypedQuery<? extends IMObject> build() {
            return service.createQuery(query);
        }
    }

    /**
     * An iterator over relationships that returns {@link ObjectRelationship} instances where the object is lazily
     * constructed.
     */
    private class LazyRelationshipIterator extends AdaptingIterator<R, ObjectRelationship<T, R>> {

        public LazyRelationshipIterator(Iterator<R> iterator) {
            super(iterator);
        }

        /**
         * Adapts an object.
         *
         * @param relationship the object to adapt
         * @return the object. May be {@code null}
         */
        @Override
        ObjectRelationship<T, R> adapt(R relationship) {
            Reference reference = accessor.apply(relationship);
            T object = (reference != null) ? resolver.apply(reference, getState()) : null;
            return object != null ? new ObjectRelationshipImpl<>(object, relationship) : null;
        }
    }

    /**
     * An iterator over references that returns {@link IMObject} instances where the object is lazily loaded.
     */
    private class LazyObjectIterator extends AdaptingIterator<Reference, T> {

        public LazyObjectIterator(Iterator<Reference> references) {
            super(references);
        }

        /**
         * Adapts an object.
         *
         * @param object the object to adapt
         * @return the object. May be {@code null}
         */
        @Override
        T adapt(Reference object) {
            return resolver.apply(object, getState());
        }
    }

    /**
     * An {@link Iterable} that given a list of {@code Reference}s, queries from {@code firstResult} up
     * to {@code maxResults} corresponding IMObjects.
     */
    private class PagingReferenceIterable extends PagingIterable<Reference, T> {

        public PagingReferenceIterable(List<Reference> references, State state, int firstResult, int maxResults) {
            super(references, firstResult, maxResults, state);
        }

        /**
         * Returns the list of objects corresponding to the supplied references.
         * <p/>
         * The query may return fewer objects than requested.
         *
         * @param list  the list to query
         * @param state the active state
         * @return the adapted objects
         */
        @Override
        protected List<T> query(List<Reference> list, State state) {
            return fetch(list, state);
        }
    }

    /**
     * An {@link Iterable} that given a list of {@link Relationship}s, queries from {@code firstResult} up
     * to {@code maxResults} corresponding {@link ObjectRelationship}s.
     */
    private class PagingObjectRelationIterable extends PagingIterable<R, ObjectRelationship<T, R>> {

        public PagingObjectRelationIterable(List<R> list, int firstResult, int maxResults, State state) {
            super(list, firstResult, maxResults, state);
        }

        /**
         * Query the list, adapting them to instances of {@code ObjectRelationship}.
         *
         * @param list  the list to query
         * @param state the active state
         * @return the adapted objects
         */
        @Override
        protected List<ObjectRelationship<T, R>> query(List<R> list, State state) {
            List<ObjectRelationship<T, R>> result = new ArrayList<>();
            Set<Reference> references = list.stream().map(accessor).collect(Collectors.toSet());
            Map<Reference, T> matches = new HashMap<>();
            List<T> objects = fetch(references, state);
            objects.forEach(object -> matches.put(object.getObjectReference(), object));
            for (R relationship : list) {
                T match = matches.get(accessor.apply(relationship));
                if (match != null) {
                    result.add(new ObjectRelationshipImpl<>(match, relationship));
                }
            }
            return result;
        }
    }

    private class SortableIterable implements Iterable<T> {

        /**
         * The list to query.
         */
        private final List<Reference> list;

        /**
         * The first result in the list.
         */
        private final int firstResult;

        /**
         * The maximum number of results to return.
         */
        private final int maxResults;

        /**
         * The active state.
         */
        private final State state;

        /**
         * The order criteria.
         */
        private final Order[] order;

        public SortableIterable(List<Reference> list, State state, int firstResult, int maxResults,
                                Order... order) {
            if (maxResults < 1) {
                throw new IllegalArgumentException("Argument 'maxResults' must be positive");
            }
            this.list = list;
            this.state = state;
            this.firstResult = firstResult;
            this.maxResults = maxResults;
            this.order = order;
        }

        /**
         * Returns an iterator over the elements.
         *
         * @return an iterator
         */
        @Override
        @SuppressWarnings("unchecked")
        public Iterator<T> iterator() {
            TypedQuery<? extends IMObject> query = newQuery(list, state)
                    .orderBy(order)
                    .build();
            List<T> results = (List<T>) query.setFirstResult(firstResult)
                    .setMaxResults(maxResults)
                    .getResultList();
            return results.iterator();
        }
    }

    /**
     * An {@link Iterable} that given a list of {@code X}, queries from {@code firstResult} up to {@code maxResults}
     * objects, adapting them to a list of {@code Y}.
     * <p/>
     * This is expected to be more efficient than retrieving objects individually.
     */
    private static abstract class PagingIterable<X, Y> implements Iterable<Y> {

        /**
         * The list to query.
         */
        private final List<X> list;

        /**
         * The first result in the list.
         */
        private final int firstResult;

        /**
         * The maximum number of results to return.
         */
        private final int maxResults;

        /**
         * The active state.
         */
        private final State state;

        /**
         * Constructs a {@link PagingIterable}.
         *
         * @param list        the list to query
         * @param firstResult the first result in the list
         * @param maxResults  the maximum number of results to return
         * @param state       the active state
         */
        public PagingIterable(List<X> list, int firstResult, int maxResults, State state) {
            this.list = list;
            this.state = state;
            this.firstResult = firstResult;
            this.maxResults = maxResults;
            if (maxResults < 1) {
                throw new IllegalArgumentException("Argument 'maxResults' must be positive");
            }
        }

        /**
         * Returns an iterator over the elements.
         *
         * @return an iterator
         */
        @Override
        public Iterator<Y> iterator() {
            List<Y> result = new ArrayList<>();
            int size = list.size();
            int first = firstResult;
            int read = 0;
            while (first < size && read < maxResults) {
                int last = first + (maxResults - read);
                if (last > size) {
                    last = size;
                }
                if (last > first) {
                    List<X> subList = list.subList(first, last);
                    result.addAll(query(subList, state));
                    read = result.size();
                    first = last;
                }
            }
            return result.iterator();
        }

        /**
         * Query the list, adapting them to instances of {@code Y}.
         * <p/>
         * The query may return fewer objects than requested.
         *
         * @param list  the list to query
         * @param state the active state
         * @return the adapted objects
         */
        protected abstract List<Y> query(List<X> list, State state);
    }

    /**
     * An iterator that adapts the results of an iterator to a different type.
     */
    private abstract static class AdaptingIterator<A, B> implements Iterator<B> {

        /**
         * The iterator to adapt.
         */
        private final Iterator<A> iterator;

        /**
         * The next object to return.
         */
        private B next;

        /**
         * Constructs an {@link AdaptingIterator}.
         *
         * @param iterator the iterator to adapt
         */
        protected AdaptingIterator(Iterator<A> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            if (next == null) {
                next = resolve();
            }
            return next != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public B next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            B result = next;
            next = null;
            return result;
        }

        /**
         * Adapts an object.
         * <p/>
         * NOTE: if the returned object is {@code null}, it will be skipped in the iteration. i.e. {@link #next}
         * will never return {@code null}.
         *
         * @param object the object to adapt
         * @return the object. May be {@code null}
         */
        abstract B adapt(A object);

        /**
         * Resolves the next object.
         *
         * @return the next object, or {@code null} if there is none
         */
        private B resolve() {
            while (next == null && iterator.hasNext()) {
                next = adapt(iterator.next());
            }
            return next;
        }
    }
}
