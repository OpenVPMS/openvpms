/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.domain.im.archetype.descriptor;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.system.common.util.ClassHelper;

/**
 * All the descriptor classes inherit from this base class, which provides
 * support for identity, hibernate and serialization.
 *
 * @author Jim Alateras
 * @author Tim Anderson
 */
public abstract class Descriptor extends IMObject {

    /**
     * An enumeration of different descriptor types
     */
    public enum DescriptorType {
        ArchetypeDescriptor,
        NodeDescriptor,
        AssertionDescriptor,
        PropertyDescriptor,
        AssertionTypeDescriptor
    }

    /**
     * An enumeration of the different validation errors
     */
    public enum ValidationError {
        IsRequired,
        DuplicateNodeDescriptor,
        NodeNotFound
    }

    /**
     * Default constructor
     */
    protected Descriptor() {
        super();
    }

    /* (non-Javadoc)
     * @see org.openvpms.component.business.domain.im.common.IMObject#clone()
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Returns the class for the specified class name.
     *
     * @param className may be null/empty
     * @return the class, or {@code null} if {@code className} null/empty
     * @throws DescriptorException if the class can't be loaded
     */
    protected Class<?> getClass(String className) {
        Class<?> result = null;
        if (!StringUtils.isEmpty(className)) {
            try {
                result = ClassHelper.getClass(className);
            } catch (Exception exception) {
                throw new DescriptorException(DescriptorException.ErrorCode.InvalidType, exception, getName(),
                                              className);
            }
        }
        return result;
    }
}
