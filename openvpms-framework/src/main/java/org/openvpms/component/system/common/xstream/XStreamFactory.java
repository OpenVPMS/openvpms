/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.basic.BigDecimalConverter;
import com.thoughtworks.xstream.converters.basic.BigIntegerConverter;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;
import com.thoughtworks.xstream.converters.basic.ByteConverter;
import com.thoughtworks.xstream.converters.basic.CharConverter;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.converters.basic.DoubleConverter;
import com.thoughtworks.xstream.converters.basic.FloatConverter;
import com.thoughtworks.xstream.converters.basic.IntConverter;
import com.thoughtworks.xstream.converters.basic.LongConverter;
import com.thoughtworks.xstream.converters.basic.NullConverter;
import com.thoughtworks.xstream.converters.basic.ShortConverter;
import com.thoughtworks.xstream.converters.basic.StringBufferConverter;
import com.thoughtworks.xstream.converters.basic.StringBuilderConverter;
import com.thoughtworks.xstream.converters.basic.StringConverter;
import com.thoughtworks.xstream.converters.basic.URIConverter;
import com.thoughtworks.xstream.converters.basic.URLConverter;
import com.thoughtworks.xstream.converters.basic.UUIDConverter;
import com.thoughtworks.xstream.converters.collections.ArrayConverter;
import com.thoughtworks.xstream.converters.collections.BitSetConverter;
import com.thoughtworks.xstream.converters.collections.CharArrayConverter;
import com.thoughtworks.xstream.converters.collections.CollectionConverter;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.converters.collections.SingletonCollectionConverter;
import com.thoughtworks.xstream.converters.collections.SingletonMapConverter;
import com.thoughtworks.xstream.converters.enums.EnumConverter;
import com.thoughtworks.xstream.converters.extended.ActivationDataFlavorConverter;
import com.thoughtworks.xstream.converters.extended.CharsetConverter;
import com.thoughtworks.xstream.converters.extended.ColorConverter;
import com.thoughtworks.xstream.converters.extended.CurrencyConverter;
import com.thoughtworks.xstream.converters.extended.DurationConverter;
import com.thoughtworks.xstream.converters.extended.EncodedByteArrayConverter;
import com.thoughtworks.xstream.converters.extended.FileConverter;
import com.thoughtworks.xstream.converters.extended.GregorianCalendarConverter;
import com.thoughtworks.xstream.converters.extended.JavaClassConverter;
import com.thoughtworks.xstream.converters.extended.JavaFieldConverter;
import com.thoughtworks.xstream.converters.extended.JavaMethodConverter;
import com.thoughtworks.xstream.converters.extended.LocaleConverter;
import com.thoughtworks.xstream.converters.extended.LookAndFeelConverter;
import com.thoughtworks.xstream.converters.extended.PathConverter;
import com.thoughtworks.xstream.converters.extended.RegexPatternConverter;
import com.thoughtworks.xstream.converters.extended.SqlDateConverter;
import com.thoughtworks.xstream.converters.extended.SqlTimeConverter;
import com.thoughtworks.xstream.converters.extended.SqlTimestampConverter;
import com.thoughtworks.xstream.converters.extended.StackTraceElementConverter;
import com.thoughtworks.xstream.converters.extended.SubjectConverter;
import com.thoughtworks.xstream.converters.extended.ThrowableConverter;
import com.thoughtworks.xstream.converters.reflection.ExternalizableConverter;
import com.thoughtworks.xstream.converters.reflection.LambdaConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.SerializableConverter;
import com.thoughtworks.xstream.converters.time.ChronologyConverter;
import com.thoughtworks.xstream.converters.time.HijrahDateConverter;
import com.thoughtworks.xstream.converters.time.InstantConverter;
import com.thoughtworks.xstream.converters.time.JapaneseDateConverter;
import com.thoughtworks.xstream.converters.time.JapaneseEraConverter;
import com.thoughtworks.xstream.converters.time.LocalDateConverter;
import com.thoughtworks.xstream.converters.time.LocalDateTimeConverter;
import com.thoughtworks.xstream.converters.time.LocalTimeConverter;
import com.thoughtworks.xstream.converters.time.MinguoDateConverter;
import com.thoughtworks.xstream.converters.time.MonthDayConverter;
import com.thoughtworks.xstream.converters.time.OffsetDateTimeConverter;
import com.thoughtworks.xstream.converters.time.OffsetTimeConverter;
import com.thoughtworks.xstream.converters.time.PeriodConverter;
import com.thoughtworks.xstream.converters.time.SystemClockConverter;
import com.thoughtworks.xstream.converters.time.ThaiBuddhistDateConverter;
import com.thoughtworks.xstream.converters.time.ValueRangeConverter;
import com.thoughtworks.xstream.converters.time.WeekFieldsConverter;
import com.thoughtworks.xstream.converters.time.YearConverter;
import com.thoughtworks.xstream.converters.time.YearMonthConverter;
import com.thoughtworks.xstream.converters.time.ZoneIdConverter;
import com.thoughtworks.xstream.converters.time.ZonedDateTimeConverter;
import com.thoughtworks.xstream.core.ClassLoaderReference;
import com.thoughtworks.xstream.core.JVM;
import com.thoughtworks.xstream.core.util.SelfStreamingInstanceChecker;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

import java.util.TimeZone;

/**
 * Factory for {@link XStream} instances.
 * <p/>
 * This suppresses registration of any {@link Converter} which causes Java 11 to log warnings about illegal
 * reflective access. E.g.:
 * <p/>
 * {@code
 * WARNING: An illegal reflective access operation has occurred
 * WARNING: Illegal reflective access by com.thoughtworks.xstream.core.util.Fields to field
 * java.util.Properties.defaults
 * }
 * The converters that are suppressed are:
 * <ul>
 *     <li>DynamicProxyConverter</li>
 *     <li>EnumSetConverter</li>
 *     <li>EnumMapConverter</li>
 *     <li>FontConverter</li>
 *     <li>PropertiesConverter</li>
 *     <li>TextAttributeConverter</li>
 *     <li>TreeMapConverter</li>
 *     <li>TreeSetConverter</li>
 * </ul>
 * <p>
 * The classes that these convert can presumably still be converted by {@link ReflectionConverter}, but the
 * conversion will be incomplete.
 *
 * @author Tim Anderson
 */
public class XStreamFactory {

    /**
     * Creates an {@link XStream}.
     * <p/>
     * This calls {@link XStream#setupDefaultSecurity} to initialise security.
     *
     * @return a new {@link XStream}.
     */
    public static XStream create() {
        XStream result = newInstance();
        XStream.setupDefaultSecurity(result);
        return result;
    }

    /**
     * Creates an {@link XStream} that supports serialisation of the default types from
     * {@link XStream#setupDefaultSecurity} as well as those specified.
     *
     * @param types the additional types to seralise
     * @return a new {@link XStream}.
     */
    public static XStream create(Class<?>... types) {
        XStream result = create();
        result.allowTypes(types);
        return result;
    }

    /**
     * Creates a new {@link XStream} innstance, with those converters that use
     * {@link com.thoughtworks.xstream.core.util.Fields} disabled.
     *
     * @return a new {@link XStream}.
     */
    private static XStream newInstance() {
        return new XStream() {
            @Override
            protected void setupConverters() {
                // this is largely copied from the the parent implementation.
                Mapper mapper = getMapper();
                ReflectionProvider reflectionProvider = getReflectionProvider();
                ClassLoaderReference classLoaderReference = getClassLoaderReference();
                registerConverter(new ReflectionConverter(mapper, reflectionProvider), PRIORITY_VERY_LOW);

                registerConverter(new SerializableConverter(mapper, reflectionProvider,
                                                            classLoaderReference), PRIORITY_LOW);
                registerConverter(new ExternalizableConverter(mapper, classLoaderReference), PRIORITY_LOW);
                registerConverter(new InternalBlackList(), PRIORITY_LOW);

                registerConverter(new NullConverter(), PRIORITY_VERY_HIGH);
                registerConverter(new IntConverter(), PRIORITY_NORMAL);
                registerConverter(new FloatConverter(), PRIORITY_NORMAL);
                registerConverter(new DoubleConverter(), PRIORITY_NORMAL);
                registerConverter(new LongConverter(), PRIORITY_NORMAL);
                registerConverter(new ShortConverter(), PRIORITY_NORMAL);
                registerConverter((Converter) new CharConverter(), PRIORITY_NORMAL);
                registerConverter(new BooleanConverter(), PRIORITY_NORMAL);
                registerConverter(new ByteConverter(), PRIORITY_NORMAL);

                registerConverter(new StringConverter(), PRIORITY_NORMAL);
                registerConverter(new StringBufferConverter(), PRIORITY_NORMAL);
                registerConverter(new DateConverter(), PRIORITY_NORMAL);
                registerConverter(new BitSetConverter(), PRIORITY_NORMAL);
                registerConverter(new URIConverter(), PRIORITY_NORMAL);
                registerConverter(new URLConverter(), PRIORITY_NORMAL);
                registerConverter(new BigIntegerConverter(), PRIORITY_NORMAL);
                registerConverter(new BigDecimalConverter(), PRIORITY_NORMAL);

                registerConverter(new ArrayConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new CharArrayConverter(), PRIORITY_NORMAL);
                registerConverter(new CollectionConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new MapConverter(mapper), PRIORITY_NORMAL);
                // registerConverter(new TreeMapConverter(mapper), PRIORITY_NORMAL);
                // registerConverter(new TreeSetConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new SingletonCollectionConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new SingletonMapConverter(mapper), PRIORITY_NORMAL);
                // registerConverter(new PropertiesConverter(), PRIORITY_NORMAL);
                registerConverter((Converter) new EncodedByteArrayConverter(), PRIORITY_NORMAL);

                registerConverter(new FileConverter(), PRIORITY_NORMAL);
                if (JVM.isSQLAvailable()) {
                    // NOTE: for historical reasons, Timestamp is stored in local time rather than UTC
                    registerConverter(new SqlTimestampConverter(TimeZone.getDefault()), PRIORITY_NORMAL);
                    registerConverter(new SqlTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new SqlDateConverter(), PRIORITY_NORMAL);
                }
                // registerConverter(new DynamicProxyConverter(mapper, classLoaderReference), PRIORITY_NORMAL);
                registerConverter(new JavaClassConverter(classLoaderReference), PRIORITY_NORMAL);
                registerConverter(new JavaMethodConverter(classLoaderReference), PRIORITY_NORMAL);
                registerConverter(new JavaFieldConverter(classLoaderReference), PRIORITY_NORMAL);

                if (JVM.isAWTAvailable()) {
                    // registerConverter(new FontConverter(mapper), PRIORITY_NORMAL);
                    registerConverter(new ColorConverter(), PRIORITY_NORMAL);
                    // registerConverter(new TextAttributeConverter(), PRIORITY_NORMAL);
                }
                if (JVM.isSwingAvailable()) {
                    registerConverter(
                            new LookAndFeelConverter(mapper, reflectionProvider), PRIORITY_NORMAL);
                }

                // JVM 4
                registerConverter(new LocaleConverter(), PRIORITY_NORMAL);
                registerConverter(new GregorianCalendarConverter(), PRIORITY_NORMAL);

                registerConverter(new SubjectConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new ThrowableConverter(getConverterLookup()), PRIORITY_NORMAL);
                registerConverter(new StackTraceElementConverter(), PRIORITY_NORMAL);
                registerConverter(new CurrencyConverter(), PRIORITY_NORMAL);
                registerConverter(new RegexPatternConverter(), PRIORITY_NORMAL);
                registerConverter(new CharsetConverter(), PRIORITY_NORMAL);

                // JVM 5
                registerConverter(new DurationConverter(), PRIORITY_NORMAL);
                registerConverter(new EnumConverter(), PRIORITY_NORMAL);
                // registerConverter(new EnumSetConverter(mapper), PRIORITY_NORMAL);
                // registerConverter(new EnumMapConverter(mapper), PRIORITY_NORMAL);
                registerConverter(new StringBuilderConverter(), PRIORITY_NORMAL);
                registerConverter(new UUIDConverter(), PRIORITY_NORMAL);

                if (JVM.loadClassForName("javax.activation.ActivationDataFlavor") != null) {
                    registerConverter(new ActivationDataFlavorConverter(), PRIORITY_NORMAL);
                }
                // JVM 7
                if (JVM.isVersion(7)) {
                    registerConverter(new PathConverter(), PRIORITY_NORMAL);
                }
                // JVM 8
                if (JVM.isVersion(8)) {
                    registerConverter(new ChronologyConverter(), PRIORITY_NORMAL);
                    registerConverter(new com.thoughtworks.xstream.converters.time.DurationConverter(),
                                      PRIORITY_NORMAL);
                    registerConverter(new HijrahDateConverter(), PRIORITY_NORMAL);
                    registerConverter(new JapaneseDateConverter(), PRIORITY_NORMAL);
                    registerConverter(new JapaneseEraConverter(), PRIORITY_NORMAL);
                    registerConverter(new InstantConverter(), PRIORITY_NORMAL);
                    registerConverter(new LocalDateConverter(), PRIORITY_NORMAL);
                    registerConverter(new LocalDateTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new LocalTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new MinguoDateConverter(), PRIORITY_NORMAL);
                    registerConverter(new MonthDayConverter(), PRIORITY_NORMAL);
                    registerConverter(new OffsetDateTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new OffsetTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new PeriodConverter(), PRIORITY_NORMAL);
                    registerConverter(new SystemClockConverter(mapper), PRIORITY_NORMAL);
                    registerConverter(new ThaiBuddhistDateConverter(), PRIORITY_NORMAL);
                    registerConverter(new ValueRangeConverter(mapper), PRIORITY_NORMAL);
                    registerConverter(new WeekFieldsConverter(mapper), PRIORITY_NORMAL);
                    registerConverter(new YearConverter(), PRIORITY_NORMAL);
                    registerConverter(new YearMonthConverter(), PRIORITY_NORMAL);
                    registerConverter(new ZonedDateTimeConverter(), PRIORITY_NORMAL);
                    registerConverter(new ZoneIdConverter(), PRIORITY_NORMAL);
                    registerConverter(new LambdaConverter(mapper, reflectionProvider, classLoaderReference),
                                      PRIORITY_NORMAL);
                }

                registerConverter(new SelfStreamingInstanceChecker(getConverterLookup(), this), PRIORITY_NORMAL);
            }
        };
    }

    private static class InternalBlackList implements Converter {

        public boolean canConvert(final Class type) {
            return (type == void.class || type == Void.class)
                   || (type != null
                       && (type.getName().equals("java.beans.EventHandler")
                           || type.getName().endsWith("$LazyIterator")
                           || type.getName().startsWith("javax.crypto.")));
        }

        public void marshal(final Object source, final HierarchicalStreamWriter writer,
                            final MarshallingContext context) {
            throw new ConversionException("Security alert. Marshalling rejected.");
        }

        public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context) {
            throw new ConversionException("Security alert. Unmarshalling rejected.");
        }
    }

}
