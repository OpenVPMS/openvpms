/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.util;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Executes {@link Runnable} and {@link Supplier} instances associated with a key, blocking concurrent execution of
 * those with the same key.<br/>
 * These are forced to run sequentially.
 * <p/>
 * This can be used to support concurrent operations on different pieces of data, but prevent concurrency on the same
 * piece of data.
 *
 * @author Tim Anderson
 */
public class BlockingExecutor<T> {

    /**
     * The keys currently in use.
     */
    private final Set<T> keys = new HashSet<>();

    /**
     * Runs a command with the given key.
     * <p/>
     * If multiple threads are executing with the same key, these will be single-threaded.
     *
     * @param key     the key
     * @param command the command to run
     * @throws InterruptedException if interrupted getting a lock for the key
     */
    public void run(T key, Runnable command) throws InterruptedException {
        try {
            lock(key);
            command.run();
        } finally {
            unlock(key);
        }
    }

    /**
     * Calls a supplier for the given key.
     * <p/>
     * If multiple threads are executing with the same key, these will be single-threaded.
     *
     * @param key      the key
     * @param supplier the supplier to call
     * @return the result of the supplier
     * @throws InterruptedException if interrupted getting a lock for the key
     */
    public <V> V get(T key, Supplier<V> supplier) throws InterruptedException {
        V result;
        try {
            lock(key);
            result = supplier.get();
        } finally {
            unlock(key);
        }
        return result;
    }

    /**
     * Acquires a lock for the given key.
     *
     * @param key the key
     * @throws InterruptedException if interrupted getting a lock for the key
     */
    private void lock(T key) throws InterruptedException {
        synchronized (keys) {
            while (!keys.add(key)) {
                keys.wait();
            }
        }
    }

    /**
     * Removes the lock for the given key.
     *
     * @param key the key
     */
    private void unlock(T key) {
        synchronized (keys) {
            keys.remove(key);
            keys.notifyAll();
        }
    }
}