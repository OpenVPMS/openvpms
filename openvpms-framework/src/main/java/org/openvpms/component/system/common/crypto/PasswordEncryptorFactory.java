package org.openvpms.component.system.common.crypto;

import org.openvpms.component.security.crypto.PasswordEncryptor;

/**
 * Factory for {@link PasswordEncryptor}s.
 *
 * @author Tim Anderson
 */
public interface PasswordEncryptorFactory {

    /**
     * Creates a password encryptor.
     *
     * @return a new password encryptor
     */
    PasswordEncryptor create();
}
