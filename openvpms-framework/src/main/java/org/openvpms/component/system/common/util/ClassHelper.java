/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.util;

import org.apache.commons.lang3.ClassUtils;

import java.util.function.Supplier;

/**
 * Class helper methods.
 *
 * @author Tim Anderson
 */
public class ClassHelper {

    /**
     * Default constructor.
     */
    private ClassHelper() {
        // no-op
    }

    /**
     * Loads a class.
     * <p/>
     * This uses the thread context class loader first. If the class is not found, it falls back to the class loader
     * responsible for loading this class.
     *
     * @param name the class name
     * @return the corresponding class
     * @throws ClassNotFoundException if the class was not found
     */
    public static Class<?> getClass(String name) throws ClassNotFoundException {
        Class<?> result = null;
        // use the context ClassLoader if one is present, falling back to the ClassLoader used to
        // load this class if no context ClassLoader exists, or the class cannot be found
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null) {
            try {
                result = ClassUtils.getClass(classLoader, name);
            } catch (ClassNotFoundException exception) {
                // no-op
            }
        }
        if (result == null) {
            result = ClassUtils.getClass(ClassHelper.class.getClassLoader(), name);
        }
        return result;
    }

    /**
     * Invokes a {@link Runnable} with the context class loader set to that supplied.
     *
     * @param loader   the class loader
     * @param runnable the runnable to execute
     */
    public static void invoke(ClassLoader loader, Runnable runnable) {
        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(loader);
            runnable.run();
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
    }

    /**
     * Invokes a {@link Supplier} with the context class loader set to that supplied.
     *
     * @param loader the class loader
     * @param call   the call to execute
     * @return the result of the invocation
     */
    public static <T> T invoke(ClassLoader loader, Supplier<T> call) {
        ClassLoader current = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(loader);
            return call.get();
        } finally {
            Thread.currentThread().setContextClassLoader(current);
        }
    }

}
