/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.jxpath;

import org.openvpms.component.model.object.IMObject;

import java.util.List;

/**
 * JXPath function helper.
 *
 * @author Tim Anderson
 */
public class FunctionHelper {

    /**
     * Helper to get access to the actual object supplied by JXPath.
     * <p/>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object the object to unwrap
     * @return the unwrapped object. May be {@code null}
     */
    public static Object unwrap(Object object) {
        if (object instanceof List) {
            List<?> values = (List<?>) object;
            object = !values.isEmpty() ? values.get(0) : null;
        }
        return object;
    }

    /**
     * Helper to get access to the actual object supplied by JXPath.
     * <p>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object the object to unwrap
     * @param type   the expected object type
     * @return the unwrapped object, or {@code null} if none was supplied, or it is of the wrong type
     */
    public static <T extends IMObject> T unwrap(Object object, Class<T> type) {
        object = unwrap(object);
        return (object != null && type.isAssignableFrom(object.getClass())) ? type.cast(object) : null;
    }

    /**
     * Helper to get access to the actual object supplied by JXPath.
     * <p>
     * This is a workaround to allow functions to be supplied null arguments, which JXPath handles by wrapping in a
     * list.
     *
     * @param object    the object to unwrap
     * @param type      the expected object type
     * @param archetype the expected archetype
     * @return the unwrapped object, or {@code null} if none was supplied, or it is of the wrong type/archetype
     */
    public static <T extends IMObject> T unwrap(Object object, Class<T> type, String archetype) {
        T result = unwrap(object, type);
        return (result != null && result.isA(archetype)) ? result : null;
    }
}
