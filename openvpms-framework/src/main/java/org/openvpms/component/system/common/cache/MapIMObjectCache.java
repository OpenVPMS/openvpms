/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.cache;

import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.HashMap;
import java.util.Map;


/**
 * Implementation of the {@link IMObjectCache} interface that is backed by a {@code Map}.
 * <p>
 * This cache grows until cleared.
 *
 * @author Tim Anderson
 */
public class MapIMObjectCache extends AbstractIMObjectCache {

    /**
     * Constructs a {@link MapIMObjectCache}.
     */
    public MapIMObjectCache() {
        super(new HashMap<>(), null);
    }

    /**
     * Constructs a {@link MapIMObjectCache} that will retrieve objects from the archetype service if they are
     * not cached.
     *
     * @param service the archetype service
     */
    public MapIMObjectCache(ArchetypeService service) {
        super(new HashMap<>(), service);
    }

    /**
     * Constructs a {@link MapIMObjectCache} that will retrieve objects from the archetype service if they are
     * not cached.
     *
     * @param map     the underlying map
     * @param service the archetype service
     */
    public MapIMObjectCache(Map<Object, IMObject> map, ArchetypeService service) {
        super(map, service);
    }

    /**
     * Constructs a {@link MapIMObjectCache}.
     *
     * @param graph   the graph to pre-populate the cache from
     * @param service the archetype service
     */
    public MapIMObjectCache(IMObjectGraph graph, ArchetypeService service) {
        super(new HashMap<>(), service);
        for (IMObject object : graph.getObjects()) {
            add(object);
        }
    }
}
