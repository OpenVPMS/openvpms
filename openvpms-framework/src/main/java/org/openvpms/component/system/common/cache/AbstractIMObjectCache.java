/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.system.common.cache;

import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Map;
import java.util.Objects;


/**
 * Abstract implementation of the {@link IMObjectCache} interface.
 * <p>
 * Note that this implementation excludes {@link Document} instances which may be too large to cache for long periods.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectCache implements IMObjectCache {

    /**
     * The cache. This stores objects by Reference and by archetype:id as IMObjectReferences constructed with
     * an archetype, id, and linkId won't match those constructed with an archetype and id.
     */
    private final Map<Object, IMObject> cache;

    /**
     * The archetype service. May be {@code null}
     */
    private final ArchetypeService service;

    /**
     * Constructs an {@link AbstractIMObjectCache}
     *
     * @param cache   the cache. Note that size limited caches should allow for the fact that 2 keys are used for each
     *                cached object
     * @param service the archetype service. If non-null, non-cached objects may retrieved
     */
    protected AbstractIMObjectCache(Map<Object, IMObject> cache, ArchetypeService service) {
        this.cache = cache;
        this.service = service;
    }

    /**
     * Adds an object to the cache.
     *
     * @param object the object to cache
     */
    public synchronized void add(IMObject object) {
        Reference reference = object.getObjectReference();
        add(reference, object);
    }

    /**
     * Removes an object from the cache, if present.
     *
     * @param object the object to remove
     */
    public synchronized void remove(IMObject object) {
        Reference reference = object.getObjectReference();
        cache.remove(reference);
        if (!reference.isNew()) {
            cache.remove(new SecondaryKey(reference));
        }
    }

    /**
     * Returns an object given its reference.
     * <p>
     * If the object isn't cached, it will be retrieved from the archetype service and added to the cache if it exists.
     *
     * @param reference the object reference. May be {@code null}
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    public synchronized IMObject get(Reference reference) {
        return retrieve(reference, null);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference
     * @param type      the expected type of the object
     * @return the object, or {@code null} if none is found
     * @throws ClassCastException if the resulting object is not of the expected type
     */
    @Override
    public <T extends IMObject> T get(Reference reference, Class<T> type) {
        return type.cast(get(reference));
    }

    /**
     * Returns an object given its reference.
     * <p>
     * If the object isn't cached, it may be retrieved.
     *
     * @param reference the object reference. May be {@code null}
     * @param active    if {@code true}, only return the object if it is active. If {@code false}, only return the
     *                  object if it is inactive
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    @Override
    public synchronized IMObject get(Reference reference, boolean active) {
        return retrieve(reference, active);
    }

    /**
     * Returns an object, but only if it is cached.
     *
     * @param reference the object reference. May be {@code null}
     * @return the object corresponding to {@code reference} or {@code null} if none is cached
     */
    @Override
    public synchronized IMObject getCached(Reference reference) {
        IMObject result = null;
        if (reference != null) {
            result = cache.get(reference);
            if (result == null && !reference.isNew()) {
                result = cache.get(new SecondaryKey(reference));
            }
        }
        return result;
    }

    /**
     * Clears the cache.
     */
    @Override
    public synchronized void clear() {
        cache.clear();
    }

    /**
     * Adds an object to the cache.
     * <p/>
     * This keys it by reference, and for persistent objects, a secondary key constructed from the objects archetype
     * and id.
     *
     * @param reference the object reference
     * @param object    the object
     */
    private void add(Reference reference, IMObject object) {
        cache.put(reference, object);
        if (!reference.isNew()) {
            cache.put(new SecondaryKey(reference), object);
        }
    }

    /**
     * Returns an object given its reference.
     * <p>
     * If the object isn't cached, it may be retrieved.
     *
     * @param reference the object reference. May be {@code null}
     * @param active    if {@code null}, an object of any active status may be returned.
     *                  if {@code true}, only return the object if it is active.
     *                  If {@code false}, only return the object if it is inactive
     * @return the object corresponding to {@code reference} or {@code null} if none exists or the active flag does not
     * match
     */
    private IMObject retrieve(Reference reference, Boolean active) {
        IMObject result = null;
        if (reference != null) {
            result = getCached(reference);
            if (result != null) {
                if (active != null && result.isActive() != active) {
                    result = null;
                }
            } else if (service != null) {
                result = (active == null) ? service.get(reference) : service.get(reference, active);
                if (result != null && !(result instanceof Document)) {
                    add(reference, result);
                }
            }
        }
        return result;
    }

    /**
     * A secondary key for an {@link IMObject} that uses its id and archetype.
     */
    private static final class SecondaryKey {

        /**
         * The reference.
         */
        private final Reference reference;

        /**
         * Constructs {@link SecondaryKey}.
         *
         * @param reference the object reference
         */
        public SecondaryKey(Reference reference) {
            this.reference = reference;
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise
         */
        @Override
        public boolean equals(Object obj) {
            boolean result = false;
            if (obj instanceof SecondaryKey) {
                Reference other = ((SecondaryKey) obj).reference;
                result = reference.equals(other.getArchetype(), other.getId());
            }
            return result;
        }

        /**
         * Returns a hash code value for the object.
         *
         * @return a hash code value for this object
         */
        @Override
        public int hashCode() {
            return Objects.hash(reference.getArchetype(), reference.getId());
        }
    }

}
