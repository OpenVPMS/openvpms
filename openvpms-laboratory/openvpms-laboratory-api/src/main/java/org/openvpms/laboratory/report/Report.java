/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.component.model.document.Document;

import java.util.List;

/**
 * A report contains the results of a lab order.
 *
 * @author Tim Anderson
 */
public interface Report {

    enum Status {

        PENDING,             // testing has not started
        WAITING_FOR_SAMPLE,  // the laboratory is waiting for a sample
        IN_PROGRESS,         // tests are in progress
        PARTIAL_RESULTS,     // partial results are available
        COMPLETED            // results are complete
    }

    /**
     * Returns the identity for the report, issued by the laboratory.
     *
     * @return the results identifier, or {@code null} if none has been issued
     */
    String getReportId();

    /**
     * Returns the report status.
     *
     * @return the report status
     */
    Status getStatus();

    /**
     * Returns the synchronisation identifier.
     * <p/>
     * This can be used to filter duplicate results if a failure occurs notifying the laboratory that results have been
     * received.
     *
     * @return the synchronisation identifier, or {@code null} if none has been set
     */
    String getSynchronisationId();

    /**
     * Returns the report summary.
     *
     * @return the report summary. May be {@code null}
     */
    String getSummary();

    /**
     * Returns the results.
     *
     * @return the results, in the order they were added
     */
    List<Results> getResults();

    /**
     * Returns the report document.
     *
     * @return the document, or {@code null} if the report has no document.
     */
    Document getDocument();

    /**
     * Determines if the report has external results.
     *
     * @return {@code true} if the report has external results
     */
    boolean hasExternalResults();
}
