/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.sync.Changes;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Document;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.order.OrderConfirmation;
import org.openvpms.laboratory.report.ExternalResults;

/**
 * Laboratory service.
 *
 * @author Tim Anderson
 */
public interface LaboratoryService {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     * @throws LaboratoryException for any error
     */
    String getName();

    /**
     * Returns the laboratory archetype that this supports.
     *
     * @return an <em>entity.laboratoryService*</em> archetype
     * @throws LaboratoryException for any error
     */
    String getLaboratoryArchetype();

    /**
     * Determines if orders for a test require confirmation after {@link #order(Order)}.
     *
     * @param test the test
     * @return {@code true} if orders require confirmation, otherwise {@code false}
     */
    boolean confirmOrders(Test test);

    /**
     * Validates an order, prior to its submission.
     * <p/>
     * Note:
     * <ul>
     *     <li>the order cannot be changed</li>
     *     <li>the service should not maintain any state with respect to the order, as the order may be discarded</li>
     *     <li>the values returned by {@link Order#getUUID()} and {@link Order#getCreated()} do not reflect what
     *      will be submitted to {@link #order(Order)}.
     * </ul>
     *
     * @param order the order
     * @return the validation status
     * @throws LaboratoryException for any error
     */
    OrderValidationStatus validate(Order order);

    /**
     * Place an order.
     * <p/>
     * For services that support:
     * <ul>
     * <li>synchronous submission, the {@link Order#setStatus(Order.Status) status} should be set to
     * {@link Order.Status#SUBMITTED SUBMITTED}, indicating that the order has been placed.</li>
     * <li>asynchronous submission, the {@link Order#setStatus(Order.Status) status} should be set to
     * {@link Order.Status#SUBMITTING SUBMITING}, and updated to {@link Order.Status#SUBMITTED SUBMITTED} on
     * successful delivery</li>
     * <li>order confirmation, the {@link Order#setStatus(Order.Status) status} should be set to
     * {@link Order.Status#CONFIRM CONFIRM}, indicating that {@link #getOrderConfirmation(Order)} should be used
     * to confirm the order</li>
     * </ul>
     * If the order cannot be submitted due to a temporary error, an exception should be
     * thrown. It will subsequently be resubmitted.<p/>
     * If the order cannot be submitted due to some permanent error (e.g. if the order is invalid), the
     * {@link Order#setStatus(Order.Status, String) status} should be set to {@link Order.Status#ERROR ERROR},
     * and a diagnostic message included.<br/>
     * An order with {@link Order.Status#ERROR ERROR} may only be cancelled.
     *
     * @param order the order to place
     * @throws LaboratoryException for any error
     */
    void order(Order order);

    /**
     * Invoked when an order has {@link Order.Status#CONFIRM}, to enable the user to confirm the order.
     *
     * @param order the order
     * @return the order confirmation, or {@code null} if no order confirmation is required
     * @throws LaboratoryException for any error
     */
    OrderConfirmation getOrderConfirmation(Order order);

    /**
     * Determines if the service can check if an order has updated.
     *
     * @param order the order
     * @return {@code true} if the service can check if the order has updated
     */
    boolean canCheck(Order order);

    /**
     * Checks an order status.
     * <p/>
     * This can be invoked:
     * <ul>
     *     <li>after an order is confirmed to trigger an immediate update of the order status</li>
     *     <li>to add any new results for an order</li>
     * </ul>
     *
     * @param order the order
     * @return {@code true} if the order was updated
     * @throws LaboratoryException for any error
     */
    boolean check(Order order);

    /**
     * Returns the document to print and include with any sample submitted for an order.
     *
     * @param order the order
     * @return the document, or {@code null} if none is required
     * @throws LaboratoryException for any error
     */
    Document getRequestForm(Order order);

    /**
     * Cancels an order.
     * <p/>
     * For providers that support:
     * <ul>
     * <li>synchronous submission, the {@link Order#setStatus(Order.Status) status} should be set to
     *      {@link Order.Status#CANCELLED CANCELLED}.</li>
     * <li>asynchronous submission, the {@link Order#setStatus(Order.Status) status} should be set to
     * {@link Order.Status#SUBMITTING SUBMITTING} and {@link Order.Status#CANCELLED CANCELLED} on completion</li>
     * </ul>
     * If the order cancellation cannot be submitted due to a temporary error, an exception should be
     * thrown. It will subsequently be resubmitted.<p/>
     * If the cancellation cannot be submitted due to some permanent error
     * (e.g. the order has been completed by the lab), the {@link Order#setStatus(Order.Status, String) status} should
     * be set to {@link Order.Status#ERROR ERROR}, and a diagnostic message included.<br/>
     *
     * @param order the order to cancel
     * @throws LaboratoryException for any error
     */
    void cancel(Order order);

    /**
     * Returns external results for an order.
     *
     * @param order the order
     * @return the external results, or {@code null} if there are none, or external results are not supported
     * @throws LaboratoryException for any error
     */
    ExternalResults getExternalResults(Order order);

    /**
     * Synchronises data.
     * <p>
     * This adds tests that aren't already present, updates existing instances if required, and deactivates those that
     * are no longer relevant.
     *
     * @param changes tracks the changes that were made
     * @throws LaboratoryException for any error
     */
    void synchroniseData(Changes<Entity> changes);

}
