/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

/**
 * Laboratory order validation status.
 *
 * @author Tim Anderson
 */
public class OrderValidationStatus {

    public enum Status {
        VALID,                // order is valid and may be submitted
        WARNING,              // the order may be submitted, but there are problems. Details are provided in the message
        ERROR                 // order is invalid and may not be submitted. Details are provided in the message.
    }

    /**
     * The status.
     */
    private final Status status;

    /**
     * The status message, for warnings and errors.
     */
    private final String message;

    /**
     * Construct a {@link OrderValidationStatus}.
     *
     * @param status the status
     */
    private OrderValidationStatus(Status status) {
        this(status, null);
    }

    /**
     * Construct a {@link OrderValidationStatus}.
     *
     * @param status  the status
     * @param message the message
     */
    private OrderValidationStatus(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * Returns the status.
     *
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null} if the status is {@code VALID}
     */
    public String getMessage() {
        return message;
    }

    /**
     * Creates a status indicating that the order is valid.
     *
     * @return a new status
     */
    public static OrderValidationStatus valid() {
        return new OrderValidationStatus(Status.VALID);
    }

    /**
     * Creates a status indicating that the order is valid but has a warning.
     *
     * @param message the warning message
     * @return a new status
     */
    public static OrderValidationStatus warning(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Argument 'message' must be provided");
        }
        return new OrderValidationStatus(Status.WARNING, message);
    }

    /**
     * Creates a status indicating that the order is invalid.
     *
     * @param message the error message
     * @return a new status
     */
    public static OrderValidationStatus error(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Argument 'message' must be provided");
        }
        return new OrderValidationStatus(Status.ERROR, message);
    }
}