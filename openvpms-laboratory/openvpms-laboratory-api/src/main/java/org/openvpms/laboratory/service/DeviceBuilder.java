/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.sync.Changes;

/**
 * Builder for {@link Device}s.
 *
 * @author Tim Anderson
 */
public interface DeviceBuilder {

    /**
     * Track changes.
     *
     * @param changes the changes
     * @return this
     */
    DeviceBuilder changes(Changes<Entity> changes);

    /**
     * Sets the device id, used for identifying the device.
     *
     * @param archetype the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param id        the device id. This must be unique to the archetype
     * @return this
     */
    DeviceBuilder deviceId(String archetype, String id);

    /**
     * Sets the device name.
     *
     * @param name the device name
     * @return this
     */
    DeviceBuilder name(String name);

    /**
     * Sets the device description.
     *
     * @param description the device description
     * @return this
     */
    DeviceBuilder description(String description);

    /**
     * Determines if the device is active or not.
     *
     * @param active if {@code true}, the device is active and may be used. If {@code false} it may not be used
     * @return this
     */
    DeviceBuilder active(boolean active);

    /**
     * Sets the laboratory that manages the device.
     *
     * @param laboratory the laboratory
     * @return this
     */
    DeviceBuilder laboratory(Laboratory laboratory);

    /**
     * Sets the locations where the device may be used.
     * <p/>
     * This replaces any other locations.
     *
     * @param locations the practice locations
     * @return this
     */
    DeviceBuilder locations(Location... locations);

    /**
     * Adds a practice location where the device may be used.
     *
     * @param location the practice location
     * @return this
     */
    DeviceBuilder addLocation(Location location);

    /**
     * Builds the device.
     * <p/>
     * The builder is reset.
     *
     * @return the device
     */
    Device build();

}
