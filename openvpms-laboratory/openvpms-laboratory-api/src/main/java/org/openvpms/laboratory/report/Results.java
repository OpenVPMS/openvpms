/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.domain.laboratory.Test;
import org.openvpms.laboratory.report.Result.Status;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * The results of a test.
 *
 * @author Tim Anderson
 */
public interface Results {

    /**
     * Returns the results identifier.
     *
     * @return the results identifier
     */
    String getResultsId();

    /**
     * Returns the results status.
     *
     * @return the results status
     */
    Status getStatus();

    /**
     * Returns the date/time when the results where produced.
     *
     * @return the date/time
     */
    OffsetDateTime getDate();

    /**
     * Returns the test.
     *
     * @return the test. May be {@code null}
     */
    Test getTest();

    /**
     * Returns the result category code.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @return the result category code. May be {@code null}
     */
    String getCategoryCode();

    /**
     * Returns the result category name.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @return the result category name. May be {@code null}
     */
    String getCategoryName();

    /**
     * Returns a short narrative of the results.
     *
     * @return the notes. May be {@code null}
     */
    String getNotes();

    /**
     * Returns the results.
     *
     * @return the results, in the order they were added
     */
    List<Result> getResults();

}
