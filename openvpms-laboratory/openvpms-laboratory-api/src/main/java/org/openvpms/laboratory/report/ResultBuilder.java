/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import java.io.InputStream;
import java.math.BigDecimal;

/**
 * Builder for {@link Result}s.
 *
 * @author Tim Anderson
 */
public interface ResultBuilder {

    /**
     * Sets the result status.
     *
     * @param status the result status
     * @return this
     */
    ResultBuilder status(Result.Status status);

    /**
     * Sets the identifier for the analyte.
     *
     * @param code the analyte identifier. May be {@code null}
     * @return this
     */
    ResultBuilder analyteCode(String code);

    /**
     * Sets the name of the analyte for which the result is being reported.
     *
     * @param name the analyte name
     * @return this
     */
    ResultBuilder analyteName(String name);

    /**
     * Sets the numerical result for the analyte.
     *
     * @param result the numerical result for the analyte. May be {@code null}
     * @return this
     */
    ResultBuilder value(BigDecimal result);

    /**
     * Sets the result for the analyte, as text.
     * <p/>
     * This is not required if a {@link #value} is provided.
     *
     * @param result the result text. May be {@code null}
     */
    ResultBuilder result(String result);

    /**
     * Sets the unit of measure for the analyte result.
     *
     * @param units the unit of measure. May be {@code null}
     * @return this
     */
    ResultBuilder units(String units);

    /**
     * Sets the qualifier.
     *
     * @param qualifier the qualifier. May be {@code null}
     */
    ResultBuilder qualifier(String qualifier);

    /**
     * Sets the low reference range value for the analyte.
     *
     * @param lowRange the low reference range value. May be {@code null}
     * @return this
     */
    ResultBuilder lowRange(BigDecimal lowRange);

    /**
     * Sets the high reference range value for the analyte.
     *
     * @param highRange the high reference range value. May be {@code null}
     * @return this
     */
    ResultBuilder highRange(BigDecimal highRange);

    /**
     * Sets the extreme low reference range value for the analyte.
     *
     * @param extremeLowRange the extreme low reference range value. May be {@code null}
     * @return this
     */
    ResultBuilder extremeLowRange(BigDecimal extremeLowRange);

    /**
     * Sets the extreme high reference range value for the analyte.
     *
     * @param extremeHighRange the extreme high reference range value. May be {@code null}
     * @return this
     */
    ResultBuilder extremeHighRange(BigDecimal extremeHighRange);

    /**
     * Determines if the result is out of range.
     * <p/>
     * This is not required if a {@link #value}, {@link #extremeLowRange} and {@link #extremeHighRange} are provided.
     *
     * @param outOfRange if {@code true}, the result is out of range
     */
    ResultBuilder outOfRange(boolean outOfRange);

    /**
     * Sets the reference range.
     * <p/>
     * This is a formatted reference range, including units.
     * <p/>
     * This is not required if a {@link #extremeLowRange}, {@link #extremeHighRange}, and optionally {@link #units}
     * are provided.
     *
     * @param range the reference range. May be {@code null}
     */
    ResultBuilder referenceRange(String range);

    /**
     * Sets a short narrative of the result.
     *
     * @param notes the result narrative. May be {@code null}
     * @return this
     */
    ResultBuilder notes(String notes);

    /**
     * Sets the image associated with the results.
     *
     * @param fileName the file name
     * @param mimeType the mime type
     * @param stream   the stream to the image content
     * @return this
     */
    ResultBuilder image(String fileName, String mimeType, InputStream stream);

    /**
     * Builds the result and adds it to the set of results.
     * <p/>
     * The builder is reset.
     *
     * @return the result
     */
    Result build();

    /**
     * Builds the result and adds it to the set of results.
     *
     * @return the parent builder
     */
    ResultsBuilder add();

}
