/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.domain.laboratory.InvestigationType;

import java.util.List;

/**
 * Manages investigation types.
 *
 * @author Tim Anderson
 */
public interface InvestigationTypes {

    /**
     * Returns an investigation type.
     * <p>
     * Investigation types managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param typeId    the investigation type identifier identifier
     * @return the investigation type, or {@code null} if none is found. The returned type may be inactive
     */
    InvestigationType getInvestigationType(String archetype, String typeId);

    /**
     * Returns all investigation types with the specified test code archetype.
     *
     * @param archetype  the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param activeOnly if {@code true}, only return active investigation types
     * @return the investigation types
     */
    List<InvestigationType> getInvestigationTypes(String archetype, boolean activeOnly);

    /**
     * Returns a builder to build investigation types.
     *
     * @return the investigation type builder
     */
    InvestigationTypeBuilder getInvestigationTypeBuilder();

}
