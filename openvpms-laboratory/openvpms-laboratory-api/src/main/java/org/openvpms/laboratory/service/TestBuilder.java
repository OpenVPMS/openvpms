/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.laboratory.Test.UseDevice;
import org.openvpms.domain.sync.Changes;

import java.math.BigDecimal;

/**
 * Builder for {@link Test}s.
 * <p/>
 * This can be used to create new tests, or update existing tests given their identifier.
 *
 * @author Tim Anderson
 */
public interface TestBuilder {

    /**
     * Track changes.
     *
     * @param changes the changes
     * @return this
     */
    TestBuilder changes(Changes<Entity> changes);

    /**
     * Sets the test code, used for identifying the test.
     *
     * @param archetype the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param code      the test code
     * @return this
     */
    TestBuilder testCode(String archetype, String code);

    /**
     * Sets the display name for the test code.
     *
     * @param name the display name for the test code
     * @return this
     */
    TestBuilder testCodeName(String name);

    /**
     * Sets the test name.
     *
     * @param name the test name
     * @return this
     */
    TestBuilder name(String name);

    /**
     * Sets the test description.
     *
     * @param description the test description
     * @return this
     */
    TestBuilder description(String description);

    /**
     * Determines if the test can be grouped with other tests in an order.
     *
     * @param group if {@code true}, the test can be grouped with other tests, otherwise it must be submitted
     *              individually
     * @return this
     */
    TestBuilder group(boolean group);

    /**
     * Determines if a laboratory device must be specified when this test is ordered.
     *
     * @param useDevice if {@link UseDevice#YES YES}, a device must be specified; if {@link UseDevice#NO NO} no device
     *                  should be specified; if {@link UseDevice#OPTIONAL OPTIONAL}, a device may be specified
     * @return this
     */
    TestBuilder useDevice(UseDevice useDevice);

    /**
     * Determines if the test is active or not.
     *
     * @param active if {@code true}, the test is active and may be ordered. If {@code false} it may not be ordered
     * @return this
     */
    TestBuilder active(boolean active);

    /**
     * Sets the test's investigation type.
     *
     * @param type the investigation type
     * @return this
     */
    TestBuilder investigationType(InvestigationType type);

    /**
     * Sets the test's turnaround notes.
     * <p/>
     * This is free-form text indicating how long the test is expected to take.
     *
     * @param notes the notes
     * @return this
     */
    TestBuilder turnaround(String notes);

    /**
     * Sets the test's specimen collection notes.
     *
     * @param notes the notes
     * @return this
     */
    TestBuilder specimen(String notes);

    /**
     * Sets the test price.
     *
     * @param price the price, excluding tax
     * @return this
     */
    TestBuilder price(BigDecimal price);

    /**
     * Builds the test.
     * <p/>
     * The builder is reset.
     *
     * @return the test
     */
    Test build();

}
