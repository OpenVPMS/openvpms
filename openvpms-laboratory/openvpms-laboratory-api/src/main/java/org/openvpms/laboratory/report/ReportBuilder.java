/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report.Status;
import org.openvpms.laboratory.service.LaboratoryService;

import java.io.InputStream;

/**
 * Builds a {@link Report}.
 *
 * @author Tim Anderson
 */
public interface ReportBuilder {

    /**
     * Sets the report identifier, issued by the laboratory.
     * <p/>
     * To avoid duplicates between laboratories, each laboratory service must provide a unique archetype.
     * on each version.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryReport</em> prefix.
     * @param id        the report identifier
     * @return this
     */
    ReportBuilder reportId(String archetype, String id);

    /**
     * Sets the report status.
     *
     * @param status the report status. If {@link Report.Status#COMPLETED COMPLETED}, this completes the order
     * @return this
     */
    ReportBuilder status(Status status);

    /**
     * Sets the synchronisation identifier.
     * <p/>
     * This can be used to filter duplicate results if a failure occurs notifying the laboratory that results have been
     * received.
     *
     * @param id the synchronisation identifier. May be {@code null}
     * @return this
     */
    ReportBuilder synchronisationId(String id);

    /**
     * Sets the report summary.
     *
     * @param summary the summary. May be {@code null}
     * @return this
     */
    ReportBuilder summary(String summary);

    /**
     * Determines if documents should be versioned.
     * <p/>
     * By default, documents are not versioned, unless they have been loaded manually.
     *
     * @param version if {@code true}, version documents, otherwise only version them if documents have been loaded
     *                manually
     * @return this
     */
    ReportBuilder version(boolean version);

    /**
     * Sets the document associated with the results.
     * <p/>
     * If there is an existing document associated with the report (i.e. one that has been built), and:
     * <ul>
     *     <li>versioning is enabled, the existing document will be versioned</li>
     *     <li>versioning is disabled, the existing document will be replaced, unless it was added manually</li>
     * </ul>
     *
     * @param fileName the file name
     * @param mimeType the mime type
     * @param stream   the stream to the document content
     * @return this
     */
    ReportBuilder document(String fileName, String mimeType, InputStream stream);

    /**
     * Indicates that results can be checked for. This is provided for services that don't automatically add results,
     * or do it periodically.
     * <p/>
     * This indicates that {@link LaboratoryService#canCheck(Order)} may be used to check if results are available.
     *
     * @return this
     */
    ReportBuilder checkResults();

    /**
     * Indicates that results can be checked for. This is provided for services that don't automatically add results,
     * or do it periodically.
     *
     * @param checkResults if {@code true}, indicates that {@link LaboratoryService#canCheck(Order)} may be used
     *                     to check if results are available
     * @return this
     */
    ReportBuilder checkResults(boolean checkResults);

    /**
     * Flags the report as having external results.
     *
     * @return this
     */
    ReportBuilder externalResults();

    /**
     * Determines if the report has external results.
     *
     * @param externalResults if {@code true}, the report has external results, otherwise the results are all inline
     * @return this
     */
    ReportBuilder externalResults(boolean externalResults);

    /**
     * Returns a {@link Results} builder.
     * <p/>
     * This updates the results with the specified id if they exist, or creates new results if they don't.     *
     *
     * @param id the results identifier. This need only be unique within a single {@link Report}
     * @return the results builder
     */
    ResultsBuilder results(String id);

    /**
     * Builds the report.
     *
     * @return the report
     * @throws LaboratoryException if the report cannot be built
     */
    Report build();
}
