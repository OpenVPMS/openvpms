/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

/**
 * Abstract implementation of {@link WebView}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractWebView implements WebView {

    /**
     * The URL.
     */
    private final String url;

    /**
     * Determines if the results should be displayed in a new browser window.
     */
    private final boolean newWindow;

    /**
     * The window width, in pixels.
     */
    private final int width;

    /**
     * The window height, in pixels.
     */
    private final int height;

    /**
     * Constructs a {@link AbstractWebView} to display external results in a new browser window.
     *
     * @param url the url
     */
    public AbstractWebView(String url) {
        this(url, true, 0, 0);
    }

    /**
     * Constructs an {@link AbstractWebView}.
     *
     * @param url       the url
     * @param newWindow if {@code true}, open the results in a new browser window, otherwise display them in an iframe
     * @param width     the window width, in pixels, or {@code 0} to use the default
     * @param height    the window height, in pixels, or {@code 0} to use the default
     */
    public AbstractWebView(String url, boolean newWindow, int width, int height) {
        this.url = url;
        this.newWindow = newWindow;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the results URL.
     *
     * @return the results URL
     */
    @Override
    public String getUrl() {
        return url;
    }

    /**
     * Determines if a new browser window should be opened for the results.
     *
     * @return {@code true} if a new browser window should be opened for the results, or {@code false} if they should
     * be displayed in a frame
     */
    @Override
    public boolean newWindow() {
        return newWindow;
    }

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels, or {@code 0} to use the default
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * Returns the window height.
     *
     * @return the window width, in pixels, or {@code 0} to use the default
     */
    @Override
    public int getHeight() {
        return height;
    }
}
