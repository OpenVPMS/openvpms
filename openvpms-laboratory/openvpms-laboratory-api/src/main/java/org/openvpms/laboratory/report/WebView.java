/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

/**
 * Enables external results to be viewed in a browser.
 *
 * @author Tim Anderson
 */
public interface WebView extends View {

    /**
     * Returns the results URL.
     *
     * @return the results URL
     */
    String getUrl();

    /**
     * Determines if a new browser window should be opened for the results.
     *
     * @return {@code true} if a new browser window should be opened for the results, or {@code false} if they should
     * be displayed in an iframe
     */
    boolean newWindow();

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels, or {@code 0} to use the default
     */
    int getWidth();

    /**
     * Returns the window height.
     *
     * @return the window width, in pixels, or {@code 0} to use the default
     */
    int getHeight();

}
