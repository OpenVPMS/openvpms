/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.patient.record.AbstractDocumentBuilder;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.Report.Status;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Results;
import org.openvpms.laboratory.report.ResultsBuilder;
import org.openvpms.laboratory.service.LaboratoryService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Builds a {@link Report} for an {@link Order}.
 *
 * @author Tim Anderson
 */
public class ReportBuilderImpl extends Builder<Report> implements ReportBuilder {

    /**
     * The underlying investigation.
     */
    private final DocumentAct investigation;

    /**
     * The order.
     */
    private final Act order;

    /**
     * The investigation bean.
     */
    private final IMObjectBean bean;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document builder.
     */
    private final ReportDocumentBuilder documentBuilder;

    /**
     * The builder context.
     */
    private State state;

    /**
     * The report identifier, issued by the laboratory.
     */
    private String reportId;

    /**
     * The status.
     */
    private String status;

    /**
     * The report identifier archetype.
     */
    private String reportIdArchetype;

    /**
     * The synchronisation identifier.
     */
    private String synchronisationId;

    /**
     * The notes.
     */
    private String summary;

    /**
     * The report document.
     */
    private Document document;

    /**
     * Determines if results can be checked for.
     */
    private Boolean checkResults;

    /**
     * Determines if there are external results.
     */
    private Boolean externalResults;

    /**
     * Protected document node.
     */
    static final String PROTECTED_DOCUMENT = "protectedDocument";

    /**
     * Results node.
     */
    static final String RESULTS = "results";

    /**
     * Check results node.
     */
    static final String CHECK_RESULTS = "checkResults";

    /**
     * External results node.
     */
    static final String EXTERNAL_RESULTS = "externalResults";

    /**
     * Synchronisation id node.
     */
    static final String SYNCHRONISATION_ID = "synchronisationId";

    /**
     * Summary node.
     */
    static final String SUMMARY = "summary";

    /**
     * Report id node.
     */
    static final String REPORT_ID = "reportId";

    /**
     * Constructs a {@link ReportBuilderImpl}.
     *
     * @param investigation      the investigation to update
     * @param order              the order to update
     * @param service            the archetype service
     * @param domainService      the domain object service
     * @param handlers           the document handlers
     * @param rules              the document rules
     * @param transactionManager the transaction manager
     */
    public ReportBuilderImpl(DocumentAct investigation, Act order, ArchetypeService service,
                             DomainService domainService, DocumentHandlers handlers, DocumentRules rules,
                             PlatformTransactionManager transactionManager) {
        super(service, domainService);
        this.investigation = investigation;
        this.order = order;
        this.domainService = domainService;
        this.handlers = handlers;
        this.transactionManager = transactionManager;
        documentBuilder = new ReportDocumentBuilder(investigation, service, handlers, rules);
        documentBuilder.version(false);
        state = new State(investigation, service);
        bean = getBean(investigation);
        status = order.getStatus2();
        synchronisationId = bean.getString(SYNCHRONISATION_ID);
        summary = bean.getString(SUMMARY);
    }

    /**
     * Sets the report identifier, issued by the laboratory.
     * <p/>
     * To avoid duplicates between laboratories, each laboratory service must provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryReport</em> prefix.
     * @param id        the report identifier
     * @return this
     */
    @Override
    public ReportBuilder reportId(String archetype, String id) {
        reportIdArchetype = archetype;
        reportId = StringUtils.trimToNull(id);
        return this;
    }

    /**
     * Sets the report status.
     *
     * @param status the report status
     * @return this
     */
    @Override
    public ReportBuilder status(Status status) {
        this.status = status.toString();
        return this;
    }

    /**
     * Sets the synchronisation identifier.
     * <p/>
     * This can be used to filter duplicate results if a failure occurs notifying the laboratory that results have been
     * received.
     *
     * @param id the synchronisation identifier. May be {@code null}
     * @return this
     */
    @Override
    public ReportBuilder synchronisationId(String id) {
        this.synchronisationId = StringUtils.trimToNull(id);
        return this;
    }

    /**
     * Sets the report summary.
     *
     * @param summary the summary. May be {@code null}
     * @return this
     */
    @Override
    public ReportBuilder summary(String summary) {
        this.summary = StringUtils.trimToNull(summary);
        return this;
    }

    /**
     * Determines if documents should be versioned.
     * <p/>
     * By default, documents are not versioned, unless they have been loaded manually.
     *
     * @param version if {@code true}, version documents, otherwise only version them if documents have been loaded
     *                manually
     * @return this
     */
    @Override
    public ReportBuilder version(boolean version) {
        documentBuilder.version(version);
        return this;
    }

    /**
     * Sets the document associated with the results.
     *
     * @param fileName the file name
     * @param mimeType the mime type
     * @param stream   the stream to the document content
     * @return this
     */
    @Override
    public ReportBuilder document(String fileName, String mimeType, InputStream stream) {
        document = documentBuilder.fileName(fileName).mimeType(mimeType).content(stream).createDocument();
        return this;
    }

    /**
     * Indicates that results can be checked for. This is provided for services that don't automatically add results,
     * or do it periodically.
     * <p/>
     * This indicates that {@link LaboratoryService#canCheck(Order)} may be used to check if results are available.
     *
     * @return this
     */
    @Override
    public ReportBuilder checkResults() {
        return checkResults(true);
    }

    /**
     * Indicates that results can be checked for. This is provided for services that don't automatically add results,
     * or do it periodically.
     *
     * @param checkResults if {@code true}, indicates that {@link LaboratoryService#canCheck(Order)} may be used
     *                     to check if results are available
     * @return this
     */
    @Override
    public ReportBuilder checkResults(boolean checkResults) {
        this.checkResults = checkResults;
        return this;
    }

    /**
     * Flags the report as having external results.
     *
     * @return this
     */
    @Override
    public ReportBuilder externalResults() {
        return externalResults(true);
    }

    /**
     * Determines if the report has external results.
     *
     * @param externalResults if {@code true}, the report has external results, otherwise the results are all inline
     * @return this
     */
    @Override
    public ReportBuilder externalResults(boolean externalResults) {
        this.externalResults = externalResults;
        return this;
    }

    /**
     * Returns a {@link Results} builder.
     * <p/>
     * This updates the results with the specified id if they exist, or creates new results if they don't.     *
     *
     * @param id the results identifier. This need only be unique within a single {@link Report}
     * @return the results builder
     */
    @Override
    public ResultsBuilder results(String id) {
        id = StringUtils.trimToNull(id);
        if (id == null) {
            throw new IllegalArgumentException("Argument 'id' may not be null or empty");
        }
        return new ResultsBuilderImpl(this, id, state, getService(), handlers);
    }

    /**
     * Builds the report.
     *
     * @return the report
     * @throws LaboratoryException if the report cannot be built
     */
    @Override
    public Report build() {
        Report report;
        try {
            TransactionTemplate template = new TransactionTemplate(transactionManager);
            template.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    doBuild();
                }
            });
            reset();
            report = domainService.create(investigation, ReportImpl.class);
        } catch (LaboratoryException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new LaboratoryException(LaboratoryMessages.failedToCreateReport(exception.getMessage()), exception);
        }
        return report;
    }

    /**
     * Builds the report.
     * <p/>
     * This is called within a transaction so all changes will roll back if there is a failure.
     */
    private void doBuild() {
        bean.setValue("endTime", new Date());   // last modified
        if (document != null) {
            // have a new document, so need to continue building it
            documentBuilder.build(document, state);
        }
        if (reportId != null) {
            setIdentity();
        }
        bean.setValue(SYNCHRONISATION_ID, synchronisationId);
        bean.setValue(SUMMARY, summary);
        if (checkResults != null && checkResults) {
            bean.setValue(CHECK_RESULTS, checkResults);
        } else {
            bean.setValue(CHECK_RESULTS, null);
        }
        if (externalResults != null && externalResults) {
            bean.setValue(EXTERNAL_RESULTS, externalResults);
        } else {
            bean.setValue(EXTERNAL_RESULTS, null);
        }
        int sequence = getNextSequence(bean.getValues(RESULTS, ActRelationship.class));
        for (Act act : state.getNew(InvestigationArchetypes.RESULTS, Act.class)) {
            ActRelationship relationship = (ActRelationship) bean.addTarget(RESULTS, act, "investigation");
            relationship.setSequence(sequence++);
        }
        if (status != null && !Objects.equals(status, order.getStatus2())) {
            order.setStatus2(status);
            state.add(order);
            if (InvestigationActStatus.PARTIAL_RESULTS.equals(status)
                || InvestigationActStatus.WAITING_FOR_SAMPLE.equals(status)) {
                investigation.setStatus2(status);
            } else if (Status.COMPLETED.toString().equals(status)) {
                order.setStatus(Order.Status.COMPLETED.toString());
                investigation.setStatus2(InvestigationActStatus.RECEIVED);
            }
        }
        state.add(investigation);
        state.save();
    }

    /**
     * Resets the builder.
     */
    private void reset() {
        reportId = null;
        status = null;
        synchronisationId = null;
        summary = null;
        document = null;
        checkResults = null;
        externalResults = null;
        documentBuilder.version(false);
        state = new State(investigation, getService());
    }

    /**
     * Updates the report identifier if required.
     */
    private void setIdentity() {
        ActIdentity identity = bean.getObject(REPORT_ID, ActIdentity.class);
        if (identity != null) {
            if (!identity.isA(reportIdArchetype)) {
                throw new IllegalStateException("Cannot change reportId archetype from "
                                                + identity.getArchetype() + " to " + reportIdArchetype);
            }
        } else {
            identity = create(reportIdArchetype, ActIdentity.class);
            investigation.addIdentity(identity);
        }
        identity.setIdentity(reportId);
    }

    /**
     * A {@link DocumentBuilder} that allows the changes to be collected in a {@link State}.
     */
    private static class ReportDocumentBuilder extends AbstractDocumentBuilder<ReportDocumentBuilder> {
        public ReportDocumentBuilder(DocumentAct investigation, ArchetypeService service, DocumentHandlers handlers,
                                     DocumentRules rules) {
            super(investigation, service, handlers, rules);
        }

        /**
         * Creates the document.
         *
         * @return the document
         */
        @Override
        protected Document createDocument() {
            return super.createDocument();
        }

        /**
         * Build the document.
         * <p/>
         * This is not supported. Use {@link #build(Document, State)} instead.
         *
         * @throws UnsupportedOperationException if invoked
         */
        @Override
        public Document build() {
            throw new UnsupportedOperationException();
        }

        /**
         * Associates a document with the investigation, and collects new and removed objects in {@code state}.
         *
         * @param document the document
         * @param state    the state collector
         */
        public void build(Document document, State state) {
            List<IMObject> toSave = new ArrayList<>();
            List<Reference> toRemove = new ArrayList<>();
            doBuild(document, toSave, toRemove);
            state.add(toSave);
            state.remove(toRemove);
        }
    }
}
