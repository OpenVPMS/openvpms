/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.OrderValidationStatus;

/**
 * A service to validate laboratory orders prior to their submission to a laboratory.
 *
 * @author Tim Anderson
 */
public interface OrderValidationService {

    /**
     * Associates an {@link OrderValidationStatus} with the laboratory/laboratory service name.
     */
    class ValidationStatus {

        /**
         * The name of the laboratory or laboratory service.
         */
        private final String name;

        /**
         * The order validation status.
         */
        private final OrderValidationStatus status;

        /**
         * Constructs a {@link ValidationStatus}.
         *
         * @param name   the name of the laboratory or laboratory service. May be {@code null}
         * @param status the order validation status
         */
        public ValidationStatus(String name, OrderValidationStatus status) {
            this.name = name;
            this.status = status;
        }

        /**
         * Returns the laboratory or laboratory service name.
         *
         * @return the name, or {@code null} if it is unknown
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the status.
         *
         * @return the status
         */
        public OrderValidationStatus.Status getStatus() {
            return status.getStatus();
        }

        /**
         * Returns the message.
         *
         * @return the message. May be {@code null} if the status is {@code VALID}
         */
        public String getMessage() {
            return status.getMessage();
        }
    }

    /**
     * Determines if an order may be submitted for an investigation.
     * <p/>
     * For investigations that:
     * <ul>
     *     <li>have an <em>entity.laboratoryService*</em> laboratory, these will be validated using
     *     the corresponding {@link LaboratoryService}'s {@link LaboratoryService#validate(Order) validate(Order)}
     *     method.</li>
     *     <li>have no laboratory or have an <em>entity.HL7Service*</em> laboratory, these will be treated as valid,
     *     as no further validation is possible.</li>
     * </ul>
     *
     * @param investigation the investigation. Must have {@code PENDING} order status
     * @return the order validation status
     */
    ValidationStatus validate(DocumentAct investigation);
}
