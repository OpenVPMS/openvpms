/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.ResultBuilder;
import org.openvpms.laboratory.report.Results;
import org.openvpms.laboratory.report.ResultsBuilder;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.openvpms.laboratory.internal.report.ResultsImpl.CATEGORY_CODE;
import static org.openvpms.laboratory.internal.report.ResultsImpl.CATEGORY_NAME;

/**
 * Default implementation of {@link ResultsBuilder}.
 *
 * @author Tim Anderson
 */
public class ResultsBuilderImpl extends Builder<Results> implements ResultsBuilder {

    /**
     * The builder state.
     */
    private final State state;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The objects not yet built.
     */
    private final List<Act> pending = new ArrayList<>();

    /**
     * The act to update.
     */
    private IMObjectBean bean;

    /**
     * The result id.
     */
    private String resultsId;

    /**
     * The result status.
     */
    private Result.Status status;

    /**
     * The result date.
     */
    private OffsetDateTime date;

    /**
     * The test.
     */
    private Reference test;

    /**
     * The category code.
     */
    private String categoryCode;

    /**
     * The category name.
     */
    private String categoryName;

    /**
     * The notes.
     */
    private String notes;

    /**
     * The notes, stored in a document act if they are too long.
     */
    private LongText longNotes;

    /**
     * Results node name.
     */
    static final String RESULTS = "results";

    /**
     * Constructs a {@link ResultsBuilderImpl}
     *
     * @param parent   the parent builder
     * @param id       the results identifier
     * @param state    the builder state
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    ResultsBuilderImpl(ReportBuilderImpl parent, String id, State state, ArchetypeService service,
                       DocumentHandlers handlers) {
        super(parent);
        this.resultsId = id;
        this.state = state;
        this.service = service;
        this.handlers = handlers;
        bean = state.getResults(id);
        if (bean != null) {
            Act update = bean.getObject(Act.class);
            date = DateRules.toOffsetDateTime(update.getActivityStartTime());
            test = bean.getTargetRef(ResultsImpl.TEST);
            categoryCode = bean.getString(CATEGORY_CODE);
            categoryName = bean.getString(CATEGORY_NAME);
            longNotes = createLongNotes(bean);
            notes = longNotes.getText();
        }
    }

    /**
     * Sets the results status.
     * <p/>
     * If not set, the status will be derived from the results.
     *
     * @param status the status
     * @return this
     */
    @Override
    public ResultsBuilder status(Result.Status status) {
        this.status = status;
        return this;
    }

    /**
     * Sets the date/time when the results where produced.
     *
     * @param date the date/time
     * @return this
     */
    @Override
    public ResultsBuilder date(OffsetDateTime date) {
        this.date = date;
        return this;
    }

    /**
     * Sets the test that the results are for.
     *
     * @param test the test. May be {@code null}
     * @return this
     */
    @Override
    public ResultsBuilder test(Test test) {
        this.test = (test != null) ? test.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the result category code.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @param code the result category code
     * @return this
     */
    @Override
    public ResultsBuilder categoryCode(String code) {
        this.categoryCode = StringUtils.trimToNull(code);
        return this;
    }

    /**
     * Sets the result category name.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @param name the result category name
     * @return this
     */
    @Override
    public ResultsBuilder categoryName(String name) {
        this.categoryName = StringUtils.trimToNull(name);
        return this;
    }

    /**
     * Sets a short narrative of the results.
     *
     * @param notes the notes. May be {@code null}
     * @return this
     */
    @Override
    public ResultsBuilder notes(String notes) {
        this.notes = StringUtils.trimToNull(notes);
        return this;
    }

    /**
     * Returns a {@link Result} builder.
     * <p/>
     * This updates the result with the specified id if it exists, or creates a new result if it doesn't.     *
     *
     * @param id the result identifier. This need only be unique within a single {@link Results}
     * @return the result builder
     */
    @Override
    public ResultBuilder result(String id) {
        id = StringUtils.trimToNull(id);
        if (id == null) {
            throw new IllegalArgumentException("Argument 'id' cannot be null or empty");
        }
        Act results = (bean != null) ? bean.getObject(Act.class) : null;
        return new ResultBuilderImpl(this, id, results, state, service, handlers, pending);
    }

    /**
     * Builds the object
     *
     * @return the object
     */
    @Override
    public Results build() {
        IMObjectBean bean = doBuild();
        return new ResultsImpl(bean, service, getDomainService());
    }

    /**
     * Builds the {@link Results}, adding it to the report.
     *
     * @return the parent builder
     */
    @Override
    public ReportBuilder add() {
        doBuild();
        return (ReportBuilder) getParent();
    }

    /**
     * Builds the results.
     *
     * @return the results bean
     */
    private IMObjectBean doBuild() {
        if (resultsId == null) {
            throw new LaboratoryException(LaboratoryMessages.noResultsId());
        }
        Act act;
        if (bean == null) {
            act = create(InvestigationArchetypes.RESULTS, Act.class);
            bean = getBean(act);
        } else {
            act = bean.getObject(Act.class);
        }
        act.setActivityEndTime(new Date());      // last modified
        bean.setValue(ResultsImpl.RESULTS_ID, resultsId);

        if (date != null) {
            act.setActivityStartTime(DateRules.toDate(date));
        }
        if (status == null) {
            Set<Act> items = state.getResultItems(act);
            items.addAll(pending);
            int pending = 0;
            int inprogress = 0;
            int completed = 0;
            for (Act item : items) {
                Result.Status itemStatus = ResultImpl.getStatus(item.getStatus());
                if (itemStatus == Result.Status.PENDING) {
                    ++pending;
                } else if (itemStatus == Result.Status.IN_PROGRESS) {
                    ++inprogress;
                } else if (itemStatus == Result.Status.COMPLETED) {
                    ++completed;
                }
            }
            if (pending > 0) {
                if (pending == items.size()) {
                    status = Result.Status.PENDING;
                } else {
                    status = Result.Status.IN_PROGRESS;
                }
            } else if (inprogress > 0) {
                status = Result.Status.IN_PROGRESS;
            } else if (completed > 0) {
                status = Result.Status.COMPLETED;
            } else {
                status = Result.Status.CANCELLED;
            }
        }
        act.setStatus(status.toString());
        if (test != null) {
            bean.setTarget(ResultsImpl.TEST, test);
        }
        if (categoryCode != null) {
            bean.setValue(CATEGORY_CODE, categoryCode);
        }
        if (categoryName != null) {
            bean.setValue(CATEGORY_NAME, categoryName);
        }

        if (longNotes == null) {
            longNotes = createLongNotes(bean);
        }
        longNotes.setText(notes, state);

        int sequence = getNextSequence(bean.getValues(ResultsImpl.ITEMS, ActRelationship.class));
        for (Act item : pending) {
            if (item.isA(InvestigationArchetypes.RESULT) && !bean.hasTarget(ResultsImpl.ITEMS, item)) {
                ActRelationship relationship = (ActRelationship) bean.addTarget(ResultsImpl.ITEMS, item, RESULTS);
                relationship.setSequence(sequence++);
            }
        }
        state.add(act);
        state.add(pending);
        IMObjectBean result = bean;
        reset();
        return result;
    }

    /**
     * Resets the builder.
     */
    private void reset() {
        pending.clear();
        bean = null;
        resultsId = null;
        status = null;
        date = null;
        test = null;
        notes = null;
        longNotes = null;
    }
}
