/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.sync.Changes;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Entity builder.
 *
 * @author Tim Anderson
 */
abstract class EntityBuilder<D, B extends EntityBuilder<D, B>> {

    /**
     * The entity archetype.
     */
    private final String archetype;

    /**
     * The supported entity identity archetype.
     */
    private final String supportedIdArchetype;

    /**
     * The domain class.
     */
    private final Class<D> domainClass;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Tracks changes.
     */
    private Changes<Entity> changes;

    /**
     * The entity id archetype.
     */
    private String entityIdArchetype;

    /**
     * The entity id.
     */
    private String entityId;

    /**
     * The entity id name.
     */
    private String entityIdName;

    /**
     * The entity name.
     */
    private String name;

    /**
     * The entity description.
     */
    private String description;

    /**
     * Determines if the entity is active.
     */
    private Boolean active;

    /**
     * Constructs an {@link EntityBuilder}.
     *
     * @param archetype            the entity archetype
     * @param supportedIdArchetype the supported entity identity archetypes
     * @param domainClass          the domain class
     * @param service              the archetype service
     * @param transactionManager   the transaction manager
     * @param domainService        the domain object factory
     */
    EntityBuilder(String archetype, String supportedIdArchetype, Class<D> domainClass, ArchetypeService service,
                  PlatformTransactionManager transactionManager, DomainService domainService) {
        this.archetype = archetype;
        this.supportedIdArchetype = supportedIdArchetype;
        this.domainClass = domainClass;
        this.service = service;
        this.transactionManager = transactionManager;
        this.domainService = domainService;
    }

    /**
     * Track changes.
     *
     * @param changes the changes
     * @return this
     */
    @SuppressWarnings("unchecked")
    public B changes(Changes<Entity> changes) {
        this.changes = changes;
        return (B) this;
    }

    /**
     * Sets the entity id, used for identifying the entity.
     *
     * @param archetype the entity identity archetype. Must be an <em>entityIdentity.*</em>
     * @param id        the identifier
     * @return this
     */
    @SuppressWarnings("unchecked")
    public B entityId(String archetype, String id) {
        this.entityIdArchetype = archetype;
        this.entityId = id;
        return (B) this;
    }

    /**
     * Sets the entity name.
     *
     * @param name the entity name
     * @return this
     */
    @SuppressWarnings("unchecked")
    public B name(String name) {
        this.name = name;
        return (B) this;
    }

    /**
     * Sets the entity description.
     *
     * @param description the entity description
     * @return this
     */
    @SuppressWarnings("unchecked")
    public B description(String description) {
        this.description = description;
        return (B) this;
    }

    /**
     * Determines if the entity is active or not.
     *
     * @param active if {@code true}, the entity is active, otherwise it is inactive
     * @return this
     */
    @SuppressWarnings("unchecked")
    public B active(boolean active) {
        this.active = active;
        return (B) this;
    }

    /**
     * Builds the object.
     * <p/>
     * The builder is reset.
     *
     * @return the domain object
     */
    public D build() {
        AtomicBoolean created = new AtomicBoolean();
        AtomicBoolean updated = new AtomicBoolean();
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        Entity test = template.execute(transactionStatus -> {
            if (entityIdArchetype == null) {
                throw new IllegalStateException("No identifier archetype provided");
            }
            if (!TypeHelper.matches(entityIdArchetype, supportedIdArchetype)) {
                throw new IllegalStateException("Expected entity identity archetype: " + supportedIdArchetype
                                                + " but got: " + entityIdArchetype);
            }
            if (entityId == null) {
                throw new IllegalStateException("No identifier provided");
            }
            Entity entity = getEntity(entityIdArchetype, entityId);
            boolean save;
            if (entity == null) {
                entity = create();
                save = true;
                created.set(true);
            } else {
                save = populate(entity);
                if (save) {
                    updated.set(true);
                }
            }
            if (save) {
                service.save(entity);
            }
            return entity;
        });
        if (changes != null) {
            if (created.get()) {
                changes.added(test);
            } else if (updated.get()) {
                changes.updated(test);
            }
        }
        reset();
        return domainService.create(test, domainClass);
    }

    /**
     * Creates a new entity.
     *
     * @return a new entity
     */
    protected Entity create() {
        Entity entity = service.create(archetype, Entity.class);
        entity.addIdentity(createIdentity(entityIdArchetype, entityId, entityIdName));
        populate(entity);
        return entity;
    }

    /**
     * Populates an entity.
     *
     * @param entity the entity to populate
     * @return {@code true} if the entity was updated
     */
    protected boolean populate(Entity entity) {
        boolean changed = false;
        if (name != null && !name.equals(entity.getName())) {
            entity.setName(name);
            changed = true;
        }
        if (description != null && !description.equals(entity.getDescription())) {
            entity.setDescription(description);
            changed = true;
        }
        if (active != null && !active.equals(entity.isActive())) {
            entity.setActive(active);
            changed = true;
        }
        return changed;
    }

    /**
     * Returns the entity corresponding to the identifier.
     *
     * @param archetype the entity id archetype
     * @param id        the entity id
     * @return the entity, or {@code null} if none exists
     */
    protected abstract Entity getEntity(String archetype, String id);

    /**
     * Updates a node if required.
     *
     * @param node  the node name
     * @param value the node value
     * @param bean  the bean
     * @return {@code true} if the node was changed
     */
    protected boolean change(String node, Object value, IMObjectBean bean) {
        boolean changed = false;
        if (!Objects.equals(value, bean.getValue(node))) {
            bean.setValue(node, value);
            changed = true;
        }
        return changed;
    }

    /**
     * Resets the builder.
     */
    protected void reset() {
        changes = null;
        entityIdArchetype = null;
        entityId = null;
        entityIdName = null;
        name = null;
        description = null;
        active = null;
    }

    /**
     * Creates an identity for an entity.
     *
     * @param archetype the identity archetype
     * @param id        the identifier
     * @param name      the identity name. If {@code null}, the id will be used
     * @return a new identity
     */
    protected EntityIdentity createIdentity(String archetype, String id, String name) {
        EntityIdentity identity = service.create(archetype, EntityIdentity.class);
        identity.setIdentity(id);
        if (name != null) {
            identity.setName(name);
        } else {
            identity.setName(id);
        }
        return identity;
    }

    /**
     * Updates relationships.
     *
     * @param bean    the bean
     * @param node    the relationship node name
     * @param targets the relationship targets
     * @return {@code true} if relationships were changed, otherwise {@code false}
     */
    protected boolean updateRelationships(IMObjectBean bean, String node, List<? extends Entity> targets) {
        boolean changed = false;
        Set<Reference> targetRefs = new HashSet<>();
        for (Entity entity : targets) {
            targetRefs.add(entity.getObjectReference());
        }
        List<Reference> existing = bean.getTargetRefs(node);
        Set<Reference> toAdd = new HashSet<>(targetRefs);
        toAdd.removeAll(existing);

        Set<Reference> toRemove = new HashSet<>(existing);
        toRemove.removeAll(targetRefs);

        for (Reference reference : toAdd) {
            bean.addTarget(node, reference);
            changed = true;
        }

        for (Reference reference : toRemove) {
            bean.removeTarget(node, reference);
            changed = true;
        }
        return changed;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Sets the display name for the entity id.
     *
     * @param name the display name for the entity id
     * @return this
     */
    @SuppressWarnings("unchecked")
    B entityIdName(String name) {
        this.entityIdName = name;
        return (B) this;
    }
}
