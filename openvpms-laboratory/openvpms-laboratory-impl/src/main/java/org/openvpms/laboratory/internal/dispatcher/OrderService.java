/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.laboratory.order.Order.Status;
import org.openvpms.laboratory.order.Order.Type;

import java.util.Arrays;

/**
 * Laboratory order service.
 *
 * @author Tim Anderson
 */
public class OrderService {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules rules;

    /**
     * Constructs an {@link OrderService}.
     *
     * @param service the archetype service
     * @param rules   the laboratory rules
     */
    public OrderService(ArchetypeService service, LaboratoryRules rules) {
        this.service = service;
        this.rules = rules;
    }

    /**
     * Returns the next PENDING order for a laboratory.
     *
     * @param laboratory the laboratory
     * @return the next PENDING order, or {@code null} if none is found
     */
    public Act next(Entity laboratory) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> from = query.from(Act.class, LaboratoryArchetypes.ORDER);
        Join<Act, IMObject> join = from.join("laboratory");
        join.on(builder.equal(join.get("entity"), laboratory.getObjectReference()));
        query.where(builder.equal(from.get("status"), Status.QUEUED.toString()));
        query.orderBy(builder.asc(from.get("startTime")));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Queues an order for an investigation.
     *
     * @param investigation the investigation
     * @return a new order
     */
    public Act order(Act investigation) {
        return createOrder(investigation, true);
    }

    /**
     * Creates an order.
     * <p/>
     * If the order status is {@link Status#QUEUED}, this queues it for send.
     *
     * @param investigation the investigation
     * @param queue         if {@code true}, set the order status to {@link Status#QUEUED} to queue if for send
     * @return a new order
     */
    public Act createOrder(Act investigation, boolean queue) {
        Act order = rules.createOrder(investigation);
        if (queue) {
            order.setStatus(Status.QUEUED.toString());
            investigation.setStatus2(InvestigationActStatus.SENT);
        } else {
            order.setStatus(Status.PENDING.toString());
            investigation.setStatus2(InvestigationActStatus.CONFIRM);
        }
        service.save(Arrays.asList(investigation, order));
        return order;
    }

    /**
     * Marks an order for cancellation.
     *
     * @param order the order
     * @return {@code true} if the order was marked, {@code false} if it was already queued
     */
    public boolean cancel(Act order) {
        boolean result = false;
        IMObjectBean bean = service.getBean(order);
        String type = bean.getString("type");
        if (Type.NEW.name().equals(type)) {
            bean.setValue("type", Type.CANCEL.name());
            order.setStatus(Status.QUEUED.name());
            service.save(order);
            result = true;
        } else if (Type.CANCEL.name().equals(type)) {
            if (Status.PENDING.name().equals(order.getStatus())) {
                order.setStatus(Status.QUEUED.name());
                service.save(order);
                result = true;
            }
        }
        return result;
    }

}
