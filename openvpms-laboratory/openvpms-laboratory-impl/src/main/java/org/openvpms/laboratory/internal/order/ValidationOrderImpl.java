/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Results;
import org.openvpms.laboratory.service.LaboratoryService;

import java.util.Collections;
import java.util.List;

/**
 * An {@link Order} for the purposes of validation by {@link LaboratoryService#validate(Order)}.
 * <p/>
 * This uses an investigation as a proxy for the order.
 *
 * @author Tim Anderson
 */
public class ValidationOrderImpl extends AbstractOrderImpl {

    /**
     * Constructs a {@link ValidationOrderImpl}.
     *
     * @param investigation the order
     * @param service       the archetype service
     * @param patientRules  the patient rules
     * @param domainService the domain service
     */
    public ValidationOrderImpl(Act investigation, ArchetypeService service, PatientRules patientRules,
                               DomainService domainService) {
        super(investigation, service, patientRules, domainService);
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation. May be {@code null}
     */
    @Override
    public Act getInvestigation() {
        return order;
    }

    /**
     * Returns the investigation identifier.
     *
     * @return the investigation identifier
     */
    @Override
    public long getInvestigationId() {
        return order.getId();
    }

    /**
     * Returns a universally unique identifier (UUID) for the order.
     *
     * @return the stringified form of the UUID
     */
    @Override
    public String getUUID() {
        // NOTE: this is invalid for anything but error reporting
        return order.getLinkId();
    }

    /**
     * Returns the order identifier, issued by the laboratory.
     * <p/>
     * This is short for: {@code getOrderIdentity().getIdentity()}
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public String getOrderId() {
        return null;
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    @Override
    public void setOrderId(String archetype, String id) {
        throw new LaboratoryException(LaboratoryMessages.cannotModifyOrder());
    }

    /**
     * Returns the identity for the order, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public ActIdentity getOrderIdentity() {
        return null;
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param identity the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    @Override
    public void setOrderIdentity(ActIdentity identity) {
        throw new LaboratoryException(LaboratoryMessages.cannotModifyOrder());
    }

    /**
     * Returns the order type.
     *
     * @return the order type
     */
    @Override
    public Type getType() {
        return Type.NEW;
    }

    /**
     * Returns the request status.
     *
     * @return the request status
     */
    @Override
    public Status getStatus() {
        return Status.PENDING;
    }

    /**
     * Returns the status message.
     *
     * @return the status message. May be {@code null}
     */
    @Override
    public String getMessage() {
        return null;
    }

    /**
     * Sets the request status.
     *
     * @param status the new request status
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status) {
        setStatus(status, null);
    }

    /**
     * Sets the request status, along with any message from the laboratory.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status, String message) {
        throw new LaboratoryException(LaboratoryMessages.cannotModifyOrder());
    }

    /**
     * Returns the laboratory report for the order.
     *
     * @return the report
     */
    @Override
    public Report getReport() {
        return new ReportPlaceholder();
    }

    /**
     * Returns a report builder to build the report for this order.
     *
     * @return the report builder
     */
    @Override
    public ReportBuilder getReportBuilder() {
        throw new LaboratoryException(LaboratoryMessages.cannotModifyOrder());
    }

    private static class ReportPlaceholder implements Report {
        @Override
        public String getReportId() {
            return null;
        }

        @Override
        public Status getStatus() {
            return Status.PENDING;
        }

        @Override
        public String getSynchronisationId() {
            return null;
        }

        @Override
        public String getSummary() {
            return null;
        }

        @Override
        public List<Results> getResults() {
            return Collections.emptyList();
        }

        @Override
        public Document getDocument() {
            return null;
        }

        @Override
        public boolean hasExternalResults() {
            return false;
        }
    }
}