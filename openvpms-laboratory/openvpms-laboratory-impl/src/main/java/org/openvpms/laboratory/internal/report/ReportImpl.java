/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.domain.im.act.ActIdentity;
import org.openvpms.component.business.domain.im.act.BeanDocumentActDecorator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.document.CompressedDocumentImpl;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.Results;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Default implementation of {@link Report}.
 *
 * @author Tim Anderson
 */
public class ReportImpl implements Report {

    /**
     * The underlying act.
     */
    private final BeanDocumentActDecorator act;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs a {@link ReportImpl}.
     *
     * @param act           the act. An <em>act.patientInvestigation</em>
     * @param service       the archetype service
     * @param domainService the domain object service
     * @param handlers      the document handlers
     */
    public ReportImpl(DocumentAct act, ArchetypeService service, DomainService domainService,
                      DocumentHandlers handlers) {
        if (!act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            throw new IllegalArgumentException("Invalid argument 'act' of archetype=" + act.getArchetype()
                                               + " passed to ReportImpl");
        }
        this.act = new BeanDocumentActDecorator(act, service);
        this.service = service;
        this.domainService = domainService;
        this.handlers = handlers;
    }

    /**
     * Returns the identity for the report, issued by the laboratory.
     *
     * @return the results identifier, or {@code null} if none has been issued
     */
    @Override
    public String getReportId() {
        ActIdentity id = act.getBean().getObject("reportId", ActIdentity.class);
        return id != null ? id.getIdentity() : null;
    }

    /**
     * Returns the report status.
     *
     * @return the report status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(act.getBean().getString("status2"));
    }

    /**
     * Returns the synchronisation identifier.
     * <p/>
     * This can be used to filter duplicate results if a failure occurs notifying the laboratory that results have been
     * received.
     *
     * @return the synchronisation identifier, or {@code null} if none has been set
     */
    @Override
    public String getSynchronisationId() {
        return act.getBean().getString("synchronisationId");
    }

    /**
     * Returns the report summary.
     *
     * @return the report summary. May be {@code null}
     */
    @Override
    public String getSummary() {
        return act.getBean().getString("summary");
    }

    /**
     * Returns the results.
     *
     * @return the results, in the order they were added
     */
    @Override
    public List<Results> getResults() {
        List<Results> results = new ArrayList<>();
        Comparator<SequencedRelationship> comparator = Comparator.comparingInt(SequencedRelationship::getSequence);
        Policy<SequencedRelationship> policy = Policies.all(SequencedRelationship.class, comparator);
        for (Act act : act.getBean().getTargets("results", Act.class, policy)) {
            results.add(new ResultsImpl(act, service, domainService));
        }
        return results;
    }

    /**
     * Returns the report document.
     *
     * @return the document, or {@code null} if the report has no document.
     */
    @Override
    public Document getDocument() {
        Document result = null;
        Document document = (act.getDocument() != null) ? service.get(act.getDocument(), Document.class) : null;
        if (document != null) {
            result = new CompressedDocumentImpl(document, handlers);
        }
        return result;
    }

    /**
     * Determines if the report has external results.
     *
     * @return {@code true} if the report has external results
     */
    @Override
    public boolean hasExternalResults() {
        return act.getBean().getBoolean("externalResults", false);
    }

}
