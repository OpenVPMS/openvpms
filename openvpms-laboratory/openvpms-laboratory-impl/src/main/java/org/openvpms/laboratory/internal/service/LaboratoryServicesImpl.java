/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.plugin.manager.PluginManager;

/**
 * Default implementation of {@link LaboratoryServices}.
 *
 * @author Tim Anderson
 */
public class LaboratoryServicesImpl implements LaboratoryServices {

    /**
     * The plugin manager.
     */
    private final PluginManager manager;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules rules;


    /**
     * Constructs a {@link LaboratoryServicesImpl}.
     *
     * @param manager the plugin manager
     * @param rules   the laboratory rules
     */
    public LaboratoryServicesImpl(PluginManager manager, LaboratoryRules rules) {
        this.manager = manager;
        this.rules = rules;
    }

    /**
     * Returns the laboratory for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory, or {@code null} if the investigation type is not associated with a laboratory at the
     * specified location
     */
    @Override
    public Entity getLaboratory(Entity investigationType, Party location) {
        return rules.getLaboratory(investigationType, location);
    }

    /**
     * Returns the laboratory service for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory service, or {@code null} if the investigation type is not associated with a laboratory
     * service at the specified location
     * @throws LaboratoryException if the service is unavailable
     */
    @Override
    public LaboratoryService getService(Entity investigationType, Party location) {
        Entity config = getLaboratory(investigationType, location);
        return (config != null) ? getService(config) : null;
    }

    /**
     * Returns the laboratory service for the specified laboratory.
     *
     * @param laboratory the laboratory. Must be an <em>entity.laboratoryService*</em
     * @return the laboratory service
     * @throws LaboratoryException if the service is unavailable
     */
    @Override
    public LaboratoryService getService(Entity laboratory) {
        LaboratoryService result = null;
        String archetype = laboratory.getArchetype();
        if (laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES)) {
            for (LaboratoryService service : manager.getServices(LaboratoryService.class)) {
                if (archetype.equals(service.getLaboratoryArchetype())) {
                    result = service;
                    break;
                }
            }
        }
        if (result == null) {
            throw new LaboratoryException(LaboratoryMessages.serviceUnavailable(laboratory.getName()));
        }
        return result;
    }

}
