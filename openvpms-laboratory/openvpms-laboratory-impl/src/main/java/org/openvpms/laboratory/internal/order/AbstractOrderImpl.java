/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.user.EmployeeImpl;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.practice.Location;
import org.openvpms.domain.user.Employee;
import org.openvpms.laboratory.order.Order;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract implementation of {@link Order}.
 *
 * @author Tim Anderson
 */
abstract class AbstractOrderImpl implements Order {

    /**
     * The order.
     */
    protected final Act order;

    /**
     * The bean wrapping the order
     */
    protected final IMObjectBean bean;

    /**
     * The archetype service.
     */
    protected final ArchetypeService service;

    /**
     * The patient rules.
     */
    protected final PatientRules patientRules;

    /**
     * The domain object service.
     */
    protected final DomainService domainService;

    /**
     * The investigation type.
     */
    private InvestigationType investigationType;

    /**
     * The laboratory device.
     */
    private Device device;

    /**
     * The laboratory.
     */
    private Laboratory laboratory;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The customer.
     */
    private Customer customer;

    /**
     * The location.
     */
    private Location location;

    /**
     * Constructs an {@link AbstractOrderImpl}.
     *
     * @param order         the order
     * @param service       the archetype service
     * @param patientRules  the patient rules
     * @param domainService the domain service
     */
    public AbstractOrderImpl(Act order, ArchetypeService service, PatientRules patientRules,
                             DomainService domainService) {
        this.order = order;
        this.service = service;
        this.patientRules = patientRules;
        this.domainService = domainService;
        bean = service.getBean(order);
    }

    /**
     * Returns the date when the order was created.
     *
     * @return the date
     */
    @Override
    public OffsetDateTime getCreated() {
        return DateRules.toOffsetDateTime(order.getActivityStartTime());
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type.
     */
    @Override
    public InvestigationType getInvestigationType() {
        if (investigationType == null) {
            investigationType = getTarget("investigationType", InvestigationType.class, true);
        }
        return investigationType;
    }

    /**
     * Returns the tests being ordered.
     *
     * @return the tests being ordered
     */
    @Override
    public List<Test> getTests() {
        List<Test> result = new ArrayList<>();
        for (Entity entity : bean.getTargets("tests", Entity.class)) {
            result.add(domainService.create(entity, Test.class));
        }
        return result;
    }

    /**
     * Returns the device to use to perform the tests.
     *
     * @return the device, {@code null} if no device was specified
     */
    @Override
    public Device getDevice() {
        if (device == null) {
            IMObject object = bean.getTarget("device");
            if (object != null) {
                device = domainService.create(object, Device.class);
            }
        }
        return device;
    }

    /**
     * Returns the laboratory used to perform the tests.
     *
     * @return the laboratory
     */
    @Override
    public Laboratory getLaboratory() {
        if (laboratory == null) {
            laboratory = getTarget("laboratory", Laboratory.class, true);
        }
        return laboratory;
    }

    /**
     * Returns the patient that the order is for.
     *
     * @return the patient
     */
    @Override
    public Patient getPatient() {
        if (patient == null) {
            patient = getTarget("patient", Patient.class, true);
        }
        return patient;
    }

    /**
     * Returns the customer responsible for the patient.
     *
     * @return the customer. May be {@code null}
     */
    @Override
    public Customer getCustomer() {
        if (customer == null) {
            Party owner = patientRules.getOwner(order);
            if (owner != null) {
                customer = domainService.create(owner, Customer.class);
            }
        }
        return customer;
    }

    /**
     * Returns the clinician responsible for the order.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public Employee getClinician() {
        return getTarget("clinician", EmployeeImpl.class, false);
    }

    /**
     * Returns the practice location where the order was placed.
     *
     * @return the practice location
     */
    @Override
    public Location getLocation() {
        if (location == null) {
            location = getTarget("location", Location.class, true);
        }
        return location;
    }

    /**
     * Returns the patient visit.
     *
     * @return the patient visit. May be {@code null}
     */
    @Override
    public Visit getVisit() {
        Visit result = null;
        Act investigation = getInvestigation();
        if (investigation != null) {
            Act act = service.getBean(investigation).getSource("event", Act.class);
            if (act != null) {
                result = domainService.create(act, Visit.class);
            }
        }
        return result;
    }

    /**
     * Returns the user that placed the order.
     *
     * @return the user that placed the order
     */
    @Override
    public Employee getUser() {
        User user = bean.getObject("createdBy", User.class);
        return (user != null) ? domainService.create(user, EmployeeImpl.class) : null;
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation. May be {@code null}
     */
    public abstract Act getInvestigation();

    /**
     * Returns the order notes.
     *
     * @return the order notes. May be {@code null}
     */
    @Override
    public String getNotes() {
        return bean.getString("description");
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AbstractOrderImpl) {
            return ((AbstractOrderImpl) obj).order.equals(order);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return order.hashCode();
    }

    /**
     * Returns the target object from the first {@link Relationship} with target object, for the specified relationship
     * node, converting it to the appropriate type using {@link DomainService}.
     *
     * @param name     the node name.
     * @param type     the type to convert to
     * @param required if {@code true}, the target object must be present or an {@link IllegalStateException} will
     *                 be thrown
     * @return the target object, or {@code null} if none is present, and it is not required
     * @throws IllegalStateException if the target isn't present but is required
     */
    private <T> T getTarget(String name, Class<T> type, boolean required) {
        T result = null;
        IMObject object = bean.getTarget(name);
        if (object == null) {
            if (required) {
                throw new IllegalStateException("Order has no " + name + ": " + order.getId());
            }
        } else {
            result = domainService.create(object, type);
        }
        return result;
    }
}