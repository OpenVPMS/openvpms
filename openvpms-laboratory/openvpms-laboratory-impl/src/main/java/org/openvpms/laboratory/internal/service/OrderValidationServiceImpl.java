/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.order.ValidationOrderImpl;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.laboratory.service.OrderValidationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A service to validate laboratory orders prior to their submission to a laboratory.
 *
 * @author Tim Anderson
 */
public class OrderValidationServiceImpl implements OrderValidationService {

    /**
     * The laboratory services.
     */
    private final LaboratoryServices laboratoryServices;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OrderValidationServiceImpl.class);

    /**
     * Constructs an {@link OrderValidationServiceImpl}.
     *
     * @param laboratoryServices the laboratory services
     * @param domainService      the domain object service
     */
    public OrderValidationServiceImpl(LaboratoryServices laboratoryServices, DomainService domainService) {
        this.laboratoryServices = laboratoryServices;
        this.domainService = domainService;
    }

    /**
     * Determines if an order may be submitted for an investigation.
     * <p/>
     * For investigations that:
     * <ul>
     *     <li>have an <em>entity.laboratoryService*</em> laboratory, these will be validated using
     *     the corresponding {@link LaboratoryService}'s {@link LaboratoryService#validate(Order) validate(Order)}
     *     method.</li>
     *     <li>have no laboratory or have an <em>entity.HL7Service*</em> laboratory, these will be treated as valid,
     *     as no further validation is possible.</li>
     * </ul>
     *
     * @param investigation the investigation. Must have {@code PENDING} order status
     * @return the order validation status
     */
    @Override
    public ValidationStatus validate(DocumentAct investigation) {
        ValidationStatus status;
        Entity laboratory = getLaboratory(investigation);
        if (!InvestigationActStatus.PENDING.equals(investigation.getStatus2())) {
            // shouldn't occur
            String name = (laboratory != null) ? laboratory.getName() : null;
            status = new ValidationStatus(name,
                                          OrderValidationStatus.error("Cannot submit orders for investigations with "
                                                                      + investigation.getStatus2() + " status"));
        } else if (laboratory != null) {
            status = validate(investigation, laboratory);
        } else {
            status = new ValidationStatus(null, OrderValidationStatus.valid());
        }
        return status;
    }

    /**
     * Determines if an order may be submitted for an investigation.
     *
     * @param investigation the investigation
     * @param laboratory    the laboratory
     * @return the order validation status
     */
    private ValidationStatus validate(DocumentAct investigation, Entity laboratory) {
        ValidationStatus status;
        LaboratoryService service = null;
        try {
            service = getLaboratoryService(laboratory);
            if (service != null) {
                Order order = domainService.create(investigation, ValidationOrderImpl.class);
                status = new ValidationStatus(getName(service, laboratory), service.validate(order));
            } else {
                status = new ValidationStatus(laboratory.getName(), OrderValidationStatus.valid());
            }
        } catch (Exception exception) {
            log.error("Failed to validate order for investigation={}, laboratory={} ({}): {}", investigation.getId(),
                      laboratory.getName(), laboratory.getId(), exception.getMessage(), exception);
            status = new ValidationStatus(getName(service, laboratory),
                                          OrderValidationStatus.error(exception.getMessage()));
        }
        return status;
    }

    /**
     * Returns the service name if present, otherwise the name associated with the laboratory.
     *
     * @param service    the service. May be {@code null}
     * @param laboratory the laboratory
     * @return the name
     */
    private String getName(LaboratoryService service, Entity laboratory) {
        String result = null;
        if (service != null) {
            try {
                result = service.getName();
            } catch (Exception exception) {
                // no-op
            }
        }
        if (result == null) {
            result = laboratory.getName();
        }
        return result;
    }

    /**
     * Returns the laboratory service associated with an investigation.
     *
     * @param laboratory the laboratory
     * @return the laboratory service, or {@code null} if the investigation isn't managed through a laboratory
     * @throws LaboratoryException if the service is unavailable
     */
    private LaboratoryService getLaboratoryService(Entity laboratory) {
        LaboratoryService result = null;
        if (laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES)) {
            // exclude HL7 laboratories
            result = laboratoryServices.getService(laboratory);
        }
        return result;
    }

    /**
     * Returns the laboratory associated with an investigation.
     *
     * @param investigation the investigation
     * @return the corresponding laboratory, or {@code null} if there is none
     */
    private Entity getLaboratory(DocumentAct investigation) {
        IMObjectBean bean = domainService.getBean(investigation);
        return bean.getTarget("laboratory", Entity.class);
    }
}
