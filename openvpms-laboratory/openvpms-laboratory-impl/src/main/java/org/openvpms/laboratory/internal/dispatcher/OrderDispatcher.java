/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.component.model.act.Act;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.service.LaboratoryService;

/**
 * Queues laboratory orders for dispatch to {@link LaboratoryService}s.
 *
 * @author Tim Anderson
 */
public interface OrderDispatcher {

    /**
     * Creates a laboratory order.
     * <p/>
     * If the investigation has its <em>confirmOrder</em> set to {@code true}, the order will be created for
     * manual submission, otherwise it will be queued.
     *
     * @param investigation the investigation to place the order for
     */
    void create(Act investigation);

    /**
     * Places an order synchronously.
     *
     * @param investigation the investigation
     * @return the order confirmation, or {@code null} if no confirmation is required
     * @throws LaboratoryException if the laboratory cannot be contacted
     */
    Confirmation order(Act investigation);

    /**
     * Queues an order cancellation.
     *
     * @param investigation the investigation to cancel
     */
    void cancel(Act investigation);

}
