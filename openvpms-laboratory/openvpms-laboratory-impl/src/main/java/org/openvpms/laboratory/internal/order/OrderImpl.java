/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.apache.commons.lang.StringUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.InvestigationNotFound;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.internal.report.ReportBuilderImpl;
import org.openvpms.laboratory.internal.report.ReportImpl;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.EnumSet;

/**
 * Default implementation of {@link Order}.
 *
 * @author Tim Anderson
 */
public class OrderImpl extends AbstractOrderImpl {

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * Allowed statuses when cancelling an order.
     */
    private static final EnumSet<Status> CANCEL_STATUSES = EnumSet.of(Status.PENDING, Status.SUBMITTING, Status.ERROR, Status.CANCELLED);

    /**
     * The order id node.
     */
    private static final String ORDER_ID = "orderId";

    /**
     * The type node.
     */
    private static final String TYPE = "type";

    /**
     * The message node.
     */
    private static final String MESSAGE = "message";

    /**
     * Constructs an {@link OrderImpl}.
     *
     * @param order              the order
     * @param service            the archetype service
     * @param patientRules       the patient rules
     * @param domainService      the domain object service
     * @param transactionManager the transaction manager
     * @param handlers           the document handlers
     * @param rules              the document rules
     */
    public OrderImpl(Act order, ArchetypeService service, PatientRules patientRules, DomainService domainService, PlatformTransactionManager transactionManager, DocumentHandlers handlers, DocumentRules rules) {
        super(order, service, patientRules, domainService);
        this.transactionManager = transactionManager;
        this.handlers = handlers;
        this.rules = rules;
    }

    /**
     * Returns the investigation identifier.
     *
     * @return the investigation identifier
     */
    @Override
    public long getInvestigationId() {
        ActIdentity identity = bean.getObject("investigationId", ActIdentity.class);
        return identity != null ? Long.parseLong(identity.getIdentity()) : -1;
    }

    /**
     * Returns a universally unique identifier (UUID) for the order.
     *
     * @return the stringified form of the UUID
     */
    @Override
    public String getUUID() {
        return order.getLinkId();
    }

    /**
     * Returns the order identifier, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public String getOrderId() {
        ActIdentity identity = getOrderIdentity();
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    @Override
    public void setOrderId(String archetype, String id) {
        ActIdentity identity = getOrderIdentity();
        if (identity == null) {
            identity = service.create(archetype, ActIdentity.class);
            bean.addValue(ORDER_ID, identity);
        } else if (!identity.isA(archetype)) {
            throw new LaboratoryException(LaboratoryMessages.differentOrderIdentifierArchetype(identity.getArchetype(), archetype));
        }
        identity.setIdentity(id);
        bean.save();
    }

    /**
     * Returns the identity for the order, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public ActIdentity getOrderIdentity() {
        return bean.getObject(ORDER_ID, ActIdentity.class);
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param identity the order identifier
     */
    @Override
    public void setOrderIdentity(ActIdentity identity) {
        ActIdentity current = getOrderIdentity();
        if (current != null) {
            bean.removeValue(ORDER_ID, current);
        }
        bean.addValue(ORDER_ID, identity);
        bean.save();
    }

    /**
     * Returns the order type.
     *
     * @return the order type
     */
    @Override
    public Type getType() {
        return Type.valueOf(bean.getString(TYPE));
    }

    /**
     * Returns the request status.
     *
     * @return the request status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(order.getStatus());
    }

    /**
     * Sets the request status.
     *
     * @param status the new request status
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status) {
        setStatus(status, null);
    }

    /**
     * Sets the request status, along with any message from the laboratory.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status, String message) {
        Type type = getType();
        if (type == Type.CANCEL && !CANCEL_STATUSES.contains(status)) {
            throw new LaboratoryException(LaboratoryMessages.invalidCancelStatus(status));
        }
        Status existing = getStatus();
        if (status != existing) {
            if (existing == Status.ERROR && type != Type.CANCEL) {
                throw new LaboratoryException(LaboratoryMessages.orderWithErrorStatusMustBeCancelled());
            }
            order.setStatus(status.name());
            setMessage(bean, message);
            if (status == Status.CONFIRM) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.CONFIRM, message);
                }
            }
            if (status == Status.SUBMITTED) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.SENT, message);
                }
            } else if (status == Status.COMPLETED) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.RECEIVED, message);
                }
            } else if (status == Status.ERROR) {
                setInvestigationOrderStatus(InvestigationActStatus.ERROR, message);
            } else if (status == Status.CANCELLED) {
                bean.setValue(TYPE, Type.CANCEL.name());
                order.setStatus(Status.CANCELLED.name());
                withTransaction(() -> {
                    Act investigation = getInvestigation();
                    if (investigation != null && !ActStatus.CANCELLED.equals(investigation.getStatus())) {
                        // NOTE: the investigation could be POSTED due to record locking. It may still be CANCELLED.
                        investigation.setStatus(ActStatus.CANCELLED);
                        setMessage(service.getBean(investigation), message);
                        service.save(investigation);
                    }
                    bean.save();
                });
            } else {
                bean.save();
            }
        }
    }

    /**
     * Returns the status message.
     *
     * @return the status message. May be {@code null}
     */
    @Override
    public String getMessage() {
        return bean.getString(MESSAGE);
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation. May be {@code null}
     */
    @Override
    public Act getInvestigation() {
        return getInvestigation(false);
    }

    /**
     * Returns the laboratory report for the order.
     *
     * @return the report
     * @throws InvestigationNotFound if the investigation cannot be found
     */
    @Override
    public Report getReport() {
        Act investigation = getInvestigation(true);
        return domainService.create(investigation, ReportImpl.class);
    }

    /**
     * Returns a report builder to build the report for this order.
     *
     * @return the report builder
     */
    @Override
    public ReportBuilder getReportBuilder() {
        return new ReportBuilderImpl(getInvestigation(true), order, service, domainService, handlers, rules,
                                     transactionManager);
    }

    /**
     * Saves the order and investigation in a transaction.
     * <p/>
     * The status and message are ignored if the investigation is cancelled, or the status is unchanged.
     *
     * @param status  the new investigation status
     * @param message the new investigation message. May be {@code null}
     */
    private void setInvestigationOrderStatus(String status, String message) {
        withTransaction(() -> {
            Act investigation = getInvestigation();
            if (investigation != null) {
                if (!status.equals(investigation.getStatus2())
                    && !ActStatus.CANCELLED.equals(investigation.getStatus())) {
                    investigation.setStatus2(status);
                    setMessage(service.getBean(investigation), message);
                    service.save(investigation);
                }
            }
            bean.save();
        });
    }

    /**
     * Sets the message node on a bean, abbreviating it if required.
     *
     * @param bean    the bean
     * @param message the message. May be {@code null}
     */
    private void setMessage(IMObjectBean bean, String message) {
        if (message != null) {
            message = StringUtils.abbreviate(message, bean.getMaxLength(MESSAGE));
        }
        bean.setValue(MESSAGE, message);
    }

    /**
     * Return the investigation associated with the order.
     *
     * @param fail if {@code true}, fail with an {@link InvestigationNotFound} if the investigation no longer exists
     * @return the investigation, or {@code null} if the investigation was not found and {@code fail == false}
     * @throws InvestigationNotFound if the investigation was not found and {@code fail == true}
     */
    private DocumentAct getInvestigation(boolean fail) {
        DocumentAct result = bean.getSource("investigation", DocumentAct.class);
        if (result == null && fail) {
            throw new InvestigationNotFound(LaboratoryMessages.investigationNotFound(getInvestigationId()));
        }
        return result;
    }

    /**
     * Runs code in a transaction.
     *
     * @param runnable the code to execute
     */
    private void withTransaction(Runnable runnable) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                runnable.run();
            }
        });
    }
}
