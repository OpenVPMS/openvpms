/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.LongTextReader;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.domain.im.act.BeanActDecorator;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.document.CompressedDocumentImpl;
import org.openvpms.laboratory.report.Result;

import java.math.BigDecimal;

import static org.openvpms.laboratory.internal.report.ResultsImpl.LONG_NOTES;
import static org.openvpms.laboratory.internal.report.ResultsImpl.NOTES;

/**
 * Default implementation of {@link Result}.
 *
 * @author Tim Anderson
 */
public class ResultImpl implements Result {

    /**
     * The underlying act.
     */
    private final BeanActDecorator act;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The long text node reader.
     */
    private final LongTextReader reader;

    /**
     * Result id node name.
     */
    static final String RESULT_ID = "resultId";

    /**
     * Status node name.
     */
    static final String STATUS = "status";

    /**
     * Analyte code node name.
     */
    static final String ANALYTE_CODE = "analyteCode";

    /**
     * Analyte name node name.
     */
    static final String ANALYTE_NAME = "name";

    /**
     * Text result node name.
     */
    static final String RESULT = "result";

    /**
     * Numeric result node name.
     */
    static final String VALUE = "value";

    /**
     * Long text result node name.
     */
    static final String LONG_RESULT = "longResult";

    /**
     * Result units node name.
     */
    static final String UNITS = "units";

    /**
     * Result qualifier node name.
     */
    static final String QUALIFIER = "qualifier";

    /**
     * Low range node name.
     */
    static final String LOW_RANGE = "lowRange";

    /**
     * High range node name.
     */
    static final String HIGH_RANGE = "highRange";

    /**
     * Extreme low range node name.
     */
    static final String EXTREME_LOW_RANGE = "extremeLowRange";

    /**
     * Extreme high range node name.
     */
    static final String EXTREME_HIGH_RANGE = "extremeHighRange";

    /**
     * Out of range node name.
     */
    static final String OUT_OF_RANGE = "outOfRange";

    /**
     * Reference range node name.
     */
    static final String REFERENCE_RANGE = "referenceRange";

    /**
     * Image node name.
     */
    static final String IMAGE = "image";

    /**
     * Constructs a {@link ResultImpl}.
     *
     * @param act      the peer to delegate to
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    public ResultImpl(Act act, ArchetypeService service, DocumentHandlers handlers) {
        this.act = new BeanActDecorator(act, service);
        this.handlers = handlers;
        this.reader = new LongTextReader(service);
    }

    /**
     * Creates a {@link ResultImpl}.
     *
     * @param bean     the bean wrapping the act
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    ResultImpl(IMObjectBean bean, ArchetypeService service, DocumentHandlers handlers) {
        this.act = new BeanActDecorator(bean);
        this.handlers = handlers;
        this.reader = new LongTextReader(service);
    }

    /**
     * Returns the result identifier.
     *
     * @return the result identifier
     */
    @Override
    public String getResultId() {
        return act.getBean().getString(RESULT_ID);
    }

    /**
     * Returns the result status.
     *
     * @return the result status
     */
    @Override
    public Status getStatus() {
        return getStatus(act.getStatus());
    }

    /**
     * Returns an identifier for the analyte.
     *
     * @return the analyte identifier. May be {@code null}
     */
    @Override
    public String getAnalyteCode() {
        return act.getBean().getString(ANALYTE_CODE);
    }

    /**
     * Returns the name of the analyte for which the result is being reported.
     *
     * @return the analyte name
     */
    @Override
    public String getAnalyteName() {
        return act.getBean().getString(ANALYTE_NAME);
    }

    /**
     * Returns the numerical result for the analyte.
     *
     * @return the numerical result for the analyte. May be {@code null}
     */
    @Override
    public BigDecimal getValue() {
        return act.getBean().getBigDecimal(VALUE);
    }

    /**
     * Returns the result for the analyte, as text.
     *
     * @return the result text. May be {@code null}
     */
    @Override
    public String getResult() {
        return getLongText(RESULT, LONG_RESULT);
    }

    /**
     * Returns the unit of measure for the analyte result.
     *
     * @return the unit of measure. May be {@code null}
     */
    @Override
    public String getUnits() {
        return act.getBean().getString(UNITS);
    }

    /**
     * Returns the qualifier.
     *
     * @return the qualifier. May be {@code null}
     */
    @Override
    public String getQualifier() {
        return act.getBean().getString(QUALIFIER);
    }

    /**
     * Returns the low reference range value for the analyte.
     *
     * @return the low reference range value. May be {@code null}
     */
    @Override
    public BigDecimal getLowRange() {
        return act.getBean().getBigDecimal(LOW_RANGE);
    }

    /**
     * Returns the high reference range value for the analyte.
     *
     * @return the high reference range value. May be {@code null}
     */
    @Override
    public BigDecimal getHighRange() {
        return act.getBean().getBigDecimal(HIGH_RANGE);
    }

    /**
     * Returns the extreme low reference range value for the analyte.
     *
     * @return the extreme low reference range value. May be {@code null}
     */
    @Override
    public BigDecimal getExtremeLowRange() {
        return act.getBean().getBigDecimal(EXTREME_LOW_RANGE);
    }

    /**
     * Returns the extreme high reference range value for the analyte.
     *
     * @return the extreme high reference range value. May be {@code null}
     */
    @Override
    public BigDecimal getExtremeHighRange() {
        return act.getBean().getBigDecimal(EXTREME_HIGH_RANGE);
    }

    /**
     * Determines if the result is out of range.
     *
     * @return {@code true} if the result is out of range, otherwise {@code false}
     */
    @Override
    public boolean getOutOfRange() {
        return act.getBean().getBoolean(OUT_OF_RANGE);
    }

    /**
     * Sets the reference range.
     *
     * @return the reference range. May be {@code null}
     */
    @Override
    public String getReferenceRange() {
        return act.getBean().getString(REFERENCE_RANGE);
    }

    /**
     * Returns a short narrative of the result.
     *
     * @return the result narrative. May be {@code null}
     */
    @Override
    public String getNotes() {
        return getLongText(NOTES, LONG_NOTES);
    }

    /**
     * Returns the result image.
     *
     * @return the document, or {@code null} if the result has no image.
     */
    @Override
    public org.openvpms.component.model.document.Document getImage() {
        org.openvpms.component.model.document.Document result = null;
        IMObjectBean bean = act.getBean();
        DocumentAct act = bean.getTarget(IMAGE, DocumentAct.class);
        if (act != null && act.getDocument() != null) {
            Document document = bean.getObject(act.getDocument(), Document.class);
            if (document != null) {
                result = new CompressedDocumentImpl(document, handlers);
            }
        }
        return result;
    }

    /**
     * Returns the underlying act.
     *
     * @return the act
     */
    public Act getAct() {
        return act;
    }

    /**
     * Converts a status string to an enum.
     *
     * @param status the status string
     * @return the corresponding status
     */
    static Status getStatus(String status) {
        if (WorkflowStatus.IN_PROGRESS.equals(status)) {
            return Status.IN_PROGRESS;
        } else if (WorkflowStatus.COMPLETED.equals(status)) {
            return Status.COMPLETED;
        } else if (WorkflowStatus.CANCELLED.equals(status)) {
            return Status.CANCELLED;
        }
        return Status.PENDING;
    }

    /**
     * Returns the value of a text node, falling back to the long text node if it isn't present.
     *
     * @param node     the text node name
     * @param longNode the text
     * @return the text. May be {@code null}
     */
    private String getLongText(String node, String longNode) {
        return reader.getText(act, node, longNode);
    }
}
