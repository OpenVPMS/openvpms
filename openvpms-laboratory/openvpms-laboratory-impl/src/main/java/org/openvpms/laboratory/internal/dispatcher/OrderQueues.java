/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.archetype.component.dispatcher.MonitoredQueues;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.entity.Entity;

/**
 * Manages {@link OrderQueue} instances.
 *
 * @author Tim Anderson
 */
class OrderQueues extends MonitoredQueues<Entity, OrderQueue> {

    /**
     * The order service.
     */
    private final OrderService orderService;

    /**
     * Constructs a {@link OrderQueues}.
     *
     * @param orderService the order service
     * @param service      the archetype service
     */
    OrderQueues(OrderService orderService, IArchetypeService service) {
        super(service, LaboratoryArchetypes.LABORATORY_SERVICES, Entity.class);
        this.orderService = orderService;
        load();
    }

    /**
     * Crates a new queue.
     *
     * @param owner the queue owner
     * @return a new queue
     */
    @Override
    protected OrderQueue createQueue(Entity owner) {
        return new OrderQueue(owner, orderService);
    }
}
