/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.laboratory.service.Laboratories;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of the {@link Laboratories} interface.
 *
 * @author Tim Anderson
 */
public class LaboratoriesImpl implements Laboratories {

    /**
     * The archetype service.
     */
    private final IArchetypeRuleService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link LaboratoriesImpl}.
     *
     * @param service       the archetype service
     * @param domainService the domain object service
     */
    public LaboratoriesImpl(IArchetypeRuleService service, DomainService domainService) {
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Returns a laboratory given its reference.
     *
     * @param reference the reference
     * @return the corresponding laboratory, or {@code null} if none is found
     */
    @Override
    public Laboratory getLaboratory(Reference reference) {
        return domainService.get(reference, Laboratory.class);
    }

    /**
     * Return a laboratory corresponding to an object.
     *
     * @param object the object
     * @return the corresponding laboratory
     */
    @Override
    public Laboratory getLaboratory(IMObject object) {
        return domainService.create(object, Laboratory.class);
    }

    /**
     * Returns laboratories of the specified archetype.
     *
     * @param archetype  the archetype. Must be an <em>entity.laboratoryService*</em>
     * @param activeOnly if {@code true}, only return active laboratories
     * @return the laboratories
     */
    @Override
    public List<Laboratory> getLaboratories(String archetype, boolean activeOnly) {
        List<Laboratory> result = new ArrayList<>();
        if (!TypeHelper.matches(archetype, LaboratoryArchetypes.LABORATORY_SERVICES)) {
            throw new IllegalStateException("Invalid laboratory archetype: " + archetype);
        }
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, archetype);
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : service.createQuery(query).getResultList()) {
            result.add(domainService.create(entity, Laboratory.class));
        }
        return result;
    }
}
