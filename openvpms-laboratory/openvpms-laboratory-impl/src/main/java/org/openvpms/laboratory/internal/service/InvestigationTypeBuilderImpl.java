/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.laboratory.service.InvestigationTypeBuilder;
import org.openvpms.laboratory.service.InvestigationTypes;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;
import java.util.Objects;

/**
 * Default implementation of {@link InvestigationTypes}.
 *
 * @author Tim Anderson
 */
public class InvestigationTypeBuilderImpl extends EntityBuilder<InvestigationType, InvestigationTypeBuilderImpl>
        implements InvestigationTypeBuilder {

    /**
     * The investigation types.
     */
    private final InvestigationTypes types;

    /**
     * The laboratory that manages the investigation type.
     */
    private Laboratory laboratory;

    /**
     * The devices that can perform tests for the investigation type.
     */
    private List<Device> devices;

    /**
     * Constructs a {@link InvestigationTypeBuilderImpl}.
     *
     * @param types              the investigation types
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    InvestigationTypeBuilderImpl(InvestigationTypes types, ArchetypeService service,
                                 PlatformTransactionManager transactionManager, DomainService domainService) {
        super(LaboratoryArchetypes.INVESTIGATION_TYPE, LaboratoryArchetypes.INVESTIGATION_TYPE_IDS,
              InvestigationType.class, service, transactionManager, domainService);
        this.types = types;
        reset();
    }

    /**
     * Sets the investigation type id, used for identifying the investigation type.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param id        the investigation type id. This must be unique to the archetype
     * @return this
     */
    @Override
    public InvestigationTypeBuilder typeId(String archetype, String id) {
        return entityId(archetype, id);
    }

    /**
     * Sets the investigation type id, used for identifying the investigation type.
     *
     * @param archetype the investigation type identity archetype. Must be an <em>entityIdentity.investigationType*</em>
     * @param id        the investigation type id. This must be unique to the archetype
     * @param name      the investigation type id name
     * @return this
     */
    @Override
    public InvestigationTypeBuilder typeId(String archetype, String id, String name) {
        entityId(archetype, id);
        return entityIdName(name);
    }

    /**
     * Sets the laboratory that manages the investigation type.
     *
     * @param laboratory the laboratory
     * @return this
     */
    @Override
    public InvestigationTypeBuilder laboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
        return this;
    }

    /**
     * Sets the devices that can perform tests for the investigation type.
     *
     * @param devices the devices
     */
    @Override
    public InvestigationTypeBuilder devices(List<Device> devices) {
        this.devices = devices;
        return this;
    }

    /**
     * Returns the entity corresponding to the identifier.
     *
     * @param archetype the entity id archetype
     * @param id        the entity id
     * @return the entity, or {@code null} if none exists
     */
    @Override
    protected Entity getEntity(String archetype, String id) {
        return types.getInvestigationType(archetype, id);
    }

    /**
     * Populates a device.
     *
     * @param entity the device to populate
     * @return {@code true} if the device was updated
     */
    @Override
    protected boolean populate(Entity entity) {
        boolean changed = super.populate(entity);
        IMObjectBean bean = getService().getBean(entity);
        if (laboratory != null && !Objects.equals(bean.getTargetRef("laboratory"), laboratory.getObjectReference())) {
            bean.setTarget("laboratory", laboratory);
            changed = true;
        }
        if (devices != null) {
            changed |= updateRelationships(bean, "devices", devices);
        }
        return changed;
    }

    /**
     * Resets the builder.
     */
    @Override
    protected void reset() {
        super.reset();
        laboratory = null;
        devices = null;
    }
}
