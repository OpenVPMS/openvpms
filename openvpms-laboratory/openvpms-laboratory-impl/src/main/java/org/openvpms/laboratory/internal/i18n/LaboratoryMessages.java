/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.laboratory.order.Order;

/**
 * Laboratory messages.
 *
 * @author Tim Anderson
 */
public class LaboratoryMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("LAB", LaboratoryMessages.class);

    /**
     * Default constructor.
     */
    private LaboratoryMessages() {
        // no-op
    }

    /**
     * Message indicating that a laboratory service is unavailable.
     *
     * @param name the service configuration name
     * @return a new message
     */
    public static Message serviceUnavailable(String name) {
        return messages.create(1, name);
    }

    /**
     * Message indicating that the order identifier archetype cannot be changed.
     *
     * @param current the current archetype
     * @param other   the other archetype
     * @return a new message
     */
    public static Message differentOrderIdentifierArchetype(String current, String other) {
        return messages.create(5, current, other);
    }

    /**
     * Message indicating investigation could not be found.
     *
     * @param investigationId the investigation identifier
     * @return a new message
     */
    public static Message investigationNotFound(long investigationId) {
        return messages.create(6, investigationId);
    }

    /**
     * Message indicating that a status is invalid for an order cancellation.
     *
     * @param status the status
     * @return a new message
     */
    public static Message invalidCancelStatus(Order.Status status) {
        return messages.create(10, status);
    }

    /**
     * Message indicating that an order with ERROR status can only be cancelled.
     *
     * @return a new message
     */
    public static Message orderWithErrorStatusMustBeCancelled() {
        return messages.create(11);
    }

    /**
     * Message indicating that an order cannot be modified.
     *
     * @return a new message
     */
    public static Message cannotModifyOrder() {
        return messages.create(12);
    }

    /**
     * Message indicating that a report couldn't be created.
     *
     * @param message the reason
     * @return a new message
     */
    public static Message failedToCreateReport(String message) {
        return messages.create(20, message);
    }

    /**
     * Message indicating that results cannot be built as no result identifier has been specified.
     *
     * @return a new message
     */
    public static Message noResultsId() {
        return messages.create(21);
    }

    /**
     * Message indicating that results cannot be built as no result identifier has been specified.
     *
     * @return a new message
     */
    public static Message noResultId() {
        return messages.create(22);
    }

    /**
     * Message indicating that an image is not supported for use with a result.
     *
     * @param fileName the image file name
     * @param mimeType the image mime type
     * @return a new message
     */
    public static Message unsupportedImage(String fileName, String mimeType) {
        return messages.create(23, fileName, mimeType);
    }
}
