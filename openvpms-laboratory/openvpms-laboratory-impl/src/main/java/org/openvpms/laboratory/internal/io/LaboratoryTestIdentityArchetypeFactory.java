/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptors;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.IOException;
import java.io.InputStream;

/**
 * Factory for <em>entityIdentity.laboratoryTest</em> archetypes.
 *
 * @author Tim Anderson
 */
class LaboratoryTestIdentityArchetypeFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The template archetype.
     */
    private static final String TEMPLATE = "/org/openvpms/archetype/laboratory/entityIdentity.laboratoryTest.adl";

    /**
     * Constructs a {@link LaboratoryTestIdentityArchetypeFactory}.
     *
     * @param service the archetype service
     */
    public LaboratoryTestIdentityArchetypeFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the laboratory test identity archetype for a laboratory.
     *
     * @param laboratory the laboratory
     * @return the archetype, or {@code null} if none exists
     */
    public String getArchetype(String laboratory) {
        checkLaboratory(laboratory);
        String archetype = getTestCodeArchetype(laboratory);
        String match = null;
        for (String existing : service.getArchetypes(LaboratoryArchetypes.TEST_CODES, false)) {
            if (existing.equalsIgnoreCase(archetype)) {
                match = existing;
                break;
            }
        }
        return match;
    }

    /**
     * Creates a new <em>entityIdentity.laboratoryTest</em> archetype for a laboratory.
     *
     * @param laboratory the laboratory
     * @return the archetype
     */
    public String create(String laboratory) {
        checkLaboratory(laboratory);
        ArchetypeDescriptors descriptors;
        try (InputStream stream = getClass().getResourceAsStream(TEMPLATE)) {
            if (stream == null) {
                throw new IllegalStateException("Archetype descriptor not found: " + TEMPLATE);
            }
            descriptors = ArchetypeDescriptors.read(stream);
        } catch (IOException exception) {
            throw new IllegalStateException("Could not read archetype descriptor: " + TEMPLATE, exception);
        }
        ArchetypeDescriptor descriptor = descriptors.getArchetypeDescriptors().get(LaboratoryArchetypes.TEST_CODE);
        if (descriptor == null) {
            throw new IllegalStateException(LaboratoryArchetypes.TEST_CODE + " archetype not found");
        }
        descriptor.setDisplayName(laboratory + " Test Code");
        String archetype = getTestCodeArchetype(laboratory);
        descriptor.setName(archetype + ".1.0");
        service.save(descriptor);
        return archetype;
    }

    /**
     * Verifies that a laboratory is alphanumeric.
     *
     * @param laboratory the laboratory to check
     */
    private void checkLaboratory(String laboratory) {
        if (!StringUtils.isAlphanumeric(laboratory)) {
            throw new IllegalArgumentException("Argument 'laboratory' must be alphanumeric");
        }
    }

    /**
     * Returns the test code archetype for a laboratory.
     *
     * @param laboratory the laboratory
     * @return the archetype
     */
    private String getTestCodeArchetype(String laboratory) {
        return LaboratoryArchetypes.TEST_CODE + laboratory;
    }
}