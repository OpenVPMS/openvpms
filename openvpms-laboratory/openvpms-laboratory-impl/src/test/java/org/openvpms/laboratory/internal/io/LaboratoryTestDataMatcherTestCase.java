/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryTestBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link LaboratoryTestDataMatcher} class.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestDataMatcherTestCase extends AbstractLaboratoryIOTestCase {

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The product price rules.
     */
    @Autowired
    private ProductPriceRules productPriceRules;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;


    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The laboratory test data matcher.
     */
    private LaboratoryTestDataMatcher matcher;

    /**
     * The test identity archetype factory.
     */
    private LaboratoryTestIdentityArchetypeFactory factory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        matcher = new LaboratoryTestDataMatcher(laboratoryRules, productPriceRules, practiceRules,
                                                getArchetypeService());
        factory = new LaboratoryTestIdentityArchetypeFactory(getArchetypeService());
    }

    /**
     * Tests {@link LaboratoryTestDataMatcher#matchTests(List)} for new tests.
     */
    @Test
    public void testMatchForNewTests() {
        String codeA = ValueStrategy.randomString("AAA");
        String codeB = ValueStrategy.randomString("BBB");
        LaboratoryTestData data1 = new LaboratoryTestData("ALab", codeA, "AAA Test", "AAA Test Description",
                                                          "AAA Test Specimen details", "AAA Test Turnaround",
                                                          null, BigDecimal.ONE, 1, null);
        LaboratoryTestData data2 = new LaboratoryTestData("ALab", codeB, "BBB Test", "BBB Test Description",
                                                          "BBB Test Specimen details", "BBB Test Turnaround",
                                                          null, BigDecimal.TEN, 2, null);
        LaboratoryTestDataSet set = matcher.matchTests(Arrays.asList(data1, data2));
        assertEquals(2, set.getData().size());
        assertEquals(0, set.getErrors().size());
        checkData(set.getData().get(0), "ALab", codeA, "AAA Test", "AAA Test Description",
                  "AAA Test Specimen details", "AAA Test Turnaround", null, 1, 1);
        checkData(set.getData().get(1), "ALab", codeB, "BBB Test", "BBB Test Description",
                  "BBB Test Specimen details", "BBB Test Turnaround", null, 10, 2);
    }

    /**
     * Tests {@link LaboratoryTestDataMatcher#matchTests(List)} where the tests already exist.
     */
    @Test
    public void testMatchForExistingTests() {
        String laboratory = ValueStrategy.randomString("ZLab");
        String archetype = factory.create(laboratory);
        String code1 = ValueStrategy.randomString("AAA");
        String code2 = ValueStrategy.randomString("BBB");
        Entity investigationType = laboratoryFactory.createInvestigationType();
        TestLaboratoryTestBuilder testBuilder = laboratoryFactory.newTest()
                .investigationType(investigationType)
                .code(archetype, code1)
                .name("AAA Test")
                .price(1);
        Entity testA = testBuilder.build();

        testBuilder.code(archetype, code2)
                .name("B Test")
                .price(2)
                .build();

        LaboratoryTestData data1 = new LaboratoryTestData(laboratory, code1, "AAA Test", "AAA Test Description",
                                                          "AAA Test Specimen details",
                                                          "AAA Test Turnaround", null, BigDecimal.ONE, 1, null);
        LaboratoryTestData data2 = new LaboratoryTestData(laboratory, code2, "BBB Test", "BBB Test Description",
                                                          "BBB Test Specimen details",
                                                          "BBB Test Turnaround", null, BigDecimal.TEN, 2, null);
        LaboratoryTestDataSet set = matcher.matchTests(Arrays.asList(data1, data2));

        assertEquals(1, set.getData().size());

        checkData(set.getData().get(0), laboratory, code1, "AAA Test", "AAA Test Description",
                  "AAA Test Specimen details", "AAA Test Turnaround", 1, 1, testA, investigationType, 1, null);

        assertEquals(1, set.getErrors().size());
        checkData(set.getErrors().get(0), laboratory, code2, "BBB Test", "BBB Test Description",
                  "BBB Test Specimen details", "BBB Test Turnaround", null, 10, null, null,
                  2, "The code '" + code2 + "' for test 'BBB Test' matches a different test: 'B Test'");
    }

    /**
     * Tests {@link LaboratoryTestDataMatcher#matchTests(List)} where the tests already exist and are linked
     * to different investigation types.
     */
    @Test
    public void testMatchForExistingTestsWithDifferentInvestigationTypes() {
        String laboratory = ValueStrategy.randomString("ZLab");
        String archetype = factory.create(laboratory);
        String code1 = ValueStrategy.randomString("AAA");
        String code2 = ValueStrategy.randomString("BBB");
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        Entity investigationType2 = laboratoryFactory.createInvestigationType();
        Entity test1 = laboratoryFactory.newTest()
                .investigationType(investigationType1)
                .code(archetype, code1)
                .name("AAA Test")
                .build();

        laboratoryFactory.newTest()
                .investigationType(investigationType2)
                .code(archetype, code2)
                .name("BBB Test")
                .build();

        LaboratoryTestData data1 = new LaboratoryTestData(laboratory, code1, "AAA Test", "AAA Test Description",
                                                          "AAA Test Specimen details",
                                                          "AAA Test Turnaround", null, BigDecimal.ONE, 1, null);
        LaboratoryTestData data2 = new LaboratoryTestData(laboratory, code2, "BBB Test", "BBB Test Description",
                                                          "BBB Test Specimen details",
                                                          "BBB Test Turnaround", null, BigDecimal.TEN, 2, null);
        LaboratoryTestDataSet set = matcher.matchTests(Arrays.asList(data1, data2));

        assertEquals(1, set.getData().size());

        checkData(set.getData().get(0), laboratory, code1, "AAA Test", "AAA Test Description",
                  "AAA Test Specimen details", "AAA Test Turnaround", null, 1, test1, investigationType1, 1, null);

        assertEquals(1, set.getErrors().size());
        checkData(set.getErrors().get(0), laboratory, code2, "BBB Test", "BBB Test Description",
                  "BBB Test Specimen details", "BBB Test Turnaround", null, 10, null, null,
                  2, "Cannot load to multiple investigation types: " + investigationType2.getName());
    }

    /**
     * Tests the {@link LaboratoryTestDataMatcher#matchProducts(List, Date)} method.
     */
    @Test
    public void testMatchProductData() {
        String laboratory = ValueStrategy.randomString("ZLab");
        String archetype = factory.create(laboratory);
        String code1 = ValueStrategy.randomString("AAA");
        String code2 = ValueStrategy.randomString("BBB");
        String code3 = ValueStrategy.randomString("CCC");
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity test1 = laboratoryFactory.newTest()
                .investigationType(investigationType)
                .code(archetype, code1)
                .name("AAA Test")
                .price(2)
                .build();

        Product product1 = productFactory.newService()
                .addTests(test1)
                .newUnitPrice()
                .fromDate(DateRules.getYesterday())
                .costAndPrice(1, 2)
                .add()
                .build();

        Entity test2 = laboratoryFactory.newTest()
                .investigationType(investigationType)
                .code(archetype, code2)
                .name("BBB Test")
                .price(2)
                .build();

        Product product2 = productFactory.newService()
                .addTests(test2)
                .newUnitPrice()
                .fromDate(DateRules.getYesterday())
                .costAndPrice(4, 8)
                .add()
                .build();

        Entity test3 = laboratoryFactory.newTest()
                .investigationType(investigationType)
                .code(archetype, code3)
                .name("CCC Test")
                .price(1)
                .build();

        Product product3 = productFactory.newService()
                .addTests(test3)
                .newUnitPrice()
                .fromDate(DateRules.getYesterday())
                .costAndPrice(1, 2)
                .add()
                .build();
        assertNotNull(get(product3));

        Product product4 = productFactory.newService()
                .addTests(test1, test2)
                .newUnitPrice()
                .fromDate(DateRules.getYesterday())
                .costAndPrice(1, 2)
                .add()
                .build();

        LaboratoryTestData data1 = new LaboratoryTestData(
                new LaboratoryTestData(laboratory, code1, "AAA Test", "AAA Test Description",
                                       "AAA Test Specimen details",
                                       "AAA Test Turnaround", null, BigDecimal.ONE, 1, null),
                test1, investigationType);
        LaboratoryTestData data2 = new LaboratoryTestData(
                new LaboratoryTestData(laboratory, code2, "BBB Test", "BBB Test Description",
                                       "BBB Test Specimen details",
                                       "BBB Test Turnaround", new BigDecimal(4), new BigDecimal(2),
                                       2, null),
                test2, investigationType);
        LaboratoryTestData data3 = new LaboratoryTestData(
                new LaboratoryTestData(laboratory, code3, "CCC Test", "CCC Test Description",
                                       "CCC Test Specimen details",
                                       "CCC Test Turnaround", BigDecimal.ONE, BigDecimal.ONE,
                                       3, null),
                test3, investigationType);

        List<LaboratoryTestProductData> productData
                = matcher.matchProducts(Arrays.asList(data1, data2, data3), new Date());
        assertEquals(3, productData.size());
        // product3 excluded as the cost price hasn't changed
        checkProductData(productData.get(0), product1, 1, 2, 4, Collections.singletonList(test1), true);
        checkProductData(productData.get(1), product4, 1, 4, 8, Arrays.asList(test1, test2), true);
        checkProductData(productData.get(2), product2, 4, 2, 4, Collections.singletonList(test2), false);
    }

    /**
     * Verifies product data matches that expected.
     *
     * @param data          the data to check
     * @param product       the expected product
     * @param oldCost       the expected old cost
     * @param newCost       the expected new cost
     * @param newPrice      the expected new price
     * @param tests         the expected tests
     * @param costIncreased the expected cost-increased flag
     */
    private void checkProductData(LaboratoryTestProductData data, Product product, int oldCost, int newCost,
                                  int newPrice, List<Entity> tests, boolean costIncreased) {
        assertEquals(product, data.getOldProduct());
        assertEquals(product, data.getNewProduct());
        checkEquals(oldCost, data.getOldCost());
        checkEquals(newCost, data.getNewCost());
        assertTrue(CollectionUtils.isEqualCollection(tests, data.getTests()));
        assertEquals(costIncreased, data.costIncreased());

        // verify the price has been added to the product, but not saved
        ProductPrice price = productPriceRules.getProductPrice(data.getNewProduct(),
                                                               ProductArchetypes.UNIT_PRICE, new Date(), null);
        assertNotNull(price);
        assertTrue(price.isNew());
        checkEquals(newCost, productPriceRules.getCostPrice(price));
        checkEquals(newPrice, price.getPrice());
    }
}