/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.patient.Patient;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.OrderQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link OrderQueryImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrderQueryImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The order factory.
     */
    private OrderFactory orderFactory;

    /**
     * The test location.
     */
    private Party location;

    /**
     * The test laboratory.
     */
    private Entity laboratory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        orderFactory = new OrderFactory(getArchetypeService(), patientRules, domainService, transactionManager);
        location = practiceFactory.createLocation();
        laboratory = laboratoryFactory.createLaboratory(location);
    }

    /**
     * Tests querying by investigation identifier.
     */
    @Test
    public void testQueryByInvestigationId() {
        Order order1 = createOrder("Spot");
        Order order2 = createOrder("Fido");

        OrderQuery query = createQuery();

        query.investigationId(order1.getInvestigationId());
        checkQuery(query, order1);

        query.investigationId(order2.getInvestigationId());
        checkQuery(query, order2);

        // check no match
        query.investigationId(-1);
        checkQuery(query);
    }

    /**
     * Tests querying by device.
     */
    @Test
    public void testQueryByDevice() {
        Entity deviceA = laboratoryFactory.createDevice(laboratory);
        Entity deviceB = laboratoryFactory.createDevice(laboratory);

        Order order1 = createOrder(deviceA);
        Order order2 = createOrder(deviceA);
        Order order3 = createOrder(deviceB);
        Order order4 = createOrder((Entity) null); // no device

        // verify that orders can be filtered by specific devices
        OrderQuery query1 = createQuery();

        checkDeviceQuery(query1, deviceA, order1, order2);
        checkDeviceQuery(query1, deviceB, order3);

        // verify that when no device is specified, all orders are returned
        checkDeviceQuery(query1, null, order1, order2, order3, order4);

        // now verify that only those orders with a device are returned
        OrderQuery query2 = createQuery();
        query2.requireDevice();
        checkQuery(query2, order1, order2, order3);
    }

    /**
     * Tests querying by status.
     */
    @Test
    public void testQueryByStatus() {
        Order order1 = createOrder(Order.Status.PENDING);
        Order order2 = createOrder(Order.Status.CONFIRM);
        Order order3 = createOrder(Order.Status.QUEUED);
        Order order4 = createOrder(Order.Status.SUBMITTING);
        Order order5 = createOrder(Order.Status.SUBMITTED);
        Order order6 = createOrder(Order.Status.COMPLETED);
        Order order7 = createOrder(Order.Status.CANCELLED);

        OrderQuery query = createQuery();
        checkStatusQuery(query, Order.Status.PENDING, order1);
        checkStatusQuery(query, Order.Status.CONFIRM, order2);
        checkStatusQuery(query, Order.Status.QUEUED, order3);
        checkStatusQuery(query, Order.Status.SUBMITTING, order4);
        checkStatusQuery(query, Order.Status.SUBMITTED, order5);
        checkStatusQuery(query, Order.Status.COMPLETED, order6);
        checkStatusQuery(query, Order.Status.COMPLETED, order6);
        checkStatusQuery(query, Order.Status.CANCELLED, order7);
    }

    /**
     * Tests querying orders by created date range.
     */
    @Test
    public void testQueryByCreated() {
        OffsetDateTime before = OffsetDateTime.now().minusMinutes(1);
        Order order1 = createOrder(Order.Status.PENDING);
        Order order2 = createOrder(Order.Status.CONFIRM);
        OffsetDateTime after = OffsetDateTime.now().plusMinutes(1);

        OrderQuery query = createQuery();
        query.created(null, before);
        checkQuery(query);

        query.created(before, null);
        checkQuery(query, order1, order2);

        query.created(before, after);
        checkQuery(query, order1, order2);

        query.created(after, null);
        checkQuery(query);
    }

    /**
     * Tests querying by patient.
     */
    @Test
    public void testQueryByPatient() {
        Patient patientA = domainService.create(patientFactory.createPatient(), Patient.class);
        Patient patientB = domainService.create(patientFactory.createPatient(), Patient.class);

        Order order1 = createOrder(laboratory, location, patientA);
        Order order2 = createOrder(laboratory, location, patientA);
        Order order3 = createOrder(laboratory, location, patientB);

        // check query by patient
        OrderQuery query1 = createQuery();
        query1.patient(patientA);
        checkQuery(query1, order1, order2);

        query1.patient(patientB);
        checkQuery(query1, order3);

        query1.patient(null);
        checkQuery(query1, order1, order2, order3);

        // check query by patient id
        OrderQuery query2 = createQuery();
        query2.patient(patientA.getId());
        checkQuery(query2, order1, order2);

        query2.patient(patientB.getId());
        checkQuery(query2, order3);
    }

    /**
     * Tests querying by patient name.
     */
    @Test
    public void testQueryByPatientName() {
        Order order1 = createOrder("Spot");
        Order order2 = createOrder("Fido1");
        Order order3 = createOrder("Fido2");

        OrderQuery query = createQuery();

        checkPatientNameQuery(query, "Spot", false, order1);
        checkPatientNameQuery(query, "spot", false, order1);
        checkPatientNameQuery(query, "Fido", false);
        checkPatientNameQuery(query, "Fido", true);
        checkPatientNameQuery(query, "Fido%", true, order2, order3);
    }

    /**
     * Tests  querying by laboratory.
     */
    @Test
    public void testQueryByLaboratory() {
        Entity laboratory2 = laboratoryFactory.createLaboratory();
        Order order1 = createOrder(laboratory, location, patientFactory.createPatient());
        Order order2 = createOrder(laboratory2, location, patientFactory.createPatient());

        OrderQuery query1 = createQuery().laboratory(laboratory.getObjectReference());
        OrderQuery query2 = createQuery().laboratory(domainService.create(laboratory2, Laboratory.class));

        checkQuery(query1, order1);
        checkQuery(query2, order2);
    }

    /**
     * Creates a query.
     * <p/>
     * The query will always be filtered by laboratory, to limit results
     *
     * @return a new query
     */
    protected OrderQuery createQuery() {
        OrderQuery query = new OrderQueryImpl(getArchetypeService(), orderFactory);
        return query.laboratory(laboratory.getObjectReference());
    }

    /**
     * Creates an order.
     *
     * @param status the order status
     * @return the order
     */
    private Order createOrder(Order.Status status) {
        Act investigation = patientFactory.newInvestigation()
                .patient(patientFactory.createPatient())
                .laboratory(laboratory)
                .location(location)
                .investigationType(laboratoryFactory.createInvestigationType())
                .order(status.toString()).build();
        return getOrder(investigation);
    }

    /**
     * Creates an order.
     *
     * @param laboratory the laboratory
     * @param location   the practice location
     * @param patient    the patient
     * @return the order
     */
    private Order createOrder(Entity laboratory, Party location, Party patient) {
        Act investigation = patientFactory.newInvestigation()
                .patient(patient)
                .laboratory(laboratory)
                .location(location)
                .investigationType(laboratoryFactory.createInvestigationType())
                .order().build();
        return getOrder(investigation);
    }

    /**
     * Creates an order.
     *
     * @param patientName the patient name
     * @return the order
     */
    private Order createOrder(String patientName) {
        Act investigation = patientFactory.newInvestigation()
                .patient(patientFactory.newPatient().name(patientName).build())
                .laboratory(laboratory)
                .location(location)
                .investigationType(laboratoryFactory.createInvestigationType())
                .order().build();
        return getOrder(investigation);
    }

    /**
     * Creates an order.
     *
     * @param device the device. May be {@code null}
     * @return the order
     */
    private Order createOrder(Entity device) {
        Act investigation = patientFactory.newInvestigation()
                .patient(patientFactory.createPatient())
                .laboratory(laboratory)
                .location(location)
                .investigationType(laboratoryFactory.createInvestigationType())
                .device(device).order().build();
        return getOrder(investigation);
    }

    /**
     * Verifies that a query by device returns the expected orders.
     *
     * @param query  the query
     * @param device the device to filter on. May be {@code null}
     * @param orders the expected orders
     */
    private void checkDeviceQuery(OrderQuery query, Entity device, Order... orders) {
        if (device != null) {
            query.device(device.getObjectReference());
        } else {
            query.device((Device) null);
        }
        checkQuery(query, orders);
    }

    /**
     * Verifies that a query by status returns the expected orders.
     *
     * @param query  the query
     * @param status the status to filter on
     * @param orders the expected orders
     */
    private void checkStatusQuery(OrderQuery query, Order.Status status, Order... orders) {
        query.status(status);
        checkQuery(query, orders);
    }

    /**
     * Verifies that a query by patient name returns the expected orders.
     *
     * @param query  the query
     * @param name   the name to filter on
     * @param like   if {@code true} perform a 'like' match
     * @param orders the expected orders
     */
    private void checkPatientNameQuery(OrderQuery query, String name, boolean like, Order... orders) {
        query.patientName(name, like);
        checkQuery(query, orders);
    }

    /**
     * Verifies that a query returns the expected orders.
     *
     * @param query  the query
     * @param orders the expected orders, in the order they will be returned
     */
    private void checkQuery(OrderQuery query, Order... orders) {
        Iterable<Order> results = query.getResults();
        List<Order> matches = new ArrayList<>();
        CollectionUtils.addAll(matches, results);
        assertEquals(orders.length, matches.size());
        for (int i = 0; i < orders.length; ++i) {
            assertEquals(orders[i], matches.get(i));
        }
    }

    /**
     * Returns an order for an investigation.
     *
     * @param investigation the investigatoin
     * @return the order
     */
    private Order getOrder(Act investigation) {
        Act order = getBean(investigation).getTarget("order", Act.class);
        assertNotNull(order);
        return orderFactory.create(order);
    }
}
