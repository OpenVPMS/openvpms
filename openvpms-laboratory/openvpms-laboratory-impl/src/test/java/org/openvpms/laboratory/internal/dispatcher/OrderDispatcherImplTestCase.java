/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.component.dispatcher.Queues;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.internal.TestLaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link OrderDispatcherImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrderDispatcherImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules laboratoryRules;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Returns the practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The test user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The test dispatcher.
     */
    private TestOrderDispatcher dispatcher;

    /**
     * The test laboratory service.
     */
    private TestLaboratoryService laboratoryService;

    /**
     * The test location.
     */
    private Party location;

    /**
     * The test laboratory.
     */
    private Entity laboratory;

    /**
     * The test investigation type.
     */
    private Entity investigationType;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party practice = practiceService.getPractice();
        if (practiceService.getServiceUser() == null) {
            IMObjectBean bean = getBean(practice);
            bean.setTarget("serviceUser", userFactory.createUser());
        }
        LaboratoryServices services = Mockito.mock(LaboratoryServices.class);
        laboratoryService = new TestLaboratoryService();
        laboratory = laboratoryFactory.createLaboratory();
        when(services.getService(laboratory)).thenReturn(laboratoryService);

        OrderService orderService = new OrderService(getArchetypeService(), laboratoryRules);
        OrderQueue queue = new OrderQueue(laboratory, orderService) {
            @Override
            public int getRetryInterval() {
                return 1;
            }
        };
        TestQueues queues = new TestQueues(queue);
        dispatcher = new TestOrderDispatcher(
                services, getArchetypeService(), domainService, practiceService, orderService, queues);

        patient = patientFactory.createPatient();
        location = practiceFactory.createLocation();
        investigationType = laboratoryFactory.createInvestigationType();
    }

    /**
     * Tests dispatching of orders.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDispatch() throws Exception {
        for (int i = 0; i < 10; ++i) {
            DocumentAct investigation = createInvestigation();
            dispatcher.create(investigation);
        }
        assertTrue(dispatcher.waitForProcess(10));
        dispatcher.destroy();

        assertEquals(10, laboratoryService.getOrdered());
    }

    /**
     * Verifies that if investigations are queued in between {@code OrderDispatcherImpl.dispatch()}
     * and {@code OrderDispatcherImpl.postDispatch()}, they are still delivered.
     *
     * @throws Exception for any error
     */
    @Test
    public void testReschedule() throws Exception {
        int count = 10;
        for (int i = 0; i < count; ++i) {
            dispatcher.blockPostDispatch();               // suspend dispatching when it reaches postDispatch()
            dispatcher.create(createInvestigation());     // queue an investigation

            assertTrue(dispatcher.waitForPostDispatch()); // when dispatching stops at postDispatch()
            dispatcher.create(createInvestigation());     // ... queue another investigation
            dispatcher.allowPostDispatch();               // ... and allow it to proceed

            assertTrue(dispatcher.waitForProcess(2));     // wait for both investigations to be processed
        }

        dispatcher.destroy();

        // verify all investigations were ordered
        assertEquals(2 * count, laboratoryService.getOrdered());
    }

    /**
     * Creates an investigation.
     *
     * @return the investigation
     */
    private DocumentAct createInvestigation() {
        return patientFactory.newInvestigation()
                .patient(patient)
                .laboratory(laboratory)
                .location(location)
                .investigationType(investigationType)
                .build();
    }

    private static class TestOrderDispatcher extends OrderDispatcherImpl {

        private final Semaphore processed = new Semaphore(0);

        private final Semaphore inProcess = new Semaphore(1);

        private final Semaphore postDispatch = new Semaphore(0);

        private final Semaphore postDispatchBlocker = new Semaphore(1);

        /**
         * Constructs a {@link OrderDispatcherImpl}.
         *
         * @param laboratoryServices the laboratory services
         * @param service            the archetype service
         * @param domainService      the domain service
         * @param practiceService    the practice service
         * @param orderService       the order service
         * @param orderQueues        the order queues
         */
        protected TestOrderDispatcher(LaboratoryServices laboratoryServices, IArchetypeService service,
                                      DomainService domainService, PracticeService practiceService,
                                      OrderService orderService, Queues<Entity, OrderQueue> orderQueues) {
            super(laboratoryServices, service, domainService, practiceService, orderService, orderQueues);
        }

        public void blockPostDispatch() {
            postDispatchBlocker.drainPermits();
        }

        public void allowPostDispatch() {
            postDispatchBlocker.release();
        }

        /**
         * Waits for {@code OrderDispatcher#process()} to complete for the specified number of invocations.
         *
         * @param count the number of invocations to wait for
         * @return {@code true}  if the wait completed successfully, {@code false} if it was interrupted
         */
        public boolean waitForProcess(int count) {
            boolean result = false;
            try {
                processed.tryAcquire(count, 30, TimeUnit.SECONDS);
                result = true;
            } catch (InterruptedException exception) {
                // no-op
            }
            return result;
        }

        public boolean waitForPostDispatch() {
            boolean result = false;
            try {
                postDispatch.tryAcquire(1, 30, TimeUnit.SECONDS);
                result = true;
            } catch (InterruptedException exception) {
                // no-op
            }
            return result;
        }

        /**
         * Schedules {@code dispatch()} to be run, unless it is already running.
         */
        @Override
        public void schedule() {
            super.schedule();
        }

        @Override
        protected void postDispatch(int waiting, long waitUntil) {
            postDispatch.release();
            try {
                postDispatchBlocker.acquire();
            } catch (InterruptedException exception) {
                throw new IllegalStateException("Interrupted waiting for processDispatch to be released");
            }
            try {
                super.postDispatch(waiting, waitUntil);
            } finally {
                postDispatchBlocker.release();
            }
        }

        /**
         * Processes an object.
         *
         * @param object the object to process
         * @param queue  the queue
         */
        @Override
        protected void process(Act object, OrderQueue queue) {
            try {
                inProcess.acquire();
            } catch (InterruptedException exception) {
                throw new IllegalStateException("Interrupted waiting for inProcess to be released");
            }
            try {
                super.process(object, queue);
            } finally {
                inProcess.release();
                processed.release();
            }
        }
    }

    private static class TestQueues implements Queues<Entity, OrderQueue> {

        private List<OrderQueue> queues = new ArrayList<>();

        public TestQueues(OrderQueue queue) {
            queues.add(queue);
        }

        /**
         * Returns the queues.
         *
         * @return the queues
         */
        @Override
        public List<OrderQueue> getQueues() {
            return queues;
        }

        /**
         * Returns a queue given its owner.
         *
         * @param owner the queue owner
         * @return the queue
         */
        @Override
        public OrderQueue getQueue(Entity owner) {
            for (OrderQueue queue : queues) {
                if (queue.getOwner().equals(owner)) {
                    return queue;
                }
            }
            return null;
        }

        /**
         * Destroys the queues.
         */
        @Override
        public void destroy() {
            queues.clear();
        }
    }

}
