/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.i18n;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.laboratory.order.Order;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link LaboratoryMessages}.
 *
 * @author Tim Anderson
 */
public class LaboratoryMessagesTestCase {

    /**
     * Verify there are tests for each message.
     */
    @Test
    public void testCoverage() {
        for (Method method : LaboratoryMessages.class.getDeclaredMethods()) {
            if (Modifier.isPublic(method.getModifiers()) && Modifier.isStatic(method.getModifiers())) {
                String test = "test" + StringUtils.capitalize(method.getName());
                try {
                    LaboratoryMessagesTestCase.class.getDeclaredMethod(test);
                } catch (NoSuchMethodException exception) {
                    fail("Test needs to be expanded to include " + method.getName());
                }
            }
        }
    }

    /**
     * Tests the {@link LaboratoryMessages#serviceUnavailable(String)} method.
     */
    @Test
    public void testServiceUnavailable() {
        assertEquals("foo is currently not available", LaboratoryMessages.serviceUnavailable("foo").getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#differentOrderIdentifierArchetype(String, String)} method.
     */
    @Test
    public void testDifferentOrderIdentifierArchetype() {
        assertEquals("Cannot change order identifier archetype from foo to bar",
                     LaboratoryMessages.differentOrderIdentifierArchetype("foo", "bar").getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#investigationNotFound(long)} method.
     */
    @Test
    public void testInvestigationNotFound() {
        assertEquals("Investigation not found: 1", LaboratoryMessages.investigationNotFound(1).getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#invalidCancelStatus(Order.Status)} method.
     */
    @Test
    public void testInvalidCancelStatus() {
        assertEquals("Cannot assign status COMPLETED to order cancellation",
                     LaboratoryMessages.invalidCancelStatus(Order.Status.COMPLETED).getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#orderWithErrorStatusMustBeCancelled()} method.
     */
    @Test
    public void testOrderWithErrorStatusMustBeCancelled() {
        assertEquals("Orders with ERROR status must be cancelled",
                     LaboratoryMessages.orderWithErrorStatusMustBeCancelled().getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#cannotModifyOrder()} method.
     */
    @Test
    public void testCannotModifyOrder() {
        assertEquals("This order cannot be modified", LaboratoryMessages.cannotModifyOrder().getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#failedToCreateReport(String)} method.
     */
    @Test
    public void testFailedToCreateReport() {
        assertEquals("Failed to create report: foo", LaboratoryMessages.failedToCreateReport("foo").getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#noResultsId()} method.
     */
    @Test
    public void testNoResultsId() {
        assertEquals("Cannot build Results: no results identifier supplied",
                     LaboratoryMessages.noResultsId().getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#noResultId()} method.
     */
    @Test
    public void testNoResultId() {
        assertEquals("Cannot build Result: no result identifier supplied",
                     LaboratoryMessages.noResultId().getMessage());
    }

    /**
     * Tests the {@link LaboratoryMessages#unsupportedImage(String, String)} method.
     */
    @Test
    public void testUnsupportedImage() {
        assertEquals("Image foo.tiff with mime type 'image/tiff' is not a supported image type",
                     LaboratoryMessages.unsupportedImage("foo.tiff", "image/tiff").getMessage());
    }
}
