/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.Orders;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link OrdersImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrdersImplTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The plugin archetype service.
     */
    private PluginArchetypeService service;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The orders.
     */
    private Orders orders;

    /**
     * Test laboratory.
     */
    private Entity laboratory;

    /**
     * Test location.
     */
    private Party location;

    /**
     * Laboratory order identifier archetype.
     */
    private static final String ORDER_ID = "actIdentity.laboratoryOrderTest";

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getServiceUser()).thenReturn(userFactory.newUser().build(false));
        service = new PluginArchetypeService((IArchetypeRuleService) getArchetypeService(),
                                             getLookupService(), practiceService);

        orders = new OrdersImpl(service, patientRules, domainService, transactionManager);

        location = practiceFactory.createLocation();
        laboratory = laboratoryFactory.createLaboratory(location);
    }

    /**
     * Tests the {@link OrdersImpl#getOrder(long)} method.
     */
    @Test
    public void testGetOrder() {
        Act investigation = createInvestigation();
        Order order1 = orders.getOrder(investigation.getId());
        assertNotNull(order1);
        assertEquals(investigation.getId(), order1.getInvestigationId());

        // verify the order can still be queried after the investigation has been deleted
        service.remove(investigation);
        assertNull(get(investigation));

        Order order2 = orders.getOrder(investigation.getId());
        assertEquals(investigation.getId(), order2.getInvestigationId());
    }

    /**
     * Tests the {@link OrdersImpl#getOrder(String, String)} method.
     */
    @Test
    public void testGetOrderByOrderId() {
        Act investigation = createInvestigation();
        Order order1 = orders.getOrder(investigation.getId());
        String id = UUID.randomUUID().toString();
        order1.setOrderId(ORDER_ID, id);

        Order order2 = orders.getOrder(ORDER_ID, id);
        assertNotNull(order2);
        assertEquals(investigation.getId(), order2.getInvestigationId());

        // verify the order can still be queried after the investigation has been deleted
        service.remove(investigation);
        assertNull(get(investigation));

        Order order3 = orders.getOrder(ORDER_ID, id);
        assertEquals(investigation.getId(), order3.getInvestigationId());
    }

    /**
     * Tests the {@link OrdersImpl#getQuery()} method.
     */
    @Test
    public void testGetQuery() {
        Entity device = laboratoryFactory.newDevice().laboratory(laboratory).build();
        Act investigation1 = createInvestigation(device);
        Act investigation2 = createInvestigation(device);
        Act investigation3 = createInvestigation();  // no device

        // query all orders by device
        Iterable<Order> iterable = orders.getQuery().device(device.getObjectReference()).getResults();
        List<Order> matches = new ArrayList<>();
        CollectionUtils.addAll(matches, iterable);
        assertEquals(2, matches.size());

        Order order1 = orders.getOrder(investigation1.getId());
        Order order2 = orders.getOrder(investigation2.getId());
        Order order3 = orders.getOrder(investigation3.getId());
        assertNotNull(order1);
        assertNotNull(order2);
        assertNotNull(order3);

        assertTrue(matches.contains(order1));
        assertTrue(matches.contains(order2));
        assertFalse(matches.contains(order3));
    }

    /**
     * Creates a new investigation.
     *
     * @return the investigation
     */
    private Act createInvestigation() {
        return createInvestigation(null);
    }

    /**
     * Creates a new investigation.
     *
     * @param device the device. May be {@code null}
     * @return the investigation
     */
    private Act createInvestigation(Entity device) {
        Party patient = patientFactory.createPatient();
        User clinician = userFactory.createClinician();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        return patientFactory.newInvestigation().patient(patient).clinician(clinician).location(location)
                .investigationType(investigationType).laboratory(laboratory).device(device).order().build();
    }
}
