/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.entity.Entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Base class for laboratory I/O tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractLaboratoryIOTestCase extends ArchetypeServiceTest {

    /**
     * Verifies data matches that expected.
     *
     * @param data        the data to check
     * @param laboratory  the expected laboratory
     * @param code        the expected code
     * @param name        the expected name
     * @param description the expected description
     * @param specimen    the expected specimen
     * @param turnaround  the expected turnaround
     * @param oldPrice    the expected old price
     * @param newPrice    the expected new price
     * @param line        the expected line
     */
    protected void checkData(LaboratoryTestData data, String laboratory, String code, String name, String description,
                             String specimen, String turnaround, Integer oldPrice, Integer newPrice, int line) {
        checkData(data, laboratory, code, name, description, specimen, turnaround, oldPrice, newPrice, null, null, line,
                  null);
    }

    /**
     * Verifies data matches that expected.
     *
     * @param data              the data to check
     * @param laboratory        the expected laboratory
     * @param code              the expected code
     * @param name              the expected name
     * @param description       the expected description
     * @param specimen          the expected specimen
     * @param turnaround        the expected turnaround
     * @param oldPrice          the expected old price
     * @param newPrice          the expected new price
     * @param test              the expected test
     * @param investigationType the expected investigation type
     * @param line              the expected line
     * @param error             the expected error
     */
    protected void checkData(LaboratoryTestData data, String laboratory, String code, String name, String description,
                             String specimen, String turnaround, Integer oldPrice, Integer newPrice, Entity test,
                             Entity investigationType, int line, String error) {
        assertEquals(laboratory, data.getLaboratory());
        assertEquals(code, data.getCode());
        assertEquals(name, data.getName());
        assertEquals(description, data.getDescription());
        assertEquals(specimen, data.getSpecimen());
        assertEquals(turnaround, data.getTurnaround());
        if (oldPrice == null) {
            assertNull(data.getOldPrice());
        } else {
            checkEquals(oldPrice, data.getOldPrice());
        }
        if (newPrice == null) {
            assertNull(data.getNewPrice());
        } else {
            checkEquals(newPrice, data.getNewPrice());
        }
        assertEquals(test, data.getTest());
        assertEquals(investigationType, data.getInvestigationType());
        assertEquals(line, data.getLine());
        assertEquals(error, data.getError());
    }
}