/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryOrderStatus;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.laboratory.order.Order;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link OrderService}.
 *
 * @author Tim Anderson
 */
public class OrderServiceTestCase extends ArchetypeServiceTest {

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The laboratory rules.
     */
    @Autowired
    private LaboratoryRules rules;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link OrderService#cancel(Act)} method.
     */
    @Test
    public void testCancel() {
        OrderService service = new OrderService(getArchetypeService(), rules);
        Party patient = patientFactory.createPatient();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct act = patientFactory.newInvestigation().patient(patient).investigationType(investigationType)
                .laboratory(laboratoryFactory.createLaboratory())
                .location(practiceFactory.createLocation())
                .build();
        Act order = service.createOrder(act, false);
        assertTrue(service.cancel(order));

        order = get(order);
        assertEquals(LaboratoryOrderStatus.QUEUED, order.getStatus());
        IMObjectBean bean = getBean(order);
        assertEquals(Order.Type.CANCEL.toString(), bean.getString("type"));

        assertFalse(service.cancel(order));
    }
}
