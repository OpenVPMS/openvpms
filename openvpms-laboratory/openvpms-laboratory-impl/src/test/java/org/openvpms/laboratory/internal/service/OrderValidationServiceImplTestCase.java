/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.TestLaboratoryService;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.laboratory.service.OrderValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link OrderValidationServiceImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrderValidationServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The order validation service.
     */
    private OrderValidationService service;

    /**
     * The laboratory services.
     */
    private LaboratoryServices laboratoryServices;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        laboratoryServices = Mockito.mock(LaboratoryServices.class);
        service = new OrderValidationServiceImpl(laboratoryServices, domainService);
    }

    /**
     * Tests the {@link OrderValidationServiceImpl#validate(DocumentAct)} method for a valid investigation.
     */
    @Test
    public void testValidate() {
        Entity laboratory = laboratoryFactory.createLaboratory();
        DocumentAct investigation = patientFactory.newInvestigation()
                .laboratory(laboratory)
                .build(false);
        addService(laboratory, new TestLaboratoryService());
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.VALID, status.getStatus());
        assertEquals("Test Laboratory Service", status.getName());
        assertNull(status.getMessage());
    }

    /**
     * Tests the {@link OrderValidationServiceImpl#validate(DocumentAct)} method for an invalid investigation.
     */
    @Test
    public void testValidationError() {
        Entity laboratory = laboratoryFactory.createLaboratory();
        DocumentAct investigation = patientFactory.newInvestigation()
                .laboratory(laboratory)
                .build(false);
        addService(laboratory, new TestLaboratoryService() {
            @Override
            public OrderValidationStatus validate(Order order) {
                return OrderValidationStatus.error("Invalid order");
            }
        });
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.ERROR, status.getStatus());
        assertEquals("Test Laboratory Service", status.getName());
        assertEquals("Invalid order", status.getMessage());
    }

    /**
     * Verifies that if an investigation is not PENDING, it will be considered invalid.
     */
    @Test
    public void testNonPendingInvestigation() {
        DocumentAct investigation = patientFactory.newInvestigation()
                .status2(InvestigationActStatus.SENT)
                .investigationType(laboratoryFactory.createInvestigationType())
                .build(false);
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.ERROR, status.getStatus());
        assertEquals("Cannot submit orders for investigations with SENT status", status.getMessage());
    }

    /**
     * Verifies that investigations without laboratories pass validation.
     */
    @Test
    public void testInvestigationWithoutLaboratory() {
        DocumentAct investigation = patientFactory.newInvestigation()
                .build(false);
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.VALID, status.getStatus());
        assertNull(status.getName());
        assertNull(status.getMessage());
    }

    /**
     * Verifies that HL7 investigations pass validation.
     */
    @Test
    public void testHL7Investigation() {
        Party location = practiceFactory.createLocation();
        User user = userFactory.createUser();
        Entity laboratory = laboratoryFactory.createHL7Laboratory(location, user);
        Entity investigationType = laboratoryFactory.createInvestigationType(laboratory);
        DocumentAct investigation = patientFactory.newInvestigation()
                .investigationType(investigationType)
                .laboratory(laboratory)
                .build(false);
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.VALID, status.getStatus());
        assertEquals(laboratory.getName(), status.getName());
        assertNull(status.getMessage());
    }

    /**
     * Verifies that the investigation is treated as invalid if the corresponding {@link LaboratoryService} is
     * unavailable.
     */
    @Test
    public void testLaboratoryUnavailable() {
        Entity laboratory = laboratoryFactory.createLaboratory();
        when(laboratoryServices.getService(laboratory))
                .thenThrow(new LaboratoryException(LaboratoryMessages.serviceUnavailable(laboratory.getName())));

        DocumentAct investigation = patientFactory.newInvestigation()
                .laboratory(laboratory)
                .build(false);
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.ERROR, status.getStatus());
        assertEquals("LAB-0001: Test Laboratory is currently not available", status.getMessage());
    }

    /**
     * Verifies that if the {@link LaboratoryService#validate(Order)} method throws an exception, the
     * returned status is ERROR.
     */
    @Test
    public void testExceptionOnValidation() {
        Entity laboratory = laboratoryFactory.createLaboratory();

        addService(laboratory, new TestLaboratoryService() {
            @Override
            public OrderValidationStatus validate(Order order) {
                throw new RuntimeException("Oops");
            }
        });

        DocumentAct investigation = patientFactory.newInvestigation()
                .laboratory(laboratory)
                .build(false);
        OrderValidationService.ValidationStatus status = service.validate(investigation);
        assertEquals(OrderValidationStatus.Status.ERROR, status.getStatus());
        assertEquals("Test Laboratory Service", status.getName());
        assertEquals("Oops", status.getMessage());
    }

    /**
     * Registers a {@link LaboratoryService} for a laboratory.
     *
     * @param laboratory the laboratory
     * @param service    the corresponding service
     */
    private void addService(Entity laboratory, LaboratoryService service) {
        when(laboratoryServices.getService(laboratory)).thenReturn(service);
    }
}