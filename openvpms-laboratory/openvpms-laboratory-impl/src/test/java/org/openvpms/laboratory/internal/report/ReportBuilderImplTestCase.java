/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.openvpms.laboratory.report.Result;
import org.openvpms.laboratory.report.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.laboratory.report.Report.Status.COMPLETED;
import static org.openvpms.laboratory.report.Report.Status.IN_PROGRESS;
import static org.openvpms.laboratory.report.Report.Status.PARTIAL_RESULTS;
import static org.openvpms.laboratory.report.Report.Status.PENDING;
import static org.openvpms.laboratory.report.Report.Status.WAITING_FOR_SAMPLE;

/**
 * Tests the {@link ReportBuilderImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ReportBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;


    /**
     * Tests building a report.
     */
    @Test
    public void testCreateReport() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        User clinician = userFactory.createClinician();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity testEntity1 = laboratoryFactory.createTest(investigationType);
        Entity testEntity2 = laboratoryFactory.createTest(investigationType);
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .clinician(clinician)
                .location(location)
                .investigationType(investigationType)
                .products(productFactory.createService())
                .addTests(testEntity1, testEntity2)
                .order()
                .laboratory(laboratory).build();

        ReportBuilder builder = createBuilder(investigation);

        builder.document("results.txt", "text/plain", IOUtils.toInputStream("some text", StandardCharsets.UTF_8))
                .reportId("actIdentity.laboratoryReportTest", "X12345678")
                .synchronisationId("someid")
                .summary("summary of the results");

        OffsetDateTime date1 = DateRules.toOffsetDateTime(DateRules.getYesterday());
        OffsetDateTime date2 = DateRules.toOffsetDateTime(DateRules.getToday());
        org.openvpms.domain.laboratory.Test test1 = getTest(testEntity1);
        org.openvpms.domain.laboratory.Test test2 = getTest(testEntity2);

        // this is somewhat long, and wouldn't (shouldn't) be used in this way, but it does test the behaviour
        // of re-using builders
        Report report1 = builder.results("1")
                .status(Result.Status.COMPLETED)
                .date(date1)
                .test(test1)
                .categoryCode("categorycode1")
                .categoryName("categoryname1")
                .notes("notes1")
                .result("8072")
                .analyteCode("CREA")
                .analyteName("analyteName a")
                .result("1.8")
                .qualifier("-")
                .outOfRange(false)
                .referenceRange("0.8 - 2.4 mg/dL")
                .notes("notes a")
                .add()
                .result("8311")
                .status(Result.Status.COMPLETED)
                .analyteName("T4 PLUS")
                .result("0.7")
                .status(Result.Status.COMPLETED)
                .outOfRange(true)
                .referenceRange("0.8 - 4.7 μg/dL")
                .add()
                .add()
                .results("2")
                .date(date2)
                .test(test2)
                .categoryCode("categorycode2")
                .categoryName("categoryname2")
                .result("8000")
                .status(Result.Status.IN_PROGRESS)
                .analyteCode("DHA")
                .analyteName("analyteName c")
                .result("-")
                .outOfRange(false)
                .referenceRange("0.9 - 3.0 mg/dL")
                .add()
                .add()
                .build();
        assertEquals("X12345678", report1.getReportId());
        assertEquals("someid", report1.getSynchronisationId());
        assertEquals("summary of the results", report1.getSummary());
        List<Results> resultsList = report1.getResults();
        assertEquals(2, resultsList.size());

        Results results1 = resultsList.get(0);
        checkResults(results1, "1", Result.Status.COMPLETED, date1, test1, "categorycode1", "categoryname1", "notes1");
        List<Result> items1 = results1.getResults();
        assertEquals(2, items1.size());
        checkRange(items1.get(0), "8072", "CREA", "analyteName a", "1.8", "-", false, "0.8 - 2.4 mg/dL", "notes a");

        String expected = isMySQL55() ? "0.8 - 4.7 ?g/dL" : "0.8 - 4.7 μg/dL";
        checkRange(items1.get(1), "8311", null, "T4 PLUS", "0.7", null, true, expected, null);

        Results results2 = resultsList.get(1);
        checkResults(results2, "2", Result.Status.IN_PROGRESS, date2, test2, "categorycode2", "categoryname2", null);
        List<Result> items2 = results2.getResults();
        assertEquals(1, items2.size());
        checkRange(items2.get(0), "8000", "DHA", "analyteName c", "-", null, false, "0.9 - 3.0 mg/dL", null);

        checkDocument(report1, "results.txt", "some text");
    }

    /**
     * Verifies that results can be updated.
     */
    @Test
    public void testUpdateResults() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .order(Order.Status.SUBMITTED.toString())
                .build();
        assertEquals(InvestigationActStatus.PENDING, investigation.getStatus2());
        org.openvpms.domain.laboratory.Test test1 = getTest(laboratoryFactory.createTest(investigationType));
        org.openvpms.domain.laboratory.Test test2 = getTest(laboratoryFactory.createTest(investigationType));

        OffsetDateTime date1 = DateRules.toOffsetDateTime(DateRules.getYesterday());
        OffsetDateTime date2 = DateRules.toOffsetDateTime(DateRules.getToday());

        Report report1 = createBuilder(investigation).results("1")
                .status(Result.Status.IN_PROGRESS)
                .date(date1)
                .test(test1)
                .categoryCode("categorycode1")
                .categoryName("categoryname1")
                .notes("original notes")
                .result("8072")
                .analyteCode("CREA")
                .analyteName("analyteName a")
                .add()
                .add()
                .build();
        List<Results> results1 = report1.getResults();
        assertEquals(1, results1.size());
        checkResults(results1.get(0), "1", Result.Status.IN_PROGRESS, date1, test1, "categorycode1", "categoryname1",
                     "original notes");
        List<Result> items1 = results1.get(0).getResults();
        assertEquals(1, items1.size());
        checkRange(items1.get(0), "8072", "CREA", "analyteName a", null, null, false, null, null);

        // now update the results
        Report report2 = createBuilder(investigation).results("1")
                .status(Result.Status.COMPLETED)
                .date(date2)
                .test(test1)
                .categoryCode("categorycode1")
                .categoryName("categoryname1")
                .notes("updated notes")
                .result("8072")
                .analyteCode("CREA")
                .analyteName("analyteName a")
                .result("1.8")
                .qualifier("-")
                .outOfRange(false)
                .referenceRange("0.8 - 2.4 mg/dL")
                .notes("notes a")
                .add()
                .add()
                .results("2")  // add a second set of results
                .date(date2)
                .test(test2)
                .categoryCode("categorycode2")
                .categoryName("categoryname2")
                .result("8000")
                .status(Result.Status.COMPLETED)
                .analyteCode("DHA")
                .analyteName("analyteName c")
                .result("-")
                .outOfRange(false)
                .referenceRange("0.9 - 3.0 mg/dL")
                .add()
                .add()
                .build();

        List<Results> results2 = report2.getResults();
        assertEquals(2, results2.size());
        checkResults(results2.get(0), "1", Result.Status.COMPLETED, date2, test1, "categorycode1", "categoryname1",
                     "updated notes");
        List<Result> items2a = results2.get(0).getResults();
        assertEquals(1, items2a.size());
        checkRange(items2a.get(0), "8072", "CREA", "analyteName a", "1.8", "-", false, "0.8 - 2.4 mg/dL", "notes a");

        checkResults(results2.get(1), "2", Result.Status.COMPLETED, date2, test2, "categorycode2", "categoryname2",
                     null);
        List<Result> items2b = results2.get(1).getResults();
        assertEquals(1, items2b.size());
        checkRange(items2b.get(0), "8000", "DHA", "analyteName c", "-", null, false, "0.9 - 3.0 mg/dL", null);
    }

    /**
     * Verifies that result status updates are propagated to the investigation.
     */
    @Test
    public void testStatusUpdates() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .order()
                .build();

        assertEquals(InvestigationActStatus.PENDING, investigation.getStatus2());
        investigation.setStatus2(InvestigationActStatus.SENT);
        save(investigation);

        createBuilder(investigation).status(PENDING).build();
        assertEquals(InvestigationActStatus.SENT, investigation.getStatus2());

        createBuilder(investigation).status(WAITING_FOR_SAMPLE).build();
        assertEquals(InvestigationActStatus.WAITING_FOR_SAMPLE, investigation.getStatus2());

        // TODO - there is no intermediate status between WAITING_FOR_SAMPLE and PARTIAL_RESULTS or COMPLETED
        // This may mean that for long running tests, they are left in the WAITING_FOR_SAMPLE state, even though
        // the tests are in progress.
        createBuilder(investigation).status(IN_PROGRESS).build();
        assertEquals(InvestigationActStatus.WAITING_FOR_SAMPLE, investigation.getStatus2());

        createBuilder(investigation).status(PARTIAL_RESULTS).build();
        assertEquals(InvestigationActStatus.PARTIAL_RESULTS, investigation.getStatus2());

        createBuilder(investigation).status(COMPLETED).build();
        assertEquals(InvestigationActStatus.RECEIVED, investigation.getStatus2());
    }

    /**
     * Verifies that documents are not versioned by default.
     */
    @Test
    public void testDocumentsNotVersionedByDefault() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .products(productFactory.createService())
                .order()
                .build();

        ReportBuilder builder = createBuilder(investigation);

        // only a single document may be attached per call to build(). Prior documents are discarded.
        Report report1 = builder.document("results1.txt", "text/plain",
                                          IOUtils.toInputStream("text1", StandardCharsets.UTF_8))
                .build();
        Document document1 = checkDocument(report1, "results1.txt", "text1");

        Report report2 = builder.document("results2.txt", "text/plain",
                                          IOUtils.toInputStream("text2", StandardCharsets.UTF_8))
                .build();
        Document document2 = checkDocument(report2, "results2.txt", "text2");

        // verify document1 is not versioned.
        checkNoVersions(investigation);
        assertNull(get(document1));  // should now be deleted

        Report report3 = builder.document("results3.txt", "text/plain",
                                          IOUtils.toInputStream("text3", StandardCharsets.UTF_8))
                .build();

        checkDocument(report3, "results3.txt", "text3");
        assertNull(get(document2));  // should now be deleted

        // verify there are still no versions
        checkNoVersions(investigation);
    }

    /**
     * Verifies that documents are versioned if there is an existing document on an investigation and versioning is
     * enabled.
     * <p/>
     * Individual results are not versioned.
     */
    @Test
    public void testDocumentVersioning() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .products(productFactory.createService())
                .order()
                .build();

        ReportBuilder builder = createBuilder(investigation);

        // only a single document may be attached per call to build(). Prior documents are discarded.
        Report report1 = builder.document("discarded.txt", "text/plain",
                                          IOUtils.toInputStream("discarded", StandardCharsets.UTF_8))
                .document("results1.txt", "text/plain", IOUtils.toInputStream("text1", StandardCharsets.UTF_8))
                .version(true)
                .build();
        Document document1 = checkDocument(report1, "results1.txt", "text1");

        Report report2 = builder.document("results2.txt", "text/plain",
                                          IOUtils.toInputStream("text2", StandardCharsets.UTF_8))
                .version(true)
                .build();
        Document document2 = checkDocument(report2, "results2.txt", "text2");

        // verify document1 is now versioned.
        checkVersion(investigation, document1, "results1.txt", "text1", 1);

        Report report3 = builder.document("results3.txt", "text/plain",
                                          IOUtils.toInputStream("text3", StandardCharsets.UTF_8))
                .version(true)
                .build();

        checkDocument(report3, "results3.txt", "text3");

        // verify document1 and document2 are versioned
        checkVersion(investigation, document1, "results1.txt", "text1", 2);
        checkVersion(investigation, document2, "results2.txt", "text2", 2);

        // verify building without updating any documents doesn't version the current document
        Report report4 = builder.status(Report.Status.COMPLETED).build();
        checkDocument(report4, "results3.txt", "text3");

        // verify document1 and document2 are versioned
        checkVersion(investigation, document1, "results1.txt", "text1", 2);
        checkVersion(investigation, document2, "results2.txt", "text2", 2);
    }

    /**
     * Verifies that documents that are flagged as protected are versioned.
     */
    @Test
    public void testDocumentVersionForProtectedDocument() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .products(productFactory.createService())
                .order()
                .build();

        ReportBuilder builder = createBuilder(investigation);

        Report report1 = builder.document("results1.txt", "text/plain",
                                          IOUtils.toInputStream("text1", StandardCharsets.UTF_8))
                .version(false)
                .build();
        Document document1 = checkDocument(report1, "results1.txt", "text1");
        getBean(investigation).setValue(ReportBuilderImpl.PROTECTED_DOCUMENT, true);
        // make the document protected so it is versioned

        Report report2 = builder.document("results2.txt", "text/plain",
                                          IOUtils.toInputStream("text2", StandardCharsets.UTF_8))
                .version(false)
                .build();
        checkDocument(report2, "results2.txt", "text2");

        // verify document1 is now versioned.
        checkVersion(investigation, document1, "results1.txt", "text1", 1);

        Report report3 = builder.document("results3.txt", "text/plain",
                                          IOUtils.toInputStream("text3", StandardCharsets.UTF_8))
                .version(false)
                .build();
        checkDocument(report3, "results3.txt", "text3");

        // verify document1 is versioned, but document2 isn't
        checkVersion(investigation, document1, "results1.txt", "text1", 1);
    }

    /**
     * Tests the {@link ReportBuilder#checkResults} and {@link ReportBuilder#externalResults} methods.
     */
    @Test
    public void testCheckExternalResults() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .products(productFactory.createService())
                .order()
                .build();

        IMObjectBean bean = getBean(investigation);
        assertFalse(bean.getBoolean(ReportBuilderImpl.CHECK_RESULTS));
        assertFalse(bean.getBoolean(ReportBuilderImpl.EXTERNAL_RESULTS));

        ReportBuilder builder1 = createBuilder(investigation);
        builder1.checkResults()
                .externalResults()
                .build();

        investigation = get(investigation);
        bean = getBean(investigation);
        assertTrue(bean.getBoolean(ReportBuilderImpl.CHECK_RESULTS));
        assertTrue(bean.getBoolean(ReportBuilderImpl.EXTERNAL_RESULTS));

        ReportBuilder builder2 = createBuilder(investigation);
        builder2.checkResults(false)
                .externalResults(false)
                .build();

        investigation = get(investigation);
        bean = getBean(investigation);
        assertFalse(bean.getBoolean(ReportBuilderImpl.CHECK_RESULTS));
        assertFalse(bean.getBoolean(ReportBuilderImpl.EXTERNAL_RESULTS));
    }

    /**
     * Verifies that  {@link Result#getReferenceRange()} and {@link Result#getOutOfRange()} are derived if ranges
     * are specified.
     */
    @Test
    public void testDeriveReferenceRange() {
        DocumentAct investigation = createInvestigation();

        Report report = createBuilder(investigation).results("1")
                .result("1")
                .analyteName("a1")
                .value(BigDecimal.valueOf(5))   // in range
                .extremeLowRange(BigDecimal.ONE).extremeHighRange(BigDecimal.valueOf(10))
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .result("2")
                .analyteName("a2")
                .value(BigDecimal.ZERO)         // out of range
                .extremeLowRange(BigDecimal.ONE).extremeHighRange(BigDecimal.valueOf(10))
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .add()
                .result("3")
                .analyteName("a3")
                .value(BigDecimal.valueOf(11)) // out of range
                .extremeLowRange(BigDecimal.ONE).extremeHighRange(BigDecimal.valueOf(10))
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .result("4")
                .analyteName("a4")
                .value(BigDecimal.valueOf(2))  // in range
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .result("5")
                .analyteName("a5")
                .value(BigDecimal.ONE)         // out of range
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .result("6")
                .analyteName("a6")
                .value(BigDecimal.valueOf(9))  // out of range
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .result("7")
                .analyteName("a7")
                .value(null)  // no value
                .lowRange(BigDecimal.valueOf(2)).highRange(BigDecimal.valueOf(8))
                .units("mg/L")
                .add()
                .add()
                .build();

        List<Results> results = report.getResults();
        assertEquals(1, results.size());
        List<Result> items = results.get(0).getResults();
        assertEquals(7, items.size());
        checkRange(items.get(0), "1", null, "a1", "5", null, false, "1 - 10 mg/L", null);
        checkRange(items.get(1), "2", null, "a2", "0", null, true, "1 - 10", null);
        checkRange(items.get(2), "3", null, "a3", "11", null, true, "1 - 10 mg/L", null);
        checkRange(items.get(3), "4", null, "a4", "2", null, false, "2 - 8 mg/L", null);
        checkRange(items.get(4), "5", null, "a5", "1", null, true, "2 - 8 mg/L", null);
        checkRange(items.get(5), "6", null, "a6", "9", null, true, "2 - 8 mg/L", null);
        checkRange(items.get(6), "7", null, "a7", null, null, false, "2 - 8 mg/L", null);
    }

    /**
     * Tests the precision when numeric values are specified.
     * <p/>
     * The value and ranges should be rounded to up to 5 decimal places. In practice, only 3 have been seen in the wild.
     */
    @Test
    public void testPrecision() {
        DocumentAct investigation = createInvestigation();

        Report report = createBuilder(investigation).results("1")
                .result("1")
                .analyteName("a1")
                .value(new BigDecimal("5.123456"))
                .lowRange(new BigDecimal("2.123456")).highRange(new BigDecimal("10.123456"))
                .add()
                .add()
                .build();

        List<Results> results = report.getResults();
        assertEquals(1, results.size());
        List<Result> items = results.get(0).getResults();
        checkRange(items.get(0), "1", null, "a1", "5.12346", null, false, "2.12346 - 10.12346", null);
    }

    /**
     * Verifies that notes greater than that that can be stored in a details node are supported, for a {@link Results}.
     */
    @Test
    public void testLongResultsNotes() {
        DocumentAct investigation = createInvestigation();

        Results results1 = createResults(investigation, RandomStringUtils.randomAlphanumeric(10000));
        DocumentAct longNote1 = getLongNote(results1);
        assertNotNull(longNote1);

        // update the notes
        Results results2 = createResults(investigation, RandomStringUtils.randomAlphanumeric(15000));
        DocumentAct longNote2 = getLongNote(results2);  // verify the same long note act is being used
        assertEquals(longNote1, longNote2);
        assertNull(get(longNote1.getDocument()));       // verify the original document has been removed

        // now update the note with short text that doesn't require a separate act and verify the act and document
        // is removed
        Results results3 = createResults(investigation, RandomStringUtils.randomAlphanumeric(500));

        DocumentAct longNote3 = getLongNote(results3);
        assertNull(longNote3);
        assertNull(get(longNote2.getDocument()));      // verify the document has been removed
    }

    /**
     * Verifies that results greater than that that can be stored in a details node are supported.
     */
    @Test
    public void testLongResults() {
        DocumentAct investigation = createInvestigation();

        String longResult = RandomStringUtils.randomAlphanumeric(10000);
        Result item1 = createResult(investigation, longResult, null);
        DocumentAct longResult1 = getLongResult(item1);
        assertNotNull(longResult1);

        // update the result
        String longerResult = RandomStringUtils.randomAlphanumeric(15000);
        Result item2 = createResult(investigation, longerResult, null);

        DocumentAct longResult2 = getLongResult(item2);  // verify the same long result act is being used
        assertEquals(longResult1, longResult2);
        assertNull(get(longResult1.getDocument()));      // verify the original document has been removed

        // now update the result with short text that doesn't require a separate act and verify the act and document
        // are removed
        String shortResult = RandomStringUtils.randomAlphanumeric(500);
        Result item3 = createResult(investigation, shortResult, null);

        DocumentAct longResult3 = getLongResult(item3);
        assertNull(longResult3);
        assertNull(get(longResult2.getDocument()));      // verify the document has been removed
    }

    /**
     * Verifies that notes greater than that that can be stored in a details node are supported, for a {@link Result}.
     */
    @Test
    public void testLongResultNotes() {
        DocumentAct investigation = createInvestigation();

        String result = "1";
        Result item1 = createResult(investigation, result, RandomStringUtils.randomAlphanumeric(10000));
        DocumentAct longNote1 = getLongNote(item1);
        assertNotNull(longNote1);

        // update the notes
        Result item2 = createResult(investigation, result, RandomStringUtils.randomAlphanumeric(15000));

        DocumentAct longNote2 = getLongNote(item2);  // verify the same long note act is being used
        assertEquals(longNote1, longNote2);
        assertNull(get(longNote1.getDocument()));    // verify the original document has been removed

        // now update the note with short text that doesn't require a separate act and verify the act and document
        // is removed
        Result item3 = createResult(investigation, result, RandomStringUtils.randomAlphanumeric(500));

        DocumentAct longNote3 = getLongNote(item3);
        assertNull(longNote3);
        assertNull(get(longNote2.getDocument()));      // verify the document has been removed
    }

    /**
     * Verifies images can be associated with a result.
     */
    @Test
    public void testImageResult() {
        DocumentAct investigation = createInvestigation();

        Result item1 = createImageResult(investigation, "image1.png", "image/png", RandomUtils.nextBytes(10));
        DocumentAct image1 = getImage(item1);
        assertNotNull(image1);

        // update the image
        byte[] content = RandomUtils.nextBytes(20);
        Result item2 = createImageResult(investigation, "image2.gif", "image/gif", content);

        DocumentAct image2 = getImage(item2);  // verify the same image act is being used
        assertEquals(image1, image2);
        assertNull(get(image1.getDocument()));    // verify the original document has been removed

        // update the result and verify the image is still attached
        Result updated = createResult(investigation, "foo", "bar");
        checkImage(updated, "image2.gif", "image/gif", content);
    }

    /**
     * Creates an investigation with associated order that is submitted.
     *
     * @return an investigation
     */
    private DocumentAct createInvestigation() {
        Party patient = patientFactory.createPatient();
        Entity laboratory = laboratoryFactory.createLaboratory();
        Party location = practiceFactory.createLocation();
        Entity investigationType = laboratoryFactory.createInvestigationType();
        return patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .location(location)
                .order(Order.Status.SUBMITTED.toString())
                .build();
    }

    /**
     * Adds/updates a {@link Results} to an investigation.
     *
     * @param investigation the investigation
     * @param notes         the notes
     * @return the results
     */
    private Results createResults(DocumentAct investigation, String notes) {
        Report report = createBuilder(investigation).results("1")
                .notes(notes)
                .add()
                .build();

        List<Results> list = report.getResults();
        assertEquals(1, list.size());
        Results results = list.get(0);
        assertEquals(notes, results.getNotes());
        return results;
    }

    /**
     * Adds/updates a {@link Result} to an investigation.
     *
     * @param investigation the investigation
     * @param result        the result text
     * @param notes         the notes
     * @return the result
     */
    private Result createResult(DocumentAct investigation, String result, String notes) {
        Report report = createBuilder(investigation).results("1")
                .result("1")
                .analyteName("a1")
                .result(result)
                .notes(notes)
                .add()
                .add()
                .build();

        List<Results> results = report.getResults();
        assertEquals(1, results.size());
        List<Result> items = results.get(0).getResults();
        assertEquals(1, items.size());

        Result item = items.get(0);
        assertEquals(result, item.getResult());
        assertEquals(notes, item.getNotes());
        return item;
    }

    /**
     * Adds/updates a {@link Result} with an image to an investigation.
     *
     * @param investigation the investigation
     * @param fileName      the image file name
     * @param mimeType      the image mime type
     * @param image         the image
     * @return the result
     */
    private Result createImageResult(DocumentAct investigation, String fileName, String mimeType, byte[] image) {
        Report report = createBuilder(investigation).results("1")
                .result("1")
                .analyteName("a1")
                .image(fileName, mimeType, new ByteArrayInputStream(image))
                .add()
                .add()
                .build();

        List<Results> results = report.getResults();
        assertEquals(1, results.size());
        List<Result> items = results.get(0).getResults();
        assertEquals(1, items.size());

        Result item = items.get(0);
        checkImage(item, fileName, mimeType, image);
        return item;
    }

    /**
     * Verifies an image on a result matches that expected.
     *
     * @param result   the result
     * @param fileName the expected file name
     * @param mimeType the expected mime type
     * @param image    the expected image content
     */
    private void checkImage(Result result, String fileName, String mimeType, byte[] image) {
        Document document = result.getImage();
        assertEquals(fileName, document.getName());
        assertEquals(mimeType, document.getMimeType());
        assertEquals(DocumentArchetypes.IMAGE_DOCUMENT, document.getArchetype());
        byte[] bytes = null;
        try {
            bytes = IOUtils.toByteArray(document.getContent());
        } catch (Exception exception) {
            fail("Failed to deserialize document: " + exception.getMessage());
        }
        assertArrayEquals(image, bytes);
    }

    /**
     * Returns the longResult act associated with a result.
     *
     * @param result the result
     * @return the corresponding act, May be {@code null}
     */
    private DocumentAct getLongResult(Result result) {
        IMObjectBean bean = getBean(((ResultImpl) result).getAct());
        return bean.getTarget("longResult", DocumentAct.class);
    }

    /**
     * Returns the longNote act associated with a {@link Results}.
     *
     * @param result the result
     * @return the corresponding act, May be {@code null}
     */
    private DocumentAct getLongNote(Results result) {
        IMObjectBean bean = getBean(((ResultsImpl) result).getAct());
        return bean.getTarget("longNotes", DocumentAct.class);
    }

    /**
     * Returns the longNote act associated with a {@link Result}.
     *
     * @param result the result
     * @return the corresponding act, May be {@code null}
     */
    private DocumentAct getLongNote(Result result) {
        IMObjectBean bean = getBean(((ResultImpl) result).getAct());
        return bean.getTarget("longNotes", DocumentAct.class);
    }

    /**
     * Returns the image act associated with a {@link Result}.
     *
     * @param result the result
     * @return the corresponding act, May be {@code null}
     */
    private DocumentAct getImage(Result result) {
        IMObjectBean bean = getBean(((ResultImpl) result).getAct());
        return bean.getTarget("image", DocumentAct.class);
    }

    /**
     * Returns a test, given its entity.
     *
     * @param test the test entity
     * @return the corresponding test
     */
    private org.openvpms.domain.laboratory.Test getTest(Entity test) {
        return domainService.create(test, org.openvpms.domain.laboratory.Test.class);
    }

    /**
     * Creates a builder for an investigation.
     *
     * @param investigation the investigation
     * @return the corresponding builder
     */
    private ReportBuilder createBuilder(DocumentAct investigation) {
        ReportBuilder builder;
        Act order = getBean(investigation).getTarget("order", Act.class);
        assertNotNull(order);

        builder = new ReportBuilderImpl(investigation, order, getArchetypeService(), domainService,
                                        handlers, documentRules, transactionManager);
        return builder;
    }

    /**
     * Verifies results match that expected.
     *
     * @param results      the results to check
     * @param id           the expected id
     * @param status       the expected status
     * @param date         the expected date
     * @param test         the expected test
     * @param categoryCode the expected category code
     * @param categoryName the expected category name
     * @param notes        the expected nodes
     */
    private void checkResults(Results results, String id, Result.Status status, OffsetDateTime date,
                              org.openvpms.domain.laboratory.Test test, String categoryCode, String categoryName,
                              String notes) {
        assertEquals(id, results.getResultsId());
        assertEquals(status, results.getStatus());
        assertEquals(date, results.getDate());
        assertEquals(test, results.getTest());
        assertEquals(categoryCode, results.getCategoryCode());
        assertEquals(categoryName, results.getCategoryName());
        assertEquals(notes, results.getNotes());
    }

    /**
     * Verifies a result matches that expected.
     *
     * @param actual         the result to check
     * @param id             the expected id
     * @param analyteCode    the expected analyte code
     * @param analyteName    the expected anayte name
     * @param result         the expected result
     * @param qualifier      the expected qualifier
     * @param outOfRange     the expected out of range flag
     * @param referenceRange the expected reference range
     * @param notes          the expected notes
     */
    private void checkRange(Result actual, String id, String analyteCode, String analyteName, String result,
                            String qualifier, boolean outOfRange, String referenceRange, String notes) {
        assertEquals(id, actual.getResultId());
        assertEquals(analyteCode, actual.getAnalyteCode());
        assertEquals(analyteName, actual.getAnalyteName());
        assertEquals(result, actual.getResult());
        assertEquals(qualifier, actual.getQualifier());
        assertEquals(outOfRange, actual.getOutOfRange());
        assertEquals(referenceRange, actual.getReferenceRange());
        assertEquals(notes, actual.getNotes());
    }

    /**
     * Verifies a report document matches that expected.
     *
     * @param report   the report
     * @param fileName the expected file name
     * @param content  the expected content
     * @return the document
     */
    private Document checkDocument(Report report, String fileName, String content) {
        Document document = report.getDocument();
        checkDocument(document, fileName, content, false);
        return document;
    }

    /**
     * Verifies a document matches that expected.
     *
     * @param document   the document
     * @param fileName   the expected file name
     * @param content    the expected content
     * @param compressed determines if the content is compressed or not
     */
    private void checkDocument(Document document, String fileName, String content, boolean compressed) {
        assertNotNull(document);
        assertEquals(fileName, document.getName());
        assertEquals("text/plain", document.getMimeType());
        String actual = null;
        try {
            if (compressed) {
                DocumentHandler handler = handlers.get(document);
                InputStream stream = handler.getContent(document);
                actual = IOUtils.toString(stream, StandardCharsets.UTF_8);
            } else {
                actual = IOUtils.toString(document.getContent(), StandardCharsets.UTF_8);
            }
        } catch (IOException exception) {
            fail("Failed to read content");
        }
        assertEquals(content, actual);
    }

    /**
     * Verifies that a document has been versioned.
     *
     * @param investigation the investigation
     * @param document      the expected document
     * @param fileName      the expected file name
     * @param content       the expected content
     * @param versions      the expected no. of versions
     */
    private void checkVersion(DocumentAct investigation, Document document, String fileName, String content,
                              int versions) {
        List<DocumentAct> acts = getBean(investigation).getTargets("versions", DocumentAct.class);
        assertEquals(versions, acts.size());
        DocumentAct match = null;
        int count = 0;
        for (DocumentAct act : acts) {
            if (Objects.equals(document.getObjectReference(), act.getDocument())) {
                match = act;
                ++count;
            }
        }

        assertNotNull(match);
        assertEquals(1, count);  // document should only be linked once

        // verify the investigationType was copied
        Reference investigationType = getBean(investigation).getTargetRef("investigationType");
        assertNotNull(investigationType);

        assertEquals(investigationType, getBean(match).getTargetRef("investigationType"));

        Document actual = get(match.getDocument(), Document.class);
        assertNotNull(actual);

        checkDocument(actual, fileName, content, true);
    }

    /**
     * Verifies that no document has been versioned.
     *
     * @param investigation the investigation
     */
    private void checkNoVersions(DocumentAct investigation) {
        List<DocumentAct> acts = getBean(investigation).getTargets("versions", DocumentAct.class);
        assertEquals(0, acts.size());
    }

    /**
     * Determines if MysQL 5.5 is being used.
     *
     * @return {@code true} if MySQL 5.5 is being used
     */
    private boolean isMySQL55() {
        boolean result = false;
        try (Connection connection = dataSource.getConnection()) {
            result = connection.getMetaData().getDatabaseProductVersion().startsWith("5.5");
        } catch (SQLException exception) {
            fail("Failed to get connection: " + exception.getMessage());
        }
        return result;
    }
}