/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.internal.dispatcher.OrderService;
import org.openvpms.laboratory.order.Order;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Base class for {@link AbstractOrderImpl} tests
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractOrderImplTest extends ArchetypeServiceTest {

    /**
     * The order service.
     */
    protected OrderService orderService;

    /**
     * The practice rules.
     */
    @Autowired
    protected PatientRules patientRules;

    /**
     * The domain object service.
     */
    @Autowired
    protected DomainService domainService;

    /**
     * The plugin archetype service.
     */
    protected PluginArchetypeService service;

    /**
     * The laboratory factory.
     */
    @Autowired
    protected TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * The user factory.
     */
    @Autowired
    protected TestUserFactory userFactory;

    /**
     * The patient.
     */
    protected Party patient;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The customer,
     */
    private Party customer;

    /**
     * The laboratory.
     */
    private Entity laboratory;

    /**
     * The investigation type.
     */
    private Entity investigationType;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * Sets up the tests.
     */
    @Before
    public void setUp() {
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getServiceUser()).thenReturn(
                new org.openvpms.component.business.domain.im.security.User());

        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
        location = practiceFactory.createLocation();
        investigationType = laboratoryFactory.createInvestigationType();
        orderService = new OrderService(getArchetypeService(), new LaboratoryRules(getArchetypeService()));
        laboratory = laboratoryFactory.createLaboratory(location);
        service = new PluginArchetypeService((IArchetypeRuleService) getArchetypeService(),
                                             getLookupService(), practiceService);
    }

    /**
     * Tests the {@link AbstractOrderImpl#getCreated()} method.
     */
    @Test
    public void testGetCreated() {
        Act act = createOrderAct();
        Order order = getOrder(act);
        OffsetDateTime expected = DateRules.toOffsetDateTime(act.getActivityStartTime());
        assertEquals(expected, order.getCreated());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getInvestigationType()} method.
     */
    @Test
    public void testGetInvestigationType() {
        Order order = createOrder();
        assertEquals(investigationType, order.getInvestigationType());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getDevice()} method.
     */
    public void testDevice() {
        DocumentAct investigation = createInvestigation();
        Order order1 = getOrder(getOrderAct(investigation));
        assertNull(order1.getDevice());

        Entity device = laboratoryFactory.createDevice(laboratory);
        patientFactory.updateInvestigation(investigation)
                .device(device)
                .build();

        Order order2 = getOrder(getOrderAct(investigation));
        assertEquals(device, order2.getDevice());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getTests()}  method.
     */
    public void testGetTests() {
        DocumentAct investigation = createInvestigation();
        Order order1 = getOrder(getOrderAct(investigation));
        assertTrue(order1.getTests().isEmpty());

        Entity test1 = laboratoryFactory.createTest(investigationType);
        Entity test2 = laboratoryFactory.createTest(investigationType);
        patientFactory.updateInvestigation(investigation)
                .addTests(test1, test2)
                .build();

        Order order2 = getOrder(getOrderAct(investigation));
        List<org.openvpms.domain.laboratory.Test> tests = order2.getTests();
        assertEquals(2, tests.size());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getLaboratory()} method.
     */
    @Test
    public void testGetLaboratory() {
        Order order = createOrder();
        assertEquals(laboratory, order.getLaboratory());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getPatient()} method.
     */
    @Test
    public void testGetPatient() {
        Order order = createOrder();
        assertEquals(patient, order.getPatient());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getCustomer()} method.
     */
    @Test
    public void testGetCustomer() {
        Order order = createOrder();
        assertEquals(customer, order.getCustomer());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getClinician()} method.
     */
    @Test
    public void testGetClinician() {
        Act act1 = createOrderAct(null);
        Order order1 = getOrder(act1);
        assertNull(order1.getClinician());

        User clinician = userFactory.createClinician();
        Act act2 = createOrderAct(clinician);
        Order order2 = getOrder(act2);
        assertEquals(clinician, order2.getClinician());

        User employee = userFactory.createClinician(ValueStrategy.randomString(), ValueStrategy.randomString());
        Act act3 = createOrderAct(employee);
        Order order3 = getOrder(act3);
        assertEquals(employee, order3.getClinician());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getLocation()} method.
     */
    @Test
    public void testGetLocation() {
        Order order = createOrder();
        assertEquals(location, order.getLocation());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getVisit()} method.
     */
    @Test
    public void testGetVisit() {
        Act investigation = createInvestigation();
        Act act = getOrderAct(investigation);
        Order order1 = getOrder(act);
        assertNull(order1.getVisit());

        Act visit = patientFactory.newVisit().patient(patient).addItem(investigation).build();
        Order order2 = getOrder(act);
        assertEquals(visit, order2.getVisit());
    }

    /**
     * Tests the {@link AbstractOrderImpl#getNotes()} method.
     */
    @Test
    public void testGetNotes() {
        DocumentAct investigation = createInvestigation();
        patientFactory.updateInvestigation(investigation)
                .description("some notes")
                .build();
        Act act = getOrderAct(investigation);
        Order order = getOrder(act);
        assertEquals("some notes", order.getNotes());
    }

    /**
     * Verifies that the {@link AbstractOrderImpl#getUser()} method is mapped to the {@link Act#getCreatedBy()} method.
     */
    @Test
    public void testUser() {
        User user1 = userFactory.createUser();
        new AuthenticationContextImpl().setUser(user1);
        Order order1 = createOrder();
        assertEquals(user1, order1.getUser());

        User user2 = userFactory.createEmployee();
        new AuthenticationContextImpl().setUser(user2);
        Order order2 = createOrder();
        assertEquals(user2, order2.getUser());
    }

    /**
     * Tests the {@link AbstractOrderImpl#equals(Object)} and {@link AbstractOrderImpl#hashCode()} methods.
     */
    @Test
    public void testEquals() {
        Act act1 = createOrderAct();
        Order orderA = getOrder(act1);
        Order orderB = getOrder(act1);
        assertEquals(orderA, orderA);
        assertEquals(orderA, orderB);

        Act act2 = createOrderAct();
        Order orderC = getOrder(act2);
        assertNotEquals(orderA, orderC);

        assertEquals(orderA.hashCode(), orderA.hashCode());
        assertEquals(orderA.hashCode(), orderB.hashCode());
        assertNotEquals(orderA.hashCode(), orderC.hashCode());
    }

    /**
     * Verifies that the order can be created using {@link DomainService}.
     */
    @Test
    public abstract void testCreateViaDomainService();

    /**
     * Creates a new order.
     *
     * @return a new order
     */
    protected Order createOrder() {
        Act act = createOrderAct();
        return getOrder(act);
    }

    /**
     * Creates a new order act.
     *
     * @param clinician the clinician
     * @return the order act
     */
    protected abstract Act createOrderAct(User clinician);

    /**
     * Returns an order act for an investigation.
     *
     * @param investigation the investigation
     * @return the order act
     */
    protected abstract Act getOrderAct(Act investigation);

    /**
     * Creates a new order act.
     *
     * @return the order act
     */
    protected Act createOrderAct() {
        return createOrderAct(userFactory.createClinician());
    }

    /**
     * Returns an order for an act created via {@link #createOrderAct()}.
     *
     * @param act the act
     * @return the corresponding order
     */
    protected abstract Order getOrder(Act act);

    /**
     * Creates an investigation.
     *
     * @return the investigation
     */
    protected DocumentAct createInvestigation() {
        return createInvestigation(userFactory.createClinician());
    }

    /**
     * Creates an investigation.
     *
     * @param clinician the clinician
     * @return the investigation
     */
    protected DocumentAct createInvestigation(User clinician) {
        return patientFactory.newInvestigation()
                .patient(patient)
                .clinician(clinician)
                .location(location)
                .investigationType(investigationType)
                .laboratory(laboratory)
                .startTime(new Date()).build();
    }
}