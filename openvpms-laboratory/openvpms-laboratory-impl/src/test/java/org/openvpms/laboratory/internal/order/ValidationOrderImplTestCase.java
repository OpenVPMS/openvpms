/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.junit.Test;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link ValidationOrderImpl} class.
 *
 * @author Tim Anderson
 */
public class ValidationOrderImplTestCase extends AbstractOrderImplTest {

    /**
     * Verifies that the {@link Order#setOrderId(String, String)} and {@link Order#setOrderIdentity(ActIdentity)}
     * methods throw an exception.
     */
    @Test
    public void testSetOrderId() {
        Order order = createOrder();
        try {
            order.setOrderId("actIdentity.laboratoryOrderTest", "1");
            fail("Expected setOrderId() to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0012: This order cannot be modified", expected.getMessage());
        }

        try {
            order.setOrderIdentity(laboratoryFactory.createOrderIdentity());
            fail("Expected setOrderId() to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0012: This order cannot be modified", expected.getMessage());
        }
    }
    /**
     * Verifies that calling {@link ValidationOrderImpl#setStatus(Order.Status)} and
     * {@link ValidationOrderImpl#setStatus(Order.Status, String)} throws an exception.
     */
    @Test
    public void testSetStatus() {
        Order order = createOrder();
        try {
            order.setStatus(Order.Status.SUBMITTED);
            fail("Expected setStatus() to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0012: This order cannot be modified", expected.getMessage());
        }

        try {
            order.setStatus(Order.Status.SUBMITTED, "submitted");
            fail("Expected setStatus() to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0012: This order cannot be modified", expected.getMessage());
        }
    }

    /**
     * Tests the {@link ValidationOrderImpl#getReport()} method.
     */
    @Test
    public void testGetReport() {
        Order order = createOrder();
        Report report = order.getReport();
        assertNull(report.getReportId());
        assertEquals(Report.Status.PENDING, report.getStatus());
        assertNull(report.getSynchronisationId());
        assertNull(report.getSummary());
        assertTrue(report.getResults().isEmpty());
        assertNull(report.getDocument());
        assertFalse(report.hasExternalResults());
    }

    /**
     * Verifies that the {@link ValidationOrderImpl#getReportBuilder()} method throws an exception.
     */
    @Test
    public void testGetReportBuilder() {
        Order order = createOrder();
        try {
            order.getReportBuilder();
            fail("Expected getReportBuilder() to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0012: This order cannot be modified", expected.getMessage());
        }
    }

    /**
     * Verifies that the order can be created using {@link DomainService}.
     */
    @Test
    public void testCreateViaDomainService() {
        Act investigation = createInvestigation();
        ValidationOrderImpl order = domainService.create(investigation, ValidationOrderImpl.class);
        assertNotNull(order);
    }

    /**
     * Creates a new order act.
     *
     * @param clinician the clinician
     * @return the order act
     */
    @Override
    protected Act createOrderAct(User clinician) {
        return createInvestigation(clinician);
    }

    /**
     * Returns an order act for an investigation.
     * <p/>
     * For validation, the investigation is used.
     *
     * @param investigation the investigation
     * @return the order act
     */
    @Override
    protected Act getOrderAct(Act investigation) {
        return investigation;
    }

    /**
     * Returns an order for an act created via {@link #createOrderAct()}.
     *
     * @param act the investigation
     * @return the corresponding order
     */
    @Override
    protected Order getOrder(Act act) {
        return new ValidationOrderImpl(act, service, patientRules, domainService);
    }
}