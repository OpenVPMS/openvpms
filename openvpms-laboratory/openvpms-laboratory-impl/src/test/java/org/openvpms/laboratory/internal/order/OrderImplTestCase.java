/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link OrderImpl} class.
 *
 * @author Tim Anderson
 */
public class OrderImplTestCase extends AbstractOrderImplTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * Verifies that the order can be created using {@link DomainService}.
     */
    @Override
    public void testCreateViaDomainService() {
        Act investigation = createInvestigation();
        Act act = orderService.order(investigation);
        OrderImpl order = domainService.create(act, OrderImpl.class);
        assertNotNull(order);
    }

    /**
     * Tests the {@link OrderImpl#setOrderId(String, String)} method.
     */
    @Test
    public void testSetOrderId() {
        Order order = createOrder();
        order.setOrderId("actIdentity.laboratoryOrderTest", "1");
        assertEquals("1", order.getOrderId());
    }

    /**
     * Tests the {@link OrderImpl#setOrderIdentity(ActIdentity)} method.
     */
    @Test
    public void testSetOrderIdentity() {
        Order order = createOrder();
        ActIdentity id = laboratoryFactory.createOrderIdentity();
        order.setOrderIdentity(id);
        assertEquals(id.getIdentity(), order.getOrderId());
        assertEquals(id, order.getOrderIdentity());
    }

    /**
     * Tests status changes.
     */
    @Test
    public void testStatusChange() {
        Act investigation = createInvestigation();
        Act act = orderService.order(investigation);
        Order order = getOrder(act);
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.QUEUED, null, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        order.setStatus(Order.Status.SUBMITTING, "Sending");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.SUBMITTING, "Sending", ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);
        // IN_PROGRESS status doesn't propagate to investigation, so neither does message

        order.setStatus(Order.Status.SUBMITTED, "Submitted OK");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.SUBMITTED, "Submitted OK", ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);
        // investigation already SENT, so message not propagated

//        order.setStatus(Order.Status.WAITING_FOR_SAMPLE, "Provide sample");
//        checkStatus(order, investigation, Order.Type.NEW, Order.Status.WAITING_FOR_SAMPLE,
//                    InvestigationActStatus.WAITING_FOR_SAMPLE, "Provide sample");
//
//        order.setStatus(Order.Status.PARTIAL_RESULTS);
//        checkStatus(order, investigation, Order.Type.NEW, Order.Status.PARTIAL_RESULTS,
//                    InvestigationActStatus.PARTIAL_RESULTS, null);

        order.setStatus(Order.Status.COMPLETED);
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.COMPLETED,
                    null, ActStatus.IN_PROGRESS, InvestigationActStatus.RECEIVED, null);
    }

    /**
     * Verifies that orders with ERROR status can only be cancelled.
     */
    @Test
    public void testOrderWithErrorStatus() {
        Act investigation = createInvestigation();
        Act act = orderService.order(investigation);
        Order order = getOrder(act);

        order.setStatus(Order.Status.ERROR, "error");
        checkStatus(order, investigation, Order.Type.NEW, Order.Status.ERROR, "error", ActStatus.IN_PROGRESS,
                    InvestigationActStatus.ERROR, "error");

        try {
            order.setStatus(Order.Status.COMPLETED);
            fail("Expected exception");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0011: Orders with ERROR status must be cancelled", expected.getMessage());
        }

        orderService.cancel(act);
        checkStatus(order, investigation, Order.Type.CANCEL, Order.Status.QUEUED, "error", ActStatus.IN_PROGRESS,
                    InvestigationActStatus.ERROR, "error");
    }

    /**
     * Tests order cancellation.
     */
    @Test
    public void testCancellation() {
        Act investigation = createInvestigation();
        Act act = orderService.order(investigation);
        Order newOrder = getOrder(act);
        newOrder.setStatus(Order.Status.SUBMITTED);

        checkStatus(newOrder, investigation, Order.Type.NEW, Order.Status.SUBMITTED, null, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        assertTrue(orderService.cancel(act));
        Order cancellation = getOrder(act);
        assertEquals(Order.Type.CANCEL, cancellation.getType());

        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.QUEUED, null, ActStatus.IN_PROGRESS,
                    InvestigationActStatus.SENT, null);

        cancellation.setStatus(Order.Status.SUBMITTING);
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.SUBMITTING,
                    null, ActStatus.IN_PROGRESS, InvestigationActStatus.SENT, null);

        try {
            cancellation.setStatus(Order.Status.SUBMITTED);
            fail("Expected setting of status to SUBMITTED for a cancellation to fail");
        } catch (LaboratoryException expected) {
            assertEquals("LAB-0010: Cannot assign status SUBMITTED to order cancellation", expected.getMessage());
        }

        cancellation.setStatus(Order.Status.ERROR, "some error");
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.ERROR,
                    "some error", ActStatus.IN_PROGRESS, InvestigationActStatus.ERROR, "some error");

//        try {
//            cancellation.setStatus(Order.Status.WAITING_FOR_SAMPLE);
//            fail("Expected exception");
//        } catch (LaboratoryException expected) {
//            assertEquals("LAB-0010: Cannot assign status WAITING_FOR_SAMPLE to order cancellation",
//                         expected.getMessage());
//        }

//        try {
//            cancellation.setStatus(Order.Status.PARTIAL_RESULTS);
//            fail("Expected exception");
//        } catch (LaboratoryException expected) {
//            assertEquals("LAB-0010: Cannot assign status PARTIAL_RESULTS to order cancellation", expected.getMessage());
//        }

        cancellation.setStatus(Order.Status.CANCELLED);
        checkStatus(cancellation, investigation, Order.Type.CANCEL, Order.Status.CANCELLED,
                    null, ActStatus.CANCELLED, InvestigationActStatus.ERROR, null);
    }

    /**
     * Tests cancellation of an order linked to a POSTED investigation.
     * <p/>
     * Investigations are locked from editing after a period of time if record locking is enabled, by setting the
     * status to POSTED. This doesn't prevent them from being cancelled by labs.
     */
    @Test
    public void testCancelPostedInvestigation() {
        Act investigation = createInvestigation();
        Act act = orderService.order(investigation);
        Order order = getOrder(act);
        order.setStatus(Order.Status.SUBMITTED);

        investigation = get(investigation);
        assertEquals(ActStatus.IN_PROGRESS, investigation.getStatus());
        investigation.setStatus(ActStatus.POSTED);
        save(investigation);

        order.setStatus(Order.Status.CANCELLED);

        checkStatus(order, investigation, Order.Type.CANCEL, Order.Status.CANCELLED, null, ActStatus.CANCELLED,
                    InvestigationActStatus.SENT, null);
    }

    /**
     * Creates a new order act.
     *
     * @param clinician the clinician
     * @return the order act
     */
    @Override
    protected Act createOrderAct(User clinician) {
        Act investigation = createInvestigation(clinician);
        return getOrderAct(investigation);
    }

    /**
     * Returns an order act for an investigation.
     *
     * @param investigation the investigation
     * @return the order act
     */
    @Override
    protected Act getOrderAct(Act investigation) {
        return orderService.order(investigation);
    }

    /**
     * Returns an order for an act created via {@link #createOrderAct()}.
     *
     * @param act the act
     * @return the corresponding order
     */
    protected OrderImpl getOrder(Act act) {
        return new OrderImpl(act, service, patientRules, domainService, transactionManager, handlers, documentRules);
    }

    /**
     * Verifies that the order and investigation state match that expected.
     *
     * @param order                    the order
     * @param investigation            the investigation
     * @param type                     the expected order type
     * @param status                   the expected order status
     * @param orderMessage             the expected order status message. May be {@code null}
     * @param investigationStatus      the expected investigation status
     * @param investigationOrderStatus the expected investigation order status
     * @param investigationMessage     the expected investigation status message
     */
    private void checkStatus(Order order, Act investigation, Order.Type type, Order.Status status,
                             String orderMessage, String investigationStatus, String investigationOrderStatus,
                             String investigationMessage) {
        Act act = get(investigation);
        assertEquals(type, order.getType());
        assertEquals(status, order.getStatus());
        assertEquals(orderMessage, order.getMessage());
        assertEquals(investigationStatus, act.getStatus());
        assertEquals(investigationOrderStatus, act.getStatus2());
        IMObjectBean bean = getBean(act);
        assertEquals(investigationMessage, bean.getString("message"));
    }
}
