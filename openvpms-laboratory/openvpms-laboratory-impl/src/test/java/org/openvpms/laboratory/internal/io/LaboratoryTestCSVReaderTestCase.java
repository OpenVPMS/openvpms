/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.io;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link LaboratoryTestCSVReader} class.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestCSVReaderTestCase extends AbstractLaboratoryIOTestCase {

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests CSV reading.
     */
    @Test
    public void testRead() {
        Document document = (Document) documentFactory.createDocument("/LaboratoryTestData.csv");
        LaboratoryTestCSVReader reader = new LaboratoryTestCSVReader(documentHandlers, ',');
        LaboratoryTestDataSet set = reader.read(document);
        List<LaboratoryTestData> data = set.getData();
        assertEquals(3, data.size());
        checkData(data.get(0), "ALab", "AAA", "A Test", "A Test Description", "A Test Specimen details",
                  "A Test Turnaround", null, 1, 2);
        checkData(data.get(1), "ALab", "BBB", "B Test", "B Test Description", "B Test Specimen details",
                  "B Test Turnaround", null, 2, 3);
        checkData(data.get(2), "ALab", "C CC", "C Test", "C Test Description", "C Test Specimen details",
                  "C Test Turnaround", null, 3, 4);
        List<LaboratoryTestData> errors = set.getErrors();
        assertEquals(4, errors.size());
        checkData(errors.get(0), "ALab", "AAA", "A Test 2", "A Test Description", "A Test Specimen details",
                  "A Test Turnaround", null, 2, null, null, 5, "Value for Code at line 2 is duplicated at line 5");
        checkData(errors.get(1), "ALab", "DDD", "D Test", "D Test Description", "D Test Specimen details",
                  "D Test Turnaround", null, null, null, null, 6, "'$4' is not a valid value for Price");
        checkData(errors.get(2), "ALab", "EEE", "E Test", "E Test Description", "E Test Specimen details",
                  "E Test Turnaround", null, null, null, null, 7, "'-1' is not a valid value for Price");
        checkData(errors.get(3), null, null, null, null, null, null, null, null, null, null, 8,
                  "'A Lab' is not a valid value for Laboratory");
    }

    /**
     * Verifies that if a CSV contains a new lines, they are replaced with exception for the specimen field, where
     * they are supported.
     */
    @Test
    public void testNewLines() {
        Document document = (Document) documentFactory.createDocument("/LaboratoryTestData4.csv");
        LaboratoryTestCSVReader reader = new LaboratoryTestCSVReader(documentHandlers, ',');

        LaboratoryTestDataSet set = reader.read(document);
        List<LaboratoryTestData> data = set.getData();
        assertEquals(5, data.size());
        checkData(data.get(0), "ALab", "AAA", "A Name", "A Description", "A Specimen", "A Turnaround", null, 1, 2);
        checkData(data.get(1), "ALab", "BBB", "B Name", "B Description", "B Specimen", "B Turnaround", null, 2, 3);
        checkData(data.get(2), "ALab", "CCC", "C Name", "C Description", "C Specimen", "C Turnaround", null, 3, 4);
        checkData(data.get(3), "ALab", "DDD", "D Name", "D Description", "D\nSpecimen", "D Turnaround", null, 4, 5);
        checkData(data.get(4), "ALab", "EEE", "E Name", "E Description", "E Specimen", "E Turnaround", null, 5, 6);
        List<LaboratoryTestData> errors = set.getErrors();
        assertEquals(0, errors.size());
    }

    /**
     * Verifies that if a CSV contains a UTF-8 byte-order mark, it is skipped.
     * @throws Exception for any error
     */
    @Test
    public void testByteOrderMark() throws Exception {
        Document document = (Document) documentFactory.createDocument("/LaboratoryTestDataBOM.csv");
        LaboratoryTestCSVReader reader = new LaboratoryTestCSVReader(documentHandlers, ',');

        // verify the file has a UTF-8 byte order mark
        try (InputStream content = documentHandlers.get(document).getContent(document);
             BOMInputStream bomInputStream = new BOMInputStream(content)) {
            assertTrue(bomInputStream.hasBOM(ByteOrderMark.UTF_8));
        }

        LaboratoryTestDataSet set = reader.read(document);
        List<LaboratoryTestData> data = set.getData();
        assertEquals(3, data.size());
        checkData(data.get(0), "ALab", "AAA", "A Test", "A Test Description", "A Test Specimen details",
                  "1-2D", null, 50, 2);
        checkData(data.get(1), "ALab", "BBB", "B Test", "B Test Description", "B Test Specimen details",
                  "1-2D", null, 60, 3);
        checkData(data.get(2), "ALab", "CCC", "C Test", "C Test Description", "C Test Specimen details",
                  "1-2D", null, 70, 4);
        List<LaboratoryTestData> errors = set.getErrors();
        assertEquals(0, errors.size());
    }
}