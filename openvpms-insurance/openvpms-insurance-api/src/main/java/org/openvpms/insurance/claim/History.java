/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.claim;

/**
 * Accesses the patient's clinical history.
 *
 * @author Tim Anderson
 */
public interface History {

    /**
     * Returns the clinical notes for the patient, up until the time of the claim.
     *
     * @return the clinical notes
     */
    Iterable<Note> getNotes();

    /**
     * Returns the clinical notes since the last claim.
     * <p/>
     * This applies to insurers that have a common insurance service. It returns notes that have not been submitted
     * to the insurance service before, or were part of a cancelled claim. The objective is to reduce redundant
     * history submissions.
     * <p/>
     * If there has been no prior claim, or the insurer isn't linked to an insurance service, all notes up to the time
     * of the claim will be returned.
     *
     * @return the clinical notes
     */
    Iterable<Note> getNotesSinceLastClaim();

}