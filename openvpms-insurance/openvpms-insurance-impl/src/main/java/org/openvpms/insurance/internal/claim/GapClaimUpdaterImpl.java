/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.Deposit;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.claim.GapClaim.GapStatus;
import org.openvpms.insurance.claim.GapClaimUpdater;
import org.openvpms.insurance.exception.InsuranceException;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Default implementation of {@link GapClaimUpdater}.
 *
 * @author Tim Anderson
 */
class GapClaimUpdaterImpl extends ClaimUpdaterImpl implements GapClaimUpdater {

    /**
     * The new deposits.
     */
    private final List<FinancialAct> newDeposits = new ArrayList<>();

    /**
     * The benefit amount.
     */
    private BigDecimal benefitAmount;

    /**
     * The benefit notes.
     */
    private String benefitNotes;

    /**
     * The vet benefit amount.
     */
    private BigDecimal vetBenefitAmount;

    /**
     * All deposits.
     */
    private Map<String, Deposit> allDeposits;

    /**
     * Constructs a {@link GapClaimUpdaterImpl}.
     *
     * @param claim   the gap claim
     * @param bean    the bean wrapping the claim act
     * @param service the archetype service
     * @param rules   the insurance rules
     */
    public GapClaimUpdaterImpl(GapClaimImpl claim, IMObjectBean bean, ArchetypeService service, InsuranceRules rules) {
        super(claim, bean, service, rules);
    }

    /**
     * Updates the gap claim with the benefit.
     * <p>
     * This is only valid when the gap status is {@link GapStatus#PENDING}.<br/>
     * This sets the gap status to {@link GapStatus#RECEIVED}.
     *
     * @param amount the benefit amount
     * @param notes  notes associated with the benefit amount
     * @throws IllegalStateException if the gap status is not {@link GapStatus#PENDING}
     */
    @Override
    public GapClaimUpdater benefit(BigDecimal amount, String notes) {
        GapStatus status = getClaim().getGapStatus();
        if (status != GapStatus.PENDING) {
            throw new IllegalStateException("Cannot set the benefit amount for gap claims with status=" + status);
        }
        if (amount == null) {
            throw new IllegalArgumentException("Argument 'amount' is required");
        }
        if (amount.compareTo(MathRules.round(amount, 2)) != 0) {
            throw new IllegalArgumentException("Argument 'amount' must be rounded to 2 decimal places");
        }
        benefitAmount = amount;
        benefitNotes = notes;
        return this;
    }

    /**
     * Updates the gap claim with the vet benefit.
     *
     * @param amount the vet benefit amount
     */
    @Override
    public GapClaimUpdater vetBenefitAmount(BigDecimal amount) {
        vetBenefitAmount = amount;
        return this;
    }

    /**
     * Determines if there is deposit with the specified identifier.
     *
     * @param id the deposit identifier
     * @return {@code true} if the deposit exists, otherwise {@code false}
     */
    @Override
    public boolean hasDeposit(String id) {
        return getDeposits().get(id) != null;
    }

    /**
     * Adds a deposit.
     * <p/>
     * A claim can have a single identifier issued by an insurer. To avoid duplicates, each insurance service must
     * provide a unique archetype.
     *
     * @param id        the deposit identifier
     * @param archetype the deposit identifier archetype. Must have an <em>actIdentity.insuranceDeposit*</em> prefix
     * @param date      the date the deposit was paid
     * @param amount    the deposit amount
     */
    @Override
    public GapClaimUpdater deposit(String id, String archetype, OffsetDateTime date, BigDecimal amount) {
        if (id == null) {
            throw new IllegalArgumentException("Argument 'id' is required");
        }
        if (!TypeHelper.isA(archetype, InsuranceArchetypes.DEPOSIT_IDENTITIES)) {
            throw new IllegalArgumentException("Argument 'archetype' must have a "
                                               + InsuranceArchetypes.DEPOSIT_IDENTITIES + " prefix");
        }
        if (date == null) {
            throw new IllegalArgumentException("Argument 'date' is required");
        }
        if (amount == null) {
            throw new IllegalArgumentException("Argument 'amount' is required");
        }
        if (amount.compareTo(MathRules.round(amount, 2)) != 0) {
            throw new IllegalArgumentException("Argument 'amount' must be rounded to 2 decimal places");
        }
        if (hasDeposit(id)) {
            throw new IllegalArgumentException("Duplicate deposit with identifier " + id);
        } else {
            ArchetypeService service = getService();
            FinancialAct act = service.create(InsuranceArchetypes.DEPOSIT, FinancialAct.class);
            ActIdentity identity = service.create(archetype, ActIdentity.class);
            identity.setIdentity(id);
            act.setActivityStartTime(DateRules.toDate(date));
            act.setTotal(amount);
            act.addIdentity(identity);
            newDeposits.add(act);
            allDeposits.put(id, new DepositImpl(act, service));
        }
        return this;
    }

    /**
     * Updates the claim.
     *
     * @return the claim
     * @throws InsuranceException if the claim cannot be updated
     */
    @Override
    public GapClaim update() {
        return (GapClaim) super.update();
    }

    /**
     * Returns the claim.
     *
     * @return the claim
     */
    @Override
    protected GapClaimImpl getClaim() {
        return (GapClaimImpl) super.getClaim();
    }

    /**
     * Updates the claim.
     *
     * @param bean   the claim bean
     * @param toSave any related objects to save if the claim has been updated
     * @return {@code true} if the claim was updated, otherwise {@code false}
     */
    @Override
    protected boolean doUpdate(IMObjectBean bean, List<IMObject> toSave) {
        boolean updated = super.doUpdate(bean, toSave);
        updated |= updateBenefit(bean);
        updated |= updateVetBenefit(bean);
        updated |= updateDeposits(bean, toSave);
        return updated;
    }

    /**
     * Updates the benefit amount.
     *
     * @param bean the claim bean
     * @return {@code true} if the benefit amount was updated
     */
    private boolean updateBenefit(IMObjectBean bean) {
        boolean updated = false;
        if (benefitAmount != null) {
            if (!Objects.equals(benefitAmount, bean.getBigDecimal(GapClaimImpl.BENEFIT_AMOUNT))
                || !Objects.equals(benefitNotes, bean.getString(GapClaimImpl.BENEFIT_NOTES))
                || !Objects.equals(GapStatus.RECEIVED.toString(), bean.getString(GapClaimImpl.GAP_STATUS))) {
                bean.setValue(GapClaimImpl.BENEFIT_AMOUNT, benefitAmount);
                bean.setValue(GapClaimImpl.BENEFIT_NOTES, benefitNotes);
                bean.setValue(GapClaimImpl.GAP_STATUS, GapStatus.RECEIVED.toString());
                updated = true;
            }
        }
        return updated;
    }

    /**
     * Updates the vet benefit amount.
     *
     * @param bean the claim bean
     * @return {@code true} if the vet benefit amount was updated
     */
    private boolean updateVetBenefit(IMObjectBean bean) {
        boolean updated = false;
        if (vetBenefitAmount != null && !Objects.equals(vetBenefitAmount,
                                                        bean.getBigDecimal(GapClaimImpl.VET_BENEFIT_AMOUNT))) {
            bean.setValue(GapClaimImpl.VET_BENEFIT_AMOUNT, vetBenefitAmount);
            updated = true;
        }
        return updated;
    }

    /**
     * Updates the deposits.
     *
     * @param bean   the claim bean
     * @param toSave any related objects to save if the claim has been updated
     * @return {@code true} if the deposits were updated
     */
    private boolean updateDeposits(IMObjectBean bean, List<IMObject> toSave) {
        boolean updated = false;
        if (!newDeposits.isEmpty()) {
            BigDecimal total = BigDecimal.ZERO;
            for (FinancialAct act : newDeposits) {
                bean.addTarget(GapClaimImpl.DEPOSITS, act, "claim");
                toSave.add(act);
            }
            for (Deposit deposit : allDeposits.values()) {
                total = total.add(deposit.getAmount());
            }
            updated = true;

            getClaim().updateDeposits(new ArrayList<>(allDeposits.values()));
            // need to propagate back to the claim as it is cached
        }
        return updated;
    }

    /**
     * Returns the deposits.
     *
     * @return the deposits
     */
    private Map<String, Deposit> getDeposits() {
        if (allDeposits == null) {
            allDeposits = new HashMap<>();
            for (Deposit deposit : getClaim().getDeposits()) {
                allDeposits.put(deposit.getDepositId(), deposit);
            }
        }
        return allDeposits;
    }
}
