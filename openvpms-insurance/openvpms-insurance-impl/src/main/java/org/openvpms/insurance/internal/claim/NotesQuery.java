/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.insurance.claim.Note;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Queries {@link Note}s for a patient.
 *
 * @author Tim Anderson
 */
class NotesQuery {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link NotesQuery}.
     *
     * @param service the archetype service
     */
    public NotesQuery(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Queries all notes linked to <em>act.patientClinicalEvents</em> dated between the specified date range.
     *
     * @param patient the patient
     * @param from    the start date (inclusive). May be {@code null}
     * @param to      the end date (exclusive). May be {@code null}
     * @param pageSize the page size
     * @return the notes
     */
    public Iterable<Note> query(Party patient, Date from, Date to, int pageSize) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> note = query.from(Act.class, PatientArchetypes.CLINICAL_NOTE);
        query.select(note);
        Join<IMObject, IMObject> withEvent = note.join("event").join("source");
        Join<IMObject, IMObject> withPatient = withEvent.join("patient").join("entity");
        withPatient.on(builder.equal(withPatient.reference(), patient.getObjectReference()));
        List<Predicate> predicates = new ArrayList<>();
        Path<Date> startTime = withEvent.get("startTime");
        Path<Date> endTime = withEvent.get("endTime");
        if (from != null) {
            predicates.add(builder.greaterThanOrEqualTo(startTime, from));
        }
        if (to != null) {
            predicates.add(builder.or(builder.isNull(endTime), builder.lessThan(endTime, to)));
        }
        query.where(predicates);
        query.orderBy(builder.asc(startTime),
                      builder.asc(withEvent.get("id")),
                      builder.asc(note.get("startTime")),
                      builder.asc(note.get("id")));
        return () -> {
            TypedQuery<Act> typedQuery = service.createQuery(query);
            return new NoteIterator(new TypedQueryIterator<>(typedQuery, pageSize), service);
        };
    }

    private static class NoteIterator implements Iterator<Note> {

        private final Iterator<Act> iterator;

        private final ArchetypeService service;

        public NoteIterator(Iterator<Act> iterator, ArchetypeService service) {
            this.iterator = iterator;
            this.service = service;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         */
        @Override
        public Note next() {
            Act act = iterator.next();
            return new NoteImpl(act, service);
        }
    }
}
