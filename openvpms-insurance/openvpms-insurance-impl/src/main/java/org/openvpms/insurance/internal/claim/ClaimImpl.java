/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.collections4.IterableUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.act.ActIdentity;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.component.system.common.cache.MapIMObjectCache;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.practice.LocationImpl;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.practice.Location;
import org.openvpms.insurance.claim.Attachment;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.ClaimHandler;
import org.openvpms.insurance.claim.ClaimUpdater;
import org.openvpms.insurance.claim.Condition;
import org.openvpms.insurance.claim.History;
import org.openvpms.insurance.claim.Invoice;
import org.openvpms.insurance.claim.Note;
import org.openvpms.insurance.exception.InsuranceException;
import org.openvpms.insurance.internal.i18n.InsuranceMessages;
import org.openvpms.insurance.internal.policy.PolicyImpl;
import org.openvpms.insurance.policy.Policy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of {@link Claim}.
 *
 * @author Tim Anderson
 */
public class ClaimImpl implements Claim {

    /**
     * The claim.
     */
    private final IMObjectBean claim;

    /**
     * The claim act.
     */
    private final Act act;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The customer rules.
     */
    private final CustomerRules customerRules;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The allocations.
     */
    private final Allocations allocations;

    /**
     * Cache of objects to reduce redundant retrieval.
     */
    private final IMObjectCache cache;

    /**
     * The policy.
     */
    private PolicyImpl policy;

    /**
     * The claim conditions.
     */
    private List<Condition> conditions;

    /**
     * The clinical history, up to the time of the claim.
     */
    private List<Note> history;

    /**
     * The claim attachments.
     */
    private List<Attachment> attachments;

    /**
     * The claim handler.
     */
    private ClaimHandler handler;

    /**
     * Constructs a {@link ClaimImpl}.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public ClaimImpl(Act claim, IArchetypeService service, InsuranceRules insuranceRules,
                     CustomerRules customerRules, PatientRules patientRules, DocumentHandlers handlers,
                     PlatformTransactionManager transactionManager, DomainService domainService) {
        this(service.getBean(claim), claim, service, insuranceRules, customerRules, patientRules, handlers,
             transactionManager, domainService);
    }

    /**
     * Constructs a {@link ClaimImpl}.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public ClaimImpl(IMObjectBean claim, IArchetypeService service, InsuranceRules insuranceRules,
                     CustomerRules customerRules, PatientRules patientRules, DocumentHandlers handlers,
                     PlatformTransactionManager transactionManager, DomainService domainService) {
        this(claim, claim.getObject(Act.class), service, insuranceRules, customerRules, patientRules, handlers,
             transactionManager, domainService);
    }

    /**
     * Constructs a {@link ClaimImpl}.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    private ClaimImpl(IMObjectBean claim, Act act, IArchetypeService service, InsuranceRules insuranceRules,
                      CustomerRules customerRules, PatientRules patientRules, DocumentHandlers handlers,
                      PlatformTransactionManager transactionManager, DomainService domainService) {
        this.claim = claim;
        this.act = act;
        this.service = service;
        this.insuranceRules = insuranceRules;
        this.customerRules = customerRules;
        this.patientRules = patientRules;
        this.handlers = handlers;
        this.transactionManager = transactionManager;
        this.domainService = domainService;
        this.allocations = new Allocations(this, service);
        cache = new MapIMObjectCache(service);
    }

    /**
     * Returns the OpenVPMS identifier for this claim.
     *
     * @return the claim identifier
     */
    @Override
    public long getId() {
        return claim.getObject().getId();
    }

    /**
     * Returns the claim identifier, issued by the insurer.
     *
     * @return the claim identifier, or {@code null} if none has been issued
     */
    @Override
    public String getInsurerId() {
        ActIdentity identity = getIdentity();
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Sets the claim identifier, issued by the insurer.
     * <p>
     * A claim can have a single identifier issued by an insurer. To avoid duplicates, each insurance service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.insuranceClaim</em> prefix.
     * @param id        the claim identifier
     * @throws InsuranceException if the identifier cannot be set
     */
    @Override
    public void setInsurerId(String archetype, String id) {
        state().insurerId(archetype, id)
                .update();
    }

    /**
     * Returns the date when the claim was created.
     *
     * @return the date
     */
    @Override
    public OffsetDateTime getCreated() {
        return DateRules.toOffsetDateTime(claim.getDate("startTime"));
    }

    /**
     * Returns the date when the claim was completed.
     * <p>
     * This represents the date when the claim was cancelled, settled, or declined.
     *
     * @return the date, or {@code null} if the claim hasn't been completed
     */
    @Override
    public OffsetDateTime getCompleted() {
        return DateRules.toOffsetDateTime(claim.getDate("endTime"));
    }

    /**
     * Returns the discount amount, including tax.
     *
     * @return the discount amount
     */
    @Override
    public BigDecimal getDiscount() {
        BigDecimal result = BigDecimal.ZERO;
        for (Condition condition : getConditions()) {
            result = result.add(condition.getDiscount());
        }
        return result;
    }

    /**
     * Returns the discount tax amount.
     *
     * @return the discount tax amount
     */
    @Override
    public BigDecimal getDiscountTax() {
        BigDecimal result = BigDecimal.ZERO;
        for (Condition condition : getConditions()) {
            result = result.add(condition.getDiscountTax());
        }
        return result;
    }

    /**
     * Returns the total amount being claimed, including tax.
     *
     * @return the total amount
     */
    @Override
    public BigDecimal getTotal() {
        return claim.getBigDecimal("amount", BigDecimal.ZERO);
    }

    /**
     * Returns the total tax amount.
     *
     * @return the tax amount
     */
    @Override
    public BigDecimal getTotalTax() {
        return claim.getBigDecimal("tax", BigDecimal.ZERO);
    }

    /**
     * Returns the total amount that has been allocated toward invoices in this claim, excluding amounts that
     * have been allocated by the payment of other claims.
     * <p/>
     * This can be used to determine deposits made on invoices prior to making a gap claim.
     * <p>
     * Specifically, this is the sum of {@link FinancialAct#getAllocatedAmount() allocated amounts} of the invoices
     * being claimed, minus any allocations which can be attributed to items in the invoices that have been claimed by
     * other claims. <br/>
     * The other claims must have a gap claim status of PAID or NOTIFIED, as this indicates that payments have been
     * allocated.
     *
     * @return the current paid amount
     */
    public BigDecimal getCurrentPaid() {
        return allocations.getConditionTotal();
    }

    /**
     * Returns the outstanding balance.
     *
     * @return the total amount outstanding on the claim
     */
    @Override
    public BigDecimal getCurrentBalance() {
        return getTotal().subtract(getCurrentPaid());
    }

    /**
     * Returns the allocation of payments to a condition.
     *
     * @param condition the condition
     * @return the allocation of payments to the condition
     */
    public BigDecimal getAllocation(ConditionImpl condition) {
        return allocations.getAllocation(condition);
    }

    /**
     * Returns the animal that the claim applies to.
     *
     * @return the animal
     */
    @Override
    public Patient getAnimal() {
        return getPolicy().getAnimal();
    }

    /**
     * Returns the policy that a claim is being made on.
     *
     * @return the policy
     */
    @Override
    public Policy getPolicy() {
        if (policy == null) {
            policy = new PolicyImpl(claim.getTarget("policy", Act.class), service, customerRules, patientRules,
                                    domainService);
        }
        return policy;
    }

    /**
     * Changes the policy for a claim. This can be used if a policy was submitted with an incorrect insurer or policy
     * number.
     *
     * @param insurer      the insurer
     * @param policyNumber the policy number
     * @return the updated policy
     */
    @Override
    public Policy setPolicy(Party insurer, String policyNumber) {
        state().policy(insurer, policyNumber)
                .update();
        return getPolicy();
    }

    /**
     * Returns the customer.
     *
     * @return the customer
     */
    public Party getCustomer() {
        return ((PolicyImpl) getPolicy()).getCustomer();
    }

    /**
     * Returns the patient.
     *
     * @return the patient
     */
    public Party getPatient() {
        return ((PolicyImpl) getPolicy()).getPatient();
    }

    /**
     * Returns the insurer.
     *
     * @return the insurer
     */
    public Party getInsurer() {
        return getPolicy().getInsurer();
    }

    /**
     * Returns the claim status.
     *
     * @return the claim status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(act.getStatus());
    }

    /**
     * Sets the claim status.
     *
     * @param status the claim status
     */
    @Override
    public void setStatus(Status status) {
        state().status(status)
                .update();
    }

    /**
     * Sets the claim status, along with any message from the insurer.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     */
    @Override
    public void setStatus(Status status, String message) {
        state().status(status, message)
                .update();
    }

    /**
     * Returns the conditions being claimed.
     *
     * @return the conditions being claimed
     */
    @Override
    public List<Condition> getConditions() {
        if (conditions == null) {
            conditions = collectConditions();
        }
        return conditions;
    }

    /**
     * Returns the clinical history for the patient.
     *
     * @return the clinical history
     */
    @Override
    public List<Note> getClinicalHistory() {
        if (history == null) {
            history = collectHistory();
        }
        return history;
    }

    /**
     * Returns the clinical history for the patient.
     * <p/>
     * This can return the full history for the patient, or if an insurer is linked to an insurance service, the
     * history from when a claim was last submitted. This can be used to send redundant history.
     *
     * @return the clinical history
     */
    @Override
    public History getHistory() {
        return new HistoryImpl(getPatient(), getInsurer(), act, insuranceRules, service);
    }

    /**
     * Returns the attachments.
     *
     * @return the attachments
     */
    @Override
    public List<Attachment> getAttachments() {
        if (attachments == null) {
            attachments = collectAttachments();
        }
        return attachments;
    }

    /**
     * Returns the user handling the claim.
     *
     * @return the clinician
     */
    @Override
    public User getClinician() {
        return claim.getTarget("clinician", User.class);
    }

    /**
     * Returns the claim handler.
     *
     * @return the claim handler
     */
    @Override
    public ClaimHandler getClaimHandler() {
        if (handler == null) {
            User user = claim.getTarget("user", User.class);
            if (user == null) {
                throw new IllegalStateException("Claim has no user");
            }
            Location location = getLocation();
            handler = new ClaimHandler() {
                @Override
                public String getName() {
                    return user.getName();
                }

                @Override
                public Phone getPhone() {
                    return location != null ? location.getPhone() : null;
                }

                @Override
                public Email getEmail() {
                    return location != null ? location.getEmail() : null;
                }
            };
        }
        return handler;
    }

    /**
     * Returns the location where the claim was created.
     *
     * @return the practice location
     */
    @Override
    public Location getLocation() {
        return new LocationImpl(getLocationParty(), customerRules, domainService);
    }

    /**
     * Returns the location, as a party.
     *
     * @return the location, as a party
     */
    public Party getLocationParty() {
        Party location = claim.getTarget("location", Party.class);
        if (location == null) {
            throw new IllegalStateException("Claim has no location");
        }
        return location;
    }

    /**
     * Sets a message on the claim. This may be used by insurance service to convey to users the status of the claim,
     * or why a claim was declined.
     *
     * @param message the message. May be {@code null}
     */
    @Override
    public void setMessage(String message) {
        state().message(message)
                .update();
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    @Override
    public String getMessage() {
        return claim.getString("message");
    }

    /**
     * Determines if this claim can be cancelled.
     *
     * @return {@code true} if the claim is {@link Status#PENDING}, {@link Status#POSTED}, {@link Status#SUBMITTED}
     * or {@link Status#ACCEPTED}.
     */
    @Override
    public boolean canCancel() {
        Status status = getStatus();
        return (status == Status.PENDING || status == Status.POSTED || status == Status.SUBMITTED
                || Status.ACCEPTED == status);
    }

    /**
     * Finalises the claim prior to submission.
     * <p>
     * The claim can only be finalised if it has {@link Status#PENDING PENDING} status, and all attachments have
     * content, and no attachments have {@link Attachment.Status#ERROR ERROR} status.
     *
     * @throws InsuranceException if the claim cannot be finalised
     */
    @Override
    public void finalise() {
        Status status = getStatus();
        if (status != Status.PENDING) {
            Lookup lookup = claim.getLookup("status");
            String displayName = (lookup != null) ? lookup.getName() : status.name();
            throw new InsuranceException(InsuranceMessages.cannotFinaliseClaimWithStatus(displayName));
        }
        for (Attachment attachment : getAttachments()) {
            if (attachment.getStatus() == Attachment.Status.ERROR) {
                throw new InsuranceException(InsuranceMessages.cannotFinaliseClaimAttachmentError(
                        attachment.getFileName()));
            }
            if (!attachment.hasContent()) {
                throw new InsuranceException(InsuranceMessages.cannotFinaliseClaimNoAttachment(
                        attachment.getFileName()));
            }
        }
        try {
            withTransaction(this::finaliseClaim);
        } catch (InsuranceException exception) {
            throw exception;
        } catch (Exception exception) {
            throw new InsuranceException(InsuranceMessages.failedToFinaliseClaim(exception.getMessage()), exception);
        }
    }

    /**
     * Performs an operation in a transaction.
     *
     * @param operation the operation
     */
    protected void withTransaction(Runnable operation) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                operation.run();
            }
        });
    }

    /**
     * Returns an updater to update the claim's state.
     * <p/>
     * This can be used to update multiple aspects of a claim in a single transaction.
     *
     * @return a new builder
     */
    @Override
    public ClaimUpdater state() {
        return new ClaimUpdaterImpl(this, claim, service, insuranceRules);
    }

    /**
     * Reloads the claim.
     *
     * @return the latest instance of the claim
     */
    public ClaimImpl reload() {
        Act object = service.get(act.getObjectReference(), Act.class);
        if (object == null) {
            throw new IllegalStateException("Cannot reload claim=" + act.getObjectReference()
                                            + ". It has been deleted");
        }
        return newInstance(object, service, insuranceRules, customerRules, patientRules, handlers, transactionManager,
                           domainService);
    }

    /**
     * Returns all invoices associated with this claim.
     * <p>
     * NOTE: the results of this call are not cached.
     *
     * @return the invoices
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public List<FinancialAct> getInvoices() {
        Map<Object, IMObject> invoices = new HashMap<>();
        IMObjectCache cache = new MapIMObjectCache(invoices, service);
        for (Condition condition : getConditions()) {
            for (Invoice invoice : condition.getInvoices()) {
                Reference reference = new IMObjectReference(CustomerAccountArchetypes.INVOICE, invoice.getId());
                if (cache.get(reference) == null) {
                    throw new IllegalStateException("Failed to retrieve invoice=" + reference
                                                    + " associated with claim=" + getId());
                }
            }
        }
        return new ArrayList(invoices.values());
    }

    /**
     * Returns the total amount that has been allocated toward invoices in this claim, excluding amounts that
     * have been allocated by the payment of other claims.
     * <p/>
     * This can be used to determine deposits made on invoices prior to making a gap claim in order to refund
     * overpayments.
     * <p>
     * Specifically, this is the sum of payments of the invoices being claimed, minus any payments which can be
     * attributed to items in the invoices that have been claimed by other claims. <br/>
     * The other claims must be pre-paid or have a gap claim status of PAID or NOTIFIED, as this indicates that payments
     * have been allocated.
     * <p/>
     * NOTE: this may be different to {@link #getCurrentPaid()}, which excludes payments greater than that being
     * claimed.
     *
     * @return the allocated amount
     */
    public BigDecimal getInvoiceAllocation() {
        return allocations.getInvoiceTotal();
    }

    /**
     * Returns the policy implementation.
     *
     * @return the policy implementation. May be {@code null}
     */
    protected PolicyImpl getPolicyImpl() {
        getPolicy();
        return policy;
    }

    /**
     * Updates the cached copy of the policy.
     * <p/>
     * This is required if {@link ClaimUpdaterImpl} changes the policy.
     *
     * @param policy the new policy
     */
    protected void updatePolicy(Act policy) {
        this.policy = new PolicyImpl(policy, service, customerRules, patientRules, domainService);
    }

    /**
     * Returns the cache.
     *
     * @return the cache
     */
    protected IMObjectCache getCache() {
        return cache;
    }

    /**
     * Finalises the claim.
     */
    protected void finaliseClaim() {
        setStatus(Status.POSTED);
        for (Attachment attachment : getAttachments()) {
            if (attachment.getStatus() == Attachment.Status.PENDING) {
                attachment.setStatus(Attachment.Status.POSTED);
            }
        }
    }

    /**
     * Creates a new instance of the claim.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     * @return a new instance
     */
    protected ClaimImpl newInstance(Act claim, IArchetypeService service, InsuranceRules insuranceRules,
                                    CustomerRules customerRules, PatientRules patientRules, DocumentHandlers handlers,
                                    PlatformTransactionManager transactionManager, DomainService domainService) {
        return new ClaimImpl(claim, service, insuranceRules, customerRules, patientRules, handlers, transactionManager,
                             domainService);
    }

    /**
     * Returns the claim.
     *
     * @return the claim
     */
    protected IMObjectBean getClaim() {
        return claim;
    }

    /**
     * Collects the list of claim conditions.
     *
     * @return the claim conditions
     */
    protected List<Condition> collectConditions() {
        List<Condition> result = new ArrayList<>();
        Party patient = getPatient();
        for (Act act : claim.getTargets("items", Act.class)) {
            result.add(new ConditionImpl(act, patient, this, service));
        }
        return result;
    }

    /**
     * Collects the clinical history up to the time of the claim.
     *
     * @return the clinical history
     */
    protected List<Note> collectHistory() {
        Iterable<Note> notes = getHistory().getNotes();
        return IterableUtils.toList(notes);
    }

    /**
     * Collects attachments.
     *
     * @return the attachments
     */
    protected List<Attachment> collectAttachments() {
        List<Attachment> result = new ArrayList<>();
        for (DocumentAct act : claim.getTargets("attachments", DocumentAct.class)) {
            result.add(new AttachmentImpl(act, service, handlers));
        }
        return result;
    }

    /**
     * Returns the claim identity, as specified by the insurance provider.
     *
     * @return the claim identity, or {@code null} if none is registered
     */
    protected ActIdentity getIdentity() {
        return claim.getObject("insurerId", ActIdentity.class);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Returns the insurance rules.
     *
     * @return the insurance rules
     */
    protected InsuranceRules getInsuranceRules() {
        return insuranceRules;
    }

    /**
     * Resets any allocations.
     * <p/>
     * This is required after making changes to invoices associated with the claim.
     */
    protected void resetAllocations() {
        cache.clear(); // need to clear the cache to pick up invoice changes
        allocations.clear();
    }
}
