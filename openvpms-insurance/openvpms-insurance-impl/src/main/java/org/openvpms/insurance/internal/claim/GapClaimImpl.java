/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.lang.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.credit.CreditActAllocator;
import org.openvpms.archetype.rules.finance.credit.CreditAllocation;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.insurance.claim.Deposit;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.claim.GapClaimUpdater;
import org.openvpms.insurance.internal.i18n.InsuranceMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Default implementation of {@link GapClaim}.
 *
 * @author Tim Anderson
 */
public class GapClaimImpl extends ClaimImpl implements GapClaim {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The deposits.
     */
    private List<Deposit> deposits;

    /**
     * Gap claim node name.
     */
    static final String GAP_CLAIM = "gapClaim";

    /**
     * Gap status node name.
     */
    static final String GAP_STATUS = "status2";

    /**
     * Paid amount node name.
     */
    static final String PAID = "paid";

    /**
     * Benefit amount node name.
     */
    static final String BENEFIT_AMOUNT = "benefitAmount";

    /**
     * Vet benefit amount node name.
     */
    static final String VET_BENEFIT_AMOUNT = "vetBenefitAmount";

    /**
     * Benefit notes node name.
     */
    static final String BENEFIT_NOTES = "benefitNotes";

    /**
     * The deposits node name.
     */
    static final String DEPOSITS = "deposits";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(GapClaimImpl.class);

    /**
     * Default gap benefit payment type.
     */
    public static final String GAP_BENEFIT_PAYMENT = "GAP_BENEFIT_PAYMENT";

    /**
     * Constructs a {@link GapClaimImpl}.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param accountRules       the customer account rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public GapClaimImpl(FinancialAct claim, IArchetypeService service, InsuranceRules insuranceRules,
                        CustomerRules customerRules, CustomerAccountRules accountRules, PatientRules patientRules,
                        DocumentHandlers handlers, PlatformTransactionManager transactionManager,
                        DomainService domainService) {
        this(service.getBean(claim), service, insuranceRules, customerRules, accountRules, patientRules, handlers,
             transactionManager, domainService);
    }

    /**
     * Constructs a {@link GapClaimImpl}.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param accountRules       the customer account rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public GapClaimImpl(IMObjectBean claim, IArchetypeService service, InsuranceRules insuranceRules,
                        CustomerRules customerRules, CustomerAccountRules accountRules, PatientRules patientRules,
                        DocumentHandlers handlers, PlatformTransactionManager transactionManager,
                        DomainService domainService) {
        super(claim, service, insuranceRules, customerRules, patientRules, handlers, transactionManager, domainService);
        this.accountRules = accountRules;
    }

    /**
     * Returns the benefit amount.
     *
     * @return the benefit amount
     */
    @Override
    public BigDecimal getBenefitAmount() {
        return getClaim().getBigDecimal(BENEFIT_AMOUNT, BigDecimal.ZERO);
    }

    /**
     * Returns the benefit amount paid to the vet.
     *
     * @return the vet benefit amount
     */
    @Override
    public BigDecimal getVetBenefitAmount() {
        return getClaim().getBigDecimal(VET_BENEFIT_AMOUNT, BigDecimal.ZERO);
    }

    /**
     * Updates the gap claim with the vet benefit.
     * <p/>
     * This is the benefit amount minus any fees and taxes.
     *
     * @param amount the vet benefit amount
     */
    @Override
    public void setVetBenefitAmount(BigDecimal amount) {
        state().vetBenefitAmount(amount)
                .update();
    }

    /**
     * Returns the amount that the customer has paid towards the claim.
     *
     * @return the amount the customer has paid
     */
    @Override
    public BigDecimal getPaid() {
        return getClaim().getBigDecimal(PAID, BigDecimal.ZERO);
    }

    /**
     * Records the invoice(s) associated with a claim as being fully paid.
     * <p/>
     * These can be paid before any benefit amount is received.
     */
    public void fullyPaid() {
        GapStatus status = getGapStatus();
        if (status == GapStatus.PAID || status == GapStatus.NOTIFIED) {
            throw new IllegalStateException("Cannot update paid status for gap claim with status=" + status);
        }
        IMObjectBean claim = getClaim();
        claim.setValue(GAP_STATUS, GapStatus.PAID);
        claim.setValue(PAID, getTotal());
        claim.save();
        resetAllocations();
    }

    /**
     * Records the gap amount as being paid.
     * <p>
     * This updates the gap status to {@link GapStatus#PAID}.
     * <p>
     * For non-zero benefit amounts, it creates a payment of that amount to adjust the customer balance.
     * This has a single Other line item, and Payment Type 'Gap Payment' which will be allocated against the claim
     * invoices, where possible.
     *
     * @param till     the payment till. Required for a non-zero benefit amount
     * @param location the practice location
     * @return the payment to adjust the customer balance, or {@code null} if none was created
     */
    public FinancialAct gapPaid(Entity till, Party location) {
        List<FinancialAct> adjustment = null;
        GapStatus status = getGapStatus();
        if (status != GapStatus.RECEIVED) {
            throw new IllegalStateException("Cannot update paid status for gap claim with status=" + status);
        }
        Party customer = getCustomer();
        BigDecimal benefitAmount = getBenefitAmount();
        BigDecimal gapAmount = getGapAmount();
        IMObjectBean claim = getClaim();
        claim.setValue(GAP_STATUS, GapStatus.PAID);
        claim.setValue(PAID, gapAmount);
        if (!MathRules.isZero(benefitAmount)) {
            if (till == null) {
                throw new IllegalArgumentException("Argument 'till' is required for a non-zero benefit amount");
            }
            String notes = getBenefitPaymentNotes();
            if (notes != null) {
                int maxLength = claim.getMaxLength("notes");
                if (notes.length() > maxLength) {
                    notes = StringUtils.abbreviate(notes, maxLength);
                }
            }
            String paymentType = getPaymentType();
            adjustment = new ArrayList<>(accountRules.createPaymentOther(customer, benefitAmount, till, location,
                                                                         paymentType, notes));
            IArchetypeService service = getService();
            CreditActAllocator allocator = new CreditActAllocator(service, getInsuranceRules());
            List<FinancialAct> invoices = getInvoices();
            Set<FinancialAct> toSave = new LinkedHashSet<>();
            toSave.add(claim.getObject(FinancialAct.class));
            CreditAllocation allocation = allocator.allocate(adjustment.get(0), invoices, false);
            toSave.addAll(adjustment);
            if (allocation.isModified()) {
                toSave.addAll(allocation.getModified());
            }
            service.save(toSave);
        } else {
            claim.save();
        }
        resetAllocations();
        return adjustment != null ? adjustment.get(0) : null;
    }

    /**
     * Returns the payment type to use, when creating a gap benefit payment.
     * <p/>
     * This uses the payment type associated with the insurer, if there is one. If not, defaults to
     * {@link #GAP_BENEFIT_PAYMENT}.
     *
     * @return the payment type
     */
    protected String getPaymentType() {
        Party insurer = getInsurer();
        IMObjectBean bean = getService().getBean(insurer);
        Lookup paymentType = bean.getObject("paymentType", Lookup.class);
        return paymentType != null ? paymentType.getCode() : GAP_BENEFIT_PAYMENT;
    }

    /**
     * Returns the gap amount. This is the difference between the claim total and the benefit amount.
     *
     * @return the gap amount
     */
    @Override
    public BigDecimal getGapAmount() {
        BigDecimal total = getTotal();
        BigDecimal benefit = getBenefitAmount();
        return getInsuranceRules().getGapAmount(total, benefit);
    }

    /**
     * Returns the notes associated with the benefit amount.
     *
     * @return the notes associated with the benefit amount. May be {@code null}
     */
    @Override
    public String getBenefitNotes() {
        return getClaim().getString(BENEFIT_NOTES);
    }

    /**
     * Returns the gap claim status.
     *
     * @return the status, or {@code null} if this is not a gap claim
     */
    @Override
    public GapStatus getGapStatus() {
        GapStatus result = null;
        IMObjectBean claim = getClaim();

        String status;
        if (claim.getBoolean(GAP_CLAIM)) {
            status = claim.getString(GAP_STATUS);
            result = (status != null) ? GapStatus.valueOf(status) : GapStatus.PENDING;
        }
        return result;
    }

    /**
     * Updates the gap claim with the benefit.
     * <p>
     * This sets the benefit status to {@link GapStatus#RECEIVED}.
     *
     * @param amount the benefit amount
     * @param notes  notes associated with the benefit amount
     */
    @Override
    public void setBenefit(BigDecimal amount, String notes) {
        state().benefit(amount, notes)
                .update();
    }

    /**
     * Updates the {@link GapStatus} to {@link GapStatus#NOTIFIED}.
     * <p>
     * This is only valid when the gap status is {@link GapStatus#PAID}.
     */
    @Override
    public void paymentNotified() {
        GapStatus status = getGapStatus();
        if (status != GapStatus.PAID) {
            throw new IllegalStateException("Cannot update gap status to NOTIFIED when gap status=" + status);
        }

        withTransaction(() -> {
            IMObjectBean claim = getClaim();
            claim.setValue(GAP_STATUS, GapStatus.NOTIFIED.toString());
            claim.save();
            if (getStatus() == Status.PRE_SETTLED) {
                state().status(Status.SETTLED)
                        .update();
            }
        });
    }

    /**
     * Returns the deposits where the insurer has paid the vet for the claim.
     *
     * @return the deposits
     */
    @Override
    public List<Deposit> getDeposits() {
        if (deposits == null) {
            deposits = new ArrayList<>();
            for (Act act : getClaim().getTargets(DEPOSITS, Act.class)) {
                deposits.add(new DepositImpl(act, getService()));
            }
        }
        return deposits;
    }

    /**
     * Returns an updater to update the claim's state.
     * <p/>
     * This can be used to update multiple aspects of a claim in a single transaction.
     *
     * @return a new builder
     */
    @Override
    public GapClaimUpdater state() {
        return new GapClaimUpdaterImpl(this, getClaim(), getService(), getInsuranceRules());
    }

    /**
     * Reloads the claim.
     *
     * @return the latest instance of the claim
     */
    @Override
    public GapClaimImpl reload() {
        return (GapClaimImpl) super.reload();
    }

    /**
     * Finalises the claim.
     */
    @Override
    protected void finaliseClaim() {
        super.finaliseClaim();
        IMObjectBean claim = getClaim();
        claim.setValue(GAP_STATUS, GapStatus.PENDING.toString());
    }

    /**
     * Creates a new instance of the claim.
     *
     * @param claim              the claim
     * @param service            the archetype service
     * @param insuranceRules     the insurance rules
     * @param customerRules      the customer rules
     * @param patientRules       the patient rules
     * @param handlers           the document handlers
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     * @return a new instance
     */
    @Override
    protected ClaimImpl newInstance(Act claim, IArchetypeService service, InsuranceRules insuranceRules,
                                    CustomerRules customerRules, PatientRules patientRules, DocumentHandlers handlers,
                                    PlatformTransactionManager transactionManager, DomainService domainService) {
        return new GapClaimImpl(service.getBean(claim), service, insuranceRules, customerRules, accountRules,
                                patientRules, handlers, transactionManager, domainService);
    }

    /**
     * Updates the cached copy of the deposits.
     * <p/>
     * This is required if {@link GapClaimUpdaterImpl} changes the deposits.
     *
     * @param deposits the deposits
     */
    protected void updateDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    /**
     * Returns the  benefit payment notes.
     *
     * @return the payment benefit notes
     */
    private String getBenefitPaymentNotes() {
        try {
            return InsuranceMessages.benefitPaymentNotes(getInsurerId(), getPatient().getName(),
                                                         getInsurer().getName()).getMessage();
        } catch (Throwable exception) {
            log.error("Failed to generate benefit payment notes", exception);
            return null;
        }
    }
}
