/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.component.business.domain.im.act.ActIdentity;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.Claim.Status;
import org.openvpms.insurance.claim.ClaimUpdater;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.exception.InsuranceException;
import org.openvpms.insurance.internal.i18n.InsuranceMessages;
import org.openvpms.insurance.internal.policy.PolicyImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * The default implementation of {@link ClaimUpdater}.
 *
 * @author Tim Anderson
 */
class ClaimUpdaterImpl implements ClaimUpdater {

    /**
     * The claim.
     */
    private final ClaimImpl claim;

    /**
     * The bean wrapping the claim act.
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The insurance rules.
     */
    private final InsuranceRules rules;

    /**
     * The claim the identifier archetype. Must have an <em>actIdentity.insuranceClaim</em> prefix.
     */
    private String archetype;

    /**
     * The claim identifier.
     */
    private String id;

    /**
     * The insurer.
     */
    private Party insurer;

    /**
     * The policy number.
     */
    private String policyNumber;

    /**
     * The claim status.
     */
    private Status status;

    /**
     * The status message.
     */
    private String message;

    /**
     * Determines if the message has been set.
     */
    private boolean hasMessage;

    /**
     * Constructs a {@link ClaimUpdaterImpl}.
     *
     * @param claim   the claim
     * @param bean    the bean wrapping the claim act
     * @param service the archetype service
     * @param rules   the insurance rules
     */
    public ClaimUpdaterImpl(ClaimImpl claim, IMObjectBean bean, ArchetypeService service, InsuranceRules rules) {
        this.claim = claim;
        this.bean = bean;
        this.service = service;
        this.rules = rules;
    }

    /**
     * Sets the claim identifier, issued by the insurer.
     * <p>
     * A claim can have a single identifier issued by an insurer. To avoid duplicates, each insurance service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.insuranceClaim</em> prefix.
     * @param id        the claim identifier
     * @return this
     */
    @Override
    public ClaimUpdater insurerId(String archetype, String id) {
        if (!TypeHelper.isA(archetype, InsuranceArchetypes.CLAIM_IDENTITIES)) {
            throw new IllegalArgumentException("Argument 'archetype' must have a "
                                               + InsuranceArchetypes.CLAIM_IDENTITIES + " prefix");
        }
        if (id == null) {
            throw new IllegalArgumentException("Argument 'id' is required");
        }
        this.archetype = archetype;
        this.id = id;
        return this;
    }

    /**
     * Changes the policy for a claim. This can be used if a policy was submitted with an incorrect insurer or policy
     * number.
     *
     * @param insurer      the insurer
     * @param policyNumber the policy number
     * @return this
     */
    @Override
    public ClaimUpdater policy(Party insurer, String policyNumber) {
        if (insurer == null) {
            throw new IllegalArgumentException("Argument 'insurer' is null");
        }
        if (policyNumber == null) {
            throw new IllegalArgumentException("Argument 'policyNumber' is null");
        }
        this.insurer = insurer;
        this.policyNumber = policyNumber;
        return this;
    }

    /**
     * Sets the claim status.
     *
     * @param status the claim status
     * @return this
     */
    @Override
    public ClaimUpdater status(Status status) {
        this.status = status;
        return this;
    }

    /**
     * Sets the claim status, along with any message from the insurer.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     */
    @Override
    public ClaimUpdater status(Status status, String message) {
        return status(status).message(message);
    }

    /**
     * Sets a message on the claim. This may be used by insurance service to convey to users the status of the claim,
     * or why a claim was declined.
     *
     * @param message the message. May be {@code null}
     */
    @Override
    public ClaimUpdater message(String message) {
        this.message = message;
        hasMessage = true;
        return this;
    }

    /**
     * Updates the claim.
     *
     * @return the claim
     * @throws InsuranceException if the claim cannot be updated
     */
    @Override
    public Claim update() {
        List<IMObject> toSave = new ArrayList<>();
        boolean updated = doUpdate(bean, toSave);
        if (updated) {
            bean.save(toSave);
        }
        return claim;
    }

    /**
     * Returns the claim.
     *
     * @return the claim
     */
    protected ClaimImpl getClaim() {
        return claim;
    }

    /**
     * Returns the claim bean.
     *
     * @return the claim bean
     */
    protected IMObjectBean getBean() {
        return bean;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Updates the claim.
     *
     * @param bean   the claim bean
     * @param toSave any related objects to save if the claim has been updated
     * @return {@code true} if the claim was updated, otherwise {@code false}
     */
    protected boolean doUpdate(IMObjectBean bean, List<IMObject> toSave) {
        boolean updated = updateId();
        updated |= updatePolicy(toSave);
        updated |= updateStatus();
        updated |= updateMessage();
        return updated;
    }

    /**
     * Updates the claim identifier.
     *
     * @return {@code true} if the claim identifier was updated
     */
    private boolean updateId() {
        boolean updated = false;
        if (archetype != null && id != null) {
            ActIdentity identity = claim.getIdentity();
            if (identity == null) {
                identity = service.create(archetype, ActIdentity.class);
                bean.addValue("insurerId", identity);
                updated = true;
            } else if (!identity.isA(archetype)) {
                throw new InsuranceException(InsuranceMessages.differentClaimIdentifierArchetype(
                        identity.getArchetype(), archetype));
            }
            if (!Objects.equals(id, identity.getIdentity())) {
                identity.setIdentity(id);
                updated = true;
            }
        }
        return updated;
    }

    /**
     * Updates the policy, if one has been set.
     *
     * @param toSave the collection of objects to save on commit
     * @return {@code true} if the policy was updated
     */
    private boolean updatePolicy(List<IMObject> toSave) {
        boolean updated = false;
        if (insurer != null && policyNumber != null) {
            PolicyImpl policy = claim.getPolicyImpl();
            if (!policy.getInsurer().equals(insurer) || !Objects.equals(policy.getPolicyNumber(), policyNumber)) {
                Act act = rules.getPolicyForClaim(claim.getCustomer(), claim.getPatient(), insurer,
                                                  policyNumber, (FinancialAct) bean.getObject(),
                                                  policy.getAct());
                Relationship relationship = bean.setTarget("policy", act);
                act.addActRelationship((ActRelationship) relationship);
                claim.updatePolicy(act); // need to propagate back to the claim as it is cached
                toSave.add(act);
                updated = true;
            }
        }
        return updated;
    }

    /**
     * Updates the claim status. If the status represents a terminal state (CANCELLED, ACCEPTED, DECLINED), the
     * end date will be updated with the current time, otherwise it will be cleared.
     *
     * @return {@code true} if the status was updated
     */
    private boolean updateStatus() {
        boolean updated = false;
        if (status != null) {
            Status current = claim.getStatus();
            if (current == Status.CANCELLED || current == Status.SETTLED || current == Status.DECLINED) {
                throw new IllegalStateException("Cannot update claim status when it is " + current);
            }
            if (status == Status.PRE_SETTLED && !(claim instanceof GapClaim)) {
                throw new IllegalStateException("PRE_SETTLED is not a valid status for standard claims");
            }
            bean.setValue("status", status.name());
            if (status == Status.CANCELLED || status == Status.SETTLED || status == Status.DECLINED) {
                bean.setValue("endTime", new Date());
            } else {
                bean.setValue("endTime", null);
            }
            updated = true;
        }
        return updated;
    }

    /**
     * Updates the message.
     *
     * @return {@code true} if the message was updated
     */
    private boolean updateMessage() {
        boolean updated = false;
        if (hasMessage) {
            if (!StringUtils.isEmpty(message)) {
                message = StringUtils.abbreviate(message, bean.getMaxLength("message"));
            }
            if (!Objects.equals(message, bean.getString("message"))) {
                bean.setValue("message", message);
                updated = true;

            }
        }
        return updated;
    }
}
