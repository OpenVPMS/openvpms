/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.History;
import org.openvpms.insurance.claim.Note;

import java.util.Date;

/**
 * Default implementation of {@link History}.
 *
 * @author Tim Anderson
 */
public class HistoryImpl implements History {

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * The insurer.
     */
    private final Party insurer;

    /**
     * The claim.
     */
    private final Act claim;

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The query.
     */
    private final NotesQuery query;

    /**
     * The maximum no. of notes to retrieve at a time.
     */
    private static final int PAGE_SIZE = 100;

    /**
     * Constructs a {@link HistoryImpl}.
     *
     * @param patient        the patient
     * @param insurer        the insurer
     * @param claim          the claim
     * @param insuranceRules the insurance rules
     * @param service        the archetype service
     */
    public HistoryImpl(Party patient, Party insurer, Act claim, InsuranceRules insuranceRules,
                       ArchetypeService service) {
        this.patient = patient;
        this.insurer = insurer;
        this.claim = claim;
        this.insuranceRules = insuranceRules;
        this.query = new NotesQuery(service);
    }

    /**
     * Returns the clinical notes for the patient, up until the time of the claim.
     *
     * @return the clinical notes
     */
    @Override
    public Iterable<Note> getNotes() {
        return query.query(patient, null, claim.getActivityStartTime(), PAGE_SIZE);
    }

    /**
     * Returns the clinical notes since the last claim.
     * <p/>
     * This applies to insurers that have a common insurance service. It returns notes that have not been submitted
     * to the insurance service before, or were part of a cancelled claim. The objective is to reduce redundant
     * history submissions.
     *
     * @return the clinical notes
     */
    @Override
    public Iterable<Note> getNotesSinceLastClaim() {
        Date from = insuranceRules.getMostRecentTreatmentDateForService(patient, insurer, claim);
        return query.query(patient, from, claim.getActivityStartTime(), PAGE_SIZE);
    }
}