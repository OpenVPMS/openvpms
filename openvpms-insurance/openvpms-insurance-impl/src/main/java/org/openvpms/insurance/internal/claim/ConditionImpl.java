/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.collections4.IterableUtils;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.insurance.claim.Condition;
import org.openvpms.insurance.claim.Invoice;
import org.openvpms.insurance.claim.Item;
import org.openvpms.insurance.claim.Note;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of the {@link Condition} interface.
 *
 * @author Tim Anderson
 */
public class ConditionImpl implements Condition {

    /**
     * The condition.
     */
    private final IMObjectBean condition;

    /**
     * The underlying act.
     */
    private final Act act;

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The parent claim.
     */
    private final ClaimImpl claim;

    /**
     * The invoices being claimed for the condition.
     */
    private List<InvoiceImpl> invoices;

    /**
     * The consultation notes.
     */
    private List<Note> notes;

    /**
     * Constructs a {@link ConditionImpl}
     *
     * @param act     the condition act
     * @param patient the patient
     * @param claim   the claim
     * @param service the archetype service
     */
    public ConditionImpl(Act act, Party patient, ClaimImpl claim, ArchetypeService service) {
        this.condition = service.getBean(act);
        this.patient = patient;
        this.act = act;
        this.claim = claim;
        this.service = service;
    }

    /**
     * Returns the condition identifier.
     *
     * @return the condition identifier
     */
    public long getId() {
        return act.getId();
    }

    /**
     * The date when treatment for the condition was started.
     *
     * @return date when treatment for the condition was started
     */
    @Override
    public OffsetDateTime getTreatedFrom() {
        return DateRules.toOffsetDateTime(act.getActivityStartTime());
    }

    /**
     * The date when treatment for the condition was ended.
     *
     * @return date when treatment for the condition was ended
     */
    @Override
    public OffsetDateTime getTreatedTo() {
        return DateRules.toOffsetDateTime(act.getActivityEndTime());
    }

    /**
     * Returns the diagnosis.
     *
     * @return the diagnosis
     */
    @Override
    public Lookup getDiagnosis() {
        return condition.getLookup("reason");
    }

    /**
     * Returns the condition description.
     * <p>
     * This can provide a short summary of the condition.
     *
     * @return the condition description. May be {@code null}
     */
    @Override
    public String getDescription() {
        return condition.getObject().getDescription();
    }

    /**
     * Returns the status of the animal as a result of this condition.
     *
     * @return the status of the animal
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(act.getStatus());
    }

    /**
     * Returns the reason for euthanasing the animal, if {@link #getStatus()} is {@code EUTHANASED}.
     *
     * @return the reason for euthanasing the animal
     */
    @Override
    public String getEuthanasiaReason() {
        return condition.getString("euthanasiaReason");
    }

    /**
     * Returns the consultation notes.
     *
     * @return the consultation notes
     */
    @Override
    public List<Note> getConsultationNotes() {
        if (notes == null) {
            Date from = DateRules.getDate(act.getActivityStartTime());
            Date to = DateRules.getNextDate(act.getActivityEndTime());
            NotesQuery query = new NotesQuery(service);
            notes = IterableUtils.toList(query.query(patient, from, to, 100));
        }
        return notes;
    }

    /**
     * Returns the discount amount, including tax.
     *
     * @return the discount amount
     */
    @Override
    public BigDecimal getDiscount() {
        BigDecimal discount = BigDecimal.ZERO;
        for (Invoice invoice : getInvoices()) {
            discount = discount.add(invoice.getDiscount());
        }
        return discount;
    }

    /**
     * Returns the discount tax amount.
     *
     * @return the discount tax amount
     */
    @Override
    public BigDecimal getDiscountTax() {
        BigDecimal tax = BigDecimal.ZERO;
        for (Invoice invoice : getInvoices()) {
            tax = tax.add(invoice.getDiscountTax());
        }
        return tax;
    }

    /**
     * Returns the total amount, including tax.
     *
     * @return the total amount
     */
    @Override
    public BigDecimal getTotal() {
        BigDecimal total = BigDecimal.ZERO;
        for (Invoice invoice : getInvoices()) {
            total = total.add(invoice.getTotal());
        }
        return total;
    }

    /**
     * Returns the total tax amount.
     *
     * @return the tax amount
     */
    @Override
    public BigDecimal getTotalTax() {
        BigDecimal tax = BigDecimal.ZERO;
        for (Invoice invoice : getInvoices()) {
            tax = tax.add(invoice.getTotalTax());
        }
        return tax;
    }

    /**
     * Returns the total amount that has been paid toward invoices being claimed for the condition, excluding amounts
     * that have been allocated by the payment of other claims.
     * <p/>
     * Specifically, this is the sum of allocated amounts of the invoices being claimed, minus any allocations which
     * can be attributed to items in the invoices that have been claimed by other claims. <br/>
     * The other claims must be pre-paid claims or gap claims with status of PAID or NOTIFIED, as this indicates
     * that payments have been allocated.
     *
     * @return the current paid amount
     */
    @Override
    public BigDecimal getCurrentPaid() {
        return claim.getAllocation(this);
    }

    /**
     * Returns the outstanding balance for invoices being claimed for the condition.
     * <p/>
     * This is the condition total - the current paid amount.
     *
     * @return the total amount outstanding
     */
    @Override
    public BigDecimal getCurrentBalance() {
        return getTotal().subtract(getCurrentPaid());
    }

    /**
     * Returns the invoices being claimed.
     *
     * @return the invoices being claimed
     */
    @Override
    public List<Invoice> getInvoices() {
        List<InvoiceImpl> invoices = getInvoiceImpls();
        return new ArrayList<>(invoices);
    }

    /**
     * Returns the invoices being claimed.
     *
     * @return the invoices being claimed
     */
    public List<InvoiceImpl> getInvoiceImpls() {
        if (invoices == null) {
            invoices = collectInvoices();
        }
        return invoices;
    }

    /**
     * Collects the invoices.
     *
     * @return the invoices
     */
    private List<InvoiceImpl> collectInvoices() {
        List<InvoiceImpl> result = new ArrayList<>();

        Map<FinancialAct, List<FinancialAct>> invoices = new HashMap<>();
        // map of invoices to their items claimed by this condition

        IMObjectCache cache = claim.getCache();
        // access items and invoices through the cache to avoid redundant retrieval

        for (Reference itemRef : condition.getTargetRefs("items")) {
            // for each claimed invoice item...
            FinancialAct item = (FinancialAct) cache.get(itemRef);
            if (item == null) {
                throw new IllegalStateException("Invoice item=" + itemRef + " not found");
            }

            // get the corresponding invoice...
            IMObjectBean bean = service.getBean(item);
            Reference ref = bean.getSourceRef("invoice");
            FinancialAct invoice = (FinancialAct) cache.get(ref);
            if (invoice == null) {
                throw new IllegalStateException("Invoice item=" + item.getObjectReference() + " has no invoice");
            }
            List<FinancialAct> items = invoices.computeIfAbsent(invoice, k -> new ArrayList<>());
            items.add(item);
        }

        // build the corresponding InvoiceImpls for each collected invoice and their items
        for (Map.Entry<FinancialAct, List<FinancialAct>> entry : invoices.entrySet()) {
            FinancialAct invoice = entry.getKey();
            List<Item> items = new ArrayList<>();
            for (FinancialAct item : entry.getValue()) {
                items.add(new ItemImpl(item, service));
            }
            items.sort(Comparator.comparing(Item::getDate));
            result.add(new InvoiceImpl(invoice, items));
        }
        result.sort(Comparator.comparing(Invoice::getDate));
        return result;
    }

}
