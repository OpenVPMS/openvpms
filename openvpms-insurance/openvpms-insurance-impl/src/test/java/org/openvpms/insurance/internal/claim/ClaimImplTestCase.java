/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.collections4.IterableUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.insurance.ClaimStatus;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceTestHelper;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.functor.ActComparator;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.patient.Patient;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.ClaimHandler;
import org.openvpms.insurance.claim.Condition;
import org.openvpms.insurance.claim.Invoice;
import org.openvpms.insurance.claim.Item;
import org.openvpms.insurance.claim.Note;
import org.openvpms.insurance.policy.Policy;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link ClaimImpl} class.
 *
 * @author Tim Anderson
 */
public class ClaimImplTestCase extends AbstractClaimTest {

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Tests the {@link ClaimImpl} methods.
     */
    @Test
    public void testClaim() {
        // set up a user for the createdBy nodes
        User author = userFactory.createUser();
        AuthenticationContext context = new AuthenticationContextImpl();
        context.setUser(author);

        String treatFrom1 = "2017-09-27";
        String treatTo1 = "2017-09-29";

        // patient history
        DocumentAct note1 = createNote("2015-05-01", patient, clinician, "Note 1");
        DocumentAct note2 = createNote("2015-05-02", patient, clinician, "Note 2");
        createVisit("2015-05-01", "2015-05-02", patient, note1, note2);
        DocumentAct note3 = createNote("2015-07-01", patient, clinician, "Note 3");
        DocumentAct addendum1 = createAddendum("2015-07-03", patient, clinician, "Note 3 addendum 1");
        DocumentAct addendum2 = createAddendum("2015-07-04", patient, clinician, "Note 3 addendum 2");
        patientFactory.updateNote(note3)
                .addAddenda(addendum1, addendum2)
                .build();
        createVisit("2015-07-01", "2015-07-02", patient, note3);

        Act note4 = createNote(treatFrom1, patient, clinician, "Note 4");
        createVisit(treatFrom1, treatTo1, patient, clinician, note4);

        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();
        Product product3 = productFactory.createMedication();
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        BigDecimal discount1 = new BigDecimal("0.10");
        BigDecimal discountTax1 = new BigDecimal("0.01");
        BigDecimal tax1 = new BigDecimal("0.08");
        BigDecimal total1 = new BigDecimal("0.90");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1, product1, ONE, ONE, discount1, tax1);
        Date itemDate2 = getDatetime("2017-09-27 11:00:00");
        BigDecimal discount2 = ZERO;
        BigDecimal discountTax2 = ZERO;
        BigDecimal tax2 = new BigDecimal("0.91");
        BigDecimal total2 = BigDecimal.TEN;
        FinancialAct invoiceItem2 = createInvoiceItem(itemDate2, product2, ONE, TEN, discount2, tax2);
        FinancialAct invoice1Act = createInvoice(getDate("2017-09-27"), invoiceItem1, invoiceItem2);

        Date itemDate3 = getDatetime("2017-09-28 15:00:00");
        BigDecimal discount3 = ZERO;
        BigDecimal discountTax3 = ZERO;
        BigDecimal tax3 = new BigDecimal("0.91");
        BigDecimal total3 = BigDecimal.TEN;
        FinancialAct invoiceItem3 = createInvoiceItem(itemDate3, product3, new BigDecimal("2"), new BigDecimal("5"),
                                                      discount3, tax3);
        FinancialAct invoice2Act = createInvoice(getDate("2017-09-28"), invoiceItem3);

        // pay the invoices
        BigDecimal toPay = invoice1Act.getTotal().add(invoice2Act.getTotal());
        makePayment(toPay);

        context.setUser(user);

        Act claimAct = newClaim()
                .claimId(InsuranceArchetypes.CLAIM_IDENTITY, "CLM987654")
                .item()
                .treatmentDates(treatFrom1, treatTo1)
                .diagnosis("VENOM_328", "Abcess", "328")
                .invoiceItems(invoiceItem1, invoiceItem2, invoiceItem3)
                .add()
                .build();

        Claim claim = getClaim(claimAct);

        // check the claim
        assertEquals(claimAct.getId(), claim.getId());
        assertEquals("CLM987654", claim.getInsurerId());
        assertEquals(Claim.Status.PENDING, claim.getStatus());
        Policy policy = claim.getPolicy();
        assertNotNull(policy);
        assertEquals("POL123456", policy.getPolicyNumber());

        ClaimHandler handler = claim.getClaimHandler();
        assertNotNull(handler);
        assertEquals(user.getName(), handler.getName());
        assertEquals(locationEmail, handler.getEmail());
        assertEquals(locationPhone, handler.getPhone());

        // check the animal
        Patient animal = claim.getAnimal();
        assertEquals(patient.getId(), animal.getId());
        assertEquals(patient.getName(), animal.getName());
        assertEquals(dateOfBirth, DateRules.toDate(animal.getDateOfBirth()));
        assertEquals("Canine", animal.getSpeciesName());
        assertEquals("Pug", animal.getBreedName());
        assertEquals(Patient.Sex.MALE, animal.getSex());
        assertEquals("Black", animal.getColourName());
        assertEquals("123454321", animal.getMicrochip().getIdentity());
        assertEquals(patient.getCreated(), animal.getCreated());

        // check the condition
        assertEquals(1, claim.getConditions().size());
        Condition condition1 = claim.getConditions().get(0);
        checkCondition(condition1, getDate(treatFrom1), getDate(treatTo1), "VENOM_328");
        assertEquals(2, condition1.getInvoices().size());

        BigDecimal conditionDiscount = discount1.add(discount2).add(discount3);
        BigDecimal conditionDiscountTax = discountTax1.add(discountTax2).add(discountTax3);
        BigDecimal conditionTotal = total1.add(total2).add(total3);
        BigDecimal conditionTax = tax1.add(tax2).add(tax3);

        checkEquals(conditionDiscount, condition1.getDiscount());
        checkEquals(conditionDiscountTax, condition1.getDiscountTax());
        checkEquals(conditionTotal, condition1.getTotal());
        checkEquals(conditionTax, condition1.getTotalTax());

        assertNull(condition1.getEuthanasiaReason());

        // check the consultation notes
        List<Note> notes = condition1.getConsultationNotes();
        assertEquals(1, notes.size());
        checkNote(notes.get(0), getDate(treatFrom1), clinician, "Note 4", author, 0);

        // check the first invoice
        Invoice invoice1 = condition1.getInvoices().get(0);
        checkInvoice(invoice1, invoice1Act.getId(), discount1.add(discount2), discountTax1.add(discountTax2),
                     tax1.add(tax2), total1.add(total2));

        List<Item> items1 = invoice1.getItems();
        assertEquals(2, items1.size());
        checkItem(items1.get(0), invoiceItem1.getId(), itemDate1, product1, discount1, discountTax1, tax1, total1);
        checkItem(items1.get(1), invoiceItem2.getId(), itemDate2, product2, discount2, discountTax2, tax2, total2);

        // check the second invoice
        Invoice invoice2 = condition1.getInvoices().get(1);
        checkInvoice(invoice2, invoice2Act.getId(), discount3, discountTax3, tax3, total3);

        List<Item> items2 = invoice2.getItems();
        assertEquals(1, items2.size());
        checkItem(items2.get(0), invoiceItem3.getId(), itemDate3, product3, discount3, discountTax3, tax3, total3);

        checkEquals(conditionTotal, claim.getTotal());
        checkEquals(conditionTax, claim.getTotalTax());
        checkEquals(conditionDiscount, claim.getDiscount());
        checkEquals(discountTax1.add(discountTax2).add(discountTax3), claim.getDiscountTax());
        checkEquals(conditionTotal, claim.getCurrentPaid());
        checkEquals(ZERO, claim.getCurrentBalance());

        // check the history
        List<Note> history = IterableUtils.toList(claim.getHistory().getNotes());
        assertEquals(4, history.size());
        checkNote(history.get(0), getDate("2015-05-01"), clinician, "Note 1", author, 0);
        checkNote(history.get(1), getDate("2015-05-02"), clinician, "Note 2", author, 0);
        Note n3 = checkNote(history.get(2), getDate("2015-07-01"), clinician, "Note 3", author, 2);
        checkNote(n3.getNotes().get(0), getDate("2015-07-03"), clinician, "Note 3 addendum 1", author, 0);
        checkNote(n3.getNotes().get(1), getDate("2015-07-04"), clinician, "Note 3 addendum 2", author, 0);
        checkNote(history.get(3), getDate(treatFrom1), clinician, "Note 4", author, 0);
    }

    /**
     * Tests the {@link ClaimImpl#getHistory() method.
     */
    @Test
    public void testGetHistory() {
        // set up a user for the createdBy nodes
        User author = userFactory.createUser();
        AuthenticationContext context = new AuthenticationContextImpl();
        context.setUser(author);

        DocumentAct note1 = createNote("2015-05-01", patient, clinician, "Note 1");
        DocumentAct note2 = createNote("2015-05-02", patient, clinician, "Note 2");
        createVisit("2015-05-01", "2015-05-02", patient, note1, note2);
        DocumentAct note3 = createNote("2015-07-01", patient, clinician, "Note 3");
        DocumentAct addendum1 = createAddendum("2015-07-03", patient, clinician, "Note 3 addendum 1");
        DocumentAct addendum2 = createAddendum("2015-07-04", patient, clinician, "Note 3 addendum 2");
        patientFactory.updateNote(note3)
                .addAddenda(addendum1, addendum2)
                .build();
        createVisit("2015-07-01", "2015-07-02", patient, note3);

        Act note4 = createNote("2017-09-27", patient, clinician, "Note 4");
        createVisit("2017-09-27", "2017-09-29", patient, clinician, note4);

        Entity insuranceService = insuranceFactory.createInsuranceService();
        insuranceFactory.updateInsurer(insurer1)
                .insuranceService(insuranceService)
                .build();

        Act claim1Act = newClaim().item().treatmentDates("2017-10-01", "2017-10-02").add()
                .status(ClaimStatus.SUBMITTED).build();
        Claim claim1 = getClaim(claim1Act);

        checkHistory(claim1.getHistory().getNotes(), note1, note2, note3, note4);
        checkHistory(claim1.getHistory().getNotesSinceLastClaim(), note1, note2, note3, note4);
        // no prior claim submitted, so getNotesSinceLastClaim() should return the same results

        // create a new claim and verify getNotesSinceLastClaim() excludes  all the prior notes
        Act note5 = createNote("2024-01-01", patient, clinician, "Note 5");
        createVisit("2024-01-01", "2024-01-02", patient, clinician, note5);

        Act claim2Act = newClaim().item().treatmentDates("2024-01-01", "2024-01-02").add()
                .status(ClaimStatus.POSTED).build();
        Claim claim2 = getClaim(claim2Act);
        checkHistory(claim2.getHistory().getNotes(), note1, note2, note3, note4, note5);
        checkHistory(claim2.getHistory().getNotesSinceLastClaim(), note5);

        // now mark the original claim as cancelled. getNotesSinceLastClaim() should return all notes
        claim1Act.setStatus(ClaimStatus.CANCELLED);
        save(claim1Act);

        checkHistory(claim2.getHistory().getNotes(), note1, note2, note3, note4, note5);
        checkHistory(claim2.getHistory().getNotesSinceLastClaim(), note1, note2, note3, note4, note5);
    }

    /**
     * Tests the {@link Claim#canCancel()} method.
     */
    @Test
    public void testCancel() {
        Product product1 = productFactory.createService();
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        BigDecimal discount1 = new BigDecimal("0.10");
        BigDecimal tax1 = new BigDecimal("0.08");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1, product1, ONE, ONE, discount1, tax1);
        createInvoice(getDate("2017-09-27"), invoiceItem1);

        Act claimAct = createClaim(invoiceItem1);

        Claim claim = getClaim(claimAct);
        assertEquals(Claim.Status.PENDING, claim.getStatus());
        assertTrue(claim.canCancel());

        claim.setStatus(Claim.Status.POSTED);
        assertTrue(claim.canCancel());

        claim.setStatus(Claim.Status.SUBMITTED);
        assertTrue(claim.canCancel());

        claim.setStatus(Claim.Status.ACCEPTED);
        assertTrue(claim.canCancel());

        claim.setStatus(Claim.Status.SETTLED);
        assertFalse(claim.canCancel());

        claimAct.setStatus(Claim.Status.DECLINED.toString());
        assertFalse(claim.canCancel());

        claimAct.setStatus(Claim.Status.CANCELLING.toString());
        assertFalse(claim.canCancel());

        claim.setStatus(Claim.Status.CANCELLED);
        assertFalse(claim.canCancel());
    }

    /**
     * Verifies that when an <em>act.patientInsuranceClaim</em> is deleted, related items and their attachments are
     * deleted, but charges and original documents are retained.
     */
    @Test
    public void testDeleteClaim() {
        Product product1 = productFactory.createService();
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        BigDecimal discount1 = new BigDecimal("0.10");
        BigDecimal tax1 = new BigDecimal("0.08");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1, product1, ONE, ONE, discount1, tax1);
        FinancialAct invoice1 = createInvoice(getDate("2017-09-27"), invoiceItem1);

        Act claimAct = createClaim(invoiceItem1);

        IMObjectBean bean = getBean(claimAct);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        DocumentAct documentAct = PatientTestHelper.createDocumentAttachment(itemDate1, patient);
        DocumentAct attachment = InsuranceTestHelper.createAttachment(documentAct);
        Document content = create(DocumentArchetypes.DEFAULT_DOCUMENT, Document.class);
        content.setName(documentAct.getName());
        attachment.setDocument(content.getObjectReference());
        bean.addTarget("attachments", attachment, "claim");
        save(claimAct, attachment, content);

        remove(claimAct);
        assertNull(get(claimAct));
        assertNull(get(items.get(0)));
        assertNull(get(attachment));

        // NOTE: the document content is NOT deleted when the act is deleted.
        // It would be possible to set up .drl rules to handle claim deletion but it would likely interfere with
        // deletion from editors+
        assertNotNull(get(content));

        // verify the original document hasn't been deleted
        assertNotNull(get(documentAct));

        // verify the policy hasn't been deleted
        assertNotNull(get(policyAct));

        // verify the invoice hasn't been deleted
        assertNotNull(get(invoiceItem1));
        assertNotNull(get(invoice1));
    }

    /**
     * Verifies the claim policy can be changed.
     */
    @Test
    public void testSetPolicy() {
        Act claimAct1 = createClaim();

        Party insurer2 = insuranceFactory.createInsurer();

        Claim claim1 = getClaim(claimAct1);
        Policy existing = claim1.getPolicy();
        Policy update1 = claim1.setPolicy(insurer1, "POL123456");
        assertEquals(existing, update1);                 // policy should not change
        checkPolicy(update1, insurer1, "POL123456");

        Policy update2 = claim1.setPolicy(insurer2, "POL123456");
        assertNotEquals(update1, update2);               // policy should be changed
        assertEquals(existing.getId(), update2.getId()); // but should be same underlying act
        checkPolicy(update2, insurer2, "POL123456");

        Policy update3 = claim1.setPolicy(insurer2, "POL987654");
        assertNotEquals(update2, update3);               // policy should be changed
        assertEquals(existing.getId(), update3.getId()); // but should be same underlying act
        checkPolicy(update3, insurer2, "POL987654");

        // associate the policy with another claim.
        policyAct = get(policyAct);
        insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .item()
                .diagnosis("VENOM_328", "Abcess", "328")
                .add()
                .build();

        claim1 = getClaim(claimAct1);
        Policy update4 = claim1.setPolicy(insurer2, "POL1111111");
        assertNotEquals(update3, update4);              // policy should be changed
        assertEquals(existing.getId(), update3.getId()); // as should the underlying act
        assertNotEquals(update4.getId(), policyAct.getId());
        checkPolicy(update4, insurer2, "POL1111111");
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for a single invoice fully claimed on a single condition.
     */
    @Test
    public void testCurrentPaidForSingleInvoice() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        FinancialAct invoice = createInvoice(invoiceItem1);

        Act claimAct = createClaim(invoiceItem1);

        ClaimImpl claim1 = getClaim(claimAct);
        checkCurrentPaid(claim1, ZERO, TEN);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), ZERO, TEN);
        checkEquals(ZERO, claim1.getInvoiceAllocation());

        makePayment(invoice.getTotal());
        ClaimImpl claim2 = getClaim(claimAct);  // need to recreate due to caching
        checkCurrentPaid(claim2, TEN, ZERO);
        assertEquals(1, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), TEN, ZERO);
        checkEquals(TEN, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for a single invoice partially claimed on a single condition.
     */
    @Test
    public void testCurrentPaidForSinglePartiallyClaimedInvoice() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        FinancialAct invoiceItem2 = createInvoiceItem(TEN);
        FinancialAct invoice = createInvoice(invoiceItem1, invoiceItem2);

        Act claimAct = createClaim(invoiceItem1);

        ClaimImpl claim1 = getClaim(claimAct);
        checkCurrentPaid(claim1, ZERO, TEN);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), ZERO, TEN);
        checkEquals(ZERO, claim1.getInvoiceAllocation());

        makePayment(invoice.getTotal());

        ClaimImpl claim2 = getClaim(claimAct);  // need to recreate due to caching
        checkCurrentPaid(claim2, TEN, ZERO);
        assertEquals(1, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), TEN, ZERO);
        checkEquals(invoice.getTotal(), claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for multiple invoices fully claimed on a single condition.
     */
    @Test
    public void testCurrentPaidForMultipleInvoiceOnSingleCondition() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        createInvoice(invoiceItem1);

        FinancialAct invoiceItem2 = createInvoiceItem(TEN);
        createInvoice(invoiceItem2);

        Act claimAct = createClaim(invoiceItem1, invoiceItem2);

        ClaimImpl claim1 = getClaim(claimAct);
        int total = 20;
        checkCurrentPaid(claim1, 0, total);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), 0, total);
        checkEquals(ZERO, claim1.getInvoiceAllocation());

        makePayment(total);
        ClaimImpl claim2 = getClaim(claimAct);  // need to recreate due to caching
        checkCurrentPaid(claim2, total, 0);
        assertEquals(1, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), total, 0);
        checkEquals(20, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for single invoice claimed on multiple conditions.
     */
    @Test
    public void testCurrentPaidForSingleInvoiceOnMultipleCondition() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        FinancialAct invoiceItem2 = createInvoiceItem(TEN);
        createInvoice(invoiceItem1, invoiceItem2);

        Act claimAct = newClaim()
                .item(invoiceItem1)
                .item(invoiceItem2)
                .build();

        ClaimImpl claim1 = getClaim(claimAct);
        int total = 20;
        checkCurrentPaid(claim1, 0, total);
        assertEquals(2, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), ZERO, TEN);
        checkCurrentPaid(claim1.getConditions().get(1), ZERO, TEN);
        checkEquals(ZERO, claim1.getInvoiceAllocation());

        makePayment(total);
        ClaimImpl claim2 = getClaim(claimAct);  // need to recreate due to caching
        checkCurrentPaid(claim2, total, 0);
        assertEquals(2, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), TEN, ZERO);
        checkCurrentPaid(claim2.getConditions().get(1), TEN, ZERO);
        checkEquals(total, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for a single invoice where an item was claimed in another claim.
     */
    @Test
    public void testCurrentPaidForSingleInvoiceOnMultipleClaims() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        int five = 5;
        FinancialAct invoiceItem2 = createInvoiceItem(five);
        createInvoice(invoiceItem1, invoiceItem2);

        int total = 15;
        makePayment(total);

        Act claim1Act = newClaim()
                .item(invoiceItem1)
                .status(ClaimStatus.POSTED)
                .build();
        // finalise this claim - allocations will go it it rather than claim2

        Act claim2Act = newClaim()
                .item(invoiceItem2)
                .build();

        ClaimImpl claim1 = getClaim(claim1Act);
        checkCurrentPaid(claim1, TEN, ZERO);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), TEN, ZERO);
        checkEquals(total, claim1.getInvoiceAllocation());

        ClaimImpl claim2 = getClaim(claim2Act);
        checkCurrentPaid(claim2, five, 0);
        assertEquals(1, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), five, 0);
        checkEquals(five, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for a single invoice where an item was claimed in another claim and only a partial payment has been made.
     */
    @Test
    public void testCurrentPaidForSingleInvoiceOnMultipleClaimsPartialPayment() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        int five = 5;
        FinancialAct invoiceItem2 = createInvoiceItem(five);
        createInvoice(invoiceItem1, invoiceItem2);

        int paid = 7;
        makePayment(paid);

        Act claim1Act = newClaim()
                .item(invoiceItem1)
                .status(ClaimStatus.POSTED)
                .build();
        // finalise this claim - allocations will go it it rather than claim2

        Act claim2Act = newClaim()
                .item(invoiceItem2)
                .build();

        ClaimImpl claim1 = getClaim(claim1Act);
        int balance = 3;
        checkCurrentPaid(claim1, paid, balance);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), paid, balance);
        checkEquals(paid, claim1.getInvoiceAllocation());

        // verify nothing is allocated to the second claim
        ClaimImpl claim2 = getClaim(claim2Act);
        checkCurrentPaid(claim2, 0, five);
        assertEquals(1, claim2.getConditions().size());
        checkCurrentPaid(claim2.getConditions().get(0), 0, five);
        checkEquals(0, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for where one of the invoices contains a negative item.
     */
    @Test
    public void testCurrentPaidForSingleInvoiceMultipleClaimsWithNegativeItem() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        FinancialAct invoiceItem2 = createInvoiceItem(TEN);
        FinancialAct invoiceItem3 = createInvoiceItem(-5);
        createInvoice(invoiceItem1, invoiceItem2, invoiceItem3);
        int claim2Total = 5;
        int total = 15;
        makePayment(total);

        Act claim1Act = newClaim()
                .item(invoiceItem1)
                .status(ClaimStatus.POSTED)
                .build();
        // finalise this claim - allocations will go it it rather than claim2

        Act claim2Act = newClaim()
                .item(invoiceItem2, invoiceItem3)
                .build();

        ClaimImpl claim1 = getClaim(claim1Act);
        checkCurrentPaid(claim1, invoiceItem1.getTotal(), ZERO);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), invoiceItem1.getTotal(), ZERO);
        checkEquals(total, claim1.getInvoiceAllocation());

        ClaimImpl claim2 = getClaim(claim2Act);
        checkCurrentPaid(claim2, claim2Total, 0);
        checkCurrentPaid(claim2.getConditions().get(0), claim2Total, 0);
        checkEquals(claim2Total, claim2.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * for where one of the invoices is negative.
     */
    @Test
    public void testCurrentPaidForNegativeInvoice() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        FinancialAct invoiceItem2 = createInvoiceItem(-1);
        createInvoice(invoiceItem1);
        createInvoice(invoiceItem2);
        int total = 9;
        makePayment(total);

        Act claimAct = newClaim()
                .item(invoiceItem1, invoiceItem2)
                .build();

        ClaimImpl claim = getClaim(claimAct);
        checkCurrentPaid(claim, total, 0);
        assertEquals(1, claim.getConditions().size());
        checkCurrentPaid(claim.getConditions().get(0), total, 0);
        checkEquals(total, claim.getInvoiceAllocation());
    }

    /**
     * Tests the {@link ClaimImpl#getCurrentPaid()} and {@link ConditionImpl#getCurrentPaid()} methods
     * where the invoice item was in a prior claim that has been cancelled.
     */
    @Test
    public void testCurrentPaidForCancelledPriorClaim() {
        FinancialAct invoiceItem1 = createInvoiceItem(TEN);
        createInvoice(invoiceItem1);

        int four = 4;
        int six = 6;
        makePayment(six);

        Act cancelledClaim = newClaim()
                .item(invoiceItem1)
                .status(ClaimStatus.CANCELLED)
                .build();
        assertEquals(ClaimStatus.CANCELLED, cancelledClaim.getStatus());

        Act claimAct = newClaim()
                .item(invoiceItem1)
                .build();

        ClaimImpl claim1 = getClaim(claimAct);
        checkCurrentPaid(claim1, six, four);
        assertEquals(1, claim1.getConditions().size());
        checkCurrentPaid(claim1.getConditions().get(0), six, four);
        checkEquals(6, claim1.getInvoiceAllocation());
    }

    /**
     * Creates a claim populated with a single condition and the specified invoice items.
     *
     * @param invoiceItems the invoice items
     * @return the claim
     */
    private Act createClaim(FinancialAct... invoiceItems) {
        return newClaim().item(invoiceItems).build();
    }

    /**
     * Verifies the current paid and balance on a claim matches that expected.
     *
     * @param claim           the claim
     * @param expectedPaid    the expected current paid
     * @param expectedBalance the expected balance
     */
    private void checkCurrentPaid(Claim claim, int expectedPaid, int expectedBalance) {
        checkCurrentPaid(claim, BigDecimal.valueOf(expectedPaid), BigDecimal.valueOf(expectedBalance));
    }

    /**
     * Verifies the current paid and balance on a claim matches that expected.
     *
     * @param claim           the claim
     * @param expectedPaid    the expected current paid
     * @param expectedBalance the expected balance
     */
    private void checkCurrentPaid(Claim claim, BigDecimal expectedPaid, BigDecimal expectedBalance) {
        checkEquals(expectedPaid, claim.getCurrentPaid());
        checkEquals(expectedBalance, claim.getCurrentBalance());
    }

    /**
     * Verifies the current paid and balance on a condition matches that expected.
     *
     * @param condition       the condition
     * @param expectedPaid    the expected current paid
     * @param expectedBalance the expected balance
     */
    private void checkCurrentPaid(Condition condition, int expectedPaid, int expectedBalance) {
        checkCurrentPaid(condition, BigDecimal.valueOf(expectedPaid), BigDecimal.valueOf(expectedBalance));
    }

    /**
     * Verifies the current paid and balance on a condition matches that expected.
     *
     * @param condition       the condition
     * @param expectedPaid    the expected current paid
     * @param expectedBalance the expected balance
     */
    private void checkCurrentPaid(Condition condition, BigDecimal expectedPaid, BigDecimal expectedBalance) {
        checkEquals(expectedPaid, condition.getCurrentPaid());
        checkEquals(expectedBalance, condition.getCurrentBalance());
    }

    /**
     * Creates and saves a POSTED payment for the customer.
     *
     * @param amount the amount
     */
    private void makePayment(int amount) {
        makePayment(BigDecimal.valueOf(amount));
    }

    /**
     * Creates and saves a POSTED payment for the customer.
     *
     * @param amount the amount
     */
    private void makePayment(BigDecimal amount) {
        accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .cash(amount)
                .build();
    }

    /**
     * Verifies a condition matches that expected.
     *
     * @param condition   the condition to check
     * @param treatedFrom the expected treated-from date
     * @param treatedTo   the expected treated-to date
     * @param diagnosis   the expected diagnosis code
     */
    private void checkCondition(Condition condition, Date treatedFrom, Date treatedTo, String diagnosis) {
        assertEquals(treatedFrom, DateRules.toDate(condition.getTreatedFrom()));
        assertEquals(treatedTo, DateRules.toDate(condition.getTreatedTo()));
        Lookup lookup = condition.getDiagnosis();
        assertNotNull(lookup);
        assertEquals(diagnosis, lookup.getCode());
    }

    /**
     * Verifies a note matches that expected.
     *
     * @param note      the note to check
     * @param date      the expected date
     * @param clinician the expected clinician
     * @param text      the expected tex
     * @param author    the expected author
     * @param notes     the expected no. of addenda
     * @return the note
     */
    private Note checkNote(Note note, Date date, User clinician, String text, User author, int notes) {
        assertEquals(0, DateRules.compareTo(date, DateRules.toDate(note.getDate())));
        assertEquals(clinician, note.getClinician());
        assertEquals(text, note.getText());
        assertEquals(notes, note.getNotes().size());
        assertEquals(author, note.getAuthor());
        return note;
    }

    /**
     * Verifies an invoice matches that expected.
     *
     * @param invoice     the invoice to check
     * @param id          the expected id
     * @param discount    the expected discount
     * @param discountTax the expected discount tax
     * @param tax         the expected tax
     * @param total       the expected total
     */
    private void checkInvoice(Invoice invoice, long id, BigDecimal discount, BigDecimal discountTax, BigDecimal tax,
                              BigDecimal total) {
        assertEquals(id, invoice.getId());
        checkEquals(discount, invoice.getDiscount());
        checkEquals(discountTax, invoice.getDiscountTax());
        checkEquals(tax, invoice.getTotalTax());
        checkEquals(total, invoice.getTotal());
    }

    /**
     * Verifies an invoice item matches that expected.
     *
     * @param item        the invoice item to check
     * @param id          the expected id
     * @param date        the expected date
     * @param product     the expected product
     * @param discount    the expected discount
     * @param discountTax the expected discount tax
     * @param tax         the expected tax
     * @param total       the expected total
     */
    private void checkItem(Item item, long id, Date date, Product product, BigDecimal discount, BigDecimal discountTax,
                           BigDecimal tax, BigDecimal total) {
        assertEquals(id, item.getId());
        assertEquals(date, DateRules.toDate(item.getDate()));
        assertEquals(product, item.getProduct());
        checkEquals(discount, item.getDiscount());
        checkEquals(discountTax, item.getDiscountTax());
        checkEquals(tax, item.getTotalTax());
        checkEquals(total, item.getTotal());
    }

    /**
     * Verifies that note history matches that expected.
     *
     * @param history  the history
     * @param expected the expected notes
     */
    private void checkHistory(Iterable<Note> history, Act... expected) {
        List<Note> actual = IterableUtils.toList(history);
        assertEquals(expected.length, actual.size());
        for (int i = 0; i < actual.size(); ++i) {
            checkNote(expected[i], actual.get(i));
        }
    }

    /**
     * Verifies a note matches that expected.
     *
     * @param expected the expected note
     * @param actual   the actual note
     */
    private void checkNote(Act expected, Note actual) {
        User author = get(expected.getCreatedBy(), User.class);
        assertNotNull(author);
        IMObjectBean bean = getBean(expected);
        List<Act> expectedAddenda;
        if (expected.isA(PatientArchetypes.CLINICAL_NOTE)) {
            expectedAddenda = bean.getTargets("addenda", Act.class);
            expectedAddenda.sort(ActComparator.ascending());
        } else {
            expectedAddenda = Collections.emptyList();
        }
        Note note = checkNote(actual, expected.getActivityStartTime(), clinician, bean.getString("note"), author,
                              expectedAddenda.size());
        if (!expectedAddenda.isEmpty()) {
            List<Note> actualAddenda = note.getNotes();
            assertEquals(expectedAddenda.size(), actualAddenda.size());
            for (int i = 0; i < actualAddenda.size(); i++) {
                checkNote(expectedAddenda.get(i), actualAddenda.get(i));
            }
        }
    }

    /**
     * Creates a patient visit.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param patient   the patient
     * @param items     the visit items
     * @return a new visit
     */
    private Act createVisit(String startTime, String endTime, Party patient, Act... items) {
        return createVisit(startTime, endTime, patient, null, items);
    }

    /**
     * Creates a patient visit.
     *
     * @param startTime the start time
     * @param endTime   the end time
     * @param patient   the patient
     * @param clinician the clinician. May be {@code null}
     * @param items     the visit items
     * @return a new visit
     */
    private Act createVisit(String startTime, String endTime, Party patient, User clinician, Act... items) {
        return patientFactory.newVisit()
                .startTime(startTime)
                .endTime(endTime)
                .patient(patient)
                .clinician(clinician)
                .addItems(items)
                .build();
    }

    /**
     * Creates a patient clinical note.
     *
     * @param date      the date
     * @param patient   the patient
     * @param clinician the clinician
     * @param note      the note
     * @return a new note
     */
    private DocumentAct createNote(String date, Party patient, User clinician, String note) {
        return patientFactory.newNote()
                .startTime(date)
                .patient(patient)
                .clinician(clinician)
                .note(note)
                .build();
    }

    /**
     * Creates a patient clinical note addendum.
     *
     * @param date      the date
     * @param patient   the patient
     * @param clinician the clinician
     * @param note      the note
     * @return a new note
     */
    private DocumentAct createAddendum(String date, Party patient, User clinician, String note) {
        return patientFactory.newAddendum()
                .startTime(date)
                .patient(patient)
                .clinician(clinician)
                .note(note)
                .build();
    }
}