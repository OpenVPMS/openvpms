/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.junit.Before;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemBuilder;
import org.openvpms.archetype.test.builder.insurance.TestClaimBuilder;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.party.TestEmailContactBuilder;
import org.openvpms.archetype.test.builder.party.TestPhoneContactBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.insurance.policy.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Base class for insurance claim tests.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractClaimTest extends ArchetypeServiceTest {

    /**
     * The test practice.
     */
    protected Party practice;

    /**
     * The test customer.
     */
    protected Party customer;

    /**
     * The test patient.
     */
    protected Party patient;

    /**
     * Patient date of birth.
     */
    protected Date dateOfBirth;

    /**
     * The test clinician.
     */
    protected User clinician;

    /**
     * The practice location.
     */
    protected Party location;

    /**
     * Practice location phone.
     */
    protected Contact locationPhone;

    /**
     * Practice location email.
     */
    protected Contact locationEmail;

    /**
     * The claim handler.
     */
    protected User user;

    /**
     * The policy.
     */
    protected Act policyAct;

    /**
     * The test insurer.
     */
    protected Party insurer1;

    /**
     * The insurance rules.
     */
    protected InsuranceRules insuranceRules;

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    protected TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    protected TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    protected TestUserFactory userFactory;

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules customerAccountRules;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService service;

    /**
     * The document handlers.
     */
    private DocumentHandlers handlers;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        // set up a 10% tax rate on the practice
        practice = practiceFactory.newPractice()
                .addTaxType(BigDecimal.TEN)
                .build();

        // customer
        customer = customerFactory.newCustomer()
                .title("MS")
                .firstName("J")
                .lastName("Bloggs")
                .addAddress("12 Broadwater Avenue", "CAPE_WOOLAMAI", "VIC", "3925")
                .addHomePhone("9123456")
                .addWorkPhone("98765432")
                .addMobilePhone("04987654321")
                .addEmail("foo@test.com")
                .build();

        IArchetypeService service = getArchetypeService();
        handlers = new DocumentHandlers(service);

        insuranceRules = new InsuranceRules((IArchetypeRuleService) service, transactionManager);

        // practice location
        locationPhone = new TestPhoneContactBuilder<>(service).phone("5123456").build();
        locationEmail = new TestEmailContactBuilder<>(service).email("vetsrus@test.com").build();
        location = practiceFactory.newLocation()
                .addContact(locationEmail)
                .addContact(locationPhone)
                .build();

        // clinician
        clinician = userFactory.createClinician();

        // claim handler
        user = userFactory.createUser("Z", "Smith");

        // patient
        dateOfBirth = DateRules.getDate(DateRules.getToday(), -1, DateUnits.YEARS);
        patient = patientFactory.newPatient()
                .name("Fido")
                .species("CANINE")
                .breed("PUG")
                .sex("MALE")
                .dateOfBirth(dateOfBirth)
                .addMicrochip("123454321")
                .colour("BLACK")
                .owner(customer)
                .build();

        // insurer
        insurer1 = insuranceFactory.createInsurer();

        // policy
        policyAct = insuranceFactory.createPolicy(customer, patient, insurer1, "POL123456");
    }

    /**
     * Verifies a policy matches that expected.
     *
     * @param policy       the policy
     * @param insurer      the expected insurer
     * @param policyNumber the expected policy number
     */
    protected void checkPolicy(Policy policy, Party insurer, String policyNumber) {
        assertEquals(insurer, policy.getInsurer());
        assertEquals(policyNumber, policy.getPolicyNumber());
    }

    /**
     * Creates a claim.
     *
     * @param act the claim act
     * @return a new claim
     */
    protected ClaimImpl getClaim(Act act) {
        return new ClaimImpl(act, (IArchetypeRuleService) getArchetypeService(), insuranceRules, customerRules,
                             patientRules, handlers, transactionManager, service);
    }

    /**
     * Creates a gap claim.
     *
     * @param act the claim act
     * @return a new gap claim
     */
    protected GapClaimImpl createGapClaim(FinancialAct act) {
        return new GapClaimImpl(act, (IArchetypeRuleService) getArchetypeService(), insuranceRules, customerRules,
                                customerAccountRules, patientRules, handlers, transactionManager, service);
    }

    /**
     * Creates an invoice item for a single item of the specified amount.
     *
     * @param amount the amount
     * @return the new invoice item
     */
    protected FinancialAct createInvoiceItem(int amount) {
        return createInvoiceItem(BigDecimal.valueOf(amount));
    }

    /**
     * Creates an invoice item for a single item of the specified amount.
     *
     * @param amount the amount
     * @return the new invoice item
     */
    protected FinancialAct createInvoiceItem(BigDecimal amount) {
        return new TestInvoiceItemBuilder(null, getArchetypeService())
                .patient(patient)
                .product(productFactory.createService())
                .quantity(BigDecimal.ONE)
                .unitPrice(amount)
                .clinician(clinician)
                .build(false);
    }

    /**
     * Creates an invoice item.
     *
     * @param date     the date
     * @param product  the product
     * @param quantity the quantity
     * @param price    the unit price
     * @param discount the discount
     * @param tax      the tax
     * @return the new invoice item
     */
    protected FinancialAct createInvoiceItem(Date date, Product product, BigDecimal quantity, BigDecimal price,
                                             BigDecimal discount, BigDecimal tax) {
        return new TestInvoiceItemBuilder(null, getArchetypeService())
                .startTime(date)
                .patient(patient)
                .product(product)
                .quantity(quantity)
                .unitPrice(price)
                .discount(discount)
                .tax(tax)
                .clinician(clinician)
                .build(false);
    }

    /**
     * Creates and saves a POSTED invoice.
     *
     * @param items the invoice items
     * @return the invoice
     */
    protected FinancialAct createInvoice(FinancialAct... items) {
        return createInvoice(new Date(), items);
    }

    /**
     * Creates and saves a POSTED invoice.
     *
     * @param date  the invoice date
     * @param items the invoice items
     * @return the invoice
     */
    protected FinancialAct createInvoice(Date date, FinancialAct... items) {
        return new TestInvoiceBuilder(getArchetypeService())
                .startTime(date)
                .customer(customer)
                .status(ActStatus.POSTED)
                .add(items)
                .build();
    }

    /**
     * Creates a builder pre-populated with policy, location, clinician and claim handler details.
     *
     * @return a new claim builder
     */
    protected TestClaimBuilder newClaim() {
        return insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user);
    }

}
