/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link AttachmentImpl} class.
 *
 * @author Tim Anderson
 */
public class AttachmentImplTestCase extends ArchetypeServiceTest {

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests accessors.
     *
     * @throws Exception for any error
     */
    @Test
    public void testAccessors() throws Exception {
        Document image = documentFactory.createImage();
        assertTrue(image.getSize() > 0);

        DocumentAct act = patientFactory.newAttachment()
                .patient(patientFactory.createPatient())
                .document(image)
                .build();

        AttachmentImpl attachment = new AttachmentImpl(act, getArchetypeService(), documentHandlers);
        assertEquals("image.png", attachment.getFileName());
        assertEquals("image/png", attachment.getMimeType());
        assertEquals(image.getSize(), attachment.getSize());
        byte[] expectedContent = IOUtils.toByteArray(documentHandlers.get(image).getContent(image));
        byte[] actualContent = IOUtils.toByteArray(attachment.getContent());
        assertArrayEquals(expectedContent, actualContent);
    }
}
