/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.credit.CreditActAllocator;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestPaymentBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.Deposit;
import org.openvpms.insurance.claim.GapClaim;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.math.MathRules.ONE_HUNDRED;

/**
 * Tests the {@link GapClaimImpl} class.
 *
 * @author Tim Anderson
 */
public class GapClaimImplTestCase extends AbstractClaimTest {

    /**
     * The till used for payments.
     */
    private Entity till;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;


    /**
     * Sets up the test case.
     */
    @Override
    public void setUp() {
        super.setUp();
        till = practiceFactory.createTill();
        practiceFactory.updateLocation(location)
                .tills(till)
                .build();
    }

    /**
     * Verifies that paying the gap generates an adjustment.
     */
    @Test
    public void testPayGap() {
        checkPayClaim(true, BigDecimal.valueOf(80));
    }

    /**
     * Verifies that paying the gap generates an adjustment.
     */
    @Test
    public void testPayFullClaim() {
        checkPayClaim(false, BigDecimal.valueOf(80));
    }

    /**
     * Verifies that no adjustment is created if there is a zero benefit amount.
     */
    @Test
    public void testZeroBenefit() {
        checkPayClaim(true, ZERO);
        checkPayClaim(false, ZERO);
    }

    /**
     * Tests the {@link GapClaimImpl#setVetBenefitAmount(BigDecimal)} method.
     */
    @Test
    public void testSetVetBenefitAmount() {
        FinancialAct claimAct = createClaim();
        GapClaimImpl claim = createGapClaim(claimAct);
        BigDecimal amount = BigDecimal.valueOf(50);
        claim.setVetBenefitAmount(amount);

        claim = createGapClaim(get(claimAct));
        checkEquals(amount, claim.getVetBenefitAmount());
    }

    /**
     * Tests settling a gap claim.
     */
    @Test
    public void testSettle() {
        FinancialAct claimAct = createClaim();
        GapClaimImpl claim = createGapClaim(claimAct);

        BigDecimal amount = BigDecimal.valueOf(80);
        claim.setBenefit(amount, "accepted");
        FinancialAct payment = claim.gapPaid(till, location);
        checkBenefitPayment(payment, amount, GapClaimImpl.GAP_BENEFIT_PAYMENT, claim);
        BigDecimal vetBenefit = BigDecimal.valueOf(70);
        OffsetDateTime date1 = DateRules.toOffsetDateTime(DateRules.getYesterday());
        OffsetDateTime date2 = DateRules.toOffsetDateTime(DateRules.getToday());
        claim.state()
                .vetBenefitAmount(vetBenefit)
                .deposit("1", "actIdentity.insuranceDepositTest", date1, BigDecimal.valueOf(40))
                .deposit("2", "actIdentity.insuranceDepositTest", date2, BigDecimal.valueOf(30))
                .status(Claim.Status.SETTLED)
                .update();

        claim = createGapClaim(get(claimAct));
        assertEquals(Claim.Status.SETTLED, claim.getStatus());
        checkEquals(vetBenefit, claim.getVetBenefitAmount());
        List<Deposit> deposits = claim.getDeposits();
        assertEquals(2, deposits.size());
        checkDeposit(deposits, "1", date1, BigDecimal.valueOf(40));
        checkDeposit(deposits, "2", date2, BigDecimal.valueOf(30));
    }

    /**
     * Verifies that deposits must have unique identifiers.
     */
    @Test
    public void testDuplicateDeposit() {
        FinancialAct claimAct = createClaim();
        GapClaimImpl claim = createGapClaim(claimAct);
        BigDecimal amount = BigDecimal.valueOf(80);
        claim.setBenefit(amount, "accepted");
        FinancialAct payment = claim.gapPaid(till, location);
        checkBenefitPayment(payment, amount, GapClaimImpl.GAP_BENEFIT_PAYMENT, claim);
        BigDecimal vetBenefit = BigDecimal.valueOf(70);
        OffsetDateTime date1 = DateRules.toOffsetDateTime(DateRules.getYesterday());
        OffsetDateTime date2 = DateRules.toOffsetDateTime(DateRules.getToday());

        try {
            claim.state()
                    .vetBenefitAmount(vetBenefit)
                    .deposit("1", "actIdentity.insuranceDepositTest", date1, BigDecimal.valueOf(40))
                    .deposit("1", "actIdentity.insuranceDepositTest", date2, BigDecimal.valueOf(30))
                    .status(Claim.Status.SETTLED)
                    .update();
        } catch (IllegalArgumentException exception) {
            assertEquals("Duplicate deposit with identifier 1", exception.getMessage());
        }

        // add a single deposit
        claim.state()
                .vetBenefitAmount(vetBenefit)
                .deposit("1", "actIdentity.insuranceDepositTest", date1, BigDecimal.valueOf(40))
                .status(Claim.Status.SETTLED)
                .update();

        try {
            // now try and add another deposit with the same id
            claim.state()
                    .deposit("1", "actIdentity.insuranceDepositTest", date2, BigDecimal.valueOf(30))
                    .update();
        } catch (IllegalArgumentException exception) {
            assertEquals("Duplicate deposit with identifier 1", exception.getMessage());
        }

        claim = createGapClaim(get(claimAct));
        assertEquals(Claim.Status.SETTLED, claim.getStatus());
        checkEquals(vetBenefit, claim.getVetBenefitAmount());
        List<Deposit> deposits = claim.getDeposits();
        assertEquals(1, deposits.size());
        checkDeposit(deposits, "1", date1, BigDecimal.valueOf(40));
    }

    /**
     * Verifies that when an insurer specifies a payment type, it is used for the benefit payment.
     */
    @Test
    public void testInsurerPaymentType() {
        String paymentType = "SPECIAL_PAYMENT_TYPE";
        insuranceFactory.updateInsurer(insurer1)
                .paymentType(paymentType)
                .build();
        FinancialAct claimAct = createClaim();
        GapClaimImpl claim = createGapClaim(claimAct);

        BigDecimal amount = BigDecimal.valueOf(80);
        claim.setBenefit(amount, "accepted");
        FinancialAct payment = claim.gapPaid(till, location);
        checkBenefitPayment(payment, amount, paymentType, claim);
    }

    /**
     * Checks payment of a gap claim.
     * <p>
     * When a credit adjustment is created, verifies that it is allocated against the invoice.
     *
     * @param payGap  if {@code true}, pay the gap, otherwise pay the full claim
     * @param benefit the benefit
     */
    protected void checkPayClaim(boolean payGap, BigDecimal benefit) {
        BigDecimal total = BigDecimal.valueOf(95);
        BigDecimal gap = total.subtract(benefit);
        Product product1 = productFactory.createMedication();
        BigDecimal tax = new BigDecimal("8.64");
        BigDecimal discount = BigDecimal.valueOf(5);

        FinancialAct invoiceItem1 = createInvoiceItem(new Date(), product1, ONE, ONE_HUNDRED, discount,
                                                      tax);
        FinancialAct invoice1 = createInvoice(new Date(), invoiceItem1);

        FinancialAct invoiceItem2 = createInvoiceItem(new Date(), product1, ONE, ONE_HUNDRED, discount,
                                                      tax);
        FinancialAct invoice2 = createInvoice(new Date(), invoiceItem2);

        FinancialAct claimAct = insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .gapClaim(true)
                .item()
                .diagnosis("VENOM_328", "Abscess", "328")
                .invoiceItems(invoiceItem2)
                .add()
                .build();

        GapClaimImpl claim = createGapClaim(claimAct);

        // claim not submitted
        assertEquals(Claim.Status.PENDING, claim.getStatus());
        assertEquals(GapClaim.GapStatus.PENDING, claim.getGapStatus());
        checkEquals(total, claim.getTotal());
        checkEquals(ZERO, claim.getBenefitAmount());
        checkEquals(total, claim.getGapAmount());
        checkEquals(discount, claim.getDiscount());
        checkEquals(tax, claim.getTotalTax());
        assertNull(claim.getBenefitNotes());
        checkEquals(ZERO, claim.getCurrentPaid());
        checkEquals(total, claim.getCurrentBalance());

        // simulate claim acceptance.
        claim.setStatus(Claim.Status.ACCEPTED);
        claim.setBenefit(benefit, "Accepted");

        assertEquals(GapClaim.GapStatus.RECEIVED, claim.getGapStatus());
        assertEquals("Accepted", claim.getBenefitNotes());
        checkEquals(benefit, claim.getBenefitAmount());
        checkEquals(gap, claim.getGapAmount());
        checkEquals(total, claim.getTotal());

        FinancialAct adjustment = null;

        BigDecimal pay = (payGap) ? gap : total;

        // allocate the payment against the claim
        List<FinancialAct> paymentActs = createPayment(pay);
        FinancialAct payment = paymentActs.get(0);
        CreditActAllocator allocator = new CreditActAllocator(getArchetypeService(), insuranceRules);
        List<FinancialAct> updated = allocator.allocate(payment, invoice2);
        assertEquals(2, updated.size());
        updated.add(paymentActs.get(1));
        save(updated);

        // check allocations
        checkAllocation(payment, pay);
        checkAllocation(invoice2, pay);
        checkAllocation(invoice1, ZERO);

        if (payGap) {
            // now record the gap as being paid
            Entity till = practiceFactory.createTill();
            adjustment = claim.gapPaid(till, location);
        } else {
            claim.fullyPaid();
        }

        checkEquals(claim.getTotal(), claim.getCurrentPaid());
        checkEquals(ZERO, claim.getCurrentBalance());

        assertEquals(GapClaim.GapStatus.PAID, claim.getGapStatus());

        if (payGap) {
            checkEquals(gap, claim.getPaid());
            if (!MathRules.isZero(benefit)) {
                checkBenefitPayment(adjustment, benefit, GapClaimImpl.GAP_BENEFIT_PAYMENT, claim);
            } else {
                assertNull(adjustment);
            }
        } else {
            checkEquals(total, claim.getPaid());
        }

        // check allocations
        checkAllocation(payment, pay);
        checkAllocation(invoice2, total);
        checkAllocation(invoice1, ZERO);
    }

    /**
     * Creates a claim.
     *
     * @return a new claim
     */
    private FinancialAct createClaim() {
        BigDecimal tax = new BigDecimal("8.64");
        BigDecimal discount = BigDecimal.valueOf(5);
        FinancialAct invoiceItem1 = createInvoiceItem(new Date(), productFactory.createService(), ONE, ONE_HUNDRED,
                                                      discount, tax);
        createInvoice(new Date(), invoiceItem1);

        return newClaim()
                .gapClaim(true)
                .item()
                .diagnosis("VENOM_328", "Abcess", "328")
                .invoiceItems(invoiceItem1)
                .add()
                .build();
    }

    /**
     * Verifies a deposit exists.
     *
     * @param deposits the deposits
     * @param id       the deposit id
     * @param date     the expected date
     * @param amount   the expected amount
     */
    private void checkDeposit(List<Deposit> deposits, String id, OffsetDateTime date, BigDecimal amount) {
        boolean found = false;
        for (Deposit deposit : deposits) {
            if (deposit.getDepositId().equals(id)) {
                assertEquals(date, deposit.getDate());
                checkEquals(amount, deposit.getAmount());
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    /**
     * Creates but does not save, a POSTED payment.
     *
     * @param amount the payment amount
     * @return the payment and item
     */
    private List<FinancialAct> createPayment(BigDecimal amount) {
        TestPaymentBuilder builder = accountFactory.newPayment();
        FinancialAct payment = builder.customer(customer)
                .status(FinancialActStatus.POSTED)
                .cash(amount)
                .till(till)
                .build(false);
        List<FinancialAct> result = new ArrayList<>();
        result.add(payment);
        result.addAll(builder.getItems());
        return result;
    }

    /**
     * Verifies that the latest instance of an act is allocated correctly.
     *
     * @param act      the act
     * @param expected the expected allocated amount
     */
    private void checkAllocation(FinancialAct act, BigDecimal expected) {
        checkEquals(expected, get(act).getAllocatedAmount());
    }

    /**
     * Verifies a benefit payment matches that expected.
     *
     * @param payment     the payment
     * @param amount      expected amount
     * @param paymentType the expected payment type
     */
    private void checkBenefitPayment(FinancialAct payment, BigDecimal amount, String paymentType, Claim claim) {
        assertNotNull(payment);
        assertTrue(payment.isA(CustomerAccountArchetypes.PAYMENT));
        assertEquals(ActStatus.POSTED, payment.getStatus());
        checkEquals(amount, payment.getTotal());
        IMObjectBean bean = getBean(payment);
        assertEquals("Benefit Payment for Insurance Claim " + claim.getInsurerId() +
                     " for " + patient.getName() + " submitted to " + insurer1.getName(),
                     bean.getString("notes"));
        FinancialAct item = bean.getTarget("items", FinancialAct.class);
        assertNotNull(item);
        assertTrue(item.isA(CustomerAccountArchetypes.PAYMENT_OTHER));
        IMObjectBean itemBean = getBean(item);
        assertEquals(paymentType, itemBean.getString("paymentType"));

        checkAllocation(payment, amount); // adjustment should be fully allocated
    }
}
