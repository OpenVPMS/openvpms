<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>openvpms-insurance</artifactId>
        <groupId>org.openvpms</groupId>
        <version>2.4.0.8-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>openvpms-insurance-impl</artifactId>

    <build>
        <plugins>
            <plugin>
                <groupId>org.openvpms</groupId>
                <artifactId>openvpms-archetype-maven-plugin</artifactId>
                <version>${project.version}</version>
                <configuration>
                    <dir>src/test/resources</dir>
                    <driver>${build.jdbc.driverClassName}</driver>
                    <url>${build.jdbc.url}</url>
                    <username>${build.jdbc.username}</username>
                    <password>${build.jdbc.password}</password>
                    <skip>${skipTests}</skip>
                </configuration>
                <executions>
                    <execution>
                        <phase>process-test-classes</phase>
                        <goals>
                            <goal>load</goal>
                        </goals>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>${jdbc.groupId}</groupId>
                        <artifactId>${jdbc.artifactId}</artifactId>
                        <version>${jdbc.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <!-- generates a jar of the test sources -->
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- generates a jar of the test classes. -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                        <configuration>
                            <excludes>
                                <exclude>applicationContext.xml</exclude>
                                <exclude>actIdentity.insuranceDepositTest.adl</exclude>
                            </excludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-framework</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-archetypes</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-insurance-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-domain-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-plugin-manager</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-plugin-services-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-archetypes</artifactId>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-dbcp2</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>${jdbc.groupId}</groupId>
            <artifactId>${jdbc.artifactId}</artifactId>
            <version>${jdbc.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.servicemix.bundles</groupId>
            <artifactId>org.apache.servicemix.bundles.spring-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

</project>