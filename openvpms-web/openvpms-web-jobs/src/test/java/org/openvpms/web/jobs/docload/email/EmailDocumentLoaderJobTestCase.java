/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.sun.mail.imap.IMAPStore;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.util.InMemoryResource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.mail.Folder;
import javax.mail.FolderNotFoundException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EmailDocumentLoaderJob}.
 *
 * @author Tim Anderson
 */
public class EmailDocumentLoaderJobTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules rules;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The email provider.
     */
    private GreenMail greenMail;

    /**
     * The mail server password resolver.
     */
    private MailPasswordResolver passwordResolver;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The mail session.
     */
    private Session session;

    /**
     * The IMAP store.
     */
    private IMAPStore store;

    /**
     * Sets up the test case.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        greenMail = new GreenMail(ServerSetupTest.SMTP_IMAP);
        greenMail.start();

        store = greenMail.getImap().createStore();
        greenMail.setUser("foo@gmail.com", "bar");
        store.connect("foo@gmail.com", "bar");
        store.getFolder("archive").create(Folder.HOLDS_MESSAGES);
        store.getFolder("error").create(Folder.HOLDS_MESSAGES);

        session = greenMail.getSmtp().createSession();

        PasswordEncryptor encryptor = new PasswordEncryptor() {
            @Override
            public String encrypt(String password) {
                return password;
            }

            @Override
            public String decrypt(String encryptedPassword) {
                return encryptedPassword;
            }
        };
        passwordResolver = new MailPasswordResolver(
                Mockito.mock(OAuth2AuthorizedClientManager.class), encryptor);
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        greenMail.stop();
    }

    /**
     * Tests the job.
     *
     * @throws Exception for any error
     */
    @Test
    public void testJob() throws Exception {
        DocumentAct investigation = createInvestigation();
        assertNull(investigation.getDocument());

        String fileName = "pathlab-" + investigation.getId() + "-abcdefg1234.pdf";
        Resource attachment = createAttachment(fileName);
        MimeMessage email = createMessage("results@pathology.com", attachment);
        Transport.send(email);
        greenMail.waitForIncomingEmail(1);

        TestEmailDocumentLoaderJob job = runJob();

        // verify the investigation status changes
        investigation = checkInvestigation(investigation, attachment);
        assertEquals(InvestigationActStatus.RECEIVED, investigation.getStatus2());

        String message = "The following documents were loaded:\n" +
                         "   Identifier: " + investigation.getId() + ", Attachment: pathlab-" + investigation.getId()
                         + "-abcdefg1234.pdf, From: results@pathology.com, Subject: my subject, Received: x \n";
        checkStatus(job, 1, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 1 document", "COMPLETED", message,
                    null);

        // verify the message is moved from INBOX to archive
        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 1);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that attachments are versioned.
     *
     * @throws Exception for any error
     */
    @Test
    public void testVersioning() throws Exception {
        DocumentAct investigation = createInvestigation();
        Resource attachment1 = createAttachment("pathlab-" + investigation.getId() + "-a.pdf");
        Resource attachment2 = createAttachment("pathlab-" + investigation.getId() + "-b.pdf");
        sendMessage("results@path.com", attachment1);
        sendMessage("results@path.com", attachment2);
        greenMail.waitForIncomingEmail(2);

        TestEmailDocumentLoaderJob job = runJob();

        checkInvestigation(investigation, attachment2, attachment1);

        long id = investigation.getId();
        String message = "The following documents were loaded:\n"
                         + "   Identifier: " + id + ", Attachment: pathlab-" + id + "-a.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n"
                         + "   Identifier: " + id + ", Attachment: pathlab-" + id + "-b.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n";
        checkStatus(job, 2, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 2 documents", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 2);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that an email can have multiple attachments for the same investigation.
     * <p/>
     * This is not likely to work too well in practice, as the last attachment becomes the primary document and
     * the others are versioned, but it does mean that all documents are loaded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testEmailWithMultipleAttachmentsForSameInvestigations() throws Exception {
        DocumentAct investigation = createInvestigation();
        Resource attachment1 = createAttachment(investigation.getId() + "-a.pdf");
        Resource attachment2 = createAttachment(investigation.getId() + "-b.pdf");
        sendMessage("results@pathlab.com", attachment1, attachment2);
        greenMail.waitForIncomingEmail(1);
        TestEmailDocumentLoaderJob job = runJob();

        checkInvestigation(investigation, attachment2, attachment1);

        String message = "The following documents were loaded:\n" +
                         "   Identifier: " + investigation.getId() + ", Attachment: " + investigation.getId()
                         + "-a.pdf, From: results@pathlab.com, Subject: my subject, Received: x \n"
                         + "   Identifier: " + investigation.getId() + ", Attachment: " + investigation.getId()
                         + "-b.pdf, From: results@pathlab.com, Subject: my subject, Received: x \n";
        checkStatus(job, 2, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 2 documents", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 1);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that an email can have multiple attachments for the same investigation.
     * <p/>
     * This is not likely to work too well in practice, as the last attachment becomes the primary document and
     * the others are versioned, but it does mean that all documents are loaded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testEmailWithOneValidOneInvalidAttachment() throws Exception {
        DocumentAct investigation = createInvestigation();
        long id = investigation.getId();
        Resource attachment1 = createAttachment(id + ".pdf");
        Resource attachment2 = createAttachment("advertising.pdf");
        sendMessage("results@pathlab.com", attachment1, attachment2);
        greenMail.waitForIncomingEmail(1);
        TestEmailDocumentLoaderJob job = runJob();

        checkInvestigation(investigation, attachment1);

        String message = "The following documents couldn't be loaded as there is no identifier in the attachment "
                         + "name:\n"
                         + "   Attachment: advertising.pdf, From: results@pathlab.com, Subject: my subject, "
                         + "Received: x \n"
                         + "\n"
                         + "\n"
                         + "The following documents were loaded:\n"
                         + "   Identifier: " + id + ", Attachment: " + id + ".pdf, From: results@pathlab.com, " +
                         "Subject: my subject, Received: x \n";
        checkStatus(job, 1, 0, 1, 0, 0, 0, "Email Document Loader Job: Test Job failed with 1 error", "ERROR", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 0);
        checkCount(store, "error", 1);
    }

    /**
     * Verifies that an email can have multiple attachments for different investigations.
     *
     * @throws Exception for any error
     */
    @Test
    public void testEmailWithMultipleAttachmentsForDifferentInvestigations() throws Exception {
        DocumentAct investigation1 = createInvestigation();
        DocumentAct investigation2 = createInvestigation();
        Resource attachment1 = createAttachment(investigation1.getId() + ".pdf");
        Resource attachment2 = createAttachment(investigation2.getId() + ".pdf");
        sendMessage("results@pathlab.com", attachment1, attachment2);
        greenMail.waitForIncomingEmail(2);
        TestEmailDocumentLoaderJob job = runJob();

        checkInvestigation(investigation1, attachment1);
        checkInvestigation(investigation2, attachment2);

        String message = "The following documents were loaded:\n" +
                         "   Identifier: " + investigation1.getId() + ", Attachment: " + investigation1.getId()
                         + ".pdf, From: results@pathlab.com, Subject: my subject, Received: x \n"
                         + "   Identifier: " + investigation2.getId() + ", Attachment: " + investigation2.getId()
                         + ".pdf, From: results@pathlab.com, Subject: my subject, Received: x \n";
        checkStatus(job, 2, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 2 documents", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 1);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that emails with no attachments are moved to the archive folder.
     */
    @Test
    public void testEmailWithNoAttachment() throws Exception {
        sendMessage("results@pathology.com");
        greenMail.waitForIncomingEmail(1);

        TestEmailDocumentLoaderJob job = runJob();

        String message = "The following emails had no attachments:\n"
                         + "   From: results@pathology.com, Subject: my subject, Received: x \n";
        checkStatus(job, 0, 0, 0, 0, 1, 0, "Email Document Loader Job: Test Job loaded 0 document", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 1);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that messages can be filtered by From address.
     *
     * @throws Exception for any error
     */
    @Test
    public void testFilterByFrom() throws Exception {
        DocumentAct investigation1 = createInvestigation();
        DocumentAct investigation2 = createInvestigation();
        DocumentAct investigation3 = createInvestigation();
        Resource attachment1 = createAttachment("pathlab-" + investigation1.getId() + ".pdf");
        Resource attachment2 = createAttachment("pathlab-" + investigation2.getId() + ".pdf");
        Resource attachment3 = createAttachment("pathlab-" + investigation3.getId() + ".pdf");
        sendMessage("results@path1.com", attachment1);
        sendMessage("results@path2.com", attachment2);
        sendMessage("results@path3.com", attachment3);
        greenMail.waitForIncomingEmail(3);

        Entity config = createConfig("results@path1.com, results@path2.com", "INBOX", "archive", "error");
        // only process results from these addresses
        TestEmailDocumentLoaderJob job = runJob(config);

        checkInvestigation(investigation1, attachment1);
        checkInvestigation(investigation2, attachment2);
        checkInvestigation(investigation3, null);

        String message = "The following documents were loaded:\n"
                         + "   Identifier: " + investigation1.getId()
                         + ", Attachment: pathlab-" + investigation1.getId() + ".pdf, From: results@path1.com, "
                         + "Subject: my subject, Received: x \n"
                         + "   Identifier: " + investigation2.getId()
                         + ", Attachment: pathlab-" + investigation2.getId() + ".pdf, From: results@path2.com, "
                         + "Subject: my subject, Received: x \n";

        checkStatus(job, 2, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 2 documents", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 1);
        checkCount(store, "archive", 2);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that when there is no corresponding act for an attachment, the message is moved to the error folder.
     *
     * @throws Exception for any error
     */
    @Test
    public void testNoActMovedToErrorFolder() throws Exception {
        Resource attachment = createAttachment("pathlab-0-a.pdf");
        sendMessage("results@path.com", attachment);
        greenMail.waitForIncomingEmail(1);

        TestEmailDocumentLoaderJob job = runJob();

        assertNull(job.getException());
        String message = "The following documents couldn't be loaded as there is no record corresponding to the "
                         + "identifier:\n"
                         + "   Identifier: 0, Attachment: pathlab-0-a.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n";
        checkStatus(job, 0, 0, 0, 1, 0, 0, "Email Document Loader Job: Test Job failed with 1 error", "ERROR", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 0);
        checkCount(store, "error", 1);
    }

    /**
     * Verifies duplicate messages are excluded.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDuplicate() throws Exception {
        DocumentAct investigation = createInvestigation();
        Resource attachment = createAttachment("pathlab-" + investigation.getId() + "-.pdf");
        sendMessage("results@path.com", attachment);
        sendMessage("results@path.com", attachment);
        greenMail.waitForIncomingEmail(2);

        TestEmailDocumentLoaderJob job = runJob();

        checkInvestigation(investigation, attachment);

        long id = investigation.getId();
        String message = "The following documents were skipped, as they have already been loaded:\n"
                         + "   Identifier: " + id + ", Attachment: pathlab-" + id + "-.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n"
                         + "\n"
                         + "\n"
                         + "The following documents were loaded:\n"
                         + "   Identifier: " + id + ", Attachment: pathlab-" + id + "-.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n";

        checkStatus(job, 1, 1, 0, 0, 0, 0, "Email Document Loader Job: Test Job loaded 1 document", "COMPLETED", message,
                    null);

        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 2);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that if emails aren't moved to a different folder, they aren't subsequently reprocessed.
     *
     * @throws Exception for any error
     */
    @Test
    public void testProcessedEmailsNotReprocessed() throws Exception {
        DocumentAct investigation = createInvestigation();
        Resource attachment = createAttachment("pathlab-" + investigation.getId() + "-.pdf");
        sendMessage("results@path.com", attachment);
        sendMessage("results@path.com", createAttachment("random.pdf"));
        greenMail.waitForIncomingEmail(2);

        Entity config = createConfig(null, "INBOX", null, null);
        TestEmailDocumentLoaderJob job1 = runJob(config);

        checkInvestigation(investigation, attachment);

        assertNull(job1.getException());
        String message = "The following documents couldn't be loaded as there is no identifier in the attachment "
                         + "name:\n"
                         + "   Attachment: random.pdf, From: results@path.com, Subject: my subject, Received: x \n"
                         + "\n"
                         + "\n"
                         + "The following documents were loaded:\n"
                         + "   Identifier: " + investigation.getId()
                         + ", Attachment: pathlab-" + investigation.getId() + "-.pdf, From: results@path.com, "
                         + "Subject: my subject, Received: x \n";
        checkStatus(job1, 1, 0, 1, 0, 0, 0, "Email Document Loader Job: Test Job failed with 1 error", "ERROR", message,
                    null);

        checkCount(store, "INBOX", 2);

        // run the job again and verify nothing is loaded
        TestEmailDocumentLoaderJob job2 = runJob(config);
        assertNull(job2.getException());
        checkStatus(job2, 0, 0, 0, 0, 0, 0, null, null, null, null);
        checkCount(store, "INBOX", 2);
    }

    /**
     * Verifies an exception is caught if the source folder does not exist.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMissingSourceFolder() throws Exception {
        Entity config = createConfig(null, "badsource", null, null);
        TestEmailDocumentLoaderJob job = runJob(config);
        assertTrue(job.getException() instanceof FolderNotFoundException);
        checkStatus(job, 0, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job terminated unexpectedly", "ERROR",
                    "The load failed with error message: badsource not found", "badsource not found");
    }

    /**
     * Verifies an exception is caught if the target folder does not exist.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMissingTargetFolder() throws Exception {
        Entity config = createConfig(null, "INBOX", "badtarget", null);
        TestEmailDocumentLoaderJob job = runJob(config);
        assertTrue(job.getException() instanceof FolderNotFoundException);
        checkStatus(job, 0, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job terminated unexpectedly", "ERROR",
                    "The load failed with error message: badtarget not found",
                    "badtarget not found");
    }

    /**
     * Verifies an exception is caught if the error folder does not exist.
     *
     * @throws Exception for any error
     */
    @Test
    public void testMissingErrorFolder() throws Exception {
        Entity config = createConfig(null, "INBOX", "archive", "baderror");
        TestEmailDocumentLoaderJob job = runJob(config);
        assertTrue(job.getException() instanceof FolderNotFoundException);
        checkStatus(job, 0, 0, 0, 0, 0, 0, "Email Document Loader Job: Test Job terminated unexpectedly", "ERROR",
                    "The load failed with error message: baderror not found",
                    "baderror not found");
    }

    /**
     * Tests a failure loading attachments from an email.
     * <p/>
     * When an exception occurs, the email is left in the source folder rather than moved to the error folder.
     * If the error is transient, it will be processed on a subsequent poll.
     *
     * @throws Exception for any error
     */
    @Test
    public void testExceptionDuringProcessing() throws Exception {
        sendMessage("results@path.com", createAttachment("results.pdf"));
        greenMail.waitForIncomingEmail(1);

        Entity config = createConfig(null, "INBOX", "archive", "error");
        TestEmailDocumentLoaderJob job = new TestEmailDocumentLoaderJob(config) {
            @Override
            protected EmailLoader createLoader(Pattern idPattern,
                                               EmailLoaderListener listener) {
                return new EmailLoader(idPattern, getArchetypeService(), transactionManager, handlers,
                                       rules, listener) {
                    @Override
                    public boolean process(Email email) throws IOException {
                        throw new IOException("Random I/O exception");
                    }
                };
            }
        };
        job.execute(null);

        String message = "The following documents failed to load due to error:\n"
                         + "   Attachment: (unknown), From: results@path.com, Subject: my subject, "
                         + "Received: x - Random I/O exception\n";

        checkStatus(job, 0, 0, 0, 0, 0, 1, "Email Document Loader Job: Test Job failed with 1 error", "ERROR", message, null);
        checkCount(store, "INBOX", 1);
        checkCount(store, "archive", 0);
        checkCount(store, "error", 0);
    }

    /**
     * Verifies that attachments can only be loaded to investigations.
     */
    @Test
    public void testLoadToIncorrectAct() throws Exception {
        Party patient = patientFactory.createPatient();
        DocumentAct act = patientFactory.newAttachment()
                .patient(patient)
                .build();
        Resource attachment = createAttachment("pathlab-" + act.getId() + "-.pdf");
        sendMessage("results@pathlab.com", attachment);
        greenMail.waitForIncomingEmail(1);
        TestEmailDocumentLoaderJob job = runJob();

        long id = act.getId();
        String message = "The following documents couldn't be loaded as there is no record corresponding to the "
                         + "identifier:\n"
                         + "   Identifier: " + id + ", Attachment: pathlab-" + id + "-.pdf, "
                         + "From: results@pathlab.com, Subject: my subject, Received: x \n";

        checkStatus(job, 0, 0, 0, 1, 0, 0, "Email Document Loader Job: Test Job failed with 1 error", "ERROR", message, null);
        checkCount(store, "INBOX", 0);
        checkCount(store, "archive", 0);
        checkCount(store, "error", 1);
    }

    /**
     * Checks the job status.
     *
     * @param job           the job
     * @param loaded        the expected no. of loaded documents
     * @param alreadyLoaded the no. of documents that were already loaded
     * @param missingId     the no. of attachments with no id
     * @param missingAct    the no. of attachments with no corresponding act
     * @param noAttachment  the no. of emails no with attachments
     * @param errors        the no. of errors
     * @param subject       the expected subject
     * @param reason        the expected reason
     * @param message       the expected message
     * @param exception     the expected exception message
     */
    private void checkStatus(TestEmailDocumentLoaderJob job, int loaded, int alreadyLoaded, int missingId,
                             int missingAct, int noAttachment, int errors, String subject, String reason,
                             String message, String exception) {
        EmailLoaderListener listener = job.getListener();
        assertNotNull(listener);
        assertEquals(loaded, listener.getLoaded().size());
        assertEquals(alreadyLoaded, listener.getAlreadyLoaded().size());
        assertEquals(missingId, listener.getMissingId().size());
        assertEquals(missingAct, listener.getMissingAct().size());
        assertEquals(noAttachment, listener.getNoAttachments().size());
        assertEquals(errors, listener.getErrors().size());
        assertEquals(loaded + alreadyLoaded + missingId + missingAct + noAttachment + errors, listener.getProcessed());
        assertEquals(subject, job.getSubject());
        assertEquals(reason, job.getReason());
        String jobMessage = job.getMessage();
        if (jobMessage != null) {
            // replace dynamic timestamps to enable comparison
            jobMessage = jobMessage.replaceAll("Received: [^-\n]*", "Received: x ");
        }
        assertEquals(message, jobMessage);
        if (exception == null) {
            assertNull(job.getException());
        } else {
            assertNotNull(job.getException());
            assertEquals(exception, job.getException().getMessage());
        }
    }

    /**
     * Verifies an investigation has the expected attachment and versions
     *
     * @param act              the investigation
     * @param document         the expected document. May be {@code null}
     * @param documentVersions the expected document versions
     * @return the reloaded investigation
     * @throws IOException for any I/O error
     */
    private DocumentAct checkInvestigation(DocumentAct act, Resource document, Resource... documentVersions)
            throws IOException {
        act = get(act); // reload to work with the latest instance
        if (document == null) {
            assertNull(act.getDocument());
        } else {
            checkDocument(act, document);
        }
        IMObjectBean bean = getBean(act);
        List<DocumentAct> versions = bean.getTargets("versions", DocumentAct.class);
        assertEquals(documentVersions.length, versions.size());
        for (int i = 0; i < documentVersions.length; ++i) {
            checkDocument(versions.get(i), documentVersions[i]);
        }
        return act;
    }

    /**
     * Verifies an act has the expected attachment.
     *
     * @param act        the act
     * @param attachment the expected attachment
     * @throws IOException for any I/O error
     */
    private void checkDocument(DocumentAct act, Resource attachment) throws IOException {
        assertEquals(attachment.getFilename(), act.getFileName());
        assertEquals("application/pdf", act.getMimeType());
        assertNotNull(act.getDocument());
        Document document = get(act.getDocument(), Document.class);
        assertEquals(act.getMimeType(), document.getMimeType());
        byte[] attachmentBytes = IOUtils.toByteArray(attachment.getInputStream());
        byte[] docBytes = IOUtils.toByteArray(handlers.get(document).getContent(document));
        assertEquals(attachmentBytes.length, docBytes.length);
        assertEquals(docBytes.length, document.getSize());
        assertNotNull(document);
        assertEquals(attachment.getFilename(), document.getName());
    }

    /**
     * Creates an investigation.
     *
     * @return the investigation
     */
    private DocumentAct createInvestigation() {
        Party patient = patientFactory.createPatient();
        return patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(laboratoryFactory.createInvestigationType())
                .build();
    }

    /**
     * Runs the job.
     *
     * @return the job
     * @throws JobExecutionException if job execution fails
     */
    private TestEmailDocumentLoaderJob runJob() throws JobExecutionException {
        Entity config = createConfig(null, "INBOX", "archive", "error");
        return runJob(config);
    }

    /**
     * Runs the job.
     *
     * @param config the job configuration
     * @return the job
     * @throws JobExecutionException if job execution fails
     */
    private TestEmailDocumentLoaderJob runJob(Entity config) throws JobExecutionException {
        TestEmailDocumentLoaderJob job = new TestEmailDocumentLoaderJob(config);
        job.execute(null);
        return job;
    }

    /**
     * Checks the number of messages in a folder matches that expected.
     *
     * @param store the store
     * @param name  the folder name
     * @param count the expected message count
     * @throws Exception for any error
     */
    private void checkCount(Store store, String name, int count) throws Exception {
        try (Folder folder = store.getFolder(name)) {
            folder.open(Folder.READ_WRITE);
            folder.expunge(); // remove any messages flagged as deleted
            assertEquals(count, folder.getMessageCount());
        }
    }

    /**
     * Creates a job configuration.
     *
     * @param from         the from address. May be {@code null}
     * @param sourceFolder the source folder
     * @param targetFolder the target folder. May be {@code null}
     * @param errorFolder  the error folder. May be {@code null}
     * @return the configuration
     */
    private Entity createConfig(String from, String sourceFolder, String targetFolder, String errorFolder) {
        Entity config = create("entity.jobEmailDocumentLoader", Entity.class);
        IMObjectBean bean = getBean(config);
        bean.setValue("name", "Test Job");
        bean.setValue("host", "localhost");
        bean.setValue("port", ServerSetupTest.IMAP.getPort());
        bean.setValue("username", "foo@gmail.com");
        bean.setValue("password", "bar");
        bean.setValue("from", from);
        bean.setValue("sourceFolder", sourceFolder);
        bean.setValue("targetFolder", targetFolder);
        bean.setValue("errorFolder", errorFolder);
        bean.setTarget("notify", userFactory.createUser());
        return config;
    }

    /**
     * Creates a new mime message with an attachment with the specified file name.
     *
     * @param from        the from address
     * @param attachments the attachments
     * @throws MessagingException for any mail error
     */
    private void sendMessage(String from, Resource... attachments) throws MessagingException {
        Transport.send(createMessage(from, attachments));
    }

    /**
     * Creates a new mime message with an attachment with the specified file name.
     *
     * @param from        the from address
     * @param attachments the attachments
     * @return a new mime message
     * @throws MessagingException for any mail error
     */
    private MimeMessage createMessage(String from, Resource... attachments) throws MessagingException {
        MimeMessageHelper message = new MimeMessageHelper(new MimeMessage(session), true, "UTF-8");
        message.setFrom(from);
        message.setTo("foo@gmail.com");
        message.setSubject("my subject");
        message.setText("my text <img src='cid:logo'>", true);
        message.addInline("logo", createAttachment("logo.gif"));
        for (Resource attachment : attachments) {
            assertNotNull(attachment.getFilename());
            message.addAttachment(attachment.getFilename(), attachment);
        }
        return message.getMimeMessage();
    }

    /**
     * Creates a random resource for an email.
     *
     * @param fileName the file name
     * @return the resource
     */
    private Resource createAttachment(String fileName) {
        byte[] data = RandomUtils.nextBytes(10);
        return new InMemoryResource(data, fileName) {
            @Override
            public String getFilename() {
                return getDescription();
            }
        };
    }

    private class TestEmailDocumentLoaderJob extends EmailDocumentLoaderJob {

        private EmailLoaderListener listener;

        private Throwable exception;

        private String subject;

        private String reason;

        private String text;

        public TestEmailDocumentLoaderJob(Entity config) {
            super(config, (IArchetypeRuleService) getArchetypeService(), transactionManager, handlers, rules,
                  passwordResolver);
        }

        public EmailLoaderListener getListener() {
            return listener;
        }

        public Throwable getException() {
            return exception;
        }

        public String getSubject() {
            return subject;
        }

        public String getReason() {
            return reason;
        }

        public String getMessage() {
            return text;
        }

        /**
         * Invoked on completion of a job. Sends a message notifying the registered users of completion or failure of
         * the job.
         *
         * @param listener  the loader listener
         * @param exception the exception, if the job failed, otherwise {@code null}
         */
        @Override
        protected void complete(EmailLoaderListener listener, Throwable exception) {
            this.listener = listener;
            this.exception = exception;
            super.complete(listener, exception);
        }

        /**
         * Notifies users.
         *
         * @param users   the users to notify.
         * @param subject the subject
         * @param reason  the reason
         * @param text    the message text
         */
        @Override
        protected void notifyUsers(Set<User> users, String subject, String reason, String text) {
            super.notifyUsers(users, subject, reason, text);
            this.subject = subject;
            this.reason = reason;
            this.text = text;
        }
    }
}