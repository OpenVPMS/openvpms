/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.openvpms.web.jobs.util.ArchetypeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link AccountReminderJob}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AccountReminderJobTestCase extends AbstractAccountReminderJobTest {

    /**
     * The archetype services.
     */
    @Autowired
    private ArchetypeServices services;

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules customerRules;

    /**
     * The account reminder rules.
     */
    @Autowired
    private AccountReminderRules accountReminderRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party practice = practiceFactory.newPractice().build(false);
        practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getPractice()).thenReturn(practice);
    }

    /**
     * Tests the job.
     */
    @Test
    public void testJob() {
        Entity config = createConfig(BigDecimal.ZERO);
        Date now = new Date();
        Party customer = createCustomer("12345678");
        FinancialAct invoice1 = createInvoice(customer);
        checkEquals(BigDecimal.ZERO, invoice1.getAllocatedAmount());
        runJob(config, now);
        checkReminder(invoice1, now, 0, null);
        Date start2 = DateRules.getDate(now, 2, DateUnits.WEEKS);
        runJob(config, start2);
        checkReminder(invoice1, now, 1, null);

        Date start3 = DateRules.getDate(now, 4, DateUnits.WEEKS);
        runJob(config, start3);
        checkReminder(invoice1, now, 2, null);
    }

    /**
     * Verifies reminders are only generated for POSTED invoices and counter sales.
     */
    @Test
    public void testReminderForStatus() {
        checkReminderForStatus(true);  // invoice
        checkReminderForStatus(false); // counter sale
    }

    /**
     * Verifies that reminders are generated for invoices with a balance >= the minimum balance, but not for those
     * below the minimum balance.
     */
    @Test
    public void testMinBalance() {
        Party customer1 = createCustomer("1");
        Party customer2 = createCustomer("2");
        Party customer3 = createCustomer("3");
        Party customer4 = createCustomer("4");
        Party customer5 = createCustomer("5");
        Party customer6 = createCustomer("6");

        Entity config = createConfig(BigDecimal.TEN);

        FinancialAct invoice1 = createInvoice(customer1, 20);
        FinancialAct invoice2 = createInvoice(customer2, 10);  // same as min balance, so should generate reminder
        FinancialAct invoice3 = createInvoice(customer3, 5);
        FinancialAct invoice4 = createInvoice(customer4, 100);
        pay(customer4, 90); // pay invoice4 to the min balance

        FinancialAct invoice5 = createInvoice(customer5, 100);
        pay(customer5, 95); // pay invoice5 to below the minimum balance

        FinancialAct invoice6 = createInvoice(customer6, 100);
        pay(customer6, 100); // pay invoice6

        Date now = new Date();
        runJob(config, now);
        checkReminder(invoice1, now, 0, null);
        checkReminder(invoice2, now, 0, null);
        checkNoReminder(invoice3);
        checkReminder(invoice4, now, 0, null);
        checkNoReminder(invoice5);
        checkNoReminder(invoice6);
    }

    /**
     * Verifies that reminders are only generated if the invoice is overdue with regard to the customer's payment
     * terms.
     */
    @Test
    public void testReminderForPaymentTerms() {
        Party customer1 = createCustomer("1");  // due immediately. The first job
        Party customer2 = createCustomer("2", 1, DateUnits.WEEKS);
        Party customer3 = createCustomer("3", 30, DateUnits.DAYS);

        Entity config = createConfig(BigDecimal.ZERO);
        // reminder 0 - generated from 2 days -> 2 weeks overdue
        // reminder 1 - generated from 2 weeks -> 4 weeks overdue
        // reminder 2 - generated from 4 weeks -> 1 year overdue

        Date today = DateRules.getToday();
        Date yesterday = DateRules.getYesterday();
        Date sevenDays = DateRules.getDate(today, -1, DateUnits.WEEKS);
        Date eightDays = DateRules.getDate(today, -8, DateUnits.DAYS);
        Date thirtyDays = DateRules.getDate(today, -30, DateUnits.DAYS);
        Date thirtyOneDays = DateRules.getDate(today, -31, DateUnits.DAYS);

        FinancialAct invoice1 = createInvoice(today, customer1, 10);
        FinancialAct invoice1b = createInvoice(yesterday, customer1, 10);
        FinancialAct invoice1c = createInvoice(sevenDays, customer1, 10);
        FinancialAct invoice1d = createInvoice(eightDays, customer1, 10);
        FinancialAct invoice1e = createInvoice(thirtyDays, customer1, 10);
        FinancialAct invoice1f = createInvoice(thirtyOneDays, customer1, 10);

        FinancialAct invoice2a = createInvoice(today, customer2, 10);
        FinancialAct invoice2b = createInvoice(yesterday, customer2, 10);
        FinancialAct invoice2c = createInvoice(sevenDays, customer2, 10);
        FinancialAct invoice2d = createInvoice(eightDays, customer2, 10);
        FinancialAct invoice2e = createInvoice(thirtyDays, customer2, 10);
        FinancialAct invoice2f = createInvoice(thirtyOneDays, customer2, 10);

        FinancialAct invoice3a = createInvoice(today, customer3, 10);
        FinancialAct invoice3b = createInvoice(yesterday, customer3, 10);
        FinancialAct invoice3c = createInvoice(sevenDays, customer3, 10);
        FinancialAct invoice3d = createInvoice(eightDays, customer3, 10);
        FinancialAct invoice3e = createInvoice(thirtyDays, customer3, 10);
        FinancialAct invoice3f = createInvoice(thirtyOneDays, customer3, 10);

        Date now = new Date();
        runJob(config, now);

        // check customer1 - no payment terms. As the first reminder starts at 2 days, the first 2 invoices won't get a
        // reminder
        checkNoReminder(invoice1);
        checkNoReminder(invoice1b);
        checkReminder(invoice1c, now, 0, null);
        checkReminder(invoice1d, now, 0, null);
        checkReminder(invoice1e, now, 2, null);
        checkReminder(invoice1f, now, 2, null);

        // check customer2 - 7 days payment terms.
        checkNoReminder(invoice2a);
        checkNoReminder(invoice2b);
        checkNoReminder(invoice2c);
        checkReminder(invoice2d, now, 0, null);
        checkReminder(invoice2e, now, 2, null);
        checkReminder(invoice2f, now, 2, null);

        // check customer3 - 30 days payment terms
        checkNoReminder(invoice3a);
        checkNoReminder(invoice3b);
        checkNoReminder(invoice3c);
        checkNoReminder(invoice3d);
        checkNoReminder(invoice3e);
        checkReminder(invoice3f, now, 2, null);
    }

    /**
     * Verifies no reminder is generated if the invoice {@code sendReminder} flag is {@code false}.
     */
    @Test
    public void testNoReminderForSendReminderDisabled() {
        Party customer = createCustomer("12345678");
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer, 20, true, false);
        Entity config = createConfig(BigDecimal.TEN);
        runJob(config, now);
        checkNoReminder(invoice);

        // now re-run for an existing reminder, and verify it isn't updated. This is because invoices with
        // sendReminder == false are excluded by the query
        Act reminder = addReminder(invoice, ReminderItemStatus.PENDING);
        runJob(config, now);
        checkReminderStatus(reminder, ReminderItemStatus.PENDING);
    }

    /**
     * Verifies no reminder is generated if the customer is inactive.
     */
    @Test
    public void testNoReminderForInactiveCustomer() {
        Party customer = customerFactory.newCustomer()
                .addMobilePhone("12345678")
                .active(false)
                .build();
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer);
        Entity config = createConfig(BigDecimal.TEN);
        runJob(config, now);

        checkNoReminder(invoice);

        // now re-run for an existing reminder, and verify it isn't updated. This is because inactive customers
        // are excluded by the query.
        Act reminder = addReminder(invoice, ReminderItemStatus.PENDING);
        runJob(config, now);
        checkReminderStatus(reminder, ReminderItemStatus.PENDING);
    }

    /**
     * Verifies no reminder is generated if the customer has no mobile phone.
     */
    @Test
    public void testNoReminderForNoMobile() {
        Party customer = customerFactory.newCustomer()
                .addHomePhone("12345678")
                .build();
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer);
        Entity config = createConfig(BigDecimal.TEN);
        runJob(config, now);
        checkNoReminder(invoice);

        // now re-run for an existing reminder, and verify it is set to ERROR.
        Act reminder = addReminder(invoice, ReminderItemStatus.PENDING);
        runJob(config, now);
        checkReminder(invoice, now, 0, "Customer has no SMS contact");
        checkReminderStatus(reminder, ReminderItemStatus.ERROR);
    }

    /**
     * Verifies no reminder is generated if the customer and invoice have no location.
     */
    @Test
    public void testNoReminderForNoLocation() {
        Party customer = createCustomer("12345678");
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer, 20, false, true);
        Entity config = createConfig(BigDecimal.TEN);
        runJob(config, now);
        checkNoReminder(invoice);

        // now re-run for an existing reminder, and verify it is set to ERROR.
        Act reminder = addReminder(invoice, ReminderItemStatus.PENDING);
        runJob(config, now);
        checkReminder(invoice, now, 0, "Cannot determine the practice location of the Invoice");
        checkReminderStatus(reminder, ReminderItemStatus.ERROR);
    }

    /**
     * Verify an error is raised if the SMS template is invalid.
     */
    @Test
    public void testInvalidSMSTemplate() {
        Party customer = createCustomer("12345678");
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer);
        Entity template = createXPathTemplate("bad");
        Entity config = createConfig(BigDecimal.ZERO, template);
        runJob(config, now);
        Act reminder = checkReminder(invoice, now, 0, "Generated SMS text was empty");

        // re-run with a correct template and verify the reminder updates
        IMObjectBean bean = getBean(template);
        bean.setValue("content", "'good'");
        bean.save();
        runJob(config, now);
        checkReminder(invoice, now, 0, null);
        checkReminderStatus(reminder, ReminderItemStatus.COMPLETED);
    }

    /**
     * Verify an error is raised if the SMS text is too long.
     */
    @Test
    public void testSMSTooLong() {
        Party customer = createCustomer("12345678");
        Date now = new Date();
        FinancialAct invoice = createInvoice(customer);
        String text = RandomStringUtils.randomAlphabetic(161);
        Entity template = createXPathTemplate("'" + text + "'");
        Entity config = createConfig(BigDecimal.ZERO, template);
        runJob(config, now);
        Act reminder = checkReminder(invoice, now, 0, "SMS is too long: " + text);

        // re-run with a correct template and verify the reminder updates
        IMObjectBean bean = getBean(template);
        bean.setValue("content", "'good'");
        bean.save();
        runJob(config, now);
        checkReminder(invoice, now, 0, null);
        checkReminderStatus(reminder, ReminderItemStatus.COMPLETED);
    }

    /**
     * Verifies that if an invoice has a PENDING or ERROR reminder, it is CANCELLED if a subsequent reminder is
     * successfully generated.
     */
    @Test
    public void testOldReminderCancelled() {
        Party customer = createCustomer("12345678");
        Date now = new Date();
        FinancialAct invoice1 = createInvoice(customer);
        Act reminder1 = addReminder(invoice1, ReminderItemStatus.PENDING);
        FinancialAct invoice2 = createInvoice(customer);
        Act reminder2 = addReminder(invoice2, ReminderItemStatus.COMPLETED);
        FinancialAct invoice3 = createInvoice(customer);
        Act reminder3 = addReminder(invoice3, ReminderItemStatus.CANCELLED);
        FinancialAct invoice4 = createInvoice(customer);
        Act reminder4 = addReminder(invoice4, ReminderItemStatus.ERROR);

        Date start = DateRules.getDate(now, 2, DateUnits.WEEKS);
        runJob(createConfig(BigDecimal.ZERO), start);

        // verify the reminderCount 0 reminders have been cancelled, except for those that were COMPLETED
        checkReminderStatus(reminder1, ReminderItemStatus.CANCELLED);
        checkReminderStatus(reminder2, ReminderItemStatus.COMPLETED); // unchanged
        checkReminderStatus(reminder3, ReminderItemStatus.CANCELLED); // unchanged
        checkReminderStatus(reminder4, ReminderItemStatus.CANCELLED);

        // verify there is a reminderCount 1 reminder for each invoice
        checkReminder(invoice1, now, 1, null);
        checkReminder(invoice2, now, 1, null);
        checkReminder(invoice3, now, 1, null);
        checkReminder(invoice4, now, 1, null);
    }

    /**
     * Verifies that no reminders are generated for invoices in a gap claim.
     */
    @Test
    public void testNoReminderForInvoiceInGapClaim() {
        Party customer = createCustomer("12345678");
        FinancialAct invoice = createInvoice(customer);
        IMObjectBean bean = getBean(invoice);
        FinancialAct item = bean.getTarget("items", FinancialAct.class);
        assertNotNull(item);

        Act policy = insuranceFactory.createPolicy(customer, patientFactory.createPatient(customer),
                                                   insuranceFactory.createInsurer(), "ABCDEFG");

        User clinician = userFactory.createClinician();
        insuranceFactory.newClaim()
                .policy(policy)
                .location(bean.getTarget("location", Party.class))
                .clinician(clinician)
                .claimHandler(clinician)
                .gapClaim(true)
                .item()
                .diagnosis("VENOM_328", "Abcess", "328")
                .invoiceItems(item)
                .add()
                .build();

        Date now = new Date();
        Entity config = createConfig(BigDecimal.TEN);
        runJob(config, now);
        checkNoReminder(invoice);

        // now re-run for an existing reminder, and verify it is cancelled as the invoice is in a gap claim
        Act reminder = addReminder(invoice, ReminderItemStatus.PENDING);
        runJob(config, now);
        checkReminderStatus(reminder, ReminderItemStatus.CANCELLED);
    }

    /**
     * Verifies that no reminders are generated for invoices completed prior to the <em>ignoreInterval</em>
     */
    @Test
    public void testNoReminderForOldInvoices() {
        Party customer = createCustomer("12345678");
        FinancialAct invoice1 = createInvoice(customer);
        invoice1.setActivityEndTime(DateRules.getDate(DateRules.getYesterday(), -1, DateUnits.YEARS));

        FinancialAct invoice2 = createInvoice(customer);
        invoice2.setActivityEndTime(DateRules.getDate(DateRules.getToday(), -364, DateUnits.DAYS));
        save(invoice1, invoice2);

        Date now = new Date();
        runJob(createConfig(BigDecimal.ZERO), now); // ignoreInterval = 1 YEAR

        checkNoReminder(invoice1);
        checkReminder(invoice2, now, 2, null);
    }

    /**
     * Verifies reminders are only generated for POSTED invoices and counter sales.
     *
     * @param invoice if {@code true} check invoices, else check counter sales
     */
    private void checkReminderForStatus(boolean invoice) {
        Party customer = createCustomer("1");
        FinancialAct charge1 = createCharge(customer, FinancialActStatus.IN_PROGRESS, invoice);
        FinancialAct charge2 = createCharge(customer, FinancialActStatus.COMPLETED, invoice);
        FinancialAct charge3 = createCharge(customer, FinancialActStatus.ON_HOLD, invoice);
        FinancialAct charge4 = createCharge(customer, FinancialActStatus.POSTED, invoice);
        Date now = new Date();
        runJob(createConfig(BigDecimal.ZERO), now);
        checkNoReminder(charge1);
        checkNoReminder(charge2);
        checkNoReminder(charge3);
        checkReminder(charge4, now, 0, null);
    }

    private FinancialAct createCharge(Party customer, String status, boolean invoice) {
        return invoice ? createInvoice(customer, status) : createCounterSale(customer, status);
    }

    /**
     * Verifies a reminder has the expected status.
     *
     * @param reminder the reminder
     * @param status   the expected status
     */
    private void checkReminderStatus(Act reminder, String status) {
        reminder = get(reminder);
        assertEquals(status, reminder.getStatus());
    }

    /**
     * Adds a reminder to a charge with the specified status.
     *
     * @param charge the charge
     * @param status the reminder status
     * @return the reminder
     */
    private Act addReminder(FinancialAct charge, String status) {
        IMObjectBean bean = getBean(charge);
        Act act = create(AccountReminderArchetypes.CHARGE_REMINDER_SMS, Act.class);
        IMObjectBean reminder = getBean(act);
        reminder.setValue("startTime", new Date());
        reminder.setValue("count", 0);
        reminder.setValue("status", status);
        bean.addTarget("reminders", act, "charge");
        bean.save(act);
        return act;
    }

    /**
     * Makes a payment.
     *
     * @param customer the customer
     * @param amount   the amount to pay
     */
    private void pay(Party customer, int amount) {
        accountFactory.newPayment()
                .customer(customer)
                .cash(amount)
                .till(practiceFactory.createTill())
                .status(FinancialActStatus.POSTED)
                .build();
    }

    /**
     * Creates a customer with a mobile phone.
     *
     * @param phone the phone number
     * @return a new customer
     */
    private Party createCustomer(String phone) {
        return customerFactory.newCustomer()
                .addMobilePhone(phone)
                .build();
    }

    private Party createCustomer(String phone, int paymentTerms, DateUnits units) {
        return customerFactory.newCustomer()
                .addMobilePhone(phone)
                .addClassifications(customerFactory.newAccountType().paymentTerms(paymentTerms, units).build())
                .build();
    }

    /**
     * Creates an invoice dated 3 days ago for a total of $20.
     *
     * @param customer the customer
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer) {
        return createInvoice(customer, 20);
    }

    /**
     * Creates an invoice dated 3 days ago for a total of $20.
     *
     * @param customer the customer
     * @param status   the invoice status
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer, String status) {
        return createInvoice(customer, 20, true, true, status);
    }

    /**
     * Creates an invoice dated 3 days ago.
     *
     * @param customer the customer
     * @param amount   the invoice amount
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer, int amount) {
        return createInvoice(customer, amount, true, true);
    }

    /**
     * Creates an invoice dated 3 days ago.
     *
     * @param customer     the customer
     * @param amount       the invoice amount
     * @param location     if {@code true}, attach a location to the invoice
     * @param sendReminder determines if a reminder should be sent
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer, int amount, boolean location, boolean sendReminder) {
        return createInvoice(customer, amount, location, sendReminder, FinancialActStatus.POSTED);
    }

    /**
     * Creates an invoice dated 3 days ago.
     *
     * @param customer     the customer
     * @param amount       the invoice amount
     * @param location     if {@code true}, attach a location to the invoice
     * @param sendReminder determines if a reminder should be sent
     * @param status       the invoice status
     * @return a new invoice
     */
    private FinancialAct createInvoice(Party customer, int amount, boolean location, boolean sendReminder,
                                       String status) {
        Date date = DateRules.getDate(DateRules.getToday(), -3, DateUnits.DAYS);
        TestInvoiceBuilder builder = newInvoice(date, customer, amount, sendReminder, status);
        if (location) {
            builder.location(practiceFactory.createLocation());
        }
        return builder.build();
    }

    /**
     * Creates a POSTED invoice with the specified date and <em>sendReminder</em> flag set.
     *
     * @param date     the date
     * @param customer the customer
     * @param amount   the amount
     * @return a new invoice
     */
    private FinancialAct createInvoice(Date date, Party customer, int amount) {
        return newInvoice(date, customer, amount, true, FinancialActStatus.POSTED)
                .location(practiceFactory.createLocation())
                .build();
    }

    /**
     * Returns an invoice builder with details populated.
     *
     * @param customer     the customer
     * @param amount       the invoice amount
     * @param sendReminder determines if a reminder should be sent
     * @param status       the invoice status
     * @return a new invoice builder
     */
    private TestInvoiceBuilder newInvoice(Date date, Party customer, int amount, boolean sendReminder, String status) {
        return accountFactory.newInvoice()
                .customer(customer)
                .item()
                .patient(patientFactory.createPatient(customer))
                .product(productFactory.createMedication())
                .unitPrice(amount)
                .add()
                .status(status)
                .startTime(date)
                .endTime(date)
                .sendReminder(sendReminder);
    }

    /**
     * Creates a counter sale dated 3 days ago with the specified status with {@code sendReminder=true}.
     *
     * @param customer the customer
     * @param status   the status
     * @return a new counter sale
     */
    private FinancialAct createCounterSale(Party customer, String status) {
        Date date = DateRules.getDate(DateRules.getToday(), -3, DateUnits.DAYS);
        return accountFactory.newCounterSale()
                .customer(customer)
                .item()
                .medicationProduct()
                .unitPrice(20)
                .add()
                .status(status)
                .startTime(date)
                .endTime(date)
                .sendReminder(true)
                .location(practiceFactory.createLocation())
                .build();
    }

    /**
     * Verifies the reminder information associated with an invoice matches that expected.
     *
     * @param invoice       the invoice to check
     * @param sentOnOrAfter the reminderSent should be on or after this date
     * @param reminderCount the expected reminder count
     * @param error         the expected error message, or {@code null} if no error is expected
     * @return the reminder}
     */
    private Act checkReminder(FinancialAct invoice, Date sentOnOrAfter, int reminderCount, String error) {
        Act result = null;
        IMObjectBean bean = getBean(get(invoice));
        assertEquals(reminderCount, bean.getInt("reminderCount"));
        if (error == null) {
            assertNotNull(bean.getDate("reminderSent"));
            assertTrue(bean.getDate("reminderSent").compareTo(sentOnOrAfter) >= 0);
        } else {
            assertNull(bean.getDate("reminderSent"));
        }
        int found = 0;
        for (Act act : bean.getTargets("reminders", Act.class)) {
            IMObjectBean reminder = getBean(act);
            if (reminder.getInt("count") == reminderCount) {
                result = act;
                found++;
                if (error == null) {
                    assertEquals(ReminderItemStatus.COMPLETED, act.getStatus());
                } else {
                    assertEquals(ReminderItemStatus.ERROR, act.getStatus());
                    assertEquals(error, reminder.getString("error"));
                }
            }
        }
        assertEquals(1, found);
        return result;
    }

    /**
     * Verifies no reminders have been generated for an invoice.
     *
     * @param invoice the invoice
     */
    private void checkNoReminder(FinancialAct invoice) {
        IMObjectBean bean = getBean(get(invoice));
        assertNull(bean.getDate("reminderSent"));
        assertNull(bean.getObject("reminderCount"));
        assertEquals(0, bean.getValues("reminders").size());
    }

    /**
     * Runs the job.
     *
     * @param config    the job configuration
     * @param startDate the date/time to base date calculations on
     */
    private void runJob(Entity config, Date startDate) {
        AccountReminderJob job = createJob(config, startDate);
        job.execute();
    }

    /**
     * Creates a new job.
     *
     * @param config the job configuration
     * @param date   the date to start from
     * @return a new job
     */
    private AccountReminderJob createJob(Entity config, Date date) {
        return new TestAccountReminderJob(config, date, createSMSService(), services, getLookupService(),
                                          customerRules, accountReminderRules, practiceService,
                                          smsTemplateEvaluator, transactionManager);
    }

    /**
     * Creates the job configuration.
     *
     * @param minBalance the minimum balance
     * @return the job configuration
     */
    private Entity createConfig(BigDecimal minBalance) {
        Entity template = createXPathTemplate("concat('Your invoice is due for payment. Balance=', $balance)");
        return createConfig(minBalance, template);
    }


    private static class TestAccountReminderJob extends AccountReminderJob {

        private final Date startDate;

        /**
         * Constructs a {@link TestAccountReminderJob}.
         *
         * @param configuration        the job configuration
         * @param startDate            the from date
         * @param smsService           the SMS service
         * @param services             the archetype services
         * @param lookups              the lookup service
         * @param customerRules        the customer rules
         * @param accountReminderRules the insurance rules
         * @param practiceService      the practice service
         * @param templateEvaluator    the SMS template evaluator
         * @param transactionManager   the transaction manager
         */
        public TestAccountReminderJob(Entity configuration, Date startDate, SimpleSMSService smsService,
                                      ArchetypeServices services, LookupService lookups, CustomerRules customerRules,
                                      AccountReminderRules accountReminderRules, PracticeService practiceService,
                                      SMSTemplateEvaluator templateEvaluator,
                                      PlatformTransactionManager transactionManager) {
            super(configuration, smsService, services, lookups, customerRules, accountReminderRules, practiceService,
                  templateEvaluator, transactionManager);
            this.startDate = startDate;
        }

        /**
         * Returns the date/time to base date calculations on.
         *
         * @return the start date
         */
        @Override
        protected Date getStartDate() {
            return startDate;
        }
    }

}