/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.statement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.finance.statement.StatementService;
import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.job.statement.TestEndPeriodJobBuilder;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link EndPeriodJob}.
 *
 * @author Tim Anderson
 */
public class EndPeriodJobTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The statement service.
     */
    private StatementService statementService;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        statementService = Mockito.mock(StatementService.class);
    }

    /**
     * Verifies that the statement date is calculated as the end-of-month prior to the current date.
     */
    @Test
    public void testStatementDate() {
        Entity config = new TestEndPeriodJobBuilder(getArchetypeService()).build(false);
        EndPeriodJob job1 = createJob(config, getDate("2021-12-01"));
        assertEquals(getDate("2021-11-30"), job1.getStatementDate());

        EndPeriodJob job2 = createJob(config, getDate("2021-12-31"));
        assertEquals(getDate("2021-11-30"), job2.getStatementDate());

        EndPeriodJob job3 = createJob(config, getDate("2022-01-01"));
        assertEquals(getDate("2021-12-31"), job3.getStatementDate());
    }

    /**
     * Verifies the system message sent the 'notify' user, on successful job completion.
     */
    @Test
    public void testJob() {
        checkJob(false);
        checkJob(true);
    }

    /**
     * Verifies the system message sent the 'notify' user, after job interruption.
     */
    @Test
    public void testJobInterruption() {
        Party customer = customerFactory.createCustomer();
        User user = userFactory.createUser();
        Entity config = new TestEndPeriodJobBuilder(getArchetypeService())
                .runAs(user)
                .notify(user)
                .build(false);
        Date now = getDate("2021-03-01");
        Date statementDate = getDate("2021-02-28");
        EndPeriodJob job = createJob(config, now, customer);
        job.interrupt();
        job.execute(null);
        checkMessage(user, SystemMessageReason.ERROR, "End Period interrupted for statement date "
                                                      + DateFormatter.formatDate(statementDate, false),
                     "The job was terminated before completion.\n\n0 customers were processed in .*\\.");
    }

    /**
     * Verifies the system message sent the 'notify' user, after job error.
     */
    @Test
    public void testJobFailure() {
        User user = userFactory.createUser();
        Entity config = new TestEndPeriodJobBuilder(getArchetypeService())
                .runAs(user)
                .notify(user)
                .build(false);
        Date now = getDate("2021-12-28");
        Date statementDate = getDate("2021-11-30");
        EndPeriodJob job = new EndPeriodJob(config, statementService, (IArchetypeRuleService) getArchetypeService()) {
            @Override
            protected void process(EndPeriodJob.Processor processor) {
                throw new IllegalStateException("Some error");
            }

            @Override
            protected Date getNow() {
                return now;
            }
        };
        job.execute(null);
        checkMessage(user, SystemMessageReason.ERROR,
                     "End Period terminated unexpectedly for statement date " +
                     DateFormatter.formatDate(statementDate, false),
                     "The job failed with error message: Some error\n\n" +
                     "Prior to termination, 0 customers were processed in .*\\.");
    }

    /**
     * Verifies the system message sent the 'notify' user, on successful job completion.
     *
     * @param postCompletedCharges if {@code true} post completed charges
     */
    private void checkJob(boolean postCompletedCharges) {
        Mockito.reset(statementService);
        User user = userFactory.createUser();
        Entity config1 = new TestEndPeriodJobBuilder(getArchetypeService())
                .runAs(user)
                .notify(user)
                .postCompletedCharges(postCompletedCharges)
                .build(false);

        Party customer1 = customerFactory.createCustomer();
        Party customer2 = customerFactory.newCustomer().active(false).build();
        Date now = getDate("2021-12-01");
        Date statementDate = getDate("2021-11-30");
        EndPeriodJob job = createJob(config1, now, customer1, customer2);
        job.execute(null);
        checkMessage(user, SystemMessageReason.COMPLETED, "End Period completed for statement date " +
                                                          DateFormatter.formatDate(statementDate, false),
                     "2 customers were processed in .*\\.");
        verify(statementService, times(2)).endPeriod(any(), any(), anyBoolean());
        verify(statementService).endPeriod(customer1, statementDate, postCompletedCharges);
        verify(statementService).endPeriod(customer2, statementDate, postCompletedCharges);
    }

    /**
     * Verifies a message exists with content matching that expected.
     *
     * @param user          the user
     * @param reason        the expected reason
     * @param subject       the expected subject
     * @param messageRegexp the expected message, as a regular expression
     */
    private void checkMessage(User user, String reason, String subject, String messageRegexp) {
        IArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, MessageArchetypes.SYSTEM);
        Join<Act, IMObject> to = root.join("to");
        to.on(builder.equal(to.get("entity"), user.getObjectReference()));
        Act act = service.createQuery(query).getSingleResult();
        assertEquals(subject, act.getDescription());
        assertEquals(reason, act.getReason());
        IMObjectBean bean = getBean(act);
        assertTrue(bean.getString("message").matches(messageRegexp));
    }

    /**
     * Creates a job that only performs end-of-period for the specified customers.
     *
     * @param config    the job configuration
     * @param customers the customers
     * @param now       the date/time to make the statement date relative to
     * @return a new job
     */
    private EndPeriodJob createJob(Entity config, Date now, Party... customers) {
        return new EndPeriodJob(config, statementService, (IArchetypeRuleService) getArchetypeService()) {
            @Override
            protected Iterator<Party> getCustomers() {
                return Arrays.asList(customers).iterator();
            }

            @Override
            protected Date getNow() {
                return now;
            }
        };
    }
}