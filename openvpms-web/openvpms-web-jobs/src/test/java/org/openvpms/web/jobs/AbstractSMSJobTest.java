/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs;

import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestSMSTemplateBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.internal.message.OutboundMessageBuilderImpl;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.InboundMessageBuilder;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessageBuilder;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;

/**
 * Base class for testing jobs that send SMSes.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractSMSJobTest extends ArchetypeServiceTest {

    /**
     * The SMS template evaluator.
     */
    @Autowired
    protected SMSTemplateEvaluator smsTemplateEvaluator;

    /**
     * The document factory.
     */
    @Autowired
    protected TestDocumentFactory documentFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Creates a mock SMS service that supports single part messages.
     *
     * @return a new service
     */
    protected SimpleSMSService createSMSService() {
        SMSService smsService = new TestSMSService();
        return new SimpleSMSService(smsService, getArchetypeService());
    }

    /**
     * Returns a builder for an SMS template.
     *
     * @param archetype the SMS template archetype
     * @return a new builder
     */
    protected TestSMSTemplateBuilder newSMSTemplate(String archetype) {
        return documentFactory.newSMSTemplate(archetype);
    }

    protected class TestSMSService implements SMSService {
        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public int getMaxParts() {
            return 1;
        }

        @Override
        public void send(OutboundMessage message) {
            message.setStatus(OutboundMessage.Status.SENT);
        }

        @Override
        public int getParts(String message) {
            return SMSLengthCalculator.getParts(message);
        }

        @Override
        public OutboundMessage getOutboundMessage(long id) {
            return null;
        }

        @Override
        public OutboundMessage getOutboundMessage(String archetype, String id) {
            return null;
        }

        @Override
        public OutboundMessageBuilder getOutboundMessageBuilder() {
            return new OutboundMessageBuilderImpl(getArchetypeService(), domainService);
        }

        @Override
        public Iterable<OutboundMessage> getSent(OffsetDateTime from, String archetype) {
            return null;
        }

        @Override
        public InboundMessage getInboundMessage(long id) {
            return null;
        }

        @Override
        public InboundMessage getInboundMessage(String archetype, String id) {
            return null;
        }

        @Override
        public InboundMessageBuilder getInboundMessageBuilder() {
            return null;
        }
    }
}
