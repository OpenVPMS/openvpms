/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import org.apache.commons.lang.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.resource.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Loads attachments from email messages to patient investigations.
 *
 * @author Tim Anderson
 */
class EmailLoader {

    /**
     * The id pattern.
     */
    private final Pattern pattern;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The transaction template.
     */
    private final TransactionTemplate template;

    /**
     * The listener.
     */
    private final EmailLoaderListener listener;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(EmailLoader.class);


    /**
     * Constructs an {@link EmailLoader}.
     *
     * @param pattern            the id pattern
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param handlers           the document handlers
     * @param rules              the document rules
     * @param listener           the listener
     */
    public EmailLoader(Pattern pattern, ArchetypeService service, PlatformTransactionManager transactionManager,
                       DocumentHandlers handlers, DocumentRules rules, EmailLoaderListener listener) {
        this.pattern = pattern;
        this.service = service;
        this.handlers = handlers;
        this.rules = rules;
        this.listener = listener;
        template = new TransactionTemplate(transactionManager);
    }

    /**
     * Processes an email.
     *
     * @param email the email
     * @return if the email was processed
     * @throws MessagingException for any mail error
     */
    public boolean process(Email email) throws MessagingException, IOException {
        boolean result;
        Multipart parts = email.getParts();
        int attachments = 0;
        if (parts != null) {
            int processed = 0;
            boolean error = false;
            for (int i = 0; i < parts.getCount(); i++) {
                BodyPart part = parts.getBodyPart(i);
                if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                    attachments++;
                    if (processAttachment(part, email)) {
                        processed++;
                    } else {
                        error = true;
                    }
                }
            }
            result = attachments == 0 || (!error && processed > 0);
        } else {
            result = true;
        }
        if (attachments == 0) {
            listener.noAttachment(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate());
        }
        return result;
    }

    /**
     * Processes an attachment.
     *
     * @param part  the attachment
     * @param email the email
     * @return {@code true} if the attachment was loaded, otherwise {@code false}
     * @throws MessagingException for any mail error
     */
    private boolean processAttachment(BodyPart part, Email email) throws MessagingException {
        boolean result = false;
        String fileName = part.getFileName();
        long id = getId(fileName);
        if (id != -1) {
            Boolean status = template.execute(transactionStatus -> {
                try {
                    return processAttachment(id, part, fileName, email);
                } catch (OpenVPMSException exception) {
                    throw exception;
                } catch (Exception exception) {
                    throw new IllegalStateException(exception.getMessage(), exception);
                }
            });
            result = (status != null) && status;
        } else {
            listener.missingId(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(), fileName);
        }
        return result;
    }

    /**
     * Processes an attachment.
     *
     * @param id       the act identifier
     * @param part     the message body part
     * @param fileName the file name
     * @param email    the email
     * @return {@code true} if the attachment was processed
     * @throws MessagingException for any messaging error
     * @throws IOException        if the attachment cannot be deserialised
     */
    private boolean processAttachment(long id, BodyPart part, String fileName, Email email)
            throws MessagingException, IOException {
        boolean result = false;
        DocumentAct act = getInvestigation(id);
        if (act != null) {
            String mimeType = parseMimeType(part, fileName);
            if (mimeType != null) {
                result = addAttachment(id, part, fileName, email, act, mimeType);
            } else {
                listener.error(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(), fileName,
                               Messages.get("emaildocload.nomimetype"));
            }
        } else {
            listener.missingAct(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(), fileName,
                                id);
        }
        return result;
    }

    /**
     * Parses the mime type for an attachment.
     *
     * @param part     the message body part
     * @param fileName the file name
     * @return the mime type, or {@code null} if one can't be found
     */
    private String parseMimeType(BodyPart part, String fileName) {
        String result = null;
        String contentType = null;
        try {
            contentType = part.getContentType();
            result = StringUtils.trimToNull(new ContentType(contentType).getBaseType());
            if (result != null) {
                result = result.toLowerCase();
            }
        } catch (Exception exception) {
            log.error("Cannot determine mime type for attachment={}: {}", fileName, exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Adds an attachment, if it isn't a duplicate.
     *
     * @param id       the act identifier
     * @param part     the message body part
     * @param fileName the file name
     * @param email    the email
     * @param act      the act to load to
     * @param mimeType the mime type
     * @return {@code true} if the attachment was added
     * @throws MessagingException for any messaging error
     * @throws IOException        if the attachment cannot be deserialised
     */
    private boolean addAttachment(long id, BodyPart part, String fileName, Email email, DocumentAct act,
                                  String mimeType)
            throws MessagingException, IOException {
        boolean result;
        DocumentHandler handler = handlers.get(fileName, mimeType);
        try (InputStream stream = part.getInputStream()) {
            Document document = handler.create(fileName, stream, mimeType, -1);
            if (!rules.isDuplicate(act, document)) {
                List<IMObject> objects = rules.addDocument(act, document);
                act.setStatus2(InvestigationActStatus.RECEIVED);
                service.save(objects);
                listener.loaded(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(),
                                fileName, id);
                result = true;
            } else {
                listener.alreadyLoaded(email.getMessageId(), email.getFrom(), email.getSubject(), email.getDate(),
                                       fileName, id);
                result = true;
            }
        }
        return result;
    }

    /**
     * Returns the investigation associated with an identifier.
     *
     * @param id the act identifier
     * @return the act, or {@code null} if none is found
     */
    private DocumentAct getInvestigation(long id) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> root = query.from(DocumentAct.class, InvestigationArchetypes.PATIENT_INVESTIGATION);
        query.where(builder.equal(root.get("id"), id));
        return service.createQuery(query).getFirstResult();
    }

    /**
     * Returns the act id from a file name.
     *
     * @param name the file name. May be {@code null}
     * @return the act id, or {@code -1} if none can be found
     */
    private long getId(String name) {
        long result = -1;
        if (name != null) {
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()) {
                result = Long.parseLong(matcher.group(1));
            }
        }
        return result;
    }
}