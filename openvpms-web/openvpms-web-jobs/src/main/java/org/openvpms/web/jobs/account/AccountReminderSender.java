/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.web.component.service.SimpleSMSService;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.account.AccountReminderEvaluator;
import org.openvpms.web.workspace.customer.account.AccountReminderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Sends account reminder SMS messages.
 *
 * @author Tim Anderson
 */
class AccountReminderSender {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The customer rules.
     */
    private final CustomerRules customerRules;

    /**
     * The account reminder rules.
     */
    private final AccountReminderRules reminderRules;

    /**
     * The SMS service.
     */
    private final SimpleSMSService smsService;

    /**
     * The account reminder template evaluator.
     */
    private final AccountReminderEvaluator evaluator;

    /**
     * The account reminder config.
     */
    private final AccountReminderConfig config;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The maximum no. of message parts supported by the provider.
     */
    private final int maxParts;

    /**
     * The communication logging subject.
     */
    private final String subject;

    /**
     * Communication logging reason code.
     */
    private static final String REASON = "ACCOUNT_REMINDER";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AccountReminderSender.class);

    /**
     * Constructs an {@link AccountReminderSender}.
     *
     * @param service            the archetype service
     * @param customerRules      the customer rules
     * @param reminderRules      the reminder rules
     * @param smsService         the SMS service
     * @param evaluator          the reminder template evaluator
     * @param config             the reminder configuration
     * @param transactionManager the transaction manager
     */
    public AccountReminderSender(ArchetypeService service, CustomerRules customerRules,
                                 AccountReminderRules reminderRules, SimpleSMSService smsService,
                                 AccountReminderEvaluator evaluator, AccountReminderConfig config,
                                 PlatformTransactionManager transactionManager) {
        this.service = service;
        this.customerRules = customerRules;
        this.reminderRules = reminderRules;
        this.smsService = smsService;
        this.evaluator = evaluator;
        this.config = config;
        this.transactionManager = transactionManager;
        maxParts = smsService.getMaxParts();
        subject = Messages.get("sms.log.account.subject");
    }

    /**
     * Sends an account reminder SMS if required.
     *
     * @param charge the charge
     * @param count  the reminder count configuration
     * @return the reminder processing status
     * @throws SMSException if the send fails
     */
    public AccountReminder.Status send(FinancialAct charge, AccountReminderConfig.ReminderCount count) {
        AccountReminder reminder = new AccountReminder(charge, count.getCount(), customerRules, service);
        if (needsReminder(charge)) {
            Party customer = reminder.getCustomer();
            if (customer == null) {
                reminder.failed(Messages.get("reporting.reminder.customerinactive"));
            } else {
                Contact contact = reminder.getContact();
                if (contact == null) {
                    reminder.failed(Messages.get("reporting.reminder.nosmscontact"));
                } else {
                    Party location = reminder.getLocation();
                    if (location == null) {
                        String displayName = DescriptorHelper.getDisplayName(charge.getArchetype(), service);
                        reminder.failed(Messages.format("reporting.reminder.nolocation", displayName));
                    } else {
                        send(charge, reminder, count, customer, contact, location);
                    }
                }
            }
        } else {
            // cancel any persistent reminders for this charge
            reminder.cancel();
        }
        return reminder.getStatus();
    }

    /**
     * Send an account reminder.
     *
     * @param charge   the charge
     * @param reminder the reminder
     * @param count    the reminder count configuration
     * @param customer the customer
     * @param contact  the contact
     * @param location the location
     */
    private void send(FinancialAct charge, AccountReminder reminder, AccountReminderConfig.ReminderCount count,
                      Party customer, Contact contact, Party location) {
        try {
            String message = evaluator.evaluate(count.getSMSTemplate(), charge, customer, location);
            if (StringUtils.isEmpty(message)) {
                reminder.failed(Messages.get("reporting.reminder.emptysms"), true);
            } else if (smsService.getParts(message) > maxParts) {
                reminder.failed(Messages.format("reporting.reminder.smstoolong", message), true);
            } else {
                // execute in a transaction so the SMS rolls back if the reminder cannot be updated
                TransactionTemplate template = new TransactionTemplate(transactionManager);
                template.executeWithoutResult(transactionStatus -> {
                    smsService.send(message, contact, customer, null, subject, REASON, location);
                    reminder.sent(config.getNextReminder(charge.getActivityEndTime(), count.getCount()));
                });
            }
        } catch (AccountReminderException exception) {
            log.error(exception.getMessage(), exception);
            reminder.failed(exception.getMessage());
        }
    }

    /**
     * Determines if the customer should be notified of an unpaid charge.
     * <p/>
     * A customer should be notified if the charge balance >= the minimum balance, and the charge
     * isn't in a gap claim (invoices only). An invoice in a standard claim is fully paid.
     *
     * @param charge the charge
     * @return {@code true} if the customer should be notified, otherwise {@code false}
     */
    private boolean needsReminder(FinancialAct charge) {
        return reminderRules.needsReminder(charge, config.getMinBalance());
    }
}