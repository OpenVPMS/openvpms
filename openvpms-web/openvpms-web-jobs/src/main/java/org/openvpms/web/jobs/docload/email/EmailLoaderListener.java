/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Listener for {@link EmailLoader} events.
 *
 * @author Tim Anderson
 */
class EmailLoaderListener {

    public static class MailData {

        private final String messageId;

        private final String from;

        private final String subject;

        private final Date date;

        private final String name;

        private final long id;

        private final String error;

        public MailData(String messageId, String from, String subject, Date date, String name, long id) {
            this(messageId, from, subject, date, name, id, null);
        }

        public MailData(String messageId, String from, String subject, Date date, String name, long id,
                        String error) {
            this.messageId = messageId;
            this.from = from;
            this.subject = subject;
            this.date = date;
            this.name = name;
            this.id = id;
            this.error = error;
        }

        /**
         * Returns the message identifier.
         *
         * @return the message identifier
         */
        public String getMessageId() {
            return messageId;
        }

        /**
         * Returns the from address.
         *
         * @return the from address
         */
        public String getFrom() {
            return from;
        }

        /**
         * Returns the subject.
         *
         * @return the subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * Returns the received date.
         *
         * @return the received date
         */
        public Date getDate() {
            return date;
        }

        /**
         * Returns the attachment name.
         *
         * @return the attachment name. May be {@code null}
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the act id.
         *
         * @return the id, or {@code -1} if it is not known
         */
        public long getId() {
            return id;
        }

        /**
         * The error message, if the attachment couldn't be loaded.
         *
         * @return the error message. May be {@code null}
         */
        public String getError() {
            return error;
        }
    }

    /**
     * The loaded attachments.
     */
    private final List<MailData> loaded = new ArrayList<>();

    /**
     * The skipped attachments.
     */
    private final List<MailData> skipped = new ArrayList<>();

    /**
     * The attachments with missing identifiers.
     */
    private final List<MailData> missingId = new ArrayList<>();

    /**
     * The attachments with missing acts.
     */
    private final List<MailData> missingAct = new ArrayList<>();

    /**
     * The attachments that couldn't be loaded due to error.
     */
    private final List<MailData> errors = new ArrayList<>();

    /**
     * Emails that were skipped as they have no attachments.
     */
    private final List<MailData> noAttachments = new ArrayList<>();

    /**
     * Notifies when an attachment is loaded.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    public void loaded(String messageId, String from, String subject, Date date, String name, long id) {
        loaded.add(new MailData(messageId, from, subject, date, name, id));
    }

    /**
     * Returns the loaded attachments.
     *
     * @return the loaded attachments
     */
    public List<MailData> getLoaded() {
        return loaded;
    }

    /**
     * Notifies that an attachment couldn't be loaded as it or another attachment had already been processed.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    public void alreadyLoaded(String messageId, String from, String subject, Date date, String name, long id) {
        skipped.add(new MailData(messageId, from, subject, date, name, id));
    }

    /**
     * Returns the attachments that weren't loaded as the corresponding act was already associated with a document.
     *
     * @return the attachments that were already loaded
     */
    public List<MailData> getAlreadyLoaded() {
        return skipped;
    }

    /**
     * Notifies that an attachment couldn't be loaded as no identifier could be parsed from the attachment filename.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     */
    public void missingId(String messageId, String from, String subject, Date date, String name) {
        missingId.add(new MailData(messageId, from, subject, date, name, -1));
    }

    /**
     * Returns the attachments that don't have an identifier in the filename.
     *
     * @return the attachments that don't have an identifier
     */
    public List<MailData> getMissingId() {
        return missingId;
    }

    /**
     * Notifies that an attachment couldn't be loaded as there was no corresponding act.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    public void missingAct(String messageId, String from, String subject, Date date, String name, long id) {
        missingAct.add(new MailData(messageId, from, subject, date, name, id));
    }

    /**
     * Returns the attachments that don't have a corresponding act.
     *
     * @return the attachments with no corresponding act
     */
    public List<MailData> getMissingAct() {
        return missingAct;
    }

    /**
     * Notifies that an email has no attachments.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     */
    public void noAttachment(String messageId, String from, String subject, Date date) {
        noAttachments.add(new MailData(messageId, from, subject, date, null, -1));
    }

    /**
     * Returns emails with no attachments.
     *
     * @return the emails with no attachments
     */
    public List<MailData> getNoAttachments() {
        return noAttachments;
    }

    /**
     * Notifies that an attachment couldn't be loaded due to error.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param exception the error
     */
    public void error(String messageId, String from, String subject, Date date, String name, Throwable exception) {
        errors.add(new MailData(messageId, from, subject, date, name, -1, exception.getMessage()));
    }

    /**
     * Notifies that a file couldn't be loaded due to error.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param message   the error message
     */
    public void error(String messageId, String from, String subject, Date date, String name, String message) {
        errors.add(new MailData(messageId, from, subject, date, name, -1, message));
    }

    /**
     * Returns the no. of attachments that failed to load due to error.
     *
     * @return the no. of errors
     */
    public List<MailData> getErrors() {
        return errors;
    }

    /**
     * Returns the no. of attachment processed.
     *
     * @return the no. of attachment processed
     */
    public int getProcessed() {
        return loaded.size() + missingId.size() + missingAct.size() + skipped.size() + noAttachments.size()
               + errors.size();
    }
}