/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Manages the reminder state for a charge.
 *
 * @author Tim Anderson
 */
class AccountReminder {

    /**
     * The processing status.
     */
    public enum Status {
        UNKNOWN,   // unknown status. Reminder is yet to be processed.
        SENT,      // reminder was sent
        SKIPPED,   // reminder was skipped
        ERROR,     // reminder failed to be sent
        CANCELLED; // reminder was cancelled

        /**
         * Determines if the status indicates the reminder is persistent.
         *
         * @return {@code true} if the status indicates a persistent change
         */
        boolean isPersistent() {
            return this == SENT || this == ERROR || this == CANCELLED;
        }
    }

    /**
     * The customer rules.
     */
    private final CustomerRules customerRules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The charge bean.
     */
    private final IMObjectBean bean;

    /**
     * The customer.
     */
    private final Party customer;

    /**
     * The current reminder count.
     */
    private final int count;

    /**
     * The objects to save.
     */
    private final Set<IMObject> toSave = new HashSet<>();

    /**
     * The reminder bean.
     */
    private IMObjectBean reminder;

    /**
     * The processing status.
     */
    private Status status = Status.UNKNOWN;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AccountReminder.class);

    /**
     * Customer node name.
     */
    private static final String CUSTOMER = "customer";

    /**
     * Location node name.
     */
    private static final String LOCATION = "location";

    /**
     * Status node name.
     */
    private static final String STATUS = "status";

    /**
     * Error node name.
     */
    private static final String ERROR = "error";

    /**
     * Count node name.
     */
    private static final String COUNT = "count";

    /**
     * Reminders node name.
     */
    private static final String REMINDERS = "reminders";

    /**
     * Charge node name.
     */
    private static final String CHARGE = "charge";

    /**
     * Practice node name.
     */
    private static final String PRACTICE = "practice";

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * End time node name.
     */
    private static final String END_TIME = "endTime";

    /**
     * Proessed node name.
     */
    private static final String PROCESSED = "processed";

    /**
     * Reminder count node name.
     */
    private static final String REMINDER_COUNT = "reminderCount";

    /**
     * Reminder sent node name.
     */
    private static final String REMINDER_SENT = "reminderSent";

    /**
     * Constructs an {@link AccountReminder}.
     *
     * @param charge        the charge
     * @param count         the reminder count
     * @param customerRules the customer rules
     * @param service       the archetype service
     */
    public AccountReminder(FinancialAct charge, int count, CustomerRules customerRules, ArchetypeService service) {
        this.service = service;
        this.customerRules = customerRules;
        bean = service.getBean(charge);
        this.count = count;
        customer = bean.getTarget(CUSTOMER, Party.class, Policies.active());
        reminder = init();
    }

    /**
     * Returns the customer.
     *
     * @return the customer, or {@code null} if it is inactive
     */
    public Party getCustomer() {
        return customer;
    }

    /**
     * Returns the customer SMS contact.
     *
     * @return the customer SMS contact, or {@code null} if the customer doesn't have one
     */
    public Contact getContact() {
        return customer != null ? customerRules.getSMSContact(customer) : null;
    }

    /**
     * Returns the location associated with a charge, or the customer location if none is found.
     *
     * @return the location, or {@code null} if none can be determined
     */
    public Party getLocation() {
        Party location = bean.getTarget(LOCATION, Party.class);
        if (location == null && customer != null) {
            location = service.getBean(customer).getTarget(PRACTICE, Party.class);
        }
        return location;
    }

    /**
     * Indicates that a reminder was sent.
     *
     * @param nextReminder the date when the next reminder is due. May be {@code null}
     */
    public void sent(Date nextReminder) {
        Date now = new Date();
        if (reminder == null) {
            createReminder(now);
        }
        reminder.setValue(END_TIME, nextReminder);
        reminder.setValue(STATUS, ReminderItemStatus.COMPLETED);
        bean.setValue(REMINDER_COUNT, count);
        bean.setValue(REMINDER_SENT, now);
        reminder.setValue(PROCESSED, now);
        toSave.add(bean.getObject());
        toSave.add(reminder.getObject());
        service.save(toSave);
        status = Status.SENT;
    }

    /**
     * Indicates that the reminder should be cancelled.
     * <p/>
     * If there is no persistent reminder, this is a no-op.
     */
    public void cancel() {
        if (reminder != null) {
            log.info("Cancelling reminder for charge={}, customer={}, count={}", bean.getObject().getId(),
                     getCustomerId(), count);
            reminder.setValue(STATUS, ReminderItemStatus.CANCELLED);
            toSave.add(reminder.getObject());
            service.save(toSave);
            status = Status.CANCELLED;
        } else {
            status = Status.SKIPPED;
        }
    }

    /**
     * Indicates that the reminder couldn't be sent.
     * <p/>
     * If the reminder is persistent, it will be updated and saved else the error will only be logged.
     *
     * @param error the reason
     * @return {@code true} if the reminder was updated, otherwise {@code false}
     */
    public boolean failed(String error) {
        return failed(error, false);
    }

    /**
     * Indicates that the reminder couldn't be sent.
     * <p/>
     * If the reminder is persistent, it will be updated and saved else the error will only be logged.
     *
     * @param error    the reason
     * @param forceLog if {@code true}, make the reminder persistent
     * @return {@code true} if the reminder was updated, otherwise {@code false}
     */
    public boolean failed(String error, boolean forceLog) {
        log.info("Failed to send account reminder for charge={}, customer={}, count={}: {}", bean.getObject().getId(),
                 getCustomerId(), count, error);
        boolean result = false;
        Date now = new Date();
        if (reminder == null && forceLog) {
            createReminder(now);
        }
        if (reminder != null) {
            int maxLength = reminder.getMaxLength(ERROR);
            reminder.setValue(STATUS, ReminderItemStatus.ERROR);
            reminder.setValue(ERROR, StringUtils.abbreviate(error, maxLength));
            reminder.setValue(PROCESSED, now);
            toSave.add(reminder.getObject());
            service.save(toSave);
            result = true;
            status = Status.ERROR;
        } else {
            status = Status.SKIPPED;
        }
        return result;
    }

    /**
     * Returns the reminder processing status.
     *
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Returns the customer id.
     *
     * @return the customer id
     */
    private long getCustomerId() {
        long result;
        if (customer != null) {
            result = customer.getId();
        } else {
            Reference ref = bean.getTargetRef(CUSTOMER);
            result = (ref != null) ? ref.getId() : -1;
        }
        return result;
    }

    /**
     * Initialises this.
     * <p/>
     * This cancels reminders with a count less than the current count
     *
     * @return the reminder associated with the reminder count. May be {@code null}
     */
    private IMObjectBean init() {
        IMObjectBean result = null;
        List<Act> reminders = bean.getTargets(REMINDERS, Act.class);
        for (Act act : reminders) {
            IMObjectBean reminder = service.getBean(act);
            int existing = reminder.getInt(COUNT);
            if (existing == count) {
                result = reminder;
            } else if (existing < count &&
                       (ReminderItemStatus.PENDING.equals(act.getStatus())
                        || ReminderItemStatus.ERROR.equals(act.getStatus()))) {
                act.setStatus(ReminderItemStatus.CANCELLED);
                toSave.add(act);
            }
        }
        return result;
    }

    /**
     * Creates the reminder act for the current reminder count.
     *
     * @param startTime the reminder start time, indicating when it is scheduled to be sent
     */
    private void createReminder(Date startTime) {
        Act act = service.create(AccountReminderArchetypes.CHARGE_REMINDER_SMS, Act.class);
        reminder = service.getBean(act);
        reminder.setValue(START_TIME, startTime);
        reminder.setValue(COUNT, count);
        toSave.add(act);
        toSave.add(bean.getObject());
        bean.addTarget(REMINDERS, act, CHARGE);
    }
}