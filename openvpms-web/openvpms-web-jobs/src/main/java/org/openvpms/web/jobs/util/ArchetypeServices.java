/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.util;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;

/**
 * Helper to access {@link IArchetypeRuleService} and {@link IArchetypeService} implementations.
 * <p/>
 * For the purposes of the webapp, the {@link IArchetypeRuleService} implementation triggers rules
 * whereas {@link IArchetypeService} doesn't.
 * <p/>
 * This class exists to make it easier to access either implementation without using bean names.
 *
 * @author Tim Anderson
 */
public class ArchetypeServices {

    /**
     * The rule-based archetype service.
     */
    private final IArchetypeRuleService ruleService;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs an {@link ArchetypeServices}.
     *
     * @param ruleService the rule-based archetype service
     * @param service     the archetype service
     */
    public ArchetypeServices(IArchetypeRuleService ruleService, IArchetypeService service) {
        this.ruleService = ruleService;
        if (service instanceof IArchetypeRuleService) {
            throw new IllegalArgumentException("Argument 'service' cannot implement IArchetypeRuleService");
        }
        this.service = service;
    }

    /**
     * Returns the rule-based archetype service.
     * <p/>
     * This triggers rules on save and delete.
     *
     * @return the archetype service
     */
    public IArchetypeRuleService getRuleService() {
        return ruleService;
    }

    /**
     * Returns the archetype service.
     * <p/>
     * This does not trigger rules on save or delete.
     *
     * @return the archetype service
     */
    public IArchetypeService getService() {
        return service;
    }
}
