/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.jobs.JobCompletionNotifier;
import org.openvpms.web.jobs.docload.DocumentLoaderJob;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/**
 * Loads attachments from email messages in an IMAP folder to patient investigations.
 *
 * @author Tim Anderson
 */
@DisallowConcurrentExecution
public class EmailDocumentLoaderJob implements InterruptableJob {

    /**
     * The job configuration.
     */
    private final Entity configuration;

    /**
     * The archetype service
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The mail server password resolver.
     */
    private final MailPasswordResolver passwordResolver;

    /**
     * Used to send messages to users on completion or failure.
     */
    private final JobCompletionNotifier notifier;

    /**
     * Determines if loading should stop.
     */
    private final AtomicBoolean stop = new AtomicBoolean(false);

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(EmailDocumentLoaderJob.class);

    /**
     * Constructs a {@link DocumentLoaderJob}.
     *
     * @param configuration      the configuration
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param handlers           the document handlers
     * @param passwordResolver   the mail server password resolver
     */
    public EmailDocumentLoaderJob(Entity configuration, IArchetypeRuleService service,
                                  PlatformTransactionManager transactionManager, DocumentHandlers handlers,
                                  DocumentRules rules, MailPasswordResolver passwordResolver) {
        this.configuration = configuration;
        this.service = service;
        this.transactionManager = transactionManager;
        this.handlers = handlers;
        this.rules = rules;
        this.passwordResolver = passwordResolver;
        notifier = new JobCompletionNotifier(service);
    }


    /**
     * Called by the {@link Scheduler} when a {@link Trigger} fires that is associated with the {@code Job}.
     *
     * @throws JobExecutionException if there is an exception while executing the job.
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        MailServer server = new MailServer(configuration, service);
        IMObjectBean bean = service.getBean(configuration);
        List<String> from = getFrom(bean);
        Pattern idPattern = Pattern.compile(bean.getString("idPattern"));
        String sourceFolder = bean.getString("sourceFolder");
        String targetFolder = bean.getString("targetFolder");
        String errorFolder = bean.getString("errorFolder");
        boolean logLoad = bean.getBoolean("log");

        EmailLoaderListener listener = logLoad ? new LoggingEmailLoaderListener(log) : new EmailLoaderListener();
        if (logLoad) {
            log.info("Loading attachments from {}@{}:{} using pattern={}, sourceFolder={}, targetFolder={}, " +
                     "errorFolder={}", server.getUsername(), server.getHost(), server.getPort(), idPattern,
                     sourceFolder, targetFolder, errorFolder);
        }

        EmailLoader loader = createLoader(idPattern, listener);

        EmailFolderLoader folderLoader = new EmailFolderLoader(server, passwordResolver, listener);
        stop.set(false);
        try {
            folderLoader.process(loader, from, sourceFolder, targetFolder, errorFolder, stop);
            complete(listener, null);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
            complete(listener, exception);
        }
    }

    /**
     * Called by the {@link Scheduler} when a user interrupts the {@code Job}.
     */
    @Override
    public void interrupt() {
        stop.set(true);
    }

    /**
     * Creates an email loader.
     *
     * @param idPattern the id pattern
     * @param listener  the listener
     * @return a new email loader
     */
    protected EmailLoader createLoader(Pattern idPattern, EmailLoaderListener listener) {
        return new EmailLoader(idPattern, service, transactionManager, handlers, rules, listener);
    }

    /**
     * Invoked on completion of a job. Sends a message notifying the registered users of completion or failure of the
     * job.
     *
     * @param listener  the loader listener
     * @param exception the exception, if the job failed, otherwise {@code null}
     */
    protected void complete(EmailLoaderListener listener, Throwable exception) {
        if ((listener.getProcessed() != 0) || exception != null) {
            Set<User> users = notifier.getUsers(configuration);
            if (!users.isEmpty()) {
                notifyUsers(listener, exception, users);
            }
        }
    }

    /**
     * Notifies users.
     *
     * @param users   the users to notify.
     * @param subject the subject
     * @param reason  the reason
     * @param text    the message text
     */
    protected void notifyUsers(Set<User> users, String subject, String reason, String text) {
        notifier.send(users, subject, reason, text);
    }

    /**
     * Notifies users of completion or failure of the job.
     *
     * @param listener  the loader listener
     * @param exception the exception, if the job failed, otherwise {@code null}
     * @param users     the users to notify
     */
    private void notifyUsers(EmailLoaderListener listener, Throwable exception, Set<User> users) {
        String subject;
        String reason;
        StringBuilder text = new StringBuilder();
        if (exception != null) {
            reason = SystemMessageReason.ERROR;
            subject = Messages.format("emaildocload.subject.exception", configuration.getName());
            text.append(Messages.format("emaildocload.exception", exception.getMessage()));
        } else {
            int loaded = listener.getLoaded().size();
            int errors = listener.getMissingId().size() + listener.getMissingAct().size() + listener.getErrors().size();
            if (errors != 0) {
                reason = SystemMessageReason.ERROR;
                subject = Messages.format("emaildocload.subject.errors", configuration.getName(), errors);
            } else {
                reason = SystemMessageReason.COMPLETED;
                subject = Messages.format("emaildocload.subject.success", configuration.getName(), loaded);
            }
        }
        append(text, listener.getErrors(), "emaildocload.error", "emaildocload.error.item");
        append(text, listener.getMissingId(), "emaildocload.missingId", "emaildocload.missingId.item");
        append(text, listener.getMissingAct(), "emaildocload.missingAct", "emaildocload.missingAct.item");
        append(text, listener.getAlreadyLoaded(), "emaildocload.alreadyLoaded", "emaildocload.alreadyLoaded.item");
        append(text, listener.getNoAttachments(), "emaildocload.noAttachment", "emaildocload.noAttachment.item");
        append(text, listener.getLoaded(), "emaildocload.loaded", "emaildocload.loaded.item");
        notifyUsers(users, subject, reason, text.toString());
    }

    /**
     * Appends items to the text.
     *
     * @param text        the message text
     * @param attachments the entries to append
     * @param headingKey  the heading resource bundle key
     * @param itemKey     the item resource bundle key
     */
    private void append(StringBuilder text, List<EmailLoaderListener.MailData> attachments, String headingKey, String itemKey) {
        if (!attachments.isEmpty()) {
            if (text.length() != 0) {
                text.append("\n\n");
            }
            text.append(Messages.get(headingKey));
            text.append("\n");
            for (EmailLoaderListener.MailData attachment : attachments) {
                String date = attachment.getDate() != null ? DateFormatter.formatDateTime(attachment.getDate(), true)
                                                           : "";
                text.append(Messages.format(itemKey, attachment.getId(),
                                            StringUtils.defaultString(attachment.getName(), "(unknown)"),
                                            StringUtils.defaultString(attachment.getFrom(), "(unknown)"),
                                            StringUtils.defaultString(attachment.getSubject(), "(none)"),
                                            date,
                                            StringUtils.defaultString(attachment.getError(), "(none)")));
                text.append("\n");
            }
        }
    }

    /**
     * Returns the from-addresses to filter emails on.
     * <p/>
     * This splits the value of 'from' node, which is a comma-separated string
     *
     * @param bean the configuration bean
     * @return the from-addresses
     */
    private List<String> getFrom(IMObjectBean bean) {
        List<String> result = new ArrayList<>();
        for (String value : bean.getString("from", "").split(",")) {
            value = StringUtils.trimToNull(value);
            if (value != null) {
                result.add(value);
            }
        }
        return result;
    }
}