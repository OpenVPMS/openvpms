/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.event.Listener;
import org.openvpms.web.echo.style.ResourceStyleSheetProperties;
import org.openvpms.web.echo.style.StyleSheetProperties;
import org.openvpms.web.echo.style.Theme;
import org.openvpms.web.echo.style.Themes;
import org.springframework.beans.factory.DisposableBean;

import java.io.IOException;
import java.util.Map;

/**
 * Implementation of {@link StyleSheetProperties} that sources the main colours from the practice, if present.
 *
 * @author Tim Anderson
 */
public class PracticeStyleSheetProperties implements StyleSheetProperties, DisposableBean {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The properties.
     */
    private final Map<String, String> properties;

    /**
     * The themes.
     */
    private final Themes themes;

    /**
     * The default style resource name.
     */
    private final String defaultName;

    /**
     * The override style resource name. May be {@code null}
     */
    private final String overrideName;

    /**
     * Listener for practice update events.
     */
    private final Listener<PracticeService.Update> listener;

    /**
     * The last theme loaded.
     */
    private String lastTheme;

    /**
     * Constructs a {@link PracticeStyleSheetProperties}.
     *
     * @param practiceService the practice service
     * @param service         the archetype service
     * @param themes          the themes
     * @param defaultName     the default resource name, minus any extension
     * @param overrideName    the override resource name, minus any extension. May be {@code null}
     */
    public PracticeStyleSheetProperties(PracticeService practiceService, ArchetypeService service, Themes themes,
                                        String defaultName, String overrideName) throws IOException {
        this.practiceService = practiceService;
        this.service = service;
        this.themes = themes;
        this.defaultName = defaultName;
        this.overrideName = overrideName;
        StyleSheetProperties loader = new ResourceStyleSheetProperties(defaultName, overrideName);
        properties = loader.getProperties();
        Party practice = practiceService.getPractice();
        if (practice != null) {
            updateTheme(practice);
        }
        listener = event -> updateTheme(event.getPractice());
        practiceService.addListener(listener);
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        practiceService.removeListener(listener);
    }

    /**
     * Returns the default style sheet name.
     * <p/>
     * This is the resource name, minus any extension.
     *
     * @return the default style sheet name
     */
    @Override
    public String getDefaultName() {
        return defaultName;
    }

    /**
     * Returns the override style sheet name.
     * <p/>
     * This is the resource name, minus any extension.
     *
     * @return the override style sheet name. May be {@code null}
     */
    @Override
    public String getOverrideName() {
        return overrideName;
    }

    /**
     * Returns the style sheet properties.
     *
     * @return the properties
     */
    @Override
    public synchronized Map<String, String> getProperties() {
        return properties;
    }

    /**
     * Invoked when the practice updates.
     * <p/>
     * Updates the theme if required.
     *
     * @param practice the practice
     */
    private synchronized void updateTheme(Party practice) {
        IMObjectBean bean = service.getBean(practice);
        String id = bean.getString("theme");
        if (id != null && !id.equals(lastTheme)) {
            Theme theme = themes.getTheme(id);
            if (theme != null) {
                properties.putAll(theme.getExpressions());
            }
            lastTheme = id;
        }
    }
}