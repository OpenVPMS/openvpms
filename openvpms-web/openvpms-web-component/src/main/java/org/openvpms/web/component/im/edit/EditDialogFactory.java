/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.edit;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;

import java.lang.reflect.Constructor;


/**
 * A factory for {@link EditDialog} instances.
 *
 * @author Tim Anderson
 */
public class EditDialogFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Editor implementations.
     */
    private ArchetypeHandlers<EditDialog> dialogs;

    /**
     * Constructs an {@link EditDialogFactory}.
     *
     * @param service the archetype service
     */
    public EditDialogFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new dialog for an editor.
     *
     * @param editor  the editor
     * @param context the context
     * @return a new dialog
     */
    public EditDialog create(IMObjectEditor editor, Context context) {
        EditDialog result = null;
        String archetype = editor.getObject().getArchetype();
        ArchetypeHandler<?> handler = getDialogs().getHandler(archetype);
        if (handler != null) {
            Class<?> type = handler.getType();
            Constructor<?> ctor = ConstructorUtils.getMatchingAccessibleConstructor(type, editor.getClass(),
                                                                                    context.getClass());
            if (ctor != null) {
                try {
                    result = (EditDialog) ctor.newInstance(editor, context);
                } catch (Throwable throwable) {
                    throw new IllegalStateException("Failed to construct edit dialog " + type.getName()
                                                    + " for editor " + editor.getClass().getName() + ": "
                                                    + throwable.getMessage(), throwable);
                }
            } else {
                throw new IllegalStateException("No valid constructor found for edit dialog " + type.getName()
                                                + " for editor " + editor.getClass().getName());
            }
        }
        if (result == null) {
            // no editor registered, so use the default
            result = new EditDialog(editor, context);
        }
        return result;
    }

    /**
     * Creates a new dialog for an editor.
     *
     * @param editor  the editor
     * @param actions the edit actions
     * @param context the context
     * @return a new dialog
     */
    public EditDialog create(IMObjectEditor editor, EditActions actions, Context context) {
        EditDialog result = null;
        String archetype = editor.getObject().getArchetype();
        ArchetypeHandler<?> handler = getDialogs().getHandler(archetype);
        if (handler != null) {
            Class<?> type = handler.getType();
            Constructor<?> ctor = ConstructorUtils.getMatchingAccessibleConstructor(type, editor.getClass(),
                                                                                    actions.getClass(),
                                                                                    context.getClass());
            if (ctor != null) {
                try {
                    result = (EditDialog) ctor.newInstance(editor, actions, context);
                } catch (Throwable throwable) {
                    throw new IllegalStateException("Failed to construct edit dialog " + type.getName()
                                                    + " for editor " + editor.getClass().getName() + ": "
                                                    + throwable.getMessage(), throwable);
                }
            } else {
                throw new IllegalStateException("No valid constructor found for edit dialog " + type.getName()
                                                + " for editor " + editor.getClass().getName());
            }
        }
        if (result == null) {
            // no editor registered, so use the default
            result = new EditDialog(editor, actions, context);
        }
        return result;
    }

    /**
     * Determines if the dialog for an editor supports {@link EditActions}.
     *
     * @param editor the editor
     * @return {@code true} if there is a dialog for the editor that supports {@link EditActions},
     * otherwise {@code false}
     */
    public boolean supportsEditActions(IMObjectEditor editor) {
        ArchetypeHandler<?> handler = getDialogs().getHandler(editor.getObject().getArchetype());
        if (handler != null) {
            Constructor<?> ctor = ConstructorUtils.getMatchingAccessibleConstructor(
                    handler.getType(), editor.getClass(), EditActions.class, Context.class);
            return ctor != null;
        }
        return false;
    }

    /**
     * Returns the dialogs.
     *
     * @return the dialogs
     */
    private synchronized ArchetypeHandlers<EditDialog> getDialogs() {
        if (dialogs == null) {
            dialogs = new ArchetypeHandlers<>("EditDialogFactory.properties", "DefaultEditDialogFactory.properties",
                                              EditDialog.class, service);
        }
        return dialogs;
    }
}
