/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.ActStatusHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.EditActions;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;


/**
 * A edit dialog for acts that disables the Apply button for
 * <em>POSTED<em> acts, as a workaround for OVPMS-733.
 *
 * @author Tim Anderson
 */
public class ActEditDialog extends EditDialog {

    /**
     * Determines if the act has been POSTED. If so, it can no longer be saved.
     */
    private boolean posted;

    /**
     * Monitors act status changes.
     */
    private ModifiableListener statusListener;

    /**
     * Constructs an {@link ActEditDialog}.
     *
     * @param editor  the editor
     * @param context the context
     */
    public ActEditDialog(IMObjectEditor editor, Context context) {
        super(editor, context);
    }

    /**
     * Constructs an {@link ActEditDialog}.
     *
     * @param editor  the editor
     * @param actions the supported dialog actions
     * @param context the context
     */
    public ActEditDialog(IMObjectEditor editor, EditActions actions, Context context) {
        super(editor, actions, context);
    }


    /**
     * Determines if the current object can be saved.
     *
     * @return {@code true} if the current object can be saved
     */
    @Override
    protected boolean canSave() {
        return super.canSave() && (getEditor().getObject().isNew() || !posted);
    }

    /**
     * Determines if the act has been saved with POSTED status.
     *
     * @return {@code true} if the act has been saved
     */
    protected boolean isPosted() {
        return posted;
    }

    /**
     * Saves the current object.
     *
     * @param editor the editor
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave(IMObjectEditor editor) {
        super.doSave(editor);
        if (!posted) {
            posted = getPosted();
        }
    }

    /**
     * Sets the editor.
     * <p>
     * If there is an existing editor, its selection path will be set on the editor.
     *
     * @param editor the editor. May be {@code null}
     */
    @Override
    protected void setEditor(IMObjectEditor editor) {
        if (editor != null) {
            if (statusListener != null) {
                // remove the old listener
                IMObjectEditor old = getEditor();
                if (old != null) {
                    Property status = editor.getProperty("status");
                    if (status != null) {
                        status.removeModifiableListener(statusListener);
                    }
                }
                statusListener = null;
            }
            final Property status = editor.getProperty("status");
            if (status != null) {
                onStatusChanged(status);
                statusListener = modifiable -> onStatusChanged(status);
                status.addModifiableListener(statusListener);
            }
        }
        super.setEditor(editor);
        posted = isSavedPosted();
    }

    /**
     * Invoked to reload the object being edited when save fails.
     * <p/>
     * This implementation reloads the editor, but returns {@code false} if the act is saved and has been POSTED.
     *
     * @param editor the editor
     * @return {@code true} if the editor was reloaded and the act is not now POSTED.
     */
    @Override
    protected boolean reload(IMObjectEditor editor) {
        boolean result = super.reload(editor);
        if (result) {
            IMObjectEditor newEditor = getEditor();
            result = newEditor.getObject().isNew() || !isPosted();
        }
        return result;
    }

    /**
     * Disables the apply button if the act status is <em>POSTED</em>, otherwise enables it.
     *
     * @param status the act status property
     */
    private void onStatusChanged(Property status) {
        if (ActStatus.POSTED.equals(status.getString())) {
            getButtons().setEnabled(APPLY_ID, false);
        } else if (!isSaveDisabled()) {
            getButtons().setEnabled(APPLY_ID, true);
        }
    }

    /**
     * Determines if the act is persistent and POSTED.
     * <p/>
     * NOTE: this doesn't determine if the persistent instance is POSTED. For that, use {@link #isSavedPosted()}.
     *
     * @return {@code true} if the act is persistent and POSTED
     */
    private boolean getPosted() {
        boolean result = false;
        IMObjectEditor editor = getEditor();
        if (editor != null) {
            Act act = (Act) editor.getObject();
            result = !act.isNew() && ActStatus.POSTED.equals(act.getStatus());
        }
        return result;
    }

    /**
     * Determines if the act has been saved posted.
     *
     * @return {@code true} if the act has been saved posted
     */
    private boolean isSavedPosted() {
        boolean result = false;
        IMObjectEditor editor = getEditor();
        if (editor != null) {
            Act act = (Act) editor.getObject();
            result = ActStatusHelper.isPosted(act, ServiceHelper.getArchetypeService());
        }
        return result;
    }

}
