/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.filetransfer.UploadListener;
import org.openvpms.archetype.rules.doc.DelegatingDocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.SupportedContentDocumentHandler;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.jasper.JRXMLDocumentHandler;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.LookupListCellRenderer;
import org.openvpms.web.component.im.list.LookupListModel;
import org.openvpms.web.component.im.lookup.BoundLookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;


/**
 * Editor for <em>entity.documentTemplate</em>s entities.
 * <p/>
 * This supports associating a single mandatory <em>act.documentTemplate</em> with the template, representing the
 * template content.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateEditor extends AbstractDocumentTemplateEditor {

    /**
     * The output format.
     */
    private final Property outputFormat;

    /**
     * The output format editor.
     */
    private final PropertyComponentEditor outputFormatEditor;

    /**
     * Determines if a letter archetype has been selected.
     */
    private boolean letter;

    /**
     * The type node name.
     */
    static final String TYPE = "type";

    /**
     * The output format node name.
     */
    static final String OUTPUT_FORMAT = "outputFormat";

    /**
     * Constructs a {@link DocumentTemplateEditor}.
     *
     * @param template the object to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context. May be {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentTemplateEditor(Entity template, IMObject parent, LayoutContext context) {
        super(template, parent, false, null, context);
        String type = getTypeCode();
        updateDocumentHandler(type);
        letter = DocumentTemplate.isLetter(type);
        getProperty(TYPE).addModifiableListener(modifiable -> onTypeChanged());
        outputFormat = getProperty(OUTPUT_FORMAT);

        // create the output format editor. Only enable it if the template is a Letter
        BoundLookupField field = new BoundLookupField(outputFormat,
                                                      new LookupListModel(new NodeLookupQuery(template, outputFormat),
                                                                          false, false, true));
        LookupFieldFactory.setDefaultStyle(field);
        field.setCellRenderer(LookupListCellRenderer.INSTANCE);
        field.setEnabled(letter);
        outputFormatEditor = new PropertyComponentEditor(outputFormat, field);
        if (type == null) {
            enableUpload(false);
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        // TODO - this may leave any uploaded documents unreachable
        return new DocumentTemplateEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Returns the document template type code.
     *
     * @return the document template type code, or {@code null} if no document template type has been selected
     */
    public String getTypeCode() {
        Lookup type = getType();
        return (type != null) ? type.getCode() : null;
    }

    /**
     * Sets the document template type code.
     *
     * @param code the code. May be {@code null}
     */
    public void setTypeCode(String code) {
        Lookup lookup = null;
        if (code != null) {
            LookupService lookups = ServiceHelper.getLookupService();
            lookup = lookups.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, code);
        }
        CollectionProperty property = getCollectionProperty(TYPE);
        List<?> values = property.getValues();
        if (!values.isEmpty()) {
            property.remove(values.get(0));
        }
        if (lookup != null) {
            property.add(lookup);
        }
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        DocumentTemplateLayoutStrategy strategy = new DocumentTemplateLayoutStrategy(getSelector());
        strategy.addComponent(new ComponentState(outputFormatEditor));
        return strategy;
    }

    /**
     * Invoked to upload a document.
     */
    @Override
    protected void onUpload() {
        UploadListener listener = new DocumentUploadListener(getDocumentHandler()) {
            protected void upload(Document doc) {
                onUpload(doc);
            }

            @Override
            protected boolean checkSupported(String fileName, String contentType) {
                boolean supported = isContentSupported(fileName, contentType);
                if (!supported) {
                    ErrorHelper.show(getUnsupportedContentMessage(fileName));
                }
                return supported;
            }
        };
        UploadDialog dialog = new UploadDialog(listener, getLayoutContext().getHelpContext());
        dialog.show();
    }

    /**
     * Validates that the content associated with the content property is supported.
     *
     * @param fileName  the file name
     * @param mimeType  the mime type
     * @param validator the validator
     * @param content   the content property
     * @return {@code true} if the content is valid, otherwise {@code false}
     */
    @Override
    protected boolean validateContent(String fileName, String mimeType, Validator validator, Property content) {
        boolean valid = isContentSupported(fileName, mimeType);
        if (!valid) {
            validator.add(content, getUnsupportedContentMessage(fileName));
        }
        return valid;
    }

    /**
     * Returns a message indicating that content is unsupported for the template type.
     *
     * @param fileName the file name
     * @return the message
     */
    private String getUnsupportedContentMessage(String fileName) {
        Lookup type = getType();
        String name = (type != null) ? type.getName() : Messages.get("imobject.none");
        return Messages.format("document.template.unsupportedcontent", fileName, name);
    }

    /**
     * Updates the document handler based on the document template type.
     * <p/>
     * This limits what may be uploaded.
     *
     * @param type the document template type code
     */
    private void updateDocumentHandler(String type) {
        ArchetypeService service = getService();
        if (DocumentTemplate.isReport(type)) {
            setDocumentHandler(new JRXMLDocumentHandler(service));
        } else if (DocumentTemplate.isForm(type)) {
            setDocumentHandler(new DelegatingDocumentHandler(new MergeableDocumentTemplateHandler(service),
                                                             new JRXMLDocumentHandler(service),
                                                             new StaticDocumentTemplateHandler(service)));
        } else {
            setDocumentHandler(new DelegatingDocumentHandler(new MergeableDocumentTemplateHandler(service),
                                                             new JRXMLDocumentHandler(service)));
        }
    }

    /**
     * Returns the document template type lookup.
     *
     * @return the lookup, or {@code null} if none has been selected
     */
    private Lookup getType() {
        return getBean(getObject()).getObject(TYPE, Lookup.class);
    }

    /**
     * Invoked when the type changes.
     * <p/>
     * Disables the outputFormatEditor if the template type isn't a letter.
     */
    private void onTypeChanged() {
        String type = getTypeCode();
        boolean newLetter = DocumentTemplate.isLetter(type);
        if (newLetter != letter) {
            letter = newLetter;
            if (!letter) {
                // output format not supported for other template types
                outputFormat.setValue(null);
            }
            outputFormatEditor.getComponent().setEnabled(letter);
        }
        updateDocumentHandler(type);
        enableUpload(type != null);
    }

    private static class MergeableDocumentTemplateHandler extends SupportedContentDocumentHandler {

        private static final String[] SUPPORTED_EXTENSIONS = {DocFormats.ODT_EXT, DocFormats.DOC_EXT,
                                                              DocFormats.RTF_EXT};

        private static final String[] SUPPORTED_MIME_TYPES = {DocFormats.ODT_TYPE, DocFormats.DOC_TYPE,
                                                              DocFormats.RTF_TYPE};

        public MergeableDocumentTemplateHandler(ArchetypeService service) {
            super(SUPPORTED_EXTENSIONS, SUPPORTED_MIME_TYPES, service);
        }
    }

    private static class StaticDocumentTemplateHandler extends SupportedContentDocumentHandler {

        private static final String[] SUPPORTED_EXTENSIONS = {DocFormats.PDF_EXT};

        private static final String[] SUPPORTED_MIME_TYPES = {DocFormats.PDF_TYPE};

        public StaticDocumentTemplateHandler(ArchetypeService service) {
            super(SUPPORTED_EXTENSIONS, SUPPORTED_MIME_TYPES, service);
        }
    }
}