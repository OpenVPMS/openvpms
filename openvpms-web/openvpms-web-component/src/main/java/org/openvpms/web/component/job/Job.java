/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.job;

import org.openvpms.component.model.user.User;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Represents a job that is run asynchronously, with optional listeners that are invoked on completion,
 * cancellation, or failure.
 *
 * @author Tim Anderson
 * @see JobManager
 */
public interface Job<T> extends Supplier<T> {

    /**
     * The name of the job.
     *
     * @return name of the job
     */
    String getName();

    /**
     * Returns the time when the job was created.
     *
     * @return the time when the job was created
     */
    Date getCreated();

    /**
     * Returns the time when the job was started.
     *
     * @return the time when the job was started, or {@code null} if the job has yet to start
     */
    Date getStarted();

    /**
     * Returns the user that initiated the job.
     *
     * @return the user that initiated the job
     */
    User getUser();

    /**
     * Returns the listener to invoke when the job completes successfully.
     *
     * @return the completion listener. May be {@code null}
     */
    Consumer<T> getCompletionListener();

    /**
     * Returns the listener to invoke when the job is cancelled.
     *
     * @return the cancellation listener. May be {@code null}
     */
    Runnable getCancellationListener();

    /**
     * Returns the listener top invoke when the job fails.
     *
     * @return the failure listener. May be {@code null}
     */
    Consumer<Throwable> getFailureListener();

    /**
     * Cancels the job.
     *
     * @param thread a handle to the job thread. This may be used to interrupt blocking threads
     */
    void cancel(JobThread thread);
}
