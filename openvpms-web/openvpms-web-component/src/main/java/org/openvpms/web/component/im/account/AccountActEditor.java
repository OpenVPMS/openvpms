/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.account;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.act.ActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;

import java.util.Date;


/**
 * Editor for account acts.
 *
 * @author Tim Anderson
 */
public abstract class AccountActEditor extends ActEditor {

    /**
     * Determines if the act has been saved with POSTED status.
     */
    private boolean savedPosted;

    /**
     * Constructs a {@link AccountActEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public AccountActEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        savedPosted = !act.isNew() && ActStatus.POSTED.equals(getSavedStatus());
    }

    /**
     * Save any edits.
     * <p/>
     * If the act status is {@link ActStatus#POSTED POSTED}, the start time will be set to the current time, as a
     * workaround for OVPMS-734.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    public void save() {
        if (!savedPosted && ActStatus.POSTED.equals(getStatus())) {
            setStartTime(new Date(), true);
            savedPosted = true;
        }
        super.save();
    }

    /**
     * Returns the object being edited.
     *
     * @return the object being edited
     */
    @Override
    public FinancialAct getObject() {
        return (FinancialAct) super.getObject();
    }

}
