/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.view;

import nextapp.echo2.app.Extent;

/**
 * Layout rendering hint.
 *
 * @author Tim Anderson
 */
public class Hint {

    /**
     * Hint builder.
     */
    public static class Builder {

        /**
         * The width.
         */
        private Extent width;

        /**
         * The height.
         */
        private Extent height;

        /**
         * Determines if labels should render multi-line.
         */
        private boolean multiline;

        /**
         * Sets the column width.
         *
         * @param width the width
         * @return the builder
         */
        public Builder width(int width) {
            return width(width, Extent.EX);
        }

        /**
         * Sets the column width.
         *
         * @param width the width
         * @param units the extent units
         * @return the builder
         */
        public Builder width(int width, int units) {
            return width(new Extent(width, units));
        }

        /**
         * Sets the width.
         *
         * @param width the width
         * @return the builder
         */
        public Builder width(Extent width) {
            this.width = width;
            return this;
        }

        /**
         * Sets the row height.
         *
         * @param height the row height
         * @return the builder
         */
        public Builder height(int height) {
            return height(height, Extent.EM);
        }

        /**
         * Sets the height.
         *
         * @param height the height
         * @param units  the extent units
         * @return the builder
         */
        public Builder height(int height, int units) {
            return height(new Extent(height, units));
        }

        /**
         * Sets the height.
         *
         * @param height the height
         * @return the builder
         */
        public Builder height(Extent height) {
            this.height = height;
            return this;
        }

        /**
         * Determines if labels should render multi-line.
         *
         * @param multiline if {@code true}, render labels multi-line, else render them on a single line
         * @return the builder
         */
        public Builder multiline(boolean multiline) {
            this.multiline = multiline;
            return this;
        }

        /**
         * Builds the hint.
         *
         * @return a new hint
         */
        public Hint build() {
            return new Hint(this);
        }
    }

    /**
     * The width.
     */
    private final Extent width;

    /**
     * The height.
     */
    private final Extent height;

    /**
     * Determines if labels should render multi-line.
     */
    private final boolean multiline;

    /**
     * Constructs a {@link Hint}.
     *
     * @param builder the builder
     */
    private Hint(Builder builder) {
        width = builder.width;
        height = builder.height;
        multiline = builder.multiline;
    }

    /**
     * Returns the width.
     *
     * @return the width, or {@code null} if none is specified
     */
    public Extent getWidth() {
        return width;
    }

    /**
     * Determines if default width should be used.
     *
     * @return {@code true} if the default width should be used, otherwise {@code false}
     */
    public boolean useDefaultWidth() {
        return width == null;
    }

    /**
     * Returns the height.
     *
     * @return the height, or {@code null} if none is specified
     */
    public Extent getHeight() {
        return height;
    }

    /**
     * Determines if the default height should be used.
     *
     * @return {@code true} if the default height should be used, otherwise {@code false}
     */
    public boolean useDefaultHeight() {
        return height == null;
    }

    /**
     * Determines if the component should render in a single row.
     *
     * @return {@code true} if the component should render in a single row
     */
    public boolean singleRow() {
        return height != null && height.getValue() == 1
               && (height.getUnits() == Extent.EX || height.getUnits() == Extent.EM);
    }

    /**
     * Determines if new lines should be preserved in labels.
     *
     * @return {@code true} if new lines should be preserved in labels, {@code false} if they should be rendered in a
     * single line
     */
    public boolean isMultiline() {
        return multiline;
    }

    /**
     * Creates a hint with a column width.
     *
     * @param width the column width
     * @return a new hint
     */
    public static Hint width(int width) {
        return newHint().width(width).build();
    }

    /**
     * Creates a hint with a width extent.
     *
     * @param width the width extent
     * @return a new hint
     */
    public static Hint width(Extent width) {
        return newHint().width(width).build();
    }

    /**
     * Creates a hint with a row height.
     *
     * @param height the row height
     * @return a new hint
     */
    public static Hint height(int height) {
        return newHint().height(height).build();
    }

    /**
     * Creates a hint with a column width and row height.
     *
     * @param width  the column width
     * @param height the row height
     * @return a new hint
     */
    public static Hint size(int width, int height) {
        return newHint().width(width).height(height).build();
    }

    /**
     * Creates a hint to indicate that new lines in labels should be preserved.
     *
     * @return a new hint
     */
    public static Hint multiline() {
        return newHint().multiline(true).build();
    }

    /**
     * Returns a new {@link Hint} builder.
     *
     * @return a new hint builder
     */
    public static Builder newHint() {
        return new Builder();
    }
}
