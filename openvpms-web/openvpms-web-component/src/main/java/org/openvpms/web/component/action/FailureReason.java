/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.Reference;

/**
 * Describes the reason for an action failure.
 *
 * @author Tim Anderson
 */
public class FailureReason {

    /**
     * Reason for the failure.
     */
    public enum Reason {
        ERROR,               // failure due to non-specific error. The message describes it.
        EXCEPTION,           // failure due to exception.
        OBJECT_NOT_FOUND,    // failure due to an object not being found
        OBJECT_CHANGED       // failure due to an object being changed
    }

    /**
     * The reason for the failure.
     */
    private final Reason reason;

    /**
     * A descriptive reason for the failure.
     */
    private final String message;

    /**
     * The reference to the object that was changed or not found. May be {@code null}
     */
    private final Reference reference;

    /**
     * The exception that caused the failure. May be {@code null}.
     */
    private final Throwable exception;

    /**
     * Constructs a {@link FailureReason}.
     *
     * @param reason    the reason
     * @param message   a message describing the reason
     * @param reference the reference to the object that caused the failure. May be {@code null}
     * @param exception the exception that caused the failure. May be {@code null}
     */
    private FailureReason(Reason reason, String message, Reference reference, Throwable exception) {
        this.reason = reason;
        this.reference = reference;
        this.exception = exception;
        this.message = message;
    }

    /**
     * Returns the reason for the failure.
     *
     * @return the reason
     */
    public Reason getReason() {
        return reason;
    }

    /**
     * Returns a message describing the reason.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the reference to the object that caused the failure.
     *
     * @return the reference to the object. May be {@code null}
     */
    public Reference getReference() {
        return reference;
    }

    /**
     * Returns the exception that caused the failure.
     *
     * @return the exception. May be {@code null}
     */
    public Throwable getException() {
        return exception;
    }

    /**
     * Returns a string representation of this.
     *
     * @return a string representation of this
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(reason);
        result.append(", message=").append(message);
        if (reference != null) {
            result.append(", reference=").append(reference);
        }
        if (exception != null) {
            result.append(",exception=").append(exception);
        }
        return result.toString();
    }

    /**
     * Creates a reason caused by a non-specific error.
     *
     * @param message the error message
     * @return a new reason
     */
    public static FailureReason error(String message) {
        return new FailureReason(Reason.ERROR, message, null, null);
    }

    /**
     * Creates a reason caused by an object not being found.
     *
     * @param message a descriptive message
     * @return a new reason
     */
    public static FailureReason objectNotFound(String message) {
        return new FailureReason(Reason.OBJECT_NOT_FOUND, message, null, null);
    }

    /**
     * Creates a reason caused by an object not being found.
     *
     * @param reference the reference
     * @param message   a descriptive message
     * @return a new reason
     */
    public static FailureReason objectNotFound(Reference reference, String message) {
        return new FailureReason(Reason.OBJECT_NOT_FOUND, message, reference, null);
    }


    /**
     * Creates a reason caused by an object being changed.
     *
     * @param message   a descriptive message
     * @return a new reason
     */
    public static FailureReason objectChanged(String message) {
        return new FailureReason(Reason.OBJECT_CHANGED, message, null, null);
    }

    /**
     * Creates a reason caused by an object being changed.
     *
     * @param reference the reference
     * @param message   a descriptive message
     * @return a new reason
     */
    public static FailureReason objectChanged(Reference reference, String message) {
        return new FailureReason(Reason.OBJECT_CHANGED, message, reference, null);
    }

    /**
     * Creates a reason caused by an exception.
     *
     * @param exception the exception
     * @return a new reason
     */
    public static FailureReason exception(Throwable exception) {
        String message = exception.getMessage();
        if (message == null) {
            message = exception.toString();
        }
        return exception(message, exception);
    }

    /**
     * Creates a reason caused by an exception.
     *
     * @param message a descriptive message
     * @param exception the exception
     * @return a new reason
     */
    public static FailureReason exception(String message, Throwable exception) {
        return new FailureReason(Reason.EXCEPTION, message, null, exception);
    }
}