/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.print;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.report.DocumentConverter;
import org.openvpms.web.component.print.ProtectedPrinterServiceLocator;

/**
 * Factory for {@link PrinterContext} instances.
 *
 * @author Tim Anderson
 */
public class PrinterContextFactory {

    /**
     * The printer locator.
     */
    private final DocumentPrinterServiceLocator locator;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The document converter.
     */
    private final DocumentConverter converter;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link PrinterContextFactory}.
     *
     * @param locator       the printer locator
     * @param service       the archetype service
     * @param converter     the document converter
     * @param domainService the domain object service
     */
    public PrinterContextFactory(DocumentPrinterServiceLocator locator, IArchetypeService service,
                                 DocumentConverter converter, DocumentHandlers handlers, DomainService domainService) {
        this.locator = locator;
        this.service = service;
        this.converter = converter;
        this.handlers = handlers;
        this.domainService = domainService;
    }

    /**
     * Creates a new {@link PrinterContext}.
     *
     * @return a new printer context
     */
    public PrinterContext create() {
        return new PrinterContext(new ProtectedPrinterServiceLocator(locator), service, converter, handlers,
                                  domainService);
    }
}