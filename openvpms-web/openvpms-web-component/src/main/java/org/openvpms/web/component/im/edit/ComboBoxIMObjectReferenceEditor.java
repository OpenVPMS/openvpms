/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.bound.Binder;
import org.openvpms.web.component.bound.BoundComboBox;
import org.openvpms.web.component.bound.BoundIMObjectReferenceComboBox;
import org.openvpms.web.component.bound.ComboBoxPropertyEditor;
import org.openvpms.web.component.edit.AbstractPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.focus.FocusGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link IMObjectReferenceEditor} implemented using a combo box.
 * <p/>
 * Subclasses must provide the list of objects to select from.
 *
 * @author Tim Anderson
 */
public abstract class ComboBoxIMObjectReferenceEditor<T extends IMObject>
        extends AbstractPropertyEditor implements IMObjectReferenceEditor<T> {

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * The editor.
     */
    private ComboBoxPropertyEditor<T> editor;

    /**
     * Constructs a {@link ComboBoxIMObjectReferenceEditor}.
     *
     * @param property the reference property
     * @param context  the layout context
     */
    public ComboBoxIMObjectReferenceEditor(Property property, LayoutContext context) {
        super(property);
        this.layoutContext = context;
    }

    /**
     * Sets the value property to the supplied object.
     *
     * @param object the object. May  be {@code null}
     * @return {@code true} if the value was set, {@code false} if it cannot be set due to error, or is the same as
     * the existing value
     */
    @Override
    public boolean setObject(T object) {
        return updateProperty(object);
    }

    /**
     * Returns the object corresponding to the reference.
     *
     * @return the object, or {@code null} if the reference is {@code null} or the object no longer exists
     */
    @Override
    @SuppressWarnings("unchecked")
    public T getObject() {
        T object = null;
        Property property = getProperty();
        Reference reference = property.getReference();
        if (reference != null) {
            object = (T) IMObjectHelper.getObject(reference, property.getArchetypeRange(), layoutContext.getContext());
        }
        return object;
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        if (editor == null) {
            // enable subclasses to monitor property updates without registering listeners
            BoundComboBox<T> combo = new BoundIMObjectReferenceComboBox<T>(getProperty(), getSelectable()) {
                @Override
                protected Binder createBinder(Property property) {
                    return new IMObjectComboBoxBinder(property) {
                        @Override
                        @SuppressWarnings("unchecked")
                        protected boolean updateProperty(Property property, Object object) {
                            return ComboBoxIMObjectReferenceEditor.this.updateProperty((T) object);
                        }
                    };
                }
            };
            editor = new ComboBoxPropertyEditor<>(combo);
        }
        return editor.getComponent();
    }

    /**
     * Determines if the reference is null.
     * This treats an entered but incorrect name as being non-null.
     *
     * @return {@code true} if the reference is null; otherwise {@code false}
     */
    @Override
    public boolean isNull() {
        return getProperty().getValue() == null && StringUtils.isEmpty(editor.getComponent().getText());
    }

    /**
     * Determines if objects may be created.
     *
     * @param create if {@code true}, objects may be created
     */
    @Override
    public void setAllowCreate(boolean create) {
        // not supported
    }

    /**
     * Determines if objects may be created.
     *
     * @return {@code true} if objects may be created
     */
    @Override
    public boolean allowCreate() {
        return false;
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group, or {@code null} if the editor hasn't been rendered
     */
    @Override
    public FocusGroup getFocusGroup() {
        return editor.getFocusGroup();
    }

    /**
     * Updates the underlying property with the specified value.
     *
     * @param value the value to update with. May be {@code null}
     * @return {@code true} if the property was modified
     */
    protected boolean updateProperty(T value) {
        boolean modified;
        Property property = getProperty();
        if (value != null) {
            modified = property.setValue(value.getObjectReference());
        } else {
            modified = property.setValue(null);
        }
        return modified;
    }

    /**
     * Returns the selectable objects.
     * <p/>
     * These are the {@link #getObjects() default objects} plus the {@link #getObject() current object} if it is not
     * present.<p/>
     * This is to allow display of objects which are inactive or not accessible to the current user.
     *
     * @return the selectable objects
     */
    protected List<T> getSelectable() {
        List<T> objects = getObjects();
        T current = getObject();
        if (!objects.contains(current)) {
            objects = new ArrayList<>(objects);
            objects.add(current);
        }
        return objects;
    }

    /**
     * Returns the default objects to select from.
     *
     * @return the default objects to select from
     */
    protected abstract List<T> getObjects();

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return layoutContext;
    }

}
