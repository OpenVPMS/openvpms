/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workspace;

import echopointng.KeyStrokes;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.button.AbstractButton;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.app.ProtectedLocationContext;
import org.openvpms.web.component.app.UserMailContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.delete.AbstractIMObjectDeletionListener;
import org.openvpms.web.component.im.delete.ConfirmingDeleter;
import org.openvpms.web.component.im.delete.IMObjectDeleter;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.edit.IMObjectActions;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.im.util.IMObjectCreatorListener;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.Selection;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.component.print.Printer;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;


/**
 * Abstract implementation of the {@link CRUDWindow} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCRUDWindow<T extends IMObject> implements CRUDWindow<T> {

    /**
     * Edit button identifier.
     */
    public static final String EDIT_ID = "button.edit";

    /**
     * New button identifier.
     */
    public static final String NEW_ID = "button.new";

    /**
     * Delete button identifier.
     */
    public static final String DELETE_ID = "button.delete";

    /**
     * Print button identifier.
     */
    public static final String PRINT_ID = "button.print";

    /**
     * Mail button identifier.
     */
    public static final String MAIL_ID = "button.mail";

    /**
     * The archetypes that this may create.
     */
    private final Archetypes<T> archetypes;

    /**
     * The context.
     */
    private final Context context;

    /**
     * Help context.
     */
    private final HelpContext help;

    /**
     * The object.
     */
    private T object;

    /**
     * The selection path. May be {@code null}
     */
    private List<Selection> selectionPath;

    /**
     * Determines the operations that may be performed on the selected object.
     */
    private IMObjectActions<T> actions;

    /**
     * The listener.
     */
    private CRUDWindowListener<T> listener;

    /**
     * The component representing this.
     */
    private Component component;

    /**
     * The buttons.
     */
    private ButtonSet buttons;

    /**
     * Email context.
     */
    private MailContext mailContext;

    /**
     * Constructs an {@code AbstractCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create
     * @param actions    determines the operations that may be performed on the selected object. If {@code null},
     *                   actions should be registered via {@link #setActions(IMObjectActions)}
     * @param context    the context
     * @param help       the help context
     */
    public AbstractCRUDWindow(Archetypes<T> archetypes, IMObjectActions<T> actions, Context context, HelpContext help) {
        this.archetypes = archetypes;
        this.actions = actions;
        this.context = context;
        this.help = help;
    }

    /**
     * Sets the event listener.
     *
     * @param listener the event listener.
     */
    public void setListener(CRUDWindowListener<T> listener) {
        this.listener = listener;
    }

    /**
     * Returns the event listener.
     *
     * @return the event listener
     */
    public CRUDWindowListener<T> getListener() {
        return listener;
    }

    /**
     * Returns the component representing this.
     *
     * @return the component
     */
    public Component getComponent() {
        if (component == null) {
            component = doLayout();
        }
        return component;
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    public void setObject(T object) {
        this.object = object;
        this.selectionPath = null;
        context.setCurrent(object);
        getComponent();
        ButtonSet buttons = getButtons();
        if (buttons != null) {
            enableButtons(buttons, object != null);
        }
    }

    /**
     * Returns the object.
     *
     * @return the object, or {@code null} if there is none set
     */
    public T getObject() {
        return object;
    }

    /**
     * Sets the selection path.
     *
     * @param path the path. May be {@code null}
     */
    @Override
    public void setSelectionPath(List<Selection> path) {
        selectionPath = path;
    }

    /**
     * Returns the object's archetype descriptor.
     *
     * @return the object's archetype descriptor or {@code null} if there
     * is no object set
     */
    public ArchetypeDescriptor getArchetypeDescriptor() {
        T object = getObject();
        ArchetypeDescriptor archetype = null;
        if (object != null) {
            archetype = DescriptorHelper.getArchetypeDescriptor(object, getService());
        }
        return archetype;
    }

    /**
     * Creates and edits a new object.
     */
    public void create() {
        if (actions.canCreate()) {
            onCreate(getArchetypes());
        }
    }

    /**
     * Determines if the current object can be edited.
     *
     * @return {@code true} if an object exists and there is no edit button or it is enabled
     */
    public boolean canEdit() {
        return canEdit(object);
    }

    /**
     * Edits the current object.
     */
    public void edit() {
        edit(selectionPath);
    }

    /**
     * Edits the current object.
     *
     * @param path the path to view. May be {@code null}
     */
    @Override
    public void edit(List<Selection> path) {
        T object = getObject();
        if (object != null) {
            T previous = object;
            if (!object.isNew()) {
                // use the latest instance
                object = IMObjectHelper.reload(object);
            }
            if (object != null) {
                if (canEdit(object)) {
                    edit(object, path);
                } else {
                    ErrorHelper.show(Messages.format("imobject.noedit", getDisplayName(object)));
                    onRefresh(object);
                }
            } else {
                ErrorHelper.show(Messages.format("imobject.noexist", getDisplayName(previous)));
                onRefresh(previous);
            }
        }
    }

    /**
     * Deletes the current object.
     */
    public void delete() {
        T object = IMObjectHelper.reload(getObject());
        if (object == null) {
            ErrorHelper.show(Messages.format("imobject.noexist", archetypes.getDisplayName()));
            onRefresh(getObject());
        } else if (getActions().canDelete(object)) {
            delete(object);
        } else {
            deleteDisallowed(object);
        }
    }

    /**
     * Returns the context.
     *
     * @return the context
     */
    public Context getContext() {
        return context;
    }

    /**
     * Sets the mail context.
     * <p>
     * This is used to determine email addresses when mailing.
     *
     * @param context the mail context. May be {@code null}
     */
    public void setMailContext(MailContext context) {
        this.mailContext = context;
    }

    /**
     * Returns the mail context.
     *
     * @return the mail context. May be {@code null}
     */
    public MailContext getMailContext() {
        return mailContext;
    }

    /**
     * Returns the help context.
     *
     * @return the help context
     */
    public HelpContext getHelpContext() {
        return help;
    }

    /**
     * Sets the buttons.
     *
     * @param buttons the buttons
     */
    public void setButtons(ButtonSet buttons) {
        this.buttons = buttons;
        layoutButtons(buttons);
        enableButtons(buttons, getObject() != null);
    }

    /**
     * Refreshes the button display.
     */
    public void refreshButtons() {
        if (buttons != null) {
            buttons.removeAll();
            layoutButtons(buttons);
            enableButtons(buttons, getObject() != null);
        }
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeRuleService getService() {
        return ServiceHelper.getArchetypeService();
    }

    /**
     * Returns the archetypes that this may create.
     *
     * @return the archetypes
     */
    protected Archetypes<T> getArchetypes() {
        return archetypes;
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @param actions the actions
     */
    protected void setActions(IMObjectActions<T> actions) {
        this.actions = actions;
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    protected IMObjectActions<T> getActions() {
        return actions;
    }

    /**
     * Returns an {@link IMObjectDeleter} to delete an object.
     * <p>
     * This implementation returns an instance that prompts for confirmation.
     *
     * @return a new deleter
     */
    @SuppressWarnings("unchecked")
    protected IMObjectDeleter<T> getDeleter() {
        return (IMObjectDeleter<T>) ServiceHelper.getBean(ConfirmingDeleter.class);
    }

    /**
     * Lays out the component.
     *
     * @return the component
     */
    protected Component doLayout() {
        return layoutButtons();
    }

    /**
     * Lays out the buttons.
     *
     * @return the button container
     */
    protected Component layoutButtons() {
        if (buttons == null) {
            ButtonRow row = new ButtonRow("ControlRow");
            buttons = row.getButtons();
        }
        buttons.setHideDisabled(true);
        layoutButtons(buttons);
        enableButtons(buttons, false);
        return buttons.getContainer();
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(createNewButton());
        buttons.add(createEditButton());
        buttons.add(createDeleteButton());
    }

    /**
     * Helper to create a new button with id {@link #EDIT_ID} linked to {@link #edit()}.
     * Editing will only be invoked if {@link #canEdit} is {@code true}
     *
     * @return a new button
     */
    protected Button createEditButton() {
        return ButtonFactory.create(EDIT_ID, () -> {
            if (canEdit()) {
                edit();
            }
        });
    }

    /**
     * Helper to create a new button with id {@link #NEW_ID} linked to {@link #create()}.
     *
     * @return a new button
     */
    protected Button createNewButton() {
        return ButtonFactory.create(NEW_ID, this::create);
    }

    /**
     * Helper to create a new button with id {@link #DELETE_ID} linked to {@link #delete()}.
     *
     * @return a new button
     */
    protected Button createDeleteButton() {
        return ButtonFactory.create(DELETE_ID, (Runnable) this::delete);
    }

    /**
     * Helper to create a new button with id {@link #PRINT_ID} linked to {@link #onPrint()}.
     *
     * @return a new button
     */
    protected Button createPrintButton() {
        return ButtonFactory.create(PRINT_ID, this::onPrint);
    }

    /**
     * Helper to create a new button with id {@link #MAIL_ID} linked to {@link #onMail()}.
     *
     * @return a new button
     */
    protected Button createMailButton() {
        return ButtonFactory.create(MAIL_ID, this::onMail);
    }

    /**
     * Returns the button set.
     *
     * @return the button set
     */
    protected ButtonSet getButtons() {
        return buttons;
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        T object = getObject();
        buttons.setEnabled(NEW_ID, actions.canCreate());
        buttons.setEnabled(EDIT_ID, enable && actions.canEdit(object));
        buttons.setEnabled(DELETE_ID, enable && actions.canDelete(object));
    }

    /**
     * Enables/disables print, mail and print-preview.
     *
     * @param buttons the buttons
     * @param enable  if {@code true}, enable print/print preview, else disable it
     */
    protected void enablePrintPreview(ButtonSet buttons, boolean enable) {
        buttons.setEnabled(PRINT_ID, enable);
        buttons.setEnabled(MAIL_ID, enable);
        String tooltip = null;
        if (enable) {
            buttons.addKeyListener(KeyStrokes.ALT_MASK | KeyStrokes.VK_V, new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    onPreview();
                }
            });
            tooltip = Messages.get("print.preview.tooltip");
        } else {
            buttons.removeKeyListener(KeyStrokes.ALT_MASK | KeyStrokes.VK_V);
        }
        AbstractButton button = getButtons().getButton(PRINT_ID);
        if (button != null) {
            button.setToolTipText(tooltip);
        }
    }

    /**
     * Invoked when the 'new' button is pressed.
     *
     * @param archetypes the archetypes
     */
    @SuppressWarnings("unchecked")
    protected void onCreate(Archetypes<T> archetypes) {
        IMObjectCreatorListener listener = new IMObjectCreatorListener() {
            public void created(IMObject object) {
                onCreated((T) object);
            }

            public void cancelled() {
                // ignore
            }
        };

        HelpContext help = getHelpContext().subtopic("new");
        IMObjectCreator.create(archetypes, listener, help);
    }

    /**
     * Invoked when a new object has been created.
     *
     * @param object the new object
     */
    protected void onCreated(T object) {
        edit(object, null);
    }

    /**
     * Determines if an object can be edited.
     *
     * @param object the object
     * @return {@code true} if an object exists and there is no edit button or it is enabled
     */
    protected boolean canEdit(T object) {
        boolean edit = false;
        if (actions.canEdit(object)) {
            ButtonSet buttons = getButtons();
            edit = buttons != null && buttons.isEnabled(EDIT_ID);
        }
        return edit;
    }

    /**
     * Edits an object.
     *
     * @param object the object to edit
     * @param path   the selection path. May be {@code null}
     */
    protected void edit(T object, List<Selection> path) {
        try {
            HelpContext edit = createEditTopic(object);
            LayoutContext context = createLayoutContext(edit);
            IMObjectEditor editor = createEditor(object, context);
            editor.getComponent();
            edit(editor, path);
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Deletes an object.
     *
     * @param object the object to delete
     */
    protected void delete(T object) {
        IMObjectDeleter<T> deleter = getDeleter();
        HelpContext delete = getHelpContext().subtopic("delete");
        Context local = new LocalContext(context);
        // nest the context so the global context isn't updated. See OVPMS-2046
        deleter.delete(object, local, delete, new AbstractIMObjectDeletionListener<T>() {
            public void deleted(T object) {
                onDeleted(object);
            }

            public void deactivated(T object) {
                onSaved(object, false);
            }
        });
    }

    /**
     * Invoked if an object may not be deleted.
     * <p>
     * This implementation is a no-op
     *
     * @param object the object
     */
    protected void deleteDisallowed(T object) {
    }

    /**
     * Creates a topic for editing.
     *
     * @param object the object to edit
     * @return the edit topic
     */
    protected HelpContext createEditTopic(IMObject object) {
        return help.topic(object, "edit");
    }

    /**
     * Creates a topic for printing.
     *
     * @param object the object to print
     * @return the print topic
     */
    protected HelpContext createPrintTopic(IMObject object) {
        return help.topic(object, "print");
    }

    /**
     * Edits an object.
     *
     * @param editor the object editor
     * @return the edit dialog
     */
    protected EditDialog edit(IMObjectEditor editor) {
        return edit(editor, null);
    }

    /**
     * Edits an object.
     *
     * @param editor the object editor
     * @param path   the selection path. May be {@code null}
     * @return the edit dialog
     */
    @SuppressWarnings("unchecked")
    protected EditDialog edit(final IMObjectEditor editor, List<Selection> path) {
        T object = (T) editor.getObject();
        final boolean isNew = object.isNew();
        EditDialog dialog = createEditDialog(editor);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            public void onClose(WindowPaneEvent event) {
                onEditCompleted(editor, isNew);
            }
        });
        context.setCurrent(object);
        dialog.show();
        if (path != null) {
            // set the selection path after showing the dialog so the focus moves to the leaf of the selection path
            editor.setSelectionPath(path);
        }
        return dialog;
    }

    /**
     * Invoked when the 'print' button is pressed.
     */
    protected void onPrint() {
        run(this::print, getObject(), "printdialog.title");
    }

    /**
     * Invoked when the 'mail' button is pressed.
     */
    protected void onMail() {
        run(this::mail, getObject(), "mail.title");
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit.
     * @param context the layout context
     * @return a new editor
     */
    protected IMObjectEditor createEditor(T object, LayoutContext context) {
        return ServiceHelper.getBean(IMObjectEditorFactory.class).create(object, context);
    }

    /**
     * Creates a new edit dialog.
     * <p>
     * This implementation uses {@link EditDialogFactory#create}.
     *
     * @param editor the editor
     * @return a new edit dialog
     */
    protected EditDialog createEditDialog(IMObjectEditor editor) {
        return ServiceHelper.getBean(EditDialogFactory.class).create(editor, context);
    }

    /**
     * Invoked when the editor is closed.
     *
     * @param editor the editor
     * @param isNew  determines if the object is a new instance
     */
    @SuppressWarnings("unchecked")
    protected void onEditCompleted(IMObjectEditor editor, boolean isNew) {
        if (editor.isDeleted()) {
            onDeleted((T) editor.getObject());
        } else if (editor.isSaved() && !editor.isCancelled()) {
            onSaved(editor, isNew);
        } else {
            // cancelled/no changes to save
            onRefresh(editor);
        }
    }

    /**
     * Invoked when the editor is saved and closed.
     *
     * @param editor the editor
     * @param isNew  determines if the object is a new instance
     */
    @SuppressWarnings("unchecked")
    protected void onSaved(IMObjectEditor editor, boolean isNew) {
        onSaved((T) editor.getObject(), isNew);
        setSelectionPath(editor.getSelectionPath());
    }

    /**
     * Invoked when the object has been saved.
     *
     * @param object the object
     * @param isNew  determines if the object is a new instance
     */
    protected void onSaved(T object, boolean isNew) {
        setObject(object);
        if (listener != null) {
            listener.saved(object, isNew);
        }
    }

    /**
     * Invoked when the object has been deleted.
     *
     * @param object the object
     */
    protected void onDeleted(T object) {
        setObject(null);
        if (listener != null) {
            listener.deleted(object);
        }
    }

    /**
     * Creates a new printer.
     *
     * @param object the object to print
     * @return an instance of {@link InteractiveIMPrinter}.
     * @throws OpenVPMSException for any error
     */
    protected IMPrinter<T> createPrinter(T object) {
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(object, context);
        IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
        IMPrinter<T> printer = factory.create(object, locator, context);
        HelpContext help = createPrintTopic(object);
        InteractiveIMPrinter<T> interactive = new InteractiveIMPrinter<>(printer, context, help);
        interactive.setMailContext(getMailContext());
        return interactive;
    }

    /**
     * Invoked to preview the current object.
     */
    protected void onPreview() {
        run(this::preview, getObject(), true, "preview.title");
    }

    /**
     * Previews an object.
     *
     * @param object the object to preview
     */
    protected void preview(T object) {
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(object, context);
        IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
        IMPrinter<T> printer = factory.create(object, locator, context);
        preview(printer);
    }

    /**
     * Previews a document.
     *
     * @param printer the printer
     */
    protected void preview(Printer printer) {
        DocumentJobManager manager = ServiceHelper.getBean(DocumentJobManager.class);
        manager.preview(printer, getContext().getUser());
    }

    /**
     * Invoked on editor completion, when the object needs to be refreshed.
     *
     * @param editor the editor
     */
    @SuppressWarnings("unchecked")
    protected void onRefresh(IMObjectEditor editor) {
        onRefresh((T) editor.getObject());
        setSelectionPath(editor.getSelectionPath());
    }

    /**
     * Invoked when the object needs to be refreshed.
     *
     * @param object the object
     */
    protected void onRefresh(T object) {
        setObject(null);
        if (listener != null) {
            listener.refresh(object);
        }
    }

    /**
     * Creates a layout context for editing an object.
     *
     * @param help the help context
     * @return a new layout context.
     */
    protected LayoutContext createLayoutContext(HelpContext help) {
        return new DefaultLayoutContext(true, new ProtectedLocationContext(getContext()), help);
    }

    /**
     * Prints an object.
     *
     * @param object the object to print
     */
    protected void print(T object) {
        IMPrinter<T> printer = createPrinter(object);
        printer.print();
    }

    /**
     * Mail an object.
     *
     * @param object the object to mail
     */
    protected void mail(T object) {
        IMPrinter<T> printer = createPrinter(object);
        mail(printer);
    }

    /**
     * Mails a document generated by the specified printer.
     *
     * @param printer the printer
     */
    protected <X> void mail(IMPrinter<X> printer) {
        Job<Document> job = JobBuilder.<Document>newJob(printer.getDisplayName(),
                                                        getContext().getUser())
                .get(() -> printer.getDocument(DocFormats.PDF_TYPE, true))
                .completed((document) -> mail(printer.getDisplayName(), document, printer.getReporter()))
                .build();
        DocumentJobManager manager = ServiceHelper.getBean(DocumentJobManager.class);
        manager.runInteractive(job, Messages.get("document.mail.title"), Messages.get("document.mail.cancel"));
    }

    /**
     * Mails a document.
     *
     * @param subject  the mail subject
     * @param document the document
     * @param reporter the reporter to use for email templates
     */
    protected <X> void mail(String subject, Document document, Reporter<X> reporter) {
        MailDialogFactory factory = ServiceHelper.getBean(MailDialogFactory.class);
        MailContext mailContext = getMailContext();
        if (mailContext == null) {
            mailContext = new UserMailContext(getContext());
        }
        MailDialog dialog = factory.create(mailContext, createLayoutContext(getHelpContext()));
        MailEditor editor = dialog.getMailEditor();
        editor.setSubject(subject);
        editor.addAttachment(document);

        //  make the object available to the editor for subsequent template expansion
        editor.setObject(reporter.getObject());

        DocumentTemplate template = reporter.getTemplate();
        if (template != null) {
            EmailTemplate emailTemplate = template.getEmailTemplate();
            if (emailTemplate != null) {
                editor.setContent(emailTemplate);
            }
        }
        dialog.show();
    }

    /**
     * Updates the context for the specified short name. If the supplied object matches the short name, it will be
     * added to the context, otherwise, the context entry will be set null.
     *
     * @param shortName the short name to match
     * @param object    the object. May be {@code null}
     */
    protected void updateContext(String shortName, T object) {
        if (TypeHelper.isA(object, shortName)) {
            context.setObject(shortName, object);
        } else {
            context.setObject(shortName, null);
        }
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @param action    the action to execute, when the selected object is an instance of {@code archetype}
     * @param title     the title resource bundle key, used when displaying an error dialog if the action fails.
     *                  May be {@code null}
     * @return a new listener
     */
    protected Runnable action(String archetype, Consumer<T> action, String title) {
        return action(new String[]{archetype}, action, title);
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetypes the archetypes. May contain wildcards
     * @param action     the action to execute, when the selected object is an instance of {@code archetype}
     * @param title      the title resource bundle key, used when displaying an error dialog if the action fails.
     *                   May be {@code null}
     * @return a new listener
     */
    protected Runnable action(String[] archetypes, Consumer<T> action, String title) {
        return action(archetypes, action, true, title);
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @param action    the action to execute, when the selected object is an instance of {@code archetype}
     * @param required  if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title     the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(String archetype, Consumer<T> action, boolean required, String title) {
        return action(new String[]{archetype}, action, required, title);
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetypes the archetypes. May contain wildcards
     * @param action     the action to execute, when the selected object is an instance of {@code archetype}
     * @param required   if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title      the title resource bundle key, used when displaying an error dialog if the action fails.
     *                   May be {@code null}
     * @return a new listener
     */
    protected Runnable action(String[] archetypes, Consumer<T> action, boolean required, String title) {
        return action(archetypes, action, null, required, title);
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetype the archetype. May contain wildcards
     * @param action    the action to execute, when the selected object is an instance of {@code archetype}
     * @param condition the condition that must evaluate {@code true} in order to run the action. May be {@code null}
     * @param title     the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(String archetype, Consumer<T> action, Predicate<T> condition, String title) {
        return action(new String[]{archetype}, action, condition, title);
    }


    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetypes the archetypes. May contain wildcards
     * @param action     the action to execute, when the selected object is an instance of {@code archetype}
     * @param condition  the condition that must evaluate {@code true} in order to run the action. May be {@code null}
     * @param title      the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(String[] archetypes, Consumer<T> action, Predicate<T> condition, String title) {
        return action(archetypes, action, condition, true, title);
    }


    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object, but only if it matches the supplied archetype.
     *
     * @param archetypes the archetypes. May contain wildcards
     * @param action     the action to execute, when the selected object is an instance of {@code archetype}
     * @param condition  the condition that must evaluate {@code true} in order to run the action. May be {@code null}
     * @param required   if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title      the title resource bundle key, used when displaying an error dialog if the action fails.
     *                   May be {@code null}
     * @return a new listener
     */
    protected Runnable action(String[] archetypes, Consumer<T> action, Predicate<T> condition, boolean required,
                              String title) {
        return () -> {
            T object = getObject();
            if (TypeHelper.isA(object, archetypes)) {
                AbstractCRUDWindow.this.run(action, condition, object, required, title);
            }
        };
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object.
     *
     * @param action the action to execute
     * @param title  the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(Consumer<T> action, String title) {
        return action(action, true, title);
    }

    /**
     * Creates a button action that runs an action against the selected object when clicked.
     * <p>
     * The action is run with the latest instance of the selected object.
     *
     * @param action   the action to execute
     * @param required if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title    the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(Consumer<T> action, boolean required, String title) {
        return action(action, null, required, title);
    }

    /**
     * Creates an action listener that runs an action against the selected object when clicked.
     * <p/>
     * The action is run with the latest instance of the selected object.
     * <p/>
     * If a condition is specified, this must evaluate {@code true} for the action to run.
     *
     * @param action    the action to execute
     * @param condition the condition that must evaluate {@code true} in order to run the action. May be {@code null}
     * @param required  if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title     the title resource bundle key, used when displaying an error dialog if the action fails
     * @return a new listener
     */
    protected Runnable action(Consumer<T> action, Predicate<T> condition, boolean required, String title) {
        return () -> {
            T object = getObject();
            AbstractCRUDWindow.this.run(action, condition, object, required, title);
        };
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return getService().getBean(object);
    }

    /**
     * Returns the display name for an object.
     *
     * @param object the object
     * @return the object's display name
     */
    protected String getDisplayName(IMObject object) {
        return DescriptorHelper.getDisplayName(object, getService());
    }

    /**
     * Returns the display name for a node.
     *
     * @param object the object
     * @param node   the node
     * @return a display name for the node, or {@code null} if none exists
     */
    protected String getDisplayName(IMObject object, String node) {
        return DescriptorHelper.getDisplayName(object, node, getService());
    }

    /**
     * Returns the display name for an archetype.
     *
     * @param archetype the archetype
     * @return the object's display name
     */
    protected String getDisplayName(String archetype) {
        return DescriptorHelper.getDisplayName(archetype, getService());
    }

    /**
     * Runs an action with the latest instance of an object.
     *
     * @param action the action
     * @param object the object. May be {@code null}
     * @param title  the title resource bundle key, used when displaying an error dialog if the action fails
     */
    private void run(Consumer<T> action, T object, String title) {
        run(action, object, true, title);
    }

    /**
     * Runs an action with the latest instance of an object.
     *
     * @param action   the action
     * @param object   the object. May be {@code null}
     * @param required if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param title    the title resource bundle key, used when displaying an error dialog if the action fails.
     *                 May be {@code null}
     */
    private void run(Consumer<T> action, T object, boolean required, String title) {
        run(action, null, object, required, title);
    }

    /**
     * Runs an action with the latest instance of an object.
     *
     * @param action    the action
     * @param condition the condition that must evaluate {@code true} in order to run the action. May be {@code null}
     * @param object    the object. May be {@code null}
     * @param required  if {@code true}, the object is required, otherwise the action supports a {@code null} argument
     * @param titleKey  the title resource bundle key, used when displaying an error dialog if the action fails.
     *                  May be {@code null}
     */
    private void run(Consumer<T> action, Predicate<T> condition, T object, boolean required, String titleKey) {
        try {
            T latest = IMObjectHelper.reload(object);
            if (latest != null || !required) {
                if (condition != null) {
                    if (condition.test(latest)) {
                        action.accept(latest);
                    } else {
                        // condition no longer true
                        onRefresh(object);
                    }
                } else {
                    action.accept(latest);
                }
            } else {
                String displayName = object != null ? getDisplayName(object)
                                                    : getArchetypes().getDisplayName();
                String title = titleKey != null ? Messages.get(titleKey) : null;
                ErrorHelper.show(title, Messages.format("imobject.noexist", displayName));
                if (object != null) {
                    onRefresh(object);
                }
            }
        } catch (Throwable exception) {
            String title = titleKey != null ? Messages.get(titleKey) : null;
            if (object != null) {
                String displayName = getDisplayName(object);
                ErrorHelper.show(title, displayName, object, exception);
            } else {
                ErrorHelper.show(title, exception);
            }
        }
    }

}
