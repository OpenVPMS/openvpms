/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.payment;

import echopointng.HttpPaneEx;
import org.openvpms.eftpos.internal.event.EFTPOSEventMonitor;
import org.openvpms.eftpos.service.WebTransactionDisplay;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.util.ApplicationInstanceConsumer;
import org.openvpms.web.echo.util.TaskQueues;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * Dialog to display {@link WebTransactionDisplay} instances.
 *
 * @author Tim Anderson
 */
public class WebEFTPaymentDialog extends ModalDialog {

    /**
     * The transaction display.
     */
    private final WebTransactionDisplay display;

    /**
     * The EFTPOS event monitor.
     */
    private final EFTPOSEventMonitor monitor;

    /**
     * The print listener.
     */
    private ApplicationInstanceConsumer<Receipt> printListener;

    /**
     * The completion listener.
     */
    private ApplicationInstanceConsumer<Transaction> completionListener;

    /**
     * Constructs a {@link WebEFTPaymentDialog}.
     *
     * @param display the transaction display
     * @param help    the help context
     */
    public WebEFTPaymentDialog(WebTransactionDisplay display, HelpContext help) {
        super(Messages.get("customer.payment.eft.title"), new String[]{CANCEL_ID}, help);
        this.display = display;
        setContentWidth(display.getWidth());
        setContentHeight(display.getHeight());
        monitor = ServiceHelper.getBean(EFTPOSEventMonitor.class);
    }

    /**
     * Show the window.
     */
    @Override
    public void show() {
        super.show();

        // need to set up listeners after the dialog is shown, otherwise they will be suspended.
        printListener = new ApplicationInstanceConsumer<>(0, TaskQueues.QueueMode.QUEUE_LAST, this::onPrint);
        completionListener = new ApplicationInstanceConsumer<>(0, TaskQueues.QueueMode.QUEUE_LAST, this::onCompleted);
        monitor.addTransactionListener(display.getTransaction(), completionListener);

        HttpPaneEx frame = new HttpPaneEx(display.getUrl());
        getLayout().add(frame);
    }

    /**
     * Disposes of this instance.
     */
    @Override
    public void dispose() {
        super.dispose();
        if (printListener != null) {
            printListener.dispose();
            ;
        }
        if (completionListener != null) {
            completionListener.dispose();
        }
    }

    /**
     * Prints a receipt.
     *
     * @param receipt the receipt
     */
    private void onPrint(Receipt receipt) {
        InformationDialog.show("Merchant Receipt", receipt.getReceipt(),
                               new PopupDialogListener() {
                                   @Override
                                   public void onOK() {
                                       WebEFTPaymentDialog.this.close(OK_ID);
                                   }
                               });
    }

    private void onCompleted(Transaction transaction) {
        // only display the notification if the dialog is still popped up
        if (getParent() != null) {
            if (transaction.getStatus() == Transaction.Status.APPROVED) {
                InformationDialog.show("Transaction complete",
                                       new PopupDialogListener() {
                                           @Override
                                           public void onOK() {
                                               WebEFTPaymentDialog.this.close(OK_ID);
                                           }
                                       });

            } else {
                ErrorDialog.show("EFT", "Failed to perform transaction: "
                                        + transaction.getStatus(),
                                 new PopupDialogListener() {
                                     @Override
                                     public void onOK() {
                                         WebEFTPaymentDialog.this.close(CANCEL_ID);
                                     }
                                 });
            }
        }
    }
}
