/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.alert;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.account.AccountType;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.view.IMObjectViewer;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * Viewer for an {@link Alert}.
 *
 * @author Tim Anderson
 */
public class AlertViewer {

    /**
     * The alert.
     */
    private final Alert alert;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * Constructs an {@link AlertViewer}.
     *
     * @param alert   the alert
     * @param context the context
     * @param help    the help context
     */
    public AlertViewer(Alert alert, Context context, HelpContext help) {
        this.alert = alert;
        this.context = context;
        this.help = help;
    }

    /**
     * Returns the component to view the alert.
     *
     * @return the component
     */
    public Component getComponent() {
        Column column = ColumnFactory.create();
        AccountType accountType = null;
        Act act;
        if (alert instanceof AccountTypeAlert) {
            // AccountTypeAlerts don't have an act, so create a dummy one for rendering purposes.
            ArchetypeService service = ServiceHelper.getArchetypeService();
            act = service.create(CustomerArchetypes.ALERT, Act.class);
            IMObjectBean bean = service.getBean(act);
            AccountTypeAlert accountTypeAlert = (AccountTypeAlert) alert;
            bean.setValue("startTime", null);
            bean.setTarget("customer", accountTypeAlert.getCustomer());
            bean.setValue("alertType", accountTypeAlert.getAlertType().getCode());
            accountType = accountTypeAlert.getAccountType();
        } else {
            act = alert.getAlert();
        }
        if (act != null) {
            IMObjectViewer viewer = new IMObjectViewer(act, null, new ActiveAlertLayoutStrategy(accountType),
                                                       new DefaultLayoutContext(context, help));
            column.add(viewer.getComponent());
        }
        return column;
    }
}
