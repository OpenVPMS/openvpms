/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import nextapp.echo2.app.Component;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.IMObjectCollectionViewer;
import org.openvpms.web.component.im.view.SingleIMObjectCollectionViewer;
import org.openvpms.web.component.property.CollectionProperty;

/**
 * A {@link IMObjectCollectionViewer} for collections of {@link Participation} containing at most one element.
 *
 * @author Tim Anderson
 */
public class SingleParticipationCollectionViewer extends SingleIMObjectCollectionViewer {

    /**
     * Constructs a {@link SingleParticipationCollectionViewer}.
     *
     * @param property the collection to view
     * @param parent   the parent object
     * @param context  the layout context. May be {@code null}
     */
    public SingleParticipationCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext context) {
        super(property, parent, context);
    }

    /**
     * Creates a component for the object.
     *
     * @param object  the object
     * @param parent  the parent
     * @param context the layout context
     * @return the component
     */
    @Override
    protected Component createComponent(IMObject object, IMObject parent, LayoutContext context) {
        return createComponent(object, "entity");
    }
}