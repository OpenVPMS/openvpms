/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.job;

import echopointng.ProgressBar;
import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Color;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.TaskQueueHandle;
import nextapp.echo2.webcontainer.ContainerContext;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.model.user.User;
import org.openvpms.web.echo.dialog.MessageDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Manages scheduling of {@link Job}s.
 * <p/>
 * Two scheduling methods are provided:
 * <ul>
 *     <li>{@link JobManager#run(Job) run} - runs jobs in the background.<br/>
 *     Neither the job nor its listeners can update the UI directly.</li>
 *     <li> {@link JobManager#runInteractive(Job, String, String) runInteractive} - runs jobs in the
 *         background, but the job listeners are invoked in the UI thread.
 *     </li>
 * </ul>
 *
 * @author Tim Anderson
 */
public abstract class JobManager implements DisposableBean {

    /**
     * The no. of milliseconds to wait before displaying a cancellation dialog.
     */
    private final int timeout;

    /**
     * The active jobs.
     */
    private final Set<JobFuture<?>> futures = Collections.synchronizedSet(new HashSet<>());

    /**
     * The executor used to schedule jobs.
     */
    private final ExecutorService executor;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(JobManager.class);

    /**
     * Completion listener name, for debugging.
     */
    private static final String COMPLETION_LISTENER = "CompletionListener";

    /**
     * Cancellation listener name, for debugging.
     */
    private static final String CANCELLATION_LISTENER = "CancellationListener";

    /**
     * Failure listener name, for debugging.
     */
    private static final String FAILURE_LISTENER = "FailureListener";

    /**
     * Constructs a {@link JobManager}.
     */
    public JobManager() {
        this(1000, "Job-%d");
    }

    /**
     * Constructs a {@link JobManager}.
     *
     * @param timeout       the no. of milliseconds to wait before displaying a cancellation dialog
     * @param namingPattern the naming pattern to use for threads created by the thread factory
     */
    public JobManager(int timeout, String namingPattern) {
        this.timeout = timeout;
        ThreadFactory factory = new BasicThreadFactory.Builder()
                .namingPattern(namingPattern)
                .daemon(true)
                .build();
        executor = Executors.newCachedThreadPool(factory);
    }

    /**
     * Runs a job in the background.
     * <p/>
     * Use this for jobs whose completion, cancellation, or failure listeners do not need to be run within the UI
     * thread.
     *
     * @param job the job
     * @return a future that may be used to cancel the job. This will invoke {@link Job#cancel(JobThread)}
     */
    public <T> Future<T> run(Job<T> job) {
        JobHandle<T> handle = new JobHandle<>(job);
        CompletableFuture<T> future = CompletableFuture.supplyAsync(handle, executor);
        JobFuture<T> jobFuture = new JobFuture<>(handle, future);
        futures.add(jobFuture);
        future.whenComplete((result, throwable) -> whenCompleted(job, jobFuture, result, throwable));
        return jobFuture;
    }

    /**
     * Runs a job interactively.
     * <p/>
     * Use this for jobs whose completion, cancellation, or failure listeners must be run within the UI thread.
     *
     * @param job     the job to run
     * @param title   the cancel dialog title
     * @param message the cancel dialog message
     * @return a future that may be used to cancel the job. This will invoke {@link Job#cancel(JobThread)}
     */
    public <T> Future<T> runInteractive(Job<T> job, String title, String message) {
        State state = new State(ApplicationInstance.getActive());
        JobHandle<T> handle = new JobHandle<>(job);
        CompletableFuture<T> future = CompletableFuture.supplyAsync(handle, executor);
        JobFuture<T> jobFuture = new JobFuture<>(handle, future);
        futures.add(jobFuture);
        CancelDialog dialog = new CancelDialog(jobFuture, title, message, state);
        future.whenComplete((result, throwable) -> whenCompleted(job, state, jobFuture, dialog, result, throwable));
        try {
            jobFuture.get(timeout, TimeUnit.MILLISECONDS);
        } catch (TimeoutException exception) {
            // only show the dialog if the job doesn't complete in time
            dialog.showUnlessTaskFinished();
        } catch (Throwable exception) {
            // do nothing. whenComplete() should receive the same exception
        }
        return jobFuture;
    }

    /**
     * Invoked by the containing {@code BeanFactory} on destruction of a bean.
     */
    @Override
    public void destroy() {
        executor.shutdown();
    }

    /**
     * Queues a listener for execution in the UI thread, unless the current thread is the UI thread, in which case
     * it will be executed directly.
     * <p/>
     * This is only invoked once on completion, cancellation or failure of an interactive job.<br/>
     * NOTE: this method is only exposed for testing purposes.
     *
     * @param state    the UI state
     * @param listener the listener to execute
     */
    protected void queueListener(State state, Runnable listener) {
        state.runOrQueue(listener);
    }

    /**
     * Invoked when a background job has completed.
     *
     * @param job       the job
     * @param future    the job future
     * @param result    the result of the job
     * @param throwable any exception generated. May be {@code null}
     */
    private <T> void whenCompleted(Job<T> job, JobFuture<T> future, T result, Throwable throwable) {
        futures.remove(future);
        if (throwable != null && !future.isCancelled()) {
            jobFailed(job, getCause(throwable));
        } else if (future.isDone() && !future.isCancelled()) {
            jobCompleted(job, result);
        } else {
            jobCancelled(job);
        }
    }

    /**
     * Invoked when an interactive job has completed.
     *
     * @param job       the job
     * @param state     the application state
     * @param future    the job future
     * @param dialog    the cancellation dialog
     * @param result    the result of the job
     * @param throwable any exception generated. May be {@code null}
     */
    private <T> void whenCompleted(Job<T> job, State state, JobFuture<T> future, CancelDialog dialog,
                                   T result, Throwable throwable) {
        futures.remove(future);
        dialog.scheduleClose();
        if (throwable != null && !future.isCancelled()) {
            interactiveJobFailed(job, getCause(throwable), state);
        } else if (future.isDone() && !future.isCancelled()) {
            interactiveJobCompleted(job, result, state);
        } else {
            interactiveJobCancelled(job, state);
        }
    }

    /**
     * Returns the cause of an exception.
     *
     * @param exception the exception
     * @return the cause of the exception, if it is a {@link CompletionException}, otherwise the exception itself
     */
    private Throwable getCause(Throwable exception) {
        if (exception instanceof CompletionException && exception.getCause() != null) {
            exception = exception.getCause();
        }
        return exception;
    }

    /**
     * Invoked when a background job completes successfully.
     *
     * @param job    the job
     * @param result the result
     */
    @SuppressWarnings("unchecked")
    private void jobCompleted(Job<?> job, Object result) {
        Consumer<Object> listener = (Consumer<Object>) job.getCompletionListener();
        if (listener != null) {
            Runnable command = () -> listener.accept(result);
            runProtected(command, job, COMPLETION_LISTENER);
        }
    }

    /**
     * Invoked when a background job is cancelled.
     *
     * @param job the job
     */
    private void jobCancelled(Job<?> job) {
        Runnable listener = job.getCancellationListener();
        if (listener != null) {
            runProtected(listener, job, CANCELLATION_LISTENER);
        }
    }

    /**
     * Invoked when an interactive job completes successfully.
     *
     * @param job   the job
     * @param state the application state
     */
    @SuppressWarnings("unchecked")
    private void interactiveJobCompleted(Job<?> job, Object result, State state) {
        Consumer<Object> listener = (Consumer<Object>) job.getCompletionListener();
        if (listener != null) {
            Runnable command = () -> listener.accept(result);
            queueListener(command, job, state, COMPLETION_LISTENER);
        } else {
            state.queueDispose(); // queue self destruction
        }
    }

    /**
     * Invoked when an interactive job is cancelled.
     *
     * @param job   the job
     * @param state the application state
     */
    private void interactiveJobCancelled(Job<?> job, State state) {
        Runnable listener = job.getCancellationListener();
        if (listener != null) {
            queueListener(listener, job, state, CANCELLATION_LISTENER);
        } else {
            state.queueDispose(); // queue self destruction
        }
    }

    /**
     * Invoked when an interactive job fails.
     *
     * @param job       the job
     * @param exception the reason for the failure
     * @param state     the application state
     */
    private void interactiveJobFailed(Job<?> job, Throwable exception, State state) {
        Consumer<Throwable> listener = job.getFailureListener();
        String username = getUserName(job);
        if (listener != null) {
            Runnable command = () -> listener.accept(exception);
            queueListener(command, job, state, FAILURE_LISTENER);
        } else {
            log.warn("Job {} run by {} failed with exception {}", job.getName(), username, exception.getMessage(),
                     exception);
            state.queueDispose(); // queue self destruction
        }
    }

    /**
     * Queues a listener for execution in the UI thread, unless the current thread is the UI thread, in which case
     * it will be executed directly.
     * <p/>
     * On completion of the listener, the queue will be destroyed.
     *
     * @param listener     the listener to execute
     * @param job          the job
     * @param state        the application state
     * @param listenerName the listener name, for diagnostic purposes
     */
    private void queueListener(Runnable listener, Job<?> job, State state, String listenerName) {
        Runnable command = () -> {
            runProtected(listener, job, listenerName);
            state.dispose();
        };
        queueListener(state, command);
    }

    /**
     * Runs a listener in a try/catch block, logging any errors.
     *
     * @param listener     the listener
     * @param job          the job
     * @param listenerName the listener name, for diagnostic purposes
     */
    private void runProtected(Runnable listener, Job<?> job, String listenerName) {
        try {
            listener.run();
        } catch (Throwable exception) {
            String username = getUserName(job);
            log.error("{} for Job {} run by {} failed with exception {}", listenerName, job.getName(), username,
                      exception.getMessage(), exception);
        }
    }

    /**
     * Invoked when a background job fails.
     *
     * @param job       the job
     * @param exception the reason for the failure
     */
    private void jobFailed(Job<?> job, Throwable exception) {
        Consumer<Throwable> listener = job.getFailureListener();
        String username = getUserName(job);
        if (listener != null) {
            Runnable command = () -> listener.accept(exception);
            runProtected(command, job, FAILURE_LISTENER);
        } else {
            log.warn("Job {} run by {} failed with exception {}", job.getName(), username, exception.getMessage(),
                     exception);
        }
    }

    /**
     * Returns the username of the user that launched the job, for diagnostic purposes.
     *
     * @param job the job
     * @return the username
     */
    private String getUserName(Job<?> job) {
        User user = job.getUser();
        return (user != null) ? user.getUsername() : "<unknown>";
    }

    protected static class State {

        /**
         * The application instance reference. Use a weak reference to avoid memory leaks if the user closes their
         * session.
         */
        private final WeakReference<ApplicationInstance> appRef;

        /**
         * The no. of queued tasks.
         */
        private final AtomicInteger queued = new AtomicInteger();

        /**
         * The task queue, in order to asynchronously trigger processing. Use a weak reference to avoid memory leaks if
         * the user closes their session.<br/>
         * NOTE: the handle holds a reference to the ApplicationInstance.
         */
        private WeakReference<TaskQueueHandle> taskQueueRef;

        /**
         * Constructs a {@link State}.
         *
         * @param app the application instance
         */
        public State(ApplicationInstance app) {
            appRef = new WeakReference<>(app);
            TaskQueueHandle taskQueue = app.createTaskQueue();
            taskQueueRef = new WeakReference<>(taskQueue);
            ContainerContext context
                    = (ContainerContext) app.getContextProperty(ContainerContext.CONTEXT_PROPERTY_NAME);
            if (context != null) {
                // set the task queue to call back in 500ms
                context.setTaskQueueCallbackInterval(taskQueue, 500);
            }

            taskQueueRef = new WeakReference<>(taskQueue);
        }

        /**
         * Queues a task to be run in the UI thread. If the current thread is the UI thread, it will be invoked
         * immediately.
         *
         * @param task the task to run
         */
        public void runOrQueue(Runnable task) {
            if (isUIThread()) {
                task.run();
            } else {
                queue(task);
            }
        }

        /**
         * Queues a task to be run in the UI thread.
         *
         * @param task the task to run
         */
        public void queue(Runnable task) {
            ApplicationInstance app = appRef.get();
            TaskQueueHandle taskQueue = taskQueueRef.get();
            if (app != null && taskQueue != null) {
                queued.incrementAndGet();
                Runnable wrapper = () -> {
                    try {
                        task.run();
                    } finally {
                        queued.decrementAndGet();
                    }
                };
                app.enqueueTask(taskQueue, wrapper);
            }
        }

        /**
         * Determines if the current thread is the UI thread.
         *
         * @return {@code true} if the current thread is the UI thread, otherwise {@code false}
         */
        public boolean isUIThread() {
            ApplicationInstance app = appRef.get();
            return (app != null && app == ApplicationInstance.getActive());
        }

        /**
         * Disposes of this.
         */
        public void dispose() {
            ApplicationInstance app = appRef.get();
            TaskQueueHandle taskQueue = taskQueueRef.get();
            if (app != null && taskQueue != null) {
                app.removeTaskQueue(taskQueue);
            }
            appRef.clear();
            taskQueueRef.clear();
        }

        /**
         * Queue disposing of this. If there are no other tasks queued, dispose immediately.
         */
        public void queueDispose() {
            if (queued.get() > 0) {
                queue(this::dispose);
            } else {
                dispose();
            }
        }
    }

    /**
     * Dialog to prompt to cancel a job.
     */
    static class CancelDialog extends MessageDialog {

        private final Future<?> future;

        private final ProgressBar bar;

        private final State state;

        private final Object lock = new Object();

        private boolean closed;

        private boolean shown;

        /**
         * Constructs a {@link CancelDialog}.
         */
        public CancelDialog(Future<?> future, String title, String message, State state) {
            super(title, message, CANCEL);
            this.future = future;
            bar = new ProgressBar();
            bar.setCompletedColor(Color.GREEN);
            bar.setNumberOfBlocks(10);
            this.state = state;
            setDefaultCloseAction(null);
        }

        /**
         * Shows the dialog, unless the task has finished.
         */
        public void showUnlessTaskFinished() {
            synchronized (lock) {
                if (!closed) {
                    shown = true;
                    show();
                }
            }
        }

        /**
         * Show the window.
         */
        @Override
        public void show() {
            queueRefresh();
            super.show();
        }

        /**
         * Processes a user request to close the window (via the close button).
         */
        @Override
        public void userClose() {
            shown = false;
            if (getParent() != null) {
                super.userClose();
            }
        }

        /**
         * Schedules closure of the dialog in the UI thread, if it is visible.
         */
        public void scheduleClose() {
            synchronized (lock) {
                closed = true;
                if (shown) {
                    state.runOrQueue(this::userClose);
                }
            }
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Label content = LabelFactory.text(getMessage(), true);
            Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, content, bar);
            getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
        }

        /**
         * Cancels the operation.
         * <p/>
         * This implementation closes the dialog, setting the action to {@link #CANCEL_ID}.
         */
        @Override
        protected void doCancel() {
            if (!future.isDone()) {
                future.cancel(true);
            }
            super.doCancel();
        }

        /**
         * Queues a refresh of the display.
         */
        protected void queueRefresh() {
            state.queue(this::refresh);
        }

        /**
         * Refreshes the display if future isn't complete, else closes the dialog.
         */
        protected void refresh() {
            if (!future.isDone() && !future.isCancelled()) {
                advance();
                queueRefresh();
            } else {
                userClose();
            }
        }

        /**
         * Advances the progress bar.
         */
        private void advance() {
            int value = bar.getValue() + 1;
            if (value > bar.getMaximum()) {
                value = bar.getMinimum();
            }
            bar.setValue(value);
        }
    }

    /**
     * Handle to a job, to enable it to be interrupted.
     */
    private static class JobHandle<T> implements Supplier<T>, JobThread {

        /**
         * The job.
         */
        private final Job<T> job;

        /**
         * The job, with the security context of the original thread.
         */
        private final Supplier<T> jobWithContext;

        /**
         * Synchronisation helper.
         */
        private final Object lock = new Object();

        /**
         * The thread executing the job.
         */
        private Thread thread;

        /**
         * Constructs a {@link JobHandle}.
         *
         * @param job the job
         */
        public JobHandle(Job<T> job) {
            this.job = job;
            jobWithContext = RunAs.inheritSecurityContext(job);
        }

        /**
         * Gets a result.
         *
         * @return a result
         */
        @Override
        public T get() {
            synchronized (lock) {
                thread = Thread.currentThread();
            }
            try {
                return jobWithContext.get();
            } finally {
                synchronized (lock) {
                    thread = null;
                }
            }
        }

        /**
         * Cancels the job.
         */
        public void cancel() {
            try {
                job.cancel(this);
            } catch (Throwable exception) {
                log.debug("cancel() threw exception for job={}: {}", job.getName(), exception.getMessage(), exception);
            }
        }

        /**
         * Interrupts the job thread.
         */
        @Override
        public void interrupt() {
            synchronized (lock) {
                if (thread != null && thread != Thread.currentThread()) {
                    thread.interrupt();
                }
            }
        }
    }

    /**
     * A {@link Future} that supports cancellation of jobs.
     */
    private static class JobFuture<T> implements Future<T> {

        private final JobHandle<T> job;

        private final Future<T> future;

        private JobFuture(JobHandle<T> job, Future<T> future) {
            this.job = job;
            this.future = future;
        }

        /**
         * Attempts to cancel execution of this task.
         *
         * @param mayInterruptIfRunning {@code true} if the thread executing this task should be interrupted; otherwise,
         *                              in-progress tasks are allowed to complete
         * @return {@code false} if the task could not be cancelled, typically because it has already completed
         * normally; {@code true} otherwise
         */
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            if (!isDone()) {
                job.cancel();
            }
            return future.cancel(mayInterruptIfRunning);
        }

        /**
         * Returns {@code true} if this task was cancelled before it completed normally.
         *
         * @return {@code true} if this task was cancelled before it completed
         */
        @Override
        public boolean isCancelled() {
            return future.isCancelled();
        }

        /**
         * Returns {@code true} if this task completed.
         *
         * @return {@code true} if this task completed
         */
        @Override
        public boolean isDone() {
            return future.isDone();
        }

        /**
         * Waits if necessary for the computation to complete, and then retrieves its result.
         *
         * @return the computed result
         * @throws CancellationException if the computation was cancelled
         * @throws ExecutionException    if the computation threw an exception
         * @throws InterruptedException  if the current thread was interrupted while waiting
         */
        @Override
        public T get() throws InterruptedException, ExecutionException {
            return future.get();
        }

        /**
         * Waits if necessary for at most the given time for the computation to complete, and then retrieves its result,
         * if available.
         *
         * @param timeout the maximum time to wait
         * @param unit    the time unit of the timeout argument
         * @return the computed result
         * @throws CancellationException if the computation was cancelled
         * @throws ExecutionException    if the computation threw an exception
         * @throws InterruptedException  if the current thread was interrupted while waiting
         * @throws TimeoutException      if the wait timed out
         */
        @Override
        public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return future.get(timeout, unit);
        }
    }

}
