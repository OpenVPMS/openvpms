/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.service.logo;

import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.doc.ImageService.Image;
import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.party.Party;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * Servlet that returns the practice logo, if one is configured, otherwise the default OpenVPMS logo.
 *
 * @author Tim Anderson
 */
public class LogoServlet extends HttpServlet {

    /**
     * Caches the logo for the current request.
     */
    private final ThreadLocal<Logo> current = new ThreadLocal<>();

    /**
     * The logo service.
     */
    private LogoService logoService;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * The default logo.
     */
    private static final String DEFAULT_LOGO = "/images/openvpms.png";

    /**
     * The default logo mime type.
     */
    private static final String DEFAULT_MIME_TYPE = "image/png";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LogoServlet.class);

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        logoService = context.getBean(LogoService.class);
        practiceService = context.getBean(PracticeService.class);
    }

    /**
     * Receives standard HTTP requests from the public <code>service</code> method and dispatches
     * them to the <code>do</code><i>XXX</i> methods.
     * <p/>
     * This overrides the superclass in order to cache the logo for the current request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException      if an input or output error occurs while the servlet is handling the HTTP request
     * @throws ServletException if the HTTP request cannot be handled
     */
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // cache the logo for the request, as it's needed by getLastModified() and doGet()
            current.set(getLogo());
            super.service(request, response);
        } finally {
            current.remove();
        }
    }

    /**
     * Called by the server (via the <code>service</code> method) to allow a servlet to handle a GET request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Logo logo = current.get();
            if (logo != null) {
                response.setContentType(logo.getMimeType());
                int length = logo.getSize();
                if (length > 0) {
                    response.setContentLength(length);
                }
                try (InputStream stream = logo.getContent()) {
                    IOUtils.copy(stream, response.getOutputStream());
                } catch (IOException exception) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (Exception exception) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns the time the <code>HttpServletRequest</code> object was last modified.
     *
     * @param request the request
     * @return the time the object was last modified, in milliseconds, or -1 if the time is not known
     */
    @Override
    protected long getLastModified(HttpServletRequest request) {
        Logo logo = current.get();
        return logo != null ? logo.getLastModified() : -1;
    }

    /**
     * Returns the logo.
     *
     * @return the logo, or {@code null} if none is found
     */
    private Logo getLogo() {
        Logo result = getPracticeLogo();
        if (result == null) {
            result = getDefaultLogo();
        }
        return result;
    }

    /**
     * Returns the practice logo.
     *
     * @return the practice logo, or {@code null} if none is configured
     */
    private Logo getPracticeLogo() {
        Logo result = null;
        try {
            Party practice = practiceService.getPractice();
            if (practice != null) {
                Image image = logoService.getImage(practice);
                if (image != null) {
                    result = new PracticeLogo(image);
                }
            }
        } catch (Exception exception) {
            log.error("Failed to retrieve practice logo: {}", exception.getMessage());
        }
        return result;
    }

    /**
     * Returns the default logo.
     *
     * @return the default logo, or {@code null} if it can't be located
     */
    private Logo getDefaultLogo() {
        Logo result = null;
        try {
            URL url = getServletContext().getResource(DEFAULT_LOGO);
            if (url != null) {
                File file = new File(url.toURI());
                result = new DefaultLogo(file);
            }
        } catch (Exception exception) {
            log.error("Failed to retrieve default logo: {}", exception.getMessage());
        }
        return result;
    }

    private interface Logo {

        /**
         * Returns the logo content.
         *
         * @return a stream to the content
         * @throws IOException for any I/O error
         */
        InputStream getContent() throws IOException;

        /**
         * Returns the logo mime type.
         *
         * @return the mime type
         */
        String getMimeType();

        /**
         * Returns the last-modified timestamp.
         *
         * @return the last-modified timestamp, or {@code -1} if it is not known
         */
        long getLastModified();

        /**
         * Returns logo size.
         *
         * @return the image size, in bytes, or {@code 0} if it cannot be determined
         */
        int getSize();
    }

    /**
     * Practice logo.
     */
    private static class PracticeLogo implements Logo {

        private final Image image;

        public PracticeLogo(Image image) {
            this.image = image;
        }

        @Override
        public InputStream getContent() throws IOException {
            return image.getInputStream();
        }

        @Override
        public String getMimeType() {
            return image.getMimeType();
        }

        @Override
        public long getLastModified() {
            Date modified = image.getModified();
            return modified != null ? modified.getTime() : 0;
        }

        @Override
        public int getSize() {
            return (int) image.getSize();
        }
    }

    private static class DefaultLogo implements Logo {

        private final File file;

        public DefaultLogo(File file) {
            this.file = file;
        }

        @Override
        public InputStream getContent() throws IOException {
            return new FileInputStream(file);
        }

        @Override
        public String getMimeType() {
            return DEFAULT_MIME_TYPE;
        }

        @Override
        public long getLastModified() {
            long result = -1;
            try {
                result = file.lastModified();
            } catch (Exception e) {
                // ignore
            }
            return result;
        }

        @Override
        public int getSize() {
            int length = 0;
            try {
                length = (int) file.length();
            } catch (Exception ignore) {
                // no-op
            }
            return length;
        }
    }
}
