/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Builds a call for an {@link ActionBuilder}.
 *
 * @author Tim Anderson
 */
public class CallBuilder<T extends CallBuilder<T>> {

    public static class CallBehaviourBuilder<T extends CallBuilder<T>>
            extends Behaviour.BehaviourBuilder<CallBehaviourBuilder<T>> {

        /**
         * The parent builder.
         */
        private final T parent;

        /**
         * Constructs a {@link CallBehaviourBuilder}.
         *
         * @param parent    the parent builder
         * @param behaviour the initial behaviour
         */
        private CallBehaviourBuilder(T parent, Behaviour behaviour) {
            super(behaviour);
            this.parent = parent;
        }

        /**
         * Updates the behaviour.
         *
         * @return the list call builder
         */
        public T update() {
            return parent.behaviour(build());
        }
    }

    /**
     * The parent builder.
     */
    private final ActionBuilder parent;

    /**
     * Determines the retry behaviour.
     */
    private Behaviour behaviour;

    /**
     * Constructs a {@link CallBuilder}.
     *
     * @param parent    the parent builder
     * @param behaviour the initial behaviour
     */
    public CallBuilder(ActionBuilder parent, Behaviour behaviour) {
        this.parent = parent;
        this.behaviour = behaviour;
    }

    /**
     * Specifies to fail on retry if any object is changed or missing.
     *
     * @return this
     */
    public T failIfChangedOrMissing() {
        return behaviour().useOriginalObject().update();
    }

    /**
     * Specifies to skip the action on retry if any object is changed or missing.
     *
     * @return this
     */
    public T skipIfChangedOrMissing() {
        return behaviour().skipIfChanged().skipIfMissing().update();
    }

    /**
     * Specifies to skip the action on retry if any object is missing.
     *
     * @return this
     */
    public T skipIfMissing() {
        return skipIfMissing(true);
    }

    /**
     * Determines the behaviour if any object is missing on retry.
     *
     * @param skipIfMissing if {@code true}, skip the action if any object is missing; if {@code false}, fail
     * @return this
     */
    public T skipIfMissing(boolean skipIfMissing) {
        return behaviour().skipIfMissing(skipIfMissing).update();
    }

    /**
     * Specifies to run the action on retry, if any object has changed.
     *
     * @return this
     */
    public T runIfChanged() {
        return behaviour().runIfChanged().update();
    }

    /**
     * Indicates that the latest instances of objects should always be used.
     *
     * @return this
     */
    public T useLatestInstance() {
        return behaviour().useLatestInstance().update();
    }

    /**
     * Indicates that the latest instances of objects should be used on retry.
     *
     * @return this
     */
    public T useLatestInstanceOnRetry() {
        return behaviour().useLatestInstanceOnRetry().update();
    }

    /**
     * Returns a builder to specify the behaviour.
     *
     * @return a behaviour builder
     */
    @SuppressWarnings("unchecked")
    protected CallBehaviourBuilder<T> behaviour() {
        return new CallBehaviourBuilder<>((T) this, behaviour);
    }

    /**
     * Sets the retry behaviour.
     *
     * @param behaviour the behaviour
     * @return this
     */
    protected T behaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
        return getThis();
    }

    /**
     * Returns the parent builder.
     *
     * @return the parent builder
     */
    protected ActionBuilder getParent() {
        return parent;
    }

    /**
     * Returns the retry behaviour
     *
     * @return the retry behaviour
     */
    protected Behaviour getBehaviour() {
        return behaviour;
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    private T getThis() {
        return (T) this;
    }

    /**
     * Synchronous consumer. This calls the consumer and then the listener.
     */
    static class SyncConsumer<T> implements BiConsumer<T, Consumer<ActionStatus>> {

        private final Consumer<T> consumer;

        public SyncConsumer(Consumer<T> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void accept(T t, Consumer<ActionStatus> listener) {
            consumer.accept(t);
            listener.accept(ActionStatus.success());
        }
    }
}
