/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import nextapp.echo2.app.ApplicationInstance;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

/**
 * Runs an action repeatedly, until the action succeeds or the number of retries are exhausted.
 *
 * @author Tim Anderson
 */
class ActionRunner extends AsynchronousAction {

    /**
     * Retry button identifier.
     */
    protected static final String RETRY_ID = "button.retry";

    /**
     * An identifier for this runner, for logging purposes.
     */
    private final long id;

    /**
     * The action to run.
     */
    private final Action action;

    /**
     * The action to run on success. May be {@code null}
     */
    private final Action successAction;

    /**
     * The action to run on skip. May be {@code null}
     */
    private final Action skipAction;

    /**
     * The action to run on failure. May be {@code null}
     */
    private final FailureAction failureAction;

    /**
     * Determines if this is interactive or not.
     */
    private final boolean interactive;

    /**
     * The retry behaviour.
     */
    private final Retries retries;

    /**
     * The number of attempts at running the action.
     */
    private int attempts;

    /**
     * Seed for the id.
     */
    private static final AtomicLong seed = new AtomicLong();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ActionRunner.class);

    /**
     * Constructs an {@link ActionRunner}.
     *
     * @param action        the action to run
     * @param successAction the action to run on success. May be {@code null}
     * @param skipAction    the action to run when skipped. May be {@code null}
     * @param failureAction the action to run on failure. May be {@code null}
     * @param interactive   if {@code true}, this can display dialogs, else it logs
     * @param retries       the retry behaviour
     */
    public ActionRunner(Action action, Action successAction, Action skipAction, FailureAction failureAction,
                        boolean interactive, Retries retries) {
        this.id = seed.getAndIncrement();
        this.action = action;
        this.successAction = successAction;
        this.skipAction = skipAction;
        this.failureAction = failureAction;
        this.interactive = interactive;
        this.retries = retries;
    }

    /**
     * Runs the action.
     */
    @Override
    public void run() {
        run(new DefaultActionStatusConsumer(interactive));
    }

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    @Override
    public void run(Consumer<ActionStatus> listener) {
        attempts = 0;
        super.run(listener);
    }

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    @Override
    protected void runAction(Consumer<ActionStatus> listener) {
        attempts++;
        try {
            action.run(status -> {
                if (status.succeeded()) {
                    runSuccess(listener);
                } else if (status.canRetry()) {
                    retry(status, listener);
                } else if (status.skipped()) {
                    runSkipped(status, listener);
                } else {
                    runFailed(status, listener);
                }
            });
        } catch (Throwable exception) {
            retry(ActionStatus.retry(exception), listener);
        }
    }

    /**
     * Pauses the thread, when running automatic retries.
     * <p/>
     * If this runner is interactive, it pauses for up to a second. If it is running in the background,
     * it pauses for attempts^2 * {@link Retries#delay()} milliseconds.
     */
    protected void delay() {
        long delay = interactive ? RandomUtils.nextLong(0, 1000) : attempts * attempts * retries.delay();
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ignore) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Retries the action, according to the retry behaviour.
     *
     * @param status   the action status
     * @param listener the listener to notify on completion
     */
    private void retry(ActionStatus status, Consumer<ActionStatus> listener) {
        if (retries.automaticRetry()) {
            log("Retrying failed action", status, false);

            if (attempts > 1 || retries.delayFirst()) {
                // can skip delay on the first attempt if objects are being reloaded for the first time
                delay();
            }
            runAction(listener);
        } else if (retries.interactiveRetry() && canShowDialog()) {
            // prompt the user to retry
            status.setNotified();
            ActionBuilder.newErrorDialog(status)
                    .button(RETRY_ID, () -> {
                        log("Retrying failed action", status, false);
                        runProtected(listener);
                    })
                    .cancel(() -> runFailed(ActionStatus.failed(status), listener))
                    .show();
        } else {
            // no more retries possible
            runFailed(ActionStatus.failed(status), listener);
        }
    }

    /**
     * Runs the success action, if present.
     *
     * @param listener the listener to notify on completion
     */
    private void runSuccess(Consumer<ActionStatus> listener) {
        if (successAction != null) {
            try {
                successAction.run(listener);
            } catch (Throwable exception) {
                listener.accept(ActionStatus.failed(exception));
            }
        } else {
            listener.accept(ActionStatus.success());
        }
    }

    /**
     * Runs the skip action, if present.
     * <p/>
     * The listener will be notified of the original status, even if the skip action itself fails.
     *
     * @param status   the status
     * @param listener the listener to notify on completion
     */
    private void runSkipped(ActionStatus status, Consumer<ActionStatus> listener) {
        log("Skipping failed action", status, true);
        if (!interactive) {
            // don't log again
            status.setNotified();
        }
        if (skipAction != null) {
            try {
                skipAction.run(subStatus -> {
                    if (subStatus.failed()) {
                        // skip action has failed. Just log it, as the original skip status is most likely more relevant
                        // to users
                        log("Skip action failed", subStatus, true);
                    }
                    listener.accept(status);
                });
            } catch (Throwable exception) {
                log.error("Error running skip action: {}", exception.getMessage(), exception);
                listener.accept(status);
            }
        } else {
            listener.accept(status);
        }
    }

    /**
     * Runs the failure action, if present.
     * <p/>
     * The listener will be notified of the original failure, even if the failure action itself fails.
     *
     * @param status   the status
     * @param listener the listener to notify on completion
     */
    private void runFailed(ActionStatus status, Consumer<ActionStatus> listener) {
        log("Action failed", status, true);
        if (!interactive) {
            // don't log again
            status.setNotified();
        }
        if (failureAction != null) {
            try {
                failureAction.run(status, () -> {
                    listener.accept(status);
                });
            } catch (Throwable exception) {
                log.error("Error running failure action: {}", exception.getMessage(), exception);
                listener.accept(status);
            }
        } else {
            listener.accept(status);
        }
    }

    /**
     * Determines if this action is interactive, and currently running in the UI thread.
     *
     * @return {@code true} if dialogs can be displayed, otherwise {@code false}
     */
    private boolean canShowDialog() {
        return interactive && ApplicationInstance.getActive() != null;
    }

    /**
     * Logs a status.
     *
     * @param message the message
     * @param status  the status to log
     * @param error   if {@code true}, log with error severity, else log with debug severity
     */
    private void log(String message, ActionStatus status, boolean error) {
        if (status.getException() != null) {
            if (error) {
                log.error("{}, id={}, attempts={}, status: {}", message, id, attempts, status, status.getException());
            } else {
                log.debug("{}, id={}, attempts={}, status: {}", message, id, attempts, status, status.getException());
            }
        } else {
            if (error) {
                log.error("{}, id={}, attempts={}, status: {}", message, id, attempts, status);
            } else {
                log.debug("{}, id={}, attempts={}, status: {}", message, id, attempts, status);
            }
        }
    }
}
