/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.archetype.ValidationError;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;


/**
 * Validation helper.
 *
 * @author Tim Anderson
 */
public class ValidationHelper {

    /**
     * Validates an object.
     *
     * @param object  the object to validate
     * @param service the archetype service
     * @return a list of validation errors, or {@code null} if the object is valid
     */
    public static List<ValidatorError> validate(IMObject object, ArchetypeService service) {
        List<ValidatorError> result = null;
        List<ValidationError> errors = service.validate(object);
        if (!errors.isEmpty()) {
            result = new ArrayList<>();
            for (ValidationError error : errors) {
                result.add(new ValidatorError(error));
            }
        }
        return result;
    }

    /**
     * Displays the first error from a validator.
     * <p>
     * The error will be formatted.
     *
     * @param validator the validator
     */
    public static void showError(Validator validator) {
        showError(null, validator, null);
    }

    /**
     * Displays the first error from a validator.
     * <p>
     * The error will be formatted.
     *
     * @param title     the dialog title. May  be {@code null}
     * @param validator the validator
     * @param listener  the listener to notify when the dialog closes. May be {@code null}
     */
    public static void showError(String title, Validator validator, Runnable listener) {
        showError(title, validator, null, true, listener);
    }

    /**
     * Display the first error from a validator.
     *
     * @param title     the dialog title. May  be {@code null}
     * @param validator the validator
     * @param key       resource bundle key, if the error should be included in text. May be {@code null}
     * @param formatted if {@code true} format the message, otherwise display as is
     */
    public static void showError(String title, Validator validator, String key, boolean formatted) {
        showError(title, validator, key, formatted, null);
    }

    /**
     * Display the first error from a validator.
     *
     * @param title     the dialog title. May  be {@code null}
     * @param validator the validator
     * @param key       resource bundle key, if the error should be included in text. May be {@code null}
     * @param formatted if {@code true} format the message, otherwise display as is
     * @param listener  the listener to notify when the dialog closes. May be {@code null}
     */
    public static void showError(String title, Validator validator, String key, boolean formatted, Runnable listener) {
        ValidatorError error = validator.getFirstError();
        if (error != null) {
            String message = (formatted) ? error.toString() : error.getMessage();
            if (key != null) {
                message = Messages.format(key, message);
            }
            ErrorHandler.getInstance().error(title, message, null, listener);
        }
    }

}
