/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.DefaultIMObjectCollectionViewer;
import org.openvpms.web.component.im.view.DelegatingCollectionViewer;
import org.openvpms.web.component.im.view.IMObjectCollectionViewer;
import org.openvpms.web.component.im.view.SingleIMObjectCollectionViewer;
import org.openvpms.web.component.property.CollectionProperty;

/**
 * An {@link IMObjectCollectionViewer} for collections of {@link Lookup}s.
 * <p/>
 * For collections with:
 * <ul>
 *     <li>0..1 cardinality, this uses {@link SingleIMObjectCollectionViewer}</li>
 *     <li>* cardinality, this uses {@link DefaultIMObjectCollectionViewer}</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class LookupCollectionViewer extends DelegatingCollectionViewer {

    /**
     * Constructs a {@link LookupCollectionViewer}.
     *
     * @param property the collection property
     * @param parent   the parent object
     * @param context  the layout context
     */
    public LookupCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext context) {
        super(property.getMaxCardinality() == 1
              ? new SingleLookupCollectionViewer(property, parent, context)
              : new DefaultIMObjectCollectionViewer(property, parent, context));
    }

}
