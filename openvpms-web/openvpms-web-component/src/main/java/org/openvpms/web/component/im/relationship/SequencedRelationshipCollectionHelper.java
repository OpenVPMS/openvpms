/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


/**
 * Helper for collections of sequenced relationships.
 *
 * @author Tim Anderson
 */
public class SequencedRelationshipCollectionHelper {

    /**
     * Sorts objects on their sequence and id nodes.
     *
     * @param objects the objects. This list is modified
     * @return the objects
     */
    public static <T extends IMObject> List<T> sortByNode(List<T> objects) {
        IMObjectSorter.sort(objects, "sequence", "id");
        return objects;
    }

    /**
     * Sorts relationships by sequence, id, and linkId.
     *
     * @param objects the objects. This list is modified
     * @return the objects
     */
    public static <T extends SequencedRelationship> List<T> sort(List<T> objects) {
        objects.sort(SequencedRelationshipCollectionHelper::compare);
        return objects;
    }


    /**
     * Sorts a list of relationship states on sequence.
     *
     * @param states the states to sort
     */
    public static void sortStates(List<RelationshipState> states) {
        states.sort(SequenceComparator.INSTANCE);
    }

    /**
     * Determines if there is a sequence node in each of a set of relationship archetypes.
     *
     * @return {@code true} if all archetypes have a sequence node
     */
    public static boolean hasSequenceNode(String[] shortNames) {
        boolean hasSequence = true;
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        for (String shortName : shortNames) {
            ArchetypeDescriptor descriptor = service.getArchetypeDescriptor(shortName);
            if (descriptor != null && descriptor.getNodeDescriptor("sequence") == null) {
                hasSequence = false;
                break;
            }
        }
        return hasSequence;
    }

    /**
     * Sequences relationships, using the first relationship sequence as the starting point.
     * <p>
     * This preserves gaps in the sequence.
     *
     * @param states the states to sequence
     */
    public static void sequenceStates(List<RelationshipState> states) {
        int last = -1;
        for (RelationshipState state : states) {
            Relationship object = state.getRelationship();
            last = sequence(object, last);
        }
    }

    /**
     * Sequences relationships, using the first relationship sequence as the starting point.
     * <p>
     * This preserves gaps in the sequence.
     * <p/>
     * NOTE: The items should be sorted prior to sequencing.
     *
     * @param objects the objects to sequence
     */
    public static <T extends IMObject> void sequence(Collection<T> objects) {
        int last = -1;
        for (IMObject object : objects) {
            last = sequence(object, last);
        }
    }

    /**
     * Sorts a map of targets to relationships on their relationship sequence.
     *
     * @param map the map to sort
     * @return the sorted map entries
     */
    public static <T extends SequencedRelationship> List<Map.Entry<IMObject, T>> sort(Map<IMObject, T> map) {
        List<Map.Entry<IMObject, T>> entries = new ArrayList<>(map.entrySet());
        entries.sort((o1, o2) -> {
            T r1 = o1.getValue();
            T r2 = o2.getValue();
            return compare(r1, r2);
        });
        return entries;
    }

    /**
     * Returns the next sequence.
     *
     * @param objects the objects to search. May be unordered.
     * @return the next sequence
     */
    public static <T extends IMObject> int getNextSequence(Collection<T> objects) {
        int last = -1;
        for (IMObject object : objects) {
            if (object instanceof SequencedRelationship) {
                int sequence = ((SequencedRelationship) object).getSequence();
                if (sequence > last) {
                    last = sequence;
                }
            }
        }
        return last + 1;
    }

    /**
     * Compares two relationships on sequence, id, and linkId.
     *
     * @param x the first object to compare.
     * @param y the second object to compare
     * @return the value {@code 0} if {@code x == y};
     * a value less than {@code 0} if {@code x < y}; and
     * a value greater than {@code 0} if {@code x > y}
     */
    private static <T extends SequencedRelationship> int compare(T x, T y) {
        int compare = Integer.compare(x.getSequence(), y.getSequence());
        if (compare == 0) {
            compare = Long.compare(x.getId(), y.getId());
            if (compare == 0) {
                compare = x.getLinkId().compareTo(y.getLinkId());
            }
        }
        return compare;
    }

    private static int sequence(IMObject object, int last) {
        if (object instanceof SequencedRelationship) {
            SequencedRelationship relationship = (SequencedRelationship) object;
            if (last != -1 && relationship.getSequence() <= last) {
                relationship.setSequence(++last);
            } else {
                last = relationship.getSequence();
            }
        }
        return last;
    }

    /**
     * Comparator to help order {@link RelationshipState} instances on sequence.
     */
    private static class SequenceComparator implements Comparator<RelationshipState> {

        /**
         * Singleton instance.
         */
        public static SequenceComparator INSTANCE = new SequenceComparator();

        /**
         * Compares its two arguments for order.  Returns a negative integer,
         * zero, or a positive integer as the first argument is less than, equal
         * to, or greater than the second.<p>
         *
         * @param o1 the first object to be compared.
         * @param o2 the second object to be compared.
         * @return a negative integer, zero, or a positive integer as the
         * first argument is less than, equal to, or greater than the
         * second.
         * @throws ClassCastException if the arguments' types prevent them from
         *                            being compared by this Comparator.
         */
        public int compare(RelationshipState o1, RelationshipState o2) {
            SequencedRelationship r1 = (SequencedRelationship) o1.getRelationship();
            SequencedRelationship r2 = (SequencedRelationship) o2.getRelationship();
            return Integer.compare(r1.getSequence(), r2.getSequence());
        }
    }

}
