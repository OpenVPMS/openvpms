/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.List;


/**
 * {@link Relationship} helper.
 *
 * @author Tim Anderson
 */
public class RelationshipHelper {

    /**
     * Returns the reference to the related object in a relationship.
     *
     * @param primary      the primary object
     * @param relationship the relationship
     * @return the related (i.e. non-primary object) reference
     */
    public static Reference getRelated(IMObject primary, Relationship relationship) {
        return getRelated(primary.getObjectReference(), relationship);
    }

    /**
     * Returns the reference to the related object in a relationship.
     *
     * @param primary      the primary object reference
     * @param relationship the relationship
     * @return the related (i.e. non-primary object) reference
     */
    public static Reference getRelated(Reference primary, Relationship relationship) {
        return primary.equals(relationship.getSource()) ? relationship.getTarget() : relationship.getSource();
    }

    /**
     * Helper to return the short names for the target of a set of relationships.
     *
     * @param service           the archetype service
     * @param relationshipTypes the relationship types
     * @return the target node archetype short names
     */
    public static String[] getTargetShortNames(ArchetypeService service, String... relationshipTypes) {
        return DescriptorHelper.getNodeShortNames(relationshipTypes, "target", service);
    }

    /**
     * Returns the targets of a list of relationships.
     * <p/>
     * If a target cannot be resolved, it is silently ignored.
     *
     * @param relationships the relationships
     * @param service       the archetype service
     * @return the targets of the relationships
     */
    @SuppressWarnings("unchecked")
    public static <R extends Relationship, T extends IMObject> List<T> getTargets(List<R> relationships,
                                                                                  ArchetypeService service) {
        List<T> targets = new ArrayList<>();
        for (Relationship relationship : relationships) {
            if (relationship.getTarget() != null) {
                T target = (T) service.get(relationship.getTarget());
                if (target != null) {
                    targets.add(target);
                }
            }
        }
        return targets;
    }
}
