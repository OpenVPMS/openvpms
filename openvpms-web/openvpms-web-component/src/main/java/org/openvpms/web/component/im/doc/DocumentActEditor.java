/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.act.AbstractActEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.DocumentVersionsCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

import java.util.function.Consumer;

import static org.openvpms.web.component.im.doc.DocumentActLayoutStrategy.DOCUMENT;
import static org.openvpms.web.component.im.doc.DocumentActLayoutStrategy.VERSIONS;


/**
 * Editor for {@link DocumentAct}s.
 *
 * @author Tim Anderson
 */
public class DocumentActEditor extends AbstractActEditor {

    public enum DocumentStatus {
        NEEDS_UPDATE,  // document needs to be generated
        PROMPT,        // attribute have changed, but may not require regeneration
        UP_TO_DATE     // document is up-to-date
    }

    /**
     * The last document template.
     */
    private Reference lastTemplate;

    /**
     * The document editor. May be {@code null}.
     */
    private DocumentEditor docEditor;

    /**
     * Determines if the document needs to be regenerated.
     */
    private DocumentStatus documentStatus = DocumentStatus.UP_TO_DATE;

    /**
     * The document versions editor. May be {@code null}.
     */
    private ActRelationshipCollectionEditor versionsEditor;

    /**
     * The document template node.
     */
    private static final String DOC_TEMPLATE = "documentTemplate";


    /**
     * Constructs a {@link DocumentActEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context. May be {@code null}.
     */
    public DocumentActEditor(DocumentAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        Property document = getProperty(DOCUMENT);
        if (document != null) {
            HelpContext help = context.getHelpContext().topic("document");
            docEditor = new VersioningDocumentEditor(document, new DefaultLayoutContext(context, help));
            ModifiableListener listener = modifiable -> onDocumentUpdate();
            docEditor.addModifiableListener(listener);
            addEditor(docEditor);
        }
        Property versions = getProperty(VERSIONS);
        if (versions != null) {
            versionsEditor = new DocumentVersionsCollectionEditor((CollectionProperty) versions, act, context);
            addEditor(versionsEditor);
        }
        lastTemplate = getTemplateRef();
    }

    /**
     * Returns the object being edited.
     *
     * @return the object being edited
     */
    @Override
    public DocumentAct getObject() {
        return (DocumentAct) super.getObject();
    }

    /**
     * Sets the document template, an instance of <em>entity.documentTemplate<em>.
     *
     * @param template the template. May be {@code null}
     */
    public void setTemplate(Entity template) {
        setParticipant(DOC_TEMPLATE, template);
    }

    /**
     * Returns the document template, an instance of <em>entity.documentTemplate</em>.
     *
     * @return the document template. May be {@code null}
     */
    public Entity getTemplate() {
        return getParticipant(DOC_TEMPLATE);
    }

    /**
     * Sets a document template via its reference.
     *
     * @param template the template reference. May be {@code null}
     */
    public void setTemplateRef(Reference template) {
        setParticipant(DOC_TEMPLATE, template);
    }

    /**
     * Returns a reference to the current template, an instance of <em>entity.documentTemplate</em>.
     *
     * @return a reference to the current template. May be {@code null}
     */
    public Reference getTemplateRef() {
        return getParticipantRef(DOC_TEMPLATE);
    }

    /**
     * Sets the document.
     *
     * @param document the document. May be {@code null}
     * @throws IllegalStateException if the archetype doesn't support documents
     */
    public void setDocument(Document document) {
        if (docEditor == null) {
            throw new IllegalStateException("Documents are not supported by: " + getDisplayName());
        }
        docEditor.setDocument(document);
    }

    /**
     * Returns the document.
     *
     * @return the document. May be {@code null}
     * @throws IllegalStateException if the archetype doesn't support documents
     */
    public Document getDocument() {
        return (Document) getObject(getDocumentRef());
    }

    /**
     * Returns the document reference.
     *
     * @return the document reference. May be {@code null}
     * @throws IllegalStateException if the archetype doesn't support documents
     */
    public Reference getDocumentRef() {
        if (docEditor == null) {
            throw new IllegalStateException("Documents are not supported by: " + getDisplayName());
        }
        return docEditor.getReference();
    }

    /**
     * Determines if the document needs to be generated.
     *
     * @return {@code true} if the document needs to be generated, otherwise {@code false}
     */
    public DocumentStatus getDocumentStatus() {
        return documentStatus;
    }

    /**
     * Resets the document status.
     * <p/>
     * Use this when the user doesn't want to update the document after attributes have changed.
     */
    public void resetDocumentStatus() {
        documentStatus = DocumentStatus.UP_TO_DATE;
    }

    /**
     * Generates the document.
     *
     * @param listener the listener to notify if the operation was successful or not
     */
    public void generateDocument(Consumer<Boolean> listener) {
        DefaultValidator validator = new DefaultValidator();
        if (!validate(validator)) {
            ValidationHelper.showError(validator);
            listener.accept(false);
        } else {
            generate(listener);
        }
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        DocumentTemplateParticipationEditor editor = getDocumentTemplateEditor();
        if (editor != null) {
            editor.addModifiableListener(modifiable -> onTemplateUpdate());
        }
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        saveObject();
        saveChildren();
    }

    /**
     * Deletes the object.
     *
     * @throws OpenVPMSException if the delete fails
     */
    @Override
    protected void doDelete() {
        DocumentAct act = getObject();
        if (act.getDocument() != null) {
            // need to remove document relationship prior to deletion to avoid foreign key constraints
            act.setDocument(null);
            getService().save(act);
        }
        super.doDelete();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new DocumentActLayoutStrategy(docEditor, versionsEditor);
    }

    /**
     * Returns the document editor.
     *
     * @return the document editor. May be {@code null}
     */
    protected DocumentEditor getDocumentEditor() {
        return docEditor;
    }

    /**
     * Returns the document versions editor.
     *
     * @return the document versions editor. May be {@code null}
     */
    protected ActRelationshipCollectionEditor getVersionsEditor() {
        return versionsEditor;
    }

    /**
     * Returns the document template participation editor.
     *
     * @return document template participation editor. May be {@code null}
     */
    protected DocumentTemplateParticipationEditor getDocumentTemplateEditor() {
        ParticipationEditor<?> editor = getParticipationEditor(DOC_TEMPLATE, true);
        return (editor instanceof DocumentTemplateParticipationEditor) ?
               (DocumentTemplateParticipationEditor) editor : null;
    }

    /**
     * Indicates that a document attribute has been modified.
     *
     * @param mandatory if {@code true}, indicates that document needs to be updated, otherwise prompt to update
     */
    protected void documentAttributeModified(boolean mandatory) {
        if (mandatory || docEditor != null && docEditor.getReference() == null) {
            setDocumentStatus(DocumentStatus.NEEDS_UPDATE);
        } else if (documentStatus != DocumentStatus.NEEDS_UPDATE) {
            setDocumentStatus(DocumentStatus.PROMPT);
        }
    }

    /**
     * Sets the document status.
     *
     * @param documentStatus the document status
     */
    protected void setDocumentStatus(DocumentStatus documentStatus) {
        this.documentStatus = documentStatus;
    }

    /**
     * Invoked when the document reference is updated.
     * </p>
     * Updates the fileName and mimeType properties.
     */
    private void onDocumentUpdate() {
        getProperty("fileName").setValue(docEditor.getName());
        getProperty("mimeType").setValue(docEditor.getMimeType());
    }

    /**
     * Invoked when the document template updates.
     * <p>
     * If the template is different to the prior instance, and there is a document node, indicates that the document
     * needs to be updated.
     */
    private void onTemplateUpdate() {
        Reference template = getTemplateRef();
        if ((template != null && lastTemplate != null && !template.equals(lastTemplate))
            || (template != null && lastTemplate == null)) {
            lastTemplate = template;
            if (docEditor != null) {
                documentAttributeModified(true); // indicate the document needs to be updated
            }
        }
    }

    /**
     * Generates the document.
     * <p>
     * If the act supports versioning, any existing saved document will be copied to new version act.
     */
    private void generate(Consumer<Boolean> listener) {
        DocumentAct act = getObject();
        Context context = getLayoutContext().getContext();
        HelpContext help = getLayoutContext().getHelpContext();
        DocumentGenerator.Listener generationListener = new DocumentGenerator.AbstractListener() {
            public void generated(Document document) {
                docEditor.setDocument(document);
                documentStatus = DocumentStatus.UP_TO_DATE;
                listener.accept(true);
            }
        };
        DocumentGeneratorFactory factory = ServiceHelper.getBean(DocumentGeneratorFactory.class);
        DocumentGenerator generator = factory.create(act, context, help, generationListener);
        try {
            generator.generate();
        } catch (Exception exception) {
            ErrorHelper.show(exception, () -> {
                listener.accept(false);
            });
        }
    }

    /**
     * Versions the existing document if necessary.
     *
     * @param reference the old document reference. May be {@code null}
     * @return {@code true} if it the document was versioned
     */
    private boolean versionOldDocument(Reference reference) {
        boolean versioned = false;
        if (reference != null && !reference.isNew() && versionsEditor != null) {
            DocumentRules rules = ServiceHelper.getBean(DocumentRules.class);
            DocumentAct version = rules.createVersion((DocumentAct) getObject());
            if (version != null) {
                versionsEditor.add(version);
                versionsEditor.refresh();
                versioned = true;
            }
        }
        return versioned;
    }

    private class VersioningDocumentEditor extends DocumentEditor {

        /**
         * Constructs a {@code VersioningDocumentEditor}.
         *
         * @param property the property being edited
         * @param context  the layout context
         */
        public VersioningDocumentEditor(Property property, LayoutContext context) {
            super(property, context);
        }

        /**
         * Sets the document.
         *
         * @param document the new document
         * @throws ArchetypeServiceException for any error
         */
        @Override
        public void setDocument(Document document) {
            boolean versioned = versionOldDocument(getReference());
            super.setDocument(document, versioned);
        }
    }

}
