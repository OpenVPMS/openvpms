/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

/**
 * A strategy for performing an operation on an editor, and handling any errors when the operation fails.
 *
 * @author Tim Anderson
 */
public interface IMObjectEditorOperation<T extends IMObjectEditor> {

    /**
     * Performs the editor operation.
     *
     * @param editor the editor
     */
    void apply(T editor);

    /**
     * Invoked when the operation fails.
     *
     * @param editor    the editor
     * @param exception the reason for the operation failing
     * @param listener  the listener to notify when error handling is complete. May be {@code null}
     */
    void failed(T editor, Throwable exception, Runnable listener);
}