/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.DropdownLookupPropertyEditor;
import org.openvpms.web.component.im.lookup.LookupQuery;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Editor for <em>lookup.suburb</em> properties that supports matching on suburb name or postcode.
 *
 * @author Tim Anderson
 */
public class SuburbLookupPropertyEditor extends DropdownLookupPropertyEditor {

    /**
     * Constructs a {@link SuburbLookupPropertyEditor}.
     *
     * @param property the property being edited
     * @param parent   the parent object
     * @param context  the layout context
     */
    public SuburbLookupPropertyEditor(Property property, IMObject parent, LayoutContext context) {
        super(property, parent, context);
    }

    /**
     * Creates a lookup query.
     *
     * @param property the lookup property
     * @param object   the parent object
     * @return a new lookup query
     */
    @Override
    protected LookupQuery createLookupQuery(Property property, IMObject object) {
        return new SuburbLookupQuery(object, property);
    }

    /**
     * Creates a table model to display lookups.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<Lookup> createTableModel(LayoutContext context) {
        return new DefaultDescriptorTableModel<>("lookup.suburb", context, "name", "postCode");
    }

    /**
     * Determines if a lookup matches the supplied text.
     *
     * @param lookup the lookup
     * @param text   the text
     * @return {@code true} if they match, otherwise {@code false}
     */
    @Override
    protected boolean matches(Lookup lookup, String text) {
        boolean result = super.matches(lookup, text);
        if (!result) {
            IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(lookup);
            String postcode = bean.getString("postCode");
            if (postcode != null) {
                result = StringUtils.contains(postcode, text);
            }
        }
        return result;
    }

    private static class SuburbLookupQuery extends NodeLookupQuery {

        /**
         * Constructs a {@link SuburbLookupQuery for an object and property.
         *
         * @param object   the object
         * @param property the property
         * @param object
         * @param property
         */
        public SuburbLookupQuery(IMObject object, Property property) {
            super(object, property);
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<org.openvpms.component.model.lookup.Lookup> getLookups() {
            List<org.openvpms.component.model.lookup.Lookup> lookups = super.getLookups();
            if (lookups.isEmpty()) {
                // don't constrain lookups
                lookups = new ArrayList<>(ServiceHelper.getLookupService().getLookups("lookup.suburb"));
                sort(lookups);
            }
            return lookups;
        }
    }
}