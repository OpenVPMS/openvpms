/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper to mark an SMS reply as read.
 * <p/>
 * This supports retries if the change cannot be saved.
 *
 * @author Tim Anderson
 */
public class SMSReplyUpdater {

    /**
     * The action factory.
     */
    private final ActionFactory factory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SMSReplyUpdater.class);

    /**
     * Constructs a {@link SMSReplyUpdater}.
     *
     * @param actionFactory the action factory
     * @param service       the archetype service
     */
    public SMSReplyUpdater(ActionFactory actionFactory, ArchetypeService service) {
        this.factory = actionFactory;
        this.service = service;
    }

    /**
     * Mark PENDING replies as READ.
     * <p/>
     * This logs failures, rather than reporting them to the user.
     *
     * @param reply the reply
     */
    public void markRead(Act reply) {
        factory.newAction()
                .backgroundOnly()
                .withObject(reply)
                .useLatestInstanceOnRetry()
                .skipIfMissing()
                .call(object -> {
                    if (MessageStatus.PENDING.equals(object.getStatus())) {
                        object.setStatus(MessageStatus.READ);
                        service.save(object);
                    }
                })
                .onFailure(status -> log.warn("Failed to mark reply read: {}", status.getReason()))
                .run();
    }
}