/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.act.PrintedFlagUpdater;
import org.openvpms.web.component.mail.MailContext;


/**
 * Prints an {@link Act}. On successful printing, updates the {@code printed} flag, if the act has one.
 *
 * @author Tim Anderson
 */
public class PrintActTask extends PrintIMObjectTask {

    /**
     * Constructs a {@link PrintActTask}.
     *
     * @param act       the object to print
     * @param context   the mail context. May be {@code null}
     * @param printMode the print mode
     */
    public PrintActTask(Act act, MailContext context, PrintMode printMode) {
        super(act, context, printMode);
    }

    /**
     * Constructs a {@link PrintActTask}.
     *
     * @param archetype the archetype of the act to print
     * @param context   the mail context. May be {@code null}
     */
    public PrintActTask(String archetype, MailContext context) {
        super(archetype, context);
    }

    /**
     * Invoked when the object is successfully printed.
     * <p/>
     * This updates the printed flag, if the object has one, and notifies completion of the task.
     *
     * @param object  the printed object
     * @param context the task context
     */
    @Override
    protected void onPrinted(IMObject object, TaskContext context) {
        object = setPrinted((Act) object);
        super.onPrinted(object, context);
    }

    /**
     * Sets the printed flag on an act.
     *
     * @param object the act
     * @return the updated instance of the act. This may be different to that supplied, if it needed to be reloaded
     */
    protected Act setPrinted(Act object) {
        PrintedFlagUpdater updater = new PrintedFlagUpdater();
        Act updated = updater.setPrinted(object);
        return updated != null ? updated : object;
    }
}
