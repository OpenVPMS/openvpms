/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.openvpms.component.model.party.Party;

/**
 * An {@link Context} where location and stock location changes are not propagated.
 * <p>
 * This is to prevent changes that an editor makes to the location/stock location from propagating to the global
 * context.
 */
public class ProtectedLocationContext extends DelegatingContext {

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Constructs a {@link ProtectedLocationContext}.
     *
     * @param context the context to delegate to
     */
    public ProtectedLocationContext(Context context) {
        super(context);
        this.location = context.getLocation();
        this.stockLocation = context.getStockLocation();
    }

    /**
     * Sets the current location.
     *
     * @param location the current location
     */
    @Override
    public void setLocation(Party location) {
        this.location = location;
    }

    /**
     * Returns the current location.
     *
     * @return the current location
     */
    @Override
    public Party getLocation() {
        return location;
    }

    /**
     * Sets the current stock location.
     *
     * @param location the current location
     */
    @Override
    public void setStockLocation(Party location) {
        this.stockLocation = location;
    }

    /**
     * Returns the current stock location.
     *
     * @return the current stock location, or {@code null} if there is no current location
     */
    @Override
    public Party getStockLocation() {
        return stockLocation;
    }
}
