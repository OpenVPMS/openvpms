/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.action.FailureReason;


/**
 * Listener for {@link IMObjectDeleter} events.
 *
 * @author Tim Anderson
 */
public interface IMObjectDeletionListener<T extends IMObject> {

    /**
     * Notifies that an object has been deleted.
     *
     * @param object the deleted object
     */
    void deleted(T object);

    /**
     * Notifies that an object has been deactivated.
     *
     * @param object the deactivated object
     */
    void deactivated(T object);

    /**
     * Notifies that an object cannot be deleted, and has already been deactivated.
     *
     * @param object the object
     */
    void alreadyDeactivated(T object);

    /**
     * Notifies that deletion is unsupported.
     *
     * @param object the object that cannot be deleted
     * @param reason the reason
     */
    void unsupported(T object, String reason);

    /**
     * Notifies that an object has failed to be deleted.
     *
     * @param object the object that failed to be deleted
     * @param reason the reason for the failure
     */
    void failed(T object, FailureReason reason);

}
