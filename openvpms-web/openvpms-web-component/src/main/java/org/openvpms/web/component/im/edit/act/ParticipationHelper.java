/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Participation;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Participation helper.
 *
 * @author Tim Anderson
 */
public class ParticipationHelper {

    /**
     * Helper to return a participation.
     *
     * @param property the participation property
     * @return the participation, or {@code null} if one wasn't found or {@code create} was {@code false}
     */
    public static Participation getParticipation(Property property, boolean create) {
        Object value = null;
        if (property.isCollection() && property instanceof CollectionProperty) {
            CollectionProperty collection = (CollectionProperty) property;
            List values = collection.getValues();
            if (!values.isEmpty()) {
                value = values.get(0);
            } else if (create) {
                value = create(collection);
                if (value != null) {
                    collection.add(value);
                }
            }
        } else {
            value = property.getValue();
        }
        return (value instanceof Participation) ? (Participation) value : null;
    }

    /**
     * Creates a participation.
     *
     * @param property the participation collection.
     * @return a new participation or {@code null} if one cannot be created
     */
    public static Participation create(CollectionProperty property) {
        Participation result = null;
        String[] archetypes = DescriptorHelper.getShortNames(property.getDescriptor(),
                                                             ServiceHelper.getArchetypeService());
        if (archetypes.length == 1) {
            Object value = IMObjectCreator.create(archetypes[0]);
            result = (value instanceof Participation) ? (Participation) value : null;
        }
        return result;
    }
}
