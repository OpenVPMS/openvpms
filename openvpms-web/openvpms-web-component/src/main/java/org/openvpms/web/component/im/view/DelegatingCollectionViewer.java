/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.view;

import nextapp.echo2.app.Component;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.property.CollectionProperty;

/**
 * An {@link IMObjectCollectionViewer} that delegates to another.
 *
 * @author Tim Anderson
 */
public class DelegatingCollectionViewer implements IMObjectCollectionViewer {

    /**
     * The viewer to delegate to.
     */
    private final IMObjectCollectionViewer viewer;

    /**
     * Constructs a {@link DelegatingCollectionViewer}.
     *
     * @param viewer the viewer to delegate to
     */
    public DelegatingCollectionViewer(IMObjectCollectionViewer viewer) {
        this.viewer = viewer;
    }

    /**
     * Returns the collection property.
     *
     * @return the collection property
     */
    @Override
    public CollectionProperty getProperty() {
        return viewer.getProperty();
    }

    /**
     * Returns the view component.
     *
     * @return the view component
     */
    @Override
    public Component getComponent() {
        return viewer.getComponent();
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object. May be {@code null}
     */
    @Override
    public IMObject getSelected() {
        return viewer.getSelected();
    }
}
