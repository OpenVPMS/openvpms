/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.product.ProductPriceUpdater;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.math.Weight;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.EditableIMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.MultipleEntityLinkCollectionEditor;
import org.openvpms.web.component.im.relationship.RelationshipCollectionEditor;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.NoOpPropertyTransformer;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.openvpms.web.component.im.product.ProductDoseEditor.MAX_WEIGHT;
import static org.openvpms.web.component.im.product.ProductDoseEditor.MIN_WEIGHT;
import static org.openvpms.web.component.im.product.ProductDoseEditor.WEIGHT_UNITS;


/**
 * A {@link Product} editor that recalculates prices when {@link ProductSupplier} relationships change.
 *
 * @author Tim Anderson
 */
public class ProductEditor extends AbstractIMObjectEditor {

    /**
     * The product price updater.
     */
    private final ProductPriceUpdater updater;

    /**
     * Cache of supplier cost prices where autoPriceUpdate is true, keyed on product-supplier reference.
     * This is used to detect when unit prices should be recalculated.
     */
    private final Map<Reference, BigDecimal> supplierCostPrices = new HashMap<>();

    /**
     * Concentration node.
     */
    private static final String CONCENTRATION = "concentration";

    /**
     * Prices node.
     */
    private static final String PRICES = "prices";

    /**
     * Doses node.
     */
    private static final String DOSES = "doses";

    /**
     * Stock locations node.
     */
    private static final String STOCK_LOCATIONS = "stockLocations";

    /**
     * Locations nodes.
     */
    private static final String LOCATIONS = "locations";

    /**
     * Suppliers node.
     */
    private static final String SUPPLIERS = "suppliers";

    /**
     * Preferred supplier node.
     */
    private static final String PREFERRED = "preferred";

    /**
     * Constructs a {@link ProductEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context. May be {@code null}.
     */
    public ProductEditor(Product object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        CollectionProperty suppliers = getCollectionProperty(SUPPLIERS);
        CollectionProperty stockLocations = getCollectionProperty(STOCK_LOCATIONS);
        CollectionProperty locations = getCollectionProperty(LOCATIONS);
        if (suppliers != null && stockLocations != null) {
            initCostPrices(suppliers);
            initStockLocations(object, stockLocations);
        }
        if (locations != null) {
            addEditor(new ProductLocationCollectionEditor(locations, object, getLayoutContext()));
        }
        updater = new ProductPriceUpdater(ServiceHelper.getBean(ProductPriceRules.class),
                                          ServiceHelper.getBean(PracticeRules.class),
                                          getService());
        Property instructions = getProperty("dispInstructions");
        if (instructions != null) {
            // disable macro expansion so that macros can be expanded when the product is dispensed
            instructions.setTransformer(NoOpPropertyTransformer.INSTANCE);
        }
    }

    /**
     * Returns the current prices, ordered on from date.
     * <p/>
     * Where multiple prices have the same from-date, they will be ordered on id.
     *
     * @return the current prices
     */
    public List<ProductPrice> getPrices() {
        List<ProductPrice> prices = new ArrayList<>();
        for (IMObject object : getPricesEditor().getCurrentObjects()) {
            prices.add((ProductPrice) object);
        }
        prices.sort((o1, o2) -> {
            int compare = DateRules.compareTo(o1.getFromDate(), o2.getFromDate());
            if (compare == 0) {
                compare = Long.compare(o1.getId(), o2.getId());
            }
            return compare;
        });
        return prices;
    }

    /**
     * Returns the prices editor.
     *
     * @return the prices, or {@code null} if the product doesn't support prices, or the component has not been created
     */
    public ProductPriceCollectionEditor getPricesEditor() {
        return (ProductPriceCollectionEditor) getEditor(PRICES, false);
    }

    /**
     * Returns the suppliers editor.
     *
     * @return the prices, or {@code null} if the product doesn't support prices, or the component has not been created
     */
    public EditableIMObjectCollectionEditor getSuppliersEditor() {
        return (EditableIMObjectCollectionEditor) getEditor(SUPPLIERS, false);
    }

    /**
     * Returns the product supplier references.
     *
     * @return the product supplier references
     */
    public List<Reference> getSuppliers() {
        List<Reference> result = new ArrayList<>();
        EditableIMObjectCollectionEditor suppliers = getSuppliersEditor();
        if (suppliers != null) {
            for (IMObject object : suppliers.getCurrentObjects()) {
                Relationship relationship = (Relationship) object;
                Reference target = relationship.getTarget();
                if (target != null) {
                    result.add(target);
                }
            }
        }
        return result;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean valid = super.doValidation(validator);
        if (valid) {
            valid = validateDoses(validator) && validateSuppliers(validator);
        }
        return valid;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        RelationshipCollectionEditor stockLocations = (RelationshipCollectionEditor) getEditor(STOCK_LOCATIONS, false);
        if (stockLocations != null) {
            strategy.addComponent(new ComponentState(stockLocations));
        }
        ProductLocationCollectionEditor locations = (ProductLocationCollectionEditor) getEditor(LOCATIONS, false);
        if (locations != null) {
            strategy.addComponent(new ComponentState(locations));
        }
        return strategy;
    }

    /**
     * Invoked when layout has completed. This can be used to perform
     * processing that requires all editors to be created.
     */
    @Override
    protected void onLayoutCompleted() {
        EditableIMObjectCollectionEditor editor = getSuppliersEditor();
        if (editor != null) {
            editor.addModifiableListener(modifiable -> onSupplierChanged());
        }
    }

    /**
     * Initialises the cache of cost prices, in order to detect changes requiring unit prices to recalculate.
     *
     * @param suppliers the product-supplier relationships
     */
    private void initCostPrices(CollectionProperty suppliers) {
        for (Object object : suppliers.getValues()) {
            Relationship relationship = (Relationship) object;
            ProductSupplier ps = new ProductSupplier(relationship, getService());
            if (ps.isAutoPriceUpdate()) {
                supplierCostPrices.put(relationship.getObjectReference(), ps.getCostPrice());
            }
        }
    }

    /**
     * Initialises the product - stock locations relationship editors.
     *
     * @param object the product
     * @param stockLocations the stock locations property
     */
    private void initStockLocations(Product object, CollectionProperty stockLocations) {
        RelationshipCollectionEditor relationshipCollectionEditor
                = new MultipleEntityLinkCollectionEditor(stockLocations, object, getLayoutContext()) {
            @Override
            protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
                IMObjectEditor editor = super.createEditor(object, context);
                if (editor instanceof ProductStockLocationEditor) {
                    ((ProductStockLocationEditor) editor).setProductEditor(ProductEditor.this);
                }
                return editor;
            }
        };
        // create the editors so their validators will be invoked even if the user doesn't directly edit them.
        // This is because they may include a preferred supplier which must be present in the product-supplier
        // relationships
        for (IMObject relationship : relationshipCollectionEditor.getCurrentObjects()) {
            relationshipCollectionEditor.getEditor(relationship);
        }
        addEditor(relationshipCollectionEditor);
    }


    /**
     * Invoked when a product-supplier relationship changes. This recalculates product prices if required.
     */
    private void onSupplierChanged() {
        updateUnitPrices();
        updatePreferred();
        RelationshipCollectionEditor stockLocations = (RelationshipCollectionEditor) getEditor(STOCK_LOCATIONS, false);
        if (stockLocations != null) {
            // force the stock location relationships to revalidate, as they may have a supplier which is no longer
            // valid
            stockLocations.resetValid();
        }
    }

    /**
     * Updates unit prices if the current product-supplier relationship editor has autoPriceUpdate set {@code true}
     * and the cost price changes.
     */
    private void updateUnitPrices() {
        EditableIMObjectCollectionEditor suppliers = getSuppliersEditor();
        EditableIMObjectCollectionEditor prices = getPricesEditor();
        if (suppliers != null && prices != null) {
            boolean updated = false;
            for (IMObjectEditor editor : suppliers.getEditors()) {
                Relationship rel = (Relationship) editor.getObject();
                ProductSupplier supplier = new ProductSupplier(rel, getService());
                if (supplier.isAutoPriceUpdate()) {
                    updated |= updatePrices(prices, supplier);
                } else {
                    supplierCostPrices.remove(rel.getObjectReference());
                }
            }
            if (updated) {
                prices.refresh();
            }
        }
    }

    /**
     * Updates unit prices if the cost price has changed.
     *
     * @param prices          the prices
     * @param productSupplier the product-supplier relationship
     * @return {@code true} if prices where updated, otherwise {@code false}
     */
    private boolean updatePrices(EditableIMObjectCollectionEditor prices, ProductSupplier productSupplier) {
        boolean result = false;
        Reference reference = productSupplier.getRelationship().getObjectReference();
        BigDecimal existingCost = supplierCostPrices.get(reference);
        BigDecimal costPrice = productSupplier.getCostPrice();
        if (existingCost == null || !MathRules.equals(existingCost, costPrice)) {
            IMObjectEditor current = prices.getCurrentEditor();
            if (current != null && !prices.getCollection().getValues().contains(current.getObject())) {
                prices.add(current.getObject());
            }
            Collection<IMObjectEditor> currentPrices = prices.getEditors();
            List<ProductPrice> updated = updater.update((Product) getObject(), productSupplier, false);
            if (!updated.isEmpty()) {
                for (ProductPrice price : updated) {
                    if (price.isNew()) {
                        // unsaved price
                        String name = getName(productSupplier.getSupplierRef());
                        IMObjectBean bean = getBean(price);
                        bean.setValue("notes", Messages.format("product.price.autoupdate", name));
                    }
                    if (price.getProduct() == null) {
                        // not yet added to the product
                        prices.add(price);
                    } else {
                        updatePriceEditor(price, currentPrices);
                    }
                }
                supplierCostPrices.put(reference, costPrice);
                result = true;
            }
        }
        return result;
    }

    /**
     * Refreshes the price editor associated with a product price.
     *
     * @param price   the price
     * @param editors the price editors
     */
    private void updatePriceEditor(ProductPrice price, Collection<IMObjectEditor> editors) {
        for (IMObjectEditor editor : editors) {
            if (editor.getObject().equals(price) && (editor instanceof ProductPriceEditor)) {
                ((ProductPriceEditor) editor).refresh();
                break;
            }
        }
    }

    /**
     * Verifies that if there are doses, a concentration is required and that doses don't overlap on weight range.
     *
     * @param validator the validator
     * @return {@code true} if the doses are valid, or the product has no doses
     */
    private boolean validateDoses(Validator validator) {
        boolean result = true;
        EditableIMObjectCollectionEditor editor = (EditableIMObjectCollectionEditor) getEditor(DOSES);
        if (editor != null) {
            result = validateConcentration(editor, validator) && validateDoses(editor, validator);
        }
        return result;
    }

    /**
     * Verifies a concentration has been specified if there are doses.
     *
     * @param doses     the doses editor
     * @param validator the validator
     * @return {@code true} if a concentration has been specified no concentration is required, otherwise {@code false}
     */
    private boolean validateConcentration(EditableIMObjectCollectionEditor doses, Validator validator) {
        boolean result = true;
        if (!doses.getCurrentObjects().isEmpty()) {
            Property property = getProperty(CONCENTRATION);
            BigDecimal concentration = property.getBigDecimal(BigDecimal.ZERO);
            if (MathRules.isZero(concentration)) {
                result = false;
                validator.add(this, new ValidatorError(Messages.format("product.concentration.required")));
            }
        }
        return result;
    }

    /**
     * Verifies that doses don't overlap on weight range.
     *
     * @param editor    the dose editor
     * @param validator the validator
     * @return {@code true} if the doses are valid, otherwise {@code false}
     */
    private boolean validateDoses(EditableIMObjectCollectionEditor editor, Validator validator) {
        Lookup all = new org.openvpms.component.business.domain.im.lookup.Lookup();
        // dummy to indicate the dose applies to all species.
        Map<Lookup, List<IMObject>> dosesBySpecies = new LinkedHashMap<>();
        for (IMObject dose : editor.getCurrentObjects()) {
            IMObjectBean bean = getBean(dose);
            List<Lookup> values = bean.getValues("species", Lookup.class);
            Lookup species = !values.isEmpty() ? values.get(0) : all;
            List<IMObject> doses = dosesBySpecies.computeIfAbsent(species, k -> new ArrayList<>());
            doses.add(dose);
        }
        for (Map.Entry<Lookup, List<IMObject>> entry : dosesBySpecies.entrySet()) {
            List<IMObject> doses = new ArrayList<>(entry.getValue());
            while (doses.size() > 1) {
                IMObjectBean dose = getBean(doses.remove(0));
                BigDecimal minWeight = dose.getBigDecimal(MIN_WEIGHT, BigDecimal.ZERO);
                BigDecimal maxWeight = dose.getBigDecimal(MAX_WEIGHT, BigDecimal.ZERO);
                WeightUnits units = WeightUnits.fromString(dose.getString(WEIGHT_UNITS));
                for (IMObject otherDose : doses) {
                    IMObjectBean other = getBean(otherDose);
                    BigDecimal otherMinWeight = other.getBigDecimal(MIN_WEIGHT, BigDecimal.ZERO);
                    BigDecimal otherMaxWeight = other.getBigDecimal(MAX_WEIGHT, BigDecimal.ZERO);
                    WeightUnits otherUnits = WeightUnits.fromString(other.getString(WEIGHT_UNITS));
                    if (units != null && otherUnits != null && !units.equals(otherUnits)) {
                        otherMinWeight = Weight.convert(otherMinWeight, otherUnits, units);
                        otherMaxWeight = Weight.convert(otherMaxWeight, otherUnits, units);
                    }
                    if (MathRules.intersects(minWeight, maxWeight, otherMinWeight, otherMaxWeight)) {
                        validator.add(this, new ValidatorError(Messages.format("product.dose.weightOverlap",
                                                                               dose.getObject().getName(),
                                                                               otherDose.getName())));
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Invoked when a product-supplier relationship changes to ensure that only one is preferred.
     */
    private void updatePreferred() {
        EditableIMObjectCollectionEditor suppliers = getSuppliersEditor();
        if (suppliers != null) {
            IMObjectEditor currentEditor = suppliers.getCurrentEditor();
            if (currentEditor != null && currentEditor.getProperty(PREFERRED).getBoolean()) {
                // current relationship is preferred. Ensure other suppliers are not preferred
                for (IMObject object : suppliers.getCurrentObjects()) {
                    if (!object.equals(currentEditor.getObject())) {
                        IMObjectEditor editor = suppliers.getEditor(object);
                        if (editor != null) {
                            Property preferred = editor.getProperty(PREFERRED);
                            if (preferred.getBoolean()) {
                                preferred.setValue(false);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Ensures that a product has only one preferred supplier.
     *
     * @param validator the validator
     * @return {@code true} if there is at most one preferred supplier, otherwise {@code false}
     */
    private boolean validateSuppliers(Validator validator) {
        boolean valid = true;
        EditableIMObjectCollectionEditor editor = getSuppliersEditor();
        if (editor != null) {
            Reference preferred = null;
            for (IMObject object : editor.getCurrentObjects()) {
                Relationship relationship = (Relationship) object;
                IMObjectBean bean = getBean(relationship);
                if (bean.getBoolean(PREFERRED)) {
                    if (preferred != null) {
                        IMObject first = getObject(preferred);
                        IMObject second = getObject(relationship.getTarget());
                        String firstName = (first != null) ? first.getName() : null;
                        String secondName = (second != null) ? second.getName() : null;
                        // shouldn't fail to retrieve suppliers...
                        String message = Messages.format("product.supplier.multiplepreferred", firstName, secondName);
                        validator.add(editor.getProperty(), new ValidatorError(editor.getProperty(), message));
                        valid = false;
                        break;
                    } else {
                        preferred = relationship.getTarget();
                    }
                }
            }
        }
        return valid;
    }

}
