/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.IMReport;
import org.openvpms.report.ReportException;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.im.doc.FileNameFormatter;


/**
 * Generates {@link Document}s from a {@link DocumentAct}.
 *
 * @author Tim Anderson
 */
public class DocumentActReporter extends TemplatedReporter<IMObject> {

    /**
     * The report factory.
     */
    private final ReportFactory reportFactory;

    /**
     * The report.
     */
    private IMReport<IMObject> report;

    /**
     * Document template node name.
     */
    private static final String DOCUMENT_TEMPLATE = "documentTemplate";

    /**
     * Constructs a {@link DocumentActReporter}.
     *
     * @param act           the document act
     * @param formatter     the file name formatter
     * @param service       the archetype service
     * @param lookups       the lookup service
     * @param reportFactory the report factory
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DocumentException         if the template cannot be found
     */
    public DocumentActReporter(DocumentAct act, FileNameFormatter formatter, ArchetypeService service,
                               LookupService lookups, ReportFactory reportFactory) {
        this(act, getTemplate(act, true, service), formatter, service, lookups, reportFactory);
    }

    /**
     * Constructs a {@link DocumentActReporter}.
     *
     * @param act           the document act
     * @param template      the document template
     * @param formatter     the file name formatter
     * @param service       the archetype service
     * @param lookups       the lookup service
     * @param reportFactory the report factory
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentActReporter(DocumentAct act, DocumentTemplate template, FileNameFormatter formatter,
                               ArchetypeService service, LookupService lookups, ReportFactory reportFactory) {
        super(act, template, formatter, service, lookups);
        this.reportFactory = reportFactory;
    }

    /**
     * Constructs a {@link DocumentActReporter}.
     *
     * @param act           the document act
     * @param locator       the template locator, if a template isn't associated with the act
     * @param formatter     the file name formatter
     * @param service       the archetype service
     * @param lookups       the lookup service
     * @param reportFactory the report factory
     */
    public DocumentActReporter(DocumentAct act, DocumentTemplateLocator locator, FileNameFormatter formatter,
                               ArchetypeService service, LookupService lookups, ReportFactory reportFactory) {
        super(act, locator, formatter, service, lookups);
        this.reportFactory = reportFactory;
        setTemplate(getTemplate(act, false, service));
    }

    /**
     * Returns the report.
     *
     * @return the report
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DocumentException         if the template cannot be found
     */
    @Override
    public IMReport<IMObject> getReport() {
        if (report == null) {
            report = createReport();
        }
        return report;
    }

    /**
     * Returns the template associated with a document act.
     * <p>
     * Document acts must have a template to be able to be used in reporting.
     *
     * @param act     the document act
     * @param service the archetype service
     * @return the template, or {@code null} if the act has no template
     */
    public static DocumentTemplate getTemplate(DocumentAct act, ArchetypeService service) {
        DocumentTemplate result = null;
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode(DOCUMENT_TEMPLATE)) {
            Entity template = bean.getTarget(DOCUMENT_TEMPLATE, Entity.class);
            if (template != null) {
                result = new DocumentTemplate(template, service);
            }
        }
        return result;
    }

    /**
     * Creates a report.
     *
     * @return a new report
     * @throws ReportException if no template exists
     */
    protected IMReport<IMObject> createReport() {
        DocumentTemplate template = resolveTemplate();
        return reportFactory.isIMObjectReport(template)
               ? reportFactory.createIMObjectReport(template)
               : reportFactory.createStaticContentReport(template);
    }

    /**
     * Returns the report factory.
     *
     * @return the report factory
     */
    protected ReportFactory getReportFactory() {
        return reportFactory;
    }

    /**
     * Helper to return the <em>entity.documentTemplate</em> associated with a document act.
     *
     * @param act      the document act
     * @param required if {@code true} the template is required, {@code false} if it is optional
     * @param service  the archetype service
     * @return the associated template, or {@code null} if it is not found
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DocumentException         if the template cannot be found, and the template is required
     */
    private static DocumentTemplate getTemplate(DocumentAct act, boolean required, ArchetypeService service) {
        DocumentActTemplateLocator locator = new DocumentActTemplateLocator(act, service);
        DocumentTemplate template = locator.getTemplate();
        if (template == null && required) {
            throw new DocumentException(DocumentException.ErrorCode.DocumentHasNoTemplate,
                                        DescriptorHelper.getDisplayName(act, service));
        }
        return template;
    }

}
