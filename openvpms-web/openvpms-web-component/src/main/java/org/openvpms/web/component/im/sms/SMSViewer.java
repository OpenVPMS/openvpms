/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableCellRenderer;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.act.ActHelper;
import org.openvpms.web.component.im.table.AbstractIMObjectTableModel;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.TableEx;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SMS viewer.
 * <p/>
 * This displays a list of SMS and their replies, in order of when the SMS was last updated, lowest timestamp first.
 * <p/>
 * The displayed date is the time when the message is sent or received. Messages may appear chronologically out of order
 * if a reply to an older message was received more recently, however the reply will appear in context.
 *
 * @author Tim Anderson
 */
public class SMSViewer {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The SMS reply updater.
     */
    private final SMSReplyUpdater replyUpdater;

    /**
     * The messages to display.
     */
    private final List<Act> messages = new ArrayList<>();

    /**
     * The component.
     */
    private final Component component;

    /**
     * Constructs a {@link SMSViewer}.
     *
     * @param sms          the SMS to display
     * @param service      the archetype service
     * @param replyUpdater the SMS reply updater
     */
    public SMSViewer(List<Act> sms, ArchetypeService service, SMSReplyUpdater replyUpdater) {
        this.service = service;
        this.replyUpdater = replyUpdater;
        for (Act act : ActHelper.sort(sms)) {
            messages.add(act);
            IMObjectBean bean = service.getBean(act);
            List<Act> replies = ActHelper.sort(bean.getTargets("replies", Act.class));
            messages.addAll(replies);
        }
        IMObjectTableModel<Act> model = new SMSTableModel();
        model.setObjects(messages);
        TableEx table = new TableEx();
        table.setModel(model);
        table.setStyleName("SMSViewer.table");
        table.setColumnModel(model.getColumnModel());
        table.setHeaderVisible(false);
        if (!messages.isEmpty()) {
            // scroll to the most recent message
            table.setScrollToRow(messages.size() - 1);
        }
        component = ColumnFactory.create(Styles.LARGE_INSET, table);
    }

    /**
     * Returns the viewer.
     *
     * @return the viewer
     */
    public Component getComponent() {
        return component;
    }

    /**
     * Marks all replies as being read.
     */
    public void markAsRead() {
        messages.stream().filter(act -> act.isA(SMSArchetypes.REPLY)).forEach(this::markAsRead);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Marks a reply as read.
     *
     * @param act the reply act
     */
    private void markAsRead(Act act) {
        replyUpdater.markRead(act);
    }

    private class SMSTableModel extends AbstractIMObjectTableModel<Act> {

        public SMSTableModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(new TableColumn(0, null, new DateRenderer(), null));
            model.addColumn(new TableColumn(1, null, new MessageRenderer(), null));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return null;
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Act object, TableColumn column, int row) {
            return object;
        }
    }

    /**
     * Renders the SMS text.
     */
    private class MessageRenderer implements TableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
            Act object = (Act) value;
            String message = service.getBean(object).getString("message");
            Label label = LabelFactory.create(true, true);
            label.setText(message);
            String style = "SMSViewer.message";
            if (object.isA(SMSArchetypes.REPLY)) {
                label.setTextAlignment(Alignment.ALIGN_LEFT);
                label.setLayoutData(TableHelper.alignRight());
                style = "SMSViewer.reply";
            }
            label.setStyleName(style);
            return label;
        }
    }

    /**
     * Renders the SMS date.
     */
    private static class DateRenderer implements TableCellRenderer {

        /**
         * Renders the component.
         *
         * @param table  the table
         * @param value  the value to render
         * @param column the column
         * @param row    the row
         * @return the component
         */
        @Override
        public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
            Label result;
            Act object = (Act) value;
            Date date = getDate(object);
            if (date != null) {
                result = LabelFactory.text(formatDate(date));
                result.setLayoutData(TableHelper.alignTop());
            } else {
                result = LabelFactory.create();
            }
            return result;
        }

        /**
         * Returns the date to render.
         * <p/>
         * For outgoing messages, this is {@link Act#getActivityEndTime() endTime}, which reflects the date when the
         * message was sent, or {@link Act#getActivityStartTime() startTime} if the message is yet to be sent.<br/>
         * For incoming messages, this is {@link Act#getActivityStartTime() startTime} which reflects when the message
         * was received.<br/>
         * For outgoing messages {@link Act#getActivityStartTime() startTime} holds when the message was last updated,
         * to enable better db indexes to be used to locate recently updated messages.
         *
         * @param object the SMS/SMS reply
         * @return the date to render
         */
        private Date getDate(Act object) {
            Date date = null;
            if (object.isA(SMSArchetypes.MESSAGE)) {
                date = object.getActivityEndTime();
            }
            if (date == null) {
                date = object.getActivityStartTime();
            }
            return date;
        }

        /**
         * Formats a date/time.
         * <p/>
         * If the date is:
         * <ul>
         *     <li>today, {@code "<time>"} is returned</li>
         *     <li>yesterday, {@code "Yesterday  <time>"} is returned</li>
         *     <li>in the last 6 days, {@code "<day name> <time>"} is returned</li>
         *     <li>otherwise {@code "<full date time>"} is returned</li>
         * </ul>
         *
         * @param date the date/time to format
         * @return the formatted date/time
         */
        private String formatDate(Date date) {
            String text = "";
            if (!DateRules.isToday(date)) {
                if (DateRules.isYesterday(date)) {
                    text = Messages.get("date.format.yesterday");
                } else {
                    Date today = DateRules.getToday();
                    if (DateRules.between(date, DateRules.getDate(today, -6, DateUnits.DAYS), today)) {
                        text = DateFormatter.getDayFormat().format(date);
                    } else {
                        text = DateFormatter.getFullDateFormat().format(date);
                    }
                }
                text += " ";
            }
            text += DateFormatter.getFullTimeFormat().format(date);
            return text;
        }
    }
}