/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;


/**
 * An {@link IMObjectLayoutStrategy} for <em>contact.emailAddress</em> that enables mail messages to be sent.
 *
 * @author Tim Anderson
 */
public class EmailContactViewLayout extends AbstractContactViewLayout {

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        ComponentState emailAddress = createEmail(properties.get("emailAddress"), (Contact) object, context);
        if (emailAddress != null) {
            addComponent(emailAddress);
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Creates a component for the email address.
     *
     * @param property the emailAddress property
     * @param contact  the email contact
     * @param context  the layout context
     * @return the email component, or {@code null} if not all of the details are present
     */
    private ComponentState createEmail(Property property, Contact contact, LayoutContext context) {
        ComponentState result = null;
        MailContext mailContext = context.getMailContext();
        if (mailContext != null) {
            EmailLauncher launcher = EmailLauncher.create(contact, getService(), context.getContext(),
                                                          context.getHelpContext(), mailContext);
            if (launcher != null) {
                Button writeButton = launcher.getWriteButton();
                Row row = RowFactory.create(Styles.WIDE_CELL_SPACING, writeButton);
                Component mailTo = launcher.getMailToLink();
                if (mailTo != null) {
                    row.add(mailTo);
                }
                result = new ComponentState(row, property);
            }
        }
        return result;
    }
}