/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.DocumentConverter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentGeneratorFactory;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Factory for {@link AttachmentGenerator} instances.
 *
 * @author Tim Anderson
 */
public class AttachmentGeneratorFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document generator factory.
     */
    private final DocumentGeneratorFactory documentGeneratorFactory;

    /**
     * The printer factory.
     */
    private final IMPrinterFactory printerFactory;

    /**
     * The document job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * The converter.
     */
    private final DocumentConverter converter;

    /**
     * Constructs an {@link AttachmentGeneratorFactory}.
     *
     * @param service                  the archetype service
     * @param documentGeneratorFactory the document generator factory
     * @param printerFactory           the printer factory
     * @param jobManager               the job manager
     * @param converter                the converter
     */
    public AttachmentGeneratorFactory(ArchetypeService service, DocumentGeneratorFactory documentGeneratorFactory,
                                      IMPrinterFactory printerFactory, DocumentJobManager jobManager,
                                      DocumentConverter converter) {
        this.service = service;
        this.documentGeneratorFactory = documentGeneratorFactory;
        this.printerFactory = printerFactory;
        this.jobManager = jobManager;
        this.converter = converter;
    }

    /**
     * Creates a new {@link AttachmentGenerator}.
     *
     * @param context the context
     * @param help    the help context
     * @return a new attachment generator
     */
    public AttachmentGenerator create(Context context, HelpContext help) {
        return new AttachmentGenerator(service, documentGeneratorFactory, printerFactory, converter, jobManager,
                                       context, help);
    }
}
