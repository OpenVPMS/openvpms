/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.payment;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.account.AccountActEditor;
import org.openvpms.web.component.im.act.ActHelper;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;

import java.math.BigDecimal;


/**
 * An editor for {@link Act}s which have an archetype of
 * <em>act.customerAccountPayment</em>, <em>act.customerAccountRefund</em>,
 * <em>act.supplierAccountPayment</em> or <em>act.supplierAccountRefund</em>.
 *
 * @author Tim Anderson
 */
public abstract class PaymentEditor extends AccountActEditor {

    /**
     * Constructs a {@link PaymentEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public PaymentEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
    }

    /**
     * Adds a new payment item, returning its editor.
     *
     * @return the payment item editor, or {@code null} if an item couldn't be created
     */
    public PaymentItemEditor addItem() {
        return addItem(null);
    }

    /**
     * Adds a new payment item, returning its editor.
     *
     * @param archetype the payment item archetype, or {@code null} to use the default
     * @return the payment item editor, or {@code null} if an item couldn't be created
     */
    public PaymentItemEditor addItem(String archetype) {
        ActRelationshipCollectionEditor items = getItems();
        PaymentItemEditor result = (PaymentItemEditor) ((archetype != null) ? items.add(archetype) : items.add());
        if (result != null && items.getCurrentEditor() == result) {
            // set the default focus to that of the item editor
            getFocusGroup().setDefault(result.getFocusGroup().getDefaultFocus());
        }
        return result;
    }

    /**
     * Returns the current payment item editor.
     *
     * @return the current payment item, or {@code null} if there is none
     */
    public PaymentItemEditor getCurrentItem() {
        return (PaymentItemEditor) getItems().getCurrentEditor();
    }

    /**
     * Returns the editor for an unsaved item.
     * <p/>
     * If the current item is unsaved, the editor for it will be returned, else a new item will be added.
     *
     * @return the editor for an unsaved item
     */
    public PaymentItemEditor getUnsavedItem() {
        return getUnsavedItem(null);
    }

    /**
     * Returns the editor for an unsaved item.
     * <p/>
     * If the current item is unsaved, the editor for it will be returned, else a new item will be added.
     *
     * @param archetype the type of the item to return, or {@code null} to use the default archetype
     * @return the editor for an unsaved item
     */
    public PaymentItemEditor getUnsavedItem(String archetype) {
        PaymentItemEditor editor = getCurrentItem();
        IMObject object = (editor != null) ? editor.getObject() : null;
        if (object == null || !object.isNew()) {
            editor = addItem(archetype);
        } else if (object.isNew() && archetype != null && !object.isA(archetype)) {
            // replace the existing unsaved object with one of the specified archetype
            getItems().remove(object);
            editor = addItem(archetype);
        }
        return editor;
    }

    /**
     * Update totals when an act item changes.
     */
    protected void onItemsChanged() {
        Property amount = getProperty("amount");
        BigDecimal value = ActHelper.sum(getObject(), getItems().getCurrentActs(), "amount");
        amount.setValue(value);
    }

}
