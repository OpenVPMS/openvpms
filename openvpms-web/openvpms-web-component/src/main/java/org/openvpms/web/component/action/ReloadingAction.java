/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * An action that can reload objects before retrying.
 * <p/>
 * The action may be synchronous or asynchronous. Synchronous actions are invoked within a transaction.
 * Asynchronous actions are responsible for their own transaction management.
 *
 * @author Tim Anderson
 */
public abstract class ReloadingAction extends AsynchronousAction {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Determines the retry behaviour of the action.
     */
    private final Behaviour behaviour;

    /**
     * Determines if the action runs asynchronously or synchronously.
     */
    private final boolean async;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The object suppliers.
     */
    private final List<Supplier<? extends IMObject>> suppliers = new ArrayList<>();

    /**
     * The current instances of the objects, keyed on reference.
     */
    private final Map<Reference, IMObject> objects = new HashMap<>();

    /**
     * The current instances, keyed on the suppliers that returned them.
     */
    private final Map<ObjectSupplier<?>, IMObject> objectsBySupplier = new HashMap<>();

    /**
     * Constructs a {@link ReloadingAction}.
     *
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public ReloadingAction(Behaviour behaviour, boolean async, ArchetypeService service,
                           PlatformTransactionManager transactionManager) {
        this.behaviour = behaviour;
        this.async = async;
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Adds an object.
     *
     * @param object the object to add
     */
    protected void addObject(IMObject object) {
        objects.put(object.getObjectReference(), object);
        addObject(ObjectSupplier.create(object.getArchetype(), () -> object));
    }

    /**
     * Adds an object supplier.
     * <p/>
     * This will be used prior to running the action, and after a failure.
     *
     * @param supplier the supplier to add
     */
    protected <T extends IMObject> void addObject(ObjectSupplier<T> supplier) {
        ObjectSupplier<T> wrapper = new ReloadingObjectSupplier<>(supplier.getArchetype(), service, supplier,
                                                                  behaviour.useLatestInstance());
        suppliers.add(new Supplier<T>(supplier, wrapper));
    }

    /**
     * Returns the current cached instance of an object.
     *
     * @param object the object
     * @return the cached instance, or {@code null} if it was not previously added
     */
    @SuppressWarnings("unchecked")
    protected <T extends IMObject> T getCurrentObject(T object) {
        return (T) objects.get(object.getObjectReference());
    }

    /**
     * Returns the current cached instance of an object, given its supplier.
     *
     * @param supplier the object supplier
     * @return the cached instance, or {@code null} if it was not previously added
     */
    @SuppressWarnings("unchecked")
    protected <T extends IMObject> T getCurrentObject(ObjectSupplier<T> supplier) {
        return (T) objectsBySupplier.get(supplier);
    }

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    @Override
    protected final void runAction(Consumer<ActionStatus> listener) {
        if (async) {
            runAsynchronousAction(listener);
        } else {
            runSynchronousAction(listener);
        }
    }

    /**
     * Executes the action.
     * <p/>
     * For synchronous actions, this is run within a transaction.
     *
     * @param listener the listener
     */
    protected abstract void execute(Consumer<ActionStatus> listener);

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Runs the action synchronously.
     * <p/>
     * This loads the object(s) and executes the action within a transaction.
     * <p/>
     * Notification of the listener is deferred till after the transaction completes.
     *
     * @param listener the listener to notify when the action completes
     */
    private void runSynchronousAction(Consumer<ActionStatus> listener) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        MutableObject<ActionStatus> deferredStatus = new MutableObject<>();
        template.execute(txnStatus -> {
            ActionStatus result = load();
            if (result.succeeded()) {
                execute(deferredStatus::setValue);
            } else {
                deferredStatus.setValue(result);
            }
            return null;
        });
        if (deferredStatus.getValue() != null) {
            listener.accept(deferredStatus.getValue());
        } else {
            throw new IllegalStateException("Synchronous listener did not notify completion status");
        }
    }

    /**
     * Runs the action synchronously.
     *
     * @param listener the listener to notify when the action completes
     */
    private void runAsynchronousAction(Consumer<ActionStatus> listener) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        ActionStatus result = template.execute(txnStatus -> load());
        if (result == null) {
            // should never occur
            throw new IllegalStateException("Load failed");
        } else if (result.succeeded()) {
            execute(listener);
        } else {
            listener.accept(result);
        }
    }

    /**
     * Loads each object.
     * <p/>
     * Any supplier that is an {@link ReloadingObjectSupplier} will return a cached instance on the first access,
     * and reload it on subsequent access.
     *
     * @return {@link ActionStatus#success()} if all objects were loaded successfully according to the
     * {@link #behaviour}; {@link ActionStatus#skipped()} if an object couldn't be loaded or was changed, and the
     * {@link #behaviour} indicates the action can be skipped; otherwise {@link ActionStatus#failed()}
     */
    private ActionStatus load() {
        ActionStatus status = null;
        for (Supplier<? extends IMObject> supplier : suppliers) {
            status = load(supplier);
            if (!status.succeeded()) {
                break;
            }
        }
        return (status != null) ? status : ActionStatus.success();
    }

    /**
     * Loads an object.
     *
     * @param supplier the object supplier
     * @return {@link ActionStatus#success()} if the object was loaded successfully according to the
     * {@link #behaviour}; {@link ActionStatus#skipped()} if an object couldn't be loaded or was changed, and the
     * {@link #behaviour} indicates the action can be skipped; otherwise {@link ActionStatus#failed()}
     */
    private ActionStatus load(Supplier<? extends IMObject> supplier) {
        ActionStatus result = null;
        Throwable cause = null;
        IMObject current = null;
        try {
            current = supplier.get();
        } catch (Throwable exception) {
            cause = exception;
        }
        ObjectSupplier<? extends IMObject> key = supplier.user;
        IMObject existing = getCurrentObject(key);
        if (current != null) {
            if (existing != null && existing.getVersion() != current.getVersion()) {
                FailureReason reason = FailureReason.objectChanged(existing.getObjectReference(),
                                                                   format("imobject.changed", existing.getArchetype()));
                if (behaviour.skipIfChanged()) {
                    result = ActionStatus.skipped(reason);
                } else if (behaviour.failIfChanged()) {
                    result = ActionStatus.failed(reason);
                }
            }
            if (result == null) {
                if (existing == null || !behaviour.useOriginalObject()) {
                    objects.put(current.getObjectReference(), current);
                    objectsBySupplier.put(key, current);
                }
                result = ActionStatus.success();
            }
        } else {
            if (cause != null) {
                result = ActionStatus.failed(cause);
            } else {
                String message = format("imobject.noexist", supplier.getArchetype());
                FailureReason reason;
                if (existing != null) {
                    reason = FailureReason.objectNotFound(existing.getObjectReference(), message);
                } else {
                    reason = FailureReason.error(message);
                }
                result = ActionStatus.failed(reason);
            }
        }
        return result;
    }

    /**
     * Formats a message with an archetype display name.
     *
     * @param key       the resource bundle key
     * @param archetype the archetype to get the display name of
     * @return the formatted message
     */
    private String format(String key, String archetype) {
        return Messages.format(key, DescriptorHelper.getDisplayName(archetype, service));
    }

    private static class Supplier<T extends IMObject> {
        private final ObjectSupplier<T> user;

        private final ObjectSupplier<T> delegate;

        public Supplier(ObjectSupplier<T> user, ObjectSupplier<T> delegate) {
            this.user = user;
            this.delegate = delegate;
        }

        public T get() {
            return delegate.get();
        }

        public String getArchetype() {
            return delegate.getArchetype();
        }
    }
}
