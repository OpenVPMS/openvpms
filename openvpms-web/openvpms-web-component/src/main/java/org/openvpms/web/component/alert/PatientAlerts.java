/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.alert;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.prefs.UserPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Patient alerts.
 *
 * @author Tim Anderson
 */
class PatientAlerts extends Alerts {

    /**
     * Constructs a {@link PatientAlerts}.
     *
     * @param preferences the preferences
     * @param service     the archetype service
     */
    public PatientAlerts(UserPreferences preferences, ArchetypeService service) {
        super(preferences, "patientAlerts", service);
    }

    /**
     * Returns alerts for a party.
     *
     * @param patient the party
     * @return the alerts for a party
     */
    @Override
    public List<Alert> getAlerts(Party patient) {
        List<Alert> result = new ArrayList<>();
        AlertQueryBuilder builder = new PatientAlertQueryBuilder(patient);
        ArchetypeService service = getService();

        for (Act act : builder.createQuery().getResultList()) {
            Entity alertType = service.getBean(act).getTarget("alertType", Entity.class);
            if (alertType != null) {
                result.add(new Alert(alertType, act));
            }
        }
        if (result.size() > 1) {
            Collections.sort(result);
        }
        return result;
    }

    protected class PatientAlertQueryBuilder extends AlertQueryBuilder {

        /**
         * Constructs a {@link PatientAlertQueryBuilder}.
         *
         * @param party the patient to query alerts for
         */
        public PatientAlertQueryBuilder(Party party) {
            super(party, PatientArchetypes.ALERT, "patient");
        }

        /**
         * Builds the query.
         * <p/>
         * This implementation finds alerts that are IN_PROGRESS and active now, where the alert type is active.
         *
         * @param from    the act root to query from
         * @param builder the criteria builder
         */
        @Override
        protected void build(Root<Act> from, CriteriaBuilder builder) {
            super.build(from, builder);
            Join<IMObject, IMObject> join = from.join("alertType").join("entity");
            join.on(builder.equal(join.get("active"), true));
        }
    }

}
