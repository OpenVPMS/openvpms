/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus;
import org.openvpms.web.component.im.edit.AbstractEditDialog;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;

import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.NEEDS_UPDATE;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.PROMPT;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.UP_TO_DATE;

/**
 * An edit dialog for {@link DocumentActEditor}s that support document generation on save.
 *
 * @author Tim Anderson
 */
public class DocumentActEditDialog extends EditDialog {

    /**
     * Constructs an {@link AbstractEditDialog}.
     *
     * @param editor  the editor
     * @param context the context
     */
    public DocumentActEditDialog(DocumentActEditor editor, Context context) {
        super(editor, context);
    }

    /**
     * Returns the editor.
     *
     * @return the editor, or {@code null} if none has been set
     */
    @Override
    public DocumentActEditor getEditor() {
        return (DocumentActEditor) super.getEditor();
    }

    /**
     * Saves the current object, if saving is enabled.
     */
    @Override
    protected void onApply() {
        if (documentOutOfDate()) {
            generateDocument(() -> DocumentActEditDialog.super.onApply());
        } else {
            super.onApply();
        }
    }

    /**
     * Saves the current object, if saving is enabled, and closes the editor.
     */
    @Override
    protected void onOK() {
        if (documentOutOfDate()) {
            generateDocument(() -> DocumentActEditDialog.super.onOK());
        } else {
            super.onOK();
        }
    }

    /**
     * Generates the document, if required.
     * <p/>
     * If the {@link DocumentStatus} is:
     * <ul>
     *     <li>NEEDS_UPDATE - this forces generation of the document</li>
     *     <li>PROMPT - the user will be prompted to update the document</li>
     * </ul>
     *
     * @param listener the listener to notify on successful completion
     */
    private void generateDocument(Runnable listener) {
        DocumentActEditor editor = getEditor();
        if (editor.getDocumentStatus() == NEEDS_UPDATE) {
            generateDocument(editor, listener);
        } else if (editor.getDocumentStatus() == PROMPT) {
            ConfirmationDialog.newDialog()
                    .titleKey("document.update.title")
                    .messageKey("document.update.message")
                    .yesNoCancel()
                    .yes(() -> {
                        generateDocument(editor, listener);
                    })
                    .no(() -> {
                        editor.resetDocumentStatus();
                        listener.run();
                    })
                    .show();
        } else {
            listener.run();
        }
    }

    /**
     * Generates the document.
     *
     * @param editor   the editor
     * @param listener the listener to notify on successful generation
     */
    private void generateDocument(DocumentActEditor editor, Runnable listener) {
        editor.generateDocument(success -> {
            if (success != null && success) {
                listener.run();
            }
        });
    }

    /**
     * Determines if the document needs is out-of-date and can be regenerated.
     *
     * @return {@code true} if the document is out of date, otherwise {@code false}
     */
    private boolean documentOutOfDate() {
        DocumentActEditor editor = getEditor();
        return canSave() && editor != null && editor.getDocumentStatus() != UP_TO_DATE;
    }
}
