/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import echopointng.DateField;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import org.openvpms.web.component.bound.BoundCheckBox;
import org.openvpms.web.component.bound.BoundDateFieldFactory;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.bound.BoundTextField;
import org.openvpms.web.component.im.view.Hint;
import org.openvpms.web.echo.factory.ComponentFactory;
import org.openvpms.web.echo.text.PasswordField;
import org.openvpms.web.echo.text.TextArea;
import org.openvpms.web.echo.text.TextComponent;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.echo.util.StyleSheetHelper;


/**
 * Abstract implementation of the {@link PropertyComponentFactory} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPropertyComponentFactory implements PropertyComponentFactory {

    /**
     * The style name to use.
     */
    private final String style;

    /**
     * The no. of columns to display for numeric fields.
     */
    private final int numericLength;


    /**
     * Constructs an {@link AbstractPropertyComponentFactory}.
     *
     * @param style the style name to use
     */
    public AbstractPropertyComponentFactory(String style) {
        this.style = style;
        numericLength = StyleSheetHelper.getNumericLength();
    }

    /**
     * Creates components for boolean, string, numeric and date properties.
     *
     * @param property the property
     * @return a new component, or {@code null} if the property isn't supported
     */
    public Component create(Property property) {
        return create(property, null);
    }

    /**
     * Creates a new component bound to the specified property.
     *
     * @param property the property to bind
     * @param hint     a rendering hint to determine the layout of the component. May be {@code null}
     * @return a new component, or {@code null} if the property type isn't supported
     */
    @Override
    public Component create(Property property, Hint hint) {
        Component result = null;
        if (property.isBoolean()) {
            result = createBoolean(property);
        } else if (property.isString()) {
            if (property.isPassword()) {
                result = createPassword(property);
            } else {
                result = createString(property, hint);
            }
        } else if (property.isNumeric()) {
            result = createNumeric(property);
        } else if (property.isDate()) {
            result = createDate(property);
        }
        return result;
    }

    /**
     * Returns a component bound to a boolean property.
     *
     * @param property the property to bind
     * @return a new component
     */
    protected Component createBoolean(Property property) {
        BoundCheckBox result = new BoundCheckBox(property);
        ComponentFactory.setStyle(result, getStyle());
        return result;
    }

    /**
     * Returns a component bound to a string property.
     * This implementation returns a text field to display the node, or a text
     * area if it is large.
     *
     * @param property the property to bind
     * @param hint     a rendering hint to determine the layout of the component. May be {@code null}
     * @return a new component
     */
    protected Component createString(Property property, Hint hint) {
        int maxDisplayLength = 50;
        int length = property.getMaxLength();
        int maxColumns = Math.min(length, maxDisplayLength);
        Component result;
        if (hint == null || (hint.useDefaultWidth() && hint.useDefaultHeight())) {
            result = createString(property, maxColumns);
        } else {
            Extent width = hint.getWidth();
            Extent height = hint.getHeight();

            if (hint.useDefaultHeight() || hint.singleRow()) {
                // single line text field
                if (width == null) {
                    width = new Extent(maxDisplayLength, Extent.EX);
                }
                TextComponent component = new BoundTextField(property);
                component.setWidth(width);
                result = component;
            } else {
                TextArea textArea = BoundTextComponentFactory.createTextArea(property);
                textArea.setWidth(width);
                textArea.setHeight(height);
                result = textArea;
            }
            ComponentFactory.setStyle(result, getStyle());
        }
        return result;
    }

    /**
     * Returns a component bound to a string property.
     * This implementation returns a text field to display the node, or a text
     * area if it is large.
     *
     * @param property the property to bind
     * @param columns  the maximum no, of columns to display
     * @return a new component
     */
    protected Component createString(Property property, int columns) {
        TextComponent result;
        if (property.getMaxLength() > 255) {
            if (property.getMaxLength() < 500) {
                result = BoundTextComponentFactory.createTextArea(property, columns, 5);
            } else {
                result = BoundTextComponentFactory.createTextArea(property, 80, 15);
            }
        } else {
            result = BoundTextComponentFactory.create(property, columns);
        }
        ComponentFactory.setStyle(result, getStyle());
        return result;
    }

    /**
     * Returns a component bound to a numeric property.
     *
     * @param property the property to bind
     * @return a new component
     */
    protected Component createNumeric(Property property) {
        TextField result = BoundTextComponentFactory.createNumeric(property, numericLength);
        ComponentFactory.setStyle(result, getStyle());
        return result;
    }

    /**
     * Returns a component bound to a date property.
     *
     * @param property the property to bind
     * @return a new component
     */
    protected Component createDate(Property property) {
        DateField result = BoundDateFieldFactory.create(property);
        ComponentFactory.setStyle(result, getStyle());
        ComponentFactory.setStyle(result.getTextField(), getStyle());
        return result;
    }

    /**
     * Returns a component bound to a password property.
     *
     * @param property the property to bind
     * @return a new component
     */
    protected Component createPassword(Property property) {
        PasswordField result = BoundTextComponentFactory.createPassword(property);
        ComponentFactory.setStyle(result, getStyle());
        return result;
    }

    /**
     * Returns the component style name.
     *
     * @return the component style name
     */
    protected String getStyle() {
        return style;
    }
}
