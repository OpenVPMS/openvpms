/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * An action that passes an {@link IMObject} to a {@link Consumer}.
 */
class IMObjectConsumerAction<T extends IMObject> extends ConsumerAction<T, T, BiConsumer<T, Consumer<ActionStatus>>> {

    /**
     * Constructs an {@link IMObjectConsumerAction}.
     *
     * @param object             the object
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public IMObjectConsumerAction(T object, Behaviour behaviour, boolean async,
                                  BiConsumer<T, Consumer<ActionStatus>> consumer, ArchetypeService service,
                                  PlatformTransactionManager transactionManager) {
        super(object, behaviour, async, consumer, service, transactionManager);
    }

    /**
     * Constructs an {@link IMObjectConsumerAction}.
     *
     * @param supplier           the object supplier
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public IMObjectConsumerAction(ObjectSupplier<T> supplier, Behaviour behaviour, boolean async,
                                  BiConsumer<T, Consumer<ActionStatus>> consumer, ArchetypeService service,
                                  PlatformTransactionManager transactionManager) {
        super(supplier, behaviour, async, consumer, service, transactionManager);
    }

    /**
     * Returns the current cached version of an object.
     *
     * @param object the original object
     * @return the cached version of the object
     */
    @Override
    protected T getCurrent(T object) {
        return getCurrentObject(object);
    }

    /**
     * Returns the current cached version of an object.
     *
     * @param supplier the object supplier
     * @return the cached version of the object
     */
    @Override
    protected T getCurrent(ObjectSupplier<T> supplier) {
        return getCurrentObject(supplier);
    }
}
