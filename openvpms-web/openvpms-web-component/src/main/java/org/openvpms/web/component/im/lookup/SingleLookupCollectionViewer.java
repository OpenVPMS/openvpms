/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.IMObjectCollectionViewer;
import org.openvpms.web.component.im.view.SingleIMObjectCollectionViewer;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.IMObjectProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.ReadOnlyProperty;
import org.openvpms.web.echo.factory.LabelFactory;

/**
 * {@link IMObjectCollectionViewer} for a {@link Lookup} collection with 0..1 elements.
 * <p/>
 * This renders the name node.
 *
 * @author Tim Anderson
 */
public class SingleLookupCollectionViewer extends SingleIMObjectCollectionViewer {

    /**
     * Constructs a {@link SingleLookupCollectionViewer}.
     *
     * @param property the collection property
     * @param parent   the parent object
     * @param context  the layout context
     */
    public SingleLookupCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext context) {
        super(property, parent, context);
    }

    /**
     * Creates a component for the object.
     *
     * @param object  the object
     * @param parent  the parent
     * @param context the layout context
     * @return the component
     */
    @Override
    protected Component createComponent(IMObject object, IMObject parent, LayoutContext context) {
        return createComponent(object, "name");
    }
}