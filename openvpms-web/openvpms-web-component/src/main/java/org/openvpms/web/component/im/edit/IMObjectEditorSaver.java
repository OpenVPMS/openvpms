/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Helper to manage saving editors within transactions.
 *
 * @author Tim Anderson
 */
public class IMObjectEditorSaver {

    /**
     * An operation to save an {@link IMObjectEditor}.
     *
     * @author Tim Anderson
     * @see IMObjectEditorSaver
     */
    public static class SaveOperation extends AbstractIMObjectEditorOperation<IMObjectEditor> {

        /**
         * Constructs an {@link IMObjectEditorOperation}.
         */
        public SaveOperation() {
            super(new SaveFailureFormatter());
        }

        /**
         * Perform the operation.
         *
         * @param editor the editor
         */
        @Override
        public void apply(IMObjectEditor editor) {
            editor.save();
        }
    }

    /**
     * The save strategy.
     */
    private final IMObjectEditorOperation<IMObjectEditor> strategy;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectEditorSaver.class);

    /**
     * Constructs an {@link IMObjectEditorSaver}.
     */
    public IMObjectEditorSaver() {
        this(new SaveOperation());
    }

    /**
     * Constructs an {@link IMObjectEditorSaver}.
     *
     * @param strategy the save strategy
     */
    public IMObjectEditorSaver(IMObjectEditorOperation<IMObjectEditor> strategy) {
        this.strategy = strategy;
    }

    /**
     * Saves an editor.
     *
     * @param editor the editor to save
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor) {
        return save(editor, false);
    }

    /**
     * Saves an editor.
     *
     * @param editor          the editor to save
     * @param suppressDialogs if {@code true}, suppress any error dialogs
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor, boolean suppressDialogs) {
        return save(editor, suppressDialogs, null);
    }

    /**
     * Saves an editor.
     *
     * @param editor   the editor to save
     * @param listener the listener to notify when the error dialog closes, if the save was unsuccessful
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor, Runnable listener) {
        return save(editor, false, listener);
    }

    /**
     * Saves an editor.
     * <p/>
     * If the editor fails to save, and error dialogs are not being suppressed,
     * {@link IMObjectEditorOperation#failed(IMObjectEditor, Throwable, Runnable)} will be invoked to handle the
     * failure.
     *
     * @param editor          the editor to save
     * @param suppressDialogs if {@code true}, suppress any error dialogs
     * @param listener        the listener to notify when the error dialog closes, if the save was unsuccessful
     * @return {@code true} if the editor was successfully saved
     */
    protected boolean save(IMObjectEditor editor, boolean suppressDialogs, Runnable listener) {
        boolean result;
        TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
        try {
            // perform validation and save in the one transaction
            Boolean saved = template.execute(transactionStatus -> validateAndSave(editor, listener, suppressDialogs));
            result = saved != null && saved;
            if (result) {
                editor.committed();
            }
        } catch (Throwable exception) {
            result = false;
            log(editor, exception);
            if (!suppressDialogs) {
                strategy.failed(editor, exception, listener);
            }
        }
        return result;
    }

    /**
     * Creates a validator to validate an editor.
     *
     * @return a new validator
     */
    protected Validator createValidator() {
        return new DefaultValidator();
    }

    /**
     * Displays validation errors.
     *
     * @param validator the validator
     * @param listener  the listener to notify when the error dialog closes. May be {@code null}
     */
    protected void showError(Validator validator, Runnable listener) {
        ValidationHelper.showError(null, validator, listener);
    }

    /**
     * Saves the editor if it is valid. This is designed to be called from within a transaction.
     *
     * @param editor          the editor
     * @param listener        the listener to notify on completion, if an error dialog was displayed
     * @param suppressDialogs if {@code true}, suppress any error dialogs
     * @return {@code true} if the editor was saved, otherwise {@code false}
     */
    private boolean validateAndSave(IMObjectEditor editor, Runnable listener, boolean suppressDialogs) {
        Validator validator = createValidator();
        boolean result = editor.validate(validator);
        if (result) {
            strategy.apply(editor);
        } else if (!suppressDialogs) {
            showError(validator, listener);
        }
        return result;
    }

    /**
     * Logs a save error.
     *
     * @param editor    the editor
     * @param exception the exception
     */
    private void log(IMObjectEditor editor, Throwable exception) {
        String context = Messages.format("logging.error.editcontext", editor.getObject().getObjectReference(),
                                         editor.getClass().getName());
        String message = ErrorFormatter.format(exception, editor.getDisplayName());
        log.error(Messages.format("logging.error.messageandcontext", message, context), exception);
    }
}
