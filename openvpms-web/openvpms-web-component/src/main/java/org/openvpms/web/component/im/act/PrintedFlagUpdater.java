/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.act;

import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Helper to set the printed flag on an act.
 * <p/>
 * This supports retries if the changes cannot be saved.
 *
 * @author Tim Anderson
 */
public class PrintedFlagUpdater {

    /**
     * The final statuses of financial acts that allow the printed flag to be set.
     */
    private final List<String> finalStatuses;

    /**
     * The action factory.
     */
    private final ActionFactory factory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Printed node name.
     */
    private static final String PRINTED = "printed";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PrintedFlagUpdater.class);

    /**
     * Constructs a {@link PrintedFlagUpdater}.
     */
    public PrintedFlagUpdater() {
        this(ActStatus.POSTED, ActStatus.CANCELLED);
    }

    /**
     * Constructs a {@link PrintedFlagUpdater}.
     *
     * @param finalStatuses statuses of financial acts that determine if the printed flag may be updated
     */
    public PrintedFlagUpdater(String... finalStatuses) {
        this.finalStatuses = Arrays.asList(finalStatuses);
        factory = ServiceHelper.getBean(ActionFactory.class);
        service = ServiceHelper.getArchetypeService(false);  // suppress execution of business rules
    }

    /**
     * Sets an act's printed flag.
     * <p/>
     * This suppresses execution of business rules to allow the printed flag to be set on financial acts that have been
     * {@code POSTED}.
     * <p/>
     * This supports retries if the act cannot be saved. If the flag cannot be set after N retries, it is ignored;
     * failing to set the printed flag shouldn't terminate any workflows.
     * <p/>
     * If the act is updated, a different instance may be returned, as it may have needed to be reloaded.
     *
     * @param act the act to update
     * @return the saved act, or {@code null} if it wasn't updated
     */
    public <T extends Act> T setPrinted(T act) {
        MutableObject<T> result = new MutableObject<>();
        factory.newAction()
                .backgroundOnly()
                .withObject(act)
                .useLatestInstanceOnRetry()
                .skipIfMissing()
                .call(obj -> {
                    if (canUpdate(obj) && update(obj)) {
                        service.save(obj);
                        result.setValue(obj);
                    }
                })
                .onFailure(status -> log.warn("Failed to set printed flag: {}", status.getReason()))
                .run();
        return result.getValue();
    }

    /**
     * Determines if the printed flag can be updated.
     * <p/>
     * This implementation returns {@code true} if:
     * <ul>
     *     <li>it is a financial act or estimate, and the act has a final status; or</li>
     *     <li>it is any other act</li>
     * </ul>
     * For financial acts and estimates, it can only be updated if the act is in a status that cannot be changed.<br/>
     * This is so users are prompted to print the acts when they can no longer be modified.
     *
     * @param act the act
     * @return {@code true} if the printed flag can be updated, otherwise {@code false}
     */
    protected boolean canUpdate(Act act) {
        return !isFinancial(act) || isFinalStatus(act);
    }

    /**
     * Determines if the act is in a status that cannot be changed.
     *
     * @param act the act
     * @return {@code true} if the act is in a status that cannot be changed otherwise {@code false}
     */
    protected boolean isFinalStatus(Act act) {
        String status = act.getStatus();
        return status != null && finalStatuses.contains(status);
    }

    /**
     * Updates an act's printed flag.
     *
     * @param act the act to update
     * @return {@code true} if the flag was changed, otherwise {@code false}
     */
    protected boolean update(Act act) {
        boolean result = false;
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode(PRINTED) && !bean.getBoolean(PRINTED)) {
            bean.setValue(PRINTED, true);
            result = true;
        }
        return result;
    }

    /**
     * Determines if the act is financial.
     * </p>
     * This includes any {@link FinancialAct}, or <em>act.customerEstimation</em>, which is modelled using {@link Act}
     * for historical reasons.
     *
     * @param act the act
     * @return {@code true} if the act is financial, otherwise {@code false}
     */
    private boolean isFinancial(Act act) {
        return act instanceof FinancialAct || act.isA(EstimateArchetypes.ESTIMATE);
    }
}