/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.act.Act;

/**
 * Task to update an {@link Act} held in the {@link TaskContext}.
 * <p/>
 * This supports:
 * <li>
 * <li>skipping the update, if the act has a particular status</li>
 * <li>reloading the act if the update fails</li>
 * </li>
 *
 * @author Tim Anderson
 */
public class UpdateActTask<T extends Act> extends AbstractUpdateActTask<T> {

    /**
     * Constructs an {@link UpdateActStatusTask}
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with
     */
    public UpdateActTask(String archetype, TaskProperties properties) {
        super(archetype, properties);
    }
}