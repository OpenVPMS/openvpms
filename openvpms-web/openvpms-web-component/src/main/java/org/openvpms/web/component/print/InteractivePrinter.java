/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.print.impl.exception.PrintNotSupported;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.event.VetoListener;
import org.openvpms.web.echo.event.Vetoable;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.servlet.DownloadServlet;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * {@link Printer} implementation that provides interactive support if
 * the underlying implementation requires it. Pops up a dialog with options to
 * print, preview, or cancel.
 *
 * @author Tim Anderson
 */
public class InteractivePrinter implements Printer {

    /**
     * The printer to delegate to.
     */
    private final Printer printer;

    /**
     * The dialog title.
     */
    private final String title;

    /**
     * If {@code true} display a 'skip' button that simply closes the dialog.
     */
    private final boolean skip;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The job manager, used to run jobs and enable them to be cancelled.
     */
    private final DocumentJobManager manager;

    /**
     * The print listener. May be {@code null}.
     */
    private PrinterListener listener;

    /**
     * The cancel listener. May be {@code null}.
     */
    private VetoListener<Vetoable> cancelListener;

    /**
     * Determines if printing should occur interactively or be performed
     * without user intervention. If the printer doesn't support non-interactive
     * printing, requests to do non-interactive printing are ignored.
     */
    private boolean interactive;

    /**
     * The mail context. If non-null, documents may be mailed.
     */
    private MailContext mailContext;

    /**
     * The print dialog, or {@code null} if none is being displayed.
     */
    private PrintDialog dialog;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InteractivePrinter.class);


    /**
     * Constructs an {@link InteractivePrinter}.
     *
     * @param printer the printer to delegate to
     * @param context the context
     * @param help    the help context
     */
    public InteractivePrinter(Printer printer, Context context, HelpContext help) {
        this(printer, false, context, help);
    }

    /**
     * Constructs an {@link InteractivePrinter}.
     *
     * @param printer the printer to delegate to
     * @param skip    if {@code true} display a 'skip' button that simply closes the dialog
     * @param context the context
     * @param help    the help context
     */
    public InteractivePrinter(Printer printer, boolean skip, Context context, HelpContext help) {
        this(null, printer, skip, context, help);
    }

    /**
     * Constructs an {@link InteractivePrinter}.
     *
     * @param title   the dialog title. May be {@code null}
     * @param printer the printer to delegate to
     * @param context the context
     * @param help    the help context
     */
    public InteractivePrinter(String title, Printer printer, Context context, HelpContext help) {
        this(title, printer, false, context, help);
    }

    /**
     * Constructs an {@link InteractivePrinter}.
     *
     * @param title   the dialog title. May be {@code null}
     * @param printer the printer to delegate to
     * @param skip    if {@code true} display a 'skip' button that simply closes the dialog
     * @param context the context
     * @param help    the help context
     */
    public InteractivePrinter(String title, Printer printer, boolean skip, Context context, HelpContext help) {
        this.title = title;
        this.printer = printer;
        this.skip = skip;
        this.context = context;
        this.help = help;
        interactive = printer.getInteractive();
        manager = ServiceHelper.getBean(DocumentJobManager.class);
    }

    /**
     * Prints the object to the default printer.
     *
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print() {
        DocumentPrinter printer = getDefaultPrinter();
        print(printer);
    }

    /**
     * Prints the object.
     *
     * @param printer the printer. May be {@code null}
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print(DocumentPrinter printer) {
        if (interactive || printer == null) {
            printInteractive(printer);
        } else {
            doPrint(printer);
        }
    }

    /**
     * Initiates printing of an object.
     * <p/>
     * If the underlying printer requires interactive support or no printer is specified, pops up a {@link PrintDialog}
     * prompting if printing of an object should proceed, invoking {@link #doPrint} if 'OK' is selected, or
     * {@link #preview()} if 'preview' is selected.
     *
     * @param reference the printer name. May be {@code null}
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print(PrinterReference reference) {
        DocumentPrinter printer = null;
        if (reference != null) {
            printer = getPrinterLocator().getPrinter(reference.getArchetype(), reference.getId());
            if (printer == null) {
                log.warn("Printer not found: " + reference);
            }
        }
        if (interactive || printer == null) {
            printInteractive(printer);
        } else {
            doPrint(printer);
        }
    }

    /**
     * Returns the default printer for the object.
     *
     * @return the default printer for the object, or {@code null} if none
     * is defined
     * @throws OpenVPMSException for any error
     */
    @Override
    public DocumentPrinter getDefaultPrinter() {
        return printer.getDefaultPrinter();
    }

    /**
     * Returns a document for the object, corresponding to that which would be printed.
     *
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public Document getDocument() {
        return getDocument(DocFormats.PDF_TYPE, false);
    }

    /**
     * Returns a document for the object, corresponding to that which would be printed.
     *
     * @param mimeType the mime type. If {@code null} the default mime type associated with the report will be used.
     * @param email    if {@code true} indicates that the document will be emailed. Documents generated from templates
     *                 can perform custom formatting
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public Document getDocument(String mimeType, boolean email) {
        return printer.getDocument(mimeType, email);
    }

    /**
     * Determines if printing should occur interactively or be performed
     * without user intervention.
     *
     * @param interactive if {@code true} print interactively
     * @throws OpenVPMSException for any error
     */
    public void setInteractive(boolean interactive) {
        this.interactive = interactive;
    }

    /**
     * Determines if printing should occur interactively.
     *
     * @return {@code true} if printing should occur interactively,
     * {@code false} if it can be performed non-interactively
     * @throws OpenVPMSException for any error
     */
    @Override
    public boolean getInteractive() {
        return interactive;
    }

    /**
     * Sets the number of copies to print.
     *
     * @param copies the no. of copies to print
     */
    @Override
    public void setCopies(int copies) {
        printer.setCopies(copies);
    }

    /**
     * Returns the number of copies to print.
     *
     * @return the no. of copies to print
     */
    @Override
    public int getCopies() {
        return printer.getCopies();
    }

    /**
     * Returns a display name for the objects being printed.
     *
     * @return a display name for the objects being printed
     */
    @Override
    public String getDisplayName() {
        return printer.getDisplayName();
    }

    /**
     * Returns the printer locator.
     *
     * @return the printer locator
     */
    @Override
    public DocumentPrinterServiceLocator getPrinterLocator() {
        return printer.getPrinterLocator();
    }

    /**
     * Sets the listener for print events.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setListener(PrinterListener listener) {
        this.listener = listener;
    }

    /**
     * Sets a listener to veto cancel events.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setCancelListener(VetoListener<Vetoable> listener) {
        cancelListener = listener;
    }

    /**
     * Sets a context to support mailing documents.
     *
     * @param context the mail context. If {@code null}, mailing won't be enabled
     */
    public void setMailContext(MailContext context) {
        this.mailContext = context;
    }

    /**
     * Returns the mail context.
     *
     * @return the mail context
     */
    public MailContext getMailContext() {
        return mailContext;
    }

    /**
     * Returns the print dialog.
     *
     * @return the print dialog, if one is being displayed, otherwise {@code null}
     */
    public PrintDialog getPrintDialog() {
        return dialog;
    }

    /**
     * Returns the underlying printer.
     *
     * @return the printer
     */
    protected Printer getPrinter() {
        return printer;
    }

    /**
     * Returns a title for the print dialog.
     *
     * @return a title for the print dialog
     */
    protected String getTitle() {
        return title;
    }

    /**
     * Returns the context.
     *
     * @return the context
     */
    protected Context getContext() {
        return context;
    }

    /**
     * Returns the help context.
     *
     * @return the help context
     */
    protected HelpContext getHelpContext() {
        return help;
    }

    /**
     * Creates a new print dialog.
     *
     * @return a new print dialog
     */
    protected PrintDialog createDialog() {
        String title = getTitle();
        if (title == null) {
            title = Messages.get("printdialog.title");
        }
        boolean mail = mailContext != null;
        return new PrintDialog(title, true, mail, skip, context.getLocation(), help) {
            @Override
            protected void onPreview() {
                preview();
            }

            @Override
            protected void onMail() {
                mail(this);
            }
        };
    }

    /**
     * Prints interactively.
     *
     * @param printer the default printer to print to
     * @throws OpenVPMSException for any error
     */
    protected void printInteractive(DocumentPrinter printer) {
        dialog = createDialog();
        if (printer == null) {
            printer = getDefaultPrinter();
        }
        dialog.setDefaultPrinter(printer);
        dialog.setCopies(this.printer.getCopies());
        dialog.setCancelListener(cancelListener);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            public void onClose(WindowPaneEvent event) {
                try {
                    handleAction(dialog);
                } finally {
                    dialog = null;
                }
            }
        });
        dialog.show();
    }

    /**
     * Performs the print.
     *
     * @param printer the printer to use
     * @throws OpenVPMSException for any error
     */
    protected void doPrint(DocumentPrinter printer) {
        Job<?> job = newJob()
                .run(() -> this.printer.print(printer))
                .completed(() -> printed(printer))
                .cancelled(this::cancelled)
                .failed(this::printFailed)
                .build();
        manager.runInteractive(job, Messages.get("document.print.title"), Messages.get("document.print.cancel"));
    }

    /**
     * Handles a print dialog action.
     *
     * @param dialog the dialog
     */
    protected void handleAction(PrintDialog dialog) {
        String action = dialog.getAction();
        if (PrintDialog.OK_ID.equals(action)) {
            PrinterReference reference = dialog.getPrinter();
            DocumentPrinter selected = getPrinter(reference);
            if (selected == null) {
                download(); // no printer so can't print. Download it instead.
            } else {
                printer.setCopies(dialog.getCopies());
                doPrint(selected);
            }
        } else if (PrintDialog.SKIP_ID.equals(action)) {
            skipped();
        } else if (MailDialog.SEND_ID.equals(action)) {
            mailed();
        } else {
            cancelled();
        }
    }

    protected void preview() {
        manager.preview(this, getContext().getUser());
    }

    /**
     * Generates a document and pops up a mail document with it as an attachment.
     * <p>
     * If emailed, then the print dialog is closed.
     *
     * @param parent the parent print dialog
     */
    protected void mail(PrintDialog parent) {
        Job<?> job = JobBuilder.<Document>newJob(getDisplayName(), getContext().getUser())
                .get(() -> getDocument(DocFormats.PDF_TYPE, true))
                .completed(document -> mail(document, parent))
                .failed(this::failed)
                .build();
        manager.runInteractive(job, Messages.get("document.mail.title"), Messages.get("document.mail.cancel"));
    }

    /**
     * Invoked when the object has been successfully printed.
     * Notifies any registered listener.
     *
     * @param printer the printer that was used to print the object. May be {@code null}
     */
    protected void printed(DocumentPrinter printer) {
        if (listener != null) {
            listener.printed(printer);
        }
    }

    /**
     * Invoked when printing fails.
     * <p/>
     * This gives the user the opportunity to download the document, if printing it is not supported.
     *
     * @param exception the cause
     */
    protected void printFailed(Throwable exception) {
        if (exception instanceof PrintNotSupported) {
            ConfirmationDialog.newDialog()
                    .title(Messages.get("document.print.title"))
                    .message(Messages.get("document.print.unsupported"))
                    .yesNo()
                    .yes(this::download)
                    .no(this::cancelled)
                    .show();
        } else {
            failed(exception);
        }
    }

    /**
     * Invoked when the print is cancelled.
     * Notifies any registered listener.
     */
    protected void cancelled() {
        if (listener != null) {
            listener.cancelled();
        }
    }

    /**
     * Invoked when the print is skipped.
     * Notifies any registered listener.
     */
    protected void skipped() {
        if (listener != null) {
            listener.skipped();
        }
    }

    /**
     * Invoked when the print job is mailed instead.
     * Notifies any registered listener that the printing has been skipped.
     */
    protected void mailed() {
        if (listener != null) {
            listener.skipped();
        }
    }

    /**
     * Invoked when the object has been failed to print.
     * Notifies any registered listener.
     *
     * @param exception the cause of the failure
     */
    protected void failed(Throwable exception) {
        if (listener != null) {
            listener.failed(exception);
        } else {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Mails a document as an attachment.
     * <p>
     * If emailed, then the print dialog is closed.
     *
     * @param document the document to mail
     * @param parent   the parent print dialog
     */
    protected void mail(Document document, PrintDialog parent) {
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, help.subtopic("email"));
        MailDialog dialog = createMailDialog(document, mailContext, layoutContext);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                if (MailDialog.SEND_ID.equals(dialog.getAction())) {
                    // close the parent dialog. This will notify registered listeners of the action taken,
                    // so need to propagate the action to the parent.
                    parent.close(MailDialog.SEND_ID);
                }
            }
        });
        show(dialog);
    }

    /**
     * Creates a dialog to email a document as an attachment.
     * <p>
     * If emailed, then the print dialog is closed.
     *
     * @param document      the document to mail
     * @param mailContext   the mail context
     * @param layoutContext the layout context
     */
    protected MailDialog createMailDialog(Document document, MailContext mailContext, LayoutContext layoutContext) {
        MailDialog dialog = ServiceHelper.getBean(MailDialogFactory.class).create(mailContext, layoutContext);
        MailEditor editor = dialog.getMailEditor();
        editor.setSubject(getDisplayName());
        editor.addAttachment(document);
        return dialog;
    }

    /**
     * Shows the dialog.
     *
     * @param dialog the dialog
     */
    protected void show(MailDialog dialog) {
        dialog.show();
    }

    /**
     * Returns the job manager.
     *
     * @return the job manager
     */
    protected DocumentJobManager getJobManager() {
        return manager;
    }

    /**
     * Invoked when there is no printer to print a document.
     * <p/>
     * This downloads the document and notifies listeners that it has been printed so that the printed flag is updated.
     */
    private void download() {
        JobBuilder<Document> builder = newJob();
        Job<Document> job = builder.get(this::getDocument)
                .completed(document -> {
                    DownloadServlet.startDownload(document);
                    printed(null);
                })
                .build();
        manager.runInteractive(job, Messages.get("document.download.title"), Messages.get("document.download.cancel"));
    }

    /**
     * Returns the printer given its reference.
     *
     * @param reference the printer reference. May be {@code null}
     * @return the corresponding printer. May be {@code null}
     */
    private DocumentPrinter getPrinter(PrinterReference reference) {
        return (reference != null) ? getPrinterLocator().getPrinter(reference.getArchetype(), reference.getId()) : null;
    }

    /**
     * Creates a new job builder.
     *
     * @return a new job builder
     */
    private <T> JobBuilder<T> newJob() {
        return JobBuilder.newJob(getDisplayName(), getContext().getUser());
    }
}
