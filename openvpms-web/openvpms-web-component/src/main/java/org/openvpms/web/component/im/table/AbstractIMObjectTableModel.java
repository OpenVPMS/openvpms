/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.component.model.object.IMObject;


/**
 * Abstract {@link IMObject} table model.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectTableModel<T extends IMObject>
        extends AbstractIMTableModel<T> implements IMObjectTableModel<T> {

    /**
     * Constructs an {@link AbstractIMObjectTableModel}.
     * <p/>
     * The column model must be set using {@link #setTableColumnModel}.
     */
    public AbstractIMObjectTableModel() {
    }

    /**
     * Constructs an {@link AbstractIMObjectTableModel}.
     *
     * @param model the table column model. If {@code null} it must be set using {@link #setTableColumnModel}.
     */
    public AbstractIMObjectTableModel(TableColumnModel model) {
        super(model);
    }

}
