/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * {@link Action} factory.
 *
 * @author Tim Anderson
 */
public class ActionFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Constructs an {@link ActionFactory}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public ActionFactory(ArchetypeService service, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Runs an action.
     * <p/>
     * If the action fails, it will be retried.
     *
     * @param action the action to run
     */
    public void run(Runnable action) {
        newAction().action(action).run();
    }

    /**
     * Returns a builder for a new action.
     *
     * @return an action builder
     */
    public ActionBuilder newAction() {
        return new ActionBuilder(service, transactionManager);
    }
}
