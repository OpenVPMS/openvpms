/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.pretty.MessageHelper;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.FailureReason;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.error.ExceptionHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


/**
 * Abstract implementation of the {@link IMObjectDeleter} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectDeleter<T extends IMObject> implements IMObjectDeleter<T> {

    /**
     * The deletion handler factory.
     */
    private final IMObjectDeletionHandlerFactory factory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractIMObjectDeleter.class);

    /**
     * Constructs an {@link AbstractIMObjectDeleter}.
     *
     * @param factory the deletion handler factory
     * @param service the archetype service
     */
    public AbstractIMObjectDeleter(IMObjectDeletionHandlerFactory factory, ArchetypeService service) {
        this.factory = factory;
        this.service = service;
    }

    /**
     * Attempts to delete an object.
     *
     * @param object   the object to delete
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify
     */
    @Override
    public void delete(T object, Context context, HelpContext help, IMObjectDeletionListener<T> listener) {
        try {
            IMObjectDeletionHandler<T> handler = factory.create(object);
            Deletable deletable = handler.getDeletable();
            if (deletable.canDelete()) {
                delete(handler, context, help, listener);
            } else if (object.isActive()) {
                if (handler.canDeactivate()) {
                    deactivate(handler, listener, help);
                } else {
                    unsupported(object, deletable.getReason(), listener);
                }
            } else {
                deactivated(object, help, listener);
            }
        } catch (Throwable exception) {
            FailureReason reason = getFailureReason(object, exception, getDisplayName(object));
            log.error("Failed to delete object={}, reason={}", object.getObjectReference(), reason);
            listener.failed(object, reason);
        }
    }

    /**
     * Invoked to delete an object.
     *
     * @param handler  the deletion handler
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify
     */
    protected abstract void delete(IMObjectDeletionHandler<T> handler, Context context, HelpContext help,
                                   IMObjectDeletionListener<T> listener);

    /**
     * Invoked to deactivate an object.
     *
     * @param handler  the deletion handler
     * @param listener the listener
     * @param help     the help context
     */
    protected abstract void deactivate(IMObjectDeletionHandler<T> handler, IMObjectDeletionListener<T> listener,
                                       HelpContext help);

    /**
     * Invoked when an object cannot be de deleted, and has already been deactivated.
     *
     * @param object   the object
     * @param help     the help context
     * @param listener the listener to notify
     */
    protected abstract void deactivated(T object, HelpContext help, IMObjectDeletionListener<T> listener);

    /**
     * Invoked when deletion and deactivation of an object is not supported.
     *
     * @param object   the object
     * @param reason   reason the object couldn't be deleted. May be {@code null}
     * @param listener the listener to notify
     */
    protected void unsupported(T object, String reason, IMObjectDeletionListener<T> listener) {
        listener.unsupported(object, reason);
    }

    /**
     * Performs deletion.
     *
     * @param handler  the deletion handler
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify
     */
    protected void doDelete(IMObjectDeletionHandler<T> handler, Context context, HelpContext help,
                            final IMObjectDeletionListener<T> listener) {
        T object = handler.getObject();
        try {
            handler.delete(context, help);
            listener.deleted(object);
        } catch (Throwable exception) {
            FailureReason reason = getFailureReason(object, exception, getDisplayName(object));
            if (isAlreadyDeleted(object, reason)) {
                listener.deleted(object);
            } else {
                log.error("Failed to delete object={}, reason={}", object.getObjectReference(), reason);
                listener.failed(object, reason);
            }
        }
    }

    /**
     * Performs deactivation.
     *
     * @param handler  the deletion handler
     * @param listener the listener to notify
     */
    protected void doDeactivate(IMObjectDeletionHandler<T> handler, IMObjectDeletionListener<T> listener) {
        T object = handler.getObject();
        try {
            handler.deactivate();
            listener.deactivated(object);
        } catch (Throwable exception) {
            FailureReason reason = getFailureReason(object, exception, getDisplayName(object));
            log.error("Failed to deactivate object={}, reason={}", object.getObjectReference(), reason);
            listener.failed(object, reason);
        }
    }

    /**
     * Determines the reason for deletion failure.
     *
     * @param object      the object that couldn't be deleted
     * @param cause       the cause
     * @param displayName the display name for the object
     * @return the deletion failure reason
     */
    protected FailureReason getFailureReason(IMObject object, Throwable cause, String displayName) {
        FailureReason reason;
        Throwable rootCause = ExceptionHelper.getRootCause(cause);
        if (ExceptionHelper.isModifiedExternally(rootCause)) {
            // Delete failed as the object (or a related object) has already been deleted
            // Don't propagate the exception
            String message;
            if (rootCause instanceof ObjectNotFoundException) {
                ObjectNotFoundException notFoundException = (ObjectNotFoundException) rootCause;
                if (service.get(object.getObjectReference()) == null) {
                    message = Messages.format("imobject.notfound", displayName);
                    reason = FailureReason.objectNotFound(object.getObjectReference(), message);
                } else {
                    // TODO - really want an IMObjectReference to get the display name.
                    Serializable identifier = notFoundException.getIdentifier();
                    message = Messages.format("imobject.notfound",
                                              MessageHelper.infoString(notFoundException.getEntityName(), identifier));
                    reason = FailureReason.objectNotFound(message);
                }
            } else {
                message = ErrorFormatter.format(cause, ErrorFormatter.Category.DELETE, displayName);
                reason = FailureReason.objectChanged(message);
            }
        } else {
            // non-specific error
            String message = Messages.format("imobject.delete.failed", object.getObjectReference());
            reason = FailureReason.exception(message, cause);
        }
        return reason;
    }

    /**
     * Returns the display name for an object.
     *
     * @param object the object
     * @return the display name
     */
    protected String getDisplayName(IMObject object) {
        return DescriptorHelper.getDisplayName(object, service);
    }

    /**
     * Determines if deletion failed because the object has already been deleted.
     *
     * @param object the object
     * @param reason the deletion failure reason
     * @return {@code true} if the object has already been deleted, otherwise {@code false}
     */
    private boolean isAlreadyDeleted(T object, FailureReason reason) {
        return reason.getReason() == FailureReason.Reason.OBJECT_NOT_FOUND
               && object.getObjectReference().equals(reason.getReference());
    }
}
