/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import java.lang.reflect.Modifier;


/**
 * Factory for {@link Reporter} instances.
 *
 * @author Tim Anderson
 */
public class ReporterFactory {

    /**
     * The report factory.
     */
    private final ReportFactory factory;

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * Reporter implementations.
     */
    private final ArchetypeHandlers<? extends Reporter<?>> reporters;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ReporterFactory.class);

    /**
     * Constructs a {@link ReporterFactory}.
     *
     * @param factory   the report factory
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    @SuppressWarnings("unchecked")
    public ReporterFactory(ReportFactory factory, FileNameFormatter formatter, IArchetypeService service,
                           LookupService lookups) {
        this.factory = factory;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
        Class<?> type = Reporter.class;
        reporters = new ArchetypeHandlers<>("ReporterFactory.properties", null,
                                            (Class<? extends Reporter<?>>) type, "reporter", service);
    }

    /**
     * Creates a new {@link Reporter}.
     *
     * @param object   the object to report on
     * @param template the document template
     * @param type     the expected type of the reporter
     * @return a new reporter
     */
    @SuppressWarnings("unchecked")
    public <T extends IMObject, R extends Reporter<T>> R create(T object, DocumentTemplate template, Class<R> type) {
        R result = newInstance(object.getArchetype(), object, template, type);
        if (result == null) {
            if (type.isAssignableFrom(IMObjectReporter.class)) {
                result = (R) createIMObjectReporter(object, template);
            } else {
                throw new IllegalArgumentException("No Reporters extend " + type.getName() + " and support archetype="
                                                   + object.getArchetype());
            }
        }
        return result;
    }

    /**
     * Creates a new {@link Reporter}.
     *
     * @param objects  the objects to report on
     * @param template the document template
     * @param type     the expected type of the reporter
     * @return a new reporter
     */
    @SuppressWarnings("unchecked")
    public <T extends IMObject, R extends Reporter<T>> R create(Iterable<T> objects, DocumentTemplate template,
                                                                Class<R> type) {
        R result = newInstance(template.getType(), objects, template, type);
        if (result == null) {
            if (type.isAssignableFrom(IMObjectReporter.class)) {
                result = (R) createIMObjectReporter(objects, template);
            } else {
                throw new IllegalArgumentException("No Reporters extend " + type.getName()
                                                   + " and support archetype=" + template.getType());
            }
        }
        return result;
    }

    /**
     * Creates a new {@link Reporter}.
     *
     * @param object  the object to report on
     * @param locator the document template locator
     * @param type    the expected type of the reporter
     * @return a new reporter
     */
    @SuppressWarnings("unchecked")
    public <T extends IMObject, R extends Reporter<T>> R create(T object, DocumentTemplateLocator locator,
                                                                Class<R> type) {
        R result = newInstance(object.getArchetype(), object, locator, type);
        if (result == null) {

            if (type.isAssignableFrom(IMObjectReporter.class)) {
                result = (R) createIMObjectReporter(object, locator);
            } else {
                throw new IllegalArgumentException("No Reporters extend " + type.getName() + " and support archetype="
                                                   + object.getArchetype());
            }
        }
        return result;
    }

    /**
     * Creates a new {@link Reporter}.
     *
     * @param objects the objects to report on
     * @param locator the document template locator
     * @param type    the expected type of the reporter
     * @return a new reporter
     */
    @SuppressWarnings("unchecked")
    public <T extends IMObject, R extends Reporter<T>> R create(Iterable<T> objects, DocumentTemplateLocator locator,
                                                                Class<R> type) {
        R result = newInstance(locator.getType(), objects, locator, type);
        if (result == null) {
            if (type.isAssignableFrom(IMObjectReporter.class)) {
                result = (R) createIMObjectReporter(objects, locator);
            } else {
                throw new IllegalArgumentException("No Reporters extend " + type.getName() + " and support type="
                                                   + locator.getType());
            }
        }
        return result;
    }

    /**
     * Creates a new {@link IMObjectReporter}.
     *
     * @param object   the object to report on
     * @param template the document template
     * @return a new reporter
     */
    public <T extends IMObject> IMObjectReporter<T> createIMObjectReporter(T object, DocumentTemplate template) {
        return new IMObjectReporter<>(object, template, factory, formatter, service, lookups);
    }

    /**
     * Creates a new {@link IMObjectReporter}.
     *
     * @param object  the object to report on
     * @param locator the document template locator
     * @return a new reporter
     */
    public <T extends IMObject> IMObjectReporter<T> createIMObjectReporter(T object, DocumentTemplateLocator locator) {
        return new IMObjectReporter<>(object, locator, factory, formatter, service, lookups);
    }

    /**
     * Creates a new {@link IMObjectReporter}.
     *
     * @param objects  the objects to report on
     * @param template the document template
     * @return a new reporter
     */
    public <T extends IMObject> IMObjectReporter<T> createIMObjectReporter(Iterable<T> objects,
                                                                           DocumentTemplate template) {
        return new IMObjectReporter<>(objects, template, factory, formatter, service, lookups);
    }

    /**
     * Creates a new {@link IMObjectReporter}.
     *
     * @param objects the objects to report on
     * @param locator the document template locator
     * @return a new reporter
     */
    public <T extends IMObject> IMObjectReporter<T> createIMObjectReporter(Iterable<T> objects,
                                                                           DocumentTemplateLocator locator) {
        return new IMObjectReporter<>(objects, locator, factory, formatter, service, lookups);
    }

    /**
     * Creates a {@link ObjectSetReporter}.
     *
     * @param objects  the objects to report on
     * @param template the document template
     * @return a new reporter
     */
    public ObjectSetReporter createObjectSetReporter(Iterable<ObjectSet> objects, DocumentTemplate template) {
        return new ObjectSetReporter(objects, template, factory, formatter, service, lookups);
    }

    /**
     * Creates a {@link ObjectSetReporter}.
     *
     * @param objects the objects to report on
     * @param locator the document template locator
     * @return a new reporter
     */
    public ObjectSetReporter createObjectSetReporter(Iterable<ObjectSet> objects, DocumentTemplateLocator locator) {
        return new ObjectSetReporter(objects, locator, factory, formatter, service, lookups);
    }

    /**
     * Attempts to create a new reporter.
     *
     * @param archetype the archetype short name to create a reporter for
     * @param object    the object to report on
     * @param template  the template
     * @param type      the expected type of the reporter
     * @return a new reporter, or {@code null} if no appropriate constructor can be found or construction fails
     */
    @SuppressWarnings("unchecked")
    private <R extends Reporter<?>> R newInstance(String archetype, Object object, Object template, Class<R> type) {
        boolean concrete = !type.isInterface() && !Modifier.isAbstract(type.getModifiers());
        ArchetypeHandler<? extends Reporter<?>> handler;
        if (concrete) {
            handler = reporters.getHandler(type);
        } else {
            handler = reporters.getHandler(archetype);
        }
        Object result = null;
        if (handler != null) {
            if (!type.isAssignableFrom(handler.getType())) {
                log.error("Reporter of type " + handler.getClass().getName() + " for archetype=" + archetype
                          + " is not an instance of " + type.getName());

            } else {
                DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
                beanFactory.registerSingleton("object", object);
                beanFactory.registerSingleton("template", template);
                beanFactory.registerSingleton("factory", factory);
                beanFactory.registerSingleton("formatter", formatter);
                beanFactory.registerSingleton("service", service);
                beanFactory.registerSingleton("lookups", lookups);

                try {
                    result = beanFactory.createBean(handler.getType(), AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR,
                                                    false);
                } catch (Throwable exception) {
                    log.error(exception.getMessage(), exception);
                }
            }
        }
        return (R) result;
    }
}
