/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.PropertySet;

/**
 * A view layout strategy for {@link Contact} archetypes that excludes the "purposes" node if empty.
 *
 * @author Tim Anderson
 */
public abstract class AbstractContactViewLayout extends AbstractLayoutStrategy {

    /**
     * The nodes to display.
     */
    private final static ArchetypeNodes nodes = new ArchetypeNodes().excludeIfEmpty("purposes");

    /**
     * Constructs an {@link AbstractContactViewLayout}.
     */
    public AbstractContactViewLayout() {
        super(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        String name = object.getName();

        if (!StringUtils.isEmpty(name)) {
            // exclude the name node if it is unchanged from its default
            IMObjectBean bean = getBean(object);
            if (bean.hasNode("name") && bean.isDefaultValue("name")) {
                ArchetypeNodes nodes = new ArchetypeNodes(getArchetypeNodes()).exclude("name");
                setArchetypeNodes(nodes);
            }
        }
        return super.apply(object, properties, parent, context);
    }
}
