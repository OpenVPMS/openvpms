/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.view.layout;

import org.openvpms.component.business.service.archetype.handler.ShortNamePairArchetypeHandlers;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategyFactory;


/**
 * Implementation of the {@link IMObjectLayoutStrategyFactory} for objects displayed in tables.
 *
 * @author Tim Anderson
 */
public class TableLayoutStrategyFactory extends AbstractLayoutStrategyFactory {

    /**
     * Layout strategy implementations.
     */
    private static ShortNamePairArchetypeHandlers _strategies;

    /**
     * Constructs a {@link TableLayoutStrategyFactory}.
     *
     * @param service the archetype service
     */
    public TableLayoutStrategyFactory(ArchetypeService service) {
        super(service);
    }

    /**
     * Returns the strategy implementations.
     *
     * @return the strategy implementations
     */
    protected ShortNamePairArchetypeHandlers getStrategies() {
        synchronized (TableLayoutStrategyFactory.class) {
            if (_strategies == null) {
                _strategies = load("TableLayoutStrategyFactory.properties");
            }
        }
        return _strategies;
    }

}
