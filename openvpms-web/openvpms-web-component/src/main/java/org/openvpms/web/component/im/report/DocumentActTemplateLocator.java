/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.system.ServiceHelper;

/**
 * A {@link DocumentTemplateLocator} for {@link DocumentAct}s, where the template is linked via a
 * <em>documentTemplate</em> node.
 *
 * @author Tim Anderson
 */
public class DocumentActTemplateLocator implements DocumentTemplateLocator {

    /**
     * The act.
     */
    private final DocumentAct act;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Document template node name.
     */
    private static final String DOCUMENT_TEMPLATE = "documentTemplate";

    /**
     * Constructs a {@link DocumentActTemplateLocator}.
     *
     * @param act the act
     */
    public DocumentActTemplateLocator(DocumentAct act) {
        this(act, ServiceHelper.getArchetypeService());
    }

    /**
     * Constructs a {@link DocumentActTemplateLocator}.
     *
     * @param act     the act
     * @param service the archetype service
     */
    public DocumentActTemplateLocator(DocumentAct act, ArchetypeService service) {
        this.act = act;
        this.service = service;
    }

    /**
     * Returns the document template.
     *
     * @return the document template, or {@code null} if the template cannot be located
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public DocumentTemplate getTemplate() {
        DocumentTemplate result = null;
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode(DOCUMENT_TEMPLATE)) {
            Entity template = bean.getTarget(DOCUMENT_TEMPLATE, Entity.class);
            if (template != null) {
                result = new DocumentTemplate(template, service);
            }
        }
        return result;
    }

    /**
     * Returns the type that the template applies to.
     *
     * @return the type. Corresponds to an <em>lookup.documentTemplateType</em> code
     */
    @Override
    public String getType() {
        // NOTE: while this returns the act archetype, any linked template could refer to a different type
        return act.getArchetype();
    }
}