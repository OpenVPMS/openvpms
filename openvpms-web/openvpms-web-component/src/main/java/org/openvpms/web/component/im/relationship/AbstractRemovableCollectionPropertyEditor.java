/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractCollectionPropertyEditor;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A {@link CollectionPropertyEditor} where removed objects are deleted via the {@link IArchetypeService}
 * on save.
 *
 * @author Tim Anderson
 */
public abstract class AbstractRemovableCollectionPropertyEditor extends AbstractCollectionPropertyEditor {

    /**
     * The set of removed objects.
     */
    private final Set<IMObject> removed = new HashSet<>();

    /**
     * The set of removed editors.
     */
    private final Map<IMObject, IMObjectEditor> removedEditors = new HashMap<>();

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs an {@link AbstractRemovableCollectionPropertyEditor}.
     *
     * @param property the collection property
     */
    public AbstractRemovableCollectionPropertyEditor(CollectionProperty property) {
        super(property);
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Saves the collection.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        if (!removed.isEmpty()) {
            remove();
        }
        super.doSave();
    }

    /**
     * Invoked when removing an object.
     * <p/>
     * This queues it for deletion on save.
     *
     * @param object the object being removed
     * @param editor the object's editor. May be {@code null}
     */
    @Override
    protected void remove(IMObject object, IMObjectEditor editor) {
        queueRemove(object, editor);
        super.remove(object, editor);
    }

    /**
     * Queues an object for removal from the database.
     * <p/>
     * This will be removed on save.
     *
     * @param object the object
     * @param editor the object's editor. May be {@code null}
     */
    protected void queueRemove(IMObject object, IMObjectEditor editor) {
        removed.add(object);
        if (editor != null) {
            // use the editor to delete the object on save
            removedEditors.put(object, editor);
        }
    }

    /**
     * Invoked by {@link #doSave()} to remove objects queued for removal.
     * <p/>
     * If an {@link RemoveHandler} is registered, removal will be delegated to this, otherwise the
     * {@link IArchetypeService} will be used.
     *
     * @throws OpenVPMSException if the save fails
     */
    protected void remove() {
        IMObject[] toRemove = removed.toArray(new IMObject[0]);
        RemoveHandler handler = getRemoveHandler();
        for (IMObject object : toRemove) {
            IMObjectEditor editor = removedEditors.get(object);
            if (editor != null) {
                if (handler != null) {
                    handler.remove(editor);
                } else {
                    editor.delete();
                }
            } else {
                if (handler != null) {
                    handler.remove(object);
                } else {
                    service.remove(object);
                }
            }
            removed.remove(object);
            removedEditors.remove(object);
        }
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }
}
