/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.service;

import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.web.security.mail.MailPasswordResolver;

/**
 * A {@link MailService} using settings from the {@link PracticeService}.
 *
 * @author Tim Anderson
 */
public class PracticeMailService extends MailService {

    /**
     * The practice service.
     */
    private final PracticeService service;

    /**
     * Constructs a {@link PracticeMailService}.
     *
     * @param service          the practice service
     * @param passwordResolver the mail server password resolver
     */
    public PracticeMailService(PracticeService service, MailPasswordResolver passwordResolver) {
        super(passwordResolver);
        this.service = service;
    }

    /**
     * Returns the mail server settings.
     *
     * @return the settings, or {@code null} if none is configured
     */
    @Override
    protected MailServer getMailServer() {
        return service.getMailServer();
    }
}
