/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.component.model.act.Act;

/**
 * Implementation of {@link IMObjectActions} for viewing acts.
 *
 * @author Tim Anderson
 */
public class ViewActActions<T extends Act> extends ActActions<T> {

    /**
     * View actions.
     */
    private static final ActActions<Act> INSTANCE = new ViewActActions<>();

    /**
     * Default constructor.
     */
    protected ViewActActions() {
        super();
    }

    /**
     * Determines if objects can be created.
     *
     * @return {@code false}
     */
    @Override
    public boolean canCreate() {
        return false;
    }

    /**
     * Determines if an act can be edited.
     *
     * @param act the act to check
     * @return {@code false}
     */
    @Override
    public boolean canEdit(T act) {
        return false;
    }

    /**
     * Determines if an act can be deleted.
     *
     * @param act the act to check
     * @return {@code false}
     */
    @Override
    public boolean canDelete(T act) {
        return false;
    }

    /**
     * Determines if an act can be posted (i.e. finalised).
     *
     * @param act the act to check
     * @return {@code false}
     */
    @Override
    public boolean canPost(T act) {
        return false;
    }

    /**
     * Posts the act.
     * <p/>
     * This implementation is a no-op.
     *
     * @param act the act to check
     * @return {@code false}
     */
    @Override
    public boolean post(T act) {
        return false;
    }

    /**
     * Updates an act's printed status.
     * <p/>
     * This implementation is a no-op.
     *
     * @param act the act to update
     * @return {@code null}
     */
    @Override
    public T setPrinted(T act) {
        return null;
    }

    /**
     * Returns the singleton instance.
     *
     * @return the instance
     */
    @SuppressWarnings("unchecked")
    public static <T extends Act> ActActions<T> getInstance() {
        return (ActActions<T>) INSTANCE;
    }
}
