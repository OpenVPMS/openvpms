/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.action.Behaviour;
import org.openvpms.web.system.ServiceHelper;

/**
 * Task to update an {@link IMObject} held in the {@link TaskContext}.
 * <p/>
 * This supports reloading the object if the update fails. When retrying is enabled, subclasses should verify
 * the reloaded object is in the correct state to be updated.
 *
 * @author Tim Anderson
 */
public abstract class AbstractUpdateIMObjectTask<T extends IMObject> extends SynchronousTask {

    /**
     * The archetype.
     */
    private final String archetype;

    /**
     * Determines if the object should be saved.
     */
    private final boolean save;

    /**
     * Determines if saving should be retried if it fails.
     */
    private final boolean retry;

    /**
     * Properties to populate the object with. May be {@code null}
     */
    private final TaskProperties properties;

    /**
     * The action factory.
     */
    private final ActionFactory actionFactory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Skip the task if the object is missing.
     */
    private boolean skipIfMissing;

    /**
     * Constructs an {@link AbstractUpdateIMObjectTask}
     *
     * @param archetype the archetype of the object to update
     * @param save      determines if the object should be saved
     * @param retry     determines if the update should be retried if it fails
     */
    public AbstractUpdateIMObjectTask(String archetype, boolean save, boolean retry) {
        this(archetype, null, save, retry);
    }

    /**
     * Constructs an {@link AbstractUpdateIMObjectTask}
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with. May be {@code null}
     * @param save       determines if the object should be saved
     * @param retry      determines if the update should be retried if it fails
     */
    public AbstractUpdateIMObjectTask(String archetype, TaskProperties properties, boolean save, boolean retry) {
        this.archetype = archetype;
        this.properties = properties;
        this.save = save;
        this.retry = retry;
        actionFactory = ServiceHelper.getBean(ActionFactory.class);
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Specifies to skip the update if the object has been deleted.
     */
    public void skipIfMissing() {
        this.skipIfMissing = true;
    }

    /**
     * Executes the task.
     * <p/>
     * This delegates to {@link #update(IMObject, TaskContext)} to update the object.
     * <p/>
     * If the update fails, and retries were enabled, it will be retried up to {@link Behaviour#DEFAULT_RETRIES} times.
     *
     * @param context the task context
     */
    public void execute(TaskContext context) {
        actionFactory.newAction()
                .backgroundOnly()
                .automaticRetry((retry) ? Behaviour.DEFAULT_RETRIES : 0)
                .withObject(archetype, () -> getObject(context))
                .useLatestInstanceOnRetry()
                .skipIfMissing(skipIfMissing)
                .call(object -> update(object, context))
                .onFailure(this::notifyCancelled)
                .run();
    }

    /**
     * Updates the object, if {@link #canUpdate(IMObject, TaskContext)} returns {@code true}.
     * The updated object will be saved if required.
     * <p/>
     * The object will be added to the task context even if it has not been updated, to ensure the context holds
     * the most up-to-date instance.
     *
     * @param object  the object to update
     * @param context the task context
     */
    protected void update(T object, TaskContext context) {
        if (canUpdate(object, context)) {
            populate(object, context);
            if (save) {
                service.save(object);
            }
        }
        context.addObject(object);
    }

    /**
     * Determines if the object can be updated.
     * <p/>
     * Subclasses can override this to prevent the object being updated.
     *
     * @param object  the object
     * @param context the task context
     * @return {@code true}
     */
    protected boolean canUpdate(T object, TaskContext context) {
        return true;
    }

    /**
     * Populates an object.
     *
     * @param object  the object to populate
     * @param context the task context
     */
    protected void populate(T object, TaskContext context) {
        if (properties != null) {
            populate(object, properties, context);
        }
    }

    /**
     * Returns the object.
     *
     * @param context the task context
     * @return the object, or {@code null} if none is found
     */
    @SuppressWarnings("unchecked")
    private T getObject(TaskContext context) {
        return (T) context.getObject(archetype);
    }
}