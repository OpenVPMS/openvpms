/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A pageable table model that tracks marked rows across pages.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMarkablePagedIMTableModel<T> extends PagedIMTableModel<T, T> {

    /**
     * Listener for mark/unmark events.
     */
    private final ListMarkModel.Listener listener;

    /**
     * Constructs a {@link AbstractMarkablePagedIMTableModel}.
     *
     * @param model the underlying table model
     */
    public AbstractMarkablePagedIMTableModel(IMTableModel<T> model) {
        super(model);
        ListMarkModel rowMarks = model.getRowMarkModel();
        if (rowMarks != null) {
            listener = new ListMarkModel.Listener() {
                @Override
                public void changed(int index, boolean marked) {
                    setMarked(index, marked);
                }

                @Override
                public void cleared() {
                    clearMarks();
                }
            };
            rowMarks.addListener(listener);
        } else {
            listener = null;
        }
    }

    /**
     * Marks/unmarks an object.
     *
     * @param object the object
     * @param mark   if {@code true} mark the object, else unmark it
     */
    public void setMarked(T object, boolean mark) {
        mark(object, mark);
        int index = getObjects().indexOf(object);
        if (index != -1) {
            ListMarkModel rowMarks = getModel().getRowMarkModel();
            if (rowMarks != null) {
                rowMarks.removeListener(listener);
                rowMarks.setMarked(index, mark);
                rowMarks.addListener(listener);
            }
        }
    }

    /**
     * Determines if at least one object is marked.
     *
     * @return {@code true} if one or more objects are marked.
     */
    public abstract boolean isMarked();

    /**
     * Returns the marked objects.
     * <p/>
     * This can be used if all objects are held in memory.
     *
     * @param objects all objects in the collection
     * @return the marked objects
     */
    public List<T> getMarked(Collection<T> objects) {
        List<T> result = new ArrayList<>();
        for (T object : objects) {
            if (isMarked(object)) {
                result.add(object);
            }
        }
        return result;
    }

    /**
     * Unmark all rows.
     */
    public void unmarkAll() {
        ListMarkModel rowMarks = getModel().getRowMarkModel();
        if (rowMarks != null) {
            rowMarks.removeListener(listener);
            rowMarks.clear();
            clearMarks();
            rowMarks.addListener(listener);
        }
    }

    /**
     * Flags an object as marked/unmarked.
     *
     * @param object the object
     * @param mark   if {@code true}, flag it is marked, else flag it as unmarked
     */
    protected abstract void mark(T object, boolean mark);

    /**
     * Determines if an object is marked.
     *
     * @param object the object
     * @return {@code true if the object is marked}
     */
    protected abstract boolean isMarked(T object);

    /**
     * Clear all marks.
     */
    protected abstract void clearMarks();

    /**
     * Sets the objects for the current page.
     *
     * @param objects the objects to set
     */
    @Override
    protected void setPage(List<T> objects) {
        ListMarkModel rowMarks = getModel().getRowMarkModel();
        if (rowMarks != null) {
            rowMarks.removeListener(listener);
        }
        super.setPage(objects);
        if (rowMarks != null) {
            int i = 0;
            for (T object : objects) {
                if (isMarked(object)) {
                    rowMarks.setMarked(i, true);
                }
                i++;
            }
            rowMarks.addListener(listener);
        }
    }

    /**
     * Marks/unmarks an object.
     *
     * @param row  the row of the object
     * @param mark if {@code true}, mark the object, otherwise unmark it
     */
    protected void setMarked(int row, boolean mark) {
        T object = getObjects().get(row);
        mark(object, mark);
    }

}
