/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.property.Property;

import java.util.List;

/**
 * Binds an {@link IMObject} {@link Reference} {@link Property} to a combo box that displays their names.
 *
 * @author Tim Anderson
 */
public class BoundIMObjectReferenceComboBox<T extends IMObject> extends AbstractBoundIMObjectComboBox<T> {

    /**
     * Constructs a {@link BoundIMObjectReferenceComboBox}.
     *
     * @param property the property
     * @param objects  the objects
     */
    public BoundIMObjectReferenceComboBox(Property property, List<? extends T> objects) {
        super(property, objects);
    }

    /**
     * Creates a binder for the property.
     *
     * @param property the property
     * @return a new binder
     */
    @Override
    protected Binder createBinder(Property property) {
        return new IMObjectComboBoxBinder(property) {
            @Override
            protected boolean updateProperty(Property property, Object object) {
                Reference reference = (object != null) ? ((IMObject) object).getObjectReference() : null;
                return property.setValue(reference);
            }
        };
    }

    /**
     * Returns the text for a property value.
     *
     * @param value the value. May be {@code null}
     * @return the corresponding text
     */
    @Override
    protected String getText(Object value) {
        String result = null;
        if (value instanceof IMObject) {
            result = ((IMObject) value).getName();
        } else if (value instanceof Reference) {
            Reference reference = (Reference) value;
            for (IMObject object : getObjects()) {
                if (reference.equals(object.getObjectReference())) {
                    result = object.getName();
                    break;
                }
            }
        }
        return result;
    }
}
