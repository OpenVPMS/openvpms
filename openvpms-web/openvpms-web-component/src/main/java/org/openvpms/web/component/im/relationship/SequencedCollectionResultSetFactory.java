/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.AbstractCollectionResultSetFactory;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.CollectionResultSetFactory;
import org.openvpms.web.component.im.query.IMObjectListResultSet;
import org.openvpms.web.component.im.query.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.openvpms.web.component.im.relationship.SequencedRelationshipCollectionHelper.sort;

/**
 * A {@link CollectionResultSetFactory} that orders the set on sequence.
 *
 * @author Tim Anderson
 */
public class SequencedCollectionResultSetFactory extends AbstractCollectionResultSetFactory
        implements CollectionResultSetFactory {

    /**
     * The singleton instance.
     */
    public static final CollectionResultSetFactory INSTANCE = new SequencedCollectionResultSetFactory();

    /**
     * Default constructor.
     */
    private SequencedCollectionResultSetFactory() {
        super();
    }

    /**
     * Creates a new result set.
     *
     * @param property the collection property
     * @param context  the context
     * @return a new result set
     */
    @Override
    @SuppressWarnings("unchecked")
    public ResultSet<IMObject> createResultSet(CollectionPropertyEditor property, Context context) {
        RelationshipCollectionTargetPropertyEditor editor = (RelationshipCollectionTargetPropertyEditor) property;
        List<Map.Entry<IMObject, Relationship>> entries = sort(editor.getTargets());
        List<IMObject> sorted = new ArrayList<>();
        for (Map.Entry<IMObject, Relationship> entry : entries) {
            sorted.add(entry.getKey());
        }
        return new IMObjectListResultSet<>(sorted, DEFAULT_ROWS);
    }

}
