/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.job;

import org.openvpms.component.model.user.User;

import java.util.Date;
import java.util.function.Consumer;

/**
 * Abstract implementation of {@link Job}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractJob<T> implements Job<T> {

    /**
     * The display name for the job.
     */
    private final String name;

    /**
     * The user that launched the job.
     */
    private final User user;

    /**
     * The time when the job was created.
     */
    private final Date created;

    /**
     * The time when the job was started.
     */
    private Date started;

    /**
     * Constructs an {@link AbstractJob}.
     *
     * @param name the display name for the job
     * @param user the user that launched the job
     */
    public AbstractJob(String name, User user) {
        this.name = name;
        this.user = user;
        created = new Date();
    }

    /**
     * The name of the job.
     *
     * @return name of the job
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the time when the job was created.
     *
     * @return the time when the job was created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * Returns the user that initiated the job.
     *
     * @return the user that initiated the job
     */
    public User getUser() {
        return user;
    }

    /**
     * Returns the time when the job was started.
     *
     * @return the time when the job was started, or {@code null} if the job has yet to start
     */
    @Override
    public synchronized Date getStarted() {
        return started;
    }

    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public final T get() {
        started();
        return runJob();
    }

    /**
     * Returns the listener to invoke when the job completes successfully.
     *
     * @return the completion listener. May be {@code null}
     */
    @Override
    public Consumer<T> getCompletionListener() {
        return null;
    }

    /**
     * Returns the listener to invoke when the job is cancelled.
     *
     * @return the cancellation listener. May be {@code null}
     */
    @Override
    public Runnable getCancellationListener() {
        return null;
    }

    /**
     * Returns the listener top invoke when the job fails.
     *
     * @return the failure listener. May be {@code null}
     */
    @Override
    public Consumer<Throwable> getFailureListener() {
        return null;
    }

    /**
     * Cancels the job.
     *
     * @param thread a handle to the job thread. This may be used to interrupt blocking threads
     */
    @Override
    public void cancel(JobThread thread) {
        thread.interrupt();
    }

    /**
     * Indicate that the job has started.
     */
    protected synchronized void started() {
        started = new Date();
    }

    /**
     * Runs the job.
     */
    protected abstract T runJob();
}
