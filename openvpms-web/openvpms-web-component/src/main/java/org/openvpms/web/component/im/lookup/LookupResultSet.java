/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.lookup;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.AbstractIMObjectResultSet;
import org.openvpms.web.component.im.query.DefaultQueryExecutor;
import org.openvpms.web.component.im.query.ResultSet;


/**
 * A {@link ResultSet} for lookups.
 * <p/>
 * This searches the <em>id</em>, <em>name</em> and <em>code</em> nodes for the query value.
 *
 * @author Tim Anderson
 */
public class LookupResultSet extends AbstractIMObjectResultSet<Lookup> {

    /**
     * Constructs a {@link LookupResultSet}.
     *
     * @param archetypes the archetypes to query
     * @param value      the value to query on. May be {@code null}
     * @param sort       the sort criteria. May be {@code null}
     * @param rows       the maximum no. of rows per page
     */
    public LookupResultSet(ShortNameConstraint archetypes, String value, SortConstraint[] sort, int rows) {
        super(archetypes, value, null, sort, rows, false, new DefaultQueryExecutor<>());
        setSearch(value, ID, NAME, "code");
    }
}
