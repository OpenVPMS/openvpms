/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.openvpms.component.business.service.archetype.ArchetypeServiceHelper;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.business.service.archetype.helper.lookup.LookupAssertion;
import org.openvpms.component.business.service.archetype.helper.lookup.LookupAssertionFactory;
import org.openvpms.component.business.service.archetype.helper.lookup.RemoteLookup;
import org.openvpms.component.business.service.archetype.helper.lookup.TargetLookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Factory for {@link LookupPropertyEditor} instances.
 *
 * @author Tim Anderson
 */
public class LookupPropertyEditorFactory {

    /**
     * Editor implementations.
     */
    private static ArchetypeHandlers<LookupPropertyEditor> editors;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LookupPropertyEditorFactory.class);

    /**
     * Prevent construction.
     */
    private LookupPropertyEditorFactory() {
    }

    /**
     * Creates a new editor.
     *
     * @param property the property to edit
     * @param parent   the parent object
     * @param context  the layout context
     * @return an editor for {@code object}
     */
    public static LookupPropertyEditor create(Property property, IMObject parent, LayoutContext context) {
        LookupPropertyEditor result = null;
        ArchetypeHandler<LookupPropertyEditor> handler = null;
        String shortName = null;
        if (property.getDescriptor() != null) {
            LookupAssertion assertion = LookupAssertionFactory.create(property.getDescriptor(),
                                                                      ServiceHelper.getArchetypeService(),
                                                                      ServiceHelper.getLookupService());
            if (assertion instanceof RemoteLookup) {
                shortName = ((RemoteLookup) assertion).getShortName();
                handler = getEditors().getHandler(shortName);
            } else if (assertion instanceof TargetLookup) {
                handler = getEditors().getHandler(((TargetLookup) assertion).getArchetypes());
            }
        }

        if (handler != null) {
            try {
                if (shortName != null) {
                    try {
                        result = handler.create(new Object[]{shortName, property, parent, context});
                    } catch (NoSuchMethodException ignore) {
                        result = create(handler, property, parent, context);
                    }
                } else {
                    result = create(handler, property, parent, context);
                }
            } catch (Throwable throwable) {
                log.error(throwable.getMessage(), throwable);
            }
        }
        if (result == null) {
            result = new DefaultLookupPropertyEditor(property, parent);
        }
        return result;
    }

    private static LookupPropertyEditor create(ArchetypeHandler<LookupPropertyEditor> handler, Property property,
                                               IMObject parent, LayoutContext context)
            throws InvocationTargetException, IllegalAccessException, InstantiationException, NoSuchMethodException {
        LookupPropertyEditor result;
        try {
            result = handler.create(new Object[]{property, parent, context});
        } catch (NoSuchMethodException ignore2) {
            result = handler.create(new Object[]{property, parent});
        }
        return result;
    }

    /**
     * Returns the editors.
     *
     * @return the editors
     */
    private static synchronized ArchetypeHandlers<LookupPropertyEditor> getEditors() {
        if (editors == null) {
            editors = new ArchetypeHandlers<>("LookupPropertyEditorFactory.properties", LookupPropertyEditor.class,
                                              ArchetypeServiceHelper.getArchetypeService());
        }
        return editors;
    }

}
