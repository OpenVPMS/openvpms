/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.LinkedList;


/**
 * Helper to manage a set of document references.
 *
 * @author Tim Anderson
 */
public class DocReferenceMgr {

    /**
     * The original reference.
     */
    private final Reference original;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The set of references to manage.
     */
    private final LinkedList<Reference> references = new LinkedList<>();

    /**
     * Determines if all documents should be deleted.
     */
    private boolean deleteAll;

    /**
     * Constructs a {@link DocReferenceMgr}.
     *
     * @param service the archetype service
     */
    public DocReferenceMgr(ArchetypeService service) {
        this(null, service);
    }

    /**
     * Constructs a {@link DocReferenceMgr}.
     *
     * @param original the original reference. May be {@code null}
     * @param service  the archetype service
     */
    public DocReferenceMgr(Reference original, ArchetypeService service) {
        if (original != null) {
            references.add(original);
        }
        this.original = original;
        this.service = service;
    }

    /**
     * Adds a new document reference.
     *
     * @param reference the reference to add
     */
    public void add(Reference reference) {
        references.add(reference);
        setDeleteAll(false);
    }

    /**
     * Determines if all documents should be deleted.
     * <p/>
     * If {@code true} and a document is subsequently added, the flag will be set {@code false}.
     *
     * @param all if {@code true}, delete all documents, otherwise delete all but the last
     */
    public void setDeleteAll(boolean all) {
        this.deleteAll = all;
    }

    /**
     * Removes a document reference.
     *
     * @param reference the reference to remove
     */
    public void remove(Reference reference) {
        references.remove(reference);
    }

    /**
     * Commits the changes.
     *
     * @throws ArchetypeServiceException for any error
     */
    public void commit() {
        int size = (deleteAll) ? 0 : 1;
        while (references.size() > size) {
            delete(references.removeFirst());
        }
    }

    /**
     * Rolls back the changes. Every document bar the original will be removed.
     *
     * @throws ArchetypeServiceException for any error
     */
    public void rollback() {
        while (references.size() > 1) {
            Reference reference = references.removeLast();
            delete(reference);
        }
        if (references.size() == 1 && original != null && !references.get(0).equals(original)) {
            delete(references.removeLast());
        }
    }

    /**
     * Deletes all documents.
     *
     * @throws ArchetypeServiceException for any error
     */
    public void delete() {
        while (!references.isEmpty()) {
            delete(references.removeLast());
        }
    }

    /**
     * Removes the document associated with a document reference.
     *
     * @param reference the reference of the document to remove
     * @throws ArchetypeServiceException for any error
     */
    private void delete(Reference reference) {
        service.remove(reference);
    }
}
