/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.im.act.PrintedFlagUpdater;

import static org.openvpms.archetype.rules.act.ActStatus.CANCELLED;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;


/**
 * Implementation of {@link IMObjectActions} for acts.
 *
 * @author Tim Anderson
 */
public abstract class ActActions<T extends Act> extends AbstractIMObjectActions<T> {

    /**
     * Determines if a confirmation should be displayed prior to printing unfinalised acts.
     */
    private final boolean warnOnPrintUnfinalised;


    /**
     * Default edit actions.
     */
    private static final ActActions<Act> EDIT = new ActActions<Act>() {
    };

    /**
     * Edit actions for acts that a warning should be displayed for if they are unfinalised when printing.
     */
    private static final ActActions<Act> EDIT_WARN_ON_PRINT_UNFINALISED = new ActActions<Act>(true) {
    };

    /**
     * Default constructor.
     */
    public ActActions() {
        this(false);
    }

    /**
     * Constructs an {@link ActActions}.
     *
     * @param warnOnPrintUnfinalised if {@code true}, printing an unfinalised act should display a confirmation
     */
    public ActActions(boolean warnOnPrintUnfinalised) {
        this.warnOnPrintUnfinalised = warnOnPrintUnfinalised;
    }

    /**
     * Determines if an act can be edited.
     *
     * @param act the act to check
     * @return {@code true} if the act isn't locked
     */
    public boolean canEdit(T act) {
        return super.canEdit(act) && !isLocked(act);
    }

    /**
     * Determines if an act can be deleted.
     *
     * @param act the act to check
     * @return {@code true} if the act isn't locked
     */
    public boolean canDelete(T act) {
        return super.canDelete(act) && !isLocked(act);
    }

    /**
     * Determines if an act can be posted (i.e finalised).
     * <p/>
     * This implementation returns {@code true} if the act status isn't {@code POSTED} or {@code CANCELLED}.
     *
     * @param act the act to check
     * @return {@code true} if the act can be posted
     */
    public boolean canPost(T act) {
        String status = act.getStatus();
        return !POSTED.equals(status) && !CANCELLED.equals(status);
    }

    /**
     * Posts the act. This changes the act's status to {@code POSTED}, and saves it.
     *
     * @param act the act to check
     * @return {@code true} if the act was posted
     */
    public boolean post(T act) {
        if (canPost(act)) {
            act.setStatus(POSTED);
            return SaveHelper.save(act);
        }
        return false;
    }

    /**
     * Updates an act's printed status.
     *
     * @param act the act to update
     * @return the saved act, or {@code null} if it wasn't updated
     */
    public T setPrinted(T act) {
        PrintedFlagUpdater updater = createPrintFlagUpdater();
        return updater.setPrinted(act);
    }

    /**
     * Determines if an act is unfinalised, for the purposes of printing.
     *
     * @param act the act
     * @return {@code true} if the act is unfinalised, otherwise {@code false}
     */
    public boolean isUnfinalised(Act act) {
        return !ActStatus.POSTED.equals(act.getStatus());
    }

    /**
     * Determines if a confirmation should be displayed before printing an unfinalised act.
     *
     * @return {@code false}
     */
    public boolean warnWhenPrintingUnfinalisedAct() {
        return warnOnPrintUnfinalised;
    }

    /**
     * Determines if an act is locked from changes.
     *
     * @param act the act
     * @return {@code true} if the act status is {@link ActStatus#POSTED}
     */
    public boolean isLocked(T act) {
        return ActStatus.POSTED.equals(act.getStatus());
    }

    /**
     * Returns actions that only support viewing acts.
     *
     * @return the actions
     */
    public static <T extends Act> ActActions<T> view() {
        return ViewActActions.getInstance();
    }

    /**
     * Returns actions that support editing acts.
     *
     * @return the actions
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T extends Act> ActActions<T> edit() {
        return (ActActions) EDIT;
    }

    /**
     * Returns actions that support editing acts.
     *
     * @param warnOnPrintUnfinalised if {@code true}, printing an unfinalised act should display a confirmation
     * @return the actions
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T extends Act> ActActions<T> edit(boolean warnOnPrintUnfinalised) {
        return warnOnPrintUnfinalised ? (ActActions) EDIT_WARN_ON_PRINT_UNFINALISED : (ActActions) EDIT;
    }

    /**
     * Creates a new print flag updater to update the printed flag of acts.
     *
     * @return a new print flag updater
     */
    protected PrintedFlagUpdater createPrintFlagUpdater() {
        return new PrintedFlagUpdater();
    }
}
