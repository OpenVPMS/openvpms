/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;

/**
 * Factory for {@link OAuth2AuthorizedClientProvider} that configures the provider to support authorization_code
 * grant flows, and refresh tokens.
 *
 * @author Tim Anderson
 */
public class OAuth2AuthorizedClientProviderFactory implements FactoryBean<OAuth2AuthorizedClientProvider> {

    /**
     * Creates a new {@link OAuth2AuthorizedClientProvider}.
     *
     * @return a new instance
     */
    @Override
    public OAuth2AuthorizedClientProvider getObject() {
        return OAuth2AuthorizedClientProviderBuilder.builder()
                .authorizationCode()
                .refreshToken()
                .build();
    }

    @Override
    public Class<?> getObjectType() {
        return OAuth2AuthorizedClientProvider.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
