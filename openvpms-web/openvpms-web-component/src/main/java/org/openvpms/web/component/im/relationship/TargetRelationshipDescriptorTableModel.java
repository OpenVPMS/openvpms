/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableColumn;

import java.util.Arrays;
import java.util.List;

/**
 * A {@link RelationshipDescriptorTableModel} that displays the target of a relationship.
 *
 * @author Tim Anderson
 */
public class TargetRelationshipDescriptorTableModel extends RelationshipDescriptorTableModel<Relationship> {

    /**
     * Constructs a {@link TargetRelationshipDescriptorTableModel}.
     * <p/>
     * Enables selection if the context is in edit mode.
     *
     * @param archetypes the archetypes
     * @param context    the layout context
     */
    public TargetRelationshipDescriptorTableModel(String[] archetypes, LayoutContext context) {
        super(archetypes, context, true);
    }

    /**
     * Returns the sort constraints, given a primary sort column.
     * <p>
     * If the column is not sortable, this implementation returns null.
     *
     * @param primary   the primary sort column
     * @param ascending whether to sort in ascending or descending order
     * @return the sort criteria, or {@code null} if the column isn't sortable
     */
    @Override
    protected List<SortConstraint> getSortConstraints(DescriptorTableColumn primary, boolean ascending) {
        if ("target".equals(primary.getName())) {
            //  NOTE: this must be prefixed by the alias
            return Arrays.asList(new NodeSortConstraint("entity.target.name", ascending),
                                 new NodeSortConstraint("entity.target.id", ascending));
        }
        return super.getSortConstraints(primary, ascending);
    }
}
