/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.processor;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.layout.GridLayoutData;
import org.openvpms.archetype.component.processor.BatchProcessor;
import org.openvpms.archetype.component.processor.BatchProcessorListener;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;


/**
 * Dialog to display an {@link ProgressBarProcessor}.
 *
 * @author Tim Anderson
 */
public class BatchProcessorDialog extends ModalDialog {

    /**
     * The processor.
     */
    private final ProgressBarProcessor<?> processor;

    /**
     * Determines if the dialog should close on completion.
     */
    private final boolean closeOnCompletion;

    /**
     * Constructs a {@link BatchProcessorDialog}.
     *
     * @param title     the dialog title
     * @param processor the processor
     * @param help      the help context
     */
    public BatchProcessorDialog(String title, ProgressBarProcessor<?> processor, HelpContext help) {
        this(title, null, processor, false, help);
    }

    /**
     * Constructs a {@link BatchProcessorDialog}.
     *
     * @param title             the dialog title
     * @param processor         the processor
     * @param closeOnCompletion determines if the dialog should close on completion. Don't set {@code true} if
     *                          {@link BatchProcessor#setListener(BatchProcessorListener)} is used externally
     * @param help              the help context
     */
    public BatchProcessorDialog(String title, String message, ProgressBarProcessor<?> processor,
                                boolean closeOnCompletion, HelpContext help) {
        super(title, CANCEL, help);
        Grid grid = new Grid();
        grid.setWidth(new Extent(100, Extent.PERCENT));
        grid.setHeight(new Extent(100, Extent.PERCENT));
        Component content;
        if (message != null) {
            Label label = LabelFactory.text(message);
            content = ColumnFactory.create(Styles.WIDE_CELL_SPACING, label, processor.getComponent());
        } else {
            content = processor.getComponent();
        }
        GridLayoutData data = new GridLayoutData();
        data.setAlignment(Alignment.ALIGN_CENTER);
        content.setLayoutData(data);
        grid.add(content);
        getLayout().add(grid);
        this.processor = processor;
        this.closeOnCompletion = closeOnCompletion;
    }

    /**
     * Shows the dialog, and starts the processor.
     */
    public void show() {
        super.show();
        if (closeOnCompletion) {
            processor.setListener(new BatchProcessorListener() {
                @Override
                public void completed() {
                    close();
                }

                @Override
                public void error(Throwable exception) {
                    ErrorHelper.show(exception);
                }
            });
            super.show();
        }
        processor.process();
    }

    /**
     * Cancels the operation.
     * <p/>
     * This implementation cancels the processor.
     */
    @Override
    protected void doCancel() {
        processor.cancel();
        super.doCancel();
    }

    /**
     * Returns the processor.
     *
     * @return the processor
     */
    protected ProgressBarProcessor<?> getProcessor() {
        return processor;
    }
}
