/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * An action that passes a list of {@link IMObject}s to a {@link Consumer}, reloading them on retry.
 */
class IMObjectsConsumerAction<T extends IMObject> extends ReloadingAction {

    /**
     * The objects.
     */
    private final List<T> objects;

    /**
     * The consumer.
     */
    private final BiConsumer<List<T>, Consumer<ActionStatus>> consumer;

    /**
     * Constructs an {@link IMObjectsConsumerAction}.
     *
     * @param objects            the objects
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public IMObjectsConsumerAction(List<T> objects, Behaviour behaviour,
                                   boolean async, BiConsumer<List<T>, Consumer<ActionStatus>> consumer,
                                   ArchetypeService service, PlatformTransactionManager transactionManager) {
        super(behaviour, async, service, transactionManager);
        this.objects = objects;
        this.consumer = consumer;
        for (T object : objects) {
            addObject(object);
        }
    }

    /**
     * Executes the action.
     * <p/>
     * For synchronous actions, this is run within a transaction.
     *
     * @param listener the listener
     */
    @Override
    protected void execute(Consumer<ActionStatus> listener) {
        ActionStatus result = null;
        List<T> latest = new ArrayList<>();
        for (T object : objects) {
            T current = getCurrentObject(object);
            if (current == null) {
                // should never occur
                result = ActionStatus.failed(Messages.get("action.internalerror"));
            } else {
                latest.add(current);
            }
        }
        if (result == null) {
            consumer.accept(latest, listener);
        } else {
            listener.accept(result);
        }
    }
}
