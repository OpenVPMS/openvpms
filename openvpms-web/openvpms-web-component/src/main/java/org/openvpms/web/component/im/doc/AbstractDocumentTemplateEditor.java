/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.filetransfer.UploadListener;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.select.BasicSelector;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.event.ActionListener;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.UnsupportedDoc;

/**
 * Editor for document templates that have an associated mandatory/optional document act.
 *
 * @author Tim Anderson
 */
public class AbstractDocumentTemplateEditor extends AbstractIMObjectEditor {

    /**
     * The format for revisions, when a user loads a template. This enables better sorting than if revisions were based
     * on locale-based timestamps.
     */
    private static final DateTimeFormatter REVISION_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * The upload selector.
     */
    private final BasicSelector<DocumentAct> selector;

    /**
     * Helper to hook the document into the validation support.
     */
    private final SimpleProperty content = new SimpleProperty("content", IMObjectReference.class);

    /**
     * Manages old document references to avoid orphaned documents.
     */
    private final DocReferenceMgr refMgr;

    /**
     * The document handler.
     */
    private DocumentHandler handler;

    /**
     * The document act that has the template.
     */
    private DocumentAct act;

    /**
     * Determines if the document has changed.
     */
    private boolean docModified = false;

    /**
     * Determines if the document should be deleted on save.
     */
    private boolean deleteDocOnSave;

    /**
     * The revision node.
     */
    private static final String REVISION = "revision";

    /**
     * Constructs a {@link AbstractDocumentTemplateEditor}.
     *
     * @param template    the object to edit
     * @param parent      the parent object. May be {@code null}
     * @param docRequired determines if the document template is required
     * @param handler     the document handler. May be {@code null}
     * @param context     the layout context. May be {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public AbstractDocumentTemplateEditor(Entity template, IMObject parent, boolean docRequired,
                                          DocumentHandler handler, LayoutContext context) {
        super(template, parent, context);
        setDocumentRequired(docRequired);
        this.handler = handler;
        refMgr = new DocReferenceMgr(getService());
        getDocumentAct();
        content.setValue(act.getDocument());

        selector = new BasicSelector<>("button.upload");
        selector.getSelect().addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                onUpload();
            }
        });
        updateDisplay(act);
    }

    /**
     * Determines if the object has been changed.
     *
     * @return {@code true} if the object has been changed
     */
    @Override
    public boolean isModified() {
        return super.isModified() || docModified;
    }

    /**
     * Clears the modified status of the object.
     */
    @Override
    public void clearModified() {
        super.clearModified();
        docModified = false;
    }

    /**
     * Cancel any edits. Once complete, query methods may be invoked, but the behaviour of other methods is undefined.
     */
    @Override
    public void cancel() {
        super.cancel();
        try {
            refMgr.rollback();
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Sets the document handler.
     *
     * @param handler the handler
     */
    protected void setDocumentHandler(DocumentHandler handler) {
        this.handler = handler;
    }

    /**
     * Returns the document handler.
     *
     * @return the document handler
     */
    protected DocumentHandler getDocumentHandler() {
        return handler;
    }

    /**
     * Returns the document act, creating it if none exists.
     *
     * @return the document act
     */
    protected DocumentAct getDocumentAct() {
        if (act == null) {
            TemplateHelper helper = new TemplateHelper(getService());
            act = helper.getDocumentAct((Entity) getObject());
            if (act == null) {
                act = createDocumentAct();
                IMObjectBean bean = getBean(act);
                bean.setTarget("template", getObject());
            } else if (act.getDocument() != null) {
                refMgr.add(act.getDocument());
            }
        }
        return act;
    }

    /**
     * Creates a new document act.
     *
     * @return a new document act
     */
    protected DocumentAct createDocumentAct() {
        return create(DocumentArchetypes.DOCUMENT_TEMPLATE_ACT, DocumentAct.class);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateContent(validator);
    }

    /**
     * Determines if the document is required.
     *
     * @param required if {@code true}, the document is required for the editor to be valid
     */
    protected void setDocumentRequired(boolean required) {
        content.setRequired(required);
    }

    /**
     * Determines if the document is required.
     *
     * @return {@code true} if the document is required
     */
    protected boolean isDocumentRequired() {
        return content.isRequired();
    }

    /**
     * Determines if the document should be deleted on save.
     *
     * @param delete if {@code true}, delete the document on save
     */
    protected void setDeleteDocument(boolean delete) {
        deleteDocOnSave = delete;
    }

    /**
     * Save any modified child Saveable instances.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void saveChildren() {
        super.saveChildren();
        if (deleteDocOnSave) {
            if (isDocumentRequired()) {
                throw new IllegalStateException("Document is required but has been flagged for deletion");
            }
            if (act != null && !act.isNew()) {
                getService().remove(act);
                act = null;
            }
            refMgr.delete();
        } else if (act != null) {
            if (docModified) {
                getService().save(act);
                refMgr.commit();
                docModified = false;
            }
        }
    }

    /**
     * Deletes the object.
     *
     * @throws OpenVPMSException if the delete fails
     */
    @Override
    protected void doDelete() {
        getService().remove(act);
        act = null;
        super.doDelete();
    }

    /**
     * Deletes any child Deletable instances.
     *
     * @throws OpenVPMSException if the delete fails
     */
    @Override
    protected void deleteChildren() {
        super.deleteChildren();
        refMgr.delete();
    }

    /**
     * Returns the selector.
     *
     * @return the selector
     */
    protected ComponentState getSelector() {
        return new ComponentState(selector.getComponent(), content);
    }

    /**
     * Enables/disables the upload button.
     *
     * @param enable if {@code true}, enable the button, else disable it
     */
    protected void enableUpload(boolean enable) {
        selector.getSelect().setEnabled(enable);
    }

    /**
     * Invoked to upload a document.
     */
    protected void onUpload() {
        UploadListener listener = new DocumentUploadListener(handler) {
            protected void upload(Document doc) {
                onUpload(doc);
            }
        };
        UploadDialog dialog = new UploadDialog(listener, getLayoutContext().getHelpContext());
        dialog.show();
    }

    /**
     * Invoked when a document is uploaded.
     *
     * @param document the uploaded document
     */
    protected void onUpload(Document document) {
        getService().save(document);
        getDocumentAct(); // make sure there is a current act.
        populateDocumentAct(act, document);
        replaceDocReference(document);
        updateRevision();
        updateDisplay(act);
        docModified = true;
    }

    /**
     * Populates a document act with details from a document.
     *
     * @param act      the act to populate
     * @param document the document
     */
    protected void populateDocumentAct(DocumentAct act, Document document) {
        act.setFileName(document.getName());
        getService().deriveValue(act, "name");
        act.setMimeType(document.getMimeType());
        if (getParent() == null) {
            act.setDescription(document.getDescription());
        } else {
            act.setDescription(getParent().getName());
        }
    }

    /**
     * Updates the display with the selected act.
     *
     * @param act the act
     */
    protected void updateDisplay(DocumentAct act) {
        selector.setObject(act);
    }

    /**
     * Returns the content of the current document as a stream, if one is present.
     *
     * @return the document content as a stream, or {@code null} if no document is present
     * @throws DocumentException for any error
     */
    protected InputStream getContent() {
        InputStream result = null;
        Reference reference = getDocumentAct().getDocument();
        if (reference != null) {
            Document document = getService().get(reference, Document.class);
            if (document != null) {
                result = getContent(document);
            }
        }
        return result;
    }

    /**
     * Returns the document content as a stream.
     *
     * @param document the document
     * @return the document content
     * @throws DocumentException for any error
     */
    protected InputStream getContent(Document document) {
        return handler.getContent(document);
    }

    /**
     * Validates that the content associated with the content property is supported.
     *
     * @param fileName  the file name
     * @param mimeType  the mime type
     * @param validator the validator
     * @param content   the content property
     * @return {@code true} if the content is valid, otherwise {@code false}
     */
    protected boolean validateContent(String fileName, String mimeType, Validator validator, Property content) {
        boolean valid = isContentSupported(fileName, mimeType);
        if (!valid) {
            validator.add(content, new DocumentException(UnsupportedDoc, fileName, mimeType).getMessage());
        }
        return valid;
    }

    /**
     * Determines if the content is supported.
     *
     * @param fileName the file name
     * @param mimeType the mime type
     * @return {@code true} if the content is supported, otherwise {@code false}
     */
    protected boolean isContentSupported(String fileName, String mimeType) {
        return handler.canHandle(fileName, mimeType);
    }

    /**
     * Determines if the content is valid.
     *
     * @param validator the validator
     * @return {@code true} if the content is valid, otherwise {@code false}
     */
    private boolean validateContent(Validator validator) {
        boolean valid = content.validate(validator);
        if (valid && content.getReference() != null) {
            DocumentAct act = getDocumentAct();
            String fileName = act.getFileName();
            String mimeType = act.getMimeType();
            valid = validateContent(fileName, mimeType, validator, content);
        }
        return valid;
    }

    /**
     * Replaces the existing document reference with that of a new document.
     * The existing document is queued for deletion.
     *
     * @param document the new document
     */
    private void replaceDocReference(Document document) {
        Reference ref = document.getObjectReference();
        act.setDocument(ref);
        refMgr.add(ref);
        content.setValue(ref);
    }

    /**
     * Updates the revision using the current date/time.
     */
    private void updateRevision() {
        Property revision = getProperty(REVISION);
        if (revision != null) {
            String value = REVISION_FORMAT.format(LocalDateTime.now());
            revision.setValue(value);
        }
    }
}
