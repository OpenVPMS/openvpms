/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import java.util.function.Consumer;

/**
 * Base for actions where the action runs synchronously.
 *
 * @author Tim Anderson
 */
public abstract class SynchronousAction extends AbstractAction {

    /**
     * Runs the action, catching any exceptions.
     *
     * @param listener the listener to notify on completion
     */
    @Override
    protected void runProtected(Consumer<ActionStatus> listener) {
        ActionStatus result;
        try {
            result = runAction();
        } catch (Throwable exception) {
            result = ActionStatus.retry(exception);
        }
        listener.accept(result);
    }

    /**
     * Runs the action.
     *
     * @return the action status
     */
    protected abstract ActionStatus runAction();
}
