/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import java.util.function.Consumer;

/**
 * A retryable action.
 *
 * @author Tim Anderson
 */
public interface Action extends Runnable {

    /**
     * Runs the action.
     */
    @Override
    void run();

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    void run(Consumer<ActionStatus> listener);
}