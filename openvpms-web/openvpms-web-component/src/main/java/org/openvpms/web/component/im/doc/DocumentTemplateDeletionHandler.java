/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.delete.AbstractEntityDeletionHandler;
import org.openvpms.web.component.im.delete.Deletable;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * An {@link IMObjectDeletionHandler} for <em>entity.documentTemplate</em>, <em>entity.documentTemplateEmailUser</em>,
 * and <em>entity.documentTemplateEmailSystem</em>
 *
 * @author Tim Anderson
 */
public class DocumentTemplateDeletionHandler extends AbstractEntityDeletionHandler<Entity> {

    /**
     * The archetypes this deletion handler supports.
     */
    private static final String[] ARCHETYPES = {DocumentArchetypes.DOCUMENT_TEMPLATE,
                                                DocumentArchetypes.USER_EMAIL_TEMPLATE,
                                                DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE};

    /**
     * Constructs a {@link DocumentTemplateDeletionHandler}.
     *
     * @param object             the object to delete
     * @param factory            the editor factory
     * @param transactionManager the transaction manager
     * @param service            the archetype service
     */
    public DocumentTemplateDeletionHandler(Entity object, IMObjectEditorFactory factory,
                                           PlatformTransactionManager transactionManager,
                                           IArchetypeRuleService service) {
        super(object, null, factory, transactionManager, service);
    }

    /**
     * Determines if the object can be deleted.
     *
     * @return {@link Deletable#yes()} if the object can be deleted, otherwise {@link Deletable#no}
     */
    @Override
    public Deletable getDeletable() {
        if (!getObject().isA(ARCHETYPES)) {
            return Deletable.no(Messages.format("imobject.delete.unsupportedarchetype", getDisplayName()));
        }
        return super.getDeletable();
    }

    /**
     * Returns the participation archetypes to check.
     *
     * @return the participation archetypes to check
     */
    @Override
    protected String[] getParticipations() {
        return new String[]{DocumentArchetypes.DOCUMENT_TEMPLATE_PARTICIPATION};
    }
}
