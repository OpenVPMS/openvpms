/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.button.ButtonGroup;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.TaskRules;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.web.component.im.edit.DefaultIMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.relationship.EntityLinkEditor;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutableProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.join;

/**
 * Editor for <em>entityLink.productTask</em> relationships.
 *
 * @author Tim Anderson
 */
public class ProductTaskEditor extends EntityLinkEditor {

    /**
     * The task rules.
     */
    private final TaskRules rules;

    /**
     * The start node name.
     */
    static final String START = "start";

    /**
     * The start units node name.
     */
    static final String START_UNITS = "startUnits";

    /**
     * The work list node name.
     */
    static final String WORK_LIST = "worklist";

    /**
     * Constructs a {@link ProductTaskEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public ProductTaskEditor(EntityLink object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        rules = ServiceHelper.getBean(TaskRules.class);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateWorklist(validator);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Creates the property set.
     *
     * @param object    the object being edited
     * @param archetype the object archetype
     * @param variables the variables for macro expansion. May be {@code null}
     * @return the property set
     */
    @Override
    protected PropertySet createPropertySet(IMObject object, ArchetypeDescriptor archetype, Variables variables) {
        return new PropertySetBuilder(object, archetype, variables)
                .mutable(START)
                .mutable(START_UNITS)
                .mutable(WORK_LIST)
                .build();
    }

    /**
     * Validates the work list.
     *
     * @param validator the validator
     * @return {@code true} if the work list is valid, otherwise {@code false}
     */
    private boolean validateWorklist(Validator validator) {
        Entity taskType = getTargetEditor().getObject();
        Property property = getProperty(WORK_LIST);
        Reference reference = property.getReference();
        if (reference != null) {
            Entity worklist = (Entity) getObject(reference);
            if (worklist == null) {
                validator.add(property, Messages.format("imobject.noexist",
                                                        getDisplayName(ScheduleArchetypes.ORGANISATION_WORKLIST)));
            } else if (!worklist.isActive()) {
                validator.add(property, Messages.format("product.task.inactiveWorklist", worklist.getName()));
            } else if (!rules.hasTaskType(worklist, taskType)) {
                validator.add(property, Messages.format("product.task.invalidWorklist", worklist.getName(),
                                                        taskType.getName()));
            }
        }
        return validator.isValid();
    }

    private static class LayoutStrategy extends AbstractLayoutStrategy {

        public LayoutStrategy() {
            super(ArchetypeNodes.all().exclude(START_UNITS));
        }

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            addComponent(createStart(object, properties, context));
            addComponent(createWorkList(object, properties, context));
            return super.apply(object, properties, parent, context);
        }

        /**
         * Creates a component for the start period.
         *
         * @param object     the object
         * @param properties the object's properties
         * @param context    the layout context
         * @return a new component
         */
        private ComponentState createStart(IMObject object, PropertySet properties, LayoutContext context) {
            MutableProperty start = (MutableProperty) properties.get(START);
            MutableProperty startUnits = (MutableProperty) properties.get(START_UNITS);
            ComponentState startComponent = createComponentPair(start, startUnits, object, context);
            ButtonGroup group = new ButtonGroup();
            RadioButton button1 = ButtonFactory.create("product.task.startWhenInvoiced", group, () -> {
                start.setValue(null);
                start.setRequired(false);
                startUnits.setRequired(false);
            });
            RadioButton button2 = ButtonFactory.create(null, group, () -> {
                start.setRequired(true);
                startUnits.setRequired(true);
                if (startUnits.getString() == null) {
                    startUnits.setValue(DateUnits.DAYS.toString());
                }
                startComponent.getFocusGroup().setFocus();
            });
            if (start.getInt() == 0) {
                button1.setSelected(true);
            } else {
                button2.setSelected(true);
            }
            start.addModifiableListener(modifiable -> {
                if (start.getValue() == null) {
                    button1.setSelected(true);
                } else {
                    button2.setSelected(true);
                }
            });
            Row row = RowFactory.create(Styles.CELL_SPACING, button2, startComponent.getComponent(),
                                        LabelFactory.create("product.task.startAfterInvoicing"));
            Column column = ColumnFactory.create(button1, row);
            FocusGroup focus = new FocusGroup(START);
            focus.add(button1);
            focus.add(button2);
            focus.add(startComponent.getFocusGroup());
            ComponentState result = new ComponentState(column, start, focus);
            result.getLabel().setLayoutData(ComponentGrid.layout(Alignment.ALIGN_TOP));
            return result;
        }

        /**
         * Creates a component for the work list.
         *
         * @param object     the object
         * @param properties the object's properties
         * @param context    the layout context
         * @return a new component
         */
        private ComponentState createWorkList(IMObject object, PropertySet properties, LayoutContext context) {
            Relationship relationship = (Relationship) object;
            MutableProperty worklist = (MutableProperty) properties.get(WORK_LIST);
            IMObjectReferenceEditor<Entity> worklistEditor = new DefaultIMObjectReferenceEditor<Entity>(
                    worklist, object, context) {
                @Override
                protected Query<Entity> createQuery(String name) {
                    Query<Entity> query = super.createQuery(name);
                    if (relationship.getTarget() != null) {
                        query.setConstraints(join("taskTypes").add(eq("target", relationship.getTarget())));
                    }
                    return query;
                }
            };
            ButtonGroup group = new ButtonGroup();
            RadioButton button1 = ButtonFactory.create("product.task.useFollowUp", group, () -> {
                worklist.setValue(null);
                worklist.setRequired(false);
            });
            RadioButton button2 = ButtonFactory.create(null, group, () -> worklist.setRequired(true));
            if (worklist.getReference() == null) {
                button1.setSelected(true);
            } else {
                button2.setSelected(true);
            }
            worklist.addModifiableListener(modifiable -> {
                if (worklist.getReference() != null) {
                    button2.setSelected(true);
                } else {
                    button1.setSelected(true);
                }
            });
            Row row = RowFactory.create(Styles.CELL_SPACING, button2, worklistEditor.getComponent());
            Column column = ColumnFactory.create(button1, row);
            FocusGroup focus = new FocusGroup(WORK_LIST);
            focus.add(button1);
            focus.add(button2);
            focus.add(worklistEditor.getFocusGroup());
            ComponentState result = new ComponentState(column, worklist, focus);
            result.getLabel().setLayoutData(ComponentGrid.layout(Alignment.ALIGN_TOP));
            return result;
        }
    }
}