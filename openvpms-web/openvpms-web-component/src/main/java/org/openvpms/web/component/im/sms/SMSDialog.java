/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import echopointng.KeyStrokes;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.component.model.party.Contact;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ReloadingContext;
import org.openvpms.web.component.macro.MacroDialog;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Collections;
import java.util.List;

/**
 * Dialog to edit and send SMS messages.
 *
 * @author Tim Anderson
 */
public class SMSDialog extends ModalDialog {

    /**
     * The SMS editor.
     */
    private final SMSEditor editor;

    /**
     * The editor container.
     */
    private final Column container;

    /**
     * Constructs an {@link SMSDialog}.
     *
     * @param phone   the phone contact to send to. May be {@code null}
     * @param context the context
     * @param help    the help context
     */
    public SMSDialog(Contact phone, Context context, HelpContext help) {
        this(phone != null ? Collections.singletonList(phone) : null, context, help);
    }

    /**
     * Constructs an {@link SMSDialog}.
     *
     * @param phones  the phone numbers to select from. May be {@code null}
     * @param context the context
     * @param help    the help context
     */
    public SMSDialog(List<Contact> phones, final Context context, HelpContext help) {
        super(Messages.get("sms.send.title"), OK_CANCEL, help);
        MacroVariables variables = new MacroVariables(new ReloadingContext(context),
                                                      ServiceHelper.getArchetypeService(),
                                                      ServiceHelper.getLookupService());
        SMSService smsService = ServiceHelper.getSMSService();
        editor = createEditor(phones, variables, context, smsService);
        editor.setMaxParts(smsService.getMaxParts());

        container = ColumnFactory.create(Styles.WIDE_CELL_SPACING);
        getLayout().add(ColumnFactory.create(Styles.INSET, container));
        getFocusGroup().add(0, editor.getFocusGroup());

        getButtons().addKeyListener(KeyStrokes.ALT_MASK | KeyStrokes.VK_M, new ActionListener() {
            public void onAction(ActionEvent event) {
                onMacro(context);
            }
        });
        editor.getFocusGroup().setFocus();
        resize("SMSDialog.size");
    }

    /**
     * Sets the message to send.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        editor.setMessage(message);
    }

    /**
     * Creates an SMS editor.
     *
     * @param phones     the phones
     * @param variables  the variables
     * @param context    the context
     * @param smsService the SMS service
     * @return the editor
     */
    protected SMSEditor createEditor(List<Contact> phones, MacroVariables variables, Context context,
                                     SMSService smsService) {
        return new SMSEditor(phones, variables, context, smsService);
    }

    /**
     * Invoked when the 'OK' button is pressed. This sets the action and closes
     * the window.
     */
    @Override
    protected void onOK() {
        if (send()) {
            super.onOK();
        }
    }

    /**
     * Displays the macros.
     *
     * @param context the context
     */
    protected void onMacro(Context context) {
        MacroDialog dialog = new MacroDialog(context, getHelpContext());
        dialog.show();
    }

    /**
     * Sends the message.
     *
     * @return {@code true} if the message was sent
     */
    protected boolean send() {
        boolean result = false;
        try {
            Validator validator = new DefaultValidator();
            if (editor.validate(validator)) {
                editor.send();
                result = true;
            } else {
                ValidationHelper.showError(validator);
            }
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        return result;
    }

    /**
     * Returns the editor.
     *
     * @return the editor
     */
    protected SMSEditor getEditor() {
        return editor;
    }

    /**
     * Returns the editor container.
     *
     * @return the editor container
     */
    protected Column getContainer() {
        return container;
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        container.add(editor.getComponent());
    }
}
