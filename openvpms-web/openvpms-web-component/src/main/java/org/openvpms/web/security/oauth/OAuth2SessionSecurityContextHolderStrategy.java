/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;

import java.util.concurrent.atomic.AtomicReference;

/**
 * A {@link SecurityContextHolderStrategy} designed to hold the principal when performing OAuth2 authorization flows.
 * This must be bound to the session.
 * <p/>
 * TODO - this can be removed when there is a facility to determine the email address of {@link OAuth2AuthorizedClient}.
 *
 * @author Tim Anderson
 * @see OAuth2RequestLauncher
 * @see PrincipalResolvingOAuth2AuthorizationCodeGrantFilter
 */
public class OAuth2SessionSecurityContextHolderStrategy implements SecurityContextHolderStrategy {

    /**
     * The security context.
     */
    private final AtomicReference<SecurityContext> context = new AtomicReference<>();

    @Override
    public void clearContext() {
        context.set(null);
    }

    @Override
    public SecurityContext getContext() {
        SecurityContext result = context.get();
        if (result == null) {
            result = createEmptyContext();
            if (!context.compareAndSet(null, result)) {
                result = context.get();
            }
        }
        return result;
    }

    @Override
    public void setContext(SecurityContext context) {
        this.context.set(context);
    }

    @Override
    public SecurityContext createEmptyContext() {
        return new SecurityContextImpl();
    }
}
