/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * An {@link LookupQuery} that queries lookups for the specified archetype.
 *
 * @author Tim Anderson
 */
public class ArchetypeLookupQuery extends AbstractLookupQuery {

    /**
     * The archetype.
     */
    private final String archetype;

    /**
     * The codes to limit to.
     */
    private final String[] codes;


    /**
     * Constructs an {@link ArchetypeLookupQuery}.
     *
     * @param archetype the lookup archetype
     */
    public ArchetypeLookupQuery(String archetype) {
        this(archetype, new String[0]);
    }

    /**
     * Constructs an {@link ArchetypeLookupQuery}.
     *
     * @param archetype the lookup archetype
     * @param codes     the codes to include
     */
    public ArchetypeLookupQuery(String archetype, String[] codes) {
        this.archetype = archetype;
        this.codes = codes;
    }

    /**
     * Returns the lookups.
     *
     * @return the lookups
     */
    public List<Lookup> getLookups() {
        Collection<Lookup> lookups = ServiceHelper.getLookupService().getLookups(archetype);
        List<Lookup> result = new ArrayList<>(lookups);
        if (codes.length != 0) {
            result.removeIf(lookup -> !ArrayUtils.contains(codes, lookup.getCode()));
        }
        sort(result);
        return result;
    }
}
