/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.PeriodRelationship;
import org.openvpms.component.model.object.Relationship;

import java.util.Date;


/**
 * Relationship state for {@link PeriodRelationship PeriodRelationships}.
 *
 * @author Tim Anderson
 */
public class PeriodRelationshipState extends RelationshipState {

    /**
     * Constructs a {@link PeriodRelationshipState}.
     *
     * @param relationship      the relationship
     * @param sourceId          the source id
     * @param sourceName        the source name
     * @param sourceDescription the source description
     * @param targetId          the target id
     * @param targetName        the target name
     * @param targetDescription the target description
     * @param active            determines if the source and target are active
     */
    public PeriodRelationshipState(PeriodRelationship relationship,
                                   long sourceId, String sourceName,
                                   String sourceDescription, long targetId,
                                   String targetName, String targetDescription,
                                   boolean active) {
        super(relationship, sourceId, sourceName, sourceDescription, targetId,
              targetName, targetDescription, active);
    }

    /**
     * Constructs a {@link PeriodRelationshipState}.
     *
     * @param parent       the parent object
     * @param relationship the relationship
     * @param source       determines if parent is the source or target of the
     *                     relationship
     * @throws ArchetypeServiceException for any archetype service exception
     */
    public PeriodRelationshipState(IMObject parent, PeriodRelationship relationship, boolean source) {
        super(parent, relationship, source);
    }

    /**
     * Determines if the relationship is active.
     * It is active if:
     * <ul>
     * <li>the underlying {@link Relationship} is active
     * <li>the underlying objects are active
     * <li>{@link PeriodRelationship#getActiveEndTime} is null or greater than the current time
     * </ul>
     *
     * @return {@code true} if this is active; otherwise {@code false}
     */
    @Override
    public boolean isActive() {
        boolean result = super.isActive();
        if (result) {
            Date endTime = getRelationship().getActiveEndTime();
            result = endTime == null || endTime.getTime() > System.currentTimeMillis();
        }
        return result;
    }

    /**
     * Returns the relationship.
     *
     * @return the relationship
     */
    @Override
    public PeriodRelationship getRelationship() {
        return (PeriodRelationship) super.getRelationship();
    }
}
