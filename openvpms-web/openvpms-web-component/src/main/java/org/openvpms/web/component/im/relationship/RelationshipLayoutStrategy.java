/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategyFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectReferenceViewer;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.echo.factory.LabelFactory;


/**
 * Layout strategy for {@link Relationship}s.
 * <p/>
 * This returns a viewer for the "non-current" object in a relationship.<br/>
 * This returns the "non-current" or target side of the relationship.<br/>
 * "Non-current" refers the object that is NOT currently being viewed/edited. If the source and target object don't
 * refer to the current object being viewed/edited, then the target object of the relationship is used.
 *
 * @author Tim Anderson
 */
public class RelationshipLayoutStrategy implements IMObjectLayoutStrategy {

    /**
     * If {@code true}, displays the "non-current" object in the relationship,
     * otherwise display its name, with an optional hyperlink.
     */
    private final boolean displayInine;


    /**
     * Constructs a {@link RelationshipLayoutStrategy}.
     * <p/>
     * Displays the name of the "non-current" object in the relationship.
     */
    public RelationshipLayoutStrategy() {
        this(false);
    }

    /**
     * Constructs a {@link RelationshipLayoutStrategy}.
     *
     * @param displayInline if {@code true}, displays the "non-current" object
     *                      in the relationship, otherwise display its name,
     *                      with an optional hyperlink
     */
    public RelationshipLayoutStrategy(boolean displayInline) {
        this.displayInine = displayInline;
    }

    /**
     * Pre-registers a component for inclusion in the layout.
     * <p/>
     * This implementation is a no-op.
     *
     * @param state the component state
     */
    public void addComponent(ComponentState state) {
        // do nothing
    }

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to
     * create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    public ComponentState apply(IMObject object, PropertySet properties,
                                IMObject parent, LayoutContext context) {
        ComponentState result;
        Relationship relationship = (Relationship) object;
        Reference ref = getObject(relationship, parent);
        if (!displayInine) {
            IMObjectReferenceViewer viewer = new IMObjectReferenceViewer(ref, context.getContextSwitchListener(),
                                                                         context.getContext());
            result = new ComponentState(viewer.getComponent());
        } else {
            IMObject entity = context.getCache().get(ref);
            if (entity != null) {
                IMObjectLayoutStrategyFactory factory = context.getLayoutStrategyFactory();
                IMObjectLayoutStrategy strategy = factory.create(entity, object);
                result = strategy.apply(entity, new PropertySetBuilder(entity, context).build(), object, context);
            } else {
                result = new ComponentState(LabelFactory.create());
            }
        }
        return result;
    }

    /**
     * Returns a reference to the object in a relationship. This returns either:
     * <ul>
     * <li>the object that isn't the same as parent, if parent is supplied; or
     * <li>the target of the relationship
     * </ul>
     *
     * @param relationship the relationship
     * @param parent       the parent object. May be {@code null}
     * @return the object. May be {@code null}
     */
    protected Reference getObject(Relationship relationship, IMObject parent) {
        Reference result;
        if (parent == null) {
            result = relationship.getTarget();
        } else {
            Reference ref = parent.getObjectReference();

            if (relationship.getSource() != null && ref.equals(relationship.getSource())) {
                result = relationship.getTarget();
            } else if (relationship.getTarget() != null && ref.equals(relationship.getTarget())) {
                result = relationship.getSource();
            } else {
                result = relationship.getTarget();
            }
        }
        return result;
    }

}
