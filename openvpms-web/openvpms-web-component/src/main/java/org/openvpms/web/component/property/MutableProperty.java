/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.openvpms.web.resource.i18n.Messages;

/**
 * A property with changeable attributes.
 * <p/>
 * Note that an optional property may be made required, but the reverse is not true; these will fail validation.
 *
 * @author Tim Anderson
 */
public class MutableProperty extends DelegatingProperty {

    /**
     * Overrides the default read-only flag.
     */
    private MutableBoolean readOnly;

    /**
     * Overrides the default hidden flag.
     */
    private MutableBoolean hidden;

    /**
     * Overrides the default minimum cardinality.
     */
    private MutableInt minCardinality;

    /**
     * Constructs a {@code DelegatingProperty}
     *
     * @param property the property to delegate to
     */
    public MutableProperty(Property property) {
        super(property);
    }

    /**
     * Determines if the property is read-only.
     *
     * @param readOnly {@code true} if it is read-only, {@code false} if it is editable
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setReadOnly(boolean readOnly) {
        boolean changed = false;
        if (isReadOnly() != readOnly) {
            this.readOnly = new MutableBoolean(readOnly);
            changed = true;
        }
        return changed;
    }

    /**
     * Determines if the property is read-only.
     *
     * @return {@code true} if the property is read-only
     */
    @Override
    public boolean isReadOnly() {
        return readOnly != null ? readOnly.booleanValue() : super.isReadOnly();
    }

    /**
     * Determines if the property is hidden.
     *
     * @param hidden {@code true} if it is hidden, {@code false} if it is visible
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setHidden(boolean hidden) {
        boolean changed = false;
        if (isHidden() != hidden) {
            this.hidden = new MutableBoolean(hidden);
            changed = true;
        }
        return changed;
    }

    /**
     * Determines if the property is hidden.
     *
     * @return {@code true} if the property is hidden; otherwise {@code false}
     */
    @Override
    public boolean isHidden() {
        return hidden != null ? hidden.booleanValue() : super.isHidden();
    }

    /**
     * Determines if the property is required.
     *
     * @param required {@code true} if it is required, {@code false} if it is optional
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setRequired(boolean required) {
        boolean changed = false;
        if (isRequired() != required) {
            if (required) {
                minCardinality = new MutableInt(1);
            } else {
                minCardinality = null;
            }
            changed = true;
        }
        return changed;
    }

    /**
     * Determines if the property is required.
     *
     * @return {@code true} if the property is required; otherwise {@code false}
     */
    @Override
    public boolean isRequired() {
        return getMinCardinality() > 0;
    }

    /**
     * Returns the minimum cardinality.
     *
     * @return the minimum cardinality
     */
    @Override
    public int getMinCardinality() {
        return minCardinality != null ? minCardinality.intValue() : super.getMinCardinality();
    }

    /**
     * Makes the property editable.
     *
     * @param required if {@code true}, the property is required, else it is optional
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setEditable(boolean required) {
        boolean changed = setRequired(required);
        changed |= setReadOnly(false);
        changed |= setHidden(false);
        return changed;
    }

    /**
     * Makes a property viewable.
     *
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setViewable() {
        boolean changed = setReadOnly(true);
        changed |= setHidden(false);
        return changed;
    }

    /**
     * Makes the property unsupported.
     * <p/>
     * This hides it, makes it optional, and read-only.
     *
     * @return {@code true} if the property was changed, otherwise {@code false}
     */
    public boolean setUnsupported() {
        boolean changed = setHidden(true);
        changed |= setReadOnly(false);
        changed |= setReadOnly(true);
        return changed;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    public boolean validate(Validator validator) {
        if (super.validate(validator)) {
            if (isRequired() && getValue() == null) {
                validator.add(this, Messages.format("property.error.required", getDisplayName()));
            }
        }
        return validator.isValid();
    }
}