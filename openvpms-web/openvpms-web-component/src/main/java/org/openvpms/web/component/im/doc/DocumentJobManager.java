/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.job.JobManager;
import org.openvpms.web.component.job.JobThread;
import org.openvpms.web.component.print.Printer;
import org.openvpms.web.echo.servlet.DownloadServlet;
import org.openvpms.web.resource.i18n.Messages;

import java.util.concurrent.Future;

/**
 * A {@link JobManager} for document jobs.
 *
 * @author Tim Anderson
 */
public class DocumentJobManager extends JobManager {

    /**
     * Constructs a {@link DocumentJobManager}.
     */
    public DocumentJobManager() {
        this(1000);
    }

    /**
     * Constructs a {@link DocumentJobManager}.
     *
     * @param timeout the no. of milliseconds to wait before displaying a cancellation dialog
     */
    public DocumentJobManager(int timeout) {
        super(timeout, "DocumentJob-%d");
    }

    /**
     * Constructs a {@link DocumentJobManager}.
     *
     * @param timeout       the no. of milliseconds to wait before displaying a cancellation dialog
     * @param namingPattern the naming pattern to use for threads created by the thread factory
     */
    public DocumentJobManager(int timeout, String namingPattern) {
        super(timeout, namingPattern);
    }

    /**
     * Interactively previews a document.
     *
     * @param printer the printer
     * @param user    the user launching the preview
     * @return a future that may be used to cancel the job. This will invoke {@link Job#cancel(JobThread)}
     */
    public Future<?> preview(Printer printer, User user) {
        Job<Document> job = JobBuilder.<Document>newJob(printer.getDisplayName(), user)
                .get(printer::getDocument)
                .completed(DownloadServlet::startDownload)
                .build();
        return runInteractive(job, Messages.get("document.preview.title"), Messages.get("document.preview.cancel"));
    }
}