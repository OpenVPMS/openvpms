/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

/**
 * Determines {@link Action} retry behaviour.
 *
 * @author Tim Anderson
 */
class Retries {

    /**
     * Determines if retries after the background retries should be interactive.
     */
    private final boolean interactive;

    /**
     * The delay between background retries, in milliseconds.
     */
    private final long delay;

    /**
     * Determines if there should be a delay on the first retry.
     */
    private final boolean delayFirst;

    /**
     * The number of times to retry in the background.
     */
    private int automatic;

    /**
     * Constructs a {@link Retries}.
     *
     * @param automatic   the number of times to retry automatically
     * @param interactive determines if subsequent retries should be interactive
     * @param delay       the delay between background retries, in milliseconds
     * @param delayFirst  determines if there should be a delay on the first retry
     */
    public Retries(int automatic, boolean interactive, long delay, boolean delayFirst) {
        this.automatic = automatic;
        this.interactive = interactive;
        this.delay = delay;
        this.delayFirst = delayFirst;
    }

    /**
     * Determines if the next retry should be automatic.
     *
     * @return {@code true} if it should be automatic, otherwise check {@link #interactiveRetry()}
     */
    public boolean automaticRetry() {
        boolean result = false;
        if (automatic > 0) {
            result = true;
            automatic--;
        }
        return result;
    }

    /**
     * Determines if the next retry should be interactive i.e. prompt the user.
     *
     * @return {@code true} if the next retry should be interactive, {@code false} if retries shouldn't be interactive
     */
    public boolean interactiveRetry() {
        return interactive;
    }

    /**
     * Returns the delay to use when running in the background.
     *
     * @return the delay, in milliseconds
     */
    public long delay() {
        return delay;
    }

    /**
     * Determines if there should be a delay on the first try.
     * <p>
     * return {@code true} if there should be a delay, otherwise {@code false}
     */
    public boolean delayFirst() {
        return delayFirst;
    }
}
