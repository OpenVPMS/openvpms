/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.filter;

import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;

import java.util.Objects;


/**
 * Node filter that enables nodes to be excluded by value.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class ValueNodeFilter implements NodeFilter {

    /**
     * The node name.
     */
    private final String name;

    /**
     * The node value.
     */
    private final Object value;


    /**
     * Constructs a {@link ValueNodeFilter}.
     *
     * @param name  the name of the node to filter
     * @param value the value to exclude the the node on
     */
    public ValueNodeFilter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Determines if a node should be included.
     *
     * @param descriptor the node descriptor
     * @param object     the object. May be {@code null}
     * @return {@code true} if the node should be included; otherwise {@code false}
     */
    @Override
    public boolean include(NodeDescriptor descriptor, IMObject object) {
        boolean result;
        if (object == null) {
            result = true;
        } else if (!descriptor.getName().equals(name)) {
            result = true;
        } else {
            Object other = descriptor.getValue(object);
            result = !Objects.equals(value, other);
        }
        return result;
    }
}
