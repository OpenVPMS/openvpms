/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Row;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;

import static org.openvpms.web.component.im.product.ProductTaskEditor.START;
import static org.openvpms.web.component.im.product.ProductTaskEditor.START_UNITS;
import static org.openvpms.web.component.im.product.ProductTaskEditor.WORK_LIST;

/**
 * View layout strategy for <em>entityLink.productTask</em>.
 *
 * @author Tim Anderson
 */
public class ProductTaskLayoutStrategy extends AbstractLayoutStrategy {

    public ProductTaskLayoutStrategy() {
        super(ArchetypeNodes.all().exclude(START_UNITS));
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        addComponent(createStart(object, properties, context));
        addComponent(createWorkList(object, properties, context));
        return super.apply(object, properties, parent, context);
    }

    /**
     * Creates a component for the start period.
     *
     * @param object     the object
     * @param properties the object's properties
     * @param context    the layout context
     * @return a new component
     */
    private ComponentState createStart(IMObject object, PropertySet properties, LayoutContext context) {
        ComponentState result;
        Property start = properties.get(START);
        if (start.getInt() == 0) {
            result = new ComponentState(LabelFactory.create("product.task.startWhenInvoiced"), start);
        } else {
            Property startUnits = properties.get(START_UNITS);
            ComponentState pair = createComponentPair(start, startUnits, object, context);
            Row row = RowFactory.create(Styles.CELL_SPACING, pair.getComponent(),
                                        LabelFactory.create("product.task.startAfterInvoicing"));
            result = new ComponentState(row, start);
        }
        return result;
    }

    /**
     * Creates a component for the work list.
     *
     * @param object     the object
     * @param properties the object's properties
     * @param context    the layout context
     * @return a new component
     */
    private ComponentState createWorkList(IMObject object, PropertySet properties, LayoutContext context) {
        ComponentState result;
        Property worklist = properties.get(WORK_LIST);
        if (worklist.getReference() == null) {
            result = new ComponentState(LabelFactory.create("product.task.useFollowUp"), worklist);
        } else {
            result = context.getComponentFactory().create(worklist, object);
        }
        return result;
    }
}