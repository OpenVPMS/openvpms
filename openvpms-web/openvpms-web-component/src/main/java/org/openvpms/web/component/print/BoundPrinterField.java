/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.web.component.bound.Binder;
import org.openvpms.web.component.bound.SelectFieldBinder;
import org.openvpms.web.component.property.Property;

import java.util.List;
import java.util.Objects;

/**
 * Binds a property to a {@link PrinterField}.
 *
 * @author Tim Anderson
 */
public class BoundPrinterField extends PrinterField {

    /**
     * The binder.
     */
    private final Binder binder;

    /**
     * Constructs a {@link BoundPrinterField} that shows all available printers.
     *
     * @param property the property to bind
     */
    public BoundPrinterField(Property property) {
        super();
        binder = createBinder(property);
    }

    /**
     * Constructs a {@link BoundPrinterField}.
     *
     * @param property the property to bind
     * @param printers the available printers
     */
    public BoundPrinterField(Property property, List<PrinterReference> printers) {
        super(printers);
        binder = createBinder(property);
    }

    /**
     * Constructs a {@link BoundPrinterField}.
     *
     * @param property the property to bind
     * @param model    the model
     */
    public BoundPrinterField(Property property, PrinterListModel model) {
        super(model);
        binder = createBinder(property);
    }

    /**
     * Life-cycle method invoked when the {@code Component} is added to a registered hierarchy.
     */
    @Override
    public void init() {
        super.init();
        binder.bind();
    }

    /**
     * Life-cycle method invoked when the {@code Component} is removed from a registered hierarchy.
     */
    @Override
    public void dispose() {
        super.dispose();
        binder.unbind();
    }

    /**
     * Creates a binder for the property.
     *
     * @param property the property
     * @return a new binder
     */
    private SelectFieldBinder createBinder(Property property) {
        return new SelectFieldBinder(this, property) {
            /**
             * Updates the property from the field.
             *
             * @param property the property to update
             * @return {@code true} if the property was updated
             */
            @Override
            protected boolean setProperty(Property property) {
                PrinterReference fieldValue = (PrinterReference) getFieldValue();
                String value = (fieldValue != null) ? fieldValue.toString() : null;
                boolean result = property.setValue(value);
                if (result) {
                    PrinterReference propertyValue = PrinterReference.fromString(property.getString());
                    if (!Objects.equals(fieldValue, propertyValue)) {
                        setField();
                    }
                }
                return result;
            }

            /**
             * Returns the index of a value in the list.
             *
             * @param value the value
             * @return the index, or {@code -1} if the value wasn't found
             */
            @Override
            protected int indexOf(Object value) {
                if (value instanceof String) {
                    value = PrinterReference.fromString((String) value);
                }
                return super.indexOf(value);
            }
        };
    }
}
