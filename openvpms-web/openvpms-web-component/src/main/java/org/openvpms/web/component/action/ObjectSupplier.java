/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;

import java.util.function.Supplier;

/**
 * Resolves an object, just prior to its use.
 *
 * @author Tim Anderson
 */
public interface ObjectSupplier<T extends IMObject> extends Supplier<T> {

    /**
     * Gets the object.
     *
     * @return the object, or {@code null} if it cannot be retrieved
     */
    @Override
    T get();

    /**
     * Returns the archetype.
     *
     * @return the archetype
     */
    String getArchetype();

    /**
     * Creates a new object supplier.
     *
     * @param archetype the archetype that the supplier returns, for error reporting purposes
     * @param supplier  the supplier to delegate to
     * @return a new object supplier
     */
    static <T extends IMObject> ObjectSupplier<T> create(String archetype, Supplier<T> supplier) {
        return new ObjectSupplier<T>() {
            @Override
            public T get() {
                return supplier.get();
            }

            @Override
            public String getArchetype() {
                return archetype;
            }
        };
    }
}
