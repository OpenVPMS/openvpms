/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import nextapp.echo2.app.TextField;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.combo.ComboBox;
import org.openvpms.web.echo.event.ActionListener;

import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Helper to bind an IMObject property to a combo box.
 *
 * @author Tim Anderson
 */
public abstract class ComboBoxBinder extends Binder {

    /**
     * The combo box.
     */
    private final ComboBox component;

    /**
     * Listener for actions on the field.
     */
    private final ActionListener actionListener;

    /**
     * Listener for text field updates.
     */
    private final PropertyChangeListener listener;


    /**
     * Constructs an {@link ComboBoxBinder}.
     *
     * @param property  the property to bind
     * @param component the combo box
     */
    public ComboBoxBinder(Property property, ComboBox component) {
        this(property, component, true);
    }

    /**
     * Constructs an {@link ComboBoxBinder}.
     *
     * @param property  the property to bind
     * @param component the combo box
     * @param bind      if {@code true} bind the property
     */
    public ComboBoxBinder(Property property, ComboBox component, boolean bind) {
        super(property, false);
        this.component = component;
        listener = evt -> setProperty();
        actionListener = new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
            }
        };

        if (bind) {
            bind();
        }
    }

    /**
     * Registers the binder with the property to receive updates.
     */
    @Override
    public void bind() {
        super.bind();
        component.getTextField().addPropertyChangeListener(TextField.TEXT_CHANGED_PROPERTY, listener);
        component.addActionListener(actionListener);
    }

    /**
     * Deregisters the binder from the property.
     */
    @Override
    public void unbind() {
        super.unbind();
        component.getTextField().removePropertyChangeListener(TextField.TEXT_CHANGED_PROPERTY, listener);
        component.removeActionListener(actionListener);
    }

    /**
     * Updates the property from the field.
     *
     * @param property the property to update
     * @return {@code true} if the property was updated
     */
    @Override
    protected boolean setProperty(Property property) {
        boolean result;
        String text = getFieldValue();
        Object object = getObject(text);
        result = updateProperty(property, object);
        if (result) {
            object = property.getValue();
            String newText = getText(object);
            if (newText != null) {
                // if it's null, the object may be a placeholder
                updateField(newText);
            }
        }
        return result;
    }

    /**
     * Updates the property with an object.
     *
     * @param property the property
     * @param object   the object. May be {@code null}
     * @return {@code true} if the value was set, {@code false} if it
     * cannot be set due to error, or is the same as the existing value
     */
    protected boolean updateProperty(Property property, Object object) {
        return property.setValue(object);
    }

    /**
     * Returns the value of the field.
     *
     * @return the value of the field
     */
    @Override
    protected String getFieldValue() {
        return component.getTextField().getText();
    }

    /**
     * Sets the value of the field.
     *
     * @param value the value to set
     */
    @Override
    protected void setFieldValue(Object value) {
        updateField(getText(value));
    }

    /**
     * Returns the object matching the specified text.
     *
     * @param text the text
     * @return the object, or {@code null} if there is no match
     */
    protected abstract Object getObject(String text);

    /**
     * Returns the text for the specified property value.
     *
     * @param value the property value
     * @return the corresponding text, or {@code null}
     */
    protected abstract String getText(Object value);

    /**
     * Updates the field.
     *
     * @param newText the new text
     */
    private void updateField(String newText) {
        TextField field = component.getTextField();
        if (!Objects.equals(field.getText(), newText)) {
            field.setText(newText);
        }
    }
}
