/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.ContentPane;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.WindowPane;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import nextapp.echo2.app.layout.GridLayoutData;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.edit.AlertListener.Category;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Manages the display of alerts.
 *
 * @author Tim Anderson
 */
public class AlertManager {

    /**
     * The container.
     */
    private final Component container;

    /**
     * The listener to handle alert messages.
     */
    private final AlertListener listener;

    /**
     * The maximum number of alerts to display.
     */
    private final int maxAlerts;

    /**
     * The current alerts.
     */
    private final List<Alert> alerts = new ArrayList<>();

    /**
     * The number of alerts added over time.
     */
    private int alertCount;

    /**
     * The default offset for alerts.
     */
    private static final int OFFSET = 5;

    /**
     * Constructs an {@link AlertManager}.
     *
     * @param container the container used to locate the parent for alerts; Alerts will be registered on the nearest
     *                  parent ContentPane
     */
    public AlertManager(Component container, int maxAlerts) {
        this.container = container;
        this.maxAlerts = maxAlerts;
        listener = new AlertListener() {
            @Override
            public String onAlert(String message) {
                return show(message);
            }

            @Override
            public void onAlert(String id, String message) {
                show(id, message);
            }

            @Override
            public String onAlert(String message, Category category) {
                return show(message, category);
            }

            @Override
            public void onAlert(String id, String message, Category category) {
                show(id, message, category);
            }

            @Override
            public void cancel(String id) {
                cancelAlert(id);
            }
        };
    }

    /**
     * Returns the alert listener.
     *
     * @return the alert listener
     */
    public AlertListener getListener() {
        return listener;
    }

    /**
     * Clear any alerts.
     */
    public void clear() {
        for (Alert alert : alerts.toArray(new Alert[0])) {
            alert.userClose();
        }
    }

    /**
     * Displays an alert.
     *
     * @param message the alert message
     * @return a handle to cancel the alert
     */
    public String show(String message) {
        return show(message, Category.INFO);
    }

    /**
     * Displays an alert.
     *
     * @param message  the alert message
     * @param category the alert category
     * @return a handle to cancel the alert
     */
    public String show(String message, Category category) {
        String id = UUID.randomUUID().toString();
        show(id, message, category);
        return id;
    }

    /**
     * Displays an alert.
     *
     * @param id      the identifier to associate with the message, for subsequent cancellation
     * @param message the alert message
     */
    public void show(String id, String message) {
        show(id, message, Category.INFO);
    }

    /**
     * Displays an alert.
     *
     * @param id       the identifier to associate with the message, for subsequent cancellation
     * @param message  the alert message
     * @param category the alert category
     */
    public void show(String id, String message, Category category) {
        if (alerts.size() < maxAlerts) {
            Alert alert = new Alert(id, message, category);
            int y = OFFSET;
            for (Alert existing : alerts) {
                y = getNextY(existing);
            }
            alert.setPositionY(new Extent(y));
            getContentPane().add(alert);
            alert.addWindowPaneListener(new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    remove(alert);
                }
            });
            alerts.add(alert);
            alertCount++;
        }
    }

    /**
     * Returns the number of alerts added over time.
     * <p/>
     * This can be used to detect if new alerts have been raised since last checking.
     *
     * @return the alert count
     */
    public int getAlertCount() {
        return alertCount;
    }

    /**
     * Returns the content pane for alerts.
     *
     * @return the content pane
     */
    protected Component getContentPane() {
        Component component = container;
        while (component != null && !(component instanceof ContentPane)) {
            component = component.getParent();
        }
        if (component == null) {
            component = ApplicationInstance.getActive().getDefaultWindow().getContent();
        }
        return component;
    }

    /**
     * Invoked to cancel an alert.
     *
     * @param id the alert identifier
     */
    protected void cancelAlert(String id) {
        for (Alert alert : alerts.toArray(new Alert[0])) {
            if (Objects.equals(alert.id, id)) {
                alert.userClose();
            }
        }
    }

    /**
     * Returns the Y position of the next alert.
     *
     * @param alert the alert to position relative to
     * @return the next Y position
     */
    private int getNextY(WindowPane alert) {
        Extent position = alert.getPositionY();
        int y = (position != null) ? position.getValue() : OFFSET;
        Extent height = alert.getHeight();
        if (height != null) {
            y += height.getValue() + OFFSET;
        } else {
            y += OFFSET;
        }
        return y;
    }

    /**
     * Removes an alert and repositions the remaining alerts.
     *
     * @param alert the alert to remove
     */
    private void remove(Alert alert) {
        alerts.remove(alert);
        shuffle();
    }

    /**
     * Adjusts the positions of the alerts.
     */
    private void shuffle() {
        int y = OFFSET;
        for (WindowPane alert : alerts) {
            alert.setPositionY(new Extent(y));
            y = getNextY(alert);
        }
    }

    private static class Message extends WindowPane {
        Message(String message, String styleName) {
            setStyleName(styleName);
            setClosable(false);
            setPositionX(new Extent(OFFSET));
            setPositionY(new Extent(OFFSET));
            int fontSize = StyleSheetHelper.getProperty("font.size", 10);
            Extent height = getHeight(message, fontSize);
            setHeight(height);
            setMinimumHeight(height);
            Label label = LabelFactory.create(true, true);
            label.setStyleName(styleName);
            label.setText(message);
            Button button = ButtonFactory.create(null, "Message.close");
            button.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    userClose();
                }
            });

            GridLayoutData layoutData = new GridLayoutData();
            label.setLayoutData(layoutData);
            Grid grid = GridFactory.create(2, label, button);
            grid.setColumnWidth(0, Styles.FULL_WIDTH);
            grid.setWidth(Styles.FULL_WIDTH);
            grid.setHeight(Styles.FULL_HEIGHT);
            add(grid);
        }

        /**
         * Returns the height of the alert.
         *
         * @param message  the message
         * @param fontSize the font size
         * @return the height
         */
        private Extent getHeight(String message, int fontSize) {
            int lines = StringUtils.countMatches(message, "\n");
            if (lines == 0) {
                // no lines, so calculate lines based on message length
                lines = (message.length() / 60);
            }
            lines += 3; // at least one line + 2 for padding
            if (lines > 10) {
                lines = 10;
            }
            return new Extent(fontSize * lines);
        }
    }

    private static class Alert extends Message {

        private final String id;

        public Alert(String id, String message, Category category) {
            super(message, category == Category.INFO ? "InformationMessage" : "ErrorMessage");
            this.id = id;
        }
    }

}
