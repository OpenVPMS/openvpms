/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A {@link MarkablePagedIMObjectTableModel} where the marked objects are stored in a map.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMapBasedMarkablePagedIMTableModel<K, V> extends AbstractMarkablePagedIMTableModel<V> {

    /**
     * The marked objects, in the order they were selected.
     */
    private final Map<K, V> marked;

    /**
     * Constructs an {@link AbstractMapBasedMarkablePagedIMTableModel}.
     *
     * @param model the underlying table model
     */
    public AbstractMapBasedMarkablePagedIMTableModel(IMTableModel<V> model) {
        this(model, new LinkedHashMap<>());
    }

    /**
     * Constructs an {@link AbstractMapBasedMarkablePagedIMTableModel}.
     *
     * @param model the underlying table model
     * @param map   the map to use
     */
    public AbstractMapBasedMarkablePagedIMTableModel(IMTableModel<V> model, Map<K, V> map) {
        super(model);
        this.marked = map;
    }

    /**
     * Returns the marked resources.
     *
     * @return the marked resources
     */
    public Collection<V> getMarked() {
        return marked.values();
    }

    /**
     * Determines if at least one object is marked.
     *
     * @return {@code true} if one or more objects are marked.
     */
    @Override
    public boolean isMarked() {
        return !marked.isEmpty();
    }

    /**
     * Determines if an object is marked.
     *
     * @param object the object
     * @return {@code true if the object is marked}
     */
    protected boolean isMarked(V object) {
        return marked.containsKey(getKey(object));
    }

    /**
     * Clear all marks.
     */
    @Override
    protected void clearMarks() {
        marked.clear();
    }

    /**
     * Flags an object as marked/unmarked
     *
     * @param object the object
     * @param mark   if {@code true}, flag it is marked, else flag it as unmarked
     */
    @Override
    protected void mark(V object, boolean mark) {
        K key = getKey(object);
        if (mark) {
            marked.put(key, object);
        } else {
            marked.remove(key);
        }
    }

    /**
     * Returns the key for a value.
     *
     * @param value the value
     * @return the corresponding key
     */
    protected abstract K getKey(V value);
}
