/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.table;

import nextapp.echo2.app.event.TableModelEvent;
import nextapp.echo2.app.event.TableModelListener;
import nextapp.echo2.app.list.ListSelectionModel;
import nextapp.echo2.app.table.TableModel;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.EvenOddTableCellRenderer;
import org.openvpms.web.echo.table.KeyTable;

import java.util.List;


/**
 * Table for domain objects.
 *
 * @author Tim Anderson
 */
public class IMTable<T> extends KeyTable {

    /**
     * The listener.
     */
    private final TableModelListener modelListener;

    /**
     * Constructs an {@link IMTable}.
     *
     * @param model the table model
     */
    public IMTable(IMTableModel<T> model) {
        modelListener = event -> {
            if (event.getType() == TableModelEvent.STRUCTURE_CHANGED) {
                initialise(model);
            } else if (event.getFirstRow() != event.getLastRow()) {
                // clear any selection
                setSelected(null);
            }
        };

        setStyleName(Styles.DEFAULT);
        setAutoCreateColumnsFromModel(false);
        setModel(model);   // NOTE: need to set the model prior to initialise() to replace DefaultTableModel

        model.addTableModelListener(modelListener);
        initialise(model);
    }

    /**
     * Sets the objects to display in the table.
     *
     * @param objects the objects to display
     */
    public void setObjects(List<T> objects) {
        getModel().setObjects(objects);
    }

    /**
     * Returns the objects displayed in the table.
     *
     * @return the object being displayed.
     */
    public List<T> getObjects() {
        return getModel().getObjects();
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object, or {@code null} if no object is selected
     */
    public T getSelected() {
        T result = null;
        int index = getSelectionModel().getMinSelectedIndex();
        if (index != -1) {
            List<T> objects = getModel().getObjects();
            if (index < objects.size()) {
                result = objects.get(index);
            }
        }
        return result;
    }

    /**
     * Sets the selected object.
     * <p/>
     * If the object doesn't exist in the table, any existing selection will be cleared.
     *
     * @param object the object to select. May be {@code null} to deselect any selection
     * @return {@code true} if the object was selected, {@code false} if it doesn't exist
     */
    public boolean setSelected(T object) {
        boolean result = false;
        int index = getObjects().indexOf(object);
        ListSelectionModel selection = getSelectionModel();
        if (index != -1) {
            selection.setSelectedIndex(index, true);
            result = true;
        } else {
            selection.clearSelection();
        }
        return result;
    }

    /**
     * Deselects an object if it is selected.
     *
     * @param object the object to deselect. May be {@code null} to deselect any selection
     * @return {@code true} if the object was selected, otherwise {@code false}
     */
    public boolean deselect(T object) {
        boolean result = false;
        int index = getObjects().indexOf(object);
        ListSelectionModel selection = getSelectionModel();
        if (index != -1) {
            selection.setSelectedIndex(index, false);
            result = true;
        }
        return result;
    }

    /**
     * Returns the model.
     *
     * @return the model
     */
    @SuppressWarnings("unchecked")
    public IMTableModel<T> getModel() {
        return (IMTableModel<T>) super.getModel();
    }

    /**
     * Sets the {@code TableModel} being visualized.
     *
     * @param model the new model (may not be null)
     */
    public void setModel(IMTableModel<T> model) {
        super.setModel(model);
    }

    /**
     * Initialises this.
     *
     * @param model the table model
     */
    @SuppressWarnings("unchecked")
    private void initialise(IMTableModel<T> model) {
        setSelectionEnabled(model.getEnableSelection());
        setRolloverEnabled(model.getEnableSelection());
        TableModel current = getModel();
        setModel(model);
        setColumnModel(model.getColumnModel());
        if (getDefaultRenderer(Object.class) == null) {
            setDefaultRenderer(Object.class, EvenOddTableCellRenderer.INSTANCE);
        }
        if (current != model) {
            // need to add a listener to the model to be notified of column changes
            model.addTableModelListener(modelListener);
        }
    }

}
