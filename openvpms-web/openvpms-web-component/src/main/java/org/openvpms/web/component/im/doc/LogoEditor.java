/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.layout.LayoutContext;

/**
 * An editor for <em>participation.logo</em> participation relationships.
 *
 * @author Tim Anderson
 */
public class LogoEditor extends AbstractImageParticipationEditor {

    /**
     * Constructs a {@link LogoEditor}.
     *
     * @param act     the logo act
     * @param parent  the parent entity
     * @param context the layout context
     */
    public LogoEditor(DocumentAct act, Entity parent, LayoutContext context) {
        super(act, parent, DocumentArchetypes.LOGO_ACT, "owner", context);
    }

}
