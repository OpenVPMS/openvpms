/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Builds a call for an {@link ActionBuilder} to supply a list of objects.
 *
 * @author Tim Anderson
 */
public class ListCallBuilder<T extends IMObject> extends CallBuilder<ListCallBuilder<T>> {

    /**
     * The objects.
     */
    private List<T> objects;

    /**
     * Constructs a {@link ListCallBuilder}.
     *
     * @param parent    the parent builder
     * @param behaviour the initial behaviour
     */
    public ListCallBuilder(ActionBuilder parent, Behaviour behaviour) {
        super(parent, behaviour);
    }

    /**
     * Sets the objects to supply to the call.
     *
     * @param objects the objects
     * @return this
     */
    public ListCallBuilder<T> withObjects(List<T> objects) {
        this.objects = objects;
        return behaviour().useOriginalObject().update();
    }

    /**
     * Sets the call to invoke.
     *
     * @param consumer the consumer
     * @return the parent builder
     */
    public ActionBuilder call(Consumer<List<T>> consumer) {
        return call(new SyncConsumer<>(consumer), false);
    }

    /**
     * Sets the call to invoke.
     * <p/>
     * This is responsible for notifying the listener when the call completes.
     *
     * @param consumer the consumer
     * @return the parent builder
     */
    public ActionBuilder call(BiConsumer<List<T>, Consumer<ActionStatus>> consumer) {
        return call(consumer, true);
    }

    /**
     * Sets the call to invoke.
     *
     * @param consumer the consumer
     * @param async    determines if the action runs asynchronously or synchronously
     * @return the parent builder
     */
    private ActionBuilder call(BiConsumer<List<T>, Consumer<ActionStatus>> consumer, boolean async) {
        if (objects == null) {
            throw new IllegalStateException("No objects specified");
        }
        return getParent().action(objects, getBehaviour(), async, consumer);
    }
}