/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Extent;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;

/**
 * A layout strategy for products.
 * <p/>
 * This excludes the locations node, if products aren't being filtered by location.
 *
 * @author Tim Anderson
 */
public class ProductLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Default width for text areas.
     */
    private static final Extent DEFAULT_TEXT_AREA_WIDTH = new Extent(80, Extent.EX);

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        boolean useLocationProducts = ProductHelper.useLocationProducts(context.getContext());
        ArchetypeNodes nodes = new ArchetypeNodes();
        if (!useLocationProducts) {
            nodes.exclude("locations");
        }
        setArchetypeNodes(nodes);
        addMultiLineText("printedDescription", properties, new Extent(50, Extent.EX), context);
        // render the same width as printedName etc
        return super.apply(object, properties, parent, context);
    }

    /**
     * Adds a multi-line text field for a property.
     *
     * @param name the property name
     * @param properties the properties
     * @param context  the layout context
     */
    protected void addMultiLineText(String name, PropertySet properties, LayoutContext context) {
        addMultiLineText(name, properties, DEFAULT_TEXT_AREA_WIDTH, context);
    }


    /**
     * Adds a multi-line text field for a property.
     *
     * @param name the property name
     * @param properties the properties
     * @param context  the layout context
     */
    protected void addMultiLineText(String name, PropertySet properties, Extent width, LayoutContext context) {
        Property property = properties.get(name);
        if (property != null) {
            addComponent(createMultiLineText(property, 2, 5, width, context));
        }
    }
}
