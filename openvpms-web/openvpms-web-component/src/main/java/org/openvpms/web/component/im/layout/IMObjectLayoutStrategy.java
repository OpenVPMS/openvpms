/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.layout;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.PropertySet;


/**
 * SaveStrategy for laying out an {@link IMObject} in a {@code Component}.
 *
 * @author Tim Anderson
 */
public interface IMObjectLayoutStrategy {

    /**
     * Pre-registers a component for inclusion in the layout.
     * <p/>
     * The component must be associated with a property.
     *
     * @param state the component state
     * @throws IllegalStateException if the component isn't associated with a property
     */
    void addComponent(ComponentState state);

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to
     * create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context);

}
