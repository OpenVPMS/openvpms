/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.property.ModifiableListener;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Monitors {@link ParticipationCollectionEditor} editors for changes to the entity.
 * <p/>
 * This only supports those editors with a maximum cardinality of 1.
 * <p/>
 * This exists to support monitoring of entity changes when the child {@link ParticipationEditor} can be destroyed
 * and recreated for optional participation relationships. This prevents listeners being registered directly,
 * and registering a listener on the underlying collection generates too many events.
 *
 * @author Tim Anderson
 */
public class ParticipationMonitor<T extends Entity> {

    /**
     * The listener.
     */
    private final ModifiableListener modifiableListener;

    /**
     * The collection editor.
     */
    private final ParticipationCollectionEditor collectionEditor;

    /**
     * The listener to notify.
     */
    private Consumer<T> listener;

    /**
     * The current entity reference, used to detect changes.
     */
    private Reference entity;

    /**
     * Constructs a {@link ParticipationMonitor}.
     *
     * @param collectionEditor the collection editor to monitor
     * @param listener         the listener to notify when the entity changes
     */
    ParticipationMonitor(ParticipationCollectionEditor collectionEditor, Consumer<T> listener) {
        if (collectionEditor.getCollection().getMaxCardinality() > 1) {
            throw new IllegalStateException("Cannot monitor collections with cardinality > 1");
        }
        this.collectionEditor = collectionEditor;
        modifiableListener = modifiable -> onChanged();
        this.collectionEditor.addModifiableListener(modifiableListener);
        this.listener = listener;
        Entity current = getEntity();
        entity = (current != null) ? current.getObjectReference() : null;
    }

    /**
     * Disposes of the monitor.
     */
    public void dispose() {
        if (collectionEditor != null) {
            collectionEditor.removeModifiableListener(modifiableListener);
            listener = null;
        }
    }

    /**
     * Invoked when the collection changes.
     * <p/>
     * This notifies the registered listener if the change is to the entity.
     */
    private void onChanged() {
        T current = getEntity();
        Reference currentRef = (current != null) ? current.getObjectReference() : null;
        if (!Objects.equals(entity, currentRef)) {
            entity = currentRef;
            listener.accept(current);
        }
    }

    /**
     * Returns the current entity.
     *
     * @return the current entity. May be {@code null}
     */
    private T getEntity() {
        ParticipationEditor<T> editor = getEditor();
        return editor != null ? editor.getEntity() : null;
    }

    /**
     * Returns the {@link ParticipationEditor}.
     *
     * @return the editor, or {@code null} if no object is being edited
     */
    @SuppressWarnings("unchecked")
    private ParticipationEditor<T> getEditor() {
        IMObjectEditor result = collectionEditor.getCurrentEditor();
        if (result == null) {
            Collection<IMObject> objects = collectionEditor.getCurrentObjects();
            if (!objects.isEmpty()) {
                result = collectionEditor.getEditor(objects.iterator().next());
            }
        }
        return (result instanceof ParticipationEditor) ? (ParticipationEditor<T>) result : null;
    }
}
