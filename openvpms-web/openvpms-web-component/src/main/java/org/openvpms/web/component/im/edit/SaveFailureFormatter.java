/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Formats the error when an {@link IMObjectEditor} save operation fails.
 *
 * @author Tim Anderson
 */
public class SaveFailureFormatter implements AbstractIMObjectEditorOperation.FailureFormatter {

    /**
     * Returns the error title, when displaying a failure.
     *
     * @param editor the editor
     * @return the title
     */
    @Override
    public String getTitle(IMObjectEditor editor) {
        return Messages.format("imobject.save.failed", editor.getDisplayName());
    }

    /**
     * Returns a user-displayable message, describing the reason for a failure,
     * after the editor has been successfully reloaded.
     *
     * @param editor    the editor
     * @param exception the reason for the failure
     * @return the message
     */
    @Override
    public String getMessage(IMObjectEditor editor, Throwable exception) {
        String message = ErrorFormatter.format(exception, editor.getDisplayName());
        if (!message.endsWith(".")) {
            message += ".";
        }
        return Messages.format("imobject.save.reverted", message);
    }
}