/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A {@link DocumentTemplateLocator} that locates {@link DocumentTemplate}s based on their
 * <em>lookup.documentTemplateType</em>.
 *
 * @author Tim Anderson
 */
public abstract class TypeBasedDocumentTemplateLocator implements DocumentTemplateLocator {

    /**
     * The <em>lookup.documentTemplateType</em> code.
     */
    private final String type;

    /**
     * The template helper.
     */
    private final TemplateHelper helper;

    /**
     * Constructs a {@link TypeBasedDocumentTemplateLocator}.
     *
     * @param type    the <em>lookup.documentTemplateType</em> code
     * @param service the archetype service
     */
    public TypeBasedDocumentTemplateLocator(String type, ArchetypeService service) {
        this.type = type;
        helper = new TemplateHelper(service);
    }

    /**
     * Returns the document template.
     *
     * @return the document template, or {@code null} if the template cannot be located
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public DocumentTemplate getTemplate() {
        return helper.getDocumentTemplate(type);
    }

    /**
     * Returns the type that the template applies to.
     *
     * @return the type. Corresponds to a <em>lookup.documentTemplateType</em> code
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * Returns the document template for the specified type, associated with an organisation location or practice.
     * <p/>
     * If there are multiple relationships, the lowest id will be returned.
     *
     * @param organisation the organisation
     * @return the template corresponding to the type or {@code null} if none can be found
     */
    protected DocumentTemplate getTemplate(Party organisation) {
        return helper.getDocumentTemplate(type, organisation);
    }
}