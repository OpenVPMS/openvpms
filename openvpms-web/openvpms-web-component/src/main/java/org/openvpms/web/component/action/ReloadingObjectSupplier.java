/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.function.Supplier;

/**
 * An {@link ObjectSupplier} that reloads the object from the archetype service after the first access.
 */
class ReloadingObjectSupplier<T extends IMObject> implements ObjectSupplier<T> {

    /**
     * The archetype.
     */
    private final String archetype;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The object supplier.
     */
    private final Supplier<T> supplier;

    /**
     * Determines if this is the first time accessing the object.
     */
    private boolean first;

    /**
     * Constructs a {@link ReloadingObjectSupplier}.
     *
     * @param archetype     the archetype
     * @param service       the archetype service
     * @param supplier      the object supplier
     * @param reloadOnFirst determines if the object should be loaded on first access
     */
    public ReloadingObjectSupplier(String archetype, ArchetypeService service, Supplier<T> supplier,
                                   boolean reloadOnFirst) {
        this.archetype = archetype;
        this.service = service;
        this.supplier = supplier;
        this.first = !reloadOnFirst;
    }

    /**
     * Gets the object.
     *
     * @return the object, or {@code null} if it cannot be retrieved
     */
    @Override
    @SuppressWarnings("unchecked")
    public T get() {
        T result = supplier.get();
        if (first) {
            first = false;
        } else if (result != null) {
            result = (T) service.get(result.getObjectReference(), result.getClass());
        }
        return result;
    }

    /**
     * Returns the archetype.
     *
     * @return the archetype
     */
    @Override
    public String getArchetype() {
        return archetype;
    }
}