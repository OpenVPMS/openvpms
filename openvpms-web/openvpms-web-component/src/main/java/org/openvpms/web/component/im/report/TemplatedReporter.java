/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.LookupHelper;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.ReportException;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.web.component.im.doc.FileNameFormatter;

import java.util.Iterator;
import java.util.Map;


/**
 * Base class for implementations that generate {@link Document}s using a template.
 *
 * @author Tim Anderson
 */
public abstract class TemplatedReporter<T> extends Reporter<T> {

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The document template entity to use. May be {@code null}
     */
    private DocumentTemplate template;

    /**
     * The document template locator.
     */
    private DocumentTemplateLocator locator;

    /**
     * Cache of template names, keyed on template archetype short name.
     */
    private Map<String, String> templateNames;


    /**
     * Constructs a {@link TemplatedReporter} for a single object.
     *
     * @param object    the object
     * @param template  the document template to use
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    public TemplatedReporter(T object, DocumentTemplate template, FileNameFormatter formatter,
                             ArchetypeService service, LookupService lookups) {
        super(object);
        this.template = template;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Constructs a {@link TemplatedReporter} for a single object.
     *
     * @param object    the object
     * @param locator   the document template locator
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    public TemplatedReporter(T object, DocumentTemplateLocator locator, FileNameFormatter formatter,
                             ArchetypeService service, LookupService lookups) {
        super(object);
        this.locator = locator;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Constructs a {@link TemplatedReporter} for a collection of objects.
     *
     * @param objects   the objects
     * @param template  the document template to use
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    public TemplatedReporter(Iterable<T> objects, DocumentTemplate template, FileNameFormatter formatter,
                             ArchetypeService service, LookupService lookups) {
        super(objects);
        this.template = template;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Constructs a {@link TemplatedReporter} for a collection of objects.
     *
     * @param objects   the objects
     * @param locator   the document template locator
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    public TemplatedReporter(Iterable<T> objects, DocumentTemplateLocator locator, FileNameFormatter formatter,
                             ArchetypeService service, LookupService lookups) {
        super(objects);
        this.locator = locator;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
    }

    /**
     * Returns the type that the template applies to.
     *
     * @return the type. Corresponds to an <em>lookup.documentTemplateType</em> code
     */
    public String getType() {
        return locator.getType();
    }

    /**
     * Returns a display name for the objects being reported on.
     *
     * @return a display name for the objects being printed
     */
    public String getDisplayName() {
        String result = null;
        DocumentTemplate template = getTemplate();
        if (template != null) {
            result = template.getName();
        }
        if (StringUtils.isEmpty(result)) {
            if (templateNames == null) {
                templateNames = LookupHelper.getNames(lookups, DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE);
            }
            String shortName = getType();
            result = templateNames.get(shortName);
            if (result == null) {
                result = DescriptorHelper.getDisplayName(shortName, service);
            }
        }
        return result;
    }

    /**
     * Returns the document template.
     *
     * @return the document template, or {@code null} if none can be found
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentTemplate getTemplate() {
        if (template == null && locator != null) {
            template = locator.getTemplate();
        }
        return template;
    }

    /**
     * Returns the template, throwing an exception if none exists.
     *
     * @return the template
     * @throws ReportException if no template exists
     */
    protected DocumentTemplate resolveTemplate() {
        DocumentTemplate template = getTemplate();
        if (template == null) {
            String type = getType();
            Lookup lookup = lookups.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, type);
            String name = lookup != null ? lookup.getName() : type;
            throw new ReportException(ReportMessages.noTemplateForType(name));
        }
        return template;
    }

    /**
     * Registers the document template.
     *
     * @param template the template to use. May be {@code null}
     */
    protected void setTemplate(DocumentTemplate template) {
        this.template = template;
    }

    /**
     * Updates the document name.
     *
     * @param document the document to update
     */
    @Override
    protected void setName(Document document) {
        DocumentTemplate template = getTemplate();
        if (template != null) {
            Object value = getObject();
            if (value == null) {
                Iterator<T> iterator = getObjects().iterator();
                if (iterator.hasNext()) {
                    // use the first object in the list to format the file name
                    value = iterator.next();
                }
            }
            if (value instanceof IMObject) {
                IMObject object = (IMObject) value;
                String fileName = formatter.format(template.getName(), object, template);
                String extension = FilenameUtils.getExtension(document.getName());
                document.setName(fileName + "." + extension);
            }
            super.setName(document);
        }
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

}
