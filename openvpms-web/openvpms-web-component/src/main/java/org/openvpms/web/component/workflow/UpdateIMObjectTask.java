/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.object.IMObject;


/**
 * Task to update an {@link IMObject} held in the {@link TaskContext}.
 *
 * @author Tim Anderson
 */
public class UpdateIMObjectTask<T extends IMObject> extends AbstractUpdateIMObjectTask<T> {

    /**
     * Constructs an {@link UpdateIMObjectTask}.
     * The object is saved on update.
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with
     */
    public UpdateIMObjectTask(String archetype, TaskProperties properties) {
        this(archetype, properties, true);
    }

    /**
     * Constructs an {@link UpdateIMObjectTask}.
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with
     * @param save       determines if the object should be saved
     */
    public UpdateIMObjectTask(String archetype, TaskProperties properties, boolean save) {
        this(archetype, properties, save, save);
    }

    /**
     * Constructs an {@link UpdateIMObjectTask}.
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with
     * @param save       determines if the object should be saved
     * @param retry      determines if the update should be retried if it fails
     */
    public UpdateIMObjectTask(String archetype, TaskProperties properties, boolean save, boolean retry) {
        super(archetype, properties, save, retry);
    }

}
