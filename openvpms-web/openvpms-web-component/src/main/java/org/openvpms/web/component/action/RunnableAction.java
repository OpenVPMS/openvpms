/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.apache.commons.lang.mutable.MutableBoolean;

import java.util.function.Consumer;

/**
 * An action that delegates to a {@link Runnable}.
 */
class RunnableAction implements Action {

    /**
     * The action to run.
     */
    private final Runnable action;

    /**
     * Determines if the action can be retried.
     * This is a mutable so actions can be created prior to the final interactive status being known.
     */
    private final boolean retryable;

    /**
     * Determines if this can run interactively.
     */
    private final MutableBoolean interactive;

    /**
     * Constructs a {@link RunnableAction}.
     *
     * @param action      the action to run
     * @param interactive determines if this can run interactively
     * @param retryable   determines if this can be retried
     */
    public RunnableAction(Runnable action, MutableBoolean interactive, boolean retryable) {
        this.action = action;
        this.interactive = interactive;
        this.retryable = retryable;
    }

    /**
     * Runs the action.
     */
    @Override
    public void run() {
        run(new DefaultActionStatusConsumer(interactive.booleanValue()));
    }

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    @Override
    public void run(Consumer<ActionStatus> listener) {
        try {
            action.run();
            listener.accept(ActionStatus.success());
        } catch (Throwable exception) {
            if (retryable) {
                listener.accept(ActionStatus.retry(exception));
            } else {
                listener.accept(ActionStatus.failed(exception));
            }
        }
    }
}
