/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.act.Act;

/**
 * Task to update the status of an {@link Act} held in the {@link TaskContext}.
 *
 * @author Tim Anderson
 */
public class UpdateActStatusTask extends AbstractUpdateActTask<Act> {

    /**
     * The status to set.
     */
    private final String status;

    /**
     * Constructs an {@link UpdateActStatusTask}.
     * <p/>
     * The object is saved on update.
     *
     * @param archetype the archetype of the object to update
     * @param status    the status to update the act to
     */
    public UpdateActStatusTask(String archetype, String status) {
        super(archetype, true);
        this.status = status;
        skipWhenStatusIn(status);
    }

    /**
     * Populates an object.
     *
     * @param object  the object to populate
     * @param context the task context
     */
    @Override
    protected void populate(Act object, TaskContext context) {
        object.setStatus(status);
    }
}