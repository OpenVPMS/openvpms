/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.apache.commons.lang3.mutable.MutableObject;
import org.openvpms.archetype.rules.doc.LongTextReader;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.system.ServiceHelper;

/**
 * A read-only text property where long text may be read from a related document.
 *
 * @author Tim Anderson
 */
public class DocumentBackedTextProperty extends DelegatingProperty {

    /**
     * The parent object.
     */
    private final IMObject object;

    /**
     * The document act relationship node, or {@code null} if the parent object is a {@link DocumentAct} that
     * holds the document.
     */
    private final String relationshipNode;

    /**
     * The cached text, if it was loaded from a document.
     */
    private MutableObject<String> cached;

    /**
     * Constructs a {@link DocumentBackedTextProperty}.
     * <p/>
     * Use this when the document associated with the object holds the long text.
     *
     * @param object   the parent object where the document is used to store the long text
     * @param property the text property
     */
    public DocumentBackedTextProperty(DocumentAct object, Property property) {
        this(object, property, null);
    }

    /**
     * Constructs a {@link DocumentBackedTextProperty}
     * <p/>
     * Use this when a {@link DocumentAct} linked to by the node holds the long text.
     *
     * @param object           the parent object
     * @param property         the text property
     * @param relationshipNode the relationship node that refers to a {@link DocumentAct} used to store the long text
     */
    public DocumentBackedTextProperty(IMObject object, Property property, String relationshipNode) {
        super(property);
        this.object = object;
        this.relationshipNode = relationshipNode;
    }


    /**
     * Constructs a {@link DocumentBackedTextProperty}
     * <p/>
     * Use this when a {@link DocumentAct} linked to by the node holds the long text.
     *
     * @param object           the parent object
     * @param node             the text node
     * @param relationshipNode the relationship node that refers to a {@link DocumentAct} used to store the long text
     */
    public DocumentBackedTextProperty(IMObject object, String node, String relationshipNode) {
        super(getProperty(object, node));
        this.object = object;
        this.relationshipNode = relationshipNode;
    }

    /**
     * Sets the value of the property.
     * The value will only be set if it is valid, and different to the existing
     * value. If the value is set, any listeners will be notified.
     *
     * @param value the property value
     * @return {@code true} if the value was set, {@code false} if it
     * cannot be set due to error, or is the same as the existing value
     */
    @Override
    public boolean setValue(Object value) {
        return false;
    }

    /**
     * Returns the value of the property.
     *
     * @return the property value
     */
    @Override
    public Object getValue() {
        Object value = super.getValue();
        if (value == null) {
            if (cached == null) {
                cached = new MutableObject<>(loadText());
            }
            value = cached.getValue();
        }
        return value;
    }

    /**
     * Loads text from a document, if any.
     *
     * @return the text. May be {@code null}
     */
    private String loadText() {
        LongTextReader reader = new LongTextReader(ServiceHelper.getArchetypeService());
        String result = null;
        if (relationshipNode != null) {
            result = reader.getText(object, getName(), relationshipNode);
        } else {
            result = reader.getText((DocumentAct) object, getName());
        }
        return result;
    }

    /**
     * Returns the text associated with a document.
     *
     * @param act the document act
     * @return the text. May be {@code null}
     */
    private String getText(DocumentAct act) {
        Document document = IMObjectHelper.getObject(act.getDocument(), Document.class);
        return (document != null) ? TextDocumentHandler.asString(document) : null;
    }

    /**
     * Returns a {@link Property} for the specified object and node.
     *
     * @param object the object
     * @param node   the node
     * @return a new property
     */
    private static Property getProperty(IMObject object, String node) {
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(object);
        return new IMObjectProperty(object, bean.getNode(node));
    }
}
