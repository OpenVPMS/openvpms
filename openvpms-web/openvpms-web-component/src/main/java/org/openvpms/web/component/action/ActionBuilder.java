/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.apache.commons.lang.mutable.MutableBoolean;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.error.ErrorReportingDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.ErrorDialogBuilder;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * {@link Action} builder.
 *
 * @author Tim Anderson
 */
public class ActionBuilder {

    /**
     * Helper to differentiate between {@code Consumer<IMObject>} and {@code Consumer<IMObjectBean>}
     *
     * @see #asBean(Consumer)
     */
    public interface BeanConsumer extends Consumer<IMObjectBean> {

    }

    /**
     * Helper to differentiate between {@code BiConsumer<IMObject, Consumer<ActionStatus>} and
     * {@code BiConsumer<IMObjectBean, Consumer<ActionStatus>}
     */
    public interface BiBeanConsumer extends BiConsumer<IMObjectBean, Consumer<ActionStatus>> {

    }

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * Determines if error dialogs and prompts can be displayed.
     * This is a mutable so actions can be created prior to the final interactive status being known.
     */
    private final MutableBoolean interactive = new MutableBoolean(true);

    /**
     * The action to run.
     */
    private Action action;

    /**
     * The retry behaviour.
     */
    private Behaviour behaviour = Behaviour.getDefault();

    /**
     * The action to invoke on success. This is only run once.
     */
    private Runnable success;

    /**
     * The action to invoke on skip. This is only run once.
     */
    private Runnable skip;

    /**
     * The action to invoke on failure. This is only run once.
     */
    private FailureAction failure;

    /**
     * Determines if the user should be prompted to retry the action if it fails.
     */
    private boolean interactiveRetry = true;

    /**
     * Constructs an {@link ActionBuilder}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public ActionBuilder(ArchetypeService service, PlatformTransactionManager transactionManager) {
        this.service = service;
        this.transactionManager = transactionManager;
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the supplied object to pass to the call.
     *
     * @param object the object
     * @return the call builder
     */
    public <T extends IMObject> ObjectCallBuilder<T> withObject(T object) {
        return this.<T>newObjectCallBuilder().withObject(object);
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the object provided by the supplier, to pass to the call.
     *
     * @param archetype the archetype, used for error reporting if the object cannot be retrieved
     * @param supplier  the object supplier
     * @return this
     */
    public <T extends IMObject> ObjectCallBuilder<T> withObject(String archetype, Supplier<T> supplier) {
        return this.<T>newObjectCallBuilder().withObject(archetype, supplier);
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the object provided by the supplier, to pass to the call.
     *
     * @param supplier the object supplier
     * @return this
     */
    public <T extends IMObject> ObjectCallBuilder<T> withObject(ObjectSupplier<T> supplier) {
        return this.<T>newObjectCallBuilder().withObject(supplier);
    }

    /**
     * Returns a {@link ObjectCallBuilder} to that uses the supplied objects to pass to the call.
     *
     * @param objects the objects
     * @return the call builder
     */
    public <T extends IMObject> ListCallBuilder<T> withObjects(List<T> objects) {
        return this.<T>newListCallBuilder().withObjects(objects);
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the latest instance of the supplied object, to pass to the call.
     *
     * @param object the object
     * @return the call builder
     */
    public <T extends IMObject> ObjectCallBuilder<T> withLatest(T object) {
        return this.<T>newObjectCallBuilder().withObject(object)
                .useLatestInstance();
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the latest instance the object provided by the supplier,
     * to pass to the call.
     *
     * @param archetype the archetype, used for error reporting if the object cannot be retrieved
     * @param supplier  the object supplier
     * @return this
     */
    public <T extends IMObject> ObjectCallBuilder<T> withLatest(String archetype, Supplier<T> supplier) {
        return this.<T>newObjectCallBuilder().withObject(archetype, supplier)
                .useLatestInstance();
    }

    /**
     * Returns a {@link ObjectCallBuilder} that uses the latest instance the object provided by the supplier,
     * to pass to the call.
     *
     * @param supplier the object supplier
     * @return this
     */
    public <T extends IMObject> ObjectCallBuilder<T> withLatest(ObjectSupplier<T> supplier) {
        return this.<T>newObjectCallBuilder().withObject(supplier)
                .useLatestInstance();
    }

    /**
     * Returns a {@link ObjectCallBuilder} to that uses the supplied objects to pass to the call.
     *
     * @param objects the objects
     * @return the call builder
     */
    public <T extends IMObject> ListCallBuilder<T> withLatest(List<T> objects) {
        return this.<T>newListCallBuilder().withObjects(objects)
                .useLatestInstance();
    }

    /**
     * Registers a {@link Runnable} action.
     *
     * @param action the action to run
     * @return this
     */
    public ActionBuilder action(Runnable action) {
        this.action = new RunnableAction(action, interactive, true);
        return this;
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied object.
     * <p/>
     * On retry, the latest instance of the object will be used; the action will fail if it no longer exists.
     *
     * @param object the object to supply
     * @param action the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(T object, Consumer<T> action) {
        return withObject(object).useLatestInstanceOnRetry().call(action);
    }


    /**
     * Registers a {@link BiConsumer} action that takes the supplied object.
     * <p/>
     * On retry, the latest instance of the object will be used; the action will fail if it no longer exists.
     *
     * @param object the object to supply
     * @param action the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(T object, BiConsumer<T, Consumer<ActionStatus>> action) {
        return withObject(object).useLatestInstanceOnRetry().call(action);
    }

    /**
     * Registers a {@link Consumer} action that takes the object provided by an {@link Supplier}.
     * <p/>
     * On retry, the supplier will be invoked again to obtain the object and the latest instance retrieved;
     * the action will fail if it no longer exists.
     *
     * @param archetype the archetype, used for error reporting if the object cannot be retrieved
     * @param supplier  the object supplier
     * @param action    the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(String archetype, Supplier<T> supplier, Consumer<T> action) {
        return withObject(archetype, supplier).useLatestInstanceOnRetry().call(action);
    }

    /**
     * Registers a {@link Consumer} action that takes the object provided by an {@link ObjectSupplier}.
     * <p/>
     * On retry, the supplier will be invoked again to obtain the object and the latest instance retrieved;
     * the action will fail if it no longer exists.
     *
     * @param supplier the object supplier
     * @param action   the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(ObjectSupplier<T> supplier, Consumer<T> action) {
        return withObject(supplier).useLatestInstanceOnRetry().call(action);
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied object.
     * <p/>
     * The object will be wrapped in an {@link IMObjectBean}.
     * <p/>
     * On retry, the latest instance of the object will be used; the action will fail if it no longer exists.
     *
     * @param object the object to supply
     * @param action the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(T object, BeanConsumer action) {
        return withObject(object).useLatestInstanceOnRetry().asBean(action);
    }

    /**
     * Registers a {@link Consumer} action that takes the object provided by an {@link ObjectSupplier}.
     * <p/>
     * The object will be wrapped in an {@link IMObjectBean}.
     * <p/>
     * On retry, the supplier will be invoked again to obtain the object and the latest instance retrieved;
     * the action will fail if it no longer exists.
     *
     * @param supplier the object supplier
     * @param action   the action to run
     * @return this
     */
    public <T extends IMObject> ActionBuilder action(ObjectSupplier<T> supplier, BeanConsumer action) {
        return withObject(supplier).useLatestInstanceOnRetry().asBean(action);
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied objects.
     * <p/>
     * On retry, the latest instance of the objects will be used; the action will fail if one or more no longer exist
     * or have changed.
     *
     * @param objects the objects to supply
     * @param action  the action to run
     */
    public <T extends IMObject> ActionBuilder action(List<T> objects, Consumer<List<T>> action) {
        return withObjects(objects).useLatestInstanceOnRetry().call(action);
    }

    /**
     * Indicates that this action should be run interactively only.
     * <p/>
     * This disables automatic retries.
     *
     * @return this
     */
    public ActionBuilder interactiveOnly() {
        interactive.setValue(true);
        automaticRetry(0);
        return interactiveRetry(true);
    }

    /**
     * Indicates that this action should be run in the background only.
     * <p/>
     * This enables automatic retries, and disables interactive behaviour.
     *
     * @return this
     */
    public ActionBuilder backgroundOnly() {
        interactive.setValue(false);
        automaticRetry();
        return interactiveRetry(false);
    }

    /**
     * Indicates to retry the action automatically, if it fails.
     *
     * @return this
     */
    public ActionBuilder automaticRetry() {
        return automaticRetry(Behaviour.DEFAULT_RETRIES);
    }

    /**
     * Sets the maximum number of times to retry the action automatically, if it fails.
     *
     * @param times the number of times, or {@code 0} to not retry automatically. Default {@code 3}
     * @return this
     */
    public ActionBuilder automaticRetry(int times) {
        behaviour = Behaviour.newBehaviour(behaviour).retries(times).build();
        return this;
    }

    /**
     * Indicates that the action should only be run once.
     * <p/>
     * This disables retries.
     *
     * @return this
     */
    public ActionBuilder onceOnly() {
        automaticRetry(0);
        interactiveRetry(false);
        return this;
    }

    /**
     * Sets the initial delay before retrying the action.
     * <p/>
     * This only applies when performing automatic retries.
     *
     * @param delay the delay in milliseconds, or {@code 0} to disable the delay
     * @return this
     */
    public ActionBuilder delay(long delay) {
        behaviour = Behaviour.newBehaviour(behaviour)
                .delay(delay)
                .build();
        return this;
    }

    /**
     * Indicates that the user should be prompted to retry the action if it fails.
     *
     * @return this
     */
    public ActionBuilder interactiveRetry() {
        return interactiveRetry(true);
    }

    /**
     * Determines if the user should be prompted to retry the action if it fails.
     *
     * @param interactiveRetry if {@code true} the user should be prompted to retry the action if it fails,
     *                         {@code false} if no prompting should occur. Default {@code true}
     * @return this
     */
    public ActionBuilder interactiveRetry(boolean interactiveRetry) {
        this.interactiveRetry = interactiveRetry;
        return this;
    }

    /**
     * Sets the action to invoke on success.
     *
     * @param action the action
     * @return this
     */
    public ActionBuilder onSuccess(Runnable action) {
        this.success = action;
        return this;
    }

    /**
     * Sets the action to invoke on skip.
     *
     * @param action the action
     * @return this
     */
    public ActionBuilder onSkip(Runnable action) {
        this.skip = action;
        return this;
    }

    /**
     * Sets action to invoke on failure.
     *
     * @param action the action
     * @return this
     */
    public ActionBuilder onFailure(Runnable action) {
        return onFailure(status -> action.run());
    }

    /**
     * Set the action to invoke on failure.
     *
     * @param action the action
     * @return this
     */
    public ActionBuilder onFailure(Consumer<ActionStatus> action) {
        return onFailure((status, listener) -> {
            action.accept(status);
            listener.run();
        });
    }

    /**
     * Set the action to invoke on failure.
     *
     * @param action the action
     * @return this
     */
    public ActionBuilder onFailure(FailureAction action) {
        this.failure = action;
        return this;
    }

    /**
     * Builds the action.
     *
     * @return the action
     */
    public Action build() {
        if (action == null) {
            throw new IllegalStateException("No action() specified");
        }
        boolean delayFirst = behaviour.useLatestInstance();
        // only delay on the first retry if the latest objects were being used. If the objects were not the latest
        // instance, reloading them may correct the problem, so no delay is necessary
        Retries retries = new Retries(behaviour.getRetries(), interactiveRetry, behaviour.getDelay(), delayFirst);
        return new ActionRunner(action, createRunnableAction(success), createRunnableAction(skip),
                                failure, interactive.booleanValue(), retries);
    }

    /**
     * Builds the action and runs it.
     */
    public void run() {
        run(null);
    }

    /**
     * Builds the action and runs it.
     *
     * @param listener the listener to notify when the action completes. May be {@code null}
     */
    public void run(Consumer<ActionStatus> listener) {
        Action action = build();
        if (listener != null) {
            action.run(listener);
        } else {
            action.run();
        }
    }

    /**
     * Builds the action, and runs it once only.
     */
    public void runOnce() {
        runOnce(null);
    }

    /**
     * Builds the action, and runs it once only.
     *
     * @param listener the listener to notify when the action completes. May be {@code null}
     */
    public void runOnce(Consumer<ActionStatus> listener) {
        Action action = onceOnly().build();
        if (listener != null) {
            action.run(listener);
        } else {
            action.run();
        }
    }

    /**
     * Invokes a consumer with an {@link IMObject}.
     * <p/>
     * This is provided to avoid casts when using lambdas.
     *
     * @param consumer the consumer
     * @return the consumer
     */
    public static <T extends IMObject> Consumer<T> asObject(Consumer<T> consumer) {
        return consumer;
    }

    /**
     * Invokes a consumer with an {@link IMObjectBean}.
     *
     * @param consumer the consumer
     * @return a bean consumer
     */
    public static BeanConsumer asBean(Consumer<IMObjectBean> consumer) {
        return consumer::accept;
    }

    /**
     * Returns a consumer that updates the node of a bean.
     *
     * @param node  the node name
     * @param value the node value
     * @return a bean consumer
     */
    public static BeanConsumer update(String node, Object value) {
        return bean -> {
            if (!Objects.equals(bean.getValue(node), value)) {
                bean.setValue(node, value);
                bean.save();
            }
        };
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied objects.
     *
     * @param objects   the objects to supply
     * @param behaviour the retry behaviour
     * @param async     determines if the action runs asynchronously or synchronously
     * @param action    the action to run
     */
    protected <T extends IMObject> ActionBuilder action(List<T> objects, Behaviour behaviour, boolean async,
                                                        BiConsumer<List<T>, Consumer<ActionStatus>> action) {
        Action wrapper = new IMObjectsConsumerAction<>(objects, behaviour, async, action, service, transactionManager);
        return action(wrapper, behaviour);
    }

    /**
     * Registers a {@link Consumer} action that takes the object provided by an {@link ObjectSupplier}.
     * <p/>
     * The object will be wrapped in a {@link IMObjectBean}.
     *
     * @param supplier  the object supplier
     * @param behaviour the retry behaviour
     * @param async     determines if the action runs asynchronously or synchronously
     * @param action    the action to run
     * @return this
     */
    protected <T extends IMObject> ActionBuilder action(ObjectSupplier<T> supplier, Behaviour behaviour,
                                                        boolean async, BiBeanConsumer action) {
        return action(new BeanConsumerAction<>(supplier, behaviour, async, action, service, transactionManager),
                      behaviour);
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied object.
     * <p/>
     * The object will be wrapped in a {@link IMObjectBean}.
     *
     * @param object    the object to supply
     * @param behaviour the retry behaviour
     * @param async     determines if the action runs asynchronously or synchronously
     * @param action    the action to run
     * @return this
     */
    protected <T extends IMObject> ActionBuilder action(T object, Behaviour behaviour, boolean async,
                                                        BiBeanConsumer action) {
        return action(new BeanConsumerAction<T>(object, behaviour, async, action, service, transactionManager),
                      behaviour);
    }

    /**
     * Registers a {@link Consumer} action that takes the object provided by an {@link ObjectSupplier}.
     * <p/>
     * On retry, the supplier will be invoked again to obtain the object.
     *
     * @param supplier  the object supplier
     * @param behaviour the retry behaviour
     * @param async     determines if the action runs asynchronously or synchronously
     * @param action    the action to run
     * @return this
     */
    protected <T extends IMObject> ActionBuilder action(ObjectSupplier<T> supplier, Behaviour behaviour,
                                                        boolean async, BiConsumer<T, Consumer<ActionStatus>> action) {
        Action wrapper = new IMObjectConsumerAction<>(supplier, behaviour, async, action, service, transactionManager);
        return action(wrapper, behaviour);
    }

    /**
     * Registers a {@link Consumer} action that takes the supplied object.
     *
     * @param object    the object to supply
     * @param behaviour the retry behaviour
     * @param async     determines if the action runs asynchronously or synchronously
     * @param action    the action to run
     * @return this
     */
    protected <T extends IMObject> ActionBuilder action(T object, Behaviour behaviour, boolean async,
                                                        BiConsumer<T, Consumer<ActionStatus>> action) {
        return action(new IMObjectConsumerAction<>(object, behaviour, async, action, service, transactionManager),
                      behaviour);
    }

    /**
     * Creates an error dialog builder for an action status, with the message pre-populated.
     *
     * @param status the action status
     * @return a new builder
     */
    protected static ErrorDialogBuilder newErrorDialog(ActionStatus status) {
        boolean html = false;
        String message = status.getMessage();
        Throwable exception = status.getException();
        if (message == null && exception != null) {
            message = ErrorFormatter.formatHTML(exception);
            html = true;
        }
        if (message == null) {
            message = Messages.get("action.internalerror"); // no message and no exception
        }
        ErrorDialogBuilder builder;
        if (exception != null && ErrorReportingDialog.canReportErrors()) {
            builder = ErrorReportingDialog.newDialog()
                    .cause(exception);
        } else {
            builder = ErrorDialog.newDialog();
        }
        return builder.message(message, html);
    }

    /**
     * Creates a builder for a call taking a single object.
     *
     * @return a new builder
     */
    private <T extends IMObject> ObjectCallBuilder<T> newObjectCallBuilder() {
        return new ObjectCallBuilder<>(this, behaviour);
    }

    /**
     * Creates a builder for a call taking a list of objects.
     *
     * @return a new builder
     */
    private <T extends IMObject> ListCallBuilder<T> newListCallBuilder() {
        return new ListCallBuilder<>(this, behaviour);
    }

    /**
     * Sets the action.
     *
     * @param action    the action
     * @param behaviour the retry behaviour
     * @return this
     */
    private ActionBuilder action(Action action, Behaviour behaviour) {
        this.action = action;
        this.behaviour = behaviour;
        return this;
    }

    /**
     * Creates a {@link RunnableAction} if an action is specified.
     *
     * @param action the action to delegate to. May be {@code null}
     * @return the new action, or {@code null} if no action is specified
     */
    private RunnableAction createRunnableAction(Runnable action) {
        return (action != null) ? new RunnableAction(action, interactive, false) : null;
    }
}