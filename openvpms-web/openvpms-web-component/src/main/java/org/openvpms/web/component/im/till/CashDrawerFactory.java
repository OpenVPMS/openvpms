/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.till;

import org.openvpms.archetype.rules.finance.till.TillRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.till.Till;
import org.openvpms.tilldrawer.service.TillDrawerService;

/**
 * Factory for {@link CashDrawer} instances.
 *
 * @author Tim Anderson
 */
public class CashDrawerFactory {

    /**
     * The till drawer service.
     */
    private final TillDrawerService drawerService;

    /**
     * The till rules.
     */
    private final TillRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link CashDrawerFactory}.
     *
     * @param drawerService the till drawer service
     * @param rules         the till rules
     * @param service       the archetype service
     * @param domainService the domain service
     */
    public CashDrawerFactory(TillDrawerService drawerService, TillRules rules, ArchetypeService service,
                             DomainService domainService) {
        this.drawerService = drawerService;
        this.rules = rules;
        this.service = service;
        this.domainService = domainService;
    }

    /**
     * Creates a new {@link CashDrawer} for a till.
     *
     * @param till the till
     * @return a new {@link CashDrawer}
     */
    public CashDrawer create(Entity till) {
        return new CashDrawer(domainService.create(till, Till.class), drawerService, rules);
    }

    /**
     * Creates a new {@link CashDrawer} for the till associated with an act.
     *
     * @param act the act
     * @return a new {@link CashDrawer}, or {@code null} if the act has no till
     */
    public CashDrawer create(Act act) {
        CashDrawer result = null;
        IMObjectBean bean = service.getBean(act);
        if (bean.hasNode("till")) {
            Entity till = bean.getTarget("till", Entity.class);
            result = (till != null) ? create(till) : null;
        }
        return result;
    }
}
