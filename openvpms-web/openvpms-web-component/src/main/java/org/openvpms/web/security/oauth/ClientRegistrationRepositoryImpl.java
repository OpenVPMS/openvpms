/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.apache.commons.lang.StringUtils;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Default implementation of {@link ClientRegistrationRepository}.
 * <p/>
 * This uses {@code lookup.oauth2ClientRegistration} to hold the client registration details.
 *
 * @author Tim Anderson
 */
public class ClientRegistrationRepositoryImpl implements ClientRegistrationRepository {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The password encryptor, used to decrypt the client secret.
     */
    private final PasswordEncryptor encryptor;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookupService;

    /**
     * URL path delimiter.
     */
    private static final char PATH_DELIMITER = '/';

    /**
     * Constructs a {@link ClientRegistrationRepositoryImpl}.
     *
     * @param practiceService the practice service
     * @param encryptor       the password encryptor
     * @param service         the archetype service
     * @param lookupService   the lookup service
     */
    public ClientRegistrationRepositoryImpl(PracticeService practiceService, PasswordEncryptor encryptor,
                                            IArchetypeService service, LookupService lookupService) {
        this.practiceService = practiceService;
        this.encryptor = encryptor;
        this.service = service;
        this.lookupService = lookupService;
    }

    /**
     * Returns the client registration identified by the provided {@code registrationId},
     * or {@code null} if not found.
     *
     * @param registrationId the registration identifier
     * @return the {@link ClientRegistration} if found, otherwise {@code null}
     */
    @Override
    public ClientRegistration findByRegistrationId(String registrationId) {
        ClientRegistration registration = null;
        Lookup lookup = lookupService.getLookup("lookup.oauth2ClientRegistration", registrationId);
        if (lookup != null) {
            if ("gmail".equals(registrationId)) {
                registration = createGmailRegistration(lookup);
            } else if ("outlook".equals(registrationId)) {
                registration = createOutlookRegistration(lookup);
            }
        }
        return registration;
    }

    /**
     * Returns the redirect URI for a client registration.
     *
     * @param registrationId the client registration id
     * @param request        the HTTP request
     * @return the redirect URI
     */
    public String getRedirectURI(String registrationId, HttpServletRequest request) {
        // NOTE: derived from DefaultOAuth2AuthorizationRequestResolver
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("registrationId", registrationId);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(UrlUtils.buildFullRequestUrl(request))
                .replacePath(request.getContextPath())
                .replaceQuery(null)
                .fragment(null)
                .build();
        String scheme = uriComponents.getScheme();
        uriVariables.put("baseScheme", (scheme != null) ? scheme : "");
        String host = uriComponents.getHost();
        uriVariables.put("baseHost", (host != null) ? host : "");
        int port = uriComponents.getPort();
        uriVariables.put("basePort", (port == -1) ? "" : ":" + port);
        String path = uriComponents.getPath();
        if (!StringUtils.isEmpty(path) && path.charAt(0) != PATH_DELIMITER) {
            path = PATH_DELIMITER + path;
        }
        uriVariables.put("basePath", (path != null) ? path : "");
        uriVariables.put("baseUrl", uriComponents.toUriString());
        return UriComponentsBuilder.fromUriString(getRedirectURITemplate())
                .buildAndExpand(uriVariables)
                .toUriString();
    }

    /**
     * Returns the redirect URI template.
     * <p/>
     * This uses the practice {@link PracticeService#getBaseURL() base URL} if present.
     *
     * @return the redirect URI template
     */
    private String getRedirectURITemplate() {
        String baseURL = practiceService.getBaseURL();
        StringBuilder redirectUri = new StringBuilder();
        if (!StringUtils.isEmpty(baseURL)) {
            redirectUri.append(baseURL);
            if (!baseURL.endsWith("" + PATH_DELIMITER)) {
                redirectUri.append(PATH_DELIMITER);
            }
        } else {
            redirectUri.append("{baseScheme}://{baseHost}{basePort}{basePath}/");
        }
        redirectUri.append("oauth2/code/{registrationId}");
        return redirectUri.toString();
    }

    /**
     * Creates a {@link ClientRegistration} for Gmail.
     *
     * @param lookup the configuration
     * @return a corresponding {@link ClientRegistration}
     */
    private ClientRegistration createGmailRegistration(Lookup lookup) {
        // NOTE: google oauth2 requires client secret
        IMObjectBean bean = service.getBean(lookup);
        return populate(CommonOAuth2Provider.GOOGLE.getBuilder("gmail"), bean)
                .authorizationUri("https://accounts.google.com/o/oauth2/v2/auth?access_type=offline")
                // need access_type=offline to get refresh token
                .scope("openid", "profile", "email", "https://mail.google.com/")
                .build();
    }

    /**
     * Creates a {@link ClientRegistration} for Outlook.
     *
     * @param lookup the configuration
     * @return a corresponding {@link ClientRegistration}
     */
    private ClientRegistration createOutlookRegistration(Lookup lookup) {
        IMObjectBean bean = service.getBean(lookup);
        String tenantId = bean.getString("tenantId");
        return populate(ClientRegistration.withRegistrationId("outlook"), bean)
                // Microsoft apparently supports public clients, but haven't been able to get it to work
                // .clientAuthenticationMethod(ClientAuthenticationMethod.NONE)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationUri("https://login.microsoftonline.com/" + tenantId
                                  + "/oauth2/v2.0/authorize?prompt=consent")
                .tokenUri("https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token")
                .jwkSetUri("https://login.microsoftonline.com/" + tenantId + "/discovery/v2.0/keys")
                .userInfoUri("https://graph.microsoft.com/oidc/userinfo")
                .userNameAttributeName(IdTokenClaimNames.SUB)
                .scope("openid", "offline_access", "email", "profile",
                       "https://outlook.office.com/IMAP.AccessAsUser.All",
                       "https://outlook.office.com/SMTP.Send")
                .build();
    }

    /**
     * Populates a client registration builder.
     *
     * @param builder the builder to populate
     * @param bean    the configuration
     * @return the builder
     */
    private ClientRegistration.Builder populate(ClientRegistration.Builder builder, IMObjectBean bean) {
        String encryptedSecret = bean.getString("clientSecret");
        String clientSecret = encryptedSecret != null ? encryptor.decrypt(encryptedSecret) : null;
        String redirectUri = getRedirectURITemplate();
        return builder.clientId(bean.getString("clientId"))
                .clientSecret(clientSecret)
                .redirectUri(redirectUri);
    }
}