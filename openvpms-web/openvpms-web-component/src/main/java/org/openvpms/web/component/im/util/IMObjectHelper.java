/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.util;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.service.archetype.ArchetypeServiceHelper;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.ArchetypeQueryHelper;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;
import org.openvpms.component.system.common.util.StringUtilities;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.PatternSyntaxException;


/**
 * {@link IMObject} helper methods.
 *
 * @author Tim Anderson
 */
public class IMObjectHelper {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectHelper.class);


    /**
     * Returns an object given its reference.
     *
     * @param reference the object reference. May be {@code null}
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    public static IMObject getObject(Reference reference) {
        return getObject(reference, (Context) null);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the object reference. May be {@code null}
     * @param type      the expected type
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    public static <T extends IMObject> T getObject(Reference reference, Class<T> type) {
        IMObject object = getObject(reference);
        return (object != null) ? type.cast(object) : null;
    }

    /**
     * Returns an object given its reference.
     * This checks the specified context first. If not found in the context,
     * tries to retrieve it from the archetype service.
     * <p>
     * Note that if the object in the context is only partially populated,
     * (as indicated by a {@code version < 0}), the actual object will
     * be retrieved from the archetype service.
     *
     * @param reference the object reference. May be {@code null}
     * @param context   the context to use. If {@code null} accesses the archetype service
     * @return the object corresponding to {@code reference} or {@code null} if none exists
     */
    public static IMObject getObject(Reference reference, Context context) {
        IMObject result = null;
        if (reference != null) {
            if (context != null) {
                result = context.getObject(reference);
            }
            if (result == null || result.getVersion() < 0) {
                try {
                    IArchetypeService service = ArchetypeServiceHelper.getArchetypeService();
                    result = service.get(reference);
                } catch (OpenVPMSException error) {
                    log.error(error.getMessage(), error);
                }
            }
        }
        return result;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean wrapping the object
     */
    public static IMObjectBean getBean(IMObject object) {
        return ServiceHelper.getArchetypeService().getBean(object);
    }

    /**
     * Returns the name of an object, given its reference.
     *
     * @param reference the object reference. May be {@code null}
     * @return the name or {@code null} if none exists
     */
    public static String getName(Reference reference) {
        return reference != null ? getName(reference, ServiceHelper.getArchetypeService()) : null;
    }

    /**
     * Returns the name of an object, given its reference.
     *
     * @param reference the object reference. May be {@code null}
     * @param service   the archetype service
     * @return the name or {@code null} if none exists
     */
    public static String getName(Reference reference, IArchetypeService service) {
        String result = null;
        try {
            result = ArchetypeQueryHelper.getName(reference, service);
        } catch (OpenVPMSException error) {
            log.error(error.getMessage(), error);
        }
        return result;
    }

    /**
     * Determines if an object associated with a reference is active.
     *
     * @param reference the object reference. May be {@code null}
     * @return {@code true} if the object is active, otherwise {@code false}
     */
    public static boolean isActive(Reference reference) {
        if (reference != null) {
            try {
                ObjectRefConstraint constraint
                        = new ObjectRefConstraint("o", reference);
                ArchetypeQuery query = new ArchetypeQuery(constraint);
                query.add(new NodeSelectConstraint("o.active"));
                query.setMaxResults(1);
                Iterator<ObjectSet> iter = new ObjectSetQueryIterator(query);
                if (iter.hasNext()) {
                    ObjectSet set = iter.next();
                    return set.getBoolean("o.active");
                }
            } catch (OpenVPMSException error) {
                log.error(error.getMessage(), error);
            }
        }
        return false;
    }

    /**
     * Returns an object given its reference and descriptor. If the reference is
     * null, determines if the specified archetype range matches that of the
     * current object being viewed/edited and returns that instead.
     *
     * @param reference  the object reference. May be {@code null}
     * @param shortNames the archetype range
     * @param context    the context
     * @return the object matching {@code reference}, or {@code shortNames},
     * or {@code null} if there are no matches
     */
    public static IMObject getObject(Reference reference, String[] shortNames, Context context) {
        IMObject result;
        if (reference == null) {
            result = match(shortNames, context);
        } else {
            result = getObject(reference, context);
        }
        return result;
    }

    /**
     * Reloads an object.
     * <p/>
     * If an object is unsaved, it is returned unchanged.
     *
     * @param object the object to reload. May be {@code null}
     * @return the object, or {@code null} if it couldn't be reloaded
     */
    public static <T extends IMObject> T reload(T object) {
        return reload(object, false);
    }

    /**
     * Reloads an object.
     * <p/>
     * If an object is unsaved, it is returned unchanged.
     *
     * @param object      the object to reload. May be {@code null}
     * @param failOnError if {@code true}, fail if the object cannot be reloaded
     * @return the object, or {@code null} if it couldn't be reloaded and {@code failOnError == false}
     * @throws IllegalStateException if the object was saved but cannot be reloaded and {@code failOnError == true}
     */
    @SuppressWarnings("unchecked")
    public static <T extends IMObject> T reload(T object, boolean failOnError) {
        T result = object;
        if (object != null && !object.isNew()) {
            IArchetypeService service = ArchetypeServiceHelper.getArchetypeService();
            try {
                result = (T) service.get(object.getObjectReference());
            } catch (OpenVPMSException error) {
                log.error(error.getMessage(), error);
            }
            if (result == null && failOnError) {
                throw new IllegalStateException(Messages.format("imobject.noexist",
                                                                DescriptorHelper.getDisplayName(object, service)));
            }
        }
        return result;
    }

    /**
     * Returns a value from an object, given the value's node descriptor name.
     *
     * @param object the object
     * @param node   the node name
     * @return the value corresponding to {@code node}. May be
     * {@code null}
     */
    public static Object getValue(IMObject object, String node) {
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(object);
        return (bean.hasNode(node)) ? bean.getValue(node) : null;
    }

    /**
     * Returns the first object instance from a collection with matching short
     * name.
     *
     * @param shortName the short name
     * @param objects   the objects to search
     * @return the first object from the collection with matching short name, or
     * {@code null} if none exists.
     */
    public static <T extends IMObject> T getObject(String shortName, Collection<T> objects) {
        T result = null;
        for (T object : objects) {
            if (TypeHelper.isA(object, shortName)) {
                result = object;
                break;
            }
        }
        return result;
    }

    /**
     * Returns an object corresponding to a reference, from a list of objects.
     * <p>
     *
     * @param reference the reference. May be {@code null}
     * @param objects   the list of objects. May contain nulls.
     * @return the corresponding object, or {@code null} if none is found
     */
    public static <T extends IMObject> T getObject(Reference reference, Collection<T> objects) {
        if (reference != null) {
            for (T object : objects) {
                if (object != null && Objects.equals(object.getObjectReference(), reference)) {
                    return object;
                }
            }
        }
        return null;
    }

    /**
     * Returns a list of objects with matching name.
     * Names are treated as case-insensitive.
     *
     * @param name    the name. May contain wildcards. If null or empty,
     *                indicates no filtering.
     * @param objects the objects to filter
     */
    public static <T extends IMObject> List<T> findByName(String name, Collection<T> objects) {
        List<T> result = new ArrayList<T>();
        if (StringUtils.isEmpty(name)) {
            result.addAll(objects);
        } else {
            try {
                String regex = StringUtilities.toRegEx(name.toLowerCase());
                for (T object : objects) {
                    String value = object.getName();
                    if (value != null && value.toLowerCase().matches(regex)) {
                        result.add(object);
                    }
                }
            } catch (PatternSyntaxException exception) {
                log.warn(exception.getMessage(), exception);
            }
        }
        return result;
    }

    /**
     * Returns the nearest common superclass for a set of archetype short names.
     *
     * @param shortNames the archetype short names
     * @return the common implementation class type
     */
    public static Class<?> getType(String[] shortNames) {
        Class result = null;
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Set<Class> classes = new HashSet<>();
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        for (String shortName : shortNames) {
            ArchetypeDescriptor archetype = service.getArchetypeDescriptor(shortName);
            try {
                Class clazz = loader.loadClass(archetype.getClassName());
                classes.add(clazz);
            } catch (ClassNotFoundException exception) {
                log.error(exception.getMessage(), exception);
            }
        }
        for (Class clazz : classes) {
            if (result == null) {
                result = clazz;
            } else {
                while (!result.isAssignableFrom(clazz)) {
                    result = result.getSuperclass();
                }
            }
            if (result == Object.class) {
                // shouldn't be the case, default to something sensible
                result = IMObject.class;
                break;
            }
        }
        return (result == null) ? IMObject.class : result;
    }

    /**
     * Determines if an object has the same object reference and version as another.
     *
     * @param object the object. May be {@code null}
     * @param other  the object. May be {@code null}
     * @return {@code true} if the objects have the same object references
     * and version, otherwise {@code false}
     */
    public static boolean isSame(IMObject object, IMObject other) {
        if (object != null && other != null) {
            return object.getObjectReference().equals(other.getObjectReference())
                   && object.getVersion() == other.getVersion();
        }
        return false;
    }

    /**
     * Determines if a newer version of an object exists.
     *
     * @param object the object
     * @return {@code true{@code  if a newer version exists,
     * otherwise {@code false}. If {@code object == null}
     * also returns {@code false}
     */
    public static boolean hasNewerVersion(IMObject object) {
        boolean result = false;
        if (object != null) {
            try {
                IArchetypeService service
                        = ArchetypeServiceHelper.getArchetypeService();
                IMObject o = service.get(object.getObjectReference());
                if (o != null && o.getVersion() > object.getVersion()) {
                    result = true;
                }
            } catch (OpenVPMSException exception) {
                log.error(exception.getMessage(), exception);
            }
        }
        return result;
    }

    /**
     * Determines if an active instance of an archetype exists.
     *
     * @param archetype the archetype
     * @return {@code true} if an active instance of an archetype exists
     */
    public static boolean hasActiveInstance(String archetype) {
        return ServiceHelper.getBean(SingletonService.class).exists(archetype);
    }

    /**
     * Determines if the current object being edited matches the specified archetype range.
     *
     * @param shortNames the archetype range
     * @param context    the context
     * @return the current object being edited, or {@code null} if its type doesn't matches the specified
     * descriptor's archetype range
     */
    private static IMObject match(String[] shortNames, Context context) {
        IMObject result = null;
        IMObject object = context.getCurrent();
        if (object != null) {
            for (String shortName : shortNames) {
                if (object.isA(shortName)) {
                    result = object;
                    break;
                }
            }
        }
        return result;
    }

}

