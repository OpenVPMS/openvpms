/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.DocumentTemplateLocatorFactory;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Factory for {@link DocumentGenerator}.
 *
 * @author Tim Anderson
 */
public class DocumentGeneratorFactory {

    /**
     * The document template locator factory.
     */
    private final DocumentTemplateLocatorFactory templateLocatorFactory;

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The report factory.
     */
    private final ReportFactory reportFactory;

    /**
     * The document job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * The document rules.
     */
    private final DocumentRules documentRules;

    /**
     * Constructs a {@link DocumentGeneratorFactory}.
     *
     * @param templateLocatorFactory the document template locator factory
     * @param formatter              the formatter
     * @param service                the archetype service
     * @param lookups                the lookup service
     * @param reportFactory          the report factory
     * @param jobManager             the document job manager
     * @param documentRules          the document rules
     */
    public DocumentGeneratorFactory(DocumentTemplateLocatorFactory templateLocatorFactory, FileNameFormatter formatter,
                                    ArchetypeService service, LookupService lookups, ReportFactory reportFactory,
                                    DocumentJobManager jobManager, DocumentRules documentRules) {
        this.templateLocatorFactory = templateLocatorFactory;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
        this.reportFactory = reportFactory;
        this.jobManager = jobManager;
        this.documentRules = documentRules;
    }

    /**
     * Constructs a {@link DocumentGenerator}.
     *
     * @param act      the document act
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify
     */
    public DocumentGenerator create(DocumentAct act, Context context, HelpContext help,
                                    DocumentGenerator.Listener listener) {
        return new DocumentGenerator(act, templateLocatorFactory, context, help, formatter, service, lookups,
                                     reportFactory, jobManager, documentRules, listener);
    }

}