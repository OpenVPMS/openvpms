/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.system.ServiceHelper;

/**
 * A document template locator for patient investigation templates.
 *
 * @author Tim Anderson
 */
public class InvestigationTemplateLocator extends TypeBasedDocumentTemplateLocator {

    /**
     * Constructs an {@link InvestigationTemplateLocator}.
     */
    public InvestigationTemplateLocator() {
        this(ServiceHelper.getArchetypeService());
    }

    /**
     * Constructs an {@link InvestigationTemplateLocator}.
     *
     * @param service the archetype service
     */
    public InvestigationTemplateLocator(ArchetypeService service) {
        super(InvestigationArchetypes.PATIENT_INVESTIGATION, service);
    }
}
