/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.apache.commons.beanutils.ConstructorUtils;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;

/**
 * Factory for {@link DocumentViewer} instances.
 *
 * @author Tim Anderson
 */
public class DocumentViewerFactory {

    /**
     * Maps archetypes to their corresponding viewers.
     */
    private final ArchetypeHandlers<DocumentViewer> viewers;

    /**
     * Constructs a {@link DocumentViewerFactory}.
     *
     * @param service the archetype service
     */
    public DocumentViewerFactory(IArchetypeService service) {
        viewers = new ArchetypeHandlers<>("DocumentViewerFactory", "DefaultDocumentViewerFactory", DocumentViewer.class,
                                          service);
    }

    /**
     * Creates a document viewer.
     *
     * @param act     the document act
     * @param link    if {@code true} enable a hyperlink to the document
     * @param context the layout context
     * @return a viewer for the document act
     */
    public DocumentViewer create(DocumentAct act, boolean link, LayoutContext context) {
        Class<?> type = getType(act);

        try {
            return (DocumentViewer) ConstructorUtils.invokeConstructor(type, new Object[]{act, link, context});
        } catch (Throwable exception) {
            throw new IllegalStateException("Failed to construct " + type.getName() + " for "
                                            + act.getArchetype(), exception);
        }
    }

    /**
     * Creates a document viewer.
     *
     * @param act      the document act
     * @param link     if {@code true} enable a hyperlink to the document
     * @param template if {@code true}, display as a template, otherwise generate the document if required
     * @param context  the layout context
     * @return a viewer for the document act
     */
    public DocumentViewer create(DocumentAct act, boolean link, boolean template, LayoutContext context) {
        Class<?> type = getType(act);

        try {
            return (DocumentViewer) ConstructorUtils.invokeConstructor(type, new Object[]{act, link, template,
                                                                                          context});
        } catch (Throwable exception) {
            throw new IllegalStateException("Failed to construct " + type.getName() + " for "
                                            + act.getArchetype(), exception);
        }
    }

    /**
     * Returns the viewer type for a given object.
     *
     * @param object the object
     * @return the viewer typee for the object
     */
    private Class<?> getType(IMObject object) {
        ArchetypeHandler<DocumentViewer> handler = viewers.getHandler(object.getArchetype());
        return (handler != null) ? handler.getType() : DocumentViewer.class;
    }

}
