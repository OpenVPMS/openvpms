/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;


/**
 * Task properties.
 *
 * @author Tim Anderson
 */
public class TaskProperties {

    /**
     * The properties.
     */
    private final Map<String, TaskProperty> properties = new HashMap<>();

    /**
     * Adds a property.
     *
     * @param property the property to add
     */
    public void add(TaskProperty property) {
        properties.put(property.getName(), property);
    }

    /**
     * Adds a constant property.
     *
     * @param name  the property name
     * @param value the property value
     */
    public void add(String name, Object value) {
        add(new Constant(name, value));
    }

    /**
     * Adds a property that is derived on demand.
     *
     * @param name     the property name
     * @param supplier the property value supplier
     */
    public void add(String name, Supplier<Object> supplier) {
        TaskProperty property = new TaskProperty() {
            @Override
            public String getName() {
                return name;
            }

            @Override
            public Object getValue(TaskContext context) {
                return supplier.get();
            }
        };
        add(property);
    }

    /**
     * Returns the named property.
     *
     * @param name the property name
     * @return the property corresponding to {@code name} or {@code null} if it doesn't exist
     */
    public TaskProperty get(String name) {
        return properties.get(name);
    }

    /**
     * Returns the properties.
     *
     * @return the properties
     */
    public Collection<TaskProperty> getProperties() {
        return properties.values();
    }

    /**
     * Constant property.
     */
    private static class Constant implements TaskProperty {

        /**
         * The property name.
         */
        private final String name;

        /**
         * The property value.
         */
        private final Object value;

        /**
         * Constructs a {@link Constant}.
         *
         * @param name  the property name
         * @param value the property value
         */
        public Constant(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        /**
         * Returns the property name.
         *
         * @return the property name
         */
        public String getName() {
            return name;
        }

        /**
         * Returns the property value.
         *
         * @param context the task context
         * @return the property value
         */
        public Object getValue(TaskContext context) {
            return value;
        }
    }
}
