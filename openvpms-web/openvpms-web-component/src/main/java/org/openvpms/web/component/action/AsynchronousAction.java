/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

/**
 * An action that runs asynchronously.
 *
 * @author Tim Anderson
 */
public abstract class AsynchronousAction extends AbstractAction {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AsynchronousAction.class);

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    protected abstract void runAction(Consumer<ActionStatus> listener);

    /**
     * Runs the action, catching any exceptions.
     *
     * @param listener the listener to notify on completion
     */
    protected void runProtected(Consumer<ActionStatus> listener) {
        listener = new OnceOnlyListener(listener);
        try {
            runAction(listener);
        } catch (Throwable exception) {
            listener.accept(ActionStatus.retry(exception));
        }
    }

    /**
     * A listener that ensure it only invokes its delegate once.
     */
    protected class OnceOnlyListener implements Consumer<ActionStatus> {

        private Consumer<ActionStatus> listener;

        public OnceOnlyListener(Consumer<ActionStatus> listener) {
            this.listener = listener;
        }

        /**
         * Performs this operation on the given argument.
         *
         * @param status the input argument
         */
        @Override
        public void accept(ActionStatus status) {
            if (listener != null) {
                listener.accept(status);
                listener = null;
            } else {
                log.warn("Attempt to invoke listener multiple times for action: {}, status={}",
                         AsynchronousAction.this, status);
            }
        }
    }
}
