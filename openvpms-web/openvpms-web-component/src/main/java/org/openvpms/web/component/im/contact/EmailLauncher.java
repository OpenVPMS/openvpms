/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.ToAddressFormatter;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * Creates components to launch email editors.
 *
 * @author Tim Anderson
 */
public class EmailLauncher {

    /**
     * The email address.
     */
    private final String address;

    /**
     * The email contact.
     */
    private final Contact email;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The mail context.
     */
    private final MailContext mailContext;

    /**
     * The maximum no. of characters to display.
     */
    private int length = -1;

    /**
     * Constructs an {@link EmailLauncher}.
     *
     * @param address     the email address
     * @param email       the email contact
     * @param context     the context
     * @param help        the parent help context
     * @param mailContext the mail context
     */
    private EmailLauncher(String address, Contact email, Context context, HelpContext help, MailContext mailContext) {
        this.address = address;
        this.email = email;
        this.context = context;
        this.help = help.topic("email");
        this.mailContext = mailContext;
    }

    /**
     * Sets the maximum length of the email address, before it gets abbreviated.
     *
     * @param length the length, or {@code -1} to not abbreviate the email address
     */
    public void setEmailLength(int length) {
        if (length > 5) {
            this.length = length;
        }
    }

    /**
     * Returns a button to display an {@link MailDialog}.
     *
     * @return a new button
     */
    public Button getWriteButton() {
        Button button = ButtonFactory.create(null, "hyperlink", () -> {
            MailDialogFactory factory = ServiceHelper.getBean(MailDialogFactory.class);
            MailDialog dialog = factory.create(mailContext, email, new DefaultLayoutContext(context, help));
            dialog.show();
        });
        button.setToolTipText(Messages.get("mail.internal.tooltip"));

        String text;
        if (length > 0 && address.length() > length) {
            text = StringUtils.abbreviateMiddle(address, "...", length);
            button.setToolTipText(address);
        } else {
            text = address;
        }
        button.setText(text);
        return button;
    }

    /**
     * Creates a mailto: link, unless the showMailTo option has been disabled at the practice.
     *
     * @return a new link. May be {@code null}
     */
    public Component getMailToLink() {
        Label html = null;
        boolean showMailTo = false;

        Party practice = context.getPractice();
        if (practice != null) {
            IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(practice);
            showMailTo = bean.getBoolean("showMailTo");
        }
        if (showMailTo) {
            ToAddressFormatter formatter = new ToAddressFormatter();
            String escaped = StringEscapeUtils.escapeXml11("<" + address + ">");
            String name = StringEscapeUtils.escapeXml11(formatter.getName(email));
            String uri = "../images/buttons/external-link.png";
            html = LabelFactory.html("<a href=\"mailto:" + name + escaped + "\"><img src=\"" + uri + "\"/></a>");
            html.setToolTipText(Messages.get("mail.external.tooltip"));
        }
        return html;
    }

    /**
     * Creates an email launcher for the preferred email address for a party.
     *
     * @param party       the party
     * @param service     the archetype service
     * @param context     the context
     * @param help        the parent help context
     * @param mailContext the mail context
     * @return an email launcher, or {@code null} if the customer doesn't have an email contact
     */
    public static EmailLauncher create(Party party, ArchetypeService service, Context context, HelpContext help,
                                       MailContext mailContext) {
        Contact email = ContactHelper.getPreferredEmail(party, service);
        if (email != null) {
            return create(email, service, context, help, mailContext);
        }
        return null;
    }

    /**
     * Creates an email launcher for an email address.
     *
     * @param email       the email
     * @param service     the archetype service
     * @param context     the context
     * @param help        the parent help context
     * @param mailContext the mail context
     * @return an email launcher, or {@code null} if the contact is incomplete
     */
    public static EmailLauncher create(Contact email, ArchetypeService service, Context context, HelpContext help,
                                       MailContext mailContext) {
        String address = ContactHelper.getEmail(email, service);
        if (address != null) {
            return new EmailLauncher(address, email, context, help, mailContext);
        }
        return null;
    }
}