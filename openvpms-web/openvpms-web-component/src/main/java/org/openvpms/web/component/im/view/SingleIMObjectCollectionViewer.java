/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.view;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.IMObjectProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.ReadOnlyProperty;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.text.TextComponent;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

/**
 * A {@link IMObjectCollectionViewer} for collections containing at most one element.
 *
 * @author Tim Anderson
 */
public abstract class SingleIMObjectCollectionViewer implements IMObjectCollectionViewer {

    /**
     * The collection property.
     */
    private final CollectionProperty property;

    /**
     * The parent object.
     */
    private final IMObject parent;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The selected object.
     */
    private IMObject selected;

    /**
     * The component.
     */
    private Component component;


    /**
     * Constructs a {@link SingleIMObjectCollectionViewer}.
     *
     * @param property the collection property
     * @param parent   the parent object
     * @param context  the layout context
     */
    public SingleIMObjectCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext context) {
        this.property = property;
        this.parent = parent;
        this.context = context;
    }

    /**
     * Returns the collection property.
     *
     * @return the collection property
     */
    @Override
    public CollectionProperty getProperty() {
        return property;
    }

    /**
     * Returns the view component.
     *
     * @return the view component
     */
    @Override
    public Component getComponent() {
        if (component == null) {
            List<?> values = property.getValues();
            if (!values.isEmpty()) {
                selected = (IMObject) values.get(0);
                component = createComponent(selected, parent, context);
            } else {
                component = createEmptyCollectionViewer();
            }
        }
        return component;
    }

    /**
     * Returns the selected object.
     *
     * @return the selected object. May be {@code null}
     */
    @Override
    public IMObject getSelected() {
        return selected;
    }

    /**
     * Returns a component for an empty collection.
     *
     * @return the component
     */
    public static Component createEmptyCollectionViewer() {
        TextComponent component = TextComponentFactory.create(20);
        component.setText(Messages.get("imobject.none"));
        component.setEnabled(false);
        return component;
    }

    /**
     * Creates a component for the object.
     *
     * @param object  the object
     * @param parent  the parent
     * @param context the layout context
     * @return the component
     */
    protected abstract Component createComponent(IMObject object, IMObject parent, LayoutContext context);

    /**
     * Renders the node of an object.
     *
     * @param object the object
     * @param node   the node name
     * @return the corresponding component
     */
    protected Component createComponent(IMObject object, String node) {
        NodeDescriptor descriptor = context.getArchetypeDescriptor(object).getNodeDescriptor(node);
        if (descriptor != null) {
            Property property = new IMObjectProperty(object, descriptor);
            if (context.isEdit() && !property.isReadOnly()) {
                property = new ReadOnlyProperty(property);
            }
            return context.getComponentFactory().create(property, object).getComponent();
        }
        return LabelFactory.create();
    }
}
