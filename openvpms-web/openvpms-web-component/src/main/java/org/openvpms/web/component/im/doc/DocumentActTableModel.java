/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.act.ActAmountTableModel;
import org.openvpms.web.component.im.util.IMObjectNames;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.system.ServiceHelper;

import java.util.Comparator;
import java.util.List;


/**
 * Table model for document acts.
 *
 * @author Tim Anderson
 */
public class DocumentActTableModel extends ActAmountTableModel<DocumentAct> {

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The document viewer factory.
     */
    private final DocumentViewerFactory factory;

    /**
     * The fileName/reference model index.
     */
    private int docIndex;

    /**
     * The versions model index.
     */
    private int versionsIndex = -1;


    /**
     * Constructs a {@link DocumentActTableModel}.
     *
     * @param context the layout context
     */
    public DocumentActTableModel(LayoutContext context) {
        this(true, true, true, context);
    }

    /**
     * Constructs a {@code DocumentActTableModel}.
     *
     * @param showArchetype determines if the archetype column should be displayed
     * @param showStatus    determines if the status column should be displayed
     * @param showVersions  determines if the versions column should be displayed
     * @param context       the layout context
     */
    public DocumentActTableModel(boolean showArchetype, boolean showStatus, boolean showVersions,
                                 LayoutContext context) {
        super(showArchetype, showStatus, false);
        this.context = context;
        factory = ServiceHelper.getBean(DocumentViewerFactory.class);

        if (showVersions) {
            DefaultTableColumnModel model = (DefaultTableColumnModel) getColumnModel();
            versionsIndex = getNextModelIndex(model);
            model.addColumn(createTableColumn(versionsIndex, "document.acttablemodel.versions"));
        }
    }


    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param act    the object
     * @param column the table column
     * @param row    the table row
     * @return the value at the given coordinate
     */
    @Override
    protected Object getValue(DocumentAct act, TableColumn column, int row) {
        Object result;
        int index = column.getModelIndex();
        if (index == docIndex) {
            result = createDocumentViewer(act);
        } else if (index == versionsIndex) {
            result = getVersions(act);
        } else {
            result = super.getValue(act, column, row);
        }
        return result;
    }

    /**
     * Returns the object description.
     *
     * @param object the object
     * @return the description. May be {@code null}
     */
    @Override
    protected String getDescription(DocumentAct object) {
        String description = super.getDescription(object);
        if (description == null && object.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            // provide some context for investigations
            description = getInvestigationProductName(object);
        }
        return description;
    }

    /**
     * Creates a component to view the associated document.
     *
     * @param act the document
     * @return a component to view the associated document
     */
    protected Component createDocumentViewer(DocumentAct act) {
        DocumentViewer viewer = factory.create(act, true, context);
        viewer.setShowNoDocument(false);
        return viewer.getComponent();
    }

    /**
     * Returns the name of the product associated with an investigation.
     * <p/>
     * If there are multiple products, the one with the lowest id will be returned.
     *
     * @param object the investigation
     * @return the product name. May be {@code null}
     */
    private String getInvestigationProductName(DocumentAct object) {
        String result = null;
        IMObjectBean bean = getBean(object);
        List<Reference> products = bean.getTargetRefs("products");
        int size = products.size();
        if (size > 0) {
            if (size > 1) {
                // make sure the same product is selected each time it is rendered
                products.sort(Comparator.comparingLong(Reference::getId));
            }
            IMObjectNames names = getContext().getNames();
            result = names.getName(products.get(0));
        }
        return result;
    }

    /**
     * Helper to create a column model.
     * Adds a customer column before the amount index.
     *
     * @param showArchetype   determines if the showArchetype column should be displayed
     * @param showStatus      determines if the status column should be displayed
     * @param showAmount      determines if the credit/debit amount should be displayed
     * @param showDescription determines if the description column should be displayed
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(boolean showArchetype, boolean showStatus,
                                                 boolean showAmount, boolean showDescription) {
        DefaultTableColumnModel model = (DefaultTableColumnModel) super.createColumnModel(showArchetype, showStatus,
                                                                                          showAmount, showDescription);
        docIndex = getNextModelIndex(model);
        model.addColumn(createTableColumn(docIndex, "document.acttablemodel.doc"));
        if (showAmount) {
            model.moveColumn(model.getColumnCount() - 1, getColumnOffset(model, AMOUNT_INDEX));
        }
        return model;
    }

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return context;
    }

    /**
     * Returns the number of versions that a document has.
     *
     * @param act the document
     * @return the number of versions the document has
     */
    private Label getVersions(DocumentAct act) {
        Label result = null;
        IMObjectBean bean = getBean(act);
        if (bean.hasNode(DocumentRules.VERSIONS)) {
            int versions = bean.getValues(DocumentRules.VERSIONS).size();
            if (versions > 0) {
                result = TableHelper.rightAlign(Integer.toString(versions));
            }
        }
        return result;
    }


}
