/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.action.ActionBuilder.BeanConsumer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Builds a call for an {@link ActionBuilder} that takes a single object.
 *
 * @author Tim Anderson
 */
public class ObjectCallBuilder<T extends IMObject> extends CallBuilder<ObjectCallBuilder<T>> {

    /**
     * The object to supply to the call.
     */
    private T object;

    /**
     * The supplier that provides the object for the call.
     */
    private ObjectSupplier<T> supplier;

    /**
     * Constructs a {@link ObjectCallBuilder}.
     *
     * @param parent    the parent builder
     * @param behaviour the initial behaviour
     */
    public ObjectCallBuilder(ActionBuilder parent, Behaviour behaviour) {
        super(parent, behaviour);
    }

    /**
     * Use the supplied object.
     * <p/>
     * On retry, the same object will be used.
     *
     * @param object the object
     * @return this
     */
    public ObjectCallBuilder<T> withObject(T object) {
        return object(object, null)
                .behaviour().useOriginalObject().update();
    }

    /**
     * Use the object returned by the supplier.
     *
     * @param archetype the archetype, used for error reporting if the object cannot be retrieved
     * @param supplier  the object supplier
     * @return this
     */
    public ObjectCallBuilder<T> withObject(String archetype, Supplier<T> supplier) {
        return withObject(ObjectSupplier.create(archetype, supplier));
    }

    /**
     * Use the object returned by the supplier.
     *
     * @param supplier the object supplier
     * @return this
     */
    public ObjectCallBuilder<T> withObject(ObjectSupplier<T> supplier) {
        return object(null, supplier)
                .behaviour().useOriginalObject().update();
    }

    /**
     * Sets the call to invoke.
     *
     * @param consumer the consumer
     * @return the parent builder
     */
    public ActionBuilder call(Consumer<T> consumer) {
        return call(new SyncConsumer<>(consumer), false);
    }

    /**
     * Sets the call to invoke.
     * <p/>
     * This is responsible for notifying the listener when the call completes.
     *
     * @param consumer the consumer
     * @return the parent builder
     */
    public ActionBuilder call(BiConsumer<T, Consumer<ActionStatus>> consumer) {
        return call(consumer, true);
    }

    /**
     * Sets the call to invoke.
     *
     * @param consumer the consumer
     * @return the parent builder
     */
    public ActionBuilder asBean(BeanConsumer consumer) {
        if (object != null) {
            getParent().action(object, getBehaviour(), false, new SyncBeanConsumer(consumer));
        } else if (supplier != null) {
            getParent().action(supplier, getBehaviour(), false, new SyncBeanConsumer(consumer));
        } else {
            throw new IllegalStateException("No object/supplier specified");
        }
        return getParent();
    }

    /**
     * Sets the call to invoke.
     *
     * @param consumer the consumer
     * @param async     determines if the action runs asynchronously or synchronously
     * @return the parent builder
     */
    private ActionBuilder call(BiConsumer<T, Consumer<ActionStatus>> consumer, boolean async) {
        if (object != null) {
            getParent().action(object, getBehaviour(), async, consumer);
        } else if (supplier != null) {
            getParent().action(supplier, getBehaviour(), async, consumer);
        } else {
            throw new IllegalStateException("No object/supplier specified");
        }
        return getParent();
    }

    /**
     * Sets the object or supplier to use.
     *
     * @param object   the object. May be {@code null}
     * @param supplier the supplier. May be {@code null}
     * @return this
     */
    private ObjectCallBuilder<T> object(T object, ObjectSupplier<T> supplier) {
        this.object = object;
        this.supplier = supplier;
        return this;
    }

    /**
     * Synchronous consumer. This calls the consumer and then the listener.
     */
    static class SyncBeanConsumer implements ActionBuilder.BiBeanConsumer {

        private final Consumer<IMObjectBean> consumer;

        public SyncBeanConsumer(Consumer<IMObjectBean> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void accept(IMObjectBean bean, Consumer<ActionStatus> listener) {
            consumer.accept(bean);
            listener.accept(ActionStatus.success());
        }
    }
}