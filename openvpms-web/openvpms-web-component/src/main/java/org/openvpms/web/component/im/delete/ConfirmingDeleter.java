/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;


/**
 * An {@link IMObjectDeleter} that prompts for confirmation to delete objects.
 *
 * @author Tim Anderson
 */
public class ConfirmingDeleter<T extends IMObject> extends AbstractIMObjectDeleter<T> {

    /**
     * Constructs a {@link ConfirmingDeleter}.
     *
     * @param factory the deletion handler factory
     * @param service the archetype service
     */
    public ConfirmingDeleter(IMObjectDeletionHandlerFactory factory, ArchetypeService service) {
        super(factory, service);
    }

    /**
     * Invoked to remove an object.
     * <p>
     * Pops up a dialog prompting if deletion of an object should proceed, deleting it if OK is selected.
     *
     * @param handler  the deletion handler
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify
     */
    @Override
    protected void delete(IMObjectDeletionHandler<T> handler, Context context, HelpContext help,
                          IMObjectDeletionListener<T> listener) {
        T object = handler.getObject();
        String type = getDisplayName(object);
        String name = (object.getName() != null) ? object.getName() : type;
        ConfirmationDialog.newDialog()
                .titleKey("imobject.delete.title", type)
                .messageKey("imobject.delete.message", name)
                .ok(true, () -> doDelete(handler, context, help, listener))
                // disable OK shortcut to help prevent accidental deletion
                .cancel()
                .help(help)
                .show();
    }

    /**
     * Invoked when an object cannot be deleted, and must therefore be deactivated.
     * <p>
     * This implementation prompts the user to deactivate the object, or cancel.
     *
     * @param handler  the deletion handler
     * @param listener the listener
     * @param help     the help context
     */
    @Override
    protected void deactivate(IMObjectDeletionHandler<T> handler, IMObjectDeletionListener<T> listener,
                              HelpContext help) {
        T object = handler.getObject();
        String type = getDisplayName(object);
        String name = (object.getName() != null) ? object.getName() : type;
        ConfirmationDialog.newDialog()
                .titleKey("imobject.deactivate.title", type)
                .messageKey("imobject.deactivate.message", name)
                .ok(true, () -> doDeactivate(handler, listener))
                // disable OK shortcut to help prevent accidental deactivation
                .cancel()
                .help(help)
                .show();
    }

    /**
     * Invoked when an object cannot be de deleted, and has already been deactivated.
     *
     * @param object   the object
     * @param help     the help context
     * @param listener the listener to notify
     */
    protected void deactivated(T object, HelpContext help, IMObjectDeletionListener<T> listener) {
        ErrorDialog.newDialog()
                .messageKey("imobject.delete.deactivated", getDisplayName(object), object.getName())
                .help(help)
                .listener(() -> listener.alreadyDeactivated(object))
                .show();
    }

    /**
     * Invoked when deletion and deactivation of an object is not supported.
     *
     * @param object   the object
     * @param reason   reason the object couldn't be deleted. May be {@code null}
     * @param listener the listener to notify
     */
    @Override
    protected void unsupported(T object, String reason, IMObjectDeletionListener<T> listener) {
        String type = getDisplayName(object);
        String message = (reason == null)
                         ? Messages.format("imobject.delete.unsupported", getDisplayName(object))
                         : reason;
        ErrorDialog.newDialog()
                .titleKey("imobject.delete.title", type)
                .message(message)
                .listener(() -> listener.unsupported(object, reason))
                .show();
    }
}
