/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.im.edit.AbstractIMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.EntityResultSet;
import org.openvpms.web.component.im.query.FilteredResultSet;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.relationship.EntityLinkEditor;
import org.openvpms.web.component.property.Property;

import java.util.List;

/**
 * An editor for <em>entityLink.documentTemplateEmailAttachment</em>.
 * <p/>
 * This limits the target entity.documentTemplate to forms with static content.
 *
 * @author Tim Anderson
 */
public class EmailDocumentTemplateAttachmentEditor extends EntityLinkEditor {

    /**
     * Constructs an {@link EmailDocumentTemplateAttachmentEditor}.
     *
     * @param relationship the link
     * @param parent       the parent object
     * @param context      the layout context
     */
    public EmailDocumentTemplateAttachmentEditor(EntityLink relationship, IMObject parent, LayoutContext context) {
        super(relationship, parent, context);
    }

    /**
     * Creates a new editor for the relationship target.
     *
     * @param property the target property
     * @param context  the layout context
     * @return a new reference editor
     */
    @Override
    protected IMObjectReferenceEditor<Entity> createTargetEditor(Property property, LayoutContext context) {
        return new AbstractIMObjectReferenceEditor<Entity>(property, getObject(), context) {
            @Override
            protected Query<Entity> createQuery(String name) {
                StaticFormTemplateQuery query = new StaticFormTemplateQuery();
                query.setValue(name);
                return query;
            }
        };
    }

    private static class StaticFormTemplateQuery extends DocumentTemplateQuery {

        /**
         * Constructs a {@link StaticFormTemplateQuery}.
         */
        public StaticFormTemplateQuery() {
            setTemplateTypes(PatientArchetypes.DOCUMENT_FORM, CustomerArchetypes.DOCUMENT_FORM,
                             SupplierArchetypes.DOCUMENT_FORM);
        }

        /**
         * Determines if the query selects a particular object reference.
         *
         * @param reference the object reference to check
         * @return {@code true} if the object reference is selected by the query
         */
        @Override
        public boolean selects(Reference reference) {
            boolean result;
            ResultSet<Entity> set = super.createResultSet(null);
            if (set instanceof EntityResultSet) {
                EntityResultSet<Entity> entityResultSet = (EntityResultSet<Entity>) set;
                entityResultSet.setReferenceConstraint(reference);
                ResultSet<Entity> filteredResultSet = getFilteredResultSet(entityResultSet);
                result = filteredResultSet.hasNext();
            } else {
                result = false;
            }
            return result;
        }

        /**
         * Creates the result set.
         * </p/>
         * This excludes templates that don't have a PDF mime type.
         *
         * @param sort the sort criteria. May be {@code null}
         * @return a new result set
         */
        @Override
        protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
            return getFilteredResultSet(super.createResultSet(sort));
        }

        /**
         * Creates a result set to exclude templates that don't have a PDF mime type.
         *
         * @param set the underlying set
         * @return a filtered result set
         */
        private ResultSet<Entity> getFilteredResultSet(ResultSet<Entity> set) {
            return new FilteredResultSet<Entity>(set) {
                @Override
                protected void filter(Entity object, List<Entity> results) {
                    DocumentTemplate template = new DocumentTemplate(object, getService());
                    if (DocFormats.PDF_TYPE.equals(template.getMimeType())) {
                        results.add(object);
                    }
                }
            };
        }
    }
}