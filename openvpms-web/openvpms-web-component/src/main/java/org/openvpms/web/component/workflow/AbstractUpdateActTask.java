/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workflow;

import org.openvpms.component.model.act.Act;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Task to update an {@link Act} held in the {@link TaskContext}.
 * <p/>
 * This supports:
 * <li>
 *     <li>skipping the update, if the act has a particular status</li>
 *     <li>reloading the act if the update fails</li>
 * </li>
 *
 * @author Tim Anderson
 */
public class AbstractUpdateActTask<T extends Act> extends AbstractUpdateIMObjectTask<T> {

    /**
     * Statuses that prevent the act from being updated.
     */
    private final List<String> skipWhenStatuses = new ArrayList<>();

    /**
     * Constructs an {@link AbstractUpdateActTask}.
     *
     * @param archetype the archetype of the object to update
     * @param save      determines if the object should be saved
     */
    public AbstractUpdateActTask(String archetype, boolean save) {
        super(archetype, save, save);
    }

    /**
     * Constructs an {@link AbstractUpdateActTask}.
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with. May be {@code null}
     */
    public AbstractUpdateActTask(String archetype, TaskProperties properties) {
        this(archetype, properties, true);
    }

    /**
     * Constructs an {@link AbstractUpdateActTask}.
     *
     * @param archetype  the archetype of the object to update
     * @param properties properties to populate the object with. May be {@code null}
     * @param save       determines if the object should be saved
     */
    public AbstractUpdateActTask(String archetype, TaskProperties properties, boolean save) {
        super(archetype, properties, save, save);
    }

    /**
     * Sets the statuses that prevent the act from being updated.
     * <p/>
     * This adds to any existing statuses.
     *
     * @param statuses the statuses
     */
    public void skipWhenStatusIn(String... statuses) {
        skipWhenStatuses.addAll(Arrays.asList(statuses));
    }

    /**
     * Determines if the object can be updated.
     *
     * @param object  the object
     * @param context the task context
     * @return {@code true} if the act status is not a status skipped by {@link #skipWhenStatusIn(String...)},
     * otherwise {@code false}
     */
    protected boolean canUpdate(Act object, TaskContext context) {
        boolean result;
        if (!skipWhenStatuses.isEmpty()) {
            result = !skipWhenStatuses.contains(object.getStatus());
        } else {
            result = true;
        }
        return result;
    }
}