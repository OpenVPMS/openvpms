/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.SelectField;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.component.model.party.Party;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.web.component.bound.SpinBox;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Print dialog.
 *
 * @author Tim Anderson
 */
public class PrintDialog extends PopupDialog {

    /**
     * The preview button identifier.
     */
    protected static final String PREVIEW_ID = "button.preview";

    /**
     * The mail button identifier.
     */
    protected static final String MAIL_ID = "button.mail";

    /**
     * The printers.
     */
    private final SelectField printers;

    /**
     * Determines if the preview button should be added.
     */
    private final boolean preview;

    /**
     * Determines if the mail button should be added.
     */
    private final boolean mail;

    /**
     * The no. of copies to print.
     */
    private final SpinBox copies;


    /**
     * Constructs a {@link PrintDialog}.
     *
     * @param title    the window title
     * @param preview  if {@code true} add a 'preview' button
     * @param mail     if {@code true} add a 'mail' button
     * @param skip     if {@code true} display a 'skip' button that simply closes the dialog
     * @param location the current practice location. May be {@code null}
     * @param help     the help context. May be {@code null}
     */
    public PrintDialog(String title, boolean preview, boolean mail, boolean skip, Party location, HelpContext help) {
        super(title, "PrintDialog", (skip) ? OK_SKIP_CANCEL : OK_CANCEL, help);
        setModal(true);
        copies = new SpinBox(1, 99);
        printers = new PrinterField(getPrinters(location));
        this.preview = preview;
        this.mail = mail;

        FocusGroup parent = getFocusGroup();
        FocusGroup child = new FocusGroup("PrintDialog");
        child.add(printers);
        child.add(copies.getFocusGroup());
        parent.add(0, child); // insert before buttons
    }

    /**
     * Sets the default printer.
     *
     * @param printer the default printer. May be {@code null}
     */
    public void setDefaultPrinter(DocumentPrinter printer) {
        PrinterListModel model = (PrinterListModel) printers.getModel();
        PrinterReference reference = (printer != null) ?
                                     new PrinterReference(printer.getArchetype(), printer.getId()) : null;
        int index = model.indexOf(reference);
        if (index != -1) {
            printers.setSelectedIndex(index);
        }
    }

    /**
     * Returns the selected printer.
     *
     * @return the selected printer, or {@code null} if none is selected
     */
    public PrinterReference getPrinter() {
        return (PrinterReference) printers.getSelectedItem();
    }

    /**
     * Sets the number of copies to print.
     *
     * @param copies the number of copies to print
     */
    public void setCopies(int copies) {
        this.copies.setValue(copies);
    }

    /**
     * Returns the number of copies to print.
     *
     * @return the number of copies to print
     */
    public int getCopies() {
        return copies.getValue();
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING);
        doLayout(column);
        getLayout().add(ColumnFactory.create(Styles.INSET, column));
    }

    /**
     * Lays out the dialog.
     *
     * @param container the container
     */
    protected void doLayout(Component container) {
        if (preview) {
            addButton(PREVIEW_ID, this::onPreview);
        }
        if (mail) {
            addButton(MAIL_ID, this::onMail);
        }

        Grid grid = GridFactory.create(2);
        grid.add(LabelFactory.create("printdialog.printer"));
        grid.add(printers);
        grid.add(LabelFactory.create("printdialog.copies"));
        grid.add(copies);

        setFocus(copies);

        container.add(grid);
    }

    /**
     * Invoked when the preview button is pressed.
     * This implementation does nothing.
     */
    protected void onPreview() {
    }

    /**
     * Invoked when the mail button is pressed.
     * This implementation does nothing.
     */
    protected void onMail() {
    }

    /**
     * Returns the printers available at a practice location.
     *
     * @param location the practice location. May be {@code null}
     * @return the printers
     */
    protected List<PrinterReference> getPrinters(Party location) {
        List<PrinterReference> available = PrintHelper.getPrinters();
        List<PrinterReference> result;
        if (location != null) {
            LocationRules rules = ServiceHelper.getBean(LocationRules.class);
            Set<PrinterReference> locationSet = new HashSet<>(rules.getPrinters(location));
            if (!locationSet.isEmpty()) {
                Set<PrinterReference> set = new HashSet<>(available);
                PrinterReference defaultPrinter = rules.getDefaultPrinter(location);
                if (defaultPrinter != null) {
                    // add the default printer, if one is defined, but not listed as a location printer
                    locationSet = new HashSet<>(locationSet);
                    locationSet.add(defaultPrinter);
                }
                set.retainAll(locationSet);
                result = new ArrayList<>(set);
            } else {
                result = available;
            }
        } else {
            result = available;
        }
        return result;
    }

}
