/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;


/**
 * Viewer for {@link Reference}s of type <em>document.*</em>.
 *
 * @author Tim Anderson
 */
public class DocumentViewer {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The reference to view.
     */
    private final Reference reference;

    /**
     * The parent object. May be {@code null}
     */
    private final IMObject parent;

    /**
     * The document name. May be {@code null}
     */
    private final String name;

    /**
     * Determines if a hyperlink should be created, to enable downloads of
     * the document.
     */
    private final boolean link;

    /**
     * Determines if the document should be downloaded as a template.
     */
    private final boolean template;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The downloader.
     */
    private Downloader downloader;

    /**
     * Listener for downloader events.
     */
    private DownloaderListener listener;

    /**
     * Determines if a message should be displayed if no document is present.
     */
    private boolean showNoDocument = true;

    /**
     * The maximum file name display length, or {@code -1} to display the entire file name.
     */
    private int nameLength = -1;

    /**
     * Parent bean, if a parent is specified.
     */
    private IMObjectBean bean;

    /**
     * Document template node name.
     */
    private static final String DOCUMENT_TEMPLATE = "documentTemplate";


    /**
     * Constructs a {@link DocumentViewer}.
     *
     * @param act     the document act
     * @param link    if {@code true} enable an hyperlink to the object
     * @param context the layout context
     */
    public DocumentViewer(DocumentAct act, boolean link, LayoutContext context) {
        this(act, link, false, context);
    }

    /**
     * Constructs a {@link DocumentViewer}.
     *
     * @param act      the document act
     * @param link     if {@code true} enable a hyperlink to the object
     * @param template if {@code true}, display as a template, otherwise generate the document if required
     * @param context  the layout context
     */
    public DocumentViewer(DocumentAct act, boolean link, boolean template, LayoutContext context) {
        this(act.getDocument(), act, act.getFileName(), link, template, context);
    }

    /**
     * Constructs a {@link DocumentViewer}.
     *
     * @param reference the reference to view
     * @param parent    the parent. May be {@code null}
     * @param link      if {@code true} enable a hyperlink to the object
     * @param context   the layout context
     */
    public DocumentViewer(Reference reference, IMObject parent, boolean link, LayoutContext context) {
        this(reference, parent, null, link, false, context);
    }

    /**
     * Constructs a {@link DocumentViewer}.
     *
     * @param reference the reference to view
     * @param parent    the parent. May be {@code null}
     * @param link      if {@code true} enable a hyperlink to the object
     * @param template  if {@code true}, display as a template, otherwise generate the document if required
     * @param context   the layout context
     */
    public DocumentViewer(Reference reference, IMObject parent, boolean link, boolean template, LayoutContext context) {
        this(reference, parent, null, link, template, context);
    }

    /**
     * Constructs a {@link DocumentViewer}.
     *
     * @param reference the reference to view. May be {@code null}
     * @param parent    the parent. May be {@code null}
     * @param name      the document file name. May be {@code null}
     * @param link      if {@code true} enable a hyperlink to the object
     * @param template  if {@code true}, display as a template, otherwise generate the document if required
     * @param context   the layout context
     */
    public DocumentViewer(Reference reference, IMObject parent, String name, boolean link, boolean template,
                          LayoutContext context) {
        this.reference = reference;
        this.parent = parent;
        this.service = ServiceHelper.getArchetypeService();
        if (name != null) {
            this.name = name;
        } else if (parent instanceof DocumentAct) {
            this.name = ((DocumentAct) parent).getFileName();
        } else if (reference != null) {
            this.name = DescriptorHelper.getDisplayName(reference.getArchetype(), service);
        } else {
            this.name = null;
        }
        this.link = link;
        this.template = template;
        this.context = context;
    }

    /**
     * Registers a listener for download events.
     * <p>
     * This enables download events to be intercepted. Only applicable if {@code link} was specified at construction.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setDownloadListener(DownloaderListener listener) {
        if (downloader != null) {
            downloader.setListener(listener);
        } else {
            this.listener = listener;
        }
    }

    /**
     * Determines if a message should be displayed if no document is present.
     *
     * @param show if {@code true} show a message, otherwise leave the component blank. Defaults to {@code true}
     */
    public void setShowNoDocument(boolean show) {
        showNoDocument = show;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        Component result = getDocument();
        if (result == null) {
            // no document, so render a placeholder
            result = getPlaceholder();
        }
        return result;
    }

    /**
     * Shortens long names to the specified number of characters.
     *
     * @param length the maximum length
     */
    public void setNameLength(int length) {
        if (downloader != null) {
            downloader.setNameLength(length);
        } else {
            this.nameLength = length;
        }
    }

    /**
     * Creates a component representing the document.
     *
     * @return the component, or {@code null} if there is no associated document
     */
    protected Component getDocument() {
        Component result = null;
        boolean hasDoc = false;
        if (reference != null) {
            hasDoc = true;
        } else if (parent != null) {
            //  if there is a document template, the document can be generated
            IMObjectBean bean = getBean();
            hasDoc = bean.hasNode(DOCUMENT_TEMPLATE);
        }
        if (hasDoc) {
            if (link) {
                downloader = getDownloader();
                result = downloader.getComponent();
            } else {
                Label label = LabelFactory.create();
                label.setText(name);
                result = label;
            }
        }
        return result;
    }

    /**
     * Creates a placeholder if there is no document.
     *
     * @return a placeholder
     */
    protected Component getPlaceholder() {
        Component result;
        Label label = LabelFactory.create();
        if (showNoDocument) {
            label.setText(Messages.get("document.none"));
        }
        result = label;
        return result;
    }

    /**
     * Returns the parent of the document.
     *
     * @return the parent. May be {@code null}
     */
    protected IMObject getParent() {
        return parent;
    }

    /**
     * Returns a downloader for the document.
     *
     * @return a downloader
     */
    protected Downloader getDownloader() {
        Downloader result = createDownloader();
        result.setNameLength(nameLength);
        result.setListener(listener);
        return result;
    }

    /**
     * Creates a new downloader.
     *
     * @return a new downloader
     */
    protected Downloader createDownloader() {
        Downloader result;
        if (parent instanceof DocumentAct) {
            result = new DocumentActDownloader((DocumentAct) parent, template, context.getContext(),
                                               ServiceHelper.getBean(FileNameFormatter.class),
                                               ServiceHelper.getBean(ReportFactory.class));
        } else {
            result = new DocumentRefDownloader(reference, name, getContext().getContext().getUser());
        }
        return result;
    }

    /**
     * Determines if clickable links should be displayed.
     *
     * @return {@code true} if clickable links should be displayed, {@code false} if only labels should be displayed
     */
    protected boolean link() {
        return link;
    }

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return context;
    }

    /**
     * Returns the parent bean.
     *
     * @return the bean, or {@code null} if no parent was supplied
     */
    protected IMObjectBean getBean() {
        if (bean == null && parent != null) {
            bean = service.getBean(parent);
        }
        return bean;
    }
}
