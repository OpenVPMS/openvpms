/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.select;

import org.openvpms.component.model.object.IMObject;

/**
 * A {@link Selector} for {@link IMObject}s.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectSelector<T extends IMObject> extends Selector<T> {

    /**
     * Constructs an {@link AbstractIMObjectSelector}.
     * <p/>
     * Displays button(s) to the left of the object display.
     *
     * @param buttonId the button identifier
     */
    public AbstractIMObjectSelector(String buttonId) {
        super(buttonId);
    }

    /**
     * Constructs an {@link AbstractIMObjectSelector}.
     *
     * @param style    determines the layout of the button(s)
     * @param editable determines if the selector is editable
     */
    public AbstractIMObjectSelector(ButtonStyle style, boolean editable) {
        super(style, editable);
    }

    /**
     * Constructs an {@link AbstractIMObjectSelector}.
     *
     * @param buttonId        the button identifier
     * @param style           determines the layout of the button(s)
     * @param editable        determines if the selector is editable
     * @param enableShortcuts if {@code true}, include button shortcuts
     */
    public AbstractIMObjectSelector(String buttonId, ButtonStyle style, boolean editable, boolean enableShortcuts) {
        super(buttonId, style, editable, enableShortcuts);
    }

    /**
     * Sets the current object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(T object) {
        if (object != null) {
            setObject(object.getName(), object.getDescription(), object.isActive());
        } else {
            setObject(null, null, true);
        }
    }
}
