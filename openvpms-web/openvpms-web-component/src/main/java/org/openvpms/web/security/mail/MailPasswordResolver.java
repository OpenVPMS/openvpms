/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.mail;

import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;

import static org.openvpms.archetype.rules.practice.MailServer.AuthenticationMethod.OAUTH2;

/**
 * Determines the password for a {@link MailServer}.
 *
 * @author Tim Anderson
 */
public class MailPasswordResolver {

    /**
     * The OAuth2 authorized client manager.
     */
    private final OAuth2AuthorizedClientManager clientManager;

    /**
     * The password encryptor.
     */
    private final PasswordEncryptor encryptor;

    /**
     * Constructs a {@link MailPasswordResolver}.
     *
     * @param clientManager the OAuth2 authorized client manager
     * @param encryptor     the password encryptor
     */
    public MailPasswordResolver(OAuth2AuthorizedClientManager clientManager, PasswordEncryptor encryptor) {
        this.clientManager = clientManager;
        this.encryptor = encryptor;
    }

    /**
     * Returns the decrypted password.
     * <p/>
     * For OAuth2, this will request a new token via the client manager, if the existing token has expired.
     *
     * @param settings the mail server settings
     * @return the decrypted password. May be {@code null} if no password was supplied
     */
    public String getPassword(MailServer settings) {
        String result;
        if (settings.getAuthenticationMethod() == OAUTH2) {
            OAuth2AuthorizeRequest request =
                    OAuth2AuthorizeRequest.withClientRegistrationId(settings.getOauthClientRegistrationId())
                            .principal(settings.getUsername())
                            .build();
            OAuth2AuthorizedClient client = clientManager.authorize(request);
            result = (client != null) ? client.getAccessToken().getTokenValue() : null;
        } else {
            String password = settings.getPassword();
            result = (password != null) ? encryptor.decrypt(password) : null;
        }
        return result;
    }
}