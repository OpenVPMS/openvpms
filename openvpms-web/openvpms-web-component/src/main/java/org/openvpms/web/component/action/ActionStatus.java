/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

/**
 * The status of an {@link Action}.
 *
 * @author Tim Anderson
 */
public class ActionStatus {

    /**
     * The status.
     */
    private final Status status;

    /**
     * The failure reason, if the action wasn't successful.
     */
    private final FailureReason reason;

    /**
     * Determines if the user has been notified.
     */
    private boolean notified;

    /**
     * Constructs a {@link ActionStatus}.
     *
     * @param status the status
     */
    private ActionStatus(Status status) {
        this(status, null);
    }

    /**
     * Constructs a {@link ActionStatus}.
     *
     * @param status the status
     * @param reason the failure reason. May be {@code null}
     */
    private ActionStatus(Status status, FailureReason reason) {
        this.status = status;
        this.reason = reason;
    }

    /**
     * Determines if the action succeeded.
     *
     * @return {@code true} if the action succeeded, {@code false} if it failed
     */
    public boolean succeeded() {
        return status == Status.SUCCESS;
    }

    /**
     * Determines if the action was skipped, due to a precondition not being met.
     *
     * @return {@code true} if the action was skipped
     */
    public boolean skipped() {
        return status == Status.SKIPPED;
    }

    /**
     * Determines if the action failed.
     *
     * @return {@code true} if the action failed, {@code false} if it succeeded
     */
    public boolean failed() {
        return status == Status.FAILED;
    }

    /**
     * Determines if the action can be retried.
     *
     * @return {@code true} if the action can be retried, otherwise {@code false}
     */
    public boolean canRetry() {
        return status == Status.RETRY;
    }

    /**
     * Returns the error message.
     * <p>
     * Only applicable if the action didn't succeed.
     *
     * @return the error message. May be {@code null}
     */
    public String getMessage() {
        return (reason != null) ? reason.getMessage() : null;
    }

    /**
     * Returns the exception.
     * <p>
     * Only applicable if the action didn't succeed.
     *
     * @return the exception. May be {@code null}
     */
    public Throwable getException() {
        return reason != null ? reason.getException() : null;
    }

    /**
     * Determines if the user has been notified.
     *
     * @return {@code true} if the user has been notified, otherwise {@code false}
     */
    public boolean notified() {
        return notified;
    }

    /**
     * Indicates that the user has been notified.
     */
    public void setNotified() {
        this.notified = true;
    }

    /**
     * Returns the failure reason.
     *
     * @return the failure reason, or {@code null} if the action was successful
     */
    public FailureReason getReason() {
        return reason;
    }

    /**
     * Returns a string representation of the status.
     *
     * @return a string representation of the status
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("status=").append(status);
        if (reason != null) {
            result.append(",reason=").append(reason);
        }
        return result.toString();
    }

    /**
     * Creates a status indicating that the action was skipped due to a precondition not being met.
     *
     * @param reason the reason for the action being skipped
     * @return a new status
     */
    public static ActionStatus skipped(FailureReason reason) {
        return new ActionStatus(Status.SKIPPED, reason);
    }

    /**
     * Creates a status indicating that the action ran successfully.
     *
     * @return a new status
     */
    public static ActionStatus success() {
        return new ActionStatus(Status.SUCCESS);
    }

    /**
     * Creates a status indicating that the action failed to run, but may be retried.
     *
     * @param exception the cause
     * @return a new status
     */
    public static ActionStatus retry(Throwable exception) {
        return retry(FailureReason.exception(exception));
    }

    /**
     * Creates a status indicating that the action failed to run, but may be retried.
     *
     * @param reason the reason for the failure
     * @return a new status
     */
    public static ActionStatus retry(FailureReason reason) {
        return new ActionStatus(Status.RETRY, reason);
    }

    /**
     * Creates a status indicating that the action failed to run and shouldn't be retried.
     *
     * @param message the error message
     * @return a new status
     */
    public static ActionStatus failed(String message) {
        return failed(FailureReason.error(message));
    }

    /**
     * Creates a status indicating that the action failed to run and shouldn't be retried.
     *
     * @param exception the cause
     * @return a new status
     */
    public static ActionStatus failed(Throwable exception) {
        return failed(FailureReason.exception(exception));
    }

    /**
     * Creates a status indicating that the action failed to run and shouldn't be retried.
     *
     * @param reason the reason for the failure
     * @return a new status
     */
    public static ActionStatus failed(FailureReason reason) {
        return new ActionStatus(Status.FAILED, reason);
    }

    /**
     * Creates a status indicating that the action failed and can't be retried.
     *
     * @param status the original status
     * @return a new status
     */
    public static ActionStatus failed(ActionStatus status) {
        return new ActionStatus(Status.FAILED, status.getReason());
    }

    enum Status {
        SUCCESS,  // ran successfully
        SKIPPED,  // a precondition was not met, so the action was skipped
        RETRY,    // failed to run, but may be retried
        FAILED    // failed to run and shouldn't be retried
    }
}