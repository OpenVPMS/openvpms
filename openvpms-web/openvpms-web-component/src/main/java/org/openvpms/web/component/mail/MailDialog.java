/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import echopointng.KeyStrokeListener;
import echopointng.KeyStrokes;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import nextapp.echo2.app.filetransfer.UploadListener;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.doc.DocumentUploadListener;
import org.openvpms.web.component.im.doc.UploadDialog;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserDialog;
import org.openvpms.web.component.im.query.BrowserFactory;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryFactory;
import org.openvpms.web.component.macro.MacroDialog;
import org.openvpms.web.component.mail.AttachmentGenerator.Result;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.button.ShortcutButtons;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.Vetoable;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.focus.FocusCommand;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Collection;
import java.util.Iterator;

import static org.openvpms.archetype.rules.doc.DocumentArchetypes.USER_EMAIL_TEMPLATE;


/**
 * Dialog to send emails.
 *
 * @author Tim Anderson
 */
public class MailDialog extends ModalDialog {

    /**
     * Send button identifier.
     */
    public static final String SEND_ID = "send";

    /**
     * The mail editor.
     */
    private final MailEditor editor;

    /**
     * The attachment browser. May be {@code null}
     */
    private final AttachmentBrowser attachments;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Edit button identifier.
     */
    private static final String EDIT_ID = "button.edit";

    /**
     * Template button identifier.
     */
    private static final String TEMPLATE_ID = "button.template";

    /**
     * Don't send button identifier.
     */
    private static final String DONT_SEND_ID = "button.dontSend";

    /**
     * Attach button identifier.
     */
    private static final String ATTACH_ID = "button.attach";

    /**
     * Attach file button identifier.
     */
    private static final String ATTACH_FILE_ID = "button.attachFile";

    /**
     * The editor button identifiers.
     */
    private static final String[] NEW_SEND_ATTACH_ALL_CANCEL = {SEND_ID, TEMPLATE_ID, ATTACH_ID, ATTACH_FILE_ID,
                                                                CANCEL_ID};

    /**
     * The editor button identifiers.
     */
    private static final String[] NEW_SEND_ATTACH_FILE_CANCEL = {SEND_ID, TEMPLATE_ID, ATTACH_FILE_ID, CANCEL_ID};

    /**
     * The cancel confirmation button identifiers.
     */
    private static final String[] EDIT_DONT_SEND = {EDIT_ID, DONT_SEND_ID};


    /**
     * Constructs a {@link MailDialog}.
     *
     * @param mailContext the mail context
     * @param preferred   the preferred contact. May be {@code null}
     * @param context     the layout context
     */
    public MailDialog(MailContext mailContext, Contact preferred, LayoutContext context) {
        this(mailContext, preferred, mailContext.createAttachmentBrowser(), context);
    }

    /**
     * Constructs a {@link MailDialog}.
     *
     * @param mailContext the mail context
     * @param preferred   the preferred contact. May be {@code null}
     * @param attachments the attachment browser. May be {@code null}
     * @param context     the layout context
     */
    public MailDialog(MailContext mailContext, Contact preferred, AttachmentBrowser attachments,
                      LayoutContext context) {
        this(Messages.get("mail.title"), mailContext, preferred, attachments, context);
    }

    /**
     * Constructs a {@link MailDialog}.
     *
     * @param title       the window title
     * @param mailContext the mail context
     * @param preferred   the preferred contact to display. May be {@code null}
     * @param attachments the attachment browser. May be {@code null}
     * @param context     the layout context
     */
    public MailDialog(String title, MailContext mailContext, Contact preferred, AttachmentBrowser attachments,
                      LayoutContext context) {
        this(title, new MailEditor(mailContext, preferred, context), attachments, context);
    }

    /**
     * Constructs a {@link MailDialog}.
     *
     * @param editor      the mail editor
     * @param attachments the attachment browser. May be {@code null}
     * @param context     the layout context
     */
    public MailDialog(MailEditor editor, AttachmentBrowser attachments, LayoutContext context) {
        this(Messages.get("mail.title"), editor, attachments, context);
    }

    /**
     * Constructs a {@link MailDialog}.
     *
     * @param title       the window title
     * @param editor      the mail editor
     * @param attachments the attachment browser. May be {@code null}
     * @param context     the layout context
     */
    public MailDialog(String title, MailEditor editor, AttachmentBrowser attachments, LayoutContext context) {
        super(title, "MailDialog", attachments != null ? NEW_SEND_ATTACH_ALL_CANCEL : NEW_SEND_ATTACH_FILE_CANCEL,
              context.getHelpContext());
        setDefaultCloseAction(CANCEL_ID);
        this.attachments = attachments;
        this.context = context;
        this.editor = editor;
        this.service = ServiceHelper.getArchetypeService();

        getLayout().add(editor.getComponent());
        getFocusGroup().add(editor.getFocusGroup());
        setCancelListener(this::onCancel);
        ButtonSet buttons = getButtons();
        buttons.addKeyListener(KeyStrokes.ALT_MASK | KeyStrokes.VK_M, new ActionListener() {
            public void onAction(ActionEvent event) {
                onMacro();
            }
        });
        registerShortcuts();
        editor.getFocusGroup().setFocus();
    }

    /**
     * Returns the mail editor.
     *
     * @return the mail editor
     */
    public MailEditor getMailEditor() {
        return editor;
    }

    /**
     * Invoked just prior to the dialog closing.
     */
    @Override
    protected void onClosing() {
        try {
            editor.dispose();
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Creates the layout split pane.
     *
     * @return a new split pane
     */
    @Override
    protected SplitPane createSplitPane() {
        return SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL, "PopupWindow.Layout");
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (TEMPLATE_ID.equals(button)) {
            newFromTemplate();
        } else if (ATTACH_ID.equals(button)) {
            attach();
        } else if (ATTACH_FILE_ID.equals(button)) {
            attachFile();
        } else if (SEND_ID.equals(button)) {
            if (send()) {
                close(SEND_ID);
            }
        } else {
            super.onButton(button);
        }
    }

    /**
     * Registers any dialog keyboard shortcuts directly on the mail editor.
     * <p>
     * This is required as the events would otherwise be swallowed by the editor.
     */
    protected void registerShortcuts() {
        ShortcutButtons buttons = getButtons().getButtons();
        KeyStrokeListener listener = editor.getKeyStrokeListener();
        KeyStrokeListener shortcutListener = buttons.getKeyStrokeListener();
        int[] keys = shortcutListener.getKeyCombinations();
        if (keys.length > 0) {
            for (int key : keys) {
                String command = shortcutListener.getKeyCombinationCommand(key);
                if (command != null) {
                    listener.addKeyCombination(key, command);
                } else {
                    listener.addKeyCombination(key);
                }
            }
            // register a listener to pass events through to the dialog
            listener.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    buttons.processInput(event);
                }
            });
        }
    }

    /**
     * Creates a new {@link Mailer}.
     *
     * @param editor the mail editor
     * @return a new mailer
     */
    protected Mailer createMailer(MailEditor editor) {
        return ServiceHelper.getBean(MailerFactory.class).create(editor.getMailContext());
    }

    /**
     * Sends the email.
     *
     * @param mailer the mailer
     */
    protected void send(Mailer mailer) {
        mailer.send();
    }

    /**
     * Populates a mailer from an editor.
     *
     * @param mailer the mailer
     * @param editor the editor
     */
    protected void populate(Mailer mailer, MailEditor editor) {
        mailer.setFrom(editor.getFrom());
        mailer.setTo(editor.getTo());
        mailer.setCc(editor.getCc());
        mailer.setBcc(editor.getBcc());
        mailer.setSubject(editor.getSubject());
        mailer.setBody(editor.getMessage());
        for (Reference attachment : editor.getAttachments()) {
            Document document = service.get(attachment, Document.class);
            if (document != null) {
                mailer.addAttachment(document);
            }
        }
    }

    /**
     * Displays a popup of available email templates.
     * <p>
     * If there is already text presents, displays a  warning before proceeding.
     */
    private void newFromTemplate() {
        if (!StringUtils.isBlank(editor.getMessage())) {
            ConfirmationDialog.show(Messages.get("mail.replace.title"), Messages.get("mail.replace.message"),
                                    ConfirmationDialog.YES_NO, new PopupDialogListener() {
                        @Override
                        public void onYes() {
                            onTemplate();
                        }
                    });
        } else {
            onTemplate();
        }
    }

    /**
     * Displays a popup of available email templates to select from.
     */
    private void onTemplate() {
        Query<Entity> query = QueryFactory.create(USER_EMAIL_TEMPLATE, context.getContext());
        Browser<Entity> browser = BrowserFactory.create(query, context);
        String title = Messages.format("imobject.select.title", DescriptorHelper.getDisplayName(
                USER_EMAIL_TEMPLATE, service));
        BrowserDialog<Entity> dialog = new BrowserDialog<>(title, browser, getHelpContext().subtopic("template"));
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                Entity selected = dialog.getSelected();
                if (selected != null) {
                    editor.setContent(new EmailTemplate(selected, service), true);
                }
            }
        });
        dialog.show();
    }

    /**
     * Attaches documents from the attachment browser.
     */
    private void attach() {
        FocusCommand focus = new FocusCommand();
        HelpContext help = getHelpContext().subtopic("attach");
        AttachmentDialog dialog = new AttachmentDialog(attachments, help);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            public void onClose(WindowPaneEvent event) {
                focus.restore();
                if (BrowserDialog.OK_ID.equals(dialog.getAction())) {
                    MailDialog.this.attach(attachments.getSelections());
                }
                attachments.clearSelections();
            }
        });
        dialog.show();
    }

    /**
     * Attaches documents.
     *
     * @param documents the documents to attach
     */
    private void attach(Collection<MailAttachment> documents) {
        AttachmentGeneratorFactory factory = ServiceHelper.getBean(AttachmentGeneratorFactory.class);
        AttachmentGenerator generator = factory.create(context.getContext(), getHelpContext());
        attach(documents.iterator(), generator);
    }

    /**
     * Attaches documents.
     * <br/>
     * This operation is asynchronous, so the method is called recursively until they have all been processed.<br/>
     * It aborts on first failure.
     *
     * @param iterator  iterator over the acts
     * @param generator the attachment generator
     */
    private void attach(Iterator<MailAttachment> iterator, AttachmentGenerator generator) {
        if (iterator.hasNext()) {
            generator.generate(iterator.next(), result -> {
                if (result.getDocument() != null) {
                    try {
                        editor.addAttachment(result.getDocument());
                        attach(iterator, generator); // attach the next document
                    } catch (Exception exception) {
                        attachError(Result.error(exception));
                    }
                } else {
                    attachError(result);
                }
            });
        }
    }

    /**
     * Handles an attachment error. This displays the error. If the error was due to an exception, an error reporting
     * dialog may be displayed to report the error.
     *
     * @param result the attachment result
     */
    private void attachError(Result result) {
        if (result.getCause() != null) {
            // will display an error reporting dialog if necessary
            ErrorHelper.show(Messages.get("mail.attach.error"), result.getCause());
        } else {
            ErrorHelper.show(Messages.get("mail.attach.error"), result.getError());
        }
    }

    /**
     * Attaches a file.
     */
    private void attachFile() {
        FocusCommand focus = new FocusCommand();
        UploadListener listener = new DocumentUploadListener() {
            protected void upload(Document document) {
                focus.restore();
                editor.addAttachment(document);
            }
        };
        UploadDialog dialog = new UploadDialog(listener, getHelpContext().subtopic("attachFile"));
        dialog.show();
    }

    /**
     * Sends the email.
     *
     * @return {@code true} if the mail was sent
     */
    private boolean send() {
        boolean result = false;
        try {
            Validator validator = new DefaultValidator();
            if (editor.validate(validator)) {
                Mailer mailer = createMailer(editor);
                populate(mailer, editor);
                send(mailer);
                result = true;
            } else {
                ValidationHelper.showError(Messages.get("mail.error.title"), validator, "mail.error.message", false);
            }
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        return result;
    }

    /**
     * Invoked when the 'cancel' button is pressed. This prompts for confirmation if the editor has a message body
     * or attachments.
     *
     * @param action the action to veto if cancel is selected
     */
    private void onCancel(final Vetoable action) {
        if (!editor.getAttachments().isEmpty() || !StringUtils.isEmpty(editor.getMessage())) {
            final ConfirmationDialog dialog = new ConfirmationDialog(Messages.get("mail.cancel.title"),
                                                                     Messages.get("mail.cancel.message"),
                                                                     EDIT_DONT_SEND);
            dialog.addWindowPaneListener(new WindowPaneListener() {
                public void onClose(WindowPaneEvent e) {
                    action.veto(EDIT_ID.equals(dialog.getAction()));
                }
            });
            dialog.show();
        } else {
            action.veto(false);
        }
    }

    /**
     * Displays the macros.
     */
    private void onMacro() {
        MacroDialog dialog = new MacroDialog(context.getContext(), getHelpContext());
        dialog.show();
    }

}
