/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.error;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import static org.openvpms.web.echo.style.Styles.CELL_SPACING;
import static org.openvpms.web.echo.style.Styles.LARGE_INSET;
import static org.openvpms.web.echo.style.Styles.WIDE_CELL_SPACING;


/**
 * An {@link ErrorDialog} that provides the option to report errors back to OpenVPMS.
 *
 * @author Tim Anderson
 */
public class ErrorReportingDialog extends ErrorDialog {

    /**
     * The logger.
     */
    private final Logger log = LoggerFactory.getLogger(ErrorReportingDialog.class);

    /**
     * The error reporter.
     */
    private ErrorReporter reporter;

    /**
     * The error report, or {@code null} if the exception isn't reportable.
     */
    private ErrorReport report;

    /**
     * Constructs an {@link ErrorReportingDialog}.
     *
     * @param builder the builder
     */
    ErrorReportingDialog(ErrorReportingDialogBuilder builder) {
        super(builder);
        if (builder.getCause() != null) {
            report(builder.getMessage(), builder.isMessageHTML(), builder.getCause());
        }
    }

    /**
     * Determines if error reporting is supported.
     *
     * @return {@code true} if error reporting is supported, otherwise {@code false}
     */
    public static boolean canReportErrors() {
        boolean result = false;
        if (ApplicationInstance.getActive() instanceof ContextApplicationInstance) {
            ApplicationContext context = ServiceHelper.getContext();
            result = context.containsBean("errorReporter");
        }
        return result;
    }

    /**
     * Constructs an {@link ErrorReportingDialogBuilder}.
     *
     * @return a new builder
     */
    public static ErrorReportingDialogBuilder newDialog() {
        return new ErrorReportingDialogBuilder();
    }

    /**
     * Report an exception, if the {@link ErrorReporter} indicates it is reportable.
     *
     * @param message   the message
     * @param html      if {@code true}, the message is an HTML fragment, else it is plain text
     * @param exception the exception
     */
    private void report(String message, boolean html, Throwable exception) {
        ApplicationContext context = ServiceHelper.getContext();
        if (context.containsBean("errorReporter")) {
            reporter = context.getBean("errorReporter", ErrorReporter.class);
            if (reporter.isReportable(exception)) {
                if (html) {
                    message = StringEscapeUtils.unescapeHtml4(message);
                }
                report = new ErrorReport(message, exception);
                addButton("errorreportdialog.report", this::reportError);
            }
        }
    }

    /**
     * Pops up a dialog to report the error.
     */
    private void reportError() {
        SendReportDialog dialog = new SendReportDialog();
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                doReport(dialog.getFrom());
            }

            @Override
            public void onCancel() {
                ErrorReportingDialog.this.onCancel();
            }
        });
        dialog.show(true);
    }

    /**
     * Sends the report and closes the dialog.
     *
     * @param from the from address
     */
    private void doReport(String from) {
        reporter.report(report, from);
        onOK();
    }

    /**
     * Shows the report in a new dialog.
     */
    private void showReport() {
        try {
            String xml = report.toXML();
            InformationDialog dialog = InformationDialog.newDialog()
                    .title(Messages.get("errorreportdialog.showtitle"))
                    .message(xml)
                    .build();
            dialog.show(true);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
            ErrorDialog.show(exception);
        }
    }

    /**
     * Confirmation dialog that prompts to send the report.
     */
    private class SendReportDialog extends ConfirmationDialog {

        /**
         * The from field.
         */
        private final TextField from;

        /**
         * Constructs a {@link SendReportDialog}.
         */
        SendReportDialog() {
            super(Messages.get("errorreportdialog.title"), Messages.get("errorreportdialog.message"), new String[0]);
            from = TextComponentFactory.create(40);
            addButton("errorreportdialog.send", this::onOK);
            addButton("errorreportdialog.nosend", this::onCancel);

            PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
            Party practice = practiceService.getPractice();
            if (practice != null) {
                PartyRules partyRules = ServiceHelper.getBean(CustomerRules.class);
                String email = partyRules.getEmailAddress(practice);
                from.setText(email);
            }
        }

        /**
         * Returns the from email address.
         *
         * @return the from email address
         */
        public String getFrom() {
            return StringUtils.trimToNull(from.getText());
        }

        /**
         * Invoked when the OK button is pressed.
         * <p/>
         * Validates the email address. If valid, closes the dialog.
         */
        @Override
        protected void onOK() {
            boolean valid = false;
            String from = getFrom();
            if (from == null) {
                ErrorDialog.show(Messages.format("errorreportdialog.emailrequired"));
            } else {
                try {
                    new InternetAddress(from, true);
                    valid = true;
                } catch (AddressException exception) {
                    ErrorDialog.show(Messages.format("errorreportdialog.invalidemail", from));
                }
            }
            if (valid) {
                super.onOK();
            }
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Label message = LabelFactory.create(true);
            message.setText(getMessage());
            Button show = ButtonFactory.create("errorreportdialog.showlink", "hyperlink",
                                               ErrorReportingDialog.this::showReport);
            from.setStyleName(Styles.EDIT);
            Label email = LabelFactory.create("errorreportdialog.email");
            Label content = LabelFactory.create("errorreportdialog.show");
            Column column = ColumnFactory.create(WIDE_CELL_SPACING, message,
                                                 RowFactory.create(CELL_SPACING, email, from),
                                                 RowFactory.create(CELL_SPACING, content, show));
            getLayout().add(ColumnFactory.create(LARGE_INSET, column));
        }
    }

}
