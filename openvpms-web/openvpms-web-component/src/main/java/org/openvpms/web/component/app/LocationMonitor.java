/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.openvpms.component.model.party.Party;

import java.util.function.Consumer;

/**
 * Monitors for location changes in the {@link GlobalContext}.
 *
 * @author Tim Anderson
 */
public class LocationMonitor {

    /**
     * The global context.
     */
    private final GlobalContext context;

    /**
     * The context listener.
     */
    private final ContextListener listener;

    /**
     * The location id.
     */
    private long id;

    /**
     * Determines if the listener is registered,
     */
    private boolean registered;

    /**
     * Constructs a {@link LocationMonitor}.
     *
     * @param context  the global context
     * @param callback the callback to invoke when the location changes
     */
    public LocationMonitor(GlobalContext context, Consumer<Party> callback) {
        this.context = context;
        id = getLocationId();
        if (callback != null) {
            listener = (key, value) -> callback.accept((Party) value);
        } else {
            listener = null;
        }
    }

    /**
     * Registers the listener for location changes.
     */
    public void register() {
        if (!registered) {
            context.addListener(Context.LOCATION_SHORTNAME, listener);
            registered = true;
        }
    }

    /**
     * Unregisters the listener for location changes.
     */
    public void unregister() {
        if (registered) {
            context.removeListener(listener);
            registered = false;
        }
    }

    /**
     * Determines if the location has changed since the last time this method was called.
     * <p/>
     * For the very first call, the current location is compared with that at construction.
     *
     * @return {@code true} if the location has changed
     */
    public boolean changed() {
        long current = id;
        id = getLocationId();
        return current != id;
    }

    /**
     * Returns the current location.
     *
     * @return the current location. May be {@code null}
     */
    public Party getLocation() {
        return context.getLocation();
    }

    /**
     * Returns the current location id.
     *
     * @return the current location id, or {@code -1} if there is none
     */
    private long getLocationId() {
        Party location = getLocation();
        return location != null ? location.getId() : -1;
    }

}
