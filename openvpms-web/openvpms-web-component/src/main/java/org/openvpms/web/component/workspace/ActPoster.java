/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.workspace;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.action.ActionStatus;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

import java.util.function.Consumer;

/**
 * Helper to post an act.
 * <p/>
 * This verifies that the act can be posted
 *
 * @author Tim Anderson
 */
public class ActPoster<T extends Act> {

    /**
     * The act to post.
     */
    private final T act;

    /**
     * The actions that determine if the act can be posted or not.
     */
    private final ActActions<T> actions;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The action factory.
     */
    private final ActionFactory factory;

    /**
     * Constructs an {@link ActPoster}.
     *
     * @param act     the act to post
     * @param actions the actions that determine if the act can be posted or not
     * @param help    the post help context
     */
    public ActPoster(T act, ActActions<T> actions, HelpContext help) {
        this.act = act;
        this.actions = actions;
        this.help = help;
        factory = ServiceHelper.getBean(ActionFactory.class);
    }

    /**
     * Prompts the user to post the act, posting it if they confirm.
     * <p/>
     * The listener will be notified with the act on completion or cancellation. The act will be
     *
     * @param listener the listener to notify
     */
    public void post(Consumer<T> listener) {
        factory.newAction()
                .withObject(act)
                .call((object, completionListener) -> {
                    if (actions.canPost(object)) {
                        confirmPost(object, help, completionListener);
                    } else {
                        // silently ignore if the act cannot be posted
                        completionListener.accept(ActionStatus.success());
                    }
                })
                .onFailure(status -> listener.accept(act))
                .onSuccess(() -> listener.accept(act))
                .runOnce();
    }

    /**
     * Confirms that the user wants to post the act, posting it if accepted.
     *
     * @param act      the act to post
     * @param help     the help context
     * @param listener the listener to notify on completion
     */
    protected void confirmPost(T act, HelpContext help, Consumer<ActionStatus> listener) {
        String displayName = DescriptorHelper.getDisplayName(act, ServiceHelper.getArchetypeService());
        ConfirmationDialog.newDialog()
                .titleKey("act.post.title", displayName)
                .messageKey("act.post.message", displayName)
                .help(help)
                .okCancel()
                .ok(() -> postConfirmed(act, listener))
                .cancel(() -> listener.accept(ActionStatus.success()))
                .show();
    }

    /**
     * Invoked when the user confirms to post. This verifies that the act can still be posted, before delegating
     * to {@link #post(Act, Consumer)}.
     *
     * @param act the act to post
     * @param listener the listener to notify on completion
     */
    protected void postConfirmed(T act, Consumer<ActionStatus> listener) {
        try {
            if (actions.canPost(act)) {
                post(act, listener);
            } else {
                listener.accept(ActionStatus.success());
            }
        } catch (Throwable exception) {
            listener.accept(ActionStatus.failed(exception));
        }
    }

    /**
     * Posts the act.
     *
     * @param act      the act to post
     * @param listener the listener to notify on completion
     */
    protected void post(T act, Consumer<ActionStatus> listener) {
        actions.post(act);
        listener.accept(ActionStatus.success());
    }
}