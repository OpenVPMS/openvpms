/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.layout.RowLayoutData;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.sms.SMSDialog;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;


/**
 * A {@link IMObjectLayoutStrategy} for <em>contact.phoneNumber</em> that enables SMS messages to be sent to mobiles.
 *
 * @author Tim Anderson
 */
public class PhoneContactViewLayout extends AbstractContactViewLayout {

    /**
     * The nodes to display. Exclude the area code as a formatted number is displayed.
     */
    private final static ArchetypeNodes nodes = new ArchetypeNodes().exclude("areaCode").excludeIfEmpty("purposes");

    /**
     * The telephone number node.
     */
    private static final String TELEPHONE_NUMBER = "telephoneNumber";

    /**
     * Constructs a {@link PhoneContactViewLayout}.
     */
    public PhoneContactViewLayout() {
        setArchetypeNodes(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        PhoneLink link = new PhoneLink(ServiceHelper.getBean(CustomerRules.class), getService(), -1);
        Component phone = link.getLink((Contact) object, false);
        if (phone != null) {
            addComponent(new ComponentState(phone, properties.get(TELEPHONE_NUMBER)));
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out child components in a grid.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doSimpleLayout(IMObject object, IMObject parent, List<Property> properties, Component container,
                                  LayoutContext context) {
        IMObjectBean bean = getBean(object);
        String phone = bean.getString(TELEPHONE_NUMBER);
        Party practice = context.getContext().getPractice();
        if (bean.getBoolean("sms") && !StringUtils.isEmpty(phone) && SMSHelper.isSMSEnabled(practice)) {
            Button send = ButtonFactory.create("button.sms.send");
            send.addActionListener(new ActionListener() {
                public void onAction(ActionEvent e) {
                    onSend((Contact) object, context);
                }
            });
            RowLayoutData rowLayout = new RowLayoutData();
            Alignment topRight = new Alignment(Alignment.RIGHT, Alignment.TOP);
            rowLayout.setAlignment(topRight);
            send.setLayoutData(rowLayout);
            ComponentGrid grid = createGrid(object, properties, context);
            Row row = RowFactory.create(Styles.WIDE_CELL_SPACING, createGrid(grid));
            ButtonSet set = new ButtonSet(row);
            set.add(send);
            container.add(ColumnFactory.create(Styles.SMALL_INSET, row));
        } else {
            super.doSimpleLayout(object, parent, properties, container, context);
        }
    }

    /**
     * Displays an SMS dialog to send a message to the specified phone.
     *
     * @param contact the phone contact
     * @param context the layout context
     */
    private void onSend(Contact contact, LayoutContext context) {
        HelpContext help = context.getHelpContext().subtopic("sms");
        SMSDialog dialog = new SMSDialog(contact, context.getContext(), help);
        dialog.show();
    }
}
