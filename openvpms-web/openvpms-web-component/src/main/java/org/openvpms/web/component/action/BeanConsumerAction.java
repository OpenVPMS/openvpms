/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * An action that passes an {@link IMObject} wrapped in a bean to a {@link Consumer}.
 */
class BeanConsumerAction<T extends IMObject>
        extends ConsumerAction<T, IMObjectBean, BiConsumer<IMObjectBean, Consumer<ActionStatus>>> {

    /**
     * Constructs a {@link BeanConsumerAction}.
     *
     * @param object             the object
     * @param behaviour          the retry behaviour
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public BeanConsumerAction(T object, Behaviour behaviour, boolean async,
                              BiConsumer<IMObjectBean, Consumer<ActionStatus>> consumer, ArchetypeService service,
                              PlatformTransactionManager transactionManager) {
        super(object, behaviour, async, consumer, service, transactionManager);
    }

    /**
     * Constructs a {@link BeanConsumerAction}.
     *
     * @param supplier           the object supplier
     * @param behaviour          the retry behaviour
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public BeanConsumerAction(ObjectSupplier<T> supplier, Behaviour behaviour, boolean async,
                              BiConsumer<IMObjectBean, Consumer<ActionStatus>> consumer, ArchetypeService service,
                              PlatformTransactionManager transactionManager) {
        super(supplier, behaviour, async, consumer, service, transactionManager);
    }

    /**
     * Returns the current cached version of an object.
     *
     * @param object the original object
     * @return the cached version of the object
     */
    @Override
    protected IMObjectBean getCurrent(T object) {
        IMObject current = getCurrentObject(object);
        return getService().getBean(current);
    }

    /**
     * Returns the current cached version of an object.
     *
     * @param supplier the object supplier
     * @return the cached version of the object
     */
    @Override
    protected IMObjectBean getCurrent(ObjectSupplier<T> supplier) {
        IMObject current = getCurrentObject(supplier);
        return getService().getBean(current);
    }
}
