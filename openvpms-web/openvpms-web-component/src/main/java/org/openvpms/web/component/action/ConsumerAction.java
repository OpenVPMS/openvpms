/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * A reloading action that passes the object to a {@link BiConsumer}.<p/>
 * This consumer takes the object and the listener to notify when the consumer has completed.
 * <p/>
 * The action may be synchronous or asynchronous. Synchronous actions are invoked within a transaction.
 * Asynchronous actions are responsible for their own transaction management.
 */
abstract class ConsumerAction<T extends IMObject, A, C extends BiConsumer<A, Consumer<ActionStatus>>>
        extends ReloadingAction {

    /**
     * The object to supply.
     */
    private final T object;

    /**
     * The object supplier.
     */
    private final ObjectSupplier<T> supplier;

    /**
     * The consumer.
     */
    private final C consumer;

    /**
     * Constructs a {@link ConsumerAction}.
     *
     * @param object             the object to supply
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public ConsumerAction(T object, Behaviour behaviour, boolean async, C consumer, ArchetypeService service,
                          PlatformTransactionManager transactionManager) {
        super(behaviour, async, service, transactionManager);
        this.object = object;
        this.supplier = null;
        this.consumer = consumer;
        addObject(object);
    }

    /**
     * Constructs a {@link ConsumerAction}.
     *
     * @param supplier           the object supplier
     * @param behaviour          determines the behaviour for changed and missing objects
     * @param async              determines if the action runs asynchronously or synchronously
     * @param consumer           the consumer
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     */
    public ConsumerAction(ObjectSupplier<T> supplier, Behaviour behaviour, boolean async, C consumer,
                          ArchetypeService service, PlatformTransactionManager transactionManager) {
        super(behaviour, async, service, transactionManager);
        this.object = null;
        this.supplier = supplier;
        this.consumer = consumer;
        addObject(supplier);
    }

    /**
     * Executes the action.
     *
     * @param listener the listener
     */
    @Override
    protected final void execute(Consumer<ActionStatus> listener) {
        ActionStatus result;
        A current = object != null ? getCurrent(object) : getCurrent(supplier);
        if (current == null) {
            // should never occur
            result = ActionStatus.failed(Messages.get("action.internalerror"));
            listener.accept(result);
        } else {
            consumer.accept(current, listener);
        }
    }

    /**
     * Returns the current cached version of an object.
     *
     * @param object the original object
     * @return the cached version of the object
     */
    protected abstract A getCurrent(T object);

    /**
     * Returns the current cached version of an object.
     *
     * @param supplier the object supplier
     * @return the cached version of the object
     */
    protected abstract A getCurrent(ObjectSupplier<T> supplier);
}
