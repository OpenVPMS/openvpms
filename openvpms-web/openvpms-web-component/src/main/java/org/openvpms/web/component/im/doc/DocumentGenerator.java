/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.ParameterType;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.report.DocumentActReporter;
import org.openvpms.web.component.im.report.DocumentTemplateLocatorFactory;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.util.AbstractIMObjectSaveListener;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Collection;
import java.util.List;
import java.util.Set;


/**
 * Generates a document from a document act.
 * <p>
 * For document acts that have an existing document and no document template, the existing document will be returned.
 *
 * @author Tim Anderson
 */
public class DocumentGenerator {

    /**
     * Document generation listeners.
     */
    public interface Listener {

        /**
         * Invoked when generation completes.
         *
         * @param document the generated document
         */
        void generated(Document document);

        /**
         * Invoked if parameter entry is cancelled.
         */
        void cancelled();

        /**
         * Invoked if parameter entry is skipped.
         */
        void skipped();

        /**
         * Invoked if an error occurs trying to save a document.
         *
         * @param cause the cause of the error, or {@code null} if the error has been handled
         */
        void error(Throwable cause);
    }

    /**
     * Abstract implementation of the {@link Listener} interface that provides no-op implementations of each of the
     * methods.
     */
    public static abstract class AbstractListener implements Listener {

        /**
         * Invoked when generation completes.
         *
         * @param document the generated document
         */
        @Override
        public void generated(Document document) {
        }

        /**
         * Invoked if parameter entry is cancelled.
         */
        @Override
        public void cancelled() {
        }

        /**
         * Invoked if parameter entry is skipped.
         */
        @Override
        public void skipped() {
        }

        /**
         * Invoked if an error occurs trying to save a document.
         * <p/>
         * This implementation displays the error if present.
         *
         * @param cause the cause of the error, or {@code null} if the error has been handled
         */
        @Override
        public void error(Throwable cause) {
            if (cause != null) {
                ErrorHelper.show(cause);
            }
        }
    }

    /**
     * Letter archetypes. These support parameter prompting.
     */
    public static final String LETTER = "act.*DocumentLetter";

    /**
     * The document act.
     */
    private final DocumentAct act;

    /**
     * The document template locator factory.
     */
    private final DocumentTemplateLocatorFactory templateLocatorFactory;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The report factory.
     */
    private final ReportFactory reportFactory;

    /**
     * The document job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * The document rules.
     */
    private final DocumentRules documentRules;

    /**
     * The listener to notify.
     */
    private final Listener listener;

    /**
     * The mime type to use. This overrides the template output format.
     */
    private String mimeType;

    /**
     * Constructs a {@link DocumentGenerator}.
     *
     * @param act                    the document act
     * @param templateLocatorFactory the document template locator factory
     * @param context                the context
     * @param help                   the help context
     * @param formatter              the file name formatter
     * @param service                the archetype service
     * @param lookups                the lookup service
     * @param reportFactory          the report factory
     * @param jobManager             the document job manager
     * @param documentRules          the document rules
     * @param listener               the listener to notify
     */
    public DocumentGenerator(DocumentAct act, DocumentTemplateLocatorFactory templateLocatorFactory,
                             Context context, HelpContext help, FileNameFormatter formatter,
                             ArchetypeService service, LookupService lookups, ReportFactory reportFactory,
                             DocumentJobManager jobManager, DocumentRules documentRules, Listener listener) {
        this.act = act;
        this.templateLocatorFactory = templateLocatorFactory;
        this.context = context;
        this.help = help;
        this.formatter = formatter;
        this.service = service;
        this.lookups = lookups;
        this.reportFactory = reportFactory;
        this.jobManager = jobManager;
        this.documentRules = documentRules;
        this.listener = listener;
    }

    /**
     * Sets the mime type to use. This overrides the {@link DocumentTemplate#getOutputFormat() output format} of the
     * document template.
     *
     * @param mimeType the mime type. May be {@code null}
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Generates the document.
     * <p>
     * The document will not be saved.
     *
     * @throws DocumentException for any document error
     */
    public void generate() {
        generate(false, false);
    }

    /**
     * Generates the document, notifying the listener on completion.
     *
     * @param save    if {@code true} save the act and generated document
     * @param version if {@code true}  and saving the document, version any old document if the act supports it
     * @throws DocumentException for any document error
     */
    public void generate(boolean save, boolean version) {
        generate(save, version, false);
    }

    /**
     * Generates the document, notifying the listener on completion.
     *
     * @param save    if {@code true} save the act and generated document
     * @param version if {@code true}  and saving the document, version any old document if the act supports it
     * @param skip    if {@code true} and parameters are prompted for, allows document generation to be skipped
     * @throws DocumentException for any document error
     */
    public void generate(boolean save, boolean version, boolean skip) {
        DocumentTemplate template = templateLocatorFactory.getTemplate(act, context);
        if (template != null) {
            DocumentActReporter reporter = new DocumentActReporter(act, template, formatter, service, lookups,
                                                                   reportFactory);
            reporter.setFields(ReportContextFactory.create(context));
            generate(reporter, save, version, skip);
        } else if (act.getDocument() != null) {
            Document existing = service.get(act.getDocument(), Document.class);
            if (existing == null) {
                throw new DocumentException(DocumentException.ErrorCode.NotFound,
                                            DescriptorHelper.getDisplayName(act, service));
            }
            listener.generated(existing);
        } else {
            throw new DocumentException(DocumentException.ErrorCode.NotFound,
                                        DescriptorHelper.getDisplayName(act, service));
        }
    }

    /**
     * Generates a document.
     * <p/>
     * If the {@link #setMimeType(String) mime type} has been specified, this will be used. If not, and the reporter
     * has a template that specifies an {@link DocumentTemplate#getOutputFormat() output format}, this will be used,
     * otherwise the default format for the template will be used.
     *
     * @param reporter the reporter
     * @return the generated document
     */
    protected Document generate(DocumentActReporter reporter) {
        String type = mimeType;
        if (type == null) {
            DocumentTemplate template = reporter.getTemplate();
            if (template != null) {
                DocumentTemplate.Format format = template.getOutputFormat();
                if (format != null && format.getMimeType() != null) {
                    type = format.getMimeType();
                }
            }
        }
        return reporter.getDocument(type, false);
    }

    /**
     * Generates a document using a {@link DocumentActReporter}.
     *
     * @param reporter the reporter
     * @param save     if {@code true} save the act and generated document
     * @param version  if {@code true}  and saving the document, version any old document if the act supports it
     * @param skip     if {@code true} and parameters are prompted for, allows document generation to be skipped
     * @throws DocumentException for any document error
     */
    private void generate(DocumentActReporter reporter, boolean save, boolean version, boolean skip) {
        Job<Set<ParameterType>> job = JobBuilder.<Set<ParameterType>>newJob(reporter.getDisplayName(),
                                                                            context.getUser())
                .get(reporter::getParameterTypes) // can trigger a call to OpenOffice
                .completed(parameters -> {
                    if (parameters.isEmpty() || !act.isA(LETTER)) {
                        generate(reporter, save, version);
                    } else {
                        // only support parameter prompting for letters
                        promptParameters(reporter, save, version, skip);
                    }
                })
                .cancelled(listener::cancelled)
                .failed(listener::error)
                .build();
        jobManager.runInteractive(job, Messages.get("document.generate.title"),
                                  Messages.get("document.generate.cancel"));
    }

    /**
     * Generates a document from a report.
     *
     * @param reporter the reporter
     * @param save     if {@code true}, save the document
     * @param version  if {@code true}  and saving the document, version any old document if the act supports it
     */
    private void generate(DocumentActReporter reporter, boolean save, boolean version) {
        Job<?> job = JobBuilder.<Document>newJob(reporter.getDisplayName(), context.getUser())
                .get(() -> generate(reporter))
                .completed(document -> generated(document, save, version))
                .cancelled(listener::cancelled)
                .failed(listener::error)
                .build();
        jobManager.runInteractive(job, Messages.get("document.generate.title"),
                                  Messages.get("document.generate.cancel"));
    }

    /**
     * Invoked when a document is generated.
     *
     * @param document the document
     * @param save     if {@code true} save the document
     * @param version  if {@code true}, version the document
     */
    private void generated(Document document, boolean save, boolean version) {
        if (save) {
            List<IMObject> changes = documentRules.addDocument(act, document, version);
            SaveHelper.save(changes, new AbstractIMObjectSaveListener() {
                @Override
                public void saved(Collection<? extends IMObject> objects) {
                    listener.generated(document);
                }

                @Override
                protected void onErrorClosed() {
                    listener.error(null);
                }
            });
        } else {
            listener.generated(document);
        }
    }

    /**
     * Pops up a dialog to prompt for report parameters.
     *
     * @param reporter the report er
     * @param save     if {@code true}, save the document
     * @param version  if {@code true}  and saving the document, version any old document if the act supports it
     * @param skip     if {@code true} allow parameter entry (and therefore generation) to be skipped
     */
    private void promptParameters(DocumentActReporter reporter, final boolean save, final boolean version,
                                  boolean skip) {
        Set<ParameterType> parameters = reporter.getParameterTypes();
        String title = Messages.format("document.input.parameters", reporter.getTemplate().getName());
        MacroVariables variables = new MacroVariables(context, service, lookups);
        ParameterDialog dialog = new ParameterDialog(title, parameters, act, context, help.subtopic("parameters"),
                                                     variables, skip, true);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                reporter.setParameters(dialog.getValues());
                generate(reporter, save, version);
            }

            @Override
            public void onSkip() {
                listener.skipped();
            }

            @Override
            public void onCancel() {
                listener.cancelled();
            }
        });
        dialog.show();
    }
}
