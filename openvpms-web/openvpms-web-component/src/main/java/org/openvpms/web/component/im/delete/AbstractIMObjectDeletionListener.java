/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.delete;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.action.FailureReason;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;


/**
 * Abstract implementation of the {@link IMObjectDeletionListener} interface.
 *
 * @author Tim Anderson
 */
public class AbstractIMObjectDeletionListener<T extends IMObject> implements IMObjectDeletionListener<T> {

    /**
     * Callback to notify on closure of dialogs notifying failure.
     */
    private Runnable failureCallback;

    /**
     * Default constructor.
     */
    public AbstractIMObjectDeletionListener() {
        super();
    }

    /**
     * Notifies that an object has been deleted.
     * <p>
     * This implementation does nothing.
     *
     * @param object the deleted object
     */
    public void deleted(T object) {
    }

    /**
     * Notifies that an object has been deactivated.
     * <p>
     * This implementation does nothing.
     *
     * @param object the deactivated object
     */
    public void deactivated(T object) {
    }

    /**
     * Notifies that an object cannot be deleted, and has already been deactivated.
     *
     * @param object the object
     */
    @Override
    public void alreadyDeactivated(T object) {
    }

    /**
     * Notifies that deletion is unsupported.
     *
     * @param object the object that cannot be deleted
     * @param reason the reason
     */
    @Override
    public void unsupported(T object, String reason) {
        String type = getDisplayName(object);
        String title = Messages.format("imobject.delete.title", type);
        String message = (reason == null)
                         ? Messages.format("imobject.delete.unsupported", getDisplayName(object))
                         : reason;
        error(title, message);
    }

    /**
     * Notifies that an object has failed to be deleted.
     * <p>
     * This implementation displays an error dialog.
     *
     * @param object the object that failed to be deleted
     * @param reason the reason for the failure
     */
    public void failed(T object, FailureReason reason) {
        String title = Messages.get("imobject.delete.failed.title");
        String message = reason.getMessage();
        if (reason.getException() != null) {
            error(title, message, reason.getException());
        } else {
            error(title, message);
        }
    }

    /**
     * Sets the callback to be notified when an error dialog is closed.
     *
     * @param callback the callback. May be {@code null}
     */
    protected void setFailureCallback(Runnable callback) {
        failureCallback = callback;
    }

    /**
     * Displays an error.
     * <p/>
     * Any failure callback will be notified on completion.
     *
     * @param title   the title
     * @param message the message
     */
    protected void error(String title, String message) {
        ErrorHelper.show(title, message, failureCallback);
    }

    /**
     * Displays an error.
     * <p/>
     * Any failure callback will be notified on completion.
     *
     * @param title   the title
     * @param message the message
     * @param cause   the cause
     */
    protected void error(String title, String message, Throwable cause) {
        ErrorHelper.show(title, message, (String) null, cause, failureCallback);
    }

    /**
     * Returns the display name for an object.
     *
     * @param object the object
     * @return the display name
     */
    private String getDisplayName(IMObject object) {
        return DescriptorHelper.getDisplayName(object, ServiceHelper.getArchetypeService());
    }
}
