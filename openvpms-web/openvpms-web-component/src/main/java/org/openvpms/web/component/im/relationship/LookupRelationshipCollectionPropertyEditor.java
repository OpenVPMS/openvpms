/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.lookup.LookupRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.property.CollectionProperty;


/**
 * A {@link CollectionPropertyEditor} for collections of {@link LookupRelationship}s.
 *
 * @author Tim Anderson
 */
public class LookupRelationshipCollectionPropertyEditor extends RelationshipCollectionPropertyEditor {

    /**
     * Constructs an {@link LookupRelationshipCollectionPropertyEditor}.
     *
     * @param property the collection property
     * @param parent   the parent object
     * @throws ArchetypeServiceException for any archetype service error
     */
    public LookupRelationshipCollectionPropertyEditor(CollectionProperty property, Lookup parent) {
        super(property, parent);
    }

    /**
     * Adds a relationship to the related object.
     *
     * @param object       the related object
     * @param relationship the relationship to add
     */
    @Override
    protected void addRelationship(IMObject object, Relationship relationship) {
        Lookup lookup = (Lookup) object;
        lookup.addLookupRelationship((LookupRelationship) relationship);
    }

    /**
     * Removes a relationship from a related object.
     *
     * @param object       the related object
     * @param relationship the relationship to remove
     */
    @Override
    protected void removeRelationship(IMObject object, Relationship relationship) {
        Lookup lookup = (Lookup) object;
        lookup.removeLookupRelationship((LookupRelationship) relationship);
    }

}
