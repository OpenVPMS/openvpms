/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import echopointng.command.JavaScriptEval;
import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.webcontainer.ContainerInstance;
import org.openvpms.web.component.retry.Retryer;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialog;

/**
 * SMS viewer.
 *
 * @author Tim Anderson
 */
public class SMSViewerDialog extends ModalDialog {

    /**
     * The viewer.
     */
    private final SMSViewer viewer;

    /**
     * Constructs an {@link SMSViewerDialog}.
     *
     * @param title  the window title
     * @param viewer the SMS viewer
     */
    public SMSViewerDialog(String title, SMSViewer viewer) {
        super(title, PopupDialog.OK, null);
        this.viewer = viewer;
        getLayout().add(viewer.getComponent());
        resize("SMSViewerDialog.size");
    }

    /**
     * Show the window.
     */
    @Override
    public void show() {
        super.show();
        focusOnMessages();
    }

    /**
     * Invoked when the 'OK' button is pressed. This marks any replies as read, and closes the window.
     */
    @Override
    protected void onOK() {
        markAsRead();
        super.onOK();
    }

    /**
     * Marks any replies as read.
     */
    protected void markAsRead() {
        Retryer.run(viewer::markAsRead);
    }

    /**
     * Moves the focus to the panel displaying the messages if the panel is scrollable, so users can page/up down
     * without having to click on the dialog first.<p/>
     * This is done by making the div containing the messages focusable, by giving it a tabindex prior to giving it
     * the focus. Normally, divs won't receive focus unless they have scrollbars.
     * <p/>
     * If the panel isn't scrollable, the focus is moved to the OK button. Without this, pressing page up/down scrolls
     * the parent window.
     */
    private void focusOnMessages() {
        SplitPane layout = getLayout();
        String paneId = ContainerInstance.getElementId(layout) + "_pane1";
        // the id of the first pane in the split pane, containing the messages

        Component ok = getButtons().getButton(OK_ID);
        String okId = ContainerInstance.getElementId(ok);

        int index = ok.getFocusTraversalIndex();
        // use the same tabindex as the OK button; tab should move to OK next

        String javaScript = "var pane=document.getElementById('" + paneId + "');"
                            + "if (pane.clientHeight < pane.scrollHeight) {"
                            + "pane.tabindex='" + index + "';"
                            + "pane.focus()} else {"
                            + "var ok=document.getElementById('" + okId + "');"
                            + "ok.focus();}";
        // TODO - tab order doesn't work on Firefox or Chrome. Why?

        JavaScriptEval eval = new JavaScriptEval(javaScript);
        ApplicationInstance.getActive().enqueueCommand(eval);
    }
}
