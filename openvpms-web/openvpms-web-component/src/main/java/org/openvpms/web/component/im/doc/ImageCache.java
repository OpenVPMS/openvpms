/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.AbstractImageService;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.ImageService;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.io.File;

/**
 * Implementation of {@link ImageService} that enables images to be cached in a temporary directory provided by the
 * servlet container.
 * <p/>
 * Images are cached in the filesystem. The directory may be specified at construction, otherwise
 * they are cached in a directory named <em>ImageCache</em> under <em>"javax.servlet.context.tempdir"</em>.
 * <p/>
 * URLS returned by {@link Image#getURL()} are temporary.
 *
 * @author Tim Anderson
 */
public class ImageCache extends AbstractImageService implements ServletContextAware {

    /**
     * The servlet context.
     */
    private ServletContext context;

    /**
     * The cache directory.
     */
    private File dir;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ImageCache.class);

    /**
     * The property of the temporary directory used to store images under.
     */
    private static final String TEMP_DIR = "javax.servlet.context.tempdir";

    /**
     * Constructs an {@link ImageCache}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     */
    public ImageCache(DocumentHandlers handlers, ArchetypeService service) {
        this(handlers, service, null);
    }

    /**
     * Constructs an {@link ImageCache}.
     *
     * @param handlers the document handlers
     * @param service  the archetype service
     * @param dir      the directory to cache images. May be {@code null}
     */
    public ImageCache(DocumentHandlers handlers, ArchetypeService service, File dir) {
        super(handlers, service);
        this.dir = dir;
        if (dir != null && !dir.exists() && !dir.mkdirs()) {
            throw new IllegalArgumentException(
                    "Argument 'dir' specifies a directory that does not exist, and cannot be created: " + dir);
        }
    }

    /**
     * Set the ServletContext that this object runs in.
     *
     * @param context ServletContext object to be used by this object
     */
    @Override
    public void setServletContext(ServletContext context) {
        this.context = context;
    }

    /**
     * Returns the directory to store images.
     *
     * @return the directory
     */
    @Override
    protected synchronized File getDir() {
        if (dir == null) {
            File root = (context != null) ? (File) context.getAttribute(TEMP_DIR) : new File("./");
            File cache = new File(root, "ImageCache");
            if (!cache.exists() && !cache.mkdirs()) {
                throw new IllegalStateException("Failed to create ImageCache directory: " + cache);
            } else {
                dir = cache;
                log.info("Using image cache: {}", dir.getAbsolutePath());
            }
        }
        return dir;
    }
}
