/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.error;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.security.ArchetypeAccessDeniedException;
import org.openvpms.component.service.archetype.ValidationError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Formats error messages.
 *
 * @author Tim Anderson
 */
public class ErrorFormatter {

    /**
     * The message category.
     */
    public enum Category {
        DEFAULT, DELETE
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception the exception
     * @return the exception message
     */
    public static String format(Throwable exception) {
        return format(exception, null);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception the exception
     * @return the exception message as an HTML fragment
     */
    public static String formatHTML(Throwable exception) {
        return formatHTML(exception, null);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception   the exception
     * @param displayName the display name to include in the message. May be {@code null}
     * @return the exception message
     */
    public static String format(Throwable exception, String displayName) {
        return format(exception, Category.DEFAULT, displayName);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception   the exception
     * @param displayName the display name to include in the message. May be {@code null}
     * @return the exception message as an HTML fragment
     */
    public static String formatHTML(Throwable exception, String displayName) {
        return formatHTML(exception, Category.DEFAULT, displayName);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception   the exception
     * @param displayName the display name to include in the message. May be {@code null}
     * @return the exception message
     */
    public static String format(Throwable exception, Category category, String displayName) {
        return formatError(exception, category, displayName, false);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     *
     * @param exception   the exception
     * @param displayName the display name to include in the message. May be {@code null}
     * @return the exception message, as an HTML fragment
     */
    public static String formatHTML(Throwable exception, Category category, String displayName) {
        return formatError(exception, category, displayName, true);
    }

    /**
     * Returns the preferred exception message from an exception hierarchy.
     * <p/>
     * This examines the root cause first. If there is no message, it then uses the exception itself.
     *
     * @param exception   the exception
     * @param category    the message category
     * @param displayName the display name to include in the message. May be {@code null}
     * @param html        if {@code true}, the resulting string is an HTML fragment, else it is plain text
     * @return the exception message, as an HTML fragment
     */
    private static String formatError(Throwable exception, Category category, String displayName, boolean html) {
        String result = null;
        for (Throwable cause : ExceptionUtils.getThrowableList(exception)) {
            result = formatIfPresent(cause, category, displayName, html);
            if (result != null) {
                break;
            }
        }
        if (result == null) {
            result = exception.getLocalizedMessage();
            if (html) {
                result = escape(result);
            }
        }
        if (result == null) {
            result = exception.getClass().getName();
        }
        return result;
    }

    /**
     * Returns a formatted message for an exception, if one is configured.
     *
     * @param exception   the exception
     * @param category    the message category
     * @param displayName the display name
     * @param html        if {@code true}, the resulting text is an HTML fragment
     * @return the formatted message, or {@code null} if none is available
     */
    private static String formatIfPresent(Throwable exception, Category category, String displayName,
                                          boolean html) {
        String result = null;
        String className = exception.getClass().getName();
        String message = exception.getMessage();
        if (html && message != null) {
            message = StringEscapeUtils.escapeHtml4(message);
        }
        if (exception instanceof ArchetypeAccessDeniedException) {
            result = formatAccessDeniedException((ArchetypeAccessDeniedException) exception, html);
        } else if (exception instanceof ValidationException) {
            result = formatValidationException((ValidationException) exception, html);
        } else if (displayName != null) {
            String cat = (category == Category.DEFAULT) ? "" : "." + category.toString().toLowerCase();
            result = formatIfPresent(className + cat + ".displayName", html, message, displayName);
        }
        if (result == null) {
            result = formatIfPresent(className, html, message);
        }
        return result;
    }

    /**
     * Formats an {@link ArchetypeAccessDeniedException}.
     *
     * @param exception the exception
     * @param html      if {@code true}, the resulting text is an HTML fragment
     * @return the formatted error, or {@code null} if there is no formatting resource
     */
    private static String formatAccessDeniedException(ArchetypeAccessDeniedException exception, boolean html) {
        String archetype = DescriptorHelper.getDisplayName(exception.getArchetype(),
                                                           ServiceHelper.getArchetypeService());
        if (archetype == null) {
            archetype = exception.getArchetype();
        }
        return formatIfPresent(exception.getClass().getName(), html, archetype, exception.getOperation());
    }

    /**
     * Formats a validation exception.
     *
     * @param exception the exception
     * @param html      if {@code true}, the resulting text is an HTML fragment
     * @return the formatted error, or {@code null} if there is no formatting resource
     */
    private static String formatValidationException(ValidationException exception, boolean html) {
        String result = null;
        List<ValidationError> errors = exception.getErrors();
        if (!errors.isEmpty()) {
            ValidationError error = errors.get(0);
            String archetypeName = null;
            String nodeName = null;
            ArchetypeDescriptor archetype
                    = ServiceHelper.getArchetypeService().getArchetypeDescriptor(error.getArchetype());
            if (archetype != null) {
                archetypeName = archetype.getDisplayName();
                NodeDescriptor descriptor = archetype.getNodeDescriptor(error.getNode());
                if (descriptor != null) {
                    nodeName = descriptor.getDisplayName();
                }
            }
            result = formatIfPresent(error.getClass().getName(), html, archetypeName, nodeName, error.getMessage());
        }
        return result;
    }

    /**
     * Formats a message if one is available.
     * <p/>
     * This looks for:
     * <ul>
     * <li>{@code "<key>.html"} if {@code html} is {@code true}, or</li>
     * <li>{@code "<key>"} if {@code html} is {@code false}, or no html key is present</li>
     * </ul>
     * If {@code html} is {@code true}, the returned message will be an HTML fragment.
     *
     * @param key  the message key
     * @param html if {@code true}, use a .html
     * @param args the message arguments
     * @return the formatted message, or {@code null} if there is no message corresponding to the key
     */
    private static String formatIfPresent(String key, boolean html, Object... args) {
        String result = null;
        if (html) {
            result = Messages.formatNull(key + ".html", args);
        }
        if (result == null) {
            result = Messages.formatNull(key, args);
            if (html) {
                result = escape(result);
            }
        }
        return result;
    }

    /**
     * Escapes characters in a message so it can be embedded in an HTML fragment.
     *
     * @param message the message. May be {@code null}
     * @return the escaped message, or {@code null} if {@code message} is {@code null}
     */
    private static String escape(String message) {
        return (message != null) ? StringEscapeUtils.escapeHtml4(message) : null;
    }

}
