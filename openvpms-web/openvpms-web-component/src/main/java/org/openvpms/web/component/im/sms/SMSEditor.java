/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.macro.Macros;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.bound.BoundSelectFieldFactory;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.list.PairListModel;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.AbstractModifiable;
import org.openvpms.web.component.property.ErrorListener;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.ModifiableListeners;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetImpl;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.StringPropertyTransformer;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.SMSTextArea;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * An editor for SMS messages.
 *
 * @author Tim Anderson
 */
public class SMSEditor extends AbstractModifiable {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The text message.
     */
    private final SMSTextArea message;

    /**
     * The text property. Used to support macro expansion.
     */
    private final SimpleProperty messageProperty;

    /**
     * The phone property.
     */
    private final SimpleProperty phoneProperty;

    /**
     * Focus group.
     */
    private final FocusGroup focus;

    /**
     * Used to track property modification, and perform validation.
     */
    private final Editors editors;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The SMS service.
     */
    private final SMSService smsService;

    /**
     * The phone number, if 0 or 1 no. are provided.
     */
    private TextField phone;

    /**
     * The phone selector, if multiple phone numbers are provided.
     */
    private SelectField phoneSelector;

    /**
     * The selected contact.
     */
    private Contact selected;

    /**
     * The maximum number of parts in multi-part SMS messages.
     */
    private int maxParts;

    /**
     * The source of the SMS.
     */
    private Act source;

    /**
     * Ad hoc SMS reason code.
     */
    private static final String AD_HOC_SMS = "AD_HOC_SMS";

    /**
     * Constructs an {@link SMSEditor}.
     *
     * @param context the context
     */
    public SMSEditor(Context context) {
        this(Collections.emptyList(), null, context, ServiceHelper.getSMSService());
    }

    /**
     * Constructs an {@link SMSEditor}.
     * <p>
     * If no phone numbers are supplied, the phone number will be editable, otherwise it will be read-only.
     * If there are multiple phone numbers, they will be displayed in a dropdown, with the first no. as the default
     *
     * @param contacts   the available mobile contacts. May be {@code null}
     * @param variables  the variables for macro expansion. May be {@code null}
     * @param context    the context
     * @param smsService the SMS service
     */
    public SMSEditor(List<Contact> contacts, Variables variables, Context context, SMSService smsService) {
        this.context = context;
        service = ServiceHelper.getArchetypeService();
        this.smsService = smsService;
        int length = (contacts == null) ? 0 : contacts.size();
        phoneProperty = new SimpleProperty("phone", null, String.class, Messages.get("sms.phone"));
        phoneProperty.setRequired(true);
        if (length <= 1) {
            phone = BoundTextComponentFactory.create(phoneProperty, 20);
            if (length == 1) {
                onSelected(contacts.get(0));
                phoneProperty.setValue(formatPhone(selected));
                phone.setEnabled(false);
            }
        } else {
            final PairListModel model = formatPhones(contacts);
            phoneSelector = BoundSelectFieldFactory.create(phoneProperty, model);
            phoneSelector.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    int index = phoneSelector.getSelectedIndex();
                    if (index >= 0 && index < model.size()) {
                        onSelected((Contact) model.getValue(index));
                    } else {
                        onSelected(null);
                    }
                }
            });
            phoneSelector.setSelectedIndex(0);
        }

        messageProperty = new SimpleProperty("message", null, String.class, Messages.get("sms.message"));
        messageProperty.setRequired(true);
        messageProperty.setMaxLength(-1); // don't limit length as it is determined by maxParts and encoding
        Macros macros = ServiceHelper.getMacros();
        messageProperty.setTransformer(new StringPropertyTransformer(messageProperty, false, macros, null, variables));

        message = new BoundSMSTextArea(messageProperty, 40, 15);
        message.setStyleName(Styles.DEFAULT);
        focus = new FocusGroup("SMSEditor");
        if (phone != null && phone.isEnabled()) {
            focus.add(phone);
        } else if (phoneSelector != null) {
            focus.add(phoneSelector);
        }
        focus.add(message);
        focus.setDefault(message);

        PropertySet properties = new PropertySetImpl(phoneProperty, messageProperty);
        editors = new Editors(properties, new ModifiableListeners());
        editors.addModifiableListener(modifiable -> resetValid());

        setMaxParts(1);
    }

    /**
     * Sends an SMS.
     *
     * @throws SMSException if the SMS can't be sent
     */
    public void send() {
        String phone = getPhone();
        String message = getMessage();
        Party party = (selected != null) ?
                      ((org.openvpms.component.business.domain.im.party.Contact) selected).getParty() : null;
        Party customer = context.getCustomer();
        Party patient = context.getPatient();
        Party location = context.getLocation();
        OutboundMessage sms = smsService.getOutboundMessageBuilder()
                .recipient(party)
                .phone(phone)
                .message(message)
                .customer(customer)
                .patient(patient)
                .subject(Messages.get("sms.log.adhoc.subject"))
                .reason(AD_HOC_SMS)
                .location(location)
                .source(IMObjectHelper.reload(source))
                .build();
        // reload any source, to ensure the latest instance is being used
        smsService.send(sms);
    }

    /**
     * Returns the phone number.
     *
     * @return the phone number. May be {@code null}
     */
    public String getPhone() {
        String result = phoneProperty.getString();
        if (result != null) {
            // strip any spaces, hyphens, and brackets, and any characters after the last digit.
            result = result.replaceAll("[\\s\\-()]", "").replaceAll("[^\\d+].*", "");
        }
        return result;
    }

    /**
     * Sets the message to send.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        messageProperty.setValue(message);
    }

    /**
     * Sets the maximum number of parts of the message.
     *
     * @param maxParts the maximum length
     */
    public void setMaxParts(int maxParts) {
        if (maxParts <= 0) {
            maxParts = 1;
        }
        this.maxParts = maxParts;
        message.setMaxParts(maxParts);
    }

    /**
     * Sets the source of the SMS.
     *
     * @param source the source. May be {@code null}
     */
    public void setSource(Act source) {
        this.source = source;
    }

    /**
     * Returns the selected contact.
     *
     * @return the selected contact. May be {@code null}
     */
    public Contact getContact() {
        return selected;
    }

    /**
     * Returns the message to send.
     *
     * @return the message to send
     */
    public String getMessage() {
        return messageProperty.getString();
    }

    /**
     * Returns the editor component.
     *
     * @return the component
     */
    public Component getComponent() {
        ComponentGrid grid = new ComponentGrid();
        layout(grid);
        return grid.createGrid();
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group
     */
    public FocusGroup getFocusGroup() {
        return focus;
    }

    /**
     * Determines if the object has been modified.
     *
     * @return {@code true} if the object has been modified
     */
    public boolean isModified() {
        return editors.isModified();
    }

    /**
     * Clears the modified status of the object.
     */
    public void clearModified() {
        editors.clearModified();
    }

    /**
     * Adds a listener to be notified when this changes.
     *
     * @param listener the listener to add
     */
    public void addModifiableListener(ModifiableListener listener) {
        editors.addModifiableListener(listener);
    }

    /**
     * Adds a listener to be notified when this changes, specifying the order of the listener.
     *
     * @param listener the listener to add
     * @param index    the index to add the listener at. The 0-index listener is notified first
     */
    public void addModifiableListener(ModifiableListener listener, int index) {
        editors.addModifiableListener(listener, index);
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    public void removeModifiableListener(ModifiableListener listener) {
        editors.removeModifiableListener(listener);
    }

    /**
     * Sets a listener to be notified of errors.
     *
     * @param listener the listener to register. May be {@code null}
     */
    @Override
    public void setErrorListener(ErrorListener listener) {
        editors.setErrorListener(listener);
    }

    /**
     * Returns the listener to be notified of errors.
     *
     * @return the listener. May be {@code null}
     */
    @Override
    public ErrorListener getErrorListener() {
        return editors.getErrorListener();
    }

    /**
     * Resets the cached validity state of the object, to force revalidation of the object and its descendants.
     */
    @Override
    public void resetValid() {
        super.resetValid();
        editors.resetValid();
    }

    /**
     * Lays out the component in a grid.
     *
     * @param grid the grid
     */
    protected void layout(ComponentGrid grid) {
        grid.add(LabelFactory.create("sms.phone"), (phone != null) ? phone : phoneSelector);
        grid.add(LabelFactory.create("sms.message"), message);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    protected boolean doValidation(Validator validator) {
        return editors.validate(validator) && validatePhone(validator) && validateMessage(validator);
    }

    /**
     * Validates the phone number.
     *
     * @param validator the validator
     * @return {@code true} if the message is valid, otherwise {@code false}
     */
    private boolean validatePhone(Validator validator) {
        boolean valid = true;
        String phone = getPhone();
        if (phone == null || phone.isEmpty()) {
            valid = false;
            String error = Messages.format("property.error.required", phoneProperty.getDisplayName());
            validator.add(phoneProperty, error);
        }
        return valid;
    }

    /**
     * Validates the message.
     *
     * @param validator the validator
     * @return {@code true} if the message is valid, otherwise {@code false}
     */
    private boolean validateMessage(Validator validator) {
        boolean valid = true;
        String message = StringUtils.trimToEmpty(getMessage());
        if (message.isEmpty()) {
            String error = Messages.format("property.error.required", messageProperty.getDisplayName());
            validator.add(messageProperty, error);
            valid = false;
        } else {
            int parts = smsService.getParts(message);
            if (parts > maxParts) {
                valid = false;
                String error = Messages.format("sms.toolong", parts, maxParts);
                validator.add(this, error);
            }
        }
        return valid;
    }

    /**
     * Formats phone numbers that are flagged for SMS messaging.
     * <p>
     * The preferred no.s are at the head of the list
     *
     * @param contacts the SMS contacts
     * @return a list of phone numbers
     */
    private PairListModel formatPhones(List<Contact> contacts) {
        List<PairListModel.Pair> phones = new ArrayList<>();
        PairListModel.Pair preferred = null;
        for (Contact contact : contacts) {
            String phone = formatPhone(contact);
            PairListModel.Pair pair = new PairListModel.Pair(phone, contact);
            IMObjectBean bean = service.getBean(contact);
            if (bean.getBoolean("preferred")) {
                preferred = pair;
            }
            phones.add(pair);
        }
        phones.sort((o1, o2) -> ComparatorUtils.nullLowComparator(null).compare(o1.getKey(), o2.getKey()));
        if (preferred != null && !phones.get(0).equals(preferred)) {
            phones.remove(preferred);
            phones.add(0, preferred);
        }
        return new PairListModel(phones);
    }

    /**
     * Formats a mobile phone number. Adds the name afterward if it is not the default value.
     *
     * @param contact the phone contact
     * @return a formatted number, including an area code, if specified
     */
    private String formatPhone(Contact contact) {
        IMObjectBean bean = service.getBean(contact);
        String areaCode = bean.getString("areaCode");
        String phone = bean.getString("telephoneNumber");
        if (!StringUtils.isEmpty(areaCode)) {
            phone = Messages.format("phone.withAreaCode", areaCode, phone);
        } else {
            phone = Messages.format("phone.noAreaCode", phone);
        }
        String name = contact.getName();
        if (!StringUtils.isEmpty(name) && bean.hasNode("name") && !bean.isDefaultValue("name")) {
            phone += " (" + name + ")";
        }
        return phone;
    }

    /**
     * Invoked when a contact is selected.
     *
     * @param selected the selected contact. May be {@code null}
     */
    private void onSelected(Contact selected) {
        this.selected = selected;
    }
}
