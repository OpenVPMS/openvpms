/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientActEditor;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An editor for {@link Act}s which have an archetype of <em>act.patientReminder</em>.
 *
 * @author Tim Anderson
 */
public class ReminderEditor extends PatientActEditor {

    /**
     * The reminder rules.
     */
    private final ReminderRules rules;

    /**
     * Determines if matching reminders should be marked completed on save.
     */
    private boolean markCompleted = true;

    /**
     * Constructs a {@link ReminderEditor}.
     *
     * @param act     the reminder act
     * @param parent  the parent. May be {@code null}
     * @param context the layout context
     */
    public ReminderEditor(Act act, Act parent, LayoutContext context) {
        super(act, parent, context);
        if (!act.isA(ReminderArchetypes.REMINDER)) {
            throw new IllegalArgumentException("Invalid act type:" + act.getArchetype());
        }
        if (!UserHelper.isAdmin(context.getContext().getUser())) {
            // can't add items
            // TODO - this should be supported by user authorities
            ActRelationshipCollectionEditor items = new ActRelationshipCollectionEditor(
                    getCollectionProperty("items"), act, context) {
                @Override
                protected boolean canAdd() {
                    return false;
                }
            };
            addEditor(items);
        }
        addStartEndTimeListeners();
        rules = ServiceHelper.getBean(ReminderRules.class);
    }

    /**
     * Sets the initial time.
     * <p/>
     * This defaults to when the reminder was created, but can be back-dated during charging.
     * <p/>
     * Due dates are calculated relative to this.
     *
     * @param initial the created time
     */
    public void setInitialTime(Date initial) {
        getProperty("initialTime").setValue(initial);
    }

    /**
     * Returns the initial time.
     *
     * @return the initial time. May be {@code null}
     */
    public Date getInitialTime() {
        return getProperty("initialTime").getDate();
    }

    /**
     * Sets the reminder type.
     *
     * @param reminderType the reminder type. May be {@code null}
     */
    public void setReminderType(Entity reminderType) {
        setParticipant("reminderType", reminderType);
    }

    /**
     * Returns the reminder type.
     *
     * @return the reminder type
     */
    public Entity getReminderType() {
        return getParticipant("reminderType");
    }

    /**
     * Sets the product.
     *
     * @param product the product. May be {@code null}
     */
    public void setProduct(Product product) {
        setParticipant("product", product);
    }

    /**
     * Returns the product.
     *
     * @return the product. May be {@code null}
     */
    public Product getProduct() {
        return (Product) getParticipant("product");
    }

    /**
     * Returns the reminder count.
     *
     * @return the reminder count
     */
    public int getReminderCount() {
        return getProperty("reminderCount").getInt();
    }

    /**
     * Determines if matching reminders should be marked completed, if the reminder is new and IN_PROGRESS when it is
     * saved.
     * <p/>
     * Defaults to {@code true}.
     *
     * @param markCompleted if {@code true}, mark matching reminders as completed
     */
    public void setMarkMatchingRemindersCompleted(boolean markCompleted) {
        this.markCompleted = markCompleted;
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician. May be {@code null}.
     */
    @Override
    public void setClinician(User clinician) {
        super.setClinician(clinician);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        ActRelationshipCollectionEditor items = getItems();
        if (items != null) {
            strategy.addComponent(new ComponentState(items));
        }
        return strategy;
    }

    /**
     * Invoked when layout has completed. All editors have been created.
     */
    @Override
    protected void onLayoutCompleted() {
        Editor reminderType = getEditor("reminderType");

        if (reminderType != null) {
            // add a listener to update the due date when the reminder type is modified
            ModifiableListener listener = new ModifiableListener() {
                public void modified(Modifiable modifiable) {
                    onReminderTypeChanged();
                }
            };
            reminderType.addModifiableListener(listener);
        }

        ActRelationshipCollectionEditor items = getItems();
        if (items != null) {
            items.setExcludeDefaultValueObject(false);
        }
    }

    /**
     * Invoked when the start time changes.
     * <p>
     * For reminders, the start time represents the next reminder date.
     */
    @Override
    protected void onStartTimeChanged() {
        updateReminderItemSendDates();
    }

    /**
     * Invoked when the end time changes. For reminders, the end represents the original due date.
     * <p>
     * This recalculates the next reminder date.
     */
    @Override
    protected void onEndTimeChanged() {
        Date dueDate = getEndTime();
        if (dueDate != null) {
            Entity reminderType = getReminderType();
            if (reminderType != null) {
                Date nextReminderDate = getNextReminderDate(reminderType, dueDate);
                setStartTime(nextReminderDate);
            }
        }
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        boolean isNew = getObject().isNew();
        super.doSave();
        if (markCompleted && isNew) {
            rules.markMatchingRemindersCompleted(getObject());
        }
    }

    /**
     * Validates the object.
     * <p/>
     * This extends validation by ensuring that the start time is less than the end time, if non-null.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean valid = super.doValidation(validator);
        if (valid) {
            valid = validateItems(validator);
        }
        return valid;
    }

    /**
     * Validates that the start and end times are valid.
     * <p/>
     * This implementation always returns {@code true}
     *
     * @param validator the validator
     * @return {@code true}
     */
    @Override
    protected boolean validateStartEndTimes(Validator validator) {
        return true;
    }

    /**
     * Propagates the next reminder date to each of the reminder items with the same reminder count.
     */
    private void updateReminderItemSendDates() {
        Date start;
        ActRelationshipCollectionEditor items = getItems();
        if (items != null) {
            start = getStartTime();
            int count = getReminderCount();
            for (Act item : items.getCurrentActs()) {
                IMObjectEditor editor = items.getEditor(item);
                if (editor instanceof ReminderItemEditor) {
                    ReminderItemEditor itemEditor = (ReminderItemEditor) editor;
                    if (itemEditor.getCount() == count) {
                        itemEditor.setEndTime(start);
                    }
                }
            }
        }
    }

    /**
     * Ensures that reminder item counts aren't duplicated.
     *
     * @param validator the validator
     * @return {@code true} if the items are valid
     */
    private boolean validateItems(Validator validator) {
        boolean valid = true;
        ActRelationshipCollectionEditor items = getItems();
        if (items != null) {
            List<Act> acts = items.getCurrentActs();
            if (acts.size() > 1) {
                Map<Integer, Set<String>> map = new HashMap<>();
                for (Act act : acts) {
                    IMObjectBean bean = getBean(act);
                    int count = bean.getInt("count");
                    Set<String> set = map.computeIfAbsent(count, k -> new HashSet<>());
                    String archetype = act.getArchetype();
                    if (set.contains(archetype)) {
                        String message = Messages.format("patient.reminder.duplicateItem", getDisplayName(archetype),
                                                         count);
                        validator.add(this, new ValidatorError(message));
                        valid = false;
                        break;
                    } else {
                        set.add(archetype);
                    }
                }
            }
        }
        return valid;
    }

    /**
     * Updates the Due Date based on the reminderType reminder interval.
     */
    private void onReminderTypeChanged() {
        try {
            Date initial = getInitialTime();
            Entity reminderType = getReminderType();
            if (initial != null && reminderType != null) {
                Date dueDate = rules.calculateReminderDueDate(initial, reminderType);
                setEndTime(dueDate);
            }
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Determines the next reminder date, based on the due date and current reminder count.
     *
     * @param reminderType the reminder type
     * @param dueDate      the due date
     * @return the next reminder date
     */
    private Date getNextReminderDate(Entity reminderType, Date dueDate) {
        Date next = rules.getNextReminderDate(dueDate, reminderType, getReminderCount());
        if (next == null) {
            next = dueDate;
        }
        return next;
    }

    /**
     * Returns the reminder items.
     *
     * @return the reminder items, or {@code null} if they haven't been created yet
     */
    private ActRelationshipCollectionEditor getItems() {
        return (ActRelationshipCollectionEditor) getEditor("items", false);
    }

}
