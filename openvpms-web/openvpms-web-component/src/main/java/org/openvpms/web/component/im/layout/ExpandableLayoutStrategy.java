/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.layout;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.layout.ColumnLayoutData;
import nextapp.echo2.app.layout.RowLayoutData;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.filter.ComplexNodeFilter;
import org.openvpms.web.component.im.filter.NodeFilter;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;

import java.util.List;


/**
 * Layout strategy that adds a button to expand/collapse the layout.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class ExpandableLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Determines if the button should be included.
     */
    private final boolean showButton;

    /**
     * Determines if only required nodes should be shown.
     */
    private boolean showOptional;

    /**
     * Button indicating to expand/collapse the layout.
     */
    private Button button;


    /**
     * Constructs an {@link ExpandableLayoutStrategy}.
     */
    public ExpandableLayoutStrategy() {
        this(false, true);
    }

    /**
     * Constructs an {@link ExpandableLayoutStrategy}.
     *
     * @param showOptional if {@code true} show optional fields as well as mandatory ones.
     */
    public ExpandableLayoutStrategy(boolean showOptional) {
        this(showOptional, true);
    }

    /**
     * Construct a new {@code ExpandableLayoutStrategy}.
     *
     * @param showOptional if {@code true} show optional fields as well as mandatory ones.
     * @param showButton   if {@code true} show button to expand/collapse the layout
     */
    public ExpandableLayoutStrategy(boolean showOptional, boolean showButton) {
        this.showOptional = showOptional;
        this.showButton = showButton;
    }

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to
     * create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        button = null;
        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns the button to expand/collapse the layout.
     *
     * @return the layout button, or {@code null} if the layout cannot be expanded/collapsed
     */
    public Button getButton() {
        return button;
    }

    public boolean isShowOptional() {
        return showOptional;
    }

    public void setShowOptional(boolean showOptional) {
        this.showOptional = showOptional;
    }

    /**
     * Lay out the object in the specified container.
     *
     * @param object     the object to lay out
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doLayout(IMObject object, PropertySet properties, IMObject parent, Component container,
                            LayoutContext context) {
        super.doLayout(object, properties, parent, container, context);
        if (button == null && showButton) {
            Row row = getButtonRow();
            ColumnLayoutData right = new ColumnLayoutData();
            right.setAlignment(new Alignment(Alignment.RIGHT, Alignment.TOP));
            row.setLayoutData(right);
            container.add(row);
        }
    }

    /**
     * Lays out child components in a 2x2 grid.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doSimpleLayout(IMObject object, IMObject parent, List<Property> properties,
                                  Component container, LayoutContext context) {
        if (button != null || !showButton) {
            super.doSimpleLayout(object, parent, properties, container,
                                 context);
        } else if (!properties.isEmpty()) {
            Row group = RowFactory.create();
            super.doSimpleLayout(object, parent, properties, group,
                                 context);
            group.add(getButtonRow());
            container.add(group);
        }
    }

    /**
     * Lays out each child component in a tabbed pane.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doComplexLayout(IMObject object, IMObject parent,
                                   List<Property> properties, Component container, LayoutContext context) {
        if (button == null && showButton && !properties.isEmpty()) {
            Row row = getButtonRow();
            ColumnLayoutData right = new ColumnLayoutData();
            right.setAlignment(new Alignment(Alignment.RIGHT, Alignment.TOP));
            row.setLayoutData(right);
            container.add(row);
        }
        super.doComplexLayout(object, parent, properties, container,
                              context);
    }

    /**
     * Returns a node filter.
     *
     * @param object  the object to filter nodes for
     * @param context the context
     * @return a node filter to filter nodes
     */
    @Override
    protected NodeFilter getNodeFilter(IMObject object, LayoutContext context) {
        return new ComplexNodeFilter(showOptional, false);
    }

    /**
     * Determines if the layout button should be shown.
     *
     * @return {@code true} if the layout button should be included;
     * otherwise {@code false}
     */
    protected boolean showButton() {
        return showButton;
    }

    /**
     * Creates a row with the layout button in the top right.
     *
     * @return a new row
     */
    protected Row getButtonRow() {
        String key = (showOptional) ? "minus" : "plus";
        button = ButtonFactory.create(key);
        RowLayoutData right = new RowLayoutData();
        right.setAlignment(new Alignment(Alignment.RIGHT, Alignment.TOP));
        right.setWidth(new Extent(100, Extent.PERCENT));
        button.setLayoutData(right);
        Row wrapper = new Row();
        wrapper.add(button);
        wrapper.setLayoutData(right);
        return wrapper;
    }

}
