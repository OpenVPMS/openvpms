/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.list;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.list.AbstractListComponent;
import nextapp.echo2.app.list.ListModel;
import org.openvpms.web.echo.combo.ComboBox;


/**
 * List cell renderer that renders any 'All' and 'None' items in bold.
 *
 * @author Tim Anderson
 * @see AllNoneListModel
 */
public abstract class AllNoneListCellRenderer<T> extends AbstractListCellRenderer<T> {

    /**
     * Constructs an {@link AbstractListCellRenderer}.
     *
     * @param type the type that this can render
     */
    public AllNoneListCellRenderer(Class<T> type) {
        super(type);
    }

    /**
     * Determines if an object represents 'All'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'All'.
     */
    @Override
    protected boolean isAll(Component list, T object, int index) {
        ListModel model = getModel(list);
        return model instanceof AllNoneListModel && ((AllNoneListModel) model).isAll(index);
    }

    /**
     * Determines if an object represents 'None'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'None'.
     */
    @Override
    protected boolean isNone(Component list, T object, int index) {
        ListModel model = getModel(list);
        return model instanceof AllNoneListModel && ((AllNoneListModel) model).isNone(index);
    }

    /**
     * Determines if an object represents 'Default'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'Default'.
     */
    @Override
    protected boolean isDefault(Component list, T object, int index) {
        ListModel model = getModel(list);
        return model instanceof AllNoneListModel && ((AllNoneListModel) model).isDefault(index);
    }

    /**
     * Returns the list model.
     *
     * @param list the list
     * @return the list model
     */
    protected ListModel getModel(Component list) {
        ListModel model;
        if (list instanceof AbstractListComponent) {
            model = ((AbstractListComponent) list).getModel();
        } else if (list instanceof ComboBox) {
            model = ((ComboBox) list).getListModel();
        } else {
            throw new IllegalStateException("Unsupported component: " + list);
        }
        return model;
    }

}
