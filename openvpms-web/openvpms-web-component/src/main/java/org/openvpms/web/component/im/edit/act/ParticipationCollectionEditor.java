/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.act;

import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractEditableIMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.DefaultIMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.DelegatingCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;


/**
 * Editor for collections of {@link Participation}s.
 *
 * @author Tim Anderson
 */
public class ParticipationCollectionEditor extends DelegatingCollectionEditor {

    /**
     * Constructs a {@link ParticipationCollectionEditor}.
     *
     * @param property the collection property
     * @param object   the parent object
     * @param context  the layout context
     */
    public ParticipationCollectionEditor(CollectionProperty property, IMObject object, LayoutContext context) {
        String[] shortNames = property.getArchetypeRange();
        int max = property.getMaxCardinality();
        AbstractEditableIMObjectCollectionEditor editor;
        if (max == 1 && shortNames.length == 1) {
            editor = new SingleParticipationCollectionEditor(property, object, context) {
                @Override
                protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
                    return ParticipationCollectionEditor.this.createEditor(object, context);
                }

            };
        } else {
            editor = new DefaultIMObjectCollectionEditor(property, object, context) {
                @Override
                protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
                    return ParticipationCollectionEditor.this.createEditor(object, context);
                }
            };
        }
        setEditor(editor);
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit
     * @param context the layout context
     * @return an editor to edit {@code object}
     */
    protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
        return ServiceHelper.getBean(IMObjectEditorFactory.class).create(object, getObject(), context);
    }

}
