/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.domain.practice.Location;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * A printer service locator that is resilient to failures in the underlying {@link DocumentPrinterServiceLocator},
 * to enable users to download documents if the print service is unavailable.
 * <p/>
 * No {@link DocumentPrinterServiceLocator} method will throw exceptions, however any returned {@link DocumentPrinter} may.
 *
 * @author Tim Anderson
 */
public class ProtectedPrinterServiceLocator implements DocumentPrinterServiceLocator {

    private final DocumentPrinterServiceLocator locator;

    private static final Logger log = LoggerFactory.getLogger(ProtectedPrinterServiceLocator.class);

    public ProtectedPrinterServiceLocator(DocumentPrinterServiceLocator locator) {
        this.locator = locator;
    }

    /**
     * Locates a printer.
     *
     * @param archetype the printer service archetype or {@code null} to indicate the
     *                  {@link #getJavaPrintService Java Print Service API backed service}
     * @param id        the printer identifier
     * @return the corresponding printer
     */
    @Override
    public DocumentPrinter getPrinter(String archetype, String id) {
        try {
            return locator.getPrinter(archetype, id);
        } catch (Throwable exception) {
            log.warn("Failed to retrieve printer for service=" + archetype + ", id=" + id
                     + ": " + exception.getMessage(), exception);
        }
        return null;
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter() {
        try {
            return locator.getDefaultPrinter();
        } catch (Throwable exception) {
            log.warn("Failed to retrieve default printer: " + exception.getMessage(), exception);
        }
        return null;
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter(Location location) {
        try {
            return locator.getDefaultPrinter();
        } catch (Throwable exception) {
            log.warn("Failed to retrieve default printer for location=" + location.getName() + ": "
                     + exception.getMessage(), exception);
        }
        return null;
    }

    /**
     * Returns the printers available at a practice location.
     *
     * @param location the practice location
     * @return the printers
     */
    @Override
    public List<DocumentPrinter> getPrinters(Location location) {
        try {
            return locator.getPrinters(location);
        } catch (Throwable exception) {
            log.warn("Failed to retrieve printer for location=" + location.getName() + ": " + exception.getMessage(),
                     exception);
        }
        return Collections.emptyList();
    }

    /**
     * Returns all available printers.
     *
     * @return the printers
     */
    @Override
    public List<DocumentPrinter> getPrinters() {
        try {
            return locator.getPrinters();
        } catch (Throwable exception) {
            log.warn("Failed to retrieve printers: " + exception.getMessage(), exception);
        }
        return Collections.emptyList();
    }

    /**
     * Returns the available printer services.
     *
     * @return the printer services
     */
    @Override
    public List<DocumentPrinterService> getServices() {
        try {
            return locator.getServices();
        } catch (Throwable exception) {
            log.warn("Failed to retrieve printer services: " + exception.getMessage(), exception);
        }
        return Collections.emptyList();
    }

    /**
     * Returns the Java Print Service API backed service.
     *
     * @return the service implemented using the Java Print Service API.
     */
    @Override
    public DocumentPrinterService getJavaPrintService() {
        // should never throw an exception
        return locator.getJavaPrintService();
    }
}
