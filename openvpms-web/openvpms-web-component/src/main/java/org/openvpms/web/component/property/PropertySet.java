/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;

import java.util.Collection;

/**
 * Set of {@link Property} instances that tracks modification of derived values.
 *
 * @author Tim Anderson
 */
public interface PropertySet {

    /**
     * Returns the named property.
     *
     * @param name the name
     * @return the property corresponding to {@code name}, or {@code null} if none exists
     */
    Property get(String name);

    /**
     * Returns the properties.
     *
     * @return the properties
     */
    Collection<Property> getProperties();

    /**
     * Returns the editable properties.
     * <p/>
     * These are the non-hidden, modifiable properties.
     *
     * @return the editable properties
     */
    Collection<Property> getEditable();

    /**
     * Determines if any of the properties have been modified.
     *
     * @return {@code true} if at least one property has been modified
     */
    boolean isModified();

    /**
     * Clears the modified status of all properties.
     */
    void clearModified();

    /**
     * Updates derived properties. Any derived property that has changed
     * since the last call will notify their registered listeners.
     *
     * @throws ArchetypeServiceException for any archetype service error
     */
    void updateDerivedProperties();
}
