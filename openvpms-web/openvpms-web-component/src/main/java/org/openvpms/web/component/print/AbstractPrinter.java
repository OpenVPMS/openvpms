/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentTemplatePrinter;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.print.exception.PrinterException;
import org.openvpms.print.impl.exception.PrintNotSupported;
import org.openvpms.print.impl.i18n.PrintMessages;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.PrintProperties;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.print.PrinterContext;

import java.util.Objects;


/**
 * Abstract implementation of the {@link Printer} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPrinter implements Printer {

    /**
     * The printer context.
     */
    private final PrinterContext printerContext;

    /**
     * The context.
     */
    private final Context context;

    /**
     * Determines if printing should be interactive.
     */
    private boolean interactive = true;

    /**
     * The no. of copies to print.
     */
    private int copies;

    /**
     * Constructs an {@link AbstractPrinter}.
     *
     * @param printerContext the printer context
     * @param context        the context
     */
    public AbstractPrinter(PrinterContext printerContext, Context context) {
        this.printerContext = printerContext;
        this.context = context;
    }

    /**
     * Prints the object to the default printer.
     *
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print() {
        print((PrinterReference) null);
    }

    /**
     * Prints the object.
     *
     * @param reference the printer reference. May be {@code null}
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print(PrinterReference reference) {
        DocumentPrinter printer = (reference != null) ? printerContext.getPrinter(reference) : getDefaultPrinter();
        print(printer);
    }

    /**
     * Returns the default printer for the object.
     *
     * @return the default printer for the object, or {@code null} if none is defined
     * @throws OpenVPMSException for any error
     */
    @Override
    public DocumentPrinter getDefaultPrinter() {
        return getDefaultLocationPrinter();
    }

    /**
     * Determines if printing should occur interactively.
     *
     * @return {@code true} if printing should occur interactively,
     * {@code false} if it can be performed non-interactively
     */
    @Override
    public boolean getInteractive() {
        return interactive;
    }

    /**
     * Sets the number of copies to print.
     *
     * @param copies the no. of copies to print
     */
    @Override
    public void setCopies(int copies) {
        this.copies = copies;
    }

    /**
     * Returns the number of copies to print.
     *
     * @return the no. of copies to print
     */
    @Override
    public int getCopies() {
        return copies;
    }

    /**
     * Returns the printer locator.
     *
     * @return the printer locator
     */
    @Override
    public DocumentPrinterServiceLocator getPrinterLocator() {
        return printerContext.getPrinterLocator();
    }

    /**
     * Returns the printer context.
     *
     * @return the printer context
     */
    protected PrinterContext getPrinterContext() {
        return printerContext;
    }

    /**
     * Returns the print properties for an object.
     *
     * @param printer the printer
     * @return the print properties
     * @throws OpenVPMSException for any error
     */
    protected PrintProperties getProperties(DocumentPrinter printer) {
        PrintProperties result = new PrintProperties(printer.getId());
        result.setCopies(getCopies());
        return result;
    }

    /**
     * Returns the print properties for an object.
     *
     * @param printer  the printer
     * @param template the document template. May be {@code null}
     * @return the print properties
     * @throws OpenVPMSException for any error
     */
    protected PrintProperties getProperties(DocumentPrinter printer, DocumentTemplate template) {
        PrintProperties properties = new PrintProperties(printer.getId());
        properties.setCopies(getCopies());
        if (template != null) {
            properties.setMediaSize(template.getMediaSize());
            properties.setOrientation(template.getOrientationRequested());
            DocumentTemplatePrinter relationship = getDocumentTemplatePrinter(template, printer);
            if (relationship != null) {
                properties.setMediaTray(relationship.getMediaTray());
                properties.setSides(relationship.getSides());
            }
        }
        return properties;
    }

    /**
     * Prints a document.
     *
     * @param document the document to print
     * @param printer  the printer
     * @throws PrinterException for any print error
     */
    protected void print(Document document, DocumentPrinter printer) {
        if (printer == null) {
            throw new PrinterException(PrintMessages.noPrinter());
        }
        DocumentConverter converter = getConverter();
        if (printer.canPrint(document.getMimeType())) {
            printer.print(getDecompressed(document), getProperties(printer));
        } else if (printer.canPrint(DocFormats.PDF_TYPE) && converter.canConvert(document, DocFormats.PDF_TYPE)) {
            Document pdf = converter.convert(document, DocFormats.PDF_TYPE);
            printer.print(getDecompressed(pdf), getProperties(printer));
        } else {
            throw new PrintNotSupported(PrintMessages.unsupportedDocument(document.getName(), printer.getName()));
        }
    }

    /**
     * Determines if printing should occur interactively.
     *
     * @param interactive if {@code true} print interactively
     */
    protected void setInteractive(boolean interactive) {
        this.interactive = interactive;
    }

    /**
     * Returns the default printer for a template for the current practice or location.
     * <p/>
     * If none is defined, falls back to {@link #getDefaultLocationPrinter()}.
     *
     * @param template an <em>entity.documentTemplate</em>, or {@code null}
     * @return the default printer name. May be {@code null}
     */
    protected DocumentPrinter getDefaultPrinter(DocumentTemplate template) {
        DocumentPrinter result = null;
        DocumentTemplatePrinter printer = (template != null) ? getDocumentTemplatePrinter(template) : null;
        if (printer != null) {
            PrinterReference reference = printer.getPrinter();
            if (reference != null) {
                result = printerContext.getPrinter(reference);
            }
        }
        if (result == null) {
            result = getDefaultLocationPrinter();
        }
        return result;
    }

    /**
     * Returns the default printer for the practice location.
     * <p/>
     * If there is a printer configured on the location, this will be used, otherwise the default returned
     * by the printer service will be used.
     *
     * @return the printer name. May be {@code null} if none is defined
     */
    protected DocumentPrinter getDefaultLocationPrinter() {
        DocumentPrinter result = null;
        Party location = context.getLocation();
        if (location != null) {
            result = printerContext.getDefaultPrinter(location);
        }
        if (result == null) {
            result = printerContext.getDefaultPrinter();
        }
        return result;
    }

    /**
     * Helper to return the document template printer relationship for a template and printer and current
     * location/practice.
     *
     * @param template an template
     * @param printer  the printer
     * @return the relationship, or {@code null} if none is found
     */
    protected DocumentTemplatePrinter getDocumentTemplatePrinter(DocumentTemplate template, DocumentPrinter printer) {
        DocumentTemplatePrinter relationship = getDocumentTemplatePrinter(template);
        if (relationship != null) {
            // make sure the relationship is for the same printer
            PrinterReference reference = new PrinterReference(printer.getArchetype(), printer.getId());
            if (Objects.equals(reference, relationship.getPrinter())) {
                return relationship;
            }
        }
        return null;
    }

    /**
     * Returns the <em>entityRelationship.documentTemplatePrinter</em>
     * associated with an <em>entity.documentTemplate</em> for the context practice or location.
     * <p>
     * The location relationship will be returned if present, otherwise the practice relationship will be returned.
     *
     * @param template the document template
     * @return the corresponding document template printer relationship, or {@code null} if none is found
     */
    protected DocumentTemplatePrinter getDocumentTemplatePrinter(DocumentTemplate template) {
        DocumentTemplatePrinter printer = null;
        Party location = context.getLocation();
        Party practice = context.getPractice();
        if (location != null) {
            printer = template.getPrinter(location);
        }
        if (printer == null && practice != null) {
            printer = template.getPrinter(practice);
        }
        return printer;
    }

    /**
     * Helper to determine if printing should occur interactively for a
     * particular document template, printer and the current practice.
     * If no relationship is defined, defaults to {@code true}.
     *
     * @param template the template
     * @param printer  the printer
     * @return {@code true} if printing should occur interactively
     */
    protected boolean getInteractive(DocumentTemplate template, DocumentPrinter printer) {
        DocumentTemplatePrinter relationship = getDocumentTemplatePrinter(template, printer);
        return relationship == null || relationship.getInteractive();
    }

    /**
     * Returns a document given its reference.
     *
     * @param reference the reference. May be {@code null}
     * @return the corresponding document, or {@code null} if none is found
     */
    protected Document getDocument(Reference reference) {
        return reference != null ? (Document) getService().get(reference) : null;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return printerContext.getService();
    }

    /**
     * Returns the document converter.
     *
     * @return the document converter
     */
    protected DocumentConverter getConverter() {
        return printerContext.getConverter();
    }

    /**
     * Returns the context.
     *
     * @return the context
     */
    protected Context getContext() {
        return context;
    }

    /**
     * Returns a decompressed version of a document.
     *
     * @param document the document
     * @return the decompressed document, or {@code document} if no decompression is required
     */
    private Document getDecompressed(Document document) {
        PrinterContext context = getPrinterContext();
        return context.getDecompressedDocument(document);
    }
}
