/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import nextapp.echo2.app.Extent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.openvpms.web.component.property.DelegatingProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.text.PasswordField;
import org.openvpms.web.echo.text.TextDocument;
import org.openvpms.web.system.ServiceHelper;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Objects;


/**
 * Binds a {@link Property} to a {@code PasswordField}.
 *
 * @author Tim Anderson
 */
public class BoundPasswordField extends PasswordField implements BoundProperty {

    /**
     * The binder.
     */
    public Binder binder;

    /**
     * The password encryptor for passwords that need to be retained.
     */
    private final PasswordEncryptor encryptor;

    /**
     * The password encoder, for passwords that are hashed.
     */
    private final PasswordEncoder encoder;

    /**
     * Determines if passwords are hashed.
     */
    private final boolean isHash;

    /**
     * Wrapper property.
     */
    private final Property wrapper;

    /**
     * A placeholder password to avoid sending decrypted passwords to the UI if the user doesn't change it.
     */
    private String placeholder;

    /**
     * The plain text password, if the user has entered it.
     * <p/>
     * This is used for validation.
     */
    private String plainText;


    /**
     * Constructs a {@code BoundPasswordField}.
     *
     * @param property the property to bind
     */
    public BoundPasswordField(Property property) {
        super();
        encryptor = ServiceHelper.getBean(PasswordEncryptor.class);
        encoder = ServiceHelper.getBean(PasswordEncoder.class);
        setDocument(new TextDocument());
        int columns = property.getMaxLength();
        if (columns > 20) {
            columns = 20;
        }
        if (columns <= 10) {
            setWidth(new Extent(columns, Extent.EM));
        } else {
            setWidth(new Extent(columns, Extent.EX));
        }
        isHash = isHash(property);
        String encrypted = property.getString();
        if (encrypted != null && encrypted.length() != 0) {
            // don't expose actual length of password. For hashed passwords, this isn't available anyway
            placeholder = StringUtils.repeat('x', 14);
        }
        wrapper = new DelegatingProperty(property) {

            @Override
            public boolean setValue(Object value) {
                boolean result;
                String text = (value != null) ? value.toString() : null;
                placeholder = text;
                plainText = text;
                if (text != null) {
                    if (!isHash) {
                        text = encryptor.encrypt(text);
                    } else {
                        text = encoder.encode(text);
                    }
                    result = super.setValue(text);
                } else {
                    result = super.setValue(null);
                }
                return result;
            }

            @Override
            public Object getValue() {
                return placeholder;
            }
        };

        binder = new TextComponentBinder(this, wrapper);
        if (!StringUtils.isEmpty(property.getDescription())) {
            setToolTipText(property.getDescription());
        }
    }

    /**
     * Life-cycle method invoked when the {@code Component} is added to a registered hierarchy.
     */
    @Override
    public void init() {
        super.init();
        binder.bind();
    }

    /**
     * Life-cycle method invoked when the {@code Component} is removed from a registered hierarchy.
     */
    @Override
    public void dispose() {
        super.dispose();
        binder.unbind();
    }

    /**
     * Returns the property.
     *
     * @return the property
     */
    @Override
    public Property getProperty() {
        return wrapper;
    }

    /**
     * Determines if two password fields match.
     * <p/>
     * This only applies when the user has entered text into the field as hashed fields cannot be compared.
     *
     * @param other the other field
     * @return {@code true} if the fields match
     */
    public boolean matches(BoundPasswordField other) {
        return Objects.equals(plainText, other.plainText);
    }

    /**
     * Returns the plain text of a password.
     * <p/>
     * This is only available if the password has been input.
     *
     * @return the plain text. May be {@code  null}
     */
    public String getPlainText() {
        return plainText;
    }

    /**
     * Determines if a password is hashed or encrypted.
     *
     * @param property the password property
     * @return {@code true} if the password is hashed, {@code false} if it is encrypted
     */
    private boolean isHash(Property property) {
        boolean isHash = false;
        NodeDescriptor descriptor = property.getDescriptor();
        if (descriptor != null) {
            AssertionDescriptor password = descriptor.getAssertionDescriptor("password");
            if (password != null) {
                NamedProperty hash = password.getProperty("hash");
                if (hash != null && "true".equalsIgnoreCase((String) hash.getValue())) {
                    isHash = true;
                }
            }
        }
        return isHash;
    }

}