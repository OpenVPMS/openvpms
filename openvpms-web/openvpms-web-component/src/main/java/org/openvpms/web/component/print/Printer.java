/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.print.service.DocumentPrinter;


/**
 * Document print interface. Provides support for interactive and background
 * printing.
 *
 * @author Tim Anderson
 */
public interface Printer {

    /**
     * Prints the object to the default printer.
     *
     * @throws OpenVPMSException for any error
     */
    void print();

    /**
     * Prints the object.
     *
     * @param reference the printer reference. May be {@code null}
     * @throws OpenVPMSException for any error
     */
    void print(PrinterReference reference);

    /**
     * Prints the object.
     *
     * @param printer the printer
     * @throws OpenVPMSException for any error
     */
    void print(DocumentPrinter printer);

    /**
     * Returns the default printer for the object.
     *
     * @return the default printer for the object, or {@code null} if none is defined
     * @throws OpenVPMSException for any error
     */
    DocumentPrinter getDefaultPrinter();

    /**
     * Returns a document corresponding to that which would be printed.
     *
     * @return a document
     * @throws OpenVPMSException for any error
     */
    Document getDocument();

    /**
     * Returns a document corresponding to that which would be printed.
     * <p/>
     * If the document cannot be converted to the specified mime-type, it will be returned unchanged.
     *
     * @param mimeType the mime type. If {@code null} the default mime type associated with the report will be used.
     * @param email    if {@code true} indicates that the document will be emailed. Documents generated from templates
     *                 can perform custom formatting
     * @return a document
     * @throws OpenVPMSException for any error
     */
    Document getDocument(String mimeType, boolean email);

    /**
     * Determines if printing should occur interactively.
     *
     * @return {@code true} if printing should occur interactively, {@code false} if it can be performed
     * non-interactively
     * @throws OpenVPMSException for any error
     */
    boolean getInteractive();

    /**
     * Sets the number of copies to print.
     *
     * @param copies the no. of copies to print
     */
    void setCopies(int copies);

    /**
     * Returns the number of copies to print.
     *
     * @return the no. of copies to print
     */
    int getCopies();

    /**
     * Returns a display name for the objects being printed.
     *
     * @return a display name for the objects being printed
     */
    String getDisplayName();

    /**
     * Returns the printer locator.
     *
     * @return the printer locator
     */
    DocumentPrinterServiceLocator getPrinterLocator();

}
