/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.job;

import org.openvpms.component.model.user.User;
import org.openvpms.web.component.util.ErrorHelper;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Builder for {@link Job} instances.
 *
 * @author Tim Anderson
 */
public class JobBuilder<T> {

    /**
     * The job name.
     */
    private final String name;

    /**
     * The user launching the job.
     */
    private final User user;

    /**
     * The command to run.
     */
    private Supplier<T> command;

    /**
     * The listener to invoke when the job has completed.
     */
    private Consumer<T> completed;

    /**
     * The listener to invoke when the job has been cancelled.
     */
    private Runnable cancelled;

    /**
     * The listener to invoke if the job fails.
     */
    private Consumer<Throwable> failed;

    /**
     * Constructs a {@link JobBuilder}.
     *
     * @param name the job name
     * @param user the user launching the job
     */
    public JobBuilder(String name, User user) {
        this.name = name;
        this.user = user;
        failed = ErrorHelper::show;
    }

    /**
     * Sets the command to run.
     *
     * @param command the command
     * @return this
     */
    public JobBuilder<T> run(Runnable command) {
        return get(() -> {
            command.run();
            return null;
        });
    }

    /**
     * Sets the command to run.
     *
     * @param command the command
     * @return this
     */
    public JobBuilder<T> get(Supplier<T> command) {
        this.command = command;
        return this;
    }

    /**
     * Sets the listener to be invoked when the job completes successfully {
     *
     * @param listener the listener
     */
    public JobBuilder<T> completed(Consumer<T> listener) {
        this.completed = listener;
        return this;
    }

    /**
     * Sets the listener to invoke when the job completes successfully.
     *
     * @param listener the listener
     * @return this
     */
    public JobBuilder<T> completed(Runnable listener) {
        return completed(value -> listener.run());
    }

    /**
     * Sets the listener to invoke when the job is cancelled.
     *
     * @param listener the listener
     * @return this
     */
    public JobBuilder<T> cancelled(Runnable listener) {
        this.cancelled = listener;
        return this;
    }

    /**
     * Sets the listener to invoke when the job fails.
     *
     * @param listener the listener
     * @return this
     */
    public JobBuilder<T> failed(Consumer<Throwable> listener) {
        this.failed = listener;
        return this;
    }

    /**
     * Builds the job.
     *
     * @return a new job
     */
    public Job<T> build() {
        return new DefaultJob<>(name, user, command, completed, cancelled, failed);
    }

    /**
     * Returns a new job builder.
     *
     * @param name the job name
     * @param user the user launching the job
     * @return a new job builder
     */
    public static <T> JobBuilder<T> newJob(String name, User user) {
        return new JobBuilder<>(name, user);
    }

    /**
     * Default implementation of {@link Job}.
     */
    static class DefaultJob<T> extends AbstractJob<T> {

        private final Supplier<T> command;

        private final Consumer<T> completionListener;

        private final Runnable cancellationListener;

        private final Consumer<Throwable> failureListener;

        public DefaultJob(String name, User user, Supplier<T> command, Consumer<T> completionListener,
                          Runnable cancellationListener, Consumer<Throwable> failureListener) {
            super(name, user);
            this.command = command;
            this.completionListener = completionListener;
            this.cancellationListener = cancellationListener;
            this.failureListener = failureListener;
        }

        /**
         * Returns the listener to invoke when the job completes successfully.
         *
         * @return the completion listener. May be {@code null}
         */
        @Override
        public Consumer<T> getCompletionListener() {
            return completionListener;
        }

        /**
         * Returns the listener to invoke when the job is cancelled.
         *
         * @return the cancellation listener. May be {@code null}
         */
        @Override
        public Runnable getCancellationListener() {
            return cancellationListener;
        }

        /**
         * Returns the listener top invoke when the job fails.
         *
         * @return the failure listener. May be {@code null}
         */
        @Override
        public Consumer<Throwable> getFailureListener() {
            return failureListener;
        }

        /**
         * Runs the job.
         */
        @Override
        protected T runJob() {
            // note that any exception generated here will be propagated
            return (command != null) ? command.get() : null;
        }
    }

}
