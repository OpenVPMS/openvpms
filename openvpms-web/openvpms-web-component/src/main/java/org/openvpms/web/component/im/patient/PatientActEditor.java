/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.patient;

import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.edit.act.AbstractActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;


/**
 * An editor for acts with a patient.
 *
 * @author Tim Anderson
 */
public class PatientActEditor extends AbstractActEditor {

    /**
     * The patient node.
     */
    private static final String PATIENT = "patient";

    /**
     * The clinician node.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Constructs a {@link PatientActEditor}.
     * <p>
     * If a parent act is specified, the following applies:
     * <ul>
     * <li>if {@code act} is new, its start time defaults to that of the parent</li>
     * <li>if the parent has a <em>patient</em> node, its value will used to set the patient</li>
     * </ul>
     * <li>If the parent has no <em>patient</em> node or there is no parent, the patient will be set from the context.
     *
     * @param act     the act to edit
     * @param parent  the parent act. May be {@code null}
     * @param context the layout context
     */
    public PatientActEditor(Act act, Act parent, LayoutContext context) {
        super(act, parent, context);
        if (act.isNew()) {
            if (parent != null) {
                // default the act start time to that of the parent
                act.setActivityStartTime(parent.getActivityStartTime());

                IMObjectBean bean = getBean(parent);
                if (bean.hasNode(PATIENT)) {
                    setPatient(bean.getTargetRef(PATIENT));
                } else {
                    initParticipant(PATIENT, context.getContext().getPatient());
                }
            } else {
                initParticipant(PATIENT, context.getContext().getPatient());
            }
            initParticipant(CLINICIAN, context.getContext().getClinician());
        }
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient. May be {@code null}
     */
    public void setPatient(Party patient) {
        setPatient(patient != null ? patient.getObjectReference() : null);
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    public Party getPatient() {
        return (Party) getParticipant(PATIENT);
    }

    /**
     * Sets the patient.
     *
     * @param patient the patient reference. May be {@code null}
     */
    public void setPatient(Reference patient) {
        setParticipant(PATIENT, patient);
    }

    /**
     * Returns the patient reference.
     *
     * @return the patient reference. May be {@code null}
     */
    public Reference getPatientRef() {
        return getParticipantRef(PATIENT);
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician. May be {@code null}.
     */
    public void setClinician(User clinician) {
        setParticipant(CLINICIAN, clinician);
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician reference. May be {@code null}.
     */
    public void setClinician(IMObjectReference clinician) {
        setParticipant(CLINICIAN, clinician);
    }

    /**
     * Returns the clinician reference.
     *
     * @return the clinician reference. May be {@code null}
     */
    public Reference getClinicianRef() {
        return getParticipantRef(CLINICIAN);
    }
}
