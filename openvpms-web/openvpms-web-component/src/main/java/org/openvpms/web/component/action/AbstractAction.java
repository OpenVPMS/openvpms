/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import java.util.function.Consumer;

/**
 * Abstract implementation of {@link Action}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAction implements Action {

    /**
     * Runs the action.
     */
    @Override
    public void run() {
        run(new DefaultActionStatusConsumer());
    }

    /**
     * Runs the action.
     *
     * @param listener the listener to notify when the action completes
     */
    @Override
    public void run(Consumer<ActionStatus> listener) {
        runProtected(listener);
    }

    /**
     * Runs the action, catching any exceptions.
     *
     * @param listener the listener to notify on completion
     */
    protected abstract void runProtected(Consumer<ActionStatus> listener);

}
