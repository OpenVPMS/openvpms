/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.webcontainer.command.BrowserOpenWindowCommand;
import org.openvpms.web.echo.servlet.ServletHelper;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationCodeGrantFilter;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;

/**
 * Initiates an OAuth code grant authorization flow.
 * <p/>
 * This opens a new browser window with a URL for an OAuth2 {@link ClientRegistration} which triggers the
 * {@link OAuth2AuthorizationRequestRedirectFilter}.
 * <p/>
 * It must be constructed with a session-bound {@link OAuth2ResponseManager} to hold the email address
 * the authorization flow is for. This is required in order to store the correct email address with the
 * {@link OAuth2AuthorizedClient} when the flow completes.
 * <p/>
 * TODO - this requires jumping through some hoops with Spring Security in order to register a custom
 * {@link OAuth2AuthorizationCodeGrantFilter}. A better approach would be to make a call to get the email address
 * after authorization is complete.
 *
 * @author Tim Anderson
 * @see OAuth2ResponseManager
 */
public class OAuth2RequestLauncher {

    /**
     * The strategy for storing the OAUth2 principal.
     */
    private final OAuth2ResponseManager responseManager;

    /**
     * Constructs an {@link OAuth2RequestLauncher}.
     *
     * @param responseManager the strategy for storing the OAUth2 principal
     */
    public OAuth2RequestLauncher(OAuth2ResponseManager responseManager) {
        this.responseManager = responseManager;
    }

    /**
     * Launches an OAuth2 authorisation request for the given client registration and email address.
     *
     * @param clientRegistrationId the client registration identifier
     */
    public void launch(String clientRegistrationId) {
        responseManager.clear();
        String uri = ServletHelper.getRedirectURI("oauth2/authorization/" + clientRegistrationId);
        ApplicationInstance.getActive().enqueueCommand(new BrowserOpenWindowCommand(uri, "Authorise",
                                                                                    "width=500,height=550,popup=true"));
    }
}
