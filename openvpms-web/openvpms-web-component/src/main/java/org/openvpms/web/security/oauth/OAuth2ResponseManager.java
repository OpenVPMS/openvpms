/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.security.oauth;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2Error;

/**
 * Manages the result of an OAuth2 code grant flow.
 * <p/>
 * This needs to be bound to the session.
 *
 * @author Tim Anderson
 * @see OAuth2RequestLauncher
 * @see OAuth2CodeGrantCompleteServlet
 */
public class OAuth2ResponseManager {

    /**
     * The security context holder strategy.
     */
    private final OAuth2SessionSecurityContextHolderStrategy securityContextHolderStrategy;

    /**
     * The error, if the code grant flow failed.
     */
    private OAuth2Error error;

    /**
     * Constructs a {@link OAuth2ResponseManager}.
     *
     * @param securityContextHolderStrategy the security context holder strategy
     */
    public OAuth2ResponseManager(OAuth2SessionSecurityContextHolderStrategy securityContextHolderStrategy) {
        this.securityContextHolderStrategy = securityContextHolderStrategy;
    }

    /**
     * Returns the email address of the user, if the OAuth2 code grant flow was successful.
     *
     * @return the email address, or {@code null} if the code grant flow was not successful or hasn't completed
     */
    public String getEmail() {
        String result = null;
        if (error == null) {
            Authentication authentication = securityContextHolderStrategy.getContext().getAuthentication();
            result = (authentication != null) ? authentication.getName() : null;
        }
        return result;
    }

    /**
     * Returns the error raised by the OAuth2 code grant flow.
     *
     * @return the error, or {@code nullif the code grant flow was successful or hasn't completed
     */
    public OAuth2Error getError() {
        return error;
    }

    /**
     * Logs failure of the OAuth2 code grant flow.
     *
     * @param code        the error code
     * @param description the error description. May be {@code null}}
     * @param uri         the error URI. May be {@code null}
     */
    public void error(String code, String description, String uri) {
        this.error = new OAuth2Error(code, description, uri);
        securityContextHolderStrategy.clearContext();
    }

    /**
     * Indicates that the code grant flow was processed successfully.
     */
    public void success() {
        this.error = null;
    }

    /**
     * Clears the state.
     */
    public void clear() {
        this.error = null;
        securityContextHolderStrategy.clearContext();
    }
}