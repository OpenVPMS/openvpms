/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.error;

import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.ErrorDialogBuilder;

/**
 * Builder for {@link ErrorDialog}s.
 *
 * @author Tim Anderson
 */
public class ErrorReportingDialogBuilder extends ErrorDialogBuilder {

    /**
     * The cause.
     */
    private Throwable cause;

    /**
     * Constructs an {@link ErrorReportingDialogBuilder}.
     */
    ErrorReportingDialogBuilder() {
        super();
    }

    /**
     * Sets the cause.
     *
     * @param cause the cause
     * @return this
     */
    public ErrorReportingDialogBuilder cause(Throwable cause) {
        this.cause = cause;
        return this;
    }

    /**
     * Returns the cause of the error.
     *
     * @return the cause. May be {@code null}
     */
    public Throwable getCause() {
        return cause;
    }

    /**
     * Builds the dialog.
     *
     * @return the dialog
     */
    @Override
    public ErrorReportingDialog build() {
        return new ErrorReportingDialog(this);
    }

    /**
     * Returns this.
     *
     * @return this
     */
    @Override
    protected ErrorReportingDialogBuilder getThis() {
        return this;
    }

}