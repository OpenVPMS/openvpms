/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.view.layout;

import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ShortNamePairArchetypeHandlers;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.DefaultLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Abstract implementation of the {@link IMObjectLayoutStrategyFactory} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractLayoutStrategyFactory implements IMObjectLayoutStrategyFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractLayoutStrategyFactory.class);

    /**
     * Constructs an {@link AbstractLayoutStrategyFactory}.
     *
     * @param service the archetype service
     */
    protected AbstractLayoutStrategyFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new layout strategy for an object.
     *
     * @param object the object to create the layout strategy for
     * @return a new layout strategy
     */
    public IMObjectLayoutStrategy create(IMObject object) {
        return create(object, null);
    }

    /**
     * Creates a new layout strategy for an object.
     *
     * @param object the object to create the layout strategy for
     * @param parent the parent object. May be {@code null}
     */
    public IMObjectLayoutStrategy create(IMObject object, IMObject parent) {
        IMObjectLayoutStrategy result = null;
        ArchetypeHandler<?> handler;
        if (parent != null) {
            String primary = object.getArchetype();
            String secondary = parent.getArchetype();
            handler = getStrategies().getHandler(primary, secondary);
        } else {
            String shortName = object.getArchetype();
            handler = getStrategies().getHandler(shortName);
        }
        if (handler != null) {
            try {
                result = (IMObjectLayoutStrategy) handler.create();
            } catch (Throwable throwable) {
                log.error(throwable.getMessage(), throwable);
            }
        }
        if (result == null) {
            result = new DefaultLayoutStrategy();
        }

        return result;
    }

    /**
     * Returns the strategy implementations.
     *
     * @return the strategy implementations
     */
    protected abstract ShortNamePairArchetypeHandlers getStrategies();

    /**
     * Helper to load the strategy implementations.
     *
     * @param name the resource name
     * @return the strategy implementations
     */
    protected ShortNamePairArchetypeHandlers load(String name) {
        return new ShortNamePairArchetypeHandlers(name, IMObjectLayoutStrategy.class, service);
    }

}
