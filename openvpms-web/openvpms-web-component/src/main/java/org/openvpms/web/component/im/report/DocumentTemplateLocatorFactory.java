/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandler;
import org.openvpms.component.business.service.archetype.handler.ArchetypeHandlers;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;

import static org.apache.commons.lang3.reflect.ConstructorUtils.getMatchingAccessibleConstructor;

/**
 * Factory for {@link DocumentTemplateLocator} instances.
 * <p/>
 * This uses instances registered
 *
 * @author Tim Anderson
 */
public class DocumentTemplateLocatorFactory {

    /**
     * The archetype handlers.
     */
    private final ArchetypeHandlers<DocumentTemplateLocator> handlers;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DocumentTemplateLocatorFactory.class);

    /**
     * Constructs a {@link DocumentTemplateLocatorFactory}.
     *
     * @param service the archetype service
     */
    public DocumentTemplateLocatorFactory(ArchetypeService service) {
        handlers = new ArchetypeHandlers<>("DefaultDocumentTemplateLocatorFactory", DocumentTemplateLocator.class,
                                           service);
    }

    /**
     * Locates a document template for the supplied object.
     *
     * @param object  the object
     * @param context the context
     * @return a template for the object, or {@code null}, if none is found
     */
    public DocumentTemplate getTemplate(IMObject object, Context context) {
        DocumentTemplateLocator locator = getDocumentTemplateLocator(object, context);
        return locator.getTemplate();
    }

    /**
     * Returns a {@link DocumentTemplateLocator} for the specified object.
     * <p/>
     * This uses the registered {@link ArchetypeHandler} for the supplied object, if one is present.
     * If not, or it cannot be constructed, a {@link ContextDocumentTemplateLocator} is returned.
     *
     * @param object  th object
     * @param context the context
     * @return the template locator
     */
    public DocumentTemplateLocator getDocumentTemplateLocator(IMObject object, Context context) {
        ArchetypeHandler<DocumentTemplateLocator> handler = handlers.getHandler(object.getArchetype());
        DocumentTemplateLocator locator = null;
        if (handler != null) {
            try {
                locator = create(handler, object, context);
                if (locator == null) {
                    locator = create(handler, object);
                }
                if (locator == null) {
                    locator = create(handler);
                }
            } catch (Exception exception) {
                log.warn("Failed to create {}: {}", handler.getType(), exception.getMessage(), exception);
            }
        }
        if (locator == null) {
            locator = new ContextDocumentTemplateLocator(object, context);
        }
        return locator;
    }

    /**
     * Attempts to create a {@link DocumentTemplateLocator} given a handler with the specified parameters.
     *
     * @param handler    the handler
     * @param parameters the parameters
     * @return the new {@link DocumentTemplateLocator}, or {@code null} if no constructor exists for the parameters
     * @throws Exception if construction fails
     */
    private DocumentTemplateLocator create(ArchetypeHandler<DocumentTemplateLocator> handler, Object... parameters)
            throws Exception {
        Class<?>[] types = new Class[parameters.length];
        for (int i = 0; i < parameters.length; ++i) {
            types[i] = parameters[i].getClass();
        }
        Constructor<DocumentTemplateLocator> constructor = getMatchingAccessibleConstructor(handler.getType(), types);
        return (constructor != null) ? constructor.newInstance(parameters) : null;
    }
}