/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.till;

import org.openvpms.archetype.rules.finance.till.TillRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.domain.till.Till;
import org.openvpms.tilldrawer.exception.TillDrawerException;
import org.openvpms.tilldrawer.service.TillDrawerService;

/**
 * Helper to open a cash drawer associated with a till.
 *
 * @author Tim Anderson
 */
public class CashDrawer {

    /**
     * The till.
     */
    private final Till till;

    /**
     * The till drawer service.
     */
    private final TillDrawerService tillDrawerService;

    /**
     * The till rules.
     */
    private final TillRules rules;

    /**
     * Constructs a {@link CashDrawer}.
     *
     * @param till the till
     */
    public CashDrawer(Till till, TillDrawerService tillDrawerService, TillRules rules) {
        this.till = till;
        this.tillDrawerService = tillDrawerService;
        this.rules = rules;
    }

    /**
     * Determines if the drawer can be opened.
     *
     * @return {@code true} if the drawer can be opened
     * @throws TillDrawerException for any error
     */
    public boolean canOpen() {
        return tillDrawerService.canOpen(till);
    }

    /**
     * Determines if an act needs the cash drawer open.
     * <p/>
     * The cash drawer needs to be opened for POSTED payments or refunds that have cash, cheque, or credit items,
     * or payment EFT items with a non-zero cash out.
     * <p/>
     * Credit items return {@code true} to support users that put the receipt in the drawer.
     *
     * @param act the act
     * @return {@code true} if the act needs the cash drawer open
     */
    public boolean needsOpen(Act act) {
        return rules.needsDrawerOpen(act);
    }

    /**
     * Opens the drawer.
     *
     * @throws TillDrawerException if the drawer cannot be opened
     */
    public void open() {
        tillDrawerService.open(till);
    }
}
