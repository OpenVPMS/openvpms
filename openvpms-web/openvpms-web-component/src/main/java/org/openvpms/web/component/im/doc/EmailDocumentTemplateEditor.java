/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.doc.DelegatingDocumentHandler;
import org.openvpms.archetype.rules.doc.SupportedContentDocumentHandler;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.jasper.JRXMLDocumentHandler;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


/**
 * Editor for <em>entity.documentTemplateEmail*</em> entities.
 *
 * @author Tim Anderson
 */
public class EmailDocumentTemplateEditor extends AbstractDocumentTemplateEditor {

    /**
     * Document content type.
     */
    protected static final String DOCUMENT_CONTENT = "DOCUMENT";

    /**
     * The content type node name.
     */
    private static final String CONTENT_TYPE = "contentType";

    /**
     * Constructs a {@link EmailDocumentTemplateEditor}.
     *
     * @param template the object to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context. May be {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public EmailDocumentTemplateEditor(Entity template, IMObject parent, LayoutContext context) {
        super(template, parent, false, null, context);
        ArchetypeService service = getService();
        setDocumentHandler(new DelegatingDocumentHandler(new EmailDocumentHandler(service),
                                                         new JRXMLDocumentHandler(service)));
        updateDocumentState();
        disableMacroExpansion("subject");
        disableMacroExpansion("subjectSource");
        disableMacroExpansion("content");
        disableMacroExpansion("contentSource");
        getProperty(CONTENT_TYPE).addModifiableListener(modifiable -> onContentTypeChanged());
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        // TODO - this may leave any uploaded documents unreachable
        return new EmailDocumentTemplateEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Returns the content type.
     *
     * @return the content type. May be {@code null}
     */
    protected String getContentType() {
        return getProperty(CONTENT_TYPE).getString();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new EmailDocumentTemplateLayoutStrategy(getSelector());
    }

    /**
     * Save any edits.
     * <p/>
     * This uses {@link #saveChildren()} to save the children prior to invoking {@link #saveObject()}.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        super.doSave();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateDefaultEmailAddress(validator);
    }

    /**
     * Validates the default email address, if it is specified.
     * <p/>
     * This supports both addr-spec and name-addr formats from RFC5322.
     *
     * @param validator the validator
     * @return {@code true} if there is none, or it is valid; otherwise {@code false}
     */
    protected boolean validateDefaultEmailAddress(Validator validator) {
        Property property = getProperty("defaultEmailAddress");
        String email = property.getString();
        if (email != null) {
            try {
                new InternetAddress(email, true);
            } catch (AddressException exception) {
                validator.add(property, new ValidatorError(property, exception.getMessage()));
            }
        }
        return validator.isValid();
    }

    /**
     * Invoked when the content type changes.
     * <p/>
     * This changes the layout to switch between document and text content.
     */
    private void onContentTypeChanged() {
        String type = getContentType();
        if (type != null) {
            // NOTE: select fields deselect the current value before selecting the new one, so need to check to
            // see if a value is present before changing layout
            updateDocumentState();
            onLayout();
        }
    }

    /**
     * Invoked to update how the document is handled.
     * <p/>
     * If the content type is DOCUMENT, then the document is required, otherwise it will be deleted on save.
     */
    private void updateDocumentState() {
        boolean isDocument = DOCUMENT_CONTENT.equals(getContentType());
        setDocumentRequired(isDocument);
        setDeleteDocument(!isDocument);
    }

    private static class EmailDocumentHandler extends SupportedContentDocumentHandler {

        private static final String[] SUPPORTED_EXTENSIONS = {DocFormats.ODT_EXT, DocFormats.DOC_EXT,
                                                              DocFormats.HTML_EXT, DocFormats.RTF_EXT};

        private static final String[] SUPPORTED_MIME_TYPES = {DocFormats.ODT_TYPE, DocFormats.DOC_TYPE,
                                                              DocFormats.HTML_TYPE, DocFormats.RTF_TYPE};


        public EmailDocumentHandler(ArchetypeService service) {
            super(SUPPORTED_EXTENSIONS, SUPPORTED_MIME_TYPES, service);
        }
    }
}