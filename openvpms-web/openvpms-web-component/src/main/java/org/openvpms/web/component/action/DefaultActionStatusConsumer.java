/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import nextapp.echo2.app.ApplicationInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

/**
 * Default action status consumer implementation.
 * <p/>
 * If interactive, this displays an error dialog if the status indicates a failure, and the user hasn't already been
 * notified. If not interactive, it logs the error.
 *
 * @author Tim Anderson
 */
class DefaultActionStatusConsumer implements Consumer<ActionStatus> {

    /**
     * Determines if this is interactive or not.
     */
    private final boolean interactive;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DefaultActionStatusConsumer.class);

    /**
     * Constructs a {@link DefaultActionStatusConsumer}.
     */
    public DefaultActionStatusConsumer() {
        this(true);
    }

    /**
     * Constructs a {@link DefaultActionStatusConsumer}.
     *
     * @param interactive if {@code true}, the consumer can display error dialogs, else it logs
     */
    public DefaultActionStatusConsumer(boolean interactive) {
        this.interactive = interactive;
    }

    /**
     * Performs this operation on the given argument.
     *
     * @param status the input argument
     */
    @Override
    public void accept(ActionStatus status) {
        if (status.failed() && !status.notified()) {
            status.setNotified();
            if (canShowDialog()) {
                ActionBuilder.newErrorDialog(status)
                        .show();
            } else {
                log.error(status.toString(), status.getException());
            }
        }
    }

    /**
     * Determines if a dialog can be displayed.
     *
     * @return {@code true} if a dialog can be displayed, otherwise {@code false}
     */
    private boolean canShowDialog() {
        return interactive && ApplicationInstance.getActive() != null;
    }
}
