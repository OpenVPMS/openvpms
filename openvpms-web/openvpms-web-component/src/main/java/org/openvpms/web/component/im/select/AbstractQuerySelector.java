/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.select;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.DocumentEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.web.component.error.DialogErrorHandler;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserDialog;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.DialogManager;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.DocumentListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.focus.FocusCommand;
import org.openvpms.web.echo.text.TextField;

import java.util.List;
import java.util.Objects;

/**
 * A selector that allows available objects to be queried.
 *
 * @author Tim Anderson
 */
public abstract class AbstractQuerySelector<T> extends Selector<T> {

    /**
     * Display name for the types of object this may select.
     */
    private final String type;

    /**
     * Update listener for the text field.
     */
    private final DocumentListener textListener;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The selected object.
     */
    private T object;

    /**
     * Determines if objects may be created.
     */
    private boolean allowCreate;

    /**
     * The previous selector text, to avoid spurious updates.
     */
    private String prevText;

    /**
     * The listener. May be {@code null}
     */
    private SelectorListener<T> listener;

    /**
     * Determines if a selection dialog is currently been popped up.
     */
    private boolean inSelect;

    /**
     * Constructs an {@link AbstractQuerySelector}.
     *
     * @param type        display name for the types of objects this may select
     * @param allowCreate determines if objects may be created
     * @param context     the layout context
     */
    public AbstractQuerySelector(String type, boolean allowCreate, LayoutContext context) {
        this(type, allowCreate, ButtonStyle.RIGHT, true, context);
    }

    /**
     * Constructs an {@link AbstractQuerySelector}.
     *
     * @param type        display name for the types of objects this may select
     * @param allowCreate determines if objects may be created
     * @param style       the button style
     * @param editable    determines if the selector is editable
     * @param context     the layout context
     */
    public AbstractQuerySelector(String type, boolean allowCreate, ButtonStyle style, boolean editable,
                                 LayoutContext context) {
        super(style, editable);
        this.type = type;
        this.allowCreate = allowCreate;
        textListener = new DocumentListener() {
            public void onUpdate(DocumentEvent event) {
                onTextChanged();
            }
        };
        this.context = context;
        setFormat(Format.NAME);
        getSelect().addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                onSelect();
            }
        });

        TextField text = getTextField();

        text.getDocument().addDocumentListener(textListener);

        // Register an action listener for Enter
        text.addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                onTextAction();
            }
        });
    }

    /**
     * Sets the current object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(T object) {
        this.object = object;
        TextField text = getTextField();
        text.getDocument().removeDocumentListener(textListener);
        if (object != null) {
            super.setObject(getName(object), getDescription(object), getActive(object));
        } else {
            super.setObject(null, null, true);
        }
        text.getDocument().addDocumentListener(textListener);
        prevText = text.getText();
    }


    /**
     * Returns the current object.
     *
     * @return the current object. May be {@code null}
     */
    public T getObject() {
        return object;
    }

    /**
     * Sets the listener.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setListener(SelectorListener<T> listener) {
        this.listener = listener;
    }

    /**
     * Determines if the selector is valid.
     * It is valid if no dialog is currently displayed and:
     * <ul>
     * <li>an object has been selected and the entered text is the same as its name
     * <li>no object is present and no text is input
     * </ul>
     *
     * @return {@code true} if the selector is valid, otherwise {@code false}
     */
    public boolean isValid() {
        boolean valid = !inSelect;
        if (valid) {
            String text = getText();
            if (object != null) {
                valid = Objects.equals(getName(object), text);
            } else {
                valid = StringUtils.isEmpty(text);
            }
        }
        return valid;
    }

    /**
     * Determines if a selection dialog has been popped up.
     *
     * @return {@code true} if a selection dialog has been popped up
     * otherwise {@code false}
     */
    public boolean inSelect() {
        return inSelect;
    }

    /**
     * Determines if objects may be created.
     *
     * @param create if {@code true}, objects may be created
     */
    public void setAllowCreate(boolean create) {
        allowCreate = create;
    }

    /**
     * Determines if objects may be created.
     *
     * @return {@code true} if objects may be created
     */
    public boolean allowCreate() {
        return allowCreate;
    }

    /**
     * Returns the display name for the types of objects this may select.
     *
     * @return the type display name
     */
    public String getType() {
        return type;
    }

    /**
     * Returns the name of the object.
     *
     * @param object the object
     * @return the object name. May be {@code null}
     */
    protected abstract String getName(T object);

    /**
     * Returns the description of the object.
     *
     * @param object the object
     * @return the object description. May be {@code null}
     */
    protected abstract String getDescription(T object);

    /**
     * Determines if the object is active.
     *
     * @param object the object
     * @return {@code true} if the object is active, {@code false} if it is inactive
     */
    protected abstract boolean getActive(T object);

    /**
     * Pop up a dialog to select an object.
     *
     * @param runQuery if {@code true} run the query
     */
    protected void onSelect(boolean runQuery) {
        try {
            FocusCommand focus = new FocusCommand();
            String text = getText();
            if (StringUtils.isEmpty(text)) {
                text = null;
            }
            Browser<T> browser = createBrowser(text, runQuery);
            BrowserDialog<T> popup = createDialog(browser);

            popup.addWindowPaneListener(new WindowPaneListener() {
                public void onClose(WindowPaneEvent event) {
                    focus.restore();
                    setInSelect(false);
                    if (popup.createNew()) {
                        onCreate();
                    } else {
                        T object = popup.getSelected();
                        if (object != null) {
                            onSelected(object, browser);
                        }
                    }
                }
            });

            setInSelect(true);
            popup.show();
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Invoked when an object is selected via a browser.
     *
     * @param object  the selected object
     * @param browser the browser
     */
    protected void onSelected(T object, Browser<T> browser) {
        T current = getObject();
        setObject(object);
        getFocusGroup().setFocus(); // set the focus back to the component
        if (listener != null && !Objects.equals(current, object)) {
            listener.selected(object, browser);
        }
    }

    /**
     * Invoked to create a new object. Notifies the listener.
     */
    protected void onCreate() {
        if (listener != null) {
            listener.create();
        }
    }

    /**
     * Creates a new browser.
     *
     * @param value    a value to filter results by. May be {@code null}
     * @param runQuery if {@code true} run the query
     * @return a return a new browser
     */
    protected abstract Browser<T> createBrowser(String value, boolean runQuery);

    /**
     * Creates a new dialog to display a browser.
     *
     * @param browser the browser
     * @return the dialog
     */
    protected BrowserDialog<T> createDialog(Browser<T> browser) {
        return new BrowserDialog<>(type, browser, allowCreate, context.getHelpContext());
    }

    /**
     * Returns the results matching the specified value.
     *
     * @param value the value. May be {@code null}
     * @return the results matching the value
     */
    protected abstract ResultSet<T> getMatches(String value);

    /**
     * Creates the select button.
     *
     * @param buttonId        the button identifier
     * @param enableShortcuts if {@code true}, enable shortcuts
     * @return the select button
     */
    protected Button createSelectButton(String buttonId, boolean enableShortcuts) {
        Button select = ButtonFactory.create(null, "select");
        select.setId(buttonId);
        return select;
    }

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return context;
    }

    /**
     * Invoked when the text field is updated.
     */
    protected void onTextChanged() {
        String text = getText();
        if (!Objects.equals(text, prevText)) {
            if (StringUtils.isEmpty(text)) {
                setObject(null);
                notifySelected();
            } else {
                try {
                    ResultSet<T> set = getMatches(text);
                    if (set != null && set.hasNext()) {
                        IPage<T> page = set.next();
                        List<T> rows = page.getResults();
                        int size = rows.size();
                        if (size == 0) {
                            setObject(null);
                            notifySelected();
                        } else if (size == 1) {
                            T object = rows.get(0);
                            setObject(object);
                            notifySelected();
                        } else if (canSelect()) {
                            onSelect(true);
                        }
                    }
                } catch (OpenVPMSException exception) {
                    ErrorHelper.show(exception);
                    listener.selected(null);
                }
            }
        }
    }

    /**
     * Pops up a dialog to select an object.
     * <p/>
     * Only pops up a dialog if one isn't already visible, no error dialog is being displayed, and there are
     * no modal dialogs displayed above the button.
     * <p/>
     * The error dialog requirement avoids the following event sequence:
     * <ol>
     * <li>text is entered in the field, and enter pressed</li>
     * <li>echo invokes document listener (i.e. {@link #onTextChanged()})</li>
     * <li>the selector listener is triggered, but clears the text and displays an error message. This can
     * occur if a product template fails to expand</li>
     * <li>echo invokes the action listener (i.e. {@link #onSelect()})</li>
     * </ol>
     * The modal dialog requirement avoids the following event sequence:
     * <ol>
     * <li>text is entered into the field that resolves to a single object, but <em>enter is not pressed</em></li>
     * <li>the button is pressed</li>
     * <li>echo invokes {@link #onTextChanged()}, which locates the object, and triggers the selector listener</li>
     * <li>echo invokes the action listener (i.e. {@link #onSelect()})</li>
     * </ol>
     */
    private void onSelect() {
        if (canSelect()) {
            onSelect(false);
        }
    }

    /**
     * Invoked by the action listener associated with the text field.
     * <p/>
     * This is provided to handle Enter being pressed in the field when it is empty, to display a search dialog.
     * <p/>
     * Note that {@link #onTextChanged} will have been invoked just prior to this method if the text was updated.
     */
    private void onTextAction() {
        if (!isValid() || StringUtils.isEmpty(getText())) {
            onSelect();
        }
    }

    /**
     * Determines if a selection dialog has been popped up.
     *
     * @param select if {@code true} denotes that a selection dialog has been popped up
     */
    private void setInSelect(boolean select) {
        this.inSelect = select;
    }

    /**
     * Notifies the listener of selection via the text field.
     */
    private void notifySelected() {
        if (listener != null) {
            listener.selected(getObject());
        }
    }

    /**
     * Determines if a browser can be shown.
     * <p/>
     * A browser can be shown if no browser is already visible, no error dialog is being displayed, and there are
     * no modal dialogs displayed above the button.
     *
     * @return {@code true} if a browser can be shown
     */
    private boolean canSelect() {
        return !inSelect && !inError() && !DialogManager.isHidden(getComponent());
    }

    /**
     * Determines if an error dialog is being displayed.
     *
     * @return {@code true} if an error dialog is being displayed
     */
    private boolean inError() {
        ErrorHandler handler = ErrorHandler.getInstance();
        return handler instanceof DialogErrorHandler && ((DialogErrorHandler) handler).inError();
    }
}
