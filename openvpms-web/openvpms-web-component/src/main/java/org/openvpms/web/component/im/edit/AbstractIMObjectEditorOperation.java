/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.error.ErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A strategy for performing an operation on an editor, and handling failures.
 * <p/>
 * This supports reloading the editor.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectEditorOperation<T extends IMObjectEditor> implements IMObjectEditorOperation<T> {

    /**
     * Formats operation failure reasons for display to users.
     */
    public interface FailureFormatter {

        /**
         * Returns the error title, when displaying a failure.
         *
         * @param editor the editor
         * @return the title
         */
        String getTitle(IMObjectEditor editor);

        /**
         * Returns a user-displayable message, describing the reason for a failure,
         * after the editor has been successfully reloaded.
         *
         * @param editor    the editor
         * @param exception the reason for the failure
         * @return the message
         */
        String getMessage(IMObjectEditor editor, Throwable exception);
    }

    /**
     * The failure formatter.
     */
    private final FailureFormatter formatter;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractIMObjectEditorOperation.class);

    /**
     * Constructs a {@link AbstractIMObjectEditorOperation}.
     *
     * @param formatter the formatter for formatting failure reasons
     */
    public AbstractIMObjectEditorOperation(FailureFormatter formatter) {
        this.formatter = formatter;
    }

    /**
     * Returns the failure formatter.
     *
     * @return the failure formatter
     */
    public FailureFormatter getFormatter() {
        return formatter;
    }

    /**
     * Invoked when the operation fails.
     * <p/>
     * This attempts to reload the editor via {@link #reload(IMObjectEditor)}.<br/>
     * If successful, this call {@link #reloaded(String, String, IMObjectEditor, Runnable)}, otherwise
     * it calls {@link #unrecoverableFailure(IMObjectEditor, Throwable, Runnable)}.
     *
     * @param editor    the editor
     * @param exception the reason for the operation failing
     * @param listener  the listener to notify when error handling is complete. May be {@code null}
     */
    @Override
    public void failed(T editor, Throwable exception, Runnable listener) {
        // attempt to reload the editor
        if (reload(editor)) {
            // reloaded the editor to the last saved state
            log.error("Reloaded editor {} for {} due to: {}", editor.getClass().getName(),
                      editor.getObject().getObjectReference(), exception.getMessage(), exception);
            String title = formatter.getTitle(editor);
            String message = formatter.getMessage(editor, exception);
            reloaded(title, message, editor, listener);
        } else {
            log.error("Failed to reload editor {} for {} due to: {}", editor.getClass().getName(),
                      editor.getObject().getObjectReference(), exception.getMessage());
            // exception will be logged via ErrorHandler so don't log stack
            unrecoverableFailure(editor, exception, listener);
        }
    }

    /**
     * Invoked to reload the object if the operation fails.
     *
     * @param editor the editor
     * @return {@code true} if the object was reloaded, otherwise {@code false}
     */
    protected boolean reload(T editor) {
        return false;
    }

    /**
     * Invoked after an error has occurred, and the editor has been successfully reloaded.
     * <p/>
     * This implementation displays the error.
     *
     * @param title     the message title
     * @param message   the message
     * @param oldEditor the previous instance of the editor
     * @param listener  the listener to notify when error handling is complete. May be {@code null}
     */
    protected void reloaded(String title, String message, T oldEditor, Runnable listener) {
        ErrorHandler.getInstance().error(title, message, null, listener);
    }

    /**
     * Invoked when the operation fails and the editor cannot be reverted.
     * <p/>
     * This implementation displays the error.
     *
     * @param editor    the editor
     * @param exception the cause of the failure
     * @param listener  the listener to notify when error handling is complete. May be {@code null}
     */
    protected void unrecoverableFailure(T editor, Throwable exception, Runnable listener) {
        String title = formatter.getTitle(editor);
        ErrorHelper.show(title, editor.getDisplayName(), editor.getObject(), exception, listener);
    }
}