/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.report.DocumentConverter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.print.TemplatedIMPrinter;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.report.TemplatedReporter;


/**
 * A printer for {@link DocumentAct}s.
 *
 * @author Tim Anderson
 */
public class DocumentActPrinter extends TemplatedIMPrinter<IMObject> {

    /**
     * Constructs a {@link DocumentActPrinter}.
     *
     * @param object         the object to print
     * @param locator        the document template locator
     * @param printerContext the printer context
     * @param context        the context
     * @param factory        the reporter factory
     */
    public DocumentActPrinter(DocumentAct object, DocumentTemplateLocator locator, PrinterContext printerContext,
                              Context context, ReporterFactory factory) {
        super(factory.create(object, locator, TemplatedReporter.class), printerContext, context);
    }

    /**
     * Prints the object.
     *
     * @param printer the printer
     * @throws OpenVPMSException for any error
     */
    @Override
    public void print(DocumentPrinter printer) {
        DocumentAct act = (DocumentAct) getObject();
        Document doc = getDocument(act.getDocument());
        if (doc == null) {
            super.print(printer);
        } else {
            print(doc, printer);
        }
    }

    /**
     * Returns a document corresponding to that which would be printed.
     *
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public Document getDocument() {
        return getDocument(null, false);
    }

    /**
     * Returns a document corresponding to that which would be printed.
     * <p>
     * If the document cannot be printed then it is returned unchanged.
     *
     * @param mimeType the mime type. If {@code null} the default mime type associated with the report will be used.
     * @param email    if {@code true} indicates that the document will be emailed. Documents generated from templates
     *                 can perform custom formatting
     * @return a document
     * @throws OpenVPMSException for any error
     */
    @Override
    public Document getDocument(String mimeType, boolean email) {
        DocumentAct act = (DocumentAct) getObject();
        Document result = getDocument(act.getDocument());
        DocumentConverter converter = getConverter();
        if (result != null && mimeType != null && !mimeType.equals(result.getMimeType()) &&
            converter.canConvert(result, mimeType)) {
            result = converter.convert(result, mimeType, email);
        }
        if (result == null) {
            result = super.getDocument(mimeType, email);
        }
        return result;
    }
}
