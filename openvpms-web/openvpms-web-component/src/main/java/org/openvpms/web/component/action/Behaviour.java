/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

/**
 * Determines the retry behaviour of an action.
 *
 * @author Tim Anderson
 * @author Tim Anderson
 */
public class Behaviour {

    public static class BehaviourBuilder<T extends BehaviourBuilder<T>> {

        /**
         * Determines which instance of an object to supply to the action.
         */
        private Instance instance = Instance.ORIGINAL;

        /**
         * Determines the behaviour if one or more objects is missing.
         */
        private Missing missing = Missing.FAIL;

        /**
         * Determines the behaviour if one or more objects is changed.
         */
        private Changed changed = Changed.FAIL;

        /**
         * The number of times to retry the action.
         */
        private int retries;

        /**
         * The initial delay before retrying the action, in milliseconds.
         */
        private long delay;

        /**
         * Constructs a {@link BehaviourBuilder}.
         */
        protected BehaviourBuilder() {
            super();
        }

        /**
         * Constructs a {@link BehaviourBuilder}.
         *
         * @param behaviour the initial behaviour
         */
        protected BehaviourBuilder(Behaviour behaviour) {
            this.instance = behaviour.instance;
            this.missing = behaviour.missing;
            this.changed = behaviour.changed;
            this.retries = behaviour.retries;
            this.delay = behaviour.delay;
        }

        /**
         * Indicates that the original instances of objects should be used on first access and retry.
         * <p/>
         * This implies {@link #failIfChanged()} and {@link #failIfMissing()}.
         *
         * @return this
         */
        public T useOriginalObject() {
            return useInstance(Instance.ORIGINAL);
        }

        /**
         * Indicates that the latest instances of objects should always be used.
         * <p/>
         * This implies {@link #runIfChanged()} and {@link #failIfMissing()}.
         *
         * @return this
         */
        public T useLatestInstance() {
            return useInstance(Instance.LATEST);
        }

        /**
         * Indicates that the latest instances of objects should be used on retry.
         * <p/>
         * This implies {@link #runIfChanged()} and {@link #failIfMissing()}.
         *
         * @return this
         */
        public T useLatestInstanceOnRetry() {
            return useInstance(Instance.LATEST_ON_RETRY);
        }

        /**
         * Indicates that the action should fail on retry, if any object is missing.
         * <p/>
         * Use this if the action cannot be re-run if an object has been deleted/cannot be retrieved since the action
         * was first run.
         * <p/>
         * This is the default behaviour.
         *
         * @return this
         */
        public T failIfMissing() {
            return missing(Missing.FAIL);
        }

        /**
         * Indicates that the action should be skipped on retry, if any object is missing.
         * <p/>
         * Use this if the action should not be re-run if an object has been deleted/cannot be retrieved since the
         * action was first run, and this is not an error.
         *
         * @return this
         */
        public T skipIfMissing() {
            return skipIfMissing(true);
        }

        /**
         * Determines the behaviour if any object is missing on retry.
         *
         * @param skipIfMissing if {@code true}, skip the action if any object is missing; if {@code false}, fail
         * @return this
         */
        public T skipIfMissing(boolean skipIfMissing) {
            return missing(skipIfMissing ? Missing.SKIP : Missing.FAIL);
        }

        /**
         * Indicates that the action should fail on retry, if any object is changed.
         * <p/>
         * Use this if the action cannot be re-run if an object has been changed since the action was first run.
         * <p/>
         * This is the default behaviour.
         *
         * @return this
         */
        public T failIfChanged() {
            return changed(Changed.FAIL);
        }

        /**
         * Indicates that the action should be skipped on retry, if any object is changed.
         * <p/>
         * Use this if the action should not be re-run if an object has been changed since the action was first run,
         * and this is not an error.
         *
         * @return this
         */
        public T skipIfChanged() {
            return changed(Changed.SKIP);
        }

        /**
         * Indicates that the action should be run on retry, if any object is changed.
         * <p/>
         * Use this if the action can safely update an object that has changed since the action was first run.
         *
         * @return this
         */
        public T runIfChanged() {
            return changed(Changed.RUN);
        }

        /**
         * Sets the number of times to retry the action, before failing.
         * <p/>
         * This may be ignored for interactive retries.
         *
         * @param retries the number of retries
         * @return this
         */
        public T retries(int retries) {
            this.retries = retries;
            return getThis();
        }

        /**
         * Sets the initial delay before retrying the action.
         * <p/>
         * This only applies when performing automatic retries.
         *
         * @param delay the delay in milliseconds, or {@code 0} to disable the delay
         * @return this
         */
        public T delay(long delay) {
            this.delay = delay;
            return getThis();
        }

        /**
         * Builds the behaviour.
         *
         * @return a new behaviour
         */
        public Behaviour build() {
            return new Behaviour(instance, missing, changed, retries, delay);
        }

        /**
         * Determines if the latest instances of objects should be used on first run.
         *
         * @param instance determines which instance of an object to supply to the action
         * @return this
         */
        private T useInstance(Instance instance) {
            this.instance = instance;
            if (instance == Instance.ORIGINAL) {
                failIfChanged();
                failIfMissing();
            } else {
                runIfChanged();
                failIfMissing();
            }
            return getThis();
        }

        /**
         * Sets the behaviour if one or more objects are missing.
         *
         * @param missing the missing behaviour
         * @return this
         */
        private T missing(Missing missing) {
            this.missing = missing;
            return getThis();
        }

        @SuppressWarnings("unchecked")
        private T getThis() {
            return (T) this;
        }

        /**
         * Sets the behaviour if one or more objects are changed.
         *
         * @param changed the changed behaviour
         * @return this
         */
        private T changed(Changed changed) {
            this.changed = changed;
            return getThis();
        }
    }

    /**
     * The default no. of times to retry automatically.
     */
    public static final int DEFAULT_RETRIES = 3;

    /**
     * Determines which instance of an object to supply to the action.
     */
    private final Instance instance;

    /**
     * The behaviour if one or more objects is missing.
     */
    private final Missing missing;

    /**
     * Determines the behaviour if one or more objects is changed.
     */
    private final Changed changed;

    /**
     * The number of times to retry the action.
     */
    private final int retries;

    /**
     * The delay before retrying the action, in milliseconds.
     */
    private final long delay;

    /**
     * The default initial delay.
     */
    private static final int DEFAULT_DELAY = 1000;

    /**
     * The default behaviour.
     */
    private static final Behaviour DEFAULT = newBehaviour().useOriginalObject()
            .retries(DEFAULT_RETRIES)
            .delay(DEFAULT_DELAY)
            .build();

    /**
     * Constructs a {@link Behaviour}.
     *
     * @param instance determines which instance of an object should be passed to the action
     * @param missing  the behaviour if one or more objects is missing
     * @param changed  the behaviour if one or more objects is changed
     * @param retries  the number of times to retry the action
     * @param delay    the delay before retrying the action, in milliseconds
     */
    private Behaviour(Instance instance, Missing missing, Changed changed, int retries, long delay) {
        this.instance = instance;
        this.missing = missing;
        this.changed = changed;
        this.delay = delay;
        this.retries = retries;
    }

    /**
     * Determines if the action should fail if one or more objects is missing.
     *
     * @return {@code true} if the action should fail, otherwise {@code false}
     */
    public boolean failIfMissing() {
        return missing == Missing.FAIL;
    }

    /**
     * Determines if the action should be skipped if one or more objects is missing.
     *
     * @return {@code true} if the action should be skipped, otherwise {@code false}
     */
    public boolean skipIfMissing() {
        return missing == Missing.SKIP;
    }

    /**
     * Determines if the action should fail, if one or more objects have changed.
     *
     * @return {@code true} if the action should fail, otherwise {@code false}
     */
    public boolean failIfChanged() {
        return changed == Changed.FAIL;
    }

    /**
     * Determines if the action should be skipped if one or more objects are changed.
     *
     * @return {@code true} if the action should be skipped, otherwise {@code false}
     */
    public boolean skipIfChanged() {
        return changed == Changed.SKIP;
    }

    /**
     * Determines if the action should be run if one or more objects are changed.
     *
     * @return {@code true} if the action should be run, otherwise {@code false}
     */
    public boolean runIfChanged() {
        return changed == Changed.RUN;
    }

    /**
     * Determines if the original instances of objects should be used on first and subsequent runs.
     *
     * @return {@code true} if the original instances of objects should be used on first and subsequent runs,
     * otherwise {@code false}
     */
    public boolean useOriginalObject() {
        return instance == Instance.ORIGINAL;
    }

    /**
     * Determines if the latest instances of objects should be used on first and subsequent runs.
     *
     * @return {@code true} if the latest instances of objects should be used on first and subsequent runs,
     * otherwise {@code false}
     */
    public boolean useLatestInstance() {
        return instance == Instance.LATEST;
    }

    /**
     * Determines if the latest instances of objects should be used on retry.
     *
     * @return {@code true} if the latest instance of objects should be used on retry; {@code false} if the instances
     * *                  from the first run should be used
     */
    public boolean useLatestInstanceOnRetry() {
        return instance == Instance.LATEST_ON_RETRY;
    }

    /**
     * Returns the number of times to retry the action, before failing.
     * <p/>
     * This may be ignored for interactive retries.
     *
     * @return the number of times to retry the action
     */
    public int getRetries() {
        return retries;
    }

    /**
     * Returns the initial delay before retrying the action.
     *
     * @return the delay, in milliseconds
     */
    public long getDelay() {
        return delay;
    }

    /**
     * Returns a builder for a new behaviour.
     *
     * @return a new builder
     */
    public static BehaviourBuilder<?> newBehaviour() {
        return new BehaviourBuilder<>();
    }

    /**
     * Returns a builder for a new behaviour.
     *
     * @param behaviour the initial state
     * @return a new builder
     */
    public static BehaviourBuilder<?> newBehaviour(Behaviour behaviour) {
        return new BehaviourBuilder<>(behaviour);
    }

    /**
     * Returns the default behaviour.
     * <p/>
     * This indicates the action to fail if any object has been changed or is missing on retry.
     *
     * @return the default behaviour
     */
    public static Behaviour getDefault() {
        return DEFAULT;
    }

    /**
     * Determines which instance of an object to supply to actions.
     */
    private enum Instance {

        ORIGINAL, LATEST,

        LATEST_ON_RETRY
    }

    /**
     * Determines the behaviour if one or more objects is missing.
     */
    private enum Missing {
        FAIL, SKIP
    }

    /**
     * Determines the behaviour if one or more objects is changed.
     */
    private enum Changed {
        FAIL, SKIP, RUN
    }
}
