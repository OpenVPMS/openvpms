/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.edit;

/**
 * A listener to receive alerts during editing.
 *
 * @author Tim Anderson
 */
public interface AlertListener {

    /**
     * The alert category.
     */
    enum Category {
        INFO,
        ERROR
    }

    /**
     * Invoked to notify of an alert.
     *
     * @param message the message
     * @return an identifier that may be used to subsequently cancel the alert
     */
    String onAlert(String message);

    /**
     * Invoked to notify of an alert.
     *
     * @param message  the message
     * @param category the alert category
     * @return an identifier that may be used to subsequently cancel the alert
     */
    String onAlert(String message, Category category);

    /**
     * Invoked to notify of an alert.
     *
     * @param id      an identifier to associate with the message, for subsequent cancellation
     * @param message the message
     */
    void onAlert(String id, String message);

    /**
     * Invoked to notify of an alert.
     *
     * @param id       an identifier to associate with the message, for subsequent cancellation
     * @param message  the message
     * @param category the alert category
     */
    void onAlert(String id, String message, Category category);

    /**
     * Cancels an alert.
     *
     * @param id the alert identifier
     */
    void cancel(String id);
}
