/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.archetype.rules.doc.BaseDocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.component.system.common.jxpath.JXPathHelper;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.macro.Macros;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.IMReport;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Evaluates the content of an <em>entity.documentTemplateEmail</em> based on its <em>contentType</em>.
 *
 * @author Tim Anderson
 */
public class EmailTemplateEvaluator {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The macros.
     */
    private final Macros macros;

    /**
     * The report factory.
     */
    private final ReportFactory factory;

    /**
     * The document converter
     */
    private final DocumentConverter converter;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(EmailTemplateEvaluator.class);

    /**
     * Constructs an {@link EmailTemplateEvaluator}.
     *
     * @param service   the service
     * @param lookups   the lookups
     * @param macros    the macros
     * @param factory   the report factory
     * @param converter the document converter
     */
    public EmailTemplateEvaluator(ArchetypeService service, LookupService lookups, Macros macros,
                                  ReportFactory factory, DocumentConverter converter) {
        this.service = service;
        this.lookups = lookups;
        this.macros = macros;
        this.factory = factory;
        this.converter = converter;
    }

    /**
     * Returns the email subject.
     *
     * @param template the template
     * @param object   the object to evaluate expressions against. May be {@code null}
     * @param context  the context, used to locate the object to evaluate
     * @return the email subject. May be {@code null}
     */
    public String getSubject(EmailTemplate template, Object object, Context context) {
        String result = null;
        EmailTemplate.SubjectType type = template.getSubjectType();
        String subject = template.getSubject();
        String expression = template.getSubjectSource();
        switch (type) {
            case TEXT:
                result = subject;
                break;
            case MACRO:
                result = evaluateMacros(subject, expression, object, context);
                break;
            case XPATH:
                result = evaluateXPath(subject, expression, object, context);
                break;
        }
        return result;
    }

    /**
     * Returns the email message.
     *
     * @param template the template
     * @param object   the object to evaluate expressions against. May be {@code null}
     * @param context  the context, used to locate the object to report on
     * @return the email message, as HTML or an HTML fragment, or {@code null} if no content was present
     */
    public String getMessage(EmailTemplate template, Object object, Context context) {
        String result = null;
        EmailTemplate.ContentType type = template.getContentType();
        String content = template.getContent();
        String expression = template.getContentSource();
        switch (type) {
            case TEXT:
                result = content;
                break;
            case MACRO:
                result = evaluateMacros(content, expression, object, context);
                break;
            case XPATH:
                result = evaluateXPath(content, expression, object, context);
                break;
            case DOCUMENT:
                result = evaluateDocument(template, expression, object, context);
        }
        if (result != null && EmailTemplate.ContentType.DOCUMENT != type) {
            result = toHTML(result);
        }
        return result;
    }

    /**
     * Returns an {@link Reporter} for the template message body, if the template has a document body.
     *
     * @param template the template
     * @param object   the object to evaluate expressions against. May be {@code null}
     * @param context  the context, used to locate the object to report on
     * @return a new reporter, or {@code null} if the content is not a supported document
     */
    public Reporter<IMObject> getMessageReporter(EmailTemplate template, Object object, Context context) {
        Reporter<IMObject> result = null;
        if (EmailTemplate.ContentType.DOCUMENT == template.getContentType()) {
            String expression = template.getContentSource();
            Object contextBean = getContextBean(expression, object, context);
            if (contextBean instanceof IMObject) {
                if (factory.isIMObjectReport(template)) {
                    result = createReporter((IMObject) contextBean, template, context);
                }
            }
        }
        return result;
    }

    /**
     * Returns an {@link Reporter} for the template message body, if the template has a document body.
     *
     * @param template the template
     * @param objects  the objects to pass to the document
     * @param context  the context to pass to the document
     * @return a new reporter, or {@code null} if the content is not a supported document
     */
    public Reporter<ObjectSet> getMessageReporter(EmailTemplate template, List<ObjectSet> objects, Context context) {
        Reporter<ObjectSet> result = null;
        if (EmailTemplate.ContentType.DOCUMENT == template.getContentType()) {
            if (factory.isObjectSetReport(template, objects.size())) {
                result = createReporter(objects, template, context);
            }
        }
        return result;
    }

    /**
     * Returns the email message.
     *
     * @param reporter the reporter used to generate the message
     * @return the email message, as HTML or an HTML fragment
     */
    public String getMessage(Reporter<?> reporter) {
        String result;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        reporter.generate(DocFormats.HTML_TYPE, true, bytes);
        result = new String(bytes.toByteArray(), StandardCharsets.UTF_8);
        return result;
    }

    /**
     * Creates a reporter for an object.
     *
     * @param object   the object to report on
     * @param template the report template
     * @param context  the context
     * @return a new reporter
     */
    private Reporter<IMObject> createReporter(IMObject object, EmailTemplate template, Context context) {
        Reporter<IMObject> result;
        result = new Reporter<IMObject>(object) {
            @Override
            protected IMReport<IMObject> getReport() {
                return factory.createIMObjectReport(template);
            }
        };
        result.setFields(ReportContextFactory.create(context));
        return result;
    }

    /**
     * Creates a reporter for a list of {@link ObjectSet}s.
     *
     * @param objects  the objects to report on
     * @param template the report template
     * @param context  the context
     * @return a new reporter
     */
    private Reporter<ObjectSet> createReporter(List<ObjectSet> objects, BaseDocumentTemplate template,
                                               Context context) {
        Reporter<ObjectSet> result;
        result = new Reporter<ObjectSet>(objects) {
            @Override
            protected IMReport<ObjectSet> getReport() {
                return factory.createObjectSetReport(template);
            }
        };
        result.setFields(ReportContextFactory.create(context));
        return result;
    }

    /**
     * Evaluates a value containing macros.
     *
     * @param value      the value
     * @param object     the object to evaluate against. May be {@code null}
     * @param expression the expression used to locate the source object. May be {@code null}
     * @param context    the context
     * @return the expanded text. May be {@code null}
     */
    private String evaluateMacros(String value, String expression, Object object, Context context) {
        Object contextBean = getContextBean(expression, object, context);
        MacroVariables variables = new MacroVariables(context, service, lookups);
        return macros.runAll(value, contextBean, variables, null, true);
    }

    /**
     * Evaluates a value as a JXPath expression.
     *
     * @param value      the value
     * @param object     the object to evaluate expressions against. May be {@code null}
     * @param expression the expression used to locate the source object. May be {@code null}
     * @param context    the context
     * @return the result of the expression. May be {@code null}
     */
    private String evaluateXPath(String value, String expression, Object object, Context context) {
        MacroVariables variables = new MacroVariables(context, service, lookups);
        variables.declareVariable("nl", "\n");     // to make expressions with newlines simpler
        Object contextBean = getContextBean(expression, object, context);
        JXPathContext jxPathContext = JXPathHelper.newContext(contextBean);
        jxPathContext.setVariables(variables);
        return (String) jxPathContext.getValue(value, String.class);
    }

    /**
     * Evaluates a document template, returning the content as HTML.
     *
     * @param expression the expression used to locate the source object. May be {@code null}
     * @param object     the object to evaluate expressions against. May be {@code null}
     * @param context    the context
     * @return the document as HTML
     */
    private String evaluateDocument(EmailTemplate template, String expression, Object object, Context context) {
        String result = null;
        Object contextBean = getContextBean(expression, object, context);
        if (DocFormats.HTML_TYPE.equals(template.getMimeType())) {
            Document document = template.getDocument();
            if (document != null) {
                DocumentHandlers handlers = ServiceHelper.getBean(DocumentHandlers.class);
                DocumentHandler handler = handlers.get(document);
                try {
                    int size = document.getSize();
                    if (size <= 0) {
                        size = 1024;
                    }
                    ByteArrayOutputStream stream = new ByteArrayOutputStream(size);
                    IOUtils.copy(handler.getContent(document), stream);
                    result = new String(stream.toByteArray(), StandardCharsets.UTF_8);
                } catch (IOException exception) {
                    log.error("Failed to get HTML document, id=" + document.getId(), exception);
                }
            }
        } else if (contextBean instanceof IMObject && factory.isIMObjectReport(template)) {
            Reporter<IMObject> reporter = createReporter((IMObject) contextBean, template, context);
            result = getMessage(reporter);
        } else {
            Document document = template.getDocument();
            if (document != null && converter.canConvert(document, DocFormats.HTML_TYPE)) {
                byte[] converted = converter.export(document, DocFormats.HTML_TYPE, true);
                result = new String(converted, StandardCharsets.UTF_8);
            }
        }
        return result;
    }

    /**
     * Converts plain text to HTML by escaping any HTML characters and replacing new lines with &lt;br/&gt;
     *
     * @param string the string
     * @return the corresponding HTML fragment
     */
    private String toHTML(String string) {
        string = StringEscapeUtils.escapeHtml4(string);
        string = string.replaceAll("\n", "<br/>");
        return string;
    }

    /**
     * Returns the bean to supply to macros, xpath expressions and documents.
     * <p>
     * If the expression is non-null, it will be evaluated against the object and context, and the result
     * returned, otherwise the object will be returned.
     *
     * @param expression the expression used to locate the source object. May be {@code null}
     * @param object     the object to evaluate against. May be {@code null}
     * @param context    the context
     * @return the context bean
     */
    private Object getContextBean(String expression, Object object, Context context) {
        Object result;
        if (object == null) {
            object = new Object();
        }
        if (!StringUtils.isBlank(expression)) {
            MacroVariables variables = new MacroVariables(context, service, lookups);
            JXPathContext xpathContext = JXPathHelper.newContext(object);
            xpathContext.setVariables(variables);
            result = xpathContext.getValue(expression);
        } else {
            result = object;
        }
        return result;
    }
}
