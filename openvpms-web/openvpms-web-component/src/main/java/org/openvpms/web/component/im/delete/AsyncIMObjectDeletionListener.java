/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;


import org.openvpms.component.model.object.IMObject;

/**
 * An {@link IMObjectDeletionListener} that notifies listeners of completion/failure after dialogs close.
 *
 * @author Tim Anderson
 */
public abstract class AsyncIMObjectDeletionListener<T extends IMObject> extends AbstractIMObjectDeletionListener<T> {

    /**
     * Constructs an {@link AsyncIMObjectDeletionListener}.
     */
    public AsyncIMObjectDeletionListener() {
        setFailureCallback(this::failed);
    }

    /**
     * Notifies that an object has been deleted.
     *
     * @param object the deleted object
     */
    @Override
    public void deleted(T object) {
        completed();
    }

    /**
     * Notifies that an object has been deactivated.
     * <p>
     * This implementation does nothing.
     *
     * @param object the deactivated object
     */
    @Override
    public void deactivated(T object) {
        completed();
    }

    /**
     * Notifies that an object cannot be deleted, and has already been deactivated.
     *
     * @param object the object
     */
    @Override
    public void alreadyDeactivated(T object) {
        completed();
    }

    /**
     * Invoked after an object has been deleted or deactivated.
     */
    protected abstract void completed();

    /**
     * Invoked after deletion has failed, and the user has closed the error dialog.
     */
    protected abstract void failed();
}