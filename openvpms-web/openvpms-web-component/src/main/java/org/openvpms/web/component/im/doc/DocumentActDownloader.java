/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentException;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportFactory;
import org.openvpms.report.openoffice.OpenOfficeException;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.DocumentActReporter;
import org.openvpms.web.component.im.report.DocumentActTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.DocumentGenerationRequired;
import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.DocumentHasNoTemplate;
import static org.openvpms.archetype.rules.doc.DocumentException.ErrorCode.NotFound;


/**
 * Downloads a document from a {@link DocumentAct}.
 *
 * @author Tim Anderson
 */
public class DocumentActDownloader extends Downloader {

    /**
     * The document act.
     */
    private final DocumentAct act;

    /**
     * Determines if the document should be downloaded as a template.
     */
    private final boolean asTemplate;

    /**
     * Determines if the description should be displayed. If not, just the name is displayed.
     */
    private final boolean showDescription;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The file name formatter.
     */
    private final FileNameFormatter formatter;

    /**
     * The report factory.
     */
    private final ReportFactory reportFactory;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The template, when there is no document present.
     */
    private DocumentTemplate template;

    /**
     * PDF button style name.
     */
    private static final String PDF_STYLE_NAME = "download.pdf";

    /**
     * Constructs a {@link DocumentActDownloader}.
     *
     * @param act           the act
     * @param context       the context
     * @param formatter     the file name formatter
     * @param reportFactory the report factory
     */
    public DocumentActDownloader(DocumentAct act, Context context, FileNameFormatter formatter,
                                 ReportFactory reportFactory) {
        this(act, false, context, formatter, reportFactory);
    }

    /**
     * Constructs a {@link DocumentActDownloader}.
     *
     * @param act           the act
     * @param asTemplate    determines if the document should be downloaded as a template
     * @param context       the context
     * @param formatter     the file name formatter
     * @param reportFactory the report factory
     */
    public DocumentActDownloader(DocumentAct act, boolean asTemplate, Context context, FileNameFormatter formatter,
                                 ReportFactory reportFactory) {
        this(act, asTemplate, false, context, formatter, reportFactory);
    }

    /**
     * Constructs a {@link DocumentActDownloader}.
     *
     * @param act             the act
     * @param asTemplate      determines if the document should be downloaded as a template
     * @param showDescription determines if the description should be displayed. If not, just the name is displayed
     * @param context         the context
     * @param formatter       the file name formatter
     * @param reportFactory   the report factory
     */
    public DocumentActDownloader(DocumentAct act, boolean asTemplate, boolean showDescription, Context context,
                                 FileNameFormatter formatter, ReportFactory reportFactory) {
        this.act = act;
        this.context = context;
        this.asTemplate = asTemplate;
        this.showDescription = showDescription;
        this.formatter = formatter;
        this.reportFactory = reportFactory;
        lookups = ServiceHelper.getLookupService();
    }

    /**
     * Returns a component representing the downloader.
     *
     * @return the component
     */
    public Component getComponent() {
        Component component;
        Button button;
        boolean generated = false;
        String fileName = act.getFileName();
        String name = fileName;
        String mimeType = act.getMimeType();
        if (asTemplate) {
            DocumentTemplate template = getTemplate();
            if (template != null) {
                fileName = template.getFileName();
                String docName = template.getDocumentName();
                if (!StringUtils.isEmpty(docName)) {
                    name = docName;
                }
                mimeType = template.getMimeType();
            } else {
                mimeType = null;
            }
        } else if (act.getDocument() == null) {
            DocumentTemplate template = getTemplate();
            if (template != null) {
                fileName = template.getFileName();
                name = formatter.format(template.getName(), act, template);
                mimeType = template.getMimeType();
                if (reportFactory.hasMergeableContent(template)) {
                    generated = true;
                }
            }
        }

        String description = showDescription ? act.getDescription() : null;

        if (generated) {
            // if the document is generated, then its going to be a PDF, at least for the foreseeable future.
            // Fairly expensive to determine the mime type otherwise. TODO
            button = ButtonFactory.create(() -> selected(DocFormats.PDF_TYPE));
            button.setStyleName(PDF_STYLE_NAME);

            setButtonName(button, name, description);
        } else {
            button = ButtonFactory.create(() -> selected(null));
            if (name != null) {
                setButtonName(button, name, description);
                String style = (fileName != null) ? getStyleName(fileName) : getStyleName(name);
                button.setStyleName(style);
            } else {
                button.setStyleName(DEFAULT_BUTTON_STYLE);
            }
        }

        DocumentConverter converter = ServiceHelper.getBean(DocumentConverter.class);
        if (!generated && mimeType != null && converter.canConvert(name, mimeType, DocFormats.PDF_TYPE)) {
            Button asPDF = ButtonFactory.create(() -> selected(DocFormats.PDF_TYPE));
            asPDF.setStyleName(PDF_STYLE_NAME);
            asPDF.setProperty(Button.PROPERTY_TOOL_TIP_TEXT, Messages.get("file.download.asPDF.tooltip"));
            component = RowFactory.create(Styles.CELL_SPACING, button, asPDF);
        } else {
            component = RowFactory.create(Styles.CELL_SPACING, button);
        }
        return component;
    }

    /**
     * Returns the context.
     *
     * @return the context
     */
    protected Context getContext() {
        return context;
    }

    /**
     * Returns the document for download.
     *
     * @param mimeType the expected mime type. If {@code null}, then no conversion is required.
     * @return the document for download
     * @throws ArchetypeServiceException for any archetype service error
     * @throws DocumentException         if the document can't be found
     * @throws OpenOfficeException       if the document cannot be converted
     */
    protected Document getDocument(String mimeType) {
        Document document;
        if (!asTemplate) {
            Reference ref = act.getDocument();
            if (ref != null) {
                document = getDocumentByRef(ref, mimeType);
            } else {
                DocumentTemplate template = getTemplate();
                if (template != null) {
                    if (template.isLetter()) {
                        // the template must be used to generate the document before it can be downloaded.
                        throw new DocumentException(DocumentGenerationRequired, getDisplayName());
                    }
                    document = getDocument(template, mimeType);
                } else {
                    throw new DocumentException(DocumentHasNoTemplate, getDisplayName());
                }
            }
        } else {
            document = getTemplateDocument(mimeType);
        }
        if (document == null) {
            throw new DocumentException(NotFound, getDisplayName());
        }
        return document;
    }

    /**
     * Returns the current user.
     *
     * @return the user
     */
    @Override
    protected User getUser() {
        return context.getUser();
    }

    /**
     * Returns the template locator.
     *
     * @return the template locator
     */
    protected DocumentTemplateLocator getTemplateLocator() {
        return new DocumentActTemplateLocator(act, getService());
    }

    /**
     * Returns the act display name.
     *
     * @return the act display name
     */
    private String getDisplayName() {
        return DescriptorHelper.getDisplayName(act, getService());
    }

    /**
     * Generates a document from a template using a {@link DocumentActReporter}.
     *
     * @param template the template
     * @param mimeType the mime type. May be {@code null}
     * @return the document
     */
    private Document getDocument(DocumentTemplate template, String mimeType) {
        Document document;
        DocumentActReporter reporter = new DocumentActReporter(act, template, formatter, getService(), lookups,
                                                               reportFactory);
        reporter.setFields(ReportContextFactory.create(context));
        if (mimeType == null) {
            document = reporter.getDocument();
        } else {
            document = reporter.getDocument(mimeType, true);
        }
        return document;
    }

    /**
     * Returns a template document, converted to the specified mime type if conversion is supported.
     *
     * @param mimeType the desired mime type
     * @return the document, or {@code null} if there is no template document
     */
    private Document getTemplateDocument(String mimeType) {
        Document result = null;
        DocumentTemplate template = getTemplate();
        if (template != null) {
            Document document = template.getDocument();
            if (document == null) {
                throw new DocumentException(DocumentException.ErrorCode.TemplateHasNoDocument, template.getName());
            }
            if (mimeType != null && !mimeType.equals(document.getMimeType())) {
                result = convert(document, mimeType);
            } else {
                result = document;
            }
        }
        return result;
    }

    /**
     * Returns the document template, loading it if required.
     *
     * @return the document template. May be {@code null}
     */
    private DocumentTemplate getTemplate() {
        if (template == null) {
            template = getTemplateLocator().getTemplate();
        }
        return template;
    }
}