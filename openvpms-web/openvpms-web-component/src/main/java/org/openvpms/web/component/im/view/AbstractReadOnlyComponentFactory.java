/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.view;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.app.ContextSwitchListener;
import org.openvpms.web.component.im.doc.DocumentViewer;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategyFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.factory.LabelFactory;

import java.util.List;


/**
 * An {@link IMObjectComponentFactory} that returns read-only components.
 *
 * @author Tim Anderson
 */
public abstract class AbstractReadOnlyComponentFactory extends AbstractIMObjectComponentFactory {

    /**
     * The layout strategy factory.
     */
    private final IMObjectLayoutStrategyFactory strategies;


    /**
     * Constructs an {@link AbstractReadOnlyComponentFactory}.
     *
     * @param context    the layout context
     * @param strategies the layout strategy factory
     * @param style      the style name to use
     */
    public AbstractReadOnlyComponentFactory(LayoutContext context, IMObjectLayoutStrategyFactory strategies,
                                            String style) {
        super(context, style);
        this.strategies = strategies;
    }

    /**
     * Creates components for boolean, string, numeric and date properties.
     *
     * @param property the property
     * @return a new component, or {@code null} if the property isn't supported
     */
    @Override
    public Component create(Property property) {
        return create(property, (Hint) null);
    }

    /**
     * Creates a new component bound to the specified property.
     *
     * @param property the property to bind
     * @param hint     a rendering hint to determine the layout of the component. May be {@code null}
     * @return a new component, or {@code null} if the property type isn't supported
     */
    @Override
    public Component create(Property property, Hint hint) {
        Component component = super.create(property, hint);
        if (component != null) {
            component.setEnabled(false);
            component.setFocusTraversalParticipant(false);
        }
        return component;
    }

    /**
     * Create a component to display a property.
     *
     * @param property the property to display
     * @param context  the context object
     * @return a component to display {@code object}
     */
    public ComponentState create(Property property, IMObject context) {
        return create(property, context, null);
    }

    /**
     * Create a component to display a property.
     *
     * @param property the property to display
     * @param context  the context object
     * @param hint     a rendering hint to determine the layout of the component. May be {@code null}
     * @return a component to display {@code object}
     */
    @Override
    public ComponentState create(Property property, IMObject context, Hint hint) {
        Component component = null;
        if (!property.isLookup()) {
            component = create(property, hint); // isString() returns true for lookups
        }
        if (component == null) {
            // not a simple property
            if (property.isLookup()) {
                component = createLookup(property, context);
            } else if (property.isCollection()) {
                component = getCollectionViewer((CollectionProperty) property, context);
                // need to enable this otherwise table selection is disabled
            } else if (property.isObjectReference()) {
                component = getObjectViewer(property, context);
                // need to enable this for hyperlinks to work
            } else {
                Label label = LabelFactory.create();
                label.setText("No viewer for type " + property.getType());
                component = label;
            }
        }
        component.setFocusTraversalParticipant(false);
        return new ComponentState(component, property);
    }

    /**
     * Create a component to display an object.
     *
     * @param object  the object to display
     * @param context the object's parent. May be {@code null}
     * @return a component to display {@code object}
     */
    public ComponentState create(IMObject object, IMObject context) {
        IMObjectLayoutStrategy strategy = strategies.create(object, context);
        LayoutContext layout = getLayoutContext();
        layout.setRendered(object);
        if (layout.getComponentFactory() != this) {
            // make sure the viewer gets the appropriate component factory
            int depth = layout.getLayoutDepth();
            layout = new DefaultLayoutContext(layout);
            layout.setLayoutDepth(depth); // reset the depth
            layout.setComponentFactory(this);
        }
        IMObjectViewer viewer = new IMObjectViewer(object, context, strategy, layout);
        return new ComponentState(viewer.getComponent(), viewer.getFocusGroup());
    }

    /**
     * Returns a component to display a lookup property.
     *
     * @param property the lookup property
     * @param context  the context object
     * @return a component to display the property
     */
    protected abstract Component createLookup(Property property, IMObject context);

    /**
     * Returns a viewer for an object reference.
     *
     * @param property the object reference property
     * @param context  the context object
     * @return an component to display the object reference.
     */
    protected Component getObjectViewer(Property property, IMObject context) {
        IMObjectReference ref = (IMObjectReference) property.getValue();
        boolean link = false;
        ContextSwitchListener listener = null;
        LayoutContext layout = getLayoutContext();
        if (!layout.isEdit()) {
            // enable hyperlinks if no edit is in progress.
            listener = layout.getContextSwitchListener();
            link = true;
        }
        String[] range = property.getArchetypeRange();
        if (TypeHelper.matches(range, "document.*")) {
            return createDocumentViewer(context, ref, link, layout);
        }

        return getObjectViewer(ref, listener, layout);
    }

    /**
     * Creates a component to view a document.
     *
     * @param reference the reference to view. May be {@code null}
     * @param listener  the listener to notify. May be {@code null}
     * @param layout    the layout context
     * @return a component to view the document
     */
    protected Component getObjectViewer(Reference reference, ContextSwitchListener listener, LayoutContext layout) {
        return new IMObjectReferenceViewer(reference, listener, layout.getContext()).getComponent();
    }

    /**
     * Creates a component to view a document.
     *
     * @param reference the reference to view
     * @param context   the parent. May be {@code null}
     * @param link      if {@code true} enable an hyperlink to the object
     * @param layout    the layout context
     * @return a component to view the document
     */
    protected Component createDocumentViewer(IMObject context, Reference reference, boolean link, LayoutContext layout) {
        return new DocumentViewer(reference, context, link, false, layout).getComponent();
    }

    /**
     * Returns a component to display a collection.
     *
     * @param property the collection
     * @param parent   the parent object
     * @return a collection to display the node
     */
    protected Component getCollectionViewer(CollectionProperty property, IMObject parent) {
        Component result = null;
        if (property.getMaxCardinality() == 1) {
            List<?> values = property.getValues();
            if (values.isEmpty()) {
                result = getEmptyCollectionViewer();
            }
        } else if (property.getMinCardinality() == 0 && property.getMaxCardinality() == 0) {
            // nothing to display, so return an empty label
            result = LabelFactory.create();
        }
        if (result == null) {
            IMObjectCollectionViewer viewer = IMObjectCollectionViewerFactory.create(property, parent,
                                                                                     getLayoutContext());
            result = viewer.getComponent();
        }
        return result;
    }

    /**
     * Returns a component for an empty collection.
     *
     * @return the component
     */
    protected Component getEmptyCollectionViewer() {
        return SingleIMObjectCollectionViewer.createEmptyCollectionViewer();
    }

}
