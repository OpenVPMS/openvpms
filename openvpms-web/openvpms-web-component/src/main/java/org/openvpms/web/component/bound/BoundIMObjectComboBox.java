/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.bound;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.list.ObjectListModel;
import org.openvpms.web.component.property.Property;

import java.util.List;

/**
 * Binds an {@link IMObject} {@link Property} to a combo box that displays their names.
 *
 * @author Tim Anderson
 */
public class BoundIMObjectComboBox<T extends IMObject> extends AbstractBoundIMObjectComboBox<T> {

    /**
     * Constructs a {@link BoundIMObjectComboBox}.
     *
     * @param property the property
     * @param objects  the objects
     */
    public BoundIMObjectComboBox(Property property, List<? extends T> objects) {
        super(property, objects);
    }

    /**
     * Constructs a {@link BoundIMObjectComboBox}.
     *
     * @param property the property
     * @param model    the model
     */
    public BoundIMObjectComboBox(Property property, ObjectListModel<T> model) {
        super(property, model);
    }

    /**
     * Returns the text for a property value.
     *
     * @param value the value. May be {@code null}
     * @return the corresponding text
     */
    @Override
    protected String getText(Object value) {
        String result = null;
        if (value instanceof IMObject) {
            result = ((IMObject) value).getName();
        }
        return result;
    }
}
