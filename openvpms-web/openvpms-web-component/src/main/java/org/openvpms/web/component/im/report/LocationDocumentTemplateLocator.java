/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.system.ServiceHelper;


/**
 * An {@link DocumentTemplateLocator} that resolves document templates associated with an organisation location
 * or practice.
 * <p/>
 * The algorithm for template selection is:
 * <ol>
 * <li>if a location is specified, and it has a template for the given archetype, use it; otherwise
 * <li>if a practice is specified, and it has a template for the given archetype, use it; otherwise
 * <li>use the first available template for the archetype, if any
 * </ol>
 *
 * @author Tim Anderson
 */
public class LocationDocumentTemplateLocator extends TypeBasedDocumentTemplateLocator {

    /**
     * An <em>party.organisationLocation</em>. May be {@code null}
     */
    private final Party location;

    /**
     * An <em>party.organisationPractice</em>. May be {@code null}
     */
    private final Party practice;


    /**
     * Constructs a {@link LocationDocumentTemplateLocator}.
     *
     * @param type     the type that the document template applies to. A <em>lookup.documentTemplateType</em> code
     * @param location the practice location. May be {@code null}
     * @param practice the practice. May be {@code null}
     */
    public LocationDocumentTemplateLocator(String type, Party location, Party practice) {
        super(type, ServiceHelper.getArchetypeService());
        this.location = location;
        this.practice = practice;
    }

    /**
     * Returns the document template.
     * <p/>
     * This implementation looks first at the practice location for a match. If there is no practice location,
     * or no match, it then looks at the practice. If there is no practice or no match that it returns the first
     * template for the archetype short name.
     *
     * @return the document template, or {@code null} if the template cannot be located
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public DocumentTemplate getTemplate() {
        DocumentTemplate result = null;
        if (location != null) {
            result = getTemplate(location);
        }
        if (result == null && practice != null) {
            result = getTemplate(practice);
        }
        if (result == null) {
            result = super.getTemplate();
        }
        return result;
    }
}