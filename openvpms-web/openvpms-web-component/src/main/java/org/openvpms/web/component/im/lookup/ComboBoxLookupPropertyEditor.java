/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.bound.ComboBoxPropertyEditor;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.style.Styles;

/**
 * An editor for lookup properties that uses a {@link BoundLookupComboBox} to display the lookups.
 *
 * @author Tim Anderson
 */
public class ComboBoxLookupPropertyEditor extends ComboBoxPropertyEditor<Lookup> implements LookupPropertyEditor {

    /**
     * Constructs a {@link ComboBoxLookupPropertyEditor}.
     *
     * @param property the property being edited
     * @param parent   the parent object
     */
    public ComboBoxLookupPropertyEditor(Property property, IMObject parent) {
        this(create(property, parent));
    }

    /**
     * Constructs a {@link ComboBoxLookupPropertyEditor}.
     *
     * @param component the edit component
     */
    public ComboBoxLookupPropertyEditor(BoundLookupComboBox component) {
        super(component);
    }

    /**
     * Refreshes the set of available lookups.
     */
    @Override
    public void refresh() {
        ((BoundLookupComboBox) getComponent()).refresh();
    }

    /**
     * Creates a {@link BoundLookupComboBox} for a property.
     *
     * @param property the property
     * @param parent   the parent object
     * @return a new {@link BoundLookupComboBox}
     */
    private static BoundLookupComboBox create(Property property, IMObject parent) {
        BoundLookupComboBox result = new BoundLookupComboBox(property, parent);
        result.setStyleName(Styles.EDIT);
        return result;
    }

}
