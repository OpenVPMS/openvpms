/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import org.junit.Test;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.message.OutboundMessage.Status;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link SMSDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class SMSDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * Verifies that only PENDING SMS can be deleted.
     */
    @Test
    public void testDelete() {
        Act message1 = createMessage(Status.PENDING);
        checkDelete(message1, createHandler(message1));

        Act message2 = createMessage(Status.SENT);
        checkCannotDelete(message2, createHandler(message2));

        Act message3 = createMessage(Status.EXPIRED);
        checkCannotDelete(message3, createHandler(message3));

        Act message4 = createMessage(Status.DELIVERED);
        checkCannotDelete(message4, createHandler(message4));

        Act message5 = createMessage(Status.ERROR);
        checkCannotDelete(message5, createHandler(message5));

        Act message6 = createMessage(Status.REVIEWED);
        checkCannotDelete(message6, createHandler(message6));
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link SMSDeletionHandler} for
     * <em>act.smsMessage</em>.
     */
    @Test
    public void testFactory() {
        Act message = createMessage(Status.PENDING);
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);
        IMObjectDeletionHandler<Act> handler = factory.create(message);
        assertTrue(handler instanceof SMSDeletionHandler);
        checkDelete(message, handler);
    }

    /**
     * Creates a deletion handler
     *
     * @param message the message
     * @return the handler
     */
    private SMSDeletionHandler createHandler(Act message) {
        return new SMSDeletionHandler(message, factory, transactionManager, ServiceHelper.getArchetypeService());
    }

    /**
     * Verifies a message can be deleted.
     *
     * @param message the message
     * @param handler the deletion handler
     */
    private void checkDelete(Act message, IMObjectDeletionHandler<Act> handler) {
        assertTrue(handler.getDeletable().canDelete());
        assertFalse(handler.canDeactivate());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(message));
    }

    /**
     * Verifies a message can't be deleted.
     *
     * @param message the message
     * @param handler the deletion handler
     */
    private void checkCannotDelete(Act message, IMObjectDeletionHandler<Act> handler) {
        assertFalse(handler.getDeletable().canDelete());
        assertFalse(handler.canDeactivate());
        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            // no-op
        }
        assertNotNull(get(message)); // verify the message wasn't deleted
    }

    /**
     * Creates an <em>act.smsMessage</em>.
     *
     * @param status the status
     * @return a new message
     */
    private Act createMessage(Status status) {
        Act message = create(SMSArchetypes.MESSAGE, Act.class);
        message.setStatus(status.toString());
        IMObjectBean bean = getBean(message);
        bean.setValue("message", "test");
        bean.save();
        return message;
    }
}
