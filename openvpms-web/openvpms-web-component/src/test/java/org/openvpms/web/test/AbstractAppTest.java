/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.test;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Window;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.prefs.PreferenceService;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.job.JobManager;
import org.openvpms.web.component.prefs.UserPreferences;
import org.openvpms.web.echo.error.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertNotNull;


/**
 * Abstract base class for tests requiring Spring and Echo2 to be set up.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAppTest extends ArchetypeServiceTest {

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The location rules.
     */
    @Autowired
    private LocationRules locationRules;

    /**
     * The user rules.
     */
    @Autowired
    private UserRules userRules;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * The application.
     */
    private ContextApplicationInstance app;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        PreferenceService preferences = getPreferenceService();
        practiceService = new PracticeService(getArchetypeService(), practiceRules, null);
        UserPreferences userPreferences = new UserPreferences(preferences, practiceService);
        app = new ContextApplicationInstance(new GlobalContext(), practiceRules, locationRules,
                                             userRules, userPreferences) {
            /**
             * Switches the current workspace to display an object.
             *
             * @param object the object to view
             */
            @Override
            public void switchTo(IMObject object) {
            }

            /**
             * Switches the current workspace to one that supports a particular archetype.
             *
             * @param shortName the archetype short name
             */
            @Override
            public void switchTo(String shortName) {
            }

            @Override
            public Window init() {
                return new Window();
            }

            @Override
            public void lock() {
            }

            @Override
            public void unlock() {
            }
        };
        app.setApplicationContext(applicationContext);
        ApplicationInstance.setActive(app);
        app.doInit();
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        practiceService.destroy();
        app.dispose();
        ApplicationInstance.setActive(null);
    }

    /**
     * Finds a component based on its type.
     *
     * @param type the component type
     * @return the corresponding component
     * @throws AssertionError if the component is not found
     */
    protected <T extends Component> T findComponent(Class<T> type) {
        T component = EchoTestHelper.findComponent(ApplicationInstance.getActive().getDefaultWindow(), type);
        assertNotNull(component);
        return component;
    }

    /**
     * Returns the practice service.
     *
     * @return the practice service
     */
    protected PracticeService getPracticeService() {
        return practiceService;
    }

    /**
     * Returns the preference service.
     *
     * @return the preference service
     */
    protected PreferenceService getPreferenceService() {
        return Mockito.mock(PreferenceService.class);
    }

    /**
     * Initialises the error handler, so that errors are collected in the supplied array.
     *
     * @param errors the list to correct errors in
     */
    protected void initErrorHandler(List<String> errors) {
        // register an ErrorHandler to collect errors
        ErrorHandler.setInstance(new ErrorHandler() {
            @Override
            public void error(Throwable cause) {
                errors.add(cause.getMessage());
            }

            public void error(String title, String message, boolean html, Throwable cause, Runnable listener) {
                errors.add(message);
                if (listener != null) {
                    listener.run();
                }
            }
        });
    }

    /**
     * Processes any queued tasks.
     */
    protected void processQueuedTasks() {
        while (app.hasQueuedTasks()) {
            app.processQueuedTasks();
        }
    }

    /**
     * Processes any queued tasks.
     * <p/>
     * This can be used to wait for jobs performed by {@link JobManager}.
     *
     * @param times     the max no. of times to process unless terminated by condition
     * @param condition the condition to evaluate to determine if processing has completed
     */
    protected void processQueuedTasks(int times, Supplier<Boolean> condition) {
        if (!condition.get()) {
            processQueuedTasks();
        }
        for (int i = 0; i < times - 1 && !condition.get(); ++i) {
            sleep(1000);
            processQueuedTasks();
        }
    }

    /**
     * Sleeps for the specified time, silently ignoring interrupts.
     *
     * @param time the time, in milliseconds
     */
    protected void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ignore) {
            // do nothing
        }
    }

}
