/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.BaseDocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.archetype.test.builder.doc.TestEmailTemplateBuilder;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.Deletable;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link DocumentTemplateDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DocumentTemplateDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory editorFactory;

    /**
     * Verifies that an <em>entity.documentTemplate</em> with associated <em>act.documentTemplate</em> can be
     * deleted.
     */
    @Test
    public void testDeleteForDocumentTemplate() {
        Entity template = createDocumentTemplate();
        checkDelete(template);
    }

    /**
     * Verifies that an <em>entity.documentTemplateEmailSystem</em> with associated <em>act.documentTemplate</em> can be
     * deleted.
     */
    @Test
    public void testDeleteSystemEmailTemplate() {
        Document attachmentContent = documentFactory.createPDF("Customer Handout.pdf");
        Entity attachment = documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document(attachmentContent)
                .build();
        Entity template = createDocumentSystemEmailTemplate(attachment);
        checkDeleteEmailTemplate(template, attachment, attachmentContent);
    }

    /**
     * Verifies that an <em>entity.documentTemplateEmailUser</em> with associated <em>act.documentTemplate</em> can be
     * deleted.
     */
    @Test
    public void testDeleteUserEmailTemplate() {
        Document attachmentContent = documentFactory.createPDF("Patient Handout.pdf");
        Entity attachment = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(attachmentContent)
                .build();
        Entity template = createDocumentUserEmailTemplate(attachment);
        checkDeleteEmailTemplate(template, attachment, attachmentContent);
    }

    /**
     * Verifies that an <em>entity.documentTemplate</em> can be deactivated.
     */
    @Test
    public void testDeactivate() {
        Entity entity = createDocumentTemplate();
        DocumentTemplate template = new DocumentTemplate(entity, getArchetypeService());
        template.getDocument();
        template.getDocumentAct();
        checkDeactivate(entity);
    }

    /**
     * Verifies that an <em>entity.documentTemplateEmailSystem</em> can be deactivated.
     */
    @Test
    public void testDeactivateSystemEmailTemplate() {
        Entity entity = createDocumentSystemEmailTemplate(null);
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());
        checkDeactivate(entity);
    }

    /**
     * Verifies that an <em>entity.documentTemplateEmailUser</em> can be deactivated.
     */
    @Test
    public void testDeactivateUserEmailTemplate() {
        Entity template = createDocumentUserEmailTemplate(null);
        checkDeactivate(template);
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link DocumentTemplateDeletionHandler} for
     * <em>entity.documentTemplate</em>.
     */
    @Test
    public void testFactoryForDocumentTemplate() {
        Entity template = createDocumentTemplate();
        checkFactory(new DocumentTemplate(template, getArchetypeService()));
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link DocumentTemplateDeletionHandler} for
     * <em>entity.documentTemplateEmailSystem</em>.
     */
    @Test
    public void testFactoryForSystemEmailTemplate() {
        Entity template = createDocumentSystemEmailTemplate(null);
        checkFactory(new EmailTemplate(template, getArchetypeService()));
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link DocumentTemplateDeletionHandler} for
     * <em>entity.documentTemplateEmailUser</em>.
     */
    @Test
    public void testFactoryForUserEmailTemplate() {
        Entity template = createDocumentUserEmailTemplate(null);
        checkFactory(new EmailTemplate(template, getArchetypeService()));
    }

    /**
     * Verifies that <em>entity.documentTemplate</em> can't be deleted if it has participations
     * other than <em>participation.document</em>.
     */
    @Test
    public void testDeleteWithParticipations() {
        Entity template = createDocumentTemplate();
        Document document = createDocument();
        DocumentAct act = createAct(template, document);

        Act form = create(PatientArchetypes.DOCUMENT_FORM, Act.class);
        Party patient = TestHelper.createPatient();
        IMObjectBean bean = getBean(form);
        bean.setTarget("patient", patient);
        bean.setTarget("documentTemplate", template);
        save(form);

        IMObjectDeletionHandler<Entity> handler = createDeletionHandler(template);
        Deletable deletable = handler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Document Template has relationships and cannot be deleted.", deletable.getReason());

        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException to be thrown");
        } catch (IllegalStateException expected) {
            // do nothing
        }

        // verify nothing was deleted
        assertNotNull(get(template));
        assertNotNull(get(document));
        assertNotNull(get(act));
        assertNotNull(get(form));

        // verify it can be deactivated
        handler.deactivate();
        assertFalse(get(template).isActive());

        // deactivation doesn't propagate
        assertTrue(get(document).isActive());
        assertTrue(get(act).isActive());
    }

    /**
     * Verifies that a template can be deleted if it has links to email and sms templates.
     */
    @Test
    public void testDeleteWithLinks() {
        TestDocumentTemplateBuilder builder = documentFactory.newTemplate();
        Entity template = builder.type(PatientArchetypes.DOCUMENT_FORM)
                .blankDocument()
                .emailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("foo")
                .add()
                .smsTemplate().content("text").add()
                .build();

        Document document = (Document) builder.getDocument();
        assertNotNull(document);
        assertFalse(document.isNew());
        DocumentAct act = builder.getTemplateAct();
        assertNotNull(act);
        assertFalse(act.isNew());

        Entity email = builder.getEmailTemplate();
        assertNotNull(email);
        assertFalse(email.isNew());

        Entity sms = builder.getSMSTemplate();
        assertNotNull(sms);
        assertFalse(sms.isNew());

        IMObjectDeletionHandler<Entity> handler = createDeletionHandler(template);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));

        // verify the template were deleted.
        assertNull(get(template));
        assertNull(get(document));
        assertNull(get(act));

        // ... but the targets weren't
        assertNotNull(get(email));
        assertNotNull(get(sms));
    }

    /**
     * Verifies an <em>entity.documentTemplateEmailSystem</em> cannot be deleted if it is linked to an
     * <em>entity.documentTemplate</em>.
     */
    @Test
    public void testDeleteSystemEmailTemplateWithLinks() {
        Entity email = createTextSystemEmailTemplate();
        checkDeleteEmailTemplateWithLinks(email);
    }

    /**
     * Verifies an <em>entity.documentTemplateEmailUser</em> cannot be deleted if it is linked to an
     * <em>entity.documentTemplate</em>.
     */
    @Test
    public void testDeleteUserEmailTemplateWithLinks() {
        Entity email = createTextSystemEmailTemplate();
        checkDeleteEmailTemplateWithLinks(email);
    }

    /**
     * Creates a new deletion handler for a template.
     *
     * @param template the template
     * @return a new deletion handler
     */
    protected DocumentTemplateDeletionHandler createDeletionHandler(Entity template) {
        return new DocumentTemplateDeletionHandler(template, editorFactory, ServiceHelper.getTransactionManager(),
                                                   ServiceHelper.getArchetypeService());
    }

    /**
     * Verifies that when an email template is deleted, associated attachments are retained.
     *
     * @param template          the template
     * @param attachment        the attachment
     * @param attachmentContent the attachment content
     */
    private void checkDeleteEmailTemplate(Entity template, Entity attachment, Document attachmentContent) {
        checkDelete(template);
        assertNotNull(get(attachment));
        assertNotNull(get(attachmentContent));
    }

    /**
     * Verifies an email template cannot be deleted if it is linked to an <em>entity.documentTemplate</em>>.
     *
     * @param email the email template
     */
    private void checkDeleteEmailTemplateWithLinks(Entity email) {
        Entity template = createDocumentTemplate();
        IMObjectBean bean = getBean(template);
        bean.setTarget("email", email);
        bean.save();

        IMObjectDeletionHandler<Entity> handler = createDeletionHandler(email);
        Deletable deletable = handler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals(DescriptorHelper.getDisplayName(email, getArchetypeService())
                     + " has relationships and cannot be deleted.", deletable.getReason());

        try {
            handler.delete(new LocalContext(), new HelpContext("foo", null));
            fail("Expected IllegalStateException to be thrown");
        } catch (IllegalStateException expected) {
            // do nothing
        }
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link DocumentTemplateDeletionHandler} for
     * the specified template.
     *
     * @param template the template
     */
    private void checkFactory(BaseDocumentTemplate template) {
        Document document = template.getDocument();
        assertNotNull(document);
        DocumentAct act = template.getDocumentAct();
        assertNotNull(act);
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Entity entity = template.getEntity();
        IMObjectDeletionHandler<Entity> handler = factory.create(entity);
        assertTrue(handler instanceof DocumentTemplateDeletionHandler);
        handler.delete(new LocalContext(), new HelpContext("foo", null));

        assertNull(get(entity));
        assertNull(get(document));
        assertNull(get(act));
    }

    /**
     * Verifies a template associated with a document can be deactivated.
     *
     * @param template the template
     */
    private void checkDeactivate(Entity template) {
        Document document = documentFactory.createJRXML();
        DocumentAct act = createAct(template, document);
        assertTrue(template.isActive());
        IMObjectDeletionHandler<Entity> handler = createDeletionHandler(template);
        handler.deactivate();

        assertFalse(get(template).isActive());

        // deactivation doesn't propagate
        assertTrue(get(document).isActive());
        assertTrue(get(act).isActive());
    }

    /**
     * Verifies that a document template with an associated act can be deleted.
     *
     * @param template the template
     */
    private void checkDelete(Entity template) {
        BaseDocumentTemplate documentTemplate = template.isA(DocumentArchetypes.DOCUMENT_TEMPLATE)
                                                    ? new DocumentTemplate(template, getArchetypeService())
                                                    : new EmailTemplate(template, getArchetypeService());
        DocumentAct act = documentTemplate.getDocumentAct();;
        Document document = documentTemplate.getDocument();
        assertNotNull(act);
        assertNotNull(document);
        IMObjectDeletionHandler<Entity> handler = createDeletionHandler(template);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));

        assertNull(get(template));
        assertNull(get(document));
        assertNull(get(act));
    }

    /**
     * Creates an <em>act.documentTemplate</em>, associating it with the supplied document and
     * <em>entity.documentTemplate*</em>.
     *
     * @param template the <em>entity.documentTemplate</em>
     * @param document the document
     * @return the act
     */
    private DocumentAct createAct(Entity template, Document document) {
        return documentFactory.attachDocument(template, document);
    }

    /**
     * Creates an <em>entity.documentTemplate</em>
     *
     * @return a new template
     */
    private Entity createDocumentTemplate() {
        return documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(documentFactory.createJRXML())
                .build();
    }

    /**
     * Creates a document.
     *
     * @return a new document
     */
    private Document createDocument() {
        return documentFactory.createJRXML();
    }

    /**
     * Creates a system email template with document content and an optional attachment.
     *
     * @param attachment the attachment. May be {@code null}
     * @return the template
     */
    private Entity createDocumentSystemEmailTemplate(Entity attachment) {
        return createDocumentEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE, attachment);
    }

    /**
     * Creates a user email template with document content and an optional attachment.
     *
     * @param attachment the attachment. May be {@code null}
     * @return the template
     */
    private Entity createDocumentUserEmailTemplate(Entity attachment) {
        return createDocumentEmailTemplate(DocumentArchetypes.USER_EMAIL_TEMPLATE, attachment);
    }

    /**
     * Creates an email template with document content and an optional attachment.
     *
     * @param archetype  the email template archetype
     * @param attachment the attachment. May be {@code null}
     * @return the template
     */
    private Entity createDocumentEmailTemplate(String archetype, Entity attachment) {
        TestEmailTemplateBuilder builder = documentFactory.newEmailTemplate(archetype)
                .subject("foo")
                .contentType(EmailTemplate.ContentType.DOCUMENT)
                .document(documentFactory.createJRXML());
        if (attachment != null) {
            builder.addAttachments(attachment);
        }
        return builder.build();
    }

    /**
     * Creates a system email template with text content.
     *
     * @return the template
     */
    private Entity createTextSystemEmailTemplate() {
        return documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("foo")
                .contentType(EmailTemplate.ContentType.TEXT)
                .content("bar")
                .build();
    }

}
