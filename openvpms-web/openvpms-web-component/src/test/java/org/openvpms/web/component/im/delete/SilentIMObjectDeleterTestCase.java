/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.MessageDialog;

import static org.junit.Assert.assertNull;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;


/**
 * Tests the {@link SilentIMObjectDeleter} class.
 *
 * @author Tim Anderson
 */
public class SilentIMObjectDeleterTestCase extends AbstractIMObjectDeleterTest {

    /**
     * Creates a new {@link IMObjectDeleter}.
     *
     * @param factory the deletion handler factory
     * @return a new deleter
     */
    @Override
    protected IMObjectDeleter<IMObject> createDeleter(IMObjectDeletionHandlerFactory factory) {
        return new SilentIMObjectDeleter<>(factory, getArchetypeService());
    }

    /**
     * Verifies any confirmation of deletion dialog matches that expected.
     * <p/>
     * For {@link SilentIMObjectDeleter}, there should be no confirmation.
     *
     * @param object the object being deleted
     */
    protected void checkDeletionConfirmation(IMObject object) {
        ConfirmationDialog windowPane = findWindowPane(ConfirmationDialog.class);
        assertNull(windowPane);
    }

    /**
     * Verifies any deactivation confirmation dialog matches that expected.
     * <p/>
     * For {@link SilentIMObjectDeleter}, there should be no dialog.
     *
     * @param object the object being deactivated
     */
    @Override
    protected void checkDeactivationConfirmation(IMObject object) {
        MessageDialog windowPane = findWindowPane(MessageDialog.class);
        assertNull(windowPane);
    }

    /**
     * Verifies any deactivated error dialog matches that expected.
     * <p/>
     * For {@link SilentIMObjectDeleter}, there should be no dialog.
     *
     * @param object the object being deleted
     */
    @Override
    protected void checkDeactivatedError(IMObject object) {
        MessageDialog windowPane = findWindowPane(MessageDialog.class);
        assertNull(windowPane);
    }
}
