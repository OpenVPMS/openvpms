/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentActTemplateLocator}.
 *
 * @author Tim Anderson
 */
public class DocumentActTemplateLocatorTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests the {@link DocumentActTemplateLocator#getTemplate()} method.
     */
    @Test
    public void testGetTemplate() {
        Entity entity = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .blankDocument()
                .build();
        DocumentAct act = patientFactory.newForm()
                .template(entity)
                .build(false);

        DocumentActTemplateLocator locator = new DocumentActTemplateLocator(act, getArchetypeService());
        DocumentTemplate template = locator.getTemplate();
        assertNotNull(template);
        assertEquals(entity, template.getEntity());
    }

    /**
     * Verifies that {@link DocumentActTemplateLocator} is returned by {@link DocumentTemplateLocatorFactory}
     * for acts with templates.
     */
    @Test
    public void testFactory() {
        checkFactory(CustomerArchetypes.DOCUMENT_FORM);
        checkFactory(CustomerArchetypes.DOCUMENT_LETTER);
        checkFactory(PatientArchetypes.DOCUMENT_FORM);
        checkFactory(PatientArchetypes.DOCUMENT_LETTER);
        checkFactory(SupplierArchetypes.DOCUMENT_FORM);
        checkFactory(SupplierArchetypes.DOCUMENT_LETTER);
    }

    /**
     * Verifies that {@link DocumentActTemplateLocator} is returned by {@link DocumentTemplateLocatorFactory}
     * for the supplied archetype.
     *
     * @param archetype the archetype
     */
    private void checkFactory(String archetype) {
        IMObject object = create(archetype);
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        DocumentTemplateLocator locator = factory.getDocumentTemplateLocator(object, new LocalContext());
        assertTrue(locator instanceof DocumentActTemplateLocator);
    }
}