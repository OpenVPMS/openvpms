/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentTemplateLocatorFactory} class.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateLocatorFactoryTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Verifies {@link ContextDocumentTemplateLocator} is used by default.
     */
    @Test
    public void testContextTemplateLocator() {
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        TemplateHelper helper = new TemplateHelper(getArchetypeService());
        Entity entity = helper.getTemplateForType(CustomerAccountArchetypes.INVOICE);
        if (entity == null) {
            entity = documentFactory.newTemplate()
                    .type(CustomerAccountArchetypes.INVOICE)
                    .blankDocument()
                    .build();
        }

        IMObject invoice = create(CustomerAccountArchetypes.INVOICE);

        DocumentTemplateLocator locator = factory.getDocumentTemplateLocator(invoice, new LocalContext());
        assertTrue(locator instanceof ContextDocumentTemplateLocator);
        DocumentTemplate template = factory.getTemplate(invoice, new LocalContext());
        assertNotNull(template);
        assertEquals(entity, template.getEntity());
    }

    /**
     * Verifies {@link DocumentActTemplateLocator} is used for patient forms.
     */
    @Test
    public void testDocumentActTemplateLocator() {
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        Entity entity = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        IMObject object = patientFactory.newForm()
                .template(entity)
                .build(false);
        DocumentTemplateLocator locator = factory.getDocumentTemplateLocator(object, new LocalContext());
        assertTrue(locator instanceof DocumentActTemplateLocator);

        DocumentTemplate template = factory.getTemplate(object, new LocalContext());
        assertNotNull(template);
        assertEquals(entity, template.getEntity());
    }

    /**
     * Verifies that {@code null} if there is no template for a particular type.
     */
    @Test
    public void testNoTemplate() {
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());

        IMObject object = create(PatientArchetypes.SPECIES);
        // select an archetype where the is not likely to be a template

        assertNull(factory.getTemplate(object, new LocalContext()));
    }
}
