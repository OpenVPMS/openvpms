/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import org.junit.Test;
import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.action.ActionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link SMSReplyUpdater}.
 *
 * @author Tim Anderson
 */
public class SMSReplyUpdaterTestCase extends ArchetypeServiceTest {

    /**
     * The action factory.
     */
    @Autowired
    private ActionFactory actionFactory;

    /**
     * The SMS factory.
     */
    @Autowired
    private TestSMSFactory smsFactory;

    /**
     * Tests the {@link SMSReplyUpdater#markRead(Act)} method.
     */
    @Test
    public void testMarkRead() {
        checkMarkRead(MessageStatus.PENDING, MessageStatus.READ);
        checkMarkRead(MessageStatus.READ, MessageStatus.READ);
        checkMarkRead(MessageStatus.COMPLETED, MessageStatus.COMPLETED);
    }

    /**
     * Verifies the status of a reply after running {@link SMSReplyUpdater#markRead(Act)}.
     *
     * @param initial  the initial status
     * @param expected the expected status
     */
    private void checkMarkRead(String initial, String expected) {
        Act reply = smsFactory.newReply()
                .message("foo")
                .status(initial)
                .build();
        SMSReplyUpdater updater = new SMSReplyUpdater(actionFactory, getArchetypeService());
        updater.markRead(reply);
        Act updated = get(reply);
        assertEquals(expected, updated.getStatus());
    }
}