/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.i18n;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.web.component.i18n.WebComponentMessages;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link WebComponentMessages} class.
 *
 * @author Tim Anderson
 */
public class WebComponentMessagesTestCase {

    /**
     * Verify there are tests for each message.
     */
    @Test
    public void testCoverage() {
        for (Method method : WebComponentMessages.class.getDeclaredMethods()) {
            if (Modifier.isPublic(method.getModifiers()) && Modifier.isStatic(method.getModifiers())) {
                String test = "test" + StringUtils.capitalize(method.getName());
                try {
                    WebComponentMessagesTestCase.class.getDeclaredMethod(test);
                } catch (NoSuchMethodException exception) {
                    fail("Test needs to be expanded to include " + method.getName());
                }
            }
        }
    }

    /**
     * Tests the {@link WebComponentMessages#activeSingletonExists(String)} method.
     */
    @Test
    public void testActiveSingletonExists() {
        assertEquals("Cannot save foo. An active instance already exists.",
                     WebComponentMessages.activeSingletonExists("foo").getMessage());
    }
}
