/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Editor test helper methods.
 *
 * @author Tim Anderson
 */
public class EditorTestHelper {

    /**
     * Asserts that a {@link Modifiable} is valid.
     *
     * @param modifiable the modifiable
     */
    public static void assertValid(Modifiable modifiable) {
        DefaultValidator validator = new DefaultValidator();
        if (!modifiable.validate(validator)) {
            ValidatorError firstError = validator.getFirstError();
            if (firstError == null) {
                fail("Expected " + modifiable.getClass().getName()
                     + " to be valid but validation failed. No validation error raised");
            } else {
                fail("Expected " + modifiable.getClass().getName() + " to be valid but validation failed with:  "
                     + firstError);
            }
        }
    }

    /**
     * Verifies a {@link Modifiable} is invalid.
     *
     * @param modifiable the modifiable
     * @param expected the expected error message
     */
    public static void assertInvalid(Modifiable modifiable, String expected) {
        Validator validator = new DefaultValidator();
        assertFalse(modifiable.validate(validator));
        assertNotNull(validator.getFirstError());
        assertEquals(expected, validator.getFirstError().getMessage());
    }
}
