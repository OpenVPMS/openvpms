/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.action;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.action.ActionBuilder.BeanConsumer;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.action.ActionBuilder.asBean;
import static org.openvpms.web.component.action.ActionBuilder.asObject;
import static org.openvpms.web.component.action.ActionBuilder.update;
import static org.openvpms.web.component.action.ActionRunner.RETRY_ID;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;

/**
 * Tests the {@link ActionBuilder} class.
 *
 * @author Tim Anderson
 */
public class ActionBuilderTestCase extends AbstractAppTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The practice newBuilder().
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
    }

    /**
     * Tests the {@link ActionBuilder#action(Runnable)} method.
     */
    @Test
    public void testRunnableAction() {
        MutableInt calls = new MutableInt();
        Runnable action = calls::increment;
        ActionBuilder builder = newBuilder().action(action);
        verifyActionSucceeds(builder);

        // verify the action was only invoked once
        assertEquals(1, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#action(IMObject, Consumer)} method.
     */
    @Test
    public void testObjectConsumerAction() {
        Party original = createStaleObject();
        MutableInt calls = new MutableInt();

        // the action will be invoked twice. The first time it will fail as the object is out of date.
        // The second time, it will be supplied the latest instance.
        ActionBuilder builder = newBuilder().action(original, asObject(party -> {
            calls.increment();
            party.setName("New Name");
            save(party);
        }));
        verifyActionSucceeds(builder);

        assertEquals("New Name", get(original).getName());
        assertEquals(2, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#action(String, Supplier, Consumer)} method.
     */
    @Test
    public void testSupplierConsumerAction() {
        Party original = createStaleObject();
        MutableInt calls = new MutableInt();

        Supplier<Party> supplier = () -> original;
        ActionBuilder builder = newBuilder().action(original.getArchetype(), supplier, asObject(party -> {
            calls.increment();
            party.setName("New Name");
            save(party);
        }));
        verifyActionSucceeds(builder);
        assertEquals("New Name", get(original).getName());
        assertEquals(2, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#action(ObjectSupplier, Consumer)} method.
     */
    @Test
    public void testObjectSupplierConsumerAction() {
        Party original = createStaleObject();
        MutableInt calls = new MutableInt();

        ObjectSupplier<Party> supplier = new TestSupplier<>(original);
        ActionBuilder builder = newBuilder().action(supplier, asObject(object -> {
            calls.increment();
            object.setName("New Name");
            save(object);
        }));
        verifyActionSucceeds(builder);
        assertEquals("New Name", get(original).getName());
        assertEquals(2, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#action(IMObject, BeanConsumer)} method.
     */
    @Test
    public void testObjectBeanAction() {
        Party location = createStaleObject();
        MutableInt calls = new MutableInt();
        ActionBuilder builder = newBuilder().action(location, asBean(bean -> {
            calls.increment();
            bean.setValue("name", "New Name");
            bean.save();
        }));
        verifyActionSucceeds(builder);
        assertEquals(2, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#action(ObjectSupplier, BeanConsumer)} method.
     */
    @Test
    public void testObjectSupplierBeanAction() {
        Party location = createStaleObject();
        ObjectSupplier<Party> supplier = new TestSupplier<>(location);
        MutableInt calls = new MutableInt();

        ActionBuilder builder = newBuilder().action(supplier, asBean(bean -> {
            calls.increment();
            bean.setValue("name", "New Name");
            bean.save();
        }));
        verifyActionSucceeds(builder);
        assertEquals("New Name", get(location).getName());
        assertEquals(2, calls.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#update(String, Object)} method.
     */
    @Test
    public void testUpdateAction() {
        Party location = createStaleObject();
        ActionBuilder builder = newBuilder().action(location, update("name", "New Name"));
        verifyActionSucceeds(builder);
        assertEquals("New Name", get(location).getName());
    }

    /**
     * Verifies that if an object is stale before it is updated, it is reloaded and the update succeeds.
     */
    @Test
    public void testObjectChangedBeforeUpdate() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer

        // now run the job. This should reload the object in order to successfully update it
        ActionBuilder builder = newBuilder().action(original, update("name", "New Name"));
        verifyActionSucceeds(builder);
        Party latest = get(original);
        assertEquals("New Name", latest.getName());
    }

    /**
     * Tests {@link ActionBuilder#withObject(IMObject)}, when used in conjunction with the
     * {@link ObjectCallBuilder#useLatestInstanceOnRetry()}, {@link ObjectCallBuilder#useLatestInstance()},
     * {@link ObjectCallBuilder#skipIfChangedOrMissing()} and {@link ObjectCallBuilder#failIfChangedOrMissing()}
     * methods.
     */
    @Test
    public void testWithObject() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer

        // Test the default behaviour. This fails if the object has changed on retry.
        ActionBuilder builder1 = newBuilder().withObject(original)
                .asBean(update("name", "New Name"));
        verifyActionFails(builder1, "Practice Location has been changed by another user");
        checkUnchanged(reloaded);

        // Now test useLatestInstanceOnRetry(). This reloads objects after the first call
        MutableInt calls2 = new MutableInt();
        ActionBuilder builder2 = newBuilder().withObject(original)
                .useLatestInstanceOnRetry()
                .call(object -> {
                    calls2.increment();
                    object.setName("New Name 2");
                    save(object);
                });
        verifyActionSucceeds(builder2);
        assertEquals(2, calls2.intValue());  // first call fails, second call succeeds
        assertEquals("New Name 2", get(original).getName());

        // Now test useLatestInstance(). This loads objects before passing to the call
        MutableInt calls3 = new MutableInt();
        ActionBuilder builder3 = newBuilder().withObject(original)
                .useLatestInstance()
                .call(object -> {
                    calls3.increment();
                    object.setName("New Name 3");
                    save(object);
                });
        verifyActionSucceeds(builder3);
        assertEquals(1, calls3.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());

        // Now test skipIfChangedOrMissing(). This skips the action on the second pass if the object is changed
        MutableInt calls4 = new MutableInt();
        ActionBuilder builder4 = newBuilder().withObject(original)
                .skipIfChangedOrMissing()
                .call(object -> {
                    calls4.increment();
                    object.setName("New Name 4");
                    save(object);
                });
        verifyActionSkipped(builder4, "Practice Location has been changed by another user");
        assertEquals(1, calls4.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());  // unchanged from previous

        // Now test failIfChangedOrMissing(). This skips the action on the second pass if the object is deleted
        remove(get(original));
        MutableInt calls5 = new MutableInt();
        ActionBuilder builder5 = newBuilder().withObject(original)
                .failIfChangedOrMissing()
                .call(object -> {
                    calls5.increment();
                    object.setName("New Name 5");
                    save(object);
                });
        verifyActionFails(builder5, "Practice Location no longer exists");
        assertEquals(1, calls5.intValue());  // single call as the object was reloaded first
    }

    /**
     * Tests {@link ActionBuilder#withObject(String, Supplier)}, when used in conjunction with the
     * {@link ObjectCallBuilder#useLatestInstanceOnRetry()}, {@link ObjectCallBuilder#useLatestInstance()},
     * {@link ObjectCallBuilder#skipIfChangedOrMissing()} and {@link ObjectCallBuilder#failIfChangedOrMissing()}
     * methods.
     */
    @Test
    public void testWithSupplier() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer
        String archetype = original.getArchetype();
        Supplier<Party> supplier = () -> original;

        // Test the default behaviour. This fails if the object has changed on retry.
        ActionBuilder builder1 = newBuilder().withObject(archetype, supplier)
                .asBean(update("name", "New Name"));
        verifyActionFails(builder1, "Practice Location has been changed by another user");
        checkUnchanged(reloaded);

        // Now test useLatestInstanceOnRetry(). This reloads objects after the first call
        MutableInt calls2 = new MutableInt();
        ActionBuilder builder2 = newBuilder().withObject(archetype, supplier)
                .useLatestInstanceOnRetry()
                .call(object -> {
                    calls2.increment();
                    object.setName("New Name 2");
                    save(object);
                });
        verifyActionSucceeds(builder2);
        assertEquals(2, calls2.intValue());  // first call fails, second call succeeds
        assertEquals("New Name 2", get(original).getName());

        // Now test useLatestInstance(). This loads objects before passing to the call
        MutableInt calls3 = new MutableInt();
        ActionBuilder builder3 = newBuilder().withObject(archetype, supplier)
                .useLatestInstance()
                .call(object -> {
                    calls3.increment();
                    object.setName("New Name 3");
                    save(object);
                });
        verifyActionSucceeds(builder3);
        assertEquals(1, calls3.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());

        // Now test skipIfChangedOrMissing(). This skips the action on the second pass if the object is changed
        MutableInt calls4 = new MutableInt();
        ActionBuilder builder4 = newBuilder().withObject(archetype, supplier)
                .skipIfChangedOrMissing()
                .call(object -> {
                    calls4.increment();
                    object.setName("New Name 4");
                    save(object);
                });
        verifyActionSkipped(builder4, "Practice Location has been changed by another user");
        assertEquals(1, calls4.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());  // unchanged from previous

        // Now test failIfChangedOrMissing(). This skips the action on the second pass if the object is deleted
        remove(get(original));
        MutableInt calls5 = new MutableInt();
        ActionBuilder builder5 = newBuilder().withObject(archetype, supplier)
                .failIfChangedOrMissing()
                .call(object -> {
                    calls5.increment();
                    object.setName("New Name 5");
                    save(object);
                });
        verifyActionFails(builder5, "Practice Location no longer exists");
        assertEquals(1, calls5.intValue());  // single call as the object was reloaded first
    }

    /**
     * Tests {@link ActionBuilder#withObject(ObjectSupplier)}, when used in conjunction with the
     * {@link ObjectCallBuilder#useLatestInstanceOnRetry()}, {@link ObjectCallBuilder#useLatestInstance()},
     * {@link ObjectCallBuilder#skipIfChangedOrMissing()} and {@link ObjectCallBuilder#failIfChangedOrMissing()}
     * methods.
     */
    @Test
    public void testWithObjectSupplier() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer
        ObjectSupplier<Party> supplier = new TestSupplier<>(original);

        // Test the default behaviour. This fails if the object has changed on retry.
        ActionBuilder builder1 = newBuilder().withObject(supplier)
                .asBean(update("name", "New Name"));
        verifyActionFails(builder1, "Practice Location has been changed by another user");
        checkUnchanged(reloaded);

        // Now test useLatestInstanceOnRetry(). This reloads objects after the first call
        MutableInt calls2 = new MutableInt();
        ActionBuilder builder2 = newBuilder().withObject(supplier)
                .useLatestInstanceOnRetry()
                .call(object -> {
                    calls2.increment();
                    object.setName("New Name 2");
                    save(object);
                });
        verifyActionSucceeds(builder2);
        assertEquals(2, calls2.intValue());  // first call fails, second call succeeds
        assertEquals("New Name 2", get(original).getName());

        // Now test useLatestInstance(). This loads objects before passing to the call
        MutableInt calls3 = new MutableInt();
        ActionBuilder builder3 = newBuilder().withObject(supplier)
                .useLatestInstance()
                .call(object -> {
                    calls3.increment();
                    object.setName("New Name 3");
                    save(object);
                });
        verifyActionSucceeds(builder3);
        assertEquals(1, calls3.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());

        // Now test skipIfChangedOrMissing(). This skips the action on the second pass if the object is changed
        MutableInt calls4 = new MutableInt();
        ActionBuilder builder4 = newBuilder().withObject(supplier)
                .skipIfChangedOrMissing()
                .call(object -> {
                    calls4.increment();
                    object.setName("New Name 4");
                    save(object);
                });
        verifyActionSkipped(builder4, "Practice Location has been changed by another user");
        assertEquals(1, calls4.intValue());  // single call as the object was reloaded first
        assertEquals("New Name 3", get(original).getName());  // unchanged from previous

        // Now test failIfChangedOrMissing(). This skips the action on the second pass if the object is deleted
        remove(get(original));
        MutableInt calls5 = new MutableInt();
        ActionBuilder builder5 = newBuilder().withObject(supplier)
                .failIfChangedOrMissing()
                .call(object -> {
                    calls5.increment();
                    object.setName("New Name 5");
                    save(object);
                });
        verifyActionFails(builder5, "Practice Location no longer exists");
        assertEquals(1, calls5.intValue());  // single call as the object was reloaded first
    }

    /**
     * Tests {@link ActionBuilder#withLatest(IMObject)} method.
     */
    @Test
    public void testWithLatest() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer

        MutableInt calls = new MutableInt();
        ActionBuilder builder = newBuilder().withLatest(original)
                .call(object -> {
                    calls.increment();
                    object.setName("New Name");
                    save(object);
                });
        verifyActionSucceeds(builder);
        assertEquals(1, calls.intValue());
        assertEquals("New Name", get(original).getName());
    }

    /**
     * Tests {@link ActionBuilder#withLatest(String, Supplier)} method.
     */
    @Test
    public void testWithLatestSupplier() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer

        String archetype = original.getArchetype();
        Supplier<Party> supplier = () -> original;

        MutableInt calls = new MutableInt();
        ActionBuilder builder = newBuilder().withLatest(archetype, supplier)
                .call(object -> {
                    calls.increment();
                    object.setName("New Name");
                    save(object);
                });
        verifyActionSucceeds(builder);
        assertEquals(1, calls.intValue());
        assertEquals("New Name", get(original).getName());
    }

    /**
     * Tests {@link ActionBuilder#withLatest(ObjectSupplier)} method.
     */
    @Test
    public void testWithLatestObjectSupplier() {
        Party original = createStaleObject();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        assertEquals("Original Name", original.getName());         // but the local version hasn't
        assertTrue(reloaded.getVersion() > original.getVersion()); // and the persistent version is newer

        ObjectSupplier<Party> supplier = new TestSupplier<>(original);

        MutableInt calls = new MutableInt();
        ActionBuilder builder = newBuilder().withLatest(supplier)
                .call(object -> {
                    calls.increment();
                    object.setName("New Name");
                    save(object);
                });
        verifyActionSucceeds(builder);
        assertEquals(1, calls.intValue());
        assertEquals("New Name", get(original).getName());
    }

    /**
     * Tests the {@link ActionBuilder#withObjects(List)} method when used in conjunction with the
     * {@link CallBuilder#useLatestInstanceOnRetry()}, {@link CallBuilder#useLatestInstance()},
     * {@link CallBuilder#skipIfChangedOrMissing()} and {@link CallBuilder#failIfChangedOrMissing()}
     * methods.
     */
    @Test
    public void testWithObjects() {
        Party original1 = createStaleObject();
        Party original2 = createStaleObject();

        List<Party> objects = Arrays.asList(original1, original2);

        // Test the default behaviour. This fails if the object has changed on retry.
        MutableInt calls1 = new MutableInt();
        ActionBuilder builder1 = newBuilder().withObjects(objects).call(
                list -> {
                    calls1.increment();
                    assertEquals(2, list.size());
                    list.get(0).setName("New Name 1");
                    list.get(1).setName("New Name 2");
                    save(list);
                });
        verifyActionFails(builder1, "Practice Location has been changed by another user");
        assertEquals(1, calls1.intValue());
        assertEquals("Updated Name", get(original1).getName());
        assertEquals("Updated Name", get(original2).getName());

        // Now test useLatestInstanceOnRetry(). This reloads objects after the first call
        MutableInt call2 = new MutableInt();
        ActionBuilder builder2 = newBuilder().withObjects(objects)
                .useLatestInstanceOnRetry()
                .call(
                        list -> {
                            call2.increment();
                            assertEquals(2, list.size());
                            list.get(0).setName("New Name 3");
                            list.get(1).setName("New Name 4");
                            save(list);
                        });
        verifyActionSucceeds(builder2);
        assertEquals(2, call2.intValue());
        assertEquals("New Name 3", get(original1).getName());
        assertEquals("New Name 4", get(original2).getName());

        // Now test useLatestInstance(). This loads objects before passing to the call
        MutableInt call3 = new MutableInt();
        ActionBuilder builder3 = newBuilder().withObjects(objects)
                .useLatestInstance()
                .call(
                        list -> {
                            call3.increment();
                            assertEquals(2, list.size());
                            list.get(0).setName("New Name 5");
                            list.get(1).setName("New Name 6");
                            save(list);
                        });
        verifyActionSucceeds(builder3);
        assertEquals(1, call3.intValue());
        assertEquals("New Name 5", get(original1).getName());
        assertEquals("New Name 6", get(original2).getName());

        // Now test skipIfChangedOrMissing(). This skips the action on the second pass if the object is changed
        MutableInt call4 = new MutableInt();
        ActionBuilder builder4 = newBuilder().withObjects(objects)
                .skipIfChangedOrMissing()
                .call(
                        list -> {
                            call4.increment();
                            assertEquals(2, list.size());
                            list.get(0).setName("New Name 7");
                            list.get(1).setName("New Name 8");
                            save(list);
                        });
        verifyActionSkipped(builder4, "Practice Location has been changed by another user");
        assertEquals(1, call4.intValue());
        assertEquals("New Name 5", get(original1).getName());
        assertEquals("New Name 6", get(original2).getName());

        // Now test failIfChangedOrMissing(). This skips the action on the second pass if the object is deleted
        remove(get(original1));
        MutableInt call5 = new MutableInt();
        ActionBuilder builder5 = newBuilder().withObjects(objects)
                .failIfChangedOrMissing()
                .call(
                        list -> {
                            call5.increment();
                            assertEquals(2, list.size());
                            list.get(0).setName("New Name 9");
                            list.get(1).setName("New Name 10");
                            save(list);
                        });
        verifyActionFails(builder5, "Practice Location no longer exists");
        assertEquals(1, call5.intValue());
    }

    /**
     * Tests the {@link ActionBuilder#withLatest(List)} method.
     */
    @Test
    public void testWithLatestObjects() {
        Party original1 = createStaleObject();
        Party original2 = createStaleObject();

        List<Party> objects = Arrays.asList(original1, original2);

        MutableInt calls = new MutableInt();
        ActionBuilder builder1 = newBuilder()
                .withLatest(objects)
                .call(list -> {
                    calls.increment();
                    assertEquals(2, list.size());
                    list.get(0).setName("New Name 1");
                    list.get(1).setName("New Name 2");
                    save(list);
                });
        verifyActionSucceeds(builder1);
        assertEquals(1, calls.intValue());
        assertEquals("New Name 1", get(original1).getName());
        assertEquals("New Name 2", get(original2).getName());
    }

    /**
     * Verifies that if an object is deleted before it is updated, the action fails with the correct error message.
     */
    @Test
    public void testObjectDeletedBeforeUpdate() {
        Party location = practiceFactory.createLocation();
        remove(location);

        ActionBuilder builder = newBuilder().action(location, update("name", "New Name"));
        verifyActionFails(builder, "Practice Location no longer exists");
    }

    /**
     * Verifies that if an action fails and is running interactively, the user is prompted to retry.
     */
    @Test
    public void testInteractiveRetry() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(() -> {
                    if (calls.getAndIncrement() < 1) {
                        throw new RuntimeException("Operation failed");
                    }
                })
                .interactiveOnly()                  // disable automatic retries
                .onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        findMessageDialogAndFireButton(ErrorDialog.class, "Error", "Operation failed", RETRY_ID);
        checkSucceeded(status.getValue());
        assertEquals(2, calls.intValue());
        assertEquals(1, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that if an action fails and is running in the background, the user is never prompted to retry.
     */
    @Test
    public void testBackgroundRetry() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(() -> {
                    if (calls.getAndIncrement() < 1) {
                        throw new RuntimeException("Operation failed");
                    }
                })
                .backgroundOnly()                  // disable interactive retries
                .automaticRetry(1)
                .delay(0)                          // disable delays
                .onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        assertNull(EchoTestHelper.findWindowPane(ErrorDialog.class));
        checkSucceeded(status.getValue());
        assertEquals(2, calls.intValue());
        assertEquals(1, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that an action can be retried automatically before prompting the user.
     */
    @Test
    public void testAutomaticAndInteractiveRetry() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(() -> {
                    // fail on the initial call + 3 automatic retries, succeed after the user has been prompted
                    if (calls.getAndIncrement() < 4) {
                        throw new RuntimeException("Operation failed");
                    }
                })
                .automaticRetry()   // default retries is 3
                .interactiveRetry()
                .onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        findMessageDialogAndFireButton(ErrorDialog.class, "Error", "Operation failed", RETRY_ID);
        checkSucceeded(status.getValue());
        assertEquals(5, calls.intValue());
        assertEquals(1, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that if an action fails and is running interactively, the user is prompted to retry and can
     * cancel the action.
     */
    @Test
    public void testInteractiveRetryCancellation() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(() -> {
                    if (calls.getAndIncrement() < 1) {
                        throw new RuntimeException("Operation failed");
                    }
                })
                .interactiveOnly()                  // disable background retries
                .onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        findMessageDialogAndFireButton(ErrorDialog.class, "Error", "Operation failed", ErrorDialog.CANCEL_ID);
        checkFailed(status.getValue(), "Operation failed");
        assertEquals(1, calls.intValue());
        assertEquals(0, success.intValue());
        assertEquals(1, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that if the {@link ActionBuilder#onSuccess(Runnable)} code throws an exception, it is reflected
     * in the {@link ActionStatus}.
     * The {@link ActionBuilder#onFailure(Runnable)} method should not be invoked, as that applies to the original
     * action.
     */
    @Test
    public void testOnSuccessMethodFailure() {
        MutableInt calls = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(calls::increment)
                .onSuccess(() -> {
                    throw new RuntimeException("Operation failed");
                })
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        checkFailed(status.getValue(), "Operation failed");
        assertEquals(1, calls.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that if the {@link ActionBuilder#onFailure(Runnable)} code throws an exception, its status
     * is discarded, and the original failure is reflected in the final status.
     * <p/>
     * This is because the original failure is likely to be more important to the user.
     */
    @Test
    public void testOnFailureMethodFailure() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = newBuilder().action(() -> {
                    calls.increment();
                    throw new RuntimeException("action() failed");
                })
                .onSuccess(success::increment)
                .backgroundOnly()                   // disable interactive retries
                .automaticRetry(0)                  // disable automatic retries
                .onFailure(() -> {
                    failure.increment();
                    throw new RuntimeException("onFailure() operation failed");
                })
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);
        checkFailed(status.getValue(), "action() failed"); // status represents the original failure
        assertEquals(1, calls.intValue());
        assertEquals(0, success.intValue());
        assertEquals(1, failure.intValue());               // verify onFailure() was called only once
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies that if the {@link ActionBuilder#onSkip(Runnable)} code throws an exception, its status
     * is discarded, and the original skip reason is reflected in the final status.
     * <p/>
     * This is because the original skip reason is likely to be more important to the user.
     */
    @Test
    public void testOnSkipMethodFailure() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Party object = createStaleObject();

        Action action = newBuilder()
                .withObject(object)
                .skipIfChangedOrMissing()
                .call(obj -> {
                    calls.increment();
                    obj.setName("New Name");
                    save(obj);
                })
                .onSuccess(success::increment)
                .backgroundOnly()                   // disable interactive retries
                .onFailure(failure::increment)
                .onSkip(() -> {
                    skip.increment();
                    throw new RuntimeException("onSkip() operation failed");
                })
                .build();
        action.run(status::setValue);
        checkSkipped(status.getValue(), "Practice Location has been changed by another user");
        assertEquals(1, calls.intValue());
        assertEquals(0, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(1, skip.intValue());
    }

    /**
     * Verifies{@link ActionBuilder#runOnce} method only calls the action once.
     */
    @Test
    public void testRunOnce() {
        MutableInt calls = new MutableInt();
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        newBuilder().action(() -> {
                    calls.increment();
                    throw new RuntimeException("action() failed");
                })
                .onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .runOnce(status::setValue);
        checkFailed(status.getValue(), "action() failed");
        assertEquals(1, calls.intValue());
        assertEquals(0, success.intValue());
        assertEquals(1, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Verifies the action is successful if no {@link ActionBuilder#onSuccess(Runnable)} action is specified.
     */
    @Test
    public void testNoSuccessAction() {
        MutableInt calls = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        newBuilder().action(calls::increment)
                .run(status::setValue);
        checkSucceeded(status.getValue());
        assertEquals(1, calls.intValue());
    }

    /**
     * Verifies that if the action fails, and there is no {@link ActionBuilder#onFailure(Runnable)} action, the
     * status reflects the failure.
     */
    @Test
    public void testNoFailureAction() {
        MutableInt calls = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        newBuilder().action(() -> {
                    calls.increment();
                    throw new RuntimeException("action() failed");
                })
                .backgroundOnly()
                .automaticRetry(1)
                .run(status::setValue);
        checkFailed(status.getValue(), "action() failed");
        assertEquals(2, calls.intValue());
    }

    /**
     * Verifies that if the action is skipped, and there is no {@link ActionBuilder#onSkip(Runnable)} action, the
     * status reflects the skip.
     */
    @Test
    public void testNoSkipAction() {
        Party object = createStaleObject();
        MutableInt calls = new MutableInt();

        MutableObject<ActionStatus> status = new MutableObject<>();

        newBuilder().withObject(object)
                .skipIfChangedOrMissing()
                .call(obj -> {
                    calls.increment();
                    obj.setName("New Name");
                    save(obj);
                })
                .run(status::setValue);
        checkSkipped(status.getValue(), "Practice Location has been changed by another user");
        assertEquals(1, calls.intValue());
    }

    /**
     * Verifies that the call is invoked within a transaction when the call is synchronous.
     * The object should be reloaded in the same transaction, but this is a little difficult to test TODO.
     * <p/>
     * The completion callback is not invoked within a transaction.
     */
    @Test
    public void testSynchronousActionHasTransaction() {
        assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
        Party object = createStaleObject();
        newBuilder().withLatest(object)
                .call(obj -> {
                    assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
                }).run(status -> {
                    assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
                });
    }

    /**
     * Verifies that the call for an asynchronous action is not called within a transaction.
     * <p/>
     * Asynchronous actions must manage their own transactions.
     */
    @Test
    public void testAsynchronousActionHasNoTransaction() {
        assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
        Party object = createStaleObject();
        newBuilder().withLatest(object)
                .call((obj, listener) -> {
                    assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
                    listener.accept(ActionStatus.success());
                }).run(status -> {
                    assertFalse(TransactionSynchronizationManager.isActualTransactionActive());
                });
    }

    /**
     * Creates a new action builder.
     *
     * @return the action builder
     */
    private ActionBuilder newBuilder() {
        return new ActionBuilder(getArchetypeService(), transactionManager);
    }

    /**
     * Runs an action, and verifies it succeeds.
     *
     * @param builder the action builder
     */
    private void verifyActionSucceeds(ActionBuilder builder) {
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = builder.onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);

        checkSucceeded(status.getValue());
        assertEquals(1, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Runs an action, and verifies it fails.
     *
     * @param builder the action builder
     * @param message the expected message
     */
    private void verifyActionFails(ActionBuilder builder, String message) {
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = builder.onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);

        checkFailed(status.getValue(), message);
        assertEquals(0, success.intValue());
        assertEquals(1, failure.intValue());
        assertEquals(0, skip.intValue());
    }

    /**
     * Runs an action, and verifies it is skipped.
     *
     * @param builder the action builder
     * @param message the expected message
     */
    private void verifyActionSkipped(ActionBuilder builder, String message) {
        MutableInt success = new MutableInt();
        MutableInt failure = new MutableInt();
        MutableInt skip = new MutableInt();
        MutableObject<ActionStatus> status = new MutableObject<>();

        Action action = builder.onSuccess(success::increment)
                .onFailure(failure::increment)
                .onSkip(skip::increment)
                .build();
        action.run(status::setValue);

        checkSkipped(status.getValue(), message);
        assertEquals(0, success.intValue());
        assertEquals(0, failure.intValue());
        assertEquals(1, skip.intValue());
    }

    /**
     * Creates an object, then updates it, returning the original version.
     * <p/>
     * This simulates an object being changed by another user.
     *
     * @return a stale object
     */
    private Party createStaleObject() {
        Party original = practiceFactory.newLocation().name("Original Name").build();
        practiceFactory.updateLocation(get(original))
                .name("Updated Name")
                .build();
        Party reloaded = get(original);
        assertEquals("Updated Name", reloaded.getName());          // verify the persistent version has changed
        return original;
    }

    /**
     * Verifies an action succeeded.
     *
     * @param status the action status
     */
    private void checkSucceeded(ActionStatus status) {
        checkStatus(status, true, null);
    }

    /**
     * Verifies an action failed.
     *
     * @param status  the action status
     * @param message the expected message
     */
    private void checkFailed(ActionStatus status, String message) {
        checkStatus(status, false, message);
    }

    /**
     * Verifies an action was skipped.
     *
     * @param status  the action status
     * @param message the expected message
     */
    private void checkSkipped(ActionStatus status, String message) {
        assertNotNull(status);
        assertTrue(status.skipped());
        assertFalse(status.succeeded());
        assertFalse(status.failed());
        assertEquals(message, status.getMessage());
    }

    /**
     * Verifies an action status matches that expected.
     *
     * @param status    the status to check
     * @param succeeded if {@code true}, the action is expected to have succeeded, otherwise it has failed
     * @param message   the expected message
     */
    private void checkStatus(ActionStatus status, boolean succeeded, String message) {
        assertNotNull(status);
        assertEquals(succeeded, status.succeeded());
        assertEquals(!succeeded, status.failed());
        assertEquals(message, status.getMessage());
    }

    /**
     * Verify an object is unchanged with respect to its persistent version.
     *
     * @param object the object to check
     */
    private void checkUnchanged(Party object) {
        // verify the object hasn't changed
        Party latest = get(object);
        assertEquals(object.getVersion(), latest.getVersion());
    }

    /**
     * An {@link ObjectSupplier} that simply returns the supplied object.
     */
    private static class TestSupplier<T extends IMObject> implements ObjectSupplier<T> {
        private final T object;

        public TestSupplier(T object) {
            this.object = object;
        }

        @Override
        public T get() {
            return object;
        }

        @Override
        public String getArchetype() {
            return object.getArchetype();
        }
    }
}