/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.deposit.DepositArchetypes;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.stock.StockArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;


/**
 * Helper for document tests.
 *
 * @author Tim Anderson
 */
public class DocumentTestHelper {

    /**
     * Helper to create a report context.
     *
     * @return a new context
     */
    public static Context createReportContext() {
        Context context = new LocalContext();
        addContext(context, PracticeArchetypes.PRACTICE, "Vets R Us");
        addContext(context, PracticeArchetypes.LOCATION, "Main Clinic");
        addContext(context, StockArchetypes.STOCK_LOCATION, "Main Stock");
        context.setCustomer(TestHelper.createCustomer("J", "Smith", false));
        addContext(context, PatientArchetypes.PATIENT, "Fido");
        addContext(context, SupplierArchetypes.SUPPLIER_ORGANISATION, "Vet Supplies");
        addContext(context, ProductArchetypes.MEDICATION, "Acepromazine");
        addContext(context, DepositArchetypes.DEPOSIT_ACCOUNT, "Main Deposit");
        addContext(context, TillArchetypes.TILL, "Main Till");
        User clinician = TestHelper.createClinician(false);
        clinician.setName("Vet");
        context.setClinician(clinician);
        User user = TestHelper.createUser("User", false);
        context.setUser(user);
        addContext(context, PatientArchetypes.CLINICAL_EVENT);
        addContext(context, CustomerAccountArchetypes.INVOICE);
        addContext(context, ScheduleArchetypes.APPOINTMENT);
        addContext(context, ScheduleArchetypes.TASK);
        return context;
    }

    /**
     * Creates an adds an object to the context, setting its name.
     *
     * @param context   the context
     * @param shortName the archetype short name of the object to add
     * @param name      the object name
     */
    private static void addContext(Context context, String shortName, String name) {
        addContext(context, shortName).setName(name);
    }

    /**
     * Creates an adds an object to the context.
     *
     * @param context   the context
     * @param shortName the archetype short name of the object to add
     * @return the added object
     */
    private static IMObject addContext(Context context, String shortName) {
        IMObject object = TestHelper.create(shortName);
        context.addObject(object);
        return object;
    }

}
