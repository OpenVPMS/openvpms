/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.bound;

import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.macro.Macros;
import org.openvpms.macro.impl.AbstractMacros;
import org.openvpms.macro.impl.ExpressionMacro;
import org.openvpms.macro.impl.ExpressionMacroRunner;
import org.openvpms.macro.impl.Macro;
import org.openvpms.macro.impl.MacroContext;
import org.openvpms.macro.impl.MacroFactory;
import org.openvpms.macro.impl.MacroRunner;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.StringPropertyTransformer;
import org.openvpms.web.echo.text.TextComponent;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;


/**
 * Tests text components that bind to properties.
 *
 * @author Tim Anderson
 */
public abstract class AbstractBoundTextComponentTest extends AbstractBoundFieldTest<TextComponent, String> {

    /**
     * Constructs an {@link AbstractBoundTextComponentTest}.
     * <p/>
     * Two test values are passed to the superclass: "value1" and "value2".
     */
    AbstractBoundTextComponentTest() {
        super("value1", "value2");
    }

    /**
     * Constructs an {@link AbstractBoundTextComponentTest}.
     *
     * @param value1 the first test value
     * @param value2 the second test value
     */
    AbstractBoundTextComponentTest(String value1, String value2) {
        super(value1, value2);
    }

    /**
     * Returns the value of the field.
     *
     * @param field the field
     * @return the value of the field
     */
    protected String getValue(TextComponent field) {
        return field.getText();
    }

    /**
     * Sets the value of the field.
     *
     * @param field the field
     * @param value the value to set
     */
    protected void setValue(TextComponent field, String value) {
        field.setText(value);
    }

    /**
     * Creates a new property.
     *
     * @return a new property
     */
    protected Property createProperty() {
        return new SimpleProperty("String", String.class);
    }

    /**
     * Tests expansion of single macros when the text is updated by the field.
     */
    void checkSingleMacroExpansion() {
        // verify that when the cursor is within the macro text, it moves to the end of the expanded text
        checkMacroExpansion("@short", 0, "a", 1);
        checkMacroExpansion("@short 1", 0, "a 1", 1);
        checkMacroExpansion("1 @short 2", 2, "1 a 2", 3);
        checkMacroExpansion("3 @short 4", 3, "3 a 4", 3);

        checkMacroExpansion("@same", 0, "12345", 5);
        checkMacroExpansion("@same 1", 0, "12345 1", 5);
        checkMacroExpansion("1 @same 2", 2, "1 12345 2", 7);
        checkMacroExpansion("3 @same 4", 3, "3 12345 4", 7);

        checkMacroExpansion("@long", 0, "abcdef", 6);
        checkMacroExpansion("@long 1", 0, "abcdef 1", 6);
        checkMacroExpansion("1 @long 2", 2, "1 abcdef 2", 8);
        checkMacroExpansion("3 @long 4", 3, "3 abcdef 4", 8);

        // when the cursor position is before the macro text, it doesn't move
        checkMacroExpansion("1 @short 2", 0, "1 a 2", 0);
        checkMacroExpansion("1 @short 2", 1, "1 a 2", 1);

        checkMacroExpansion("1 @same 2", 0, "1 12345 2", 0);
        checkMacroExpansion("1 @same 2", 1, "1 12345 2", 1);

        checkMacroExpansion("1 @long 2", 0, "1 abcdef 2", 0);
        checkMacroExpansion("1 @long 2", 1, "1 abcdef 2", 1);

        // when the cursor position is after the macro text, it keeps its relative position
        checkMacroExpansion("1 @short 2", 8, "1 a 2", 3);
        checkMacroExpansion("1 @short 2", 9, "1 a 2", 4);

        checkMacroExpansion("1 @same 2", 7, "1 12345 2", 7);
        checkMacroExpansion("1 @same 2", 8, "1 12345 2", 8);

        checkMacroExpansion("1 @long 2", 7, "1 abcdef 2", 8);
        checkMacroExpansion("1 @long 2", 8, "1 abcdef 2", 9);
    }

    /**
     * Checks macro expansion when the text is updated from the field.
     * <p/>
     * The UI can send the cursor position and text properties in any order, so this checks twice; the first
     * time sends the text followed by the cursor position, the second the other way around.
     *
     * @param inputText     the input text
     * @param inputPosition the input cursor position
     * @param expandedText  the expanded text
     * @param newPosition   the new cursor position
     */
    private void checkMacroExpansion(String inputText, int inputPosition, String expandedText, int newPosition) {
        // send text before cursor position
        checkMacroExpansion(inputText, inputPosition, expandedText, newPosition, false);

        // send cursor position before text
        checkMacroExpansion(inputText, inputPosition, expandedText, newPosition, true);
    }

    /**
     * Checks macro expansion when the text is updated from the field.
     * <p/>
     * The UI can send the cursor position and text properties in any order, so this checks twice; the first
     * time sends the text followed by the cursor position, the second the other way around.
     *
     * @param inputText        the input text
     * @param inputPosition    the input cursor position
     * @param expandedText     the expanded text
     * @param newPosition      the new cursor position
     * @param cursorBeforeText if {@code true} send the {@link TextComponent#PROPERTY_CURSOR_POSITION} before the text,
     *                         else send it after
     */
    private void checkMacroExpansion(String inputText, int inputPosition, String expandedText, int newPosition,
                                     boolean cursorBeforeText) {
        Property property = createProperty();
        Macros macros = new TestMacros();
        TextComponent component = createField(property);

        // used to verify the TEXT_CHANGED_PROPERTY event is fired once
        AtomicInteger count = new AtomicInteger();
        component.addPropertyChangeListener(TextComponent.TEXT_CHANGED_PROPERTY, evt -> count.incrementAndGet());

        property.setTransformer(new StringPropertyTransformer(property, macros));

        // simulate the UI updating the field
        if (cursorBeforeText) {
            component.processInput(TextComponent.PROPERTY_CURSOR_POSITION, inputPosition);
        }
        component.processInput(TextComponent.TEXT_CHANGED_PROPERTY, inputText);
        if (!cursorBeforeText) {
            component.processInput(TextComponent.PROPERTY_CURSOR_POSITION, inputPosition);
        }

        // verify the text and cursor position match that expected
        assertEquals(expandedText, component.getText());
        assertEquals(newPosition, component.getCursorPosition());

        // check the event was fired twice - once for the original text, the second time for the expansion
        assertEquals(2, count.get());
    }

    private static class TestMacros extends AbstractMacros {

        private final MacroFactory factory;

        TestMacros() {
            factory = Mockito.mock(MacroFactory.class);
            Mockito.when(factory.create(any(), any())).thenAnswer((Answer<MacroRunner>) invocationOnMock -> {
                return new ExpressionMacroRunner((MacroContext) invocationOnMock.getArguments()[1]);
            });
            // add macro that replaces @short with a
            add(new ExpressionMacro("@short", "short macro", "'a'"));

            // add macro that replaces @long with abcdef (i.e. longer than macro text)
            add(new ExpressionMacro("@long", "long macro", "'abcdef'"));

            // add macro that replaces @same with 12345 (i.e. same length as macro text)
            add(new ExpressionMacro("@same", "same macro", "'12345'"));
        }

        /**
         * Creates a new macro context.
         *
         * @param macros    the macros, keyed on code
         * @param object    the object to evaluate macros against
         * @param variables the scoped variables
         * @return a new macro context
         */
        @Override
        protected MacroContext createMacroContext(Map<String, Macro> macros, Object object, Variables variables) {
            return new MacroContext(macros, factory, object, variables, null);
        }
    }


}
