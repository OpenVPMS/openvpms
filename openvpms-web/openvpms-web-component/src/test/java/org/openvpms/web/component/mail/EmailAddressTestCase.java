/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link EmailAddress}.
 *
 * @author Tim Anderson
 */
public class EmailAddressTestCase {

    /**
     * Tests {@link EmailAddress#parse(String)}.
     */
    @Test
    public void testParse() {
        // valid addresses
        EmailAddress address1 = EmailAddress.parse("foo@bar.com");
        assertNull(address1.getName());
        assertEquals("foo@bar.com", address1.getAddress());

        EmailAddress address2 = EmailAddress.parse("<foo@bar.com>");
        assertNull(address2.getName());
        assertEquals("foo@bar.com", address2.getAddress());

        EmailAddress address3 = EmailAddress.parse("\"Foo Bar\" <foo@bar.com>");
        assertEquals("Foo Bar", address3.getName());
        assertEquals("foo@bar.com", address3.getAddress());

        EmailAddress address4 = EmailAddress.parse("Foo Bar <foo@bar.com>");
        assertEquals("Foo Bar", address4.getName());
        assertEquals("foo@bar.com", address4.getAddress());

        EmailAddress address5 = EmailAddress.parse("<foo@bar.com>"); // valid according to JavaMail
        assertNull(address5.getName());
        assertEquals("foo@bar.com", address5.getAddress());

        // invalid addresses
        EmailAddress invalid1 = EmailAddress.parse(null);
        assertNull(invalid1);

        EmailAddress invalid2 = EmailAddress.parse("");
        assertNull(invalid2);

        EmailAddress invalid3 = EmailAddress.parse("@bar.com");
        assertNull(invalid3);

        EmailAddress invalid4 = EmailAddress.parse("foo@");
        assertNull(invalid4);
    }
}