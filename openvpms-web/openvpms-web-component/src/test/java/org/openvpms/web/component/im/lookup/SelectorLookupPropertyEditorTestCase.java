/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.select.AbstractQuerySelector;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.test.AbstractAppTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.workflow.ScheduleArchetypes.VISIT_REASON;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link SelectorLookupPropertyEditor}.
 *
 * @author Tim Anderson
 */
public class SelectorLookupPropertyEditorTestCase extends AbstractAppTest {

    /**
     * Test selector editor.
     */
    private TestEditor editor;

    /**
     * The property being edited.
     */
    private SimpleProperty property;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        Act object = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        object.setReason(null);
        property = new SimpleProperty("reason", String.class);
        property.setRequired(true);
        editor = new TestEditor(
                VISIT_REASON, property, object,
                new DefaultLayoutContext(true, new LocalContext(), new HelpContext("foo", null)));
    }

    /**
     * Tests behaviour when the text field is populated with text for which there is only one match.
     */
    @Test
    public void testSetTextForOneMatch() {
        Lookup lookup1 = createLookup("Checkup");
        Lookup lookup2 = createLookup("Surgery");
        editor.setQueryObjects(lookup1, lookup2);
        TextField textField = editor.getSelector().getTextField();

        // check the preconditions
        assertNull(editor.getObject());
        assertFalse(editor.isModified());
        assertNull(property.getString());
        assertFalse(editor.isValid());

        // now set the text field with a partial name. It should resolve to the lookup.
        checkSet("Ch", lookup1);
        textField.setText("Ch");

        // now clear the text field. The property should be set to null and be invalid
        checkSet(null, null);

        // now verify it uses contains matches
        checkSet("eck", lookup1);
    }

    /**
     * Tests behaviour when the text field is populated with text for which there are two matches.
     */
    @Test
    public void testSetTextForTwoMatches() {
        Lookup lookup1 = createLookup("Day surgery");
        Lookup lookup2 = createLookup("Overnight surgery");

        editor.setQueryObjects(lookup1, lookup2);

        editor.setSelection(lookup2); // select lookup2 when the browser is displayed
        checkSet("surgery", lookup2);

        // now enter a partial match, this time selecting lookup 1
        editor.setSelection(lookup1);
        checkSet("surg", lookup1);
    }

    /**
     * Verifies that when wildcards are specified, they override the default contains search.
     */
    @Test
    public void testWildcards() {
        Lookup lookup1 = createLookup("Checkup");
        Lookup lookup2 = createLookup("Surgery");
        Lookup lookup3 = createLookup("Vaccination");
        editor.setQueryObjects(lookup1, lookup2, lookup3);

        // check starts-with
        checkSet("ch*", lookup1);

        // check contains
        checkSet("*urge*", lookup2);

        // check ends-with
        checkSet("*up", lookup1);
    }

    /**
     * Tests the behaviour when there is no match for the entered text.
     */
    @Test
    public void testNoMatch() {
        Lookup lookup1 = createLookup("Vaccination");
        Lookup lookup2 = createLookup("Checkup");
        editor.setQueryObjects(lookup1, lookup2);
        checkSet("vacc", lookup1);

        // no match
        TextField field = editor.getSelector().getTextField();
        field.setText("*vax*");
        assertEquals("*vax*", field.getText());
        assertTrue(editor.isModified());
        assertEquals(lookup1, editor.getObject());             // still matches the previous selection
        assertEquals(lookup1.getCode(), property.getString());
        assertInvalid(editor, "*vax* is not a valid value for Reason");

        // verify the field can be set to a valid value
        checkSet("*vac*", lookup1);
    }

    /**
     * Sets the field to the supplied text, and verify the appropriate lookup is selected.
     *
     * @param text     the text. If {@code null}, the field will be invalid as the property is mandatory
     * @param expected the expected lookup, or {@code null} if there are no matches expecfted
     */
    private void checkSet(String text, Lookup expected) {
        TextField field = editor.getSelector().getTextField();
        field.setText(text);

        if (expected != null) {
            assertEquals(expected.getName(), field.getText());
            assertEquals(expected, editor.getObject());
            assertTrue(editor.isModified());
            assertEquals(expected.getCode(), property.getString());
            assertTrue(editor.isValid());
        } else {
            assertNull(editor.getObject());
            assertFalse(editor.isValid());
            assertNull(property.getString());
        }
    }

    /**
     * Creates a lookup.
     *
     * @param name the name
     * @return the lookup
     */
    private Lookup createLookup(String name) {
        Lookup lookup = create(VISIT_REASON, Lookup.class);
        lookup.setCode(name.toUpperCase());
        lookup.setName(name);
        return lookup;
    }

    /**
     * Test verion of the {@link SelectorLookupPropertyEditor} that allows the lookups being queried
     * to be specified.
     */
    private static class TestEditor extends SelectorLookupPropertyEditor {

        /**
         * The available lookups.
         */
        private List<Lookup> lookups = Collections.emptyList();

        /**
         * The selection, for when a browser is displayed.
         */
        private Lookup selection;

        /**
         * Constructs a {@link SelectorLookupPropertyEditor}.
         *
         * @param archetype the lookup archetype to query
         * @param property  the property being edited
         * @param parent    the parent object
         * @param context   the layout context
         */
        public TestEditor(String archetype, Property property, IMObject parent, LayoutContext context) {
            super(archetype, property, parent, context);
        }

        /**
         * Sets the lookups that are queried.
         *
         * @param lookups the lookups
         */
        public void setQueryObjects(Lookup... lookups) {
            this.lookups = Arrays.asList(lookups);
        }

        /**
         * Sets the selection, for when the browser is displayed.
         *
         * @param selection the selection
         */
        public void setSelection(Lookup selection) {
            this.selection = selection;
        }

        /**
         * Returns the selector.
         *
         * @return the selector
         */
        @Override
        public AbstractQuerySelector<Lookup> getSelector() {
            return super.getSelector();
        }

        /**
         * Returns the object corresponding to the property.
         *
         * @return the object. May be {@code null}
         */
        @Override
        public Lookup getObject() {
            if (lookups != null) {
                String code = getProperty().getString();
                return lookups.stream().filter(l -> l.getCode().equals(code)).findFirst().orElse(null);
            }
            return null;
        }

        /**
         * Returns the available lookups for a property.
         *
         * @param property the property
         * @return the available lookups
         */
        @Override
        protected List<Lookup> getLookups(Property property) {
            return lookups;
        }

        /**
         * Creates a new selector.
         *
         * @param property    the property
         * @param context     the layout context
         * @param allowCreate determines if objects may be created
         * @return a new selector
         */
        @Override
        protected AbstractQuerySelector<Lookup> createSelector(Property property, LayoutContext context,
                                                               boolean allowCreate) {
            return new TestLookupSelector(property, allowCreate, context);
        }

        private class TestLookupSelector extends LookupSelector {
            public TestLookupSelector(Property property, boolean allowCreate, LayoutContext context) {
                super(property, allowCreate, context);
            }

            /**
             * Pop up a dialog to select an object.
             *
             * @param runQuery if {@code true} run the query
             */
            @Override
            protected void onSelect(boolean runQuery) {
                assertNotNull("selection cannot be null", selection);
                Browser<Lookup> browser = createBrowser(getText(), runQuery);
                onSelected(selection, browser);
            }
        }
    }
}