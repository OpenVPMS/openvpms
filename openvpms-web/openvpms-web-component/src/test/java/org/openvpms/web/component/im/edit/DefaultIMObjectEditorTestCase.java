/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.junit.Test;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link DefaultIMObjectEditor}.
 *
 * @author Tim Anderson
 */
public class DefaultIMObjectEditorTestCase extends AbstractAppTest {

    /**
     * Tests the {@link DefaultIMObjectEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party patient = TestHelper.createPatient();
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DefaultIMObjectEditor editor = new DefaultIMObjectEditor(patient, null, context);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof DefaultIMObjectEditor);
    }

    /**
     * Verifies that a singleton instance cannot be saved, if one already exists and is active.
     */
    @Test
    public void testSingleton() {
        ArchetypeDescriptor descriptor = new ArchetypeDescriptor();
        String archetype = TestHelper.randomName("entity.testSingleton");
        String name = archetype + ".1.0";
        descriptor.setName(name);
        descriptor.setClassName(org.openvpms.component.business.domain.im.common.Entity.class.getName());
        descriptor.setSingleton(true);
        descriptor.setDisplayName("Test Singleton");
        NodeDescriptor id = new NodeDescriptor();
        id.setName("id");
        id.setPath("/id");
        id.setType(Long.class.getName());
        NodeDescriptor active = new NodeDescriptor();
        active.setName("active");
        active.setPath("/active");
        active.setType(Boolean.class.getName());
        descriptor.addNodeDescriptor(id);
        descriptor.addNodeDescriptor(active);
        save(descriptor);

        IMObject object1 = create(archetype);
        save(object1);

        IMObject object2 = create(archetype);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DefaultIMObjectEditor editor = new DefaultIMObjectEditor(object2, null, context);
        assertInvalid(editor, "There is a limit of a single active Test Singleton");

        // now deactivate object1 and verify object2 can be saved
        object1.setActive(false);
        save(object1);

        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));
    }
}
