/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.product;

import nextapp.echo2.app.Table;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceTestHelper;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditableIMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectTableCollectionEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityLinkEditor;
import org.openvpms.web.component.im.relationship.RelationshipState;
import org.openvpms.web.component.im.table.IMTable;
import org.openvpms.web.component.im.view.IMObjectView;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.math.MathRules.ONE_HUNDRED;
import static org.openvpms.archetype.test.TestHelper.getDatetime;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link ProductEditor}.
 *
 * @author Tim Anderson
 */
public class ProductEditorTestCase extends AbstractAppTest {

    /**
     * The product rules.
     */
    @Autowired
    private ProductRules rules;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        practiceFactory.getPractice();   // make sure the tax rates are removed
        super.setUp();
    }

    /**
     * Verifies that uncommitted unit prices are calculated when autoPriceUpdate is set true.
     */
    @Test
    public void testUnitPriceCalculatedWhenAutoPriceUpdateTrue() {
        Product product = productFactory.newMedication().build(false);
        Party supplier = supplierFactory.createSupplier();

        assertTrue(product.getProductPrices().isEmpty());
        ProductEditor editor = createEditor(product);
        IMObjectTableCollectionEditor prices = editor.getPricesEditor();

        // add a new price. This won't be added to the product until the product-supplier relationship is populated
        ProductPriceEditor priceEditor = (ProductPriceEditor) prices.add(ProductArchetypes.UNIT_PRICE);
        assertNotNull(priceEditor);
        checkEquals(ZERO, priceEditor.getCost());
        checkEquals(BigDecimal.valueOf(100), priceEditor.getMarkup());
        checkEquals(ZERO, priceEditor.getPrice());
        assertFalse(priceEditor.isValid());
        ProductPrice unit = priceEditor.getObject();
        assertNull(unit.getPrice());  // underlying price should be null
        assertTrue(product.getProductPrices().isEmpty()); // not added yet

        Date from = unit.getFromDate();

        // populate an entityLink.productSupplier
        BigDecimal listPrice = ONE;
        BigDecimal newPrice = BigDecimal.valueOf(2);
        addSupplier(editor, supplier, listPrice, true);

        // price should be updated
        assertTrue(priceEditor.isValid());
        checkEquals(listPrice, priceEditor.getCost());
        checkEquals(ONE_HUNDRED, priceEditor.getMarkup());
        checkEquals(newPrice, priceEditor.getPrice());
        assertTrue(product.getProductPrices().contains(unit)); // now added

        // verify the persistent prices match those expected
        assertTrue(SaveHelper.save(editor));
        product = get(product);
        assertEquals(1, product.getProductPrices().size());
        checkPrice(product, newPrice, listPrice, from, null, "Price created by auto-update of supplier: "
                                                             + supplier.getName());
    }

    /**
     * Verifies that a new unit price is created when autoPriceUpdate is set true and the existing unit price is
     * persistent.
     */
    @Test
    public void testNewUnitPriceCalculatedWhenAutoPriceUpdateTrue() {
        Product product = productFactory.newMedication().build(false);
        assertTrue(product.getProductPrices().isEmpty());

        Date date1 = DateRules.getYesterday();
        ProductPrice unit1 = ProductPriceTestHelper.createUnitPrice(ONE, ZERO, date1, null);
        product.addProductPrice(unit1);
        save(product);

        ProductEditor editor = createEditor(product);
        List<ProductPrice> prices1 = editor.getPrices();
        assertEquals(1, prices1.size());

        // add a supplier. This should update the price.
        Party supplier = supplierFactory.createSupplier();
        BigDecimal listPrice = BigDecimal.valueOf(2);
        EntityLinkEditor productSupplier = addSupplier(editor, supplier, listPrice, true);
        assertTrue(editor.isValid());

        List<ProductPrice> prices2 = editor.getPrices();
        assertEquals(2, prices2.size());

        // verify unit1 has been closed off
        assertEquals(unit1, prices2.get(0));
        assertEquals(date1, unit1.getFromDate());
        Date unit1ToDate = unit1.getToDate();
        assertNotNull(unit1ToDate);
        assertTrue(unit1ToDate.compareTo(DateRules.getToday()) >= 0);
        ProductPriceTestHelper.checkPrice(unit1, ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED);

        // verify a new prices has been created, starting at the end of unit1.
        ProductPrice unit2 = prices2.get(1);
        assertEquals(unit2.getFromDate(), unit1ToDate);
        assertNull(unit2.getToDate());
        ProductPriceTestHelper.checkPrice(unit2, BigDecimal.valueOf(4), listPrice, ONE_HUNDRED, ONE_HUNDRED,
                                          unit1ToDate, null);

        // now update the list price again, and verify the unsaved price is updated
        BigDecimal listPrice3 = BigDecimal.valueOf(3);
        setListPrice(productSupplier, listPrice3);
        List<ProductPrice> prices3 = editor.getPrices();
        assertEquals(2, prices3.size());
        ProductPriceTestHelper.checkPrice(prices3.get(0), ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED, date1, unit1ToDate);
        ProductPriceTestHelper.checkPrice(prices3.get(1), BigDecimal.valueOf(6), listPrice3, ONE_HUNDRED, ONE_HUNDRED,
                                          unit1ToDate, null);

        // verify the persistent prices match those expected
        assertTrue(SaveHelper.save(editor));
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ONE, ZERO, date1, unit1ToDate, null);
        checkPrice(product, BigDecimal.valueOf(6), listPrice3, unit1ToDate, null,
                   "Price created by auto-update of supplier: " + supplier.getName());
    }

    /**
     * Verifies that unit prices are not calculated when autoPriceUpdate is set false.
     */
    @Test
    public void testUnitPriceNotCalculatedWhenAutoPriceUpdateFalse() {
        Product product = productFactory.newMedication().build(false);
        assertTrue(product.getProductPrices().isEmpty());

        Date date1 = DateRules.getYesterday();
        ProductPrice unit1 = ProductPriceTestHelper.createUnitPrice(ONE, ZERO, date1, null);
        product.addProductPrice(unit1);
        save(product);

        ProductEditor editor = createEditor(product);
        List<ProductPrice> prices1 = editor.getPrices();
        assertEquals(1, prices1.size());

        // add a supplier with autoPriceUpdate -= false
        Party supplier = supplierFactory.createSupplier();
        BigDecimal listPrice = BigDecimal.valueOf(2);
        addSupplier(editor, supplier, listPrice, false);
        assertTrue(editor.isValid());

        // verify the price is unchanged
        List<ProductPrice> prices2 = editor.getPrices();
        assertEquals(1, prices2.size());
        ProductPriceTestHelper.checkPrice(prices2.get(0), ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED, date1, null);

        // now verify the persistent price hasn't changed
        assertTrue(SaveHelper.save(editor));
        product = get(product);
        Set<ProductPrice> prices = product.getProductPrices();
        assertEquals(1, prices.size());
        checkPrice(product, ONE, ZERO, date1, null, null);
    }

    /**
     * Verifies that the unit prices are only updated when the cost price changes on a product-supplier relationship.
     */
    @Test
    public void testAutoPriceUpdateOnlyOccursWhenCostPriceChanges() {
        Party supplier1 = supplierFactory.createSupplier();
        Party supplier2 = supplierFactory.createSupplier();
        Party supplier3 = supplierFactory.createSupplier();
        supplier3.setActive(false);
        save(supplier3);
        Product product = productFactory.newMedication().build(false);

        Date date1 = DateRules.getYesterday();
        ProductPrice unit1 = ProductPriceTestHelper.createUnitPrice(ONE, ZERO, date1, null);
        product.addProductPrice(unit1);
        ProductSupplier ps1 = rules.createProductSupplier(product, supplier1);
        ps1.setPackageSize(5);
        ps1.setListPrice(BigDecimal.valueOf(20));
        ps1.setAutoPriceUpdate(true);
        ProductSupplier ps2 = rules.createProductSupplier(product, supplier2);
        ps2.setPackageSize(10);
        ps2.setListPrice(BigDecimal.valueOf(25));
        ps2.setAutoPriceUpdate(false);
        ProductSupplier ps3 = rules.createProductSupplier(product, supplier3);
        ps3.setPackageSize(12);
        ps3.setListPrice(BigDecimal.valueOf(30));
        ps3.setAutoPriceUpdate(false);
        save(product);

        ProductEditor editor = createEditor(product);
        List<ProductPrice> prices1 = editor.getPrices();
        assertEquals(1, prices1.size());
        checkPrice(product, ONE, ZERO, date1, null, null);
        EntityLinkEditor supplierEditor2 = getSupplierEditor(editor, supplier2);
        setAutoPriceUpdate(supplierEditor2, true);

        // supplier3 is inactive so shouldn't trigger updates
        EntityLinkEditor supplierEditor3 = getSupplierEditor(editor, supplier3);
        setAutoPriceUpdate(supplierEditor3, true);

        List<ProductPrice> prices2 = editor.getPrices();
        assertEquals(2, prices2.size());

        // verify unit1 has been closed off
        assertEquals(unit1, prices2.get(0));
        assertEquals(date1, unit1.getFromDate());
        Date unit1ToDate = unit1.getToDate();
        assertNotNull(unit1ToDate);
        assertTrue(unit1ToDate.compareTo(DateRules.getToday()) >= 0);
        ProductPriceTestHelper.checkPrice(unit1, ONE, ZERO, ONE_HUNDRED, ONE_HUNDRED);

        // verify a new prices has been created, starting at the end of unit1.
        ProductPrice unit2 = prices2.get(1);
        assertEquals(unit2.getFromDate(), unit1ToDate);
        assertNull(unit2.getToDate());
        ProductPriceTestHelper.checkPrice(unit2, BigDecimal.valueOf(5), new BigDecimal("2.5"), ONE_HUNDRED, ONE_HUNDRED,
                                          unit1ToDate, null);

        // verify the persistent prices match those expected
        assertTrue(SaveHelper.save(editor));
        product = get(product);
        assertEquals(2, product.getProductPrices().size());
        checkPrice(product, ONE, ZERO, date1, unit1ToDate, null);
        checkPrice(product, BigDecimal.valueOf(5), new BigDecimal("2.5"), unit1ToDate, null,
                   "Price created by auto-update of supplier: " + supplier2.getName());
    }

    /**
     * Verifies that a product is invalid if its unit prices date range overlap.
     */
    @Test
    public void testDateRangeOverlap() {
        Product product = productFactory.newMedication().build(false);
        ProductPrice unit1 = ProductPriceTestHelper.createUnitPrice("2016-01-01", null);
        ProductPrice unit2 = ProductPriceTestHelper.createUnitPrice("2016-03-01", null);
        product.addProductPrice(unit1);
        product.addProductPrice(unit2);

        ProductEditor editor = createEditor(product);

        assertFalse(editor.isValid());
        unit1.setToDate(unit2.getFromDate());

        assertTrue(editor.isValid());
    }

    /**
     * Verifies that if two prices have date range overlaps, but the from and to dates are the same, the editor
     * automatically adjusts the times so they no longer overlap.
     */
    @Test
    public void testAdjustTimeOverlap() {
        Product product = productFactory.newMedication().build(false);
        ProductPrice unit1 = ProductPriceTestHelper.createUnitPrice("2016-01-01", null);
        Date fromDate = getDatetime("2016-03-25 10:00:00");
        ProductPrice unit2 = ProductPriceTestHelper.createUnitPrice(fromDate, null);
        product.addProductPrice(unit1);
        product.addProductPrice(unit2);

        ProductEditor editor = createEditor(product);
        assertFalse(editor.isValid());

        // now set the unit1 to-date so that it overlaps unit2 by 5 minutes
        unit1.setToDate(getDatetime("2016-03-25 10:05:00"));
        assertTrue(editor.isValid());

        // verify the editor has adjusted the dates to be the same
        assertEquals(fromDate, unit1.getToDate());
    }

    /**
     * Verifies the product is invalid if there are multiple preferred suppliers.
     */
    @Test
    public void testMultiplePreferredSuppliers() {
        Party supplier = supplierFactory.createSupplier();
        Product product = productFactory.createMedication();
        ProductSupplier relationship1 = rules.createProductSupplier(product, supplier);
        ProductSupplier relationship2 = rules.createProductSupplier(product, supplier);
        relationship1.setPreferred(true);
        relationship2.setPreferred(true);
        save(product);

        ProductEditor editor1 = createEditor(product);
        assertFalse(editor1.isValid());

        relationship2.setPreferred(false);
        ProductEditor editor2 = createEditor(product);
        assertTrue(editor2.isValid());
    }

    /**
     * Verifies that if a product has a relationship to an inactive supplier, the supplier relationship preferred flag
     * can be changed and the product saved.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testChangePreferredForInactiveSupplierRelationship() {
        // create 2 supplier relationships to the product, with supplier1 preferred and supplier2 not preferred
        Party supplier1 = supplierFactory.createSupplier();
        Party supplier2 = supplierFactory.createSupplier();
        Product product = productFactory.createMedication();
        ProductSupplier relationship1 = rules.createProductSupplier(product, supplier1);
        ProductSupplier relationship2 = rules.createProductSupplier(product, supplier2);
        relationship1.setPreferred(true);
        relationship2.setPreferred(false);
        save(product);

        // now deactivate supplier1
        supplier1.setActive(false);
        save(supplier1);

        ProductEditor editor = createEditor(product);
        editor.getComponent();
        assertTrue(editor.isValid());

        // verify both supplier relationships can be edited
        EditableIMObjectCollectionEditor suppliers = editor.getSuppliersEditor();
        IMObjectEditor rel1editor = suppliers.getEditor(relationship1.getRelationship());
        IMObjectEditor rel2editor = suppliers.getEditor(relationship2.getRelationship());
        assertNotNull(rel1editor);
        assertNotNull(rel2editor);

        // need to select the supplier2 relationship, in order to toggle preferred
        IMTable<RelationshipState> table = (IMTable<RelationshipState>) EchoTestHelper.findComponent(
                suppliers.getComponent(), IMTable.class);
        assertEquals(1, table.getObjects().size()); // should only be 1 relationship visible, as supplier1 inactive
        table.setSelected(table.getObjects().get(0));
        table.processInput(Table.INPUT_ACTION, null);  // simulate a click - this will edit the relationship

        // verify supplier1 is preferred and supplier2 not preferred
        assertTrue(rel1editor.getProperty("preferred").getBoolean());
        assertFalse(rel2editor.getProperty("preferred").getBoolean());

        // now make supplier2 preferred, and verify supplier1 no longer preferred
        rel2editor.getProperty("preferred").setValue(true);

        assertFalse(rel1editor.getProperty("preferred").getBoolean());
        assertTrue(rel2editor.getProperty("preferred").getBoolean());
        assertTrue(SaveHelper.save(editor));

        // verify the changes were saved
        product = get(product);
        List<ProductSupplier> ps1 = rules.getProductSuppliers(product, supplier1);
        assertEquals(1, ps1.size());
        assertFalse(ps1.get(0).isPreferred());

        List<ProductSupplier> ps2 = rules.getProductSuppliers(product, supplier2);
        assertEquals(1, ps2.size());
        assertTrue(ps2.get(0).isPreferred());
    }

    /**
     * Verifies that when a product - stock location relationship specifies a supplier, the supplier must be
     * listed in the product - supplier relationships.
     */
    @Test
    public void testStockLocationSupplierDependentOnSuppliers() {
        Party supplier1 = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        Product product = productFactory.newMedication()
                .newProductStockLocation()
                .supplier(supplier1)
                .stockLocation(stockLocation)
                .add().build();

        ProductEditor editor = createEditor(product);
        String message = stockLocation.getName() + " specifies " + supplier1.getName()
                          + " as the preferred supplier, but " + supplier1.getName() + " is not a supplier of "
                          + product.getName();
        assertInvalid(editor, message);

        EditableIMObjectCollectionEditor suppliers = editor.getSuppliersEditor();
        ProductSupplier relationship1 = rules.createProductSupplier(product, supplier1);
        suppliers.add(relationship1.getRelationship());

        assertValid(editor);

        suppliers.remove(relationship1.getRelationship());
        assertInvalid(editor, message);
    }

    /**
     * Verifies that the correct layout strategy is used for each product.
     */
    @Test
    public void testLayoutStrategy() {
        checkLayoutStrategy(productFactory.createMedication(), MedicationLayoutStrategy.class);
        checkLayoutStrategy(productFactory.createMerchandise(), ProductLayoutStrategy.class);
        checkLayoutStrategy(productFactory.createService(), ProductLayoutStrategy.class);
        checkLayoutStrategy(productFactory.createTemplate(), TemplateLayoutStrategy.class);
        checkLayoutStrategy(ProductTestHelper.createPriceTemplate(), ProductLayoutStrategy.class);
    }

    /**
     * Verifies that administrators have permission to delete prices with a from-date less than today.
     * <p/>
     * This is based on the delete button being enabled.
     */
    @Test
    public void testDeletionPriceWithFromDateLessThanTodayDeletion() {
        User user = userFactory.createUser();
        User admin = userFactory.createAdministrator();
        Product product1 = productFactory.newMedication().build(false);
        ProductPrice price1 = ProductPriceTestHelper.createUnitPrice(DateRules.getToday(), null);
        ProductPrice price2 = ProductPriceTestHelper.createUnitPrice(DateRules.getTomorrow(), null);
        ProductPrice price3 = ProductPriceTestHelper.createUnitPrice(DateRules.getYesterday(), null);
        ProductPrice price4 = ProductPriceTestHelper.createUnitPrice((Date) null, null);
        product1.addProductPrice(price1);
        product1.addProductPrice(price2);
        product1.addProductPrice(price3);
        product1.addProductPrice(price4);

        TestProductEditor editor1 = createEditor(product1, user);
        checkDeleteEnabled(editor1, price1, true);
        checkDeleteEnabled(editor1, price2, true);
        checkDeleteEnabled(editor1, price3, true);
        checkDeleteEnabled(editor1, price4, true);

        TestProductEditor editor2 = createEditor(product1, admin);
        checkDeleteEnabled(editor2, price1, true);
        checkDeleteEnabled(editor2, price2, true);
        checkDeleteEnabled(editor2, price3, true);
        checkDeleteEnabled(editor2, price4, true);

        save(product1);

        TestProductEditor editor3 = createEditor(product1, user);
        checkDeleteEnabled(editor3, price1, true);
        checkDeleteEnabled(editor3, price2, true);
        checkDeleteEnabled(editor3, price3, false);
        checkDeleteEnabled(editor3, price4, false);

        TestProductEditor editor4 = createEditor(product1, admin);
        checkDeleteEnabled(editor4, price1, true);
        checkDeleteEnabled(editor4, price2, true);
        checkDeleteEnabled(editor4, price3, true);
        checkDeleteEnabled(editor4, price4, true);

        Product product2 = productFactory.newMedication().build(false);
        ProductPrice price5 = ProductPriceTestHelper.createUnitPrice(DateRules.getYesterday(), null);
        product2.addProductPrice(price5);

        // verify the user can still delete a back-dated price in the current editor
        TestProductEditor editor5 = createEditor(product2, user);
        checkDeleteEnabled(editor5, price5, true);
        assertTrue(SaveHelper.save(editor5));

        checkDeleteEnabled(editor5, price5, true);
    }

    /**
     * Verifies that macros aren't expanded in the dispensing instructions field.
     * <p/>
     * Expansion of these macros need to occur when the medication is dispensed.
     */
    @Test
    public void testMacroNotExpandedInDispensingInstructions() {
        Product product = productFactory.newMedication().build(false);
        TestProductEditor editor = createEditor(product);
        Lookup macro = lookupFactory.newMacro().uniqueCode("@test").expression("'test macro'").build();
        editor.getProperty("dispInstructions").setValue(macro.getCode());   // should expand
        editor.getProperty("usageNotes").setValue(macro.getCode());         // shouldn't expand
        assertTrue(SaveHelper.save(editor));
        product = get(product);
        IMObjectBean bean = getBean(product);
        assertEquals(macro.getCode(), bean.getString("dispInstructions"));   // verify the macro didn't expand
        assertEquals("test macro", bean.getString("usageNotes"));            // verify the macro expanded
    }

    /**
     * Creates a new product editor.
     *
     * @param product the product to edit
     * @return a new editor
     */
    protected TestProductEditor createEditor(Product product) {
        return createEditor(product, null);
    }

    /**
     * Creates a new product editor.
     *
     * @param product the product to edit
     * @param user    the user. May be {@code null}
     * @return a new editor
     */
    protected TestProductEditor createEditor(Product product, User user) {
        LocalContext local = new LocalContext();
        local.setUser(user);
        DefaultLayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        TestProductEditor editor = new TestProductEditor(product, null, context);
        editor.getComponent();
        return editor;
    }

    /**
     * Determines if the delete button is enabled when a price is selected, for the supplied user.
     *
     * @param editor  the editor
     * @param price   the price
     * @param enabled if {@code true} the delete button should be enabled, {@code false} if it should be disabled
     */
    private void checkDeleteEnabled(TestProductEditor editor, ProductPrice price, boolean enabled) {
        ProductPriceCollectionEditor prices = editor.getPricesEditor();
        prices.selectAndEdit(price);
        assertNotNull(prices.getCurrentEditor());
        assertEquals(price, prices.getCurrentEditor().getObject());
        assertEquals(enabled, prices.getButtons().isEnabled(IMObjectTableCollectionEditor.DELETE_ID));
    }

    /**
     * Verifies a product contains the expected price.
     *
     * @param product the product
     * @param price   the expected price
     * @param cost    the expected cost price
     * @param from    the expected from date
     * @param to      the expected to date
     * @param notes   the expected notes
     */
    private void checkPrice(Product product, BigDecimal price, BigDecimal cost, Date from, Date to, String notes) {
        ProductPrice productPrice = ProductPriceTestHelper.checkPrice(product.getProductPrices(), price, cost,
                                                                      ONE_HUNDRED, ONE_HUNDRED, from, to, null);
        IMObjectBean bean = getBean(productPrice);
        assertEquals(notes, bean.getString("notes"));
    }

    /**
     * Verifies that the create layout strategy is used for a product.
     *
     * @param product  the product
     * @param expected the expected layout stragegy
     */
    private void checkLayoutStrategy(Product product, Class<? extends AbstractLayoutStrategy> expected) {
        TestProductEditor editor = createEditor(product);
        assertNotNull(editor.getView().getLayout());
        assertEquals(expected, editor.getView().getLayout().getClass());
    }

    /**
     * Adds an <em>entityLink.productSupplier</em> relationship set to auto-update prices.
     *
     * @param editor          the product editor
     * @param supplier        the supplier
     * @param listPrice       the list price
     * @param autoPriceUpdate if {@code true}, automatically calculate unit prices
     * @return the relationship editor
     */
    private EntityLinkEditor addSupplier(ProductEditor editor, Party supplier, BigDecimal listPrice,
                                         boolean autoPriceUpdate) {
        EditableIMObjectCollectionEditor suppliers = editor.getSuppliersEditor();
        EntityLinkEditor relationshipEditor = (EntityLinkEditor) suppliers.getFirstEditor(true);
        assertNotNull(relationshipEditor);
        relationshipEditor.getComponent();
        relationshipEditor.setTarget(supplier);
        setPackageSize(relationshipEditor, ONE);
        setListPrice(relationshipEditor, listPrice);
        setAutoPriceUpdate(relationshipEditor, autoPriceUpdate);
        return relationshipEditor;
    }

    private EntityLinkEditor getSupplierEditor(ProductEditor editor, Party supplier) {
        EntityLinkEditor relationshipEditor = null;
        EditableIMObjectCollectionEditor suppliers = editor.getSuppliersEditor();
        for (IMObject object : suppliers.getCurrentObjects()) {
            EntityLink link = (EntityLink) object;
            if (Objects.equals(supplier.getObjectReference(), link.getTarget())) {
                relationshipEditor = (EntityLinkEditor) suppliers.getEditor(object);
                break;
            }
        }
        assertNotNull(relationshipEditor);
        return relationshipEditor;
    }

    private void setAutoPriceUpdate(EntityLinkEditor relationshipEditor, boolean autoPriceUpdate) {
        relationshipEditor.getProperty("autoPriceUpdate").setValue(autoPriceUpdate);
    }

    private void setPackageSize(EntityLinkEditor relationshipEditor, BigDecimal size) {
        relationshipEditor.getProperty("packageSize").setValue(size);
    }

    /**
     * Sets the list price in a product-supplier relationship.
     *
     * @param editor    the relationship editor
     * @param listPrice the list price
     */
    private void setListPrice(EntityLinkEditor editor, BigDecimal listPrice) {
        editor.getProperty("listPrice").setValue(listPrice);
    }

    /**
     * Product editor that exposes the view.
     */
    private static class TestProductEditor extends ProductEditor {

        /**
         * Constructs a {@link TestProductEditor}.
         *
         * @param object        the object to edit
         * @param parent        the parent object. May be {@code null}
         * @param layoutContext the layout context. May be {@code null}.
         */
        public TestProductEditor(Product object, IMObject parent, LayoutContext layoutContext) {
            super(object, parent, layoutContext);
        }

        /**
         * Returns the view, creating it if it doesn't exist.
         *
         * @return the view
         */
        @Override
        public IMObjectView getView() {
            return super.getView();
        }
    }
}
