/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.act;

import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ActHelper} class.
 *
 * @author Tim Anderson
 */
public class ActHelperTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link ActHelper#getTargetActs(Collection)} method.
     */
    @Test
    public void testGetTargetActs() {
        Party patient = TestHelper.createPatient();
        Act event = createAct(PatientArchetypes.CLINICAL_EVENT, patient);
        Act note = createAct(PatientArchetypes.CLINICAL_NOTE, patient);
        Act problem = createProblem(patient);
        Act weight = createAct(PatientArchetypes.PATIENT_WEIGHT, patient);
        DocumentAct form = PatientTestHelper.createDocumentForm(patient);

        List<Relationship> relationships = new ArrayList<>();
        IMObjectBean bean = getBean(event);
        relationships.add(bean.addTarget("items", note, "event"));
        relationships.add(bean.addTarget("items", problem, "events"));
        relationships.add(bean.addTarget("items", weight, "event"));
        relationships.add(bean.addTarget("items", form, "event"));
        save(event, note, problem, weight, form);

        List<Act> targetActs = ActHelper.getTargetActs(relationships);
        assertEquals(4, targetActs.size());
        assertTrue(targetActs.contains(note));
        assertTrue(targetActs.contains(problem));
        assertTrue(targetActs.contains(weight));
        assertTrue(targetActs.contains(form));
    }

    /**
     * Tests the {@link ActHelper#getActs(Collection) method} where the acts have different types.
     */
    @Test
    public void testGetActs() {
        Party patient = TestHelper.createPatient();
        Act note = createAct(PatientArchetypes.CLINICAL_NOTE, patient);
        Act problem = createProblem(patient);
        Act weight = createAct(PatientArchetypes.PATIENT_WEIGHT, patient);
        FinancialAct invoiceItem = FinancialTestHelper.createInvoiceItem(
                new Date(), patient, null, TestHelper.createProduct(), BigDecimal.ONE, BigDecimal.ZERO,
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
        DocumentAct form = PatientTestHelper.createDocumentForm(patient);
        save(note, problem, weight, invoiceItem, form);

        List<Reference> refs = new ArrayList<>();
        refs.add(note.getObjectReference());
        refs.add(problem.getObjectReference());
        refs.add(weight.getObjectReference());
        refs.add(invoiceItem.getObjectReference());
        refs.add(form.getObjectReference());

        List<Act> acts = ActHelper.getActs(refs);
        assertEquals(5, acts.size());
        assertTrue(acts.contains(note));
        assertTrue(acts.contains(problem));
        assertTrue(acts.contains(weight));
        assertTrue(acts.contains(invoiceItem));
        assertTrue(acts.contains(form));
    }

    /**
     * Helper to create a new <em>act.patientClinicalProblem</em>.
     *
     * @param patient the patient
     * @return a new problem act
     */
    private Act createProblem(Party patient) {
        Act act = createAct(PatientArchetypes.CLINICAL_PROBLEM, patient);
        Lookup lookup = TestHelper.getLookup("lookup.diagnosis", "HEART_MURMUR");
        act.setReason(lookup.getCode());
        return act;
    }

    /**
     * Helper to create an act linked to a patient.
     *
     * @param shortName the archetype short name
     * @param patient   the patient
     * @return a new act
     */
    private Act createAct(String shortName, Party patient) {
        Act result = create(shortName, Act.class);
        IMObjectBean bean = getBean(result);
        bean.setTarget("patient", patient);
        return result;
    }
}
