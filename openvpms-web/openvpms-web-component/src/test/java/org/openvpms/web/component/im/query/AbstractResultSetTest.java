/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.query;

import org.openvpms.archetype.rules.act.EstimateActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Base class for {@link ResultSet} test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractResultSetTest extends ArchetypeServiceTest {

    /**
     * Test customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * Helper to create a new <code>act.customerEstimation</code>, and save it.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param product  the product
     * @return a new act
     */
    protected Act createEstimate(Party customer, Party patient, Product product) {
        return accountFactory.newEstimate()
                .customer(customer)
                .status(EstimateActStatus.IN_PROGRESS)
                .item().patient(patient).product(product).add()
                .build();
    }

    /**
     * Helper to create a new act.
     *
     * @param shortName the act short name
     * @return a new act
     */
    protected Act createAct(String shortName) {
        return create(shortName, Act.class);
    }

}
