/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.app;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.deposit.DepositArchetypes;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.stock.StockArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;

import java.util.function.BiConsumer;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link DelegatingContext}.
 *
 * @author Tim Anderson
 */
public class DelegatingContextTestCase extends ArchetypeServiceTest {

    /**
     * Tests accessors.
     */
    @Test
    public void testAccessors() {
        check(Context::getAppointment, Context::setAppointment, ScheduleArchetypes.APPOINTMENT, Act.class);
        check(Context::getClinician, Context::setClinician, UserArchetypes.USER, User.class);
        check(Context::getCustomer, Context::setCustomer, CustomerArchetypes.PERSON, Party.class);
        check(Context::getCurrent, Context::setCurrent, create(CustomerArchetypes.PERSON),
              create(CustomerArchetypes.PERSON));
        check(Context::getDepartment, Context::setDepartment, PracticeArchetypes.DEPARTMENT, Entity.class);
        check(Context::getDeposit, Context::setDeposit, DepositArchetypes.DEPOSIT_ACCOUNT, Party.class);
        check(Context::getLocation, Context::setLocation, PracticeArchetypes.LOCATION, Party.class);
        check(Context::getPatient, Context::setPatient, PatientArchetypes.PATIENT, Party.class);
        check(Context::getPractice, Context::setPractice, PracticeArchetypes.PRACTICE, Party.class);
        check(Context::getProduct, Context::setProduct, ProductArchetypes.MEDICATION, Product.class);
        check(Context::getSchedule, Context::setSchedule, ScheduleArchetypes.ORGANISATION_SCHEDULE, Party.class);
        check(Context::getScheduleDate, Context::setScheduleDate, DateRules.getToday(), DateRules.getTomorrow());
        check(Context::getScheduleView, Context::setScheduleView, ScheduleArchetypes.SCHEDULE_VIEW, Entity.class);
        check(Context::getStockLocation, Context::setStockLocation, StockArchetypes.STOCK_LOCATION, Party.class);
        check(Context::getSupplier, Context::setSupplier, SupplierArchetypes.SUPPLIER_PERSON, Party.class);
        check(Context::getTask, Context::setTask, ScheduleArchetypes.TASK, Act.class);
        check(Context::getTerminal, Context::setTerminal, "entity.EFTPOSTerminalTest", Entity.class);
        check(Context::getTill, Context::setTill, TillArchetypes.TILL, Entity.class);
        check(Context::getUser, Context::setUser, UserArchetypes.USER, User.class);
        check(Context::getWorkList, Context::setWorkList, ScheduleArchetypes.ORGANISATION_WORKLIST, Entity.class);
        check(Context::getWorkListDate, Context::setWorkListDate, DateRules.getToday(), DateRules.getTomorrow());
        check(Context::getWorkListView, Context::setWorkListView, ScheduleArchetypes.WORK_LIST_VIEW, Entity.class);
    }

    /**
     * Tests the {@link DelegatingContext#addObject(IMObject)} and {@link DelegatingContext#removeObject(IMObject)}
     * methods.
     */
    @Test
    public void testAddRemoveObject() {
        LocalContext fallback = new LocalContext();
        DelegatingContext context = new DelegatingContext(new LocalContext(), fallback) {
        };

        Product product1 = create(ProductArchetypes.MEDICATION, Product.class);
        Product product2 = create(ProductArchetypes.MERCHANDISE, Product.class);
        context.addObject(product1);
        fallback.addObject(product2);

        assertEquals(product1, context.getProduct());
        context.removeObject(product1);
        assertEquals(product2, context.getProduct());
        fallback.removeObject(product2);

        assertNull(context.getProduct());
    }

    /**
     * Checks an accessor that accepts IMObject instances.
     *
     * @param getter    the getter
     * @param setter    the setter
     * @param archetype the archetype
     * @param type      the IMObject type
     */
    private <T extends IMObject> void check(Function<Context, T> getter, BiConsumer<Context, T> setter,
                                            String archetype, Class<T> type) {
        T object1 = create(archetype, type);
        T object2 = create(archetype, type);
        check(getter, setter, object1, object2);
    }

    /**
     * Checks an accessor.
     *
     * @param getter  the getter
     * @param setter  the setter
     * @param object1 the first test object
     * @param object2 the second test object
     */
    private <T> void check(Function<Context, T> getter, BiConsumer<Context, T> setter, T object1, T object2) {
        LocalContext fallback = new LocalContext();
        DelegatingContext context = new DelegatingContext(new LocalContext(), fallback) {
        };

        assertNull(getter.apply(context));

        setter.accept(context, object1);
        assertEquals(object1, getter.apply(context));

        setter.accept(fallback, object2);
        assertEquals(object2, getter.apply(fallback));
        assertEquals(object1, getter.apply(context));

        setter.accept(context, null);
        assertEquals(object2, getter.apply(context));
    }

}
