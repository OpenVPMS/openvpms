/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.error;

import org.hibernate.StaleObjectStateException;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.ValidationError;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.security.ArchetypeAccessDeniedException;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.report.openoffice.OpenOfficeConnectionException;
import org.openvpms.report.openoffice.OpenOfficeDocumentException;
import org.openvpms.report.openoffice.OpenOfficeException;
import org.springframework.transaction.UnexpectedRollbackException;

import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ErrorFormatter}.
 *
 * @author Tim Anderson
 */
public class ErrorFormatterTestCase extends ArchetypeServiceTest {

    /**
     * Test OpenOfficeConnectionException.
     */
    private final OpenOfficeConnectionException openOfficeConnectionException
            = new OpenOfficeConnectionException(ReportMessages.failedToConnectToOpenOffice("foo"),
                                                new IOException("foo"));

    /**
     * Test ArchetypeServiceException. This is not explicitly configured, but the StaleObjectStateException is.
     */
    private final ArchetypeServiceException archetypeServiceException = new ArchetypeServiceException(
            ArchetypeServiceException.ErrorCode.FailedToSaveObject, new StaleObjectStateException("foo", 12));

    /**
     * Tests the {@link ErrorFormatter#format(Throwable)} method.
     */
    @Test
    public void testFormatException() {
        // test exceptions where there is a configured plain text message
        assertEquals("An internal application error has occurred.", ErrorFormatter.format(new NullPointerException()));
        assertEquals("There is not enough memory to perform this operation.\n\n"
                     + "Contact your administrator if the problem persists.",
                     ErrorFormatter.format(new OutOfMemoryError()));

        // test exceptions where the root cause is configured
        assertEquals("Failed to save object. It may have been changed by another user.",
                     ErrorFormatter.format(archetypeServiceException));

        // test exceptions where there is only an HTML message configured
        assertEquals("REPORT-0080: Failed to connect to OpenOffice: foo",
                     ErrorFormatter.format(openOfficeConnectionException));

        // test exceptions where there is no configured message
        assertEquals("foo", ErrorFormatter.format(new IOException("foo")));
    }

    /**
     * Tests the {@link ErrorFormatter#formatHTML(Throwable)} method.
     */
    @Test
    public void testFormatHTMLException() {
        // test exceptions where there is a configured plain text message
        assertEquals("An internal application error has occurred.", ErrorFormatter.format(new NullPointerException()));

        // test exceptions where the root cause is configured
        assertEquals("Failed to save object. It may have been changed by another user.",
                     ErrorFormatter.formatHTML(archetypeServiceException));

        // test exceptions where an HTML message configured
        assertEquals("REPORT-0080: Failed to connect to OpenOffice: foo.<br/><br/>Please refer to " +
                     "<a href=\"https://openvpms.org/documentation/csh/2.4/topics/troubleshooting#oofailedtoconnect\">"
                     + "the documentation</a> to troubleshoot this issue.",
                     ErrorFormatter.formatHTML(openOfficeConnectionException));

        // test exceptions where there is no configured message
        assertEquals("foo", ErrorFormatter.formatHTML(new IOException("foo")));
    }

    /**
     * Tests the {@link ErrorFormatter#format(Throwable, String)} method.
     */
    @Test
    public void testFormatExceptionWithDisplayName() {
        // test exceptions where there is a configured plain text message
        assertEquals("An internal application error has occurred.",
                     ErrorFormatter.format(new NullPointerException(), "Payment"));

        // test exceptions where the root cause is configured
        assertEquals("The Payment could not be saved. It may have been changed by another user.",
                     ErrorFormatter.format(archetypeServiceException, "Payment"));

        // test exceptions where there is only an HTML message configured
        assertEquals("REPORT-0080: Failed to connect to OpenOffice: foo",
                     ErrorFormatter.format(openOfficeConnectionException, "Payment"));

        // test exceptions where there is no configured message
        assertEquals("foo", ErrorFormatter.format(new IOException("foo"), "Payment"));
    }

    /**
     * Verifies plain text is escaped before being included in HTML messages.
     */
    @Test
    public void testEscapePlainTextForHTMLMessages() {
        ValidationError error = new ValidationError(new IMObjectReference(PatientArchetypes.PATIENT, 10), "name",
                                                    "Fi<do>");
        ValidationException exception = new ValidationException(
                Collections.singletonList(error), ValidationException.ErrorCode.FailedToValidObjectAgainstArchetype);
        assertEquals("Failed to validate Name of Patient (Pet): Fi&lt;do&gt;", ErrorFormatter.formatHTML(exception));
        assertEquals("Failed to validate Name of Patient (Pet): Fi&lt;do&gt;",
                     ErrorFormatter.formatHTML(exception, "Patient"));

        assertEquals("REPORT-0080: Failed to connect to OpenOffice: &lt;some error&gt;.<br/><br/>Please refer to " +
                     "<a href=\"https://openvpms.org/documentation/csh/2.4/topics/troubleshooting#oofailedtoconnect\">"
                     + "the documentation</a> to troubleshoot this issue.",
                     ErrorFormatter.formatHTML(new OpenOfficeConnectionException(
                             ReportMessages.failedToConnectToOpenOffice("<some error>"))));
    }


    /**
     * Tests formatting of {@link ArchetypeAccessDeniedException}.
     */
    @Test
    public void testFormatAccessDeniedException() {
        ArchetypeAccessDeniedException accessDeniedException = new ArchetypeAccessDeniedException("foo", "save");
        String expected = "You do not have permission to save instances of foo";
        assertEquals(expected, ErrorFormatter.format(accessDeniedException));
        assertEquals(expected, ErrorFormatter.formatHTML(accessDeniedException));
        assertEquals(expected, ErrorFormatter.format(accessDeniedException, "Payment"));
        assertEquals(expected, ErrorFormatter.formatHTML(accessDeniedException, "Payment"));
    }

    /**
     * Tests formatting of {@link NullPointerException} for each of the {@link ErrorFormatter} methods.
     */
    @Test
    public void testNullPointerException() {
        NullPointerException exception = new NullPointerException();
        String expected = "An internal application error has occurred.";
        assertEquals(expected, ErrorFormatter.format(exception));
        assertEquals(expected, ErrorFormatter.formatHTML(exception));
        assertEquals(expected, ErrorFormatter.format(exception, "Payment"));
        assertEquals(expected, ErrorFormatter.formatHTML(exception, "Payment"));
        assertEquals(expected, ErrorFormatter.format(exception, ErrorFormatter.Category.DEFAULT, "Payment"));
        assertEquals(expected, ErrorFormatter.format(exception, ErrorFormatter.Category.DELETE, "Payment"));
    }

    /**
     * Tests formatting of {@link StaleObjectStateException} for each of the {@link ErrorFormatter} methods.
     */
    @Test
    public void testFormatStaleObjectStateException() {
        StaleObjectStateException exception = new StaleObjectStateException("foo", 12);

        String expected1 = "Failed to save object. It may have been changed by another user.";
        assertEquals(expected1, ErrorFormatter.format(exception));
        assertEquals(expected1, ErrorFormatter.formatHTML(exception));

        String expected2 = "The Payment could not be saved. It may have been changed by another user.";
        assertEquals(expected2, ErrorFormatter.format(exception, "Payment"));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, "Payment"));
        assertEquals(expected2, ErrorFormatter.format(exception, ErrorFormatter.Category.DEFAULT, "Payment"));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, ErrorFormatter.Category.DEFAULT, "Payment"));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, ErrorFormatter.Category.DEFAULT, "Payment"));

        String expected3 = "The Payment could not be deleted. It may have been changed by another user.";
        assertEquals(expected3, ErrorFormatter.format(exception, ErrorFormatter.Category.DELETE, "Payment"));
        assertEquals(expected3, ErrorFormatter.formatHTML(exception, ErrorFormatter.Category.DELETE, "Payment"));
    }

    /**
     * Tests formatting of {@link UnexpectedRollbackException} for each of the {@link ErrorFormatter} methods.
     */
    @Test
    public void testFormatUnexpectedRollbackException() {
        UnexpectedRollbackException exception = new UnexpectedRollbackException("foo");
        String expected1 = "Failed to save object. It may have been changed by another user";
        assertEquals(expected1, ErrorFormatter.format(exception));
        assertEquals(expected1, ErrorFormatter.formatHTML(exception));

        String expected2 = "The Payment could not be saved. It may have been changed by another user.";
        assertEquals(expected2, ErrorFormatter.format(exception, "Payment"));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, "Payment"));
        assertEquals(expected2, ErrorFormatter.format(exception, ErrorFormatter.Category.DEFAULT, "Payment"));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, ErrorFormatter.Category.DEFAULT, "Payment"));

        String expected3 = "The Payment could not be deleted. It may have been changed by another user.";
        assertEquals(expected3, ErrorFormatter.format(exception, ErrorFormatter.Category.DELETE, "Payment"));
        assertEquals(expected3, ErrorFormatter.formatHTML(exception, ErrorFormatter.Category.DELETE, "Payment"));
    }

    /**
     * Tests formatting of {@link ValidationException}.
     */
    @Test
    public void testFormatValidationException() {
        ValidationError error = new ValidationError(new IMObjectReference(PatientArchetypes.PATIENT, 10), "species",
                                                    "foo");
        ValidationException exception = new ValidationException(
                Collections.singletonList(error), ValidationException.ErrorCode.FailedToValidObjectAgainstArchetype);

        String expected = "Failed to validate Species of Patient (Pet): foo";
        assertEquals(expected, ErrorFormatter.format(exception));
        assertEquals(expected, ErrorFormatter.formatHTML(exception));
        assertEquals(expected, ErrorFormatter.format(exception, "Bar"));
        assertEquals(expected, ErrorFormatter.formatHTML(exception, "Bar"));
    }

    /**
     * Tests formatting of {@link OpenOfficeException}.
     * <p/>
     * The message resource includes the exception message.
     */
    @Test
    public void testFormatOpenOfficeException() {
        OpenOfficeException exception = new OpenOfficeException(
                ReportMessages.failedToPrintOpenOfficeDocument("foo"));

        String expected = "REPORT-0065: Failed to print 'foo'\n\nContact your administrator if the problem persists.";
        assertEquals(expected, ErrorFormatter.format(exception));
        assertEquals(expected, ErrorFormatter.formatHTML(exception));
        assertEquals(expected, ErrorFormatter.format(exception, "Payment"));
        assertEquals(expected, ErrorFormatter.formatHTML(exception, "Payment"));
    }

    /**
     * Tests formatting of {@link OpenOfficeConnectionException}.
     * <p/>
     * This only has an HTML message configured. For plain text, it should return the exception message.
     */
    public void testFormatOpenOfficeConnectionException() {
        String expected1 = "Failed to connect to OpenOffice: bar";
        assertEquals(expected1, ErrorFormatter.format(openOfficeConnectionException));
        assertEquals(expected1, ErrorFormatter.format(openOfficeConnectionException, "Payment"));

        String expected2 =
                "Failed to connect to OpenOffice: bar.<br/><br/>Please refer to " +
                "<a href=\"https://openvpms.org/documentation/csh/2.2/topics/troubleshooting#oofailedtoconnect\">"
                + "the documentation</a> to troubleshoot this issue.";
        assertEquals(expected2, ErrorFormatter.formatHTML(openOfficeConnectionException));
        assertEquals(expected2, ErrorFormatter.formatHTML(openOfficeConnectionException, "Payment"));
    }

    /**
     * Tests formatting of {@link OpenOfficeDocumentException}.
     */
    @Test
    public void testFormatOpenOfficeDocumentException() {
        OpenOfficeDocumentException exception = new OpenOfficeDocumentException(
                ReportMessages.failedToLoadOpenOfficeDocument("foo"));
        String expected1 = "REPORT-0060: Failed to load 'foo' into OpenOffice";
        assertEquals(expected1, ErrorFormatter.format(exception));
        assertEquals(expected1, ErrorFormatter.format(exception, "Payment"));

        String expected2 =
                "REPORT-0060: Failed to load 'foo' into OpenOffice.<br/><br/>Please refer to " +
                "<a href=\"https://openvpms.org/documentation/csh/2.4/topics/troubleshooting#oodocumentexception\">" +
                "the documentation</a> to troubleshoot this issue.";
        assertEquals(expected2, ErrorFormatter.formatHTML(exception));
        assertEquals(expected2, ErrorFormatter.formatHTML(exception, "Payment"));
    }

}