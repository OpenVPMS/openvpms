/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.junit.Test;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Contact;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link FromAddressFormatter} class.
 *
 * @author Tim Anderson
 */
public class FromAddressFormatterTestCase extends AbstractAddressFormatterTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link FromAddressFormatter#format(Contact)} method.
     */
    @Override
    @Test
    public void testFormat() {
        AddressFormatter formatter = new FromAddressFormatter();
        Contact email = contactFactory.createEmail("main@clinic.com");
        practiceFactory.newLocation()
                .name("Main Clinic")
                .addContact(email)
                .build(false);

        assertEquals("Main Clinic <main@clinic.com>", formatter.format(email));

        email.setName("Main Clinic Accounts");

        assertEquals("Main Clinic Accounts (Main Clinic) <main@clinic.com>", formatter.format(email));
    }

    /**
     * Creates a new address formatter.
     *
     * @return a new address formatter
     */
    @Override
    protected AddressFormatter createAddressFormatter() {
        return new FromAddressFormatter();
    }
}
