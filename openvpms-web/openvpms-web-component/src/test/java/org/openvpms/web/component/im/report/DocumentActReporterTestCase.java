/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ReportFactory;
import org.openvpms.report.StaticContentIMReport;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentActReporter} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DocumentActReporterTestCase extends ArchetypeServiceTest {

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The report factory.
     */
    @Autowired
    private ReportFactory reportFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;


    /**
     * Verifies that static content can be used to generate a document.
     */
    @Test
    public void testStaticContent() throws Exception {
        byte[] content = RandomUtils.nextBytes(100);
        Document pdf = documentFactory.newDocument()
                .name("test.pdf")
                .mimeType(DocFormats.PDF_TYPE)
                .content(content)
                .build();
        Entity template = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(pdf)
                .build();
        Party patient = patientFactory.createPatient();
        DocumentAct form = patientFactory.createForm(patient, template);

        IArchetypeService service = getArchetypeService();
        LookupService lookups = getLookupService();
        FileNameFormatter formatter = new FileNameFormatter(service, lookups, patientRules);
        DocumentActReporter reporter = new DocumentActReporter(form, formatter, service, lookups, reportFactory);
        assertTrue(reporter.getReport() instanceof StaticContentIMReport);
        Document document = reporter.getDocument();

        assertEquals(template.getName() + ".pdf", document.getName());
        assertEquals(DocFormats.PDF_TYPE, document.getMimeType());

        // must be a different document
        assertNotEquals(pdf.getId(), document.getId());

        // verify the content matches that of the original document
        ByteArrayOutputStream actual = new ByteArrayOutputStream();
        IOUtils.copy(handlers.get(document).getContent(document), actual);
        assertArrayEquals(content, actual.toByteArray());
    }
}
