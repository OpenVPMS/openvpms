/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjects;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.edit.AbstractCollectionPropertyEditorTest;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.property.CollectionProperty;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link EntityRelationshipCollectionPropertyEditor} class.
 *
 * @author Tim Anderson
 */
public class EntityRelationshipCollectionPropertyEditorTestCase extends AbstractCollectionPropertyEditorTest {

    /**
     * Verifies that the relationship state matches that expected.
     */
    @Test
    public void testRelationshipState() {
        Party customer = createParent();
        CollectionProperty property = getCollectionProperty(customer);
        EntityRelationshipCollectionPropertyEditor editor = new EntityRelationshipCollectionPropertyEditor(property,
                                                                                                           customer);
        assertEquals(0, editor.getRelationships().size());

        Party patient1 = TestHelper.createPatient("Fido", true);
        Party patient2 = TestHelper.createPatient("Spot", true);

        EntityRelationship relationship1 = createOwnerRelationship(customer, patient1);
        assertTrue(editor.add(relationship1));
        checkRelationships(editor, relationship1);
        checkState(editor, relationship1, "Fido");

        EntityRelationship relationship2 = createOwnerRelationship(customer, patient2);
        assertTrue(editor.add(relationship2));
        checkRelationships(editor, relationship1, relationship2);
        checkState(editor, relationship2, "Spot");

        editor.remove(relationship2);
        checkRelationships(editor, relationship1);

        relationship1.setTarget(patient2.getObjectReference());
        assertFalse(editor.add(relationship1));
        checkRelationships(editor, relationship1);
        checkState(editor, relationship1, "Spot");
    }

    /**
     * Returns the parent of the collection.
     *
     * @return the parent object
     */
    @Override
    protected Party createParent() {
        return TestHelper.createCustomer(true);
    }

    /**
     * Returns the name of the collection node.
     *
     * @return the node name
     */
    protected String getCollectionNode() {
        return "patients";
    }

    /**
     * Returns an editor for a collection property.
     *
     * @param property the collection property
     * @param parent   the parent of the collection
     * @param objects  the objects service
     * @return a new editor for the property
     */
    protected CollectionPropertyEditor createEditor(CollectionProperty property, IMObject parent, IMObjects objects) {
        return new EntityRelationshipCollectionPropertyEditor(property, (Entity) parent);
    }

    /**
     * Returns an object to add to the collection.
     *
     * @param parent the parent of the collection
     * @return a new object to add to the collection
     */
    protected IMObject createObject(IMObject parent) {
        return createOwnerRelationship(parent, TestHelper.createPatient(true));
    }

    /**
     * Makes an object valid or invalid.
     *
     * @param object the object
     * @param valid  if {@code true}, make it valid, otherwise make it invalid
     */
    @Override
    protected void makeValid(IMObject object, boolean valid) {
        IMObjectBean bean = getBean(object);
        bean.setValue("activeStartTime", valid ? new Date() : null);
    }

    /**
     * Modify an object so that its version will increment on save.
     *
     * @param object the object to modify
     */
    @Override
    protected void modify(IMObject object) {
        IMObjectBean bean = getBean(object);
        Date date = bean.getDate("activeStartTime", new Date());
        bean.setValue("activeStartTime", DateRules.getDate(date, 1, DateUnits.DAYS));
    }

    /**
     * Verifies relationships and their associated state.
     *
     * @param editor        the editor
     * @param relationships the expected relationships
     */
    private void checkRelationships(EntityRelationshipCollectionPropertyEditor editor,
                                    EntityRelationship... relationships) {
        List<IMObject> objects = editor.getObjects();
        assertEquals(relationships.length, objects.size());
        Collection<RelationshipState> states = editor.getRelationships();
        assertEquals(relationships.length, states.size());

        for (EntityRelationship relationship : relationships) {
            assertTrue(objects.contains(relationship));
            boolean found = false;
            for (RelationshipState state : states) {
                if (state.getRelationship().equals(relationship)) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }
    }

    /**
     * Checks relationship state for a relationship.
     *
     * @param editor       the collection editor
     * @param relationship the relationship
     * @param name         the expected target name
     */
    private void checkState(EntityRelationshipCollectionPropertyEditor editor, EntityRelationship relationship,
                            String name) {
        RelationshipState state = editor.getRelationshipState(relationship);
        assertEquals(state.getSource(), relationship.getSource());
        assertEquals(state.getSourceId(), relationship.getSource().getId());
        assertEquals(state.getTarget(), relationship.getTarget());
        assertEquals(state.getTargetId(), relationship.getTarget().getId());
        assertNotNull(state);
        assertEquals(name, state.getTargetName());
    }

    /**
     * Creates an owner relationship between a customer and patient.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return the relationship
     */
    private EntityRelationship createOwnerRelationship(IMObject customer, IMObject patient) {
        EntityRelationship result = create(PatientArchetypes.PATIENT_OWNER, EntityRelationship.class);
        result.setSource(customer.getObjectReference());
        result.setTarget(patient.getObjectReference());
        return result;
    }
}
