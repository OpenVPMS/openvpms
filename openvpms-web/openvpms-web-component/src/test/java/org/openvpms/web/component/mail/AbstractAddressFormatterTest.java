/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.component.model.party.Contact;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Base class for {@link AddressFormatter} test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAddressFormatterTest extends ArchetypeServiceTest {

    /**
     * The contact factory.
     */
    @Autowired
    protected TestContactFactory contactFactory;

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The address formatter.
     */
    private AddressFormatter formatter;

    /**
     * The test contact.
     */
    private Contact email;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        formatter = createAddressFormatter();
        email = contactFactory.createEmail("foo@bar.com");
        customerFactory.newCustomer()
                .title("MR")
                .name("Foo", "Bar")
                .addContact(email)
                .build(false);
    }

    /**
     * Tests the {@link AddressFormatter#getAddress(Contact)} method.
     */
    @Test
    public void testGetAddress() {
        assertNull(formatter.getAddress(null));
        assertEquals("foo@bar.com", formatter.getAddress(email));
    }

    /**
     * Tests the {@link AddressFormatter#getName(Contact)} method.
     */
    @Test
    public void testGetName() {
        assertEquals("Bar,Foo", formatter.getName(email));

        email.setName("Foo Bar");
        assertEquals("Foo Bar", formatter.getName(email));
    }

    /**
     * Tests the {@link AddressFormatter#getQualifiedName(Contact)} method.
     */
    @Test
    public void testGetQualifiedName() {
        assertEquals("Bar,Foo", formatter.getQualifiedName(email));

        email.setName("Foo Bar");
        assertEquals("Foo Bar (Bar,Foo)", formatter.getQualifiedName(email));
    }

    /**
     * Tests the {@link AddressFormatter#getNameAddress(Contact, boolean)} method.
     */
    @Test
    public void testGetNameAddress() {
        assertEquals("Bar,Foo <foo@bar.com>", formatter.getNameAddress(email, false));
        assertEquals("\"Bar,Foo\" <foo@bar.com>", formatter.getNameAddress(email, true));

        email.setName("Foo Bar");
        assertEquals("Foo Bar <foo@bar.com>", formatter.getNameAddress(email, false));
        assertEquals("\"Foo Bar\" <foo@bar.com>", formatter.getNameAddress(email, true));
    }

    /**
     * Tests the {@link AddressFormatter#format(Contact)} method.
     */
    @Test
    public void testFormat() {
        assertEquals("Bar,Foo <foo@bar.com> - Customer", formatter.format(email));

        email.setName("Foo Bar");
        assertEquals("Foo Bar (Bar,Foo) <foo@bar.com> - Customer", formatter.format(email));
    }

    /**
     * Tests the {@link AddressFormatter#getType(Contact)} method.
     */
    @Test
    public void testGetType() {
        assertEquals("Customer", formatter.getType(email));
    }

    /**
     * Verifies that whitespace in the contact name is trimmed, for OVPMS-3011.
     */
    @Test
    public void testWhitespace() {
        Contact contact = contactFactory.newEmail()
                .name(" Bar ")
                .email("foo@bar.com")
                .build();
        customerFactory.newCustomer()
                .title("MR")
                .addContact(contact)
                .build(false);

        assertEquals("Bar <foo@bar.com>", formatter.getNameAddress(contact, false));
    }

    /**
     * Creates a new address formatter.
     *
     * @return a new address formatter
     */
    protected abstract AddressFormatter createAddressFormatter();

}
