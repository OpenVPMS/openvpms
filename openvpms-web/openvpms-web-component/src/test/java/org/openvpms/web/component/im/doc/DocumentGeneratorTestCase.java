/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.report.DocFormats;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.report.DocumentActReporter;
import org.openvpms.web.component.im.report.DocumentTemplateLocatorFactory;
import org.openvpms.web.component.job.JobManager;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentGenerator}.
 *
 * @author Tim Anderson
 */
public class DocumentGeneratorTestCase extends AbstractAppTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules rules;

    /**
     * The report factory.
     */
    @Autowired
    private ReportFactory reportFactory;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules documentRules;

    /**
     * The document job manager.
     */
    private TestJobManager jobManager;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        jobManager = new TestJobManager();
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        super.tearDown();
        jobManager.destroy();
    }

    /**
     * Verifies that {@link Context} fields are available to documents generated via {@link DocumentGenerator}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testContextFields() throws Exception {
        Party customer = customerFactory.createCustomer("Foo", "Bar");
        Entity template = documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document("/customerformtemplate.jrxml")
                .build();
        DocumentAct act = create(CustomerArchetypes.DOCUMENT_FORM, DocumentAct.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", customer);
        bean.setTarget("documentTemplate", template);

        // set up the Context
        Context context = DocumentTestHelper.createReportContext();

        AtomicReference<Document> ref = new AtomicReference<>();
        AtomicReference<Thread> threadRef = new AtomicReference<>();
        DocumentGenerator.Listener listener = new DocumentGenerator.AbstractListener() {
            @Override
            public void generated(Document document) {
                ref.set(document);
                threadRef.set(Thread.currentThread());
            }
        };
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        FileNameFormatter formatter = new FileNameFormatter(getArchetypeService(), getLookupService(), rules);
        DocumentGenerator generator = new DocumentGenerator(act, factory, context, new HelpContext("foo", null),
                                                            formatter, getArchetypeService(), getLookupService(),
                                                            reportFactory, jobManager, documentRules, listener) {
            @Override
            protected Document generate(DocumentActReporter reporter) {
                // generate the document as a CSV to allow comparison
                return reporter.getDocument(DocFormats.CSV_TYPE, false);
            }
        };

        // generate the document, and convert it to a string
        generator.generate();
        processJobs(2);
        Document document = ref.get();
        assertNotNull(document);
        String result = documentFactory.toString(document).trim();

        // the customer name should be followed by each of the context fields.
        assertEquals("Foo,Bar,Vets R Us,Main Clinic,Main Stock,\"Smith,J\",Fido,Vet Supplies,Acepromazine,"
                     + "Main Deposit,Main Till,Vet,User,Visit,Invoice,Appointment,Task", result);

        // verify the listener was invoked in the UI thread
        assertEquals(Thread.currentThread(), threadRef.get());
    }

    /**
     * Verifies that the outputFormat is used when generating documents.
     */
    @Test
    public void testOutputFormat() {
        checkOutputFormat("ODT", ".odt");
        checkOutputFormat("PDF", ".pdf");
    }

    /**
     * Processes jobs. This is required as the UI threed needs to be invoked after each job has completed to
     * invoke listeners.
     *
     * @param tasks the no. of tasks
     */
    private void processJobs(int tasks) {
        for (int i = 0; i < tasks; ++i) {
            try {
                assertTrue(jobManager.semaphore.tryAcquire(10, TimeUnit.SECONDS)); // wait for the job to run
            } catch (InterruptedException ignore) {
                // do nothing
            }
            processQueuedTasks();  // simulate UI thread
        }
    }

    /**
     * Verifies that the output is used when generating documents.
     *
     * @param outputFormat the output format
     * @param extension    the expected file name extension
     */
    private void checkOutputFormat(String outputFormat, String extension) {
        Party customer = customerFactory.createCustomer("Foo", "Bar");
        Entity template = documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_LETTER)
                .document("/customerlettertemplate.jrxml")
                .outputFormat(outputFormat)
                .build();
        DocumentAct act = create(CustomerArchetypes.DOCUMENT_LETTER, DocumentAct.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("customer", customer);
        bean.setTarget("documentTemplate", template);

        AtomicReference<Document> ref = new AtomicReference<>();
        DocumentGenerator.Listener listener = new DocumentGenerator.AbstractListener() {
            @Override
            public void generated(Document document) {
                ref.set(document);
            }
        };
        FileNameFormatter formatter = new FileNameFormatter(getArchetypeService(), getLookupService(), rules);
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        DocumentGenerator generator = new DocumentGenerator(act, factory, new LocalContext(),
                                                            new HelpContext("foo", null), formatter,
                                                            getArchetypeService(), getLookupService(),
                                                            reportFactory, jobManager, documentRules, listener);

        // generate the document and verify it is of the correct type
        generator.generate();
        processQueuedTasks(10, () -> ref.get() != null);
        Document document = ref.get();
        assertNotNull(document);
        assertEquals(template.getName() + extension, document.getName());
    }

    /**
     * An implementation of {@link JobManager} to help with synchronisation.
     */
    private static class TestJobManager extends DocumentJobManager {

        private final Semaphore semaphore = new Semaphore(0);

        /**
         * Queues a listener to be invoked in the UI thread.
         * <p/>
         * This is only invoked once on completion, cancellation or failure of an interactive job.<br/>
         * NOTE: this method is only exposed for testing purposes.
         *
         * @param state    the UI state
         * @param listener the listener to execute
         */
        @Override
        protected void queueListener(State state, Runnable listener) {
            super.queueListener(state, listener);
            semaphore.release();
        }
    }
}
