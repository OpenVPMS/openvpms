/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.junit.Before;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.bound.AbstractBoundFieldTest;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

/**
 * Tests bound lookup field components.
 *
 * @author Tim Anderson
 */
public abstract class AbstractBoundLookupFieldTest<T> extends AbstractBoundFieldTest<T, String> {

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory factory;

    /**
     * The first lookup test value.
     */
    private Lookup lookup1;

    /**
     * The second lookup test value.
     */
    private Lookup lookup2;

    /**
     * First lookup code.
     */
    private static final String CODE1 = "value1";

    /**
     * Second lookup code.
     */
    private static final String CODE2 = "value2";


    /**
     * Constructs an {@link AbstractBoundLookupFieldTest}.
     */
    public AbstractBoundLookupFieldTest() {
        super(CODE1, CODE2);
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        lookup1 = factory.newLookup(PatientArchetypes.SPECIES).code(CODE1).name("Species 1").build(false);
        lookup2 = factory.newLookup(PatientArchetypes.SPECIES).code(CODE2).name("Species 2").build(false);
    }

    /**
     * Creates a new property.
     *
     * @return a new property
     */
    protected Property createProperty() {
        return new SimpleProperty("lookup", String.class);
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @return a new bound field
     */
    @Override
    protected T createField(Property property) {
        ListLookupQuery lookups = new ListLookupQuery(Arrays.asList(lookup1, lookup2));
        return createField(property, lookups);
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @param lookups  the lookups
     * @return a new bound field
     */
    protected abstract T createField(Property property, LookupQuery lookups);

}
