/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.system.ServiceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;


/**
 * Base class for tests for the {@link DocumentActEditor}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDocumentActEditorTest<T extends DocumentActEditor> extends AbstractIMObjectEditorTest<T> {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Constructs an {@link AbstractDocumentActEditorTest}.
     *
     * @param type      the editor type
     * @param archetype the archetype that the editor supports
     */
    public AbstractDocumentActEditorTest(Class<T> type, String archetype) {
        super(type, archetype);
    }

    /**
     * Creates a new act.
     *
     * @return a new act
     */
    protected DocumentAct createAct() {
        return (DocumentAct) newObject();
    }

    /**
     * Creates a new editor.
     *
     * @param act the act to edit
     * @return a new editor
     */
    protected abstract DocumentActEditor createEditor(DocumentAct act);

    /**
     * Helper to invoke save on an editor in a transaction.
     *
     * @param editor the editor
     * @return {@code true} if the save was successful, otherwise {@code false}
     */
    protected boolean save(DocumentActEditor editor) {
        return SaveHelper.save(editor);
    }

    /**
     * Helper to invoke delete on an editor in a transaction.
     *
     * @param editor the editor
     */
    protected void delete(DocumentActEditor editor) {
        TransactionTemplate txn = new TransactionTemplate(
                ServiceHelper.getTransactionManager());
        txn.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                editor.delete();
            }
        });
    }

    /**
     * Helper to create an image document.
     *
     * @return a new image document
     */
    protected Document createImage() {
        String name = "/org/openvpms/web/resource/image/openvpms.png";
        return createDocument(name);
    }

    /**
     * Creates a document from a resource.
     *
     * @param path the resource path
     * @return a new document
     */
    protected Document createDocument(String path) {
        return (Document) documentFactory.createDocument(path);
    }

    /**
     * Creates a new <em>entity.documentTemplate</em>, associated with an <em>act.documentTemplate</em>.
     *
     * @param archetype the archetype the template is associated with
     * @return a new template
     */
    protected Entity createDocumentTemplate(String archetype) {
        return documentFactory.newTemplate().type(archetype).blankDocument().name("blank").build();
    }

}
