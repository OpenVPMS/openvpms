/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;


/**
 * Tests the {@link ConfirmingDeleter} class.
 *
 * @author Tim Anderson
 */
public class ConfirmingDeleterTestCase extends AbstractIMObjectDeleterTest {

    /**
     * Creates a new {@link IMObjectDeleter}.
     *
     * @param factory the deletion handler factory
     * @return a new deleter
     */
    @Override
    protected IMObjectDeleter<IMObject> createDeleter(IMObjectDeletionHandlerFactory factory) {
        return new ConfirmingDeleter<>(factory, getArchetypeService());
    }

    /**
     * Verifies any confirmation of deletion dialog matches that expected.
     *
     * @param object the object being deleted
     */
    @Override
    protected void checkDeletionConfirmation(IMObject object) {
        ConfirmationDialog dialog = findWindowPane(ConfirmationDialog.class);
        assertNotNull(dialog);
        String title = "Delete " + getDisplayName(object) + "?";
        String message = "Delete " + object.getName() + "? This operation cannot be undone.";
        assertEquals(title, dialog.getTitle());
        assertEquals(message, dialog.getMessage());
        fireDialogButton(dialog, ConfirmationDialog.OK_ID);
    }

    /**
     * Verifies any deactivation confirmation dialog matches that expected.
     *
     * @param object the object being deactivated
     */
    @Override
    protected void checkDeactivationConfirmation(IMObject object) {
        ConfirmationDialog dialog = findWindowPane(ConfirmationDialog.class);
        assertNotNull(dialog);
        String title = "Deactivate " + getDisplayName(object) + "?";
        String message = object.getName() + " has relationships and cannot be deleted.\n\n" +
                         "Do you want to deactivate it instead?";
        assertEquals(title, dialog.getTitle());
        assertEquals(message, dialog.getMessage());
        fireDialogButton(dialog, ConfirmationDialog.OK_ID);
    }

    /**
     * Verifies any deactivated error dialog matches that expected.
     *
     * @param object the object being deleted
     */
    @Override
    protected void checkDeactivatedError(IMObject object) {
        ErrorDialog dialog = findWindowPane(ErrorDialog.class);
        assertNotNull(dialog);
        String message = getDisplayName(object) + " " + object.getName()
                         + " cannot be deleted as it has relationships.\n\n"
                         + "It is currently deactivated.";
        assertEquals("Error", dialog.getTitle());
        assertEquals(message, dialog.getMessage());
        fireDialogButton(dialog, ErrorDialog.OK_ID);
    }
}
