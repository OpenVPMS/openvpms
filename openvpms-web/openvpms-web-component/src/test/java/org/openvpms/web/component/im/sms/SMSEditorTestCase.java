/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.sms;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link SMSEditor}.
 *
 * @author Tim Anderson
 */
public class SMSEditorTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The SMS service.
     */
    private SMSService smsService;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        smsService = Mockito.mock(SMSService.class);
        when(smsService.getMaxParts()).thenReturn(1);
        when(smsService.getParts(anyString()))
                .then(invocation -> SMSLengthCalculator.getParts(invocation.getArgument(0)));
    }

    /**
     * Verifies that macros are expanded.
     */
    @Test
    public void testMacroExpansion() {
        Party location = practiceFactory.newLocation()
                .name("Pets R Us")
                .addPhone("912345678")
                .build();

        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build();
        Entity schedule = schedulingFactory.createSchedule(location);
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Act appointment = schedulingFactory.newAppointment()
                .startTime("2019-02-22 09:00:00")
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customer)
                .patient(patient)
                .build();

        Context context = new LocalContext();
        context.setLocation(location);
        context.setAppointment(appointment);

        String expression = "concat(expr:if(boolean($appointment.patient), " +
                            "concat($appointment.patient.entity.name, \"'s\"), 'Your'), ' appointment at ', " +
                            "$appointment.schedule.entity.location.target.name, ' is confirmed for ', " +
                            "date:format($appointment.startTime, 'dd/MM/yy'), ' @ ', " +
                            "date:format($appointment.startTime, 'hh:mm'), $nl, 'Call us on ', " +
                            "party:getTelephone($location), ' if you need to change the appointment')";
        lookupFactory.newMacro()
                .code("@testconfappointment")
                .expression(expression)
                .build();

        MacroVariables variables = new MacroVariables(context, getArchetypeService(), getLookupService());
        SMSEditor editor = new SMSEditor(Collections.emptyList(), variables, context, smsService);
        editor.setMessage("@testconfappointment");
        assertEquals("Fido's appointment at Pets R Us is confirmed for 22/02/19 @ 09:00\n" +
                     "Call us on 912345678 if you need to change the appointment", editor.getMessage());

        // verify the alternate text is used when the patient is not present
        IMObjectBean appointmentBean = getBean(appointment);
        appointmentBean.removeValues("patient");
        editor.setMessage("@testconfappointment");
        assertEquals("Your appointment at Pets R Us is confirmed for 22/02/19 @ 09:00\n" +
                     "Call us on 912345678 if you need to change the appointment", editor.getMessage());
    }

    /**
     * Verifies that if a phone number is not present or invalid, a validation error is raised.
     */
    @Test
    public void testInvalidPhone() {
        Party customer1 = customerFactory.newCustomer()
                .build();
        Party customer2 = customerFactory.newCustomer()
                .addPhone("")
                .build();
        Party customer3 = customerFactory.newCustomer()
                .addPhone("      ")
                .build();
        Party customer4 = customerFactory.newCustomer()
                .addPhone("need to get an updated number")
                .build();
        checkInvalid(customer1, "a message", "Phone is required");
        checkInvalid(customer2, "a message", "Phone is required");
        checkInvalid(customer3, "a message", "Phone is required");
        checkInvalid(customer4, "a message", "Phone is required");
    }

    /**
     * Verifies that if a message is not present or invalid, a validation error is raised.
     */
    @Test
    public void testInvalidMessage() {
        Party customer = customerFactory.newCustomer()
                .addPhone("123456789")
                .build();
        checkInvalid(customer, null, "Message is required");
        checkInvalid(customer, "", "Message is required");
        checkInvalid(customer, "   ", "Message is required");
        checkInvalid(customer, StringUtils.repeat("x", 161),
                     "SMS is too long: 2 parts were entered but only 1 is supported");
    }

    /**
     * Verifies that the given party's contacts and message generates the expected validation error.
     *
     * @param party    the party
     * @param message  the message
     * @param expected the expected validation error
     */
    private void checkInvalid(Party party, String message, String expected) {
        List<Contact> contacts = new ArrayList<>(party.getContacts());
        Context context = new LocalContext();
        MacroVariables variables = new MacroVariables(context, getArchetypeService(), getLookupService());
        SMSEditor editor = new SMSEditor(contacts, variables, context, smsService);
        editor.setMessage(message);
        assertInvalid(editor, expected);
    }
}
