/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.action.FailureReason;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.error.DialogErrorHandler;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Base class for {@link IMObjectDeleter} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectDeleterTest extends AbstractAppTest {

    /**
     * The help context.
     */
    private final HelpContext help = new HelpContext("foo", null);

    /**
     * The context.
     */
    private final Context context = new LocalContext();

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The deletion handler factory.
     */
    private IMObjectDeletionHandlerFactory factory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        factory.setApplicationContext(applicationContext);

        // register a DialogErrorHandler so errors display in an ErrorDialog
        ErrorHandler.setInstance(new DialogErrorHandler());
    }

    /**
     * Verifies that attempting to delete an entity with participations deactivates it.
     */
    @Test
    public void testDeleteEntityWithParticipations() {
        // create a customer and associated invoice
        Party customer = customerFactory.createCustomer();
        Party pet = patientFactory.createPatient(customer);

        accountFactory.newInvoice()
                .customer(customer)
                .item().patient(pet).product(productFactory.createMedication()).unitPrice(100).add()
                .status(ActStatus.POSTED)
                .build();
        // customer has participation relationships to the invoice

        // verify the customer is deactivated rather than deleted
        checkDeactivate(customer);

        // now attempt deletion again. The deactivated() method should be invoked
        customer = get(customer);
        checkDeactivated(customer);
    }

    /**
     * Verifies that attempting to delete an entity which is the source of a relationship deactivates it instead.
     */
    @Test
    public void testDeleteSourceWithEntityRelationships() {
        Party customer = customerFactory.createCustomer();
        Party pet = patientFactory.createPatient(customer);

        // verify the customer is deactivated
        checkDeactivate(customer);
        assertNotNull(get(pet));  // the pet should still exist
    }

    /**
     * Verifies that attempting to delete an entity which is the target of a relationship
     * invokes {@link AbstractIMObjectDeleter#delete}, and performs the removal.
     */
    @Test
    public void testDeleteTargetWithEntityRelationships() {
        Party customer = customerFactory.createCustomer();
        Party pet = patientFactory.createPatient(customer);

        // verify the pet is deleted
        checkDelete(pet);

        assertNotNull(get(customer));
    }

    /**
     * Verifies that <em>entity.documentTemplate</em> can be deleted despite having an
     * <em>participation.document</em> participation.
     */
    @Test
    public void testDeleteTemplate() {
        // create a template with associated act.documentTemplate
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);

        // verify the template is deleted
        checkDelete(template);
    }

    /**
     * Verifies that <em>entity.documentTemplate</em> can't be deleted if it has participations
     * other than <em>participation.document</em>. These will be deactivated.
     */
    @Test
    public void testDeleteTemplateWithParticipations() {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Party patient = patientFactory.createPatient();
        Act act = patientFactory.createForm(patient, template);

        // verify attempting to delete the template deactivates it
        checkDeactivate(template);

        // now attempt deletion again. The deactivated() method should be invoked
        checkDeactivated(template);
        assertNotNull(get(act));    // act should still exist
    }

    /**
     * Verifies that attempting to delete an entity that is the target of an entity link deactivates it.
     */
    @Test
    public void testDeleteWithEntityLinks() {
        Party location = practiceFactory.createLocation();
        customerFactory.newCustomer()
                .practice(location)
                .build();

        // verify attempting to delete the location deactivates it
        checkDeactivate(location);
    }

    /**
     * Verifies POSTED acts can't be deleted or deactivated.
     */
    @Test
    public void testDeletePostedAct() {
        Party customer = customerFactory.createCustomer();
        Product product = productFactory.createService();

        FinancialAct charge = accountFactory.newCounterSale()
                .customer(customer)
                .item().product(product).unitPrice(100).add()
                .status(ActStatus.POSTED)
                .build();

        // verify attempting to delete or deactivate the charge is unsupported
        checkUnsupported(charge);
    }

    /**
     * Verifies that attempting to delete an inactive object that cannot be deleted displays the correct prompts.
     */
    @Test
    public void testAlreadyDeactivated() {
        Party location = practiceFactory.newLocation()
                .active(false)
                .build();
        customerFactory.newCustomer()
                .practice(location)
                .build();
        checkDeactivated(location);
    }

    /**
     * Verifies that deleting an object goes through the correct prompts, and ignores the error when the object
     * is not found.
     */
    @Test
    public void testDeleteAlreadyDeleted() {
        Party customer = customerFactory.createCustomer();
        remove(customer);
        assertNull(get(customer));
        checkDelete(customer);
    }

    /**
     * Verifies that if an object is modified by another user prior to its deletion, an appropriate error is displayed
     * and it is not deleted.
     */
    @Test
    public void testDeleteFailure() {
        Party customer = customerFactory.createCustomer();

        // update a copy of the customer
        customerFactory.updateCustomer(get(customer))
                .firstName(ValueStrategy.random())
                .build();

        IMObjectDeleter<IMObject> deleter = createDeleter(factory);
        TestListener<IMObject> listener = new TestListener<>();
        deleter.delete(customer, context, help, listener);

        checkDeletionConfirmation(customer);

        // verify the failure() method of the listener was called
        checkListener(listener, false, false, false, false, true);

        // verify an error dialog was displayed
        findMessageDialogAndFireButton(ErrorDialog.class,
                                       "Delete failed",
                                       "The Customer could not be deleted. It may have been changed by another user.",
                                       ErrorDialog.OK_ID);

        // verify the customer wasn't deleted
        assertNotNull(get(customer));
    }

    /**
     * Creates a new {@link IMObjectDeleter}.
     *
     * @param factory the deletion handler factory
     * @return a new deleter
     */
    protected abstract IMObjectDeleter<IMObject> createDeleter(IMObjectDeletionHandlerFactory factory);

    /**
     * Verifies any confirmation of deletion dialog matches that expected.
     *
     * @param object the object being deleted
     */
    protected abstract void checkDeletionConfirmation(IMObject object);

    /**
     * Verifies any deactivation confirmation dialog matches that expected.
     *
     * @param object the object being deactivated
     */
    protected abstract void checkDeactivationConfirmation(IMObject object);

    /**
     * Verifies any deactivated error dialog matches that expected.
     *
     * @param object the object being deleted
     */
    protected abstract void checkDeactivatedError(IMObject object);

    /**
     * Verifies an {@link IMObjectDeleter} can delete an object, displaying the correct prompts.
     *
     * @param object the object to delete
     */
    private void checkDelete(IMObject object) {
        IMObjectDeleter<IMObject> deleter = createDeleter(factory);
        TestListener<IMObject> listener = new TestListener<>();
        deleter.delete(object, context, help, listener);

        checkDeletionConfirmation(object);

        checkListener(listener, true, false, false, false, false);

        // now verify the object has been deleted
        assertNull(get(object));
    }

    /**
     * Verifies an {@link IMObjectDeleter} can deactivate an object, displaying the correct prompts.
     */
    private void checkDeactivate(IMObject object) {
        // verify the object is saved and active
        IMObject saved = get(object);
        assertNotNull(saved);
        assertTrue(saved.isActive());

        IMObjectDeleter<IMObject> deleter = createDeleter(factory);
        TestListener<IMObject> listener = new TestListener<>();
        deleter.delete(object, context, help, listener);

        checkDeactivationConfirmation(object);

        checkListener(listener, false, true, false, false, false);

        // now verify the object has been deactivated
        IMObject reloaded = get(object);
        assertNotNull(object);
        assertFalse(reloaded.isActive());
    }

    /**
     * Verifies {@link ConfirmingDeleter} is run on a deactivated object that cannot be deleted, the correct
     * prompts are displayed.
     */
    private void checkDeactivated(IMObject object) {
        // verify the object is saved and inactive
        IMObject saved = get(object);
        assertNotNull(saved);
        assertFalse(saved.isActive());

        IMObjectDeleter<IMObject> deleter = createDeleter(factory);
        TestListener<IMObject> listener = new TestListener<>();
        deleter.delete(object, context, help, listener);

        checkDeactivatedError(object);

        checkListener(listener, false, false, true, false, false);

        // now verify the object is unchanged
        IMObject reloaded = get(object);
        assertNotNull(object);
        assertFalse(reloaded.isActive());
        assertEquals(saved.getVersion(), reloaded.getVersion());
    }

    /**
     * Verifies when an {@link IMObjectDeleter} is run for an object that cannot be deleted or deactivated, the correct
     * prompts are displayed.
     */
    private void checkUnsupported(IMObject object) {
        // verify the object is saved
        IMObject saved = get(object);
        assertNotNull(saved);

        IMObjectDeleter<IMObject> deleter = createDeleter(factory);

        TestListener<IMObject> listener = new TestListener<>();
        deleter.delete(object, context, help, listener);

        ErrorDialog dialog = findWindowPane(ErrorDialog.class);
        assertNotNull(dialog);
        String displayName = getDisplayName(object);
        String title = "Delete " + displayName + "?";
        String message = "Cannot delete " + displayName + ". It has been finalised.";
        assertEquals(title, dialog.getTitle());
        assertEquals(message, dialog.getMessage());
        fireDialogButton(dialog, ConfirmationDialog.OK_ID);

        checkListener(listener, false, false, false, true, false);

        // now verify the object is unchanged
        IMObject reloaded = get(object);
        assertNotNull(object);
        assertTrue(reloaded.isActive());
        assertEquals(saved.getVersion(), reloaded.getVersion());
    }

    /**
     * Verifies that the appropriate listener methods have been invoked.
     *
     * @param listener           the listener to check
     * @param deleted            if {@code true} expect the deleted() method to have been invoked
     * @param deactivated        if {@code true} expect the deactivated() method to have been invoked
     * @param alreadyDeactivated if {@code true} expect the alreadyDeactivated() method to have been invoked
     * @param unsupported        if {@code true} expect the unsupported() method to have been invoked
     * @param failed             if {@code true} expect the failed() method to have been invoked
     */
    private void checkListener(TestListener<?> listener, boolean deleted, boolean deactivated,
                               boolean alreadyDeactivated, boolean unsupported, boolean failed) {
        assertEquals(deleted, listener.deletedInvoked());
        assertEquals(deactivated, listener.deactivatedInvoked());
        assertEquals(alreadyDeactivated, listener.alreadyDeactivatedInvoked());
        assertEquals(unsupported, listener.unsupportedInvoked());
        assertEquals(failed, listener.failedInvoked());
    }

    private static class TestListener<T extends IMObject> extends AbstractIMObjectDeletionListener<T> {

        /**
         * Determines if deleted() was invoked.
         */
        private boolean deleted;

        /**
         * Determines if deactivated() was invoked.
         */
        private boolean deactivated;

        /**
         * Determines if alreadyDeactivated() was invoked.
         */
        private boolean alreadyDeactivated;

        /**
         * Determines if unsupported() was invoked.
         */
        private boolean unsupported;

        /**
         * Determines if failed() was invoked.
         */
        private boolean failed;

        /**
         * Determines if deleted() was invoked.
         *
         * @return {@code true} if deleted() was invoked
         */
        public boolean deletedInvoked() {
            return deleted;
        }

        /**
         * Determines if deactivated() was invoked.
         *
         * @return {@code true} if deactivated() was invoked
         */
        public boolean deactivatedInvoked() {
            return deactivated;
        }

        /**
         * Determines if alreadyDeactivated() was invoked.
         *
         * @return {@code true} if alreadyDeactivated() was invoked
         */
        public boolean alreadyDeactivatedInvoked() {
            return alreadyDeactivated;
        }

        /**
         * Determines if unsupported() was invoked.
         *
         * @return {@code true} if unsupported() was invoked
         */
        public boolean unsupportedInvoked() {
            return unsupported;
        }

        /**
         * Determines if failed() was invoked.
         *
         * @return {@code true} if a failed() was invoked
         */
        public boolean failedInvoked() {
            return failed;
        }


        @Override
        public void deleted(T object) {
            super.deleted(object);
            deleted = true;
        }

        @Override
        public void deactivated(T object) {
            super.deactivated(object);
            this.deactivated = true;
        }

        @Override
        public void alreadyDeactivated(T object) {
            super.alreadyDeactivated(object);
            this.alreadyDeactivated = true;
        }

        @Override
        public void unsupported(T object, String reason) {
            super.unsupported(object, reason);
            this.unsupported = true;
        }

        @Override
        public void failed(T object, FailureReason reason) {
            super.failed(object, reason);
            failed = true;
        }
    }
}