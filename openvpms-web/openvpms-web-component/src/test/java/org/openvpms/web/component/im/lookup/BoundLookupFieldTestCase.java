/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.lookup;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.web.component.property.IMObjectProperty;
import org.openvpms.web.component.property.Property;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


/**
 * Tests the {@link BoundLookupField} class.
 *
 * @author Tim Anderson
 */
public class BoundLookupFieldTestCase extends AbstractBoundLookupFieldTest<BoundLookupField> {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Constructs a {@link BoundLookupFieldTestCase}.
     */
    public BoundLookupFieldTestCase() {
        super();
    }

    /**
     * Verifies that when a property is part of a persistent object, the default selection only applies when
     * the object is unsaved.
     */
    @Test
    public void testDefaultSelectionForPersistentObject() {
        // the default value for the orientation field of entity.documentTemplate is 'PORTRAIT'
        Entity template1 = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        BoundLookupField field1 = new BoundLookupField(getOrientation(template1), template1);
        Lookup selected1 = field1.getSelected();
        assertNotNull(selected1);
        assertEquals(DocumentTemplate.PORTRAIT, selected1.getCode());

        // verify that when the default is overridden, it is not reset by the field
        Entity template2 = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .orientation(DocumentTemplate.LANDSCAPE)
                .build();

        BoundLookupField field2 = new BoundLookupField(getOrientation(template2), template2);
        Lookup selected2 = field2.getSelected();
        assertNotNull(selected2);
        assertEquals(DocumentTemplate.LANDSCAPE, selected2.getCode());

        // verify that when the default is overridden and set to null, it is not reset by the field
        Entity template3 = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .orientation(null)
                .build();

        BoundLookupField field3 = new BoundLookupField(getOrientation(template3), template3);
        Lookup selected3 = field3.getSelected();
        assertNull(selected3);
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @return a new bound field
     */
    @Override
    protected BoundLookupField createField(Property property, LookupQuery lookups) {
        return new BoundLookupField(property, lookups, false);
    }

    /**
     * Returns the value of the field.
     *
     * @param field the field
     * @return the value of the field
     */
    @Override
    protected String getValue(BoundLookupField field) {
        return field.getSelectedCode();
    }

    /**
     * Sets the value of the field.
     *
     * @param field the field
     * @param value the value to set
     */
    @Override
    protected void setValue(BoundLookupField field, String value) {
        field.setSelected(value);
    }

    /**
     * Returns a property bound to the 'orientation' node of an <em>entity.documentTemplate</em>.
     *
     * @param template the template
     * @return the corresponding property
     */
    private Property getOrientation(Entity template) {
        return new IMObjectProperty(template, DescriptorHelper.getNode(template.getArchetype(), "orientation",
                                                                       getArchetypeService()));
    }

}
