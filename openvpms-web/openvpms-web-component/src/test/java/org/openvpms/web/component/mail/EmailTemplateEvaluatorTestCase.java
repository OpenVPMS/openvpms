/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import com.google.common.base.Charsets;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.macro.Macros;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link EmailTemplateEvaluator}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class EmailTemplateEvaluatorTestCase extends AbstractAppTest {

    /**
     * The macro service.
     */
    @Autowired
    private Macros macros;

    /**
     * The report factory.
     */
    @Autowired
    private ReportFactory reportFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The template evaluator.
     */
    private EmailTemplateEvaluator evaluator;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        DocumentConverter converter = Mockito.mock(DocumentConverter.class);
        evaluator = new EmailTemplateEvaluator(getArchetypeService(), getLookupService(), macros, reportFactory,
                                               converter);
    }

    /**
     * Tests a template with TEXT subject and content nodes.
     */
    @Test
    public void testPlainText() {
        EmailTemplate template = createTemplate(EmailTemplate.SubjectType.TEXT, "Plain text subject &",
                                                EmailTemplate.ContentType.TEXT, "Plain\ntext\ncontent &");
        String subject = evaluator.getSubject(template, new Object(), new LocalContext());
        String message = evaluator.getMessage(template, new Object(), new LocalContext());

        assertEquals("Plain text subject &", subject);
        assertEquals("Plain<br/>text<br/>content &amp;", message);
    }

    /**
     * Tests a template with MACRO subject and content nodes.
     */
    @Test
    public void testMacro() {
        lookupFactory.getMacro("@cpname", "concat($customer.firstName, ' & ', $patient.name)");
        Party customer = customerFactory.newCustomer()
                .name("Sue", "Smith")
                .build(false);
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build(false);

        EmailTemplate template = createTemplate(EmailTemplate.SubjectType.MACRO, "Reminder for @cpname",
                                                EmailTemplate.ContentType.MACRO, "Dear @cpname\nplease");

        LocalContext context = new LocalContext();
        context.setCustomer(customer);
        context.setPatient(patient);
        String subject = evaluator.getSubject(template, new Object(), context);
        String message = evaluator.getMessage(template, new Object(), context);

        assertEquals("Reminder for Sue & Fido", subject);
        assertEquals("Dear Sue &amp; Fido<br/>please", message);
    }

    /**
     * Tests a template with XPATH subject and content nodes.
     */
    @Test
    public void testXPath() {
        Party customer = customerFactory.newCustomer()
                .name("Sue", "Smith")
                .build(false);
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .build(false);
        EmailTemplate template = createTemplate(
                EmailTemplate.SubjectType.XPATH,
                "concat('Reminder for ', $customer.firstName, ' & ', $patient.name)",
                EmailTemplate.ContentType.XPATH,
                "concat('Dear ', $customer.firstName, ' & ', $patient.name,$nl,'please')");

        LocalContext context = new LocalContext();
        context.setCustomer(customer);
        context.setPatient(patient);
        String subject = evaluator.getSubject(template, new Object(), context);
        String message = evaluator.getMessage(template, new Object(), context);

        assertEquals("Reminder for Sue & Fido", subject);
        assertEquals("Dear Sue &amp; Fido<br/>please", message);
    }

    /**
     * Tests a template with a static HTML document.
     */
    @Test
    public void testStaticHTML() {
        Document html = documentFactory.newDocument()
                .name("test.html")
                .content("<html><body>some html text</body></html>")
                .mimeType(DocFormats.HTML_TYPE)
                .build();

        Entity entity = documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("Reminder")
                .document(html)
                .build();
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());

        String subject = evaluator.getSubject(template, new Object(), new LocalContext());
        String message = evaluator.getMessage(template, new Object(), new LocalContext());

        assertEquals("Reminder", subject);
        assertEquals("<html><body>some html text</body></html>", message);
    }

    /**
     * Tests a template with a JasperReport.
     *
     * @throws Exception for any error
     */
    @Test
    public void testJasperReport() throws Exception {
        Entity entity = documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("Reminder")
                .contentSource("$customer")
                .document("/EmailTemplateEvaluator.jrxml", DocFormats.XML_TYPE)
                .build();
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());

        Party customer = customerFactory.newCustomer()
                .name("Joe", "Smith")
                .build(false);
        LocalContext context = new LocalContext();
        context.setCustomer(customer);
        String subject = evaluator.getSubject(template, new Object(), context);
        String message = evaluator.getMessage(template, new Object(), context);

        assertEquals("Reminder", subject);
        try (InputStream stream = getClass().getResourceAsStream("/EmailTemplateEvaluator.expected.html")) {
            assertNotNull(stream);
            String expected = IOUtils.toString(stream, Charsets.UTF_8);
            expected = expected.replaceAll("\r\n", "\n");
            assertEquals(expected, message);
        }
    }

    /**
     * Tests setting the subject source.
     */
    @Test
    public void testSubjectSource() {
        Entity entity = documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("concat('Statement: ', openvpms:get(., 'firstName'), ' ', openvpms:get(., 'lastName'))")
                .subjectType(EmailTemplate.SubjectType.XPATH)
                .subjectSource("$customer")
                .build();
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());

        Party customer = customerFactory.newCustomer()
                .name("Joe", "Smith")
                .build(false);
        LocalContext context = new LocalContext();
        context.setCustomer(customer);
        String subject = evaluator.getSubject(template, new Object(), context);
        assertEquals("Statement: Joe Smith", subject);
    }

    /**
     * Tests setting the content source.
     */
    @Test
    public void testContentSource() {
        Entity entity = documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subject("Your statement")
                .content("concat('Dear ', openvpms:get(., 'firstName'), ' ', openvpms:get(., 'lastName'))")
                .contentType(EmailTemplate.ContentType.XPATH)
                .contentSource("$customer")
                .build();
        EmailTemplate template = new EmailTemplate(entity, getArchetypeService());

        Party customer = customerFactory.newCustomer()
                .name("Jo", "Smith")
                .build(false);
        LocalContext context = new LocalContext();
        context.setCustomer(customer);
        String content = evaluator.getMessage(template, new Object(), context);
        assertEquals("Dear Jo Smith", content);
    }

    /**
     * Helper to create an <em>entity.documentTemplateEmail</em>.
     *
     * @param subjectType the subject type
     * @param subject     the subject
     * @param contentType the content type
     * @param content     the content
     * @return a new template
     */
    private EmailTemplate createTemplate(EmailTemplate.SubjectType subjectType, String subject,
                                         EmailTemplate.ContentType contentType, String content) {
        Entity entity = documentFactory.newEmailTemplate(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE)
                .subjectType(subjectType)
                .subject(subject)
                .contentType(contentType)
                .content(content)
                .build();
        return new EmailTemplate(entity, getArchetypeService());
    }
}


