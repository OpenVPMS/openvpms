/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.report;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link InvestigationTemplateLocator}.
 *
 * @author Tim Anderson
 */
public class InvestigationTemplateLocatorTestCase extends ArchetypeServiceTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Tests the {@link InvestigationTemplateLocator#getTemplate()} method.
     */
    @Test
    public void testGetTemplate() {
        TemplateHelper helper = new TemplateHelper(getArchetypeService());
        Entity entity = helper.getTemplateForType(InvestigationArchetypes.PATIENT_INVESTIGATION);
        if (entity == null) {
            entity = documentFactory.newTemplate()
                    .type(InvestigationArchetypes.PATIENT_INVESTIGATION)
                    .blankDocument()
                    .build();
        }

        InvestigationTemplateLocator locator = new InvestigationTemplateLocator(getArchetypeService());
        DocumentTemplate template = locator.getTemplate();
        assertNotNull(template);
        assertEquals(entity, template.getEntity());
    }

    /**
     * Verifies that {@link InvestigationTemplateLocator} is returned by {@link DocumentTemplateLocatorFactory}
     * for investigations.
     */
    @Test
    public void testFactory() {
        IMObject investigation = create(InvestigationArchetypes.PATIENT_INVESTIGATION);
        DocumentTemplateLocatorFactory factory = new DocumentTemplateLocatorFactory(getArchetypeService());
        DocumentTemplateLocator locator = factory.getDocumentTemplateLocator(investigation, new LocalContext());
        assertTrue(locator instanceof InvestigationTemplateLocator);
    }
}