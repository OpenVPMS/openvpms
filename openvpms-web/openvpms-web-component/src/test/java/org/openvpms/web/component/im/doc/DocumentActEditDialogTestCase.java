/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.NEEDS_UPDATE;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.PROMPT;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.UP_TO_DATE;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Tests the {@link DocumentActEditDialog}.
 *
 * @author Tim Anderson
 */
public class DocumentActEditDialogTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The act to edit.
     */
    private DocumentAct act;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Override
    @Before
    public void setUp() {
        super.setUp();
        act = customerFactory.newLetter()
                .customer(customerFactory.createCustomer())
                .build(false);
        context = new LocalContext();
    }

    /**
     * Verifies that pressing the Apply button generates the document if required.
     */
    @Test
    public void testApply() {
        DocumentActEditor editor = createEditor();
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();

        editor.setTemplate(createDocumentTemplate());
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());

        // flagging an optional attribute modified should not change the status
        editor.documentAttributeModified(false);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());

        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);
        processQueuedTasks(10, () -> act.getDocument() != null);

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);
        assertNotNull(get(docRef1));

        // verify that changing a property doesn't change the document status
        editor.getProperty("description").setValue("a description");
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        // no new document should be generated
        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);
        processQueuedTasks(); // shouldn't be any
        assertEquals(docRef1, act.getDocument());

        // now simulate an optional attribute updating, but click NO on the confirmation.
        // No document should generate
        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.NO_ID);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        processQueuedTasks(); // shouldn't be any
        assertEquals(docRef1, act.getDocument());

        // now simulate an optional attribute updating, and click YES on the confirmation.
        // A document should generate
        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.YES_ID);
        processQueuedTasks(10, () -> !Objects.equals(act.getDocument(), docRef1));

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef2 = act.getDocument();
        assertNotNull(docRef2);
        assertNotNull(get(docRef2));

        // now simulate an optional then mandatory attribute updating, and click YES on the confirmation.
        // A document should generate without prompting
        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());

        editor.documentAttributeModified(true);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);
        processQueuedTasks(10, () -> !Objects.equals(act.getDocument(), docRef2));

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef3 = act.getDocument();
        assertNotNull(docRef3);
        assertNotNull(get(docRef3));

        // now simulate an optional attribute updating, and click CANCEL on the confirmation.
        // The editor will still flag the document status as PROMPT
        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.APPLY_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.CANCEL_ID);
        processQueuedTasks(); // shouldn't be any
        assertEquals(PROMPT, editor.getDocumentStatus());
        assertEquals(docRef3, act.getDocument());
    }

    /**
     * Verifies that pressing the OK button generates the document if required.
     */
    @Test
    public void testOK() {
        DocumentActEditor editor = createEditor();
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        DocumentActEditDialog dialog1 = createDialog(editor);
        dialog1.show();

        editor.setTemplate(createDocumentTemplate());
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());

        fireDialogButton(dialog1, DocumentActEditDialog.OK_ID);
        processQueuedTasks(10, () -> act.getDocument() != null);

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);
        assertNotNull(get(docRef1));
        assertNull(dialog1.getParent());  // verify dialog is closed
    }

    /**
     * Verifies that a document isn't generated if a simple property changes.
     */
    @Test
    public void testOKWithSimplePropertyChange() {
        DocumentActEditor editor = createEditorWithDocument();
        Reference docRef = act.getDocument();
        assertNotNull(docRef);

        // simulate editing the act
        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();
        assertNotNull(dialog.getParent());  // dialog open

        // verify that changing a property doesn't change the document status
        editor.getProperty("description").setValue("a description");
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        // no new document should be generated
        fireDialogButton(dialog, DocumentActEditDialog.OK_ID);
        processQueuedTasks(); // shouldn't be any
        assertEquals(docRef, act.getDocument());
        assertNull(dialog.getParent()); // verify dialog closed
    }

    /**
     * Verifies that when OK is pressed after a mandatory document update, a new document is generated without
     * prompting.
     */
    @Test
    public void testOKWithMandatoryUpdate() {
        DocumentActEditor editor = createEditorWithDocument();
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);

        // now simulate a mandatory attribute updating, and click YES on the confirmation.
        // A document should generate without prompting
        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();
        editor.documentAttributeModified(true);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.OK_ID);
        processQueuedTasks(10, () -> !Objects.equals(act.getDocument(), docRef1));

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef2 = act.getDocument();
        assertNotNull(docRef2);
        assertNotEquals(docRef1, docRef2);
        assertNotNull(get(docRef2));
        assertNull(dialog.getParent());  // verify dialog is closed
    }

    /**
     * Simulate changing an optional attribute and verify clicking YES when it prompts to generate the document,
     * generates a new one.
     */
    @Test
    public void testOKWithYesPrompt() {
        DocumentActEditor editor = createEditorWithDocument();
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);

        // now simulate an optional attribute updating, and click YES on the confirmation.
        // A document should generate
        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();
        assertNotNull(dialog.getParent());  // dialog open

        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.OK_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.YES_ID);
        processQueuedTasks(10, () -> !Objects.equals(act.getDocument(), docRef1));

        // verify a document was generated
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        Reference docRef2 = act.getDocument();
        assertNotNull(docRef2);
        assertNotEquals(docRef1, docRef2);
        assertNotNull(get(docRef2));
        assertNull(dialog.getParent());  // verify dialog is closed
    }

    /**
     * Simulate changing an optional attribute and verify clicking NO when it prompts to generate the document
     * doesn't generate a new one.
     */
    @Test
    public void testOKWithNoPrompt() {
        DocumentActEditor editor = createEditorWithDocument();
        Reference docRef = act.getDocument();
        assertNotNull(docRef);

        // now simulate an optional attribute updating, but click NO on the confirmation.
        // No document should generate
        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();
        assertNotNull(dialog.getParent());   // dialog open

        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.OK_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.NO_ID);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        processQueuedTasks(); // shouldn't be any
        assertEquals(docRef, act.getDocument());

        assertNull(dialog.getParent());  // verify dialog is closed
    }

    /**
     * Simulate changing an optional attribute and verify clicking CANCEL when it prompts to generate the document.
     * A new document shouldn't be created and the dialog should remain open.
     */
    @Test
    public void testOKWithCancelPrompt() {
        DocumentActEditor editor = createEditorWithDocument();
        Reference docRef = act.getDocument();
        assertNotNull(docRef);

        // now simulate an optional attribute updating, and click CANCEL on the confirmation.
        DocumentActEditDialog dialog = createDialog(editor);
        dialog.show();
        editor.documentAttributeModified(false);
        assertEquals(PROMPT, editor.getDocumentStatus());
        fireDialogButton(dialog, DocumentActEditDialog.OK_ID);

        fireUpdateButtonConfirmation(ConfirmationDialog.CANCEL_ID);
        processQueuedTasks();  // shouldn't be any

        // prompt should remain
        assertEquals(PROMPT, editor.getDocumentStatus());

        // dialog should still be open
        assertNotNull(dialog.getParent());

        // verify no new document was generated
        assertEquals(docRef, act.getDocument());
    }

    /**
     * Creates a new <em>entity.documentTemplate</em>, associated with an <em>act.documentTemplate</em>.
     *
     * @return a new template
     */
    protected Entity createDocumentTemplate() {
        return documentFactory.newTemplate().type(CustomerArchetypes.DOCUMENT_LETTER)
                .blankDocument()
                .name("blank")
                .build();
    }

    /**
     * Verifies a confirmation dialog is being displayed to update the document, and fires the specified button.
     *
     * @param buttonId the button identifier
     */
    private void fireUpdateButtonConfirmation(String buttonId) {
        String prompt = "One or more attributes have changed.\n\n" +
                        "Do you want to update the document?";
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Update Document", prompt, buttonId);
    }

    /**
     * Creates an edit dialog.
     *
     * @param editor the editor
     * @return a new dialog
     */
    private DocumentActEditDialog createDialog(DocumentActEditor editor) {
        return new DocumentActEditDialog(editor, context);
    }

    /**
     * Creates an editor.
     *
     * @return a new editor
     */
    private DocumentActEditor createEditor() {
        return new DocumentActEditor(act, null, new DefaultLayoutContext(
                context, new HelpContext("foo", null)));
    }

    /**
     * Creates an editor with an existing document.
     *
     * @return the editor
     */
    private DocumentActEditor createEditorWithDocument() {
        DocumentActEditor editor = createEditor();
        editor.setTemplate(createDocumentTemplate());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> act.getDocument() != null);
        assertNotNull(act.getDocument());
        assertTrue(SaveHelper.save(editor));
        return editor;
    }
}