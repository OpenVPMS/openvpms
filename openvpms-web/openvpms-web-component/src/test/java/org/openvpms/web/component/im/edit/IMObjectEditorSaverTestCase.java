/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.StaleStateException;
import org.hibernate.exception.LockAcquisitionException;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestAppointmentBuilder;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditorSaver.SaveOperation;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link IMObjectEditorSaver}.
 *
 * @author Tim Anderson
 */
public class IMObjectEditorSaverTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Verifies that if an exception is thrown during save, and  reloading isn't supported, the error itself is
     * displayed.
     */
    @Test
    public void testErrorMessageWhenExceptionThrownForNonReloadingEditor() {
        checkErrorMessageForExceptionDuringSave(new IllegalStateException("Save error"), false, "Save error");
        checkErrorMessageForExceptionDuringSave(new ObjectNotFoundException("foo", "bar"), false,
                                                "[bar#foo] may have been deleted by another user");
    }

    /**
     * Verifies that if an exception is thrown during save, and reloading is supported, a '
     * Your changes have been reverted' message is displayed.
     */
    @Test
    public void testErrorMessageWhenExceptionThrownForReloadingEditor() {
        checkErrorMessageForExceptionDuringSave(new IllegalStateException("Save error"), true,
                                                "Save error.\n\n" +
                                                "Your changes have been reverted.");
        checkErrorMessageForExceptionDuringSave(
                new ObjectNotFoundException("foo", "bar"), true,
                "The Appointment could not be saved. It may have been changed by another user.\n\n" +
                "Your changes have been reverted.");
    }

    /**
     * Verifies that validation errors are displayed when a rule triggered by the save fails due to a validation error,
     * and the editor doesn't support reloading.
     */
    @Test
    public void testErrorMessageWhenRuleThrowsValidationExceptionThrownForNonReloadingEditor() {
        checkErrorMessageForRuleValidationException(false,
                                                    "Failed to validate Customer of Task: must supply at least 1 item");
    }

    /**
     * Verifies that validation errors are displayed when a rule triggered by the save fails due to a validation error,
     * and the editor supports reloading.
     */
    @Test
    public void testErrorMessageWhenRuleThrowsValidationExceptionThrownForReloadingEditor() {
        String error = "Failed to validate Customer of Task: must supply at " +
                       "least 1 item.\n\nYour changes have been reverted.";
        checkErrorMessageForRuleValidationException(true, error);
    }

    /**
     * Tests the errors displayed for deadlocks.
     */
    @Test
    public void testErrorMessageForDeadlock() {
        // simulate a deadlock stacktrace
        CannotAcquireLockException exception = new CannotAcquireLockException(
                "could not execute batch",
                new LockAcquisitionException(
                        "could not execute batch",
                        new BatchUpdateException(
                                new MySQLTransactionRollbackException(
                                        "Deadlock found when trying to get lock; try restarting transaction"))));

        // check error when an editor supports reloading
        checkErrorMessageForExceptionDuringSave(exception, true, "The Appointment could not be saved due to a " +
                                                                 "deadlock. Please retry the operation.\n\n" +
                                                                 "Your changes have been reverted.");

        // check error when reloading is not supported
        checkErrorMessageForExceptionDuringSave(exception, false, "The Appointment could not be saved due to a " +
                                                                  "deadlock. Please retry the operation.");
    }

    /**
     * Tests the error displayed for Hibernate StaleStateExceptions.
     */
    @Test
    public void testErrorMessageForStaleStateException() {
        HibernateOptimisticLockingFailureException exception = new HibernateOptimisticLockingFailureException(
                new StaleStateException("Batch update returned unexpected row count from update [0]; actual row count: " +
                                        "0; expected: 1"));
        checkErrorMessageForExceptionDuringSave(
                exception, true,
                "The Appointment could not be saved. It may have been changed by another user.\n\n" +
                "Your changes have been reverted.");
    }

    /**
     * Verify that the expected error message is displayed if an exception is thrown when the editor is saved.
     *
     * @param exception the exception to throw
     * @param reload    if {@code true}, support reloading
     * @param error     the expected error message
     */
    private void checkErrorMessageForExceptionDuringSave(RuntimeException exception, boolean reload, String error) {
        List<String> errors = new ArrayList<>();
        initErrorHandler(errors);
        Act appointment = createAppointment();

        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = new DefaultIMObjectEditor(appointment, context) {
            @Override
            public void save() {
                throw exception;
            }
        };

        SaveOperation strategy = new SaveOperation() {
            protected boolean reload(IMObjectEditor editor) {
                return reload;
            }
        };
        IMObjectEditorSaver saver = new IMObjectEditorSaver(strategy);
        saver.save(editor);
        assertEquals(1, errors.size());
        assertEquals(error, errors.get(0));
    }

    /**
     * Creates an appointment.
     *
     * @return the appointment
     */
    private Act createAppointment() {
        return newAppointment().build();
    }

    /**
     * Returns a builder for new appointment, with the required details populated.
     *
     * @return a new builder
     */
    private TestAppointmentBuilder newAppointment() {
        Party location = practiceFactory.createLocation();
        return schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .schedule(schedulingFactory.createSchedule(location))
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customerFactory.createCustomer());
    }

    /**
     * Verify that the expected error message is displayed if an exception is thrown when the editor is saved.
     * <p/>
     * This saves an invalid act.customerTask, so when the archetypeService.save.act.customerAppointment.after rule
     * fires, a ValidationException will be thrown.
     *
     * @param reload if {@code true}, support reloading
     * @param error  the expected error message
     */
    private void checkErrorMessageForRuleValidationException(boolean reload, String error) {
        List<String> errors = new ArrayList<>();
        initErrorHandler(errors);
        TestAppointmentBuilder builder = newAppointment();
        Act task = schedulingFactory.newTask()
                .startTime(DateRules.getTomorrow())
                .taskType(schedulingFactory.createTaskType())
                .customer(builder.getCustomer())
                .patient(builder.getPatient())
                .workList(schedulingFactory.createWorkList())
                .build();

        // link the appointment to a task, so the task is so the task is updated by the rule when the appointment is
        // saved.
        Act appointment = builder.task(task)
                .build();

        IMObjectBean taskBean = getBean(task);
        taskBean.removeValues("customer");
        getArchetypeService().save(task, false);  // force saving an invalid object

        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = new DefaultIMObjectEditor(appointment, context);
        editor.getProperty("status").setValue(AppointmentStatus.COMPLETED);
        SaveOperation strategy = new SaveOperation() {
            public boolean reload(IMObjectEditor editor) {
                return reload;
            }
        };
        IMObjectEditorSaver saver = new IMObjectEditorSaver(strategy);
        saver.save(editor);
        assertEquals(1, errors.size());
        assertEquals(error, errors.get(0));
    }
}
