/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.junit.Test;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Base class for {@link IMObjectEditor} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractIMObjectEditorTest<T extends IMObjectEditor> extends AbstractAppTest {

    /**
     * The editor type.
     */
    private final Class<T> type;

    /**
     * The archetype that the editor supports.
     */
    private final String archetype;

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * Constructs an {@link AbstractIMObjectEditorTest}.
     *
     * @param type      the editor type
     * @param archetype the archetype that the editor supports
     */
    public AbstractIMObjectEditorTest(Class<T> type, String archetype) {
        this.type = type;
        this.archetype = archetype;
    }

    /**
     * Tests the implementation of the {@link IMObjectEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        checkNewInstance(archetype, type);
    }

    /**
     * Verifies the editor is created for the archetype by {@link IMObjectEditorFactory}.
     */
    @Test
    public void testFactory() {
        checkFactory(archetype, type);
    }

    /**
     * Verifies that the editor is registered with the {@link IMObjectEditorFactory}.
     *
     * @param archetype the archetype to create
     * @param type      the expected type of the editor
     */
    protected void checkFactory(String archetype, Class<T> type) {
        IMObject object = create(archetype);
        LayoutContext context = createLayoutContext();
        IMObjectEditor editor = factory.create(object, context);
        assertEquals(type, editor.getClass());
    }

    /**
     * Creates an instance of the editor.
     *
     * @param object the object to edit
     * @return the editor
     */
    protected IMObjectEditor createEditor(IMObject object) {
        LayoutContext context = createLayoutContext();
        return createEditor(object, context);
    }

    /**
     * Creates a layout context.
     *
     * @return the layout context
     */
    protected LayoutContext createLayoutContext() {
        return createLayoutContext(new LocalContext());
    }

    /**
     * Creates a layout context.
     *
     * @param context the context
     * @return the layout context
     */
    protected LayoutContext createLayoutContext(Context context) {
        LayoutContext result = new DefaultLayoutContext(context, new HelpContext("foo", null));
        result.setEdit(true);
        return result;
    }

    /**
     * Creates an instance of the editor, cast to the expected type.
     *
     * @param object the object to edit
     * @return the editor
     */
    protected T newEditor(IMObject object) {
        return type.cast(createEditor(object));
    }

    /**
     * Creates an instance of the editor, cast to the expected type.
     *
     * @param object  the object to edit
     * @param context the context
     * @return the editor
     */
    protected T newEditor(IMObject object, Context context) {
        return type.cast(createEditor(object, createLayoutContext(context)));
    }

    /**
     * Creates an instance of the editor.
     *
     * @param object  the object to edit
     * @param context the layout context
     * @return the editor
     */
    protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
        return factory.create(object, context);
    }

    /**
     * Creates a new instance of the archetype.
     *
     * @return a new object
     */
    protected IMObject newObject() {
        return create(archetype);
    }

    /**
     * Checks the implementation of the {@link IMObjectEditor#newInstance()} method.
     *
     * @param archetype the archetype to create
     * @param type      the expected type of the editor
     */
    protected void checkNewInstance(String archetype, Class<T> type) {
        IMObject object = create(archetype);
        IMObjectEditor editor = createEditor(object);

        assertEquals(type, editor.getClass());
        IMObjectEditor newInstance = editor.newInstance();
        assertNotNull(newInstance);
        assertNotEquals(editor, newInstance);
        assertEquals(type, newInstance.getClass());
    }

    /**
     * Returns the editor factory.
     *
     * @return the factory
     */
    protected IMObjectEditorFactory getFactory() {
        return factory;
    }
}
