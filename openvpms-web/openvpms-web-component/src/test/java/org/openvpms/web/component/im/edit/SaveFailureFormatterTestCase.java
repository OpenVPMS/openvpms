/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.StaleStateException;
import org.hibernate.exception.LockAcquisitionException;
import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.business.service.archetype.ValidationError;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.component.business.service.security.ArchetypeAccessDeniedException;
import org.openvpms.component.business.service.security.OpenVPMSAccessDeniedException;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.transaction.UnexpectedRollbackException;

import java.sql.BatchUpdateException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link SaveFailureFormatter}.
 *
 * @author Tim Anderson
 */
public class SaveFailureFormatterTestCase extends AbstractAppTest {

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Tests the {@link SaveFailureFormatter#getTitle(IMObjectEditor)} method.
     */
    @Test
    public void testGetTitle() {
        SaveFailureFormatter formatter = new SaveFailureFormatter();
        IMObjectEditor editor = createEditor();
        assertEquals("Failed to save Appointment", formatter.getTitle(editor));
    }

    /**
     * Tests the {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} method, for a validation error.
     */
    @Test
    public void testGetMessageForValidationError() {
        SaveFailureFormatter formatter = new SaveFailureFormatter();
        IMObjectEditor editor = createEditor();
        ValidationError error = new ValidationError(editor.getObject().getObjectReference(), "appointmentType",
                                                    "Appointment Type is required");
        ValidationException exception = new ValidationException(
                Collections.singletonList(error), ValidationException.ErrorCode.FailedToValidObjectAgainstArchetype);

        assertEquals("Failed to validate Appointment Type of Appointment: " +
                     "Appointment Type is required.\n\nYour changes have been reverted.",
                     formatter.getMessage(editor, exception));
    }

    /**
     * Verifies a '...modified by another user' message is returned by
     * {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} for {@link ObjectNotFoundException}.
     */
    @Test
    public void testGetMessageForObjectNotFoundException() {
        ObjectNotFoundException exception = new ObjectNotFoundException("foo", "bar");
        checkModifiedByAnotherUser(exception);
    }

    /**
     * Verifies a '...modified by another user' message is returned by
     * {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} for {@link StaleStateException}.
     */
    @Test
    public void testGetMessageForStaleStateException() {
        HibernateOptimisticLockingFailureException exception = new HibernateOptimisticLockingFailureException(
                new StaleStateException("Batch update returned unexpected row count from update [0]; actual " +
                                        "row count: 0; expected: 1"));
        checkModifiedByAnotherUser(exception);
    }

    /**
     * Verifies a '...modified by another user' message is returned by
     * {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} for {@link UnexpectedRollbackException}.
     */
    @Test
    public void testGetMessageForUnexpectedRollbackException() {
        UnexpectedRollbackException exception = new UnexpectedRollbackException(
                "Transaction rolled back because it has been marked as rollback-only");
        checkModifiedByAnotherUser(exception);
    }

    /**
     * Verifies a generic message is returned by {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)}
     * for a {@link CannotAcquireLockException}.
     */
    @Test
    public void testGetMessageForCannotAcquireLockException() {
        SaveFailureFormatter formatter = new SaveFailureFormatter();
        IMObjectEditor editor = createEditor();

        CannotAcquireLockException exception = new CannotAcquireLockException(
                "could not execute batch",
                new LockAcquisitionException(
                        "could not execute batch",
                        new BatchUpdateException(
                                new MySQLTransactionRollbackException(
                                        "Deadlock found when trying to get lock; try restarting transaction"))));

        assertEquals("The Appointment could not be saved due to a deadlock. Please retry the operation.\n\n" +
                     "Your changes have been reverted.",
                     formatter.getMessage(editor, exception));
    }

    /**
     * Verifies a 'You do not have permission...' message is returned by
     * {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} for {@link ArchetypeAccessDeniedException}.
     */
    @Test
    public void testGetMessageForAccessDeniedException() {
        SaveFailureFormatter formatter = new SaveFailureFormatter();
        IMObjectEditor editor = createEditor();
        OpenVPMSAccessDeniedException exception = new OpenVPMSAccessDeniedException(
                OpenVPMSAccessDeniedException.ErrorCode.AccessDenied,
                new ArchetypeAccessDeniedException(PatientArchetypes.PATIENT_MEDICATION, "save"));
        assertEquals("You do not have permission to save instances of Medication.\n\n" +
                     "Your changes have been reverted.", formatter.getMessage(editor, exception));
    }

    /**
     * Verifies a '...modified by another user' message is returned by
     * {@link SaveFailureFormatter#getMessage(IMObjectEditor, Throwable)} for the supplied exception.
     *
     * @param exception the exception to check
     */
    private void checkModifiedByAnotherUser(Throwable exception) {
        SaveFailureFormatter formatter = new SaveFailureFormatter();
        IMObjectEditor editor = createEditor();
        assertEquals("The Appointment could not be saved. It may have been changed by another user.\n\n" +
                     "Your changes have been reverted.", formatter.getMessage(editor, exception));
    }

    /**
     * Creates an editor for an appointment.
     *
     * @return the appointment
     */
    private IMObjectEditor createEditor() {
        Act appointment = schedulingFactory.newAppointment()
                .build(false);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        return new DefaultIMObjectEditor(appointment, context);
    }
}