/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.model.party.Party;
import org.openvpms.macro.impl.LookupMacros;
import org.openvpms.report.ReportFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;


/**
 * {@link StringPropertyTransformer} test case.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class StringPropertyTransformerTestCase extends ArchetypeServiceTest {

    /**
     * The archetype functions factory.
     */
    @Autowired
    private ArchetypeFunctionsFactory functions;

    /**
     * The report factory.
     */
    @Autowired
    private ReportFactory reportFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Tests {@link StringPropertyTransformer#apply}.
     */
    @Test
    public void testApply() {
        Party person = customerFactory.newCustomer().build(false);
        NodeDescriptor descriptor = PropertyTestHelper.getDescriptor(person, "name");
        Property property = new IMObjectProperty(person, descriptor);
        StringPropertyTransformer handler
                = new StringPropertyTransformer(property);

        assertNull(handler.apply(null));
        assertNull(handler.apply(""));
        assertEquals("abc", handler.apply("abc"));

        assertEquals("1", handler.apply(1));
    }

    /**
     * Verifies that an exception is thrown if a string contains control
     * characters.
     */
    @Test
    public void testExceptionForControlChars() {
        String bad = "abcd\u000012345";
        Party person = customerFactory.newCustomer().build(false);
        NodeDescriptor descriptor = PropertyTestHelper.getDescriptor(person, "name");
        Property property = new IMObjectProperty(person, descriptor);
        StringPropertyTransformer handler
                = new StringPropertyTransformer(property);
        try {
            handler.apply(bad);
            fail("Expected PropertyException to be thrown");
        } catch (PropertyException expected) {
            // expected behaviour
        }
    }

    /**
     * Tests macro expansion by {@link StringPropertyTransformer#apply}.
     */
    @Test
    public void testMacroExpansion() {
        Party person = customerFactory.newCustomer().build(false);
        NodeDescriptor descriptor = PropertyTestHelper.getDescriptor(person, "lastName");
        Property property = new IMObjectProperty(person, descriptor);
        LookupMacros macros = new LookupMacros(getLookupService(), getArchetypeService(), reportFactory, functions);
        StringPropertyTransformer handler = new StringPropertyTransformer(property, macros);

        Object text1 = handler.apply("macro1");
        assertEquals("macro 1 text", text1);

        Object text2 = handler.apply("macro2");
        assertEquals("onetwothree", text2);

        Object text3 = handler.apply("macro1 macro2");
        assertEquals("macro 1 text onetwothree", text3);

        Object text4 = handler.apply("displayName");
        assertEquals("Customer", text4);

        // verifies that invalid macros don't expand
        Object text5 = handler.apply("invalidNode");
        assertEquals("invalidNode", text5);

        // verifies that non-existent macros don't expand
        Object text6 = handler.apply("non existent");
        assertEquals("non existent", text6);
        macros.destroy();
    }

    /**
     * Verifies that 4-byte characters trigger a PropertyException. These can't be stored by MySQL as the utf-8
     * encoding in 5.x only stores 3 byte characters.
     */
    @Test
    public void testUnsupportedChar() {
        String bad1 = "\uD83D\uDE00"; // smiley
        Party person = customerFactory.newCustomer().build(false);
        NodeDescriptor descriptor = PropertyTestHelper.getDescriptor(person, "name");
        Property property = new IMObjectProperty(person, descriptor);
        StringPropertyTransformer handler = new StringPropertyTransformer(property);
        try {
            handler.apply(bad1);
            fail("Expected PropertyException to be thrown");
        } catch (PropertyException expected) {
            // expected behaviour
            assertEquals("Name contains an unsupported character: \uD83D\uDE00", expected.getMessage());
        }

        String bad2 = "\uD83D"; // surrogate missing second char
        try {
            handler.apply(bad2);
            fail("Expected PropertyException to be thrown");
        } catch (PropertyException expected) {
            // expected behaviour
            assertEquals("Name contains an unsupported character: \uD83D", expected.getMessage());
        }
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        createMacro("macro1", "'macro 1 text'");
        createMacro("macro2", "concat('one', 'two', 'three')");
        createMacro("displayName", "openvpms:get(., 'displayName')");
        createMacro("exceptionMacro", "openvpms:get(., 'invalidnode')");
        createMacro("nested", "concat('nested test: ', $macro1)");
        createMacro("numbertest", "concat('input number: ', $number)");
    }

    /**
     * Helper to create and save a macro.
     *
     * @param code       the macro code
     * @param expression the macro expression
     */
    private void createMacro(String code, String expression) {
        lookupFactory.newMacro()
                .code(code)
                .name(code)
                .expression(expression)
                .build();
    }

}
