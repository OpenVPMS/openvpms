/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EntityDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class EntityDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;


    /**
     * Tests deletion of entities with entity relationships.
     */
    @Test
    public void testDeleteWithRelationships() {
        Party customer = TestHelper.createCustomer();

        EntityDeletionHandler<Party> customerHandler = createDeletionHandler(customer);
        assertTrue(customerHandler.getDeletable().canDelete());

        Party patient = TestHelper.createPatient(customer);
        Deletable deletable = customerHandler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Customer has relationships and cannot be deleted.", deletable.getReason());

        // verify the patient can be deleted, as it is the target of the relationship
        EntityDeletionHandler<Party> patientHandler = createDeletionHandler(patient);
        assertTrue(patientHandler.getDeletable().canDelete());

        patientHandler.delete(new LocalContext(), new HelpContext("foo", null));

        assertNull(get(patient));      // verify the patient is deleted
        customer = get(customer);      // verify the customer isn't
        assertNotNull(customer);

        // verify the customer can now be deleted, as it has no more relationships
        customerHandler = createDeletionHandler(customer);
        assertTrue(customerHandler.getDeletable().canDelete());
        customerHandler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(customer));
    }

    /**
     * Tests deletion of entities with entity links.
     */
    @Test
    public void testDeleteWithLinks() {
        Entity type = ProductTestHelper.createProductType();
        Product product = ProductTestHelper.createMedication(type);

        EntityDeletionHandler<Product> productHandler = createDeletionHandler(product);
        assertTrue(productHandler.getDeletable().canDelete());

        EntityDeletionHandler<Entity> typeHandler = createDeletionHandler(type);
        assertFalse(typeHandler.getDeletable().canDelete());

        productHandler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(product));
        assertNotNull(get(type));

        assertTrue(typeHandler.getDeletable().canDelete());
        typeHandler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(type));
    }

    /**
     * Tests deletion of an entity with participation relationships.
     */
    @Test
    public void testDeletionWithParticipations() {
        Party customer = TestHelper.createCustomer();
        List<FinancialAct> invoice = FinancialTestHelper.createChargesCounter(new BigDecimal("100"),
                                                                              customer, TestHelper.createProduct(),
                                                                              ActStatus.POSTED);
        save(invoice);     // customer has participation relationships to the charge

        EntityDeletionHandler<Party> customerHandler = createDeletionHandler(customer);
        Deletable deletable = customerHandler.getDeletable();
        assertFalse(deletable.canDelete());
        assertEquals("Customer has relationships and cannot be deleted.", deletable.getReason());
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link EntityDeletionHandler} for entities.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Entity entity = ProductTestHelper.createProductType();
        Party party = TestHelper.createCustomer(false);
        Product product = TestHelper.createProduct();

        assertTrue(factory.create(entity) instanceof EntityDeletionHandler);
        assertTrue(factory.create(party) instanceof EntityDeletionHandler);
        assertTrue(factory.create(product) instanceof EntityDeletionHandler);
    }

    /**
     * Creates a new deletion handler for an entity.
     *
     * @param entity the entity
     * @return a new deletion handler
     */
    protected <T extends Entity> EntityDeletionHandler<T> createDeletionHandler(T entity) {
        return new EntityDeletionHandler<>(entity, factory, ServiceHelper.getTransactionManager(),
                                           ServiceHelper.getArchetypeService());
    }

}
