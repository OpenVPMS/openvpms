/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.test;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.ListBox;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.Window;
import nextapp.echo2.app.WindowPane;
import nextapp.echo2.app.button.AbstractButton;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.list.ListModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.query.BrowserDialog;
import org.openvpms.web.component.im.table.IMTable;
import org.openvpms.web.echo.dialog.MessageDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.dialog.SelectionDialog;

import java.util.Objects;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Helper routines for Echo Web framework tests.
 *
 * @author Tim Anderson
 */
public class EchoTestHelper {

    /**
     * Finds the {@link BrowserDialog} with the highest z-index.
     *
     * @return the browser dialog, or {@code null} if none is found
     */
    public static BrowserDialog<?> findBrowserDialog() {
        return findWindowPane(BrowserDialog.class);
    }

    /**
     * Finds the {@link EditDialog} with the highest z-index.
     *
     * @return the edit dialog, or {@code null} if none is found
     */
    public static EditDialog findEditDialog() {
        return findWindowPane(EditDialog.class);
    }

    /**
     * Selects a value in a {@link SelectionDialog}.
     *
     * @param value the value to select
     */
    public static void findSelectionDialogAndSelect(Object value) {
        SelectionDialog selectionDialog = EchoTestHelper.getWindowPane(SelectionDialog.class);
        ListBox listBox = EchoTestHelper.findComponent(selectionDialog, ListBox.class);
        assertNotNull(listBox);
        EchoTestHelper.click(listBox, value);
    }

    /**
     * Finds the {@link WindowPane} with the highest z-index.
     *
     * @param type the window pane type to find
     * @return the window pane of the specified type, or {@code null} if none is found
     */
    public static <T extends WindowPane> T findWindowPane(Class<T> type) {
        Window root = ApplicationInstance.getActive().getDefaultWindow();
        int top = 0;
        T result = null;
        for (Component component : root.getContent().getComponents()) {
            if (type.isAssignableFrom(component.getClass())) {
                T pane = type.cast(component);
                int zIndex = pane.getZIndex();
                if (result == null || zIndex > top) {
                    result = pane;
                }
            }
        }
        return result;
    }

    /**
     * Returns the {@link WindowPane} with the highest z-index, asserting that it is present.
     *
     * @param type the window pane type to locate
     * @return the window pane
     */
    public static <T extends WindowPane> T getWindowPane(Class<T> type) {
        T result = findWindowPane(type);
        assertNotNull(result);
        return result;
    }

    /**
     * Helper to click a button on a dialog.
     *
     * @param dialog   the dialog
     * @param buttonId the button identifier
     */
    public static void fireDialogButton(PopupDialog dialog, String buttonId) {
        AbstractButton button = dialog.getButtons().getButton(buttonId);
        assertNotNull(button);
        fireButton(button);
    }

    /**
     * Helper to press a button.
     *
     * @param button the button
     */
    public static void fireButton(AbstractButton button) {
        assertTrue(button.isEnabled());
        button.fireActionPerformed(new ActionEvent(button, button.getActionCommand()));
    }

    /**
     * Simulates a user clicking a row in a table.
     *
     * @param table the table
     * @param row   the row
     */
    public static void fireSelection(Table table, int row) {
        table.processInput("selection", new int[]{row});
        table.processInput("action", null);
    }

    /**
     * Finds a message dialog, verifies its contents, and clicks one of its buttons.
     *
     * @param type     the dialog type
     * @param title    the expected dialog title
     * @param message  the expected dialog message
     * @param buttonId the button identifier
     */
    public static <T extends MessageDialog> void findMessageDialogAndFireButton(Class<T> type,
                                                                                String title, String message,
                                                                                String buttonId) {
        MessageDialog dialog = findWindowPane(type);
        assertNotNull(dialog);
        assertEquals(title, dialog.getTitle());
        assertEquals(message, dialog.getMessage());
        fireDialogButton(dialog, buttonId);
    }

    /**
     * Finds and fires a button with the specified id.
     *
     * @param component the parent component
     * @param buttonId  the button identifier
     */
    public static void fireButton(Component component, String buttonId) {
        Button button = findButton(component, buttonId);
        assertNotNull(button);
        button.fireActionPerformed(new ActionEvent(button, button.getActionCommand()));
    }

    /**
     * Finds a button with the specified id.
     *
     * @param component the parent component
     * @param buttonId  the button identifier
     * @return the button, or {@code null} if none is found
     */
    public static Button findButton(Component component, String buttonId) {
        return findComponent(component, Button.class, (c) -> StringUtils.equals(buttonId, c.getId()));
    }

    /**
     * Helper to find a component of the specified type.
     *
     * @param component the component to begin the search from
     * @param type      the type of the component to find
     * @return the first matching component, or {@code null} if none is found
     */
    public static <T extends Component> T findComponent(Component component, Class<T> type) {
        return findComponent(component, type, null);
    }

    /**
     * Helper to get a component of the specified type.
     *
     * @param component the component to begin the search from
     * @param type      the type of the component to find
     * @return the first matching component
     */
    public static <T extends Component> T getComponent(Component component, Class<T> type) {
        T result = findComponent(component, type);
        assertNotNull(result);
        return result;
    }

    /**
     * Helper to find a component of the specified type, optionally matching a predicate.
     *
     * @param component the component to begin the search from
     * @param type      the type of the component to find
     * @param predicate the predicate, or {@code null} if just matching on type
     * @return the first matching component, or {@code null} if none is found
     */
    public static <T extends Component> T findComponent(Component component, Class<T> type, Predicate<T> predicate) {
        Component result = null;
        if (type.isAssignableFrom(component.getClass())
            && (predicate == null || predicate.test(type.cast(component)))) {
            result = component;
        } else {
            for (Component child : component.getComponents()) {
                result = findComponent(child, type, predicate);
                if (result != null) {
                    break;
                }
            }
        }
        return type.cast(result);
    }

    /**
     * Helper to get a component of the specified type, optionally matching a predicate.
     *
     * @param component the component to begin the search from
     * @param type      the type of the component to find
     * @param predicate the predicate, or {@code null} if just matching on type
     * @return the first matching component
     */
    public static <T extends Component> T getComponent(Component component, Class<T> type, Predicate<T> predicate) {
        T result = findComponent(component, type, predicate);
        assertNotNull(result);
        return result;
    }

    /**
     * Simulates a click on a {@code ListBox}.
     *
     * @param list  the list box
     * @param value the value to select
     */
    public static void click(ListBox list, Object value) {
        ListModel model = list.getModel();
        int index = -1;
        for (int i = 0; i < model.size(); ++i) {
            if (Objects.equals(value, model.get(i))) {
                index = i;
                break;
            }
        }
        assertNotEquals("Value not found", -1, index);
        click(list, index);
    }

    /**
     * Simulates a click on a {@code ListBox}.
     *
     * @param list  the list box
     * @param index the index to select
     */
    public static void click(ListBox list, int index) {
        list.setSelectedIndex(index);
        list.processInput("action", null);
    }

    /**
     * Cancels a dialog.
     *
     * @param dialog    the dialog
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    public static void cancelDialog(PopupDialog dialog, boolean userClose) {
        if (userClose) {
            dialog.userClose();
        } else {
            fireDialogButton(dialog, PopupDialog.CANCEL_ID);
        }
    }

    /**
     * Returns the button to sort a column in a table.
     *
     * @param table the table
     * @param name  the column name
     * @return the corresponding button
     */
    public static Button getSortButton(IMTable<?> table, String name) {
        TableColumn column = getColumnWithHeader(table, name);

        // need to render the button
        Component component = renderColumnHeader(table, column);
        assertTrue(component instanceof Button);
        return (Button) component;
    }

    /**
     * Returns the column with matching column name.
     *
     * @param table the table
     * @param name  the column name
     * @return the corresponding column
     */
    public static TableColumn getColumnWithHeader(IMTable<?> table, String name) {
        TableColumnModel columnModel = table.getModel().getColumnModel();
        TableColumn column = null;
        for (int i = 0; i < columnModel.getColumnCount(); ++i) {
            TableColumn c = columnModel.getColumn(i);
            if (name.equals(c.getHeaderValue())) {
                column = c;
                break;
            }
        }
        assertNotNull(column);
        return column;
    }

    /**
     * Renders a table column header.
     *
     * @param table  the table
     * @param column the column
     * @return the rendered column
     */
    public static Component renderColumnHeader(Table table, TableColumn column) {
        return table.getDefaultHeaderRenderer().getTableCellRendererComponent(
                table, column.getHeaderValue(), column.getModelIndex(), 0);
    }
}
