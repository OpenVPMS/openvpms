/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.job;

import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.user.User;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link JobManager}.
 *
 * @author Tim Anderson
 */
public class JobManagerTestCase extends AbstractAppTest {

    /**
     * The job manager.
     */
    private final JobManager manager = new JobManager(500, "TestJob-%d") {

    };

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Test user.
     */
    private User user;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        user = userFactory.newUser().build(false);
    }

    /**
     * Cleans up after the test.
     */
    @After
    public void tearDown() {
        manager.destroy();
    }

    /**
     * Tests the {@link JobManager#run(Job)}, with the job runs to completion.
     */
    @Test
    public void testRun() {
        TestJob<?> job = new TestJob<>(newJob().build());
        Future<?> future = manager.run(job);
        try {
            future.get();
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        job.waitForListener();
        job.checkCompleted(null);
    }

    /**
     * Verifies the result of {@link Job#get()} is passed to the {@link Job#getCompletionListener()}.
     */
    @Test
    public void testRunGet() {
        Integer initial = RandomUtils.nextInt();
        AtomicReference<Integer> completedValue = new AtomicReference<>();

        Job<Integer> job = JobBuilder.<Integer>newJob("test", user)
                .get(() -> initial)
                .completed(completedValue::set)
                .build();
        TestJob<Integer> testJob = new TestJob<>(job);
        Future<?> future = manager.run(testJob);
        try {
            assertEquals(initial, future.get());
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        testJob.waitForListener();
        testJob.checkCompleted(null);
    }

    /**
     * Tests the {@link JobManager#run(Job)}, method when the job fails.
     */
    @Test
    public void testRunFailure() {
        IllegalStateException expected = new IllegalStateException("Simulated failure");
        Job<?> job = newJob()
                .run(() -> {
                    throw expected;
                })
                .failed((throwable) -> {
                })   // suppress default logging
                .build();

        TestJob<?> testJob = new TestJob<>(job);
        Future<?> future = manager.run(testJob);
        try {
            future.get();
            fail("Expected job to fail");
        } catch (Throwable exception) {
            assertTrue(exception instanceof ExecutionException);
            assertEquals(expected, exception.getCause());
        }
        testJob.waitForListener();
        testJob.checkFailure(expected, null);
    }

    /**
     * Verifies that if the future returned by {@link JobManager#run(Job)} is cancelled,
     * the {@link Job#getCancellationListener()} is called.
     */
    @Test
    public void testCancelRun() {
        CountDownLatch latch1 = new CountDownLatch(1);
        CountDownLatch latch2 = new CountDownLatch(1);
        Job<?> job = newJob()
                .run(() -> {
                    latch1.countDown();
                    sleep(10000);
                    try {
                        latch2.await();
                    } catch (InterruptedException ignore) {
                    }
                })
                .build();
        TestJob<?> testJob = new TestJob<>(job);
        Future<?> future = manager.run(testJob);
        try {
            latch1.await();  // wait for future to run before interrupting
            future.cancel(true);
            latch2.countDown();  // give the future opportunity to cancel. Not possible if the job returns beforehand
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        testJob.waitForListener();
        testJob.checkCancelled(null);
    }

    /**
     * Tests the {@link JobManager#runInteractive(Job, String, String)} method.
     * <p/>
     * Verifies that the {@link Job#getCompletionListener()} is invoked in the UI thread.
     */
    @Test
    public void testRunInteractive() {
        TestJob<?> job = new TestJob<>(newJob().build());
        Future<?> future = manager.runInteractive(job, "Cancel", "Cancel job");
        try {
            future.get();
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        sleep(1000); // need to sleep as listeners run
        processQueuedTasks(); // need to simulate UI thread
        job.waitForListener();
        job.checkCompleted(Thread.currentThread());
    }

    /**
     * Verifies the result of {@link Job#get()} is passed to the {@link Job#getCompletionListener()}
     * by {@link JobManager#runInteractive(Job, String, String)}, and that the listener runs in the
     * UI thread.
     */
    @Test
    public void testRunInteractiveGet() {
        Integer initial = RandomUtils.nextInt();
        AtomicReference<Integer> completedValue = new AtomicReference<>();

        Job<Integer> job = JobBuilder.<Integer>newJob("test", user)
                .get(() -> initial)
                .completed(completedValue::set)
                .build();
        TestJob<Integer> testJob = new TestJob<>(job);
        Future<?> future = manager.runInteractive(testJob, "Cancel", "Cancel job");
        try {
            assertEquals(initial, future.get());
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        sleep(1000);
        processQueuedTasks();                           // need to simulate UI thread
        testJob.waitForListener();
        testJob.checkCompleted(Thread.currentThread()); // listener should be invoked in UI thread
    }

    /**
     * Tests the {@link JobManager#runInteractive(Job, String, String)} method when the job fails,
     * verifying the failure listener is called in the UI thread.
     */
    @Test
    public void testRunInteractiveFailure() {
        IllegalStateException expected = new IllegalStateException("Simulated failure");
        Job<?> job = newJob()
                .run(() -> {
                    throw expected;
                })
                .failed((throwable) -> {
                })   // suppress default logging
                .build();

        TestJob<?> testJob = new TestJob<>(job);
        Future<?> future = manager.runInteractive(testJob, "Cancel", "Cancel job");
        try {
            future.get();
            fail("Expected job to fail");
        } catch (Throwable exception) {
            assertTrue(exception instanceof ExecutionException);
            assertEquals(expected, exception.getCause());
        }
        sleep(1000);
        processQueuedTasks(); // need to simulate UI thread
        testJob.waitForListener();
        testJob.checkFailure(expected, Thread.currentThread());
    }

    /**
     * Verifies that if the future returned by {@link JobManager#runInteractive(Job, String, String)} is
     * cancelled, the {@link Job#getCancellationListener()} is called in the UI thread.
     */
    @Test
    public void testRunInteractiveCancel() {
        CountDownLatch latch = new CountDownLatch(1);
        Job<?> job = newJob()
                .run(() -> {
                    latch.countDown();
                    sleep(10000);
                })
                .build();
        TestJob<?> testJob = new TestJob<>(job);
        Future<?> future = manager.runInteractive(testJob, "Cancel", "Cancel job");
        try {
            latch.await();  // wait for future to run before interrupting
            future.cancel(true);
        } catch (Throwable exception) {
            fail("Job failed with " + exception.getMessage());
        }
        sleep(1000);
        processQueuedTasks(); // need to simulate UI thread
        testJob.waitForListener();
        testJob.checkCancelled(Thread.currentThread());
    }

    /**
     * Verifies that the job inherits the security context of the parent thread.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSecurityContextInherited() throws Exception {
        AuthenticationContextImpl context = new AuthenticationContextImpl();
        Thread parent = Thread.currentThread();
        context.setUser(user);
        Job<User> job = JobBuilder.<User>newJob("test", user)
                .get(() -> {
                    assertNotEquals(parent, Thread.currentThread());
                    return new AuthenticationContextImpl().getUser();
                })
                .build();
        Future<User> future = manager.run(job);
        assertEquals(user, future.get());
        context.setUser(null);
    }

    /**
     * Creates a job builder.
     *
     * @return a job builder
     */
    private <T> JobBuilder<T> newJob() {
        return JobBuilder.newJob("test", user);
    }

    private static class TestJob<T> extends AbstractJob<T> {

        /**
         * The job to delegate to.
         */
        private final Supplier<T> command;

        /**
         * The completion listener.
         */
        private final Consumer<T> completionListener;

        /**
         * The cancellation listener.
         */
        private final Runnable cancellationListener;

        /**
         * The failure listener.
         */
        private final Consumer<Throwable> failureListener;

        /**
         * Run counter.
         */
        private final AtomicInteger runCounter = new AtomicInteger();

        /**
         * Completion counter.
         */
        private final AtomicInteger completionCounter = new AtomicInteger();

        /**
         * The thread that invoked the completion listener.
         */
        private final AtomicReference<Thread> completionThread = new AtomicReference<>();

        /**
         * Cancellation counter.
         */
        private final AtomicInteger cancellationCounter = new AtomicInteger();

        /**
         * The thread that invoked the cancellation listener.
         */
        private final AtomicReference<Thread> cancellationThread = new AtomicReference<>();

        /**
         * Failure counter.
         */
        private final AtomicInteger failureCounter = new AtomicInteger();

        /**
         * Exception caught by the failure listener.
         */
        private final AtomicReference<Throwable> caught = new AtomicReference<>();

        /**
         * The thread that invoked the failure listener.
         */
        private final AtomicReference<Thread> failureThread = new AtomicReference<>();

        /**
         * Semaphore used to signal completion of listeners.
         */
        private final Semaphore semaphore = new Semaphore(0);

        /**
         * Constructs a {@link TestJob}.
         *
         * @param job the job to delegate to
         */
        public TestJob(Job<T> job) {
            super(job.getName(), job.getUser());
            this.command = job;
            this.completionListener = value -> runListener(completionCounter, completionThread, () -> {
                if (job.getCompletionListener() != null) {
                    job.getCompletionListener().accept(value);
                }
            });
            this.cancellationListener = () -> runListener(cancellationCounter, cancellationThread, () -> {
                if (job.getCancellationListener() != null) {
                    job.getCancellationListener().run();
                }
            });
            this.failureListener = (throwable) -> runListener(failureCounter, failureThread, () -> {
                caught.set(throwable);
                if (job.getFailureListener() != null) {
                    job.getFailureListener().accept(throwable);
                }
            });
        }

        /**
         * Returns the listener to invoke when the job completes successfully.
         *
         * @return the completion listener. May be {@code null}
         */
        @Override
        public Consumer<T> getCompletionListener() {
            return completionListener;
        }

        /**
         * Returns the listener to invoke when the job is cancelled.
         *
         * @return the cancellation listener. May be {@code null}
         */
        @Override
        public Runnable getCancellationListener() {
            return cancellationListener;
        }

        /**
         * Returns the listener top invoke when the job fails.
         *
         * @return the failure listener. May be {@code null}
         */
        @Override
        public Consumer<Throwable> getFailureListener() {
            return failureListener;
        }

        /**
         * Waits for the listener to complete.
         */
        public void waitForListener() {
            try {
                assertTrue(semaphore.tryAcquire(10, TimeUnit.SECONDS));
            } catch (InterruptedException ignore) {
                // do nothing
            }
        }

        /**
         * Verifies that the job completed successfully.
         *
         * @param thread the expected listener thread, or {@code null} if it can be any thread
         */
        public void checkCompleted(Thread thread) {
            checkNoCancelDialog();
            assertEquals(1, runCounter.get());
            assertEquals(1, completionCounter.get());
            assertEquals(0, cancellationCounter.get());
            assertEquals(0, failureCounter.get());
            assertNull(caught.get());
            if (thread != null) {
                assertEquals(thread, completionThread.get());
            }
        }

        /**
         * Verifies that the job was cancelled.
         *
         * @param thread the expected listener thread, or {@code null} if it can be any thread
         */
        public void checkCancelled(Thread thread) {
            checkNoCancelDialog();
            assertEquals(1, runCounter.get());
            assertEquals(0, completionCounter.get());
            assertEquals(1, cancellationCounter.get());
            assertEquals(0, failureCounter.get());
            assertNull(caught.get());
            if (thread != null) {
                assertEquals(thread, cancellationThread.get());
            }
        }

        /**
         * Verifies that the job failed.
         *
         * @param exception the expected exception
         * @param thread    the expected listener thread, or {@code null} if it can be any thread
         */
        public void checkFailure(Throwable exception, Thread thread) {
            checkNoCancelDialog();
            assertEquals(1, runCounter.get());
            assertEquals(0, completionCounter.get());
            assertEquals(0, cancellationCounter.get());
            assertEquals(1, failureCounter.get());
            assertEquals(exception, caught.get());
            if (thread != null) {
                assertEquals(thread, failureThread.get());
            }
        }

        /**
         * Runs the job.
         */
        @Override
        protected T runJob() {
            runCounter.incrementAndGet();
            return command.get();
        }

        /**
         * Verifies that no CancelDialog is displayed.
         */
        private void checkNoCancelDialog() {
            assertNull(EchoTestHelper.findWindowPane(JobManager.CancelDialog.class));
        }

        /**
         * Runs a listener.
         *
         * @param counter  the counter to increment
         * @param thread   the thread to update
         * @param listener the listener to run
         */
        private void runListener(AtomicInteger counter, AtomicReference<Thread> thread, Runnable listener) {
            counter.incrementAndGet();
            thread.set(Thread.currentThread());
            try {
                listener.run();
            } finally {
                semaphore.release();
            }
        }
    }
}
