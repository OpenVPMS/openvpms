/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Test;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.NEEDS_UPDATE;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.UP_TO_DATE;


/**
 * Tests the {@link DocumentActEditor} class, for templated, versioned archetypes.
 *
 * @author Tim Anderson
 */
public abstract class TemplatedVersionedDocumentActEditorTest<T extends DocumentActEditor>
        extends VersionedDocumentActEditorTest<T> {

    /**
     * The edit dialog factory.
     */
    @Autowired
    private EditDialogFactory dialogFactory;

    /**
     * Constructs a {@link TemplatedVersionedDocumentActEditorTest}.
     *
     * @param type      the editor type
     * @param archetype the archetype that the editor supports
     */
    public TemplatedVersionedDocumentActEditorTest(Class<T> type, String archetype) {
        super(type, archetype);
    }

    /**
     * Verifies that a document is generated when a template is associated with an <em>act.patientDocumentLetter</em>,
     * and that this is deleted when the act is deleted.
     */
    @Test
    public void testDocGenerationFromTemplate() {
        DocumentAct act = createAct();
        Entity template = createDocumentTemplate(act.getArchetype());
        DocumentActEditor editor = createEditor(act);
        editor.getComponent();

        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        editor.setTemplate(template);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> act.getDocument() != null);
        Reference docRef = act.getDocument();
        assertNotNull(docRef);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        assertTrue(save(editor));

        // verify the document was generated and saved
        Document doc = (Document) get(docRef);
        assertNotNull(doc);
        assertEquals(DocFormats.PDF_TYPE, doc.getMimeType());
        assertEquals("blank.pdf", doc.getName());

        // now delete the act and verify both it and the document were deleted
        delete(editor);
        assertNull(get(act));
        assertNull(get(docRef));
    }

    /**
     * Verifies that:
     * <ol>
     * <li> a document is generated when a template is associated with an <em>act.patientDocumentLetter</em>; and
     * <li>subsequent setting a new template generates a new document, versioning the original one; and
     * <li>all are deleted when the parent is deleted
     * </ol>
     */
    @Test
    public void testGenerationWithVersioning() {
        DocumentAct act = createAct();
        Entity template1 = createDocumentTemplate(act.getArchetype());
        Entity template2 = createDocumentTemplate(act.getArchetype());
        DocumentActEditor editor = createEditor(act);
        editor.getComponent();

        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        editor.setTemplate(template1);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> act.getDocument() != null);
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        assertTrue(save(editor));

        editor.setTemplate(template2);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> !Objects.equals(act.getDocument(), docRef1));
        Reference docRef2 = act.getDocument();
        assertNotNull(docRef2);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        assertTrue(save(editor));

        List<DocumentAct> versions = getVersions(act);
        assertEquals(1, versions.size());
        DocumentAct version1 = checkVersion(versions, 0, docRef1);

        // now delete the act and verify both it and the document were deleted
        delete(editor);
        assertNull(get(act));
        assertNull(get(version1));
        assertNull(get(docRef1));
        assertNull(get(docRef2));
    }

    /**
     * Verifies that the {@link EditDialogFactory} returns an {@link DocumentActEditDialog} for the editor.
     */
    @Test
    public void testEditDialogFactory() {
        DocumentAct act = createAct();
        EditDialog dialog = dialogFactory.create(createEditor(act), new LocalContext());
        assertTrue(dialog instanceof DocumentActEditDialog);
    }

    /**
     * Returns the versions of an act, sorted on increasing id.
     *
     * @param act the act
     * @return the act versions
     */
    protected List<DocumentAct> getVersions(DocumentAct act) {
        IMObjectBean bean = getBean(act);
        List<DocumentAct> versions = bean.getTargets("versions", DocumentAct.class);
        versions.sort(Comparator.comparingLong(DocumentAct::getId));
        return versions;
    }

    /**
     * Verifies a version matches that expected and points to a document.
     *
     * @param versions the available versions
     * @param index    the version index
     * @param docRef   the expected document reference
     * @return the version
     */
    protected DocumentAct checkVersion(List<DocumentAct> versions, int index, Reference docRef) {
        assertTrue(index < versions.size());
        DocumentAct version = versions.get(index);
        assertEquals(docRef, version.getDocument());
        assertNotNull(get(docRef));
        return version;
    }
}
