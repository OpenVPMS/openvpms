/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import org.junit.Test;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.checkEquals;


/**
 * Tests the {@link MoneyPropertyTransformer} class.
 *
 * @author Tim Anderson
 */
public class MoneyPropertyTransformerTestCase {

    /**
     * Tests {@link MoneyPropertyTransformer#apply}.
     */
    @Test
    public void testApply() {
        SimpleProperty property = new SimpleProperty("money", Money.class);
        MoneyPropertyTransformer handler = new MoneyPropertyTransformer(property);

        // test string conversions
        try {
            handler.apply("abc");
            fail("expected conversion from 'abc' to fail");
        } catch (PropertyException expected) {
            assertEquals(property, expected.getProperty());
        }

        checkEquals(ONE, (BigDecimal) handler.apply("1"));

        // test numeric conversions
        checkEquals(ONE, (BigDecimal) handler.apply(1L));
        checkEquals(ONE, (BigDecimal) handler.apply(new BigDecimal("1.0")));
        checkEquals(new BigDecimal("1.5"), (BigDecimal) handler.apply(1.5));
    }

    /**
     * Verifies that an optional money can be left empty.
     */
    @Test
    public void testOptionalMoney() {
        SimpleProperty property = new SimpleProperty("benefit", Money.class);
        property.setRequired(false);
        MoneyPropertyTransformer handler = new MoneyPropertyTransformer(property);

        assertNull(handler.apply(""));
        assertNull(handler.apply(null));
        checkEquals(ONE, (BigDecimal) handler.apply("1"));
    }

}
