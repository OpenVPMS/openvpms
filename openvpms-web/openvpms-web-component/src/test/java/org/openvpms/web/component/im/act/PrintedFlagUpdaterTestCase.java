/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.act;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.supplier.OrderStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * Tests the {@link PrintedFlagUpdater}.
 *
 * @author Tim Anderson
 */
public class PrintedFlagUpdaterTestCase extends AbstractAppTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * Tests the {@link PrintedFlagUpdater#setPrinted(Act)} method.
     */
    @Test
    public void testSetPrinted() {
        DocumentAct act = patientFactory.createForm(patientFactory.createPatient());
        checkPrinted(act, false);

        PrintedFlagUpdater updater = new PrintedFlagUpdater();
        Act updated = updater.setPrinted(act);
        assertNotNull(updated);
        assertSame(act, updated);

        checkPrinted(act, true);

        // verify the act isn't updated when called a second time, as it's already set
        assertNull(updater.setPrinted(act));
        checkPrinted(act, true);
    }

    /**
     * Verifies that the printed flag can be set even if the object has been subsequently modified by another user.
     */
    @Test
    public void testSetPrintedForModifiedObject() {
        DocumentAct original = patientFactory.createForm(patientFactory.createPatient());

        DocumentAct updated = get(original);   // simulate another user modifying object
        updated.setDescription("A description to force a version change");
        save(updated);
        assertNotEquals(original.getVersion(), updated.getVersion());

        checkPrinted(updated, false);

        PrintedFlagUpdater updater = new PrintedFlagUpdater();  // now verify the original can be updated
        updater.setPrinted(original);

        Act reloaded = get(original);
        checkPrinted(reloaded, true);
        assertEquals("A description to force a version change", reloaded.getDescription());
    }

    /**
     * Verifies that if an object has been deleted, attempting to set its printed flag doesn't fail.
     * <p/>
     * This is because failure to update a printed flag shouldn't terminate any workflows.
     */
    @Test
    public void testSetPrintedOnDeletedObject() {
        DocumentAct act = patientFactory.createForm(patientFactory.createPatient());
        remove(act);
        PrintedFlagUpdater updater = new PrintedFlagUpdater();  // now verify the original can be updated
        assertNull(updater.setPrinted(act));
    }

    /**
     * Verifies that the printed flag is only set on POSTED customer account acts.
     */
    @Test
    public void testSetPrintedOnAccountAct() {
        Party customer = customerFactory.createCustomer();
        FinancialAct invoice = accountFactory.newInvoice()
                .customer(customer)
                .status(ActStatus.IN_PROGRESS)
                .build();
        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .status(ActStatus.IN_PROGRESS)
                .cash(100)
                .build();
        checkPrintFinancialAct(invoice);
        checkPrintFinancialAct(payment);
    }

    /**
     * Verifies that the printed flag is only set on POSTED estimates.
     */
    @Test
    public void testSetPrintOnEstimate() {
        Party customer = customerFactory.createCustomer();
        Act estimate = accountFactory.newEstimate()
                .customer(customer)
                .status(ActStatus.IN_PROGRESS)
                .item().patient(patientFactory.createPatient()).product(productFactory.createMedication()).add()
                .build();
        checkPrintFinancialAct(estimate);
    }

    /**
     * Verifies that the printed flag is only set on POSTED or CANCELLED supplier acts by default.
     */
    @Test
    public void testSetPrintOnSupplierActs() {
        practiceFactory.getPractice();  // make sure a practice is available
        Party supplier = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        FinancialAct order = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .status(ActStatus.IN_PROGRESS)
                .item().product(productFactory.createMerchandise()).add()
                .build();
        FinancialAct delivery = supplierFactory.newDelivery()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .status(ActStatus.IN_PROGRESS)
                .item().product(productFactory.createMerchandise()).add()
                .build();
        checkPrintSupplierAct(order);
        checkPrintSupplierAct(delivery);
    }

    /**
     * Verifies that additional final statuses can be supplied that enable the printed flag to be updated.
     */
    @Test
    public void testFinalStatuses() {
        String[] finalStatuses = {OrderStatus.POSTED, OrderStatus.CANCELLED, OrderStatus.REJECTED,
                                  OrderStatus.ACCEPTED};
        PrintedFlagUpdater updater = new PrintedFlagUpdater(finalStatuses);

        FinancialAct act = supplierFactory.newOrder()
                .supplier(supplierFactory.createSupplier())
                .stockLocation(practiceFactory.createStockLocation())
                .status(OrderStatus.IN_PROGRESS)
                .item().product(productFactory.createMerchandise()).add()
                .build();

        assertNull(updater.setPrinted(act));
        checkPrinted(get(act), false);

        setStatus(act, OrderStatus.COMPLETED);
        assertNull(updater.setPrinted(act));
        checkPrinted(get(act), false);

        for (String status : finalStatuses) {
            setStatus(act, status);
            assertNotNull(updater.setPrinted(act));
            checkPrinted(get(act), true);
        }
    }

    /**
     * Verifies that for financial acts, the print flag is only set if the act is {@code POSTED}.
     *
     * @param act the act
     */
    private void checkPrintFinancialAct(Act act) {
        PrintedFlagUpdater updater = new PrintedFlagUpdater();
        assertNull(updater.setPrinted(act));
        checkPrinted(get(act), false);

        act.setStatus(ActStatus.POSTED);
        save(act);
        assertNotNull(updater.setPrinted(act));
        checkPrinted(get(act), true);
    }

    /**
     * Verifies that for financial acts, the print flag is only set if the act is {@code POSTED} or {@code CANCELLED}.
     *
     * @param act the act
     */
    private void checkPrintSupplierAct(Act act) {
        PrintedFlagUpdater updater = new PrintedFlagUpdater();
        assertNull(updater.setPrinted(act));
        checkPrinted(get(act), false);

        act.setStatus(ActStatus.POSTED);
        save(act);
        assertNotNull(updater.setPrinted(act));
        checkPrinted(get(act), true);

        setStatus(act, ActStatus.CANCELLED);
        assertNotNull(updater.setPrinted(act));
        checkPrinted(get(act), true);
    }

    /**
     * Updates an act to set its status, and unset the printed flag.
     *
     * @param act    the act
     * @param status the new status
     */
    private void setStatus(Act act, String status) {
        IMObjectBean bean = getBean(act);
        bean.setValue("printed", false);
        act.setStatus(status);
        bean.save();
    }

    /**
     * Checks the printed flag of an act.
     *
     * @param act      the act
     * @param expected the expected value of the flag
     */

    private void checkPrinted(Act act, boolean expected) {
        IMObjectBean bean = getBean(act);
        assertEquals(expected, (bean.getBoolean("printed")));
    }
}