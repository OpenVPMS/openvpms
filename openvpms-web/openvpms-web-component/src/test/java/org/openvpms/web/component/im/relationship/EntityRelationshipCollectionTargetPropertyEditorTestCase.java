/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.relationship;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.service.archetype.helper.IMObjects;
import org.openvpms.component.model.archetype.ArchetypeRange;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractCollectionPropertyEditorTest;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.property.CollectionProperty;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Tests the {@link EntityRelationshipCollectionTargetPropertyEditor} class.
 *
 * @author Tim Anderson
 */
public class EntityRelationshipCollectionTargetPropertyEditorTestCase
        extends AbstractCollectionPropertyEditorTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests the {@link EntityRelationshipCollectionTargetPropertyEditor#getArchetypeRange()} method.
     */
    @Test
    public void testGetArchetypeRange() {
        IMObject parent = createParent();
        CollectionProperty property = getCollectionProperty(parent);
        CollectionPropertyEditor editor = createEditor(property, parent, getObjects());
        String[] archetypes = editor.getArchetypeRange();
        assertEquals(1, archetypes.length);
        assertEquals("party.patientpet", archetypes[0]);
    }

    /**
     * Tests the {@link EntityRelationshipCollectionTargetPropertyEditor#getArchetypes()} method.
     */
    @Test
    public void testGetArchetypes() {
        IMObject parent = createParent();
        CollectionProperty property = getCollectionProperty(parent);
        CollectionPropertyEditor editor = createEditor(property, parent, getObjects());
        ArchetypeRange range = editor.getArchetypes();
        List<String> archetypes = range.getArchetypes();
        assertEquals(1, archetypes.size());
        assertEquals("party.patientpet", archetypes.get(0));
        assertNull(range.getDefaultArchetype());
    }

    /**
     * Returns the parent of the collection.
     *
     * @return the parent object
     */
    protected IMObject createParent() {
        return customerFactory.createCustomer();
    }

    /**
     * Returns the name of the collection node.
     *
     * @return the node name
     */
    protected String getCollectionNode() {
        return "patients";
    }

    /**
     * Returns an editor for a collection property.
     *
     * @param property the collection property
     * @param parent   the parent of the collection
     * @param objects  the objects service
     * @return a new editor for the property
     */
    protected CollectionPropertyEditor createEditor(CollectionProperty property, IMObject parent, IMObjects objects) {
        return new EntityRelationshipCollectionTargetPropertyEditor(property, (Entity) parent, objects);
    }

    /**
     * Returns an object to add to the collection.
     *
     * @param parent the parent of the collection
     * @return a new object to add to the collection
     */
    protected IMObject createObject(IMObject parent) {
        return patientFactory.createPatient();
    }

    /**
     * Makes an object valid or invalid.
     *
     * @param object the object
     * @param valid  if {@code true}, make it valid, otherwise make it invalid
     */
    @Override
    protected void makeValid(IMObject object, boolean valid) {
        IMObjectBean bean = getBean(object);
        bean.setValue("species", valid ? "CANINE" : null);
    }

    /**
     * Modify an object so that its version will increment on save.
     *
     * @param object the object to modify
     */
    @Override
    protected void modify(IMObject object) {
        IMObjectBean bean = getBean(object);
        String current = bean.getString("species");
        if ("CANINE".equals(current)) {
            bean.setValue("species", "FELINE");
        } else {
            bean.setValue("species", "CANINE");
        }
    }
}