/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link EFTPaymentItemEditor}.
 *
 * @author Tim Anderson
 */
public class EFTPaymentItemEditorTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies that the editor is invalid if the parent act is POSTED. This is because the parent can't update
     * the balance until EFT transactions are APPROVED.
     */
    @Test
    public void testValidationForPostedParentWithInProgressTransaction() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = createEFTPOSPayment(customer, terminal, practiceFactory.createLocation(),
                                                       EFTPOSTransactionStatus.IN_PROGRESS);
        FinancialAct item = createEFTPaymentItem(transaction);

        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .status(ActStatus.POSTED)
                .add(item)
                .build(false);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        EFTPaymentItemEditor editor = new EFTPaymentItemEditor(item, payment, context);
        assertInvalid(editor, "Cannot finalise Payment. It has an outstanding EFTPOS transaction.");

        payment.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(editor.isValid());
    }

    /**
     * Tests the {@link EFTPaymentItemEditor#canDelete()} and {@link EFTPaymentItemEditor#getDeletionStatus()}
     * methods.
     */
    @Test
    public void testCanDelete() {
        Party customer = customerFactory.createCustomer();
        Party location = practiceFactory.createLocation();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = createEFTPOSPayment(customer, terminal, practiceFactory.createLocation(),
                                                       EFTPOSTransactionStatus.PENDING);
        FinancialAct item = createEFTPaymentItem(transaction);
        FinancialAct payment = createPayment(customer, location, practiceFactory.createTill(), item);

        checkCanDelete(item, payment, transaction, EFTPOSTransactionStatus.PENDING, false);
        checkCanDelete(item, payment, transaction, EFTPOSTransactionStatus.IN_PROGRESS, false);
        checkCanDelete(item, payment, transaction, EFTPOSTransactionStatus.APPROVED, false);
        checkCanDelete(item, payment, transaction, EFTPOSTransactionStatus.ERROR, true);
        checkCanDelete(item, payment, transaction, EFTPOSTransactionStatus.DECLINED, true);
    }

    /**
     * Tests the {@link EFTPaymentItemEditor#requiresEFTTransaction()} method.
     */
    @Test
    public void testRequiresEFTTransaction() {
        Party customer = customerFactory.createCustomer();
        Party location = practiceFactory.createLocation();
        // verify that no EFT transaction is required if a till has no EFTPOS terminals
        Entity till1 = practiceFactory.createTill();
        FinancialAct item1 = createEFTPaymentItem();
        FinancialAct payment1 = createPayment(customer, location, till1, item1);
        assertEquals(ActStatus.IN_PROGRESS, payment1.getStatus());
        EFTPaymentItemEditor editor1 = createEditor(item1, payment1);
        assertFalse(editor1.requiresEFTTransaction());

        // verify that an EFT transaction is required if a till has an EFTPOS terminal, and none is selected
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Entity till2 = practiceFactory.newTill()
                .terminals(terminal)
                .build();
        FinancialAct item2 = createEFTPaymentItem();
        FinancialAct payment2 = createPayment(customer, location, till2, item2);
        EFTPaymentItemEditor editor2 = createEditor(item2, payment2);
        assertTrue(editor2.requiresEFTTransaction());

        // verifies that an EFT transaction is required if there is one present, but not approved
        checkRequiresEFTTransaction(EFTPOSTransactionStatus.PENDING, true);
        checkRequiresEFTTransaction(EFTPOSTransactionStatus.IN_PROGRESS, true);
        checkRequiresEFTTransaction(EFTPOSTransactionStatus.DECLINED, true);
        checkRequiresEFTTransaction(EFTPOSTransactionStatus.ERROR, true);
        checkRequiresEFTTransaction(EFTPOSTransactionStatus.APPROVED, false);
    }

    /**
     * Verifies that {@link EFTPaymentItemEditor#requiresEFTTransaction()} returns {@code false} if the
     * payment is saved and POSTED.
     * <p/>
     * Can only administratively edit payments; no EFT changes should be allowed.
     */
    @Test
    public void testRequiresEFTTransactionForPostedAct() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Party location = practiceFactory.createLocation();
        Entity till = practiceFactory.newTill()
                .terminals(terminal)
                .build();

        FinancialAct item1 = createEFTPaymentItem();
        FinancialAct payment1 = createPayment(customer, location, till, item1);
        payment1.setStatus(FinancialActStatus.POSTED);
        save(payment1);
        EFTPaymentItemEditor editor1 = createEditor(item1, payment1);
        assertFalse(editor1.requiresEFTTransaction());
    }

    /**
     * Verifies a No Terminal payment can be made.
     */
    @Test
    public void testNoTerminalPayment() {
        Party customer = customerFactory.createCustomer();
        Party location = practiceFactory.createLocation();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Entity till = practiceFactory.newTill()
                .terminals(terminal)
                .build();

        FinancialAct item1 = createEFTPaymentItem();
        FinancialAct payment = createPayment(customer, location, till, item1);
        checkNoTerminalPaymentRefund(payment, item1, location);
    }

    /**
     * Verifies a No Terminal refund can be made.
     */
    @Test
    public void testNoTerminalRefund() {
        Party customer = customerFactory.createCustomer();
        Party location = practiceFactory.createLocation();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        Entity till = practiceFactory.newTill()
                .terminals(terminal)
                .build();

        FinancialAct item1 = createEFTRefundItem();
        FinancialAct refund = createRefund(customer, location, till, item1);
        checkNoTerminalPaymentRefund(refund, item1, location);
    }

    /**
     * Tests the {@link EFTPaymentItemEditor#requiresEFTTransaction()}.
     *
     * @param status   the EFT transaction status
     * @param required determines if a transaction is required
     */
    private void checkRequiresEFTTransaction(String status, boolean required) {
        Party customer = customerFactory.createCustomer();
        Party location = practiceFactory.createLocation();
        Entity terminal = practiceFactory.createEFTPOSTerminal();

        // verify that no EFT transaction is required if a till has no EFTPOS terminals
        FinancialAct transaction = createEFTPOSPayment(customer, terminal, practiceFactory.createLocation(), status);
        Entity till = practiceFactory.newTill()
                .terminals(terminal)
                .build();
        FinancialAct item = createEFTPaymentItem(transaction);
        FinancialAct payment = createPayment(customer, location, till, item);
        EFTPaymentItemEditor editor = createEditor(item, payment);
        assertEquals(required, editor.requiresEFTTransaction());
    }

    /**
     * Verifies a No Terminal payment or refund can be made.
     *
     * @param act      the payment/refund
     * @param item     the payment/refund item
     * @param location the expected location
     */
    private void checkNoTerminalPaymentRefund(FinancialAct act, FinancialAct item, Party location) {
        assertNull(getBean(item).getTarget("eft", FinancialAct.class));
        EFTPaymentItemEditor editor = createEditor(item, act);
        assertTrue(editor.requiresEFTTransaction());
        assertTrue(editor.noTerminalTransactionRequired());

        editor.eft(() -> {
        });
        assertFalse(editor.requiresEFTTransaction());
        FinancialAct transaction = getBean(item).getTarget("eft", FinancialAct.class);
        assertNotNull(transaction);
        if (item.isA(CustomerAccountArchetypes.PAYMENT_EFT)) {
            assertEquals(EFTPOSArchetypes.PAYMENT, transaction.getArchetype());
        } else {
            assertEquals(EFTPOSArchetypes.REFUND, transaction.getArchetype());
        }
        assertEquals(EFTPOSTransactionStatus.NO_TERMINAL, transaction.getStatus());
        checkEquals(item.getTotal(), transaction.getTotal());
        assertEquals(location.getObjectReference(), getBean(transaction).getTargetRef("location"));
    }

    /**
     * Creates an <em>act.customerAccountPaymentEFT</em>.
     *
     * @return a new act
     */
    private FinancialAct createEFTPaymentItem() {
        return accountFactory.newEFTPaymentItem()
                .amount(TEN)
                .build(false);
    }

    /**
     * Creates an <em>act.customerAccountPaymentEFT</em> with an associated EFTPOS transaction.
     *
     * @param transaction the EFTPOS transaction
     * @return a new act
     */
    private FinancialAct createEFTPaymentItem(FinancialAct transaction) {
        return accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
    }

    /**
     * Creates an <em>act.customerAccountRefundEFT</em.
     *
     * @return a new act
     */
    private FinancialAct createEFTRefundItem() {
        return accountFactory.newEFTRefundItem()
                .amount(TEN)
                .build(false);
    }

    /**
     * Creates an <em>act.customerAccountPayment</em> with an associated item.
     *
     * @param customer the customer
     * @param location the practice location
     * @param till     the till
     * @param item     the payment item
     * @return a new act
     */
    private FinancialAct createPayment(Party customer, Party location, Entity till, FinancialAct item) {
        return accountFactory.newPayment()
                .customer(customer)
                .location(location)
                .till(till)
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();
    }

    /**
     * Creates an <em>act.customerAccountRefund</em> with an associated item.
     *
     * @param customer the customer
     * @param location the practice location
     * @param till     the till
     * @param item     the refund item
     * @return a new act
     */
    private FinancialAct createRefund(Party customer, Party location, Entity till, FinancialAct item) {
        return accountFactory.newRefund()
                .customer(customer)
                .location(location)
                .till(till)
                .add(item)
                .status(FinancialActStatus.IN_PROGRESS)
                .build();
    }

    /**
     * Creates an <em>act.EFTPOSPayment</em>.
     *
     * @param customer the customer
     * @param terminal the EFTPOS terminal
     * @param location the practice location
     * @param status   the transaction status
     * @return a new act
     */
    private FinancialAct createEFTPOSPayment(Party customer, Entity terminal, Party location, String status) {
        return accountFactory.newEFTPOSPayment().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(location)
                .status(status)
                .build();
    }

    /**
     * Creates an {@link EFTPaymentItemEditor}.
     *
     * @param item    the payment item
     * @param payment the parent payment
     * @return a new item
     */
    private EFTPaymentItemEditor createEditor(FinancialAct item, FinancialAct payment) {
        LocalContext localContext = new LocalContext();
        Party location = getBean(payment).getTarget("location", Party.class);
        localContext.setLocation(location);
        DefaultLayoutContext context = new DefaultLayoutContext(localContext, new HelpContext("foo", null));
        EFTPaymentItemEditor editor = new EFTPaymentItemEditor(item, payment, context);
        editor.getComponent();
        return editor;
    }

    /**
     * Tests the {@link EFTPaymentItemEditor#canDelete()} and {@link EFTPaymentItemEditor#getDeletionStatus()}
     * methods.
     *
     * @param item        the payment item
     * @param payment     the payment
     * @param transaction the EFTPOS transaction
     * @param status      the EFTPOS transaction status to set
     * @param canDelete   determines if the item can be deleted
     */
    private void checkCanDelete(FinancialAct item, FinancialAct payment, FinancialAct transaction, String status,
                                boolean canDelete) {
        transaction.setStatus(status);
        save(transaction);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        EFTPaymentItemEditor editor = new EFTPaymentItemEditor(item, payment, context);
        assertEquals(canDelete, editor.canDelete());
        if (canDelete) {
            assertNull(editor.getDeletionStatus());
        } else {
            assertEquals(status, editor.getDeletionStatus());
        }
    }
}
