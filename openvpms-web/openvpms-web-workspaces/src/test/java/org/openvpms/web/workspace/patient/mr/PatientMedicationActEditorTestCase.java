/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link PatientMedicationActEditor}.
 *
 * @author Tim Anderson
 */
public class PatientMedicationActEditorTestCase extends AbstractIMObjectEditorTest<PatientMedicationActEditor> {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Constructs a {@link PatientMedicationActEditorTestCase}.
     */
    public PatientMedicationActEditorTestCase() {
        super(PatientMedicationActEditor.class, PatientArchetypes.PATIENT_MEDICATION);
    }

    /**
     * Verifies that the expiry date matches that of the batch, and cannot be set greater than that of the batch.
     */
    @Test
    public void testBatchExpiry() {
        Context context = new LocalContext();
        Date today = DateRules.getToday();
        Date month1 = DateRules.getDate(today, 1, DateUnits.MONTHS);
        Date month2 = DateRules.getDate(today, 2, DateUnits.MONTHS);
        Party patient = patientFactory.createPatient();

        Product product = productFactory.createMedication();
        Entity batch1 = productFactory.createBatch(product, "123456788", month1);
        Entity batch2 = productFactory.createBatch(product, "123456789", month2);

        Act act = patientFactory.createMedication(patient, product);

        DefaultLayoutContext layoutContext1 = new DefaultLayoutContext(context, new HelpContext("foo", null));
        PatientMedicationActEditor editor1 = new PatientMedicationActEditor(act, null, layoutContext1);
        editor1.getComponent();

        assertNull(editor1.getBatch());
        editor1.setBatch(batch1);
        assertEquals(batch1, editor1.getBatch());
        assertEquals(month1, editor1.getExpiryDate());

        editor1.setExpiryDate(today);
        assertEquals(today, editor1.getExpiryDate());

        // verify that the expiry date can't set beyond that of the batch
        editor1.setExpiryDate(DateRules.getNextDate(month1));
        assertEquals(month1, editor1.getExpiryDate());

        // or set to null
        editor1.setExpiryDate(null);
        assertEquals(month1, editor1.getExpiryDate());

        // now change the batch, and verify the expiry changes
        editor1.setBatch(batch2);
        assertEquals(month2, editor1.getExpiryDate());

        assertTrue(SaveHelper.save(editor1));

        // now change the batch expiry. A new editor will now flag the expiry date as invalid.
        productFactory.updateBatch(batch2).expiryDate(month1).build();

        DefaultLayoutContext layoutContext2 = new DefaultLayoutContext(context, new HelpContext("foo", null));
        PatientMedicationActEditor editor2 = new PatientMedicationActEditor(act, null, layoutContext2);
        editor2.getComponent();

        assertInvalid(editor2, "The Expiry Date must be set to " + DateFormatter.formatDate(month1, false)
                               + " or earlier.");
    }

    /**
     * Verifies that when a medication is generated from an invoice, the product cannot be changed.
     */
    @Test
    public void testCantChangeProductForInvoicedMedication() {
        Party patient = patientFactory.createPatient();
        Product product = productFactory.createMedication();

        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);

        new TestInvoiceItemBuilder(null, getArchetypeService()).medication(act).build(false);
        // link the medication to an invoice item

        Context context = new LocalContext();
        context.setPatient(patient);

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        PatientMedicationActEditor editor = new PatientMedicationActEditor(act, null, layoutContext);

        assertEquals(patient, editor.getPatient());
        assertNull(editor.getProduct());

        editor.getComponent();
        assertNull(editor.getProductEditor());       // verify there is no product editor
        editor.setProduct(product);                  // verify product can be set in code
        assertEquals(product, editor.getProduct());
    }

    /**
     * Verifies that when the medication is not created from an invoice, the product can be changed.
     */
    @Test
    public void testCanChangeProductForStandaloneMedication() {
        Party patient = patientFactory.createPatient();
        Product product = productFactory.createMedication();

        Context context = new LocalContext();
        context.setPatient(patient);

        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        PatientMedicationActEditor editor = new PatientMedicationActEditor(act, null, layoutContext);

        assertEquals(patient, editor.getPatient());
        assertNull(editor.getProduct());

        editor.getComponent();
        assertNotNull(editor.getProductEditor());
        editor.setProduct(product);
        assertEquals(product, editor.getProduct());
    }
}
