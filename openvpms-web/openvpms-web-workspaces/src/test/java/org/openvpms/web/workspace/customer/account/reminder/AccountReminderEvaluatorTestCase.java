/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.reminder;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.macro.impl.MacroTestHelper;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.workspace.customer.account.AccountReminderEvaluator;
import org.openvpms.web.workspace.customer.account.AccountReminderException;
import org.openvpms.web.workspace.workflow.appointment.reminder.AppointmentReminderException;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link AccountReminderEvaluator}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AccountReminderEvaluatorTestCase extends ArchetypeServiceTest {

    /**
     * The SMS template evaluator.
     */
    @Autowired
    private SMSTemplateEvaluator smsTemplateEvaluator;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    protected TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The account reminder evaluator.
     */
    private AccountReminderEvaluator evaluator;

    /**
     * The charge.
     */
    private FinancialAct charge;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        practice = practiceFactory.newPractice().name("Vets R Us").build(false);
        evaluator = new AccountReminderEvaluator(practice, smsTemplateEvaluator, getArchetypeService(),
                                                 getLookupService());
        customer = customerFactory.createCustomer("Foo", "Bar");
        location = practiceFactory.newLocation().name("Vets R Us - Cowes").build(false);
        charge = accountFactory.newInvoice()
                .customer(customer)
                .item()
                .patient(patientFactory.createPatient(customer))
                .medicationProduct()
                .unitPrice(20)
                .add()
                .status(FinancialActStatus.POSTED)
                .sendReminder(true)
                .startTime("2021-08-01")
                .build();
        accountFactory.newPayment().customer(customer)
                .till(practiceFactory.createTill())
                .cash(10)
                .status(FinancialActStatus.POSTED)
                .build();
        charge = get(charge);  // reload to refresh invoice.getAllocatedAmount()
    }

    /**
     * Tests xpath expression evaluation.
     */
    @Test
    public void testXPathExpression() {
        String expression = "concat('Dear ', $customer.firstName, ', your account from ', "
                            + "date:format($charge.startTime, 'EEEE dd/MM/yy'), ' at ', $location.name, " +
                            "' has an outstanding balance of $', " + "$balance)";
        Entity template = createXPathTemplate(expression);
        String value = evaluator.evaluate(template, charge, customer, location);
        assertEquals("Dear Foo, your account from Sunday 01/08/21 at Vets R Us - Cowes has an outstanding balance of " +
                     "$10", value);
    }

    /**
     * Verifies that the $invoice, $balance, $customer, $location, and $practice variables are defined.
     */
    @Test
    public void testXPathVariables() {
        checkXPathExpression("$charge.id", Long.toString(charge.getId()));
        checkXPathExpression("$balance", "10.00");
        checkXPathExpression("$location.name", "Vets R Us - Cowes");
        checkXPathExpression("$customer.firstName", "Foo");
        checkXPathExpression("$practice.name", "Vets R Us");
    }

    /**
     * Verifies that an invalid XPath expression generates an {@link AppointmentReminderException}.
     */
    @Test
    public void testXPathException() {
        try {
            Entity template = createXPathTemplate("$badexpression");
            evaluator.evaluate(template, charge, location, practice);
            fail("Expected evaluation to throw an exception");
        } catch (AccountReminderException expected) {
            assertEquals("Failed to evaluate the expression in Account Reminder SMS Template", expected.getMessage());
        }
    }

    /**
     * Tests macro expression evaluation.
     */
    @Test
    public void testMacroExpression() {
        MacroTestHelper.createMacro("@customer", "$customer.firstName");
        MacroTestHelper.createMacro("@location", "$location.name");
        MacroTestHelper.createMacro("@startDate", "date:format($charge.startTime, 'dd/MM/yy')");
        MacroTestHelper.createMacro("@balance", "concat('$', $balance)");
        String expression = "Dear @customer - your invoice from @startDate at @location has an outstanding balance " +
                            "of @balance";

        Entity template = createMacroTemplate(expression);
        String value = evaluator.evaluate(template, charge, customer, location);
        assertEquals("Dear Foo - your invoice from 01/08/21 at Vets R Us - Cowes has an outstanding balance of $10",
                     value);
    }

    /**
     * Verifies that an invalid macro expression generates an {@link AccountReminderException}.
     */
    @Test
    public void testMacroException() {
        MacroTestHelper.createMacro("@badexpression", "concat(");
        try {
            Entity template = createMacroTemplate("@badexpression");
            evaluator.evaluate(template, charge, location, practice);
            fail("Expected evaluation to throw an exception");
        } catch (AccountReminderException expected) {
            assertEquals("Failed to evaluate the expression in Account Reminder SMS Template", expected.getMessage());
        }
    }

    /**
     * Verifies the result of an XPath expression matches that expected.
     *
     * @param expression the expression
     * @param expected   the expected value
     */
    private void checkXPathExpression(String expression, String expected) {
        Entity template = createXPathTemplate(expression);
        String value = evaluator.evaluate(template, charge, customer, location);
        assertEquals(expected, value);
    }

    /**
     * Creates a macro template
     *
     * @param template the macro template
     * @return a new template
     */
    protected Entity createMacroTemplate(String template) {
        return createTemplate("MACRO", template);
    }

    /**
     * Creates a template with an XPATH expression.
     *
     * @param template the template expression
     * @return a new template
     */
    protected Entity createXPathTemplate(String template) {
        return createTemplate("XPATH", template);
    }

    /**
     * Creates a template
     *
     * @param contentType the type of the content
     * @param template    the template
     * @return a new template
     */
    protected Entity createTemplate(String contentType, String template) {
        return documentFactory.newSMSTemplate("entity.documentTemplateSMSAccount")
                .contentType(contentType)
                .content(template)
                .build();
    }

}