/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.repeat;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.openvpms.web.workspace.workflow.appointment.repeat.Repeats.times;

/**
 * Base class for calendar event series tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCalendarEventSeriesTest extends AbstractScheduleEventSeriesTest {

    /**
     * The schedule.
     */
    private Party schedule;

    /**
     * The appointment type.
     */
    private Entity appointmentType;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        appointmentType = ScheduleTestHelper.createAppointmentType();
        schedule = ScheduleTestHelper.createSchedule(15, DateUnits.MINUTES.toString(), 1, appointmentType,
                                                     TestHelper.createLocation());
    }

    /**
     * Verifies that the schedule can be updated.
     */
    @Test
    public void testChangeSchedule() {
        Act event = createEvent();
        ScheduleEventSeries series = createSeries(event, Repeats.yearly(), times(2));
        checkSeries(series, event, 1, DateUnits.YEARS, 3);

        Entity schedule2 = ScheduleTestHelper.createSchedule(15, DateUnits.MINUTES.toString(), 1, appointmentType,
                                                             TestHelper.createLocation());

        IMObjectBean bean = getBean(event);
        bean.setTarget("schedule", schedule2);

        checkSave(series);
        assertEquals(schedule2, bean.getTarget("schedule"));
        checkSeries(series, event, 1, DateUnits.YEARS, 3);
    }

    /**
     * Creates a new calendar event.
     *
     * @param startTime the event start time
     * @param endTime   the event end time
     * @return a new event
     */
    @Override
    protected Act createEvent(Date startTime, Date endTime) {
        return createEvent(startTime, endTime, schedule, appointmentType);
    }

    /**
     * Creates a new calendar event.
     *
     * @param startTime       the event start time
     * @param endTime         the event end time
     * @param schedule        the schedule
     * @param appointmentType the appointment type
     */
    protected abstract Act createEvent(Date startTime, Date endTime, Entity schedule, Entity appointmentType);

    /**
     * Returns the schedule.
     *
     * @return the schedule
     */
    protected Entity getSchedule() {
        return schedule;
    }
}