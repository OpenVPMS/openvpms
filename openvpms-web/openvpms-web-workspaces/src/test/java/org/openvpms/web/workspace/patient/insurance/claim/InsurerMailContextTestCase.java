/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.AbstractMailContextTest;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNull;

/**
 * Tests the {@link InsurerMailContext}.
 *
 * @author Tim Anderson
 */
public class InsurerMailContextTestCase extends AbstractMailContextTest {

    /**
     * The contact factory.
     */
    @Autowired
    private TestContactFactory contactFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;


    /**
     * Verifies that the correct email to-addresses are returned for insurer mail contexts.
     */
    @Test
    public void testContext() {
        LocalContext context = new LocalContext();
        InsurerMailContext mailContext = new InsurerMailContext(context, new HelpContext("foo", null));

        assertNull(mailContext.getInsurer());
        assertNull(mailContext.getCustomer());
        assertNull(mailContext.getPatient());
        checkAddresses(mailContext.getToAddresses(), null);

        Contact insurerContact1 = contactFactory.createEmail("info@insurer1.com");
        Party insurer1 = insuranceFactory.newInsurer()
                .addContacts(insurerContact1)
                .build();
        Contact insurerContact2 = contactFactory.createEmail("info@insurer2.com");

        Party insurer2 = insuranceFactory.newInsurer()
                .addContacts(insurerContact2)
                .build();

        Contact customerContact = contactFactory.createEmail("customer1@gmail.com");
        Party customer = customerFactory.newCustomer()
                .addContacts(customerContact)
                .build();

        Contact vetContact = contactFactory.createEmail("vet@vetsrus.com");
        Party vet = supplierFactory.newVet()
                .addContacts(vetContact)
                .build();
        Party patient = patientFactory.newPatient()
                .owner(customer)
                .addReferredTo(vet)
                .build();

        // verify the correct to-addresses are returned for insurer1
        context.setSupplier(insurer1);
        context.setCustomer(customer);
        context.setPatient(patient);

        checkAddresses(mailContext.getToAddresses(), insurerContact1, insurerContact1, customerContact, vetContact);

        // verify the correct to-addresses are returned for insurer2
        context.setSupplier(insurer2);
        checkAddresses(mailContext.getToAddresses(), insurerContact2, insurerContact2, customerContact, vetContact);
    }

    /**
     * Verifies that the expected email addresses are returned when an insurer is updated.
     */
    @Test
    public void testUpdateInsurer() {
        LocalContext context = new LocalContext();
        InsurerMailContext mailContext = new InsurerMailContext(context, new HelpContext("foo", null));

        Contact contactA = contactFactory.createEmail("info@insurer1.com");
        Contact contactB = contactFactory.createEmail("info@insurer2.com", true);
        Party insurer = insuranceFactory.newInsurer()
                .addContacts(contactA)
                .build();

        context.setSupplier(insurer);
        checkAddresses(mailContext.getToAddresses(), contactA, contactA);

        // now update the insurer and verify the correct email addresses are returned
        insuranceFactory.updateInsurer(insurer)
                .addContacts(contactB)
                .build();
        checkAddresses(mailContext.getToAddresses(), contactB, contactA, contactB);
    }
}