/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import org.junit.Test;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.im.table.IMObjectTableModelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import static org.junit.Assert.assertEquals;
import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.DELIVERY_ITEM;
import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.RETURN_ITEM;

/**
 * Tests the {@link DeliveryItemTableModel}.
 *
 * @author Tim Anderson
 */
public class DeliveryItemTableModelTestCase extends AbstractAppTest {

    /**
     * Verifies that  {@link DeliveryItemTableModel} is returned by {@link IMObjectTableModel} for
     * <em>act.supplierDeliveryItem</em> and <em>act.supplierReturnItem</em>
     */
    @Test
    public void testFactory() {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectTableModel<IMObject> model1 = IMObjectTableModelFactory.create(new String[]{DELIVERY_ITEM}, context);
        IMObjectTableModel<IMObject> model2 = IMObjectTableModelFactory.create(new String[]{RETURN_ITEM}, context);
        assertEquals(DeliveryItemTableModel.class, model1.getClass());
        assertEquals(DeliveryItemTableModel.class, model2.getClass());
    }
}
