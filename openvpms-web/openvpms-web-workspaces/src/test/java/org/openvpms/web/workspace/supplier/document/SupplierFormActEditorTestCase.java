/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.document;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.DocumentActEditor;
import org.openvpms.web.component.im.doc.TemplatedDocumentActEditorTest;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Tests the {@link SupplierDocumentActEditor} class for <em>act.supplierDocumentForm</em> acts.
 *
 * @author Tim Anderson
 */
public class SupplierFormActEditorTestCase extends TemplatedDocumentActEditorTest<SupplierDocumentActEditor> {

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Constructs a {@link SupplierFormActEditorTestCase}.
     */
    public SupplierFormActEditorTestCase() {
        super(SupplierDocumentActEditor.class, SupplierArchetypes.DOCUMENT_FORM);
    }

    /**
     * Creates a new editor.
     *
     * @param act the act to edit
     * @return a new editor
     */
    protected DocumentActEditor createEditor(DocumentAct act) {
        DefaultLayoutContext layout = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        Context context = layout.getContext();
        context.setSupplier(supplierFactory.createSupplier());
        context.setUser(userFactory.createUser());
        return new SupplierDocumentActEditor(act, null, layout);
    }
}