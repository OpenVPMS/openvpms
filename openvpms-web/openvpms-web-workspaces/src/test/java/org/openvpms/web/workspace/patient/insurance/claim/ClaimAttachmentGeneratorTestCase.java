/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import nextapp.echo2.app.ApplicationInstance;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.insurance.TestClaimBuilder;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.insurance.claim.Attachment;
import org.openvpms.insurance.internal.InsuranceFactory;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ClaimAttachmentGenerator}.
 *
 * @author Tim Anderson
 */
public class ClaimAttachmentGeneratorTestCase extends AbstractAppTest {

    /**
     * The insurance rules.
     */
    @Autowired
    private InsuranceRules insuranceRules;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The insurer.
     */
    private Party insurer;

    /**
     * The claim builder.
     */
    private TestClaimBuilder claimBuilder;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();

        // make sure a medical records template exists
        documentFactory.newTemplate()
                .singletonByType()
                .type(ClaimAttachmentGenerator.INSURANCE_CLAIM_MEDICAL_RECORDS)
                .name("Medical Records")
                .document(documentFactory.createJRXML("Medical Records.jrxml"))
                .build();

        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
        insurer = insuranceFactory.createInsurer();
        Act policy = insuranceFactory.createPolicy(customer, patient, insurer, "123456789");

        claimBuilder = insuranceFactory.newClaim();

        location = practiceFactory.createLocation();
        User clinician = userFactory.createClinician();

        claimBuilder.policy(policy)
                .claimHandler(clinician)
                .clinician(clinician)
                .location(location);
    }

    /**
     * Verifies a PDF is generated for patient forms.
     */
    @Test
    public void testFormGeneration() {
        Entity formTemplate = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .name("Z Form")
                .blankDocument()
                .build();
        DocumentAct form = patientFactory.createForm(patient, formTemplate);

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(form)
                .build();
        checkGenerate(claim, null, "Patient Form", "Z Form.pdf", null);
    }

    /**
     * Verifies an error is raised if the template associated with a patient form doesn't have a document.
     */
    @Test
    public void testFormGenerationWithoutTemplate() {
        Entity formTemplate = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .name("Z Form")
                .build();
        DocumentAct form = patientFactory.createForm(patient, formTemplate);

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(form)
                .build();
        checkGenerate(claim, null, "Patient Form", null, "REPORT-0032: Template has no document: Z Form");
    }

    /**
     * Verifies that a document associated with a patient attachment is copied, so it cannot be deleted or changed once
     * the claim is submitted.
     */
    @Test
    public void testPatientAttachmentGeneration() {
        Document pdf = documentFactory.createPDF("report.pdf");
        DocumentAct attachment = patientFactory.newAttachment()
                .patient(patient)
                .document(pdf)
                .build();

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(attachment)
                .build();
        Document generated = checkGenerate(claim, null, "Patient Attachment", "report.pdf", null);
        assertNotNull(generated);
        assertNotEquals(pdf, generated);
    }

    /**
     * Verifies OpenOffice documents are converted to PDF.
     */
    @Test
    public void testConvertODTToPDF() {
        Document odt = documentFactory.createODT("report.odt");
        DocumentAct attachment = patientFactory.newAttachment()
                .patient(patient)
                .document(odt)
                .build();

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(attachment)
                .build();
        Document generated = checkGenerate(claim, null, "Patient Attachment", "report.pdf", null);
        assertNotNull(generated);
        assertNotEquals(odt, generated);
        assertEquals(DocFormats.PDF_TYPE, generated.getMimeType());
    }

    /**
     * Verifies Word documents are converted to PDF.
     */
    @Test
    public void testConvertDOCToPDF() {
        Document doc = documentFactory.createDOC("report.doc");
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(laboratoryFactory.createInvestigationType())
                .report(doc)
                .build();

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(investigation)
                .build();
        Document generated = checkGenerate(claim, null, "Patient Investigation", "report.pdf", null);
        assertNotNull(generated);
        assertNotEquals(doc, generated);
        assertEquals(DocFormats.PDF_TYPE, generated.getMimeType());
    }

    /**
     * Verifies an invoice attachment is generated when an invoice is added to the claim.
     */
    @Test
    public void testInvoiceGeneration() {
        // make sure an invoice template exists
        documentFactory.newTemplate()
                .singletonByType()
                .type(ClaimAttachmentGenerator.INSURANCE_CLAIM_INVOICE)
                .name("Invoice")
                .blankDocument()
                .build();

        TestInvoiceBuilder invoiceBuilder = accountFactory.newInvoice();
        FinancialAct invoice = invoiceBuilder.customer(customer)
                .item().patient(patient).product(productFactory.createService()).unitPrice(10).add()
                .build();
        FinancialAct invoiceItem = invoiceBuilder.getItems().get(0);
        FinancialAct claim = claimBuilder.item(invoiceItem)
                .build();
        checkGenerate(claim, invoiceItem, "Customer Invoice " + invoice.getId(), "Invoice.pdf", null);
    }

    /**
     * Verifies an error is raised if a patient attachment doesn't have a document.
     */
    @Test
    public void testPatientAttachmentWithoutDocument() {
        DocumentAct attachment = patientFactory.newAttachment().patient(patient).build();
        FinancialAct claim = claimBuilder.item()
                .treatmentDates(new Date(), new Date()).add()
                .attachment(attachment)
                .build();
        checkGenerate(claim, null, "Patient Attachment", null, "The Attachment has no document.");
    }

    /**
     * Verifies an error is raised if a document added to a claim is subsequently deleted.
     */
    @Test
    public void testDeleteSourceDocument() {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        DocumentAct form = patientFactory.createForm(patient, template);

        FinancialAct claim = claimBuilder.item()
                .treatmentDates(new Date(), new Date()).add()
                .attachment(form)
                .build();
        remove(form);
        checkGenerate(claim, null, "Patient Form", null, "The original document has been deleted.");
    }

    /**
     * Verifies an error is raised if a patient investigation doesn't have a document.
     */
    @Test
    public void testInvestigationGeneration() {
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(laboratoryFactory.createInvestigationType())
                .report(documentFactory.createPDF("Results.pdf"))
                .build();

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(investigation)
                .build();
        checkGenerate(claim, null, "Patient Investigation", "Results.pdf", null);
    }

    /**
     * Verifies an error is raised if a patient investigation doesn't have a document.
     */
    @Test
    public void testInvestigationWithoutDocument() {
        DocumentAct investigation = patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(laboratoryFactory.createInvestigationType())
                .build();

        FinancialAct claim = claimBuilder.item().treatmentDates(new Date(), new Date()).add()
                .attachment(investigation)
                .build();
        checkGenerate(claim, null, "Patient Investigation", null, "The Investigation has no document.");
    }

    /**
     * Tests claim attachment generation.
     *
     * @param claim        the claim
     * @param invoiceItem  an invoice item being claimed. If present, an invoice attachment will be generated.
     * @param name         the expected attachment name
     * @param documentName the expected attachment file name
     * @param error        if non-null, the expected error produced when generating an attachment
     * @return the attachment document. May be {@code null}
     */
    private Document checkGenerate(FinancialAct claim, FinancialAct invoiceItem, String name, String documentName,
                                   String error) {
        Document result = null;
        ClaimContext claimContext = new ClaimContext(claim, customer, patient, location, getArchetypeService(),
                                                     insuranceRules, Mockito.mock(InsuranceServices.class),
                                                     Mockito.mock(InsuranceFactory.class));
        Charges charges = new Charges(claimContext);
        if (invoiceItem != null) {
            charges.add(invoiceItem);
        }
        Context context = new LocalContext();
        DocumentConverter converter = new DummyConverter();
        ClaimAttachmentGenerator generator = new ClaimAttachmentGenerator(customer, patient, charges, context,
                                                                          converter);
        PropertySet set = new PropertySetBuilder(claim).build();
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        AttachmentCollectionEditor editor = new AttachmentCollectionEditor((CollectionProperty) set.get("attachments"),
                                                                           claim, layoutContext);
        editor.getComponent();
        Semaphore semaphore = new Semaphore(0);
        MutableBoolean success = new MutableBoolean();
        generator.generate(claim, insurer, editor, location, status -> {
            success.setValue(status);
            semaphore.release();
        });
        boolean done = false;
        for (int i = 0; i < 20; ++i) {
            // need to process queued tasks in order for the job to complete
            ApplicationInstance.getActive().getUpdateManager().processClientUpdates();
            try {
                if (semaphore.tryAcquire(1, 1, TimeUnit.SECONDS)) {
                    done = true;
                    break;
                }
            } catch (InterruptedException ignore) {
                // do nothing
            }
        }
        assertTrue(done);
        List<Act> acts = editor.getCurrentActs();
        if (error == null) {
            assertEquals(2, acts.size());
            assertTrue(success.booleanValue());
            checkAttachment("Patient History", "blank.pdf", acts);
            // records should always be generated. Note - the file name for JRXML templates is derived from the .jrxml.
            result = checkAttachment(name, documentName, acts);
        } else {
            assertFalse(success.booleanValue());
            DocumentAct failed = getAttachment(name, acts);
            assertEquals(Attachment.Status.ERROR.toString(), failed.getStatus());
            assertEquals(error, getBean(failed).getString("error"));
        }
        return result;
    }

    /**
     * Verify an attachment exists with the specified name and file name.
     *
     * @param name        the name
     * @param fileName    the file name
     * @param attachments the available attachments
     * @return the document
     */
    private Document checkAttachment(String name, String fileName, List<Act> attachments) {
        DocumentAct attachment = getAttachment(name, attachments);
        assertEquals(name, attachment.getName());
        assertEquals(fileName, attachment.getFileName());
        assertEquals("application/pdf", attachment.getMimeType());
        assertEquals(Attachment.Status.PENDING.toString(), attachment.getStatus());
        Document document = get(attachment.getDocument(), Document.class);
        assertNotNull(document);
        assertEquals(fileName, document.getName());
        assertEquals("application/pdf", document.getMimeType());
        return document;
    }

    /**
     * Returns an attachment given its name.
     *
     * @param name        the name
     * @param attachments the attachments to search
     * @return the corresponding attachment
     */
    private DocumentAct getAttachment(String name, List<Act> attachments) {
        DocumentAct attachment = (DocumentAct) attachments.stream()
                .filter(act -> name.equals(act.getName()))
                .findFirst()
                .orElse(null);
        assertNotNull(attachment);
        return attachment;
    }

    private class DummyConverter implements DocumentConverter {
        @Override
        public boolean canConvert(Document document, String mimeType) {
            return DocFormats.PDF_TYPE.equals(mimeType);
        }

        @Override
        public boolean canConvert(String fileName, String sourceMimeType, String targetMimeType) {
            return DocFormats.PDF_TYPE.equals(targetMimeType);
        }

        @Override
        public Document convert(Document document, String mimeType) {
            return documentFactory.createPDF(FilenameUtils.getBaseName(document.getName()) + ".pdf");
        }

        @Override
        public Document convert(Document document, String mimeType, boolean email) {
            return convert(document, mimeType);
        }

        @Override
        public byte[] export(Document document, String mimeType, boolean email) {
            return new byte[0];
        }
    }
}
