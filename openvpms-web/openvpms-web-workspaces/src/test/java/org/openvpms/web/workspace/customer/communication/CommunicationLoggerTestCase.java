/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CommunicationArchetypes;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link CommunicationLogger}.
 *
 * @author Tim Anderson
 */
public class CommunicationLoggerTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The communication logger.
     */
    private TestLogger logger;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test location.
     */
    private Party location;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        logger = new TestLogger(getArchetypeService());
        customer = customerFactory.createCustomer();
        location = practiceFactory.createLocation();
    }

    /**
     * Tests email logging.
     */
    @Test
    public void testLogEmail() {
        Party patient = patientFactory.createPatient(customer);

        // include some smiley face emojis to verify they get removed as per OVPMS-2534
        logger.logEmail(customer, patient, "from@mail.com", new String[]{"to@mail.com"},
                        new String[]{"cc1@mail.com", "cc2@mail.com"},
                        new String[]{"bcc@mail.com"}, "a subject \uD83D\uDE02", "a reason",
                        "<html><body>a message \uD83D\uDE02<body></html>",
                        "a note", "some attachments", location);
        IMObjectBean log = logger.getLog();
        assertTrue(log.isA(CommunicationArchetypes.EMAIL));
        assertEquals(customer, log.getTarget("customer"));
        assertEquals(patient, log.getTarget("patient"));
        assertEquals("from@mail.com", log.getString("from"));
        assertEquals("to@mail.com", log.getString("address"));
        assertEquals("cc1@mail.com\ncc2@mail.com", log.getString("cc"));
        assertEquals("bcc@mail.com", log.getString("bcc"));
        assertEquals("a subject \uFFFD", log.getString("description")); // smiley removed by unicode replacement char
        assertEquals("a reason", log.getString("reason"));
        assertEquals("a message \uFFFD", log.getString("message"));
        assertEquals("a note", log.getString("note"));
        assertEquals("some attachments", log.getString("attachments"));
        assertEquals(location, log.getTarget("location"));
        assertNull(log.getObject("document"));
    }

    /**
     * Verifies long emails get stored as a document.
     */
    @Test
    public void testLongEmail() {
        String message = RandomStringUtils.randomAlphabetic(50_000) + "\uD83D\uDE02";
        // add a smiley to verify it doesn't get replaced as there is no need to clean text being stored in a document.

        logger.logEmail(customer, null, "from@mail.com", new String[]{"to@mail.com"},
                        null, null, "a subject", "a reason",
                        "<html><body>" + message + "<body></html>",
                        "a note", "some attachments", location);
        IMObjectBean log = logger.getLog();
        assertNull(log.getString("message"));
        Document document = log.getObject("document", Document.class);
        assertNotNull(document);
        assertTrue(document.isA(DocumentArchetypes.TEXT_DOCUMENT));
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        String actual = handler.toString((org.openvpms.component.business.domain.im.document.Document) document);
        assertEquals(message, actual);
    }

    /**
     * Tests mail logging.
     */
    @Test
    public void testLogMail() {
        Party patient = patientFactory.createPatient(customer);
        logger.logMail(customer, patient, "an address", "a subject", "a reason", "a message", "a note",
                       location);
        IMObjectBean log = logger.getLog();
        assertTrue(log.isA(CommunicationArchetypes.MAIL));
        assertEquals(customer, log.getTarget("customer"));
        assertEquals(patient, log.getTarget("patient"));
        assertEquals("an address", log.getString("address"));
        assertEquals("a subject", log.getString("description"));
        assertEquals("a reason", log.getString("reason"));
        assertEquals("a message", log.getString("message"));
        assertEquals("a note", log.getString("note"));
        assertEquals(location, log.getTarget("location"));
        assertNull(log.getObject("document"));
    }

    @Test
    public void testLogPhone() {
        Party patient = patientFactory.createPatient(customer);
        logger.logPhone(customer, patient, "12345678", "a subject", "a reason", "a message", "a note",
                        location);
        IMObjectBean log = logger.getLog();
        assertTrue(log.isA(CommunicationArchetypes.PHONE));
        assertEquals(customer, log.getTarget("customer"));
        assertEquals(patient, log.getTarget("patient"));
        assertEquals("12345678", log.getString("address"));
        assertEquals("a subject", log.getString("description"));
        assertEquals("a reason", log.getString("reason"));
        assertEquals("a message", log.getString("message"));
        assertEquals("a note", log.getString("note"));
        assertEquals(location, log.getTarget("location"));
        assertNull(log.getObject("document"));
    }


    private static class TestLogger extends CommunicationLogger {

        private IMObjectBean log;

        /**
         * Constructs a {@link TestLogger}.
         *
         * @param service the archetype service
         */
        public TestLogger(IArchetypeService service) {
            super(service);
        }

        /**
         * Returns the last log.
         *
         * @return the last log. May be {@code null}
         */
        public IMObjectBean getLog() {
            return log;
        }

        /**
         * Saves a communication log.
         *
         * @param message  the message. If this exceeds the maximum allowed characters of the node, it will be stored as a
         *                 document instead
         * @param bean     the bean wrapping the act
         * @param fileName the document filename
         */
        @Override
        protected void saveLog(String message, IMObjectBean bean, String fileName) {
            super.saveLog(message, bean, fileName);
            log = bean;
        }
    }
}

