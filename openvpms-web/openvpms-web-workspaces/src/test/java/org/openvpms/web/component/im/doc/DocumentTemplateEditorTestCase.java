/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentTemplateEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DocumentTemplateEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link DocumentTemplateEditor} for
     * <em>entity.documentTemplate</em> instances.
     */
    @Test
    public void testFactory() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        IMObjectEditor editor = factory.create(template, new DefaultLayoutContext(new LocalContext(),
                                                                                  new HelpContext("foo", null)));
        assertTrue(editor instanceof DocumentTemplateEditor);
    }

    /**
     * Tests the {@link DocumentTemplateEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor = createEditor(template);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof DocumentTemplateEditor);
    }

    /**
     * Verifies a document can be associated with a template.
     */
    @Test
    public void testUpload() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor = createEditor(template);
        editor.getProperty("name").setValue("Z Test template");
        Document document1 = documentFactory.createJRXML();
        editor.onUpload(document1);
        assertTrue(SaveHelper.save(editor));
        Document original = checkDocument(template, document1);

        // now replace the document with another
        Document document2 = documentFactory.createDocument("/sqlreport.jrxml");
        editor.onUpload(document2);
        assertTrue(SaveHelper.save(editor));
        checkDocument(template, document2);
        assertNull(get(original));            // should have been deleted
    }

    /**
     * Verifies that when multiple uploads are done prior to save, only the last instance of a document is kept.
     */
    @Test
    public void testUploadTwice() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor = createEditor(template);
        editor.getProperty("name").setValue("Z Test template");
        Document document1 = documentFactory.createJRXML();
        editor.onUpload(document1);

        // verify the document has been saved
        document1 = get(document1);
        assertNotNull(document1);

        // upload a new document
        Document document2 = documentFactory.createDocument("/sqlreport.jrxml");
        editor.onUpload(document2);
        assertNotNull(get(document1));       // document1 should still exist

        assertTrue(SaveHelper.save(editor));
        checkDocument(template, document2);

        assertNull(get(document1));          // document1 should have been deleted
    }

    /**
     * Verifies a document can be replaced by another editor instance.
     */
    @Test
    public void testReplaceDocument() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor1 = createEditor(template);

        editor1.getProperty("name").setValue("Z Test template");
        Document document1 = documentFactory.createJRXML();
        editor1.onUpload(document1);
        assertTrue(SaveHelper.save(editor1));
        Document actual1 = checkDocument(template, document1);

        // now replace the document with another editor
        DocumentTemplateEditor editor2 = createEditor(template);
        Document document2 = documentFactory.createDocument("/sqlreport.jrxml");
        editor2.onUpload(document2);
        assertTrue(SaveHelper.save(editor2));
        Document actual2 = checkDocument(template, document2);
        assertNull(get(actual1));        // should have been deleted

        // and again
        DocumentTemplateEditor editor3 = createEditor(template);
        Document document3 = documentFactory.createJRXML();
        editor3.onUpload(document3);
        assertTrue(SaveHelper.save(editor3));
        checkDocument(template, document3);
        assertNull(get(actual2));        // should have been deleted
    }

    /**
     * Verifies that when a template is deleted, the associated document act and document is also deleted,
     * but any associated email template is retained.
     */
    @Test
    public void testDelete() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor1 = createEditor(template);

        editor1.getProperty("name").setValue("Z Test template");
        Document document = documentFactory.createJRXML();
        editor1.onUpload(document);

        DocumentAct act = editor1.getDocumentAct();
        assertNotNull(act);
        assertEquals(document.getObjectReference(), act.getDocument());

        assertTrue(SaveHelper.save(editor1));
        assertNotNull(get(act));
        assertNotNull(get(document));

        // add an email template to the document template
        Document emailTemplateContent = documentFactory.newDocument()
                .name("test.jrxml")
                .mimeType(DocFormats.XML_TYPE)
                .content("<jrxml/>")
                .build();
        Entity emailTemplate = documentFactory.newEmailTemplate()
                .subject("subject")
                .document(emailTemplateContent)
                .build();

        documentFactory.updateTemplate(template)
                .emailTemplate(emailTemplate)
                .build();

        DocumentTemplateEditor editor2 = createEditor(template);
        TransactionTemplate txn = new TransactionTemplate(ServiceHelper.getTransactionManager());
        txn.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                editor2.delete();
            }
        });

        // verify they have been deleted
        assertNull(get(template));
        assertNull(get(act));
        assertNull(get(document));

        // verify the email template has been retained
        assertNotNull(get(emailTemplate));
        assertNotNull(get(emailTemplateContent));
    }

    /**
     * Verifies the supported content for specific document template types.
     */
    @Test
    public void testSupportedContent() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor = createEditor(template);
        assertNull(editor.getTypeCode());

        checkDefaultContent(editor, null);

        checkFormContent(editor, CustomerArchetypes.DOCUMENT_FORM);
        checkFormContent(editor, PatientArchetypes.DOCUMENT_FORM);
        checkFormContent(editor, SupplierArchetypes.DOCUMENT_FORM);

        checkLetterContent(editor, CustomerArchetypes.DOCUMENT_LETTER);
        checkLetterContent(editor, PatientArchetypes.DOCUMENT_LETTER);
        checkLetterContent(editor, SupplierArchetypes.DOCUMENT_LETTER);

        checkReportContent(editor, DocumentTemplate.REPORT);
        checkReportContent(editor, DocumentTemplate.SUBREPORT);

        // everything else is mergeable
        checkDefaultContent(editor, ScheduleArchetypes.APPOINTMENT);
        checkDefaultContent(editor, CustomerAccountArchetypes.INVOICE);
    }

    /**
     * Verifies that new instances have the orientation set to PORTRAIT, and that this can be subsequently changed
     * to 'None'.
     */
    @Test
    public void testOrientation() {
        Entity template = create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class);
        DocumentTemplateEditor editor1 = createEditor(template);
        editor1.getProperty("name").setValue("Z Test template");
        Document document = documentFactory.createJRXML();
        editor1.onUpload(document);
        assertEquals(DocumentTemplate.PORTRAIT, editor1.getProperty("orientation").getString());
        editor1.getProperty("orientation").setValue(null);
        assertTrue(SaveHelper.save(editor1));

        Entity reloaded = get(template);
        DocumentTemplate template1  = new DocumentTemplate(reloaded, getArchetypeService());
        assertNull(template1.getOrientation());

        DocumentTemplateEditor editor2 = createEditor(reloaded);
        assertNull(editor2.getProperty("orientation").getString());
    }

    /**
     * Helper to create an editor to edit a template.
     *
     * @param template the template to edit
     * @return a new editor
     */
    protected DocumentTemplateEditor createEditor(Entity template) {
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DocumentTemplateEditor editor = new DocumentTemplateEditor(template, null, context);
        editor.getComponent();
        return editor;
    }

    /**
     * Verifies that the expected content can be uploaded for general document template types, which should be
     * mergeable.
     *
     * @param editor the editor
     * @param type   the report type. May be {@code null}
     */
    private void checkDefaultContent(DocumentTemplateEditor editor, String type) {
        setTypeCode(editor, type);
        checkJasperReportsContent(editor);
        checkMergeableContent(editor, true);
        checkPDFContent(editor, false);
    }

    /**
     * Sets the document template type code on an editor, ensuring it exists.
     *
     * @param editor the editor
     * @param type   the type. May be {@code null}
     */
    private void setTypeCode(DocumentTemplateEditor editor, String type) {
        if (type != null) {
            // make sure the type exists
            lookupFactory.getLookup(DocumentArchetypes.DOCUMENT_TEMPLATE_TYPE, type);
        }
        editor.setTypeCode(type);
    }

    /**
     * Verifies that the expected content can be uploaded for forms.
     *
     * @param editor    the editor
     * @param archetype the form archetype
     */
    private void checkFormContent(DocumentTemplateEditor editor, String archetype) {
        setTypeCode(editor, archetype);
        checkMergeableContent(editor, true);
        checkJasperReportsContent(editor);
        checkPDFContent(editor, true);
    }

    /**
     * Verifies that the expected content can be uploaded for letters.
     *
     * @param editor    the editor
     * @param archetype the letter archetype
     */
    private void checkLetterContent(DocumentTemplateEditor editor, String archetype) {
        setTypeCode(editor, archetype);
        checkMergeableContent(editor, true);
        checkJasperReportsContent(editor);
        checkPDFContent(editor, false);
    }

    /**
     * Verifies that the expected content can be uploaded for reports.
     *
     * @param editor the editor
     * @param type   the report type
     */
    private void checkReportContent(DocumentTemplateEditor editor, String type) {
        setTypeCode(editor, type);
        checkJasperReportsContent(editor);
        checkMergeableContent(editor, false);
        checkPDFContent(editor, false);
    }

    /**
     * Determines if mergeable content should be supported or not
     *
     * @param editor    the editor
     * @param supported if {@code true}, it must be supported, otherwise it must be rejected
     */
    private void checkMergeableContent(DocumentTemplateEditor editor, boolean supported) {
        assertEquals(supported, editor.isContentSupported("test.doc", DocFormats.DOC_TYPE));
        assertEquals(supported, editor.isContentSupported("test.doc", "application/octet-stream"));
        assertEquals(supported, editor.isContentSupported("test.odt", DocFormats.ODT_TYPE));
        assertEquals(supported, editor.isContentSupported("test.odt", "application/octet-stream"));
        assertEquals(supported, editor.isContentSupported("test.rtf", DocFormats.RTF_TYPE));
        assertEquals(supported, editor.isContentSupported("test.rtf", "application/octet-stream"));

        assertFalse(editor.isContentSupported("test.docx", DocFormats.DOCX_TYPE));
        assertFalse(editor.isContentSupported("test.docx", "application/octet-stream"));
        assertFalse(editor.isContentSupported("test.gif", "image/gif"));
        assertFalse(editor.isContentSupported("test.jpg", "image/jpeg"));
        assertFalse(editor.isContentSupported("test.png", "image/png"));
    }

    /**
     * Verifies JasperReports content is supported.
     *
     * @param editor the editor
     */
    private void checkJasperReportsContent(DocumentTemplateEditor editor) {
        assertTrue(editor.isContentSupported("test.jrxml", DocFormats.XML_TYPE));
        assertTrue(editor.isContentSupported("test.jrxml", "application/octet-stream"));
    }

    /**
     * Determines if PDF content is supported or not.
     *
     * @param editor    the editor
     * @param supported if {@code true}, it must be supported, otherwise it must be rejected
     */
    private void checkPDFContent(DocumentTemplateEditor editor, boolean supported) {
        assertEquals(supported, editor.isContentSupported("test.pdf", DocFormats.PDF_TYPE));
        assertEquals(supported, editor.isContentSupported("test.pdf", "application/octet-stream"));
    }

    /**
     * Verifies the document associated with an <em>entity.documentTemplate</em> matches that expected.
     *
     * @param template the template
     * @param expected the expected document
     * @return the actual document
     */
    private Document checkDocument(Entity template, Document expected) {
        TemplateHelper helper = new TemplateHelper(ServiceHelper.getArchetypeService());
        DocumentAct act = helper.getDocumentAct(template);
        assertNotNull(act);
        assertEquals(expected.getName(), act.getFileName());
        assertEquals(expected.getMimeType(), act.getMimeType());

        Document document = (Document) get(act.getDocument());

        documentFactory.newVerifier(expected)
                .verify(document);
        return document;
    }
}
