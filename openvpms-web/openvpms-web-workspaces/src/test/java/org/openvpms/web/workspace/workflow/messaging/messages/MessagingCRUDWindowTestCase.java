/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.messages;

import org.junit.Test;
import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.archetype.test.builder.message.TestMessageFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link MessagingCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class MessagingCRUDWindowTestCase extends AbstractAppTest {

    /**
     * The message factory.
     */
    @Autowired
    private TestMessageFactory messageFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests replying to a message.
     */
    @Test
    public void testReply() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();
        AuthenticationContext authenticationContext = new AuthenticationContextImpl();
        authenticationContext.setUser(user1);
        Act message = messageFactory.newUserMessage()
                .to(user2)
                .subject("test")
                .message("text")
                .reason("PHONE_CALL")
                .build();
        assertEquals(user1.getObjectReference(), message.getCreatedBy());

        authenticationContext.setUser(user2);
        LocalContext context = new LocalContext();
        context.setUser(user2);
        MessagingCRUDWindow window = createCRUDWindow(context);
        window.setObject(message);

        EchoTestHelper.fireButton(window.getComponent(), MessagingCRUDWindow.REPLY_ID);

        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertNotNull(dialog);
        assertEquals("Reply To Message", dialog.getTitle());
        UserMessageEditor editor = (UserMessageEditor) dialog.getEditor();
        assertEquals(user1, editor.getTo());
        assertEquals("Re: test", editor.getSubject());
        assertValid(editor);

        // verify the are no messages from user2 to user1
        List<Act> replies1 = getMessages(user2, user1);
        assertEquals(0, replies1.size());

        EchoTestHelper.fireDialogButton(dialog, UserMessageEditDialog.SEND_ID);

        // verify there is now a message from user2 to user1
        List<Act> replies = getMessages(user2, user1);
        assertEquals(1, replies.size());
    }

    /**
     * Tests forwarding a message.
     */
    @Test
    public void testForward() {
        User user1 = userFactory.createUser();
        User user2 = userFactory.createUser();
        User user3 = userFactory.createUser();
        AuthenticationContext authenticationContext = new AuthenticationContextImpl();
        authenticationContext.setUser(user1);
        Act message = messageFactory.newUserMessage()
                .to(user2)
                .subject("test")
                .message("text")
                .reason("PHONE_CALL")
                .build();
        assertEquals(user1.getObjectReference(), message.getCreatedBy());

        authenticationContext.setUser(user2);
        LocalContext context = new LocalContext();
        context.setUser(user2);
        MessagingCRUDWindow window = createCRUDWindow(context);
        window.setObject(message);

        EchoTestHelper.fireButton(window.getComponent(), MessagingCRUDWindow.FORWARD_ID);

        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertNotNull(dialog);
        assertEquals("Forward Message", dialog.getTitle());
        UserMessageEditor editor = (UserMessageEditor) dialog.getEditor();
        Act forwarded = editor.getObject();
        assertTrue(forwarded.isNew());
        assertEquals(user2, editor.getFrom());
        assertNull(editor.getTo());
        assertTrue(editor.getToUsers().isEmpty());
        assertTrue(editor.getMessage().contains("-------- Original Message --------"));
        editor.setTo(user3);
        assertValid(editor);

        // verify there are no messages from user2 to user3
        List<Act> acts1 = getMessages(user2, user3);
        assertEquals(0, acts1.size());

        EchoTestHelper.fireDialogButton(dialog, UserMessageEditDialog.SEND_ID);

        assertTrue(editor.getObject().isNew()); // the edited object is never sent,  clones are

        // verify there is now a message from user2 to user3
        List<Act> acts2 = getMessages(user2, user3);
        assertEquals(1, acts2.size());
    }

    /**
     * Creates a new {@link MessagingCRUDWindow}.
     *
     * @param context the context
     * @return a new window
     */
    private MessagingCRUDWindow createCRUDWindow(Context context) {
        Archetypes<Act> archetypes = Archetypes.create(MessageArchetypes.MESSAGES, Act.class);
        return new MessagingCRUDWindow(archetypes, context, new HelpContext("foo", null));
    }

    /**
     * Returns all messages from one user to another.
     *
     * @param fromUser the from user
     * @param toUser   the to user
     * @return the messages
     */
    private List<Act> getMessages(User fromUser, User toUser) {
        IArchetypeService service = getArchetypeService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, MessageArchetypes.USER);
        Join<Act, IMObject> to = root.join("to");
        to.on(builder.equal(to.get("entity"), toUser.getObjectReference()));
        query.where(builder.equal(root.get("from"), fromUser.getObjectReference()));

        return service.createQuery(query).getResultList();
    }
}
