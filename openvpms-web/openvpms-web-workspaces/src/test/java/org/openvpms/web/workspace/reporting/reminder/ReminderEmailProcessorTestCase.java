/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.EmailTemplate.SubjectType;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderRule.SendTo;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.app.PracticeMailContext;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.mail.DefaultMailer;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailException;
import org.openvpms.web.component.mail.Mailer;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * Tests the {@link ReminderEmailProcessor}.
 *
 * @author Tim Anderson
 */
public class ReminderEmailProcessorTestCase extends AbstractPatientReminderProcessorTest<EmailReminders> {

    /**
     * Tracks the names of sent attachments.
     */
    private final List<String> attachments = new ArrayList<>();

    /**
     * Practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The reminder processor.
     */
    private ReminderEmailProcessor processor;

    /**
     * The sender.
     */
    private JavaMailSender sender;

    /**
     * The mime message.
     */
    private MimeMessage mimeMessage;

    /**
     * Constructs a {@link ReminderEmailProcessorTestCase}.
     */
    public ReminderEmailProcessorTestCase() {
        super(ContactArchetypes.EMAIL);
    }

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();

        Document handout1 = documentFactory.newDocument()
                .name("Patient Handout.pdf")
                .content("dummy pdf content")
                .mimeType(DocFormats.PDF_TYPE)
                .build();
        Entity attachment1 = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(handout1)
                .build();

        Document handout2 = documentFactory.newDocument()
                .name("Customer Handout.pdf")
                .content("dummy pdf content")
                .mimeType(DocFormats.PDF_TYPE)
                .build();
        Entity attachment2 = documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document(handout2)
                .build();

        Entity documentTemplate = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .emailTemplate()
                .subject("openvpms:get(., 'reminderType.entity.name')")
                .subjectType(SubjectType.XPATH)
                .content("text")
                .addAttachments(attachment1, attachment2)
                .add()
                .build();

        reminderFactory.updateReminderType(reminderType)
                .name("Vaccination Reminder")
                .newCount()
                .count(0)
                .interval(1, DateUnits.WEEKS)
                .template(documentTemplate)
                .newRule().email().sendTo(SendTo.ANY).add()
                .add()
                .build();

        MailerFactory mailerFactory = Mockito.mock(MailerFactory.class);
        sender = Mockito.mock(JavaMailSender.class);
        mimeMessage = Mockito.mock(MimeMessage.class);
        Mockito.when(sender.createMimeMessage()).thenReturn(mimeMessage);

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        MailContext mailContext = new PracticeMailContext(context);
        Mailer mailer = new DefaultMailer(mailContext, sender, Mockito.mock(DocumentHandlers.class)) {
            @Override
            public void addAttachment(Document document) {
                super.addAttachment(document);
                attachments.add(document.getName());
            }
        };
        Mockito.when(mailerFactory.create(Mockito.any(), Mockito.any())).thenReturn(mailer);

        IArchetypeService service = getArchetypeService();
        EmailTemplateEvaluator evaluator = new EmailTemplateEvaluator(getArchetypeService(), getLookupService(),
                                                                      null, Mockito.mock(ReportFactory.class),
                                                                      Mockito.mock(DocumentConverter.class));
        ReminderTypes reminderTypes = new ReminderTypes(service);
        CommunicationLogger logger = Mockito.mock(CommunicationLogger.class);
        ReporterFactory reporterFactory = Mockito.mock(ReporterFactory.class);
        ReminderConfiguration config = createConfiguration();
        processor = new ReminderEmailProcessor(mailerFactory, evaluator, reporterFactory, reminderTypes, practice,
                                               reminderRules, patientRules, practiceRules, service, config, logger,
                                               Mockito.mock(MailPasswordResolver.class), actionFactory);
    }

    /**
     * Verifies that emails are sent to the REMINDER contact.
     *
     * @throws Exception for any error
     */
    @Test
    public void testReminderContact() throws Exception {
        Contact email1 = contactFactory.createEmail("x@test.com", false, "REMINDER");
        Contact email2 = contactFactory.createEmail("y@test.com", true);
        Contact email3 = contactFactory.createEmail("z@test.com");
        customer.addContact(email1);
        customer.addContact(email2);
        customer.addContact(email3);

        checkSend(null, "x@test.com");
    }

    /**
     * Verifies that emails can be sent to a contact different to the default.
     *
     * @throws Exception for any error
     */
    @Test
    public void testOverrideContact() throws Exception {
        Contact email1 = contactFactory.createEmail("x@test.com", true, "REMINDER");
        Contact email2 = contactFactory.createEmail("y@test.com", false);
        customer.addContact(email1);
        customer.addContact(email2);

        checkSend(email2, "y@test.com");
    }

    /**
     * Verifies that {@link ReminderEmailProcessor#failed(PatientReminders, Throwable)} updates reminders with the
     * failure message.
     */
    @Test
    public void testFailed() {
        Contact email = contactFactory.createEmail("x@test.com", true, "REMINDER");
        customer.addContact(email);

        Date tomorrow = DateRules.getTomorrow();
        Act item = reminderFactory.newSMSReminder()
                .sendDate(DateRules.getToday())
                .dueDate(tomorrow)
                .build();
        Act reminder = createReminder(tomorrow, reminderType, item);

        doThrow(new MailException(MailException.ErrorCode.FailedToSend, "x@test.com", "some error"))
                .when(sender).send(Mockito.<MimeMessage>any());

        EmailReminders reminders = prepare(item, reminder, null);
        try {
            processor.process(reminders);
            fail("Expected exception to be thrown");
        } catch (OpenVPMSException expected) {
            assertTrue(processor.failed(reminders, expected));
            checkItem(get(item), ReminderItemStatus.ERROR, "Failed to send email to x@test.com: some error");
        }
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder type has no reminder count.
     */
    @Test
    public void testMissingReminderCount() {
        checkMissingReminderCount(contactFactory.createEmail("x@test.com", true, "REMINDER"));
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder count has no template.
     */
    @Test
    public void testMissingReminderCountTemplate() {
        checkMissingReminderCountTemplate(contactFactory.createEmail("x@test.com", true, "REMINDER"));
    }

    /**
     * Returns the reminder processor.
     *
     * @return the reminder processor
     */
    @Override
    protected ReminderEmailProcessor getProcessor() {
        return processor;
    }

    /**
     * Creates a PENDING reminder item for reminder count 0.
     *
     * @param send    the send date
     * @param dueDate the due date
     * @return a new reminder item
     */
    @Override
    protected Act createReminderItem(Date send, Date dueDate) {
        return reminderFactory.newEmailReminder()
                .sendDate(send)
                .dueDate(dueDate)
                .build();
    }

    /**
     * Sends an email reminder.
     *
     * @param contact the contact to use. May be {@code null}
     * @param to      the expected to address
     * @throws Exception for any error
     */
    private void checkSend(Contact contact, String to) throws Exception {
        EmailReminders reminders = prepare(contact);
        processor.process(reminders);
        InternetAddress from = new InternetAddress("foo@bar.com", "Test Practice");
        verify(mimeMessage).setFrom(from);
        verify(mimeMessage).setSubject("Vaccination Reminder", "UTF-8");
        verify(mimeMessage).setRecipients(Message.RecipientType.TO, new Address[]{new InternetAddress(to)});
        verify(mimeMessage).setReplyTo(new Address[]{from});
        assertEquals(2, attachments.size());
        assertTrue(attachments.contains("Customer Handout.pdf"));
        assertTrue(attachments.contains("Patient Handout.pdf"));
    }

    /**
     * Prepares a reminder for send.
     *
     * @param contact the contact to use. May be {@code null}
     * @return the reminders
     */
    private EmailReminders prepare(Contact contact) {
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);
        return prepare(item, reminder, contact);
    }
}
