/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.supplier.AbstractSupplierStockItemEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Tests the {@link DeliveryItemEditor} class.
 *
 * @author Tim Anderson
 */
public class DeliveryItemEditorTestCase extends AbstractSupplierStockItemEditorTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The layout context.
     */
    private LayoutContext context;

    /**
     * 'Each' unit-of-measure.
     */
    private Lookup each;

    /**
     * Box unit-of-measure
     */
    private Lookup box;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        each = lookupFactory.createUnitOfMeasure("EACH");
        box = lookupFactory.createUnitOfMeasure("BOX");
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link DeliveryItemEditor} for
     * <em>act.supplierDeliveryItem</em> and <em>act.supplierReturnItem</em>instances.
     */
    @Test
    public void testFactory() {
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));

        IMObjectEditor editor1 = factory.create(create(SupplierArchetypes.DELIVERY_ITEM, FinancialAct.class),
                                                layoutContext);
        assertTrue(editor1 instanceof DeliveryItemEditor);

        IMObjectEditor editor2 = factory.create(create(SupplierArchetypes.RETURN_ITEM, FinancialAct.class),
                                                layoutContext);
        assertTrue(editor2 instanceof DeliveryItemEditor);
    }

    /**
     * Tests validation.
     */
    @Test
    public void testValidation() {
        Act delivery = create(SupplierArchetypes.DELIVERY, Act.class);
        FinancialAct item = create(SupplierArchetypes.DELIVERY_ITEM, FinancialAct.class);
        DeliveryItemEditor editor = new DeliveryItemEditor(item, delivery, context);

        delivery.setStatus(ActStatus.POSTED);
        assertFalse(editor.isValid());

        Product product = productFactory.createMedication();
        editor.setProduct(product);
        editor.setPackageSize(1);
        editor.setPackageUnits(each.getCode());
        assertTrue(editor.isValid());

        editor.getProperty("packageSize").setValue(null);
        editor.setPackageUnits(null);
        assertFalse(editor.isValid());
    }

    /**
     * Verifies that selecting a product for a manual delivery updates the prices.
     */
    @Test
    public void testProductUpdateForManualDelivery() {
        Party supplier = supplierFactory.createSupplier();
        Act delivery = createDelivery(supplier);
        Product product = productFactory.createMedication();
        createProductSupplier(product, supplier, "A1", 2, box.getCode(), BigDecimal.TEN, BigDecimal.TEN);

        FinancialAct item = createDeliveryItem();

        DeliveryItemEditor editor = new DeliveryItemEditor(item, delivery, context);
        editor.setStockLocation(practiceFactory.createStockLocation());
        assertTrue(editor.isValid());

        editor.setProduct(product);
        assertTrue(editor.isValid());

        assertEquals("A1", editor.getReorderCode());
        assertEquals(2, editor.getPackageSize());
        assertEquals(box.getCode(), editor.getPackageUnits());
        checkEquals(BigDecimal.TEN, editor.getQuantity());
        checkEquals(BigDecimal.TEN, editor.getUnitPrice());
        checkEquals(BigDecimal.TEN, editor.getListPrice());
    }

    /**
     * Verifies that selecting a product for an ESCI delivery doesn't update the prices.
     */
    @Test
    public void testProductUpdateForESCIDelivery() {
        Party supplier = supplierFactory.createSupplier();
        Act delivery = createDelivery(supplier);
        Product product = productFactory.createMedication();
        createProductSupplier(product, supplier, "A1", 2, box.getCode(), BigDecimal.TEN, BigDecimal.TEN);

        FinancialAct item = create(SupplierArchetypes.DELIVERY_ITEM, FinancialAct.class);
        IMObjectBean bean = getBean(item);
        bean.setValue("supplierInvoiceLineId", "1");
        DeliveryItemEditor editor = new DeliveryItemEditor(item, delivery, context);
        editor.setUnitPrice(new BigDecimal(12));
        populate(editor, product, BigDecimal.TEN, "B1", 1, each.getCode(), new BigDecimal(12));
        editor.setStockLocation(practiceFactory.createStockLocation());
        assertTrue(editor.isValid());

        editor.setProduct(product);
        assertTrue(editor.isValid());

        assertEquals("B1", editor.getReorderCode());
        assertEquals(1, editor.getPackageSize());
        assertEquals(each.getCode(), editor.getPackageUnits());
        checkEquals(BigDecimal.TEN, editor.getQuantity());
        checkEquals(new BigDecimal("12"), editor.getUnitPrice());
        checkEquals(new BigDecimal("12"), editor.getListPrice());
    }

    /**
     * Verifies that saving a delivery item doesn't create or update the product-supplier relationship.
     */
    @Test
    public void testSupplierRelationshipNotCreatedOrUpdatedForDelivery() {
        Party supplier = supplierFactory.createSupplier();
        Act delivery = createDelivery(supplier);
        Product product = productFactory.createMedication();
        Party stockLocation = practiceFactory.createStockLocation();

        FinancialAct item1 = createDeliveryItem();

        DeliveryItemEditor editor1 = new DeliveryItemEditor(item1, delivery, context);
        editor1.setStockLocation(stockLocation);
        editor1.setProduct(product);
        editor1.setPackageSize(1);
        assertTrue(SaveHelper.save(editor1));

        // verify no product-supplier relationship has been created
        product = get(product);
        supplier = get(supplier);
        assertTrue(productRules.getProductSuppliers(product, supplier).isEmpty());

        // now add a product-supplier relationship
        createProductSupplier(product, supplier, "A1", 2, box.getCode(), BigDecimal.TEN, BigDecimal.TEN);

        // create a new item, and verify it doesn't update the product-supplier relationship
        FinancialAct item2 = createDeliveryItem();
        DeliveryItemEditor editor2 = new DeliveryItemEditor(item2, delivery, context);
        editor2.setStockLocation(stockLocation);
        editor2.setProduct(product);
        editor2.setListPrice(new BigDecimal("20"));
        assertTrue(SaveHelper.save(editor2));

        // verify the product-supplier relationship hasn't changed
        checkProductSupplier(product, supplier, "A1", 2, box.getCode(), BigDecimal.TEN);
    }

    /**
     * Verifies that for new return items, a product-supplier relationship is created if none already exists.
     */
    @Test
    public void testCreateProductSupplierRelationship() {
        Act deliveryReturn = create(SupplierArchetypes.RETURN, Act.class);
        Party supplier = supplierFactory.createSupplier();
        IMObjectBean bean = getBean(deliveryReturn);
        bean.setTarget("supplier", supplier);
        FinancialAct item = create(SupplierArchetypes.RETURN_ITEM, FinancialAct.class);
        DeliveryItemEditor editor = new DeliveryItemEditor(item, deliveryReturn, context);
        checkCreateProductSupplierRelationship(editor, supplier);
    }

    /**
     * Verifies that for new return items, the product-supplier relationship is updated if it is different.
     */
    @Test
    public void testUpdateProductSupplierRelationship() {
        Party supplier = supplierFactory.createSupplier();
        Act deliveryReturn = create(SupplierArchetypes.RETURN, Act.class);
        IMObjectBean bean = getBean(deliveryReturn);
        bean.setTarget("supplier", supplier);
        FinancialAct item = create(SupplierArchetypes.RETURN_ITEM, FinancialAct.class);
        DeliveryItemEditor editor = new DeliveryItemEditor(item, deliveryReturn, context);
        checkUpdateProductSupplierRelationship(editor);
    }

    /**
     * Creates a delivery.
     *
     * @param supplier the supplier
     * @return the delivery
     */
    private Act createDelivery(Party supplier) {
        Act delivery = create(SupplierArchetypes.DELIVERY, Act.class);
        IMObjectBean bean = getBean(delivery);
        bean.setTarget("supplier", supplier);
        return delivery;
    }

    /**
     * Creates a pre-populated delivery item.
     *
     * @return a new delivery item
     */
    private FinancialAct createDeliveryItem() {
        FinancialAct item = create(SupplierArchetypes.DELIVERY_ITEM, FinancialAct.class);
        IMObjectBean bean = getBean(item);
        bean.setValue("reorderCode", "B1");
        bean.setValue("packageSize", 1);
        bean.setValue("packageUnits", each.getCode());
        bean.setValue("quantity", 10);
        bean.setValue("unitPrice", 12);
        bean.setValue("listPrice", 12);
        bean.setValue("tax", 12);
        return item;
    }
}
