/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientVisitNoteEditor}.
 *
 * @author Tim Anderson
 */
public class PatientVisitNoteEditorTestCase extends AbstractIMObjectEditorTest<PatientVisitNoteEditor> {

    /**
     * Constructs a {@link PatientVisitNoteEditorTestCase}.
     */
    public PatientVisitNoteEditorTestCase() {
        super(PatientVisitNoteEditor.class, PatientArchetypes.CLINICAL_EVENT);
    }

    /**
     * Tests the editor.
     */
    @Test
    public void testEditor() {
        Act visit = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        PatientVisitNoteEditor editor = createEditor(visit);
        editor.getComponent();
        editor.setNote("test");
        SaveHelper.save(editor);

        // reload the acts
        visit = get(visit);
        Act note = get(editor.getNoteEditor().getObject());

        // verify there is a link between the visit and the note
        assertNotNull(note);
        IMObjectBean bean = getBean(visit);
        assertTrue(bean.hasTarget("items", note));

        // check the note content
        assertEquals("test", getBean(note).getString("note"));
    }

    /**
     * Verifies that {@link IMObjectEditor#newInstance()} can be invoked after the editor is saved.
     */
    @Test
    public void testNewInstanceAfterSave() {
        Act visit = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        PatientVisitNoteEditor editor = createEditor(visit);
        editor.getComponent();
        editor.setNote("test");
        SaveHelper.save(editor);

        PatientVisitNoteEditor newInstance = (PatientVisitNoteEditor) editor.newInstance();
        assertNotNull(newInstance);
        assertNotEquals(editor, newInstance);
        assertEquals("test", newInstance.getNote());
    }

    /**
     * Verifies the editor is NOT created for the archetype by {@link IMObjectEditorFactory}.
     * <p/>
     * The {@link PatientClinicalEventActEditor} should be created instead. The {@link PatientVisitNoteEditor} can
     * only be created directly.
     */
    @Override
    public void testFactory() {
        IMObject object = create(PatientArchetypes.CLINICAL_EVENT);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = getFactory().create(object, context);
        assertEquals(PatientClinicalEventActEditor.class, editor.getClass());
    }

    /**
     * Creates an instance of the editor.
     *
     * @param object the object to edit
     * @return the editor
     */
    @Override
    protected PatientVisitNoteEditor createEditor(IMObject object) {
        LocalContext context = new LocalContext();
        context.setPatient(TestHelper.createPatient());
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        return createEditor(object, layoutContext);
    }

    /**
     * Creates an instance of the editor.
     *
     * @param object  the object to edit
     * @param context the layout context
     * @return the editor
     */
    @Override
    protected PatientVisitNoteEditor createEditor(IMObject object, LayoutContext context) {
        return new PatientVisitNoteEditor((Act) object, context);
    }
}
