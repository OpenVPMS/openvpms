/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.worklist;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link DefaultTaskActEditor} class.
 *
 * @author Tim Anderson
 */
public class DefaultTaskActEditorTestCase extends AbstractIMObjectEditorTest<DefaultTaskActEditor> {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The work list.
     */
    private Entity worklist;

    /**
     * The task type.
     */
    private Entity taskType;

    /**
     * The user to populate the author node with.
     */
    private User user;

    /**
     * Constructs a {@link DefaultTaskActEditorTestCase}.
     */
    public DefaultTaskActEditorTestCase() {
        super(DefaultTaskActEditor.class, ScheduleArchetypes.TASK);
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        taskType = schedulingFactory.createTaskType();
        user = userFactory.createClinician();

        worklist = schedulingFactory.newWorkList()
                .addTaskType(taskType, 100, false)
                .build();
    }

    /**
     * Verifies that a task editor can be created and saved when mandatory fields are populated.
     */
    @Test
    public void testSave() {
        DefaultTaskActEditor editor = newEditor(newObject());
        editor.getComponent();
        assertFalse(editor.isValid());
        editor.setCustomer(customerFactory.createCustomer());
        editor.setWorkList(worklist);
        assertFalse(editor.isValid());
        editor.setTaskType(taskType);
        assertFalse(editor.isValid());

        editor.setStartTime(new Date());
        assertValid(editor);  // should now be valid

        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Verifies that the default start time is based on the {@link Context#getWorkListDate()}.
     */
    @Test
    public void testDefaultStartTime() {
        Context context = new LocalContext();
        Date now = DateUtils.truncate(new Date(), Calendar.MINUTE); // the editor excludes seconds, ms

        DefaultTaskActEditor editor1 = newEditor(newObject(), context);
        assertNull(editor1.getStartTime());

        context.setWorkListDate(DateRules.getToday());
        DefaultTaskActEditor editor2 = newEditor(newObject(), context);
        assertTrue(DateRules.compareTo(now, editor2.getStartTime()) <= 0);

        context.setWorkListDate(DateRules.getTomorrow());
        DefaultTaskActEditor editor3 = newEditor(newObject(), context);
        assertEquals(DateRules.getTomorrow(), editor3.getStartTime());

        context.setWorkListDate(DateRules.getYesterday());
        DefaultTaskActEditor editor4 = newEditor(newObject(), context);
        assertEquals(DateRules.getYesterday(), editor4.getStartTime());
    }

    /**
     * Verifies that the end time cannot be set prior to the start time.
     */
    @Test
    public void testTimes() {
        DefaultTaskActEditor editor = createEditor();
        editor.getComponent();

        Date start = DateRules.getToday();
        editor.setStartTime(start);
        Date end = DateRules.getDate(start, -1, DateUnits.MINUTES);
        editor.setEndTime(end);
        assertEquals(editor.getEndTime(), start);
        assertValid(editor);
        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Tests inheritance of the work list, customer and patient from the context.
     */
    @Test
    public void testInheritance() {
        Party customer1 = customerFactory.createCustomer();
        Party customer2 = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer2);
        Entity worklist2 = schedulingFactory.createWorkList();

        LocalContext context = new LocalContext();
        context.setCustomer(customer1);
        context.setWorkList(worklist);
        context.setUser(user);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        // verify the task inherits customer1 and worklist
        Act act1 = create(ScheduleArchetypes.TASK, Act.class);
        DefaultTaskActEditor editor1 = new DefaultTaskActEditor(act1, null, layout);
        editor1.getComponent();
        editor1.setStartTime(new Date());
        assertEquals(customer1, editor1.getCustomer());
        assertEquals(worklist, editor1.getWorkList());
        assertNull(editor1.getPatient());
        editor1.setTaskType(taskType);

        // now save it. It should not inherit anything next time round is it isn't new
        assertTrue(SaveHelper.save(editor1));

        context.setCustomer(customer2);
        context.setPatient(patient);
        context.setWorkList(worklist2);

        DefaultTaskActEditor editor2 = new DefaultTaskActEditor(act1, null, layout);
        editor2.getComponent();

        // verify customer and worklist unchanged, and patient not inherited
        assertEquals(customer1, editor2.getCustomer());
        assertEquals(worklist, editor2.getWorkList());
        assertNull(editor2.getPatient());
    }

    /**
     * Creates a new editor, pre-populating the customer, worklist, and user.
     *
     * @return a new editor
     */
    private DefaultTaskActEditor createEditor() {
        Context context = new LocalContext();
        Party customer = customerFactory.createCustomer();

        // populate the context. These will be used to initialise the task
        context.setCustomer(customer);
        context.setWorkList(worklist);
        context.setUser(user);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Act act = create(ScheduleArchetypes.TASK, Act.class);
        return new DefaultTaskActEditor(act, null, layout);
    }
}
