/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestAppointmentBuilder;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Base class for appointment grid tests.
 *
 * @author Tim Anderson
 */
public class AbstractAppointmentGridTest extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    protected TestSchedulingFactory schedulingFactory;

    /**
     * The appointment rules.
     */
    @Autowired
    protected AppointmentRules rules;

    /**
     * The roster service.
     */
    @Autowired
    protected RosterService rosterService;


    /**
     * Creates an event representing an appointment.
     *
     * @param appointment the appointment
     * @return the event
     */
    protected PropertySet createEvent(Act appointment) {
        PropertySet result = new ObjectSet();
        IMObjectBean bean = getBean(appointment);
        Entity appointmentType = bean.getTarget("appointmentType", Entity.class);
        Party customer = bean.getTarget("customer", Party.class);
        Party patient = bean.getTarget("patient", Party.class);
        Lookup statusLookup = bean.getLookup("status");
        Lookup reasonLookup = bean.getLookup("reason");
        result.set(ScheduleEvent.ACT_START_TIME, appointment.getActivityStartTime());
        result.set(ScheduleEvent.ACT_END_TIME, appointment.getActivityEndTime());
        result.set(ScheduleEvent.ACT_REFERENCE, appointment.getObjectReference());
        result.set(ScheduleEvent.ACT_STATUS, appointment.getStatus());
        result.set(ScheduleEvent.ACT_STATUS_NAME, statusLookup != null ? statusLookup.getName() : null);
        result.set(ScheduleEvent.ACT_REASON, appointment.getReason());
        result.set(ScheduleEvent.ACT_REASON_NAME, reasonLookup != null ? reasonLookup.getName() : null);
        result.set(ScheduleEvent.SCHEDULE_REFERENCE, bean.getTargetRef("schedule"));
        result.set(ScheduleEvent.SCHEDULE_TYPE_REFERENCE, appointmentType.getObjectReference());
        result.set(ScheduleEvent.SCHEDULE_TYPE_NAME, appointmentType.getName());
        result.set(ScheduleEvent.CUSTOMER_NAME, customer != null ? customer.getName() : null);
        result.set(ScheduleEvent.PATIENT_NAME, patient != null ? patient.getName() : null);
        result.set(ScheduleEvent.NOTES, bean.getString("notes"));
        return result;
    }

    /**
     * Creates a {@link ScheduleEvents} for a set of appointments.
     *
     * @param appointments the appointments
     * @return a new {@link ScheduleEvents}
     */
    protected ScheduleEvents getScheduleEvents(Act... appointments) {
        List<PropertySet> events = new ArrayList<>();
        for (Act appointment : appointments) {
            events.add(createEvent(appointment));
        }
        return new ScheduleEvents(events, 0);
    }

    /**
     * Creates an appointment.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @return a new appointment
     */
    protected Act createAppointment(String startTime, String endTime, Entity schedule) {
        return newAppointment(startTime, endTime, schedule)
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .appointmentType(schedulingFactory.createAppointmentType())
                .build();
    }

    /**
     * Returns a partially populated appointment builder.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @return a new appointment builder
     */
    protected TestAppointmentBuilder newAppointment(String startTime, String endTime, Entity schedule) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .endTime(endTime)
                .schedule(schedule);
    }

    /**
     * Verifies a slot  matches that expected.
     *
     * @param grid         the grid view
     * @param schedule     the schedule
     * @param slot         the slot
     * @param time         the expected date/time. May be in ISO format to support timezones
     * @param appointment  the expected appointment. May be {@code null}
     * @param slots        the expected no. of slots the appointment occupies
     * @param availability the expected availability
     */
    protected void checkSlot(AppointmentGrid grid, Entity schedule, int slot, String time, Act appointment, int slots,
                             Availability availability) {
        Date date = time.contains("T") ? Date.from(OffsetDateTime.parse(time).toInstant()) :
                    getDatetime(time);
        Date actualTime = grid.getStartTime(slot);
        assertEquals(date, actualTime);

        // verify the passing the date for the slot returns the correct slot
        assertEquals(slot, grid.getSlot(actualTime));

        Schedule gridSchedule = getSchedule(grid, schedule);
        PropertySet event = grid.getEvent(gridSchedule, slot);
        if (appointment != null) {
            assertNotNull(event);
            assertEquals(appointment.getObjectReference(), event.getReference(ScheduleEvent.ACT_REFERENCE));
        } else {
            assertNull(event);
        }
        if (event != null) {
            int actualSlots = grid.getSlots(event, gridSchedule, slot);
            assertEquals(slots, actualSlots);
        }

        // check availability
        Availability actualAvailability = grid.getAvailability(getSchedule(grid, schedule), slot);
        assertEquals(availability, actualAvailability);
    }

    /**
     * Creates a new schedule.
     *
     * @param slotSize the slot size in minutes
     * @param start    the schedule start time as HH:MM
     * @param end      the schedule end time in HH:MM
     * @return a new schedule
     */
    protected Entity createSchedule(int slotSize, String start, String end) {
        return schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .slotSize(slotSize, DateUnits.MINUTES)
                .addAppointmentType(schedulingFactory.createAppointmentType(), 1, true)
                .times(start, end)
                .build();
    }

    /**
     * Returns the schedule from a grid.
     *
     * @param grid     the grid
     * @param schedule the schedule entity
     * @return the corresponding schedule
     */
    private Schedule getSchedule(AppointmentGrid grid, Entity schedule) {
        for (Schedule sched : grid.getSchedules()) {
            if (sched.getSchedule().equals(schedule)) {
                return sched;
            }
        }
        fail("Schedule not found");
        return null;
    }
}
