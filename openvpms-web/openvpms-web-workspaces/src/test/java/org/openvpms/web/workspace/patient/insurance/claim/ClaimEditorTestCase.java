/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.insurance.TestClaimBuilder;
import org.openvpms.archetype.test.builder.insurance.TestInsuranceFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.archetype.test.TestHelper.getDatetime;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link ClaimEditor}.
 *
 * @author Tim Anderson
 */
public class ClaimEditorTestCase extends AbstractAppTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The insurance factory.
     */
    @Autowired
    private TestInsuranceFactory insuranceFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * The test clinician.
     */
    private User clinician;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The claim handler.
     */
    private User user;

    /**
     * The policy.
     */
    private Act policyAct;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();

        // practice
        practice = practiceFactory.getPractice();

        // customer
        customer = customerFactory.newCustomer()
                .name("MS", "J", "Bloggs")
                .addAddress("12 Broadwater Avenue", "CAPE_WOOLAMAI", "VIC", "3925")
                .addHomePhone("9123456")
                .addWorkPhone("98765432")
                .addMobilePhone("04987654321")
                .addEmail("foo@test.com")
                .build();

        // practice location
        location = practiceFactory.newLocation()
                .addPhone("5123456")
                .addEmail("vetsrus@test.com")
                .build();

        // clinician
        clinician = userFactory.createClinician();

        // claim handler
        user = userFactory.createUser("Z", "Smith");

        // patient
        Date dateOfBirth = DateRules.getDate(DateRules.getToday(), -1, DateUnits.YEARS);
        patient = patientFactory.newPatient()
                .name("Fido")
                .species("CANINE")
                .breed("PUG")
                .male()
                .dateOfBirth(dateOfBirth)
                .addMicrochip("123454321")
                .colour("BLACK")
                .owner(customer)
                .build();

        // patient history
        Act note1 = patientFactory.newNote()
                .startTime("2015-05-01")
                .patient(patient)
                .clinician(clinician)
                .note("Note 1")
                .build();
        Act note2 = patientFactory.newNote()
                .startTime("2015-05-02")
                .patient(patient)
                .clinician(clinician)
                .note("Note 2")
                .build();
        patientFactory.newVisit().startTime("2015-05-01")
                .patient(patient)
                .addItems(note1, note2)
                .build();
        DocumentAct note3 = patientFactory.newNote()
                .startTime("2015-07-01")
                .patient(patient)
                .clinician(clinician)
                .note("Note 3")
                .build();
        DocumentAct addendum1 = patientFactory.newAddendum()
                .startTime("2015-07-03")
                .patient(patient)
                .clinician(clinician)
                .note("Note 3 addendum 1")
                .build();
        DocumentAct addendum2 = patientFactory.newAddendum()
                .startTime("2015-07-04")
                .patient(patient)
                .clinician(clinician)
                .note("Note 3 addendum 2")
                .build();
        patientFactory.updateNote(note3).addAddenda(addendum1, addendum2).build();

        patientFactory.newVisit()
                .startTime("2015-07-01")
                .patient(patient)
                .addItems(note3)
                .build();

        // insurer
        Party insurer = insuranceFactory.createInsurer();

        // policy
        policyAct = insuranceFactory.createPolicy(customer, patient, insurer, "POL123456");
    }

    /**
     * Tests the {@link ClaimEditor#delete()} method.
     */
    @Test
    public void testDelete() {
        Product product1 = productFactory.createService();
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        BigDecimal discount1 = new BigDecimal("0.10");
        BigDecimal tax1 = new BigDecimal("0.08");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1, product1, ONE, ONE, discount1, tax1);
        FinancialAct invoice1 = createInvoice(getDate("2017-09-27"), invoiceItem1);

        Document content1 = documentFactory.createPDF();
        DocumentAct documentAct1 = patientFactory.newAttachment().startTime(itemDate1).patient(patient)
                .document(content1).build();
        Document content2 = documentFactory.createDOC();
        DocumentAct documentAct2 = patientFactory.newAttachment().startTime(itemDate1).patient(patient)
                .document(content2).build();

        TestClaimBuilder builder = insuranceFactory.newClaim();
        FinancialAct claimAct = builder.policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .item().diagnosis("VENOM_328").treatmentDates(itemDate1, itemDate1).invoiceItems(invoiceItem1).add()
                .attachments(documentAct1, documentAct2)
                .build();
        FinancialAct item1Act = builder.getItems().get(0);
        List<DocumentAct> attachments = builder.getAttachments();
        assertEquals(2, attachments.size());

        // simulate generating the attachments
        DocumentAct attachment1 = attachments.get(0);
        Document attachmentContent1 = documentFactory.createPDF();
        attachment1.setDocument(attachmentContent1.getObjectReference());
        DocumentAct attachment2 = attachments.get(1);
        Document attachmentContent2 = documentFactory.createDOC();
        attachment2.setDocument(attachmentContent2.getObjectReference());
        save(attachment1, attachment2);

        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                ClaimEditor editor = createEditor(claimAct);
                editor.delete();
            }
        });

        assertNull(get(claimAct));
        assertNull(get(item1Act));
        assertNull(get(attachment1));
        assertNull(get(attachmentContent1));
        assertNull(get(claimAct));
        assertNull(get(attachment2));
        assertNull(get(attachmentContent2));

        // verify the original documents haven't been deleted
        assertNotNull(get(documentAct1));
        assertNotNull(get(content1));
        assertNotNull(get(documentAct2));
        assertNotNull(get(content2));

        // verify the policy hasn't been deleted
        assertNotNull(get(policyAct));

        // verify the invoice hasn't been deleted
        assertNotNull(get(invoiceItem1));
        assertNotNull(get(invoice1));
    }

    /**
     * Verifies that when a claim item is deleted, no associated invoice items are deleted.
     */
    @Test
    public void testDeleteClaimItem() {
        Date date1 = getDatetime("2017-09-27 10:00:00");
        FinancialAct invoiceItem1 = createInvoiceItem(date1);
        FinancialAct invoice1 = createPaidInvoice(date1, invoiceItem1);

        Date date2 = getDatetime("2017-10-27 15:00:00");
        FinancialAct invoiceItem2 = createInvoiceItem(date2);
        FinancialAct invoice2 = createPaidInvoice(date2, invoiceItem2);

        FinancialAct item1Act = insuranceFactory.newClaimItem().diagnosis("VENOM_328")
                .treatmentDates(date1, date1)
                .invoiceItems(invoiceItem1)
                .build();
        FinancialAct item2Act = insuranceFactory.newClaimItem().diagnosis("VENOM_328")
                .treatmentDates(date2, date2)
                .invoiceItems(invoiceItem2)
                .build();
        FinancialAct claimAct = insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician).
                claimHandler(user)
                .addItem(item1Act)
                .addItem(item2Act)
                .build();

        ClaimEditor editor = createEditor(claimAct);

        // verify the tax and amount are correct
        checkClaim(claimAct, new BigDecimal("1.64"), BigDecimal.valueOf(18));

        // remove the second item
        editor.getItems().remove(item2Act);

        // verify the tax and amount have updated
        checkClaim(claimAct, new BigDecimal("0.82"), BigDecimal.valueOf(9));

        // save the editor
        assertTrue(SaveHelper.save(editor));

        // verify the claim item has been deleted
        assertNull(get(item2Act));

        // verify the invoices haven't been deleted
        assertNotNull(get(invoice1));
        assertNotNull(get(invoiceItem1));
        assertNotNull(get(invoice2));
        assertNotNull(get(invoiceItem2));

        // verify the other claim item hasn't been deleted
        assertNotNull(item1Act);
    }

    /**
     * Verifies that when an attachment is deleted, the associated document is also deleted.
     */
    @Test
    public void testDeleteAttachment() {
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1);
        createPaidInvoice(getDate("2017-09-27"), invoiceItem1);

        Document content = documentFactory.createPDF();
        DocumentAct documentAct = patientFactory.newAttachment().startTime(itemDate1).patient(patient)
                .document(content).build();

        FinancialAct item1Act = insuranceFactory.newClaimItem()
                .diagnosis("VENOM_328").treatmentDates(itemDate1, itemDate1)
                .invoiceItems(invoiceItem1)
                .build();
        TestClaimBuilder builder = insuranceFactory.newClaim();
        FinancialAct claimAct = builder.policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .addItem(item1Act)
                .attachment(documentAct)
                .build();
        List<DocumentAct> attachments = builder.getAttachments();
        assertEquals(1, attachments.size());

        // simulate generating the attachment
        DocumentAct attachment = attachments.get(0);
        Document copy = documentFactory.createPDF();
        attachment.setDocument(copy.getObjectReference());
        save(attachment);

        ClaimEditor editor = createEditor(claimAct);

        editor.getAttachments().remove(attachment);
        assertTrue(SaveHelper.save(editor));

        assertNull(get(attachment));
        assertNull(get(copy));

        // verify the original document is untouched
        assertNotNull(get(documentAct));
        assertNotNull(get(content));
    }

    /**
     * Verifies that when an invoice item is deleted from a claim, it is not deleted from the invoice.
     */
    @Test
    public void testDeleteInvoiceItem() {
        Date date1 = getDatetime("2017-09-27 10:00:00");
        FinancialAct invoiceItem1 = createInvoiceItem(date1);
        FinancialAct invoiceItem2 = createInvoiceItem(date1);
        FinancialAct invoice1 = createPaidInvoice(date1, invoiceItem1, invoiceItem2);

        FinancialAct item1Act = insuranceFactory.newClaimItem()
                .diagnosis("VENOM_328")
                .treatmentDates(date1, date1)
                .invoiceItems(invoiceItem1, invoiceItem2)
                .build();
        FinancialAct claimAct = insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .addItem(item1Act)
                .build();

        ClaimEditor editor = createEditor(claimAct);

        // verify the tax and amount are correct
        checkClaim(claimAct, new BigDecimal("1.64"), BigDecimal.valueOf(18));

        ClaimItemEditor itemEditor = (ClaimItemEditor) editor.getItems().getEditor(item1Act);
        assertNotNull(itemEditor);

        ChargeCollectionEditor chargeCollectionEditor = itemEditor.getChargeCollectionEditor();
        chargeCollectionEditor.getComponent();
        chargeCollectionEditor.remove(invoiceItem1);

        // verify the tax and amount have updated
        checkClaim(claimAct, new BigDecimal("0.82"), BigDecimal.valueOf(9));

        // save the editor
        assertTrue(SaveHelper.save(editor));

        // verify the invoices haven't been deleted
        assertNotNull(get(invoice1));
        assertNotNull(get(invoiceItem1));
        assertNotNull(get(invoiceItem2));
    }

    /**
     * Verifies that if a claim has a policy for a different customer fail validation.
     */
    @Test
    public void testClaimWithPolicyForDifferentCustomer() {
        Product product1 = productFactory.createMedication();
        Date itemDate1 = getDatetime("2017-09-27 10:00:00");
        BigDecimal discount1 = new BigDecimal("0.10");
        BigDecimal tax1 = new BigDecimal("0.08");
        FinancialAct invoiceItem1 = createInvoiceItem(itemDate1, product1, ONE, ONE, discount1, tax1);
        createPaidInvoice(getDate("2017-09-27"), invoiceItem1);

        FinancialAct item1Act = insuranceFactory.newClaimItem()
                .diagnosis("VENOM_328")
                .treatmentDates(itemDate1, itemDate1)
                .invoiceItems(invoiceItem1)
                .build();
        FinancialAct claimAct = insuranceFactory.newClaim().policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .addItem(item1Act)
                .build();

        ClaimEditor editor1 = createEditor(claimAct);
        assertTrue(editor1.isValid());

        // change the policy customer, to simulate a change of patient ownership.
        IMObjectBean bean = getBean(policyAct);
        Party customer2 = customerFactory.createCustomer();
        bean.setTarget("customer", customer2);
        bean.save();

        ClaimEditor editor2 = createEditor(claimAct);
        assertInvalid(editor2, "Cannot make a claim with this policy.\n\n" +
                               "It belongs to a different customer: " + customer2.getName() + ".");
    }

    /**
     * Verifies a gap claim is invalid if the invoice is fully paid.
     */
    @Test
    public void testCannotMakeGapClaimWhenInvoiceFullyPaid() {
        Date date1 = new Date();
        FinancialAct invoiceItem1 = createInvoiceItem(date1);
        FinancialAct invoiceItem2 = createInvoiceItem(date1);
        createPaidInvoice(date1, invoiceItem1, invoiceItem2);

        // create a gap claim
        FinancialAct item1Act = insuranceFactory.newClaimItem()
                .diagnosis("VENOM_328")
                .treatmentDates(date1, date1)
                .invoiceItems(invoiceItem1, invoiceItem2)
                .build();
        FinancialAct claimAct = insuranceFactory.newClaim().policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .gapClaim(true)
                .addItem(item1Act)
                .build();

        ClaimEditor editor = createEditor(claimAct);
        assertInvalid(editor, "Cannot make a gap claim as all invoices in this claim have been fully paid.");
    }

    /**
     * Verifies adding a visit claims the associated invoice items.
     */
    @Test
    public void testAddVisit() {
        Date date1 = new Date();
        FinancialAct invoiceItem1 = createInvoiceItem(date1);
        FinancialAct invoiceItem2 = createInvoiceItem(date1);
        createPaidInvoice(date1, invoiceItem1, invoiceItem2);

        Act visit = patientFactory.newVisit()
                .reason("CHECKUP")
                .startTime(new Date())
                .endTime(new Date())
                .patient(patient)
                .addChargeItems(invoiceItem1, invoiceItem2)
                .build();

        FinancialAct claimAct = insuranceFactory.newClaim()
                .policy(policyAct)
                .location(location)
                .clinician(clinician)
                .claimHandler(user)
                .build(false);
        ClaimEditor editor = createEditor(claimAct);

        editor.getItems().addVisit(visit);
        assertValid(editor);
        assertTrue(SaveHelper.save(editor));

        // verify the invoice items have been claimed
        checkClaim(claimAct, new BigDecimal("1.64"), BigDecimal.valueOf(18));
        FinancialAct item = getBean(claimAct).getTarget("items", FinancialAct.class);
        List<Reference> chargeItemsRefs = getBean(item).getTargetRefs("items");
        assertEquals(2, chargeItemsRefs.size());
        assertTrue(chargeItemsRefs.contains(invoiceItem1.getObjectReference()));
        assertTrue(chargeItemsRefs.contains(invoiceItem2.getObjectReference()));
    }

    /**
     * Creates a new editor.
     *
     * @param claim the claim to edit
     * @return a new editor
     */
    private ClaimEditor createEditor(FinancialAct claim) {
        LocalContext context = new LocalContext();
        context.setPatient(patient);
        context.setCustomer(customer);
        context.setPractice(practice);
        context.setLocation(location);
        context.setUser(user);
        LayoutContext layout = new DefaultLayoutContext(true, context, new HelpContext("foo", null));
        ClaimEditor editor = new TestClaimEditor(claim, layout);
        editor.getComponent();
        return editor;
    }

    /**
     * Verifies a claim matches that expected.
     *
     * @param claim the claim
     * @param tax   the expected tax amount
     * @param total the expected total
     */
    private void checkClaim(FinancialAct claim, BigDecimal tax, BigDecimal total) {
        checkEquals(tax, claim.getTaxAmount());
        checkEquals(total, claim.getTotal());
    }

    /**
     * Creates an invoice item, with quantity=1, price=10, discount=1, tax=0.82, total=9
     *
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem(Date date) {
        BigDecimal discount = BigDecimal.ONE;
        BigDecimal tax = new BigDecimal("0.82");
        return createInvoiceItem(date, productFactory.createService(), ONE, BigDecimal.TEN, discount, tax);
    }

    /**
     * Creates an invoice item.
     *
     * @param date     the date
     * @param product  the product
     * @param quantity the quantity
     * @param price    the unit price
     * @param discount the discount
     * @param tax      the tax
     * @return the new invoice item
     */
    private FinancialAct createInvoiceItem(Date date, Product product, BigDecimal quantity, BigDecimal price,
                                           BigDecimal discount, BigDecimal tax) {
        return accountFactory.newInvoiceItem()
                .startTime(date)
                .patient(patient)
                .clinician(clinician)
                .product(product)
                .quantity(quantity)
                .unitPrice(price)
                .discount(discount)
                .tax(tax)
                .build();
    }

    /**
     * Creates and saves a POSTED invoice.
     *
     * @param date  the invoice date
     * @param items the invoice items
     * @return the invoice
     */
    private FinancialAct createInvoice(Date date, FinancialAct... items) {
        return accountFactory.newInvoice()
                .startTime(date)
                .customer(customer)
                .clinician(clinician)
                .status(FinancialActStatus.POSTED)
                .add(items)
                .build();
    }


    /**
     * Creates and saves a POSTED invoice, and simulates its payment.
     *
     * @param date  the invoice date
     * @param items the invoice items
     * @return the invoice
     */
    private FinancialAct createPaidInvoice(Date date, FinancialAct... items) {
        FinancialAct invoice = createInvoice(date, items);
        invoice.setAllocatedAmount(invoice.getTotal());  // simulate payment
        save(invoice);
        return invoice;
    }
}
