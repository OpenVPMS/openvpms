/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link TestEditor}.
 *
 * @author Tim Anderson
 */
public class TestEditorTestCase extends AbstractIMObjectEditorTest<TestEditor> {

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Constructs a {@link TestEditorTestCase}.
     */
    public TestEditorTestCase() {
        super(TestEditor.class, LaboratoryArchetypes.TEST);
    }

    /**
     * Tests which fields are editable.
     * <p/>
     * Tests that have a code, don't have editable fields, as these are expected to be managed via a laboratory.
     */
    @Test
    public void testEditable() {
        Entity test1 = laboratoryFactory.newTest()
                .build(false);

        TestEditor editor1 = newEditor(test1);
        editor1.getComponent();
        checkEditable(editor1, true);

        Entity test2 = laboratoryFactory.newTest()
                .code(LaboratoryArchetypes.TEST_CODE, "AB1234")
                .build(false);
        TestEditor editor2 = newEditor(test2);
        editor2.getComponent();
        checkEditable(editor2, false);
    }

    /**
     * Verifies that the investigation type is required, and only valid investigation types can be assigned.
     */
    @Test
    public void testValidateInvestigationType() {
        Entity test1 = laboratoryFactory.newTest()
                .build(false);

        // verify the investigation type is required
        TestEditor editor1 = newEditor(test1);
        editor1.getComponent();
        assertInvalid(editor1, "Investigation Type is required");

        // verify the investigation type is editable, and can be assigned
        checkEditable(editor1, true);
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        editor1.setInvestigationType(investigationType1);
        assertValid(editor1);

        // verify that for tests created by a laboratory service, the investigation type cannot be edited
        Entity test2 = laboratoryFactory.newTest()
                .code(LaboratoryArchetypes.TEST_CODE, "AB1234")
                .investigationType(laboratoryFactory.createInvestigationType())
                .build(false);
        TestEditor editor2 = newEditor(test2);
        editor2.getComponent();
        checkEditable(editor2, false);
        assertValid(editor2);

        // verify that user created tests cannot be assigned an investigation type managed by a laboratory service
        Entity test3 = laboratoryFactory.newTest()
                .build(false);
        Entity investigationType3 = laboratoryFactory.newInvestigationType()
                .typeId("ATYPEID")
                .build();
        TestEditor editor3 = newEditor(test3);
        editor3.getComponent();
        editor3.setInvestigationType(investigationType3);
        assertInvalid(editor3, investigationType3.getName() + " is managed by a laboratory service, and cannot be " +
                               "assigned to this Test");

        // verify that user created tests cannot be assigned an investigation type associated with an HL7 laboratory
        Entity test4 = laboratoryFactory.newTest()
                .build(false);
        Entity laboratory4 = laboratoryFactory.createHL7Laboratory(practiceFactory.createLocation(),
                                                                     userFactory.createUser());
        Entity investigationType4 = laboratoryFactory.newInvestigationType()
                .laboratory(laboratory4)
                .build();
        TestEditor editor4 = newEditor(test4);
        editor4.getComponent();
        editor4.setInvestigationType(investigationType4);
        assertInvalid(editor4, investigationType4.getName() + " has " + laboratory4.getName() +
                               " as its Laboratory and cannot be assigned to this Test");
    }

    /**
     * Verifies that fields are editable or not.
     *
     * @param editor  the editor
     * @param canEdit determines if fields should be editable or not
     */
    private void checkEditable(TestEditor editor, boolean canEdit) {
        assertFalse(editor.canEdit(TestLayoutStrategy.CODE));
        assertFalse(editor.canEdit(TestLayoutStrategy.GROUP));
        assertFalse(editor.canEdit(TestLayoutStrategy.USE_DEVICE));
        assertEquals(canEdit, editor.canEdit(TestLayoutStrategy.NAME));
        assertEquals(canEdit, editor.canEdit(TestLayoutStrategy.DESCRIPTION));
        assertEquals(canEdit, editor.canEdit(TestLayoutStrategy.TURNAROUND));
        assertEquals(canEdit, editor.canEdit(TestLayoutStrategy.SPECIMEN));
    }

}
