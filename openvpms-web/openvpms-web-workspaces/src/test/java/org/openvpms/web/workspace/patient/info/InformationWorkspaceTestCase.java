/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.info;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.test.AbstractAppTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the patient {@link InformationWorkspace}.
 *
 * @author Tim Anderson
 */
public class InformationWorkspaceTestCase extends AbstractAppTest {

    /**
     * Verifies the {@link InformationWorkspace#setObject(Party)} method updates the context.
     */
    @Test
    public void testSetObject() {
        Context context = new LocalContext();
        InformationWorkspace workspace = new InformationWorkspace(context, Mockito.mock(Preferences.class));
        Party customer1 = TestHelper.createCustomer();
        Party patient1 = TestHelper.createPatient(customer1);
        context.setCustomer(customer1);
        context.setPatient(patient1);

        Party patient2 = TestHelper.createPatient();
        workspace.setObject(patient2);

        assertEquals(patient2, workspace.getObject());

        // verify the context has been updated
        assertEquals(patient2, context.getPatient());
        assertNull(context.getCustomer());
    }

}