/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.archetype.test.builder.party.TestLocationContactBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderConfigurationBuilder;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.common.EntityLink;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base class for {@link PatientReminderProcessor} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPatientReminderProcessorTest<T extends PatientReminders> extends AbstractAppTest {

    /**
     * The action factory.
     */
    @Autowired
    protected ActionFactory actionFactory;

    /**
     * The patient rules.
     */
    @Autowired
    protected PatientRules patientRules;

    /**
     * Reminder rules.
     */
    @Autowired
    protected ReminderRules reminderRules;

    /**
     * The contact factory.
     */
    @Autowired
    protected TestContactFactory contactFactory;

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    protected TestReminderFactory reminderFactory;

    /**
     * The test customer.
     */
    protected Party customer;

    /**
     * The test patient.
     */
    protected Party patient;

    /**
     * The reminder type.
     */
    protected Entity reminderType;

    /**
     * The practice.
     */
    protected Party practice;

    /**
     * The contact archetype.
     */
    private final String archetype;

    /**
     * Constructs a {@link AbstractPatientReminderProcessorTest}.
     *
     * @param archetype the contact archetype
     */
    public AbstractPatientReminderProcessorTest(String archetype) {
        this.archetype = archetype;
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        customer = customerFactory.createCustomer("J", "Bloggs");
        patient = patientFactory.newPatient()
                .name("Spot")
                .owner(customer)
                .build();

        Entity settings = practiceFactory.newMailServer()
                .host("localhost")
                .build();

        practice = practiceFactory.newPractice()
                .name("Test Practice")
                .addEmail("foo@bar.com")
                .mailServer(settings)
                .build(false);

        reminderType = reminderFactory.newReminderType()
                .name("Vaccination")
                .defaultInterval(1, DateUnits.MONTHS)
                .cancelInterval(1, DateUnits.DAYS)
                .build();
    }

    /**
     * Verifies that the reminder item status is set to ERROR, when the customer has no relevant contact.
     */
    @Test
    public void testNoContact() {
        checkNoContact();
    }

    /**
     * Verifies that if a reminder item was not sent in time, it is cancelled.
     */
    @Test
    public void testOutOfDate() {
        checkCancelItem(DateRules.getDate(new Date(), -1, DateUnits.MONTHS), "Reminder not processed in time");
    }

    /**
     * Verifies that if a reminder type is inactive, the reminder is cancelled.
     */
    @Test
    public void testInactiveReminderType() {
        reminderType.setActive(false);
        save(reminderType);
        checkCancelItem(DateRules.getToday(), "Reminder Type is inactive");
    }

    /**
     * Verifies that if a patient associated with a reminder item is deceased, the reminder item is cancelled.
     */
    @Test
    public void testDeceasedPatient() {
        patientRules.setDeceased(patient);
        checkCancelItem(DateRules.getToday(), "Patient is deceased");
    }

    /**
     * Verifies that if a patient associated with a reminder item is inactive, the reminder item is cancelled.
     */
    @Test
    public void testInactivePatient() {
        patient.setActive(false);
        checkCancelItem(DateRules.getToday(), "Patient is inactive");
    }

    /**
     * Verifies that if a customer associated with a reminder item is inactive, the reminder item is cancelled.
     */
    @Test
    public void testInactiveCustomer() {
        customer.setActive(false);
        checkCancelItem(DateRules.getToday(), "Customer is inactive");
    }

    /**
     * Verifies that the reminder item status is set to ERROR, when the customer has no relevant contact.
     */
    protected void checkNoContact() {
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);
        PatientReminders reminders = prepare(item, reminder, null);
        assertEquals(1, reminders.getErrors().size());
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());
        checkItem(item, ReminderItemStatus.ERROR, "Customer has no " +
                                                  DescriptorHelper.getDisplayName(archetype, getArchetypeService()));
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder type has no reminder count.
     * <p/.
     * This only applies when reminders are being sent, not listed or exported.
     *
     * @param contact the customer contact
     */
    protected void checkMissingReminderCount(Contact contact) {
        customer.addContact(contact);
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Entity invalidReminderType = reminderFactory.newReminderType()
                .name("Xremindertype")
                .defaultInterval(1, DateUnits.MONTHS)
                .cancelInterval(1, DateUnits.DAYS)
                .build();
        Act reminder = createReminder(tomorrow, invalidReminderType, item);
        PatientReminders reminders = prepare(item, reminder, null);
        assertEquals(1, reminders.getErrors().size());
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());
        checkItem(item, ReminderItemStatus.ERROR, "The Reminder Type 'Xremindertype' has no Reminder Count 0");
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder count has no template.
     * <p/.
     * This only applies when reminders are being sent, not listed or exported.
     *
     * @param contact the customer contact
     */
    protected void checkMissingReminderCountTemplate(Contact contact) {
        customer.addContact(contact);
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        IMObjectBean bean = getBean(reminderType);
        for (Entity count : bean.getTargets("counts", Entity.class)) {
            IMObjectBean countBean = getBean(count);
            EntityLink template = countBean.getObject("template", EntityLink.class);
            if (template != null) {
                countBean.removeValue("template", template);
                countBean.save();
            }
        }
        Act reminder = createReminder(tomorrow, reminderType, item);
        PatientReminders reminders = prepare(item, reminder, null);
        assertEquals(1, reminders.getErrors().size());
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());
        checkItem(item, ReminderItemStatus.ERROR, "Cannot print reminder. It does not have a document template");
    }

    /**
     * Returns the reminder processor.
     *
     * @return the reminder processor
     */
    protected abstract PatientReminderProcessor<T> getProcessor();

    /**
     * Creates a PENDING reminder item for reminder count 0.
     *
     * @param send    the send date
     * @param dueDate the due date
     * @return a new reminder item
     */
    protected abstract Act createReminderItem(Date send, Date dueDate);

    /**
     * Verifies that a reminder item is cancelled.
     *
     * @param send    the item send date
     * @param message the expected message
     */
    protected void checkCancelItem(Date send, String message) {
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(send, tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);

        ReminderEvent event = new ReminderEvent(reminder, item, patient, customer);
        PatientReminderProcessor<T> processor = getProcessor();
        T reminders = processor.prepare(Collections.singletonList(event), ReminderType.GroupBy.CUSTOMER,
                                                       new Date(), false);
        assertEquals(1, reminders.getCancelled().size());
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());
        checkItem(item, ReminderItemStatus.CANCELLED, message);

        assertTrue(processor.complete(reminders));
    }

    /**
     * Verifies an item matches that expected.
     *
     * @param item    the item
     * @param status  the expect status
     * @param message the expected error message
     */
    protected void checkItem(Act item, String status, String message) {
        assertEquals(status, item.getStatus());
        assertEquals(message, getBean(item).getString("error"));
    }

    /**
     * Prepares a reminder for send.
     *
     * @param item     the reminder item
     * @param reminder the reminder
     * @param contact  the contact to use. May be {@code null}
     * @return the reminders
     */
    protected T prepare(Act item, Act reminder, Contact contact) {
        ReminderEvent event = new ReminderEvent(reminder, item, patient, customer, contact);
        return prepare(ReminderType.GroupBy.CUSTOMER, event);
    }

    /**
     * Prepares reminders for send.
     *
     * @param groupBy the reminder grouping policy. This determines which document template is selected
     * @param events  the reminder events
     * @return the reminders
     */
    protected T prepare(ReminderType.GroupBy groupBy, ReminderEvent... events) {
        return getProcessor().prepare(Arrays.asList(events), groupBy, new Date(), false);
    }

    /**
     * Creates a new reminder configuration.
     *
     * @return a new configuration
     */
    protected ReminderConfiguration createConfiguration() {
        Entity config = newReminderConfiguration()
                .build(false);
        return new ReminderConfiguration(config, getArchetypeService());
    }

    /**
     * Returns a pre-populated reminder configuration builder.
     *
     * @return the reminder configuration builder
     */
    protected TestReminderConfigurationBuilder newReminderConfiguration() {
        Party location = practiceFactory.newLocation().name("Vets R Us")
                .addPhone("9123 4567")
                .build();
        return reminderFactory.newReminderConfiguration()
                .emailAttachments(false)
                .location(location);
    }

    /**
     * Creates a new <em>contact.location</em>
     *
     * @param address   the address
     * @param preferred if {@code true}, flags the contact as preferred
     * @param purpose   the contact purpose. May be {@code null}
     * @return a new location contact
     */
    protected Contact createLocation(String address, boolean preferred, String purpose) {
        TestLocationContactBuilder<?, ?> builder = contactFactory.newLocation()
                .address(address)
                .suburbCode("THORNBURY")
                .stateCode("VIC")
                .postcode("3071")
                .preferred(preferred);
        if (purpose != null) {
            builder.purposes(purpose);
        }
        return builder.build();
    }

    /**
     * Creates a reminder for the patient.
     *
     * @param dueDate      the due date
     * @param reminderType the reminder type
     * @param item         the reminder item
     * @return a new reminder
     */
    protected Act createReminder(Date dueDate, Entity reminderType, Act item) {
        return createReminder(dueDate, patient, reminderType, item);
    }

    /**
     * Creates a reminder for a patient.
     *
     * @param dueDate      the due date
     * @param patient      the patient
     * @param reminderType the reminder type
     * @param item         the reminder item
     * @return a new reminder
     */
    protected Act createReminder(Date dueDate, Party patient, Entity reminderType, Act item) {
        return reminderFactory.newReminder()
                .dueDate(dueDate)
                .patient(patient)
                .reminderType(reminderType)
                .addItems(item)
                .build();
    }
}
