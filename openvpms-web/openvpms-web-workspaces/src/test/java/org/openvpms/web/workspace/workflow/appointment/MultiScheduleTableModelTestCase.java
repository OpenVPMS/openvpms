/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import nextapp.echo2.app.Label;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.echo.table.Cell;
import org.openvpms.web.echo.table.TableEx;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleColours;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link MultiScheduleTableModel}.
 *
 * @author Tim Anderson
 */
public class MultiScheduleTableModelTestCase extends AbstractAppointmentGridTest {

    /**
     * Tests the {@link MultiScheduleTableModel#getEvent(Cell)} method.
     */
    @Test
    public void testGetEvent() {
        Entity schedule = createSchedule(60, "09:00", "17:00"); // 9am-5pm schedule, 1 hour slots
        Date date = getDate("2019-10-07");
        Act appointment1 = createAppointment("2019-10-07 08:30", "2019-10-07 09:30", schedule);
        Act appointment2 = createAppointment("2019-10-07 11:00", "2019-10-07 11:15", schedule);
        Act appointment3 = createAppointment("2019-10-07 12:30", "2019-10-07 13:00", schedule);
        Act appointment4 = createAppointment("2019-10-07 16:45", "2019-10-07 17:15", schedule);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule, getScheduleEvents(appointment1, appointment2, appointment3, appointment4));
        MultiScheduleGrid grid = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        MultiScheduleTableModel model = new MultiScheduleTableModel(grid, new LocalContext(),
                                                                    Mockito.mock(ScheduleColours.class));
        assertEquals(3, model.getColumnCount());         // time, schedule, time
        assertEquals(10, model.getRowCount());           // 8:00 -> 17:00
        checkEvent(model, 0, 0, null, null);             // time column
        checkEvent(model, 1, 0, schedule, appointment1); // 8:00
        checkEvent(model, 1, 1, schedule, appointment1); // 9:00
        checkEvent(model, 1, 2, schedule, null);         // 10:00
        checkEvent(model, 1, 3, schedule, appointment2); // 11:00
        checkEvent(model, 1, 4, schedule, appointment3); // 12:00
        checkEvent(model, 1, 5, schedule, null);         // 13:00
        checkEvent(model, 1, 8, schedule, appointment4); // 16:00
        checkEvent(model, 1, 9, schedule, appointment4); // 17:00
        checkEvent(model, 2, 0, null, null);             // time column

        // invalid
        checkEvent(model, -1, -1, null, null);           // invalid column, row
        checkEvent(model, 1, -1, schedule, null);        // schedule exists, but invalid row
        checkEvent(model, 1, 10, schedule, null);        // schedule exists, but time doesn't extend to 18:00
        checkEvent(model, 3, 0, null, null);             // invalid column, row
    }

    /**
     * Verifies appointments are rendered correctly.
     */
    @Test
    public void testAppointmentRendering() {
        Entity schedule = createSchedule(60, "09:00", "17:00"); // 9am-5pm schedule, 1 hour slots
        Date date = getDate("2024-10-09");

        Party customer1 = customerFactory.createCustomer("MR", "J", "Smith");
        Party patient1 = patientFactory.createPatient("Fido", customer1);
        Act appointment1 = createAppointment("2024-10-09 08:30", "2024-10-09 09:30", schedule, customer1, patient1);

        // create an appointment with no patient
        Party customer2 = customerFactory.createCustomer("MS", "S", "Bloggs");
        Act appointment2 = createAppointment("2024-10-09 11:00", "2024-10-09 11:15", schedule, customer2, null);

        // create an appointment with no reason
        Party customer3 = customerFactory.createCustomer(null, "F", "Jones");
        Party patient3 = patientFactory.createPatient("Spot", customer3);
        Act appointment3 = newAppointment("2024-10-09 12:30", "2024-10-09 13:00", schedule)
                .customer(customer3)
                .patient(patient3)
                .appointmentType(schedulingFactory.createAppointmentType("New Patient"))
                .reason(ValueStrategy.value(null))
                .build();

        // create an appointment with no customer or patient
        Act appointment4 = newAppointment("2024-10-09 16:45", "2024-10-09 17:15", schedule)
                .appointmentType(schedulingFactory.createAppointmentType("Scan microchip"))
                .reason("Escaped pet")
                .build();

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule, getScheduleEvents(appointment1, appointment2, appointment3, appointment4));
        MultiScheduleGrid grid = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        MultiScheduleTableModel model = new MultiScheduleTableModel(grid, new LocalContext(),
                                                                    Mockito.mock(ScheduleColours.class));
        TableEx table = new TableEx(model, model.getColumnModel());
        table.validate();

        // time column
        checkCellText(table, 0, 0, "08:00"); // slot exists because first appointment is at 8:30 and slots are 1 hour
        checkCellText(table, 0, 1, "09:00");
        checkCellText(table, 0, 9, "17:00");

        // schedule column
        checkCellText(table, 1, 0, "Smith,J - Fido\nCheckup - Pending");
        checkCellText(table, 1, 3, "Bloggs,S\nCheckup - Pending");
        checkCellText(table, 1, 4, "Jones,F - Spot\nNo Reason - Pending");
        checkCellText(table, 1, 8, "No Customer\nEscaped pet - Pending");

        // time column
        checkCellText(table, 0, 0, "08:00");
    }

    /**
     * Creates an appointment.
     *
     * @param startTime the appointment start time
     * @param endTime   the appointment end time
     * @param schedule  the schedule
     * @param customer  the customer
     * @param patient   the patient
     * @return a new appointment
     */
    private Act createAppointment(String startTime, String endTime, Entity schedule, Party customer, Party patient) {
        return newAppointment(startTime, endTime, schedule)
                .customer(customer)
                .patient(patient)
                .appointmentType(schedulingFactory.createAppointmentType())
                .reason("Checkup")
                .build();
    }

    /**
     * Verifies the schedule and appointment at a cell matches that expected.
     *
     * @param model       the model
     * @param column      the column
     * @param row         the row
     * @param schedule    the expected schedule. May be {@code null}
     * @param appointment the expected appointment. May be {@code null}
     */
    private void checkEvent(MultiScheduleTableModel model, int column, int row, Entity schedule, Act appointment) {
        Cell cell = new Cell(column, row);
        PropertySet event = model.getEvent(cell);
        assertEquals(event, model.getEvent(column, row));
        if (schedule == null) {
            assertNull(model.getSchedule(cell));
            assertNull(model.getSchedule(column, row));
        } else {
            Schedule actual = model.getSchedule(column, row);
            assertNotNull(actual);
            assertEquals(schedule, actual.getSchedule());
            assertEquals(actual, model.getSchedule(cell));

            assertEquals(schedule, model.getScheduleEntity(cell));
            assertEquals(schedule, model.getScheduleEntity(column, row));
        }
        if (appointment == null) {
            assertNull(event);
        } else {
            assertNotNull(event);
            assertEquals(appointment.getObjectReference(), event.getReference(ScheduleEvent.ACT_REFERENCE));
            assertNotNull(schedule);
            assertEquals(schedule.getObjectReference(), event.getReference(ScheduleEvent.SCHEDULE_REFERENCE));
        }
    }

    /**
     * Verifies the text rendered for a table cell matches that expected.
     *
     * @param table    the table
     * @param column   the column
     * @param row      the row
     * @param expected the expected text
     */
    private void checkCellText(TableEx table, int column, int row, String expected) {
        Object content = table.getCellContent(column, row);
        assertTrue(content instanceof Label);
        String text = ((Label) content).getText();
        assertEquals(expected, text);
    }
}
