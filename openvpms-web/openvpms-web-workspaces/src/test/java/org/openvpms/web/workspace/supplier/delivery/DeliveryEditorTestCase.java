/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import nextapp.echo2.app.Table;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.supplier.OrderStatus;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.supplier.delivery.TestDeliveryItemVerifier;
import org.openvpms.archetype.test.builder.supplier.delivery.TestDeliveryVerifier;
import org.openvpms.archetype.test.builder.supplier.order.TestOrderBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link DeliveryEditor} class.
 *
 * @author Tim Anderson
 */
public class DeliveryEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Override
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        Party practice = practiceFactory.getPractice();
        context.setPractice(practice);
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link DeliveryEditor} for
     * <em>act.supplierDelivery</em> and <em>act.supplierReturn</em>instances.
     */
    @Test
    public void testFactory() {
        Party supplier = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));

        IMObjectEditor editor1 = factory.create(createDelivery(supplier, stockLocation), layoutContext);
        assertTrue(editor1 instanceof DeliveryEditor);

        IMObjectEditor editor2 = factory.create(createReturn(supplier, stockLocation), layoutContext);
        assertTrue(editor2 instanceof DeliveryEditor);
    }

    /**
     * Tests the {@link DeliveryEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party supplier = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        FinancialAct delivery = createDelivery(supplier, stockLocation);
        DeliveryEditor editor = createEditor(delivery);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof DeliveryEditor);
        assertEquals(delivery, newInstance.getObject());
    }

    /**
     * Verifies that when a delivery is created from an order, the order item details are copied.
     */
    @Test
    public void testCreateDeliveryFromOrder() {
        Party supplier = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMerchandise();
        TestOrderBuilder builder = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .status(OrderStatus.POSTED)
                .item().product(product1).reorderCode("1").reorderDescription("1 desc")
                .quantity(1).packageSize(10).packageUnits("BOX").unitPrice(1).add()
                .item().product(product2).reorderCode("2").packageUnits("EACH").reorderDescription("2 desc")
                .quantity(2).packageSize(1).unitPrice(2).add();
        FinancialAct order = builder.build();
        checkEquals(5, order.getTotal());

        // create a new delivery
        FinancialAct delivery = createDelivery(supplier, stockLocation);
        DeliveryEditor editor = createEditor(delivery);

        // create a delivery item for each order item
        List<FinancialAct> orderItems = builder.getItems();
        assertEquals(2, orderItems.size());
        for (FinancialAct orderItem : orderItems) {
            editor.createItem(orderItem);
        }
        assertTrue(SaveHelper.save(editor));

        // verify the delivery matches that expected
        delivery = get(delivery);
        TestDeliveryVerifier deliveryVerifier = new TestDeliveryVerifier(getArchetypeService());
        deliveryVerifier.initialise(order)
                .status(FinancialActStatus.IN_PROGRESS);
        deliveryVerifier.verify(delivery);

        // verify there is a delivery item for each of the order items
        List<FinancialAct> deliveryItems = getBean(delivery).getTargets("items", FinancialAct.class);
        assertEquals(2, deliveryItems.size());
        for (FinancialAct orderItem : orderItems) {
            TestDeliveryItemVerifier verifier = new TestDeliveryItemVerifier(getArchetypeService());
            verifier.initialise(orderItem);
            verifier.verify(deliveryItems);
        }
    }

    /**
     * Verifies that when a delivery is created from an order, the delivery is valid after selecting
     * a delivery item.
     * <p/>
     * This verifies the fix for OVPMS-2899.
     */
    @Test
    public void testValidAfterSelection() {
        Party supplier = supplierFactory.createSupplier();
        Party stockLocation = practiceFactory.createStockLocation();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMerchandise();
        TestOrderBuilder builder = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .status(OrderStatus.POSTED)
                .item().product(product1).reorderCode("1").reorderDescription("1 desc")
                .quantity(1).packageSize(10).packageUnits("BOX").unitPrice(1).add()
                .item().product(product2).reorderCode("2").packageUnits("EACH").reorderDescription("2 desc")
                .quantity(2).packageSize(1).unitPrice(2).add();
        builder.build();

        // create a new delivery
        FinancialAct delivery = createDelivery(supplier, stockLocation);
        DeliveryEditor editor = createEditor(delivery);

        // create a delivery item for each order item
        List<FinancialAct> orderItems = builder.getItems();
        assertEquals(2, orderItems.size());
        for (FinancialAct orderItem : orderItems) {
            editor.createItem(orderItem);
        }

        // simulate clicking on the first delivery item
        Table table = EchoTestHelper.findComponent(editor.getItems().getComponent(), Table.class);
        EchoTestHelper.fireSelection(table, 0);

        // verify the editor is valid and can be saved
        assertValid(editor);
        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Creates a new editor.
     *
     * @param delivery the delivery to edit
     * @return a new editor
     */
    private DeliveryEditor createEditor(FinancialAct delivery) {
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        DeliveryEditor editor = new DeliveryEditor(delivery, null, layoutContext);
        editor.getComponent();
        return editor;
    }

    /**
     * Creates a new unsaved delivery.
     *
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @return a new delivery
     */
    private FinancialAct createDelivery(Party supplier, Party stockLocation) {
        return supplierFactory.newDelivery()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .build(false);
    }

    /**
     * Creates a new unsaved return.
     *
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @return a new delivery
     */
    private FinancialAct createReturn(Party supplier, Party stockLocation) {
        return supplierFactory.newReturn()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .build(false);
    }
}
