/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.info;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the customer {@link InformationWorkspace}.
 *
 * @author Tim Anderson
 */
public class InformationWorkspaceTestCase extends AbstractAppTest {

    /**
     * Verifies the {@link InformationWorkspace#setObject(Party)} method updates the context.
     */
    @Test
    public void testSetObject() {
        Context context = new LocalContext();
        InformationWorkspace workspace = new TestWorkspace(context);
        Party customer1 = TestHelper.createCustomer();
        Party patient1 = TestHelper.createPatient(customer1);
        context.setCustomer(customer1);
        context.setPatient(patient1);

        Party customer2 = TestHelper.createCustomer();
        workspace.setObject(customer2);

        assertEquals(customer2, workspace.getObject());

        // verify the context has been updated
        assertEquals(customer2, context.getCustomer());
        assertNull(context.getPatient());
    }

    /**
     * Verifies that if an edit for an unsaved customer is cancelled, the workspace is cleared.
     */
    @Test
    public void testCancelEditForUnsavedObject() {
        Context context = new LocalContext();
        TestWorkspace workspace = new TestWorkspace(context);
        Party customer = TestHelper.createCustomer();
        workspace.setObject(customer);

        CRUDWindow<Party> window = workspace.getCRUDWindow();
        assertEquals(customer, window.getObject());

        // create a new object. This should launch an edit dialog
        window.create();
        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertNotNull(dialog);
        assertTrue(dialog.getEditor().getObject().isA(CustomerArchetypes.PERSON));

        // new object shouldn't be available in the workspace
        assertEquals(customer, window.getObject());
        assertEquals(customer, workspace.getObject());

        // cancel the edit and verify the customer has been cleared
        EchoTestHelper.fireDialogButton(dialog, EditDialog.CANCEL_ID);
        assertNull(window.getObject());
        assertNull(workspace.getObject());
    }

    /**
     * Verifies that if an edit for a saved customer is cancelled, the workspace reverts to the saved instance.
     */
    @Test
    public void testCancelEditForSavedObject() {
        Context context = new LocalContext();
        TestWorkspace workspace = new TestWorkspace(context);
        Party customer = TestHelper.createCustomer("J", "Bloggs", true);
        workspace.setObject(customer);

        CRUDWindow<Party> window = workspace.getCRUDWindow();

        // edit the object
        window.edit();
        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertNotNull(dialog);
        assertTrue(dialog.getEditor().getObject().isA(CustomerArchetypes.PERSON));

        // change the last name and save
        dialog.getEditor().getProperty("lastName").setValue("Smith");
        EchoTestHelper.fireDialogButton(dialog, EditDialog.APPLY_ID);

        // change the last name and cancel
        dialog.getEditor().getProperty("lastName").setValue("Jones");
        EchoTestHelper.fireDialogButton(dialog, EditDialog.CANCEL_ID);

        // verify the workspace has the expected object and name
        assertEquals(customer, workspace.getObject());
        assertEquals("Smith,J", workspace.getObject().getName());
    }

    /**
     * Test workspace to expose the CRUDWindow.
     */
    private static class TestWorkspace extends InformationWorkspace {
        public TestWorkspace(Context context) {
            super(context, Mockito.mock(Preferences.class));
        }

        @Override
        public CRUDWindow<Party> getCRUDWindow() {
            return super.getCRUDWindow();
        }
    }
}