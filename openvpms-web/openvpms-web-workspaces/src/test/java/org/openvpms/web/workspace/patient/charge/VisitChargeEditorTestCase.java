/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.charge;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActItemEditor;
import org.openvpms.web.workspace.customer.charge.EditorQueue;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link VisitChargeEditor}.
 *
 * @author Tim Anderson
 */
public class VisitChargeEditorTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The author.
     */
    private User author;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The layout context.
     */
    private LayoutContext layoutContext;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();

        Party customer = TestHelper.createCustomer();
        patient = TestHelper.createPatient(customer);
        author = TestHelper.createUser();
        clinician = TestHelper.createClinician();
        Party location = TestHelper.createLocation();

        layoutContext = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        layoutContext.setEdit(true);
        layoutContext.getContext().setPractice(getPractice());
        layoutContext.getContext().setCustomer(customer);
        layoutContext.getContext().setPatient(patient);
        layoutContext.getContext().setUser(author);
        layoutContext.getContext().setClinician(clinician);
        layoutContext.getContext().setLocation(location);

        new AuthenticationContextImpl().setUser(author);  // makes author the logged-in user
    }

    /**
     * Tests the {@link VisitChargeEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        FinancialAct charge = create(CustomerAccountArchetypes.INVOICE, FinancialAct.class);
        Act event = create(PatientArchetypes.CLINICAL_EVENT, Act.class);
        VisitChargeEditor editor = new VisitChargeEditor(charge, event, layoutContext);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof VisitChargeEditor);
        assertEquals(charge, newInstance.getObject());
    }

    /**
     * Verifies that changing the product from one with document templates to one without deletes the documents
     * on the invoice item.
     * <p>
     * TODO - share common tests with DefaultCustomerChargeActEditorTestCase
     */
    @Test
    public void testDeleteDocumentOnProductChange() {
        Act event = PatientTestHelper.createEvent(patient);
        save(event);

        // product with document templates
        Product product1 = productFactory.createService();
        Entity template1 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);

        // product with no templates
        Product product2 = productFactory.createService();

        // product with single template
        Product product3 = productFactory.createService();
        Entity template3 = addDocumentTemplate(product3, PatientArchetypes.DOCUMENT_LETTER);

        FinancialAct charge = create(CustomerAccountArchetypes.INVOICE, FinancialAct.class);

        VisitChargeEditor editor = new VisitChargeEditor(charge, event, layoutContext, false);
        EditorQueue queue = editor.getItems().getEditorQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        setItem(editor, itemEditor, patient, product1, BigDecimal.ONE, queue);

        save(editor);

        // verify the item has 2 documents
        FinancialAct item = itemEditor.getObject();
        IMObjectBean bean = getBean(item);
        assertEquals(2, bean.getTargets("documents").size());

        event = get(event);

        DocumentAct document1 = checkDocument(item, patient, product1, template1, author, clinician, false);
        DocumentAct document2 = checkDocument(item, patient, product1, template2, author, clinician, true);
        checkEventRelationship(event, document1);
        checkEventRelationship(event, document2);

        // change the product to one without document templates, and verify the documents are removed
        itemEditor.setProduct(product2);

        save(editor);

        assertTrue(bean.getTargets("documents").isEmpty());
        assertNull(get(document1));
        assertNull(get(document2));
        assertNull(get(document2.getDocument())); // content should also be deleted

        // change to another document, this time with a single template
        itemEditor.setProduct(product3);
        save(editor);

        assertEquals(1, bean.getTargets("documents").size());
        checkDocument(item, patient, product3, template3, author, clinician, true);
    }

}
