/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import nextapp.echo2.app.ApplicationInstance;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.statement.AbstractStatementTest;
import org.openvpms.archetype.rules.finance.statement.Statement;
import org.openvpms.archetype.rules.finance.statement.StatementProcessor;
import org.openvpms.archetype.rules.finance.statement.StatementRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.macro.Macros;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.app.PracticeMailContext;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.mail.DefaultMailer;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.Mailer;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.workspace.OpenVPMSApp;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;


/**
 * Tests the {@link StatementEmailProcessor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class StatementEmailProcessorTestCase extends AbstractStatementTest {

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules customerAccountRules;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The file name formatter.
     */
    @Autowired
    private FileNameFormatter formatter;

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The report factory.
     */
    @Autowired
    private ReportFactory reportFactory;

    /**
     * The app.
     */
    @Autowired
    private OpenVPMSApp app;

    /**
     * Statement document template.
     */
    private Entity statementTemplate;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        app.setApplicationContext(applicationContext);
        ApplicationInstance.setActive(app);
        app.doInit();

        Entity settings = practiceFactory.newMailServer()
                .name("Default Mail Server")
                .host("localhost")
                .build();
        practiceFactory.updatePractice(getPractice())
                .addEmail("foo@bar.com")
                .mailServer(settings)
                .build(false);

        Document handout1 = documentFactory.newDocument()
                .name("Patient Handout.pdf")
                .content("dummy pdf content")
                .mimeType(DocFormats.PDF_TYPE)
                .build();
        Entity attachment1 = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(handout1)
                .build();

        Document handout2 = documentFactory.newDocument()
                .name("Customer Handout.pdf")
                .content("dummy pdf content")
                .mimeType(DocFormats.PDF_TYPE)
                .build();
        Entity attachment2 = documentFactory.newTemplate()
                .type(CustomerArchetypes.DOCUMENT_FORM)
                .document(handout2)
                .build();

        Entity emailTemplate = documentFactory.newEmailTemplate()
                .subject("Statement Email")
                .content("Statement Text")
                .addAttachments(attachment1, attachment2)
                .build();

        statementTemplate = documentFactory.newTemplate()
                .name("Statement")
                .type(CustomerAccountArchetypes.OPENING_BALANCE)
                .blankDocument()
                .emailTemplate(emailTemplate)
                .build();
    }

    /**
     * Verifies that a statement email is generated if the customer has a billing email address.
     */
    @Test
    public void testEmail() {
        JavaMailSender sender = mock(JavaMailSender.class);
        MimeMessage mimeMessage = mock(MimeMessage.class);
        when(sender.createMimeMessage()).thenReturn(mimeMessage);

        Date statementDate = getDate("2007-01-01");

        Party practice = getPractice();
        Party customer = getCustomer();
        addCustomerEmail(customer);

        IArchetypeService archetypeService = getArchetypeService();
        StatementRules rules = new StatementRules(practice, getArchetypeService(), customerAccountRules);
        assertFalse(rules.hasStatement(customer, statementDate));
        List<Act> acts = getActs(customer, statementDate);
        assertEquals(0, acts.size());

        Money amount = new Money(100);
        List<FinancialAct> invoice1 = createChargesInvoice(amount, getDatetime("2007-01-01 10:00:00"));
        save(invoice1);

        acts = getActs(customer, statementDate);
        assertEquals(1, acts.size());
        checkAct(acts.get(0), invoice1.get(0), POSTED);

        List<Statement> statements = new ArrayList<>();
        StatementProcessor processor = new StatementProcessor(statementDate, practice, archetypeService,
                                                              customerAccountRules);
        processor.addListener(statements::add);
        processor.process(customer);
        assertEquals(1, statements.size());
        Macros macros = Mockito.mock(Macros.class);
        DocumentConverter converter = Mockito.mock(DocumentConverter.class);
        EmailTemplateEvaluator evaluator = new EmailTemplateEvaluator(archetypeService, getLookupService(),
                                                                      macros, reportFactory, converter);
        ReporterFactory reporterFactory = new ReporterFactory(reportFactory, formatter, archetypeService,
                                                              getLookupService());

        MailerFactory mailerFactory = Mockito.mock(MailerFactory.class);
        sender = Mockito.mock(JavaMailSender.class);
        mimeMessage = Mockito.mock(MimeMessage.class);
        Mockito.when(sender.createMimeMessage()).thenReturn(mimeMessage);

        LocalContext context = new LocalContext();
        Party location = TestHelper.createLocation();
        context.setPractice(practice);
        context.setLocation(location);
        MailContext mailContext = new PracticeMailContext(context);
        List<String> attachments = new ArrayList<>();
        Mailer mailer = new DefaultMailer(mailContext, sender, documentHandlers) {
            @Override
            public void addAttachment(Document document) {
                super.addAttachment(document);
                attachments.add(document.getName());
            }
        };
        Mockito.when(mailerFactory.create(Mockito.any(), Mockito.any())).thenReturn(mailer);

        CommunicationLogger logger = Mockito.mock(CommunicationLogger.class);

        StatementEmailProcessor emailProcessor = new StatementEmailProcessor(
                mailerFactory, evaluator, reporterFactory, practice, location, practiceRules, archetypeService,
                logger, Mockito.mock(MailPasswordResolver.class)) {
            @Override
            protected Entity getStatementTemplate(IArchetypeService service) {
                return statementTemplate;
            }
        };
        emailProcessor.process(statements.get(0));
        Mockito.verify(sender, times(1)).send(mimeMessage);

        // verify the statement and handouts were added as attachments
        assertEquals(3, attachments.size());
        assertTrue(attachments.contains("Statement.pdf"));
        assertTrue(attachments.contains("Customer Handout.pdf"));
        assertTrue(attachments.contains("Patient Handout.pdf"));

    }

    /**
     * Helper to create an email contact.
     *
     * @param customer the customer
     */
    private void addCustomerEmail(Party customer) {
        customer.getContacts().clear();
        customerFactory.updateCustomer(customer)
                .addEmail("foo@bar.com", "BILLING")
                .build();
    }

}
