/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.order;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.order.OrderRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCreditVerifier;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceVerifier;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditorTestHelper;
import org.openvpms.web.echo.dialog.OptionDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditDialog;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;
import static org.openvpms.web.test.EchoTestHelper.getWindowPane;

/**
 * Tests the {@link OrderCharger}.
 *
 * @author Tim Anderson
 */
public class OrderChargerTestCase extends AbstractAppTest {

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules accountRules;

    /**
     * The order rules.
     */
    @Autowired
    private OrderRules orderRules;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The order charger factory.
     */
    private OrderChargerFactory factory;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        assertNotNull(applicationContext);
        IArchetypeService service = applicationContext.getBean("archetypeService", IArchetypeService.class);
        IArchetypeRuleService ruleService = (IArchetypeRuleService) getArchetypeService();
        factory = new OrderChargerFactory(orderRules, accountRules, service, ruleService);
    }

    /**
     * Verifies that when a pharmacy order is invoiced for a customer different to the context customer,
     * it is assigned to the correct customer and patient.
     */
    @Test
    public void testInvoicePharmacyOrderForDifferentCustomer() {
        Party customer1 = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        Party customer2 = customerFactory.createCustomer();
        Party patient2 = patientFactory.createPatient(customer2);
        Party practice = practiceFactory.getPractice();
        Party location = practiceFactory.createLocation();

        // set up the context with customer1, patient1
        Context context = new LocalContext();
        context.setPractice(practice);
        context.setLocation(location);
        context.setCustomer(customer1);
        context.setPatient(patient1);

        // create an order for customer2, patient2
        Product product = productFactory.newMerchandise()
                .unitPrice(1)
                .build();
        FinancialAct order = customerFactory.newPharmacyOrder()
                .customer(customer2)
                .item().patient(patient2).product(product).quantity(1).add()
                .build();

        // invoice the order
        OrderCharger charger = factory.create(customer2, context, new HelpContext("foo", null));
        MutableBoolean completed = new MutableBoolean();
        charger.charge(order, completed::setTrue);

        // click the OK button on the edit dialog if it is valid
        CustomerChargeActEditDialog dialog = getWindowPane(CustomerChargeActEditDialog.class);
        EditorTestHelper.assertValid(dialog.getEditor());
        fireDialogButton(dialog, CustomerChargeActEditDialog.OK_ID);

        // verify the listener was called
        assertTrue(completed.isTrue());

        // verify the invoice is charged to customer2, patient2
        FinancialAct invoice = get(dialog.getEditor().getObject());
        TestInvoiceVerifier verifier = new TestInvoiceVerifier(getArchetypeService())
                .customer(customer2)
                .amount(1)
                .status(FinancialActStatus.IN_PROGRESS)
                .item()
                .patient(patient2)
                .product(product)
                .quantity(1)
                .unitPrice(1)
                .receivedQuantity(1)
                .total(1)
                .status(null)
                .add();
        verifier.verify(invoice);

        // verify the order is now POSTED
        assertEquals(FinancialActStatus.POSTED, get(order).getStatus());
    }

    /**
     * Verifies that when a pharmacy return is credited for a customer different to the context customer,
     * it is assigned to the correct customer and patient.
     */
    @Test
    public void testCreditPharmacyReturnForDifferentCustomer() {
        Party customer1 = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        Party customer2 = customerFactory.createCustomer();
        Party patient2 = patientFactory.createPatient(customer2);
        Party practice = practiceFactory.getPractice();
        Party location = practiceFactory.createLocation();

        // set up the context with customer1, patient1
        Context context = new LocalContext();
        context.setPractice(practice);
        context.setLocation(location);
        context.setCustomer(customer1);
        context.setPatient(patient1);

        // create a return for customer2, patient2
        Product product = productFactory.newMerchandise()
                .unitPrice(1)
                .build();
        FinancialAct pharmacyReturn = customerFactory.newPharmacyReturn()
                .customer(customer2)
                .item().patient(patient2).product(product).quantity(1).add()
                .build();

        // credit the return
        OrderCharger charger = factory.create(customer2, context, new HelpContext("foo", null));
        MutableBoolean completed = new MutableBoolean();
        charger.charge(pharmacyReturn, completed::setTrue);
        OptionDialog optionDialog = getWindowPane(OptionDialog.class);
        optionDialog.setSelected(0);
        fireDialogButton(optionDialog, OptionDialog.OK_ID);

        // click the OK button on the edit dialog if it is valid
        CustomerChargeActEditDialog dialog = getWindowPane(CustomerChargeActEditDialog.class);
        EditorTestHelper.assertValid(dialog.getEditor());
        fireDialogButton(dialog, CustomerChargeActEditDialog.OK_ID);

        // verify the listener was called
        assertTrue(completed.isTrue());

        // verify the credit is charged to customer2, patient2
        FinancialAct credit = get(dialog.getEditor().getObject());
        TestCreditVerifier verifier = new TestCreditVerifier(getArchetypeService())
                .customer(customer2)
                .amount(1)
                .status(FinancialActStatus.IN_PROGRESS)
                .item()
                .patient(patient2)
                .product(product)
                .quantity(1)
                .unitPrice(1)
                .total(1)
                .status(null)
                .add();
        verifier.verify(credit);

        // verify the return is now POSTED
        assertEquals(FinancialActStatus.POSTED, get(pharmacyReturn).getStatus());
    }
}