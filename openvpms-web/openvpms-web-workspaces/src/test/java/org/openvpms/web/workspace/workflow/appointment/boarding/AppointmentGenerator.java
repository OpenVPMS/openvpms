/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.boarding;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.stringparsers.IntegerStringParser;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Tool to generate appointments for all schedules linked to a schedule view.
 *
 * @author Tim Anderson
 */
public class AppointmentGenerator {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The appointment rules.
     */
    private final AppointmentRules appointmentRules;

    /**
     * The available clinicians.
     */
    private final List<User> clinicians = new ArrayList<>();

    /**
     * The patient iterator.
     */
    private IMObjectQueryIterator<Party> patientIterator;

    /**
     * Iterator over the clinicians.
     */
    private Iterator<User> clinicianIterator;

    /**
     * The default application context.
     */
    private static final String APPLICATION_CONTEXT = "applicationContext.xml";

    /**
     * Constructs an {@link AppointmentGenerator}.
     *
     * @param service          the archetype service
     * @param patientRules     the patient rules
     * @param appointmentRules the appointment rules
     * @param userRules        the user rules
     */
    public AppointmentGenerator(IArchetypeService service, PatientRules patientRules, AppointmentRules appointmentRules,
                                UserRules userRules) {
        this.service = service;
        this.patientRules = patientRules;
        this.appointmentRules = appointmentRules;
        ArchetypeQuery query = new ArchetypeQuery(UserArchetypes.USER);
        IMObjectQueryIterator<User> iterator = new IMObjectQueryIterator<>(service, query);
        while (iterator.hasNext()) {
            User user = iterator.next();
            if (userRules.isClinician(user)) {
                clinicians.add(user);
            }
        }
    }

    /**
     * Generates appointments for each schedule in a schedule view.
     *
     * @param name the view name
     * @param days the number of days to generate appointments for
     */
    public void generate(String name, int days) {
        ArchetypeQuery query = new ArchetypeQuery(ScheduleArchetypes.SCHEDULE_VIEW);
        query.add(Constraints.eq("name", name));
        query.add(Constraints.sort("id"));
        query.setMaxResults(1);
        IMObjectQueryIterator<Entity> iterator = new IMObjectQueryIterator<>(service, query);
        Date startDate = DateRules.getToday();
        if (iterator.hasNext()) {
            List<Act> appointments = new ArrayList<>();
            Entity view = iterator.next();
            IMObjectBean scheduleBean = service.getBean(view);
            List<Entity> schedules = scheduleBean.getTargets("schedules", Entity.class, Policies.orderBySequence());
            for (Entity schedule : schedules) {
                Date date = startDate;
                Entity appointmentType = getAppointmentType(schedule);
                if (appointmentType != null) {
                    System.out.println("Generating appointments for schedule: " + schedule.getName());
                    int slotSize = appointmentRules.getSlotSize(schedule);
                    for (int i = 0; i < days; ++i) {
                        System.out.println("Generating appointments for date: "
                                           + DateFormatter.formatDate(date, false));
                        Date next = DateRules.getNextDate(date);
                        Date start = date;
                        boolean done = false;
                        while (!done) {
                            Date end = DateRules.getDate(start, slotSize, DateUnits.MINUTES);
                            if (DateRules.dateEquals(date, end) || next.equals(end)) {
                                Patient patient = nextPatient();
                                Act appointment = ScheduleTestHelper.createAppointment(
                                        start, end, schedule, appointmentType, patient.customer, patient.patient,
                                        nextClinician());
                                appointment.setStatus(WorkflowStatus.PENDING);
                                appointments.add(appointment);
                                if (appointments.size() > 100) {
                                    service.save(appointments);
                                    appointments.clear();
                                }
                                start = end;
                            } else {
                                done = true;
                            }
                        }
                        date = next;
                    }
                }
                if (!appointments.isEmpty()) {
                    service.save(appointments);
                }
            }
        } else {
            System.out.println("No view named " + name);
        }
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     * @throws Exception for any error
     */
    public static void main(String[] args) throws Exception {
        JSAP parser = createParser();
        JSAPResult config = parser.parse(args);
        if (!config.success()) {
            displayUsage(parser);
        } else {
            String contextPath = config.getString("context");
            ApplicationContext context;
            if (!new File(contextPath).exists()) {
                context = new ClassPathXmlApplicationContext(contextPath);
            } else {
                context = new FileSystemXmlApplicationContext(contextPath);
            }

            AppointmentGenerator generator = new AppointmentGenerator(context.getBean(IArchetypeService.class),
                                                                      context.getBean(PatientRules.class),
                                                                      context.getBean(AppointmentRules.class),
                                                                      context.getBean(UserRules.class));
            generator.generate(config.getString("view"), config.getInt("days"));
        }
    }

    /**
     * Returns an appointment type for a schedule.
     *
     * @param schedule the schedule
     * @return the appointment type
     */
    private Entity getAppointmentType(Entity schedule) {
        IMObjectBean bean = service.getBean(schedule);
        return bean.getTarget("appointmentTypes", Entity.class);
    }

    /**
     * Returns the next patient.
     *
     * @return the next patient
     */
    private Patient nextPatient() {
        boolean all = false;
        if (patientIterator == null || !patientIterator.hasNext()) {
            all = true;
            patientIterator = createPatientIterator();
        }
        Patient result = getNext(patientIterator);
        if (result == null && !all) {
            patientIterator = createPatientIterator();
            result = getNext(patientIterator);
        }
        if (result == null) {
            throw new IllegalStateException("No patients found");
        }

        return result;
    }

    /**
     * Returns the next clinician.
     *
     * @return the next clinician. May be {@code null}
     */
    private User nextClinician() {
        if (clinicianIterator == null || !clinicianIterator.hasNext()) {
            clinicianIterator = clinicians.iterator();
        }
        if (clinicianIterator.hasNext()) {
            return clinicianIterator.next();
        }
        return null;
    }

    /**
     * Returns the next patient.
     *
     * @param patients the patient iterator
     * @return the next patient
     */
    private Patient getNext(IMObjectQueryIterator<Party> patients) {
        Patient result = null;
        while (patients.hasNext()) {
            Party patient = patients.next();
            Party customer = patientRules.getOwner(patient);
            if (customer != null && customer.isActive()) {
                result = new Patient(patient, customer);
                break;
            }
        }
        return result;
    }

    /**
     * Creates a new patient iterator.
     *
     * @return the patient iterator
     */
    private IMObjectQueryIterator<Party> createPatientIterator() {
        ArchetypeQuery query = new ArchetypeQuery(PatientArchetypes.PATIENT);
        query.add(Constraints.sort("id"));
        return new IMObjectQueryIterator<>(service, query);
    }

    /**
     * Creates a new command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("context").setShortFlag('c')
                                         .setLongFlag("context")
                                         .setDefault(APPLICATION_CONTEXT)
                                         .setHelp("Application context path"));
        parser.registerParameter(new FlaggedOption("view").setShortFlag('v')
                                         .setLongFlag("view").setHelp("The schedule view."));
        parser.registerParameter(new FlaggedOption("days").setShortFlag('d')
                                         .setLongFlag("days")
                                         .setStringParser(IntegerStringParser.getParser())
                                         .setHelp("The no. of days to generate appointments for."));
        return parser;
    }

    /**
     * Prints usage information.
     */
    private static void displayUsage(JSAP parser) {
        System.err.println();
        System.err
                .println("Usage: java " + AppointmentGenerator.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.exit(1);
    }

    /**
     * Associates a customer and patient.
     */
    private static class Patient {

        private final Party patient;

        private final Party customer;

        public Patient(Party patient, Party customer) {
            this.patient = patient;
            this.customer = customer;
        }
    }

}
