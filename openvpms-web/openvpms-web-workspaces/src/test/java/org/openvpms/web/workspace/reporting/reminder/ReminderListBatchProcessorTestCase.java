/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderItemQueryFactory;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes.LIST_REMINDER;

/**
 * Tests the {@link ReminderListBatchProcessor}.
 *
 * @author Tim Anderson
 */
public class ReminderListBatchProcessorTestCase extends AbstractReminderBatchProcessorTest {

    /**
     * The test practice.
     */
    private Party practice;

    /**
     * The test location.
     */
    private Party location;

    /**
     * The reminder types.
     */
    private ReminderTypes reminderTypes;

    /**
     * The reminder rules.
     */
    @Autowired
    private ReminderRules reminderRules;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The reminder configuration.
     */
    private ReminderConfiguration config;

    /**
     * Test reminder type 1.
     */
    private Entity reminderType1;

    /**
     * Test reminder type 2.
     */
    private Entity reminderType2;

    /**
     * Initial due date.
     */
    private Date due;

    /**
     * Reminder 1 for patient A.
     */
    private Act reminderA1;

    /**
     * Reminder item 1 for patient A.
     */
    private Act itemA1;

    /**
     * Reminder 2 for patient A.
     */
    private Act reminderA2;

    /**
     * Reminder item 2 for patient A.
     */
    private Act itemA2;

    /**
     * Reminder reminder 1 for patient B
     */
    private Act reminderB1;

    /**
     * Reminder item 1 for patient B
     */
    private Act itemB1;

    /**
     * Test patient C.
     */
    private Party patientC;

    /**
     * Reminder for patient C
     */
    private Act reminderC1;

    /**
     * Reminder item 1 for patient C
     */
    private Act itemC1;

    /**
     * The reminder item query source.
     */
    private ReminderItemQuerySource source;

    /**
     * The communication logger.
     */
    private TestCommunicationLogger communicationLogger;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        practice = practiceFactory.getPractice();
        location = practiceFactory.createLocation();
        ArchetypeService service = getArchetypeService();
        reminderTypes = new ReminderTypes(service);
        config = new ReminderConfiguration(create(ReminderArchetypes.CONFIGURATION), service);

        Party customer1 = customerFactory.newCustomer()
                .title("MS")
                .firstName("J")
                .lastName("Smith")
                .addAddress("103 Stafford Drive", "SALE", "VIC", "3085")
                .build();
        Party patientA = patientFactory.createPatient("Fido", customer1);
        Party patientB = patientFactory.createPatient("Spot", customer1);
        Party customer2 = customerFactory.newCustomer()
                .title("MR")
                .firstName("K")
                .lastName("Aardvark")
                .addAddress("91 Smith Rd", "KONGWAK", "VIC", "3086")
                .build();
        patientC = patientFactory.createPatient("Fluffy", customer2);

        reminderType1 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .newCount().count(0).interval(0, DateUnits.WEEKS).add()
                .newCount().count(1).interval(1, DateUnits.WEEKS).add()
                .build();

        reminderType2 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .newCount().count(0).interval(0, DateUnits.WEEKS).add()
                .newCount().count(1).interval(1, DateUnits.MONTHS).add()
                .build();

        Date send = DateRules.getToday();
        due = DateRules.getTomorrow();

        // set up reminders for patient A
        itemA1 = reminderFactory.createListReminder(send, due);
        itemA2 = reminderFactory.createListReminder(send, due);
        reminderA1 = reminderFactory.createReminder(due, patientA, reminderType1, itemA1);
        reminderA2 = reminderFactory.createReminder(due, patientA, reminderType2, itemA2);

        // set up reminders for patient B
        itemB1 = reminderFactory.createListReminder(send, due);
        reminderB1 = reminderFactory.createReminder(due, patientB, reminderType1, itemB1);

        // set up reminders for patient C
        itemC1 = reminderFactory.createListReminder(send, due);
        reminderC1 = reminderFactory.createReminder(due, patientC, reminderType2, itemC1);

        // limit the query to the items above
        ReminderItemQueryFactory factory = new TestReminderItemQueryFactory(
                LIST_REMINDER, itemA1, itemA2, itemB1, itemC1);
        source = new ReminderItemQuerySource(factory, reminderTypes, config);
        communicationLogger = new TestCommunicationLogger(service);
    }

    /**
     * Tests listing reminders.
     */
    @Test
    public void testList() {
        TestReminderListProcessor processor = createProcessor();
        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkCompleted(itemA1, itemA2, itemB1, itemC1);
        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, oneWeek);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<Act> printed = processor.getPrinted();
        assertEquals(4, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, printed.get(0));
        assertEquals(reminderA1, printed.get(1));
        assertEquals(reminderA2, printed.get(2));
        assertEquals(reminderB1, printed.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Tests listing reminders where one item is cancelled.
     */
    @Test
    public void testCancel() {
        patientFactory.updatePatient(patientC)
                .active(false)
                .build();
        // inactive patient. Reminder item should be cancelled
        TestReminderListProcessor processor = createProcessor();
        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkCompleted(itemA1, itemA2, itemB1);
        checkStatus(ReminderItemStatus.CANCELLED, "Patient is inactive", itemC1);
        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, oneWeek);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);
        // itemC1 cancelled, but reminderC1 still updated. Would be cancelled by reminder queue job

        List<Act> printed = processor.getPrinted();
        assertEquals(3, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderA1, printed.get(0));
        assertEquals(reminderA2, printed.get(1));
        assertEquals(reminderB1, printed.get(2));

        assertEquals(3, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(1, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(1, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(3, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if printing fails, reminder items have their status set to ERROR and none of the reminders are
     * updated.
     */
    @Test
    public void testPrintFailure() {
        TestReminderListProcessor processor = createProcessor();
        processor.setPrintAction(() -> processor.getListener().failed(new RuntimeException("Printer error")));

        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();
        checkStatus(ReminderItemStatus.ERROR, "Printer error", itemA1, itemA2, itemB1, itemC1);
        checkReminder(reminderA1, 0, due);
        checkReminder(reminderA2, 0, due);
        checkReminder(reminderB1, 0, due);
        checkReminder(reminderC1, 0, due);

        assertEquals(0, statistics.getCount());
        assertEquals(4, statistics.getErrors());
        assertEquals(0, statistics.getCancelled());
        assertEquals(0, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(0, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(0, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if printing is cancelled, no reminder is updated.
     */
    @Test
    public void testPrintCancellation() {
        TestReminderListProcessor processor = createProcessor();
        processor.setPrintAction(() -> processor.getListener().cancelled());

        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();
        checkStatus(ReminderItemStatus.PENDING, null, itemA1, itemA2, itemB1, itemC1);
        checkReminder(reminderA1, 0, due);
        checkReminder(reminderA2, 0, due);
        checkReminder(reminderB1, 0, due);
        checkReminder(reminderC1, 0, due);

        assertEquals(0, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(0, statistics.getCancelled());
        assertEquals(0, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(0, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(0, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a parent reminder is changed after its item is printed, but before it is updated:
     * <u>
     * <li>processing completes successfully</li>
     * <li>it is logged</li>
     * <li>the item is flagged as an ERROR, as the parent reminder count hasn't updated</li>
     * </u>
     */
    @Test
    public void testChangeReminderBeforeUpdate() {
        // set up a processor that simulates changing of the item by another user, before it can be updated
        TestReminderListProcessor processor = createProcessor();
        processor.setBeforeUpdateAction(() -> {
            reminderA1.setDescription("some description, not normally editable, but will force object to be saved");
            save(reminderA1);
        });
        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkStatus(ReminderItemStatus.ERROR,
                    "Reminder was sent, but couldn't be updated. This may lead to a duplicate reminder. " +
                    "Failure reason: Reminder has been changed by another user", itemA1);
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<Act> printed = processor.getPrinted();
        assertEquals(4, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, printed.get(0));
        assertEquals(reminderA1, printed.get(1));
        assertEquals(reminderA2, printed.get(2));
        assertEquals(reminderB1, printed.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if an item is changed after it is printed, but before it is updated:
     * <u>
     * <li>processing completes successfully</li>
     * <li>it is logged</li>
     * <li>it is flagged as an ERROR, as the parent reminder count hasn't updated</li>
     * </u>
     */
    @Test
    public void testChangeItemBeforeUpdate() {
        // set up a processor that simulates changing of the item by another user, before it can be updated
        TestReminderListProcessor processor = createProcessor();
        processor.setBeforeUpdateAction(() -> {
            itemA1.setDescription("some description, not normally editable, but will force object to be saved");
            save(itemA1);
        });
        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkStatus(ReminderItemStatus.ERROR,
                    "Reminder was sent, but couldn't be updated. This may lead to a duplicate reminder. " +
                    "Failure reason: Patient List Reminder has been changed by another user", itemA1);
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<Act> printed = processor.getPrinted();
        assertEquals(4, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, printed.get(0));
        assertEquals(reminderA1, printed.get(1));
        assertEquals(reminderA2, printed.get(2));
        assertEquals(reminderB1, printed.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a reminder is deleted after printing, but before it is updated, processing completes
     * successfully, and it is still logged.
     */
    @Test
    public void testDeleteReminderBeforeUpdate() {
        // set up a processor that simulates deletion of the reminder by another user, before it can be updated
        TestReminderListProcessor processor = createProcessor();
        processor.setBeforeUpdateAction(() -> remove(reminderA1));

        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        assertNull(get(reminderA1));
        assertNull(get(itemA1));           // deleted
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<Act> printed = processor.getPrinted();
        assertEquals(4, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, printed.get(0));
        assertEquals(reminderA1, printed.get(1));
        assertEquals(reminderA2, printed.get(2));
        assertEquals(reminderB1, printed.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a reminder item is deleted after printing, but before it is updated, processing completes
     * successfully, and it is still logged.
     */
    @Test
    public void testDeleteItemBeforeUpdate() {
        // set up a processor that simulates deletion of the item by another user, before it can be updated
        TestReminderListProcessor processor = createProcessor();
        processor.setBeforeUpdateAction(() -> remove(itemA1));

        ReminderListBatchProcessor batchProcessor = new ReminderListBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        assertNull(get(itemA1));           // deleted
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<Act> printed = processor.getPrinted();
        assertEquals(4, printed.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, printed.get(0));
        assertEquals(reminderA1, printed.get(1));
        assertEquals(reminderA2, printed.get(2));
        assertEquals(reminderB1, printed.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, LIST_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, LIST_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Creates a new {@link TestReminderListProcessor}.
     *
     * @return the processor
     */
    private TestReminderListProcessor createProcessor() {
        return new TestReminderListProcessor(reminderTypes, reminderRules, patientRules,
                                             location, practice, getArchetypeService(),
                                             config, communicationLogger, actionFactory);
    }

    private static class TestReminderListProcessor extends ReminderListProcessor {
        private final List<Act> printed = new ArrayList<>();

        private Runnable printAction;

        private Runnable beforeUpdate;

        public TestReminderListProcessor(ReminderTypes reminderTypes, ReminderRules reminderRules,
                                         PatientRules patientRules, Party location, Party practice,
                                         ArchetypeService service, ReminderConfiguration config,
                                         CommunicationLogger communicationLogger, ActionFactory actionFactory) {
            super(reminderTypes, reminderRules, patientRules, location, practice, service, config,
                  Mockito.mock(IMPrinterFactory.class), communicationLogger, actionFactory,
                  new HelpContext("foo", null));
        }

        /**
         * Sets an action to run instead of printing.
         *
         * @param action the new print behaviour
         */
        public void setPrintAction(Runnable action) {
            this.printAction = action;
        }

        /**
         * Sets an action to run after printing, but before updating reminders.
         *
         * @param action the action to run prior to updating reminders
         */
        public void setBeforeUpdateAction(Runnable action) {
            this.beforeUpdate = action;
        }

        public List<Act> getPrinted() {
            return printed;
        }

        @Override
        protected void print(List<Act> reminders) {
            if (printAction != null) {
                printAction.run();
            } else {
                this.printed.addAll(reminders);
                if (beforeUpdate != null) {
                    beforeUpdate.run();
                }
                onPrinted();
            }
        }

        protected void onPrinted() {
            getListener().printed(null);  // simulate printing completion
        }
    }

}
