/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PrinterUseManager}.
 *
 * @author Tim Anderson
 */
public class PrinterUseManagerTestCase extends ArchetypeServiceTest {

    /**
     * The practice service.
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The location rules.
     */
    @Autowired
    private LocationRules locationRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link PrinterUseManager#replace(PrinterReference, PrinterReference)} method.
     */
    @Test
    public void testChange() {
        PrinterReference printerA = PrinterReference.fromString("printerA");
        PrinterReference printerB = PrinterReference.fromString("printerB");
        PrinterReference printerC = PrinterReference.fromString("printerC");

        Party location1 = practiceFactory.newLocation()
                .defaultPrinter(printerB)
                .printers(printerA, printerB, printerC)
                .build();
        Party location2 = practiceFactory.newLocation()
                .defaultPrinter(printerB)
                .printers(printerB)
                .build();
        Entity template1 = documentFactory.newTemplate()
                .printer()
                .printer(printerA)
                .location(location1)
                .add()
                .build();
        Entity template2 = documentFactory.newTemplate()
                .printer()
                .printer(printerB)
                .location(location2)
                .add()
                .build();

        TestPrinterUseManager manager = new TestPrinterUseManager(Arrays.asList(location1, location2),
                                                                  Arrays.asList(template1, template2));
        assertFalse(manager.hasPrinters());
        manager.refresh();
        checkPrinters(manager, printerA, printerB, printerC);

        checkUse(printerA, manager, location1, template1);
        checkUse(printerB, manager, location1, location2, template2);
        checkUse(printerC, manager, location1);
        checkLocation(location1, printerB, printerA, printerB, printerC);
        checkLocation(location2, printerB, printerB);

        manager.replace(printerB, printerA);
        manager.refresh();
        checkPrinters(manager, printerA, printerC);
        checkUse(printerA, manager, location1, location2, template1, template2);
        checkUse(printerB, manager);
        checkUse(printerC, manager, location1);

        checkLocation(location1, printerA, printerA, printerC);
        checkLocation(location2, printerA, printerA);
    }

    /**
     * Tests the {@link PrinterUseManager#remove(PrinterReference)} method.
     */
    @Test
    public void testRemove() {
        PrinterReference printerA = PrinterReference.fromString("printerA");
        PrinterReference printerB = PrinterReference.fromString("printerB");

        Party location1 = practiceFactory.newLocation()
                .defaultPrinter(printerA)
                .printers(printerA)
                .build();
        Party location2 = practiceFactory.newLocation()
                .defaultPrinter(printerB)
                .printers(printerB)
                .build();
        Entity template1 = documentFactory.newTemplate()
                .printer()
                .printer(printerA)
                .location(location1)
                .add()
                .build();
        Entity template2 = documentFactory.newTemplate()
                .printer()
                .printer(printerB)
                .location(location2)
                .add()
                .build();

        TestPrinterUseManager manager = new TestPrinterUseManager(Arrays.asList(location1, location2),
                                                                  Arrays.asList(template1, template2));
        assertFalse(manager.hasPrinters());
        manager.refresh();
        checkPrinters(manager, printerA, printerB);

        checkUse(printerA, manager, location1, template1);
        checkUse(printerB, manager, location2, template2);
        checkLocation(location1, printerA, printerA);
        checkLocation(location2, printerB, printerB);

        manager.remove(printerA);
        manager.refresh();
        checkPrinters(manager, printerB);
        checkUse(printerA, manager);
        checkUse(printerB, manager, location2, template2);
        checkLocation(location1, null);
        checkLocation(location2, printerB, printerB);
    }

    /**
     * Verifies a practice location has the expected printers.
     *
     * @param location        the practice location
     * @param expectedDefault the expected default printer. May be {@code null}
     * @param expected        the expected printer references
     */
    private void checkLocation(Party location, PrinterReference expectedDefault, PrinterReference... expected) {
        location = get(location);
        PrinterReference actualDefault = locationRules.getDefaultPrinter(location);
        assertEquals(expectedDefault, actualDefault);
        Set<PrinterReference> actual = new HashSet<>(locationRules.getPrinters(location));
        assertEquals(new HashSet<>(Arrays.asList(expected)), actual);
    }

    /**
     * Verifies the expected printers are present.
     *
     * @param manager  the printer use manager
     * @param expected the expected printer references
     */
    private void checkPrinters(PrinterUseManager manager, PrinterReference... expected) {
        assertEquals(expected.length != 0, manager.hasPrinters());
        List<PrinterReference> printers = manager.getPrinters();
        assertEquals(expected.length, printers.size());
        for (PrinterReference printer : expected) {
            assertTrue(printers.contains(printer));
        }
    }

    /**
     * Verifies that printers are associated with the expected locations/templates.
     *
     * @param reference the printer reference
     * @param manager   the printer use manager
     * @param expected  the expected locations/templates
     */
    private void checkUse(PrinterReference reference, PrinterUseManager manager, Entity... expected) {
        List<Entity> use = manager.getUse(reference);
        assertEquals(expected.length, use.size());
        for (Entity entity : expected) {
            assertTrue(use.contains(entity));
        }
    }

    /**
     * PrinterUseManager implementation that enables the practice locations and templates to be specified.
     */
    private class TestPrinterUseManager extends PrinterUseManager {

        /**
         * The practice locations to query.
         */
        private List<Party> locations;

        /**
         * The templates to query.
         */
        private List<Entity> templates;

        /**
         * Constructs a {@link TestPrinterUseManager}.
         *
         * @param locations the practice locations
         * @param templates the document templates
         */
        public TestPrinterUseManager(List<Party> locations, List<Entity> templates) {
            super(getArchetypeService(), practiceService, locationRules, transactionManager);
            this.locations = locations;
            this.templates = templates;
        }

        /**
         * Initialises this.
         * <p/>
         * This should be called before use, and after making changes
         */
        @Override
        public void refresh() {
            locations = reload(locations);
            templates = reload(templates);
            super.refresh();
        }

        /**
         * Returns the practice locations.
         *
         * @return the practice locations
         */
        @Override
        protected List<Party> getLocations() {
            return locations;
        }

        /**
         * Returns active templates.
         *
         * @return active <em>entity.documentTemplate</em> instances
         */
        @Override
        protected Iterator<Entity> getTemplates() {
            return templates.iterator();
        }

        /**
         * Reloads a collection from the database.
         *
         * @param current the current objects
         * @return the reloaded objects
         */
        private <T extends IMObject> List<T> reload(List<T> current) {
            List<T> result = new ArrayList<>();
            for (T object : current) {
                object = get(object);
                if (object != null) {
                    result.add(object);
                }
            }
            return result;
        }
    }
}