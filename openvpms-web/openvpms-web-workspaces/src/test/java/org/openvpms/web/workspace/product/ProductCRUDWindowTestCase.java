/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.product;

import nextapp.echo2.app.Button;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.product.ProductQuery;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link ProductCRUDWindow} class.
 *
 * @author Tim Anderson
 */
public class ProductCRUDWindowTestCase extends AbstractAppTest {

    private ArchetypeAwareGrantedAuthority all;

    private ArchetypeAwareGrantedAuthority createAll;

    private ArchetypeAwareGrantedAuthority saveAll;

    private ArchetypeAwareGrantedAuthority removeAll;

    private ArchetypeAwareGrantedAuthority createProducts;

    private ArchetypeAwareGrantedAuthority saveProducts;

    private ArchetypeAwareGrantedAuthority removeProducts;

    @Before
    public void setUp() {
        super.setUp();
        all = createAuthority("*", "*");
        createAll = createAuthority("create", "*");
        saveAll = createAuthority("save", "*");
        removeAll = createAuthority("remove", "*");
        createProducts = createAuthority("create", "product.*");
        saveProducts = createAuthority("save", "product.*");
        removeProducts = createAuthority("remove", "product.*");
    }

    /**
     * Tests button permissions for different users.
     */
    @Test
    public void checkButtonPermissions() {
        // admin user has no special access. Access is determined by the product authorities.
        ProductCRUDWindow window1 = createWindow(UserArchetypes.ADMINISTRATOR_USER_TYPE);
        checkButton(window1, ProductCRUDWindow.NEW_ID, false);
        checkButton(window1, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window1, ProductCRUDWindow.EDIT_ID, false);
        checkButton(window1, ProductCRUDWindow.DELETE_ID, false);
        checkButton(window1, ProductCRUDWindow.COPY_ID, false);
        checkButton(window1, ProductCRUDWindow.EXPORT_ID, false);
        checkButton(window1, ProductCRUDWindow.IMPORT_ID, false);
        checkButton(window1, ProductCRUDWindow.SYNCH_ID, false);

        // user with *.* authorities can do everything
        ProductCRUDWindow window2 = createWindow(null, all);
        checkButton(window2, ProductCRUDWindow.NEW_ID, true);
        checkButton(window2, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window2, ProductCRUDWindow.EDIT_ID, true);
        checkButton(window2, ProductCRUDWindow.DELETE_ID, true);
        checkButton(window2, ProductCRUDWindow.COPY_ID, true);
        checkButton(window2, ProductCRUDWindow.EXPORT_ID, true);
        checkButton(window2, ProductCRUDWindow.IMPORT_ID, true);
        checkButton(window2, ProductCRUDWindow.SYNCH_ID, true);

        // user with create *, save *, remove * authorities can do everything
        ProductCRUDWindow window3 = createWindow(null, createAll, saveAll, removeAll);
        checkButton(window3, ProductCRUDWindow.NEW_ID, true);
        checkButton(window3, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window3, ProductCRUDWindow.EDIT_ID, true);
        checkButton(window3, ProductCRUDWindow.DELETE_ID, true);
        checkButton(window3, ProductCRUDWindow.COPY_ID, true);
        checkButton(window3, ProductCRUDWindow.EXPORT_ID, true);
        checkButton(window3, ProductCRUDWindow.IMPORT_ID, true);
        checkButton(window3, ProductCRUDWindow.SYNCH_ID, true);

        // user with create, save, remove product.* authorities can do everything
        ProductCRUDWindow window4 = createWindow(null, createProducts, saveProducts, removeProducts);
        checkButton(window4, ProductCRUDWindow.NEW_ID, true);
        checkButton(window4, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window4, ProductCRUDWindow.EDIT_ID, true);
        checkButton(window4, ProductCRUDWindow.DELETE_ID, true);
        checkButton(window4, ProductCRUDWindow.COPY_ID, true);
        checkButton(window4, ProductCRUDWindow.EXPORT_ID, true);
        checkButton(window4, ProductCRUDWindow.IMPORT_ID, true);
        checkButton(window4, ProductCRUDWindow.SYNCH_ID, true);

        // user with create, save product.* authorities can new/edit/copy
        ProductCRUDWindow window5 = createWindow(null, createProducts, saveProducts);
        checkButton(window5, ProductCRUDWindow.NEW_ID, true);
        checkButton(window5, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window5, ProductCRUDWindow.EDIT_ID, true);
        checkButton(window5, ProductCRUDWindow.DELETE_ID, false);
        checkButton(window5, ProductCRUDWindow.COPY_ID, true);
        checkButton(window5, ProductCRUDWindow.EXPORT_ID, false);
        checkButton(window5, ProductCRUDWindow.IMPORT_ID, false);
        checkButton(window5, ProductCRUDWindow.SYNCH_ID, false);

        // user with no save authorities can only view
        ProductCRUDWindow window6 = createWindow(null, createProducts);
        checkButton(window6, ProductCRUDWindow.NEW_ID, false);
        checkButton(window6, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window6, ProductCRUDWindow.EDIT_ID, false);
        checkButton(window6, ProductCRUDWindow.DELETE_ID, false);
        checkButton(window6, ProductCRUDWindow.COPY_ID, false);
        checkButton(window6, ProductCRUDWindow.EXPORT_ID, false);
        checkButton(window6, ProductCRUDWindow.IMPORT_ID, false);
        checkButton(window6, ProductCRUDWindow.SYNCH_ID, false);

        // user with no create authorities can only view
        ProductCRUDWindow window7 = createWindow(null, saveProducts);
        checkButton(window7, ProductCRUDWindow.NEW_ID, false);
        checkButton(window7, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window7, ProductCRUDWindow.EDIT_ID, false);
        checkButton(window7, ProductCRUDWindow.DELETE_ID, false);
        checkButton(window7, ProductCRUDWindow.COPY_ID, false);
        checkButton(window7, ProductCRUDWindow.EXPORT_ID, false);
        checkButton(window7, ProductCRUDWindow.IMPORT_ID, false);
        checkButton(window7, ProductCRUDWindow.SYNCH_ID, false);

        // user with individual product authorities can't do anything but view
        ArchetypeAwareGrantedAuthority createMedication = createAuthority("create", "product.medication");
        ArchetypeAwareGrantedAuthority saveMedication = createAuthority("save", "product.medication");
        ProductCRUDWindow window8 = createWindow(null, createMedication, saveMedication);
        checkButton(window8, ProductCRUDWindow.NEW_ID, false);
        checkButton(window8, ProductCRUDWindow.VIEW_ID, true);
        checkButton(window8, ProductCRUDWindow.EDIT_ID, false);
        checkButton(window8, ProductCRUDWindow.DELETE_ID, false);
        checkButton(window8, ProductCRUDWindow.COPY_ID, false);
        checkButton(window8, ProductCRUDWindow.EXPORT_ID, false);
        checkButton(window8, ProductCRUDWindow.IMPORT_ID, false);
        checkButton(window8, ProductCRUDWindow.SYNCH_ID, false);
    }

    /**
     * Check if a button is available or not.
     *
     * @param window   the window
     * @param buttonId the button identifier
     * @param exists   if {@code true}, the button must exist, otherwise it must be absent
     */
    private void checkButton(ProductCRUDWindow window, String buttonId, boolean exists) {
        Button button = EchoTestHelper.findButton(window.getComponent(), buttonId);
        if (exists) {
            assertNotNull(button);
        } else {
            assertNull(button);
        }
    }

    /**
     * Creates a new window.
     *
     * @param userType    the <em>lookup.userType</em> code for the user accessing the window. May be {@code null}
     * @param authorities the user's authorities
     * @return a new window
     */
    @SuppressWarnings("unchecked")
    private ProductCRUDWindow createWindow(String userType, ArchetypeAwareGrantedAuthority... authorities) {
        User user = TestHelper.createUser("foo", false);
        if (userType != null) {
            user.addClassification(TestHelper.getLookup(UserArchetypes.USER_TYPE, userType));
        }
        if (authorities.length != 0) {
            SecurityRole role = new SecurityRole();
            for (ArchetypeAwareGrantedAuthority authority : authorities) {
                role.addAuthority(authority);
            }
            ((org.openvpms.component.business.domain.im.security.User) user).addRole(role);
        }

        Party location = create(PracticeArchetypes.LOCATION, Party.class);

        Context context = new LocalContext();
        context.setUser(user);
        context.setLocation(location);

        Archetypes<Product> archetypes = Archetypes.create("product.*", Product.class);
        ProductQuery query = Mockito.mock(ProductQuery.class);
        ResultSet<Product> set = (ResultSet<Product>) Mockito.mock(ResultSet.class);

        FlowSheetServiceFactory factory = Mockito.mock(FlowSheetServiceFactory.class);
        Mockito.when(factory.isSmartFlowSheetEnabled(location)).thenReturn(true);

        return new ProductCRUDWindow(archetypes, query, set, context, new HelpContext("foo", null)) {
            @Override
            protected FlowSheetServiceFactory getFlowSheetServiceFactory() {
                return factory;
            }
        };
    }

    private ArchetypeAwareGrantedAuthority createAuthority(String method, String archetype) {
        ArchetypeAwareGrantedAuthority authority = new ArchetypeAwareGrantedAuthority();
        authority.setServiceName("archetypeService");
        authority.setMethod(method);
        authority.setShortName(archetype);
        return authority;
    }
}
