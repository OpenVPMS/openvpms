/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.WindowPane;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.print.PrintDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireButton;

/**
 * Tests the {@link ChargeCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class ChargeCRUDWindowTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        context.setPractice(practiceFactory.getPractice());
        context.setLocation(practiceFactory.createLocation());

        initErrorHandler(errors);
    }

    /**
     * Verifies that if a charge has been finalised by another user, it cannot be edited.
     */
    @Test
    public void testEditCharge() {
        checkEditPostedCharge(createInvoice());
        checkEditPostedCharge(createCredit());
        checkEditPostedCharge(createCounterSale());
    }

    /**
     * Verifies that if a charge has been deleted by another user, it cannot be edited.
     */
    @Test
    public void testEditDeletedCharge() {
        checkEditDeletedCharge(createInvoice());
        checkEditDeletedCharge(createCredit());
        checkEditDeletedCharge(createCounterSale());
    }

    /**
     * Tests posting a charge.
     */
    @Test
    public void testPost() {
        checkPost(createInvoice());
        checkPost(createCredit());
        checkPost(createCounterSale());
    }

    /**
     * Verifies that if a charge is posted by another user, attempting to post it again set the object to {@code null}
     * as it is no longer modifiable.
     */
    @Test
    public void testPostAlreadyPosted() {
        checkPostAlreadyPosted(createInvoice());
        checkPostAlreadyPosted(createCredit());
        checkPostAlreadyPosted(createCounterSale());
    }

    /**
     * Tests posting a charge that has been deleted by another user displays an error.
     */
    @Test
    public void testPostDeleted() {
        checkPostDeleted(createInvoice());
        checkPostDeleted(createCredit());
        checkPostDeleted(createCounterSale());
    }

    /**
     * Verifies that if a charge has been finalised by another user, it cannot be edited.
     *
     * @param charge the charge
     */
    private void checkEditPostedCharge(FinancialAct charge) {
        errors.clear();

        ChargeCRUDWindow window = createWindow(charge);
        assertTrue(window.canEdit());

        // simulate finalising the charge by another user
        FinancialAct copy = get(charge);
        copy.setStatus(POSTED);
        save(copy);

        // verify it can't be edited
        assertTrue(window.canEdit());  // works on the cached version
        window.edit();

        assertEquals(1, errors.size());
        assertEquals("Can't edit " + getDisplayName(charge), errors.get(0));
    }

    /**
     * Verifies that if a charge has been finalised by another user, it cannot be edited.
     *
     * @param charge the charge
     */
    private void checkEditDeletedCharge(FinancialAct charge) {
        errors.clear();

        ChargeCRUDWindow window = createWindow(charge);
        assertTrue(window.canEdit());

        // simulate deleting the charge by another user
        remove(charge);

        // verify it can't be edited
        assertTrue(window.canEdit());  // works on the cached version
        window.edit();

        assertEquals(1, errors.size());
        assertEquals(getDisplayName(charge) + " no longer exists", errors.get(0));
    }

    /**
     * Tests posting a charge.
     *
     * @param charge the charge
     */
    private void checkPost(FinancialAct charge) {
        errors.clear();
        ChargeCRUDWindow window = createWindow(charge);
        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);
        String displayName = getDisplayName(charge);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        FinancialAct reloaded = get(charge);
        assertEquals(POSTED, reloaded.getStatus());
        assertEquals(reloaded, window.getObject());
        assertEquals(reloaded.getVersion(), window.getObject().getVersion());

        // For invoices and counter sales, a payment workflow should be displayed when the customer balance is positive.
        // For credits, a print dialog should be displayed
        if (charge.isA(CustomerAccountArchetypes.INVOICE, CustomerAccountArchetypes.COUNTER)) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Pay Account", "Do you wish to pay the account?",
                                           ConfirmationDialog.YES_ID);
        } else {
            assertNotNull(findWindowPane(PrintDialog.class));
        }
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that if a charge is posted by another user, attempting to post it again set the object to {@code null}
     * as it is no longer modifiable.
     *
     * @param charge the charge
     */
    private void checkPostAlreadyPosted(FinancialAct charge) {
        errors.clear();
        ChargeCRUDWindow window = createWindow(charge);

        // simulate finalising the charge by another user
        FinancialAct copy = get(charge);
        copy.setStatus(POSTED);
        save(copy);

        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);

        assertNull(window.getObject());  // POSTED charge not editable

        // should be no errors or dialogs displayed
        assertTrue(errors.isEmpty());
        assertNull(EchoTestHelper.findWindowPane(WindowPane.class));
    }

    /**
     * Tests posting a charge that has been deleted by another user displays an error.
     *
     * @param charge the charge
     */
    private void checkPostDeleted(FinancialAct charge) {
        errors.clear();
        ChargeCRUDWindow window = createWindow(charge);

        // simulate deleting the charge by another user
        remove(charge);
        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);

        assertNull(window.getObject());
        assertEquals(1, errors.size());
        assertEquals(getDisplayName(charge) + " no longer exists", errors.get(0));
        assertNull(EchoTestHelper.findWindowPane(WindowPane.class));
    }

    /**
     * Creates a new charge window for a charge, with the context set to the customer.
     *
     * @param charge the charge
     * @return a new window
     */
    private ChargeCRUDWindow createWindow(FinancialAct charge) {
        IMObjectBean bean = getBean(charge);
        context.setCustomer(bean.getTarget("customer", Party.class));

        Archetypes<FinancialAct> archetypes = Archetypes.create("act.customerAccountCharges*", FinancialAct.class);
        ChargeCRUDWindow window = new ChargeCRUDWindow(archetypes, context, new HelpContext("foo", null));
        window.setObject(charge);
        return window;
    }

    /**
     * Creates an invoice.
     *
     * @return the invoice
     */
    private FinancialAct createInvoice() {
        return accountFactory.createInvoice(10, FinancialActStatus.IN_PROGRESS);
    }

    /**
     * Creates a credit.
     *
     * @return the credit
     */
    private FinancialAct createCredit() {
        return accountFactory.createCredit(10, FinancialActStatus.IN_PROGRESS);
    }

    /**
     * Creates a counter sale.
     *
     * @return the counter sale
     */
    private FinancialAct createCounterSale() {
        return accountFactory.createCounterSale(10, FinancialActStatus.IN_PROGRESS);
    }
}
