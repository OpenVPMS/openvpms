/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderItemQueryFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.ArchetypeQuery;

import java.util.Arrays;

import static org.openvpms.component.system.common.query.Constraints.in;

/**
 * A {@link ReminderItemQueryFactory} that restricts the returned items to those specified.
 */
class TestReminderItemQueryFactory extends ReminderItemQueryFactory {
    private final Act[] items;

    /**
     * Constructs a {@link TestReminderItemQueryFactory}.
     *
     * @param archetype the archetypes to query. May contain wildcards
     * @param items     the items to return
     */
    public TestReminderItemQueryFactory(String archetype, Act... items) {
        super(archetype);
        this.items = items;
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    @Override
    public ArchetypeQuery createQuery() {
        ArchetypeQuery query = super.createQuery();
        Object[] ids = Arrays.stream(items).map(IMObject::getId).toArray();
        query.add(in("item.id", ids));
        return query;
    }
}