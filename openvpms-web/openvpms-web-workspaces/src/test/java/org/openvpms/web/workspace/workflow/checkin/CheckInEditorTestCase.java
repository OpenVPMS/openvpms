/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkin;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.scheduling.TestTaskVerifier;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.util.IMObjectHelper.reload;

/**
 * Tests the {@link CheckInEditor}.
 *
 * @author Tim Anderson
 */
public class CheckInEditorTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Verifies that a patient cannot be selected that doesn't belong to the current customer.
     */
    @Test
    public void testInvalidPatient() {
        Party location = practiceFactory.createLocation();
        Party customer = customerFactory.createCustomer("J", "Smith");
        Party patient = patientFactory.newPatient().name("Fido").build();
        CheckInEditor editor = new CheckInEditor(customer, patient, schedulingFactory.createSchedule(location),
                                                 null, location, new Date(), null, null,
                                                 new HelpContext("foo", null));
        assertInvalid(editor, "Fido is not owned by Smith,J");
    }

    /**
     * Tests the {@link CheckInEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party location = practiceFactory.createLocation();
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        Entity schedule = schedulingFactory.createSchedule(location);
        Entity workList = schedulingFactory.createWorkList();
        Entity taskType = schedulingFactory.createTaskType();
        User clinician = userFactory.createClinician();
        User user = userFactory.createUser();
        Date arrivalTime = new Date();
        Act appointment = schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customer)
                .patient(patient)
                .build();
        CheckInEditor editor1 = new CheckInEditor(customer, patient, schedule, clinician, location, arrivalTime,
                                                  appointment, user, new HelpContext("foo", null));
        editor1.setWeight(BigDecimal.TEN);
        editor1.setWorkList(workList);
        editor1.setTaskType(taskType);

        CheckInEditor editor2 = editor1.newInstance();
        assertEquals(customer, editor2.getCustomer());
        assertEquals(patient, editor2.getPatient());
        assertEquals(schedule, editor2.getSchedule());
        assertEquals(clinician, editor2.getClinician());
        assertEquals(location, editor2.getLocation());
        assertEquals(arrivalTime, editor2.getArrivalTime());
        assertEquals(user, editor2.getUser());
        assertEquals(appointment, editor2.getAppointment());
        checkEquals(BigDecimal.TEN, editor2.getWeight());
        assertEquals(workList, editor2.getWorkList());
        assertEquals(taskType, editor2.getTaskType());

        // verify IllegalStateException is thrown if the customer doesn't correspond to that on the appointment
        IMObjectBean bean = getBean(appointment);
        bean.setTarget("customer", customerFactory.createCustomer());
        bean.save();
        try {
            editor2.newInstance();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            assertEquals("The appointment customer has changed", expected.getMessage());
        }

        // verify IllegalStateException is thrown if the appointment is removed
        remove(appointment);
        try {
            editor2.newInstance();
            fail("Expected IllegalStateException");
        } catch (IllegalStateException expected) {
            assertEquals("Appointment no longer exists", expected.getMessage());
        }
    }

    /**
     * Verifies a task is created when a work list and task type are selected, and the appointment reason
     * and notes are copied to the task where present.
     */
    @Test
    public void testCreateTask() {
        checkCreateTask(true, "Checkup", "Some notes", "Checkup - Some notes");
        checkCreateTask(true, "Surgery", null, "Surgery");
        checkCreateTask(true, null, "Some notes", "Appointment - Some notes");
        checkCreateTask(true, null, null, "Appointment");
        checkCreateTask(false, null, null, null);
    }

    /**
     * Verifies a task is created when a work list and task type are selected, and the appointment reason
     * and notes are copied to the task where present.
     *
     * @param fromAppointment   determines if the task is being created from an appointment or not
     * @param appointmentReason the appointment reason. May be {@code null}
     * @param appointmentNotes  the appointment notes. May be {@code null}
     * @param taskNotes         the expected task notes. May be {@code null}
     */
    private void checkCreateTask(boolean fromAppointment, String appointmentReason, String appointmentNotes,
                                 String taskNotes) {
        String reasonCode = null;
        if (fromAppointment && appointmentReason != null) {
            Lookup reason = lookupFactory.newLookup(ScheduleArchetypes.VISIT_REASON)
                    .uniqueCode("XREASON")
                    .name(appointmentReason)
                    .build();
            reasonCode = reason.getCode();
        }

        Party location = practiceFactory.createLocation();
        Entity schedule = schedulingFactory.createSchedule(location);
        Party customer = customerFactory.createCustomer("J", "Smith");
        Party patient = patientFactory.createPatient("Fido", customer);

        Act appointment = null;
        if (fromAppointment) {
            appointment = schedulingFactory.newAppointment()
                    .startTime(DateRules.getToday())
                    .schedule(schedule)
                    .appointmentType(schedulingFactory.createAppointmentType())
                    .reason(reasonCode)
                    .customer(customer)
                    .patient(patient)
                    .notes(appointmentNotes)
                    .build();
        }

        Date arrivalTime = new Date();
        CheckInEditor editor = new CheckInEditor(customer, patient, schedule,
                                                 null, location, arrivalTime, appointment, null,
                                                 new HelpContext("foo", null));

        editor.setWeight(BigDecimal.TEN);
        Entity workList = schedulingFactory.createWorkList();
        Entity taskType = schedulingFactory.createTaskType();
        editor.setWorkList(workList);
        editor.setTaskType(taskType);
        editor.save();

        Act task = reload(editor.getTask());
        assertNotNull(task);
        TestTaskVerifier verifier = new TestTaskVerifier(getArchetypeService());
        verifier.startTime(arrivalTime)
                .worklist(workList)
                .taskType(taskType)
                .customer(customer)
                .patient(patient)
                .status(TaskStatus.PENDING)
                .notes(taskNotes)
                .verify(task);
    }
}
