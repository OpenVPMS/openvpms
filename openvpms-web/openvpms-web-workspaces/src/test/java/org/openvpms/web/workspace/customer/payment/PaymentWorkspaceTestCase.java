/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Button;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.findButton;

/**
 * Tests the {@link PaymentWorkspace}.
 *
 * @author Tim Anderson
 */
public class PaymentWorkspaceTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies that the New button is disabled for OTC customers.
     */
    @Test
    public void testNewDisabledForOTC() {
        Context context = ContextApplicationInstance.getInstance().getContext();
        context.setCustomer((Party) customerFactory.createCustomer());
        TestWorkspace workspace = new TestWorkspace(context);
        workspace.getComponent();

        // verify that the new button is enabled for a standard customer
        Button new1 = findButton(workspace.getCRUDWindow().getComponent(), PaymentCRUDWindow.NEW_ID);
        assertNotNull(new1);
        assertTrue(new1.isEnabled());

        // verify that the new button is disabled for an OTC customer
        workspace.setObject((Party) practiceFactory.createOTC());
        Button new2 = findButton(workspace.getCRUDWindow().getComponent(), PaymentCRUDWindow.NEW_ID);
        assertNotNull(new2);
        assertFalse(new2.isEnabled());
    }

    /**
     * Test payment workspace, required to expose the CRUD window.
     */
    private static class TestWorkspace extends PaymentWorkspace {
        /**
         * Constructs a {@link TestWorkspace}.
         *
         * @param context the context
         */
        public TestWorkspace(Context context) {
            super(context, Mockito.mock(Preferences.class));
        }

        /**
         * Returns the CRUD window, creating it if it doesn't exist.
         *
         * @return the CRUD window
         */
        @Override
        public CRUDWindow<FinancialAct> getCRUDWindow() {
            return super.getCRUDWindow();
        }
    }
}
