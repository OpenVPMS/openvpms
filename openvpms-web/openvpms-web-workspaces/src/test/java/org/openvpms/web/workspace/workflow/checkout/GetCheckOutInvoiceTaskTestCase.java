/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.consult.AbstractGetConsultInvoiceTaskTest;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link GetCheckOutInvoiceTask}.
 *
 * @author Tim Anderson
 */
public class GetCheckOutInvoiceTaskTestCase extends AbstractGetConsultInvoiceTaskTest {

    /**
     * The visits.
     */
    private Visits visits;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        Party customer = getCustomer();
        visits = new Visits(customer, ServiceHelper.getBean(AppointmentRules.class),
                            ServiceHelper.getBean(PatientRules.class), getArchetypeService());
    }

    /**
     * Verifies that if there is an invoice linked to one of the events being checked-out, it will be returned over any
     * other invoice, unless it is POSTED.
     */
    @Test
    public void testGetInvoiceForMultipleCheckout() {
        Party location = TestHelper.createLocation();
        Party schedule = ScheduleTestHelper.createSchedule(location);
        Party customer = getCustomer();
        Party patient1 = TestHelper.createPatient(customer);
        Party patient2 = TestHelper.createPatient(customer);
        Act appointment1 = ScheduleTestHelper.createAppointment(DateRules.getToday(), DateRules.getTomorrow(), schedule,
                                                                customer, patient1);
        Act appointment2 = ScheduleTestHelper.createAppointment(DateRules.getToday(), DateRules.getTomorrow(), schedule,
                                                                customer, patient2);
        Act event1 = PatientTestHelper.createEvent(DateRules.getToday(), patient1);
        Act event2 = PatientTestHelper.createEvent(DateRules.getToday(), patient2);

        visits.add(event1, appointment1);
        visits.add(event2, appointment2);

        SynchronousTask task = createGetInvoiceTask();

        TaskContext context1 = createCheckOutContext();
        task.execute(context1);
        assertNull(context1.getObject(CustomerAccountArchetypes.INVOICE));

        Product product = TestHelper.createProduct();

        // create 2 invoices for the customer
        List<FinancialAct> acts1 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer, patient1,
                                                                            product, ActStatus.IN_PROGRESS);
        List<FinancialAct> acts2 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer, patient1,
                                                                            product, ActStatus.IN_PROGRESS);
        FinancialAct invoice1 = acts1.get(0);
        FinancialAct invoice2 = acts2.get(0);
        invoice1.setActivityStartTime(DateRules.getYesterday());
        invoice2.setActivityStartTime(DateRules.getToday());
        save(acts1);
        save(acts2);

        // link invoice2 to event2, and verify it is returned
        addToEvent(event2, acts2.get(1));

        TaskContext context2 = createCheckOutContext();
        task.execute(context2);
        assertEquals(invoice2, context2.getObject(CustomerAccountArchetypes.INVOICE));

        // now post invoice2 and verify invoice1 is returned
        invoice2.setStatus(ActStatus.POSTED);
        save(invoice2);

        TaskContext context3 = createCheckOutContext();
        task.execute(context3);
        assertEquals(invoice1, context3.getObject(CustomerAccountArchetypes.INVOICE));

        // create another invoice, and link invoice2 to event1 and verify it is still returned.
        List<FinancialAct> acts3 = FinancialTestHelper.createChargesInvoice(BigDecimal.TEN, customer, patient1,
                                                                            product, ActStatus.IN_PROGRESS);
        save(acts3);
        FinancialAct invoice3 = acts3.get(0);

        addToEvent(event1, acts1.get(1));

        TaskContext context4 = createCheckOutContext();
        task.execute(context4);
        assertEquals(invoice1, context4.getObject(CustomerAccountArchetypes.INVOICE));

        // now post invoice1 and verify invoice3 is returned
        invoice1.setStatus(ActStatus.POSTED);
        save(invoice1);

        TaskContext context5 = createCheckOutContext();
        task.execute(context5);
        assertEquals(invoice3, context5.getObject(CustomerAccountArchetypes.INVOICE));

        // now post invoice3
        invoice3.setStatus(ActStatus.POSTED);
        save(invoice3);

        // verify that invoice2 is now returned, as it is linked to one of the events, and is newer than invoice1
        TaskContext context6 = createCheckOutContext();
        task.execute(context6);
        assertEquals(invoice2, context6.getObject(CustomerAccountArchetypes.INVOICE));
    }

    /**
     * Creates a new task to populate the context with the invoice.
     *
     * @return a new task
     */
    @Override
    protected SynchronousTask createGetInvoiceTask() {
        return new GetCheckOutInvoiceTask(visits);
    }

    /**
     * Creates a new task context.
     *
     * @return a new task context
     */
    private TaskContext createCheckOutContext() {
        TaskContext context = new DefaultTaskContext(new HelpContext("foo", null));
        context.addObject(getCustomer());
        return context;
    }

}
