/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.payment;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.rules.finance.till.TillBalanceStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link AdminCustomerPaymentEditor}.
 *
 * @author Tim Anderson
 */
public class AdminCustomerPaymentEditorTestCase extends AbstractAppTest {

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules rules;

    /**
     * Tests the editor.
     */
    @Test
    public void testEditor() {
        Party customer = TestHelper.createCustomer();
        FinancialAct cash = FinancialTestHelper.createPaymentCash(BigDecimal.ONE);
        FinancialAct eft = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        List<FinancialAct> acts = FinancialTestHelper.createPayment(
                customer, TestHelper.createTill(), ActStatus.POSTED, cash, eft);
        Date startTime = DateRules.getYesterday();
        acts.get(0).setActivityStartTime(startTime);
        save(acts);

        // check the initial balance
        checkEquals(BigDecimal.valueOf(-11), rules.getBalance(customer));

        TestAdminCustomerPaymentEditor editor = new TestAdminCustomerPaymentEditor(
                acts.get(0), null, new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
        editor.getComponent();
        assertTrue(editor.validate(new DefaultValidator()));

        AdminPaymentActRelationshipCollectionEditor items = editor.getItems();
        FinancialAct currentCash = getAct(CustomerAccountArchetypes.PAYMENT_CASH, items.getCurrentActs());
        items.change(currentCash, CustomerAccountArchetypes.PAYMENT_CREDIT);

        assertTrue(SaveHelper.save(editor));

        checkEquals(BigDecimal.valueOf(-11), rules.getBalance(customer)); // balance should be unchanged
        assertNull(get(cash)); // verify the cash has been deleted

        FinancialAct payment = get(acts.get(0));
        assertNotNull(payment);
        assertEquals(startTime, payment.getActivityStartTime());
        checkEquals(BigDecimal.valueOf(11), payment.getTotal());

        checkItem(payment, CustomerAccountArchetypes.PAYMENT_CREDIT, BigDecimal.ONE);
        checkItem(payment, CustomerAccountArchetypes.PAYMENT_EFT, BigDecimal.TEN);

        // check the audit message
        IMObjectBean bean = getBean(payment);
        String audit = bean.getString("audit");
        assertNotNull(audit);
        assertTrue(audit.matches(".*Cash.*Credit Card"));
    }

    /**
     * Verifies that the editor can't be constructed with an invalid act.
     */
    @Test
    public void testInvalidAct() {
        Party customer = TestHelper.createCustomer();
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, TestHelper.createTill(),
                                                                 ActStatus.POSTED);
        checkInvalidAct(payment);

        payment.setStatus(ActStatus.IN_PROGRESS);
        save(payment);

        checkInvalidAct(payment);
    }

    /**
     * Verifies that a payment cannot be saved if the till balance is cleared.
     */
    @Test
    public void testValidation() {
        Party customer = TestHelper.createCustomer();
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, TestHelper.createTill(),
                                                                 ActStatus.POSTED);
        save(payment);
        IMObjectBean bean = getBean(payment);
        FinancialAct tillBalance = bean.getSource("tillBalance", FinancialAct.class);
        assertEquals(TillBalanceStatus.UNCLEARED, tillBalance.getStatus());

        AdminCustomerPaymentEditor editor = new AdminCustomerPaymentEditor(
                payment, null, new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
        assertTrue(editor.validate(new DefaultValidator()));

        tillBalance.setStatus(TillBalanceStatus.IN_PROGRESS);
        save(tillBalance);
        editor.resetValid();
        assertTrue(editor.validate(new DefaultValidator()));

        tillBalance.setStatus(TillBalanceStatus.CLEARED);
        save(tillBalance);
        editor.resetValid();
        assertInvalid(editor, "This payment cannot be edited as it linked to a Cleared Till Balance");
    }

    /**
     * Verifies that the status and total cannot change.
     */
    @Test
    public void testInvariants() {
        Party customer = TestHelper.createCustomer();
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, TestHelper.createTill(),
                                                                 ActStatus.POSTED);
        save(payment);

        AdminCustomerPaymentEditor editor = new AdminCustomerPaymentEditor(
                payment, null, new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
        assertTrue(editor.validate(new DefaultValidator()));

        editor.getProperty("amount").setValue(BigDecimal.ONE);

        assertInvalid(editor, "The total payment must be $10.00");

        editor.getProperty("amount").setValue(BigDecimal.TEN);
        assertTrue(editor.validate(new DefaultValidator()));

        editor.setStatus(ActStatus.IN_PROGRESS);
        assertInvalid(editor, "Status must be POSTED");
    }

    /**
     * Verifies that EFT payment items with associated transactions cannot be changed.
     */
    @Test
    public void checkCannotChangeEFTWithTransactions() {
        Party customer = TestHelper.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = accountFactory.newEFTPOSPayment().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.APPROVED)
                .build();

        FinancialAct item1 = accountFactory.newEFTPaymentItem()
                .amount(TEN)
                .build(false);
        FinancialAct item2 = accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);
        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item1, item2)
                .status(ActStatus.POSTED)
                .build();

        // check the initial balance
        checkEquals(BigDecimal.valueOf(-20), rules.getBalance(customer));

        TestAdminCustomerPaymentEditor editor = new TestAdminCustomerPaymentEditor(
                payment, null, new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
        editor.getComponent();
        assertTrue(editor.validate(new DefaultValidator()));

        AdminPaymentActRelationshipCollectionEditor items = editor.getItems();
        items.change(item1, CustomerAccountArchetypes.PAYMENT_CREDIT);
        assertTrue(editor.isValid());

        assertTrue(SaveHelper.save(editor));

        assertNull(get(item1)); // verify the first EFT item has been deleted

        // verify the balance hasn't changed
        checkEquals(BigDecimal.valueOf(-20), rules.getBalance(customer));

        // check the audit message
        IMObjectBean bean = getBean(payment);
        String audit = bean.getString("audit");
        assertNotNull(audit);
        assertTrue(audit.matches(".*EFT.*Credit Card"));

        // now verify attempting to change the EFT item with associated transactions fails
        try {
            items.change(item2, CustomerAccountArchetypes.PAYMENT_CREDIT);
            fail("Expected change to fail");
        } catch (IllegalStateException expected) {
            // do nothing
        }
    }

    /**
     * Verifies an item is present with the correct amount.
     *
     * @param payment   the payment
     * @param archetype the item archetype
     * @param total     the expected total
     */
    private void checkItem(FinancialAct payment, String archetype, BigDecimal total) {
        IMObjectBean bean = getBean(payment);
        FinancialAct item = bean.getTarget("items", FinancialAct.class, Policies.any(Predicates.targetIsA(archetype)));
        assertNotNull(item);
        checkEquals(total, item.getTotal());
    }

    /**
     * Verifies that the editor can't be constructed with an invalid act.
     *
     * @param act the act to check
     */
    private void checkInvalidAct(FinancialAct act) {
        try {
            new AdminCustomerPaymentEditor(act, null, new DefaultLayoutContext(new LocalContext(),
                                                                               new HelpContext("foo", null)));
            fail("Expected constructor to fail");
        } catch (IllegalArgumentException expected) {
            assertEquals("Argument 'act' must be saved and have POSTED status", expected.getMessage());
        }
    }

    /**
     * Returns an act from a collection given its archetype.
     *
     * @param archetype the archetype
     * @param acts      the act collection
     * @return the corresponding act
     */
    private FinancialAct getAct(String archetype, Collection<? extends IMObject> acts) {
        FinancialAct result = (FinancialAct) acts.stream().filter(act -> act.isA(archetype)).findFirst().orElse(null);
        assertNotNull(result);
        return result;
    }

    private static class TestAdminCustomerPaymentEditor extends AdminCustomerPaymentEditor {
        /**
         * Constructs a {@link TestAdminCustomerPaymentEditor}.
         *
         * @param act     the act to edit
         * @param parent  the parent object. May be {@code null}
         * @param context the layout context
         */
        public TestAdminCustomerPaymentEditor(FinancialAct act, IMObject parent, LayoutContext context) {
            super(act, parent, context);
        }

        /**
         * Returns the items collection editor.
         *
         * @return the items collection editor. May be {@code null}
         */
        @Override
        public AdminPaymentActRelationshipCollectionEditor getItems() {
            return (AdminPaymentActRelationshipCollectionEditor) super.getItems();
        }
    }
}
