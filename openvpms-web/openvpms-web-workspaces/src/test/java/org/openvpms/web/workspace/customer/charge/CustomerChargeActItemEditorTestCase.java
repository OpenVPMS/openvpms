/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerChargeBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemVerifier;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.scheduling.TestTaskVerifier;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.patient.mr.PatientMedicationActEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.archetype.rules.product.ProductArchetypes.MEDICATION;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkSavePopup;

/**
 * Tests the {@link CustomerChargeActItemEditor} class.
 *
 * @author Tim Anderson
 */
public class CustomerChargeActItemEditorTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The user
     */
    private User user;

    /**
     * The context.
     */
    private Context context;

    /**
     * The context used for createdBy, updatedBy nodes.
     */
    private AuthenticationContext authenticationContext;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        customer = customerFactory.createCustomer();

        // register an ErrorHandler to collect errors
        initErrorHandler(errors);
        context = new LocalContext();
        context.setPractice(getPractice());
        context.setCustomer(customer);

        // create a practice location, with stock control enabled
        Party location = practiceFactory.newLocation()
                .stockControl()
                .build();
        context.setLocation(location);
        context.setStockLocation(practiceFactory.createStockLocation(location));

        // set a minimum price for calculated prices.
        lookupFactory.newCurrency()
                .code("AUD")
                .minPrice(new BigDecimal("0.20"))
                .build();

        authenticationContext = new AuthenticationContextImpl();
        user = userFactory.createUser();
        context.setUser(user);
        authenticationContext.setUser(user);
    }

    /**
     * Tests populating an invoice item with a medication.
     */
    @Test
    public void testInvoiceItemMedication() {
        checkInvoiceItem(MEDICATION);
    }

    /**
     * Tests populating an invoice item with a merchandise product.
     */
    @Test
    public void testInvoiceItemMerchandise() {
        checkInvoiceItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating an invoice item with a service product.
     */
    @Test
    public void testInvoiceItemService() {
        checkInvoiceItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating an invoice item with a template product.
     */
    @Test
    public void testInvoiceItemTemplate() {
        checkItemWithTemplate(createInvoice());
    }

    /**
     * Tests populating a counter sale item with a medication product.
     */
    @Test
    public void testCounterSaleItemMedication() {
        checkCounterSaleItem(MEDICATION);
    }

    /**
     * Tests populating a counter sale item with a merchandise product.
     */
    @Test
    public void testCounterSaleItemMerchandise() {
        checkCounterSaleItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating a counter sale item with a service product.
     */
    @Test
    public void testCounterSaleItemService() {
        checkCounterSaleItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating a counter sale item with a template product.
     */
    @Test
    public void testCounterSaleItemTemplate() {
        checkItemWithTemplate(createCounterSale());
    }

    /**
     * Tests populating a credit item with a medication product.
     */
    @Test
    public void testCreditItemMedication() {
        checkCreditItem(MEDICATION);
    }

    /**
     * Tests populating a credit item with a merchandise product.
     */
    @Test
    public void testCreditItemMerchandise() {
        checkCreditItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating a credit item with a service product.
     */
    @Test
    public void testCreditItemService() {
        checkCreditItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating a credit item with a template product.
     */
    @Test
    public void testCreditItemTemplate() {
        checkItemWithTemplate(createCredit());
    }

    /**
     * Verifies that the clinician can be cleared, as a test for OVPMS-1104.
     */
    @Test
    public void testClearClinician() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.newMerchandise()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();
        addDocumentTemplate(product, PatientArchetypes.DOCUMENT_FORM);
        addDocumentTemplate(product, PatientArchetypes.DOCUMENT_LETTER);

        // set up the context
        CustomerChargeEditContext editContext = createEditContext(layout);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician.
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);

        // set the product
        editor.setProduct(product);

        // editor should now be valid
        assertValid(editor);
        editor.setClinician(null);

        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, user, null, ZERO, quantity,
                                      unitCost, new BigDecimal(10), fixedCost, new BigDecimal(2), discount, tax, total,
                                      true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests department and location service ratios.
     */
    @Test
    public void testServiceRatios() {
        practiceFactory.updatePractice(context.getPractice())
                .departments(true)
                .build();
        Entity productType = productFactory.createProductType();
        practiceFactory.updateLocation(context.getLocation())
                .addServiceRatio(productType, 3)  // triple the fixed and unit prices
                .build();
        Entity department1 = practiceFactory.createDepartment();
        Entity department2 = practiceFactory.newDepartment()
                .addServiceRatio(productType, 2) // double the fixed and unit prices
                .build();
        userFactory.updateUser(user)
                .addDepartments(department1, department2)
                .build();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        context.setDepartment(department1);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        Product product1 = productFactory.newService()
                .type(productType)
                .newUnitPrice().costAndPrice(5, "9.09").add()
                .newFixedPrice().costAndPrice(1, "1.82").add()
                .build();
        Product product2 = productFactory.newService()   // no product type, so no service ratio
                .newUnitPrice().costAndPrice(5, "9.09").add()
                .newFixedPrice().costAndPrice(1, "1.82").add()
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        // create the editor. Should inherit the department with no service ratio
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertEquals(department1, editor.getDepartment());

        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setProduct(product1);

        // editor should now be valid
        assertValid(editor);
        checkSave(charge, editor);

        // the department has no service ratio, so the location service ratio should be used
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService())
                .patient(patient)
                .product(product1)
                .department(department1)
                .createdBy(user)
                .quantity(quantity)
                .serviceRatio(3)
                .fixedCost(fixedCost)
                .fixedPrice(6)
                .unitCost(unitCost)
                .unitPrice(30)
                .tax(6)
                .total(66);

        // verify the item matches that expected
        verifier.verify(get(item));

        // now change to a department with a service ratio.
        editor.setDepartment(department2);
        checkSave(charge, editor);
        verifier.department(department2)
                .serviceRatio(2)
                .fixedPrice(4)
                .unitPrice(20)
                .tax(4)
                .total(44)
                .verify(get(item));

        // now remove the department and verify the location service ratio is applied
        editor.setDepartment(null);
        checkSave(charge, editor);
        verifier.department((Reference) null)
                .serviceRatio(3)
                .fixedPrice(6)
                .unitPrice(30)
                .tax(6)
                .total(66)
                .verify(get(item));

        // now change the product to one where there is no service ratio
        editor.setProduct(product2);
        checkSave(charge, editor);
        verifier.product(product2)
                .serviceRatio(null)
                .fixedPrice(2)
                .unitPrice(10)
                .tax(2)
                .total(22)
                .verify(get(item));

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests calendar based service ratios.
     */
    @Test
    public void testCalendarBasedServiceRatios() {
        Entity productType = productFactory.createProductType();
        Entity calendar = productFactory.createServiceRatioCalendar();
        practiceFactory.updateLocation(context.getLocation())
                .addServiceRatio(productType, 2, calendar)  // double the fixed and unit prices
                .build();

        // add an event so that the service ratio applies tomorrow and not today.
        Date tomorrow = DateRules.getTomorrow();
        schedulingFactory.createCalendarEvent(calendar, tomorrow, DateRules.getNextDate(tomorrow));

        Product product1 = productFactory.newService()
                .type(productType)
                .newUnitPrice().costAndPrice(5, "9.09").add()
                .newFixedPrice().costAndPrice(1, "1.82").add()
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);

        editor.setQuantity(ONE);
        Party patient = patientFactory.createPatient(customer);
        editor.setPatient(patient);
        editor.setProduct(product1);
        assertValid(editor);
        checkSave(charge, editor);

        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService())
                .patient(patient)
                .product(product1)
                .quantity(1)
                .serviceRatio(null)
                .createdBy(user)
                .fixedCost(1)
                .fixedPrice(2)
                .unitCost(5)
                .unitPrice(10)
                .tax(new BigDecimal("1.091"))
                .total(12);
        verifier.verify(get(item));

        // now forward-date the item and verify the service ratio is applied
        editor.setStartTime(tomorrow);
        checkSave(charge, editor);

        verifier.serviceRatio(2)
                .fixedPrice(4)
                .unitPrice(20)
                .tax(new BigDecimal("2.182"))
                .total(24)
                .verify(get(item));
    }


    /**
     * Verifies that prices and totals are correct when the customer has tax exemptions.
     */
    @Test
    public void testTaxExemption() {
        addTaxExemption(customer);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.newMerchandise()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician.
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);

        // set the product
        editor.setProduct(product);

        // editor should now be valid
        assertValid(editor);
        editor.setClinician(null);

        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal unitPriceExTax = new BigDecimal("9.00");  // rounded due to minPrice
        BigDecimal fixedPriceExTax = new BigDecimal("1.80"); // rounded due to minPrice
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, user, null, ZERO, quantity,
                                      unitCost, unitPriceExTax, fixedCost, fixedPriceExTax, discount, ZERO,
                                      new BigDecimal("19.80"), true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that prices and totals are correct when the customer has tax exemptions and a service ratio is in place.
     */
    @Test
    public void testTaxExemptionWithServiceRatio() {
        addTaxExemption(customer);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        User clinician = userFactory.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;
        BigDecimal ratio = BigDecimal.valueOf(2); // double the fixed and unit prices

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Entity productType = productFactory.createProductType();
        Product product = productFactory.newMerchandise()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .type(productType)
                .build();
        practiceFactory.updateLocation(context.getLocation())
                .addServiceRatio(productType, ratio)
                .build();

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);
        editor.setProduct(product);

        // editor should now be valid
        assertValid(editor);
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal fixedPriceExTax = new BigDecimal("3.60"); // rounded due to minPrice
        BigDecimal unitPriceExTax = new BigDecimal("18.20"); // rounded due to minPrice
        BigDecimal totalExTax = unitPriceExTax.multiply(quantity).add(fixedPriceExTax);
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, user, clinician, ZERO, quantity,
                                      unitCost, unitPriceExTax, fixedCost, fixedPriceExTax, discount, ZERO, totalExTax,
                                      true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests a product with a 10% discount on an invoice item.
     */
    @Test
    public void testInvoiceItemDiscounts() {
        Charge invoice = createInvoice();
        checkDiscounts(invoice.getCharge(), invoice.getItem(), false);
    }

    /**
     * Tests a product with a 10% discount on an invoice item with a negative quantity.
     */
    @Test
    public void testNegativeInvoiceItemDiscounts() {
        Charge invoice = createInvoice();
        checkDiscounts(invoice.getCharge(), invoice.getItem(), true);
    }

    /**
     * Tests a product with a 10% discount on a counter sale item.
     */
    @Test
    public void testCounterSaleItemDiscounts() {
        Charge charge = createCounterSale();
        checkDiscounts(charge.getCharge(), charge.getItem(), false);
    }

    /**
     * Tests a product with a 10% discount on a counter sale item with a negative quantity.
     */
    @Test
    public void testNegativeCounterSaleItemDiscounts() {
        Charge counterSale = createCounterSale();
        checkDiscounts(counterSale.getCharge(), counterSale.getItem(), true);
    }

    /**
     * Tests a product with a 10% discount on a credit item with a negative quantity.
     */
    @Test
    public void testCreditItemDiscounts() {
        Charge credit = createCredit();
        checkDiscounts(credit.getCharge(), credit.getItem(), false);
    }

    /**
     * Tests a product with a 10% discount on a credit item with a negative quantity.
     */
    @Test
    public void testNegativeCreditDiscounts() {
        Charge credit = createCredit();
        checkDiscounts(credit.getCharge(), credit.getItem(), true);
    }

    /**
     * Tests a product with a 10% discount where discounts are disabled at the practice location.
     * <p/>
     * The calculated discount should be zero.
     */
    @Test
    public void testDisableDiscounts() {
        practiceFactory.updateLocation(context.getLocation())
                .disableDiscounts(true)
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        User clinician = userFactory.createClinician();

        Entity discount = productFactory.newDiscount().percentage(BigDecimal.TEN).discountFixedPrice(true).build();
        Product product = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();
        Party patient = patientFactory.newPatient()
                .owner(customer)
                .addDiscounts(discount)
                .build();
        customerFactory.updateCustomer(customer)
                .addDiscounts(discount)
                .build();
        productFactory.updateProduct(product)
                .addDiscounts(discount)
                .build();

        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext editContext = createEditContext(layout);
        editContext.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        if (!item.isA(CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);
        editor.setQuantity(quantity);

        // editor should now be valid
        assertValid(editor);

        checkSave(charge, editor);

        item = get(item);
        // should be no discount
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = new BigDecimal("2.00");
        BigDecimal total1 = new BigDecimal("22.00");
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, user,
                                      clinician, ZERO, quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax,
                                      discount1, tax1, total1, true);
    }

    /**
     * Verifies that when a product with a dose is selected, the quantity is determined by the patient weight.
     */
    @Test
    public void testInvoiceProductDose() {
        Charge invoice = createInvoice();
        checkProductDose(invoice.getCharge(), invoice.getItem());
    }

    /**
     * Verifies that when a product with a dose is selected, the quantity is determined by the patient weight.
     */
    @Test
    public void testCreditProductDose() {
        Charge credit = createCredit();
        checkProductDose(credit.getCharge(), credit.getItem());
    }

    /**
     * Verifies that when a product with a dose is selected during a counter sale, the quantity remains unchanged
     * as there is no patient.
     */
    @Test
    public void testCounterProductDose() {
        Charge counter = createCounterSale();
        FinancialAct charge = counter.getCharge();
        FinancialAct item = counter.getItem();
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        patientFactory.createWeight(patient, new BigDecimal("4.2"));

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");

        Product product = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .newDose().weightRange(0, 10).rate(1).quantity(1).add()
                .build();

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        assertFalse((editor.isDefaultQuantity()));

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        assertFalse((editor.isDefaultQuantity()));
        editor.setProduct(product);
        checkEquals(quantity, item.getQuantity());
        assertFalse((editor.isDefaultQuantity()));
    }

    /**
     * Verifies that a product with a microchip Patient Identity displays a prompt to add a microchip.
     */
    @Test
    public void testMicrochip() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = patientFactory.createPatient(customer);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.newMerchandise()
                .patientIdentity(PatientArchetypes.MICROCHIP)
                .build();

        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setQuantity(ONE);
        editor.setPatient(patient);
        editor.setProduct(product);

        // editor should be invalid
        assertFalse(editor.isValid());
        assertTrue(editContext.getEditorQueue().getCurrent() instanceof EditDialog);
        EditDialog dialog = (EditDialog) editContext.getEditorQueue().getCurrent();
        IMObjectEditor microchip = dialog.getEditor();
        microchip.getProperty("identity").setValue("123456789");
        checkSavePopup(editContext.getEditorQueue(), PatientArchetypes.MICROCHIP, false);
        checkSave(charge, editor);

        patient = get(patient);
        assertEquals(1, patient.getIdentities().size());
        EntityIdentity identity = patient.getIdentities().iterator().next();
        assertEquals("123456789", identity.getIdentity());
        assertEquals("  (Microchip: 123456789)", patient.getDescription());

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that the editor is invalid if a quantity is less than a minimum quantity.
     * <p/>
     * Note that in practice, the minimum quantity is set by expanding a template or invoicing an estimate.
     */
    @Test
    public void testMinimumQuantities() {
        BigDecimal two = BigDecimal.valueOf(2);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = patientFactory.createPatient(customer);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.createService();
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);

        editor.setMinimumQuantity(two);
        editor.setQuantity(two);
        assertValid(editor);

        // editor should be invalid when quantity set below minimum
        editor.setQuantity(ONE);
        assertFalse(editor.isValid());

        // now set above
        editor.setQuantity(two);
        assertValid(editor);
    }

    /**
     * Verifies that a user with the appropriate user type can override minimum quantities.
     * <p/>
     * Note that in practice, the minimum quantity is set by expanding a template or invoicing an estimate.
     */
    @Test
    public void testMinimumQuantitiesOverride() {
        BigDecimal two = BigDecimal.valueOf(2);

        // set up a user that can override minimum quantities
        Lookup userType = lookupFactory.getLookup("lookup.userType", "MINIMUM_QTY_OVERRIDE");
        practiceFactory.updatePractice(context.getPractice())
                .minimumQuantitiesOverride(userType.getCode())
                .build();
        userFactory.updateUser(user)
                .addClassifications(userType)
                .build();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = patientFactory.createPatient(customer);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.createService();
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);

        editor.setMinimumQuantity(two);
        editor.setQuantity(two);
        assertValid(editor);

        // set the quantity above the minimum. The minimum quantity shouldn't change
        editor.setQuantity(BigDecimal.TEN);
        checkEquals(two, editor.getMinimumQuantity());

        // now set the quantity below the minimum. As the user has the override type, the minimum quantity should update
        editor.setQuantity(ONE);
        checkEquals(1, editor.getMinimumQuantity());
        assertValid(editor);

        // now set a negative quantity. This is supported for invoice level discounts
        BigDecimal minusOne = BigDecimal.valueOf(-1);
        editor.setQuantity(minusOne);
        checkEquals(minusOne, editor.getMinimumQuantity());
        assertValid(editor);

        // set the quantity to zero. This should disable the minimum quantity
        editor.setQuantity(ZERO);
        checkEquals(0, editor.getMinimumQuantity());
        assertValid(editor);

        // verify the minimum is disabled
        editor.setQuantity(ONE);
        checkEquals(0, editor.getMinimumQuantity());
        assertValid(editor);
    }

    /**
     * Verifies that when a product has two fixed prices, the default is selected.
     */
    @Test
    public void testDefaultFixedPrice() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = patientFactory.createPatient(customer);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        Product product = productFactory.newService()
                .newFixedPrice().price(0).defaultPrice(false).add()
                .newFixedPrice().price("0.909").defaultPrice(true).add()
                .build();

        TestCustomerChargeActItemEditor editor1 = createEditor(charge, item, createEditContext(layout), layout);
        editor1.setPatient(patient);
        editor1.setProduct(product);
        editor1.setQuantity(ONE);
        assertValid(editor1);
        save(charge, editor1);

        checkEquals(1, editor1.getFixedPrice());
        checkEquals(1, editor1.getTotal());

        // reload and verify the price doesn't change
        charge = get(charge);
        item = get(item);
        TestCustomerChargeActItemEditor editor2 = createEditor(charge, item, createEditContext(layout), layout);
        checkEquals(1, editor2.getFixedPrice());
        checkEquals(1, editor2.getTotal());
    }

    /**
     * Verifies that stock counts update.
     */
    @Test
    public void testUpdateStock() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = patientFactory.createPatient(customer);
        Product product = productFactory.newMerchandise().fixedPrice(10).build();
        checkStock(product, ZERO);

        // create an item with an initial quantity of zero.
        TestInvoiceBuilder builder = accountFactory.newInvoice();
        FinancialAct charge = builder.customer(customer)
                .item().patient(patient).product(product).quantity(0).unitPrice(1)
                .stockLocation(context.getStockLocation()).add()
                .status(ActStatus.IN_PROGRESS)
                .build();
        FinancialAct item = builder.getItems().get(0);

        TestCustomerChargeActItemEditor editor1 = createEditor(charge, item, createEditContext(layout), layout);
        assertValid(editor1);
        save(charge, editor1);
        checkStock(product, ZERO);

        charge = get(charge);
        item = get(item);

        // set to 10, and verify stock goes down to -10
        TestCustomerChargeActItemEditor editor2 = createEditor(charge, item, createEditContext(layout), layout);
        editor2.setQuantity(BigDecimal.TEN);
        save(charge, editor2);
        checkStock(product, BigDecimal.TEN.negate());

        // set to 0 and verify stock goes back to zero
        editor2.setQuantity(ZERO);
        save(charge, editor2);
        checkStock(product, ZERO);

        // set to 1, and verify stock goes down to -1
        editor2.setQuantity(ONE);
        save(charge, editor2);
        checkStock(product, ONE.negate());
    }

    /**
     * Verifies that discounts are limited to those specified on the fixed price.
     */
    @Test
    public void testFixedPriceMaximumDiscount() {
        // create a 100% discount, and associate it with the product and patient.
        Entity discount = productFactory.newDiscount().percentage(100).discountFixedPrice(true).build();

        Party patient = patientFactory.newPatient()
                .addDiscounts(discount)
                .build();
        Product product = productFactory.newMerchandise()
                .newFixedPrice().costAndPrice(10, 20).maxDiscount(75).defaultPrice(true).add()
                .newFixedPrice().costAndPrice(20, 40).maxDiscount(50).defaultPrice(false).add()
                .addDiscounts(discount)
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, createEditContext(layout), layout);

        editor.setPatient(patient);
        editor.setProduct(product);

        // fixed price should be calculated from the default $20. This has a 75% maximum discount
        checkEquals("22.0", editor.getFixedPrice());
        checkEquals("5.50", editor.getTotal());

        // now change the price to the $44, which calculated from the tax-inc $40 price. This has a 50% maximum discount
        editor.setFixedPrice(new BigDecimal("44.0"));
        checkEquals("22.00", editor.getTotal());

        // now change the price to one not linked to the product. The maximum disccount should default to 100%
        editor.setFixedPrice(new BigDecimal("11.0"));
        checkEquals(0, editor.getTotal());
    }

    /**
     * Verifies that if a reminder type is for a different species than the patient, no reminder is generated.
     */
    @Test
    public void testReminderForDifferentSpecies() {
        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = patientFactory.createPatient(customer); // CANINE
        User clinician1 = userFactory.createClinician();
        context.setClinician(clinician1);

        BigDecimal quantity1 = BigDecimal.valueOf(2);
        BigDecimal unitCost1 = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("9.09");
        BigDecimal unitPrice1IncTax = BigDecimal.TEN;
        BigDecimal fixedCost1 = ONE;
        BigDecimal fixedPrice1 = new BigDecimal("1.82");
        BigDecimal fixedPrice1IncTax = BigDecimal.valueOf(2);
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = BigDecimal.valueOf(2);
        BigDecimal total1 = BigDecimal.valueOf(22);
        Product product = productFactory.newMerchandise()
                .fixedPrice(fixedCost1, fixedPrice1)
                .unitPrice(unitCost1, unitPrice1)
                .build();
        Entity reminderType1 = addReminderType("ZReminderType1", product);
        Entity reminderType2 = addReminderType("ZReminderType2", product, "FELINE");
        Entity reminderType3 = addReminderType("ZReminderType3", product, "CANINE");
        Entity reminderType4 = addReminderType("ZReminderType4", product, "CANINE", "FELINE");

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        editor.setQuantity(quantity1);
        editor.setPatient(patient1);
        editor.setProduct(product);

        // editor should now be valid
        assertValid(editor);

        // save it
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient1, product, null, -1, user, clinician1, ZERO,
                                      quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax, discount1,
                                      tax1, total1, true);
        IMObjectBean itemBean = getBean(item);
        assertTrue(itemBean.getTargets("dispensing").isEmpty());
        assertEquals(3, itemBean.getTargets("reminders").size());

        checkReminder(item, patient1, product, reminderType1, user, clinician1);
        checkReminder(item, patient1, product, reminderType3, user, clinician1);
        checkReminder(item, patient1, product, reminderType4, user, clinician1);

        // now change the patient
        Party patient2 = patientFactory.createPatient(customer);
        IMObjectBean bean = getBean(patient2);
        bean.setValue("species", "FELINE");
        bean.save();
        editor.setPatient(patient2);

        // save it
        checkSave(charge, editor);
        item = get(item);

        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient2, product, null, -1, user, clinician1, ZERO,
                                      quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax, discount1,
                                      tax1, total1, true);

        itemBean = getBean(item);
        assertEquals(3, itemBean.getTargets("reminders").size());

        checkReminder(item, patient2, product, reminderType1, user, clinician1);
        checkReminder(item, patient2, product, reminderType2, user, clinician1);
        checkReminder(item, patient2, product, reminderType4, user, clinician1);
    }

    /**
     * Verifies that if a product has an inactive reminder type, no reminder is created for it.
     */
    @Test
    public void testInactiveReminderTypeDoesntCreateReminder() {
        Party patient = patientFactory.createPatient(customer);

        // create a product with 2 reminder types, one inactive
        Product product = productFactory.createService();
        Entity reminderType1 = addReminderType("ZReminderType1", product);
        Entity reminderType2 = addReminderType("ZReminderType2", product);
        reminderType2.setActive(false);
        save(reminderType2);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        assertValid(editor);
        checkSave(charge, editor);

        // verify there is only 1 reminder
        assertEquals(1, getBean(item).getTargets("reminders", Act.class).size());

        // verify a reminder exists for reminderType1
        checkReminder(get(item), patient, product, reminderType1, user, null);
    }

    /**
     * Verify that batch changes on an invoice item propagate to the medication and vice versa.
     */
    @Test
    public void testBatch() {
        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = patientFactory.createPatient(customer); // CANINE
        User clinician1 = userFactory.createClinician();
        context.setClinician(clinician1);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = ONE;
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        BigDecimal discount = ZERO;
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = BigDecimal.valueOf(22);
        Product product1 = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();
        Date batchExpiry1 = DateRules.getTomorrow();
        Date batchExpiry2 = DateRules.getNextDate(batchExpiry1);
        Entity batch1 = productFactory.createBatch(product1, "Z-923456789", batchExpiry1);
        Entity batch2 = productFactory.createBatch(product1, "Z-123456789", batchExpiry2);

        Product product2 = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor1 = createEditor(charge, item, editContext, layout);
        assertFalse(editor1.isValid());

        editor1.setQuantity(quantity);
        editor1.setPatient(patient1);
        editor1.setProduct(product1);

        assertEquals(batch1, editor1.getBatch());

        assertFalse(editor1.isValid()); // not valid while popup is displayed
        EditDialog medicationEditDialog = EchoTestHelper.findEditDialog();
        assertTrue(medicationEditDialog.getEditor() instanceof PatientMedicationActEditor);
        PatientMedicationActEditor medicationEditor1 = (PatientMedicationActEditor) medicationEditDialog.getEditor();
        assertEquals(batch1, medicationEditor1.getBatch());
        assertEquals(batchExpiry1, medicationEditor1.getExpiryDate());

        // change the batch on the medication, and verify it updates on the invoice item after OK is pressed
        medicationEditor1.setBatch(batch2);
        assertEquals(batch2, medicationEditor1.getBatch());
        assertEquals(batchExpiry2, medicationEditor1.getExpiryDate());

        assertEquals(batch1, editor1.getBatch());  // doesn't update until medication popup closes

        EchoTestHelper.fireDialogButton(medicationEditDialog, PopupDialog.OK_ID);
        assertEquals(batch2, editor1.getBatch());

        assertValid(editor1);

        // save it
        checkSave(charge, editor1);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient1, product1, null, -1, user, clinician1, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount, tax,
                                      total, true);
        checkBatch(item, batch2);
        checkMedication(item, patient1, product1, user, clinician1, batch2, batchExpiry2);

        // re-edit the item and change the product to one without a batch
        TestCustomerChargeActItemEditor editor2 = createEditor(charge, item, editContext, layout);

        editor2.setProduct(product2);
        assertNull(editor2.getBatch());
        assertEquals(batchExpiry2, medicationEditor1.getExpiryDate());
        List<Act> medication = editor2.getDispensingEditor().getCurrentActs();
        assertEquals(1, medication.size());
        PatientMedicationActEditor medicationEditor2
                = (PatientMedicationActEditor) editor2.getDispensingEditor().getEditor(medication.get(0));
        assertNotNull(medicationEditor2);

        assertNull(medicationEditor2.getBatch());
        assertNull(medicationEditor2.getExpiryDate());

        // now change the product back
        editor2.setProduct(product1);

        // the batch doesn't default for non-new acts
        assertNull(editor2.getBatch());

        editor2.setBatch(batch1);
        assertEquals(editor2.getBatch(), batch1);
        assertEquals(batch1, medicationEditor2.getBatch());
        assertEquals(batchExpiry1, medicationEditor2.getExpiryDate());
    }

    /**
     * Verifies that changing the product from one with document templates to one without deletes the documents
     * on the invoice item.
     */
    @Test
    public void testDeleteDocumentOnProductChange() {
        // product with document templates
        Product product1 = productFactory.createService();
        Entity template1 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);

        // product with no templates
        Product product2 = productFactory.createService();

        // product with single template
        Product product3 = productFactory.createService();
        Entity template3 = addDocumentTemplate(product3, PatientArchetypes.DOCUMENT_LETTER);

        User clinician = userFactory.createClinician();
        context.setClinician(clinician);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        CustomerChargeEditContext editContext = createEditContext(layout);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);

        editor.setPatient(patient);
        editor.setProduct(product1);
        editor.setQuantity(ONE);

        save(charge, editor);

        // verify the item has 2 documents
        IMObjectBean bean = getBean(item);
        assertEquals(2, bean.getTargets("documents").size());

        DocumentAct document1 = checkDocument(item, patient, product1, template1, user, clinician, false);
        DocumentAct document2 = checkDocument(item, patient, product1, template2, user, clinician, false);

        // change the product to one without document templates, and verify the documents are removed
        editor.setProduct(product2);

        save(charge, editor);
        assertTrue(bean.getTargets("documents").isEmpty());
        assertNull(get(document1));
        assertNull(get(document2));

        // change to another document, this time with a single template
        editor.setProduct(product3);
        save(charge, editor);
        assertEquals(1, bean.getTargets("documents").size());
        checkDocument(item, patient, product3, template3, user, clinician, false);
    }

    /**
     * Verifies that if a product has an inactive document template, no document is created for it.
     */
    @Test
    public void testInactiveDocumentTemplateDoesntCreateDocument() {
        Party patient = patientFactory.createPatient(customer);

        // create a product with 2 document templates, one inactive
        Product product = productFactory.createService();
        Entity template1 = addDocumentTemplate(product, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addDocumentTemplate(product, PatientArchetypes.DOCUMENT_FORM);
        template2.setActive(false);
        save(template2);

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        assertValid(editor);
        checkSave(charge, editor);

        // verify there is only 1 document
        assertEquals(1, getBean(item).getTargets("documents", Act.class).size());

        // verify a document exists for template1
        checkDocument(item, patient, product, template1, user, null, false);
    }

    /**
     * Verify that when the quantity is zero, the total, tax and discount is zero.
     */
    @Test
    public void testZeroQuantity() {
        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = new BigDecimal(2);
        User clinician = userFactory.createClinician();
        Entity discount = productFactory.newDiscount().percentage(10).discountFixedPrice(true).build();
        Product product = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();
        Party patient = patientFactory.createPatient(customer);
        customerFactory.updateCustomer(customer).addDiscounts(discount).build();
        productFactory.updateProduct(product).addDiscounts(discount).build();

        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        editor.setQuantity(quantity);
        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount1 = new BigDecimal("2.20");
        BigDecimal tax1 = new BigDecimal("1.80");
        BigDecimal total1 = new BigDecimal("19.80");
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, user, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount1, tax1,
                                      total1, true);

        // now set the quantity to zero
        editor.setQuantity(ZERO);
        checkSave(charge, editor);

        item = get(item);
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, user, clinician, ZERO,
                                      ZERO, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, ZERO, ZERO, ZERO,
                                      true);
    }

    /**
     * Verify there are no limits on invoicing restricted drugs.
     */
    @Test
    public void testInvoiceRestrictedDrug() {
        Party patient = patientFactory.createPatient(customer);
        Charge invoice1 = createInvoice(patient);
        Charge invoice2 = createInvoice(patient);
        checkPreventDispenseRestrictedDrugsOTC(invoice1.getCharge(), invoice1.getItem(), false);
        checkDispenseRestrictedDrugs(invoice2.getCharge(), invoice2.getItem());
    }

    /**
     * Verify there are no limits on crediting restricted drugs.
     */
    @Test
    public void testCreditRestrictedDrug() {
        Party patient = patientFactory.createPatient(customer);
        Charge credit1 = createCredit(patient);
        Charge credit2 = createCredit(patient);
        checkPreventDispenseRestrictedDrugsOTC(credit1.getCharge(), credit1.getItem(), false);
        checkDispenseRestrictedDrugs(credit2.getCharge(), credit2.getItem());
    }

    /**
     * Verifies that restricted drugs may not be dispensed when <em>sellRestrictedDrugsOTC</em> is
     * {@code true} for counter sales.
     */
    @Test
    public void testSellRestrictedDrugOverTheCounter() {
        Charge counter1 = createCounterSale();
        checkPreventDispenseRestrictedDrugsOTC(counter1.getCharge(), counter1.getItem(), true);

        Charge counter2 = createCounterSale();
        checkDispenseRestrictedDrugs(counter2.getCharge(), counter1.getItem());
    }

    /**
     * Verifies that changing the patient deletes the existing alert and creates a new one.
     */
    @Test
    public void testChangePatientRecreatesAlert() {
        Party patient1 = patientFactory.createPatient(customer);
        Party patient2 = patientFactory.createPatient(customer);

        Entity alertType = patientFactory.newAlertType().interactive(true).build();
        Product product = productFactory.newService()
                .addAlertTypes(alertType)
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient1);
        editor.setProduct(product);
        assertFalse(editor.isValid());
        checkSavePopup(editor.getEditorQueue(), PatientArchetypes.ALERT, false);
        assertValid(editor);
        checkSave(charge, editor);

        Act alert1 = checkAlert(item, patient1, product, alertType, user, null);

        // now change the patient, and verify a new alert is created and the original is deleted
        editor.setPatient(patient2);
        checkSave(charge, editor);

        checkAlert(item, patient2, product, alertType, user, null);
        assertNull(get(alert1)); // alert1 now deleted
    }

    /**
     * Verifies that if a product has an inactive alert type, no alert is created for it.
     */
    @Test
    public void testInactiveAlertTypeDoesNotCreateAlert() {
        Party patient = patientFactory.createPatient(customer);

        Entity alertType1 = patientFactory.createAlertType();
        Entity alertType2 = patientFactory.newAlertType().active(false).build();
        Product product = productFactory.newService()
                .addAlertTypes(alertType1, alertType2)
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        assertValid(editor);
        checkSave(charge, editor);

        // verify there is only 1 alert
        assertEquals(1, getBean(item).getTargets("alerts", Act.class).size());

        // verify an alert exists for alertType1
        checkAlert(item, patient, product, alertType1, user, null);
    }

    /**
     * Verifies a task is created if one is associated with the product.
     */
    @Test
    public void testProductTask() {
        Party patient1 = patientFactory.createPatient(customer);
        Party patient2 = patientFactory.createPatient(customer);

        Entity taskType = schedulingFactory.createTaskType();
        Entity worklist = schedulingFactory.newWorkList().taskTypes(taskType).build();
        Product product = productFactory.newService()
                .newProductTask()
                .taskType(taskType)
                .workList(worklist)
                .start(1, DateUnits.DAYS)
                .interactive(true)
                .add()
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient1);
        editor.setProduct(product);
        assertFalse(editor.isValid());
        checkSavePopup(editor.getEditorQueue(), ScheduleArchetypes.TASK, false);
        assertValid(editor);
        checkSave(charge, editor);

        List<Act> tasks1 = getBean(item).getTargets("tasks", Act.class);
        assertEquals(1, tasks1.size());
        Act task1 = tasks1.get(0);

        // the task start time is calculated relative to the item start time. The task start time can be set
        // by the user, to the minute, so need to truncate the item start time
        Date startTime = DateUtils.truncate(item.getActivityStartTime(), Calendar.MINUTE);
        TestTaskVerifier verifier = new TestTaskVerifier(getArchetypeService());
        verifier.startTime(DateRules.getDate(startTime, 1, DateUnits.DAYS))
                .worklist(worklist)
                .taskType(taskType)
                .customer(customer)
                .patient(patient1)
                .verify(task1);

        // now change the patient and verify the task is deleted and a new one created.
        editor.setPatient(patient2);
        checkSavePopup(editor.getEditorQueue(), ScheduleArchetypes.TASK, false);
        assertValid(editor);
        checkSave(charge, editor);

        assertNull(get(task1));
        List<Act> tasks2 = getBean(item).getTargets("tasks", Act.class);
        assertEquals(1, tasks2.size());
        Act task2 = tasks2.get(0);

        verifier.patient(patient2)
                .verify(task2);

        // now change to a product without a task, and verify task2 is deleted
        editor.setProduct(productFactory.createService());
        assertValid(editor);
        checkSave(charge, editor);

        assertNull(get(task2));
        List<Act> tasks3 = getBean(item).getTargets("tasks", Act.class);
        assertEquals(0, tasks3.size());
    }

    /**
     * Verifies that if a product has an inactive task type or work list, no task is created for it.
     */
    @Test
    public void testInactiveTaskTypeDoesNotCreateType() {
        Party patient = patientFactory.createPatient(customer);

        Entity taskType1 = schedulingFactory.createTaskType();
        Entity worklist1 = schedulingFactory.newWorkList().taskTypes(taskType1).build();
        Entity taskType2 = schedulingFactory.newTaskType().active(false).build();
        Entity taskType3 = schedulingFactory.createTaskType();
        Entity worklist3 = schedulingFactory.newWorkList().taskTypes(taskType3).active(false).build();
        Product product = productFactory.newService()
                .newProductTask()  // add a task with active task type and worklist
                .taskType(taskType1)
                .workList(worklist1)
                .interactive(false)
                .start(1, DateUnits.DAYS)
                .add()
                .newProductTask()  // add a task with inactive task type
                .taskType(taskType2)
                .start(0, DateUnits.DAYS)
                .add()
                .newProductTask()
                .taskType(taskType3) // add a task with active task type but inactive worklist
                .workList(worklist3)
                .add()
                .build();

        Charge invoice = createInvoice();
        FinancialAct charge = invoice.getCharge();
        FinancialAct item = invoice.getItem();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        assertValid(editor);
        checkSave(charge, editor);

        // verify only one task is created, for taskType1
        List<Act> tasks = getBean(item).getTargets("tasks", Act.class);
        assertEquals(1, tasks.size());
        Act task = tasks.get(0);

        // the task start time is calculated relative to the item start time. The task start time can be set
        // by the user, to the minute, so need to truncate the item start time
        Date startTime = DateUtils.truncate(item.getActivityStartTime(), Calendar.MINUTE);
        TestTaskVerifier verifier = new TestTaskVerifier(getArchetypeService());
        verifier.startTime(DateRules.getDate(startTime, 1, DateUnits.DAYS))
                .taskType(taskType1)
                .customer(customer)
                .patient(patient)
                .worklist(worklist1)
                .verify(task);
    }

    /**
     * Verifies that restricted drugs may not be dispensed when <em>sellRestrictedDrugsOTC</em> is {@code true},
     * and {@code expectRestricted == true} is specified.
     *
     * @param charge           the charge
     * @param item             the charge item
     * @param expectRestricted if {@code true}, dispensing a restricted product should fail, else it should succeed
     */
    private void checkPreventDispenseRestrictedDrugsOTC(FinancialAct charge, FinancialAct item,
                                                        boolean expectRestricted) {
        practiceFactory.updatePractice(getPractice())
                .sellRestrictedDrugsOTC(false)
                .build();
        Product restricted = productFactory.newMedication().drugSchedule(true).build();
        Product unrestricted = productFactory.newMedication().drugSchedule(false).build();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context1 = createEditContext(layout);
        context1.setEditorQueue(null); // disable popups

        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context1, layout);
        editor.setProduct(restricted);

        if (expectRestricted) {
            Validator validator = new DefaultValidator();
            assertFalse(editor.validate(validator));
            List<ValidatorError> errors = validator.getErrors(editor);
            assertEquals(1, errors.size());

            assertEquals(restricted.getName() + " cannot be sold over the counter.\n" +
                         "It must be invoiced instead.", errors.get(0).getMessage());
        } else {
            assertValid(editor);
        }

        // verify unrestricted products can be sold
        editor.setProduct(unrestricted);
        assertValid(editor);
    }

    /**
     * Verifies that there are no limits on dispensing restricted drugs when <em>sellRestrictedDrugsOTC</em> is
     * {@code true}.
     *
     * @param charge the charge
     * @param item   the charge item
     */
    private void checkDispenseRestrictedDrugs(FinancialAct charge, FinancialAct item) {
        practiceFactory.updatePractice(getPractice())
                .sellRestrictedDrugsOTC(true)
                .build();
        Product restricted = productFactory.newMedication().drugSchedule(true).build();
        Product unrestricted = productFactory.newMedication().drugSchedule(false).build();

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // should be able to dispense both restricted and unrestricted products
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        editor.setProduct(restricted);
        assertValid(editor);
        editor.setProduct(unrestricted);
        assertValid(editor);
    }

    /**
     * Adds a reminder type to a product.
     *
     * @param name    the reminder type name
     * @param product the product
     * @param species the species the reminder type applies to
     * @return the reminder type
     */
    private Entity addReminderType(String name, Product product, String... species) {
        Entity reminderType = reminderFactory.newReminderType()
                .name(name)
                .addSpecies(species)
                .defaultInterval(1, DateUnits.MONTHS)
                .cancelInterval(2, DateUnits.MONTHS)
                .build();
        productFactory.updateProduct(product)
                .newProductReminder()
                .reminderType(reminderType)
                .add()
                .build();
        return reminderType;
    }

    /**
     * Verifies stock matches that expected.
     *
     * @param product  the product
     * @param expected the expected stock
     */
    private void checkStock(Product product, BigDecimal expected) {
        StockRules rules = new StockRules(getArchetypeService());
        Party stockLocation = context.getStockLocation();
        assertNotNull(stockLocation);
        BigDecimal stock = rules.getStock(product.getObjectReference(), stockLocation.getObjectReference());
        checkEquals(expected, stock);
    }

    /**
     * Creates an edit context.
     *
     * @param layout the layout context
     * @return a new edit context
     */
    private CustomerChargeEditContext createEditContext(LayoutContext layout) {
        return new CustomerChargeEditContext(customer, layout.getContext().getLocation(), layout);
    }

    /**
     * Creates a charge item editor.
     *
     * @param charge  the charge
     * @param item    the charge item
     * @param context the layout context
     * @return a new editor
     */
    private TestCustomerChargeActItemEditor createEditor(FinancialAct charge, FinancialAct item,
                                                         LayoutContext context) {
        return createEditor(charge, item, createEditContext(context), context);
    }

    /**
     * Creates a charge item editor.
     *
     * @param charge        the charge
     * @param item          the charge item
     * @param context       the edit context
     * @param layoutContext the layout context
     * @return a new editor
     */
    private TestCustomerChargeActItemEditor createEditor(FinancialAct charge, FinancialAct item,
                                                         CustomerChargeEditContext context,
                                                         LayoutContext layoutContext) {
        TestCustomerChargeActItemEditor editor = new TestCustomerChargeActItemEditor(item, charge, context,
                                                                                     layoutContext);
        editor.getComponent();
        return editor;
    }

    /**
     * Checks populating an invoice item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkInvoiceItem(String productShortName) {
        Charge invoice = createInvoice();
        checkItem(invoice.getCharge(), invoice.getItem(), productShortName);
    }

    /**
     * Checks populating a counter sale item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkCounterSaleItem(String productShortName) {
        Charge counter = createCounterSale();
        checkItem(counter.getCharge(), counter.getItem(), productShortName);
    }

    /**
     * Checks populating a credit item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkCreditItem(String productShortName) {
        Charge credit = createCredit();
        checkItem(credit.getCharge(), credit.getItem(), productShortName);
    }

    /**
     * Checks populating a charge item with a product.
     *
     * @param charge           the charge
     * @param item             the charge item
     * @param productArchetype the product archetype short name
     */
    private void checkItem(FinancialAct charge, FinancialAct item, String productArchetype) {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = patientFactory.createPatient(customer);
        Party patient2 = patientFactory.createPatient(customer);
        User user2 = userFactory.createUser();
        User clinician1 = userFactory.createClinician();
        User clinician2 = userFactory.createClinician();

        // create product1 with reminder, investigation type and alert type
        BigDecimal quantity1 = BigDecimal.valueOf(2);
        BigDecimal unitCost1 = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("9.09");
        BigDecimal unitPrice1IncTax = BigDecimal.TEN;
        BigDecimal fixedCost1 = ONE;
        BigDecimal fixedPrice1 = new BigDecimal("1.82");
        BigDecimal fixedPrice1IncTax = BigDecimal.valueOf(2);
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = BigDecimal.valueOf(2);
        BigDecimal total1 = BigDecimal.valueOf(22);
        Entity productType = productFactory.createProductType();
        Entity batch1 = null;
        Date batchExpiry1 = DateRules.getTomorrow();
        Date batchExpiry2 = DateRules.getNextDate(batchExpiry1);
        Entity investigationType = laboratoryFactory.createInvestigationType();
        Entity alertType = patientFactory.newAlertType().interactive(true).build();
        Entity taskType = schedulingFactory.createTaskType();
        Entity worklist = schedulingFactory.newWorkList().taskTypes(taskType).build();
        Product product1 = productFactory.newProduct(productArchetype)
                .fixedPrice(fixedCost1, fixedPrice1)
                .unitPrice(unitCost1, unitPrice1)
                .addTests(laboratoryFactory.createTest(investigationType))
                .addAlertTypes(alertType)
                .newProductTask().taskType(taskType).workList(worklist).start(0, DateUnits.DAYS).interactive(true).add()
                .build();
        if (product1.isA(MEDICATION, ProductArchetypes.MERCHANDISE)) {
            // add 2 batches to product. The first should be selected due to its nearer expiry date
            batch1 = productFactory.createBatch(product1, "Z-923456789", batchExpiry1);
            productFactory.createBatch(product1, "Z-123456789", batchExpiry2);
        }
        Entity reminderType = addReminder(product1);
        Entity template1 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);

        // create  product2 with no reminder no investigation type, and a service ratio that doubles the unit and
        // fixed prices
        BigDecimal quantity2 = ONE;
        BigDecimal unitCost2 = BigDecimal.valueOf(5);
        BigDecimal unitPrice2 = BigDecimal.valueOf(5.05);
        BigDecimal fixedCost2 = BigDecimal.valueOf(0.5);
        BigDecimal fixedPrice2 = BigDecimal.valueOf(5.05);
        BigDecimal discount2 = ZERO;
        BigDecimal ratio = BigDecimal.valueOf(2);
        BigDecimal tax2 = BigDecimal.valueOf(2.036);

        // when the service ratio is applied, the unit and price will be calculated as 11.10, then rounded
        // according to minPrice
        BigDecimal roundedPrice = BigDecimal.valueOf(11.20);
        BigDecimal total2 = BigDecimal.valueOf(22.40);

        Product product2 = productFactory.newProduct(productArchetype)
                .type(productType)
                .fixedPrice(fixedCost2, fixedPrice2)
                .unitPrice(unitCost2, unitPrice2)
                .build();

        practiceFactory.updateLocation(context.getLocation())
                .addServiceRatio(productType, ratio)
                .build();

        // set up the context
        layout.getContext().setClinician(clinician1);

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, product. If product1 is a medication, it should trigger a patient medication
        // editor popup
        editor.setQuantity(quantity1);

        if (!item.isA(CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient1);
        }
        editor.setProduct(product1);

        EditorQueue queue = editContext.getEditorQueue();
        if (item.isA(INVOICE_ITEM)) {
            if (product1.isA(MEDICATION)) {
                // invoice items have a dispensing node
                assertFalse(editor.isValid()); // not valid while popup is displayed
                checkSavePopup(queue, PatientArchetypes.PATIENT_MEDICATION, false);
                // save the popup editor - should be a medication
            }

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, InvestigationArchetypes.PATIENT_INVESTIGATION, false);

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, ReminderArchetypes.REMINDER, false);

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, PatientArchetypes.ALERT, false);

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, ScheduleArchetypes.TASK, false);
        }

        // editor should now be valid
        assertValid(editor);

        // save it
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient1, product1, null, -1, user, clinician1,
                                      ZERO, quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax,
                                      discount1, tax1, total1, true);
        IMObjectBean itemBean = getBean(item);
        if (item.isA(INVOICE_ITEM)) {
            checkBatch(item, batch1);
            if (product1.isA(MEDICATION)) {
                // verify there is a medication act
                checkMedication(item, patient1, product1, user, clinician1, batch1, batchExpiry1);
            } else {
                assertTrue(itemBean.getTargets("dispensing").isEmpty());
            }

            assertEquals(1, itemBean.getTargets("investigations").size());
            assertEquals(1, itemBean.getTargets("reminders").size());
            assertEquals(2, itemBean.getTargets("documents").size());
            assertEquals(1, itemBean.getTargets("alerts").size());

            assertNotNull(investigationType);
            checkInvestigation(item, patient1, investigationType, user, clinician1);
            checkReminder(item, patient1, product1, reminderType, user, clinician1);
            checkDocument(item, patient1, product1, template1, user, clinician1, false);
            checkDocument(item, patient1, product1, template2, user, clinician1, false);
            checkAlert(item, patient1, product1, alertType, user, clinician1);
        } else {
            // act shouldn't have investigations, reminders, documents, alerts nodes
            assertFalse(itemBean.hasNode("investigations"));
            assertFalse(itemBean.hasNode("reminders"));
            assertFalse(itemBean.hasNode("documents"));
            assertFalse(itemBean.hasNode("alerts"));
        }

        // now change the patient, product, and clinician
        authenticationContext.setUser(user2);
        if (itemBean.hasNode("patient")) {
            editor.setPatient(patient2);
        }
        if (item.isA(INVOICE_ITEM)) {
            checkSavePopup(queue, InvestigationArchetypes.PATIENT_INVESTIGATION, false);
            checkSavePopup(queue, ReminderArchetypes.REMINDER, false);
            checkSavePopup(queue, PatientArchetypes.ALERT, false);
        }

        editor.setProduct(product2);
        editor.setQuantity(quantity2);
        editor.setDiscount(discount2);
        if (itemBean.hasNode("clinician")) {
            editor.setClinician(clinician2);
        }

        // should be no more popups. For medication products, the existing medication should update
        assertNull(queue.getCurrent());
        assertValid(editor);

        // save it
        checkSave(charge, editor);

        item = get(item);
        assertNotNull(item);

        // fixedPrice2 and unitPrice2 are calculated as 11.10, then rounded to minPrice
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient2, product2, null, -1, user, clinician2,
                                      ZERO, quantity2, unitCost2, roundedPrice, fixedCost2, roundedPrice, discount2,
                                      tax2, total2, true);
        assertEquals(user2.getObjectReference(), item.getUpdatedBy());

        itemBean = getBean(item);
        if (itemBean.isA(INVOICE_ITEM)) {
            if (product2.isA(MEDICATION)) {
                // verify there is a medication act. Note that it retains the original author
                Act medication = checkMedication(item, patient2, product2, user, clinician2, null, null);
                assertEquals(user2.getObjectReference(), medication.getUpdatedBy());
            } else {
                // verify there is no medication act
                assertTrue(itemBean.getTargets("dispensing").isEmpty());
            }

            assertTrue(itemBean.getTargets("investigations").isEmpty());
            assertTrue(itemBean.getTargets("reminders").isEmpty());
            assertTrue(itemBean.getTargets("documents").isEmpty());
            assertTrue(itemBean.getTargets("alerts").isEmpty());
        } else {
            assertFalse(itemBean.hasNode("dispensing"));
            assertFalse(itemBean.hasNode("investigations"));
            assertFalse(itemBean.hasNode("reminders"));
            assertFalse(itemBean.hasNode("documents"));
            assertFalse(itemBean.hasNode("alerts"));
        }

        // make sure that clinicians can be set to null, as a test for OVPMS-1104
        if (itemBean.hasNode("clinician")) {
            editor.setClinician(null);
            assertValid(editor);
            checkSave(charge, editor);

            item = get(item);
            assertNotNull(item);

            // createdBy = user doesn't change
            FinancialTestHelper.checkItem(item, item.getArchetype(), patient2, product2, null,
                                          -1, user, null, ZERO, quantity2, unitCost2, roundedPrice, fixedCost2,
                                          roundedPrice, discount2, tax2, total2, true);
            assertEquals(user2.getObjectReference(), item.getUpdatedBy());
        }

        editor.setProduct(null);       // make sure nulls are handled
        assertFalse(editor.isValid());

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies an invoice item batch matches that expected.
     *
     * @param item  the invoice item
     * @param batch the expected batch. May be {@code null}
     */
    private void checkBatch(FinancialAct item, Entity batch) {
        IMObjectBean bean = getBean(item);
        if (batch == null) {
            assertNull(bean.getTargetRef("batch"));
        } else {
            assertEquals(batch.getObjectReference(), bean.getTargetRef("batch"));
        }
    }

    /**
     * Checks populating a charge item with a template product.
     * <p/>
     * NOTE: currently, charge items with template products validate correctly, but fail to save.
     * <p/>This is because the charge item relationship editor will only expand templates if the charge item itself
     * is valid - marking the item invalid for having a template would prevent this.
     * TODO - not ideal.
     *
     * @param charge the charge
     */
    private void checkItemWithTemplate(Charge charge) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        context.getContext().setPractice(getPractice());

        Party patient = patientFactory.createPatient(customer);
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        Product product = productFactory.newTemplate()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice).build();
        // costs and prices should be ignored

        User clinician = userFactory.createClinician();
        context.getContext().setClinician(clinician);

        FinancialAct item = charge.getItem();
        CustomerChargeActItemEditor editor = new DefaultCustomerChargeActItemEditor(
                item, charge.getCharge(), createEditContext(context), context);
        editor.getComponent();
        assertFalse(editor.isValid());

        // populate quantity, patient, product
        editor.setQuantity(quantity);
        if (!item.isA(CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);

        // editor should now be valid, but won't save
        assertValid(editor);

        try {
            save(charge.getCharge(), editor);
            fail("Expected save to fail");
        } catch (IllegalStateException expected) {
            assertEquals("Cannot save with product template: " + product.getName(), expected.getMessage());
        }

        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, null, clinician, ZERO,
                                      quantity, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, true);
        if (item.isA(INVOICE_ITEM)) {
            IMObjectBean itemBean = getBean(item);
            // verify there are no medication acts
            assertTrue(itemBean.getTargets("dispensing").isEmpty());
        }

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests charging a product with a 10% discount.
     *
     * @param charge           the charge
     * @param item             the charge item
     * @param negativeQuantity if {@code true}, use a negative quantity
     */
    private void checkDiscounts(FinancialAct charge, FinancialAct item, boolean negativeQuantity) {
        BigDecimal quantity = (negativeQuantity) ? BigDecimal.valueOf(-2) : BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = new BigDecimal(2);
        User clinician = userFactory.createClinician();
        Entity discount = productFactory.newDiscount().percentage(10).discountFixedPrice(true).build();
        Product product = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .addDiscounts(discount)
                .build();
        Party patient = patientFactory.createPatient(customer);
        customerFactory.updateCustomer(customer).addDiscounts(discount).build();

        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        assertFalse(editor.isValid());

        if (!item.isA(CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);
        editor.setQuantity(quantity);

        // editor should now be valid
        assertValid(editor);

        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount1 = new BigDecimal("2.20");
        BigDecimal tax1 = new BigDecimal("1.80");
        BigDecimal total1 = new BigDecimal("19.80");
        if (negativeQuantity) {
            tax1 = tax1.negate();
            total1 = total1.negate();
        }
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, user, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount1, tax1,
                                      total1, true);

        // now remove the discounts
        editor.setDiscount(ZERO);
        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount2 = ZERO;
        BigDecimal tax2 = new BigDecimal("2.00");
        BigDecimal total2 = new BigDecimal("22.00");
        if (negativeQuantity) {
            tax2 = tax2.negate();
            total2 = total2.negate();
        }
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, user, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount2, tax2,
                                      total2, true);
    }

    /**
     * Tests charging a product with a dose.
     *
     * @param charge the charge
     * @param item   the charge item
     */
    private void checkProductDose(FinancialAct charge, FinancialAct item) {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = patientFactory.createPatient(customer);
        PatientTestHelper.createWeight(patient, new Date(), new BigDecimal("4.2"), WeightUnits.KILOGRAMS);
        User clinician = userFactory.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        BigDecimal discount = ZERO;
        BigDecimal doseQuantity = new BigDecimal("4.2");

        Product product = productFactory.newMedication()
                .fixedPrice(fixedCost, fixedPrice)
                .unitPrice(unitCost, unitPrice)
                .build();
        productFactory.updateMedication(product)
                .concentration(1)
                .newDose().weightRange(0, 10).rate(1).quantity(1).add()
                .build();

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        editContext.setEditorQueue(null);  // disable popups
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        assertFalse((editor.isDefaultQuantity()));

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        assertFalse((editor.isDefaultQuantity()));

        editor.setPatient(patient);
        editor.setClinician(clinician);
        editor.setProduct(product);
        checkEquals(doseQuantity, editor.getQuantity());
        assertTrue(editor.isDefaultQuantity());

        // editor should now be valid
        assertValid(editor);
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal tax = new BigDecimal("4.0");
        BigDecimal total = new BigDecimal("44.00");
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, user, clinician, ZERO,
                                      doseQuantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount,
                                      tax, total, true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Saves a charge and charge item editor in a single transaction, verifying the save was successful.
     *
     * @param charge the charge
     * @param editor the charge item editor
     */
    private void checkSave(final FinancialAct charge, final CustomerChargeActItemEditor editor) {
        boolean result = save(charge, editor);
        assertTrue(result);
    }

    /**
     * Saves a charge and charge item editor in a single transaction.
     *
     * @param charge the charge
     * @param editor the charge item editor
     * @return {@code true} if the save was successful, otherwise {@code false}
     */
    private boolean save(final FinancialAct charge, final CustomerChargeActItemEditor editor) {
        TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
        return template.execute(status -> {
            PatientHistoryChanges changes = new PatientHistoryChanges(null, getArchetypeService());
            ChargeSaveContext context1 = editor.getSaveContext();
            context1.setHistoryChanges(changes);
            editor.getEditContext().getInvestigations().save();
            boolean saved = SaveHelper.save(charge);
            editor.save();
            if (saved) {
                context1.save();
            }
            context1.setHistoryChanges(null);
            return saved;
        });
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} invoice with a single item, with just the customer populated.
     *
     * @return a new invoice
     */
    private Charge createInvoice() {
        return createInvoice(null);
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} invoice with a single item, with just the customer and patient populated.
     *
     * @param patient the patient
     * @return a new invoice
     */
    private Charge createInvoice(Party patient) {
        return createCharge(accountFactory.newInvoice(), patient);
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} counter sale with a single item, with just the customer populated.
     *
     * @return a new counter sale
     */
    private Charge createCounterSale() {
        return createCharge(accountFactory.newCounterSale(), null);
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} credit with a single item, with just the customer populated.
     *
     * @return a new credit
     */
    private Charge createCredit() {
        return createCredit(null);
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} credit with a single item, with just the customer and patient populated.
     *
     * @param patient the patient
     * @return a new credit
     */
    private Charge createCredit(Party patient) {
        return createCharge(accountFactory.newCredit(), patient);
    }

    /**
     * Creates an unsaved {@code IN_PROGRESS} charge with a single item, with just the customer and patient populated.
     *
     * @param builder the builder
     * @param patient the patient. May be {@code null}
     * @return
     */
    private Charge createCharge(TestCustomerChargeBuilder<?, ?> builder, Party patient) {
        FinancialAct charge = builder.customer(customer)
                .status(ActStatus.IN_PROGRESS)
                .item().patient(patient).add()
                .build(false);
        return new Charge(charge, builder.getItems().get(0));
    }

    private static class TestCustomerChargeActItemEditor extends CustomerChargeActItemEditor {

        /**
         * Constructs a {@link TestCustomerChargeActItemEditor}.
         * <p/>
         * This recalculates the tax amount.
         *
         * @param act           the act to edit
         * @param parent        the parent act
         * @param context       the edit context
         * @param layoutContext the layout context
         */
        TestCustomerChargeActItemEditor(FinancialAct act, Act parent, CustomerChargeEditContext context,
                                        LayoutContext layoutContext) {
            super(act, parent, context, layoutContext);
        }

        @Override
        public ActRelationshipCollectionEditor getDispensingEditor() {
            return super.getDispensingEditor();
        }
    }

    /**
     * Helper to associate a charge and charge item.
     */
    private static class Charge {

        private final FinancialAct charge;

        private final FinancialAct item;

        public Charge(FinancialAct charge, FinancialAct item) {
            this.charge = charge;
            this.item = item;
        }

        public FinancialAct getCharge() {
            return charge;
        }

        public FinancialAct getItem() {
            return item;
        }
    }
}
