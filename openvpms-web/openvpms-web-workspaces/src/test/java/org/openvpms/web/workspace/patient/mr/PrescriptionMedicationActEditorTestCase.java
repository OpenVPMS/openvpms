/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.prescription.PrescriptionRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PrescriptionMedicationActEditor} class.
 *
 * @author Tim Anderson
 */
public class PrescriptionMedicationActEditorTestCase extends AbstractAppTest {

    /**
     * The prescription rules.
     */
    @Autowired
    private PrescriptionRules prescriptionRules;

    /**
     * Verifies that {@link PrescriptionMedicationActEditor#newInstance()} returns {@code null}:
     */
    @Test
    public void testNewInstance() {
        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        Prescriptions prescriptions = new Prescriptions(Collections.emptyList(), prescriptionRules,
                                                        getArchetypeService());
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        PrescriptionMedicationActEditor editor = new PrescriptionMedicationActEditor(act, null, prescriptions,
                                                                                     context);
        assertNull(editor.newInstance());
    }
}