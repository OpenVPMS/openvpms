/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier;

import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Base class for {@link SupplierStockItemEditor} test cases
 *
 * @author Tim Anderson
 */
public class AbstractSupplierStockItemEditorTest extends AbstractAppTest {

    /**
     * The product rules.
     */
    @Autowired
    protected ProductRules productRules;

    /**
     * The lookup factory.
     */
    @Autowired
    protected TestLookupFactory lookupFactory;

    /**
     * The product factory.
     */
    @Autowired
    protected TestProductFactory productFactory;

    /**
     * Verifies that for new stock items, a product-supplier relationship is created if none already exists.
     *
     * @param editor   the stock item editor
     * @param supplier the supplier
     */
    protected void checkCreateProductSupplierRelationship(SupplierStockItemEditor editor, Party supplier) {
        Lookup box = lookupFactory.createUnitOfMeasure("BOX");
        Product product = productFactory.createMedication();

        populate(editor, product, BigDecimal.ONE, "abc", 10, box.getCode(), BigDecimal.ONE);
        assertTrue(SaveHelper.save(editor));

        // verify a product-supplier relationship has been created
        checkProductSupplier(product, supplier, "abc", 10, box.getCode(), BigDecimal.ONE);

        // verify the product-supplier relationship updates on subsequent save
        editor.setListPrice(BigDecimal.TEN);
        assertTrue(SaveHelper.save(editor));
        checkProductSupplier(product, supplier, "abc", 10, box.getCode(), BigDecimal.TEN);
    }

    /**
     * Verifies that for new stock items, the product-supplier relationship is updated if it is different.
     */
    protected void checkUpdateProductSupplierRelationship(SupplierStockItemEditor editor) {
        Lookup box = lookupFactory.createUnitOfMeasure("BOX");
        Product product = productFactory.createMerchandise();
        createProductSupplier(product, editor.getSupplier(), "abc", 5, box.getCode(), BigDecimal.TEN, BigDecimal.TEN);

        populate(editor, product, BigDecimal.ONE, "abc", 10, box.getCode(), BigDecimal.ONE);
        assertTrue(SaveHelper.save(editor));

        // verify the product-supplier relationship has been updated
        checkProductSupplier(product, editor.getSupplier(), "abc", 10, box.getCode(), BigDecimal.ONE);
    }

    /**
     * Helper to populate an editor.
     *
     * @param editor       the editor
     * @param product      the product
     * @param quantity     the quantity
     * @param reorderCode  the reorder code
     * @param packageSize  the package size
     * @param packageUnits the package units
     * @param listPrice    the list price
     */

    protected void populate(SupplierStockItemEditor editor, Product product, BigDecimal quantity,
                            String reorderCode, int packageSize, String packageUnits, BigDecimal listPrice) {
        editor.setProduct(product);
        editor.setQuantity(quantity);
        editor.setReorderCode(reorderCode);
        editor.setPackageSize(packageSize);
        editor.setPackageUnits(packageUnits);
        editor.setListPrice(listPrice);
    }

    /**
     * Creates a product supplier relationship.
     *
     * @param product      the product
     * @param supplier     the supplier
     * @param reorderCode  the reorder code
     * @param packageSize  the package size
     * @param packageUnits the package units
     * @param listPrice    the list price
     * @param nettPrice    the nett price
     */
    protected void createProductSupplier(Product product, Party supplier, String reorderCode, int packageSize,
                                         String packageUnits, BigDecimal listPrice, BigDecimal nettPrice) {
        ProductSupplier productSupplier = productRules.createProductSupplier(product, supplier);
        productSupplier.setReorderCode(reorderCode);
        productSupplier.setPackageSize(packageSize);
        productSupplier.setPackageUnits(packageUnits);
        productSupplier.setListPrice(listPrice);
        productSupplier.setNettPrice(nettPrice);
        save(product, supplier);
    }

    /**
     * Verifies that the there is only one product-supplier relationship, and it matches the expected values.
     *
     * @param product      the product
     * @param supplier     the supplier
     * @param reorderCode  the expected reorder code
     * @param packageSize  the expected package size
     * @param packageUnits the expected package units
     * @param listPrice    the expected list price
     */
    protected void checkProductSupplier(Product product, Party supplier, String reorderCode, int packageSize,
                                        String packageUnits, BigDecimal listPrice) {
        product = get(product);
        supplier = get(supplier);
        List<ProductSupplier> productSuppliers = productRules.getProductSuppliers(product, supplier);
        assertEquals(1, productSuppliers.size());
        ProductSupplier ps = productSuppliers.get(0);
        assertEquals(reorderCode, ps.getReorderCode());
        assertEquals(packageSize, ps.getPackageSize());
        assertEquals(packageUnits, ps.getPackageUnits());
        checkEquals(listPrice, ps.getListPrice());
    }
}
