/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.edit.payment.PaymentItemEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link CustomerPaymentEditor}.
 *
 * @author Tim Anderson
 */
public class CustomerPaymentEditorTestCase extends AbstractAppTest {

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies the till cannot be changed if there is an outstanding or approved EFTPOS transaction.
     */
    @Test
    public void testCannotChangeTill() {
        Party customer = customerFactory.createCustomer();
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct transaction = accountFactory.newEFTPOSPayment().customer(customer)
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.PENDING)
                .build();

        FinancialAct item = accountFactory.newEFTPaymentItem()
                .addTransaction(transaction)
                .amount(TEN)
                .build(false);

        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(practiceFactory.createTill())
                .add(item)
                .build();

        CustomerPaymentEditor editor = createEditor(payment);
        editor.setTill(practiceFactory.createTill());
    }

    /**
     * Verifies that {@link CustomerPaymentEditor#makeSaveableAndPostOnCompletion()} sets the status to
     * {@link FinancialActStatus#IN_PROGRESS} if the act hasn't been saved {@link FinancialActStatus#POSTED}.
     */
    @Test
    public void testMakeSaveableAndPostOnCompletion() {
        FinancialAct payment = accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .status(FinancialActStatus.POSTED)
                .build(false);

        CustomerPaymentEditor editor = createEditor(payment);
        assertEquals(FinancialActStatus.POSTED, editor.getObject().getStatus());
        assertFalse(editor.postOnCompletion());
        assertTrue(editor.canChangeStatus());

        editor.makeSaveableAndPostOnCompletion();
        assertEquals(FinancialActStatus.IN_PROGRESS, editor.getObject().getStatus());
        assertTrue(editor.postOnCompletion());
        assertTrue(editor.canChangeStatus());
    }

    /**
     * Verifies that {@link CustomerPaymentEditor#makeSaveableAndPostOnCompletion()} throws
     * {@code IllegalStateException} if the act has been saved and is POSTED.
     */
    @Test
    public void testMakeSaveableAndPostOnCompletionForPostedTranaction() {
        FinancialAct payment = accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .cash(10)
                .status(ActStatus.POSTED)
                .build();

        CustomerPaymentEditor editor = createEditor(payment);
        assertTrue(editor.isPosted());
        assertFalse(editor.canChangeStatus());
        try {
            editor.makeSaveableAndPostOnCompletion();
            fail("Expected makeSaveableAndPostOnCompletion to throw IllegalStateException");
        } catch (IllegalStateException expected) {
            // no-op
        }
    }

    /**
     * Verifies that if the editor is made POSTED during the edit, a subsequent EFT can be added and its fields
     * are editable. This tests the fix for OVPMS-2981.
     */
    @Test
    public void testCanEditEFTItemForActPostedDuringEdit() {
        Party customer = customerFactory.createCustomer();
        Entity till = practiceFactory.createTill();
        FinancialAct payment = accountFactory.newPayment()
                .customer(customer)
                .till(till)
                .eft(10)
                .status(ActStatus.IN_PROGRESS)
                .build();

        checkCanEditEFTItemForActPostedDuringEdit(payment);

        FinancialAct refund = accountFactory.newRefund()
                .customer(customer)
                .till(till)
                .eft(10)
                .status(ActStatus.IN_PROGRESS)
                .build();

        checkCanEditEFTItemForActPostedDuringEdit(refund);
    }

    /**
     * Verifies that if the editor is made POSTED during the edit, a subsequent EFT can be added and its fields
     * are editable. This tests the fix for OVPMS-2981.
     *
     * @param act the payment/refund
     */
    private void checkCanEditEFTItemForActPostedDuringEdit(FinancialAct act) {
        CustomerPaymentEditor editor1 = createEditor(act);
        assertFalse(editor1.isPosted());
        assertTrue(editor1.canChangeStatus());

        editor1.setStatus(ActStatus.POSTED);
        assertTrue(editor1.isPosted());
        assertTrue(editor1.canChangeStatus());

        boolean payment = act.isA(CustomerAccountArchetypes.PAYMENT);
        String archetype = payment ? CustomerAccountArchetypes.PAYMENT_EFT : CustomerAccountArchetypes.REFUND_EFT;
        PaymentItemEditor itemEditor1 = editor1.addItem(archetype);
        assertTrue(itemEditor1 instanceof EFTPaymentItemEditor);
        Editors editors = ((EFTPaymentItemEditor) itemEditor1).getEditors();

        if (payment) {
            Editor cashout = editors.getEditor("cashout");
            assertNotNull(cashout);
        }
        Editor amount = editors.getEditor("amount");
        assertNotNull(amount);
        itemEditor1.setAmount(TEN);
        assertTrue(SaveHelper.save(editor1));

        // now reload the editor and verify the fields cannot be changed as the act is POSTED
        CustomerPaymentEditor editor2 = createEditor(act);
        assertTrue(editor2.isPosted());
        assertFalse(editor2.canChangeStatus());
        List<EFTPaymentItemEditor> eftItemEditors = editor2.getEFTItemEditors();
        assertEquals(2, eftItemEditors.size());
        for (EFTPaymentItemEditor editor : eftItemEditors) {
            Editors editors2 = editor.getEditors();
            if (payment) {
                assertNull(editors2.getEditor("cashout"));
            }
            assertNull(editors2.getEditor("amount"));
        }
    }

    /**
     * Creates an editor
     *
     * @param act the payment/refund
     * @return a new editor
     */
    private CustomerPaymentEditor createEditor(FinancialAct act) {
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        context.setEdit(true);
        CustomerPaymentEditor editor = new CustomerPaymentEditor(act, null, context);
        editor.getComponent();
        return editor;
    }
}
