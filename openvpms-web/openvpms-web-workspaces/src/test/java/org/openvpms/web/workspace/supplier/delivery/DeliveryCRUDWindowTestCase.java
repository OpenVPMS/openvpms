/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import nextapp.echo2.app.WindowPane;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.customer.charge.ChargeCRUDWindow;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.web.test.EchoTestHelper.findEditDialog;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireButton;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Tests the {@link DeliveryCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class DeliveryCRUDWindowTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The context.
     */
    private Context context;

    /**
     * 'Each' unit-of-measure code.
     */
    private static final String EACH = "EA";

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        initErrorHandler(errors);
        context = new LocalContext();
        context.setPractice(practiceFactory.getPractice());
    }

    /**
     * Tests posting deliveries and returns.
     */
    @Test
    public void testPost() {
        FinancialAct delivery = supplierFactory.newDelivery()
                .supplier(supplierFactory.createSupplier())
                .stockLocation(practiceFactory.createStockLocation())
                .status(ActStatus.IN_PROGRESS)
                .item().product(productFactory.createMedication()).quantity(10).packageSize(10).packageUnits(EACH).add()
                .build();
        checkPost(delivery);

        FinancialAct supplierReturn = supplierFactory.newReturn()
                .supplier(supplierFactory.createSupplier())
                .stockLocation(practiceFactory.createStockLocation())
                .status(ActStatus.IN_PROGRESS)
                .item().product(productFactory.createMedication()).quantity(10).packageSize(10).packageUnits(EACH).add()
                .build();
        checkPost(supplierReturn);
    }

    /**
     * Verifies an editor is displayed if an incomplete delivery is posted.
     */
    @Test
    public void testPostIncompleteDelivery() {
        lookupFactory.createUnitOfMeasure(EACH);

        // create a delivery with an item with no package units specified. This can be saved but not POSTED.
        FinancialAct delivery = supplierFactory.newDelivery()
                .supplier(supplierFactory.createSupplier())
                .stockLocation(practiceFactory.createStockLocation())
                .status(ActStatus.IN_PROGRESS)
                .item().product(productFactory.createMedication()).quantity(10).packageSize(10).packageUnits(null).add()
                .build();

        DeliveryCRUDWindow window = createWindow(delivery);
        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);
        String displayName = getDisplayName(delivery);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        assertEquals(1, errors.size());
        assertEquals("Failed to validate property Package Units: Package Units is required", errors.get(0));
        errors.clear();

        // verify an edit dialog is displayed
        EditDialog dialog = findEditDialog();
        assertNotNull(dialog);
        assertTrue(dialog.getEditor() instanceof DeliveryEditor);
        DeliveryEditor editor = (DeliveryEditor) dialog.getEditor();

        // correct the package units and close the dialog
        FinancialAct item = (FinancialAct) editor.getItems().getActs().get(0);
        DeliveryItemEditor itemEditor = (DeliveryItemEditor) editor.getItems().getEditor(item);
        itemEditor.setPackageUnits(EACH);
        fireDialogButton(dialog, EditDialog.OK_ID);

        // verify the delivery is now posted
        FinancialAct reloaded = get(delivery);
        assertEquals(POSTED, reloaded.getStatus());

        checkPosted(reloaded);
    }

    /**
     * Tests posting an object.
     *
     * @param object the object ot post
     */
    private void checkPost(FinancialAct object) {
        DeliveryCRUDWindow window = createWindow(object);
        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);
        String displayName = getDisplayName(object);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        FinancialAct reloaded = get(object);
        assertEquals(POSTED, reloaded.getStatus());
        assertEquals(reloaded, window.getObject());

        checkPosted(object);
    }

    /**
     * Verify a confirmation dialog is displayed to invoice/credit the supplier after posting a delivery/return.
     *
     * @param object the delivery/return
     */
    private void checkPosted(FinancialAct object) {
        if (object.isA(SupplierArchetypes.DELIVERY)) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Invoice Supplier?",
                                           "Do you want to invoice the supplier?", ConfirmationDialog.OK_ID);
        } else {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Credit Supplier?",
                                           "Do you want to credit the supplier?", ConfirmationDialog.OK_ID);
        }
        assertNull(findWindowPane(WindowPane.class)); // should be no more windows
        assertTrue(errors.isEmpty());                 // should be no errors
    }

    /**
     * Creates a new {@link DeliveryCRUDWindow}.
     *
     * @param object the current object
     * @return a new window
     */
    private DeliveryCRUDWindow createWindow(FinancialAct object) {
        DeliveryCRUDWindow window = new DeliveryCRUDWindow(Archetypes.create(
                new String[]{SupplierArchetypes.DELIVERY, SupplierArchetypes.RETURN}, FinancialAct.class), context,
                                                           new HelpContext("foo", null));
        window.setObject(object);
        return window;
    }
}
