/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job;

import org.junit.Test;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link JobDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class JobDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    IMObjectEditorFactory factory;

    /**
     * Verifies that a job with relationships can be deleted.
     */
    @Test
    public void testDelete() {
        User runAs = TestHelper.createUser();
        Entity job = createJob(runAs);

        JobDeletionHandler handler = createDeletionHandler(job);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));

        assertNull(get(job));            // verify the job has been removed
        assertEquals(runAs, get(runAs)); // verify the user hasn't been removed
    }

    /**
     * Verifies jobs can be deactivated.
     */
    @Test
    public void testDeactivate() {
        User runAs = TestHelper.createUser();
        Entity job = createJob(runAs);
        assertTrue(job.isActive());

        JobDeletionHandler handler = createDeletionHandler(job);
        handler.deactivate();

        assertFalse(get(job).isActive());
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link JobDeletionHandler} for jobs.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        User runAs = TestHelper.createUser();
        Entity job = createJob(runAs);
        IMObjectDeletionHandler<Entity> handler = factory.create(job);
        assertTrue(handler instanceof JobDeletionHandler);
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(job));
    }

    /**
     * Creates a new deletion handler for a job.
     *
     * @param job the job
     * @return a new deletion handler
     */
    private JobDeletionHandler createDeletionHandler(Entity job) {
        IMObjectEditorFactory factory = applicationContext.getBean(IMObjectEditorFactory.class);
        return new JobDeletionHandler(job, factory, ServiceHelper.getTransactionManager(),
                                      ServiceHelper.getArchetypeService());
    }

    /**
     * Creates a job.
     *
     * @return a new job
     */
    private Entity createJob(User user) {
        Entity job = create("entity.jobPharmacyOrderDiscontinuation", Entity.class);
        IMObjectBean bean = getBean(job);
        bean.setTarget("runAs", user);
        bean.setTarget("notify", user);
        bean.save();
        return job;
    }

}
