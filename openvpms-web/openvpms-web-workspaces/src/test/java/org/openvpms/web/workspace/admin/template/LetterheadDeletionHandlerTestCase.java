/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestLetterheadBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.admin.template.letterhead.LetterheadDeletionHandler;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link LetterheadDeletionHandler} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class LetterheadDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Verifies that  letterhead with a logo can be deleted, and the logo is also removed.
     */
    @Test
    public void testDelete() {
        Document document = documentFactory.newDocument()
                .name(ValueStrategy.suffix(".png"))
                .content(RandomUtils.nextBytes(10))
                .build();
        TestLetterheadBuilder builder = documentFactory.newLetterhead()
                .logo(document);
        Entity letterhead = builder.build();
        DocumentAct act = builder.getLogo();

        LetterheadDeletionHandler handler = createDeletionHandler(letterhead);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(letterhead));
        assertNull(get(document));
        assertNull(get(act));
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link LetterheadDeletionHandler} for
     * letterhead.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Entity letterhead = documentFactory.newLetterhead().build();
        IMObjectDeletionHandler<Entity> handler = factory.create(letterhead);
        assertTrue(handler instanceof LetterheadDeletionHandler);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(letterhead));
    }

    /**
     * Creates a new deletion handler for letterhead.
     *
     * @param letterhead the letterhead
     * @return a new deletion handler
     */
    protected LetterheadDeletionHandler createDeletionHandler(Entity letterhead) {
        return new LetterheadDeletionHandler(letterhead, factory, ServiceHelper.getTransactionManager(),
                                             ServiceHelper.getArchetypeService());
    }
}