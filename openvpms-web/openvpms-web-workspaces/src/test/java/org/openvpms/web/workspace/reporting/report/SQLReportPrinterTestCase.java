/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.report;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.print.locator.DocumentPrinterServiceLocator;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentTestHelper;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link SQLReportPrinter}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class SQLReportPrinterTestCase extends AbstractAppTest {

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers handlers;

    /**
     * The domain object service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Verifies that the {@link Context} is available to SQL reports.
     *
     * @throws Exception for any error
     */
    @Test
    public void testContextFields() throws Exception {
        // set up the context
        Context context = DocumentTestHelper.createReportContext();

        // set up the printer to generate an SQL report
        Document document = documentFactory.createDocument("/sqlreport.jrxml");
        DocumentTemplate template = new DocumentTemplate(create(DocumentArchetypes.DOCUMENT_TEMPLATE, Entity.class),
                                                         getArchetypeService()) {
            @Override
            public boolean hasDocument() {
                return true;
            }

            @Override
            public Document getDocument() {
                return document;
            }
        };
        ReportFactory factory = ServiceHelper.getBean(ReportFactory.class);
        FileNameFormatter formatter = ServiceHelper.getBean(FileNameFormatter.class);
        DataSource dataSource = ServiceHelper.getBean("reportingDataSource", DataSource.class);
        DocumentPrinterServiceLocator printerLocator = Mockito.mock(DocumentPrinterServiceLocator.class);
        PrinterContext printerContext = new PrinterContext(printerLocator, getArchetypeService(),
                                                           Mockito.mock(DocumentConverter.class), handlers,
                                                           domainService);
        SQLReportPrinter printer = new SQLReportPrinter(template, context, factory, formatter, dataSource,
                                                        printerContext);

        // pass the customer id as a parameter
        Party customer = TestHelper.createCustomer("Foo", "Bar", true);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("customerId", customer.getId());
        printer.setParameters(parameters);

        // generate the report as a CSV to allow comparison
        Document csv = printer.getDocument(DocFormats.CSV_TYPE, false);
        String result = documentFactory.toString(csv).trim();

        // the customer name should be followed by each of the context fields.
        assertEquals("Foo,Bar,Vets R Us,Main Clinic,Main Stock,\"Smith,J\",Fido,Vet Supplies,Acepromazine,"
                     + "Main Deposit,Main Till,Vet,User,Visit,Invoice,Appointment,Task", result);
    }


}
