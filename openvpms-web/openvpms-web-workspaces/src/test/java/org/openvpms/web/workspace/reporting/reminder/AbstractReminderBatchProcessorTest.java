/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.action.ActionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Base class for {@link AbstractReminderBatchProcessor} tests.
 *
 * @author Tim Anderson
 */
public class AbstractReminderBatchProcessorTest extends ArchetypeServiceTest {

    /**
     * The action factory.
     */
    @Autowired
    protected ActionFactory actionFactory;

    /**
     * The customer factory.
     */
    @Autowired
    protected TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    protected TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    protected TestReminderFactory reminderFactory;

    /**
     * Verifies a reminder has reminder count 0, and the expected next reminder date.
     *
     * @param reminder     the reminder
     * @param nextReminder the expected next reminder date
     */
    protected void checkReminder(Act reminder, Date nextReminder) {
        checkReminder(reminder, 1, nextReminder);
    }

    /**
     * Verifies a reminder has the expected reminder count and next reminder date.
     *
     * @param reminder      the reminder
     * @param reminderCount the expected reminder count
     * @param nextReminder  the expected next reminder date
     */
    protected void checkReminder(Act reminder, int reminderCount, Date nextReminder) {
        reminder = get(reminder);
        assertNotNull(reminder);
        IMObjectBean bean = getBean(reminder);
        assertEquals(reminderCount, bean.getInt("reminderCount"));
        assertEquals(0, DateRules.compareTo(reminder.getActivityStartTime(), nextReminder, true));
    }

    /**
     * Verifies each item has COMPLETED status.
     *
     * @param items the items to check
     */
    protected void checkCompleted(Act... items) {
        checkStatus(ReminderItemStatus.COMPLETED, null, items);
    }

    /**
     * Verifies each reminder item has the expected status.
     *
     * @param status the expected status
     * @param error  the expected error
     * @param items  the items to check
     */
    protected void checkStatus(String status, String error, Act... items) {
        for (Act item : items) {
            item = get(item);
            assertNotNull(item);
            assertEquals(status, item.getStatus());
            IMObjectBean bean = getBean(item);
            assertEquals(error, bean.getString("error"));
        }
    }
}