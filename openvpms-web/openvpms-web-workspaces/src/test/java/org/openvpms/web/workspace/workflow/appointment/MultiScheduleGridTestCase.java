/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link MultiScheduleGrid}.
 *
 * @author Tim Anderson
 */
public class MultiScheduleGridTestCase extends AbstractAppointmentGridTest {

    /**
     * Tests a grid with different schedule times and slot sizes.
     */
    @Test
    public void testGridWithDifferentScheduleTimesAndSlotSizes() {
        Entity schedule1 = createSchedule(15, "09:00", "17:00"); // 9am-5pm schedule, 15 min slots
        Entity schedule2 = createSchedule(60, "08:00", "16:00"); // 8am-4pm schedule, 60 min slots
        Date date = getDate("2019-10-07");

        Act appointment1a = createAppointment("2019-10-07 09:00", "2019-10-07 09:15", schedule1);
        Act appointment1b = createAppointment("2019-10-07 09:30", "2019-10-07 10:00", schedule1);
        Act appointment2a = createAppointment("2019-10-07 08:00", "2019-10-07 10:00", schedule2);
        Act appointment2b = createAppointment("2019-10-07 12:00", "2019-10-07 13:00", schedule2);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule1, schedule2);
        Map<Entity, ScheduleEvents> map = new LinkedHashMap<>();
        map.put(schedule1, getScheduleEvents(appointment1a, appointment1b));
        map.put(schedule2, getScheduleEvents(appointment2a, appointment2b));
        MultiScheduleGrid grid = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(15, grid.getSlotSize());  // uses smallest slot size of the schedules
        assertEquals(36, grid.getSlots());

        // check schedule1
        checkSlot(grid, schedule1, 0, "2019-10-07 08:00", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 1, "2019-10-07 08:15", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 2, "2019-10-07 08:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 3, "2019-10-07 08:45", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 4, "2019-10-07 09:00", appointment1a, 1, Availability.BUSY);
        checkSlot(grid, schedule1, 5, "2019-10-07 09:15", null, 0, Availability.FREE);
        checkSlot(grid, schedule1, 6, "2019-10-07 09:30", appointment1b, 2, Availability.BUSY);
        checkSlot(grid, schedule1, 7, "2019-10-07 09:45", appointment1b, 1, Availability.BUSY);
        checkSlot(grid, schedule1, 8, "2019-10-07 10:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule1, 35, "2019-10-07 16:45", null, 0, Availability.FREE);

        // check schedule2
        checkSlot(grid, schedule2, 0, "2019-10-07 08:00", appointment2a, 8, Availability.BUSY);
        checkSlot(grid, schedule2, 1, "2019-10-07 08:15", appointment2a, 7, Availability.BUSY);
        checkSlot(grid, schedule2, 2, "2019-10-07 08:30", appointment2a, 6, Availability.BUSY);
        checkSlot(grid, schedule2, 3, "2019-10-07 08:45", appointment2a, 5, Availability.BUSY);
        checkSlot(grid, schedule2, 4, "2019-10-07 09:00", appointment2a, 4, Availability.BUSY);
        checkSlot(grid, schedule2, 5, "2019-10-07 09:15", appointment2a, 3, Availability.BUSY);
        checkSlot(grid, schedule2, 6, "2019-10-07 09:30", appointment2a, 2, Availability.BUSY);
        checkSlot(grid, schedule2, 7, "2019-10-07 09:45", appointment2a, 1, Availability.BUSY);
        checkSlot(grid, schedule2, 8, "2019-10-07 10:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule2, 15, "2019-10-07 11:45", null, 0, Availability.FREE);
        checkSlot(grid, schedule2, 16, "2019-10-07 12:00", appointment2b, 4, Availability.BUSY);
        checkSlot(grid, schedule2, 17, "2019-10-07 12:15", appointment2b, 3, Availability.BUSY);
        checkSlot(grid, schedule2, 18, "2019-10-07 12:30", appointment2b, 2, Availability.BUSY);
        checkSlot(grid, schedule2, 19, "2019-10-07 12:45", appointment2b, 1, Availability.BUSY);
        checkSlot(grid, schedule2, 20, "2019-10-07 13:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule2, 35, "2019-10-07 16:45", null, 0, Availability.UNAVAILABLE);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule1);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, grid.getSlots()));
        Schedule second = grid.getSchedules().get(1);
        assertEquals(second.getSchedule(), schedule2);
        assertNull(grid.getEvent(second, -1));
        assertNull(grid.getEvent(second, grid.getSlots()));
    }

    /**
     * Verifies that if an appointment is outside the schedule times, slots are added.
     */
    @Test
    public void testAppointmentsOutsideOfScheduleTimes() {
        Entity schedule1 = createSchedule(30, "09:00", "17:00"); // 9am-5pm schedule, 30 min slots
        Entity schedule2 = createSchedule(60, "08:00", "16:00"); // 8am-4pm schedule, 60 min slots
        Date date = getDate("2019-10-07");

        // schedule1 appointments
        Act appointment1a = createAppointment("2019-10-07 08:00", "2019-10-07 08:30", schedule1);
        Act appointment1b = createAppointment("2019-10-07 16:30", "2019-10-07 17:30", schedule1);

        // schedule2 appointments
        Act appointment2a = createAppointment("2019-10-07 07:00", "2019-10-07 07:30", schedule2);
        // shorter than schedule2 slot size

        Act appointment2b = createAppointment("2019-10-07 16:00", "2019-10-07 17:00", schedule2);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule1, schedule2);
        Map<Entity, ScheduleEvents> map = new LinkedHashMap<>();
        map.put(schedule1, getScheduleEvents(appointment1a, appointment1b));
        map.put(schedule2, getScheduleEvents(appointment2a, appointment2b));
        MultiScheduleGrid grid = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(30, grid.getSlotSize());  // uses smallest slot size of the schedules
        assertEquals(21, grid.getSlots());

        // check schedule1
        checkSlot(grid, schedule1, 0, "2019-10-07 07:00", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 1, "2019-10-07 07:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 2, "2019-10-07 08:00", appointment1a, 1, Availability.BUSY);
        checkSlot(grid, schedule1, 3, "2019-10-07 08:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule1, 4, "2019-10-07 09:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule1, 18, "2019-10-07 16:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule1, 19, "2019-10-07 16:30", appointment1b, 2, Availability.BUSY);
        checkSlot(grid, schedule1, 20, "2019-10-07 17:00", appointment1b, 1, Availability.BUSY);

        // check schedule2
        checkSlot(grid, schedule2, 0, "2019-10-07 07:00", appointment2a, 1, Availability.BUSY);
        checkSlot(grid, schedule2, 1, "2019-10-07 07:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid, schedule2, 2, "2019-10-07 08:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule2, 3, "2019-10-07 08:30", null, 0, Availability.FREE);
        checkSlot(grid, schedule2, 18, "2019-10-07 16:00", appointment2b, 2, Availability.BUSY);
        checkSlot(grid, schedule2, 19, "2019-10-07 16:30", appointment2b, 1, Availability.BUSY);
        checkSlot(grid, schedule2, 20, "2019-10-07 17:00", null, 0, Availability.UNAVAILABLE);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule1);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, grid.getSlots()));
        Schedule second = grid.getSchedules().get(1);
        assertEquals(second.getSchedule(), schedule2);
        assertNull(grid.getEvent(second, -1));
        assertNull(grid.getEvent(second, grid.getSlots()));
    }

    /**
     * Tests the behaviour of appointments that don't fall on slot boundaries.
     */
    @Test
    public void testAppointmentsNotOnSlotBoundaries() {
        Entity schedule = createSchedule(60, "09:00", "17:00"); // 9am-5pm schedule, 1 hour slots
        Date date = getDate("2019-10-07");
        Act appointment1 = createAppointment("2019-10-07 08:30", "2019-10-07 09:30", schedule);
        Act appointment2 = createAppointment("2019-10-07 11:00", "2019-10-07 11:15", schedule);
        Act appointment3 = createAppointment("2019-10-07 12:30", "2019-10-07 13:00", schedule);
        Act appointment4 = createAppointment("2019-10-07 16:45", "2019-10-07 17:15", schedule);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule, getScheduleEvents(appointment1, appointment2, appointment3, appointment4));
        MultiScheduleGrid grid = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(10, grid.getSlots());
        checkSlot(grid, schedule, 0, "2019-10-07T08:00:00+11:00", appointment1, 2, Availability.BUSY);
        checkSlot(grid, schedule, 1, "2019-10-07T09:00:00+11:00", appointment1, 1, Availability.BUSY);
        checkSlot(grid, schedule, 2, "2019-10-07T10:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 3, "2019-10-07T11:00:00+11:00", appointment2, 1, Availability.BUSY);
        checkSlot(grid, schedule, 4, "2019-10-07T12:00:00+11:00", appointment3, 1, Availability.BUSY);
        checkSlot(grid, schedule, 5, "2019-10-07T13:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 8, "2019-10-07T16:00:00+11:00", appointment4, 2, Availability.BUSY);
        checkSlot(grid, schedule, 9, "2019-10-07T17:00:00+11:00", appointment4, 1, Availability.BUSY);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, grid.getSlots()));
    }
}
