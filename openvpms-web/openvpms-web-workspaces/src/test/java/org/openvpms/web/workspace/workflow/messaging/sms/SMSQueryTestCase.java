/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.sms.TestSMSBuilder;
import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link SMSQuery}.
 *
 * @author Tim Anderson
 */
public class SMSQueryTestCase extends AbstractSMSQueryTest {

    /**
     * The SMS factory.
     */
    @Autowired
    private TestSMSFactory factory;

    /**
     * Creates a new query.
     *
     * @param context the context
     * @return a new query
     */
    @Override
    protected AbstractSMSQuery createQuery(Context context) {
        SMSQuery query = new SMSQuery(new DefaultLayoutContext(context, new HelpContext("foo", null)));
        query.getComponent();
        return query;
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if {@code true} save the object, otherwise don't save it
     * @return the new object
     */
    @Override
    protected Act createObject(String value, boolean save) {
        TestSMSBuilder builder = newSMS(value);
        return builder.build(save);
    }

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    @Override
    protected String getUniqueValue() {
        String value = getUniqueValue("Z");
        return value.length() <= 30 ? value : value.substring(0, 30);
    }

    /**
     * Creates an SMS linked to a practice location.
     *
     * @param contactName the recipient name
     * @param location    the practice location. May be {@code null}
     * @return the SMS
     */
    @Override
    protected Act createSMS(String contactName, Party location) {
        return newSMS(contactName)
                .location(location)
                .build();
    }

    /**
     * Returns a partially populated SMS.
     *
     * @param recipientName the SMS recipient name
     * @return the SMS builder
     */
    private TestSMSBuilder newSMS(String recipientName) {
        return factory.newSMS()
                .recipient(TestHelper.createCustomer("foo", recipientName, true))
                .phone("12345678")
                .message("test message");
    }
}
