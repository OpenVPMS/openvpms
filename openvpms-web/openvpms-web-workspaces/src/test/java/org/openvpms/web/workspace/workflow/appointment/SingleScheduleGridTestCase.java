/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link SingleScheduleGrid}.
 *
 * @author Tim Anderson
 */
public class SingleScheduleGridTestCase extends AbstractAppointmentGridTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;

    /**
     * The roster service.
     */
    @Autowired
    private RosterService rosterService;

    /**
     * Tests a 9am to 5pm grid.
     */
    @Test
    public void test9to5ScheduleGrid() {
        Entity schedule = createSchedule(15, "09:00", "17:00"); // 9am-5pm schedule, 15 min slots
        Date date = getDate("2019-10-07");

        Act appointment1 = createAppointment("2019-10-07 09:00", "2019-10-07 09:15", schedule);
        Act appointment2 = createAppointment("2019-10-07 09:30", "2019-10-07 10:00", schedule);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        ScheduleEvents events = getScheduleEvents(appointment1, appointment2);
        SingleScheduleGrid grid = new SingleScheduleGrid(scheduleView, date, schedule, events, rules, rosterService);
        assertEquals(32, grid.getSlots());
        checkSlot(grid, schedule, 0, "2019-10-07 09:00:00", appointment1, 1, Availability.BUSY);
        checkSlot(grid, schedule, 1, "2019-10-07 09:15:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 2, "2019-10-07 09:30:00", appointment2, 2, Availability.BUSY);
        checkSlot(grid, schedule, 3, "2019-10-07 09:45:00", appointment2, 1, Availability.BUSY);
        checkSlot(grid, schedule, 4, "2019-10-07 10:00:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 31, "2019-10-07 16:45:00", null, 0, Availability.FREE);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, 32));
    }

    /**
     * Verifies that if an appointment is outside the schedule times, slots are added.
     */
    @Test
    public void testAppointmentsOutsideOfScheduleTimes() {
        Entity schedule = createSchedule(30, "09:00", "17:00"); // 9am-5pm schedule, 30 min slots
        Date date = getDate("2019-10-07");
        Act appointment1 = createAppointment("2019-10-07 08:30", "2019-10-07 09:30", schedule);
        Act appointment2 = createAppointment("2019-10-07 16:30", "2019-10-07 17:30", schedule);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        ScheduleEvents events = getScheduleEvents(appointment1, appointment2);
        SingleScheduleGrid grid = new SingleScheduleGrid(scheduleView, date, schedule, events, rules, rosterService);

        assertEquals(18, grid.getSlots());
        checkSlot(grid, schedule, 0, "2019-10-07 08:30", appointment1, 2, Availability.BUSY);
        checkSlot(grid, schedule, 1, "2019-10-07 09:00", appointment1, 1, Availability.BUSY);
        checkSlot(grid, schedule, 2, "2019-10-07 09:30", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 15, "2019-10-07 16:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 16, "2019-10-07 16:30", appointment2, 2, Availability.BUSY);
        checkSlot(grid, schedule, 17, "2019-10-07 17:00", appointment2, 1, Availability.BUSY);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, 19));
    }

    /**
     * Tests the behaviour of appointments that don't fall on slot boundaries.
     */
    @Test
    public void testAppointmentsNotOnSlotBoundaries() {
        Entity schedule = createSchedule(60, "09:00", "17:00"); // 9am-5pm schedule, 1 hour slots
        Date date = getDate("2019-10-07");
        Act appointment1 = createAppointment("2019-10-07 08:30", "2019-10-07 09:30", schedule);
        Act appointment2 = createAppointment("2019-10-07 11:00", "2019-10-07 11:15", schedule);
        Act appointment3 = createAppointment("2019-10-07 12:30", "2019-10-07 13:00", schedule);
        Act appointment4 = createAppointment("2019-10-07 16:45", "2019-10-07 17:15", schedule);

        Entity scheduleView = schedulingFactory.createScheduleView(schedule);
        ScheduleEvents events = getScheduleEvents(appointment1, appointment2, appointment3, appointment4);
        SingleScheduleGrid grid = new SingleScheduleGrid(scheduleView, date, schedule, events, rules, rosterService);

        assertEquals(10, grid.getSlots());
        checkSlot(grid, schedule, 0, "2019-10-07 08:00", appointment1, 2, Availability.BUSY);
        checkSlot(grid, schedule, 1, "2019-10-07 09:00", appointment1, 1, Availability.BUSY);
        checkSlot(grid, schedule, 2, "2019-10-07 10:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 3, "2019-10-07 11:00", appointment2, 1, Availability.BUSY);
        checkSlot(grid, schedule, 4, "2019-10-07 12:00", appointment3, 1, Availability.BUSY);
        checkSlot(grid, schedule, 5, "2019-10-07 13:00", null, 0, Availability.FREE);
        checkSlot(grid, schedule, 8, "2019-10-07 16:00", appointment4, 2, Availability.BUSY);
        checkSlot(grid, schedule, 9, "2019-10-07 17:00", appointment4, 1, Availability.BUSY);

        // invalid slots
        Schedule first = grid.getSchedules().get(0);
        assertEquals(first.getSchedule(), schedule);
        assertNull(grid.getEvent(first, -1));
        assertNull(grid.getEvent(first, 10));
    }
}
