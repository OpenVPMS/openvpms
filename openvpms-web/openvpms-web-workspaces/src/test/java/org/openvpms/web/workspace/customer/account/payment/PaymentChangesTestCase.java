/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.payment;

import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PaymentChanges} class.
 *
 * @author Tim Anderson
 */
public class PaymentChangesTestCase extends ArchetypeServiceTest {

    /**
     * Verifies that the correct audit messages are generated.
     */
    @Test
    public void testMessages() {
        FinancialAct cash1 = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct cheque1 = FinancialTestHelper.createPaymentCheque(BigDecimal.TEN);
        FinancialAct credit1 = FinancialTestHelper.createPaymentCredit(BigDecimal.TEN);
        FinancialAct discount1 = FinancialTestHelper.createPaymentDiscount(BigDecimal.TEN);
        FinancialAct eft1 = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        FinancialAct other1 = FinancialTestHelper.createPaymentOther(BigDecimal.TEN);
        save(cash1, cheque1, credit1, discount1, eft1, other1);

        FinancialAct cash2 = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct cheque2 = FinancialTestHelper.createPaymentCheque(BigDecimal.TEN);
        FinancialAct credit2 = FinancialTestHelper.createPaymentCredit(BigDecimal.TEN);
        FinancialAct discount2 = FinancialTestHelper.createPaymentDiscount(BigDecimal.TEN);
        FinancialAct eft2 = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        FinancialAct other2 = FinancialTestHelper.createPaymentOther(BigDecimal.TEN);

        checkMessage(cash1, cheque2, "Cash", "Cheque");
        checkMessage(cheque1, credit2, "Cheque", "Credit Card");
        checkMessage(credit1, discount2, "Credit Card", "Discount");
        checkMessage(discount1, eft2, "Discount", "EFT");
        checkMessage(eft1, other2, "EFT", "Other");
        checkMessage(other1, cash2, "Other", "Cash");
    }

    /**
     * Verifies that only persistent changes are recorded  i.e. intermediate unsaved acts aren't recorded.
     */
    @Test
    public void testOnlyPersistentChangesRecorded() {
        PaymentChanges changes1 = new PaymentChanges(getArchetypeService());
        assertNull(changes1.getAuditMessage("foo", new Date()));

        FinancialAct cash1 = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct cheque1 = FinancialTestHelper.createPaymentCheque(BigDecimal.TEN);
        FinancialAct credit1 = FinancialTestHelper.createPaymentCredit(BigDecimal.TEN);
        FinancialAct discount1 = FinancialTestHelper.createPaymentDiscount(BigDecimal.TEN);
        FinancialAct ef1 = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        FinancialAct other1 = FinancialTestHelper.createPaymentOther(BigDecimal.TEN);
        List<FinancialAct> acts = FinancialTestHelper.createPayment(
                TestHelper.createCustomer(), TestHelper.createTill(), ActStatus.POSTED,
                cash1, cheque1, credit1, discount1, ef1, other1);
        save(acts);
        FinancialAct payment = acts.get(0);
        checkEquals(BigDecimal.valueOf(60), payment.getTotal());

        // verify that only the last change to a persistent act is audited, i.e. intermediate unsaved acts
        // aren't recorded
        FinancialAct discount2 = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct other2 = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct eft2 = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        changes1.addChange(cash1, discount2);
        changes1.addChange(discount2, other2);
        changes1.addChange(other2, eft2);

        Date datetime = TestHelper.getDatetime("2020-02-13 12:30:00");
        String message = changes1.getAuditMessage("admin", datetime);
        assertEquals(DateFormatter.formatDateTime(datetime) + " admin Cash ($10.00) ⇒ EFT", message);

        // verify the audit information is cleared after each call
        assertNull(changes1.getAuditMessage("foo", new Date()));
    }

    /**
     * Verifies that multiple changes are recorded.
     */
    @Test
    public void testMultipleChanges() {
        PaymentChanges changes = new PaymentChanges(getArchetypeService());
        FinancialAct cash = FinancialTestHelper.createPaymentCash(BigDecimal.TEN);
        FinancialAct cheque = FinancialTestHelper.createPaymentCheque(BigDecimal.ONE);
        FinancialAct credit = FinancialTestHelper.createPaymentCredit(BigDecimal.TEN);
        List<FinancialAct> acts = FinancialTestHelper.createPayment(
                TestHelper.createCustomer(), TestHelper.createTill(), ActStatus.POSTED, cash, cheque, credit);
        save(acts);

        FinancialAct discount = FinancialTestHelper.createPaymentDiscount(BigDecimal.TEN);
        FinancialAct other = FinancialTestHelper.createPaymentOther(BigDecimal.ONE);
        FinancialAct eft = FinancialTestHelper.createPaymentEFT(BigDecimal.TEN);
        changes.addChange(cash, discount);
        changes.addChange(cheque, other);
        changes.addChange(credit, eft);

        Date datetime = TestHelper.getDatetime("2020-02-13 12:30:00");
        String message = changes.getAuditMessage("admin", datetime);
        String date = DateFormatter.formatDateTime(datetime);
        assertEquals(date + " admin Cash ($10.00) ⇒ Discount\n" +
                     date + " admin Cheque ($1.00) ⇒ Other\n" +
                     date + " admin Credit Card ($10.00) ⇒ EFT",
                     message);

        // verify the audit information is cleared after each call
        assertNull(changes.getAuditMessage("foo", new Date()));
    }

    /**
     * Verifies a that {@link PaymentChanges#getAuditMessage(String, Date)} returns the expected message.
     *
     * @param oldPayment the old payment
     * @param newPayment the new payment
     * @param from       the expected from payment name
     * @param to         the expected to payment name
     */
    private void checkMessage(FinancialAct oldPayment, FinancialAct newPayment, String from, String to) {
        PaymentChanges changes = new PaymentChanges(getArchetypeService());
        changes.addChange(oldPayment, newPayment);

        Date datetime = TestHelper.getDatetime("2020-02-13 12:30:00");
        String message = changes.getAuditMessage("admin", datetime);
        String date = DateFormatter.formatDateTime(datetime);

        String expected = date + " admin " + from + " ("
                          + NumberFormatter.formatCurrency(oldPayment.getTotal()) + ") ⇒ " + to;
        assertEquals(expected, message);
    }

}