/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Button;
import org.junit.Test;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCounterSaleBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.prefs.UserPreferences;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.IMObjectProperty;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.openvpms.web.test.EchoTestHelper.fireButton;

/**
 * Tests the {@link ChargeItemRelationshipCollectionEditor}.
 *
 * @author Tim Anderson
 */
public class ChargeItemRelationshipCollectionEditorTestCase extends AbstractAppTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Verifies that the charges can be sorted by product type.
     */
    @Test
    public void testSortByProductType() {
        Entity typeA = productFactory.createProductType("A");
        Entity typeB = productFactory.createProductType("B");
        Product product1 = productFactory.createMedication(typeB);
        Product product2 = productFactory.createMedication();
        Product product3 = productFactory.createMedication(typeA);

        TestCounterSaleBuilder builder = accountFactory.newCounterSale();
        FinancialAct charge = builder.customer(customerFactory.createCustomer())
                .item().product(product1).add()
                .item().product(product2).add()
                .item().product(product3).add()
                .build();

        FinancialAct item1 = builder.getItem(product1); // type B
        FinancialAct item2 = builder.getItem(product2); // no product type
        FinancialAct item3 = builder.getItem(product3); // type A

        // set up the context
        LocalContext context = new LocalContext();
        context.setPractice(practiceFactory.newPractice().build(false));
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));

        // set up preferences to short the product type column
        UserPreferences preferences = (UserPreferences) layoutContext.getPreferences();
        preferences.initialise(userFactory.createUser());
        preferences.setPreference(PreferenceArchetypes.CHARGE,
                                  ChargeItemRelationshipCollectionEditor.SHOW_PRODUCT_TYPE, true);

        // create the editor
        CollectionProperty items = new IMObjectProperty(charge, getBean(charge).getNode("items"));
        ChargeItemRelationshipCollectionEditor editor
                = new ChargeItemRelationshipCollectionEditor(items, charge, layoutContext);
        editor.getComponent();

        // get the Product Type sort button
        Button sort = EchoTestHelper.getSortButton(editor.getTable().getTable(), "Product Type");

        ChargeItemTableModel<Act> model = editor.getModel();

        // now sort in ascending order
        fireButton(sort);
        List<?> objects1 = model.getObjects();
        assertEquals(3, objects1.size());
        assertEquals(item2, objects1.get(0)); // no product type
        assertEquals(item3, objects1.get(1)); // type A
        assertEquals(item1, objects1.get(2)); // type B

        // now sort in descending order
        fireButton(sort);
        List<?> objects2 = model.getObjects();
        assertEquals(3, objects2.size());
        assertEquals(item1, objects2.get(0)); // type B
        assertEquals(item3, objects2.get(1)); // type A
        assertEquals(item2, objects2.get(2)); // no product type
    }
}
