/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.WindowPane;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.AbstractTestPaymentRefundBuilder;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.error.DialogErrorHandler;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.account.payment.AdminCustomerPaymentEditDialog;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Tests the {@link CustomerPaymentEditDialog}.
 *
 * @author Tim Anderson
 */
public class CustomerPaymentEditDialogTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Sets up the test case.
     */
    @Override
    public void setUp() {
        super.setUp();

        // register a DialogErrorHandler so errors display in an ErrorDialog
        ErrorHandler.setInstance(new DialogErrorHandler());
    }

    /**
     * Verifies that if a saved payment/refund is IN_PROGRESS and the user cancels editing, they are prompted.
     * <p/>
     * No prompting occurs for ON_HOLD, or POSTED statuses.<br/>
     * Note: POSTED acts can be administratively edited (although this is done via
     * {@link AdminCustomerPaymentEditDialog}).
     */
    @Test
    public void testConfirmOnCancel() {
        FinancialAct payment1 = createCashPayment(FinancialActStatus.IN_PROGRESS);
        checkConfirmOnCancel(payment1, true);

        FinancialAct payment2 = createCashPayment(FinancialActStatus.ON_HOLD);
        checkConfirmOnCancel(payment2, false);

        FinancialAct payment3 = createCashPayment(FinancialActStatus.POSTED);
        checkConfirmOnCancel(payment3, false);

        FinancialAct refund1 = createCashRefund(FinancialActStatus.IN_PROGRESS);
        checkConfirmOnCancel(refund1, true);

        FinancialAct refund2 = createCashRefund(FinancialActStatus.ON_HOLD);
        checkConfirmOnCancel(refund2, false);

        FinancialAct refund3 = createCashRefund(FinancialActStatus.POSTED);
        checkConfirmOnCancel(refund3, false);
    }

    /**
     * Verifies that if a payment/refund with only EFT items is IN_PROGRESS and the user cancels editing, they are
     * prompted to delete it.<br/>
     * Clicking YES deletes the payment/refund and closes the dialog.<br/>
     * ON_HOLD and POSTED transactions are not deleted.
     * <p/>
     * Note: POSTED acts can be administratively edited (although this is done via
     * {@link AdminCustomerPaymentEditDialog}).
     */
    @Test
    public void testEFTConfirmDeleteOnCancel() {
        FinancialAct payment1 = createEFTPayment(FinancialActStatus.IN_PROGRESS);
        checkEFTConfirmDeleteOnCancel(payment1, true);

        FinancialAct payment2 = createEFTPayment(FinancialActStatus.ON_HOLD);
        checkEFTConfirmDeleteOnCancel(payment2, false);

        FinancialAct payment3 = createEFTPayment(FinancialActStatus.POSTED);
        checkEFTConfirmDeleteOnCancel(payment3, false);

        FinancialAct refund1 = createEFTRefund(FinancialActStatus.IN_PROGRESS);
        checkEFTConfirmDeleteOnCancel(refund1, true);

        FinancialAct refund2 = createEFTRefund(FinancialActStatus.ON_HOLD);
        checkEFTConfirmDeleteOnCancel(refund2, false);

        FinancialAct refund3 = createEFTRefund(FinancialActStatus.POSTED);
        checkEFTConfirmDeleteOnCancel(refund3, false);
    }

    /**
     * Verifies that if a payment/refund with only EFT items is IN_PROGRESS and the user cancels editing,
     * they are prompted to delete it.<br/>
     * Clicking NO retains the payment/refund and closes the dialog.<br/>
     * ON_HOLD and POSTED transactions do not prompt to delete.
     * <p/>
     * Note: POSTED acts can be administratively edited (although this is done via
     * {@link AdminCustomerPaymentEditDialog}).
     */
    @Test
    public void testEFTRejectDeleteOnCancel() {
        FinancialAct payment1 = createEFTPayment(FinancialActStatus.IN_PROGRESS);
        checkEFTRejectDeleteOnCancel(payment1, true);

        FinancialAct payment2 = createEFTPayment(FinancialActStatus.ON_HOLD);
        checkEFTRejectDeleteOnCancel(payment2, false);

        FinancialAct payment3 = createEFTPayment(FinancialActStatus.POSTED);
        checkEFTRejectDeleteOnCancel(payment3, false);

        FinancialAct refund1 = createEFTRefund(FinancialActStatus.IN_PROGRESS);
        checkEFTRejectDeleteOnCancel(refund1, true);

        FinancialAct refund2 = createEFTRefund(FinancialActStatus.ON_HOLD);
        checkEFTRejectDeleteOnCancel(refund2, false);

        FinancialAct refund3 = createEFTRefund(FinancialActStatus.POSTED);
        checkEFTRejectDeleteOnCancel(refund3, false);
    }

    /**
     * Verifies that if a payment/refund with only EFT items is IN_PROGRESS and the user cancels editing, they are
     * prompted to delete it.<br/>
     * If another user has already deleted it, the dialog closes without error.
     */
    @Test
    public void testEFTAlreadyDeletedOnCancel() {
        FinancialAct payment = createEFTPayment(FinancialActStatus.IN_PROGRESS);
        checkEFTAlreadyDeletedOnCancel(payment);

        FinancialAct refund = createEFTPayment(FinancialActStatus.IN_PROGRESS);
        checkEFTAlreadyDeletedOnCancel(refund);
    }

    /**
     * Verifies that if a payment/refund with only EFT items is IN_PROGRESS and the user cancels editing, they are
     * prompted to delete it.<br/>
     * If another user has already modified it, an error is displayed and the editor is reloaded.
     */
    @Test
    public void testEFTDeleteOnCancelButModifiedByAnotherUser() {
        FinancialAct payment1 = createEFTPayment(FinancialActStatus.IN_PROGRESS);

        LayoutContext context = createLayoutContext();
        CustomerPaymentEditor editor = createEditor(payment1, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();

        // simulate another user updating the payment
        FinancialAct updated = accountFactory.updatePayment(get(payment1))
                .eft(100)
                .build();
        assertTrue(updated.getVersion() > payment1.getVersion());

        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);

        String confirmation = "This " + getDisplayName(payment1)
                              + " has been saved.\n\nDo you want to delete it?";
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel", confirmation, ConfirmationDialog.YES_ID);
        assertFalse(closed.isTrue());

        findMessageDialogAndFireButton(ErrorDialog.class, "Delete failed",
                                       "The " + getDisplayName(payment1) + " could not be deleted. "
                                       + "It may have been changed by another user.", CustomerPaymentEditDialog.OK_ID);

        // verify a new editor has been created
        AbstractCustomerPaymentEditor newEditor = dialog.getEditor();
        assertNotSame(editor, newEditor);

        // with the same version as the updated instance
        assertEquals(updated, newEditor.getObject());
        assertEquals(updated.getVersion(), newEditor.getObject().getVersion());

        // try deleting again. This time it should work
        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel", confirmation, ConfirmationDialog.YES_ID);
        assertNull(get(payment1));
        assertTrue(closed.isTrue());
    }

    /**
     * Verifies that if an error occurs initiating EFT payment, the editor reloads.
     */
    @Test
    public void testEditorReloadedOnEFTError() {
        // check payments for apply and OK
        checkEditorReloadedOnEFTError(accountFactory.newPayment(), CustomerPaymentEditDialog.APPLY_ID);
        checkEditorReloadedOnEFTError(accountFactory.newPayment(), CustomerPaymentEditDialog.OK_ID);

        // check refunds for apply and OK
        checkEditorReloadedOnEFTError(accountFactory.newRefund(), CustomerPaymentEditDialog.APPLY_ID);
        checkEditorReloadedOnEFTError(accountFactory.newRefund(), CustomerPaymentEditDialog.OK_ID);
    }

    /**
     * Verifies that if a saved payment/refund is IN_PROGRESS and the user cancels editing, they are prompted.
     * <p/>
     * No prompting occurs for ON_HOLD, or POSTED statuses.<br/>
     * Note: POSTED acts can be administratively edited (although this is done via
     * {@link AdminCustomerPaymentEditDialog}).
     */
    private void checkConfirmOnCancel(FinancialAct act, boolean prompt) {
        LayoutContext context = createLayoutContext();
        CustomerPaymentEditor editor = createEditor(act, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();
        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);
        if (prompt) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel",
                                           "This " + getDisplayName(act) + " was saved but has not been finalised.\n\n"
                                           + "Cancel editing?", ConfirmationDialog.YES_ID);
        } else {
            // no confirmation should be displayed, and the act shouldn't be deleted
            assertNull(findWindowPane(ConfirmationDialog.class));
        }
        assertNotNull(get(act));
        assertTrue(closed.isTrue());
    }


    /**
     * Verifies that if a payment/refund with only EFT items is IN_PROGRESS and the user cancels editing,
     * they are prompted to delete it.<br/>
     * Clicking YES deletes the payment/refund and closes the dialog.
     *
     * @param act    the payment/refund
     * @param prompt if {@code true} expect a prompt, otherwise no prompt should be displayed
     */
    private void checkEFTConfirmDeleteOnCancel(FinancialAct act, boolean prompt) {
        LayoutContext context = createLayoutContext();
        CustomerPaymentEditor editor = createEditor(act, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();
        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);
        if (prompt) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel",
                                           "This " + getDisplayName(act)
                                           + " has been saved.\n\nDo you want to delete it?",
                                           ConfirmationDialog.YES_ID);
            assertNull(get(act));
        } else {
            // no confirmation should be displayed, and the act shouldn't be deleted
            assertNull(findWindowPane(ConfirmationDialog.class));
            assertNotNull(get(act));
        }
        assertTrue(closed.isTrue());
    }

    /**
     * Verifies that if a payment/refund is IN_PROGRESS and the user cancels editing, they are prompted to delete it.
     * Clicking NO retains the payment/refund and closes the dialog.
     *
     * @param act    the payment/refund
     * @param prompt if {@code true} expect a prompt, otherwise no prompt should be displayed
     */
    private void checkEFTRejectDeleteOnCancel(FinancialAct act, boolean prompt) {
        LayoutContext context = createLayoutContext();
        CustomerPaymentEditor editor = createEditor(act, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();
        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);
        if (prompt) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel",
                                           "This " + getDisplayName(act)
                                           + " has been saved.\n\nDo you want to delete it?",
                                           ConfirmationDialog.NO_ID);
        } else {
            // no confirmation should be displayed
            assertNull(findWindowPane(ConfirmationDialog.class));
        }
        assertNotNull(get(act));
        assertTrue(closed.isTrue());
    }

    /**
     * Verifies that if a payment/refund is IN_PROGRESS and the user cancels editing, they are prompted to delete it.
     * If another user has already deleted it, the dialog closes without error.
     *
     * @param act the payment/refund
     */
    private void checkEFTAlreadyDeletedOnCancel(FinancialAct act) {
        LayoutContext context = createLayoutContext();
        CustomerPaymentEditor editor = createEditor(act, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();

        fireDialogButton(dialog, CustomerPaymentEditDialog.CANCEL_ID);

        // simulate another user deleting the act
        remove(act);
        assertNull(get(act));

        findMessageDialogAndFireButton(ConfirmationDialog.class, "Cancel",
                                       "This " + getDisplayName(act)
                                       + " has been saved.\n\nDo you want to delete it?",
                                       ConfirmationDialog.YES_ID);
        assertTrue(closed.isTrue());

        // can't be re-saved
        assertNull(get(act));

        // no other dialogs should be displayed
        assertNull(EchoTestHelper.findWindowPane(WindowPane.class));
    }

    /**
     * Verifies that if an error occurs initiating EFT payment, the editor reloads.
     *
     * @param builder the payment/refund builder
     * @param button  the dialog button to press
     */
    private void checkEditorReloadedOnEFTError(AbstractTestPaymentRefundBuilder<?> builder, String button) {
        Entity till = practiceFactory.createTill();
        Party location = practiceFactory.newLocation()
                .tills(till)
                .build();
        FinancialAct act = builder.customer(customerFactory.createCustomer())
                .location(location)
                .till(till)
                .eft().amount(10).addTransaction(EFTPOSTransactionStatus.PENDING,
                                                 practiceFactory.createEFTPOSTerminal()).add()
                .status(FinancialActStatus.IN_PROGRESS)
                .build();
        List<FinancialAct> items = builder.getItems();
        assertEquals(1, items.size());
        FinancialAct item = items.get(0);

        LayoutContext context = createLayoutContext();
        context.getContext().setLocation(location);
        CustomerPaymentEditor editor = createEditor(act, context);

        MutableBoolean closed = new MutableBoolean();
        CustomerPaymentEditDialog dialog = new CustomerPaymentEditDialog(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                closed.setTrue();
            }
        });
        dialog.show();

        // remove the item, to trigger failure
        remove(item);

        fireDialogButton(dialog, button);
        String displayName = getDisplayName(act);
        findMessageDialogAndFireButton(
                ErrorDialog.class, "Failed to save " + displayName,
                "The " + displayName + " could not be saved. It may have been changed by another user.\n\n" +
                "Your changes have been reverted.", ErrorDialog.OK_ID);

        // editor should still be open
        assertFalse(closed.isTrue());

        // verify a new editor has been created
        AbstractCustomerPaymentEditor newEditor = dialog.getEditor();
        assertNotSame(editor, newEditor);
    }

    /**
     * Creates an edit layout context.
     *
     * @return a new layout context
     */
    private LayoutContext createLayoutContext() {
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        context.setEdit(true);
        return context;
    }

    /**
     * Creates a new editor.
     *
     * @param act     the act to edit
     * @param context the layout context
     * @return a new editor
     */
    private CustomerPaymentEditor createEditor(FinancialAct act, LayoutContext context) {
        CustomerPaymentEditor editor = new CustomerPaymentEditor(act, null, context);
        editor.getComponent();
        return editor;
    }

    /**
     * Creates a new payment with a single cash item.
     *
     * @param status the status
     * @return a new payment
     */
    private FinancialAct createCashPayment(String status) {
        return accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .cash(10)
                .status(status)
                .build();
    }

    /**
     * Creates a new payment with a single EFT item.
     *
     * @param status the status
     * @return a new payment
     */
    private FinancialAct createEFTPayment(String status) {
        return accountFactory.newPayment()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .eft(10)
                .status(status)
                .build();
    }

    /**
     * Creates a new refund with a single cash item.
     *
     * @param status the status
     * @return a new refund
     */
    private FinancialAct createCashRefund(String status) {
        return accountFactory.newRefund()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .cash(10)
                .status(status)
                .build();
    }

    /**
     * Creates a new refund with a single EFT item.
     *
     * @param status the status
     * @return a new refund
     */
    private FinancialAct createEFTRefund(String status) {
        return accountFactory.newRefund()
                .customer(customerFactory.createCustomer())
                .till(practiceFactory.createTill())
                .eft(10)
                .status(status)
                .build();
    }
}
