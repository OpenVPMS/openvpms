/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderRule;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.internal.i18n.SMSMessages;
import org.openvpms.sms.internal.message.OutboundMessageBuilderImpl;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.message.OutboundMessageBuilder;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link ReminderSMSProcessor}.
 *
 * @author Tim Anderson
 */
public class ReminderSMSProcessorTestCase extends AbstractPatientReminderProcessorTest<SMSReminders> {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The test document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The reminder processor.
     */
    private ReminderSMSProcessor processor;

    /**
     * The SMS service.
     */
    private SMSService smsService;

    /**
     * Constructs a {@link ReminderSMSProcessorTestCase}.
     */
    public ReminderSMSProcessorTestCase() {
        super(ContactArchetypes.PHONE);
    }

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        Entity smsTemplate = reminderFactory.createSMSTemplate(
                "XPATH",
                "concat($patient.name, ' is due for a vaccination at ', $location.name, '.', $nl, " +
                "'Please contact us on ', party:getTelephone($location), ' to make an appointment')");
        Entity documentTemplate = reminderFactory.newVaccinationVaccinationReminderTemplate()
                .smsTemplate(smsTemplate)
                .build();
        reminderFactory.updateReminderType(reminderType)
                .newCount()
                .count(0)
                .interval(0, DateUnits.WEEKS)
                .template(documentTemplate)
                .newRule().sms().sendTo(ReminderRule.SendTo.ANY).add()
                .add()
                .build();

        smsService = mock(SMSService.class);
        when(smsService.isEnabled()).thenReturn(true);
        when(smsService.getMaxParts()).thenReturn(1);
        OutboundMessageBuilder builder = new OutboundMessageBuilderImpl(getArchetypeService(), domainService);
        when(smsService.getOutboundMessageBuilder()).thenReturn(builder);
        processor = createProcessor(newReminderConfiguration().build(false));
    }

    /**
     * Verifies that reminders are sent to the REMINDER contact.
     */
    @Test
    public void testReminderContact() {
        Contact sms1 = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        Contact sms2 = contactFactory.newPhone().phone("2").sms().build();
        Contact sms3 = contactFactory.newPhone().phone("3").sms().build();
        customer.addContact(sms1);
        customer.addContact(sms2);
        customer.addContact(sms3);

        checkSend(null, "1", "Spot is due for a vaccination at Vets R Us.\n" +
                             "Please contact us on 9123 4567 to make an appointment");
    }

    /**
     * Verifies that SMS can be sent to a contact different to the default.
     */
    @Test
    public void testOverrideContact() {
        Contact sms1 = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        Contact sms2 = contactFactory.newPhone().phone("2").sms().build();
        customer.addContact(sms1);
        customer.addContact(sms2);

        checkSend(sms2, "2", "Spot is due for a vaccination at Vets R Us.\n" +
                             "Please contact us on 9123 4567 to make an appointment");
    }

    /**
     * Verifies SMS reminders when grouping by customer.
     */
    @Test
    public void testCustomerGroupedReminder() {
        Party patient2 = patientFactory.createPatient(customer);
        Entity reminderType2 = reminderFactory.createReminderType("Checkup");
        Date tomorrow = DateRules.getTomorrow();
        Act item1 = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder1 = createReminder(tomorrow, patient, reminderType, item1);
        Act item2 = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder2 = createReminder(tomorrow, patient2, reminderType2, item2);

        Contact sms = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        customer.addContact(sms);

        Entity smsTemplate = reminderFactory.createSMSTemplate(
                "XPATH",
                "concat('Hi ', $customer.firstName, ', your pets are due for ', " +
                "list:sortNamesOf(., 'reminderType', ',', ' and '), " +
                "'. Please contact us on ', party:getTelephone($location), ' to make an appointment')");
        Entity documentTemplate = documentFactory.newTemplate()
                .smsTemplate(smsTemplate)
                .build();
        Entity config = newReminderConfiguration()
                .customerTemplate(documentTemplate)
                .build();
        processor = createProcessor(config);
        ReminderEvent event1 = new ReminderEvent(reminder1, item1, patient, customer, sms);
        ReminderEvent event2 = new ReminderEvent(reminder2, item2, patient2, customer, sms);
        SMSReminders reminders = prepare(ReminderType.GroupBy.CUSTOMER, event1, event2);

        checkSendReminders(reminders, "1", "Hi J, your pets are due for Checkup and Vaccination. " +
                                           "Please contact us on 9123 4567 to make an appointment");
    }

    /**
     * Verifies SMS reminders when grouping by patient.
     */
    @Test
    public void testPatientGroupedReminder() {
        Entity reminderType2 = reminderFactory.createReminderType("Checkup");
        Date tomorrow = DateRules.getTomorrow();
        Act item1 = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder1 = createReminder(tomorrow, patient, reminderType, item1);
        Act item2 = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder2 = createReminder(tomorrow, patient, reminderType2, item2);

        Contact sms = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        customer.addContact(sms);

        Entity smsTemplate = reminderFactory.createSMSTemplate(
                "XPATH",
                "concat('Hi ', $customer.firstName, ', ', $patient.name, ' is due for ', " +
                "list:sortNamesOf(., 'reminderType', ',', ' and '), " +
                "'. Please contact us on ', party:getTelephone($location), ' to make an appointment')");
        Entity documentTemplate = documentFactory.newTemplate()
                .smsTemplate(smsTemplate)
                .build();
        Entity config = newReminderConfiguration()
                .patientTemplate(documentTemplate)
                .build();
        processor = createProcessor(config);
        ReminderEvent event1 = new ReminderEvent(reminder1, item1, patient, customer, sms);
        ReminderEvent event2 = new ReminderEvent(reminder2, item2, patient, customer, sms);
        SMSReminders reminders = prepare(ReminderType.GroupBy.PATIENT, event1, event2);

        checkSendReminders(reminders, "1", "Hi J, Spot is due for Checkup and Vaccination. " +
                                           "Please contact us on 9123 4567 to make an appointment");
    }

    /**
     * Verifies that the reminder item status is set to ERROR, when the customer no phone contact with sms enabled.
     */
    @Test
    public void testNoSMSContact() {
        Contact phone = contactFactory.newPhone().phone("1").sms(false).preferred().purposes("REMINDER").build();
        customer.addContact(phone);
        checkNoContact();
    }

    /**
     * Verifies that the reminder item status is set to ERROR, when the phone contact is incomplete.
     */
    @Test
    public void testMissingPhoneNumber() {
        Contact phone = contactFactory.newPhone().phone(null).sms().preferred().purposes("REMINDER").build();
        customer.addContact(phone);
        checkNoContact();
    }

    /**
     * Verifies that {@link ReminderSMSProcessor#failed(PatientReminders, Throwable)} updates reminders with the
     * failure message.
     */
    @Test
    public void testFailed() {
        Contact sms = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        customer.addContact(sms);

        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);
        doThrow(new SMSException(SMSMessages.noMessageText()))
                .when(smsService).send(any());

        SMSReminders reminders = prepare(item, reminder, null);
        try {
            processor.process(reminders);
            fail("Expected exception to be thrown");
        } catch (OpenVPMSException expected) {
            assertTrue(processor.failed(reminders, expected));
            checkItem(get(item), ReminderItemStatus.ERROR, "SMS-0304: Message has no text");
        }
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder type has no reminder count.
     */
    @Test
    public void testMissingReminderCount() {
        Contact phone = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        checkMissingReminderCount(phone);
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder count has no template.
     */
    @Test
    public void testMissingReminderCountTemplate() {
        Contact phone = contactFactory.newPhone().phone("1").sms().preferred().purposes("REMINDER").build();
        checkMissingReminderCountTemplate(phone);
    }

    /**
     * Returns the reminder processor.
     *
     * @return the reminder processor
     */
    @Override
    protected PatientReminderProcessor<SMSReminders> getProcessor() {
        return processor;
    }

    /**
     * Creates a PENDING reminder item for reminder count 0.
     *
     * @param send    the send date
     * @param dueDate the due date
     * @return a new reminder item
     */
    @Override
    protected Act createReminderItem(Date send, Date dueDate) {
        return reminderFactory.newSMSReminder().sendDate(send).dueDate(dueDate).build();
    }

    /**
     * Creates an SMS reminder process.
     *
     * @param config the reminder configuration
     * @return a new SMS reminder processor
     */
    private ReminderSMSProcessor createProcessor(Entity config) {
        ArchetypeService service = getArchetypeService();
        SMSTemplateEvaluator templateEvaluator = new SMSTemplateEvaluator(service, getLookupService(), null);
        ReminderSMSEvaluator evaluator = new ReminderSMSEvaluator(templateEvaluator);
        ReminderTypes reminderTypes = new ReminderTypes(service);
        SimpleSMSService simpleSMSService = new SimpleSMSService(smsService, service);
        return new ReminderSMSProcessor(simpleSMSService, evaluator, reminderTypes, practice,
                                        reminderRules, patientRules, service,
                                        new ReminderConfiguration(config, service), actionFactory);
    }

    /**
     * Sends an SMS reminder.
     *
     * @param contact the contact to use. May be {@code null}
     * @param to      the expected to address
     * @param message the expected message
     */
    private void checkSend(Contact contact, String to, String message) {
        SMSReminders reminders = prepare(contact);
        checkSendReminders(reminders, to, message);
    }

    /**
     * Sends reminders.
     *
     * @param reminders the reminders
     * @param to        the expected to address
     * @param message   the expected message
     */
    private void checkSendReminders(SMSReminders reminders, String to, String message) {
        processor.process(reminders);
        ArgumentCaptor<OutboundMessage> captor = ArgumentCaptor.forClass(OutboundMessage.class);
        verify(smsService).send(captor.capture());
        assertEquals(to, captor.getValue().getPhone());
        assertEquals(message, captor.getValue().getMessage());
        assertTrue(processor.complete(reminders));
    }

    /**
     * Prepares a reminder for send.
     *
     * @param contact the contact to use. May be {@code null}
     * @return the reminders
     */
    private SMSReminders prepare(Contact contact) {
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);
        return prepare(item, reminder, contact);
    }
}
