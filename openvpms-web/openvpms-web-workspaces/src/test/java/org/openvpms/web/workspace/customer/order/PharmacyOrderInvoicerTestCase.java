/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.order;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.order.OrderRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.hl7.TestHL7Factory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditDialog;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditor;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActItemEditor;
import org.openvpms.web.workspace.customer.charge.TestChargeEditor;
import org.openvpms.web.workspace.customer.charge.TestPharmacyOrderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.archetype.test.TestHelper.getDate;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkSavePopup;
import static org.openvpms.web.workspace.customer.order.PharmacyTestHelper.createOrder;
import static org.openvpms.web.workspace.customer.order.PharmacyTestHelper.createReturn;

/**
 * Tests the {@link PharmacyOrderInvoicer}.
 *
 * @author Tim Anderson
 */
public class PharmacyOrderInvoicerTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The HL7 factory.
     */
    @Autowired
    private TestHL7Factory hl7Factory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The order rules.
     */
    @Autowired
    private OrderRules rules;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The non-rule based archetype service.
     */
    private IArchetypeService service;

    /**
     * The context.
     */
    private Context context;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The author.
     */
    private User author;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The product.
     */
    private Product product;

    /**
     * The fixed price, including tax.
     */
    private BigDecimal fixedPriceIncTax;

    /**
     * The unit price, including tax.
     */
    private BigDecimal unitPriceIncTax;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * Sets up the test case.
     */
    @Override
    @Before
    public void setUp() {
        super.setUp();
        assertNotNull(applicationContext);
        service = applicationContext.getBean("archetypeService", IArchetypeService.class);

        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        fixedPriceIncTax = BigDecimal.valueOf(2);
        unitPriceIncTax = TEN;

        // set up a location with stock control.
        stockLocation = practiceFactory.createStockLocation();
        Party location = practiceFactory.newLocation()
                .stockControl(true)
                .stockLocation(stockLocation)
                .build();

        // create a product linked to a pharmacy
        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        product = productFactory.newMedication()
                .fixedPrice(fixedPrice)
                .unitPrice(unitPrice)
                .concentration(1)
                .newDose().minWeight(0).maxWeight(10).rate(10).quantity(1)
                .add() // add a dose to the product. This should always be overridden
                .pharmacy(pharmacy)
                .stockQuantity(stockLocation, 10)
                .build();

        clinician = userFactory.createClinician();
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
        patientFactory.createWeight(patient, ONE, WeightUnits.KILOGRAMS);

        context = new LocalContext();
        context.setPractice(getPractice());

        context.setLocation(location);
        author = userFactory.createUser();
        context.setUser(author);
        context.setClinician(clinician);
        context.setPatient(patient);
        context.setCustomer(customer);
        new AuthenticationContextImpl().setUser(author); // makes author the logged-in user
    }

    /**
     * Tests charging an order that isn't linked to an existing invoice.
     * <p>
     * This should create a new invoice.
     */
    @Test
    public void testChargeUnlinkedOrder() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");
        Date startTime = getDate("2017-07-01");
        setProductPriceDates(startTime);

        FinancialAct order = createOrder(startTime, customer, patient, product, quantity, null);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canInvoice());
        assertFalse(charger.canCredit());

        Date now = new Date();
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(null, null, layoutContext);
        TestChargeEditor editor = (TestChargeEditor) dialog.getEditor();
        checkSavePopup(editor.getQueue(), PatientArchetypes.PATIENT_MEDICATION, false);
        assertTrue(SaveHelper.save(editor));
        FinancialAct charge = get(editor.getObject());
        assertTrue(charge.isA(INVOICE));
        IMObjectBean item = checkItem(charge, INVOICE_ITEM, patient, product, quantity, unitPriceIncTax,
                                      fixedPriceIncTax, tax, total, quantity, null);
        assertEquals(startTime, item.getDate("startTime")); // charge item should have same date as the order
        checkCharge(charge, customer, author, clinician, tax, total);
        assertTrue(DateRules.compareTo(charge.getActivityStartTime(), now, true) >= 0);
    }

    /**
     * Tests charging a return that isn't linked to an existing invoice.
     * <p>
     * This should create a Credit.
     */
    @Test
    public void testCreditUnlinkedReturn() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        Date startTime = getDate("2017-07-01");
        setProductPriceDates(startTime);

        FinancialAct orderReturn = createReturn(startTime, customer, patient, product, quantity, null);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(orderReturn, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canCredit());
        assertTrue(charger.canInvoice());

        Date now = new Date();
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(null, null, layoutContext);
        TestChargeEditor editor = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor));
        FinancialAct charge = get(editor.getObject());
        assertTrue(charge.isA(CustomerAccountArchetypes.CREDIT));
        IMObjectBean item = checkItem(charge, CREDIT_ITEM, patient, product, quantity, unitPriceIncTax,
                                      fixedPriceIncTax, tax, total, null, null);
        assertEquals(startTime, item.getDate("startTime")); // charge item should have same date as the order
        checkCharge(charge, customer, author, clinician, tax, total);
        assertTrue(DateRules.compareTo(charge.getActivityStartTime(), now, true) >= 0);
    }

    /**
     * Tests charging an order that is linked to an existing IN_PROGRESS invoice.
     * <p>
     * The invoice quantity should remain the same, and the receivedQuantity updated.
     */
    @Test
    public void testChargeLinkedOrderWithSameQuantity() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");
        TestChargeEditor editor1 = createInvoice(product, quantity);
        editor1.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoice = editor1.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canInvoice());
        assertFalse(charger.canCredit());

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(invoice, null, layoutContext);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertEquals(invoice, charge);
        checkItem(charge, INVOICE_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax, tax, total,
                  quantity, null);
        checkCharge(charge, customer, author, clinician, tax, total);
    }

    /**
     * Tests charging an order that is linked to an existing IN_PROGRESS invoice, with a greater quantity than that
     * invoiced.
     * <p>
     * The invoice quantity should be updated.
     */
    @Test
    public void testChargeLinkedOrderWithGreaterQuantity() {
        BigDecimal originalQty = BigDecimal.valueOf(1);

        BigDecimal newQty = BigDecimal.valueOf(2);
        BigDecimal newTax = BigDecimal.valueOf(2);
        BigDecimal newTotal = new BigDecimal("22");

        TestChargeEditor editor1 = createInvoice(product, originalQty);
        editor1.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoice = editor1.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, newQty, invoiceItem);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canInvoice());
        assertFalse(charger.canCredit());

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(invoice, null, layoutContext);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertEquals(invoice, charge);
        checkItem(charge, INVOICE_ITEM, patient, product, newQty, unitPriceIncTax, fixedPriceIncTax, newTax, newTotal,
                  newQty, null);
        checkCharge(charge, customer, author, clinician, newTax, newTotal);
    }

    /**
     * Tests charging an order that is linked to an existing POSTED invoice, with a greater quantity than that
     * invoiced.
     * <p>
     * A new invoice should be created with the difference between that invoiced and that ordered.
     */
    @Test
    public void testChargeLinkedOrderWithGreaterQuantityToPostedInvoice() {
        BigDecimal originalQty = BigDecimal.valueOf(1);
        BigDecimal newOrderQty = BigDecimal.valueOf(2);

        BigDecimal newQty = BigDecimal.valueOf(1);
        BigDecimal newTax = new BigDecimal("1.09");
        BigDecimal newTotal = BigDecimal.valueOf(12);

        TestChargeEditor editor1 = createInvoice(product, originalQty);
        editor1.setStatus(ActStatus.POSTED);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoice = editor1.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, newOrderQty, invoiceItem);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canInvoice());
        assertFalse(charger.canCredit());

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(null, null, layoutContext);
        TestChargeEditor editor = (TestChargeEditor) dialog.getEditor();
        checkSavePopup(editor.getQueue(), PatientArchetypes.PATIENT_MEDICATION, false);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertNotEquals(invoice, charge);

        // NOTE: item tax not rounded.
        checkItem(charge, INVOICE_ITEM, patient, product, newQty, unitPriceIncTax, fixedPriceIncTax,
                  new BigDecimal("1.091"), newTotal, newOrderQty, null);
        checkCharge(charge, customer, author, clinician, newTax, newTotal);
    }

    /**
     * Tests charging an order that is linked to an existing IN_PROGRESS invoice, with a lesser quantity than that
     * invoiced.
     * <p>
     * The invoice quantity should be updated.
     */
    @Test
    public void testChargeLinkedOrderWithLesserQuantityToInProgressInvoice() {
        BigDecimal originalQty = BigDecimal.valueOf(2);

        BigDecimal newQty = BigDecimal.valueOf(1);
        BigDecimal newTax = new BigDecimal("1.09");
        BigDecimal newTotal = BigDecimal.valueOf(12);

        TestChargeEditor editor1 = createInvoice(product, originalQty);
        editor1.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoice = editor1.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, newQty, invoiceItem);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertTrue(charger.canInvoice());
        assertTrue(charger.canCredit());

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(invoice, null, layoutContext);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertTrue(charge.isA(INVOICE));

        // NOTE: item tax not rounded.
        checkItem(charge, INVOICE_ITEM, patient, product, newQty, unitPriceIncTax, fixedPriceIncTax,
                  new BigDecimal("1.091"), newTotal, newQty, null);
        checkCharge(charge, customer, author, clinician, newTax, newTotal);
    }

    /**
     * Tests charging an order that is linked to an existing POSTED invoice, with a lesser quantity than that invoiced.
     * <p>
     * A new credit should be created with the difference between that invoiced and that ordered.
     */
    @Test
    public void testChargeLinkedOrderWithLesserQuantityToPostedInvoice() {
        BigDecimal originalQty = BigDecimal.valueOf(2);
        BigDecimal newQty = BigDecimal.valueOf(1);
        BigDecimal newTax = new BigDecimal("1.09");
        BigDecimal newTotal = BigDecimal.valueOf(12);

        TestChargeEditor editor1 = createInvoice(product, originalQty);
        editor1.setStatus(ActStatus.POSTED);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, newQty, invoiceItem);
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.isValid());
        assertFalse(charger.canInvoice());
        assertTrue(charger.canCredit());

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger.charge(null, null, layoutContext);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertTrue(charge.isA(CustomerAccountArchetypes.CREDIT));

        // NOTE: item tax not rounded.
        checkItem(charge, CREDIT_ITEM, patient, product, newQty, unitPriceIncTax, fixedPriceIncTax,
                  new BigDecimal("1.091"), newTotal, newQty, null);
        checkCharge(charge, customer, author, clinician, newTax, newTotal);
    }

    /**
     * Tests applying multiple linked orders to an IN_PROGRESS invoice.
     */
    @Test
    public void testMultipleLinkedOrderToInProgressInvoice() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        TestChargeEditor editor = createInvoice(product, quantity);
        editor.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor));

        FinancialAct invoice = editor.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor);

        // charge two orders
        FinancialAct order1 = createOrder(customer, patient, product, ONE, invoiceItem);
        PharmacyOrderInvoicer charger1 = new TestPharmacyOrderInvoicer(order1, clinician, rules, service);
        assertTrue(charger1.canCharge(editor));
        charger1.charge(editor);

        FinancialAct order2 = createOrder(customer, patient, product, ONE, invoiceItem);
        PharmacyOrderInvoicer charger2 = new TestPharmacyOrderInvoicer(order2, clinician, rules, service);
        assertTrue(charger2.canCharge(editor));
        charger2.charge(editor);

        assertTrue(SaveHelper.save(editor));
        checkItem(invoice, INVOICE_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax, tax, total,
                  quantity, null);
        checkCharge(invoice, customer, author, clinician, tax, total);
    }

    /**
     * Tests applying an order and return for the same quantity to an IN_PROGRESS invoice.
     * <p>
     * This will set the invoice quantity to 0, resulting in a zero total.
     */
    @Test
    public void testOrderAndReturnToInProgressInvoice() {
        TestChargeEditor editor = createInvoice(product, ONE);
        editor.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor));

        FinancialAct invoice = editor.getObject();
        FinancialAct invoiceItem = getInvoiceItem(editor);

        FinancialAct order = createOrder(customer, patient, product, ONE, invoiceItem);
        PharmacyOrderInvoicer charger1 = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger1.canCharge(editor));
        charger1.charge(editor);

        FinancialAct orderReturn = createReturn(customer, patient, product, ONE, invoiceItem);
        PharmacyOrderInvoicer charger2 = new TestPharmacyOrderInvoicer(orderReturn, clinician, rules, service);
        assertTrue(charger2.canCharge(editor));
        charger2.charge(editor);

        assertTrue(SaveHelper.save(editor));
        checkItem(invoice, INVOICE_ITEM, patient, product, ZERO, unitPriceIncTax, fixedPriceIncTax, ZERO, ZERO, ONE,
                  ONE);
        checkCharge(invoice, customer, author, clinician, ZERO, ZERO);
    }

    /**
     * Tests charging an order that is linked to an existing POSTED invoice, with a the same quantity that was
     * invoiced.
     * <p>
     * No new invoice should be created.
     */
    @Test
    public void testOrderToPostedInvoice() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        TestChargeEditor editor = createInvoice(product, quantity);
        editor.setStatus(ActStatus.POSTED);
        assertTrue(SaveHelper.save(editor));
        FinancialAct invoiceItem = getInvoiceItem(editor);

        checkItem(editor.getObject(), INVOICE_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax, tax,
                  total, null, null);

        FinancialAct order1 = createOrder(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger1 = new TestPharmacyOrderInvoicer(order1, clinician, rules, service);
        assertTrue(charger1.isValid());
        assertTrue(charger1.canInvoice());
        assertFalse(charger1.canCredit());
        assertFalse(charger1.requiresEdit());
        charger1.charge();

        checkItem(editor.getObject(), INVOICE_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax, tax,
                  total, quantity, null);

        // verify that if another order for the same item is created, editing is required, as it would exceed the
        // original order quantity.
        FinancialAct order2 = createOrder(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger2 = new TestPharmacyOrderInvoicer(order2, clinician, rules, service);
        assertTrue(charger2.isValid());
        assertTrue(charger2.canInvoice());
        assertFalse(charger2.canCredit());
        assertTrue(charger2.requiresEdit());

        // verify that a return requires editing
        FinancialAct return1 = createReturn(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger3 = new TestPharmacyOrderInvoicer(return1, clinician, rules, service);
        assertTrue(charger3.isValid());
        assertFalse(charger3.canInvoice());
        assertTrue(charger3.canCredit());
        assertTrue(charger3.requiresEdit());
    }

    /**
     * Verifies that a return for a POSTED invoice creates a new Credit.
     */
    @Test
    public void testReturnToPostedInvoice() {
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        TestChargeEditor editor1 = createInvoice(product, quantity);
        editor1.setStatus(ActStatus.IN_PROGRESS);
        assertTrue(SaveHelper.save(editor1));

        FinancialAct invoiceItem = getInvoiceItem(editor1);

        FinancialAct order = createOrder(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger1 = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger1.canCharge(editor1));
        charger1.charge(editor1);

        editor1.setStatus(ActStatus.POSTED);
        assertTrue(SaveHelper.save(editor1));

        // now return the same quantity, and verify a Credit is created
        FinancialAct orderReturn = createReturn(customer, patient, product, quantity, invoiceItem);
        PharmacyOrderInvoicer charger2 = new TestPharmacyOrderInvoicer(orderReturn, clinician, rules, service);
        assertFalse(charger2.canCharge(editor1));
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        CustomerChargeActEditDialog dialog = charger2.charge(null, null, layoutContext);
        TestChargeEditor editor2 = (TestChargeEditor) dialog.getEditor();
        assertTrue(SaveHelper.save(editor2));
        FinancialAct charge = get(editor2.getObject());
        assertTrue(charge.isA(CustomerAccountArchetypes.CREDIT));
        checkItem(charge, CREDIT_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax, tax, total, null,
                  null);
        checkCharge(charge, customer, author, clinician, tax, total);
    }

    /**
     * Verifies that if an existing order is charged and the created invoice item deleted prior to being saved,
     * no cancellation is generated.
     */
    @Test
    public void testRemoveUnsavedInvoiceItemLinkedToOrder() {
        BigDecimal quantity = BigDecimal.valueOf(2);

        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        FinancialAct order = createOrder(customer, patient, product, quantity, null); // not linked to an invoice item

        // charge the order
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(order, clinician, rules, service);
        assertTrue(charger.canCharge(editor));
        charger.charge(editor);

        // remove the new item
        List<Act> items = editor.getItems().getCurrentActs();
        assertEquals(1, items.size());
        editor.getItems().remove(items.get(0));

        assertTrue(SaveHelper.save(editor));

        // verify no orders were submitted
        TestPharmacyOrderService pharmacyOrderService = editor.getPharmacyOrderService();
        assertTrue(pharmacyOrderService.getOrders().isEmpty());

        // verifies that the unsaved order is POSTED, but that the saved version is still IN_PROGRESS
        assertEquals(ActStatus.POSTED, order.getStatus());
        order = get(order);
        assertEquals(ActStatus.IN_PROGRESS, order.getStatus());
    }

    /**
     * Verifies a validation error is produced if an order or return is missing a customer.
     */
    @Test
    public void testMissingCustomer() {
        String expected = "Customer is required";
        FinancialAct act1 = createOrder(null, patient, product, ONE, null);
        checkRequired(act1, expected);

        FinancialAct act2 = createReturn(null, patient, product, ONE, null);
        checkRequired(act2, expected);
    }

    /**
     * Verifies a validation error is produced if an order or return is missing a patient.
     */
    @Test
    public void testMissingPatient() {
        String expected = "Patient is required";
        FinancialAct act1 = createOrder(customer, null, product, ONE, null);
        checkRequired(act1, expected);

        FinancialAct act2 = createReturn(customer, null, product, ONE, null);
        checkRequired(act2, expected);
    }

    /**
     * Verifies a validation error is produced if an order or return is missing a product.
     */
    @Test
    public void testMissingProduct() {
        String expected = "Product is required";
        FinancialAct act1 = createOrder(customer, patient, null, ONE, null);
        checkRequired(act1, expected);

        FinancialAct act2 = createReturn(customer, patient, null, ONE, null);
        checkRequired(act2, expected);
    }

    /**
     * Verifies a validation error is produced if an order or return has an invalid product.
     */
    @Test
    public void testInvalidProduct() {
        String expected = "Product does not exist or is not valid for this field";
        Product template = productFactory.createTemplate();
        FinancialAct act1 = createOrder(customer, patient, template, ONE, null);
        checkRequired(act1, expected);

        FinancialAct act2 = createReturn(customer, patient, template, ONE, null);
        checkRequired(act2, expected);
    }

    /**
     * Tests applying an order return to an invoice where the order returns aren't related to any invoice item
     * and there is no existing line item for the patient and product.
     * <p/>
     * These will result in a line item with a negative quantity.
     */
    @Test
    public void testApplyUnlinkedOrderReturnToInvoiceWithNoMatchingLineItem() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        BigDecimal five = BigDecimal.valueOf(5);
        FinancialAct act1 = createReturn(customer, patient, product, five, null); // not linked to an invoice item
        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        // add an instance item of the product for a different patient. This shouldn't be touched
        Party patient2 = patientFactory.createPatient(customer);
        addItem(editor, patient2, product, ONE, editor.getQueue());

        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        OrderInvoicer.Status status1 = invoicer1.getChargeStatus(editor);
        assertTrue(status1.canCharge());
        invoicer1.charge(editor);
        EchoTestHelper.findEditDialog();

        assertEquals(ActStatus.POSTED, act1.getStatus());

        assertTrue(SaveHelper.save(editor));
        FinancialAct charge1 = get(editor.getObject());
        assertTrue(charge1.isA(INVOICE));
        BigDecimal quantity = new BigDecimal("-5");
        checkItem(charge1, INVOICE_ITEM, patient, product, quantity, unitPriceIncTax, fixedPriceIncTax,
                  new BigDecimal("-4.727"), new BigDecimal("-52"),
                  null, null);
        checkItem(charge1, INVOICE_ITEM, patient2, product, ONE, unitPriceIncTax, fixedPriceIncTax,
                  new BigDecimal("1.091"), new BigDecimal("12"), null, null);
        checkCharge(charge1, customer, author, clinician, new BigDecimal("-3.64"), new BigDecimal("-40"));
    }

    /**
     * Tests applying an order return to an invoice where the order return isn't related to any invoice item, but
     * there is a line item for a matching patient and product. The existing line item quantity cannot go negative as
     * it has been saved, but can be deleted. A second line item with the remaining negative quantity will be added.
     */
    @Test
    public void testApplyUnlinkedOrderReturnToInvoiceWithExistingLineItem() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(customer, patient, product, ONE,
                                                                           fixedPrice, unitPrice, ZERO,
                                                                           ActStatus.IN_PROGRESS);
        save(acts);

        // set up some investigations linked to the invoice item, to verify they are correctly handled when
        // the item is removed
        DocumentAct investigation1 = createInvestigation();
        investigation1.setStatus(ActStatus.POSTED);
        DocumentAct investigation2 = createInvestigation();
        investigation2.setStatus(ActStatus.IN_PROGRESS);
        Document document = (Document) documentFactory.createJRXML();
        investigation2.setDocument(document.getObjectReference());
        DocumentAct investigation3 = createInvestigation();

        FinancialAct item1 = acts.get(1);
        IMObjectBean bean = getBean(item1);
        bean.addTarget("investigations", investigation1, "invoiceItems");
        bean.addTarget("investigations", investigation2, "invoiceItems");
        bean.addTarget("investigations", investigation3, "invoiceItems");
        bean.save(investigation1, investigation2, investigation3, document);

        FinancialAct invoice = acts.get(0);
        TestChargeEditor editor = createEditor(invoice);
        assertTrue(SaveHelper.save(editor));

        // create a return not linked to an invoice item and invoice it
        FinancialAct act1 = createReturn(customer, patient, product, BigDecimal.valueOf(5), null);
        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        OrderInvoicer.Status status1 = invoicer1.getChargeStatus(editor);
        assertTrue(status1.canCharge());
        invoicer1.charge(editor);

        assertEquals(ActStatus.POSTED, act1.getStatus());

        assertTrue(SaveHelper.save(editor));
        FinancialAct charge1 = get(editor.getObject());
        assertTrue(charge1.isA(INVOICE));
        BigDecimal tax = new BigDecimal("-3.818");
        BigDecimal total = new BigDecimal("-42");
        checkItem(charge1, INVOICE_ITEM, patient, product, new BigDecimal("-4"), unitPriceIncTax, fixedPriceIncTax, tax,
                  total, null, null);

        assertNull(get(item1));              // item1 should be deleted
        assertNotNull(get(investigation1));  // not deleted because it is POSTED
        assertNotNull(get(investigation2));  // not deleted because it has content
        assertNull(get(investigation3));     // deleted because it has no content
    }

    /**
     * Tests applying order returns to an invoice where the order returns aren't related to any invoice item,
     * but there is a line item with the same product and quantity.
     * <p/>
     * The line item will be deleted.
     */
    @Test
    public void testApplyUnlinkedOrderReturnToInvoiceWithExistingLineItemDeletesItemWhenQuantityFallsToZero() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        // add two line items to the invoice for the same product
        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);
        CustomerChargeActItemEditor item1 = addItem(editor, patient, product, new BigDecimal("11"), editor.getQueue());
        CustomerChargeActItemEditor item2 = addItem(editor, patient, product, ONE, editor.getQueue());

        FinancialAct act1 = createReturn(customer, patient, product, new BigDecimal(5), null);
        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        OrderInvoicer.Status status1 = invoicer1.getChargeStatus(editor);
        assertTrue(status1.canCharge());
        invoicer1.charge(editor);
        assertTrue(SaveHelper.save(editor));

        checkPosted(act1);  // verify the order return is POSTED
        FinancialAct charge1 = get(editor.getObject());
        assertTrue(charge1.isA(INVOICE));
        checkItems(charge1, 2);

        // verify item1 quantity decreased to 6
        IMObjectBean bean1 = checkItem(charge1, INVOICE_ITEM, patient, product, new BigDecimal("6"), unitPriceIncTax,
                                       fixedPriceIncTax, new BigDecimal("5.636"), new BigDecimal("62"), null, null);
        assertEquals(bean1.getObject(), item1.getObject());

        // verify item2 is unchanged
        IMObjectBean bean2 = checkItem(charge1, INVOICE_ITEM, patient, product, ONE, unitPriceIncTax,
                                       fixedPriceIncTax, new BigDecimal("1.091"), new BigDecimal("12"), null, null);
        assertEquals(bean2.getObject(), item2.getObject());

        checkCharge(charge1, customer, author, clinician, new BigDecimal("6.73"), new BigDecimal("74"));

        // now do another return of quantity 1. This should remove item2 as the quantities match
        FinancialAct act2 = createReturn(customer, patient, product, ONE, null);
        PharmacyOrderInvoicer invoicer2 = new TestPharmacyOrderInvoicer(act2, clinician, rules, service);
        invoicer2.charge(editor);
        assertTrue(SaveHelper.save(editor));
        checkPosted(act2);   // verify the order return is POSTED
        FinancialAct charge2 = get(editor.getObject());
        checkItems(charge2, 1); // 1 item left

        // verify item1 unchanged
        bean1 = checkItem(charge1, INVOICE_ITEM, patient, product, new BigDecimal("6"), unitPriceIncTax,
                          fixedPriceIncTax, new BigDecimal("5.636"), new BigDecimal("62"), null, null);
        assertEquals(item1.getObject(), bean1.getObject());

        checkCharge(charge2, customer, author, clinician, new BigDecimal("5.64"), new BigDecimal("62"));
    }

    /**
     * Verifies that when an order return that is not related to any invoice item is applied to an invoice item with a
     * minimum quantity, the invoice item is not deleted when its quantity falls to zero.
     */
    @Test
    public void testApplyUnlinkedOrderReturnsToInvoiceWithMinimumQuantity() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        // now add one of the product to the invoice, and set the minimum quantity to one
        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product, ONE, editor.getQueue());
        itemEditor.setMinimumQuantity(ONE);
        assertTrue(SaveHelper.save(editor));

        // create a return for the product, and invoice it
        FinancialAct act1 = createReturn(customer, patient, product, ONE, null); // not linked to an invoice item
        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        OrderInvoicer.Status status1 = invoicer1.getChargeStatus(editor);
        assertTrue(status1.canCharge());
        invoicer1.charge(editor);
        assertEquals(ActStatus.POSTED, act1.getStatus());

        // shouldn't be able to save the item, as its quantity is now zero
        checkEquals(ZERO, itemEditor.getQuantity());
        assertFalse(SaveHelper.save(editor));

        // set the minimum quantity to allow the invoice to save
        itemEditor.setMinimumQuantity(ZERO);
        assertTrue(SaveHelper.save(editor));
    }

    /**
     * Verifies that if a return is processed before an order for the same product, two line items appear on the
     * invoice, one -ve and one +ve. This is because orders do not apply to existing line items.
     */
    @Test
    public void testReturnAndOrderToInvoiceForSameProduct() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        // create a return for the product, and invoice it
        FinancialAct act1 = createReturn(customer, patient, product, ONE, null); // not linked to an invoice item
        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        invoicer1.charge(editor);

        FinancialAct act2 = createOrder(customer, patient, product, ONE, null); // not linked to an invoice item
        PharmacyOrderInvoicer invoicer2 = new TestPharmacyOrderInvoicer(act2, clinician, rules, service);
        invoicer2.charge(editor);

        assertEquals(ActStatus.POSTED, act1.getStatus());
        assertEquals(ActStatus.POSTED, act2.getStatus());

        checkSavePopup(editor.getQueue(), PatientArchetypes.PATIENT_MEDICATION, false);
        assertTrue(SaveHelper.save(editor));

        FinancialAct charge = editor.getObject();
        checkItems(charge, 2);
        checkItem(charge, INVOICE_ITEM, patient, product, ONE, unitPriceIncTax,
                  fixedPriceIncTax, new BigDecimal("1.091"), new BigDecimal(12), ONE, null);
        checkItem(charge, INVOICE_ITEM, patient, product, new BigDecimal(-1), unitPriceIncTax,
                  fixedPriceIncTax, new BigDecimal("-1.091"), new BigDecimal(-12), null, null);
        checkCharge(charge, customer, author, clinician, ZERO, ZERO);
    }

    /**
     * Verifies that if a return is processed after an order for the same product and quantity, the line item is removed
     * and any popups are cancelled.
     */
    @Test
    public void testOrderAndReturnToInvoiceForSameProduct() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        FinancialAct act1 = createOrder(customer, patient, product, ONE, null); // not linked to an invoice item
        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        invoicer1.charge(editor);

        assertFalse(editor.isValid()); // medication popup displayed

        // create a return for the product, and invoice it
        FinancialAct act2 = createReturn(customer, patient, product, ONE, null); // not linked to an invoice item
        PharmacyOrderInvoicer invoicer2 = new TestPharmacyOrderInvoicer(act2, clinician, rules, service);
        invoicer2.charge(editor);

        assertEquals(ActStatus.POSTED, act1.getStatus());
        assertEquals(ActStatus.POSTED, act2.getStatus());

        assertTrue(SaveHelper.save(editor));

        FinancialAct charge = editor.getObject();
        checkItems(charge, 0);
        checkCharge(charge, customer, author, clinician, ZERO, ZERO);
    }

    /**
     * Verifies that if a return is processed after an order for the same product and quantity, the line item is removed
     * and any popups are cancelled, and that this can be followed by a new order.
     */
    @Test
    public void testOrderAndReturnToInvoiceForSameProductFollowedByNewOrder() {
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal unitPrice = new BigDecimal("9.09");
        Product product = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        TestChargeEditor editor = createEditor();
        editor.setStatus(ActStatus.IN_PROGRESS);

        FinancialAct act1 = createOrder(customer, patient, product, ONE, null);
        FinancialAct act2 = createReturn(customer, patient, product, ONE, null);
        FinancialAct act3 = createOrder(customer, patient, product, ONE, null);

        PharmacyOrderInvoicer invoicer1 = new TestPharmacyOrderInvoicer(act1, clinician, rules, service);
        invoicer1.charge(editor);

        assertFalse(editor.isValid()); // medication popup displayed

        // now return the product. Popups should be cancelled and the line item deleted
        PharmacyOrderInvoicer invoicer2 = new TestPharmacyOrderInvoicer(act2, clinician, rules, service);
        invoicer2.charge(editor);

        assertTrue(editor.isValid());
        assertEquals(0, editor.getItems().getCurrentActs().size()); // no invoice items

        // now order the product again. A medication popup should be displayed
        PharmacyOrderInvoicer invoicer3 = new TestPharmacyOrderInvoicer(act3, clinician, rules, service);
        invoicer3.charge(editor);
        assertFalse(editor.isValid());

        checkSavePopup(editor.getQueue(), PatientArchetypes.PATIENT_MEDICATION, false);

        assertEquals(ActStatus.POSTED, act1.getStatus());
        assertEquals(ActStatus.POSTED, act2.getStatus());
        assertEquals(ActStatus.POSTED, act3.getStatus());

        assertTrue(SaveHelper.save(editor));

        FinancialAct charge = editor.getObject();
        checkItems(charge, 1);
        checkItem(charge, INVOICE_ITEM, patient, product, ONE, unitPriceIncTax,
                  fixedPriceIncTax, new BigDecimal("1.091"), new BigDecimal(12), ONE, null);
        checkCharge(charge, customer, author, clinician, new BigDecimal("1.09"), new BigDecimal(12));
    }

    /**
     * Creates an investigation for the patient.
     *
     * @return a new investigation
     */
    private DocumentAct createInvestigation() {
        return patientFactory.newInvestigation()
                .patient(patient)
                .investigationType(laboratoryFactory.createInvestigationType())
                .build();
    }

    /**
     * Verifies an act has POSTED status.
     *
     * @param act the act
     */
    private void checkPosted(Act act) {
        assertEquals(ActStatus.POSTED, act.getStatus());
    }

    /**
     * Verifies a charge has the expected number of items.
     *
     * @param charge the charge
     * @param count  the expected no. of items
     */
    private void checkItems(FinancialAct charge, int count) {
        assertEquals(count, getBean(charge).getValues("items").size());
    }

    /**
     * Verifies that a validation error is raised if a required field is missing.
     * <p>
     * Validation cannot occur using the archetype as as the delivery processor must be able to save incomplete/invalid
     * orders and returns.
     *
     * @param act      the order/return
     * @param expected the expected validation error
     */
    private void checkRequired(FinancialAct act, String expected) {
        PharmacyOrderInvoicer charger = new TestPharmacyOrderInvoicer(act, clinician, rules, service);
        assertFalse(charger.isValid());
        Validator validator = new DefaultValidator();
        assertFalse(charger.validate(validator));
        assertEquals(1, validator.getInvalid().size());
        Modifiable modifiable = validator.getInvalid().iterator().next();
        List<ValidatorError> errors = validator.getErrors(modifiable);
        assertEquals(1, errors.size());
        assertEquals(expected, errors.get(0).getMessage());
    }

    /**
     * Creates a new invoice in an editor.
     *
     * @param product  the product to invoice
     * @param quantity the quantity to invoice
     * @return the invoice editor
     */
    private TestChargeEditor createInvoice(Product product, BigDecimal quantity) {
        TestChargeEditor editor = createEditor();
        addItem(editor, patient, product, quantity, editor.getQueue());
        return editor;
    }

    /**
     * Creates a new invoice editor.
     *
     * @return a new editor
     */
    private TestChargeEditor createEditor() {
        FinancialAct charge = create(INVOICE, FinancialAct.class);
        return createEditor(charge);
    }

    /**
     * Creates a new charge editor.
     *
     * @param charge the charge to edit
     * @return a new editor
     */
    private TestChargeEditor createEditor(FinancialAct charge) {
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layoutContext.setEdit(true);
        TestChargeEditor editor = new TestChargeEditor(charge, layoutContext, false);
        editor.getComponent();
        return editor;
    }

    /**
     * Returns the invoice item from an invoice editor.
     *
     * @param editor the editor
     * @return the invoice item
     */
    private FinancialAct getInvoiceItem(TestChargeEditor editor) {
        List<Act> acts = editor.getItems().getCurrentActs();
        assertEquals(1, acts.size());
        return (FinancialAct) acts.get(0);
    }

    /**
     * Verifies the charge has an item with the expected details.
     *
     * @param charge           the charge
     * @param archetype        the item archetype
     * @param patient          the expected patient
     * @param product          the expected product
     * @param quantity         the expected quantity
     * @param unitPrice        the expected unit price
     * @param fixedPrice       the expected fixed price
     * @param tax              the expected tax
     * @param total            the expected total
     * @param receivedQuantity the expected received quantity
     * @param returnedQuantity the expected returned quantity. May be {@code null}
     * @return the item
     */
    private IMObjectBean checkItem(FinancialAct charge, String archetype, Party patient, Product product,
                                   BigDecimal quantity, BigDecimal unitPrice, BigDecimal fixedPrice, BigDecimal tax,
                                   BigDecimal total, BigDecimal receivedQuantity, BigDecimal returnedQuantity) {
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);

        int childActs = (charge.isA(INVOICE) && (quantity.compareTo(BigDecimal.ZERO) >= 0)) ? 1 : 0;
        // for invoices, there should be a medication act unless the quantity is negative

        IMObjectBean itemBean = checkItem(items, archetype, patient, product, quantity, unitPrice, fixedPrice, tax,
                                          total, childActs);
        if (bean.isA(INVOICE)) {
            checkEquals(receivedQuantity, itemBean.getBigDecimal("receivedQuantity"));
            checkEquals(returnedQuantity, itemBean.getBigDecimal("returnedQuantity"));
        }
        assertEquals(stockLocation, itemBean.getTarget("stockLocation"));
        return itemBean;
    }

    /**
     * Verifies an item's properties match that expected.
     *
     * @param items      the items to search
     * @param archetype  the expected item archetype
     * @param patient    the expected patient
     * @param product    the expected product
     * @param quantity   the expected quantity
     * @param unitPrice  the expected unit price
     * @param fixedPrice the expected fixed price
     * @param tax        the expected tax
     * @param total      the expected total
     * @param childActs  the expected no. of child acts
     * @return the item
     */
    private IMObjectBean checkItem(List<FinancialAct> items, String archetype, Party patient, Product product,
                                   BigDecimal quantity, BigDecimal unitPrice, BigDecimal fixedPrice, BigDecimal tax,
                                   BigDecimal total, int childActs) {
        return checkItem(items, archetype, patient, product, null, -1, author, clinician, ZERO, quantity, ZERO, unitPrice,
                         ZERO, fixedPrice, ZERO, tax, total, true, null, childActs);
    }

    /**
     * Sets the product price dates.
     *
     * @param startTime the start time
     */
    private void setProductPriceDates(Date startTime) {
        // back-date the product price from dates so that they will be charged.
        for (ProductPrice prices : product.getProductPrices()) {
            prices.setFromDate(startTime);
        }
        save(product);
    }

    private static class TestPharmacyOrderInvoicer extends PharmacyOrderInvoicer {

        /**
         * Constructs a {@link TestPharmacyOrderInvoicer}.
         *
         * @param act       the order/return act
         * @param clinician the current clinician. May be {@code null}
         * @param rules     the order rules
         * @param service   the archetype service. Must not be an instance of {@link IArchetypeRuleService}
         */
        public TestPharmacyOrderInvoicer(FinancialAct act, User clinician, OrderRules rules,
                                         IArchetypeService service) {
            super(act, clinician, rules, service);
        }

        /**
         * Creates a new {@link CustomerChargeActEditor}.
         *
         * @param charge  the charge
         * @param context the layout context
         * @return a new charge editor
         */
        @Override
        protected CustomerChargeActEditor createChargeEditor(FinancialAct charge, LayoutContext context) {
            return new TestChargeEditor(charge, context, true);
        }
    }

}
