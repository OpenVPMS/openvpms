/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.supplier.OrderStatus;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.supplier.TestSupplierFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.print.PrintDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.supplier.order.OrderCRUDWindow;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireButton;

/**
 * Tests the {@link OrderCRUDWindow} class.
 *
 * @author Tim Anderson
 */
public class OrderCRUDWindowTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The supplier factory.
     */
    @Autowired
    private TestSupplierFactory supplierFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The context.
     */
    private Context context;

    @Before
    public void setUp() {
        super.setUp();
        initErrorHandler(errors);
        practice = practiceFactory.getPractice();

        context = new LocalContext();
        context.setPractice(practice);
    }

    /**
     * Tests posting an order.
     */
    @Test
    public void testPost() {
        FinancialAct order = createOrder();
        OrderCRUDWindow window = createWindow();
        checkPost(window, order);

        // verify a dialog is displayed to print the order
        assertNotNull(findWindowPane(PrintDialog.class));

        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies ESCI orders are submitted when posted. No print dialog should be displayed.
     */
    @Test
    public void testPostESCIOrder() {
        Party stockLocation = practiceFactory.createStockLocation();
        Party supplier = supplierFactory.newSupplier()
                .addESCIConfiguration(stockLocation)
                .build();
        FinancialAct order = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .item().product(productFactory.createMedication()).quantity(10).add()
                .status(OrderStatus.IN_PROGRESS)
                .build();

        TestOrderCRUDWindow window = createWindow();
        checkPost(window, order);

        // verify the order is submitted and no print dialog is displayed
        assertTrue(window.orderSubmitted);
        assertNull(findWindowPane(PrintDialog.class));
    }

    /**
     * Verifies a dialog is displayed if a user doesn't have permissions to post a restricted order.
     */
    @Test
    public void testPostRestrictedOrder() {
        practiceFactory.updatePractice(practice)
                .restrictOrdering(true)
                .build();

        // create an order with a restricted medication
        Product product = productFactory.newMedication().drugSchedule(true).build();
        Party stockLocation = practiceFactory.createStockLocation();
        Party supplier = supplierFactory.createSupplier();
        FinancialAct order = supplierFactory.newOrder()
                .supplier(supplier)
                .stockLocation(stockLocation)
                .item().product(product).quantity(10).add()
                .status(OrderStatus.IN_PROGRESS)
                .build();

        // set up the context with a non-clinician user
        context.setUser(userFactory.createUser());
        OrderCRUDWindow window = createWindow();

        window.setObject(order);
        fireButton(window.getComponent(), OrderCRUDWindow.POST_ID);
        findMessageDialogAndFireButton(
                InformationDialog.class,
                "Ordering Restricted",
                "Finalising orders with restricted medication must be performed by a clinician.\n\n"
                + "Please have a clinician review and finalise this order.", InformationDialog.OK_ID);

        // verify the order is not posted
        FinancialAct reloaded1 = get(order);
        assertEquals(FinancialActStatus.IN_PROGRESS, reloaded1.getStatus());

        // now try again with a clinician and verify it succeeds
        context.setUser(userFactory.createClinician());
        checkPost(window, order);
    }

    /**
     * Tests posting an order.
     *
     * @param window the order window
     * @param order  the order
     */
    private void checkPost(OrderCRUDWindow window, FinancialAct order) {
        window.setObject(order);
        fireButton(window.getComponent(), OrderCRUDWindow.POST_ID);

        String displayName = getDisplayName(order);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        // verify the order has been posted
        FinancialAct reloaded = get(order);
        assertEquals(POSTED, reloaded.getStatus());
        assertEquals(reloaded, window.getObject());
        assertEquals(reloaded.getVersion(), window.getObject().getVersion());

        assertTrue(errors.isEmpty());
    }

    /**
     * Creates an order window.
     *
     * @return a new order window
     */
    private TestOrderCRUDWindow createWindow() {
        return new TestOrderCRUDWindow(context, new HelpContext("foo", null));
    }

    /**
     * Creates an order.
     *
     * @return a new order
     */
    private FinancialAct createOrder() {
        return supplierFactory.newOrder()
                .supplier(supplierFactory.createSupplier())
                .stockLocation(practiceFactory.createStockLocation())
                .item().product(productFactory.createMedication()).quantity(10).add()
                .status(OrderStatus.IN_PROGRESS)
                .build();
    }

    private static class TestOrderCRUDWindow extends OrderCRUDWindow {

        private boolean orderSubmitted;

        /**
         * Constructs a {@link TestOrderCRUDWindow}.
         *
         * @param context the context
         * @param help    the help context
         */
        public TestOrderCRUDWindow(Context context, HelpContext help) {
            super(Archetypes.create(SupplierArchetypes.ORDER, FinancialAct.class), context, help);
        }

        /**
         * Submits the order to the supplier, via ESCI.
         *
         * @param act the order
         * @return {@code true} if the order was submitted successfully
         */
        @Override
        protected boolean submitOrder(FinancialAct act) {
            orderSubmitted = true;
            return true;
        }

        /**
         * Schedule a poll of the ESCI inboxes to pick up messages.
         *
         * @param delay if {@code true} add a 30-second delay
         */
        @Override
        protected void scheduleCheckInbox(boolean delay) {
            // no-op
        }
    }
}