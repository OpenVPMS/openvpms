/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.roster;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.echo.table.Cell;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleColours;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link AreaRosterTableModel}.
 *
 * @author Tim Anderson
 */
public class AreaRosterTableModelTestCase extends AbstractAppTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Tests the {@link AreaRosterTableModel}.
     */
    @Test
    public void testGetEvent() {
        Party location = practiceFactory.createLocation();
        Entity rosterArea = schedulingFactory.createRosterArea(location);

        Date from = DateRules.getToday();
        Date to = DateRules.getDate(from, 7, DateUnits.DAYS);
        Act act = schedulingFactory.newRosterEvent()
                .startTime(from)
                .endTime(DateRules.getDate(from, 8, DateUnits.HOURS))
                .location(location)
                .schedule(rosterArea)
                .build();

        PropertySet event = createEvent(act);
        ScheduleEvents events = new ScheduleEvents(Collections.singletonList(event), 0);
        Map<Entity, ScheduleEvents> eventMap = new HashMap<>();
        eventMap.put(rosterArea, events);
        RosterEventGrid grid = new RosterEventGrid(from, to, 7, eventMap);
        AreaRosterTableModel model = new AreaRosterTableModel(grid, new LocalContext(),
                                                              Mockito.mock(ScheduleColours.class));
        assertNull(model.getEvent(new Cell(0, 0)));           // first column is the roster area
        assertEquals(event, model.getEvent(new Cell(1, 0)));  // first day
        assertNull(model.getEvent(new Cell(2, 0)));           // second day
        assertNull(model.getEvent(new Cell(7, 0)));           // last day
        assertNull(model.getEvent(new Cell(8, 0)));           // out of bounds

        assertNull(model.getEvent(new Cell(-1, -1)));         // out of bounds
        assertNull(model.getEvent(new Cell(0, 1)));           // out of bounds
        assertNull(model.getEvent(new Cell(7, 1)));           // out of bounds
    }

    /**
     * Creates an event representing a roster event.
     *
     * @param event the roster event
     * @return the event
     */
    protected PropertySet createEvent(Act event) {
        PropertySet result = new ObjectSet();
        result.set(ScheduleEvent.ACT_START_TIME, event.getActivityStartTime());
        result.set(ScheduleEvent.ACT_END_TIME, event.getActivityEndTime());
        result.set(ScheduleEvent.ACT_REFERENCE, event.getObjectReference());
        result.set(ScheduleEvent.SCHEDULE_REFERENCE, getBean(event).getTargetRef("schedule"));
        return result;
    }
}
