/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Base class for tests of concrete implementations of {@link DocumentCommunicationEditor}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDocumentCommunicationEditorTest<T extends DocumentCommunicationEditor>
        extends AbstractIMObjectEditorTest<T> {

    /**
     * Constructs an {@link AbstractDocumentCommunicationEditorTest}.
     *
     * @param type      the editor type
     * @param archetype the archetype that the editor supports
     */
    public AbstractDocumentCommunicationEditorTest(Class<T> type, String archetype) {
        super(type, archetype);
    }

    @Test
    public void testEditMessage() {
        LocalContext local = new LocalContext();
        Party customer = TestHelper.createCustomer();
        local.setCustomer(customer);
        local.setPatient(TestHelper.createPatient(customer));
        LayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));

        DocumentAct act = (DocumentAct) newObject();
        IMObjectBean bean = getBean(act);

        DocumentCommunicationEditor editor = (DocumentCommunicationEditor) createEditor(act, context);

        editor.getComponent();
        assertTrue(editor.isModified());
        assertTrue(SaveHelper.save(editor));

        assertFalse(editor.isModified());
        assertNull(editor.getMessage());
        assertNull(act.getDocument());
        assertNull(bean.getString("message"));

        String text1 = StringUtils.repeat("a", 5000);
        editor.setMessage(text1);
        assertTrue(editor.isModified());
        assertTrue(SaveHelper.save(editor));
        assertEquals(text1, bean.getString("message"));  // verify the message node is populated
        assertNull(act.getDocument());
        String text2 = text1 + "b";

        editor.setMessage(text2);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("message"));           // text should move to the document
        Reference document1 = act.getDocument();
        assertNotNull(document1);
        checkDocument(document1, text2);

        String text3 = text2 + "c";
        editor.setMessage(text3);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("message"));
        Reference document2 = act.getDocument();

        assertEquals(document1, document2);  // should update the same document
        checkDocument(document2, text3);

        // verify that when the text is shortened below the field length, the text moves to the node and the
        // document is removed
        editor.setMessage("short");
        assertTrue(SaveHelper.save(editor));
        assertNull(act.getDocument());        // verify the act no longer has any document
        assertNull(get(document2));
        assertEquals("short", bean.getString("message"));

        // now verify that putting the long text back in creates a new document
        editor.setMessage(text3);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("message"));
        Reference document3 = act.getDocument();
        assertNotNull(document3);
        checkDocument(act.getDocument(), text3);
    }

    /**
     * Verifies a document contains the expected text.
     *
     * @param documentRef the document reference
     * @param text        the expected text
     */
    private void checkDocument(Reference documentRef, String text) {
        Document document = get(documentRef, Document.class);
        assertNotNull(document);
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        assertEquals(text, handler.toString(document));
    }

}
