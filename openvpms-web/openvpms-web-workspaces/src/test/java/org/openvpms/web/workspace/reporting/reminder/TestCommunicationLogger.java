/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link CommunicationLogger} that tracks what has been logged.
 *
 * @author Tim Anderson
 */
class TestCommunicationLogger extends CommunicationLogger {

    /**
     * The logs.
     */
    private final List<Act> logs = new ArrayList<>();

    /**
     * Constructs a {@link TestCommunicationLogger}.
     *
     * @param service the archetype service
     */
    public TestCommunicationLogger(ArchetypeService service) {
        super(service);
    }

    /**
     * Returns the logs.
     *
     * @return the logs
     */
    public List<Act> getLogs() {
        return logs;
    }

    /**
     * Saves a communication log.
     *
     * @param message  the message. If this exceeds the maximum allowed characters of the node, it will be stored as a
     *                 document instead
     * @param bean     the bean wrapping the act
     * @param fileName the document filename
     */
    @Override
    protected void saveLog(String message, IMObjectBean bean, String fileName) {
        super.saveLog(message, bean, fileName);
        logs.add(bean.getObject(Act.class));
    }
}
