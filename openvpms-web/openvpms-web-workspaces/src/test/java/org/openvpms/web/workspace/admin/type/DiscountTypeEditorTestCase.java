/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.type;

import org.junit.Test;
import org.openvpms.archetype.rules.finance.discount.DiscountArchetypes;
import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link DiscountTypeEditor}.
 *
 * @author Tim Anderson
 */
public class DiscountTypeEditorTestCase extends AbstractIMObjectEditorTest<DiscountTypeEditor> {

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * Constructs a {@link DiscountTypeEditor}.
     */
    public DiscountTypeEditorTestCase() {
        super(DiscountTypeEditor.class, DiscountArchetypes.DISCOUNT_TYE);
    }

    /**
     * Tests that the <em>discountFixed</em> node cannot be set if the <em>type</em> node is {@code FIXED}.
     */
    @Test
    public void testValidation() {
        Entity type = productFactory.newDiscount().build(false);
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DiscountTypeEditor editor = new DiscountTypeEditor(type, null, context);
        editor.getComponent();
        editor.setRate(BigDecimal.TEN);

        assertTrue(editor.isValid());

        checkValidation(editor, DiscountRules.PERCENTAGE, false, null);
        checkValidation(editor, DiscountRules.PERCENTAGE, true, null);
        checkValidation(editor, DiscountRules.FIXED, false, null);
        checkValidation(editor, DiscountRules.FIXED, true,
                        "Include Fixed Amount cannot be selected for Fixed discounts");
        checkValidation(editor, DiscountRules.COST_RATE, false, null);
        checkValidation(editor, DiscountRules.COST_RATE, true, null);
    }

    /**
     * Checks validation of the discount.
     *
     * @param editor        editor
     * @param type          the discount type
     * @param discountFixed if {@code true}, discount the fixed amount
     * @param message       the expected validation error, or {@code null} if none is expected
     */
    private void checkValidation(DiscountTypeEditor editor, String type, boolean discountFixed, String message) {
        editor.setType(type);
        editor.setDiscountFixed(discountFixed);
        if (message == null) {
            assertTrue(editor.isValid());
        } else {
            assertInvalid(editor, message);
        }
    }
}
