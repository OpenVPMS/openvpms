/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientPrescriptionEditor} class.
 *
 * @author Tim Anderson
 */
public class PatientPrescriptionEditorTestCase extends AbstractIMObjectEditorTest<PatientPrescriptionEditor> {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Constructs a {@link PatientPrescriptionEditorTestCase}.
     */
    public PatientPrescriptionEditorTestCase() {
        super(PatientPrescriptionEditor.class, PatientArchetypes.PRESCRIPTION);
    }

    /**
     * Verifies the prescription expiry date is initially determined by the practice <em>prescriptionExpiryPeriod</em>.
     */
    @Test
    public void testDefaultExpiry() {
        Act act = create(PatientArchetypes.PRESCRIPTION, Act.class);
        Date start = act.getActivityStartTime();
        Party practice = practiceFactory.newPractice().prescriptionExpiryPeriod(6, DateUnits.MONTHS)
                .build(false);

        Context context = new LocalContext();
        context.setPractice(practice);

        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        PatientPrescriptionEditor editor1 = (PatientPrescriptionEditor) createEditor(act, layoutContext);

        assertEquals(DateRules.getDate(start, 6, DateUnits.MONTHS), act.getActivityEndTime());

        editor1.setPatient(patientFactory.createPatient());
        editor1.setProduct(productFactory.createMedication());
        editor1.setClinician(userFactory.createClinician());
        assertTrue(SaveHelper.save(editor1));

        // verify the expiry doesn't change after the prescription has been saved
        Date newExpiry = DateRules.getDate(start, 3, DateUnits.MONTHS);
        act.setActivityEndTime(newExpiry);
        createEditor(act, layoutContext);
        assertEquals(newExpiry, act.getActivityEndTime());
    }

    /**
     * Verifies the label is set from the medication dispensing instructions.
     */
    @Test
    public void testLabel() {
        Act act = create(PatientArchetypes.PRESCRIPTION, Act.class);
        Product product1 = productFactory.newMedication()
                .labelInstructions("the label")
                .build();
        Product product2 = productFactory.createMedication();

        PatientPrescriptionEditor editor = (PatientPrescriptionEditor) createEditor(act);
        editor.getComponent();
        editor.setProduct(product1);

        assertEquals("the label", editor.getLabel());

        // product without dispensing instructions
        editor.setProduct(product2);
        assertNull(editor.getLabel());
    }

    /**
     * Verifies that when a product is selected that has a dose, the dose for the patient is used to calculate
     * the quantity.
     */
    @Test
    public void testQuantityFromDose() {
        Act act = create(PatientArchetypes.PRESCRIPTION, Act.class);
        Party patient = patientFactory.createPatient();
        patientFactory.createWeight(patient, 5);
        Product product1 = productFactory.newMedication()
                .concentration(1)
                .newDose().weightRange(0, 2).rate(1).quantity(1).add()
                .newDose().weightRange(2, 10).rate(2).quantity(1).add()
                .build();

        PatientPrescriptionEditor editor = (PatientPrescriptionEditor) createEditor(act);
        editor.getComponent();
        editor.setPatient(patient);
        editor.setProduct(product1);
        checkEquals(10, editor.getQuantity());

        // change to a product without a dose. Should set to 1.
        Product product2 = productFactory.createMedication();
        editor.setProduct(product2);
        checkEquals(1, editor.getQuantity());
    }
}