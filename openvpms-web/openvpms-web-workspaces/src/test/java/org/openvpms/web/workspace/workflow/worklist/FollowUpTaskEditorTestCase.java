/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Tests the {@link FollowUpTaskEditor}.
 *
 * @author Tim Anderson
 */
public class FollowUpTaskEditorTestCase extends AbstractAppTest {

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Tests the {@link FollowUpTaskEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        List<Entity> worklists = new ArrayList<>();
        worklists.add(schedulingFactory.createWorkList());
        worklists.add(schedulingFactory.createWorkList());
        Act act = create(ScheduleArchetypes.TASK, Act.class);
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(new LocalContext(),
                                                                      new HelpContext("foo", null));
        FollowUpTaskEditor editor = new FollowUpTaskEditor(act, worklists, layoutContext);
        IMObjectEditor newInstance = editor.newInstance();
        assertNotNull(newInstance);
        assertNotEquals(editor, newInstance);
        assertEquals(FollowUpTaskEditor.class, newInstance.getClass());
    }
}
