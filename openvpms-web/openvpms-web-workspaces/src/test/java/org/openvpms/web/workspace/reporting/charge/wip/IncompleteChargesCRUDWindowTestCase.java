/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.wip;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.print.PrintDialog;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.customer.charge.ChargeCRUDWindow;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;
import static org.openvpms.web.test.EchoTestHelper.fireButton;

/**
 * Tests the {@link IncompleteChargesCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class IncompleteChargesCRUDWindowTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        context.setPractice(practiceFactory.getPractice());
        context.setLocation(practiceFactory.createLocation());

        initErrorHandler(errors);
    }

    /**
     * Tests posting a charge.
     */
    @Test
    public void testPost() {
        checkPost(accountFactory.createInvoice(10, FinancialActStatus.IN_PROGRESS));
        checkPost(accountFactory.createCounterSale(10, FinancialActStatus.IN_PROGRESS));
        checkPost(accountFactory.createCredit(10, FinancialActStatus.IN_PROGRESS));
    }

    /**
     * Tests posting a charge.
     *
     * @param charge the charge
     */
    @SuppressWarnings("unchecked")
    private void checkPost(FinancialAct charge) {
        errors.clear();

        Archetypes<FinancialAct> archetypes = Archetypes.create("act.customerAccountCharges*", FinancialAct.class);
        QueryBrowser<FinancialAct> browser = (QueryBrowser<FinancialAct>) Mockito.mock(QueryBrowser.class);
        IncompleteChargesCRUDWindow window = new IncompleteChargesCRUDWindow(archetypes, browser, context,
                                                                             new HelpContext("foo", null));
        window.setObject(charge);

        fireButton(window.getComponent(), ChargeCRUDWindow.POST_ID);
        String displayName = getDisplayName(charge);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        FinancialAct reloaded = get(charge);
        assertEquals(POSTED, reloaded.getStatus());
        assertEquals(reloaded, window.getObject());

        // For invoices and counter sales, a payment workflow should be displayed when the customer balance is positive.
        // For credits, a print dialog should be displayed
        if (charge.isA(CustomerAccountArchetypes.INVOICE, CustomerAccountArchetypes.COUNTER)) {
            findMessageDialogAndFireButton(ConfirmationDialog.class, "Pay Account", "Do you wish to pay the account?",
                                           ConfirmationDialog.YES_ID);
        } else {
            assertNotNull(findWindowPane(PrintDialog.class));
        }
        assertTrue(errors.isEmpty());
    }
}
