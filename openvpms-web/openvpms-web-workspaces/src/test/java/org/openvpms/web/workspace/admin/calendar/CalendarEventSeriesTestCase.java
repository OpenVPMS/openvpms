/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.calendar;

import org.junit.Before;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.workspace.workflow.appointment.repeat.AbstractScheduleEventSeriesTest;
import org.openvpms.web.workspace.workflow.appointment.repeat.ScheduleEventSeries;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link CalendarEventSeries} class.
 *
 * @author Tim Anderson
 */
public class CalendarEventSeriesTestCase extends AbstractScheduleEventSeriesTest {

    /**
     * The test schedule.
     */
    private Entity schedule;

    /**
     * The test location.
     */
    private Party location;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        location = TestHelper.createLocation();
        schedule = ScheduleTestHelper.createSchedule(15, DateUnits.MINUTES.toString(), 1, null, location);
    }

    /**
     * Creates a new calendar event.
     *
     * @param startTime the event start time
     * @param endTime   the event end time
     * @return a new event
     */
    @Override
    protected Act createEvent(Date startTime, Date endTime) {
        return ScheduleTestHelper.createCalendarEvent(startTime, endTime, schedule, location);
    }

    /**
     * Checks a series that was generated using calendar intervals.
     *
     * @param series   the series
     * @param event    the initial event
     * @param interval the interval
     * @param units    the interval units
     * @param count    the expected no. of events in the series
     * @param author   the expected author
     * @return the events
     */
    @Override
    protected List<Act> checkSeries(ScheduleEventSeries series, Act event, int interval, DateUnits units, int count,
                                    User author) {
        List<Act> acts = series.getEvents();
        assertEquals(count, acts.size());
        Date from = event.getActivityStartTime();
        Date to = event.getActivityEndTime();
        assertEquals(event, acts.get(0));
        IMObjectBean bean = getBean(event);
        Entity schedule = bean.getTarget("schedule", Entity.class);
        String status = event.getStatus();
        for (Act act : acts) {
            checkCalendarEvent(act, from, to, schedule, location, status, author);
            from = DateRules.getDate(from, interval, units);
            to = DateRules.getDate(to, interval, units);
        }
        return acts;
    }

    /**
     * Creates a new {@link ScheduleEventSeries}.
     *
     * @param event the event
     * @return a new series
     */
    @Override
    protected ScheduleEventSeries createSeries(Act event) {
        return new CalendarEventSeries(event, getArchetypeService());
    }

    /**
     * Verifies an event matches that expected.
     *
     * @param act       the event
     * @param startTime the expected start time
     * @param endTime   the expected end time
     * @param schedule  the expected schedule
     * @param location  the expected appointment type
     * @param status    the expected status
     * @param author    the expected author
     */
    private void checkCalendarEvent(Act act, Date startTime, Date endTime, Entity schedule, Entity location,
                                    String status, User author) {
        assertEquals(0, DateRules.compareTo(startTime, act.getActivityStartTime()));
        assertEquals(0, DateRules.compareTo(endTime, act.getActivityEndTime()));
        IMObjectBean bean = getBean(act);
        assertEquals(schedule, bean.getTarget("schedule"));
        assertEquals(location, bean.getTarget("location"));
        assertEquals(status, act.getStatus());
        assertEquals(author.getObjectReference(), act.getCreatedBy());
    }
}