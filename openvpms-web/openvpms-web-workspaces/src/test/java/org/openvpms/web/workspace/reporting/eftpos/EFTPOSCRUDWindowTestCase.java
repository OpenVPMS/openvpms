/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.eftpos;

import nextapp.echo2.app.ApplicationInstance;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupWindow;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link EFTPOSCRUDWindow} class.
 *
 * @author Tim Anderson
 */
public class EFTPOSCRUDWindowTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Verifies that EFTPOS transactions cannot be created, edited or deleted.
     */
    @Test
    public void testCreateEditAndDeleteForbidden() {
        EFTPOSCRUDWindow window = new EFTPOSCRUDWindow(Archetypes.create(EFTPOSArchetypes.TRANSACTIONS, Act.class),
                                                       new LocalContext(), new HelpContext("foo", null));

        // verify that invoking create() is a no-op
        window.create();
        assertNull(EchoTestHelper.findComponent(ApplicationInstance.getActive().getDefaultWindow(), PopupWindow.class));

        // now create a transaction, and verify it can't be edited or deleted
        Entity terminal = practiceFactory.createEFTPOSTerminal();
        FinancialAct payment = accountFactory.newEFTPOSPayment()
                .customer(customerFactory.createCustomer())
                .amount(TEN)
                .terminal(terminal)
                .location(practiceFactory.createLocation())
                .status(EFTPOSTransactionStatus.IN_PROGRESS)
                .build();

        window.setObject(payment);
        window.edit();
        ErrorDialog dialog1 = findComponent(ErrorDialog.class);
        assertEquals("Can't edit EFTPOS Payment with status IN_PROGRESS", dialog1.getMessage());
        EchoTestHelper.fireDialogButton(dialog1, ErrorDialog.OK_ID);

        window.delete();
        ErrorDialog dialog2 = findComponent(ErrorDialog.class);
        assertEquals("Can't delete EFTPOS Payment with status IN_PROGRESS", dialog2.getMessage());
    }
}
