/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.test.TestHelper.checkEquals;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;
import static org.openvpms.web.component.im.util.IMObjectHelper.getBean;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Helper routines for customer charge tests.
 *
 * @author Tim Anderson
 */
public class CustomerChargeTestHelper {

    /**
     * Adds a charge item.
     *
     * @param editor   the editor
     * @param patient  the patient
     * @param product  the product
     * @param quantity the quantity. If {@code null}, indicates the quantity won't be changed
     * @param queue    the popup editor manager
     * @return the editor for the new item
     */
    public static CustomerChargeActItemEditor addItem(CustomerChargeActEditor editor, Party patient,
                                                      Product product, BigDecimal quantity, EditorQueue queue) {
        CustomerChargeActItemEditor itemEditor = editor.addItem();
        itemEditor.getComponent();
        assertValid(editor);
        assertFalse(itemEditor.isValid());

        setItem(editor, itemEditor, patient, product, quantity, queue);
        return itemEditor;
    }

    /**
     * Sets the values of a charge item.
     *
     * @param editor     the charge editor
     * @param itemEditor the charge item editor
     * @param patient    the patient
     * @param product    the product
     * @param quantity   the quantity. If {@code null}, indicates the quantity won't be changed
     * @param queue      the popup editor manager
     */
    public static void setItem(CustomerChargeActEditor editor, CustomerChargeActItemEditor itemEditor,
                               Party patient, Product product, BigDecimal quantity, EditorQueue queue) {
        if (itemEditor.getProperty("patient") != null) {
            itemEditor.setPatient(patient);
        }
        itemEditor.setProduct(product);
        if (quantity != null) {
            itemEditor.setQuantity(quantity);
        }
        if (TypeHelper.isA(editor.getObject(), CustomerAccountArchetypes.INVOICE)) {
            if (!TypeHelper.isA(product, ProductArchetypes.TEMPLATE)) {
                checkSavePopups(editor, itemEditor, product, queue);
            } else {
                IMObjectBean bean = getBean(product);
                List<Entity> includes = bean.getTargets("includes", Entity.class);
                for (Entity include : includes) {
                    checkSavePopups(editor, itemEditor, (Product) include, queue);
                }
            }
        }
        Validator validator = new DefaultValidator();
        boolean valid = itemEditor.validate(validator);
        if (!valid) {
            ValidationHelper.showError(validator);
        }
        assertValid(itemEditor);
    }

    public static void checkSavePopups(CustomerChargeActEditor editor, CustomerChargeActItemEditor itemEditor,
                                       Product product, EditorQueue queue) {
        if (TypeHelper.isA(product, ProductArchetypes.MEDICATION)) {
            // invoice items have a dispensing node
            IMObjectBean bean = getBean(product);
            if (bean.getBoolean("label")) {
                // dispensing label should be displayed
                assertFalse("Editor should invalid after setting " + product.getName(), itemEditor.isValid());
                // not valid while popup is displayed

                checkSavePopup(queue, PatientArchetypes.PATIENT_MEDICATION, true);
                // save the popup editor - should be a medication
            }
        }

        IMObjectBean bean = getBean(product);
        for (int i = 0; i < bean.getTargetRefs("tests").size(); ++i) {
            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, InvestigationArchetypes.PATIENT_INVESTIGATION, false);
        }
        for (int i = 0; i < bean.getTargetRefs("reminders").size(); ++i) {
            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, ReminderArchetypes.REMINDER, false);
        }
        for (int i = 0; i < bean.getTargetRefs("alerts").size(); ++i) {
            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, PatientArchetypes.ALERT, false);
        }
        for (int i = 0; i < bean.getTargetRefs("tasks").size(); ++i) {
            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, ScheduleArchetypes.TASK, false);
        }
    }

    /**
     * Saves the current popup editor.
     *
     * @param queue        the popup editor manager
     * @param shortName    the expected archetype short name of the object being edited
     * @param prescription if {@code true} process prescription prompts
     */
    public static void checkSavePopup(EditorQueue queue, String shortName, boolean prescription) {
        if (prescription) {
            PopupDialog dialog = queue.getCurrent();
            if (dialog instanceof ConfirmationDialog) {
                fireDialogButton(dialog, PopupDialog.OK_ID);
            }
        }
        PopupDialog dialog = queue.getCurrent();
        assertTrue(dialog instanceof EditDialog);
        IMObjectEditor editor = ((EditDialog) dialog).getEditor();
        assertTrue(TypeHelper.isA(editor.getObject(), shortName));
        assertValid(editor);
        fireDialogButton(dialog, PopupDialog.OK_ID);
    }

    /**
     * Creates a new <em>entity.HL7ServiceLaboratory</em>.
     *
     * @param location the practice location
     * @return a new laboratory
     */
    public static Entity createHL7Laboratory(Party location) {
        User user = TestHelper.createUser();
        return new TestLaboratoryFactory(ServiceHelper.getArchetypeService())
                .createHL7Laboratory(location, user);

    }

    /**
     * Verifies an order matches that expected.
     *
     * @param order             the order
     * @param type              the expected type
     * @param patient           the expected patient
     * @param product           the expected product
     * @param quantity          the expected quantity
     * @param placerOrderNumber the expected placer order number
     * @param date              the expected date
     * @param clinician         the expected clinician
     * @param pharmacy          the expected pharmacy
     */
    public static void checkOrder(TestPharmacyOrderService.Order order, TestPharmacyOrderService.Order.Type type,
                                  Party patient, Product product, BigDecimal quantity,
                                  long placerOrderNumber, Date date, User clinician, Entity pharmacy) {
        assertEquals(type, order.getType());
        assertEquals(patient, order.getPatient());
        assertEquals(product, order.getProduct());
        checkEquals(quantity, order.getQuantity());
        assertEquals(placerOrderNumber, order.getPlacerOrderNumber());
        assertEquals(date, order.getDate());
        assertEquals(clinician, order.getClinician());
        assertEquals(pharmacy, order.getPharmacy());
    }

    /**
     * Verifies a laboratory order matches that expected.
     *
     * @param order             the order
     * @param type              the expected type
     * @param patient           the expected patient
     * @param placerOrderNumber the expected placer order number
     * @param date              the expected date
     * @param clinician         the expected clinician
     * @param laboratory        the expected laboratory
     */
    public static void checkOrder(TestLaboratoryOrderService.LabOrder order,
                                  TestLaboratoryOrderService.LabOrder.Type type, Party patient, long placerOrderNumber,
                                  Date date, User clinician, Entity laboratory) {
        assertEquals(type, order.getType());
        assertEquals(patient, order.getPatient());
        assertEquals(placerOrderNumber, order.getPlacerOrderNumber());
        assertEquals(0, DateRules.compareTo(date, order.getDate(), true));
        assertEquals(clinician, order.getClinician());
        assertEquals(laboratory, order.getLaboratory());
    }
}
