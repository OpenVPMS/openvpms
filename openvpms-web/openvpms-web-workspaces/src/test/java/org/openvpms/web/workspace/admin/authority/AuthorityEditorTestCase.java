/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.authority;

import org.junit.Test;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link AuthorityEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class AuthorityEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * Verifies that authorities with duplicate names fail validation.
     */
    @Test
    public void testDuplicate() {
        ArchetypeAwareGrantedAuthority auth1 = createAuthority();
        String name1 = TestHelper.randomName("zauth");

        ArchetypeAwareGrantedAuthority auth2 = createAuthority();
        String name2 = TestHelper.randomName("zauth");
        auth2.setName(name2);
        save(auth2);

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        AuthorityEditor editor = new AuthorityEditor(auth1, null, layoutContext);
        assertFalse(editor.isValid());
        editor.setName(name1);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        editor.setName(name2);
        assertInvalid(editor, "An authority already exists with name '" + editor.getName() + "'");

        editor.setName(name1);
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link AuthorityEditor} for
     * <em>security.security.archetypeAuthority</em> instances.
     */
    @Test
    public void testFactory() {
        ArchetypeAwareGrantedAuthority authority = createAuthority();
        IMObjectEditor editor = factory.create(authority, new DefaultLayoutContext(new LocalContext(),
                                                                                   new HelpContext("foo", null)));
        assertTrue(editor instanceof AuthorityEditor);
    }

    /**
     * Tests the {@link AuthorityEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        ArchetypeAwareGrantedAuthority authority = createAuthority();
        AuthorityEditor editor = new AuthorityEditor(authority, null, new DefaultLayoutContext(new LocalContext(),
                                                                                               new HelpContext("foo", null)));
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof AuthorityEditor);
        assertEquals(authority, newInstance.getObject());
    }

    /**
     * Creates a new authority.
     *
     * @return a new authority
     */
    private ArchetypeAwareGrantedAuthority createAuthority() {
        ArchetypeAwareGrantedAuthority result = create(UserArchetypes.AUTHORITY, ArchetypeAwareGrantedAuthority.class);
        result.setServiceName("archetypeService");
        result.setMethod("create");
        result.setShortName("*");
        return result;
    }

}
