/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActCalculator;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.invoice.InvoiceItemStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.patient.prescription.PrescriptionTestHelper;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderStatus;
import org.openvpms.archetype.rules.patient.reminder.ReminderTestHelper;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemVerifier;
import org.openvpms.archetype.test.builder.hl7.TestHL7Factory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.ParameterDialog;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.reminder.ReminderEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientParticipationEditor;
import org.openvpms.web.component.im.product.ProductParticipationEditor;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.charge.TestLaboratoryOrderService.LabOrder;
import org.openvpms.web.workspace.patient.mr.PatientMedicationActEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT_ITEM;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkOrder;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkSavePopup;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.createHL7Laboratory;
import static org.openvpms.web.workspace.customer.charge.TestPharmacyOrderService.Order;

/**
 * Tests the {@link DefaultCustomerChargeActEditor} class.
 *
 * @author Tim Anderson
 */
public class DefaultCustomerChargeActEditorTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The author.
     */
    private User author;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The layout context.
     */
    private LayoutContext layoutContext;

    /**
     * Medical record rules.
     */
    @Autowired
    private MedicalRecordRules records;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The HL7 factory.
     */
    @Autowired
    private TestHL7Factory hl7Factory;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
        author = userFactory.createUser();
        clinician = userFactory.createClinician();
        location = practiceFactory.createLocation();

        layoutContext = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        layoutContext.setEdit(true);
        layoutContext.getContext().setPractice(getPractice());
        layoutContext.getContext().setCustomer(customer);
        layoutContext.getContext().setUser(author);
        layoutContext.getContext().setClinician(clinician);
        layoutContext.getContext().setLocation(location);

        new AuthenticationContextImpl().setUser(author);  // makes author the logged-in user
    }

    /**
     * Tests creation and saving of an empty invoice.
     */
    @Test
    public void testEmptyInvoice() {
        checkEmptyCharge(create(INVOICE, FinancialAct.class));
    }

    /**
     * Tests creation and saving of an empty invoice.
     */
    @Test
    public void testEmptyCredit() {
        checkEmptyCharge(create(CREDIT, FinancialAct.class));
    }

    /**
     * Tests creation and saving of an empty counter sale.
     */
    @Test
    public void testEmptyCounterSale() {
        checkEmptyCharge(create(COUNTER, FinancialAct.class));
    }

    /**
     * Tests invoicing.
     */
    @Test
    public void testInvoice() {
        checkEditCharge(INVOICE, INVOICE_ITEM, true);
    }

    /**
     * Tests invoicing.
     */
    @Test
    public void testInvoiceWithDelayedOrderDiscontinuation() {
        checkEditCharge(INVOICE, INVOICE_ITEM, false);
    }

    /**
     * Tests counter sales.
     */
    @Test
    public void testCounterSale() {
        checkEditCharge(COUNTER, COUNTER_ITEM, true);
    }

    /**
     * Tests credits.
     */
    @Test
    public void testCredit() {
        checkEditCharge(CREDIT, CREDIT_ITEM, true);
    }

    /**
     * Tests the addition of 3 items to an invoice.
     */
    @Test
    public void testAdd3Items() {
        BigDecimal itemTotal1 = BigDecimal.valueOf(20);
        BigDecimal itemTotal2 = BigDecimal.valueOf(50);
        BigDecimal itemTotal3 = new BigDecimal("41.25");

        Product product1 = productFactory.newService().fixedPrice("18.18").build();
        Product product2 = productFactory.newService().fixedPrice("45.45").build();
        Product product3 = productFactory.newService().fixedPrice("37.50").build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);
        BigDecimal total = itemTotal1.add(itemTotal2).add(itemTotal3);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        BigDecimal quantity = ONE;
        addItem(editor, patient, product1, quantity, queue);
        addItem(editor, patient, product2, quantity, queue);
        addItem(editor, patient, product3, quantity, queue);
        save(editor);

        checkTotal(charge, total);
    }

    /**
     * Tests the addition of 3 items to an invoice, followed by the deletion of 1, verifying totals.
     */
    @Test
    public void testAdd3ItemsWithDeletion() {
        BigDecimal itemTotal1 = BigDecimal.valueOf(20);
        BigDecimal itemTotal2 = BigDecimal.valueOf(50);
        BigDecimal itemTotal3 = new BigDecimal("41.25");

        Product product1 = productFactory.newService().fixedPrice("18.18").build();
        Product product2 = productFactory.newService().fixedPrice("45.45").build();
        Product product3 = productFactory.newService().fixedPrice("37.50").build();

        for (int i = 0, j = 0; i < 3; ++i) {
            FinancialAct charge = create(INVOICE, FinancialAct.class);
            BigDecimal total = itemTotal1.add(itemTotal2).add(itemTotal3);

            TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
            editor.getComponent();
            assertTrue(editor.isValid());

            BigDecimal quantity = ONE;
            EditorQueue queue = editor.getQueue();
            CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, quantity, queue);
            CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, quantity, queue);
            CustomerChargeActItemEditor itemEditor3 = addItem(editor, patient, product3, quantity, queue);
            save(editor);

            charge = get(charge);
            checkEquals(total, charge.getTotal());
            ActCalculator calculator = new ActCalculator(getArchetypeService());
            BigDecimal itemTotal = calculator.sum(charge, "items", "total");
            checkEquals(total, itemTotal);

            if (j == 0) {
                editor.delete(itemEditor1.getObject());
                total = total.subtract(itemTotal1);
            } else if (j == 1) {
                editor.delete(itemEditor2.getObject());
                total = total.subtract(itemTotal2);
            } else if (j == 2) {
                editor.delete(itemEditor3.getObject());
                total = total.subtract(itemTotal3);
            }
            ++j;
            if (j > 2) {
                j = 0;
            }
            save(editor);
            charge = get(charge);
            checkTotal(charge, total);
        }
    }

    /**
     * Tests the addition of 3 items to an invoice, followed by the deletion of 1 in a new editor, verifying totals.
     */
    @Test
    public void testAdd3ItemsWithDeletionAfterReload() {
        BigDecimal itemTotal1 = BigDecimal.valueOf(20);
        BigDecimal itemTotal2 = BigDecimal.valueOf(50);
        BigDecimal itemTotal3 = new BigDecimal("41.25");

        Product product1 = productFactory.newService().fixedPrice("18.18").build();
        Product product2 = productFactory.newService().fixedPrice("45.45").build();
        Product product3 = productFactory.newService().fixedPrice("37.50").build();

        for (int i = 0, j = 0; i < 3; ++i) {
            FinancialAct charge = create(INVOICE, FinancialAct.class);
            BigDecimal total = itemTotal1.add(itemTotal2).add(itemTotal3);

            TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
            EditorQueue queue = editor.getQueue();
            editor.getComponent();
            assertTrue(editor.isValid());

            BigDecimal quantity = ONE;
            CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, quantity, queue);
            CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, quantity, queue);
            CustomerChargeActItemEditor itemEditor3 = addItem(editor, patient, product3, quantity, queue);
            save(editor);

            charge = get(charge);
            checkTotal(charge, total);

            editor = createCustomerChargeActEditor(charge, layoutContext);
            editor.getComponent();

            if (j == 0) {
                editor.delete(itemEditor1.getObject());
                total = total.subtract(itemTotal1);
            } else if (j == 1) {
                editor.delete(itemEditor2.getObject());
                total = total.subtract(itemTotal2);
            } else if (j == 2) {
                editor.delete(itemEditor3.getObject());
                total = total.subtract(itemTotal3);
            }
            ++j;
            if (j > 2) {
                j = 0;
            }
            save(editor);
            charge = get(charge);
            checkTotal(charge, total);
        }
    }

    /**
     * Tests the addition of 3 items to an invoice, followed by the deletion of 1 before saving.
     */
    @Test
    public void test3ItemsAdditionWithDeletionBeforeSave() {
        BigDecimal itemTotal1 = BigDecimal.valueOf(20);
        BigDecimal itemTotal2 = BigDecimal.valueOf(50);
        BigDecimal itemTotal3 = new BigDecimal("41.25");

        Product product1 = productFactory.newService().fixedPrice("18.18").build();
        Product product2 = productFactory.newService().fixedPrice("45.45").build();
        Product product3 = productFactory.newService().fixedPrice("37.50").build();

        for (int i = 0, j = 0; i < 3; ++i) {
            FinancialAct charge = create(INVOICE, FinancialAct.class);
            BigDecimal total = itemTotal1.add(itemTotal2).add(itemTotal3);

            TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
            EditorQueue queue = editor.getQueue();
            editor.getComponent();

            BigDecimal quantity = ONE;
            CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, quantity, queue);
            CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, quantity, queue);
            CustomerChargeActItemEditor itemEditor3 = addItem(editor, patient, product3, quantity, queue);

            if (j == 0) {
                editor.delete(itemEditor1.getObject());
                total = total.subtract(itemTotal1);
            } else if (j == 1) {
                editor.delete(itemEditor2.getObject());
                total = total.subtract(itemTotal2);
            } else if (j == 2) {
                editor.delete(itemEditor3.getObject());
                total = total.subtract(itemTotal3);
            }
            ++j;
            if (j > 2) {
                j = 0;
            }

            save(editor);
            charge = get(charge);
            checkTotal(charge, total);
        }
    }

    /**
     * Tests the addition of 2 items to an invoice, followed by the change of product of 1 before saving.
     */
    @Test
    public void testItemChange() {
        BigDecimal itemTotal1 = BigDecimal.valueOf(20);
        BigDecimal itemTotal2 = BigDecimal.valueOf(50);
        BigDecimal itemTotal3 = new BigDecimal("41.25");

        Product product1 = productFactory.newService().fixedPrice("18.18").build();
        Product product2 = productFactory.newService().fixedPrice("45.45").build();
        Product product3 = productFactory.newService().fixedPrice("37.50").build();

        for (int i = 0, j = 0; i < 3; ++i) {
            FinancialAct charge = create(INVOICE, FinancialAct.class);
            BigDecimal total = itemTotal1.add(itemTotal2).add(itemTotal3);

            boolean addDefaultItem = (j == 0);
            TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext, addDefaultItem);
            editor.getComponent();

            BigDecimal quantity = ONE;
            CustomerChargeActItemEditor itemEditor1;
            if (j == 0) {
                itemEditor1 = editor.getCurrentEditor();
                setItem(editor, itemEditor1, patient, product1, quantity, editor.getQueue());
            } else {
                itemEditor1 = addItem(editor, patient, product1, quantity, editor.getQueue());
            }
            CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, quantity, editor.getQueue());

            if (j == 0) {
                itemEditor1.setProduct(product3);
                total = total.subtract(itemTotal1);
            } else if (j == 1) {
                itemEditor2.setProduct(product3);
                total = total.subtract(itemTotal2);
            }
            ++j;
            if (j > 1) {
                j = 0;
            }

            save(editor);
            charge = get(charge);
            checkTotal(charge, total);
        }
    }

    /**
     * Verifies that the {@link DefaultCustomerChargeActEditor#delete()} method deletes an invoice and its item.
     * <p>
     * If any pharmacy or lab orders have been created, these are cancelled.
     */
    @Test
    public void testDeleteInvoice() {
        FinancialAct charge = create(INVOICE, FinancialAct.class);

        BigDecimal fixedPrice = BigDecimal.TEN;
        BigDecimal itemTotal = BigDecimal.valueOf(11);
        BigDecimal total = itemTotal.multiply(BigDecimal.valueOf(3));

        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        Entity laboratory = laboratoryFactory.createHL7Laboratory(location, userFactory.createUser());
        Entity taskType1 = schedulingFactory.createTaskType();
        Entity taskType2 = schedulingFactory.createTaskType();
        Entity taskType3 = schedulingFactory.createTaskType();
        Entity workList = schedulingFactory.newWorkList().taskTypes(taskType1, taskType2, taskType3).build();

        Entity alertType1 = createAlertType();
        Product product1 = productFactory.newMedication()
                .fixedPrice(fixedPrice)
                .pharmacy(pharmacy)
                .addAlertTypes(alertType1)
                .newProductTask().taskType(taskType1).workList(workList).start(0, DateUnits.DAYS).add()
                .build();
        Entity reminderType1 = addReminder(product1);
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        addHL7Test(product1, investigationType1);
        Entity template1 = addDocumentTemplate(product1);

        Entity alertType2 = createAlertType();
        Product product2 = productFactory.newMerchandise()
                .fixedPrice(fixedPrice)
                .pharmacy(pharmacy)
                .addAlertTypes(alertType2)
                .newProductTask().taskType(taskType2).workList(workList).start(0, DateUnits.DAYS).add()
                .build();
        Entity reminderType2 = addReminder(product2);
        Entity investigationType2 = laboratoryFactory.createInvestigationType();
        Entity investigationType3 = laboratoryFactory.createInvestigationType();
        addHL7Test(product2, investigationType2);
        addHL7Test(product2, investigationType3);
        Entity template2 = addDocumentTemplate(product2, PatientArchetypes.DOCUMENT_LETTER);

        Entity alertType3 = createAlertType();
        Product product3 = productFactory.newService()
                .fixedPrice(fixedPrice)
                .addAlertTypes(alertType3)
                .newProductTask().taskType(taskType3).workList(workList).start(0, DateUnits.DAYS).add()
                .build();
        Entity reminderType3 = addReminder(product3);
        Entity investigationType4 = laboratoryFactory.createInvestigationType(laboratory);
        addHL7Test(product3, investigationType4); // ordered via lab
        Entity investigationType5 = laboratoryFactory.createInvestigationType();
        Entity investigationType6 = laboratoryFactory.createInvestigationType();
        addHL7Test(product3, investigationType5);
        addHL7Test(product3, investigationType6);

        Entity template3 = addDocumentTemplate(product3);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        BigDecimal quantity = ONE;
        CustomerChargeActItemEditor item1Editor = addItem(editor, patient, product1, quantity, queue);
        CustomerChargeActItemEditor item2Editor = addItem(editor, patient, product2, quantity, queue);
        CustomerChargeActItemEditor item3Editor = addItem(editor, patient, product3, quantity, queue);
        FinancialAct item1 = item1Editor.getObject();
        FinancialAct item2 = item2Editor.getObject();
        FinancialAct item3 = item3Editor.getObject();

        save(editor);
        editor.postSave(queue, false, new MutableBoolean(false));  // submit lab orders
        checkPatientAndProductReadOnly(item1Editor, true);
        checkPatientAndProductReadOnly(item2Editor, true);
        checkPatientAndProductReadOnly(item3Editor, true);

        // check pharmacy orders
        List<Order> orders = editor.getPharmacyOrderService().getOrders();
        assertEquals(2, orders.size());
        editor.getPharmacyOrderService().clear();

        // check lab orders
        List<LabOrder> labOrders = editor.getLaboratoryOrderService().getOrders();
        assertEquals(1, labOrders.size());
        editor.getLaboratoryOrderService().clear();

        BigDecimal balance = (charge.isCredit()) ? total.negate() : total;
        checkBalance(customer, balance, ZERO);

        DocumentAct investigation1 = getInvestigation(item1, investigationType1);
        DocumentAct investigation2 = getInvestigation(item2, investigationType2);
        DocumentAct investigation3 = getInvestigation(item2, investigationType3);
        DocumentAct investigation4 = getInvestigation(item3, investigationType4);
        DocumentAct investigation5 = getInvestigation(item3, investigationType5);
        DocumentAct investigation6 = getInvestigation(item3, investigationType6);

        PatientTestHelper.addReport(investigation4);
        PatientTestHelper.addReport(investigation5);
        investigation1.setStatus(ActStatus.IN_PROGRESS);
        investigation2.setStatus(ActStatus.POSTED);
        investigation3.setStatus(ActStatus.IN_PROGRESS);
        investigation4.setStatus(ActStatus.IN_PROGRESS);
        investigation5.setStatus(ActStatus.CANCELLED);
        investigation6.setStatus(ActStatus.POSTED);
        save(investigation1, investigation2, investigation3, investigation4, investigation5, investigation6);
        Act reminder1 = getReminder(item1, reminderType1);
        Act reminder2 = getReminder(item2, reminderType2);
        Act reminder3 = getReminder(item3, reminderType3);
        reminder1.setStatus(ReminderStatus.IN_PROGRESS);
        reminder2.setStatus(ReminderStatus.COMPLETED);
        reminder3.setStatus(ReminderStatus.CANCELLED);
        save(reminder1, reminder2, reminder3);

        Act doc1 = getDocument(item1, template1);
        Act doc2 = getDocument(item2, template2);
        Act doc3 = getDocument(item3, template3);
        doc1.setStatus(ActStatus.IN_PROGRESS);
        doc2.setStatus(ActStatus.COMPLETED);
        doc3.setStatus(ActStatus.POSTED);
        save(doc1, doc2, doc3);

        Act alert1 = getAlert(item1, alertType1);
        Act alert2 = getAlert(item2, alertType2);
        Act alert3 = getAlert(item3, alertType3);
        alert1.setStatus(ActStatus.IN_PROGRESS);
        alert2.setStatus(ActStatus.COMPLETED);
        alert3.setStatus(ActStatus.COMPLETED);
        save(alert1, alert2, alert3);

        Act task1 = getTask(item1, taskType1);
        Act task2 = getTask(item2, taskType2);
        Act task3 = getTask(item3, taskType3);
        task1.setStatus(TaskStatus.IN_PROGRESS);
        task2.setStatus(TaskStatus.BILLED);
        task3.setStatus(TaskStatus.COMPLETED);
        save(task1, task2, task3);

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());

        assertTrue(delete(editor));
        assertNull(get(charge));
        for (FinancialAct item : items) {
            assertNull(get(item));
        }

        // check order cancellations
        orders = editor.getPharmacyOrderService().getOrders(true);
        assertEquals(2, orders.size());
        checkOrder(orders.get(0), Order.Type.CANCEL, patient, product1, quantity, item1.getId(),
                   item1.getActivityStartTime(), clinician, pharmacy);
        checkOrder(orders.get(1), Order.Type.CANCEL, patient, product2, quantity, item2.getId(),
                   item2.getActivityStartTime(), clinician, pharmacy);

        labOrders = editor.getLaboratoryOrderService().getOrders();
        assertEquals(1, labOrders.size());
        checkOrder(labOrders.get(0), LabOrder.Type.CANCEL, patient, investigation4.getId(),
                   investigation4.getActivityStartTime(), clinician, laboratory);

        assertNull(get(investigation1));
        assertNotNull(get(investigation2));
        assertNull(get(investigation3));
        assertNotNull(get(investigation4));
        assertNotNull(get(investigation5));
        assertNotNull(get(investigation6));

        assertNull(get(reminder1));
        assertNotNull(get(reminder2));
        assertNull(get(reminder3));

        assertNull(get(doc1));
        assertNotNull(get(doc2));
        assertNotNull(get(doc3));

        assertNull(get(alert1));
        assertNotNull(get(alert2));
        assertNotNull(get(alert3));

        assertNull(get(task1));
        assertNotNull(get(task2));
        assertNotNull(get(task3));

        checkBalance(customer, ZERO, ZERO);
    }

    /**
     * Verifies that the {@link DefaultCustomerChargeActEditor#delete()} method deletes a credit and its item.
     */
    @Test
    public void testDeleteCredit() {
        checkDeleteCharge(create(CREDIT, FinancialAct.class));
    }

    /**
     * Verifies that the {@link DefaultCustomerChargeActEditor#delete()} method deletes a counter sale and its item.
     */
    @Test
    public void testDeleteCounterSale() {
        checkDeleteCharge(create(COUNTER, FinancialAct.class));
    }

    /**
     * Verifies stock quantities update for products used in an invoice.
     */
    @Test
    public void testInvoiceStockUpdate() {
        checkChargeStockUpdate(create(INVOICE, FinancialAct.class));
    }

    /**
     * Verifies stock quantities update for products used in a credit.
     */
    @Test
    public void testCreditStockUpdate() {
        checkChargeStockUpdate(create(CREDIT, FinancialAct.class));
    }

    /**
     * Verifies stock quantities update for products used in a counter sale.
     */
    @Test
    public void testCounterSaleStockUpdate() {
        checkChargeStockUpdate(create(COUNTER, FinancialAct.class));
    }

    /**
     * Test template expansion for an invoice.
     */
    @Test
    public void testExpandTemplateInvoice() {
        checkExpandTemplate(create(INVOICE, FinancialAct.class), INVOICE_ITEM);
    }

    /**
     * Test template expansion for a credit.
     */
    @Test
    public void testExpandTemplateCredit() {
        checkExpandTemplate(create(CREDIT, FinancialAct.class), CREDIT_ITEM);
    }

    /**
     * Test template expansion for a counter sale.
     */
    @Test
    public void testExpandTemplateCounterSale() {
        checkExpandTemplate(create(COUNTER, FinancialAct.class), COUNTER_ITEM);
    }

    /**
     * Verifies that an act is invalid if the sum of the item totals don't add up to the charge total.
     */
    @Test
    public void testTotalMismatch() {
        BigDecimal itemTotal = BigDecimal.valueOf(20);
        Product product1 = productFactory.newService().fixedPrice(itemTotal).build();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(itemTotal, customer, patient, product1,
                                                                           ActStatus.IN_PROGRESS);
        save(acts);
        FinancialAct charge = acts.get(0);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        charge.setTotal(Money.ONE);
        assertFalse(editor.isValid());

        Validator validator = new DefaultValidator();
        assertFalse(editor.validate(validator));
        List<ValidatorError> list = validator.getErrors(editor);
        assertEquals(1, list.size());
        String message = Messages.format("act.validation.totalMismatch", editor.getProperty("amount").getDisplayName(),
                                         NumberFormatter.formatCurrency(charge.getTotal()),
                                         editor.getProperty("items").getDisplayName(),
                                         NumberFormatter.formatCurrency(itemTotal));
        String expected = Messages.format(ValidatorError.MSG_KEY, message);
        assertEquals(expected, list.get(0).toString());
    }

    /**
     * Verifies a prescription can be selected during invoicing.
     */
    @Test
    public void testPrescription() {
        checkPrescription(clinician, clinician);
    }

    /**
     * Verifies a prescription can be selected during invoicing where the invoice has no clinician.
     */
    @Test
    public void testPrescriptionWithNoClinician() {
        checkPrescription(clinician, null);
    }

    /**
     * Verifies that an invoice item linked to a prescription can be deleted, and that the medication is removed
     * from the prescription.
     */
    @Test
    public void testDeleteInvoiceItemLinkedToPrescription() {
        Product product1 = productFactory.newMedication().fixedPrice(1).build();
        Product product2 = productFactory.newMedication().fixedPrice(1).build();

        Act prescription = PrescriptionTestHelper.createPrescription(patient, product1, clinician);
        FinancialAct charge = create(INVOICE, FinancialAct.class);
        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, ONE, queue);
        addItem(editor, patient, product2, ONE, queue);
        save(editor);

        checkPrescription(prescription, itemEditor1);
        editor.removeItem(itemEditor1.getObject());
        save(editor);

        prescription = get(prescription);
        IMObjectBean bean = getBean(prescription);
        assertTrue(bean.getTargets("dispensing").isEmpty());
    }

    /**
     * Verifies that an unsaved invoice item can be deleted, when there is a prescription associated with the item's
     * product.
     */
    @Test
    public void testDeleteUnsavedInvoiceItemWithPrescription() {
        checkDeleteUnsavedItemWithPrescription(create(INVOICE, FinancialAct.class));
    }

    /**
     * Verifies that an unsaved credit item can be deleted, when there is a prescription associated with the item's
     * product. As it is a credit item, the prescription shouldn't be used.
     */
    @Test
    public void testDeleteUnsavedCreditItem() {
        checkDeleteUnsavedItemWithPrescription(create(CREDIT, FinancialAct.class));
    }

    /**
     * Verifies that an unsaved counter item can be deleted, when there is a prescription associated with the item's
     * product. As it is a counter item, the prescription shouldn't be used.
     */
    @Test
    public void testDeleteUnsavedCounterItem() {
        checkDeleteUnsavedItemWithPrescription(create(COUNTER, FinancialAct.class));
    }

    /**
     * Verifies that the clinician is propagated to child acts.
     */
    @Test
    public void testInitClinician() {
        Entity alertType1 = createAlertType();
        Product product1 = productFactory.newMedication()
                .fixedPrice(1)
                .addAlertTypes(alertType1)
                .build();
        Entity reminderType1 = addReminder(product1);
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        addHL7Test(product1, investigationType1);
        Entity template1 = addDocumentTemplate(product1);

        FinancialAct charge = create(INVOICE, FinancialAct.class);
        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, ONE, queue);
        FinancialAct item = itemEditor.getObject();

        assertTrue(editor.isValid());
        checkPatientAndProductReadOnly(itemEditor, false);
        save(editor);
        checkPatientAndProductReadOnly(itemEditor, false);  // investigation not submitted

        Act medication = getBean(item).getTarget("dispensing", Act.class);
        assertNotNull(medication);

        Act reminder = getReminder(item, reminderType1);
        Act alert = getAlert(item, alertType1);
        Act investigation = getInvestigation(item, investigationType1);
        Act document = getDocument(item, template1);
        checkClinician(medication, clinician);
        checkClinician(reminder, clinician);
        checkClinician(alert, clinician);
        checkClinician(investigation, clinician);
        checkClinician(document, clinician);
    }

    /**
     * Verifies that the patient on an invoice item can be changed, and that this is reflected in the patient history.
     */
    @Test
    public void testChangePatient() {
        Product product1 = productFactory.newMedication().fixedPrice(1).build();
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        addHL7Test(product1, investigationType1);
        Entity template1 = addDocumentTemplate(product1);

        Party patient2 = patientFactory.createPatient(customer);

        FinancialAct charge = create(INVOICE, FinancialAct.class);
        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, ONE, editor.getQueue());
        FinancialAct item = itemEditor.getObject();

        assertTrue(editor.isValid());
        checkPatientAndProductReadOnly(itemEditor, false);
        save(editor);
        checkPatientAndProductReadOnly(itemEditor, false);  // investigation not submitted

        Act event1 = records.getEvent(patient);  // get the clinical event
        Act medication = getBean(item).getTarget("dispensing", Act.class);
        assertNotNull(event1);
        assertNotNull(medication);

        Act investigation = getInvestigation(item, investigationType1);
        Act document = getDocument(item, template1);

        checkEventRelationships(event1, item, medication, investigation, document);

        // recreate the editor, and change the patient to patient2.
        editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        assertTrue(editor.isValid());

        item = (FinancialAct) editor.getItems().getActs().get(0);
        itemEditor = editor.getEditor(item);
        itemEditor.getComponent();
        itemEditor.setPatient(patient2);
        checkSavePopup(editor.getQueue(), InvestigationArchetypes.PATIENT_INVESTIGATION, false);
        assertTrue(editor.isValid());
        save(editor);

        event1 = get(event1);
        medication = get(medication);

        // verify that the records are now linked to event2
        Act event2 = records.getEvent(patient2);
        investigation = getInvestigation(item, investigationType1);
        document = getDocument(item, template1);

        checkEventRelationships(event2, item, medication, investigation, document);

        // event1 should no longer be linked to any acts
        assertEquals(0, event1.getSourceActRelationships().size());
    }

    /**
     * Verifies that changing a quantity on a pharmacy order sends an update.
     */
    @Test
    public void testChangePharmacyOrderQuantity() {
        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        Product product = productFactory.newMedication().fixedPrice(10).pharmacy(pharmacy).build();

        Act visit = patientFactory.createVisit(patient);
        // pre-create the visit, otherwise one will be created automatically

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product, TEN, queue);
        FinancialAct item = itemEditor.getObject();
        save(editor);

        List<Order> orders = editor.getPharmacyOrderService().getOrders(true);
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), Order.Type.CREATE, patient, product, TEN, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService())
                .patient(patient)
                .product(product)
                .quantity(TEN)
                .fixedPrice(11)
                .tax(1)
                .total(11)
                .clinician(clinician)
                .createdBy(author)
                .visit(visit)
                .status(InvoiceItemStatus.ORDERED)
                .receivedQuantity(null)
                .returnedQuantity(null);
        verifier.verify(get(item));
        editor.getPharmacyOrderService().clear();

        itemEditor.setQuantity(ONE);
        save(editor);

        orders = editor.getPharmacyOrderService().getOrders(true);
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), Order.Type.UPDATE, patient, product, ONE, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);
        verifier.quantity(ONE)
                .verify(get(item));
    }

    /**
     * Verifies that deleting an invoice item with a pharmacy order cancels the order.
     */
    @Test
    public void testDeleteInvoiceItemWithPharmacyOrder() {
        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        Product product1 = productFactory.newMedication().fixedPrice(10).pharmacy(pharmacy).build();
        Product product2 = productFactory.newMerchandise().fixedPrice(10).pharmacy(pharmacy).build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        Act item1 = addItem(editor, patient, product1, TEN, queue).getObject();
        Act item2 = addItem(editor, patient, product2, ONE, queue).getObject();
        save(editor);

        List<Order> orders = editor.getPharmacyOrderService().getOrders(true);
        assertEquals(2, orders.size());
        checkOrder(orders.get(0), Order.Type.CREATE, patient, product1, TEN, item1.getId(),
                   item1.getActivityStartTime(), clinician, pharmacy);
        checkOrder(orders.get(1), Order.Type.CREATE, patient, product2, ONE, item2.getId(),
                   item2.getActivityStartTime(), clinician, pharmacy);
        editor.getPharmacyOrderService().clear();

        editor.delete(item1);
        save(editor);

        orders = editor.getPharmacyOrderService().getOrders(true);
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), Order.Type.CANCEL, patient, product1, TEN, item1.getId(),
                   item1.getActivityStartTime(), clinician, pharmacy);
    }

    /**
     * Verifies that when a one-way pharmacy is used, the receivedQuantity on the invoice item is automatically
     * populated to be the same as the quantity, to avoid reconciliation issues.
     */
    @Test
    public void testOneWayPharmacy() {
        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, true, userFactory.createUser());
        Product product1 = productFactory.newMedication().fixedPrice(10).pharmacy(pharmacy).build();

        Act visit = patientFactory.createVisit(patient);
        // pre-create the visit, otherwise one will be created automatically

        FinancialAct charge = create(INVOICE, FinancialAct.class);
        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, TEN, queue);
        FinancialAct item = itemEditor.getObject();
        save(editor);

        List<Order> orders1 = editor.getPharmacyOrderService().getOrders();
        assertEquals(1, orders1.size());
        checkOrder(orders1.get(0), Order.Type.CREATE, patient, product1, TEN, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);
        TestInvoiceItemVerifier verifier = new TestInvoiceItemVerifier(getArchetypeService())
                .patient(patient)
                .product(product1)
                .quantity(TEN)
                .fixedPrice(11)
                .tax(1)
                .total(11)
                .clinician(clinician)
                .createdBy(author)
                .visit(visit)
                .status(InvoiceItemStatus.ORDERED)
                .receivedQuantity(TEN)    // receivedQuantity should match invoice quantity
                .returnedQuantity(null);
        verifier.verify(get(item));

        editor.getPharmacyOrderService().clear();

        // change the quantity and verify the receivedQuantity updates
        itemEditor.setQuantity(ONE);
        save(editor);
        List<Order> orders2 = editor.getPharmacyOrderService().getOrders();
        assertEquals(1, orders2.size());
        checkOrder(orders2.get(0), Order.Type.UPDATE, patient, product1, ONE, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);

        verifier.quantity(ONE)
                .receivedQuantity(ONE)
                .verify(get(item));
    }

    /**
     * Verifies that deleting an invoice item with a laboratory order cancels the order.
     */
    @Test
    public void testDeleteInvoiceItemWithLaboratoryOrder() {
        Entity laboratory = createHL7Laboratory(location);
        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory);
        Product product1 = productFactory.newService().fixedPrice(10).build();
        addHL7Test(product1, investigationType1);
        Product product2 = productFactory.newMerchandise().fixedPrice(10).build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, TEN, queue);
        CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, ONE, queue);
        Act item1 = itemEditor1.getObject();
        save(editor);
        checkPatientAndProductReadOnly(itemEditor1, false);
        checkPatientAndProductReadOnly(itemEditor2, false);
        editor.postSave(queue, false, new MutableBoolean(false));  // submit lab orders
        checkPatientAndProductReadOnly(itemEditor1, true);
        checkPatientAndProductReadOnly(itemEditor2, false);

        Act investigation1 = getInvestigation(item1, investigationType1);

        List<LabOrder> orders = editor.getLaboratoryOrderService().getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), LabOrder.Type.CREATE, patient, investigation1.getId(),
                   investigation1.getActivityStartTime(), clinician, laboratory);
        editor.getLaboratoryOrderService().clear();

        editor.delete(item1);
        save(editor);

        orders = editor.getLaboratoryOrderService().getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), LabOrder.Type.CANCEL, patient, investigation1.getId(),
                   investigation1.getActivityStartTime(), clinician, laboratory);
    }

    /**
     * Verifies an investigation can be deleted prior to being saved.
     */
    @Test
    public void testDeleteInvestigationPriorToSave() {
        Entity laboratory = createHL7Laboratory(location);
        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory);
        Product product1 = productFactory.newService().fixedPrice(10).build();
        addHL7Test(product1, investigationType1);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, TEN, queue);
        FinancialAct item = itemEditor.getObject();

        InvestigationManager manager = editor.getItems().getEditContext().getInvestigations();
        List<DocumentAct> investigations = manager.getInvestigations();
        assertEquals(1, investigations.size());
        assertEquals(1, manager.getInvestigations(item).size());

        // remove the investigation
        assertTrue(manager.removeInvestigation(investigations.get(0)));

        // verify it is no longer referenced
        assertEquals(0, manager.getInvestigations().size());
        assertEquals(0, manager.getInvestigations(item).size());
    }

    /**
     * Verifies an investigation can be deleted after being saved.
     */
    @Test
    public void testDeleteInvestigationPostSave() {
        Entity laboratory = laboratoryFactory.createLaboratory(location);
        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory);
        Product product1 = productFactory.newService()
                .fixedPrice(10)
                .addTests(laboratoryFactory.createTest(investigationType1))
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, TEN, queue);
        FinancialAct item = itemEditor.getObject();

        InvestigationManager manager = editor.getItems().getEditContext().getInvestigations();
        List<DocumentAct> investigations = manager.getInvestigations();
        assertEquals(1, investigations.size());
        assertEquals(1, manager.getInvestigations(item).size());

        save(editor);
        editor.postSave(queue, false, new MutableBoolean(false));

        DocumentAct investigation = investigations.get(0);
        assertFalse(investigation.isNew()); // has been saved
        IMObjectBean bean = getBean(investigation);

        // verify no order was created
        Act order = bean.getTarget("order", Act.class);
        assertNull(order);

        // remove the investigation
        assertTrue(manager.removeInvestigation(investigation));

        // verify it is no longer referenced
        assertEquals(0, manager.getInvestigations().size());
        assertEquals(0, manager.getInvestigations(item).size());

        save(editor);

        // verify it no longer exists
        assertNull(get(investigation));
    }

    /**
     * Verifies an investigation can be deleted after being saved.
     */
    @Test
    public void testDeleteInvestigationAfterSubmission() {
        Entity laboratory = createHL7Laboratory(location);
        Entity investigationType1 = laboratoryFactory.createInvestigationType(laboratory);
        Product product1 = productFactory.newService().fixedPrice(10).build();
        addHL7Test(product1, investigationType1);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, TEN, queue);
        FinancialAct item = itemEditor.getObject();

        InvestigationManager manager = editor.getItems().getEditContext().getInvestigations();
        List<DocumentAct> investigations = manager.getInvestigations();
        assertEquals(1, investigations.size());
        assertEquals(1, manager.getInvestigations(item).size());

        checkPatientAndProductReadOnly(itemEditor, false);
        save(editor);
        editor.postSave(queue, false, new MutableBoolean(false));  // submit lab orders
        checkPatientAndProductReadOnly(itemEditor, true);

        DocumentAct investigation = investigations.get(0);
        assertFalse(investigation.isNew()); // has been saved

        // remove the investigation
        assertTrue(manager.removeInvestigation(investigation));

        // verify it is no longer referenced
        assertEquals(0, manager.getInvestigations().size());
        assertEquals(0, manager.getInvestigations(item).size());

        save(editor);
        checkPatientAndProductReadOnly(itemEditor, true);

        // verify it no longer exists
        assertNull(get(investigation));
    }

    /**
     * Tests expansion for invoices.
     */
    @Test
    public void testTemplateExpansionForInvoice() {
        checkTemplateExpansion(INVOICE, INVOICE_ITEM, 1);
    }

    /**
     * Tests expansion for credits.
     */
    @Test
    public void testTemplateExpansionForCredit() {
        checkTemplateExpansion(CREDIT, CREDIT_ITEM, 0);
    }

    /**
     * Tests expansion for counter sales.
     */
    @Test
    public void testTemplateExpansionForCounter() {
        checkTemplateExpansion(COUNTER, COUNTER_ITEM, 0);
    }

    /**
     * Verifies that if a template is expanded, and a product is subsequently replaced with one not from a template,
     * the template reference is removed.
     */
    @Test
    public void testChangeTemplateProduct() {
        BigDecimal fixedPrice = ONE;

        Product product1 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product product2 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, template, null, queue);
        assertEquals(product1, itemEditor.getProduct());
        assertEquals(template, itemEditor.getTemplate());

        itemEditor.setProduct(product2);
        assertNull(itemEditor.getTemplate());

        save(editor);

        itemEditor.setProduct(template);
        assertEquals(product1, itemEditor.getProduct());
        assertEquals(template, itemEditor.getTemplate());
    }

    /**
     * Verifies that the invoice total is updated if a medication quantity is changed during template expansion.
     */
    @Test
    public void testChangeMedicationQuantityDuringTemplateExpansion() {
        Product product1 = productFactory.newMedication().fixedPrice(0).unitPrice(1).build();
        Product product2 = productFactory.newMedication().fixedPrice(0).unitPrice(1).build();
        Product product3 = productFactory.newMedication().fixedPrice(0).unitPrice(1).build();
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .newInclude().product(product2).quantity(1).add()
                .newInclude().product(product3).quantity(1).add()
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        itemEditor.getComponent();
        itemEditor.setPatient(patient);
        itemEditor.setProduct(template);
        checkMedication(product1, ONE, null, queue);
        checkMedication(product2, ONE, TEN, queue);
        checkMedication(product3, ONE, null, queue);
        checkEquals(new BigDecimal("13.20"), charge.getTotal());

        save(editor);
    }

    /**
     * Verifies that existing reminders are completed if a product is used with the same reminder type.
     */
    @Test
    public void testMarkMatchingRemindersCompleted() {
        Product product1 = productFactory.createMedication();
        Entity reminderType = addReminder(product1);

        Act existing = ReminderTestHelper.createReminder(patient, reminderType);
        assertEquals(ActStatus.IN_PROGRESS, existing.getStatus());
        save(existing);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act reminder = getReminder(itemEditor.getObject(), reminderType);
        existing = get(existing);
        reminder = get(reminder);

        assertEquals(ActStatus.COMPLETED, existing.getStatus());
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());
    }

    /**
     * Verifies that when products are changed from one with a reminder to one without, the reminder is deleted.
     */
    @Test
    public void testChangeProductWithReminderToOneWithout() {
        // create a product with a reminder
        Product product1 = productFactory.createMedication();
        Entity reminderType1 = addReminder(product1);

        // and one without
        Product product2 = productFactory.createMedication();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act reminder = getReminder(itemEditor.getObject(), reminderType1);
        assertEquals(ActStatus.IN_PROGRESS, reminder.getStatus());

        itemEditor.setProduct(product2);
        save(editor);

        assertNull(get(reminder));
    }

    /**
     * Verifies that the product can be changed multiple times with reminders.
     */
    @Test
    public void testChangeProductWithReminders() {
        // create a product with a reminder
        Product product1 = productFactory.createMedication();
        Entity reminderType1 = addReminder(product1);

        // and one without
        Product product2 = productFactory.createMedication();

        // and a 3rd with the same reminder type
        Product product3 = productFactory.createMedication();
        addReminder(product3, reminderType1);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act reminder1 = getReminder(itemEditor.getObject(), reminderType1);
        assertEquals(ActStatus.IN_PROGRESS, reminder1.getStatus());

        // change to a product with no reminder
        itemEditor.setProduct(product2);

        // immediately change to a product with a reminder
        itemEditor.setProduct(product3);

        // make sure there is popup, as the reminder is interactive
        checkSavePopup(queue, ReminderArchetypes.REMINDER, false);
        save(editor);

        // make sure reminder1 has been deleted, and reminder2 is IN_PROGRESS
        assertNull(get(reminder1));
        Act reminder2 = getReminder(itemEditor.getObject(), reminderType1);
        assertEquals(ActStatus.IN_PROGRESS, reminder2.getStatus());
    }

    /**
     * Verifies that existing alerts are completed if a product is used with the same alert type.
     */
    @Test
    public void testMarkMatchingAlertsCompleted() {
        Entity alertType = createAlertType();
        Product product1 = productFactory.newMedication()
                .addAlertTypes(alertType)
                .build();

        Act existing = patientFactory.createAlert(patient, alertType);
        assertEquals(ActStatus.IN_PROGRESS, existing.getStatus());

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act alert = getAlert(itemEditor.getObject(), alertType);
        existing = get(existing);
        alert = get(alert);

        assertEquals(ActStatus.COMPLETED, existing.getStatus());
        assertEquals(ActStatus.IN_PROGRESS, alert.getStatus());
    }

    /**
     * Verifies that when products are changed from one with an alert to one without, the alert is deleted.
     */
    @Test
    public void testChangeProductWithAlertToOneWithout() {
        // create a product with an alert
        Entity alertType = createAlertType();
        Product product1 = productFactory.newMedication()
                .addAlertTypes(alertType)
                .build();

        // and one without
        Product product2 = productFactory.createMedication();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act alert = getAlert(itemEditor.getObject(), alertType);
        assertEquals(ActStatus.IN_PROGRESS, alert.getStatus());

        itemEditor.setProduct(product2);
        save(editor);

        assertNull(get(alert));
    }

    /**
     * Verifies that the product can be changed multiple times with alerts.
     */
    @Test
    public void testChangeProductWithAlerts() {
        // create a product with an alert
        Entity alertType1 = createAlertType();
        Product product1 = productFactory.newMedication()
                .addAlertTypes(alertType1)
                .build();

        // and one without
        Product product2 = productFactory.createMedication();

        // and a 3rd with the same alert type
        Product product3 = productFactory.newMedication()
                .addAlertTypes(alertType1)
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, BigDecimal.ONE, queue);
        save(editor);

        Act alert1 = getAlert(itemEditor.getObject(), alertType1);
        assertEquals(ActStatus.IN_PROGRESS, alert1.getStatus());

        // change to a product with no alert
        itemEditor.setProduct(product2);

        // immediately change to a product with an alert
        itemEditor.setProduct(product3);

        // make sure there is popup, as the alert is interactive
        checkSavePopup(queue, PatientArchetypes.ALERT, false);
        save(editor);

        // make sure alert1 has been deleted, and alert2 is IN_PROGRESS
        assertNull(get(alert1));
        Act alert2 = getAlert(itemEditor.getObject(), alertType1);
        assertEquals(ActStatus.IN_PROGRESS, alert2.getStatus());
    }

    /**
     * Tests the {@link DefaultCustomerChargeActEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        FinancialAct charge = create(INVOICE, FinancialAct.class);

        DefaultCustomerChargeActEditor editor = new DefaultCustomerChargeActEditor(charge, null, layoutContext);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof DefaultCustomerChargeActEditor);
    }

    /**
     * Verifies that when a template expands, any visit notes are added to the associated patient's history.
     */
    @Test
    public void testTemplateVisitNotes() {
        Party patient2 = patientFactory.createPatient(customer);
        BigDecimal fixedPrice = new BigDecimal("0.91");
        Product productA = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product productB = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product template1 = productFactory.newTemplate()
                .visitNote("template 1 notes")
                .newInclude().product(productA).quantity(1).add()
                .newInclude().product(productB).quantity(1).add()
                .build();

        Product template2 = productFactory.newTemplate()
                .visitNote("template 2 notes")
                .newInclude().product(productA).quantity(1).add()
                .newInclude().product(productB).quantity(1).add()
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        addItem(editor, patient, template1, null, queue);
        addItem(editor, patient2, template2, null, queue);
        save(editor);

        // verify that there are 4 items in the invoice, but only two notes, one for each template
        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(4, items.size());

        IMObjectBean item1 = checkItem(items, INVOICE_ITEM, patient, productA, template1, 0, author, clinician, ONE,
                                       ONE, ZERO, ZERO, ZERO, ONE, ZERO, new BigDecimal("0.091"), ONE, true, null, 1);
        IMObjectBean item2 = checkItem(items, INVOICE_ITEM, patient2, productA, template2, 1, author, clinician, ONE,
                                       ONE, ZERO, ZERO, ZERO, ONE, ZERO, new BigDecimal("0.091"), ONE, true, null, 1);
        checkChargeEventNote(item1, patient, "template 1 notes");
        checkChargeEventNote(item2, patient2, "template 2 notes");
    }

    /**
     * Verifies that if a clinician is set before template expansion, this appears on all acts produced by the template
     * expansion.
     */
    @Test
    public void testSetClinicianBeforeTemplateExpansion() {
        User clinician2 = userFactory.createClinician();
        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMedication();
        Product product3 = productFactory.createMedication();
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .newInclude().product(product2).quantity(2).add()
                .newInclude().product(product3).quantity(3).add()
                .build();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        itemEditor.setClinician(clinician2);
        setItem(editor, itemEditor, patient, template, BigDecimal.ONE, queue);
        save(editor);

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());

        checkItem(items, INVOICE_ITEM, patient, product1, template, 0, author, clinician2, ONE, ONE, ZERO, ZERO, ZERO,
                  ZERO, ZERO, ZERO, ZERO, true, null, 1);
        BigDecimal two = BigDecimal.valueOf(2);
        checkItem(items, INVOICE_ITEM, patient, product2, template, 0, author, clinician2, two, two, ZERO, ZERO, ZERO,
                  ZERO, ZERO, ZERO, ZERO, true, null, 1);

        BigDecimal three = BigDecimal.valueOf(3);
        checkItem(items, INVOICE_ITEM, patient, product3, template, 0, author, clinician2, three, three, ZERO, ZERO,
                  ZERO, ZERO, ZERO, ZERO, ZERO, true, null, 1);
    }

    /**
     * Verifies the clinician can be changed.
     */
    @Test
    public void testChangeClinician() {
        User clinician2 = userFactory.createClinician();
        Product product = productFactory.createService();

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        setItem(editor, itemEditor, patient, product, BigDecimal.ONE, queue);

        save(editor);

        itemEditor.setClinician(null);
        save(editor);


        itemEditor.setClinician(clinician2);
        save(editor);
    }

    /**
     * Verifies clinician changes propagate to reminders.
     */
    @Test
    public void testChangeClinicianOnReminder() {
        User clinician2 = userFactory.createClinician();
        Product product = productFactory.createMedication();
        Entity reminderType = addReminder(product);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        setItem(editor, itemEditor, patient, product, BigDecimal.ONE, queue);

        save(editor);

        FinancialAct item = itemEditor.getObject();
        Act reminder = checkReminder(get(item), patient, product, reminderType, author, clinician);

        ReminderEditor reminderEditor = itemEditor.getReminderEditor(reminder.getObjectReference());
        assertNotNull(reminderEditor);
        reminderEditor.setClinician((User) null);
        save(editor);

        checkReminder(get(item), patient, product, reminderType, author, null);

        itemEditor.setClinician(null);
        save(editor);

        checkReminder(get(item), patient, product, reminderType, author, null);

        itemEditor.setClinician(clinician2);
        save(editor);

        checkReminder(get(item), patient, product, reminderType, author, clinician2);
    }

    /**
     * Verifies that changing the product from one with document templates to one without deletes the documents
     * on the invoice item.
     */
    @Test
    public void testDeleteDocumentOnProductChange() {
        // product with document templates
        Product product1 = productFactory.createService();
        Entity template1 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addDocumentTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);

        // product with no templates
        Product product2 = productFactory.createService();

        // product with single template
        Product product3 = productFactory.createService();
        Entity template3 = addDocumentTemplate(product3, PatientArchetypes.DOCUMENT_LETTER);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        setItem(editor, itemEditor, patient, product1, BigDecimal.ONE, queue);

        save(editor);

        Act event = records.getEvent(patient);  // get the clinical event
        assertNotNull(event);

        // verify the item has 2 documents
        FinancialAct item = itemEditor.getObject();
        IMObjectBean bean = getBean(item);
        assertEquals(2, bean.getTargets("documents").size());

        DocumentAct document1 = checkDocument(item, patient, product1, template1, author, clinician, false);
        DocumentAct document2 = checkDocument(item, patient, product1, template2, author, clinician, true);
        checkEventRelationship(event, document1);
        checkEventRelationship(event, document2);

        // change the product to one without document templates, and verify the documents are removed
        itemEditor.setProduct(product2);

        save(editor);

        assertTrue(bean.getTargets("documents").isEmpty());
        assertNull(get(document1));
        assertNull(get(document2));
        assertNull(get(document2.getDocument())); // content should also be deleted

        // change to another document, this time with a single template
        itemEditor.setProduct(product3);
        save(editor);

        assertEquals(1, bean.getTargets("documents").size());
        checkDocument(item, patient, product3, template3, author, clinician, true);
    }

    /**
     * Verifies that patient letters can be regenerated when the clinician changes.
     */
    @Test
    public void testChangeClinicianOnPatientLetter() {
        User clinician2 = userFactory.createClinician();
        Product product = productFactory.createService();

        // add a template to the product that prompts for parameters
        Entity template = addDocumentTemplate(product, PatientArchetypes.DOCUMENT_LETTER, true);

        FinancialAct charge = create(INVOICE, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = editor.addItem();
        setItem(editor, itemEditor, patient, product, BigDecimal.ONE, queue);

        save(editor);

        // parameter dialog should be displayed. Click OK
        ParameterDialog parameterDialog1 = findComponent(ParameterDialog.class);
        fireDialogButton(parameterDialog1, ConfirmationDialog.OK_ID);

        // verify the document has been created
        FinancialAct item = itemEditor.getObject();
        Act document1 = checkDocument(item, patient, product, template, author, clinician, true);

        // change the clinician and select Yes to recreate the Patient Letter
        itemEditor.setClinician(clinician2);
        ConfirmationDialog confirmationDialog = EchoTestHelper.getWindowPane(ConfirmationDialog.class);
        fireDialogButton(confirmationDialog, ConfirmationDialog.YES_ID);

        save(editor);

        // verify document1 now deleted
        assertNull(get(document1));

        // parameter dialog should be displayed. Click OK
        ParameterDialog parameterDialog2 = findComponent(ParameterDialog.class);
        fireDialogButton(parameterDialog2, ConfirmationDialog.OK_ID);

        // verify the document has been recreated
        checkDocument(item, patient, product, template, author, clinician2, true);
    }

    /**
     * Checks the patient and product read-only status
     *
     * @param editor   the charge item editor
     * @param readOnly if {@code true}, both should be read-only otherwise they are editable
     */
    private void checkPatientAndProductReadOnly(CustomerChargeActItemEditor editor, boolean readOnly) {
        try {
            assertEquals(readOnly, editor.isProductReadOnly());
            // need to use reflection as the methods shouldn't be public. Better approach would be to have a testing
            // subclass of CustomerChargeActItemEditor and make them public. TODO
            ProductParticipationEditor productEditor = (ProductParticipationEditor) MethodUtils.invokeMethod(
                    editor, true, "getProductEditor");
            assertNotNull(productEditor);
            assertEquals(readOnly, productEditor.isReadOnly());

            // patient editor shouldn't be available if read-only
            PatientParticipationEditor patientEditor = (PatientParticipationEditor) MethodUtils.invokeMethod(
                    editor, true, "getParticipationEditor", "patient", false);
            if (readOnly) {
                assertNull(patientEditor);
            } else {
                assertNotNull(patientEditor);
            }
        } catch (Exception exception) {
            fail(exception.getMessage());
        }
    }

    /**
     * Verifies there is a medication popup with the expected product and quantity.
     * <p/>
     * Closes the dialog on completion.
     *
     * @param product     the expected product
     * @param quantity    the expected quantity
     * @param newQuantity if non-null, updates the quantity
     * @param queue       the editor queue
     */
    private void checkMedication(Product product, BigDecimal quantity, BigDecimal newQuantity, EditorQueue queue) {
        PopupDialog dialog = queue.getCurrent();
        assertTrue(dialog instanceof EditDialog);
        IMObjectEditor editor = ((EditDialog) dialog).getEditor();
        assertTrue(editor instanceof PatientMedicationActEditor);
        PatientMedicationActEditor medicationEditor = (PatientMedicationActEditor) editor;
        assertEquals(product, medicationEditor.getProduct());
        checkEquals(quantity, medicationEditor.getQuantity());
        if (newQuantity != null) {
            medicationEditor.setQuantity(newQuantity);
        }
        assertTrue(editor.isValid());
        fireDialogButton(dialog, PopupDialog.OK_ID);
    }

    /**
     * Verifies a prescription can be selected during invoicing.
     *
     * @param prescriptionClinician the clinician assigned to the prescription
     * @param invoiceClinician      the clinician to use for invoicing. May be {@code null}
     */
    private void checkPrescription(User prescriptionClinician, User invoiceClinician) {
        BigDecimal fixedPrice = new BigDecimal("3.30");
        BigDecimal unitPrice = new BigDecimal("0.11");
        BigDecimal total = new BigDecimal("4.40");
        BigDecimal tax = new BigDecimal("0.40");

        Product product1 = productFactory.newMedication().fixedPrice("3.00").unitPrice("0.10").build(); // ex tax

        Act prescription = PrescriptionTestHelper.createPrescription(patient, product1, prescriptionClinician, 10);
        FinancialAct charge = create(INVOICE, FinancialAct.class);
        layoutContext.getContext().setClinician(invoiceClinician);
        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        CustomerChargeActItemEditor itemEditor = addItem(editor, patient, product1, null, queue);
        save(editor);

        checkPrescription(prescription, itemEditor);

        charge = get(charge);
        List<FinancialAct> items = getBean(charge).getTargets("items", FinancialAct.class);
        assertEquals(1, items.size());

        Act event = records.getEvent(patient);
        assertNotNull(event);

        checkItem(items, INVOICE_ITEM, patient, product1, null, -1, author, invoiceClinician, ZERO, TEN, ZERO, unitPrice,
                  ZERO, fixedPrice, ZERO, tax, total, true, event, 1);
    }

    /**
     * Tests template expansion.
     *
     * @param archetype     the charge archetype
     * @param itemArchetype the charge item archetype
     * @param childActs     the expected no. of child acts
     */
    private void checkTemplateExpansion(String archetype, String itemArchetype, int childActs) {
        BigDecimal fixedPrice = new BigDecimal("0.91");
        Entity discount = productFactory.newDiscount().percentage(10).discountFixedPrice(true).build();

        Product product1 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product product2 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product product3 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        addDiscount(product3, discount);
        addDiscount(customer, discount);                           // give customer a discount for product3
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).quantity(1).add()
                .newInclude().product(product2).quantity(2).add()
                .newInclude().product(product3).quantity(3).zeroPrice().add()
                .build();

        FinancialAct charge = create(archetype, FinancialAct.class);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();

        editor.getComponent();
        assertTrue(editor.isValid());

        addItem(editor, patient, template, null, queue);
        save(editor);

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());

        checkItem(items, itemArchetype, patient, product1, template, 0, author, clinician, ONE, ONE, ZERO, ZERO, ZERO,
                  ONE, ZERO, new BigDecimal("0.091"), ONE, true, null, childActs);
        BigDecimal two = BigDecimal.valueOf(2);
        checkItem(items, itemArchetype, patient, product2, template, 0, author, clinician, two, two, ZERO, ZERO, ZERO,
                  ONE, ZERO, new BigDecimal("0.091"), ONE, true, null, childActs);

        // verify that product3 is charged at zero price
        BigDecimal three = BigDecimal.valueOf(3);
        checkItem(items, itemArchetype, patient, product3, template, 0, author, clinician, three, three, ZERO, ZERO,
                  ZERO, ZERO, ZERO, ZERO, ZERO, true, null, childActs);
    }

    /**
     * Verifies that an unsaved charge item can be deleted, when there is a prescription associated with the item's
     * product.
     */
    private void checkDeleteUnsavedItemWithPrescription(FinancialAct charge) {
        Product product1 = productFactory.newMedication().fixedPrice(1).build();
        Product product2 = productFactory.newMedication().fixedPrice(1).build();

        Act prescription = PrescriptionTestHelper.createPrescription(patient, product1, clinician);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        // add items for product1 and product2, but delete product1 item before save
        CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, ONE, queue);
        addItem(editor, patient, product2, ONE, queue);
        editor.removeItem(itemEditor1.getObject());
        assertTrue(editor.isValid());
        save(editor);

        // verify there are no acts linked to the prescription
        prescription = get(prescription);
        IMObjectBean bean = getBean(prescription);
        assertTrue(bean.getTargets("dispensing").isEmpty());

        // reload the charge and verify the editor is valid
        charge = get(charge);
        editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that the {@link DefaultCustomerChargeActEditor#delete()} method deletes a charge and its items.
     *
     * @param charge the charge
     */
    private void checkDeleteCharge(FinancialAct charge) {
        BigDecimal fixedPrice = BigDecimal.TEN;
        BigDecimal itemTotal = BigDecimal.valueOf(11);
        BigDecimal total = itemTotal.multiply(BigDecimal.valueOf(3));

        Product product1 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        Product product2 = productFactory.newMerchandise().fixedPrice(fixedPrice).build();
        Product product3 = productFactory.newService().fixedPrice(fixedPrice).build();

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        BigDecimal quantity = ONE;
        addItem(editor, patient, product1, quantity, queue);
        addItem(editor, patient, product2, quantity, queue);
        addItem(editor, patient, product3, quantity, queue);

        save(editor);
        BigDecimal balance = (charge.isCredit()) ? total.negate() : total;
        checkBalance(customer, balance, ZERO);

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());

        assertTrue(delete(editor));
        assertNull(get(charge));
        for (FinancialAct item : items) {
            assertNull(get(item));
        }

        checkBalance(customer, ZERO, ZERO);
    }

    /**
     * Tests editing a charge with no items.
     *
     * @param charge the charge
     */
    private void checkEmptyCharge(FinancialAct charge) {
        DefaultCustomerChargeActEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        assertTrue(editor.isValid());
        editor.save();
        checkBalance(customer, ZERO, ZERO);

        editor.setStatus(ActStatus.POSTED);
        editor.save();
        checkBalance(customer, ZERO, ZERO);
    }

    /**
     * Tests editing of a charge.
     *
     * @param archetype                       the charge archetype
     * @param itemArchetype                   the charge item archetype
     * @param discontinueOrdersOnFinalisation if {@code true}, discontinue orders on finalisation
     */
    private void checkEditCharge(String archetype, String itemArchetype, boolean discontinueOrdersOnFinalisation) {
        if (discontinueOrdersOnFinalisation) {
            setPharmacyOrderDiscontinuePeriod(0);
        } else {
            setPharmacyOrderDiscontinuePeriod(60);
        }

        Party location = practiceFactory.newLocation().stockControl().build();   // enable stock control
        Party stockLocation = practiceFactory.createStockLocation(location);
        layoutContext.getContext().setLocation(location);
        layoutContext.getContext().setStockLocation(stockLocation);

        BigDecimal fixedPrice = BigDecimal.TEN;
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(11);
        BigDecimal itemTax = BigDecimal.valueOf(1);
        BigDecimal itemTotal = BigDecimal.valueOf(11);
        BigDecimal tax = itemTax.multiply(BigDecimal.valueOf(3));
        BigDecimal total = itemTotal.multiply(BigDecimal.valueOf(3));
        Entity pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        Entity laboratory = createHL7Laboratory(location);

        FinancialAct charge = create(archetype, FinancialAct.class);
        boolean invoice = TypeHelper.isA(charge, INVOICE);

        Product product1 = productFactory.newMedication()
                .fixedPrice(fixedPrice)
                .pharmacy(pharmacy)
                .addAlertTypes(createAlertType())
                .build();
        addReminder(product1);
        addHL7Test(product1);
        addDocumentTemplate(product1);
        int product1Acts = invoice ? 5 : 0;

        Product product2 = productFactory.newMerchandise()
                .fixedPrice(fixedPrice)
                .pharmacy(pharmacy)
                .addAlertTypes(createAlertType())
                .build();
        addReminder(product2);
        addHL7Test(product2);
        addDocumentTemplate(product2, PatientArchetypes.DOCUMENT_LETTER);
        int product2Acts = invoice ? 4 : 0;

        Product product3 = productFactory.newService()
                .fixedPrice(fixedPrice)
                .addAlertTypes(createAlertType())
                .build();
        addReminder(product3);
        Entity investigationType3 = laboratoryFactory.createInvestigationType(laboratory);
        addHL7Test(product3, investigationType3);
        addDocumentTemplate(product3);
        int product3Acts = invoice ? 4 : 0;

        BigDecimal product1Stock = BigDecimal.valueOf(100);
        BigDecimal product2Stock = BigDecimal.valueOf(50);
        initStock(product1, stockLocation, product1Stock);
        initStock(product2, stockLocation, product2Stock);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        EditorQueue queue = editor.getQueue();
        editor.getComponent();
        assertTrue(editor.isValid());

        BigDecimal quantity = ONE;
        CustomerChargeActItemEditor itemEditor1 = addItem(editor, patient, product1, quantity, queue);
        CustomerChargeActItemEditor itemEditor2 = addItem(editor, patient, product2, quantity, queue);
        CustomerChargeActItemEditor itemEditor3 = addItem(editor, patient, product3, quantity, queue);
        Act item1 = itemEditor1.getObject();
        Act item2 = itemEditor2.getObject();
        Act item3 = itemEditor3.getObject();

        assertTrue(editor.isValid());
        save(editor);

        if (invoice) {
            checkPatientAndProductReadOnly(itemEditor1, true);
            checkPatientAndProductReadOnly(itemEditor2, true);
            checkPatientAndProductReadOnly(itemEditor3, false);
            // PENDING lab orders not submitted until postSave() invoked
            assertEquals(InvoiceItemStatus.ORDERED, item1.getStatus());
            assertEquals(InvoiceItemStatus.ORDERED, item2.getStatus());
            assertNull(item3.getStatus());  // PENDING lab orders not submitted until postSave() invoked

            editor.postSave(queue, true, new MutableBoolean(false)); // submit lab orders
            assertEquals(InvoiceItemStatus.ORDERED, item1.getStatus());
            assertEquals(InvoiceItemStatus.ORDERED, item2.getStatus());
            assertEquals(InvoiceItemStatus.ORDERED, item3.getStatus());
            checkPatientAndProductReadOnly(itemEditor1, true);
            checkPatientAndProductReadOnly(itemEditor2, true);
            checkPatientAndProductReadOnly(itemEditor3, true);
        } else {
            assertNull(item1.getStatus());
            assertNull(item2.getStatus());
            assertNull(item3.getStatus());
        }

        BigDecimal balance = (charge.isCredit()) ? total.negate() : total;
        checkBalance(customer, balance, ZERO);
        editor.setStatus(ActStatus.POSTED);
        editor.save();
        checkBalance(customer, ZERO, balance);

        if (invoice) {
            List<Order> orders = editor.getPharmacyOrderService().getOrders(true);
            if (discontinueOrdersOnFinalisation) {
                assertEquals(4, orders.size());
            } else {
                assertEquals(2, orders.size());
            }
            int index = 0;
            checkOrder(orders.get(index++), Order.Type.CREATE, patient, product1, quantity, item1.getId(),
                       item1.getActivityStartTime(), clinician, pharmacy);
            if (discontinueOrdersOnFinalisation) {
                checkOrder(orders.get(index++), Order.Type.DISCONTINUE, patient, product1, quantity, item1.getId(),
                           item1.getActivityStartTime(), clinician, pharmacy);
            }
            checkOrder(orders.get(index++), Order.Type.CREATE, patient, product2, quantity, item2.getId(),
                       item2.getActivityStartTime(), clinician, pharmacy);
            if (discontinueOrdersOnFinalisation) {
                checkOrder(orders.get(index), Order.Type.DISCONTINUE, patient, product2, quantity, item2.getId(),
                           item2.getActivityStartTime(), clinician, pharmacy);
            }
            editor.getPharmacyOrderService().clear();
            String status = (discontinueOrdersOnFinalisation) ? InvoiceItemStatus.DISCONTINUED
                                                              : InvoiceItemStatus.ORDERED;
            assertEquals(status, item1.getStatus());
            assertEquals(status, item2.getStatus());
            assertEquals(status, item3.getStatus());

            List<LabOrder> labOrders = editor.getLaboratoryOrderService().getOrders();
            assertEquals(1, labOrders.size());

            Act investigation = getInvestigation(item3, investigationType3);
            checkOrder(labOrders.get(0), LabOrder.Type.CREATE, patient, investigation.getId(),
                       investigation.getActivityStartTime(), clinician, laboratory);
            editor.getLaboratoryOrderService().clear();
        } else {
            assertNull(editor.getPharmacyOrderService());
            assertNull(editor.getLaboratoryOrderService());
            assertNull(item1.getStatus());
            assertNull(item2.getStatus());
            assertNull(item3.getStatus());
        }

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());
        checkCharge(charge, customer, author, clinician, tax, total);

        Act event = records.getEvent(patient);  // get the clinical event. Should be null if not an invoice
        if (invoice) {
            assertNotNull(event);
            checkEvent(event, patient, author, clinician, location, ActStatus.COMPLETED);
        } else {
            assertNull(event);
        }

        BigDecimal discount = ZERO;
        checkItem(items, itemArchetype, patient, product1, null, -1, author, clinician, ZERO, quantity, ZERO, ZERO,
                  ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, product1Acts);
        checkItem(items, itemArchetype, patient, product2, null, -1, author, clinician, ZERO, quantity, ZERO, ZERO,
                  ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, product2Acts);
        checkItem(items, itemArchetype, patient, product3, null, -1, author, clinician, ZERO, quantity, ZERO, ZERO,
                  ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, product3Acts);

        boolean add = bean.isA(CREDIT);
        checkStock(product1, stockLocation, product1Stock, quantity, add);
        checkStock(product2, stockLocation, product2Stock, quantity, add);
        checkStock(product3, stockLocation, ZERO, ZERO, add);
    }

    /**
     * Verifies stock quantities update for products used in a charge.
     *
     * @param charge the charge
     */
    private void checkChargeStockUpdate(FinancialAct charge) {
        Party location = practiceFactory.newLocation().stockControl().build();   // enable stock control
        Party stockLocation = practiceFactory.createStockLocation(location);
        layoutContext.getContext().setLocation(location);
        layoutContext.getContext().setStockLocation(stockLocation);

        Product product1 = productFactory.createMedication();
        Product product2 = productFactory.createMerchandise();
        Product product3 = productFactory.createService();
        Product product4 = productFactory.createMerchandise();

        BigDecimal product1InitialStock = BigDecimal.valueOf(100);
        BigDecimal product2InitialStock = BigDecimal.valueOf(50);
        BigDecimal product4InitialStock = BigDecimal.valueOf(25);
        initStock(product1, stockLocation, product1InitialStock);
        initStock(product2, stockLocation, product2InitialStock);
        initStock(product4, stockLocation, product4InitialStock);

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        assertTrue(editor.isValid());

        BigDecimal quantity1 = BigDecimal.valueOf(5);
        BigDecimal quantity2 = TEN;
        BigDecimal quantity4a = BigDecimal.ONE;
        BigDecimal quantity4b = TEN;
        BigDecimal quantity4 = quantity4a.add(quantity4b);

        CustomerChargeActItemEditor item1 = addItem(editor, patient, product1, quantity1, editor.getQueue());
        CustomerChargeActItemEditor item2 = addItem(editor, patient, product2, quantity2, editor.getQueue());
        addItem(editor, patient, product4, quantity4a, editor.getQueue());
        addItem(editor, patient, product4, quantity4b, editor.getQueue());
        save(editor);

        boolean add = TypeHelper.isA(charge, CREDIT);
        BigDecimal product1Stock = checkStock(product1, stockLocation, product1InitialStock, quantity1, add);
        BigDecimal product2Stock = checkStock(product2, stockLocation, product2InitialStock, quantity2, add);
        checkStock(product4, stockLocation, product4InitialStock, quantity4, add);

        item1.setQuantity(ZERO);
        item1.setQuantity(quantity1);
        assertTrue(item1.isModified());
        item2.setQuantity(ZERO);
        item2.setQuantity(quantity2);
        assertTrue(item2.isModified());
        save(editor);
        checkStock(product1, stockLocation, product1Stock);
        checkStock(product2, stockLocation, product2Stock);

        item1.setQuantity(BigDecimal.valueOf(10)); // change product1 stock quantity
        item2.setProduct(product3);                // change the product and verify the stock for product2 reverts
        save(editor);
        checkStock(product1, stockLocation, product1Stock, BigDecimal.valueOf(5), add);
        checkStock(product2, stockLocation, product2Stock, quantity2, !add);

        // now delete the charge and verify the stock reverts
        assertTrue(delete(editor));
        checkStock(product1, stockLocation, product1InitialStock);
        checkStock(product2, stockLocation, product2InitialStock);
        checkStock(product4, stockLocation, product4InitialStock);
    }

    /**
     * Tests template expansion.
     *
     * @param charge        the charge to edit
     * @param itemArchetype the charge item archetype
     */
    private void checkExpandTemplate(FinancialAct charge, String itemArchetype) {
        Party location = practiceFactory.newLocation().stockControl().build();   // enable stock control
        Party stockLocation = practiceFactory.createStockLocation(location);
        layoutContext.getContext().setLocation(location);
        layoutContext.getContext().setStockLocation(stockLocation);

        // add a patient weight
        patientFactory.createWeight(patient, new BigDecimal("4.2"), WeightUnits.KILOGRAMS);

        BigDecimal quantity = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = BigDecimal.TEN;
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(11);
        BigDecimal discount = ZERO;
        BigDecimal itemTax = BigDecimal.valueOf(1);
        BigDecimal itemTotal = BigDecimal.valueOf(11);
        BigDecimal tax = itemTax.multiply(BigDecimal.valueOf(3));
        BigDecimal total = itemTotal.multiply(BigDecimal.valueOf(3));

        Product product1 = productFactory.newMedication().fixedPrice(fixedPrice).build();
        productFactory.updateMedication(product1)
                .newDose().weightRange(0, 10).rate(1).quantity(1).add()
                .concentration(1)
                .build();
        Product product2 = productFactory.newMerchandise().fixedPrice(fixedPrice).build();
        Product product3 = productFactory.newService().fixedPrice(fixedPrice).build();
        Product template = productFactory.newTemplate()
                .newInclude().product(product1).add()
                .newInclude().product(product2).add()
                .newInclude().product(product3).add()
                .build();

        TestChargeEditor editor = createCustomerChargeActEditor(charge, layoutContext);
        editor.getComponent();
        CustomerChargeTestHelper.addItem(editor, patient, template, null, editor.getQueue());

        boolean invoice = charge.isA(INVOICE);
        int product1Acts = 0;     // expected child acts for product1
        if (invoice) {
            product1Acts++;
        }

        save(editor);
        BigDecimal balance = (charge.isCredit()) ? total.negate() : total;
        checkBalance(customer, balance, ZERO);

        charge = get(charge);
        IMObjectBean bean = getBean(charge);
        List<FinancialAct> items = bean.getTargets("items", FinancialAct.class);
        assertEquals(3, items.size());
        checkCharge(charge, customer, author, clinician, tax, total);
        Act event = records.getEvent(patient);  // get the clinical event. Should be null if not an invoice
        if (invoice) {
            assertNotNull(event);
            checkEvent(event, patient, author, clinician, location, ActStatus.IN_PROGRESS);
        } else {
            assertNull(event);
        }

        if (TypeHelper.isA(charge, COUNTER)) {
            checkItem(items, itemArchetype, patient, product1, template, 0, author, clinician, BigDecimal.ONE, quantity,
                      ZERO, ZERO, ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, product1Acts);
        } else {
            // quantity derived from the product dose. As there is no unit price, the totals don't change
            checkItem(items, itemArchetype, patient, product1, template, 0, author, clinician, BigDecimal.ONE,
                      new BigDecimal("4.2"), ZERO, ZERO, ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event,
                      product1Acts);
        }

        checkItem(items, itemArchetype, patient, product2, template, 0, author, clinician, BigDecimal.ONE, quantity, ZERO,
                  ZERO, ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, 0);
        checkItem(items, itemArchetype, patient, product3, template, 0, author, clinician, BigDecimal.ONE, quantity, ZERO,
                  ZERO, ZERO, fixedPriceIncTax, discount, itemTax, itemTotal, true, event, 0);
    }

    /**
     * Initialises stock quantities for a product at a stock location.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param quantity      the initial stock quantity
     */
    private void initStock(Product product, Party stockLocation, BigDecimal quantity) {
        StockRules rules = new StockRules(getArchetypeService());
        rules.updateStock(product, stockLocation, quantity);
    }

    /**
     * Checks stock for a product at a stock location.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param initial       the initial stock quantity
     * @param change        the change in stock quantity
     * @param add           if {@code true} add the change, otherwise subtract it
     * @return the new stock quantity
     */
    private BigDecimal checkStock(Product product, Party stockLocation, BigDecimal initial, BigDecimal change,
                                  boolean add) {
        BigDecimal expected = add ? initial.add(change) : initial.subtract(change);
        checkStock(product, stockLocation, expected);
        return expected;
    }

    /**
     * Checks stock for a product at a stock location.
     *
     * @param product       the product
     * @param stockLocation the stock location
     * @param expected      the expected stock quantity
     */
    private void checkStock(Product product, Party stockLocation, BigDecimal expected) {
        StockRules rules = new StockRules(getArchetypeService());
        checkEquals(expected, rules.getStock(get(product), get(stockLocation)));
    }

    /**
     * Verifies a patient clinical event matches that expected.
     *
     * @param event     the event
     * @param patient   the expected patient
     * @param author    the expected author
     * @param clinician the expected clinician
     * @param location  the expected location
     * @param status    the expected status
     */
    private void checkEvent(Act event, Party patient, User author, User clinician, Party location, String status) {
        IMObjectBean bean = getBean(event);
        assertEquals(patient.getObjectReference(), bean.getTargetRef("patient"));
        assertEquals(author.getObjectReference(), event.getCreatedBy());
        assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        assertEquals(location.getObjectReference(), bean.getTargetRef("location"));
        assertEquals(status, event.getStatus());
        if (ActStatus.COMPLETED.equals(event.getStatus())) {
            assertNotNull(event.getActivityEndTime());
        } else {
            assertNull(event.getActivityEndTime());
        }
    }

    /**
     * Verifies a prescription has a link to a charge item
     *
     * @param prescription the prescription
     * @param itemEditor   the charge item editor
     */
    private void checkPrescription(Act prescription, CustomerChargeActItemEditor itemEditor) {
        prescription = get(prescription);
        assertNotNull(prescription);
        IMObjectBean prescriptionBean = getBean(prescription);
        Act item = itemEditor.getObject();
        IMObjectBean bean = getBean(item);
        List<Act> dispensing = bean.getTargets("dispensing", Act.class);
        assertEquals(1, dispensing.size());
        Act medication = dispensing.get(0);
        assertTrue(prescriptionBean.getTargets("dispensing").contains(medication));
    }

    /**
     * Deletes a charge.
     *
     * @param editor the editor to use
     * @return {@code true} if the delete was successful
     */
    private boolean delete(final DefaultCustomerChargeActEditor editor) {
        boolean result = false;
        try {
            TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
            template.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    editor.delete();
                }
            });
            result = true;
        } catch (Throwable exception) {
            logger.error(exception, exception);
        }
        return result;
    }

    /**
     * Checks the balance for a customer.
     *
     * @param customer the customer
     * @param unbilled the expected unbilled amount
     * @param balance  the expected balance
     */
    private void checkBalance(Party customer, BigDecimal unbilled, BigDecimal balance) {
        CustomerAccountRules rules = ServiceHelper.getBean(CustomerAccountRules.class);
        checkEquals(unbilled, rules.getUnbilledAmount(customer));
        checkEquals(balance, rules.getBalance(customer));
    }

    /**
     * Chekcs the total of a charge matches that expected, and that the total matches the sum of the item totals.
     *
     * @param charge the charge
     * @param total  the expected total
     */
    private void checkTotal(FinancialAct charge, BigDecimal total) {
        checkEquals(total, charge.getTotal());
        ActCalculator calculator = new ActCalculator(getArchetypeService());
        BigDecimal itemTotal = calculator.sum(charge, "items", "total");
        checkEquals(total, itemTotal);
    }

    /**
     * Adds an HL7 laboratory test to a product.
     *
     * @param product the product
     */
    private void addHL7Test(Product product) {
        addHL7Test(product, laboratoryFactory.createInvestigationType());
    }

    /**
     * Adds an HL7 laboratory test to a product.
     *
     * @param product           the product
     * @param investigationType the investigation type
     */
    private void addHL7Test(Product product, Entity investigationType) {
        Entity test = laboratoryFactory.createHL7Test(investigationType);
        productFactory.updateProduct(product)
                .addTests(test)
                .build();
    }

    /**
     * Creates a customer charge act editor.
     *
     * @param invoice the charge to edit
     * @param context the layout context
     * @return a new customer charge act editor
     */
    private TestChargeEditor createCustomerChargeActEditor(FinancialAct invoice, LayoutContext context) {
        return createCustomerChargeActEditor(invoice, context, false);
    }

    /**
     * Creates a customer charge act editor.
     *
     * @param invoice        the charge to edit
     * @param context        the layout context
     * @param addDefaultItem if {@code true} add a default item if the act has none
     * @return a new customer charge act editor
     */
    private TestChargeEditor createCustomerChargeActEditor(FinancialAct invoice, LayoutContext context,
                                                           boolean addDefaultItem) {
        return new TestChargeEditor(invoice, context, addDefaultItem);
    }
}
