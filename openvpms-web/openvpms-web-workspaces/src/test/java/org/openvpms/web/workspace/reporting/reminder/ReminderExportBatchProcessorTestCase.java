/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderCSVExporter;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderExporter;
import org.openvpms.archetype.rules.patient.reminder.ReminderItemQueryFactory;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.builder.party.TestContactFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes.EXPORT_REMINDER;

/**
 * Tests the {@link ReminderExportBatchProcessor}.
 *
 * @author Tim Anderson
 */
public class ReminderExportBatchProcessorTestCase extends AbstractReminderBatchProcessorTest {

    /**
     * The test practice.
     */
    private Party practice;

    /**
     * The test location.
     */
    private Party location;

    /**
     * The reminder types.
     */
    private ReminderTypes reminderTypes;

    /**
     * The appointment schedule.
     */
    private Entity schedule;

    /**
     * The reminder rules.
     */
    @Autowired
    private ReminderRules reminderRules;

    /**
     * The patient rules.
     */
    @Autowired
    private PatientRules patientRules;

    /**
     * The party rules.
     */
    @Autowired
    private PartyRules partyRules;

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules appointmentRules;

    /**
     * The document handlers.
     */
    @Autowired
    private DocumentHandlers documentHandlers;

    /**
     * The contact factory.
     */
    @Autowired
    private TestContactFactory contactFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The reminder configuration.
     */
    private ReminderConfiguration config;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * Test reminder type 1.
     */
    private Entity reminderType1;

    /**
     * Test reminder type 2.
     */
    private Entity reminderType2;

    /**
     * Patient A.
     */
    private Party patientA;

    /**
     * Reminder 1 for patient A.
     */
    private Act reminderA1;

    /**
     * Reminder item 1 for patient A.
     */
    private Act itemA1;

    /**
     * Reminder 2 for patient A.
     */
    private Act reminderA2;

    /**
     * Reminder item 2 for patient A.
     */
    private Act itemA2;

    /**
     * Patient B.
     */
    private Party patientB;

    /**
     * Reminder 1 for patient B.
     */
    private Act reminderB1;

    /**
     * Reminder item 1 for patient B.
     */
    private Act itemB1;

    /**
     * Patient C.
     */
    private Party patientC;

    /**
     * Reminder 1 for patient C.
     */
    private Act reminderC1;

    /**
     * Reminder item 1 for patient C.
     */
    private Act itemC1;

    /**
     * Customer 1.
     */
    private Party customer1;

    /**
     * Customer 1 contact.
     */
    private Contact contact1;

    /**
     * Customer 2.
     */
    private Party customer2;

    /**
     * Customer 2 location contact.
     */
    private Contact contact2;

    /**
     * The reminder item query source.
     */
    private ReminderItemQuerySource source;

    /**
     * Patient A appointment.
     */
    private Act appointmentA;

    /**
     * The communication logger.
     */
    private TestCommunicationLogger communicationLogger;

    /**
     * The initial due date.
     */
    private Date due;


    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        this.practice = practiceFactory.getPractice();
        location = practiceFactory.createLocation();
        IArchetypeService service = getArchetypeService();
        reminderTypes = new ReminderTypes(service);
        practiceService = Mockito.mock(PracticeService.class);
        Mockito.when(practiceService.getPractice()).thenReturn(practice);
        config = new ReminderConfiguration(create(ReminderArchetypes.CONFIGURATION), service);
        schedule = schedulingFactory.createSchedule(location);

        contact1 = contactFactory.createLocation("103 Stafford Drive", "SALE", "VIC", "3085");
        customer1 = customerFactory.newCustomer("MS", "J", "Smith")
                .addContact(contact1)
                .build();
        patientA = patientFactory.createPatient("Fido", customer1);
        patientB = patientFactory.createPatient("Spot", customer1);
        contact2 = contactFactory.createLocation("91 Smith Rd", "KONGWAK", "VIC", "3086");
        customer2 = customerFactory.newCustomer("MR", "K", "Aardvark")
                .addContact(contact2)
                .build();
        patientC = patientFactory.createPatient("Fluffy", customer2);

        reminderType1 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .newCount().count(0).interval(0, DateUnits.WEEKS).add()
                .newCount().count(1).interval(1, DateUnits.WEEKS).add()
                .build();

        reminderType2 = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.MONTHS)
                .newCount().count(0).interval(0, DateUnits.WEEKS).add()
                .newCount().count(1).interval(1, DateUnits.MONTHS).add()
                .build();

        Date send = DateRules.getToday();
        due = DateRules.getTomorrow();

        // set up reminders and appointments for patient A
        itemA1 = reminderFactory.createExportReminder(send, due);
        itemA2 = reminderFactory.createExportReminder(send, due);
        reminderA1 = reminderFactory.createReminder(due, patientA, reminderType1, itemA1);
        reminderA2 = reminderFactory.createReminder(due, patientA, reminderType2, itemA2);

        createAppointment(DateRules.getYesterday(), customer1, patientA, AppointmentStatus.COMPLETED);
        Date appointment1Start = DateRules.getDate(due, 1, DateUnits.MONTHS);
        appointmentA = createAppointment(appointment1Start, customer1, patientA, AppointmentStatus.PENDING);
        createAppointment(DateRules.getNextDate(appointment1Start), customer1, patientA, AppointmentStatus.PENDING);

        // set up reminders and appointments for patient B
        itemB1 = reminderFactory.createExportReminder(send, due);
        reminderB1 = reminderFactory.createReminder(due, patientB, reminderType1, itemB1);
        createAppointment(DateRules.getYesterday(), customer1, patientB, AppointmentStatus.CANCELLED);

        // set up reminders for patient C
        itemC1 = reminderFactory.createExportReminder(send, due);
        reminderC1 = reminderFactory.createReminder(due, patientC, reminderType2, itemC1);

        // limit the query to the items above
        ReminderItemQueryFactory factory = new TestReminderItemQueryFactory(EXPORT_REMINDER,
                                                                            itemA1, itemA2, itemB1, itemC1);
        source = new ReminderItemQuerySource(factory, reminderTypes, config);
        communicationLogger = new TestCommunicationLogger(service);
    }

    /**
     * Tests export.
     *
     * @throws Exception for any error
     */
    @Test
    public void testExport() throws Exception {
        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkCompleted(itemA1, itemA2, itemB1, itemC1);
        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, oneWeek);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<ReminderEvent> exported = processor.getExported();
        List<Act> appointments = processor.getAppointments();
        assertEquals(4, exported.size());

        // reminders are ordered on customer and patient
        check(exported.get(0), reminderC1, itemC1, patientC, customer2, contact2, null, appointments.get(0));
        check(exported.get(1), reminderA1, itemA1, patientA, customer1, contact1, appointmentA,
              appointments.get(1));
        check(exported.get(2), reminderA2, itemA2, patientA, customer1, contact1, appointmentA,
              appointments.get(2));
        check(exported.get(3), reminderB1, itemB1, patientB, customer1, contact1, null, appointments.get(3));

        List<String[]> lines = readCSV(processor);
        assertEquals(5, lines.size()); // includes header

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Tests exporting reminders where one item is cancelled.
     *
     * @throws Exception for any error
     */
    @Test
    public void testCancel() throws Exception {
        patientFactory.updatePatient(patientC)
                .active(false)
                .build();
        // inactive patient. Reminder item should be cancelled

        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkCompleted(itemA1, itemA2, itemB1);
        checkStatus(ReminderItemStatus.CANCELLED, "Patient is inactive", itemC1);
        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, oneWeek);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);
        // itemC1 cancelled, but reminderC1 still updated. Would be cancelled by reminder queue job

        List<ReminderEvent> exported = processor.getExported();
        List<Act> appointments = processor.getAppointments();
        assertEquals(3, exported.size());

        // reminders are ordered on customer and patient
        check(exported.get(0), reminderA1, itemA1, patientA, customer1, contact1, appointmentA,
              appointments.get(0));
        check(exported.get(1), reminderA2, itemA2, patientA, customer1, contact1, appointmentA,
              appointments.get(1));
        check(exported.get(2), reminderB1, itemB1, patientB, customer1, contact1, null, appointments.get(2));

        List<String[]> lines = readCSV(processor);
        assertEquals(4, lines.size()); // includes header

        assertEquals(3, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(1, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(1, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(3, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a parent reminder is changed after its item is exported, but before it is updated:
     * <u>
     * <li>processing completes successfully</li>
     * <li>it is logged</li>
     * <li>the item is flagged as an ERROR, as the parent reminder count hasn't updated</li>
     * </u>
     */
    @Test
    public void testChangeReminderBeforeUpdate() throws Exception {
        // set up a processor that simulates changing of the item by another user, before it can be updated
        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        processor.setBeforeUpdateAction(() -> {
            reminderA1.setDescription("some description, not normally editable, but will force object to be saved");
            save(reminderA1);
        });
        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkStatus(ReminderItemStatus.ERROR,
                    "Reminder was sent, but couldn't be updated. This may lead to a duplicate reminder. " +
                    "Failure reason: Reminder has been changed by another user", itemA1);
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<ReminderEvent> exported = processor.getExported();
        List<Act> appointments = processor.getAppointments();
        assertEquals(4, exported.size());

        // reminders are ordered on customer and patient
        check(exported.get(0), reminderC1, itemC1, patientC, customer2, contact2, null, appointments.get(0));
        check(exported.get(1), reminderA1, itemA1, patientA, customer1, contact1, appointmentA,
              appointments.get(1));
        check(exported.get(2), reminderA2, itemA2, patientA, customer1, contact1, appointmentA,
              appointments.get(2));
        check(exported.get(3), reminderB1, itemB1, patientB, customer1, contact1, null, appointments.get(3));

        List<String[]> lines = readCSV(processor);
        assertEquals(5, lines.size()); // includes header

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors());
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if an item is changed after it is exported, but before it is updated:
     * <u>
     * <li>processing completes successfully</li>
     * <li>it is logged</li>
     * <li>it is flagged as an ERROR, as the parent reminder count hasn't updated</li>
     * </u>
     */
    @Test
    public void testChangeItemBeforeUpdate() {
        // set up a processor that simulates changing of the item by another user, before it can be updated
        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        processor.setBeforeUpdateAction(() -> {
            itemA1.setDescription("some description, not normally editable, but will force object to be saved");
            save(itemA1);
        });
        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        checkStatus(ReminderItemStatus.ERROR,
                    "Reminder was sent, but couldn't be updated. This may lead to a duplicate reminder. " +
                    "Failure reason: Patient Export Reminder has been changed by another user", itemA1);
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<ReminderEvent> exported = processor.getExported();
        List<Act> appointments = processor.getAppointments();
        assertEquals(4, exported.size());

        // reminders are ordered on customer and patient
        check(exported.get(0), reminderC1, itemC1, patientC, customer2, contact2, null, appointments.get(0));
        check(exported.get(1), reminderA1, itemA1, patientA, customer1, contact1, appointmentA,
              appointments.get(1));
        check(exported.get(2), reminderA2, itemA2, patientA, customer1, contact1, appointmentA,
              appointments.get(2));
        check(exported.get(3), reminderB1, itemB1, patientB, customer1, contact1, null, appointments.get(3));

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a reminder is deleted after exporting, but before it is updated, processing completes
     * successfully, and it is still logged.
     */
    @Test
    public void testDeleteReminderBeforeUpdate() {
        // set up a processor that simulates deletion of the reminder by another user, before it can be updated
        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        processor.setBeforeUpdateAction(() -> remove(reminderA1));

        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        assertNull(get(reminderA1));
        assertNull(get(itemA1));           // deleted
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<ReminderEvent> exported = processor.getExported();
        assertEquals(4, exported.size());

        // reminders are ordered on customer and patient
        assertEquals(reminderC1, exported.get(0).getReminder());
        assertEquals(reminderA1, exported.get(1).getReminder());
        assertEquals(reminderA2, exported.get(2).getReminder());
        assertEquals(reminderB1, exported.get(3).getReminder());

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Verifies that if a reminder item is deleted after exporting, but before it is updated, processing completes
     * successfully, and it is still logged.
     */
    @Test
    public void testDeleteItemBeforeUpdate() {
        // set up a processor that simulates deletion of the item by another user, before it can be updated
        TestReminderExportProcessor processor = new TestReminderExportProcessor(communicationLogger);
        processor.setBeforeUpdateAction(() -> remove(itemA1));

        ReminderExportBatchProcessor batchProcessor = new ReminderExportBatchProcessor(source, processor);
        Statistics statistics = new Statistics();
        batchProcessor.setStatistics(statistics);
        batchProcessor.process();

        assertNull(get(itemA1));           // deleted
        checkCompleted(itemA2, itemB1, itemC1);

        Date oneWeek = DateRules.getDate(due, 1, DateUnits.WEEKS);
        Date oneMonth = DateRules.getDate(due, 1, DateUnits.MONTHS);
        checkReminder(reminderA1, 0, due); // not updated
        checkReminder(reminderA2, oneMonth);
        checkReminder(reminderB1, oneWeek);
        checkReminder(reminderC1, oneMonth);

        List<ReminderEvent> exported = processor.getExported();
        assertEquals(4, exported.size());

        // reminders are ordered on customer and patient
        // reminders are ordered on customer and patient
        assertEquals(reminderC1, exported.get(0).getReminder());
        assertEquals(reminderA1, exported.get(1).getReminder());
        assertEquals(reminderA2, exported.get(2).getReminder());
        assertEquals(reminderB1, exported.get(3).getReminder());

        assertEquals(4, statistics.getCount());
        assertEquals(0, statistics.getErrors()); // errors apply before update
        assertEquals(0, statistics.getCancelled());
        assertEquals(2, statistics.getCount(reminderType1, EXPORT_REMINDER));
        assertEquals(2, statistics.getCount(reminderType2, EXPORT_REMINDER));
        assertEquals(4, communicationLogger.getLogs().size());
    }

    /**
     * Reads the CSV from the exported document.
     *
     * @param processor the reminder export processor
     * @return the CSV lines
     * @throws IOException for any I/O error
     */
    private List<String[]> readCSV(TestReminderExportProcessor processor) throws IOException {
        Document document = processor.getDocument();
        assertNotNull(document);
        InputStream content = documentHandlers.get(document).getContent(document);
        CSVReader reader = new CSVReader(new InputStreamReader(content), ',');
        return reader.readAll();
    }

    /**
     * Verifies that a reminder event matches that expected.
     *
     * @param event               the reminder event
     * @param reminder            the reminder
     * @param item                the reminder item
     * @param patient             the patient
     * @param customer            the customer
     * @param contact             the contact
     * @param expectedAppointment the expected appointment. May be {@code null}
     * @param actualAppointment   the actual appointment. May be {@code null}
     */
    private void check(ReminderEvent event, Act reminder, Act item, Party patient, Party customer, Contact contact,
                       Act expectedAppointment, Act actualAppointment) {
        assertEquals(event.getReminder(), reminder);
        assertEquals(event.getItem(), item);
        assertEquals(event.getPatient(), patient);
        assertEquals(event.getCustomer(), customer);
        assertEquals(event.getContact(), contact);
        assertEquals(expectedAppointment, actualAppointment);
    }

    /**
     * Creates an appointment.
     *
     * @param startTime the start time
     * @param customer  the customer
     * @param patient   the patient
     * @param status    the status
     * @return the new appointment
     */
    private Act createAppointment(Date startTime, Party customer, Party patient, String status) {
        return schedulingFactory.newAppointment()
                .startTime(startTime)
                .schedule(schedule)
                .customer(customer)
                .patient(patient)
                .status(status)
                .appointmentType(schedulingFactory.createAppointmentType())
                .build();
    }

    private class TestReminderExportProcessor extends ReminderExportProcessor {
        private final List<ReminderEvent> exported = new ArrayList<>();

        private final List<Act> appointments = new ArrayList<>();

        private Document document;

        private Runnable beforeUpdate;

        public TestReminderExportProcessor(CommunicationLogger communicationLogger) {
            super(reminderTypes, reminderRules, patientRules, location, practice, getArchetypeService(), config,
                  communicationLogger, ReminderExportBatchProcessorTestCase.this.actionFactory);
        }

        public List<ReminderEvent> getExported() {
            return exported;
        }

        public List<Act> getAppointments() {
            return appointments;
        }

        public Document getDocument() {
            return document;
        }

        /**
         * Sets an action to run after printing, but before updating reminders.
         *
         * @param action the action to run prior to updating reminders
         */
        public void setBeforeUpdateAction(Runnable action) {
            this.beforeUpdate = action;
        }

        @Override
        protected void export(List<ReminderEvent> reminders) {
            ReminderExporter exporter = new ReminderCSVExporter(practiceService, partyRules, patientRules,
                                                                appointmentRules, getArchetypeService(),
                                                                documentHandlers) {
                /**
                 * Exports reminders to CSV.
                 *
                 * @param reminders the reminders to export
                 * @param date      the date to use for determining the next pending appointment
                 * @return the exported reminders
                 */
                @Override
                public Document export(List<ReminderEvent> reminders, Date date) {
                    Document result = super.export(reminders, date);
                    if (beforeUpdate != null) {
                        beforeUpdate.run();
                    }
                    return result;
                }

                @Override
                protected void export(ReminderEvent event, Act appointment, CSVWriter writer) {
                    exported.add(event);
                    appointments.add(appointment);
                    super.export(event, appointment, writer);
                }
            };
            document = exporter.export(reminders, new Date());
        }
    }
}
