/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.estimate;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.archetype.test.builder.customer.account.TestEstimateItemVerifier;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;

import java.math.BigDecimal;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link EstimateEditor}.
 *
 * @author Tim Anderson
 */
public class EstimateEditorTestCase extends AbstractEstimateEditorTestCase {

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        context.setPractice(getPractice());
        context.setClinician(userFactory.createClinician());
        User user = userFactory.createUser();
        context.setUser(user);

        new AuthenticationContextImpl().setUser(user); // logs the user in
    }

    /**
     * Tests template expansion.
     */
    @Test
    public void testTemplateExpansion() {
        Entity discount = productFactory.newDiscount().percentage(10).discountFixedPrice(true).build();

        // create a customer with a discount
        Party customer = customerFactory.newCustomer()
                .addDiscounts(discount).build();

        Party patient = patientFactory.createPatient(customer);
        patientFactory.createWeight(patient, new BigDecimal("4.2"));

        context.setCustomer(customer);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layout.setEdit(true);

        BigDecimal fixedPrice = new BigDecimal("0.91");
        BigDecimal unitPrice = new BigDecimal("0.91");
        BigDecimal fixedPriceIncTax = ONE;
        BigDecimal unitPriceIncTax = ONE;

        Product product1 = productFactory.newService().fixedPrice(fixedPrice).unitPrice(unitPrice).build();

        // product2 has a dose, which should be selected over the template include quantity
        Product product2 = productFactory.newMedication()
                .fixedPrice(fixedPrice).unitPrice(unitPrice)
                .concentration(1)
                .newDose().minWeight(0).maxWeight(10).rate(1).quantity(1).add()
                .build();

        Product product3 = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice).build();
        Product product4 = productFactory.newMedication().fixedPrice(fixedPrice).unitPrice(unitPrice)
                .addDiscounts(discount).build();  // customer will have a discount for this product

        Product template = productFactory.newTemplate()
                .newInclude().product(product1).lowQuantity(1).highQuantity(1).add()
                .newInclude().product(product2).lowQuantity(0).highQuantity(2).add()
                .newInclude().product(product3).lowQuantity(2).highQuantity(4).add()
                .newInclude().product(product4).lowQuantity(3).highQuantity(6).print(false).zeroPrice().add()
                .build();

        Act estimate = create(EstimateArchetypes.ESTIMATE, Act.class);
        EstimateEditor editor = new EstimateEditor(estimate, null, layout);
        editor.getComponent();
        assertFalse(editor.isValid());
        IMObjectEditor itemEditor = editor.getItems().add();
        assertTrue(itemEditor instanceof EstimateItemEditor);
        EstimateItemEditor estimateItemEditor = (EstimateItemEditor) itemEditor;
        estimateItemEditor.setPatient(patient);
        estimateItemEditor.setProduct(template);

        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        estimate = get(estimate);
        List<Act> items = getItems(estimate);
        assertEquals(4, items.size());

        User author = context.getUser();

        checkEstimate(estimate, customer, author, new BigDecimal("5.00"), new BigDecimal("12.20"));
        TestEstimateItemVerifier verifier = new TestEstimateItemVerifier(getArchetypeService());
        verifier.patient(patient).template(template).group(0).createdBy(author);

        verifier.product(product1)
                .minimumQuantity(1).lowQuantity(1).highQuantity(1)
                .fixedPrice(fixedPriceIncTax)
                .lowUnitPrice(unitPriceIncTax).highUnitPrice(unitPriceIncTax)
                .lowDiscount(0).highDiscount(0)
                .lowTotal(2).highTotal(2)
                .verify(items);
        verifier.product(product2)
                .minimumQuantity(0).lowQuantity(0).highQuantity(new BigDecimal("4.2"))
                .fixedPrice(1)
                .lowUnitPrice(unitPriceIncTax).highUnitPrice(unitPriceIncTax)
                .lowDiscount(0).highDiscount(0)
                .lowTotal(0).highTotal(new BigDecimal("5.2"))
                .verify(items);
        verifier.product(product3)
                .minimumQuantity(2).lowQuantity(2).highQuantity(4)
                .fixedPrice(fixedPriceIncTax)
                .lowUnitPrice(unitPriceIncTax).highUnitPrice(unitPriceIncTax)
                .lowDiscount(0).highDiscount(0)
                .lowTotal(3).highTotal(5)
                .verify(items);
        verifier.product(product4)
                .minimumQuantity(3).lowQuantity(3).highQuantity(6)
                .fixedPrice(0)
                .lowUnitPrice(0).highUnitPrice(0)
                .lowDiscount(0).highDiscount(0)
                .print(false)
                .lowTotal(0).highTotal(0)
                .verify(items);
    }

    /**
     * Verifies that each time a template is expanded, the template participations are assigned a new group identifier.
     */
    @Test
    public void testTemplateExpansionGroup() {
        Product product1 = productFactory.createService();
        Product product2 = productFactory.createService();
        Product product3 = productFactory.createService();

        Product template1 = productFactory.newTemplate()
                .newInclude().product(product1).highQuantity(1).add()
                .build();
        Product template2 = productFactory.newTemplate()
                .newInclude().product(product2).highQuantity(1).add()
                .build();
        Product template3 = productFactory.newTemplate()
                .newInclude().product(product3).highQuantity(1).add()
                .build();

        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        context.setCustomer(customer);
        context.setPatient(patient);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layout.setEdit(true);

        Act estimate = create(EstimateArchetypes.ESTIMATE, Act.class);
        EstimateEditor editor = new EstimateEditor(estimate, null, layout);
        editor.getComponent();
        assertFalse(editor.isValid());
        addItem(editor, template1);
        addItem(editor, template2);
        addItem(editor, template3);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        List<org.openvpms.component.model.act.Act> items = getItems(estimate);
        assertEquals(3, items.size());

        TestEstimateItemVerifier verifier = new TestEstimateItemVerifier(getArchetypeService());
        verifier.patient(patient).createdBy(context.getUser());

        verifier.product(product1).template(template1).group(0).minimumQuantity(1).verify(items);
        verifier.product(product2).template(template2).group(1).minimumQuantity(1).verify(items);
        verifier.product(product3).template(template3).group(2).minimumQuantity(1).verify(items);
    }

    /**
     * Returns the estimate items.
     *
     * @param estimate the estimate
     * @return the estimate items
     */
    private List<Act> getItems(Act estimate) {
        IMObjectBean bean = getBean(estimate);
        return bean.getTargets("items", Act.class);
    }

    /**
     * Adds an item to an estimate.
     *
     * @param editor  the estimate editor
     * @param product the product to add
     */
    private void addItem(EstimateEditor editor, Product product) {
        IMObjectEditor itemEditor = editor.getItems().add();
        assertTrue(itemEditor instanceof EstimateItemEditor);
        EstimateItemEditor estimateItemEditor = (EstimateItemEditor) itemEditor;
        estimateItemEditor.setProduct(product);
    }

    /**
     * Verifies an estimate matches that expected.
     *
     * @param estimate  the estimate to check
     * @param customer  the expected customer
     * @param author    the expected author
     * @param lowTotal  the expected low total
     * @param highTotal the expected high total
     */
    private void checkEstimate(Act estimate, Party customer, User author, BigDecimal lowTotal, BigDecimal highTotal) {
        IMObjectBean bean = getBean(estimate);
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(author.getObjectReference(), estimate.getCreatedBy());
        checkEquals(lowTotal, bean.getBigDecimal("lowTotal"));
        checkEquals(highTotal, bean.getBigDecimal("highTotal"));
    }
}
