/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link EmailDocumentTemplateEditor}.
 *
 * @author Tim Anderson
 */
public class EmailDocumentTemplateEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link DocumentTemplateEditor} for
     * <em>entity.documentTemplateEmail*</em> instances.
     */
    @Test
    public void testFactory() {
        Entity template1 = create(DocumentArchetypes.USER_EMAIL_TEMPLATE, Entity.class);
        IMObjectEditor editor1 = factory.create(template1, new DefaultLayoutContext(new LocalContext(),
                                                                                    new HelpContext("foo", null)));
        assertTrue(editor1 instanceof EmailDocumentTemplateEditor);

        Entity template2 = create(DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE, Entity.class);
        IMObjectEditor editor2 = factory.create(template2, new DefaultLayoutContext(new LocalContext(),
                                                                                    new HelpContext("foo", null)));
        assertTrue(editor2 instanceof EmailDocumentTemplateEditor);
    }

    /**
     * Tests the {@link EmailDocumentTemplateEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Entity template = create(DocumentArchetypes.USER_EMAIL_TEMPLATE, Entity.class);
        EmailDocumentTemplateEditor editor = createEditor(template);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof EmailDocumentTemplateEditor);
    }

    /**
     * Verifies that when a template is deleted, the associated document act and document is also deleted,
     * but any associated attachment is retained.
     */
    @Test
    public void testDelete() {
        Entity template = create(DocumentArchetypes.USER_EMAIL_TEMPLATE, Entity.class);
        EmailDocumentTemplateEditor editor1 = createEditor(template);
        editor1.getProperty("name").setValue("Z Test template");
        editor1.getProperty("subject").setValue("test email");
        editor1.getProperty("contentType").setValue(EmailTemplate.ContentType.DOCUMENT.name());
        Document document = documentFactory.createJRXML();
        editor1.onUpload(document);

        DocumentAct act = editor1.getDocumentAct();
        assertNotNull(act);
        assertEquals(document.getObjectReference(), act.getDocument());

        assertTrue(SaveHelper.save(editor1));
        assertNotNull(get(act));
        assertNotNull(get(document));

        // add an attachment to the email template
        Document attachmentContent = documentFactory.createPDF("Patient Handout.pdf");
        Entity attachment = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(attachmentContent)
                .build();

        documentFactory.updateEmailTemplate(template)
                .addAttachments(attachment)
                .build();

        EmailDocumentTemplateEditor editor2 = createEditor(template);

        TransactionTemplate txn = new TransactionTemplate(ServiceHelper.getTransactionManager());
        txn.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                editor2.delete();
            }
        });

        // verify they have been deleted
        assertNull(get(template));
        assertNull(get(act));
        assertNull(get(document));

        // verify the attachment has been retained
        assertNotNull(get(attachmentContent));
        assertNotNull(get(attachment));
    }

    /**
     * Verifies that the default email address can be set as both an RFC 5322 name-addr (which contains a display-name
     * and an addr-spec), and an addr-spec.
     */
    @Test
    public void testValidateDefaultEmailAddress() {
        // addr-spec format
        Entity template1 = documentFactory.newEmailTemplate()
                .subject("subject")
                .defaultEmailAddress("foo@bar.com")
                .build(false);
        EmailDocumentTemplateEditor editor1 = createEditor(template1);
        assertValid(editor1);

        // name-addr format
        Entity template2 = documentFactory.newEmailTemplate()
                .subject("subject")
                .defaultEmailAddress("\"name\" <foo@bar.com>")
                .build(false);
        EmailDocumentTemplateEditor editor2 = createEditor(template2);
        assertValid(editor2);

        // no default email address
        Entity template3 = documentFactory.newEmailTemplate()
                .subject("subject")
                .build(false);
        EmailDocumentTemplateEditor editor3 = createEditor(template3);
        assertValid(editor3);

        // invalid addr-spec
        Entity test4 = documentFactory.newEmailTemplate()
                .subject("subject")
                .defaultEmailAddress("@bar.com")
                .build(false);
        EmailDocumentTemplateEditor editor4 = createEditor(test4);
        assertInvalid(editor4, "Missing local name");

        // invalid name-addr
        Entity test5 = documentFactory.newEmailTemplate()
                .subject("subject")
                .defaultEmailAddress("\"name <foo@bar.com>")
                .build(false);
        EmailDocumentTemplateEditor editor5 = createEditor(test5);
        assertInvalid(editor5, "Missing '\"'");
    }

    /**
     * Creates a new editor.
     *
     * @param template the template to edit
     * @return a new editor
     */
    private EmailDocumentTemplateEditor createEditor(Entity template) {
        LayoutContext layoutContext = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        return new EmailDocumentTemplateEditor(template, null, layoutContext);
    }
}
