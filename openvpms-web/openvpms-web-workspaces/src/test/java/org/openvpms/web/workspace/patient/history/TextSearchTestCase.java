/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.customer.CommunicationArchetypes;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.sms.internal.SMSArchetypes;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link TextSearch} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class TextSearchTestCase extends ArchetypeServiceTest {

    /**
     * The test clinician.
     */
    private User clinician;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;


    private static final String TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
                                       "incididunt ut labore et dolore magna aliqua.";

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        clinician = userFactory.createClinician("Jo", "Bloggs");
    }

    /**
     * Tests matching on <em>act.customerAccountInvoiceItem</em>
     */
    @Test
    public void testInvoiceItem() {
        Act act = create(CustomerAccountArchetypes.INVOICE_ITEM, Act.class);
        Product product = createProduct("Z Aspirin");
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);
        bean.setTarget("product", product);
        bean.setTarget("batch", createBatch(product));

        check(act, true, "aspirin", true, true);
        check(act, false, "vaccine", true, true);
        check(act, true, "123456789", true, true);
        check(act, false, "1234567890", true, true);
        check(act, false, "123456789", true, false);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalAddendum</em>.
     */
    @Test
    public void testPatientClinicalAddendum() {
        Act act = create(PatientArchetypes.CLINICAL_ADDENDUM, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("note", TEXT);
        bean.setTarget("clinician", clinician);

        check(act, true, "magna", true, true);
        check(act, false, "fido", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalAddendum</em> with document content.
     */
    @Test
    public void testPatientClinicalAddendumWithDocument() {
        DocumentAct note = create(PatientArchetypes.CLINICAL_ADDENDUM, DocumentAct.class);
        note.setDocument(createDocument());
        check(note, true, "long", true, true);
        check(note, false, "fido", true, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalEvent</em>.
     */
    @Test
    public void testPatientClinicalEvent() {
        Act act = create(PatientArchetypes.CLINICAL_EVENT, Act.class);

        IMObjectBean bean = getBean(act);
        bean.setValue("reason", TestHelper.getLookup("lookup.visitReason", "CHECKUP").getCode());
        bean.setValue("title", "Annual health check");
        bean.setTarget("clinician", clinician);

        check(act, true, "Checkup", true, true);
        check(act, true, "health check", true, true);
        check(act, false, "surgery", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalNote</em>.
     */
    @Test
    public void testPatientClinicalNote() {
        Act note = create(PatientArchetypes.CLINICAL_NOTE, Act.class);
        IMObjectBean bean = getBean(note);
        bean.setValue("note", TEXT);
        bean.setTarget("clinician", clinician);

        check(note, true, "magna", true, true);
        check(note, false, "fido", true, true);
        check(note, true, "Jo Bloggs", true, true);
        check(note, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalNote</em> with document content.
     */
    @Test
    public void testPatientClinicalNoteWithDocument() {
        DocumentAct note = create(PatientArchetypes.CLINICAL_NOTE, DocumentAct.class);
        note.setDocument(createDocument());
        check(note, true, "long", true, true);
        check(note, false, "fido", true, true);
    }

    /**
     * Tests matching an <em>act.patientClinicalProblem</em>.
     */
    @Test
    public void testPatientClinicalProblem() {
        Act act = create(PatientArchetypes.CLINICAL_PROBLEM, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("presentingComplaint", TestHelper.getLookup("lookup.presentingComplaint", "VOMITING").getCode());
        bean.setValue("reason", TestHelper.getLookup("lookup.diagnosis", "ABDOMINAL_INJURY").getCode());
        bean.setTarget("clinician", clinician);

        check(act, true, "vomiting", true, true);
        check(act, true, "injury", true, true);
        check(act, false, "surgery", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching on <em>act.patientDocumentAttachment</em>.
     */
    @Test
    public void testAttachment() {
        checkAttachment(PatientArchetypes.DOCUMENT_ATTACHMENT);
    }

    /**
     * Test matching on <em>act.patientDocumentAttachmentVersion</em>.
     */
    @Test
    public void testAttachmentVersion() {
        checkAttachment(PatientArchetypes.DOCUMENT_ATTACHMENT_VERSION);
    }

    /**
     * Test matching on <em>act.patientDocumentForm</em>.
     */
    @Test
    public void testForm() {
        DocumentAct act = create(PatientArchetypes.DOCUMENT_FORM, DocumentAct.class);
        Entity template = documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .name("Z Certificate")
                .build();

        act.setDescription(TEXT);
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);
        bean.setTarget("documentTemplate", template);
        bean.setTarget("product", createProduct("Z Vaccine"));

        check(act, true, "magna", true, true);
        check(act, true, "certificate", true, true);
        check(act, true, "vaccine", true, true);
        check(act, false, "vaccination", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching on <em>act.patientDocumentImage</em>.
     */
    @Test
    public void testImage() {
        checkImage(PatientArchetypes.DOCUMENT_IMAGE);
    }

    /**
     * Tests matching on <em>act.patientDocumentImageVersion</em>.
     */
    @Test
    public void testImageVersion() {
        checkImage(PatientArchetypes.DOCUMENT_IMAGE_VERSION);
    }

    /**
     * Tests matching on <em>act.patientDocumentLetter</em>.
     */
    @Test
    public void testLetter() {
        checkLetter(PatientArchetypes.DOCUMENT_LETTER);
    }

    /**
     * Tests matching on <em>act.patientDocumentLetterVersion</em>.
     */
    @Test
    public void testLetterVersion() {
        checkLetter(PatientArchetypes.DOCUMENT_LETTER_VERSION);
    }

    /**
     * Tests matching on <em>act.patientInvestigation</em>.
     */
    @Test
    public void testInvestigation() {
        checkInvestigation(InvestigationArchetypes.PATIENT_INVESTIGATION);
    }

    /**
     * Tests matching on <em>act.patientInvestigationVersion</em>.
     */
    @Test
    public void testInvestigationVersion() {
        checkInvestigation(InvestigationArchetypes.PATIENT_INVESTIGATION_VERSION);
    }

    /**
     * Tests matching on <em>act.patientMedication</em>.
     */
    @Test
    public void testMedication() {
        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        Product product = createProduct("Z Revolution");
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);
        bean.setTarget("product", product);
        bean.setTarget("batch", createBatch(product));

        check(act, true, "revolution", true, true);
        check(act, false, "advocate", true, true);
        check(act, true, "123456789", true, true);
        check(act, false, "1234567890", true, true);
        check(act, false, "123456789", true, false);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching on <em>act.patientWeight</em>.
     */
    @Test
    public void testWeight() {
        Act act = create(PatientArchetypes.PATIENT_WEIGHT, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("weight", BigDecimal.TEN);
        bean.setTarget("clinician", clinician);

        check(act, true, "10 kilograms", true, true);
        check(act, false, "5", true, true);
        check(act, false, "kg", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests matching on <em>act.customerCommunicationEmail</em>.
     */
    @Test
    public void testEmail() {
        Act act = create(CommunicationArchetypes.EMAIL, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("description", "Yearly vaccination reminder");
        bean.setValue("from", "info@vetsrus.com");
        bean.setValue("address", "foo@bar.com");
        bean.setValue("cc", "foo@bar.org");
        bean.setValue("bcc", "reminders@vetsrus.com");
        bean.setValue("reason", TestHelper.getLookup("lookup.customerCommunicationReason", "A_REMINDER").getCode());
        bean.setValue("message", TEXT);
        bean.setValue("note", "a note");
        bean.setValue("document", createDocument());

        check(act, true, "yearly vaccination", true, true);
        check(act, true, "info@vetsrus.com", true, true);
        check(act, true, "foo@bar.com", true, true);
        check(act, true, "foo@bar.org", true, true);
        check(act, true, "reminders@vetsrus.com", true, true);
        check(act, true, "A Reminder", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "a note", true, true);
        check(act, true, "long content", true, true);
    }

    /**
     * Tests matching on <em>act.customerCommunicationNote</em>.
     */
    @Test
    public void testCommunicationNote() {
        Act act = create(CommunicationArchetypes.NOTE, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("description", "Yearly vaccination reminder");
        bean.setValue("reason", TestHelper.getLookup("lookup.customerCommunicationReason", "A_REMINDER_2").getCode());
        bean.setValue("message", TEXT);
        bean.setValue("document", createDocument());

        check(act, true, "yearly vaccination", true, true);
        check(act, true, "A Reminder 2", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "long content", true, true);
    }

    /**
     * Tests matching on <em>act.customerCommunicationMail</em>.
     */
    @Test
    public void testMail() {
        Act act = create(CommunicationArchetypes.MAIL, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("description", "Yearly vaccination reminder");
        bean.setValue("address", "12 Foo St");
        bean.setValue("reason", TestHelper.getLookup("lookup.customerCommunicationReason", "A_REMINDER_3").getCode());
        bean.setValue("message", TEXT);
        bean.setValue("note", "a note");
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        Document document = handler.create("mail.txt", "long content");
        save(document);
        bean.setValue("document", document.getObjectReference());

        check(act, true, "yearly vaccination", true, true);
        check(act, true, "12 Foo St", true, true);
        check(act, true, "A Reminder 3", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "a note", true, true);
        check(act, true, "long content", true, true);
    }

    /**
     * Tests matching on <em>act.customerCommunicationPhone</em>.
     */
    @Test
    public void testPhone() {
        Act act = create(CommunicationArchetypes.PHONE, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("description", "Yearly vaccination reminder");
        bean.setValue("address", "123456789");
        bean.setValue("reason", TestHelper.getLookup("lookup.customerCommunicationReason", "A_REMINDER_4").getCode());
        bean.setValue("message", TEXT);
        bean.setValue("note", "a note");
        bean.setValue("document", createDocument());

        check(act, true, "yearly vaccination", true, true);
        check(act, true, "123456789", true, true);
        check(act, true, "A Reminder 4", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "a note", true, true);
        check(act, true, "long content", true, true);
    }

    /**
     * Tests matching on <em>act.smsMessage</em>.
     */
    @Test
    public void testSMS() {
        Act act = create(SMSArchetypes.MESSAGE, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setValue("description", "Yearly vaccination reminder");
        bean.setValue("phone", "123456789");
        bean.setValue("reason", TestHelper.getLookup("lookup.customerCommunicationReason", "A_REMINDER_5").getCode());
        bean.setValue("message", TEXT);
        bean.setValue("note", "a note");

        check(act, true, "yearly vaccination", true, true);
        check(act, true, "123456789", true, true);
        check(act, true, "A Reminder 5", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "a note", true, true);
    }

    /**
     * Creates a text document.
     *
     * @return a reference to a text document
     */
    private Reference createDocument() {
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        Document document = handler.create("context.txt", "long content");
        save(document);
        return document.getObjectReference();
    }

    /**
     * Creates a new batch.
     *
     * @param product the product
     * @return a new batch
     */
    private Entity createBatch(Product product) {
        return ProductTestHelper.createBatch("123456789", product, new Date());
    }

    /**
     * Creates a new medication product with the specified name.
     *
     * @param name the product name
     * @return a new product
     */
    private Product createProduct(String name) {
        return productFactory.newMedication().name(name).build();
    }

    /**
     * Tests patient document attachments.
     *
     * @param archetype the document archetype
     */
    private void checkAttachment(String archetype) {
        DocumentAct act = create(archetype, DocumentAct.class);
        act.setDescription(TEXT);
        act.setFileName("Consent.pdf");
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);

        check(act, true, "magna", true, true);
        check(act, true, "consent", true, true);
        check(act, false, "vaccination", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests patient images.
     *
     * @param archetype the document archetype
     */
    private void checkImage(String archetype) {
        DocumentAct act = create(archetype, DocumentAct.class);
        act.setDescription(TEXT);
        act.setFileName("Image.pdf");
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);

        check(act, true, "magna", true, true);
        check(act, true, "image.pdf", true, true);
        check(act, false, "vaccination", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests patient letters.
     *
     * @param archetype the document archetype
     */
    private void checkLetter(String archetype) {
        DocumentAct act = create(archetype, DocumentAct.class);
        act.setDescription(TEXT);
        act.setFileName("Consent.pdf");
        IMObjectBean bean = getBean(act);
        bean.setTarget("clinician", clinician);
        bean.setTarget("product", createProduct("Z Vaccine"));

        check(act, true, "magna", true, true);
        check(act, true, "consent", true, true);
        check(act, true, "vaccine", true, true);
        check(act, false, "vaccination", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    /**
     * Tests patient investigations.
     *
     * @param archetype the document archetype
     */
    private void checkInvestigation(String archetype) {
        Entity investigationType = LaboratoryTestHelper.createInvestigationType();
        investigationType.setName("Z Test");
        save(investigationType);

        DocumentAct act = create(archetype, DocumentAct.class);
        act.setDescription(TEXT);
        act.setFileName("Biopsy.pdf");
        IMObjectBean bean = getBean(act);
        bean.setValue("id", 123456);
        bean.setTarget("investigationType", investigationType);
        bean.setTarget("clinician", clinician);
        boolean hasProduct = bean.hasNode("product");
        if (hasProduct) {
            bean.setTarget("product", createProduct("Z Service"));
        }

        check(act, true, "123456", true, true);
        check(act, false, "1234567", true, true);
        check(act, true, "magna", true, true);
        check(act, true, "biopsy", true, true);
        check(act, true, "test", true, true);
        if (hasProduct) {
            check(act, true, "service", true, true);
        }
        check(act, false, "vaccination", true, true);
        check(act, true, "Jo Bloggs", true, true);
        check(act, false, "Jo Bloggs", false, true);
    }

    private void check(Act act, boolean match, String text, boolean searchClinicians, boolean searchBatches) {
        IArchetypeService service = getArchetypeService();
        service.deriveValues(act);
        TextSearch search = new TextSearch(text, searchClinicians, searchBatches, service);
        assertEquals(match, search.test(act));
    }
}
