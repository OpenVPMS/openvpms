/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.dispatcher.Confirmation;
import org.openvpms.laboratory.internal.dispatcher.OrderDispatcher;
import org.openvpms.laboratory.internal.dispatcher.OrderService;

/**
 * Test implementation of {@link OrderDispatcher}.
 *
 * @author Tim Anderson
 */
public class TestOrderDispatcher implements OrderDispatcher {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The order service.
     */
    private final OrderService orderService;

    /**
     * Constructs a {@link TestOrderDispatcher}.
     *
     * @param service the archetype service
     */
    public TestOrderDispatcher(IArchetypeService service) {
        this.service = service;
        orderService = new OrderService(service, new LaboratoryRules(service));
    }

    /**
     * Creates a laboratory order.
     * <p/>
     * If the investigation has its <em>confirmOrder</em> set to {@code true}, the order will be created for
     * manual submission, otherwise it will be queued.
     *
     * @param investigation the investigation to place the order for
     */
    @Override
    public void create(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        boolean confirmOrder = bean.getBoolean("confirmOrder");
        Act order = bean.getTarget("order", Act.class);
        if (order == null) {
            if (confirmOrder) {
                orderService.createOrder(investigation, false);
            } else {
                orderService.createOrder(investigation, true);
            }
        }
    }

    /**
     * Places an order synchronously.
     *
     * @param investigation the investigation
     * @return the order confirmation, or {@code null} if no confirmation is required
     * @throws LaboratoryException if the laboratory cannot be contacted
     */
    @Override
    public Confirmation order(Act investigation) {
        throw new UnsupportedOperationException();
    }

    /**
     * Queues an order cancellation.
     *
     * @param investigation the investigation to cancel
     */
    @Override
    public void cancel(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        Act order = bean.getTarget("order", Act.class);
        if (order != null) {
            orderService.cancel(order);
        }
    }
}
