/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.TextDocumentHandler;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.macro.impl.MacroTestHelper;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientNoteEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientNoteEditorTestCase extends AbstractIMObjectEditorTest<PatientNoteEditor> {

    /**
     * Constructs a {@link PatientNoteEditorTestCase}.
     */
    public PatientNoteEditorTestCase() {
        super(PatientNoteEditor.class, PatientArchetypes.CLINICAL_NOTE);
    }

    /**
     * Tests the {@link PatientNoteEditor} for an <em>act.patientClinicalNote</em>.
     */
    @Test
    public void testNoteEditorForClinicalNote() {
        checkNoteEditor(PatientArchetypes.CLINICAL_NOTE);
    }

    /**
     * Tests the {@link PatientNoteEditor} for an <em>act.patientClinicalAddendum</em>.
     */
    @Test
    public void testNoteEditorForClinicalAddendum() {
        checkNoteEditor(PatientArchetypes.CLINICAL_ADDENDUM);
    }

    /**
     * Tests the {@link PatientNoteEditor#newInstance()} method.
     */
    @Override
    @Test
    public void testNewInstance() {
        checkNewInstance(PatientArchetypes.CLINICAL_NOTE, PatientNoteEditor.class);
        checkNewInstance(PatientArchetypes.CLINICAL_ADDENDUM, PatientNoteEditor.class);
    }

    /**
     * Verifies a {@link PatientNoteEditor} is created for <em>act.patientClinicalNote</em> and
     * <em>act.patientClinicalAddendum</em> instances by {@link IMObjectEditorFactory}.
     */
    @Test
    @Override
    public void testFactory() {
        checkFactory(PatientArchetypes.CLINICAL_NOTE, PatientNoteEditor.class);
        checkFactory(PatientArchetypes.CLINICAL_ADDENDUM, PatientNoteEditor.class);
    }

    /**
     * Tests macro expansion in a note.
     */
    @Test
    public void testMacroExpansion() {
        MacroTestHelper.createMacro("@patient", "$patient.name");
        Party patient = TestHelper.createPatient("Fido", true);
        LocalContext local = new LocalContext();
        local.setPatient(patient);
        LayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        DocumentAct act = create(PatientArchetypes.CLINICAL_NOTE, DocumentAct.class);
        PatientNoteEditor editor = new PatientNoteEditor(act, null, context);

        editor.setNote("@patient");
        assertEquals("Fido", editor.getNote());
    }

    /**
     * Tests the {@link PatientNoteEditor} for the specified archetype.
     *
     * @param archetype the note archetype
     */
    private void checkNoteEditor(String archetype) {
        Party patient = TestHelper.createPatient();
        LocalContext local = new LocalContext();
        local.setPatient(patient);
        LayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        DocumentAct act = create(archetype, DocumentAct.class);
        IMObjectBean bean = getBean(act);
        PatientNoteEditor editor = new PatientNoteEditor(act, null, context);

        editor.getComponent();
        assertTrue(editor.isModified());
        assertTrue(SaveHelper.save(editor));

        assertFalse(editor.isModified());
        assertNull(editor.getNote());
        assertNull(act.getDocument());
        assertNull(bean.getString("note"));

        String text1 = StringUtils.repeat("a", 5000);
        editor.setNote(text1);
        assertTrue(editor.isModified());
        assertTrue(SaveHelper.save(editor));
        assertEquals(text1, bean.getString("note"));  // verify the note node is populated
        assertNull(act.getDocument());
        String text2 = text1 + "b";

        editor.setNote(text2);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("note"));           // text should move to the document
        Reference document1 = act.getDocument();
        assertNotNull(document1);
        checkDocument(document1, text2);

        String text3 = text2 + "c";
        editor.setNote(text3);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("note"));
        Reference document2 = act.getDocument();

        assertEquals(document1, document2);  // should update the same document
        checkDocument(document2, text3);

        // verify that when the text is shortened below the field length, the text moves to the node and the
        // document is removed
        editor.setNote("short");
        assertTrue(SaveHelper.save(editor));
        assertNull(act.getDocument());        // verify the act no longer has any document
        assertNull(get(document2));
        assertEquals("short", bean.getString("note"));

        // now verify that putting the long text back in creates a new document
        editor.setNote(text3);
        assertTrue(SaveHelper.save(editor));
        assertNull(bean.getString("note"));
        Reference document3 = act.getDocument();
        assertNotNull(document3);
        checkDocument(act.getDocument(), text3);
    }

    /**
     * Verifies a document contains the expected text.
     *
     * @param documentRef the document reference
     * @param text        the expected text
     */
    private void checkDocument(Reference documentRef, String text) {
        Document document = get(documentRef, Document.class);
        assertNotNull(document);
        TextDocumentHandler handler = new TextDocumentHandler(getArchetypeService());
        assertEquals(text, handler.toString(document));
    }

}
