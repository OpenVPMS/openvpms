/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.till;

import nextapp.echo2.app.ListBox;
import nextapp.echo2.app.button.AbstractButton;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.deposit.DepositTestHelper;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.SelectionDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.account.ReverseConfirmationDialog;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.finance.till.TillBalanceStatus.CLEARED;
import static org.openvpms.archetype.rules.finance.till.TillBalanceStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.finance.till.TillBalanceStatus.UNCLEARED;

/**
 * Tests the {@link TillCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class TillCRUDWindowTestCase extends AbstractAppTest {

    /**
     * The test till.
     */
    private Entity till;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        customer = TestHelper.createCustomer();
        till = TestHelper.createTill();
    }

    /**
     * Verifies that payments can be added to IN_PROGRESS till balances.
     */
    @Test
    public void testCreatePayment() {
        checkCreatePaymentRefund(CustomerAccountArchetypes.PAYMENT);
    }

    /**
     * Verifies that refunds can be added to IN_PROGRESS till balances.
     */
    @Test
    public void testCreateRefund() {
        checkCreatePaymentRefund(CustomerAccountArchetypes.REFUND);
    }

    /**
     * Verifies that general users cannot edit payments.
     */
    @Test
    public void testUsersCannotEditPayments() {
        User user = TestHelper.createUser(false);
        checkCanEditPayment(false, user);
    }

    /**
     * Verifies that payments can only be edited by administrators or those with Till Balance Adjustment create and
     * save authorities, for UNCLEARED or IN_PROGRESS balances.
     */
    @Test
    public void testAdminUsersCanEditPayments() {
        User admin = TestHelper.createAdministrator(false);
        checkCanEditPayment(true, admin);

        ArchetypeAwareGrantedAuthority createTBA = userFactory.createAuthority(
                "create", TillArchetypes.TILL_BALANCE_ADJUSTMENT);
        ArchetypeAwareGrantedAuthority saveTBA = userFactory.createAuthority(
                "save", TillArchetypes.TILL_BALANCE_ADJUSTMENT);
        User user = userFactory.newUser()
                .addRoles(userFactory.createRole(createTBA, saveTBA))
                .build();
        checkCanEditPayment(true, user);
    }

    /**
     * Verifies that refunds cannot be edited by any user.
     */
    @Test
    public void testCannotEditRefund() {
        FinancialAct refund1 = FinancialTestHelper.createRefund(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(refund1);

        checkCannotEdit(refund1, TestHelper.createUser(false));

        FinancialAct refund2 = FinancialTestHelper.createRefund(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(refund2);

        checkCannotEdit(refund2, TestHelper.createAdministrator(false));
    }

    /**
     * Verifies that if the till is cleared, the payment cannot be edited.
     */
    @Test
    public void testEditPaymentForClearedTill() {
        Context context = new LocalContext();
        context.setUser(TestHelper.createAdministrator(false));
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);

        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);
        FinancialAct balance = getTillBalance(payment);
        assertEquals(UNCLEARED, balance.getStatus());

        window.setObject(balance);
        window.onChildActSelected(payment);
        window.edit();
        EditDialog dialog1 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog1);
        EchoTestHelper.fireDialogButton(dialog1, EditDialog.OK_ID);
        assertNull(EchoTestHelper.findEditDialog());

        // verify the payment can be edited for an IN_PROGRESS balance
        balance.setStatus(IN_PROGRESS);
        save(balance);
        window.onChildActSelected(payment);
        window.edit();
        EditDialog dialog2 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog2);
        EchoTestHelper.fireDialogButton(dialog2, EditDialog.OK_ID);
        assertNull(EchoTestHelper.findEditDialog());

        // verify the payment cannot be edited for an CLEARED balance
        balance.setStatus(CLEARED);
        save(balance);
        window.onChildActSelected(payment);

        window.edit();
        assertNull(EchoTestHelper.findEditDialog());
        ErrorDialog errorDialog = EchoTestHelper.getWindowPane(ErrorDialog.class);
        assertEquals("This payment cannot be edited as it linked to a Cleared Till Balance", errorDialog.getMessage());
    }

    /**
     * Verifies that adjustments can be made to UNCLEARED and IN_PROGRESS till balances.
     */
    @Test
    public void testCreateAdjustment() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());

        // verify an adjustment cannot be created until a till balance is selected
        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.ADJUST_ID, false);

        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.ADJUST_ID, true);

        // create an adjustment, and verify it updates the till balance
        window.onAdjust();
        EditDialog dialog1 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog1);
        assertTrue(dialog1.getEditor() instanceof TillBalanceAdjustmentEditor);
        TillBalanceAdjustmentEditor editor = (TillBalanceAdjustmentEditor) dialog1.getEditor();
        editor.setAmount(BigDecimal.TEN);
        EchoTestHelper.fireDialogButton(dialog1, EditDialog.OK_ID);

        tillBalance = get(tillBalance);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.ZERO, tillBalance.getTotal());

        // now set the status to IN_PROGRESS and verify a new adjustment can be made
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.ADJUST_ID, true);

        window.onAdjust();
        EditDialog dialog2 = EchoTestHelper.findEditDialog();
        assertNotNull(dialog2);
        assertTrue(dialog2.getEditor() instanceof TillBalanceAdjustmentEditor);
        TillBalanceAdjustmentEditor editor2 = (TillBalanceAdjustmentEditor) dialog2.getEditor();

        editor2.setAmount(BigDecimal.TEN);
        editor2.setCredit(true);
        EchoTestHelper.fireDialogButton(dialog2, EditDialog.OK_ID);

        tillBalance = get(tillBalance);
        assertEquals(IN_PROGRESS, tillBalance.getStatus());
        checkEquals(BigDecimal.TEN, tillBalance.getTotal());

        // now set the status to CLEARED and verify adjustments can't be made
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.ADJUST_ID, false);
    }

    /**
     * Verify that adjustments can only be edited for UNCLEARED till balances.
     */
    @Test
    public void testEditAdjustment() {
        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));

        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.TEN, tillBalance.getTotal());

        // create an adjustment for $10 and verify the balance reverts to zero
        FinancialAct adjustment = createAdjustment(tillBalance, context);

        tillBalance = get(tillBalance);
        checkEquals(BigDecimal.ZERO, tillBalance.getTotal());

        // now verify the adjustment can be edited, when it is selected
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.EDIT_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.EDIT_ID, false);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.EDIT_ID, true);

        // edit the adjustment, and toggle the credit flag. The balance should go to $20
        window.edit();

        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertNotNull(dialog);
        assertTrue(dialog.getEditor() instanceof TillBalanceAdjustmentEditor);
        TillBalanceAdjustmentEditor editor2 = (TillBalanceAdjustmentEditor) dialog.getEditor();
        editor2.setCredit(true);
        EchoTestHelper.fireDialogButton(dialog, EditDialog.OK_ID);

        tillBalance = get(tillBalance);
        checkEquals(BigDecimal.valueOf(20), tillBalance.getTotal());

        // verify adjustments cannot be edited when the till balance is IN_PROGRESS
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.EDIT_ID, false);

        // verify adjustments cannot be edited when the till balance is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.EDIT_ID, false);
    }

    /**
     * Verifies that payments can be reversed for IN_PROGRESS till balances.
     */
    @Test
    public void testReversePayment() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);
        checkReverse(payment);
    }

    /**
     * Verifies that refunds can be reversed for IN_PROGRESS till balances.
     */
    @Test
    public void testReverseRefund() {
        FinancialAct refund = FinancialTestHelper.createRefund(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(refund);
        checkReverse(refund);
    }

    /**
     * Verifies that adjustments cannot be reversed.
     */
    @Test
    public void testCannotReverseAdjustment() {
        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));

        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());

        // create an adjustment for $10 and verify the balance reverts to zero
        FinancialAct adjustment = createAdjustment(tillBalance, context);

        tillBalance = get(tillBalance);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.ZERO, tillBalance.getTotal());

        // verify that the reverse button is not enabled when the till balance is UNCLEARED
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);

        // verify that the reverse button is not enabled when the till balance is IN_PROGRESS
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);

        // verify that the reverse button is not enabled when the till balance is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
    }

    /**
     * Verifies that payments can be transferred when the till balance is UNCLEARED.
     */
    @Test
    public void testTransferPayment() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        checkTransfer(payment);
    }

    /**
     * Verifies that refunds can be transferred when the till balance is UNCLEARED.
     */
    @Test
    public void testTransferRefund() {
        FinancialAct refund = FinancialTestHelper.createRefund(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(refund);

        checkTransfer(refund);
    }

    /**
     * Verifies that till balance adjustments cannot be transferred.
     */
    @Test
    public void testCannotTransferAdjustment() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.TEN, tillBalance.getTotal());

        FinancialAct adjustment = createAdjustment(tillBalance, context);

        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);

        // verify it cannot be transferred when the till balance status is IN_PROGRESS
        tillBalance = get(tillBalance);
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(adjustment);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);

        // verify it cannot be transferred when the till balance status is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(tillBalance);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
    }

    /**
     * Verifies that clear can be run for an UNCLEARED till balance.
     */
    @Test
    public void testClearForUnclearedTillBalance() {
        checkClear(UNCLEARED);
    }

    /**
     * Verifies that clear can be run for an IN_PROGRESS till balance.
     */
    @Test
    public void testClearForInProgressTillBalance() {
        checkClear(IN_PROGRESS);
    }

    /**
     * Verifies that clear cannot be run for a CLEARED till balance,
     */
    @Test
    public void testClearCannotBeRunForClearedTillBalance() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());

        tillBalance.setStatus(CLEARED);
        save(tillBalance);

        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TestTillCRUDWindow.CLEAR_ID, false);
        window.setObject(tillBalance);

        checkButton(window, TestTillCRUDWindow.CLEAR_ID, false);
    }

    /**
     * Verifies that 'start clear' can only be selected for UNCLEARED till balances.
     */
    @Test
    public void testStartClear() {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());

        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TestTillCRUDWindow.START_CLEAR_ID, false);

        window.setObject(tillBalance);
        checkButton(window, TestTillCRUDWindow.START_CLEAR_ID, true);

        window.onStartClear(tillBalance);

        StartClearTillDialog dialog = EchoTestHelper.getWindowPane(StartClearTillDialog.class);
        EchoTestHelper.fireDialogButton(dialog, StartClearTillDialog.OK_ID);

        tillBalance = get(tillBalance);
        assertEquals(IN_PROGRESS, tillBalance.getStatus());

        // verify the button is no longer enabled
        checkButton(window, TestTillCRUDWindow.START_CLEAR_ID, false);

        // run the start clear anyway. This could occur if a different user has run start clear and the current user
        // view is out-of-date
        window.onStartClear(tillBalance);
        ErrorDialog errorDialog = EchoTestHelper.getWindowPane(ErrorDialog.class);
        assertEquals("Cannot clear till. Clear the balance with Clear In Progress status first.",
                     errorDialog.getMessage());

        // set the balance to CLEARED and verify 'start clear' is not enabled
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TestTillCRUDWindow.START_CLEAR_ID, false);

        // run the start clear anyway. This will generate a misleading error message. TODO
        window.onStartClear(tillBalance);
        ErrorDialog errorDialog2 = EchoTestHelper.getWindowPane(ErrorDialog.class);
        assertEquals("Cannot clear till. Clear the balance with Clear In Progress status first.",
                     errorDialog2.getMessage());
    }

    /**
     * Helper to create an adjustment for $10, linked to the specified till balance.
     *
     * @param tillBalance the till balance
     * @param context     the context
     * @return the adjustment
     */
    private FinancialAct createAdjustment(FinancialAct tillBalance, Context context) {
        FinancialAct adjustment = create(TillArchetypes.TILL_BALANCE_ADJUSTMENT, FinancialAct.class);
        IMObjectBean bean = getBean(adjustment);
        bean.setTarget("till", till);
        TillBalanceAdjustmentEditor editor1 = new TillBalanceAdjustmentEditor(
                adjustment, tillBalance, new DefaultLayoutContext(context, new HelpContext("foo", null)));
        editor1.getComponent();
        editor1.setAmount(BigDecimal.TEN);
        assertTrue(SaveHelper.save(editor1));
        return adjustment;
    }

    /**
     * Verifies that payments and refunds can be transferred when the till balance is UNCLEARED.
     */
    private void checkTransfer(FinancialAct act) {
        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));

        BigDecimal expected = (act.isCredit()) ? BigDecimal.TEN : BigDecimal.TEN.negate();

        FinancialAct tillBalance = getTillBalance(act);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(expected, tillBalance.getTotal());

        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(act);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, true);

        Entity till2 = TestHelper.createTill();
        window.onTransfer();

        ListBox list = getSelectionDialogList();
        EchoTestHelper.click(list, till2);

        tillBalance = get(tillBalance);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.ZERO, tillBalance.getTotal());

        FinancialAct tillBalance2 = getTillBalance(act);
        assertNotEquals(tillBalance, tillBalance2);
        assertEquals(UNCLEARED, tillBalance2.getStatus());
        checkEquals(expected, tillBalance2.getTotal());

        // verify it cannot be transferred when the till balance status is IN_PROGRESS
        tillBalance2.setStatus(IN_PROGRESS);
        save(tillBalance2);
        window.setObject(tillBalance2);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(act);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);

        // verify it cannot be transferred when the till balance status is CLEARED
        tillBalance2.setStatus(CLEARED);
        save(tillBalance2);
        window.setObject(tillBalance2);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
        window.onChildActSelected(act);
        checkButton(window, TillCRUDWindow.TRANSFER_ID, false);
    }

    /**
     * Verifies that payments and refunds can be reversed for IN_PROGRESS till balances.
     *
     * @param act the act to reverse
     */
    private void checkReverse(FinancialAct act) {
        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        context.setPractice(create(PracticeArchetypes.PRACTICE, Party.class));

        FinancialAct tillBalance = getTillBalance(act);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        BigDecimal expected = (act.isCredit()) ? BigDecimal.TEN : BigDecimal.TEN.negate();
        checkEquals(expected, tillBalance.getTotal());

        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.onChildActSelected(act);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);

        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.onChildActSelected(act);
        checkButton(window, TillCRUDWindow.REVERSE_ID, true);

        window.onReverse();

        ReverseConfirmationDialog dialog = EchoTestHelper.getWindowPane(ReverseConfirmationDialog.class);
        EchoTestHelper.fireDialogButton(dialog, ReverseConfirmationDialog.OK_ID);

        // verify that the balance is now zero
        tillBalance = get(tillBalance);
        assertEquals(IN_PROGRESS, tillBalance.getStatus());
        checkEquals(BigDecimal.ZERO, tillBalance.getTotal());

        act = get(act);
        FinancialAct reversal = getBean(act).getTarget("reversal", FinancialAct.class);
        assertNotNull(reversal);

        // verify that the act cannot be reversed when the till balance is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
        window.onChildActSelected(reversal);
        checkButton(window, TillCRUDWindow.REVERSE_ID, false);
    }

    /**
     * Verifies that payments and refunds can be added to IN_PROGRESS till balances.
     *
     * @param archetype the payment/refund archetype
     */
    private void checkCreatePaymentRefund(String archetype) {
        // create a payment to generate a new till balance
        FinancialAct initial = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(initial);

        FinancialAct tillBalance = getTillBalance(initial);
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkEquals(BigDecimal.TEN, tillBalance.getTotal());

        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        context.setCustomer(customer);
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);

        // verify that the new button is not enabled
        checkButton(window, TillCRUDWindow.NEW_ID, false);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.NEW_ID, false);

        // change the till balance status to IN_PROGRESS and verify the new button is now enabled
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.NEW_ID, true);
        window.onNew();

        // select the appropriate archetype in the archetype selection dialog
        ListBox listBox = getSelectionDialogList();
        assertEquals(2, listBox.getModel().size()); // only payment and refund can be selected
        int index = archetype.equals(CustomerAccountArchetypes.PAYMENT) ? 0 : 1;
        EchoTestHelper.click(listBox, index);

        // verify an edit dialog is displayed with a TillPaymentEditor containing the correct archetype
        EditDialog dialog = EchoTestHelper.findEditDialog();
        assertTrue(dialog.getEditor() instanceof TillPaymentEditor);
        TillPaymentEditor editor = (TillPaymentEditor) dialog.getEditor();
        assertEquals(archetype, editor.getObject().getArchetype());

        // add a payment/refund item and save it
        IMObjectEditor item = editor.getUnsavedItem();
        assertNotNull(item);
        item.getProperty("amount").setValue(BigDecimal.TEN);
        FinancialAct act = (FinancialAct) dialog.getEditor().getObject();
        EchoTestHelper.fireDialogButton(dialog, EditDialog.OK_ID);

        // verify the act has been saved, is POSTED, and has the correct amount
        act = get(act);
        assertEquals(ActStatus.POSTED, act.getStatus());
        checkEquals(BigDecimal.TEN, act.getTotal());

        // till balance should still be IN_PROGRESS, but the total should include the new act
        tillBalance = get(tillBalance);
        assertEquals(ActStatus.IN_PROGRESS, tillBalance.getStatus());
        if (act.isA(CustomerAccountArchetypes.PAYMENT)) {
            checkEquals(BigDecimal.valueOf(20), tillBalance.getTotal());
        } else {
            // refund
            checkEquals(BigDecimal.ZERO, tillBalance.getTotal());
        }

        // now clear the till balance, and verify payments and refunds can no longer be created
        tillBalance.setStatus(CLEARED);
        save(tillBalance);

        window.setObject(tillBalance);
        checkButton(window, TillCRUDWindow.NEW_ID, false);
    }

    /**
     * Returns the list box from the displayed selection dialog.
     *
     * @return the list box
     */
    private ListBox getSelectionDialogList() {
        SelectionDialog selection = EchoTestHelper.getWindowPane(SelectionDialog.class);
        ListBox listBox = EchoTestHelper.findComponent(selection, ListBox.class);
        assertNotNull(listBox);
        return listBox;
    }

    /**
     * Verifies that clear can be run for an UNCLEARED or IN_PROGRESS till balance.
     *
     * @param status the till balance status. Must be one of UNCLEARED or IN_PROGRESS
     */
    private void checkClear(String status) {
        Party location = TestHelper.createLocation();
        DepositTestHelper.createDepositAccount(location); // this will be selected in the ClearTillDialog
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);
        assertEquals(UNCLEARED, tillBalance.getStatus());

        tillBalance.setStatus(status);
        save(tillBalance);

        Context context = new LocalContext();
        context.setUser(TestHelper.createUser(false));
        context.setLocation(location);
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        checkButton(window, TestTillCRUDWindow.CLEAR_ID, false);
        window.setObject(tillBalance);

        checkButton(window, TestTillCRUDWindow.CLEAR_ID, true);

        window.onClear(tillBalance);

        ClearTillDialog dialog = EchoTestHelper.getWindowPane(ClearTillDialog.class);
        EchoTestHelper.fireDialogButton(dialog, ClearTillDialog.OK_ID);

        tillBalance = get(tillBalance);
        assertEquals(CLEARED, tillBalance.getStatus());
    }

    /**
     * Verifies that a object cannot be edited, for an till balance status.
     *
     * @param object the object associated with a till balance
     * @param user   the user to test editing with
     */
    private void checkCannotEdit(FinancialAct object, User user) {
        FinancialAct tillBalance = getTillBalance(object);

        // verify the user cannot edit when the till balance status is UNCLEARED
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkCanEdit(user, false, object, tillBalance);

        // verify cannot edit when the till balance status is IN PROGRESS
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        checkCanEdit(user, false, object, tillBalance);

        // verify the user cannot edit when the till balance status is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        checkCanEdit(user, false, object, tillBalance);
    }

    /**
     * Returns the till balance for a payment, refund or till balance adjustment.
     *
     * @param act the act associated with the till balance
     * @return the corresponding till balance
     */
    private FinancialAct getTillBalance(FinancialAct act) {
        FinancialAct tillBalance = getBean(act).getSource("tillBalance", FinancialAct.class);
        assertNotNull(tillBalance);
        return tillBalance;
    }

    /**
     * Checks if a user can administratively edit an payment.
     *
     * @param edit if {@code true}, the user can edit the object, {@code false} if they can't
     * @param user the user
     */
    private void checkCanEditPayment(boolean edit, User user) {
        FinancialAct payment = FinancialTestHelper.createPayment(BigDecimal.TEN, customer, till, ActStatus.POSTED);
        save(payment);

        FinancialAct tillBalance = getTillBalance(payment);

        // verify only admin users can edit the payment when the till balance status is UNCLEARED
        assertEquals(UNCLEARED, tillBalance.getStatus());
        checkCanEdit(user, edit, payment, tillBalance);

        // verify only admin user can edit the payment when the till balance status is IN_PROGRESS
        tillBalance.setStatus(IN_PROGRESS);
        save(tillBalance);
        checkCanEdit(user, edit, payment, tillBalance);

        // verify no user can edit the payment when the till balance status is CLEARED
        tillBalance.setStatus(CLEARED);
        save(tillBalance);
        checkCanEdit(user, false, payment, tillBalance);
    }

    /**
     * Checks if a user can edit an object.
     *
     * @param user        the user
     * @param edit        if {@code true}, the user can edit the object, {@code false} if they can't
     * @param object      the object
     * @param tillBalance the till balance the object is associated with
     */
    private void checkCanEdit(User user, boolean edit, FinancialAct object, FinancialAct tillBalance) {
        Context context = new LocalContext();
        context.setUser(user);
        TestTillCRUDWindow window = new TestTillCRUDWindow(context);
        window.setObject(tillBalance);
        checkButton(window, TestTillCRUDWindow.EDIT_ID, false);
        window.onChildActSelected(object);
        checkButton(window, TestTillCRUDWindow.EDIT_ID, edit);
    }

    /**
     * Verifies that a button exists and has the expected enabled flag.
     *
     * @param window  the window
     * @param id      the button identifier
     * @param enabled if {@code true}, expect the button to be enabled; if {@code false} it should be disabled
     */
    private void checkButton(TestTillCRUDWindow window, String id, boolean enabled) {
        AbstractButton button = window.getButtons().getButton(id);
        assertNotNull(button);
        assertEquals(enabled, button.isEnabled());
    }

    private static class TestTillCRUDWindow extends TillCRUDWindow {

        public TestTillCRUDWindow(Context context) {
            super(context, new HelpContext("foo", null));
            getComponent();
        }

        /**
         * Returns the button set.
         *
         * @return the button set
         */
        @Override
        public ButtonSet getButtons() {
            return super.getButtons();
        }

        /**
         * Invoked when a child act is selected/deselected.
         *
         * @param child the child act. May be {@code null}
         */
        @Override
        public void onChildActSelected(FinancialAct child) {
            super.onChildActSelected(child);
        }
    }
}
