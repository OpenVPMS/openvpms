/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderRule;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.test.TestDocumentPrinter;
import org.openvpms.print.test.TestDocumentPrinterService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ReminderPrintProcessor}.
 *
 * @author Tim Anderson
 */
public class ReminderPrintProcessorTestCase extends AbstractPatientReminderProcessorTest<GroupedReminders> {

    /**
     * The reminder processor.
     */
    private TestReminderPrintProcessor processor;

    /**
     * The printer factory.
     */
    @Autowired
    private IMPrinterFactory printerFactory;

    /**
     * The printer service.
     */
    @Autowired
    private TestDocumentPrinterService printerService;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The practice location, used for printer selection.
     */
    private Party practiceLocation;

    /**
     * The vaccination reminder template.
     */
    private Entity documentTemplate;


    /**
     * Constructs a {@link ReminderPrintProcessorTestCase}.
     */
    public ReminderPrintProcessorTestCase() {
        super(ContactArchetypes.LOCATION);
    }

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        documentTemplate = reminderFactory.createVaccinationReminderTemplate();
        reminderFactory.updateReminderType(reminderType)
                .defaultInterval(0, DateUnits.WEEKS)
                .newCount()
                .count(0)
                .template(documentTemplate)
                .newRule().print().sendTo(ReminderRule.SendTo.ANY).add()
                .add()
                .build();

        ArchetypeService service = getArchetypeService();
        ReminderTypes reminderTypes = new ReminderTypes(service);
        ReminderConfiguration config = createConfiguration();
        practiceLocation = practiceFactory.createLocation();
        processor = new TestReminderPrintProcessor(reminderTypes, config, practiceLocation, printerFactory);
    }

    /**
     * Verifies that reminders are sent to the REMINDER contact.
     */
    @Test
    public void testReminderContact() {
        Contact location1 = createLocation("1 St Georges Rd", true, "REMINDER");
        Contact location2 = createLocation("2 Keon St", false, null);
        Contact location3 = createLocation("3 Hutton St", false, null);
        customer.addContact(location1);
        customer.addContact(location2);
        customer.addContact(location3);

        checkSend(null, location1);
    }

    /**
     * Verifies that prints can be address to a contact different to the default.
     */
    @Test
    public void testOverrideContact() {
        Contact location1 = createLocation("1 St Georges Rd", true, "REMINDER");
        Contact location2 = createLocation("2 Keon St", false, null);
        customer.addContact(location1);
        customer.addContact(location2);

        checkSend(location2, location2);
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder type has no reminder count.
     */
    @Test
    public void testMissingReminderCount() {
        checkMissingReminderCount(createLocation("1 St Georges Rd", true, "REMINDER"));
    }

    /**
     * Verifies that the reminder item status is set to ERROR when the reminder count has no template.
     */
    @Test
    public void testMissingReminderCountTemplate() {
        checkMissingReminderCountTemplate(createLocation("1 St Georges Rd", true, "REMINDER"));
    }

    /**
     * Verify that the practice location is used for printer selection, and not that associated with the customer,
     * which should solely be used for document merges.
     */
    @Test
    public void testPracticeLocationUsedForPrinterSelection() {
        Contact contact = createLocation("1 St Georges Rd", true, "REMINDER");
        customer.addContact(contact);

        Party customerLocation = practiceFactory.createLocation();
        customerFactory.updateCustomer(customer)
                .practice(customerLocation)
                .build();

        TestDocumentPrinter printer1 = new TestDocumentPrinter("printer1");
        TestDocumentPrinter printer2 = new TestDocumentPrinter("printer2");
        printerService.addPrinter(printer1);
        printerService.addPrinter(printer2);

        documentFactory.updateTemplate(documentTemplate)
                .printer("printer1")
                .location(practiceLocation)
                .add()
                .printer("printer2")
                .location(customerLocation)
                .add()
                .build();

        checkSend(null, contact);

        // verify the printer and context used
        assertEquals(practiceLocation, processor.getContext().getLocation());

        MailContext mailContext = processor.getMailContext();
        assertTrue(mailContext instanceof CustomerMailContext);
        assertEquals(customerLocation, ((CustomerMailContext) mailContext).getLocation());

        assertNotNull(processor.getPrinter());
        assertEquals("printer1", processor.getPrinter().getId());
        assertEquals(1, printer1.getPrinted().size());
        assertTrue(printer2.getPrinted().isEmpty());
    }

    /**
     * Returns the reminder processor.
     *
     * @return the reminder processor
     */
    @Override
    protected ReminderPrintProcessor getProcessor() {
        return processor;
    }

    /**
     * Creates a PENDING reminder item for reminder count 0.
     *
     * @param send    the send date
     * @param dueDate the due date
     * @return a new reminder item
     */
    @Override
    protected Act createReminderItem(Date send, Date dueDate) {
        return reminderFactory.newPrintReminder()
                .sendDate(send)
                .dueDate(dueDate)
                .build();
    }

    /**
     * Sends an email reminder.
     *
     * @param contact the contact to use. May be {@code null}
     * @param to      the expected to address
     */
    private void checkSend(Contact contact, Contact to) {
        GroupedReminders reminders = (GroupedReminders) prepare(contact);
        processor.process(reminders);
        try {
            // give the print thread time to complete
            Thread.sleep(2000);
        } catch (InterruptedException ignore) {
            // no-op
        }
        assertEquals(to, reminders.getContact());
    }

    /**
     * Prepares a reminder for send.
     *
     * @param contact the contact to use. May be {@code null}
     * @return the reminders
     */
    private PatientReminders prepare(Contact contact) {
        Date tomorrow = DateRules.getTomorrow();
        Act item = createReminderItem(DateRules.getToday(), tomorrow);
        Act reminder = createReminder(tomorrow, reminderType, item);

        return prepare(item, reminder, contact);
    }

    private class TestReminderPrintProcessor extends ReminderPrintProcessor {

        private Context context;

        private MailContext mailContext;

        private DocumentPrinter printer;

        public TestReminderPrintProcessor(ReminderTypes reminderTypes, ReminderConfiguration config, Party location,
                                          IMPrinterFactory printerFactory) {
            super(new HelpContext("foo", null), reminderTypes, reminderRules, patientRules, location,
                  practice, getArchetypeService(), config, printerFactory,
                  Mockito.mock(CommunicationLogger.class), actionFactory);
        }

        /**
         * Returns the last context used when printing.
         *
         * @return the last print context. May be {@code null}
         */
        public Context getContext() {
            return context;
        }

        /**
         * Returns the last mail context used.
         *
         * @return the  mail context. May be {@code null}
         */
        public MailContext getMailContext() {
            return mailContext;
        }

        /**
         * Returns the last printer used.
         *
         * @return the last printer used. May be {@code null}
         */
        public DocumentPrinter getPrinter() {
            return printer;
        }

        /**
         * Creates an interactive printer.
         *
         * @param printer the printer
         * @param context the print context, used to select the printer
         * @return a new interactive printer
         */
        @Override
        protected <T> InteractiveIMPrinter<T> createPrinter(IMPrinter<T> printer, Context context) {
            this.context = context;
            return super.createPrinter(printer, context);
        }

        /**
         * Prints the reminder(s).
         *
         * @param iPrinter the interactive printer to use
         * @param printer  the document printer
         */
        @Override
        protected void print(InteractiveIMPrinter<?> iPrinter, DocumentPrinter printer) {
            super.print(iPrinter, printer);
            this.printer = printer;
            mailContext = iPrinter.getMailContext();
        }
    }
}
