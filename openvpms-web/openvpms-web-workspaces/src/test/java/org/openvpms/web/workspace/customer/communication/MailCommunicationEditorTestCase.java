/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import org.openvpms.archetype.rules.customer.CommunicationArchetypes;

/**
 * Tests the {@link MailCommunicationEditor}.
 *
 * @author Tim Anderson
 */
public class MailCommunicationEditorTestCase extends AbstractDocumentCommunicationEditorTest<MailCommunicationEditor> {

    /**
     * Constructs a {@link MailCommunicationEditorTestCase}.
     */
    public MailCommunicationEditorTestCase() {
        super(MailCommunicationEditor.class, CommunicationArchetypes.MAIL);
    }
}
