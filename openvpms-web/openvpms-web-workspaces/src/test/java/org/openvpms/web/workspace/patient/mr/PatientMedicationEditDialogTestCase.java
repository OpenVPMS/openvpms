/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditActions;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link PatientMedicationEditDialog}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class PatientMedicationEditDialogTestCase extends AbstractAppTest {

    /**
     * The dialog factory.
     */
    @Autowired
    private EditDialogFactory factory;

    /**
     * Verifies that the {@link EditDialogFactory} returns {@link PatientMedicationEditDialog} for
     * {@link PatientMedicationActEditor}.
     */
    @Test
    public void testFactory() {
        Act act = create(PatientArchetypes.PATIENT_MEDICATION, Act.class);
        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        PatientMedicationActEditor editor = new PatientMedicationActEditor(act, null, context);
        assertTrue(factory.supportsEditActions(editor));
        assertTrue(factory.create(editor, context.getContext()) instanceof PatientMedicationEditDialog);
        assertTrue(factory.create(editor, EditActions.applyOKCancel(), context.getContext())
                           instanceof PatientMedicationEditDialog);
    }
}
