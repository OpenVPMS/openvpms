/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.vetcheck;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.singleton.SingletonServiceImpl;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.internal.factory.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link VetCheckRules} class.
 *
 * @author Tim Anderson
 */
public class VetCheckRulesTestCase extends ArchetypeServiceTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Medical record rules.
     */
    @Autowired
    private MedicalRecordRules medicalRecordRules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The rules.
     */
    private VetCheckRules rules;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        practice = create(PracticeArchetypes.PRACTICE, Party.class);
        PracticeService practiceService = Mockito.mock(PracticeService.class);
        Mockito.when(practiceService.getPractice()).thenReturn(practice);

        rules = new VetCheckRules(domainService, medicalRecordRules, getArchetypeService(), transactionManager,
                                  practiceService, new SingletonServiceImpl(getArchetypeService(), transactionManager));
    }

    /**
     * Tests the {@link VetCheckRules#isVetCheckEnabled()} method
     */
    @Test
    public void testIsVetCheckEnabled() {
        assertFalse(rules.isVetCheckEnabled());
        IMObjectBean bean = getBean(practice);
        bean.setValue("enableVetCheck", true);
        assertTrue(rules.isVetCheckEnabled());
    }

    /**
     * Tests the {@link VetCheckRules#getURL()} method.
     */
    @Test
    public void testGetURL() {
        assertEquals("https://plugin.vetcheck.it", rules.getURL());
    }

    /**
     * Tests the {@link VetCheckRules#getURL(Party)} method.
     */
    @Test
    public void testGetPatientURL() {
        lookupFactory.getState("VIC", "Victoria");

        Party customer = customerFactory.newCustomer()
                .lastName("Smith")
                .firstName("J")
                .addAddress("1 Smith Ave", "PRESTON", "VIC", "3072")
                .addMobilePhone("0412345678")
                .addEmail("foo@bar.com")
                .build();
        Party patient = patientFactory.newPatient()
                .name("Fido")
                .owner(customer)
                .sex("MALE")
                .addMicrochip("12345")
                .colour("Black")
                .dateOfBirth(TestHelper.getDate("2021-08-01"))
                .build();
        String url = "https://plugin.vetcheck.it?client_first_name=J&client_last_name=Smith" +
                     "&client_address=1%20Smith%20Ave,%20Preston%20Victoria%203072&client_phone=0412345678" +
                     "&client_email_address=foo@bar.com" +
                     "&patient_id=" + patient.getId() +
                     "&pet_name=Fido&pet_species=Canine&pet_breed&pet_sex=MALE%20Entire&pet_dob=2021-08-01&pet_weight" +
                     "&pet_color=Black&pet_microchip=12345";
        assertEquals(url, rules.getURL(patient));
    }

    /**
     * Tests the {@link VetCheckRules#addLink(Party, User, String)} method.
     */
    @Test
    public void testAddLink() {
        Party patient = patientFactory.createPatient();
        User clinician = userFactory.createClinician();
        String data = "share_link=https://vtck.it/v3/d1&document_type=handout" +
                      "&title=A Parasiticide Comparison Chart&patient_id=" + patient.getId();
        VetCheckLink link = rules.addLink(patient, clinician, data);
        assertNotNull(link);
        assertEquals("A Parasiticide Comparison Chart", link.getDescription());
        assertEquals("https://vtck.it/v3/d1", link.getUrl());
        Act act = link.getAct();
        assertNotNull(act);
        assertFalse(act.isNew());
        IMObjectBean bean = getBean(act);
        assertEquals(link.getDescription(), bean.getString("description"));
        assertEquals(link.getUrl(), bean.getString("url"));
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(clinician, bean.getTarget("clinician"));

        // verify the link is added to the most recent event
        Act event = medicalRecordRules.getEvent(patient);
        assertNotNull(event);
        assertEquals(event, bean.getSource("event"));
    }

    /**
     * Tests the {@link VetCheckRules#getMostRecentLinks(Party, int)} method.
     */
    @Test
    public void testGetMostRecentLinks() {
        Party patient = patientFactory.createPatient();
        User clinician = userFactory.createClinician();
        String data1 = "share_link=https://vtck.it/v3/A&document_type=handout&title=A";
        String data2 = "share_link=https://vtck.it/v3/B&document_type=handout&title=B";
        String data3 = "share_link=https://vtck.it/v3/C&document_type=handout&title=C";
        VetCheckLink link1 = rules.addLink(patient, clinician, data1);
        VetCheckLink link2 = rules.addLink(patient, clinician, data2);
        VetCheckLink link3 = rules.addLink(patient, clinician, data3);

        List<Act> links = rules.getMostRecentLinks(patient, 4);
        assertEquals(3, links.size());
        assertEquals(link3.getAct(), links.get(0));
        assertEquals(link2.getAct(), links.get(1));
        assertEquals(link1.getAct(), links.get(2));
    }
}
