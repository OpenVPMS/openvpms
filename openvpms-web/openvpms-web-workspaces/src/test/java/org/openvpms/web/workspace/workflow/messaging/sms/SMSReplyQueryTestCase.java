/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.archetype.test.builder.sms.TestSMSFactory;
import org.openvpms.archetype.test.builder.sms.TestSMSReplyBuilder;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Tests the {@link SMSReplyQuery}.
 *
 * @author Tim Anderson
 */
public class SMSReplyQueryTestCase extends AbstractSMSQueryTest {

    /**
     * The SMS factory.
     */
    @Autowired
    private TestSMSFactory smsFactory;

    /**
     * Creates a new query.
     *
     * @param context the context
     * @return a new query
     */
    @Override
    protected AbstractSMSQuery createQuery(Context context) {
        SMSReplyQuery query = new SMSReplyQuery(new DefaultLayoutContext(context, new HelpContext("foo", null)));
        query.getComponent();
        return query;
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if {@code true} save the object, otherwise don't save it
     * @return the new object
     */
    @Override
    protected Act createObject(String value, boolean save) {
        TestSMSReplyBuilder builder = newReply(value);
        return builder.build(save);
    }

    /**
     * Creates an SMS linked to a practice location.
     *
     * @param contactName the sender name
     * @param location    the practice location. May be {@code null}
     * @return the SMS
     */
    @Override
    protected Act createSMS(String contactName, Party location) {
        return newReply(contactName)
                .location(location)
                .build();
    }

    /**
     * Returns a partially populated reply.
     *
     * @param sender the SMS sender name
     * @return the SMS reply builder
     */
    private TestSMSReplyBuilder newReply(String sender) {
        return smsFactory.newReply()
                .sender(customerFactory.createCustomer("foo", sender))
                .phone("12345678")
                .message("test reply");
    }
}