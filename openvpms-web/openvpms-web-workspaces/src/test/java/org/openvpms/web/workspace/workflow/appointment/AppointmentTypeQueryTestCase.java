/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Before;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSetIterator;
import org.openvpms.web.workspace.workflow.ScheduleTypeQueryTest;


/**
 * Tests the {@link AppointmentTypeQuery} class.
 *
 * @author Tim Anderson
 */
public class AppointmentTypeQueryTestCase extends ScheduleTypeQueryTest {

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();

        // deactivate existing appointment types so they don't interfere with tests
        AppointmentTypeQuery query = new AppointmentTypeQuery(new LocalContext());
        query.setMaxResults(ArchetypeQuery.ALL_RESULTS);
        query.setActive(BaseArchetypeConstraint.State.ACTIVE);
        ResultSetIterator<Entity> iterator = new ResultSetIterator<Entity>(query.query());
        while (iterator.hasNext()) {
            Entity appointmentType = iterator.next();
            appointmentType.setActive(false);
            save(appointmentType);
        }
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    protected Query<Entity> createQuery() {
        return new AppointmentTypeQuery(null, new LocalContext());
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if <tt>true</tt> save the object, otherwise don't save it
     * @return the new object
     */
    protected Entity createObject(String value, boolean save) {
        return ScheduleTestHelper.createAppointmentType(value, save);
    }

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    protected String getUniqueValue() {
        return getUniqueValue("ZAppointmentType");
    }

    /**
     * Creates a new schedule.
     *
     * @return a new schedule
     */
    protected Party createSchedule() {
        return ScheduleTestHelper.createSchedule(15, "MINUTES", 2, null, TestHelper.createLocation());
    }

    /**
     * Adds a schedule type to a schedule.
     *
     * @param schedule     the schedule
     * @param scheduleType type the schedule type
     */
    protected void addScheduleType(Party schedule, Entity scheduleType) {
        ScheduleTestHelper.addAppointmentType(schedule, scheduleType, 1, false);
    }
}
