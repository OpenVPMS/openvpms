/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.schedule;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link ScheduleDeletionHandler}.
 *
 * @author Tim Anderson
 */
public class ScheduleDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Verifies that a schedule with no relationships can be deleted.
     */
    @Test
    public void testDeleteScheduleNoRelationships() {
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());

        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
    }

    /**
     * Verifies that a schedule with appointment types can be deleted.
     */
    @Test
    public void testDeleteScheduleWithAppointmentTypes() {
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addAppointmentType(appointmentType, 1, true)
                .build();
        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
        assertNotNull(get(appointmentType));  // appointment type should not be deleted
    }

    /**
     * Verifies that a schedule with work lists can be deleted.
     */
    @Test
    public void testDeleteScheduleWithWorkLists() {
        Entity workList = schedulingFactory.createWorkList();
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addWorkLists(workList)
                .build();
        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
        assertNotNull(get(workList));  // work list should not be deleted
    }

    /**
     * Verifies that a schedule with templates can be deleted.
     */
    @Test
    public void testDeleteScheduleWithTemplates() {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_LETTER);
        Entity schedule = schedulingFactory.newSchedule()
                .location(practiceFactory.createLocation())
                .addTemplates(template)
                .build();
        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
        assertNotNull(get(template));  // template should not be deleted
    }

    /**
     * Verifies that a schedule that is used by acts cannot be deleted.
     */
    @Test
    public void testDeleteScheduleWithActs() {
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());

        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());

        schedulingFactory.newAppointment()
                .startTime(new Date())
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .build();

        assertFalse(handler.getDeletable().canDelete());
    }

    /**
     * Verifies that a schedule used in a schedule view can be deleted.
     */
    @Test
    public void testDeleteScheduleInViews() {
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());
        Entity view = schedulingFactory.createScheduleView(schedule);

        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
        assertNotNull(get(view));
    }

    /**
     * Verifies schedules can be deactivated.
     */
    @Test
    public void testDeactivate() {
        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());
        assertTrue(schedule.isActive());

        ScheduleDeletionHandler handler = createDeletionHandler(schedule);
        handler.deactivate();

        assertFalse(get(schedule).isActive());
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link ScheduleDeletionHandler} for schedules.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Entity schedule = schedulingFactory.createSchedule(practiceFactory.createLocation());
        IMObjectDeletionHandler<Entity> handler = factory.create(schedule);
        assertTrue(handler instanceof ScheduleDeletionHandler);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(schedule));
    }

    /**
     * Creates a new deletion handler for a schedule.
     *
     * @param schedule the schedule
     * @return a new deletion handler
     */
    protected ScheduleDeletionHandler createDeletionHandler(Entity schedule) {
        return new ScheduleDeletionHandler(schedule, factory, ServiceHelper.getTransactionManager(),
                                           ServiceHelper.getArchetypeService());
    }
}