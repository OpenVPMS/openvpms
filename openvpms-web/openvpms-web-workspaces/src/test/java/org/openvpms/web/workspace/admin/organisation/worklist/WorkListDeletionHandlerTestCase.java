/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.worklist;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.admin.job.JobDeletionHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link WorkListDeletionHandler}.
 *
 * @author Tim Anderson
 */
public class WorkListDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Verifies that a work list with no relationships can be deleted.
     */
    @Test
    public void testDeleteWorkListWithNoRelationships() {
        Entity workList = schedulingFactory.createWorkList();

        WorkListDeletionHandler handler = createDeletionHandler(workList);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(workList));
    }

    /**
     * Verifies that a work list with task types can be deleted.
     */
    @Test
    public void testDeleteWorkListWithTaskTypes() {
        Entity taskType = schedulingFactory.createTaskType();
        Entity workList = schedulingFactory.newWorkList()
                .taskTypes(taskType)
                .build();
        WorkListDeletionHandler handler = createDeletionHandler(workList);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(workList));
        assertNotNull(get(taskType));  // task type should not be deleted
    }

    /**
     * Verifies that a work list with templates can be deleted.
     */
    @Test
    public void testDeleteWorkListWithTemplates() {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_LETTER);
        Entity workList = schedulingFactory.newWorkList()
                .addTemplates(template)
                .build();
        WorkListDeletionHandler handler = createDeletionHandler(workList);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(workList));
        assertNotNull(get(template));  // template should not be deleted
    }

    /**
     * Verifies that a work list that is used by acts cannot be deleted.
     */
    @Test
    public void testDeleteWorkListWithActs() {
        Entity workList = schedulingFactory.createWorkList();

        WorkListDeletionHandler handler = createDeletionHandler(workList);
        assertTrue(handler.getDeletable().canDelete());

        schedulingFactory.newTask()
                .startTime(new Date())
                .workList(workList)
                .taskType(schedulingFactory.createTaskType())
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .build();

        assertFalse(handler.getDeletable().canDelete());
    }

    /**
     * Verifies that a work list used in a work list view can be deleted.
     */
    @Test
    public void testDeleteWorkListInViews() {
        Entity workList = schedulingFactory.createWorkList();
        Entity view = schedulingFactory.createWorkListView(workList);

        WorkListDeletionHandler handler = createDeletionHandler(workList);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(workList));
        assertNotNull(get(view));
    }

    /**
     * Verifies work lists can be deactivated.
     */
    @Test
    public void testDeactivate() {
        Entity workList = schedulingFactory.createWorkList();
        assertTrue(workList.isActive());

        WorkListDeletionHandler handler = createDeletionHandler(workList);
        handler.deactivate();

        assertFalse(get(workList).isActive());
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link WorkListDeletionHandler} for work lists.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Entity workList = schedulingFactory.createWorkList();
        IMObjectDeletionHandler<Entity> handler = factory.create(workList);
        assertTrue(handler instanceof WorkListDeletionHandler);
        assertTrue(handler.getDeletable().canDelete());
        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(workList));
    }

    /**
     * Creates a new deletion handler for a work list.
     *
     * @param workList the work list
     * @return a new deletion handler
     */
    protected WorkListDeletionHandler createDeletionHandler(Entity workList) {
        return new WorkListDeletionHandler(workList, factory, ServiceHelper.getTransactionManager(),
                                         ServiceHelper.getArchetypeService());
    }
}