/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.hl7;

import org.junit.Test;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.builder.hl7.TestHL7Factory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.hl7.util.HL7Archetypes;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.admin.role.RoleEditor;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link HL7PharmacyEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class HL7PharmacyEditorTestCase extends AbstractAppTest {

    /**
     * The HL7 factory.
     */
    @Autowired
    private TestHL7Factory hl7Factory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * Verifies that a dispense connector cannot be present when 'oneway' is selected.
     */
    @Test
    public void testOneway() {
        Entity pharmacy = hl7Factory.newHL7Pharmacy()
                .defaultSenderReceiver()
                .location(practiceFactory.createLocation())
                .user(userFactory.createUser())
                .build();

        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        HL7PharmacyEditor editor = new HL7PharmacyEditor(pharmacy, null, context);
        editor.getComponent();
        assertTrue(editor.isValid());
        editor.getProperty("oneway").setValue(true);
        assertInvalid(editor, "Cannot specify a Dispense Connector when One Way is selected.");
    }

    /**
     * Tests the {@link HL7PharmacyEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Entity pharmacy = create(HL7Archetypes.PHARMACY, Entity.class);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        HL7PharmacyEditor editor = new HL7PharmacyEditor(pharmacy, null, context);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof HL7PharmacyEditor);
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link HL7PharmacyEditor} for
     * <em>entity.HL7ServicePharmacy</em> instances.
     */
    @Test
    public void testFactory() {
        SecurityRole role = create(UserArchetypes.ROLE, SecurityRole.class);
        IMObjectEditor editor = factory.create(role, new DefaultLayoutContext(new LocalContext(),
                                                                              new HelpContext("foo", null)));
        assertTrue(editor instanceof RoleEditor);
    }
}