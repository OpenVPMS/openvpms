/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.finance.invoice.ChargeItemEventLinker;
import org.openvpms.archetype.rules.finance.invoice.InvoiceItemStatus;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestInvoiceItemVerifier;
import org.openvpms.archetype.test.builder.hl7.TestHL7Factory;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.cache.MapIMObjectCache;
import org.openvpms.hl7.impl.LaboratoriesImpl;
import org.openvpms.hl7.impl.PharmaciesImpl;
import org.openvpms.hl7.io.Connectors;
import org.openvpms.hl7.laboratory.Laboratories;
import org.openvpms.hl7.patient.PatientContextFactory;
import org.openvpms.hl7.patient.PatientEventServices;
import org.openvpms.hl7.patient.PatientInformationService;
import org.openvpms.hl7.pharmacy.Pharmacies;
import org.openvpms.laboratory.internal.dispatcher.OrderDispatcher;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkOrder;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.createHL7Laboratory;
import static org.openvpms.web.workspace.customer.charge.TestPharmacyOrderService.Order.Type.CANCEL;
import static org.openvpms.web.workspace.customer.charge.TestPharmacyOrderService.Order.Type.CREATE;
import static org.openvpms.web.workspace.customer.charge.TestPharmacyOrderService.Order.Type.UPDATE;

/**
 * Tests the {@link OrderPlacer}.
 *
 * @author Tim Anderson
 */
public class OrderPlacerTestCase extends AbstractAppTest {

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The medical record rules.
     */
    @Autowired
    private MedicalRecordRules medicalRecordRules;

    /**
     * The patient context factory.
     */
    @Autowired
    private PatientContextFactory patientContextFactory;

    /**
     * The HL7 factory.
     */
    @Autowired
    private TestHL7Factory hl7Factory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The practice rules.
     */
    @Autowired
    private PracticeRules practiceRules;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The location.
     */
    private Party location;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The pharmacy.
     */
    private Entity pharmacy;

    /**
     * The laboratory.
     */
    private Entity laboratory;

    /**
     * The pharmacy order service.
     */
    private TestPharmacyOrderService pharmacyOrderService;

    /**
     * The laboratory order service.
     */
    private TestLaboratoryOrderService laboratoryOrderService;

    /**
     * The order placer.
     */
    private OrderPlacer placer;

    /**
     * The patient information service.
     */
    private PatientInformationService informationService;

    /**
     * Sets up the test case.
     */
    @Override
    @Before
    public void setUp() {
        super.setUp();
        Party customer = customerFactory.newCustomer().build(false);
        patient = patientFactory.createPatient();
        location = practiceFactory.createLocation();
        clinician = userFactory.createClinician();
        User user = userFactory.createUser();

        pharmacy = hl7Factory.createHL7Pharmacy(location, userFactory.createUser());
        laboratory = createHL7Laboratory(location);
        pharmacyOrderService = new TestPharmacyOrderService();
        laboratoryOrderService = new TestLaboratoryOrderService();
        Connectors connectors = Mockito.mock(Connectors.class);
        PatientEventServices patientEventServices = Mockito.mock(PatientEventServices.class);
        Pharmacies pharmacies = new PharmaciesImpl(getArchetypeService(), connectors, patientEventServices);
        Laboratories laboratories = new LaboratoriesImpl(getArchetypeService(), connectors, patientEventServices);
        informationService = Mockito.mock(PatientInformationService.class);
        MapIMObjectCache cache = new MapIMObjectCache(getArchetypeService());
        OrderDispatcher dispatcher = Mockito.mock(OrderDispatcher.class);
        Party practice = practiceFactory.getPractice();

        LaboratoryServices laboratoryServices = Mockito.mock(LaboratoryServices.class);
        when(laboratoryServices.getLaboratory(Mockito.any(), eq(location))).thenReturn(laboratory);

        OrderServices services = new OrderServices(pharmacyOrderService, pharmacies, laboratoryOrderService,
                                                   laboratories, laboratoryServices, dispatcher, patientContextFactory,
                                                   informationService, medicalRecordRules, practiceRules);
        placer = new OrderPlacer(customer, location, user, practice, cache, services, getArchetypeService());
    }

    /**
     * Tests the {@link OrderPlacer#order(List, PatientHistoryChanges)} method for a pharmacy order.
     */
    @Test
    public void testPharmacyOrder() {
        Entity productType = createProductType(pharmacy);
        Product product1 = createPharmacyProduct(pharmacy);
        Product product2 = productFactory.createMedication(productType);
        Product product3 = productFactory.createMedication(); // not ordered via a pharmacy
        Act visit = createVisit(patient);
        FinancialAct item1 = createItem(product1, ONE, visit);
        FinancialAct item2 = createItem(product2, TEN, visit);
        FinancialAct item3 = createItem(product3, BigDecimal.valueOf(2), visit);

        IArchetypeService service = getArchetypeService();
        TestInvoiceItemVerifier verifier1 = getVerifier(item1);
        TestInvoiceItemVerifier verifier2 = getVerifier(item2);
        TestInvoiceItemVerifier verifier3 = getVerifier(item3);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, service);

        placer.order(Arrays.asList(item1, item2, item3), changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(2, orders.size());
        checkOrder(orders.get(0), CREATE, patient, product1, ONE, item1.getId(), item1.getActivityStartTime(),
                   clinician, pharmacy);
        checkOrder(orders.get(1), CREATE, patient, product2, TEN, item2.getId(), item2.getActivityStartTime(),
                   clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());

        // verify the status of the items
        verifyOrderStatus(item1, verifier1, InvoiceItemStatus.ORDERED);
        verifyOrderStatus(item2, verifier2, InvoiceItemStatus.ORDERED);
        verifyOrderStatus(item3, verifier3, null);
    }

    /**
     * Tests creation of a laboratory order.
     */
    @Test
    public void testLaboratoryOrder() {
        Entity investigationType = laboratoryFactory.createInvestigationType(laboratory);
        Entity test = laboratoryFactory.createHL7Test(investigationType);
        Product product = createLaboratoryProduct(test);
        Act visit = createVisit(patient);
        FinancialAct item1 = createItem(product, ONE, visit);
        Act investigation = createInvestigation(test, investigationType, visit);
        IMObjectBean bean = getBean(item1);
        bean.addTarget("investigations", investigation, "invoiceItems");
        save(item1, investigation);
        assertEquals(InvestigationActStatus.PENDING, investigation.getStatus2());

        TestInvoiceItemVerifier verifier = getVerifier(item1);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        placer.order(Arrays.asList(item1, investigation), changes);

        List<TestLaboratoryOrderService.LabOrder> orders = laboratoryOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), TestLaboratoryOrderService.LabOrder.Type.CREATE, patient, investigation.getId(),
                   investigation.getActivityStartTime(), clinician, laboratory);
        assertEquals(InvestigationActStatus.SENT, investigation.getStatus2());

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());

        verifyOrderStatus(item1, verifier, InvoiceItemStatus.ORDERED);
    }

    /**
     * Verifies that when a quantity is changed, an update order is placed.
     */
    @Test
    public void testChangeQuantity() {
        Product product = createPharmacyProduct(pharmacy);
        Act visit = createVisit(patient);
        FinancialAct item = createItem(product, ONE, visit);
        item.setStatus(InvoiceItemStatus.ORDERED);  // simulate being ordered previously

        placer.initialise(item);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        item.setQuantity(TEN);

        TestInvoiceItemVerifier verifier = getVerifier(item);

        placer.order(Collections.singletonList(item), changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), UPDATE, patient, product, TEN, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());
        verifyOrderStatus(item, verifier, InvoiceItemStatus.ORDERED);
    }

    /**
     * Verifies that when a patient is changed an exception is thrown as PlacerOrderNumbers shouldn't be
     * reused. The user interface should not permit these to change.
     */
    @Test
    public void testChangePatient() {
        Product product = createPharmacyProduct(pharmacy);
        Act visit1 = createVisit(patient);
        FinancialAct item = createItem(product, ONE, visit1);
        item.setStatus(InvoiceItemStatus.ORDERED);
        Party patient2 = patientFactory.createPatient();

        List<Act> items = Collections.singletonList(item);
        placer.initialise(item);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        IMObjectBean bean = getBean(item);
        bean.setTarget("patient", patient2);

        Act visit2 = createVisit(patient2);
        addChargeItem(visit2, item);
        save(visit2, item);

        try {
            placer.order(items, changes);
            fail("Expected to order() to fail");
        } catch (IllegalStateException expected) {
            assertEquals("Patient cannot be changed on order: previous=" + OrderPlacer.toString(patient)
                         + ", current=" + OrderPlacer.toString(patient2), expected.getMessage());
        }
    }

    /**
     * Verifies that when a product is changed, an update order is placed.
     */
    @Test
    public void testChangeProduct() {
        Product product1 = createPharmacyProduct(pharmacy);
        Product product2 = createPharmacyProduct(pharmacy);
        Act visit = createVisit(patient);
        FinancialAct item = createItem(product1, ONE, visit);
        item.setStatus(InvoiceItemStatus.ORDERED);

        List<Act> items = Collections.singletonList(item);
        placer.initialise(item);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        IMObjectBean bean = getBean(item);
        bean.setTarget("product", product2);

        try {
            placer.order(items, changes);
            fail("Expected order() to fail");
        } catch (IllegalStateException expected) {
            assertEquals("Product cannot be changed on order: previous=" + OrderPlacer.toString(product1)
                         + ", current=" + OrderPlacer.toString(product2), expected.getMessage());
        }
    }

    /**
     * Verifies that when a product is changed to a non-pharmacy product, the order is cancelled.
     * <p/>
     * NOTE: this functionality is no longer supported when editing, as the patient and product are set read-only
     * when an order is issued.
     */
    @Test
    public void testChangeProductToNonPharmacyProduct() {
        Product product1 = createPharmacyProduct(pharmacy);
        Product product2 = productFactory.createMedication();
        Act visit = createVisit(patient);
        FinancialAct item = createItem(product1, ONE, visit);
        item.setStatus(InvoiceItemStatus.ORDERED);

        List<Act> items = Collections.singletonList(item);
        placer.initialise(item);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        IMObjectBean bean = getBean(item);
        bean.setTarget("product", product2);

        TestInvoiceItemVerifier verifier = getVerifier(item);

        placer.order(items, changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), CANCEL, patient, product1, ONE, item.getId(),
                   item.getActivityStartTime(), clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());

        verifyOrderStatus(item, verifier, InvoiceItemStatus.ORDERED);
    }

    /**
     * Verifies that when a clinician is changed, an update order is placed.
     */
    @Test
    public void testChangeClinician() {
        Product product = createPharmacyProduct(pharmacy);
        User clinician2 = userFactory.createClinician();
        Act event = createVisit(patient);
        FinancialAct item = createItem(product, ONE, event);

        List<Act> items = Collections.singletonList(item);
        placer.initialise(item);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        IMObjectBean bean = getBean(item);
        bean.setTarget("clinician", clinician2);
        bean.save();

        placer.order(items, changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), UPDATE, patient, product, ONE, item.getId(),
                   item.getActivityStartTime(), clinician2, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());
    }

    /**
     * Verifies that if an item is removed, a cancellation is generated.
     */
    @Test
    public void testOrderWithCancel() {
        Product product1 = createPharmacyProduct(pharmacy);
        Product product2 = createPharmacyProduct(pharmacy);
        Act event = createVisit(patient);
        Act item1 = createItem(product1, ONE, event);
        Act item2 = createItem(product2, TEN, event);

        placer.initialise(item1);

        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        placer.order(Collections.singletonList(item2), changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(2, orders.size());
        checkOrder(orders.get(0), CREATE, patient, product2, TEN, item2.getId(),
                   item2.getActivityStartTime(), clinician, pharmacy);
        checkOrder(orders.get(1), CANCEL, patient, product1, ONE, item1.getId(),
                   item1.getActivityStartTime(), clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());
    }

    /**
     * Tests the {@link OrderPlacer#cancel} method.
     */
    @Test
    public void testCancel() {
        Product product1 = createPharmacyProduct(pharmacy);
        Product product2 = createPharmacyProduct(pharmacy);
        Product product3 = productFactory.createMedication(); // not ordered via a pharmacy
        Act event = createVisit(patient);
        Act item1 = createItem(product1, ONE, event);
        Act item2 = createItem(product2, TEN, event);
        Act item3 = createItem(product3, BigDecimal.valueOf(2), event);

        List<Act> items = Arrays.asList(item1, item2, item3);
        for (Act item : items) {
            placer.initialise(item);
        }
        placer.cancel();

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders(true);
        assertEquals(2, orders.size());
        checkOrder(orders.get(0), CANCEL, patient, product1, ONE, item1.getId(), item1.getActivityStartTime(),
                   clinician, pharmacy);
        checkOrder(orders.get(1), CANCEL, patient, product2, TEN, item2.getId(), item2.getActivityStartTime(),
                   clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(0)).updated(Mockito.any());
    }

    /**
     * Verifies that {@link PatientInformationService#updated} is invoked if a visit is created
     * during charging.
     */
    @Test
    public void testPatientInformationNotificationForNewEvent() {
        Product product = createPharmacyProduct(pharmacy);
        FinancialAct item = createItem(product, ONE);
        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        linker.prepare(Collections.singletonList(item), changes);
        changes.save();
        placer.order(Collections.singletonList(item), changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), CREATE, patient, product, ONE, item.getId(), item.getActivityStartTime(),
                   clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(1)).updated(Mockito.any());
    }

    /**
     * Verifies that {@link PatientInformationService#updated} is invoked if a completed visit is
     * linked to during charging.
     */
    @Test
    public void testPatientInformationNotificationForCompletedEvent() {
        Product product = createPharmacyProduct(pharmacy);
        Act event = createVisit(patient);
        event.setActivityEndTime(new Date());
        save(event);

        FinancialAct item = createItem(product, ONE);
        PatientHistoryChanges changes = new PatientHistoryChanges(location, getArchetypeService());
        ChargeItemEventLinker linker = new ChargeItemEventLinker(getArchetypeService());
        linker.prepare(Collections.singletonList(item), changes);
        changes.save();
        placer.order(Collections.singletonList(item), changes);

        List<TestPharmacyOrderService.Order> orders = pharmacyOrderService.getOrders();
        assertEquals(1, orders.size());
        checkOrder(orders.get(0), CREATE, patient, product, ONE, item.getId(), item.getActivityStartTime(),
                   clinician, pharmacy);

        // patient information updates should not be sent
        verify(informationService, times(1)).updated(Mockito.any());
    }

    /**
     * Creates a new visit.
     *
     * @param patient the patient
     * @return a new visit
     */
    private Act createVisit(Party patient) {
        return patientFactory.newVisit().
                patient(patient)
                .clinician(clinician)
                .build();
    }

    /**
     * Verifies invoice item order details matches that expected.
     *
     * @param item     the item
     * @param verifier the verifier to use
     * @param status   the expected status. May be {@code null}
     */
    private void verifyOrderStatus(FinancialAct item, TestInvoiceItemVerifier verifier, String status) {
        verifier.status(status)
                .receivedQuantity(null)
                .returnedQuantity(null)
                .verify(item);
    }

    /**
     * Creates an invoice item verifier from an existing invoice item.
     * This ignores medication act verification, as the item is only partially populated.
     *
     * @param item the invoice item
     * @return a new verifier
     */
    private TestInvoiceItemVerifier getVerifier(Act item) {
        return new TestInvoiceItemVerifier(getArchetypeService()).initialise(item).ignoreMedication(true);
    }

    /**
     * Creates an invoice item linked to an event.
     *
     * @param product  the product
     * @param quantity the quantity
     * @param event    the event
     * @return a new invoice item
     */
    private FinancialAct createItem(Product product, BigDecimal quantity, Act event) {
        FinancialAct item = createItem(product, quantity);
        addChargeItem(event, item);
        return item;
    }

    /**
     * Creates an investigation linked to an event.
     *
     * @param test              the test
     * @param investigationType the investigation type
     * @param visit             the visit
     * @return a new investigation
     */
    private Act createInvestigation(Entity test, Entity investigationType, Act visit) {
        Act investigation = patientFactory.newInvestigation()
                .patient(patient)
                .clinician(clinician)
                .investigationType(investigationType)
                .addTests(test)
                .build();
        patientFactory.updateVisit(visit)
                .addItem(investigation)
                .build();
        return investigation;
    }

    /**
     * Adds a charge item to an event.
     *
     * @param event the event
     * @param item  the charge item
     */
    private void addChargeItem(Act event, FinancialAct item) {
        IMObjectBean itemBean = getBean(item);
        for (IMObject object : itemBean.getValues("event", IMObject.class)) {
            itemBean.removeValue("event", object);
        }
        IMObjectBean bean = getBean(event);
        bean.addTarget("chargeItems", item, "event");
        save(event, item);
    }

    /**
     * Creates an invoice item.
     *
     * @param product  the product
     * @param quantity the quantity
     * @return a new invoice item
     */
    private FinancialAct createItem(Product product, BigDecimal quantity) {
        return accountFactory.newInvoiceItem()
                .patient(patient).product(product).quantity(quantity).clinician(clinician)
                .quantity(quantity)
                .unitPrice(1)
                .build();
    }

    /**
     * Creates a product dispensed via a pharmacy.
     *
     * @param pharmacy the pharmacy
     * @return a new product
     */
    private Product createPharmacyProduct(Entity pharmacy) {
        return productFactory.newMedication()
                .pharmacy(pharmacy)
                .build();
    }

    /**
     * Creates a product ordered via a laboratory.
     *
     * @param test the test
     * @return a new product
     */
    private Product createLaboratoryProduct(Entity test) {
        return productFactory.newService()
                .addTests(test)
                .build();
    }

    /**
     * Creates a product type linked to a pharmacy.
     *
     * @param pharmacy the pharmacy
     * @return a new product type
     */
    private Entity createProductType(Entity pharmacy) {
        return productFactory.newProductType()
                .pharmacy(pharmacy)
                .build();
    }

}
