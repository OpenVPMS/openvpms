/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.roster;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.roster.RosterSyncStatus;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link RosterEventEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class RosterEventEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Tests updating the start and end times.
     */
    @Test
    public void testStartEndTime() {
        Act event = schedulingFactory.newRosterEvent().build(false);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        RosterEventEditor editor = new RosterEventEditor(event, null, context);

        Date date = DateRules.getToday();

        // verify the default start end times are 9:00 and 17:00 respectively
        assertEquals(getTime(date, 9), editor.getStartTime());
        assertEquals(getTime(date, 17), editor.getEndTime());

        editor.setStartTime(getTime(date, 8));
        assertEquals(getTime(date, 17), editor.getEndTime());

        // now set the end time
        editor.setEndTime(getTime(date, 12));
        assertEquals(getTime(date, 8), editor.getStartTime());

        // verify when the start time is set the same as the end time, the end time moves by the previous duration
        editor.setStartTime(getTime(date, 12));
        assertEquals(getTime(date, 16), editor.getEndTime());
    }

    /**
     * Verifies that the appropriate error message is returned for start and end time validation errors.
     */
    @Test
    public void testInvalidStartEndTime() {
        Party location = practiceFactory.createLocation();
        Entity rosterArea = schedulingFactory.createRosterArea(location);
        Act event1 = schedulingFactory.newRosterEvent()
                .startTime(DateRules.getToday())
                .endTime(DateRules.getToday())
                .location(location)
                .schedule(rosterArea)
                .build(false);
        checkInvalid(event1, "Start Time cannot be the same as End Time");

        Act event2 = schedulingFactory.newRosterEvent()
                .startTime(DateRules.getTomorrow())
                .endTime(DateRules.getToday())
                .location(location)
                .schedule(rosterArea)
                .build(false);
        checkInvalid(event2, "Start Time is greater than End Time");
    }

    /**
     * Tests the {@link RosterEventEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Act event = schedulingFactory.newRosterEvent().build(false);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        RosterEventEditor editor = new RosterEventEditor(event, null, context);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof RosterEventEditor);
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link RosterEventEditor} for <em>act.rosterEvent</em>
     * instances.
     */
    @Test
    public void testFactory() {
        Act event = create(ScheduleArchetypes.ROSTER_EVENT, Act.class);
        IMObjectEditor editor = factory.create(event, new DefaultLayoutContext(new LocalContext(),
                                                                               new HelpContext("foo", null)));
        assertTrue(editor instanceof RosterEventEditor);
    }

    /**
     * Verifies that if a roster event has been synchronised, its sync statuses are set to PENDING on save.
     * <p/>
     * This enables roster synchronisation services to track and propagate changes.
     */
    @Test
    public void testSyncStatus() {
        Party location = practiceFactory.createLocation();
        User user = userFactory.createUser();
        ActIdentity id1 = schedulingFactory.createSyncId();
        ActIdentity id2 = schedulingFactory.createSyncId();

        Act event = schedulingFactory.newRosterEvent()
                .location(location)
                .schedule(schedulingFactory.createRosterArea(location))
                .addIdentities(id1, id2)
                .build(false);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        RosterEventEditor editor = new RosterEventEditor(event, null, context);

        assertTrue(SaveHelper.save(editor));

        checkStatus(id1, RosterSyncStatus.PENDING);
        checkStatus(id2, RosterSyncStatus.PENDING);

        setStatus(id1, RosterSyncStatus.SYNC);
        setStatus(id2, RosterSyncStatus.ERROR);

        assertTrue(SaveHelper.save(editor));

        checkStatus(id1, RosterSyncStatus.SYNC);
        checkStatus(id2, RosterSyncStatus.ERROR);

        editor.setUser(user);

        checkStatus(id1, RosterSyncStatus.PENDING);
        checkStatus(id2, RosterSyncStatus.PENDING);
    }

    /**
     * Verifies an event is invalid.
     *
     * @param event    the event
     * @param expected the expected error
     */
    private void checkInvalid(Act event, String expected) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        RosterEventEditor editor = new RosterEventEditor(event, null, context);
        assertInvalid(editor, expected);
    }

    /**
     * Returns a date/time for the specified date and hour.
     *
     * @param date the date
     * @param hour the hour
     * @return the date/time
     */
    private Date getTime(Date date, int hour) {
        return DateRules.getDate(date, hour, DateUnits.HOURS);
    }

    /**
     * Verifies a sync status matches that expected.
     *
     * @param identity the identity
     * @param status   the expected status
     */
    private void checkStatus(ActIdentity identity, String status) {
        IMObjectBean bean = getBean(identity);
        assertEquals(status, bean.getString("status"));
    }

    /**
     * Sets a sync status.
     *
     * @param identity the identity
     * @param status   the new status
     */
    private void setStatus(ActIdentity identity, String status) {
        schedulingFactory.updateSyncId(identity)
                .status(status)
                .build();
    }
}
