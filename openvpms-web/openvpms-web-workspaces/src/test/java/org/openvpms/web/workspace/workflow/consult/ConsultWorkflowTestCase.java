/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.consult;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActItemEditor;
import org.openvpms.web.workspace.patient.visit.VisitEditorDialog;
import org.openvpms.web.workspace.workflow.AbstractWorkflowTest;
import org.openvpms.web.workspace.workflow.checkin.CheckInWorkflowRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.cancelDialog;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;


/**
 * Tests the {@link ConsultWorkflow}.
 *
 * @author Tim Anderson
 */
public class ConsultWorkflowTestCase extends AbstractWorkflowTest {

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * The clinician.
     */
    private User clinician;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The context.
     */
    private Context context;

    /**
     * The work list.
     */
    private Entity workList;

    /**
     * The task type.
     */
    private Entity taskType;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient(customer);
        clinician = userFactory.createClinician();
        context = new LocalContext();
        location = practiceFactory.createLocation();
        context.setLocation(location);
        context.setUser(userFactory.createUser());

        taskType = schedulingFactory.createTaskType();
        workList = schedulingFactory.newWorkList().addTaskType(taskType, 1, true).build();

        // make the work list available at the location by adding it to a view.
        Entity workListView = schedulingFactory.createWorkListView(workList);
        practiceFactory.updateLocation(location)
                .workListViews(workListView)
                .build();
    }

    /**
     * Tests running a consult from an appointment.
     */
    @Test
    public void testConsultForAppointment() {
        Date date = new Date();
        Act appointment = createAppointment(date);

        // create a COMPLETED event for the previous date, with no end date. This should not be used by check-in
        // or consult
        Act previousEvent = patientFactory.newVisit()
                .startTime(DateRules.getYesterday())
                .patient(patient)
                .clinician(clinician)
                .status(ActStatus.COMPLETED)
                .build();

        CheckInWorkflowRunner runner = new CheckInWorkflowRunner(appointment, getPractice(), context);

        Act event = runner.runWorkflow(patient, customer, workList, date, clinician, location);
        assertNotEquals(previousEvent, event); // new event should have been created

        checkConsultWorkflow(get(appointment), event, clinician);
    }

    /**
     * Tests running a consult from a task.
     */
    @Test
    public void testConsultForTask() {
        Date date = new Date();

        // create a COMPLETED event for the previous date, with no end date. This should not be used by check-in
        // or consult
        Act previousEvent = patientFactory.newVisit()
                .startTime(DateRules.getYesterday())
                .patient(patient)
                .clinician(clinician)
                .status(ActStatus.COMPLETED)
                .build();

        Act appointment = createAppointment(date);
        CheckInWorkflowRunner runner = new CheckInWorkflowRunner(appointment, getPractice(), context);

        Act event = runner.runWorkflow(patient, customer, workList, date, clinician, location);
        assertNotEquals(previousEvent, event); // new event should have been created

        Act task = runner.checkTask(workList, customer, patient, clinician, TaskStatus.PENDING, "Reason X");

        checkConsultWorkflow(task, event, clinician);
    }

    /**
     * Verifies that closing the invoice edit dialog by the 'x' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByUserCloseNoSave() {
        checkCancelInvoice(false, true);
    }

    /**
     * Verifies that closing the invoice edit dialog by the 'user close' button cancels the workflow.
     * and that unsaved amounts don't affect the invoice.
     */
    @Test
    public void testCancelInvoiceByUserCloseAfterSave() {
        checkCancelInvoice(true, true);
    }

    /**
     * Verifies that cancelling the invoice edit dialog by the 'Cancel' button cancels the workflow.
     */
    @Test
    public void testCancelInvoiceByCancelButtonNoSave() {
        checkCancelInvoice(false, false);
    }

    /**
     * Verifies that when the invoice is set to <em>COMPLETE</em>, the appointment status is changed to <em>BILLED</em>.
     */
    @Test
    public void testCompleteInvoiceStatusForAppointment() {
        Act appointment = createAppointment();
        checkCompleteInvoiceStatus(appointment);
    }

    /**
     * Verifies that when the invoice is set to <em>COMPLETE</em>, the appointment status is changed to <em>BILLED</em>.
     */
    @Test
    public void testCompleteInvoiceStatusForTask() {
        Act task = createTask();
        checkCompleteInvoiceStatus(task);
    }

    /**
     * Verifies that when the invoice is set to <em>IN_PROGRESS</em> from COMPLETE, the appointment status is remains
     * <em>BILLED</em>.
     */
    @Test
    public void testInProgressInvoiceStatusForBilledAppointment() {
        Act appointment = createAppointment();
        checkChangeCompleteInvoiceToInProgress(appointment, WorkflowStatus.BILLED);
    }

    /**
     * Verifies that when the invoice is set to {@code IN_PROGRESS} from {@code COMPLETE}, the appointment status
     * remains {@code COMPLETE}.
     */
    @Test
    public void testInProgressInvoiceStatusForCompletedAppointment() {
        Act appointment = createAppointment();
        checkChangeCompleteInvoiceToInProgress(appointment, WorkflowStatus.COMPLETED);
    }

    /**
     * Verifies that when the invoice is set to <em>IN_PROGRESS</em> from COMPLETE, the appointment status is remains
     * <em>BILLED</em>.
     */
    @Test
    public void testInProgressInvoiceStatusForBilledTask() {
        Act task = createTask();
        checkChangeCompleteInvoiceToInProgress(task, WorkflowStatus.BILLED);
    }

    /**
     * Verifies that when the invoice is set to {@code IN_PROGRESS} from {@code COMPLETE}, the appointment status
     * remains {@code COMPLETE}.
     */
    @Test
    public void testInProgressInvoiceStatusForCompletedTask() {
        Act task = createTask();
        checkChangeCompleteInvoiceToInProgress(task, WorkflowStatus.COMPLETED);
    }

    /**
     * Verifies that cancelling the invoice edit dialog by the 'Cancel' button cancels the workflow,
     * and that unsaved amounts don't affect the invoice.
     */
    @Test
    public void testCancelInvoiceByCancelButtonAfterSave() {
        checkCancelInvoice(true, false);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set true, the clinician comes from the context
     * user rather than the appointment.
     */
    @Test
    public void testUseLoggedInClinicianForClinician() {
        checkUseLoggedInClinician(true, clinician, userFactory.createClinician(), clinician);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set true, and the logged-in user is not a
     * clinician, the clinician comes from the appointment.
     */
    @Test
    public void testUseLoggedInClinicianForNonClinician() {
        User user = userFactory.createUser();
        checkUseLoggedInClinician(true, user, clinician, clinician);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set false, the clinician comes from the context
     * user rather than the appointment.
     */
    @Test
    public void testUseLoggedInClinicianDisabledForClinician() {
        User clinician2 = userFactory.createClinician();
        checkUseLoggedInClinician(false, clinician, clinician2, clinician2);
    }

    /**
     * Verifies that when the practice 'usedLoggedInClinician' is set false, and the logged-in user is not a
     * clinician, the clinician comes from the appointment.
     */
    @Test
    public void testUseLoggedInClinicianDisabledForNonClinician() {
        User user = userFactory.createUser();
        checkUseLoggedInClinician(false, user, clinician, clinician);
    }

    /**
     * Verifies that a consult can be done from an appointment where the check-in occurred on a later date.
     * <p/>
     * In this case, the appointment is dated yesterday, but the check-in is done today, creating a visit with today's
     * date. The consult should select the current visit.
     */
    @Test
    public void testConsultFromBackDatedAppointment() {
        Act appointment = createAppointment(DateRules.getYesterday());

        // run check-in. This should create a visit with today's date
        CheckInWorkflowRunner runner = new CheckInWorkflowRunner(appointment, getPractice(), context);
        Date arrivalTime = new Date();
        Act event = runner.runWorkflow(patient, customer, workList, arrivalTime, clinician, location);
        assertTrue(DateRules.compareTo(event.getActivityStartTime(), arrivalTime, true) >= 0);
        checkConsultWorkflow(get(appointment), event, clinician);
    }

    /**
     * Verifies that when consulting using the task, and it is changed by another user, the workflow completes
     * successfully and the task status changes to <em>IN_PROGRESS</em>.
     * <p/>
     * TODO - add test where:
     * <ul>
     *     <li>the appointment updates before its status can be changed from CHECKED_IN</li>
     *     <li>the appointment updates when the event is created by GetClinicalEventTask</li>
     * </ul>
     */
    @Test
    public void testTaskChangedByAnotherUser() {
        Act task = createTask();
        ConsultWorkflowRunner workflow = new ConsultWorkflowRunner(task, getPractice(), context);

        // simulate changing the task by another user
        task = get(task);
        task.setDescription("set to force update");
        save(task);

        workflow.start();
        workflow.checkClinician(clinician);

        VisitEditorDialog dialog = workflow.editVisit();
        fireDialogButton(dialog, PopupDialog.OK_ID);

        workflow.checkComplete(WorkflowStatus.IN_PROGRESS);
        workflow.checkContext(context, customer, patient, clinician);
    }

    /**
     * Helper to create an appointment with <em>CHECKED_IN</em> status.
     *
     * @param customer  the customer
     * @param patient   the patient. May be {@code null}
     * @param clinician the clinician. May be {@code null}
     * @param location  the practice location
     * @return a new appointment
     */
    protected Act createAppointment(Party customer, Party patient, User clinician, Party location) {
        return createAppointment(new Date(), customer, patient, clinician, location);
    }

    /**
     * Helper to create an appointment with <em>CHECKED_IN</em> status.
     *
     * @param customer  the customer
     * @param patient   the patient. May be {@code null}
     * @param clinician the clinician. May be {@code null}
     * @param location  the practice location
     * @return a new appointment
     */
    protected Act createAppointment(Date startTime, Party customer, Party patient, User clinician, Party location) {
        return newAppointment(customer, patient, clinician, location)
                .startTime(startTime)
                .status(AppointmentStatus.CHECKED_IN)
                .build();
    }

    /**
     * Tests the effects of the practice useLoggedInClinician option during the Consult workflow.
     *
     * @param enabled              if {@code true}, enable the option, otherwise disable it
     * @param user                 the current user
     * @param appointmentClinician the appointment clinician
     * @param expectedClinician    the expected clinician on new acts
     */
    private void checkUseLoggedInClinician(boolean enabled, User user, User appointmentClinician,
                                           User expectedClinician) {
        practiceFactory.updatePractice(getPractice())
                .useLoggedInClinician(enabled)
                .build();

        context.setUser(user);
        context.setClinician(clinician);

        Date date = new Date();
        Act appointment = createAppointment(date, customer, patient, appointmentClinician, location);

        // create a COMPLETED event for the previous date, with no end date. This should not be used by check-in
        // or consult
        Act previousEvent = patientFactory.newVisit()
                .startTime(DateRules.getYesterday())
                .patient(patient).clinician(appointmentClinician)
                .status(ActStatus.COMPLETED)
                .build();

        CheckInWorkflowRunner runner = new CheckInWorkflowRunner(appointment, getPractice(), context);

        Act event = runner.runWorkflow(patient, customer, workList, date, expectedClinician, location);
        assertNotEquals(previousEvent, event); // new event should have been created

        checkConsultWorkflow(get(appointment), event, expectedClinician);
    }

    /**
     * Tests the consult workflow.
     *
     * @param act       the appointment/task
     * @param event     the event created at check-in
     * @param clinician the expected clinician
     */
    private void checkConsultWorkflow(Act act, Act event, User clinician) {
        ConsultWorkflowRunner workflow = new ConsultWorkflowRunner(act, getPractice(), context);
        workflow.start();
        if (!UserHelper.useLoggedInClinician(context)) {
            // prompts for clinician
            workflow.checkClinician(clinician);
        }

        VisitEditorDialog dialog = workflow.editVisit();
        assertEquals(event, dialog.getEditor().getEvent());
        workflow.addNote();
        CustomerChargeActItemEditor itemEditor = workflow.addVisitInvoiceItem(patient, new BigDecimal(20));
        assertEquals(clinician, itemEditor.getClinician());
        fireDialogButton(dialog, PopupDialog.OK_ID);

        workflow.checkComplete(ActStatus.IN_PROGRESS);
        workflow.checkContext(context, customer, patient, clinician);
    }

    /**
     * Verifies that cancelling the invoice cancels the workflow.
     *
     * @param save      if {@code true} save the invoice. and add an unsaved item before cancelling
     * @param userClose if {@code true} cancel by clicking the 'x' button, otherwise cancel via the 'Cancel' button
     */
    private void checkCancelInvoice(boolean save, boolean userClose) {
        Act appointment = createAppointment();
        ConsultWorkflowRunner workflow = new ConsultWorkflowRunner(appointment, getPractice(), context);
        workflow.start();

        workflow.checkClinician(clinician);

        // edit the clinical event
        VisitEditorDialog dialog = workflow.editVisit();
        BigDecimal fixedPrice = new BigDecimal("18.18");
        workflow.addVisitInvoiceItem(patient, fixedPrice);

        // next is to edit the invoice
        if (save) {
            dialog.getEditor().selectCharges();
            fireDialogButton(dialog, PopupDialog.APPLY_ID);          // save the invoice
        }
        workflow.addVisitInvoiceItem(patient, fixedPrice);    // add another item. Won't be saved

        // close the dialog
        cancelDialog(dialog, userClose);

        if (save) {
            BigDecimal fixedPriceIncTax = BigDecimal.valueOf(20);
            workflow.checkInvoice(ActStatus.IN_PROGRESS, fixedPriceIncTax, clinician);
        } else {
            FinancialAct invoice = workflow.getInvoice();
            assertNotNull(invoice);
            assertTrue(invoice.isNew()); // unsaved
        }

        workflow.checkComplete(ActStatus.IN_PROGRESS);
        workflow.checkContext(context, null, null, null);
    }

    /**
     * Verifies that when the invoice is set to <em>COMPLETE</em>, the appointment/task status is changed to
     * <em>BILLED</em>.
     *
     * @param act the appointment/task
     */
    private void checkCompleteInvoiceStatus(Act act) {
        ConsultWorkflowRunner workflow = new ConsultWorkflowRunner(act, getPractice(), context);
        workflow.start();

        workflow.checkClinician(clinician);

        VisitEditorDialog dialog = workflow.editVisit();
        workflow.addVisitInvoiceItem(patient, new BigDecimal(20));
        dialog.getEditor().getChargeEditor().setStatus(ActStatus.COMPLETED);
        fireDialogButton(dialog, PopupDialog.OK_ID);

        workflow.checkComplete(WorkflowStatus.BILLED);
        workflow.checkContext(context, customer, patient, clinician);
    }

    /**
     * Transitions a {@code COMPLETED} invoice to {@code IN_PROGRESS} and verifies that an appointment/task that
     * is {@code COMPLETED} or {@code BILLED} does not change status.
     *
     * @param act            the appointment or task
     * @param expectedStatus the expected appointment/task status
     */
    private void checkChangeCompleteInvoiceToInProgress(Act act, String expectedStatus) {
        // runs the workflow to create a COMPLETE invoice, and sets the appointment/task BILLED
        checkCompleteInvoiceStatus(act);
        if (!act.getStatus().equals(expectedStatus)) {
            act.setStatus(expectedStatus);
            save(act);
        }

        // run the workflow again, but this time mark the invoice IN_PROGRESS
        ConsultWorkflowRunner workflow = new ConsultWorkflowRunner(act, getPractice(), context);
        workflow.start();

        workflow.checkClinician(clinician);

        VisitEditorDialog dialog = workflow.editVisit();
        workflow.addVisitInvoiceItem(patient, new BigDecimal(20));
        dialog.getEditor().getChargeEditor().setStatus(ActStatus.IN_PROGRESS);
        fireDialogButton(dialog, PopupDialog.OK_ID);

        // verify the invoice is now IN_PROGRESS
        Act invoice = get(workflow.getInvoice());
        assertNotNull(invoice);
        assertEquals(ActStatus.IN_PROGRESS, invoice.getStatus());

        // verify the appointment/task is that expected
        workflow.checkComplete(expectedStatus);
        workflow.checkContext(context, customer, patient, clinician);
    }

    /**
     * Creates a new appointment with <em>CHECKED_IN</em> status.
     *
     * @return a new appointment
     */
    private Act createAppointment() {
        return createAppointment(customer, patient, clinician, location);
    }

    /**
     * Creates a new appointment with <em>CHECKED_IN</em> status.
     *
     * @param startTime the appointment start time
     * @return a new appointment
     */
    private Act createAppointment(Date startTime) {
        return createAppointment(startTime, customer, patient, clinician, location);
    }

    /**
     * Creates a new task.
     *
     * @return a new task
     */
    private Act createTask() {
        return schedulingFactory.newTask()
                .startTime(new Date())
                .taskType(taskType)
                .customer(customer)
                .patient(patient)
                .clinician(clinician)
                .workList(workList)
                .build();
    }

}
