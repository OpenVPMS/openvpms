/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.TemplatedVersionedDocumentActEditorTest;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.NEEDS_UPDATE;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.PROMPT;
import static org.openvpms.web.component.im.doc.DocumentActEditor.DocumentStatus.UP_TO_DATE;


/**
 * Tests the {@link PatientLetterEditor} class.
 *
 * @author Tim Anderson
 */
public class PatientLetterEditorTestCase extends TemplatedVersionedDocumentActEditorTest<PatientLetterEditor> {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Constructs a {@link PatientLetterEditorTestCase}.
     */
    public PatientLetterEditorTestCase() {
        super(PatientLetterEditor.class, PatientArchetypes.DOCUMENT_LETTER);
    }

    /**
     * Verifies that a document is generated when a template is associated with an <em>act.patientDocumentLetter</em>,
     * and that this is can be regenerated if a user changes an attribute that doesn't force regeneration.
     */
    @Test
    public void testDocRegeneration() {
        DocumentAct act = createAct();
        Entity template = createDocumentTemplate(act.getArchetype());
        PatientLetterEditor editor = createEditor(act);
        editor.getComponent();

        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        editor.setTemplate(template);
        assertEquals(NEEDS_UPDATE, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> act.getDocument() != null);
        Reference docRef1 = act.getDocument();
        assertNotNull(docRef1);
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());

        assertTrue(save(editor));

        // verify the document was generated and saved
        Document doc1 = (Document) get(docRef1);
        assertNotNull(doc1);
        assertEquals(DocFormats.PDF_TYPE, doc1.getMimeType());
        assertEquals("blank.pdf", doc1.getName());

        editor.setClinician(userFactory.createClinician());
        assertEquals(PROMPT, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> !Objects.equals(docRef1, act.getDocument()));
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        assertTrue(save(editor));

        Reference docRef2 = act.getDocument();   // verify a new document has been generated
        assertNotNull(docRef2);
        assertNotEquals(docRef1, docRef2);
        Document doc2 = (Document) get(docRef2);
        assertNotNull(doc2);

        // verify doc1 it was versioned
        List<DocumentAct> versions1 = getVersions(act);
        assertEquals(1, versions1.size());
        checkVersion(versions1, 0, docRef1);

        editor.setProduct(productFactory.createService());
        assertEquals(PROMPT, editor.getDocumentStatus());
        editor.generateDocument(success -> {
            assertNotNull(success);
            assertTrue(success);
        });
        processQueuedTasks(10, () -> !Objects.equals(docRef2, act.getDocument()));
        assertEquals(UP_TO_DATE, editor.getDocumentStatus());
        assertTrue(save(editor));

        Reference docRef3 = act.getDocument();   // verify a new document has been generated
        assertNotNull(docRef3);
        assertNotEquals(docRef2, docRef3);
        Document doc3 = (Document) get(docRef3);
        assertNotNull(doc3);

        // verify doc2 it was versioned
        List<DocumentAct> versions2 = getVersions(act);
        assertEquals(2, versions2.size());
        checkVersion(versions2, 0, docRef1);
        checkVersion(versions2, 1, docRef2);
    }

    /**
     * Creates a new editor.
     *
     * @param act the act to edit
     * @return a new editor
     */
    protected PatientLetterEditor createEditor(DocumentAct act) {
        DefaultLayoutContext layout = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        Context context = layout.getContext();
        context.setPatient(patientFactory.createPatient());
        context.setUser(userFactory.createUser());
        return new PatientLetterEditor(act, null, layout);
    }

    /**
     * Creates a new document.
     *
     * @return a new document
     */
    protected Document createDocument() {
        return createImage();
    }
}