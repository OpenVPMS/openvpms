/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.type;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.patient.reminder.TestReminderFactory;
import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link ReminderTypeEditor}.
 *
 * @author Tim Anderson
 */
public class ReminderTypeEditorTestCase extends AbstractIMObjectEditorTest<ReminderTypeEditor> {

    /**
     * The reminder rules.
     */
    @Autowired
    private ReminderRules reminderRules;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The reminder factory.
     */
    @Autowired
    private TestReminderFactory reminderFactory;

    /**
     * Constructs a {@link ReminderTypeEditorTestCase}.
     */
    public ReminderTypeEditorTestCase() {
        super(ReminderTypeEditor.class, ReminderArchetypes.REMINDER_TYPE);
    }

    /**
     * Verifies a reminder type can be created using
     * {@link ReminderTypeEditor#ReminderTypeEditor(IMObjectGraph, LayoutContext)}.
     */
    @Test
    public void testCreateFromGraph() {
        Entity template = documentFactory.createTemplate(PatientArchetypes.DOCUMENT_FORM);
        Entity reminderType = reminderFactory.newReminderType()
                .defaultInterval(1, DateUnits.YEARS)
                .newCount().count(0).interval(-2, DateUnits.WEEKS).template(template).newRule().print().add()
                .add()
                .newCount().count(1).interval(-1, DateUnits.WEEKS).template(template).newRule().print().add()
                .add()
                .build();

        IMObjectGraph graph = reminderRules.copyReminderType(reminderType);
        ReminderTypeEditor editor = new ReminderTypeEditor(graph, createLayoutContext());
        editor.getComponent();
        assertValid(editor);
        assertTrue(SaveHelper.save(editor));

        Entity copy = (Entity) get(editor.getObject());
        assertNotNull(copy);
        IMObjectBean bean = getBean(copy);
        assertEquals(2, bean.getTargets("counts").size());
    }
}