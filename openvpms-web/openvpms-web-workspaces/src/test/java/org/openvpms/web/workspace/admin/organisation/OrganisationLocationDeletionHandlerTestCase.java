/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandlerFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.admin.job.JobDeletionHandler;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link OrganisationLocationDeletionHandler}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class OrganisationLocationDeletionHandlerTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * Verifies that a location with a logo can be deleted, and the logo is also removed.
     */
    @Test
    public void testDelete() {
        Party location = TestHelper.createLocation();
        Document document = (Document) documentFactory.createJRXML();
        DocumentAct act = create(DocumentArchetypes.LOGO_ACT, DocumentAct.class);
        act.setDocument(document.getObjectReference());
        IMObjectBean bean = getBean(act);
        bean.setTarget("owner", location);
        save(location, act, document);

        OrganisationLocationDeletionHandler handler = createDeletionHandler(location);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(location));
        assertNull(get(document));
        assertNull(get(act));
    }

    /**
     * Verifies that the {@link IMObjectDeletionHandlerFactory} returns {@link JobDeletionHandler} for locations.
     */
    @Test
    public void testFactory() {
        IMObjectDeletionHandlerFactory factory = new IMObjectDeletionHandlerFactory(getArchetypeService());
        assertNotNull(applicationContext);
        factory.setApplicationContext(applicationContext);

        Party location = TestHelper.createLocation();
        IMObjectDeletionHandler<Party> handler = factory.create(location);
        assertTrue(handler instanceof OrganisationLocationDeletionHandler);
        assertTrue(handler.getDeletable().canDelete());

        handler.delete(new LocalContext(), new HelpContext("foo", null));
        assertNull(get(location));
    }

    /**
     * Creates a new deletion handler for a location.
     *
     * @param location the location
     * @return a new deletion handler
     */
    protected OrganisationLocationDeletionHandler createDeletionHandler(Party location) {
        return new OrganisationLocationDeletionHandler(location, factory, ServiceHelper.getTransactionManager(),
                                                       ServiceHelper.getArchetypeService());
    }

}