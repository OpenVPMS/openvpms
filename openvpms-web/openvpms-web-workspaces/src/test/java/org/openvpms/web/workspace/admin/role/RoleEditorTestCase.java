/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.role;

import org.junit.Test;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.security.SecurityRole;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link RoleEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class RoleEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * Verifies that roles with duplicate names fail validation.
     */
    @Test
    public void testDuplicate() {
        SecurityRole role1 = create(UserArchetypes.ROLE, SecurityRole.class);
        String name1 = TestHelper.randomName("zrole");

        SecurityRole role2 = create(UserArchetypes.ROLE, SecurityRole.class);
        String name2 = TestHelper.randomName("zrole");
        role2.setName(name2);
        save(role2);

        RoleEditor editor = new RoleEditor(role1, null, new DefaultLayoutContext(new LocalContext(),
                                                                                 new HelpContext("foo", null)));
        assertFalse(editor.isValid());
        editor.setName(name1);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        editor.setName(name2);
        assertInvalid(editor, "A role already exists with name '" + editor.getName() + "'");

        editor.setName(name1);
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link RoleEditor} for <em>security.role</em> instances.
     */
    @Test
    public void testFactory() {
        SecurityRole role = create(UserArchetypes.ROLE, SecurityRole.class);
        IMObjectEditor editor = factory.create(role, new DefaultLayoutContext(new LocalContext(),
                                                                              new HelpContext("foo", null)));
        assertTrue(editor instanceof RoleEditor);
    }

    /**
     * Tests the {@link RoleEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        SecurityRole role = create(UserArchetypes.ROLE, SecurityRole.class);
        RoleEditor editor = new RoleEditor(role, null, new DefaultLayoutContext(new LocalContext(),
                                                                                new HelpContext("foo", null)));
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof RoleEditor);
        assertEquals(role, newInstance.getObject());
    }

}
