/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge.department;

import nextapp.echo2.app.TextField;
import org.junit.Test;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.combo.ComboBox;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link DepartmentReferenceEditor}.
 *
 * @author Tim Anderson
 */
public class DepartmentReferenceEditorTestCase extends AbstractAppTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Verifies that department updates are propagated to the context.
     */
    @Test
    public void testUpdateContext() {
        LocalContext context = new LocalContext();
        context.setUser(userFactory.newUser().build(false)); // need a user to determine available departments

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        SimpleProperty property = new SimpleProperty("reference", Reference.class);

        Entity department1 = practiceFactory.createDepartment();
        Entity department2 = practiceFactory.createDepartment();

        DepartmentReferenceEditor editor = new DepartmentReferenceEditor(property, null, layout);
        editor.getComponent();

        assertNull(editor.getObject());
        assertNull(context.getDepartment());
        editor.setObject(department1);

        assertEquals(department1, editor.getObject());
        assertEquals(department1, context.getDepartment());

        editor.setObject(department2);
        assertEquals(department2, editor.getObject());
        assertEquals(department2, context.getDepartment());

        editor.setObject(null);
        assertNull(editor.getObject());
        assertNull(context.getDepartment());

        // verifies the editor and context updates when text entered into the combo text field
        TextField field = ((ComboBox) editor.getComponent()).getTextField();
        field.setText(department1.getName());
        assertEquals(department1, editor.getObject());
        assertEquals(department1, context.getDepartment());
    }

    /**
     * Verifies that if the department reference is populated with an inactive object, it may be selected, but does not
     * propagate to the context.
     */
    @Test
    public void testInactiveDepartment() {
        User user = userFactory.newUser().build(false);

        Entity inactiveDepartment = practiceFactory.newDepartment().active(false).build(); // inactive department
        Entity activeDepartment = practiceFactory.createDepartment();

        checkInaccessibleDepartment(user, inactiveDepartment, activeDepartment);
    }

    /**
     * Verifies that if the department reference is populated with an object not accessible to the current user,
     * it may be selected, but does not propagate to the context.
     */
    @Test
    public void testDepartmentNotAccessibleToUser() {
        Entity department1 = practiceFactory.createDepartment();
        Entity department2 = practiceFactory.createDepartment();

        // create a user that can only access department2
        User user = userFactory.newUser()
                .addDepartments(department2)
                .build(false);

        checkInaccessibleDepartment(user, department1, department2);
    }

    /**
     * Verifies that if the department reference is populated with an object not accessible to the current user,
     * it may be selected, but does not propagate to the context.
     *
     * @param user                   the user
     * @param inaccessibleDepartment the department the user cannot normally access
     * @param accessibleDepartment   the department the user can access
     */
    private void checkInaccessibleDepartment(User user, Entity inaccessibleDepartment, Entity accessibleDepartment) {
        LocalContext context = new LocalContext();
        context.setUser(user); // need a user to determine available departments

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        SimpleProperty property = new SimpleProperty("reference", Reference.class);

        // pre-populate the reference with the inaccessible department
        property.setValue(inaccessibleDepartment.getObjectReference());

        DepartmentReferenceEditor editor = new DepartmentReferenceEditor(property, null, layout);
        editor.getComponent();

        assertEquals(inaccessibleDepartment, editor.getObject());
        assertNull(context.getDepartment());

        // now change departments to an accessible one, and verify the context updates
        editor.setObject(accessibleDepartment);
        assertEquals(accessibleDepartment, editor.getObject());
        assertEquals(accessibleDepartment, context.getDepartment());

        // now verify that the inaccessible department can be reselected, but does not propagate to the context
        editor.setObject(inaccessibleDepartment);
        assertEquals(inaccessibleDepartment, editor.getObject());
        assertEquals(accessibleDepartment, context.getDepartment());
    }
}