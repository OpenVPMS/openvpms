/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link PatientWeightEditor}.
 *
 * @author Tim Anderson
 */
public class PatientWeightEditorTestCase extends AbstractIMObjectEditorTest<PatientWeightEditor> {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Constructs a {@link PatientWeightEditorTestCase}.
     */
    public PatientWeightEditorTestCase() {
        super(PatientWeightEditor.class, PatientArchetypes.PATIENT_WEIGHT);
    }

    /**
     * Verifies that the weight units default to those configured on the practice.
     */
    @Test
    public void testDefaultWeightUnits() {
        practiceFactory.newPractice()
                .defaultWeightUnits(WeightUnits.POUNDS)
                .build();
        PatientWeightEditor editor1 = (PatientWeightEditor) createEditor(create(PatientArchetypes.PATIENT_WEIGHT));
        assertEquals(WeightUnits.POUNDS.toString(), editor1.getProperty("units").getString());

        practiceFactory.newPractice()
                .defaultWeightUnits(WeightUnits.GRAMS)
                .build();

        PatientWeightEditor editor2 = (PatientWeightEditor) createEditor(create(PatientArchetypes.PATIENT_WEIGHT));
        assertEquals(WeightUnits.GRAMS.toString(), editor2.getProperty("units").getString());
    }

}
