/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.schedule;

import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.test.EchoTestHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link ScheduleEditor}.
 *
 * @author Tim Anderson
 */
public class ScheduleEditorTestCase extends AbstractIMObjectEditorTest<ScheduleEditor> {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Constructs a {@link ScheduleEditorTestCase}.
     */
    public ScheduleEditorTestCase() {
        super(ScheduleEditor.class, ScheduleArchetypes.ORGANISATION_SCHEDULE);
    }

    /**
     * Tests Start Time and End Time validation.
     */
    @Test
    public void testTimesValidation() {
        Entity schedule = schedulingFactory.newSchedule().build(false);
        Context local = new LocalContext();
        local.setLocation(practiceFactory.createLocation());
        DefaultLayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        ScheduleEditor editor = new ScheduleEditor(schedule, null, context);
        editor.getComponent();
        editor.getProperty("name").setValue("Test Schedule");

        assertTrue(editor.isValid());

        Property startTime = editor.getProperty("startTime");
        Property endTime = editor.getProperty("endTime");
        startTime.setValue(Time.valueOf("08:00:00"));
        assertInvalid(editor, "Both Start Time and End Time must be specified");

        startTime.setValue(null);
        endTime.setValue(Time.valueOf("08:00:00"));
        assertInvalid(editor, "Both Start Time and End Time must be specified");

        startTime.setValue(Time.valueOf("09:00:00"));
        endTime.setValue(Time.valueOf("09:00:00"));
        assertInvalid(editor, "Start Time must be less than End Time");

        endTime.setValue(Time.valueOf("17:00:00"));
        assertTrue(editor.isValid());
    }

    /**
     * Tests validation of the <em>maxDuration</em> field.
     */
    @Test
    public void testMaxDurationValidation() {
        Context local = new LocalContext();
        local.setLocation(practiceFactory.createLocation());
        Entity schedule = schedulingFactory.newSchedule()
                .location(local.getLocation())
                .maxDuration(72, DateUnits.HOURS)
                .build();

        // create an appointment and calendar block that won't be examined by the maxDuration check, as they end prior
        // to today. Historical events aren't checked for performance reasons.
        Date yesterday = DateRules.getYesterday();
        schedulingFactory.newAppointment()
                .startTime(DateRules.getPreviousDate(yesterday))
                .endTime(DateRules.getToday())
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .build();
        schedulingFactory.newCalendarBlock()
                .startTime(yesterday)
                .endTime(DateRules.getToday())
                .schedule(schedule)
                .type(schedulingFactory.createCalendarBlockType())
                .build();

        // create a current appointment
        Act act = schedulingFactory.newAppointment()
                .startTime(yesterday)
                .endTime(DateRules.getTomorrow())
                .schedule(schedule)
                .appointmentType(schedulingFactory.createAppointmentType())
                .customer(customerFactory.createCustomer())
                .patient(patientFactory.createPatient())
                .build();

        local.setLocation(practiceFactory.createLocation());
        DefaultLayoutContext context = new DefaultLayoutContext(local, new HelpContext("foo", null));
        ScheduleEditor editor = new ScheduleEditor(schedule, null, context);
        editor.getComponent();
        editor.setMaxDuration(12, DateUnits.HOURS);

        // verify the editor is invalid and a confirmation has been displayed. Click 'No' to revert the change
        // to maxDuration.
        assertFalse(editor.isValid());

        String durationReductionMessage
                = "The Max Duration has been reduced. This can affect the retrieval of historical appointments " +
                  "and calendar blocks.\n\nDo you want to keep this change?";
        ConfirmationDialog confirmationDialog = EchoTestHelper.getWindowPane(ConfirmationDialog.class);
        assertEquals(durationReductionMessage, confirmationDialog.getMessage());
        assertFalse(editor.isValid());

        EchoTestHelper.fireButton(confirmationDialog, ConfirmationDialog.NO_ID);

        assertEquals(72, editor.getMaxDuration());
        assertEquals(DateUnits.HOURS, editor.getMaxDurationUnits());

        // reduce the duration. The confirmation is only displayed on the first reduction.
        editor.setMaxDuration(60, DateUnits.HOURS);
        assertNull(EchoTestHelper.findWindowPane(ConfirmationDialog.class));
        assertTrue(editor.isValid());

        // now verify the editor is invalid as the duration is too short for a current event
        editor.setMaxDuration(12, DateUnits.HOURS);
        String message = "Max Duration must be >= 2 Days to support Appointment on " +
                         DateFormatter.formatDate(act.getActivityStartTime(), false) + " at " +
                         DateFormatter.formatTime(act.getActivityStartTime(), false);
        assertInvalid(editor, message);

        // set the duration to one that is big enough for the event
        editor.setMaxDuration(48, DateUnits.HOURS);
        assertTrue(editor.isValid());
    }
}