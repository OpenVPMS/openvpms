/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.email;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.mail.EmailAddress;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.rules.party.ContactArchetypes.BILLING_PURPOSE;
import static org.openvpms.archetype.rules.party.ContactArchetypes.REMINDER_PURPOSE;

/**
 * Tests the {@link PracticeEmailAddresses} class.
 *
 * @author Tim Anderson
 */
public class PracticeEmailAddressesTestCase extends ArchetypeServiceTest {

    /**
     * The practice.
     */
    private Party practice;

    /**
     * First practice location.
     */
    private Party location1;

    /**
     * Second practice location.
     */
    private Party location2;

    /**
     * Third practice location.
     */
    private Party location3;

    /**
     * Practice reminder contact.
     */
    private Contact practiceReminder;

    /**
     * Location 1 reminder contact.
     */
    private Contact location1Reminder;

    /**
     * Location 2 billing contact.
     */
    private Contact location2Billing;

    /**
     * Location 3 reminder contact.
     */
    private Contact location3Reminder;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        practice = create(PracticeArchetypes.PRACTICE, Party.class);
        location1 = TestHelper.createLocation();
        location2 = TestHelper.createLocation();
        location3 = TestHelper.createLocation();
        location1.setName("X Location 1");
        location2.setName("X Location 2");
        location2.setName("X Location 3");

        practice.setName("OpenVPMS Practice");

        practiceReminder = createEmailContact("mainreminder@practice.com", REMINDER_PURPOSE);
        Contact practiceBilling = createEmailContact("mainbilling@practice.com", BILLING_PURPOSE);
        practice.addContact(practiceReminder);
        practice.addContact(practiceBilling);

        location1Reminder = createEmailContact("branch1reminder@practice.com", REMINDER_PURPOSE);
        Contact location1Billing = createEmailContact("branch1billing@practice.com", BILLING_PURPOSE);
        location1.addContact(location1Reminder);
        location1.addContact(location1Billing);

        location2Billing = createEmailContact("branch2billing@practice.com", BILLING_PURPOSE);
        location2.addContact(location2Billing);

        location3Reminder = createEmailContact("branch3reminder@practice.com", "Name Override", REMINDER_PURPOSE);
        location3.addContact(location3Reminder);
    }

    /**
     * Tests the {@link PracticeEmailAddresses#getPracticeAddress(Party)}method.
     */
    @Test
    public void testGetAddress() {
        // customer1 has a link to location1, so should get the location1 reminder address
        Party customer1 = createCustomer(location1);
        IMObjectBean customerBean = getBean(customer1);
        customerBean.addTarget("practice", location1);

        // customer 2 has a link to location 2
        Party customer2 = createCustomer(location2);

        // customer 3 has a link to location 3
        Party customer3 = createCustomer(location3);

        // customer 4 has no location, so should get the practice reminder address
        Party customer4 = createCustomer(null);

        List<Party> locations = Arrays.asList(location1, location2, location3);
        PracticeEmailAddresses addresses = new PracticeEmailAddresses(practice, locations,
                                                                      REMINDER_PURPOSE, getArchetypeService());

        checkAddress(addresses.getPracticeAddress(customer1), "branch1reminder@practice.com", "X Location 1");
        checkAddress(addresses.getPracticeAddress(customer2), "mainreminder@practice.com", "OpenVPMS Practice");
        checkAddress(addresses.getPracticeAddress(customer3), "branch3reminder@practice.com", "Name Override");
        checkAddress(addresses.getPracticeAddress(customer4), "mainreminder@practice.com", "OpenVPMS Practice");
    }

    /**
     * Tests the {@link PracticeEmailAddresses#getContact(Party)} method.
     */
    @Test
    public void testGetContact() {
        Party location4 = TestHelper.createLocation();
        List<Party> locations = Arrays.asList(location1, location2, location3, location4);
        PracticeEmailAddresses addresses = new PracticeEmailAddresses(practice, locations,
                                                                      REMINDER_PURPOSE, getArchetypeService());

        assertEquals(practiceReminder, addresses.getContact(practice));
        assertEquals(location1Reminder, addresses.getContact(location1));
        assertNull(addresses.getContact(location2));  // no reminder contact
        assertEquals(location3Reminder, addresses.getContact(location3));
        assertNull(addresses.getContact(location4));  // no contacts
    }

    /**
     * Creates a new email contact.
     *
     * @param address the email address
     * @param purpose the contact purpose
     * @return a new contact
     */
    protected Contact createEmailContact(String address, String purpose) {
        return TestHelper.createEmailContact(address, false, purpose);
    }

    /**
     * Creates a new email contact.
     *
     * @param address the email address
     * @param name    the contact name
     * @param purpose the contact purpose
     * @return a new contact
     */
    protected Contact createEmailContact(String address, String name, String purpose) {
        Contact emailContact = TestHelper.createEmailContact(address, false, purpose);
        emailContact.setName(name);
        return emailContact;
    }

    /**
     * Verifies a contact and address matches that expected.
     *
     * @param emailAddress the address
     * @param address      the expected address
     * @param name         the expected name
     */
    private void checkAddress(EmailAddress emailAddress, String address, String name) {
        assertEquals(address, emailAddress.getAddress());
        assertEquals(name, emailAddress.getName());
    }

    /**
     * Creates a customer.
     *
     * @param location the customer practice location. May be {@code null}
     * @return a new customer
     */
    private Party createCustomer(Party location) {
        Party customer = TestHelper.createCustomer(false);
        if (location != null) {
            IMObjectBean customerBean = getBean(customer);
            customerBean.addTarget("practice", location);
        }
        return customer;
    }

}
