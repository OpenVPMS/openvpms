/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.worklist;

import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.workspace.workflow.ScheduleTypeQueryTest;


/**
 * Tests the {@link TaskTypeQuery} class.
 *
 * @author Tim Anderson
 */
public class TaskTypeQueryTestCase extends ScheduleTypeQueryTest {

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    protected Query<Entity> createQuery() {
        return new TaskTypeQuery(null, new LocalContext());
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if <tt>true</tt> save the object, otherwise don't save it
     * @return the new object
     */
    protected Entity createObject(String value, boolean save) {
        return ScheduleTestHelper.createTaskType(value, save);
    }

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    protected String getUniqueValue() {
        return getUniqueValue("ZTaskType");
    }

    /**
     * Creates a new schedule.
     *
     * @return a new schedule
     */
    protected Party createSchedule() {
        return ScheduleTestHelper.createWorkList();
    }

    /**
     * Adds a schedule type to a schedule.
     *
     * @param schedule     the schedule
     * @param scheduleType type the schedule type
     */
    protected void addScheduleType(Party schedule, Entity scheduleType) {
        IMObjectBean bean = getBean(schedule);
        EntityRelationship relationship = (EntityRelationship) bean.addTarget("taskTypes", scheduleType);
        scheduleType.addEntityRelationship(relationship);
        IMObjectBean relBean = getBean(relationship);
        relBean.setValue("noSlots", 1);
    }
}