/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.scheduling.TestAppointmentBuilder;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Base class for workflow tests.
 *
 * @author Tim Anderson
 */
public class AbstractWorkflowTest extends AbstractCustomerChargeActEditorTest {

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Returns a pre-populated appointment builder.
     *
     * @param startTime the start time
     * @param customer  the customer
     * @param patient   the patient
     * @param clinician the clinician
     * @param location  the location
     * @return a new appointment builder
     */
    protected TestAppointmentBuilder newAppointment(Date startTime, Party customer, Party patient, User clinician,
                                                    Party location) {
        return newAppointment(customer, patient, clinician, location)
                .startTime(startTime);
    }

    /**
     * Returns a pre-populated appointment builder.
     *
     * @param customer  the customer
     * @param patient   the patient
     * @param clinician the clinician
     * @param location  the location
     * @return a new appointment builder
     */
    protected TestAppointmentBuilder newAppointment(Party customer, Party patient, User clinician, Party location) {
        Lookup reason = lookupFactory.getLookup(ScheduleArchetypes.VISIT_REASON, "REASON_X", "Reason X");
        return schedulingFactory.newAppointment()
                .customer(customer)
                .patient(patient)
                .clinician(clinician)
                .schedule(schedulingFactory.createSchedule(location))
                .appointmentType(schedulingFactory.createAppointmentType())
                .reason(reason.getCode());
    }
}