/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.otc;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSTransactionStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.payment.PaymentItemEditor;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.test.EchoTestHelper.cancelDialog;
import static org.openvpms.web.test.EchoTestHelper.fireDialogButton;

/**
 * Tests the {@link OverTheCounterWorkflow}.
 *
 * @author Tim Anderson
 */
public class OverTheCounterWorkflowTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * Test context.
     */
    private Context context;

    /**
     * The act authors.
     */
    private User author;

    /**
     * The over-the-counter customer.
     */
    private Party otc;

    /**
     * Customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * Practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        context.setPractice(practiceFactory.getPractice());
        Entity till = practiceFactory.createTill();
        otc = practiceFactory.createOTC();
        Party location = practiceFactory.newLocation()
                .otc(otc)
                .tills(till)
                .build();
        context.setLocation(location);
        author = TestHelper.createUser();
        context.setUser(author);
        context.setTill(till);

        // register an ErrorHandler to collect errors
        initErrorHandler(errors);

        new AuthenticationContextImpl().setUser(author);  // makes author the logged-in user
    }

    /**
     * Tests running the workflow through to completion.
     */
    @Test
    public void testWorkflow() {
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(context);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the charge has been saved
        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);

        // edit the payment
        FinancialAct payment = editPayment(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the payment has been saved
        checkPayment(get(payment), otc, author, ONE, ActStatus.POSTED);

        // verify the charge has been posted
        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.POSTED);

        workflow.print();

        assertTrue(errors.isEmpty());
        workflow.checkComplete();
    }

    /**
     * Verify that the workflow cancels if a charge editing is cancelled via the cancel button.
     */
    @Test
    public void testCancelCharge() {
        checkCancelCharge(false);
    }

    /**
     * Verify that the workflow cancels if a charge editing is cancelled via the 'user close' button.
     */
    @Test
    public void testCancelChargeByUserClose() {
        checkCancelCharge(true);
    }

    /**
     * Verifies that the workflow cancels if payment editing is cancelled via the cancel button.
     */
    @Test
    public void testCancelPayment() {
        checkCancelPayment(false);
    }

    /**
     * Verifies that the workflow cancels if payment editing is cancelled via the 'user close' button.
     */
    @Test
    public void testCancelPaymentByUserClose() {
        checkCancelPayment(true);
    }

    /**
     * Verifies that the payment isn't deleted on cancel if there is a PENDING EFT transaction.
     * <p/>
     * Note that this will leave the OTC customer with IN_PROGRESS and IN_PROGRESS counter sale and payment.
     */
    @Test
    public void testCancelPaymentWithPendingEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.PENDING);
    }

    /**
     * Verifies that the payment isn't deleted on cancel if there is an IN_PROGRESS EFT transaction.
     * <p/>
     * Note that this will leave the OTC customer with IN_PROGRESS and IN_PROGRESS counter sale and payment.
     */
    @Test
    public void testCancelPaymentWithInProgressEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.IN_PROGRESS);
    }

    /**
     * Verifies that the payment isn't deleted on cancel if there is an APPROVED EFT transaction.
     * <p/>
     * Note that this will leave the OTC customer with IN_PROGRESS and IN_PROGRESS counter sale and payment.
     */
    @Test
    public void testCancelPaymentWithApprovedEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.APPROVED);
    }

    /**
     * Verifies that the payment is deleted on cancel if there is a DECLINED EFT transaction.
     */
    @Test
    public void testCancelPaymentWithDeclinedEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.DECLINED);
    }

    /**
     * Verifies that the payment is deleted on cancel if there is an ERROR EFT transaction.
     */
    @Test
    public void testCancelPaymentWithErrorEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.ERROR);
    }

    /**
     * Verifies that the payment is deleted on cancel if there is a NO_TERMINAL EFT transaction.
     */
    @Test
    public void testCancelPaymentWithNoTerminalEFT() {
        checkCancelPaymentWithEFT(EFTPOSTransactionStatus.NO_TERMINAL);
    }

    /**
     * Verifies that an error is raised if the charge is posted externally.
     * <p>
     * This could happen if a user goes into the OTC account and posts the charge while the workflow is active.
     * <p>
     * At present, the workflow will terminate with an error, and both the charge and payment will need to be
     * manually cleaned up.
     */
    @Test
    public void testPostCharge() {
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(context);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the charge has been saved
        charge = get(charge);
        checkCharge(charge, otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);

        charge.setStatus(ActStatus.POSTED);
        save(charge);

        assertTrue(errors.isEmpty());

        // edit the payment
        FinancialAct payment = editPayment(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        assertEquals(1, errors.size());

        assertEquals("Failed to save object. It may have been changed by another user.", errors.get(0));

        workflow.checkComplete();

        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.POSTED);
        checkPayment(get(payment), otc, author, ONE, ActStatus.IN_PROGRESS);
    }

    /**
     * Verifies that an error is raised if the payment is posted externally.
     * <p>
     * This could happen if a user goes into the OTC account and posts the payment while the workflow is active.
     * <p>
     * At present, the workflow will terminate with an error, and both the charge and payment will need to be
     * manually cleaned up. TODO
     */
    @Test
    public void testPostPayment() {
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(context);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the charge has been saved
        charge = get(charge);
        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);

        assertTrue(errors.isEmpty());

        // edit the payment
        FinancialAct payment = editPayment(workflow);

        payment = get(payment);
        payment.setStatus(ActStatus.POSTED);
        save(payment);

        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        assertEquals(1, errors.size());

        assertEquals("Failed to save object. It may have been changed by another user.", errors.get(0));

        workflow.checkComplete();

        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);
        checkPayment(get(payment), otc, author, ONE, ActStatus.POSTED);
    }

    /**
     * Verifies a charge matches that expected.
     *
     * @param charge    the charge
     * @param customer  the expected customer
     * @param author    the expected author
     * @param clinician the expected clinician
     * @param tax       the expected tax
     * @param total     the expected total
     * @param status    the expected status
     */
    protected void checkCharge(FinancialAct charge, Party customer, User author, User clinician, BigDecimal tax,
                               BigDecimal total, String status) {
        assertNotNull(charge);
        IMObjectBean bean = getBean(charge);
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(author.getObjectReference(), charge.getCreatedBy());
        if (bean.hasNode("clinician") && bean.getNode("clinician").getMaxCardinality() != 0) {
            // counter sales have an 0 cardinality clinician node to support querying
            assertEquals(clinician.getObjectReference(), bean.getTargetRef("clinician"));
        }
        checkEquals(tax, bean.getBigDecimal("tax"));
        checkEquals(total, bean.getBigDecimal("amount"));
        assertEquals(status, charge.getStatus());
    }

    /**
     * Verifies a payment matches that expected.
     *
     * @param payment  the payment
     * @param customer the expected customer
     * @param author   the expected author
     * @param total    the expected total
     * @param status   the expected status
     */
    protected void checkPayment(FinancialAct payment, Party customer, User author, BigDecimal total, String status) {
        assertNotNull(payment);
        IMObjectBean bean = getBean(payment);
        assertEquals(customer.getObjectReference(), bean.getTargetRef("customer"));
        assertEquals(author.getObjectReference(), payment.getCreatedBy());
        checkEquals(total, bean.getBigDecimal("amount"));
        assertEquals(status, payment.getStatus());
    }

    /**
     * Tests the behaviour of cancelling payments for a particular EFT transaction status.
     *
     * @param status the EFT transaction status
     */
    private void checkCancelPaymentWithEFT(String status) {
        OverTheCounterWorkflowRunner.TestOTCWorkflow eftWorkflow = new OverTheCounterWorkflowRunner.TestOTCWorkflow(
                context, new HelpContext("foo", null)) {
            @Override
            protected EditIMObjectTask createPaymentTask() {
                return new OTCPaymentTask() {
                    @Override
                    protected void create(TaskContext context) {
                        // simulate an EFT payment
                        FinancialAct charge = (FinancialAct) context.getObject(CustomerAccountArchetypes.COUNTER);
                        Entity terminal = !status.equals(EFTPOSTransactionStatus.NO_TERMINAL)
                                          ? practiceFactory.createEFTPOSTerminal() : null;
                        FinancialAct payment = accountFactory.newPayment()
                                .customer(context.getCustomer())
                                .location(context.getLocation())
                                .till(context.getTill())
                                .eft().addTransaction(status, terminal)
                                .amount(charge.getTotal())
                                .add()
                                .status(FinancialActStatus.IN_PROGRESS)
                                .build();
                        context.addObject(payment);
                        super.edit(context);
                    }
                };
            }
        };
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(eftWorkflow);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the charge has been saved
        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);

        // edit the payment. Note that as it was pre-created, it shouldn't have changed
        OTCPaymentEditor paymentEditor = workflow.getPaymentEditor();
        assertTrue(paymentEditor.isValid());
        FinancialAct payment = paymentEditor.getObject();

        // verify the payment and saved instance matches that expected
        checkPayment(payment, otc, author, ONE, ActStatus.IN_PROGRESS);
        checkPayment(get(payment), otc, author, ONE, ActStatus.IN_PROGRESS);

        // now cancel the payment
        cancelDialog(workflow.getEditDialog(), false);
        ConfirmationDialog dialog = EchoTestHelper.getWindowPane(ConfirmationDialog.class);
        if (EFTPOSTransactionStatus.PENDING.equals(status) || EFTPOSTransactionStatus.IN_PROGRESS.equals(status)
            || EFTPOSTransactionStatus.APPROVED.equals(status)) {
            assertEquals("This Payment was saved but has not been finalised.\n\nCancel editing?", dialog.getMessage());
            fireDialogButton(dialog, ConfirmationDialog.YES_ID);
            assertEquals(1, errors.size());
            if (EFTPOSTransactionStatus.APPROVED.equals(status)) {
                assertEquals("Cannot delete Payment. It has an approved EFTPOS transaction.", errors.get(0));
            } else {
                assertEquals("Cannot delete Payment. It has an outstanding EFTPOS transaction.", errors.get(0));
            }

            // neither the charge nor payment should be removed
            checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);
            checkPayment(get(payment), otc, author, ONE, ActStatus.IN_PROGRESS);
        } else {
            assertEquals("This Payment has been saved.\n\nDo you want to delete it?", dialog.getMessage());
            fireDialogButton(dialog, ConfirmationDialog.YES_ID);
            assertNull(get(charge));
            assertNull(get(payment));
            assertEquals(0, errors.size());
        }

        workflow.checkComplete();
    }

    /**
     * Verifies that if charging is cancelled, the workflow is ended and the charge is deleted.
     *
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelCharge(boolean userClose) {
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(context);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // now cancel. The charge should be deleted.
        cancelDialog(workflow.getEditDialog(), userClose);
        assertNull(get(charge));

        workflow.checkComplete();
    }

    /**
     * Verifies that if payment is cancelled, the workflow is ended and both the payment and charge is deleted.
     *
     * @param userClose if {@code true} cancel via the 'user close' button, otherwise use the 'cancel' button
     */
    private void checkCancelPayment(boolean userClose) {
        OverTheCounterWorkflowRunner workflow = new OverTheCounterWorkflowRunner(context);
        workflow.start();

        // edit the charge
        FinancialAct charge = editCharge(workflow);
        fireDialogButton(workflow.getEditDialog(), PopupDialog.OK_ID);

        // verify the charge has been saved
        checkCharge(get(charge), otc, author, null, BigDecimal.ZERO, ONE, ActStatus.IN_PROGRESS);

        // edit the payment
        FinancialAct payment = editPayment(workflow);

        // verify the payment has been saved
        checkPayment(get(payment), otc, author, ONE, ActStatus.IN_PROGRESS);

        // now cancel the payment
        cancelDialog(workflow.getEditDialog(), userClose);
        ConfirmationDialog dialog = EchoTestHelper.getWindowPane(ConfirmationDialog.class);
        assertEquals("This Payment was saved but has not been finalised.\n\nCancel editing?", dialog.getMessage());
        fireDialogButton(dialog, ConfirmationDialog.YES_ID);

        // the charge and payment should have been removed
        assertNull(get(charge));
        assertNull(get(payment));

        workflow.checkComplete();
    }

    /**
     * Helper to edit the charge.
     *
     * @param workflow the workflow
     * @return the charge act
     */
    private FinancialAct editCharge(OverTheCounterWorkflowRunner workflow) {
        Product product = productFactory.newMerchandise().fixedPrice(1).build();

        TestOTCChargeTask chargeTask = workflow.getChargeTask();
        OTCChargeEditor chargeEditor = workflow.getChargeEditor();
        addItem(chargeEditor, null, product, ONE, chargeTask.getQueue());
        fireDialogButton(chargeTask.getEditDialog(), PopupDialog.APPLY_ID); // force the charge to save

        FinancialAct charge = get(chargeEditor.getObject());
        assertNotNull(charge);
        checkCharge(charge, otc, author, null, BigDecimal.ZERO, ONE);
        assertEquals(ActStatus.IN_PROGRESS, charge.getStatus());
        return charge;
    }

    /**
     * Helper to edit the payment.
     *
     * @param workflow the workflow
     * @return the payment act
     */
    private FinancialAct editPayment(OverTheCounterWorkflowRunner workflow) {
        OTCPaymentTask paymentTask = workflow.getPaymentTask();
        OTCPaymentEditor paymentEditor = workflow.getPaymentEditor();
        PaymentItemEditor paymentItemEditor = paymentEditor.getUnsavedItem(CustomerAccountArchetypes.PAYMENT_CASH);
        paymentItemEditor.getProperty("amount").setValue(ONE);
        assertTrue(paymentEditor.isValid());
        fireDialogButton(paymentTask.getEditDialog(), PopupDialog.APPLY_ID);  // force the payment to save

        // verify the payment has been saved
        FinancialAct payment = get(paymentEditor.getObject());
        assertNotNull(payment);
        checkPayment(payment, otc, author, ONE, ActStatus.IN_PROGRESS);
        return payment;
    }

}
