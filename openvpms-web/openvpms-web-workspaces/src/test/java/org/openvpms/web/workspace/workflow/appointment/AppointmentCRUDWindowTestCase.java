/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.scheduling.TestSchedulingFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link AppointmentCRUDWindow}.
 *
 * @author Tim Anderson
 */
public class AppointmentCRUDWindowTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The scheduling factory.
     */
    @Autowired
    private TestSchedulingFactory schedulingFactory;

    /**
     * Verifies that selecting an appointment updates the context.
     * <p/>
     * This allows the appointment to be made available for macros (e.g. to SMS confirmation to the customer).
     */
    @Test
    public void testUpdateContext() {
        GlobalContext context = ContextApplicationInstance.getInstance().getContext();
        AppointmentCRUDWindow window = new AppointmentCRUDWindow(Mockito.mock(AppointmentBrowser.class),
                                                                 context, new HelpContext("foo", null));

        assertNull(context.getAppointment());
        Party location = practiceFactory.createLocation();
        Entity schedule = schedulingFactory.createSchedule(location);
        Entity appointmentType = schedulingFactory.createAppointmentType();
        Party customer = customerFactory.createCustomer();
        Act appointment = schedulingFactory.newAppointment()
                .startTime(DateRules.getToday())
                .schedule(schedule)
                .appointmentType(appointmentType)
                .customer(customer)
                .build();
        window.setObject(appointment);

        assertEquals(appointment, context.getAppointment());

        window.setObject(null);
        assertNull(context.getAppointment());
    }

    /**
     * Verifies that when a CONFIRMED appointment is cut and pasted to the same time slot in a different schedule,
     * its reminder details and status don't change.
     */
    @Test
    public void testCutPasteConfirmedAppointmentToSameTime() {
        checkCutPasteConfirmedAppointment(true);
    }

    /**
     * Verifies that when a CONFIRMED appointment is cut and pasted to a different time, its status reverts to PENDING,
     * and the reminderSent and reminderError nodes are cleared.
     */
    @Test
    public void testCutPasteConfirmedAppointmentToDifferentTime() {
        checkCutPasteConfirmedAppointment(false);
    }

    /**
     * Verifies the behaviour of cutting and pasting a CONFIRMED appointment.
     *
     * @param sameTime if {@code true}, move it to a different schedule with the same time slot
     */
    private void checkCutPasteConfirmedAppointment(boolean sameTime) {
        Party location = practiceFactory.createLocation();
        Context context = new LocalContext();
        HelpContext help = new HelpContext("foo", null);
        LayoutContext layoutContext = new DefaultLayoutContext(context, help);

        Entity appointmentType = schedulingFactory.createAppointmentType();
        Entity schedule1 = schedulingFactory.newSchedule()
                .location(location)
                .appointmentTypes(appointmentType)
                .build();
        Entity schedule2 = schedulingFactory.newSchedule()
                .location(location)
                .appointmentTypes(appointmentType)
                .build();
        Entity scheduleView = schedulingFactory.createScheduleView(schedule1, schedule2);
        practiceFactory.updateLocation(location)
                .scheduleViews(scheduleView)
                .build();

        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);

        Date start1 = getDatetime("2023-07-25 09:00");
        Date reminderSent = DateRules.getDate(start1, 1, DateUnits.HOURS);
        Act appointment = schedulingFactory.newAppointment()
                .startTime(start1)
                .schedule(schedule1)
                .appointmentType(appointmentType)
                .customer(customer)
                .patient(patient)
                .sendReminder(true)
                .reminderSent(reminderSent)
                .reminderError("an error")  // normally would be null if a reminder had been sent
                .status(AppointmentStatus.CONFIRMED)
                .build();

        AppointmentBrowser browser = new AppointmentBrowser(location, layoutContext);
        browser.getComponent();
        browser.setDate(DateRules.getDate(start1));
        assertEquals(scheduleView, browser.getScheduleView());
        browser.query();
        AppointmentCRUDWindow window = new AppointmentCRUDWindow(browser, context, help);

        browser.getComponent();
        browser.setSelected(schedule1, start1);
        PropertySet selected = browser.getSelected();
        assertNotNull(selected);
        assertEquals(appointment, browser.getAct(selected));

        // cut the appointment
        window.cut();
        assertEquals(selected, browser.getMarked());

        Date start2;
        Entity newSchedule;
        if (sameTime) {
            // move the appointment to a different schedule
            start2 = start1;
            newSchedule = schedule2;
        } else {
            // move the appointment to a slot 2 hours later
            newSchedule = schedule1;
            start2 = DateRules.getDate(start1, 2, DateUnits.HOURS);
        }
        browser.setSelected(newSchedule, start2);
        window.paste();

        // verify the appointment has been updated
        assertNull(browser.getMarked());
        Act updated;
        if (sameTime) {
            // browser won't be updated when moving across schedules
            updated = get(appointment);
        } else {
            updated = browser.getAct(browser.getSelected());
        }
        assertEquals(appointment, updated);
        assertEquals(start2, updated.getActivityStartTime());
        assertEquals(DateRules.getDate(start2, 15, DateUnits.MINUTES), updated.getActivityEndTime());

        IMObjectBean bean = getBean(updated);
        assertEquals(newSchedule.getObjectReference(), bean.getTargetRef("schedule"));
        if (sameTime) {
            assertEquals(AppointmentStatus.CONFIRMED, updated.getStatus());
            assertEquals(reminderSent, bean.getDate("reminderSent"));
            assertEquals("an error", bean.getString("reminderError"));
        } else {
            assertEquals(AppointmentStatus.PENDING, updated.getStatus());
            assertNull(bean.getDate("reminderSent"));
            assertNull(bean.getString("reminderError"));
        }
    }
}
