/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.lookup;

import org.junit.Test;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link SuburbLookupEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class SuburbLookupEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * Verifies that the code is generated from the name and postcode, when the postcode is present.
     */
    @Test
    public void testCodeForNameAndPostcode() {
        Lookup lookup = createSuburb();
        Lookup state = getState("VIC");
        SuburbLookupEditor editor = createEditor(lookup);
        assertFalse(editor.isValid());

        String name = TestHelper.randomName("Suburb");
        editor.getProperty("name").setValue(name);
        assertEquals(name.toUpperCase(), editor.getCode());
        assertEquals(name, lookup.getDescription());

        String postcode = "9999";
        editor.setPostcode(postcode);

        String code = name.toUpperCase() + "_" + postcode;
        assertEquals(code, editor.getCode());

        editor.setState(state);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        lookup = get(lookup);
        assertEquals(code, lookup.getCode());
        assertEquals(name, lookup.getName());
        assertEquals(name + " " + postcode, lookup.getDescription());

        // verifies that the code doesn't change after the suburb is saved
        editor.setPostcode("1111");
        assertEquals(code, editor.getCode());
        assertTrue(SaveHelper.save(editor));

        lookup = get(lookup);
        assertEquals(code, lookup.getCode());
    }

    /**
     * Verifies that when no postcode is present, the lookup code is generated from the name + state.
     */
    @Test
    public void testCodeForNameAndState() {
        Lookup lookup = createSuburb();
        Lookup state = getState("VIC");
        SuburbLookupEditor editor = createEditor(lookup);
        assertFalse(editor.isValid());

        String name = TestHelper.randomName("Suburb");
        editor.getProperty("name").setValue(name);
        editor.setState(state);
        String code = name.toUpperCase() + "_" + state.getCode();
        assertEquals(code, editor.getCode());

        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        lookup = get(lookup);
        assertEquals(code, lookup.getCode());
        assertEquals(name, lookup.getName());
        assertEquals(name, lookup.getDescription());

        // verifies that the code doesn't change after the suburb is saved
        editor.setState(getState("TAS"));
        assertEquals(code, editor.getCode());
        assertTrue(SaveHelper.save(editor));

        lookup = get(lookup);
        assertEquals(code, lookup.getCode());
    }

    /**
     * Tests the {@link SuburbLookupEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Lookup lookup = createSuburb();
        SuburbLookupEditor editor = createEditor(lookup);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof SuburbLookupEditor);
        assertEquals(lookup, newInstance.getObject());
    }

    /**
     * Verifies that {@link IMObjectEditorFactory} returns the {@link SuburbLookupEditor} for <em>lookup.suburb</em>
     * instances.
     */
    @Test
    public void testFactory() {
        Lookup lookup = createSuburb();
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = factory.create(lookup, context);
        assertTrue(editor instanceof SuburbLookupEditor);
    }

    /**
     * Creates a new editor.
     *
     * @param suburb the suburb to edit
     * @return a new editor
     */
    private SuburbLookupEditor createEditor(Lookup suburb) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        return new SuburbLookupEditor(suburb, null, context);
    }

    /**
     * Creates a new suburb lookup.
     *
     * @return the lookup
     */
    private Lookup createSuburb() {
        return create(ContactArchetypes.SUBURB, Lookup.class);
    }
    /**
     * Returns a state.
     *
     * @param code the state code
     * @return a state
     */
    private Lookup getState(String code) {
        return (Lookup) lookupFactory.getLookup(ContactArchetypes.STATE, code);
    }
}