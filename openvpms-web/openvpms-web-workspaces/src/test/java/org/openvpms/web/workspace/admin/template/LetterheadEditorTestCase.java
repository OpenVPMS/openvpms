/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestLetterheadBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.admin.template.letterhead.LetterheadEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;

/**
 * Tests the {@link LetterheadEditor}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class LetterheadEditorTestCase extends AbstractAppTest {

    /**
     * The editor factory.
     */
    @Autowired
    private IMObjectEditorFactory factory;

    /**
     * The document factory.
     */
    @Autowired
    private TestDocumentFactory documentFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The document rules.
     */
    @Autowired
    private DocumentRules rules;

    /**
     * Verifies that one of the logoFile or logo must be present.
     */
    @Test
    public void testValidateLogo() throws IOException {
        // create a letterhead with no logo, and verify it is initially invalid
        Entity letterhead = create(DocumentArchetypes.LETTERHEAD, Entity.class);
        letterhead.setName("dummy");
        LetterheadEditor editor = (LetterheadEditor) createEditor(letterhead);
        assertInvalid(editor, "One of Logo File or Logo must be supplied");
        Property logoFile = editor.getProperty("logoFile");
        logoFile.setValue("foo.gif");
        assertInvalid(editor, "'foo.gif' does not exist");

        // now associate a valid file, and verify it is valid
        Path file = Files.createTempFile("zz", ".png");
        logoFile.setValue(file.toString());
        assertTrue(editor.isValid());

        // and remove it - should be invalid again
        logoFile.setValue(null);
        assertFalse(editor.isValid());

        // now set a logo document. Editor should now be valid.
        Document logo = documentFactory.newDocument()
                .name(ValueStrategy.suffix(".png"))
                .content(RandomUtils.nextBytes(10))
                .build();
        editor.setLogo(logo);
        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));

        DocumentAct act = rules.getLogo(letterhead);
        assertNotNull(act);
        assertEquals(logo.getObjectReference(), act.getDocument());
    }

    /**
     * Verifies the {@link IMObjectEditorFactory} returns {@link LetterheadEditor} for <em>entity.letterhead</em>
     * instances.
     */
    @Test
    public void testFactory() {
        Entity letterhead = create(DocumentArchetypes.LETTERHEAD, Entity.class);
        IMObjectEditor editor = createEditor(letterhead);
        assertTrue(editor instanceof LetterheadEditor);
    }

    /**
     * Tests the {@link LetterheadEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Entity letterhead = create(DocumentArchetypes.LETTERHEAD, Entity.class);
        IMObjectEditor editor = createEditor(letterhead);
        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof LetterheadEditor);
        assertEquals(letterhead, newInstance.getObject());
    }

    /**
     * Verifies that the logo act is deleted if the letterhead is.
     */
    @Test
    public void testDelete() {
        TestLetterheadBuilder builder = documentFactory.newLetterhead()
                .logo(documentFactory.createJRXML());
        Entity letterhead = builder.build();
        DocumentAct logo = builder.getLogo();
        assertNotNull(logo);
        assertNotNull(logo.getDocument());

        IMObjectEditor editor = createEditor(letterhead);

        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                editor.delete();
            }
        });
        assertNull(get(letterhead));
        assertNull(get(logo));
        assertNull(get(logo.getDocument()));
    }

    /**
     * Creates a new editor for letterhead.
     *
     * @param letterhead the letterhead
     * @return a new editor
     */
    private IMObjectEditor createEditor(Entity letterhead) {
        return factory.create(letterhead, new DefaultLayoutContext(new LocalContext(),
                                                                   new HelpContext("foo", null)));
    }
}