/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.summary;

import nextapp.echo2.app.Component;
import org.junit.Test;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.prefs.UserPreferences;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.patient.CustomerPatientSummary;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link CustomerPatientSummary}.
 *
 * @author Tim Anderson
 */
public class CustomerPatientSummaryTestCase extends AbstractAppTest {

    /**
     * Verifies that the global context is not used to select the customer and patient.
     */
    @Test
    public void testContext() {
        Party globalCustomer = TestHelper.createCustomer();
        Party globalPatient = TestHelper.createPatient(globalCustomer);
        Party customer1 = TestHelper.createCustomer();
        Party customer2 = TestHelper.createCustomer();
        Party patient2 = TestHelper.createPatient(customer2);
        Party patient3 = TestHelper.createPatient();
        GlobalContext global = ContextApplicationInstance.getInstance().getContext();
        global.setCustomer(globalCustomer);
        global.setPatient(globalPatient);

        UserPreferences preferences = ContextApplicationInstance.getInstance().getPreferences();

        // verify that the customer and patient from the appointment are used to create the summary.
        // As no patient is present, there should be not patient context
        Party location = TestHelper.createLocation();
        Party schedule = ScheduleTestHelper.createSchedule(location);
        Act appointment = ScheduleTestHelper.createAppointment(new Date(), schedule,
                                                               customer1, null, WorkflowStatus.PENDING);

        TestCustomerPatientSummary summary1 = new TestCustomerPatientSummary(global, preferences);
        summary1.getSummary(appointment);
        assertNotNull(summary1.customerContext);
        assertEquals(customer1, summary1.customerContext.getCustomer());
        assertNull(summary1.customerContext.getPatient());
        assertNull(summary1.patientContext);

        // now verify that the owner of the specified patient is used
        TestCustomerPatientSummary summary2 = new TestCustomerPatientSummary(global, preferences);
        summary2.getSummary(patient2);
        assertNotNull(summary2.customerContext);
        assertEquals(customer2, summary2.customerContext.getCustomer());

        assertNotNull(summary2.patientContext);
        assertEquals(patient2, summary2.customerContext.getPatient());

        // now verify that when a patient has no owner, the customer context is null
        TestCustomerPatientSummary summary3 = new TestCustomerPatientSummary(global, preferences);
        summary3.getSummary(patient3);
        assertNull(summary3.customerContext);
        assertNotNull(summary3.patientContext);
    }

    private static class TestCustomerPatientSummary extends CustomerPatientSummary {

        /**
         * The context used to construct the customer summary.
         */
        private Context customerContext;

        /**
         * The context used to construct the patient  summary.
         */
        private Context patientContext;

        /**
         * Constructs a {@link TestCustomerPatientSummary}.
         *
         * @param context     the context
         * @param preferences user preferences
         */
        public TestCustomerPatientSummary(Context context, Preferences preferences) {
            super(context, new HelpContext("foo", null), preferences);
        }

        /**
         * Returns the customer summary component
         *
         * @param customer the customer to summarise
         * @param context  the context
         * @return the customer summary component
         */
        @Override
        protected Component getCustomerSummary(Party customer, Context context) {
            customerContext = context;
            return super.getCustomerSummary(customer, context);
        }

        /**
         * Returns the patient summary component
         *
         * @param patient the patient to summarise
         * @param context the context
         * @return the customer summary component
         */
        @Override
        protected Component getPatientSummary(Party patient, Context context) {
            patientContext = context;
            return super.getPatientSummary(patient, context);
        }
    }
}