/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.WindowPane;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.invoice.InvoiceItemStatus;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerAccountFactory;
import org.openvpms.archetype.test.builder.customer.account.TestCustomerChargeBuilder;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.customer.account.AccountActActions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.web.test.EchoTestHelper.findMessageDialogAndFireButton;
import static org.openvpms.web.test.EchoTestHelper.findWindowPane;

/**
 * Tests the {@link ChargePoster}.
 *
 * @author Tim Anderson
 */
public class ChargePosterTestCase extends AbstractAppTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The customer account rules.
     */
    @Autowired
    private CustomerAccountRules accountRules;

    /**
     * The account reminder rules.
     */
    @Autowired
    private AccountReminderRules reminderRules;

    /**
     * The customer account factory.
     */
    @Autowired
    private TestCustomerAccountFactory accountFactory;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The context.
     */
    private Context context;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        super.setUp();
        context = new LocalContext();
        context.setPractice(practiceFactory.getPractice());
        context.setLocation(practiceFactory.createLocation());

        initErrorHandler(errors);
    }

    /**
     * Tests posting a charge.
     */
    @Test
    public void testPost() {
        checkPost(accountFactory.newInvoice(10, IN_PROGRESS));
        checkPost(accountFactory.newCredit(10, IN_PROGRESS));
        checkPost(accountFactory.newCounterSale(10, IN_PROGRESS));
    }

    /**
     * Verify that posting an already posted charge is a no-op.
     */
    @Test
    public void testPostAlreadyPosted() {
        FinancialAct charge = accountFactory.createInvoice(10, POSTED);
        ChargePoster poster = createChargePoster(charge);

        MutableObject<FinancialAct> latest = new MutableObject<>();
        poster.post(latest::setValue);

        // no dialogs should be displayed
        assertNull(findWindowPane(WindowPane.class));

        // verify the listener was called with the charge, and the charge wasn't changed
        assertEquals(charge, latest.getValue());
        assertEquals(charge.getVersion(), latest.getValue().getVersion());

        // verify there are no errors
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests posting a charge that has been deleted by another user displays an error.
     */
    @Test
    public void testPostDeleted() {
        FinancialAct charge = accountFactory.createInvoice(10, IN_PROGRESS);
        remove(charge);

        ChargePoster poster = createChargePoster(charge);

        MutableObject<FinancialAct> latest = new MutableObject<>();
        poster.post(latest::setValue);

        String displayName = getDisplayName(charge);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        assertEquals(1, errors.size());
        assertEquals(getDisplayName(charge) + " may have been deleted by another user", errors.get(0));
        assertNull(EchoTestHelper.findWindowPane(WindowPane.class));
    }

    /**
     * Verifies a confirmation dialog is displayed if an invoice is being finalised and the customer has undispensed
     * orders.
     */
    @Test
    public void testUndispensedOrders() {
        Party customer = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient(customer);
        Date now = new Date();

        // create an invoice with an item that has been ordered by a pharmacy, and a smaller quantity has been received
        // than ordered
        FinancialAct invoice = accountFactory.newInvoice()
                .customer(customer)
                .item().patient(patient).medicationProduct().quantity(10).unitPrice(10)
                .status(InvoiceItemStatus.ORDERED).receivedQuantity(5)
                .add()
                .status(IN_PROGRESS)
                .build();
        ChargePoster poster = createChargePoster(invoice);

        MutableObject<FinancialAct> latest = new MutableObject<>();
        poster.post(latest::setValue);

        String displayName = getDisplayName(invoice);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName + "?",
                                       "The following products have been ordered but not dispensed.\n\n" +
                                       "Are you sure you want to finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        // verify the charge is POSTED, and has a startTime >= now
        FinancialAct reloaded = get(invoice);
        assertEquals(POSTED, reloaded.getStatus());
        assertTrue(DateRules.compareTo(reloaded.getActivityStartTime(), now, true) >= 0);

        // verify the listener was called with the charge
        assertEquals(reloaded, latest.getValue());
        assertEquals(reloaded.getVersion(), latest.getValue().getVersion());

        // verify there are no errors
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests posting a charge.
     *
     * @param builder the charge builder, fully populated
     */
    private void checkPost(TestCustomerChargeBuilder<?, ?> builder) {
        errors.clear();
        Date now = new Date();

        // build the charge, dating it from yesterday.
        FinancialAct charge = builder.startTime(DateRules.getYesterday())
                .build();

        ChargePoster poster = createChargePoster(charge);

        MutableObject<FinancialAct> latest = new MutableObject<>();
        poster.post(latest::setValue);

        String displayName = getDisplayName(charge);
        findMessageDialogAndFireButton(ConfirmationDialog.class, "Finalise " + displayName,
                                       "Are you sure you want to Finalise the " + displayName + "?",
                                       ConfirmationDialog.OK_ID);

        // verify the charge is POSTED, and has a startTime >= now
        FinancialAct reloaded = get(charge);
        assertEquals(POSTED, reloaded.getStatus());
        assertTrue(DateRules.compareTo(reloaded.getActivityStartTime(), now, true) >= 0);

        // verify the listener was called with the charge
        assertEquals(reloaded, latest.getValue());
        assertEquals(reloaded.getVersion(), latest.getValue().getVersion());

        // verify there are no errors
        assertTrue(errors.isEmpty());
    }

    /**
     * Creates a {@link ChargePoster} for a charge.
     *
     * @param charge the charge
     * @return a new {@link ChargePoster}
     */
    private ChargePoster createChargePoster(FinancialAct charge) {
        AccountActActions actions = new AccountActActions(accountRules, reminderRules, getPracticeService());
        LayoutContext layoutContext = new DefaultLayoutContext(context, new HelpContext("foo", null));
        return new ChargePoster(charge, actions, layoutContext);
    }
}