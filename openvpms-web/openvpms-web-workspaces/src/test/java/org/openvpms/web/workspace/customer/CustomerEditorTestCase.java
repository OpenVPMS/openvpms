/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import org.junit.Test;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.lookup.TestLookupFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.product.TestProductFactory;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.BrowserDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.openvpms.web.workspace.patient.PatientEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;
import static org.openvpms.web.test.EchoTestHelper.findBrowserDialog;
import static org.openvpms.web.test.EchoTestHelper.findEditDialog;
import static org.openvpms.web.test.EchoTestHelper.fireButton;

/**
 * Tests the {@link CustomerEditor}.
 *
 * @author Tim Anderson
 */
public class CustomerEditorTestCase extends AbstractAppTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The lookup factory.
     */
    @Autowired
    private TestLookupFactory lookupFactory;

    /**
     * The product factory.
     */
    @Autowired
    private TestProductFactory productFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * Tests the {@link CustomerEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party customer = customerFactory.createCustomer();
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        CustomerEditor editor = new CustomerEditor(customer, null, context);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof CustomerEditor);
    }

    /**
     * Verifies a new patient can added to a customer.
     */
    @Test
    public void testAddNewPatient() {
        String species = lookupFactory.getSpecies("CANINE").getCode();

        // create a customer, and edit it
        Party customer = customerFactory.createCustomer();
        CustomerEditor editor = createEditor(customer);

        // add a new patient
        PatientEntityRelationshipCollectionEditor patientCollectionEditor = editor.getPatientCollectionEditor();
        assertNotNull(patientCollectionEditor);
        fireButton(patientCollectionEditor.getComponent(), "add");
        PatientOwnerRelationshipEditor relationshipEditor
                = (PatientOwnerRelationshipEditor) patientCollectionEditor.getCurrentEditor();
        assertNotNull(relationshipEditor);
        fireButton(relationshipEditor.getComponent(), "button.select");
        BrowserDialog<?> patientBrowser = findBrowserDialog();
        assertNotNull(patientBrowser);
        fireButton(patientBrowser, "new");

        // edit the patient
        EditDialog patientEditDialog = findEditDialog();
        assertNotNull(patientEditDialog);
        PatientEditor patientEditor = (PatientEditor) patientEditDialog.getEditor();
        patientEditor.getProperty("name").setValue("Fido");
        patientEditor.getProperty("species").setValue(species);
        fireButton(patientBrowser, "new");
        fireButton(patientEditDialog, "ok");
        assertTrue(SaveHelper.save(editor));

        // verify the patient has been added to the customer
        customer = get(customer);
        IMObjectBean bean = getBean(customer);
        List<Party> patients = bean.getTargets("patients", Party.class);
        assertEquals(1, patients.size());
        assertEquals(patients.get(0), patientEditor.getObject());
    }

    /**
     * Verifies the behaviour of the referral and referredByCustomer nodes.
     */
    @Test
    public void testReferredBy() {
        lookupFactory.getLookup(CustomerArchetypes.CUSTOMER_REFERRAL, "CUSTOMER");
        lookupFactory.getLookup(CustomerArchetypes.CUSTOMER_REFERRAL, "GOOGLE");
        Party customer1 = customerFactory.createCustomer();
        Party customer2 = customerFactory.createCustomer();
        CustomerEditor editor = createEditor(customer1);
        assertValid(editor);

        editor.setReferral("GOOGLE");
        assertValid(editor);

        editor.setReferral("CUSTOMER");
        assertInvalid(editor, "Referred by Customer is required");
        editor.setReferredByCustomer(customer1);
        assertInvalid(editor, "The customer cannot refer themselves.\n\n" +
                              "Select a different customer.");
        editor.setReferredByCustomer(customer2);
        assertValid(editor);

        assertTrue(SaveHelper.save(editor));

        checkReferral(customer1, "CUSTOMER", customer2);

        editor.setReferral("GOOGLE");
        assertTrue(SaveHelper.save(editor));
        checkReferral(customer1, "GOOGLE", null);
    }

    /**
     * Tests customer deletion.
     */
    @Test
    public void testDelete() {
        Party referredBy = customerFactory.createCustomer();
        Party patient = patientFactory.createPatient();
        Entity discount = productFactory.newDiscount().percentage(10).discountFixedPrice(true).build();
        Lookup tax = lookupFactory.createTaxType(BigDecimal.TEN);
        Party customer = customerFactory.newCustomer()
                .referredBy("CUSTOMER")
                .referredByCustomer(referredBy)
                .addPatient(patient)
                .addDiscounts(discount)
                .addTaxExemptions(tax.getCode())
                .build();

        CustomerEditor editor = createEditor(customer);
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                editor.delete();
            }
        });

        // verify the customer is deleted
        assertNull(get(customer));

        // verify the objects it referred to haven't been deleted
        assertNotNull(get(referredBy));
        assertNotNull(get(patient));
        assertNotNull(get(discount));
        assertNotNull(get(tax));
    }

    /**
     * Verifies the referral and referredByCustomer nodes match those expected.
     *
     * @param customer   the customer
     * @param referral   the expected <em>lookup.customerReferral</em> code
     * @param referredBy the expected referred-by customer
     */
    private void checkReferral(Party customer, String referral, Party referredBy) {
        IMObjectBean bean = getBean(get(customer)); // reload to ensure values have been saved
        assertEquals(referral, bean.getString("referral"));
        assertEquals(referredBy, bean.getTarget("referredByCustomer"));
    }

    /**
     * Creates a customer editor.
     *
     * @param customer the customer to edit
     * @return the new editor
     */
    private CustomerEditor createEditor(Party customer) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        CustomerEditor editor = new CustomerEditor(customer, null, context);
        editor.getComponent();
        return editor;
    }
}
