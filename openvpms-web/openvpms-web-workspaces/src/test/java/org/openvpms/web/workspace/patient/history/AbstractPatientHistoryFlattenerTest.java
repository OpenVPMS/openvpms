/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.openvpms.archetype.rules.customer.CommunicationArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Base class for {@link AbstractPatientHistoryFlattener} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPatientHistoryFlattenerTest extends ArchetypeServiceTest {

    /**
     * Verifies that the history is flattened correctly.
     *
     * @param flattener           the flattener
     * @param acts                the primary acts, ordered on timestamp. Must correspond to {@code parentSortAscending}
     * @param archetypes          the archetypes to filter
     * @param parentSortAscending if {@code true}, the acts are sorted on ascending timestamp, else descending timestamp
     * @param childSortAscending  if {@code true}, child acts are sorted on ascending timestamp, else descending
     *                            timestamp
     * @param expected            the expected acts, in order
     */
    protected void check(AbstractPatientHistoryFlattener flattener, List<Act> acts, String[] archetypes,
                         boolean parentSortAscending, boolean childSortAscending, Act... expected) {
        List<Act> actual = flattener.flatten(acts, archetypes, null, parentSortAscending, childSortAscending);
        assertEquals(expected.length, actual.size());
        for (int i = 0; i < expected.length; ++i) {
            assertEquals("Act " + i + " incorrect", expected[i], actual.get(i));
        }
    }

    /**
     * Helper to create an <em>act.customerCommunicationNote</em>.
     *
     * @param date     the record date
     * @param customer the customer
     * @param patient  the patient
     * @param reason   the reason. May be {@code null}
     * @return a new note
     */
    protected Act createCommunication(Date date, Party customer, Party patient, String reason) {
        Act note = create(CommunicationArchetypes.NOTE, Act.class);
        IMObjectBean bean = getBean(note);
        note.setActivityStartTime(date);
        note.setReason(reason);
        bean.setTarget("customer", customer);
        bean.setTarget("patient", patient);
        bean.save();
        return note;
    }
}
