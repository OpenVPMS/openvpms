/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.estimate;

import org.openvpms.archetype.test.builder.customer.account.TestEstimateItemVerifier;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.workspace.customer.charge.AbstractCustomerChargeActEditorTest;

import java.math.BigDecimal;

/**
 * Base class for estimate tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractEstimateEditorTestCase extends AbstractCustomerChargeActEditorTest {


    /**
     * Verifies an item's properties match that expected.
     *
     * @param item          the item to check
     * @param patient       the expected patient
     * @param product       the expected product
     * @param author        the expected author
     * @param lowQuantity   the expected low quantity
     * @param highQuantity  the expected high quantity
     * @param lowUnitPrice  the expected low unit price
     * @param highUnitPrice the expected high unit price
     * @param fixedPrice    the expected fixed price
     * @param lowDiscount   the expected low discount
     * @param highDiscount  the expected high discount
     * @param lowTotal      the expected low total
     * @param highTotal     the expected high total
     */
    protected void checkItem(Act item, Party patient, Product product, User author, BigDecimal lowQuantity,
                             BigDecimal highQuantity, BigDecimal lowUnitPrice, BigDecimal highUnitPrice,
                             BigDecimal fixedPrice, BigDecimal lowDiscount, BigDecimal highDiscount,
                             BigDecimal lowTotal, BigDecimal highTotal) {
        TestEstimateItemVerifier verifier = new TestEstimateItemVerifier(getArchetypeService());
        verifier.patient(patient).product(product).createdBy(author)
                .lowQuantity(lowQuantity).highQuantity(highQuantity)
                .fixedPrice(fixedPrice)
                .lowUnitPrice(lowUnitPrice).highUnitPrice(highUnitPrice)
                .lowDiscount(lowDiscount).highDiscount(highDiscount)
                .lowTotal(lowTotal).highTotal(highTotal)
                .verify(item);
    }
}
