/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.summary;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link CustomerPatientContext} class.
 *
 * @author Tim Anderson
 */
public class CustomerPatientContextTestCase extends ArchetypeServiceTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * Tests the {@link CustomerPatientContext}.
     */
    @Test
    public void testContext() {
        Context parent = new LocalContext();
        CustomerPatientContext context = new CustomerPatientContext(parent);

        assertNull(context.getCustomer());
        assertNull(context.getPatient());
        assertNull(context.getLocation());

        // verify customer and patient aren't inherited from the parent
        Party customer1 = customerFactory.createCustomer();
        Party patient1 = patientFactory.createPatient(customer1);
        parent.setCustomer(customer1);
        parent.setPatient(patient1);

        assertNull(context.getCustomer());
        assertNull(context.getPatient());
        assertNull(context.getObject(Context.CUSTOMER_SHORTNAME));
        assertNull(context.getObject(Context.PATIENT_SHORTNAME));
        assertNull(context.getObject(customer1.getObjectReference()));
        assertNull(context.getObject(patient1.getObjectReference()));

        // verify location is inherited
        Party location1 = practiceFactory.createLocation();
        Party location2 = practiceFactory.createLocation();
        parent.setLocation(location1);
        assertEquals(location1, context.getLocation());
        context.setLocation(location2);
        assertEquals(location2, context.getLocation());

        // verify the customer and patient are returned from the context
        Party customer2 = customerFactory.createCustomer();
        Party patient2 = patientFactory.createPatient(customer2);
        context.setCustomer(customer2);
        context.setPatient(patient2);

        assertEquals(customer2, context.getCustomer());
        assertEquals(patient2, context.getPatient());
        assertEquals(customer2, context.getObject(Context.CUSTOMER_SHORTNAME));
        assertEquals(patient2, context.getObject(Context.PATIENT_SHORTNAME));
        assertEquals(customer2, context.getObject(customer2.getObjectReference()));
        assertEquals(patient2, context.getObject(patient2.getObjectReference()));
    }
}
