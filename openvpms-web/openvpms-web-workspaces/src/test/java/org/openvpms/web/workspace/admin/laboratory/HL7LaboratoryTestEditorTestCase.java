/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.junit.Test;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.archetype.test.builder.user.TestUserFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditorTest;
import org.springframework.beans.factory.annotation.Autowired;

import static org.openvpms.web.component.im.edit.EditorTestHelper.assertInvalid;
import static org.openvpms.web.component.im.edit.EditorTestHelper.assertValid;

/**
 * Tests the {@link HL7LaboratoryTestEditor}.
 *
 * @author Tim Anderson
 */
public class HL7LaboratoryTestEditorTestCase extends AbstractIMObjectEditorTest<HL7LaboratoryTestEditor> {

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The user factory.
     */
    @Autowired
    private TestUserFactory userFactory;

    /**
     * Constructs a {@link HL7LaboratoryTestEditorTestCase}.
     */
    public HL7LaboratoryTestEditorTestCase() {
        super(HL7LaboratoryTestEditor.class, LaboratoryArchetypes.HL7_TEST);
    }

    /**
     * Verifies that the test must have an investigation type with an HL7 laboratory.
     */
    @Test
    public void testValidateInvestigationType() {
        Entity test = laboratoryFactory.newHL7Test()
                .code("AB1234")
                .build(false);

        // verify the investigation type is required
        HL7LaboratoryTestEditor editor = newEditor(test);
        editor.getComponent();
        assertInvalid(editor, "Investigation Type is required");

        // verify the investigation type must be associated with an HL7 laboratory
        Entity investigationType1 = laboratoryFactory.createInvestigationType();
        editor.setInvestigationType(investigationType1);
        assertInvalid(editor, investigationType1.getName() + " cannot be assigned to this Test. " +
                              "It is not managed by an HL7 Laboratory.");

        Entity laboratory2 = laboratoryFactory.createLaboratory();
        Entity investigationType2 = laboratoryFactory.createInvestigationType(laboratory2);
        editor.setInvestigationType(investigationType2);

        assertInvalid(editor, investigationType2.getName() + " cannot be assigned to this Test. " +
                              "It is not managed by an HL7 Laboratory.");

        // verify the test is valid if an investigation type linked to an HL7 laboratory is used
        Entity laboratory3 = laboratoryFactory.createHL7Laboratory(practiceFactory.createLocation(),
                                                                   userFactory.createUser());
        Entity investigationType3 = laboratoryFactory.createInvestigationType(laboratory3);
        editor.setInvestigationType(investigationType3);
        assertValid(editor);

        // verify the test is valid if an investigation type linked to an HL7 laboratory group is used
        Entity laboratory4 = laboratoryFactory.createHL7LaboratoryGroup(laboratory3);
        Entity investigationType4 = laboratoryFactory.createInvestigationType(laboratory4);
        editor.setInvestigationType(investigationType4);
        assertValid(editor);
    }
}
