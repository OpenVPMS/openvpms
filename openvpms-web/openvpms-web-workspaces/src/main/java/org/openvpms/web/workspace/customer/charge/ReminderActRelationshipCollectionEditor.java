/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;

/**
 * An editor for <em>actRelationship.invoiceItemReminder</em> collections.
 *
 * @author Tim Anderson
 */
class ReminderActRelationshipCollectionEditor extends ActRelationshipCollectionEditor {

    /**
     * Constructs a {@link ReminderActRelationshipCollectionEditor}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public ReminderActRelationshipCollectionEditor(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
        setExcludeDefaultValueObject(false);
    }

    /**
     * Determines if items can be added to the collection.
     *
     * @return {@code false} - reminders cannot be directly added by users
     */
    @Override
    protected boolean canAdd() {
        return false;
    }
}
