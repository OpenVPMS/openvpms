/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.payment;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.payment.CustomerPaymentEditor;
import org.openvpms.web.workspace.customer.payment.CustomerPaymentLayoutStrategy;
import org.openvpms.web.workspace.customer.payment.PaymentStatus;

/**
 * An {@link CustomerPaymentEditor} that allows the types of items on {@code POSTED} payments to be changed,
 * iff the till balance they are linked to has {@code UNCLEARED} status.
 *
 * @author Tim Anderson
 */
public class AdminCustomerPaymentEditor extends CustomerPaymentEditor {

    /**
     * Constructs a {@link AdminCustomerPaymentEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public AdminCustomerPaymentEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        if (act.isNew() || !ActStatus.POSTED.equals(act.getStatus()) || !act.isA(CustomerAccountArchetypes.PAYMENT)) {
            throw new IllegalArgumentException("Argument 'act' must be saved and have POSTED status");
        }
        setExpectedAmount(act.getTotal());
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        return new AdminCustomerPaymentEditor(reloadPayment(), getParent(), getLayoutContext());
    }

    /**
     * Save any edits.
     */
    @Override
    protected void doSave() {
        updateAudit();
        super.doSave();
    }

    /**
     * Creates a collection editor for the items collection.
     *
     * @param act   the act
     * @param items the items collection
     * @return a new collection editor
     */
    @Override
    protected ActRelationshipCollectionEditor createItemsEditor(Act act, CollectionProperty items) {
        return new AdminPaymentActRelationshipCollectionEditor(items, getObject(), getLayoutContext());
    }

    /**
     * Validates the object.
     * <p/>
     * This extends validation by ensuring that the payment amount matches the expected amount, if present.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateInvariants(validator) && validateTillBalance(validator);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new AdminLayoutStrategy(getItems(), getPaymentStatus());
    }

    /**
     * Returns the archetype service.
     * <p/>
     * This implementation suppresses firing of the archetypeService.save.act.customerAccountPaymentBefore.drl
     * and archetypeService.save.act.customerAccountPaymentAfter.drl rules which perform balance calculations.
     * <p/>
     * This is not strictly necessary but does remove redundant work.
     *
     * @return the archetype service
     */
    @Override
    protected IArchetypeService getService() {
        return ServiceHelper.getArchetypeService(false);
    }

    /**
     * Updates the audit message.
     */
    private void updateAudit() {
        String message = ((AdminPaymentActRelationshipCollectionEditor) getItems()).getAuditMessage();
        if (message != null) {
            Property audit = getProperty("audit");
            StringBuilder builder = new StringBuilder(audit.getString(""));
            if (builder.length() != 0) {
                builder.append('\n');
            }
            builder.append(message);
            audit.setValue(StringUtils.abbreviate(builder.toString(), audit.getMaxLength()));
        }
    }

    /**
     * Verifies that the status hasn't changed.
     *
     * @param validator the validator
     * @return {@code true} if the invariants haven't changed, otherwise {@code false}
     */
    private boolean validateInvariants(Validator validator) {
        boolean valid = false;
        if (!ActStatus.POSTED.equals(getStatus())) {
            validator.add(this, "Status must be POSTED");
        } else {
            valid = true;
        }
        return valid;
    }

    /**
     * Verifies that the till balance hasn't been cleared.
     *
     * @param validator the validator
     * @return {@code true} if the till balance hasn't been cleared
     */
    private boolean validateTillBalance(Validator validator) {
        boolean valid = false;
        if (getRules().hasClearedTillBalance(getObject())) {
            validator.add(this, Messages.get("customer.account.payment.clearedtill.message"));
        } else {
            valid = true;
        }
        return valid;
    }

    private static class AdminLayoutStrategy extends CustomerPaymentLayoutStrategy {

        /**
         * Constructs an {@link AdminLayoutStrategy}.
         *
         * @param editor the act items editor. May be {@code null}
         * @param status the status
         */
        public AdminLayoutStrategy(IMObjectCollectionEditor editor, PaymentStatus status) {
            super(editor, status);
        }

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            MutablePropertySet readOnly = new MutablePropertySet(properties);
            for (Property property : properties.getEditable()) {
                readOnly.setReadOnly(property.getName());
            }
            return super.apply(object, readOnly, parent, context);
        }
    }
}
