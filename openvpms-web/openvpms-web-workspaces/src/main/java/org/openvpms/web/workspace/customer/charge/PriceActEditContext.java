/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.rules.finance.tax.CustomerTaxRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductPriceRules;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.service.archetype.CachingReadOnlyArchetypeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.product.PricingContext;
import org.openvpms.web.component.im.product.PricingContextFactory;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.DoseManager;
import org.openvpms.web.workspace.customer.PriceActItemEditor;

import java.math.BigDecimal;


/**
 * Edit context for {@link PriceActItemEditor}s, to enable them to share state.
 *
 * @author Tim Anderson
 */
public class PriceActEditContext {

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The practice location.
     */
    private final Party location;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The tax rules.
     */
    private final CustomerTaxRules taxRules;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The discount rules.
     */
    private final DiscountRules discountRules;

    /**
     * Stock rules.
     */
    private final StockRules stockRules;

    /**
     * Determines if departments are being used.
     */
    private final boolean useDepartments;

    /**
     * Determines if discounting has been disabled.
     */
    private final boolean disableDiscounts;

    /**
     * Determines if products must be present at the location in order to select them.
     */
    private final boolean useLocationProducts;

    /**
     * Determines if minimum quantity restrictions are in place.
     */
    private final boolean useMinimumQuantities;

    /**
     * Determines if minimum quantities can be overridden by the user.
     */
    private final boolean overrideMinimumQuantity;

    /**
     * Determines if restricted drugs can be sold over the counter.
     */
    private final boolean sellRestrictedDrugsOTC;

    /**
     * The product pricer.
     */
    private final PricingContext pricingContext;

    /**
     * The dose manager. May be {@code null}
     */
    private DoseManager doseManager;

    /**
     * Constructs a {@link PriceActEditContext}.
     *
     * @param customer the customer
     * @param location the practice location. May be {@code null}
     * @param context  the layout context. The context must supply a practice
     */
    public PriceActEditContext(Party customer, Party location, LayoutContext context) {
        this.practice = context.getContext().getPractice();
        if (practice == null) {
            throw new IllegalStateException("Context is missing the practice");
        }
        this.location = location;
        service = new CachingReadOnlyArchetypeService(context.getCache(), ServiceHelper.getArchetypeService());
        PracticeRules practiceRules = ServiceHelper.getBean(PracticeRules.class);
        disableDiscounts = getDisableDiscounts(location);
        useLocationProducts = practiceRules.useLocationProducts(practice);
        IMObjectBean bean = service.getBean(practice);
        useMinimumQuantities = bean.getBoolean("minimumQuantities", false);
        if (useMinimumQuantities) {
            User user = context.getContext().getUser();
            String userType = bean.getString("minimumQuantitiesOverride");
            overrideMinimumQuantity = userType != null && ServiceHelper.getBean(UserRules.class).isA(user, userType);
        } else {
            overrideMinimumQuantity = false;
        }
        useDepartments = practiceRules.departmentsEnabled(practice);
        sellRestrictedDrugsOTC = bean.getBoolean("sellRestrictedDrugsOTC");
        accountRules = ServiceHelper.getBean(CustomerAccountRules.class);
        locationRules = ServiceHelper.getBean(LocationRules.class);

        // use the caching service for improved performance
        ProductPriceRules priceRules = new ProductPriceRules(service);
        taxRules = new CustomerTaxRules(practice, service);
        discountRules = new DiscountRules(service);
        stockRules = new StockRules(service);

        PricingContextFactory factory = ServiceHelper.getBean(PricingContextFactory.class);
        pricingContext = factory.createCustomerPricingContext(customer, practice, location, priceRules, taxRules);
    }

    /**
     * Sets the dose manager.
     *
     * @param doseManager the dose manager. May be {@code null}
     */
    public void setDoseManager(DoseManager doseManager) {
        this.doseManager = doseManager;
    }

    /**
     * Returns the dose manager.
     *
     * @return the dose manager
     */
    public DoseManager getDoseManager() {
        return doseManager;
    }

    /**
     * Returns the pricing context.
     *
     * @return the pricing context
     */
    public PricingContext getPricingContext() {
        return pricingContext;
    }

    /**
     * Returns the price of a product.
     * <p>
     * This:
     * <ul>
     * <li>applies any service ratio to the price</li>
     * <li>subtracts any tax exclusions the customer may have</li>
     * </ul>
     *
     * @param price        the price
     * @param serviceRatio the service ratio. May be {@code null}
     * @return the price, minus any tax exclusions
     */
    public BigDecimal getPrice(Product product, ProductPrice price, BigDecimal serviceRatio) {
        return pricingContext.getPrice(product, price, serviceRatio);
    }

    /**
     * Returns the dose of a product for a patient, based on the patient's weight.
     *
     * @param product the product
     * @param patient the patient
     * @return the dose, or {@code 0} if no dose exists for the patient weight or the {@link DoseManager} hasn't been
     * registered
     */
    public BigDecimal getDose(Product product, Party patient) {
        return doseManager != null ? doseManager.getDose(product, patient) : BigDecimal.ZERO;
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location. May be {@code null}
     */
    public Party getLocation() {
        return location;
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location. May be {@code null}
     */
    public Party getStockLocation() {
        return location != null ? locationRules.getDefaultStockLocation(location) : null;
    }

    /**
     * Returns the stock location associated with a product.
     *
     * @param product the product
     * @return the stock location. May be {@code null}
     */
    public Party getStockLocation(Product product) {
        return (location != null) ? stockRules.getStockLocation(product, location) : null;
    }

    /**
     * Returns a caching read-only archetype service, used to improve performance accessing common reference data.
     *
     * @return a caching archetype service
     */
    public IArchetypeService getCachingArchetypeService() {
        return service;
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     */
    public Party getPractice() {
        return practice;
    }

    /**
     * Returns the discount rules.
     *
     * @return rthe discount rules
     */
    public DiscountRules getDiscountRules() {
        return discountRules;
    }

    /**
     * Returns the tax rules.
     *
     * @return the tax rules
     */
    public CustomerTaxRules getTaxRules() {
        return taxRules;
    }

    /**
     * Returns the stock rules.
     *
     * @return the stock rules
     */
    public StockRules getStockRules() {
        return stockRules;
    }

    /**
     * Determines if departments are being used.
     *
     * @return {@code true} if departments are being used
     */
    public boolean useDepartments() {
        return useDepartments;
    }

    /**
     * Determines if discounts are disabled at the practice location.
     *
     * @return {@code true} if discounts are disabled at the practice location
     */
    public boolean disableDiscounts() {
        return disableDiscounts;
    }

    /**
     * Determines if products must be present at the location in order to select them.
     *
     * @return {@code true} if if products must be present at the location in order to select them
     */
    public boolean useLocationProducts() {
        return useLocationProducts;
    }

    /**
     * Determines if minimum quantity restrictions are in place.
     *
     * @return {@code true} if minimum quantity restrictions are in place
     */
    public boolean useMinimumQuantities() {
        return useMinimumQuantities;
    }

    /**
     * Determines if the user can override minimum quantities.
     *
     * @return {@code true} if the user can override minimum quantities
     */
    public boolean overrideMinimumQuantities() {
        return overrideMinimumQuantity;
    }

    /**
     * Determines if restricted products can be sold over the counter.
     *
     * @return {@code true} if restriced products can be sold, otherwise {@code false}
     */
    public boolean sellRestrictedDrugsOTC() {
        return sellRestrictedDrugsOTC;
    }

    /**
     * Calculates a total.
     *
     * @param fixedPrice the fixed price
     * @param unitPrice  the unit price
     * @param quantity   the quantity
     * @param discount   the discount
     * @return the total
     */
    public BigDecimal calculateTotal(BigDecimal fixedPrice, BigDecimal unitPrice, BigDecimal quantity,
                                     BigDecimal discount) {
        return accountRules.calculateTotal(fixedPrice, unitPrice, quantity, discount);
    }

    /**
     * Determines if discounts are disabled at a practice location.
     *
     * @param location the location. May be {@code null}
     * @return {@code true} if discounts are disabled
     */
    private boolean getDisableDiscounts(Party location) {
        boolean result = false;
        if (location != null) {
            IMObjectBean bean = service.getBean(location);
            result = bean.getBoolean("disableDiscounts");
        }
        return result;
    }

}
