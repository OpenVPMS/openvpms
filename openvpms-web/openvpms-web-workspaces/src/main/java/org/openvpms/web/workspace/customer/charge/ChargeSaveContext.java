/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.workspace.patient.mr.PatientDocumentActEditor;

import java.util.ArrayList;
import java.util.List;

/**
 * Charge context, used to defer manipulation of relationships to patient history until the charge items are saved.
 * <p>
 * At save, the approach is:
 * <ol>
 * <li>save charge</li>
 * <li>save charge items</li>
 * <li>save charge item relationships to events. This may have updated the charge items again, so these are re
 * -saved</li>
 * <li>delete any investigations, reminders, documents</li>
 * </ol>
 *
 * @author Tim Anderson
 */
public class ChargeSaveContext implements CollectionPropertyEditor.RemoveHandler {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The patient history changes. These exist only as long as a save is in progress.
     */
    private PatientHistoryChanges changes;

    /**
     * The objects to remove, excluding documents.
     */
    private final List<IMObject> toRemove = new ArrayList<>();

    /**
     * Patient documents to remove.
     */
    private final List<DocumentAct> documentsToRemove = new ArrayList<>();

    /**
     * The editors to remove.
     */
    private final List<IMObjectEditor> toRemoveEditors = new ArrayList<>();

    /**
     * Constructs a {@link ChargeSaveContext}.
     *
     * @param service the archetype service
     * @param context the layout context
     */
    ChargeSaveContext(IArchetypeService service, LayoutContext context) {
        this.service = service;
        this.context = context;
    }

    /**
     * Registers the patient history changes for the current save.
     *
     * @param changes the changes. May be {@code null}
     */
    public void setHistoryChanges(PatientHistoryChanges changes) {
        this.changes = changes;
    }

    /**
     * Returns the patient history changes for the current save.
     *
     * @return the changes. May be {@code null}
     */
    public PatientHistoryChanges getHistoryChanges() {
        return changes;
    }

    /**
     * Invoked to remove an object.
     * <p>
     * Removal is deferred until {@link #save()} is invoked.
     *
     * @param object the object to remove
     */
    @Override
    public void remove(IMObject object) {
        if (object.isA("act.patientDocument*")) {
            documentsToRemove.add((DocumentAct) object);
        } else {
            toRemove.add(object);
        }
    }

    /**
     * Invoked to remove an object.
     * <p>
     * Removal is deferred until {@link #save()} is invoked.
     *
     * @param editor the object editor
     */
    @Override
    public void remove(IMObjectEditor editor) {
        toRemoveEditors.add(editor);
    }

    /**
     * Saves changes.
     *
     * @throws OpenVPMSException if the save fails
     */
    public void save() {
        for (DocumentAct document : documentsToRemove) {
            // unlink the document from any event
            changes.removeRelationship(document);
        }
        for (IMObjectEditor editor : toRemoveEditors) {
            // remove event relationships for any document editors
            IMObject object = editor.getObject();
            if (editor instanceof DocumentAct) {
                changes.removeRelationship((DocumentAct) object);
            }
        }

        changes.save();

        for (IMObject object : toRemove.toArray(new IMObject[0])) {
            service.remove(object);
            toRemove.remove(object);
        }

        for (DocumentAct document : documentsToRemove.toArray(new DocumentAct[0])) {
            if (!document.isNew()) {
                // remove documents using an editor to ensure complete deletion
                PatientDocumentActEditor editor = new PatientDocumentActEditor(document, null, context);
                editor.delete();
            }
            documentsToRemove.remove(document);
        }
        for (IMObjectEditor editor : toRemoveEditors.toArray(new IMObjectEditor[0])) {
            editor.delete();
            toRemoveEditors.remove(editor);
        }
    }
}
