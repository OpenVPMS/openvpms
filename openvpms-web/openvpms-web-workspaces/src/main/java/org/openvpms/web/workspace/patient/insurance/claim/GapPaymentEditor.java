/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.workspace.customer.credit.GapPaymentTillReferenceEditor;
import org.openvpms.web.workspace.customer.payment.CustomerPaymentEditor;
import org.openvpms.web.workspace.customer.payment.CustomerPaymentLayoutStrategy;
import org.openvpms.web.workspace.customer.payment.PaymentStatus;

import java.math.BigDecimal;
import java.util.List;

/**
 * Claim payment or refund editor.
 * <p/>
 * This prompts for a till to record the gap benefit payment if a gap benefit till is not configured.
 * and a gap payment is being made. This will also occur where a refund is being made in order to refund
 * an overpayment when making a gap claim.
 *
 * @author Tim Anderson
 */
public class GapPaymentEditor extends CustomerPaymentEditor {

    /**
     * The location of the gap claim.
     */
    private final Party gapLocation;

    /**
     * Determines if the till required.
     */
    private final boolean tillRequired;

    /**
     * The gap payment till.
     */
    private Entity gapTill;

    /**
     * The till editor, if a till is required, and none has been provided.
     */
    private GapPaymentTillReferenceEditor tillEditor;


    /**
     * Constructs a {@link GapPaymentEditor}.
     *
     * @param act          the act to edit
     * @param parent       the parent object. May be {@code null}
     * @param context      the layout context
     * @param invoice      the invoice amount
     * @param gapLocation  the location of the corresponding gap claim
     * @param gapTill      the gap payment till. May be {@code null}
     * @param tillRequired determines if the till is required
     */
    GapPaymentEditor(FinancialAct act, IMObject parent, LayoutContext context, BigDecimal invoice, Party gapLocation,
                     Entity gapTill, boolean tillRequired) {
        super(act, parent, context, invoice);
        this.gapLocation = gapLocation;
        this.gapTill = gapTill;
        this.tillRequired = tillRequired;
        act.setStatus(ActStatus.POSTED);
        setExpectedAmount(invoice);
        if (gapTill == null && tillRequired) {
            // only prompt for a till for gap benefit amount payments if one hasn't been specified
            tillEditor = new GapPaymentTillReferenceEditor(gapLocation, context);
            tillEditor.addModifiableListener(modifiable -> GapPaymentEditor.this.gapTill = tillEditor.getObject());
            addEditor(tillEditor);
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        return new GapPaymentEditor(reloadPayment(), getParent(), getLayoutContext(), getInvoiceAmount(), gapLocation,
                                    getGapPaymentTill(), tillRequired);
    }

    /**
     * Returns the till to record gap benefit payments against.
     *
     * @return the gap payment till. Non-null if the till is required and the payment is valid
     */
    public Entity getGapPaymentTill() {
        return gapTill;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new PaymentLayoutStrategy(getItems(), getPaymentStatus());
    }

    void setGapPaymentTill(Entity gapTill) {
        tillEditor.setObject(gapTill);
    }

    private class PaymentLayoutStrategy extends CustomerPaymentLayoutStrategy {
        /**
         * Constructs a {@link PaymentLayoutStrategy}.
         *
         * @param editor the act items editor
         * @param status the status editor
         */
        PaymentLayoutStrategy(IMObjectCollectionEditor editor, PaymentStatus status) {
            super(editor, status);
            // don't allow editing of the status
            getArchetypeNodes().exclude("status");
        }

        /**
         * Lays out components in a grid.
         *
         * @param object     the object to lay out
         * @param properties the properties
         * @param context    the layout context
         */
        @Override
        protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
            ComponentGrid grid = super.createGrid(object, properties, context);
            if (tillEditor != null) {
                grid.add(new ComponentState(tillEditor));
            }
            return grid;
        }
    }
}
