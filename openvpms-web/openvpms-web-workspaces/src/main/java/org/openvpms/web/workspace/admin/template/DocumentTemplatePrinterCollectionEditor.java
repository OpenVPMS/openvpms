/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.MultipleEntityRelationshipCollectionEditor;
import org.openvpms.web.component.im.relationship.RelationshipState;
import org.openvpms.web.component.im.relationship.RelationshipStateTableModel;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.print.PrinterViewer;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;

/**
 * Editor for collections of <em>entityRelationship.documentTemplatePrinter</em> instances.
 *
 * @author Tim Anderson
 */
public class DocumentTemplatePrinterCollectionEditor extends MultipleEntityRelationshipCollectionEditor {

    /**
     * Constructs a {@link MultipleEntityRelationshipCollectionEditor}.
     *
     * @param property the collection property
     * @param object   the object being edited
     * @param context  the layout context
     */
    public DocumentTemplatePrinterCollectionEditor(CollectionProperty property, Entity object, LayoutContext context) {
        super(property, object, context);
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<RelationshipState> createTableModel(LayoutContext context) {
        if (!(context.getComponentFactory() instanceof TableComponentFactory)) {
            context = new DefaultLayoutContext(context);
            context.setComponentFactory(new TableComponentFactory(context));
        }
        boolean displayTarget = getObject().isA(DocumentArchetypes.DOCUMENT_TEMPLATE);

        return new DocumentTemplatePrinterTableModel(context, displayTarget);
    }

    private static class DocumentTemplatePrinterTableModel extends RelationshipStateTableModel {

        /**
         * Constructs a {@link DocumentTemplatePrinterTableModel}.
         *
         * @param context       layout context
         * @param displayTarget if {@code true} display the relationship target, otherwise display the source
         */
        public DocumentTemplatePrinterTableModel(LayoutContext context, boolean displayTarget) {
            super(context, displayTarget);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the table column
         * @param row    the table row
         */
        @Override
        protected Object getValue(RelationshipState object, TableColumn column, int row) {
            Object result;
            if (column instanceof DescriptorTableColumn) {
                Relationship relationship = object.getRelationship();
                DescriptorTableColumn descriptorTableColumn = (DescriptorTableColumn) column;
                if (descriptorTableColumn.getName().equals("printer")) {
                    PrinterViewer printerViewer
                            = new PrinterViewer((String) descriptorTableColumn.getValue(relationship), getContext());
                    result = printerViewer.getComponent();
                } else {
                    result = descriptorTableColumn.getComponent(relationship, getContext());
                }
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }

        /**
         * Creates a new column model.
         *
         * @return a new column model
         */
        @Override
        protected TableColumnModel createTableColumnModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(NAME_INDEX, "table.imobject.name"));
            ArchetypeDescriptor archetype = ServiceHelper.getArchetypeService().getArchetypeDescriptor(
                    DocumentArchetypes.DOCUMENT_TEMPLATE_PRINTER);
            int index = ACTIVE_INDEX + 1;
            model.addColumn(new DescriptorTableColumn(index++, "printer", archetype));
            model.addColumn(new DescriptorTableColumn(index++, "paperTray", archetype));
            model.addColumn(new DescriptorTableColumn(index++, "sides", archetype));
            model.addColumn(new DescriptorTableColumn(index, "interactive", archetype));
            return model;
        }
    }
}
