/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge.department;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.edit.ComboBoxIMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;

import java.util.Collections;
import java.util.List;

/**
 * Editor for <em>entity.department</em> references.
 * <p>
 * This adds the selected department to the context.
 *
 * @author Tim Anderson
 */
public class DepartmentReferenceEditor extends ComboBoxIMObjectReferenceEditor<Entity> {

    /**
     * The available departments.
     */
    private List<Entity> departments;

    /**
     * Constructs a {@link DepartmentReferenceEditor}.
     *
     * @param property the reference property
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context
     */
    public DepartmentReferenceEditor(Property property, IMObject parent, LayoutContext context) {
        super(property, context);
    }

    /**
     * Returns the default objects to select from.
     * <p/>
     * This implementation returns those available to the current user.
     *
     * @return the default objects to select from
     */
    @Override
    protected List<Entity> getObjects() {
        if (departments == null) {
            UserRules rules = ServiceHelper.getBean(UserRules.class);
            User user = getContext().getContext().getUser();
            departments = user != null ? rules.getDepartments(user) : Collections.emptyList();
        }
        return departments;
    }

    /**
     * Updates the underlying property with the specified value.
     *
     * @param value the value to update with. May be {@code null}
     * @return {@code true} if the property was modified
     */
    @Override
    protected boolean updateProperty(Entity value) {
        boolean result = super.updateProperty(value);
        updateContext(value);
        return result;
    }

    /**
     * Updates the context with the department.
     *
     * @param department the department. May be {@code null}
     */
    private void updateContext(Entity department) {
        if (department == null || (department.isActive() && getObjects().contains(department))) {
            // don't propagate inactive departments or those not accessible to the current user to the context
            getContext().getContext().setDepartment(department);
        }
    }
}
