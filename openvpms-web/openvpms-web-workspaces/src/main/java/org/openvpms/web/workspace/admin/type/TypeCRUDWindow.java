/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.type;

import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.component.business.service.archetype.helper.IMObjectGraph;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * CRUD window for type archetypes.
 *
 * @author Tim Anderson
 */
class TypeCRUDWindow extends ResultSetCRUDWindow<Entity> {

    /**
     * Copy button identifier.
     */
    private static final String COPY_ID = "button.copy";

    /**
     * Constructs a {@link TypeCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param query      the query. May be {@code null}
     * @param set        the result set. May be {@code null}
     * @param context    the context
     * @param help       the help context
     */
    public TypeCRUDWindow(Archetypes<Entity> archetypes, Query<Entity> query, ResultSet<Entity> set, Context context,
                          HelpContext help) {
        super(archetypes, query, set, context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(COPY_ID, action(ReminderArchetypes.REMINDER_TYPE, this::copyReminderType, null));
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(COPY_ID, enable && TypeHelper.isA(getObject(), ReminderArchetypes.REMINDER_TYPE));
    }

    /**
     * Copy a reminder type, and display the copy in an editor.
     *
     * @param reminderType the reminder type to copy
     */
    private void copyReminderType(Entity reminderType) {
        ReminderRules rules = ServiceHelper.getBean(ReminderRules.class);
        IMObjectGraph graph = rules.copyReminderType(reminderType);
        HelpContext edit = createEditTopic(reminderType);
        LayoutContext context = createLayoutContext(edit);
        graph.getPrimary().setName(Messages.format("imobject.copyof", reminderType.getName()));
        ReminderTypeEditor editor = new ReminderTypeEditor(graph, context);
        edit(editor);
    }
}