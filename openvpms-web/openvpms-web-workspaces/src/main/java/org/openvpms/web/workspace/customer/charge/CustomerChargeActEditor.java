/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.invoice.ChargeItemEventLinker;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.im.act.ActHelper;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditorFactory;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.FinancialActEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.charge.TemplateChargeItems;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An editor for {@link Act}s which have an archetype of
 * <em>act.customerAccountChargesInvoice</em>,
 * <em>act.customerAccountChargesCredit</em>
 * or <em>act.customerAccountChargesCounter</em>.
 *
 * @author Tim Anderson
 */
public abstract class CustomerChargeActEditor extends FinancialActEditor {

    /**
     * The reminder rules.
     */
    private final ReminderRules reminderRules;

    /**
     * Document manager.
     */
    private final ChargeDocumentManager documentManager;

    /**
     * Determines if a default item should be added if no items are present.
     */
    private boolean addDefaultItem;

    /**
     * The customer notes editor.
     */
    private ActRelationshipCollectionEditor customerNotes;

    /**
     * The documents editor.
     */
    private ActRelationshipCollectionEditor documents;

    /**
     * The pharmacy order placer, used to place orders when invoicing.
     */
    private OrderPlacer orderPlacer;

    /**
     * The investigation submitter.
     */
    private InvestigationSubmitter investigationSubmitter;


    /**
     * Constructs an {@link CustomerChargeActEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public CustomerChargeActEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        this(act, parent, context, true);
    }

    /**
     * Constructs an {@code CustomerChargeActEditor}.
     *
     * @param act            the act to edit
     * @param parent         the parent object. May be {@code null}
     * @param context        the layout context
     * @param addDefaultItem if {@code true} add a default item if the act has none
     */
    public CustomerChargeActEditor(FinancialAct act, IMObject parent, LayoutContext context, boolean addDefaultItem) {
        super(act, parent, context);
        Party location = initLocation();
        Party customer = context.getContext().getCustomer();
        initParticipant("customer", customer);
        this.addDefaultItem = addDefaultItem;
        reminderRules = ServiceHelper.getBean(ReminderRules.class);
        initialise();
        if (act.isA(CustomerAccountArchetypes.INVOICE)) {
            ChargeItemRelationshipCollectionEditor items = getItems();
            items.setTemplateProductListener(this::templateProductExpanded);

            orderPlacer = createOrderPlacer(customer, context.getContext().getPractice(), location,
                                            context.getContext().getUser());
            List<Act> acts = getOrderActs(false);
            orderPlacer.initialise(acts);
            InvestigationManager manager = items.getEditContext().getInvestigations();
            investigationSubmitter = new InvestigationSubmitter(manager, context);
        }
        documentManager = new ChargeDocumentManager(this, context);
    }

    /**
     * Determines if a default item should be added if the charge doesn't have one.
     * <p/>
     * This only applies prior to the creation of the component. After that, it is ignored.
     *
     * @param addDefaultItem if {@code true} add a default item if the charge has none
     */
    public void setAddDefaultItem(boolean addDefaultItem) {
        this.addDefaultItem = addDefaultItem;
    }

    /**
     * Registers a listener that is invoked when the user adds an item.
     * <p/>
     * Note that this is not invoked for template expansion.
     *
     * @param listener the listener to invoke. May be {@code null}
     */
    public void setAddItemListener(Runnable listener) {
        getItems().setAddItemListener(listener);
    }

    /**
     * Returns the customer associated with the charge.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        return (Party) getParticipant("customer");
    }

    /**
     * Returns the location associated with the charge.
     *
     * @return the location. May be {@code null}
     */
    public Party getLocation() {
        return (Party) getParticipant("location");
    }

    /**
     * Returns the current department.
     *
     * @return the current department. May be {@code null}
     */
    public Entity getDepartment() {
        return getLayoutContext().getContext().getDepartment();
    }

    /**
     * Returns the current stock location.
     *
     * @return the current stock location. May be {@code null}
     */
    public Party getStockLocation() {
        return getLayoutContext().getContext().getStockLocation();
    }

    /**
     * Returns the items collection editor.
     *
     * @return the items collection editor. May be {@code null}
     */
    @Override
    public ChargeItemRelationshipCollectionEditor getItems() {
        return (ChargeItemRelationshipCollectionEditor) super.getItems();
    }

    /**
     * Returns the editor queue.
     *
     * @return the editor queue
     */
    public EditorQueue getEditorQueue() {
        return getItems().getEditorQueue();
    }

    /**
     * Returns the customer notes collection editor.
     *
     * @return the customer notes collection editor. May be {@code null}
     */
    public ActRelationshipCollectionEditor getCustomerNotes() {
        if (customerNotes == null) {
            CollectionProperty notes = (CollectionProperty) getProperty("customerNotes");
            if (notes != null && !notes.isHidden()) {
                customerNotes = createCustomerNotesEditor(getObject(), notes);
                addEditor(customerNotes);
            }
        }
        return customerNotes;
    }

    /**
     * Returns the document collection editor.
     *
     * @return the document collection editor. May be {@code null}
     */
    public ActRelationshipCollectionEditor getDocuments() {
        if (documents == null) {
            CollectionProperty notes = (CollectionProperty) getProperty("documents");
            if (notes != null && !notes.isHidden()) {
                documents = createDocumentsEditor(getObject(), notes);
                addEditor(documents);
            }
        }
        return documents;
    }

    /**
     * Adds a new charge item, returning its editor.
     *
     * @return the charge item editor, or {@code null} if an item couldn't be created
     */
    public CustomerChargeActItemEditor addItem() {
        ActRelationshipCollectionEditor items = getItems();
        CustomerChargeActItemEditor result = (CustomerChargeActItemEditor) items.add();
        if (result == null) {
            // the existing editor is invalid, preventing a new item being added, so force creation of the editor.
            // Note that this won't be made the current editor
            IMObject object = items.create();
            if (object != null) {
                result = (CustomerChargeActItemEditor) items.getEditor(object);
                items.addEdited(result);
            }
        }
        if (result != null && items.getCurrentEditor() == result) {
            // set the default focus to that of the item editor
            getFocusGroup().setDefault(result.getFocusGroup().getDefaultFocus());
        }
        return result;
    }

    /**
     * Removes an item.
     *
     * @param item the item to remove
     */
    public void removeItem(Act item) {
        getItems().remove(item);

        // if the item wasn't committed, then removal doesn't trigger onItemsChanged(), so do it manually.
        onItemsChanged();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician. May be {@code null}
     */
    public void setClinician(User clinician) {
        setParticipant("clinician", clinician);
    }

    /**
     * Flags an invoice item as being ordered via a pharmacy/laboratory.
     * <p/>
     * This suppresses it from being ordered again when the invoice is saved and updates the display.
     *
     * @param item the invoice item
     */
    public void setOrdered(Act item) {
        if (orderPlacer != null) {
            orderPlacer.initialise(item);
        }
        if (getItems().hasEditor(item)) {
            CustomerChargeActItemEditor editor = getItems().getEditor(item);
            editor.ordered();
        }
    }

    /**
     * Returns invoice items that have been ordered via a pharmacy but have not been dispensed or have been partially
     * dispensed.
     *
     * @return invoice items that have
     */
    public List<Act> getNonDispensedItems() {
        List<Act> result = new ArrayList<>();
        if (orderPlacer != null) {
            for (Act item : getItems().getCurrentActs()) {
                CustomerChargeActItemEditor editor = getItems().getEditor(item);
                if (editor.isOrdered() || orderPlacer.isPharmacyProduct(editor.getProduct())) {
                    BigDecimal quantity = editor.getQuantity();
                    BigDecimal received = editor.getReceivedQuantity();
                    if (!MathRules.equals(quantity, received)) {
                        result.add(item);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Queues a popup dialog to display after any other dialog queued by the editor.
     *
     * @param dialog the dialog to display
     */
    public void queue(PopupDialog dialog) {
        getItems().getEditorQueue().queue(null, dialog);
    }

    /**
     * Returns the unprinted documents.
     *
     * @return the unprinted documents
     */
    public ChargeDocumentManager getDocumentManager() {
        return documentManager;
    }

    /**
     * Queues tasks that must be performed after save.
     *
     * @param queue     the queue
     * @param close     if {@code true}, the editor will close on completion, unless cancelled
     * @param cancelled used to indicate if a task was cancelled. if set, this will prevent the editor from closing
     */
    public void postSave(EditorQueue queue, boolean close, MutableBoolean cancelled) {
        if (close) {
            if (investigationSubmitter != null && investigationSubmitter.canSubmit()) {
                // need to queue orders before anything else
                InvestigationSubmitter.Listener listener = new InvestigationSubmitter.Listener() {
                    @Override
                    public void submit(List<DocumentAct> investigations) {
                        submitOrders(investigations);
                        investigationSubmitter.confirm(investigations, queue);
                        queueTasks(queue);
                    }

                    @Override
                    public void skip() {
                        submitOrdersExcludingInteractiveInvestigations();
                        queueTasks(queue);
                    }

                    @Override
                    public void cancel() {
                        cancelled.setTrue();
                    }
                };
                investigationSubmitter.select(queue, listener);
            } else {
                submitOrdersExcludingInteractiveInvestigations();
                queueTasks(queue);
            }
        } else {
            submitOrdersExcludingInteractiveInvestigations();
            queueTasks(queue);
        }
    }

    /**
     * Submits investigations selected by the user.
     */
    public void submitInvestigations() {
        if (investigationSubmitter != null) {
            if (isModified()) {
                throw new IllegalStateException("Can only submit investigations after changes have been saved");
            }
            if (investigationSubmitter.canSubmit()) {
                investigationSubmitter.select(investigations -> {
                    submitOrders(investigations);
                    EditorQueue editorQueue = getEditorQueue();
                    investigationSubmitter.confirm(investigations, editorQueue);
                });
            } else {
                InformationDialog.show(Messages.get("customer.charge.investigation.submit.title"),
                                       Messages.get("customer.charge.investigation.submit.none"));
            }
        }
    }

    /**
     * Registers a listener to be notified of alerts.
     *
     * @param listener the listener. May be {@code null}
     */
    @Override
    public void setAlertListener(AlertListener listener) {
        super.setAlertListener(listener);
        if (getObject().isA(CustomerAccountArchetypes.INVOICE)) {
            ChargeItemRelationshipCollectionEditor items = getItems();
            InvestigationManager manager = items.getEditContext().getInvestigations();
            manager.setAlertListener(listener);
        }
    }

    /**
     * Validates the object.
     * <p/>
     * This extends validation by ensuring that the total matches that of the sum of the item totals.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateInvestigations(validator);
    }

    /**
     * Queues activities after save.
     *
     * @param queue the queue
     */
    protected void queueTasks(EditorQueue queue) {
        if (documentManager != null) {
            documentManager.process(queue);
        }
    }

    /**
     * Initialises the practice location.
     * <p/>
     * This populates the charge with the location if it is unset, as the location determines pricing.
     * <p/>
     * The context is updated with the charge's location and the stock location as this determines which products
     * are available.
     *
     * @return the practice location for the charge
     */
    protected Party initLocation() {
        Context context = getLayoutContext().getContext();
        Party location = getLocation();
        if (location == null) {
            location = context.getLocation();
            initParticipant("location", location);
        }
        context.setLocation(location);
        Party stockLocation = null;
        if (location != null) {
            LocationRules rules = ServiceHelper.getBean(LocationRules.class);
            stockLocation = rules.getDefaultStockLocation(location);
        }
        context.setStockLocation(stockLocation);
        return location;
    }

    /**
     * Save any edits.
     * <p/>
     * For invoices, this links items to their corresponding clinical events, creating events as required, and marks
     * matching reminders completed.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        ChargeSaveContext chargeContext = null;
        try {
            ChargeItemRelationshipCollectionEditor items = getItems();
            List<Act> reminders = getNewReminders();
            List<Act> alerts = getNewAlerts();

            boolean invoice = TypeHelper.isA(getObject(), CustomerAccountArchetypes.INVOICE);
            PatientHistoryChanges changes = new PatientHistoryChanges(
                    getLayoutContext().getContext().getLocation(), ServiceHelper.getArchetypeService());
            if (invoice) {
                // cancel any orders associated with deleted invoice items prior to physical deletion
                Set<Act> updated = orderPlacer.cancelDeleted(getOrderActs(false), changes);
                if (!updated.isEmpty()) {
                    // need to save updated items before performing deletion
                    ServiceHelper.getArchetypeService().save(updated);
                }
            }

            chargeContext = items.getSaveContext();
            chargeContext.setHistoryChanges(changes);

            boolean posted = isPosted();
            if (posted) {
                // set the endTime (aka Completed Date)
                setEndTime(new Date(), true);
            }

            super.doSave();

            if (invoice) {
                // link the items to their corresponding clinical events
                linkToEvents(changes);
                if (posted) {
                    changes.complete(getEndTime());
                }
            }
            chargeContext.save();

            // mark reminders that match the new reminders completed
            if (!reminders.isEmpty()) {
                reminderRules.markMatchingRemindersCompleted(reminders);
            }

            // mark alerts that match the new alerts completed
            if (!alerts.isEmpty()) {
                reminderRules.markMatchingAlertsCompleted(alerts);
            }

            if (invoice) {
                // submit any orders (excluding PENDING laboratory orders), and discontinue
                // finalised orders if the invoice is POSTED.
                submitOrders(getOrderActs(true), changes, true);
            }
        } finally {
            if (chargeContext != null) {
                chargeContext.setHistoryChanges(null);  // clear the history changes
            }
        }
    }

    /**
     * Deletes the object.
     * <p/>
     * This uses {@link #deleteChildren()} to delete the children prior to invoking {@link #deleteObject()}.
     *
     * @throws OpenVPMSException     if the delete fails
     * @throws IllegalStateException if the act is POSTED
     */
    @Override
    protected void doDelete() {
        if (orderPlacer != null) {
            orderPlacer.cancel();
        }
        super.doDelete();
    }

    /**
     * Links the charge items to their corresponding clinical events.
     *
     * @param changes the patient history changes
     */
    protected void linkToEvents(PatientHistoryChanges changes) {
        List<Act> chargeItems = getItems().getActs();
        ChargeItemEventLinker linker = new ChargeItemEventLinker(ServiceHelper.getArchetypeService());
        Map<FinancialAct, List<Act>> items = getItemsToLinkToEvents(chargeItems);
        linker.prepare(items, changes);

        addTemplateNotes(linker, changes);
    }

    /**
     * Returns a map of charge items to all of their acts that can be linked to events.
     * <p/>
     * This excludes items with negative quantities.
     *
     * @param chargeItems the charge items
     * @return a map of charge items to event items
     */
    protected Map<FinancialAct, List<Act>> getItemsToLinkToEvents(List<Act> chargeItems) {
        Map<FinancialAct, List<Act>> items = new LinkedHashMap<>();
        for (Act act : chargeItems) {
            FinancialAct financialAct = (FinancialAct) act;
            if (financialAct.getQuantity().compareTo(BigDecimal.ZERO) >= 0) {
                List<Act> acts = new ArrayList<>();
                CustomerChargeActItemEditor editor = getItems().getEditor(act);
                Act medication = editor.getMedication();
                if (medication != null) {
                    acts.add(medication);
                }
                acts.addAll(editor.getInvestigations());
                acts.addAll(editor.getDocuments());
                items.put(financialAct, acts);
            }
        }
        return items;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = new ChargeLayoutStrategy(getItems());
        initLayoutStrategy(strategy);
        return strategy;
    }

    /**
     * Initialises a layout strategy with the customerNotes and documents collections, if they are present.
     *
     * @param strategy the layout strategy to initialise
     */
    protected void initLayoutStrategy(IMObjectLayoutStrategy strategy) {
        ActRelationshipCollectionEditor notes = getCustomerNotes();
        ActRelationshipCollectionEditor documents = getDocuments();
        if (notes != null) {
            strategy.addComponent(new ComponentState(notes));
        }
        if (documents != null) {
            strategy.addComponent(new ComponentState(documents));
        }
    }

    /**
     * Invoked when layout has completed.
     * <p/>
     * This invokes {@link #initItems()}.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        initItems();
    }

    /**
     * Updates the amount and tax when an act item changes.
     */
    @Override
    protected void onItemsChanged() {
        super.onItemsChanged();
        calculateCosts();
    }

    /**
     * Creates a collection editor for the customer notes collection.
     *
     * @param act   the act
     * @param notes the customer notes collection
     * @return a new collection editor
     */
    protected ActRelationshipCollectionEditor createCustomerNotesEditor(Act act, CollectionProperty notes) {
        return (ActRelationshipCollectionEditor) IMObjectCollectionEditorFactory.create(notes, act, getLayoutContext());
    }

    /**
     * Creates a collection editor for the documents collection.
     *
     * @param act       the act
     * @param documents the documents collection
     * @return a new collection editor
     */
    protected ActRelationshipCollectionEditor createDocumentsEditor(Act act, CollectionProperty documents) {
        return (ActRelationshipCollectionEditor) IMObjectCollectionEditorFactory.create(documents, act,
                                                                                        getLayoutContext());
    }

    /**
     * Creates a new {@link OrderPlacer}.
     *
     * @param customer the customer
     * @param practice the practice
     * @param location the practice location
     * @param user     the user responsible for the orders
     * @return a new pharmacy order placer
     */
    protected OrderPlacer createOrderPlacer(Party customer, Party practice, Party location, User user) {
        OrderServices services = ServiceHelper.getBean(OrderServices.class);
        return new OrderPlacer(customer, location, user, practice, getLayoutContext().getCache(), services,
                               ServiceHelper.getArchetypeService());
    }

    /**
     * Determines if a default item should be added if no items are present.
     *
     * @return {@code true} if a default item should be added if no items are present
     */
    protected boolean getAddDefaultIem() {
        return addDefaultItem;
    }

    /**
     * Creates <em>act.patientClinicalNote</em> acts for any notes associated with template products, linking them to
     * the event.
     *
     * @param linker  the event linker
     * @param changes the patient history changes
     */
    protected void addTemplateNotes(ChargeItemEventLinker linker, PatientHistoryChanges changes) {
        List<TemplateChargeItems> templates = getItems().getTemplates();
        if (!templates.isEmpty()) {
            List<Act> items = getItems().getActs();
            List<Act> notes = new ArrayList<>();
            MedicalRecordRules rules = ServiceHelper.getBean(MedicalRecordRules.class);
            for (TemplateChargeItems template : templates) {
                Act item = template.findFirst(items);
                if (item != null) {
                    String visitNote = template.getVisitNote();
                    if (!StringUtils.isEmpty(visitNote)) {
                        IMObjectBean bean = getBean(item);
                        Party patient = (Party) getObject(bean.getTargetRef("patient"));
                        if (patient != null) {
                            Date itemStartTime = bean.getDate("startTime");
                            Date startTime = getStartTime();
                            if (DateRules.getDate(itemStartTime).compareTo(DateRules.getDate(startTime)) != 0) {
                                // use the item start time if its date is different to that of the invoice
                                startTime = itemStartTime;
                            }
                            User clinician = (User) getObject(bean.getTargetRef("clinician"));
                            Act note = rules.createNote(startTime, patient, visitNote, clinician);
                            notes.add(note);
                        }
                    }
                }
            }
            if (!notes.isEmpty()) {
                ServiceHelper.getArchetypeService().save(notes);
                linker.prepareNotes(notes, changes);
            }
            getItems().clearTemplates();
        }
    }

    /**
     * Ensures that there are no unsubmitted investigations if the charge is POSTED.
     *
     * @param validator the validator
     * @return {@code true} if valid, otherwise {@code false}
     */
    private boolean validateInvestigations(Validator validator) {
        boolean valid = true;
        if (isPosted() && investigationSubmitter != null) {
            if (investigationSubmitter.canSubmit()) {
                validator.add(this, new ValidatorError(Messages.get("customer.charge.investigation.unsubmitted")));
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Determines if the charge is posted and therefore not user editable
     *
     * @return {@code true} if the charge is posted
     */
    private boolean isPosted() {
        return ActStatus.POSTED.equals(getStatus());
    }

    /**
     * Submits orders excluding any pending or unconfirmed investigations that require user input.
     */
    private void submitOrdersExcludingInteractiveInvestigations() {
        InvestigationManager manager = getInvestigations();
        List<DocumentAct> unconfirmed = manager.getUnconfirmedLaboratoryInvestigations();
        List<DocumentAct> investigations = manager.getInvestigations();
        investigations.removeAll(unconfirmed);
        submitOrders(investigations);
    }

    private InvestigationManager getInvestigations() {
        return getItems().getEditContext().getInvestigations();
    }

    /**
     * Submits orders.
     *
     * @param investigations the investigations to include in the submission
     */
    private void submitOrders(List<DocumentAct> investigations) {
        if (getObject().isA(CustomerAccountArchetypes.INVOICE)) {
            if (isModified()) {
                throw new IllegalStateException("Can only submit orders after changes have been saved");
            }
            PatientHistoryChanges changes = new PatientHistoryChanges(
                    getLayoutContext().getContext().getLocation(), ServiceHelper.getArchetypeService());
            submitOrders(getOrderActs(investigations), changes, false);
        }
    }

    /**
     * Submits orders.
     *
     * @param orderActs   the order acts
     * @param changes     tracks patient history changes
     * @param discontinue if {@code true} discontinue pharmacy orders if necessary
     */
    private void submitOrders(List<Act> orderActs, PatientHistoryChanges changes,
                              boolean discontinue) {
        List<Act> excluded = new ArrayList<>(getInvestigations().getInvestigations());
        excluded.removeAll(orderActs);
        // exclude any investigations that aren't being submitted this time to avoid their cancellation

        Set<Act> updated = orderPlacer.order(orderActs, excluded, changes);
        if (discontinue && isPosted() && orderPlacer.discontinueOnFinalisation()) {
            // need to discontinue orders as Cubex will leave them visible, even after patient check-out
            // TODO - should prevent placement of orders if an invoice is being finalised at the same time
            updated.addAll(orderPlacer.discontinue(orderActs));
        }
        if (!updated.isEmpty()) {
            // need to save the items again. This time do it skipping rules
            ServiceHelper.getArchetypeService(false).save(updated);

            // notify the editors that orders have been placed
            for (Act item : updated) {
                if (item.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
                    CustomerChargeActItemEditor editor = getItems().getEditor(item);
                    editor.ordered();
                } else if (item.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
                    getInvestigations().reload((DocumentAct) item);
                }
            }
        }
    }

    /**
     * Returns all acts that may be associated with pharmacy or laboratory orders.
     *
     * @param ignorePendingLabOrders if {@code true}, ignore any investigations with PENDING order status
     * @return the acts
     */
    private List<Act> getOrderActs(boolean ignorePendingLabOrders) {
        InvestigationManager manager = getItems().getEditContext().getInvestigations();
        List<DocumentAct> investigations = manager.getInvestigations(ignorePendingLabOrders);
        return getOrderActs(investigations);
    }

    /**
     * Returns all acts that may be associated with pharmacy or laboratory orders.
     *
     * @param investigations the investigations to submit
     * @return the acts
     */
    private List<Act> getOrderActs(List<DocumentAct> investigations) {
        ChargeItemRelationshipCollectionEditor items = getItems();
        List<Act> acts = new ArrayList<>(items.getActs());
        acts.addAll(investigations);
        return acts;
    }

    /**
     * Adds a default invoice item if there are no items present and {@link #addDefaultItem} is {@code true}.
     */
    private void initItems() {
        if (addDefaultItem) {
            ActRelationshipCollectionEditor items = getItems();
            CollectionProperty property = items.getCollection();
            if (property.getValues().isEmpty()) {
                // no invoice items, so add one
                addItem();
            }
        }
    }

    /**
     * Calculates the fixed and unit costs.
     */
    private void calculateCosts() {
        Property fixedCost = getProperty("fixedCost");
        BigDecimal fixed = ActHelper.sum(getObject(), getItems().getCurrentActs(), "fixedCost");
        fixedCost.setValue(fixed);

        Property unitCost = getProperty("unitCost");
        BigDecimal cost = BigDecimal.ZERO;
        for (Act act : getItems().getCurrentActs()) {
            cost = cost.add(calcTotalUnitCost(act));
        }
        unitCost.setValue(cost);
    }

    /**
     * Calculates the total unit cost for an act, based on its <em>unitCost</em>
     * and <em>quantity</em>.
     *
     * @param act the act
     * @return the total unit cost
     */
    private BigDecimal calcTotalUnitCost(Act act) {
        IMObjectBean bean = getBean(act);
        BigDecimal unitCost = bean.getBigDecimal("unitCost", BigDecimal.ZERO);
        BigDecimal quantity = bean.getBigDecimal("quantity", BigDecimal.ZERO);
        return unitCost.multiply(quantity);
    }

    /**
     * Returns new reminders from each of the charge items.
     *
     * @return a list of new reminders
     */
    private List<Act> getNewReminders() {
        ActRelationshipCollectionEditor items = getItems();
        List<Act> reminders = new ArrayList<>();
        for (IMObjectEditor editor : items.getEditors()) {
            if (editor instanceof CustomerChargeActItemEditor) {
                CustomerChargeActItemEditor charge = (CustomerChargeActItemEditor) editor;
                for (Act reminder : charge.getReminders()) {
                    if (reminder.isNew()) {
                        reminders.add(reminder);
                    }
                }
            }
        }
        return reminders;
    }

    /**
     * Returns new alerts from each of the charge items.
     *
     * @return a list of new alerts
     */
    private List<Act> getNewAlerts() {
        return getItems().getEditContext().getAlerts().getNewAlerts();
    }

    /**
     * Invoked when a template product is expanded on an invoice.
     * <p/>
     * This appends any invoiceNote to the notes.
     *
     * @param product the template product
     */
    private void templateProductExpanded(Product product) {
        Property property = getProperty("notes");
        if (property != null) {
            IMObjectBean bean = getBean(product);
            String invoiceNote = bean.getString("invoiceNote");
            if (!StringUtils.isEmpty(invoiceNote)) {
                String value = invoiceNote;
                if (property.getValue() != null) {
                    value = property.getValue().toString();
                    if (!StringUtils.isEmpty(value)) {
                        value = value + "\n" + invoiceNote;
                    } else {
                        value = invoiceNote;
                    }
                }
                property.setValue(value);
            }
        }
    }

}