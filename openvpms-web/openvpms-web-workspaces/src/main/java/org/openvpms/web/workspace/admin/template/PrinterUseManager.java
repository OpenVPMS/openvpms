/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.DocumentTemplatePrinter;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Helper to update printer use.
 *
 * @author Tim Anderson
 */
class PrinterUseManager {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The location rules.
     */
    private final LocationRules locationRules;

    /**
     * The transaction template.
     */
    private final TransactionTemplate transactionTemplate;

    /**
     * The printer use, keyed on printer reference.
     */
    private final Map<PrinterReference, PrinterUse> usage = new HashMap<>();

    /**
     * The default printer node.
     */
    private static final String DEFAULT_PRINTER = "defaultPrinter";

    /**
     * The printers node.
     */
    private static final String PRINTERS = "printers";

    /**
     * The printer node.
     */
    private static final String PRINTER = "printer";

    /**
     * Constructs a {@link PrinterUseManager}.
     *
     * @param service            the archetype service
     * @param practiceService    the practice service
     * @param locationRules      the location rules
     * @param transactionManager the transaction manager
     */
    public PrinterUseManager(ArchetypeService service, PracticeService practiceService, LocationRules locationRules,
                             PlatformTransactionManager transactionManager) {
        this.service = service;
        this.practiceService = practiceService;
        this.locationRules = locationRules;
        transactionTemplate = new TransactionTemplate(transactionManager);
    }

    /**
     * Determines printer use.
     * <p/>
     * This must be called before use, and after making changes.
     */
    public void refresh() {
        usage.clear();
        Iterator<Entity> iterator = getTemplates();
        while (iterator.hasNext()) {
            DocumentTemplate template = new DocumentTemplate(iterator.next(), service);
            for (DocumentTemplatePrinter printer : template.getPrinters()) {
                PrinterReference reference = printer.getPrinter();
                if (reference != null) {
                    PrinterUse use = getOrCreate(reference);
                    use.addPrinter(printer, template);
                }
            }
        }
        for (Party location : getLocations()) {
            PrinterReference defaultPrinter = locationRules.getDefaultPrinter(location);
            if (defaultPrinter != null) {
                addPrinter(location, defaultPrinter);
            }
            for (PrinterReference reference : locationRules.getPrinters(location)) {
                addPrinter(location, reference);
            }
        }
    }

    /**
     * Determines if any printers are in use.
     *
     * @return {@code true} if printers are in use, otherwise {@code false}
     */
    public boolean hasPrinters() {
        return !usage.isEmpty();
    }

    /**
     * Returns the printers.
     *
     * @return the printers
     */
    public List<PrinterReference> getPrinters() {
        return new ArrayList<>(usage.keySet());
    }

    /**
     * Replaces all uses of a printer with another.
     *
     * @param from the printer to replace
     * @param to   the printer to replace with
     */
    public void replace(PrinterReference from, PrinterReference to) {
        PrinterUse use = usage.get(from);
        if (use != null) {
            use.change(to);
        }
    }

    /**
     * Removes all uses of a printer.
     *
     * @param printer the printer
     */
    public void remove(PrinterReference printer) {
        PrinterUse use = usage.get(printer);
        if (use != null) {
            use.remove();
        }
    }

    /**
     * Returns the entities that use a printer.
     *
     * @param reference the printer reference
     * @return the entities that use the printer. These may be practice locations or document templates
     */
    public List<Entity> getUse(PrinterReference reference) {
        List<Entity> result = new ArrayList<>();
        PrinterUse use = usage.get(reference);
        if (use != null) {
            result.addAll(use.getLocations());
            for (DocumentTemplate template : use.getTemplates()) {
                result.add(template.getEntity());
            }
        }
        return result;
    }

    /**
     * Returns the practice locations.
     *
     * @return the practice locations
     */
    protected List<Party> getLocations() {
        return practiceService.getLocations();
    }

    /**
     * Returns active templates.
     *
     * @return active <em>entity.documentTemplate</em> instances
     */
    protected Iterator<Entity> getTemplates() {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> from = query.from(Entity.class, DocumentArchetypes.DOCUMENT_TEMPLATE);

        query.where(builder.equal(from.get("active"), true));
        query.orderBy(builder.asc(from.get("id")));
        return new TypedQueryIterator<>(service.createQuery(query), 500);
    }

    /**
     * Adds a printer for a practice location.
     *
     * @param location  the practice location
     * @param reference the printer reference
     */
    private void addPrinter(Party location, PrinterReference reference) {
        PrinterUse use = getOrCreate(reference);
        use.addLocation(location);
    }

    /**
     * Returns the {@link PrinterUse} for a printer, creating it if it doesn't exist.
     *
     * @param reference the printer reference
     * @return the corresponding {@link PrinterUse}.
     */
    private PrinterUse getOrCreate(PrinterReference reference) {
        return usage.computeIfAbsent(reference, PrinterUse::new);
    }

    /**
     * Tracks the use of a printer.
     */
    private class PrinterUse {

        /**
         * The printer reference.
         */
        private final PrinterReference reference;

        /**
         * The practice locations where the printer is used.
         */
        private final Set<Party> locations = new HashSet<>();

        /**
         * The document template printers and their corresponding templates.
         */
        private final Map<DocumentTemplatePrinter, DocumentTemplate> printers = new HashMap<>();

        /**
         * Constructs a {@link PrinterUse}.
         *
         * @param reference the printer reference
         */
        public PrinterUse(PrinterReference reference) {
            this.reference = reference;
        }

        /**
         * Adds a location.
         *
         * @param location the location
         */
        public void addLocation(Party location) {
            locations.add(location);
        }

        /**
         * Adds a document template printer.
         *
         * @param printer  the document template printer
         * @param template the corresponding template
         */
        public void addPrinter(DocumentTemplatePrinter printer, DocumentTemplate template) {
            printers.put(printer, template);
        }

        /**
         * Returns the practice locations where the printer is used.
         *
         * @return the practice locations
         */
        public Set<Party> getLocations() {
            return locations;
        }

        /**
         * Returns the document templates that refer to the printer.
         *
         * @return the document templates
         */
        public Collection<DocumentTemplate> getTemplates() {
            return printers.values();
        }

        /**
         * Change the printer to a different one.
         *
         * @param to the printer to change to
         */
        public void change(PrinterReference to) {
            for (Party location : locations) {
                IMObjectBean bean = service.getBean(location);
                updateDefaultPrinter(bean, to);
                List<Entity> printers = bean.getTargets(PRINTERS, Entity.class);
                Entity from = getPrinter(printers, reference);
                Entity existing = getPrinter(printers, to);
                if (from != null) {
                    if (existing == null) {
                        IMObjectBean fromBean = service.getBean(from);
                        fromBean.setValue(PRINTER, to.toString());
                        fromBean.save();
                    } else {
                        remove(bean, from);
                    }
                }
            }
            for (Map.Entry<DocumentTemplatePrinter, DocumentTemplate> entry : printers.entrySet()) {
                DocumentTemplatePrinter printer = entry.getKey();
                printer.setPrinter(to.toString());
                service.save(entry.getValue().getEntity());
            }
        }

        /**
         * Removes the printer.
         */
        public void remove() {
            for (Party location : locations) {
                IMObjectBean bean = service.getBean(location);
                updateDefaultPrinter(bean, null);
                List<Entity> printers = bean.getTargets(PRINTERS, Entity.class);
                Entity printer = getPrinter(printers, reference);
                if (printer != null) {
                    remove(bean, printer);
                }
            }
            for (Map.Entry<DocumentTemplatePrinter, DocumentTemplate> entry : printers.entrySet()) {
                DocumentTemplatePrinter printer = entry.getKey();
                Entity template = entry.getValue().getEntity();
                template.removeEntityRelationship(printer.getRelationship());
                service.save(template);
            }
        }

        /**
         * Replaces the default printer with the specified value, if it matches the existing reference.
         *
         * @param bean the practice location
         * @param to   the value to replace the existing default printer with. May be {@code null}
         */
        private void updateDefaultPrinter(IMObjectBean bean, PrinterReference to) {
            PrinterReference defaultPrinter = PrinterReference.fromString(bean.getString(DEFAULT_PRINTER));
            if (defaultPrinter != null && defaultPrinter.equals(reference)) {
                bean.setValue(DEFAULT_PRINTER, to != null ? to.toString() : null);
                bean.save();
            }
        }

        /**
         * Removes a printer from a practice location.
         *
         * @param bean    the location bean
         * @param printer the printer to remove
         */
        private void remove(IMObjectBean bean, Entity printer) {
            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    bean.removeTarget(PRINTERS, printer);
                    bean.save();
                    service.remove(printer);
                }
            });
        }

        /**
         * Returns the first <em>entity.printer</em> that matches the supplied reference.
         *
         * @param printers  the printers
         * @param reference the printer reference
         * @return the printer, or {@code null} if none is found
         */
        private Entity getPrinter(List<Entity> printers, PrinterReference reference) {
            String str = reference.toString();
            for (Entity printer : printers) {
                IMObjectBean bean = service.getBean(printer);
                if (Objects.equals(str, bean.getString(PRINTER))) {
                    return printer;
                }
            }
            return null;
        }
    }
}