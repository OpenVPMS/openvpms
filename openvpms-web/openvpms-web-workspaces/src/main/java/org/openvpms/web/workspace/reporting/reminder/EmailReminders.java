/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.workspace.reporting.ReportingException;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.TemplateMissingEmailText;

/**
 * Reminders to be emailed to a customer.
 *
 * @author Tim Anderson
 */
public class EmailReminders extends GroupedReminders {

    /**
     * The email template evaluator.
     */
    private final EmailTemplateEvaluator evaluator;

    /**
     * The reporter factory.
     */
    private final ReporterFactory factory;

    /**
     * The email template.
     */
    private EmailTemplate emailTemplate;

    /**
     * Constructs a {@link EmailReminders}.
     *
     * @param groupBy   the reminder grouping policy. This is used to determine which document template, if any, is
     *                  selected to process reminders.
     * @param resend    determines if reminders are being resent
     * @param evaluator the template evaluator
     * @param factory   the reporter factory
     */
    public EmailReminders(ReminderType.GroupBy groupBy, boolean resend, EmailTemplateEvaluator evaluator,
                          ReporterFactory factory) {
        super(groupBy, resend);
        this.evaluator = evaluator;
        this.factory = factory;
    }

    /**
     * Returns the email address.
     *
     * @return the email address
     */
    public String getEmailAddress() {
        IMObjectBean bean = IMObjectHelper.getBean(getContact());
        return bean.getString("emailAddress");
    }

    /**
     * Returns the email subject.
     *
     * @param context the context
     * @return the email subject
     */
    public String getSubject(Context context) {
        List<ReminderEvent> reminders = getReminders();
        Act reminder = reminders.get(0).getReminder();
        String subject = evaluator.getSubject(emailTemplate, reminder, context);
        if (StringUtils.isEmpty(subject)) {
            subject = getTemplate().getName();
        }
        return subject;
    }

    /**
     * Returns the email message.
     *
     * @param context the context
     * @return the email message
     */
    public String getMessage(Context context) {
        String body = null;
        List<ReminderEvent> reminders = getReminders();
        if (reminders.size() == 1) {
            Act reminder = reminders.get(0).getReminder();
            Reporter<IMObject> reporter = evaluator.getMessageReporter(emailTemplate, reminder, context);
            if (reporter != null) {
                body = evaluator.getMessage(reporter);
                if (StringUtils.isEmpty(body)) {
                    throw new ReportingException(TemplateMissingEmailText, getTemplate().getName());
                }
            }
        } else {
            List<ObjectSet> sets = getObjectSets(reminders);
            Reporter<ObjectSet> reporter = evaluator.getMessageReporter(emailTemplate, sets, context);
            if (reporter != null) {
                body = evaluator.getMessage(reporter);
                if (StringUtils.isEmpty(body)) {
                    throw new ReportingException(TemplateMissingEmailText, getTemplate().getName());
                }
            }
        }
        if (body == null) {
            body = evaluator.getMessage(emailTemplate, reminders.get(0).getReminder(), context);
        }
        if (StringUtils.isEmpty(body)) {
            throw new ReportingException(TemplateMissingEmailText, getTemplate().getName());
        }
        return body;
    }

    /**
     * Sets the email template
     *
     * @param template the email template to use. May be {@code null} if there are no reminders to send
     */
    public void setEmailTemplate(EmailTemplate template) {
        this.emailTemplate = template;
    }

    /**
     * Returns the email template.
     *
     * @return the email template
     */
    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    /**
     * Creates an email attachment containing the reminders.
     *
     * @param context the reporting context
     * @return a new report
     */
    public Document createAttachment(Context context) {
        Document result;
        List<ReminderEvent> reminders = getReminders();
        DocumentTemplate template = getTemplate();
        if (reminders.size() > 1) {
            result = getDocument(factory.createObjectSetReporter(getObjectSets(reminders), template), context);
        } else {
            List<Act> acts = new ArrayList<>();
            for (ReminderEvent event : reminders) {
                acts.add(event.getReminder());
            }
            result = getDocument(factory.createIMObjectReporter(acts, template), context);
        }
        return result;
    }

    /**
     * Generates the reminder document to email.
     *
     * @param reporter the document generator
     * @return the generated document
     */
    private Document getDocument(Reporter<?> reporter, Context context) {
        reporter.setFields(ReportContextFactory.create(context));
        return reporter.getDocument(DocFormats.PDF_TYPE, true);
    }
}
