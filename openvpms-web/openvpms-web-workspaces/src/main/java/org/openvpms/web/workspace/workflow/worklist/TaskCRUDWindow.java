/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.TaskStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.workflow.DefaultTaskListener;
import org.openvpms.web.component.workflow.TaskEvent;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.LocalClinicianContext;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleCRUDWindow;


/**
 * Task CRUD window.
 *
 * @author Tim Anderson
 */
public class TaskCRUDWindow extends ScheduleCRUDWindow {

    /**
     * Transfer button identifier.
     */
    private static final String TRANSFER_ID = "button.transfer";

    /**
     * Complete button identifier.
     */
    private static final String COMPLETED_ID = "button.complete";


    /**
     * Constructs a {@link TaskCRUDWindow}.
     *
     * @param context the context
     * @param help    the help context
     */
    public TaskCRUDWindow(Context context, HelpContext help) {
        super(Archetypes.create("act.customerTask", Act.class), TaskActions.INSTANCE, context, help);
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(Act object) {
        super.setObject(object);
        getContext().setTask(object); // make available to macros etc
    }

    /**
     * Creates and edits a new object.
     */
    @Override
    public void create() {
        if (getContext().getWorkList() != null) {
            super.create();
        }
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);

        buttons.add(createConsultButton());
        buttons.add(createCheckOutButton());
        buttons.add(TRANSFER_ID, action(ScheduleArchetypes.TASK, this::transferTask,
                                        act -> getActions().canCheckoutOrConsult(act),
                                        "workflow.worklist.transfer.title"));
        buttons.add(COMPLETED_ID, action(ScheduleArchetypes.TASK, this::completeTask,
                                         act -> getActions().canCheckoutOrConsult(act),
                                         "workflow.worklist.complete.title"));
        buttons.add(createOverTheCounterButton());
        buttons.add(createFlowSheetButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        Act act = getObject();
        boolean checkout = enable && getActions().canCheckoutOrConsult(act);
        boolean complete = enable && getActions().canComplete(act);
        buttons.setEnabled(CONSULT_ID, checkout);
        buttons.setEnabled(CHECKOUT_ID, checkout);
        buttons.setEnabled(TRANSFER_ID, checkout);
        buttons.setEnabled(COMPLETED_ID, complete);
    }

    /**
     * Creates a layout context for editing an object.
     *
     * @param help the help context
     * @return a new layout context
     */
    @Override
    protected LayoutContext createLayoutContext(HelpContext help) {
        // create a local context - don't want don't want to pick up the current clinician
        Context local = new LocalClinicianContext(getContext());
        return new DefaultLayoutContext(true, local, help);
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected TaskActions getActions() {
        return (TaskActions) super.getActions();
    }

    /**
     * Transfers a task to a different work list.
     *
     * @param task the task
     */
    private void transferTask(Act task) {
        HelpContext help = getHelpContext().subtopic("transfer");
        TransferWorkflow transfer = new TransferWorkflow(task, getContext(), help);
        transfer.addTaskListener(new DefaultTaskListener() {
            public void taskEvent(TaskEvent event) {
                onRefresh(task);
            }
        });
        transfer.start();
    }

    /**
     * Marks a task completed.
     *
     * @param task the task
     */
    private void completeTask(Act task) {
        IMObjectBean bean = getBean(task);
        String name = IMObjectHelper.getName(bean.getTargetRef("customer"));
        ConfirmationDialog.newDialog().title(Messages.get("workflow.worklist.complete.title"))
                .message(Messages.format("workflow.worklist.complete.message", name))
                .yesNo()
                .yes(() -> {
                    HelpContext edit = createEditTopic(task);
                    LayoutContext context = createLayoutContext(edit);
                    TaskActEditor editor = (TaskActEditor) createEditor(task, context);
                    editor.setStatus(TaskStatus.COMPLETED);
                    SaveHelper.save(editor);
                    onSaved(task, false);
                })
                .show();
    }

    protected static class TaskActions extends ScheduleActions {

        public static final TaskActions INSTANCE = new TaskActions();

        /**
         * Determines if a consultation or checkout can be performed on an act.
         *
         * @param act the act
         * @return {@code true} if consultation can be performed
         */
        @Override
        public boolean canCheckoutOrConsult(Act act) {
            boolean result = false;
            if (canComplete(act)) {
                IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(act);
                result = bean.getTargetRef("patient") != null;
            }
            return result;
        }

        /**
         * Determines if a task can be completed.
         *
         * @param act the task
         * @return {@code true} if the task can be completed
         */
        public boolean canComplete(Act act) {
            String status = act.getStatus();
            return TaskStatus.PENDING.equals(status) || TaskStatus.IN_PROGRESS.equals(status)
                   || TaskStatus.BILLED.equals(status);
        }
    }
}
