/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Date;

/**
 * Viewer for a report date parameter.
 *
 * @author Tim Anderson
 * @see DateParameter
 */
class DateParameterViewer {


    private final Component component;

    private final SimpleProperty property;

    /**
     * Constructs a {@link DateParameterViewer}.
     *
     * @param name           the parameter name
     * @param displayName    the display name
     * @param expressionType the expression type
     * @param value          the parameter value
     * @param context        the layout context
     */
    public DateParameterViewer(String name, String displayName, Property expressionType, Property value,
                               LayoutContext context) {
        IMObjectComponentFactory factory = context.getComponentFactory();

        if (ExpressionType.VALUE.toString().equals(expressionType.getString())) {
            property = new SimpleProperty(name, value.getDate(), Date.class, displayName, true);
        } else {
            StringBuilder builder = new StringBuilder();
            String expr = Messages.get("scheduledreport.expression." + expressionType.getString());
            builder.append(expr);
            String currentValue = StringUtils.trimToNull(value.getString());
            DateOffset offset = DateOffset.parse(currentValue);
            if (offset != null) {
                builder.append(' ');
                if (offset.getOffset() > 0) {
                    builder.append("+ ");
                }
                builder.append(offset.getOffset());
                builder.append(' ');
                builder.append(Messages.get("scheduledreport.dateunits." + offset.getUnits()));
            }
            property = new SimpleProperty(name, builder.toString(), String.class, displayName, true);
        }
        component = factory.create(property);
    }

    /**
     * Returns the property.
     *
     * @return the property
     */
    public Property getProperty() {
        return property;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        return component;
    }
}
