/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.firewall;

import inet.ipaddr.AddressStringException;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import inet.ipaddr.IPAddressStringParameters;
import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.AbstractEditor;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetImpl;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

/**
 * An editor for a {@link FirewallEntry}.
 *
 * @author Tim Anderson
 */
class FirewallEntryEditor extends AbstractEditor {

    /**
     * The property editors.
     */
    private final Editors editors;

    /**
     * The address editor.
     */
    private final SimpleProperty address = new SimpleProperty("address", String.class);

    /**
     * The active flag editor.
     */
    private final SimpleProperty active = new SimpleProperty("active", Boolean.class);

    /**
     * The description.
     */
    private final SimpleProperty description = new SimpleProperty("description", String.class);

    /**
     * The address range.
     */
    private final SimpleProperty range = new SimpleProperty("range", String.class);

    /**
     * THe editor component.
     */
    private final Component component;

    /**
     * Constructs a {@link FirewallEntryEditor}.
     *
     * @param context the layout context
     */
    public FirewallEntryEditor(LayoutContext context) {
        this(null, true, null, context);
    }

    /**
     * Constructs a {@link FirewallEntryEditor}.
     *
     * @param entry   the entry to edit
     * @param context the layout context
     */
    public FirewallEntryEditor(FirewallEntry entry, LayoutContext context) {
        this(entry.getAddress(), entry.isActive(), entry.getDescription(), context);
    }

    /**
     * Constructs a {@link FirewallEntryEditor}.
     *
     * @param address     the address. May be {@code null}
     * @param active      the active flag
     * @param description the description. May be {@code null}
     * @param context     the layout context
     */
    public FirewallEntryEditor(String address, boolean active, String description, LayoutContext context) {
        this.address.setValue(address);
        this.active.setValue(active);
        this.description.setValue(description);
        updateRange();
        component = createComponent(context);
        editors = createEditors();
        this.address.addModifiableListener((modifiable) -> updateRange());
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        return component;
    }

    /**
     * Returns the address.
     *
     * @return the address. May be {@code null}
     */
    public String getAddress() {
        return address.getString();
    }

    /**
     * Returns the active flag.
     *
     * @return the active flag
     */
    public boolean getActive() {
        return active.getBoolean();
    }

    /**
     * Returns the description.
     *
     * @return the description. May be {@code null}
     */
    public String getDescription() {
        return description.getString();
    }

    /**
     * Returns a new {@link FirewallEntry} corresponding to the entered details.
     *
     * @return a new entry
     */
    public FirewallEntry getEntry() {
        return new FirewallEntry(address.getString(), active.getBoolean(), description.getString());
    }

    /**
     * Determines if none of the fields are populated.
     *
     * @return {@code true} if none of the fields are populated, otherwise {@code false}
     */
    public boolean isEmpty() {
        return StringUtils.isEmpty(address.getString()) && active.getBoolean()
               && StringUtils.isEmpty(description.getString());
    }

    /**
     * Determines if the object has been modified.
     *
     * @return {@code true} if the object has been modified
     */
    @Override
    public boolean isModified() {
        return super.isModified() || editors.isModified();
    }

    /**
     * Clears the modified status of the object.
     */
    @Override
    public void clearModified() {
        super.clearModified();
        editors.clearModified();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return editors.validate(validator) && validateAddress(validator);
    }

    /**
     * Returns the range of addresses that an address matches.
     *
     * @param address the address
     * @return the matching addresses
     */
    static String getRange(String address) {
        String result;
        try {
            IPAddressString parser = new IPAddressString(address);
            IPAddress block = parser.toAddress().toPrefixBlock();
            if (block.getLower().equals(block.getUpper())) {
                result = block.getLower().withoutPrefixLength().toString();
            } else {
                result = block.getLower().withoutPrefixLength() + " - " + block.getUpper().withoutPrefixLength();
            }
        } catch (AddressStringException exception) {
            result = exception.getMessage();
        }
        return result;
    }

    private void updateRange() {
        if (validateAddress(new DefaultValidator())) {
            range.setValue(getRange(getAddress()));
        } else {
            range.setValue(null);
        }
    }

    private boolean validateAddress(Validator validator) {
        boolean valid = address.validate(validator);
        if (valid) {
            IPAddressStringParameters params = new IPAddressStringParameters.Builder()
                    .allowIPv4(true)
                    .allowIPv6(true)
                    .allow_inet_aton(false)
                    .allowEmpty(false)
                    .allowWildcardedSeparator(false)
                    .allowMask(true)
                    .toParams();

            IPAddressString string = new IPAddressString(address.getString(), params);
            try {
                string.toAddress();

                // also run it by IpAddressMatcher, which will ultimately use the address
                new IpAddressMatcher(address.getString());
            } catch (Exception exception) {
                validator.add(this, exception.getMessage());
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Create an {@link Editors}, to manage the properties.
     *
     * @return a new {@link Editors}
     */
    private Editors createEditors() {
        PropertySet set = new PropertySetImpl(address, active, description);
        return new Editors(set, getListeners());
    }

    /**
     * Creates the editor component.
     *
     * @param context the layout context
     * @return the component
     */
    private Component createComponent(LayoutContext context) {
        address.setRequired(true);
        description.setMaxLength(20);
        range.setReadOnly(true);
        range.setDisplayName(Messages.get("admin.system.firewall.range"));

        ComponentGrid grid = new ComponentGrid();
        IMObjectComponentFactory factory = context.getComponentFactory();
        ComponentState addressField = new ComponentState(factory.create(address), address);
        ComponentState activeField = new ComponentState(factory.create(active), active);
        ComponentState descriptionField = new ComponentState(factory.create(description), description);
        grid.add(addressField);
        grid.add(activeField);
        grid.add(descriptionField);
        grid.add(new ComponentState(factory.create(range), range));
        FocusGroup focusGroup = getFocusGroup();
        focusGroup.add(addressField.getComponent());
        focusGroup.add(activeField.getComponent());
        focusGroup.add(descriptionField.getComponent());
        return grid.createGrid();
    }
}
