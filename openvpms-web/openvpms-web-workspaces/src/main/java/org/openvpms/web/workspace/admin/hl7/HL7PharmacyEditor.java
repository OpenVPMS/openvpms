/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.hl7;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for <em>entity.HL7ServicePharmacy</em>.
 * <p/>
 * This ensures that when One Way is selected, there must be no Dispense Connector set.
 *
 * @author Tim Anderson
 */
public class HL7PharmacyEditor extends AbstractIMObjectEditor {

    /**
     * Constructs an {@link HL7PharmacyEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public HL7PharmacyEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new HL7PharmacyEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateOneWay(validator);
    }

    /**
     * Ensures that if the pharmacy is one-way, no dispense connector is provided.
     *
     * @param validator the validator
     * @return {@code true} if this is valid, otherwise {@code false}
     */
    private boolean validateOneWay(Validator validator) {
        boolean valid = true;
        Property oneway = getProperty("oneway");
        if (oneway.getBoolean()) {
            if (getTarget("receiver") != null) {
                valid = false;
                Property receiver = getProperty("receiver");
                validator.add(this, Messages.format("admin.hl7.pharmacy.oneway", receiver.getDisplayName(),
                                                    oneway.getDisplayName()));
            }
        }
        return valid;
    }
}