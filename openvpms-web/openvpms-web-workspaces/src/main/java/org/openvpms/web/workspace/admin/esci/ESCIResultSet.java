/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import org.openvpms.archetype.rules.stock.StockArchetypes;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectRefSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.EntityObjectSetResultSet;
import org.openvpms.web.component.im.query.ResultSet;

import java.util.List;

import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI;
import static org.openvpms.component.system.common.query.Constraints.join;

/**
 * *
 * A {@link ResultSet} implementation that queries suppliers and their ESCI relationships.
 * <p/>
 * The returned {@link ObjectSet ObjectSet}s contain the following:
 * <ul>
 * <li><em>entity</em> - the supplier object reference</li>
 * <li><em>entity.name</em> - the supplier name</li>
 * <li><em>entity.description</em> - the supplier description</li>
 * <li><em>entity.active</em> - the supplier active state</li>
 * <li><em>stockLocation.name</em> - the stock location name</li>
 * <li><em>stockLocation.reference</em> - the stock location reference</li>
 * <li><em>config.reference</em> - the ESCI configuration reference</li>
 * </ul>
 * * @author Tim Anderson
 */
class ESCIResultSet extends EntityObjectSetResultSet {

    /**
     * Constructs an {@link ESCIResultSet}.
     *
     * @param archetypes       the archetypes to query
     * @param value            the value to query on. May be {@code null}
     * @param searchIdentities if {@code true} search on identity name
     * @param sort             the sort criteria. May be {@code null}
     * @param rows             the maximum no. of rows per page
     * @param distinct         if {@code true} filter duplicate rows
     */
    public ESCIResultSet(ShortNameConstraint archetypes, String value, boolean searchIdentities, SortConstraint[] sort,
                         int rows, boolean distinct) {
        super(archetypes, value, searchIdentities, sort, rows, distinct);
    }

    /**
     * Adds constraints to query nodes on a value.
     * <p/>
     * This creates the constraints with {@link #createValueConstraints(String, List)}. If multiple constraints
     * are returned, an or constraint is used to add them to the query.
     *
     * @param query the query to add the constraints to
     */
    @Override
    protected void addValueConstraints(ArchetypeQuery query) {
        BaseArchetypeConstraint.State state = getArchetypes().getState();
        ShortNameConstraint stockLocation = new ShortNameConstraint("stockLocation", StockArchetypes.STOCK_LOCATION,
                                                                    state);
        query.add(join("stockLocations", Constraints.shortName("config", SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI))
                          .add(join("target", stockLocation)));
        query.add(new ObjectRefSelectConstraint("stockLocation"));
        query.add(new NodeSelectConstraint("stockLocation.name"));
        query.add(new ObjectRefSelectConstraint("config"));
        super.addValueConstraints(query);
    }

    /**
     * Creates constraints to query nodes on a value.
     *
     * @param value the value to query
     * @param nodes the nodes to query
     * @return the constraints
     */
    @Override
    protected List<IConstraint> createValueConstraints(String value, List<String> nodes) {
        List<IConstraint> constraints = super.createValueConstraints(value, nodes);
        constraints.add(Constraints.eq("stockLocation.name", value));
        return constraints;
    }
}
