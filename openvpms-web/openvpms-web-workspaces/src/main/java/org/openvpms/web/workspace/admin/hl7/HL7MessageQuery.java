/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.hl7;

import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.hl7.util.HL7Archetypes;
import org.openvpms.hl7.util.HL7MessageStatuses;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

/**
 * Query for HL7 messages.
 * <p/>
 * This queries 'Incomplete' messages by default, i.e those that have PENDING or ERROR status.
 *
 * @author Tim Anderson
 */
public class HL7MessageQuery extends DateRangeActQuery<Act> {

    /**
     * Dummy incomplete status. Finds all messages that have PENDING or ERROR status.
     */
    public static final String INCOMPLETE = "INCOMPLETE";

    /**
     * The message statuses.
     */
    private static final ActStatuses STATUSES = new ActStatuses(new StatusLookupQuery());

    /**
     * Dummy incomplete status, used in the status selector.
     */
    private static final Lookup INCOMPLETE_STATUS
            = new org.openvpms.component.business.domain.im.lookup.Lookup(new ArchetypeId("lookup.local"), INCOMPLETE,
                                                                          Messages.get("admin.hl7.status.incomplete"));

    /**
     * Constructs an {@link HL7MessageQuery}.
     *
     * @param connector the connector to query messages for
     */
    public HL7MessageQuery(Entity connector) {
        super(connector, "connector", "participation.HL7Connector", new String[]{HL7Archetypes.MESSAGE},
              STATUSES, Act.class);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        Lookup selected = getStatusSelector().getSelected();
        String[] statuses;
        if (selected == INCOMPLETE_STATUS) {
            statuses = new String[]{HL7MessageStatuses.PENDING, HL7MessageStatuses.ERROR};
        } else {
            statuses = super.getStatuses();
        }
        return new ActResultSet<>(getArchetypeConstraint(), getParticipantConstraint(), getFrom(), getTo(),
                                  statuses, false, getConstraints(), getMaxResults(), sort);
    }

    private static class StatusLookupQuery extends NodeLookupQuery {

        /**
         * Constructs an {@link StatusLookupQuery}.
         */
        StatusLookupQuery() {
            super(HL7Archetypes.MESSAGE, "status");
        }

        /**
         * Returns the default lookup.
         *
         * @return {@link #INCOMPLETE_STATUS}
         */
        @Override
        public Lookup getDefault() {
            return HL7MessageQuery.INCOMPLETE_STATUS;
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<Lookup> getLookups() {
            List<Lookup> lookups = super.getLookups();
            lookups.add(0, INCOMPLETE_STATUS);
            return lookups;
        }
    }
}
