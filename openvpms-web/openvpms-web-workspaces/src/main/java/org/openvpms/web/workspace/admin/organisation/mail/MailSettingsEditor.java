/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.mail;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.practice.MailServer;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.BoundLookupField;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupQuery;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetImpl;
import org.openvpms.web.component.property.RequiredProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.security.oauth.OAuth2RequestLauncher;
import org.openvpms.web.security.oauth.OAuth2ResponseManager;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.core.OAuth2Error;

import java.util.ArrayList;
import java.util.List;

/**
 * Mail settings editor.
 * <p/>
 * This is designed to be delegated to by an {@link IMObjectEditor} implementation.
 *
 * @author Tim Anderson
 * @see MailServerEditor
 */
public class MailSettingsEditor {

    /**
     * Security node name.
     */
    public static final String SECURITY = "security";

    /**
     * Authentication method node name.
     */
    public static final String AUTHENTICATION_METHOD = "authenticationMethod";

    /**
     * Username node name.
     */
    public static final String USERNAME = "username";

    /**
     * Password node name.
     */
    public static final String PASSWORD = "password";

    /**
     * OAuth2 client registration node name.
     */
    public static final String OAUTH2_CLIENT_REGISTRATION = "oauth2ClientRegistration";

    /**
     * The object to edit.
     */
    private final Entity object;

    /**
     * The object properties.
     */
    private final PropertySet properties;

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * Authentication method dropdown.
     */
    private final ComponentState authenticationMethodComponent;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The layout strategy.
     */
    private MailSettingsLayoutStrategy layoutStrategy;

    /**
     * OAuth2 client registration lookup archetype.
     */
    private static final String REGISTRATION_LOOKUP = "lookup.oauth2ClientRegistration";

    /**
     * Authorise button id.
     */
    private static final String AUTHORISE_ID = "button.authorise";

    /**
     * Check authorisation button id.
     */
    private static final String CHECK_AUTHORISATION_ID = "button.checkAuthorisation";

    /**
     * Edit registration button id.
     */
    private static final String EDIT_REGISTRATION_ID = "button.editOAuth2Registration";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MailSettingsEditor.class);

    /**
     * Constructs a {@link MailServerEditor}.
     *
     * @param object        the object to edit
     * @param properties    the object properties
     * @param layoutContext the layout context
     */
    public MailSettingsEditor(Entity object, PropertySet properties, LayoutContext layoutContext) {
        this.object = object;
        this.properties = properties;
        this.layoutContext = layoutContext;
        service = ServiceHelper.getArchetypeService();
        lookups = ServiceHelper.getLookupService();
        Property authenticationMethod = properties.get(AUTHENTICATION_METHOD);
        authenticationMethodComponent = layoutContext.getComponentFactory().create(authenticationMethod, object);
        authenticationMethod.addModifiableListener(modifiable -> onAuthenticationMethodChanged());
        Property registration = properties.get(OAUTH2_CLIENT_REGISTRATION);
        registration.addModifiableListener(modifiable -> setUsername(null));
        properties.get(USERNAME).addModifiableListener(modifiable -> onUsernameChanged());
    }

    /**
     * Returns the username.
     *
     * @return the username. May be {@code null}
     */
    public String getUsername() {
        return StringUtils.trimToNull(properties.get(USERNAME).getString());
    }

    /**
     * Sets the username.
     *
     * @param username the username. May be {@code null}
     */
    public void setUsername(String username) {
        properties.get(USERNAME).setValue(StringUtils.trimToNull(username));
    }

    /**
     * Returns the authentication method.
     *
     * @return the authentication method. May be {@code null}
     */
    public MailServer.AuthenticationMethod getAuthenticationMethod() {
        Property authenticationMethod = properties.get(AUTHENTICATION_METHOD);
        String value = authenticationMethod.getString();
        return value != null ? MailServer.AuthenticationMethod.valueOf(value) : null;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    public boolean validate(Validator validator) {
        boolean valid = true;
        if (MailServer.AuthenticationMethod.OAUTH2 == getAuthenticationMethod() && getUsername() == null) {
            if (refreshEmailAddress() != OAuth2Status.SUCCESS) {
                valid = false;
                if (getLayoutStrategy().isAuthoriseButtonDisplayed()) {
                    validator.add(properties.get(AUTHENTICATION_METHOD), Messages.get("admin.mail.oauth2.authorise"));
                } else {
                    validator.add(properties.get(AUTHENTICATION_METHOD), Messages.get("admin.mail.oauth2.incomplete"));
                }
            }
        }
        return valid;
    }

    /**
     * Returns the layout strategy.
     *
     * @return the layout strategy
     */
    public MailSettingsLayoutStrategy getLayoutStrategy() {
        if (layoutStrategy == null) {
            layoutStrategy = new MailSettingsLayoutStrategy();
        }
        return layoutStrategy;
    }

    /**
     * Invoked when the authentication method changes.
     */
    private void onAuthenticationMethodChanged() {
        if (MailServer.AuthenticationMethod.OAUTH2 == getAuthenticationMethod()) {
            setUsername(null);
            properties.get(PASSWORD).setValue(null);
        }
        getLayoutStrategy().addSecuritySettings(object, properties, layoutContext);
    }

    /**
     * Invoked when the username changes.
     * <p/>
     * If the authentication method is OAUTH2, the Authorise button will be displayed if it isn't already.
     */
    private void onUsernameChanged() {
        if (getUsername() != null && MailServer.AuthenticationMethod.OAUTH2 == getAuthenticationMethod()
            && !getLayoutStrategy().isAuthoriseButtonDisplayed()) {
            getLayoutStrategy().showAuthoriseButton();
        }
    }

    /**
     * Updates the username with the email address from the {@link OAuth2ResponseManager}
     * if one is present, else sets it to {@code null}.
     *
     * @return the status of the refresh
     */
    private OAuth2Status refreshEmailAddress() {
        OAuth2Status result;
        OAuth2ResponseManager responseManager = ServiceHelper.getBean(OAuth2ResponseManager.class);
        String email = responseManager.getEmail();
        if (email != null) {
            setUsername(email);
            result = OAuth2Status.SUCCESS;
        } else {
            OAuth2Error error = responseManager.getError();
            if (error != null) {
                log.error("OAuth2 authorisation failed: {}, uri={}", error, error.getUri());
                ErrorDialog.newDialog().title(Messages.get("admin.mail.oauth2.title"))
                        .message(Messages.format("admin.mail.oauth2.error", error.toString()))
                        .button(AUTHORISE_ID, () -> onAuthorise(true))
                        .button(EDIT_REGISTRATION_ID, this::editClientRegistration)
                        .cancel()
                        .show();
                result = OAuth2Status.ERROR;
                getLayoutStrategy().showAuthoriseButton();
            } else {
                result = OAuth2Status.INCOMPLETE;
            }
        }
        responseManager.clear();
        return result;
    }

    /**
     * Displays a dialog to edit the selected client registration lookup.
     */
    private void editClientRegistration() {
        String clientRegistration = properties.get(OAUTH2_CLIENT_REGISTRATION).getString();
        if (clientRegistration != null) {
            editClientRegistration(clientRegistration, false);
        }
    }

    /**
     * Displays a dialog to edit a client registration lookup.
     * <p/>
     * On completion, starts the OAuth2 authorisation flow.
     *
     * @param code                  the lookup code
     * @param authoriseOnCompletion if {@code true}, start the authorisation flow
     */
    private void editClientRegistration(String code, boolean authoriseOnCompletion) {
        Lookup lookup = lookups.getLookup(REGISTRATION_LOOKUP, code);
        if (lookup != null) {
            // reload for editing
            lookup = service.get(lookup.getObjectReference(), Lookup.class);
        }
        if (lookup == null) {
            lookup = service.create(REGISTRATION_LOOKUP, Lookup.class);
            lookup.setCode(code);
        }
        Context context = layoutContext.getContext();
        IMObjectEditor editor = ServiceHelper.getBean(IMObjectEditorFactory.class).
                create(lookup, new DefaultLayoutContext(true, context, layoutContext.getHelpContext()));
        EditDialog dialog = ServiceHelper.getBean(EditDialogFactory.class).create(editor, context);
        if (authoriseOnCompletion) {
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    onAuthorise(false);
                }
            });
        }
        dialog.show();
    }

    /**
     * Starts an OAuth2 authorisation flow using {@link OAuth2RequestLauncher}.
     *
     * @param confirm if {@code true}, confirm if a username is already present
     */
    private void onAuthorise(boolean confirm) {
        String clientRegistration = properties.get(OAUTH2_CLIENT_REGISTRATION).getString();
        if (clientRegistration != null) {
            Lookup lookup = (Lookup) lookups.getLookup(REGISTRATION_LOOKUP, clientRegistration);
            if (lookup == null) {
                editClientRegistration(clientRegistration, true);
            } else {
                String username = getUsername();
                if (username == null || !confirm) {
                    launchAuthorisation();
                } else {
                    ConfirmationDialog.newDialog()
                            .title(Messages.get("admin.mail.oauth2.title"))
                            .message(Messages.format("admin.mail.oauth2.reauthorise", username))
                            .yesNo()
                            .yes(this::launchAuthorisation)
                            .show();
                }
            }
        }
    }

    /**
     * Launches the OAuth2 code grant flow.
     */
    private void launchAuthorisation() {
        setUsername(null);
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(object);
        String clientRegistrationId = bean.getString(OAUTH2_CLIENT_REGISTRATION);
        getLayoutStrategy().showCheckAuthoriseButton();
        OAuth2RequestLauncher launcher = ServiceHelper.getBean(OAuth2RequestLauncher.class);
        launcher.launch(clientRegistrationId);
    }

    /**
     * Checks if an email address is available.
     */
    private void onCheck() {
        OAuth2Status status = refreshEmailAddress();
        if (status == OAuth2Status.INCOMPLETE) {
            ConfirmationDialog.newDialog()
                    .title(Messages.get("admin.mail.oauth2.title"))
                    .message(Messages.get("admin.mail.oauth2.check"))
                    .button(AUTHORISE_ID, () -> onAuthorise(true))
                    .button(CHECK_AUTHORISATION_ID, this::onCheck)
                    .button(EDIT_REGISTRATION_ID, this::editClientRegistration)
                    .buttons(ConfirmationDialog.CANCEL_ID)
                    .show();
        }
    }

    /**
     * Layout strategy for mail settings. This renders the security details on a separate tab and supports
     * launching the OAuth2 flow.
     */
    protected class MailSettingsLayoutStrategy extends AbstractMailSettingsLayoutStrategy {

        /**
         * The authorise/check authorisation button, when OAuth2 is selected.
         */
        private final Button authButton = ButtonFactory.create();

        /**
         * Determines if the 'Authorise' button is being displayed.
         */
        private boolean authoriseButton;

        /**
         * The action to run when the authButton is pressed.
         */
        private Runnable authAction;

        /**
         * The settings tab focus group.
         */
        private FocusGroup settingsFocus;

        /**
         * Constructs a {@link MailSettingsLayoutStrategy}.
         */
        public MailSettingsLayoutStrategy() {
            super(true);
            // keep the focus group after apply(), so the settingsFocus can be updated

            authButton.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    if (authAction != null) {
                        authAction.run();
                    }
                }
            });
        }

        /**
         * Determines if the 'Authorise' button is displayed.
         *
         * @return {@code true} if the button is displayed, otherwise {@code false}
         */
        public boolean isAuthoriseButtonDisplayed() {
            return authoriseButton;
        }

        /**
         * Shows an 'Authorise' button for OAuth2 authorisation.
         */
        public void showAuthoriseButton() {
            authoriseButton = true;
            changeAuthButton(AUTHORISE_ID, "admin.mail.oauth.authorise.tooltip",
                             () -> MailSettingsEditor.this.onAuthorise(true));
        }

        /**
         * Shows an 'Check Authorisation' button for OAuth2 authorisation.
         */
        public void showCheckAuthoriseButton() {
            authoriseButton = false;
            changeAuthButton(CHECK_AUTHORISATION_ID, null, MailSettingsEditor.this::onCheck);
        }

        /**
         * Adds a tab containing the security settings, or recreates it if one is present.
         *
         * @param model      the tab pane model
         * @param object     the object to lay out
         * @param properties the object's properties
         * @param context    the layout context
         */
        @Override
        protected void addSecuritySettings(IMObjectTabPaneModel model, IMObject object, PropertySet properties,
                                           LayoutContext context) {
            if (model.size() > 0) {
                model.removeTabAt(0);
            }
            FocusGroup group = getFocusGroup();
            if (settingsFocus != null) {
                group.remove(settingsFocus);
            }
            super.addSecuritySettings(model, object, properties, context);
            group.add(settingsFocus);
        }

        /**
         * Creates a component displaying the security settings.
         *
         * @param object     the settings
         * @param properties the properties
         * @param context    the layout context
         * @return the security settings
         */
        @Override
        protected Component getSecuritySettings(IMObject object, PropertySet properties, LayoutContext context) {
            PropertySet children;
            List<Property> list = new ArrayList<>();
            settingsFocus = new FocusGroup("SecuritySettings");
            list.add(properties.get(SECURITY));
            list.add(authenticationMethodComponent.getProperty());
            Property username = properties.get(USERNAME);
            removeComponent(AUTHENTICATION_METHOD);
            if (getAuthenticationMethod() == MailServer.AuthenticationMethod.OAUTH2) {
                ComponentState auth = createComponentPair(authenticationMethodComponent,
                                                          createClientRegistration(properties));
                FocusGroup authFocus = auth.getFocusGroup();
                authFocus.add(authButton);
                auth = new ComponentState(RowFactory.create(Styles.CELL_SPACING, auth.getComponent(), authButton),
                                          auth.getProperty(), authFocus);
                showAuthoriseButton();
                addComponent(auth);
                list.add(username);
                MutablePropertySet set = new MutablePropertySet(new PropertySetImpl(list));
                set.setReadOnly(USERNAME);
                children = set;
            } else {
                addComponent(authenticationMethodComponent);
                list.add(username);
                list.add(properties.get(PASSWORD));
                children = new PropertySetImpl(list);
            }
            ComponentSet set = addComponents(new ComponentSet(settingsFocus), object, children.getProperties(),
                                             context);
            ComponentGrid grid = new ComponentGrid();
            grid.add(set, 1);
            return grid.createGrid(set);
        }

        /**
         * Changes the text and behaviour of the auth button, used to start and check the OAuth flow.
         *
         * @param key     the button resource bundle key
         * @param tooltip the tooltip resource bundle key. May be {@code null}
         * @param action  the action to execute
         */
        private void changeAuthButton(String key, String tooltip, Runnable action) {
            authButton.setText(Messages.get(key));
            authAction = action;
            authButton.setToolTipText(tooltip != null ? Messages.get(tooltip) : null);
        }

        /**
         * Creates a dropdown displaying the support OAuth2 client registrations.
         *
         * @param properties the settings properties
         * @return a new component
         */
        private ComponentState createClientRegistration(PropertySet properties) {
            Property property = properties.get(OAUTH2_CLIENT_REGISTRATION);
            LookupQuery query = new NodeLookupQuery(REGISTRATION_LOOKUP, "code");
            LookupField field = new BoundLookupField(new RequiredProperty(property), query);
            field.setStyleName(layoutContext.getComponentFactory().getStyle());
            return new ComponentState(field, property);
        }
    }

    private enum OAuth2Status {
        SUCCESS,
        INCOMPLETE,
        ERROR
    }
}