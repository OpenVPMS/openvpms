/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.list.ShortNameListCellRenderer;
import org.openvpms.web.component.im.list.ShortNameListModel;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.SelectFieldFactory;
import org.openvpms.web.echo.focus.FocusHelper;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Patient history query.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPatientHistoryQuery extends DateRangeActQuery<Act> {

    /**
     * User preferences.
     */
    private final Preferences preferences;

    /**
     * The act archetypes to query.
     */
    private String[] selectedArchetypes;

    /***
     * Determines if the items are being sorted ascending or descending.
     */
    private boolean sortAscending = true;

    /**
     * The archetype model.
     */
    private ShortNameListModel model;

    /**
     * The act archetype selector.
     */
    private SelectField archetypeSelector;

    /**
     * Button to change the sort order.
     */
    private Button sort;


    /**
     * Constructs a {@link AbstractPatientHistoryQuery}.
     *
     * @param patient     the patient to query
     * @param archetypes  the act archetypes
     * @param preferences the user preferences. May be {@code null}
     */
    public AbstractPatientHistoryQuery(Party patient, String[] archetypes, Preferences preferences) {
        super(patient, "patient", PatientArchetypes.PATIENT_PARTICIPATION, archetypes, Act.class);
        this.preferences = preferences;
        setAuto(true);
        boolean ascending = (preferences == null) || getSortAscending(preferences);
        setSortAscending(ascending);
    }

    /**
     * Sets the available archetypes to query.
     *
     * @param archetypes the archetypes
     */
    public void setAvailableArchetypes(String[] archetypes) {
        if (model == null) {
            model = new ShortNameListModel(archetypes, true, false);
            archetypeSelector = SelectFieldFactory.create(model);
            ActionListener listener = new ActionListener() {
                public void onAction(ActionEvent event) {
                    updateSelectedArchetypes(model, archetypeSelector.getSelectedIndex());
                    onQuery();
                }
            };
            archetypeSelector.addActionListener(listener);
            archetypeSelector.setCellRenderer(new ShortNameListCellRenderer());
        } else {
            archetypeSelector.setModel(new ShortNameListModel(archetypes, true, false));
        }
    }

    /**
     * Returns the available archetypes to query.
     *
     * @return the archetypes
     */
    public String[] getAvailableArchetypes() {
        return model != null ? model.getShortNames() : new String[0];
    }

    /**
     * Sets the archetypes to query.
     *
     * @param archetypes the archetypes
     */
    public void setSelectedArchetypes(String[] archetypes) {
        selectedArchetypes = archetypes;
    }

    /**
     * Returns the archetypes to query.
     *
     * @return the archetypes
     */
    public String[] getSelectedArchetypes() {
        return selectedArchetypes != null ? selectedArchetypes : new String[0];
    }

    /**
     * Determines if the visit items are being sorted ascending or descending.
     *
     * @param ascending if {@code true} visit items are to be sorted ascending; {@code false} if descending
     */
    public void setSortAscending(boolean ascending) {
        sortAscending = ascending;
        if (sort != null) {
            setSortIcon();
        }
        // update session preferences
        if (preferences != null) {
            preferences.setPreference(PreferenceArchetypes.HISTORY, "sort", ascending ? "ASC" : "DESC");
        }
    }

    /**
     * Determines if the visit items are being sorted ascending or descending.
     *
     * @return {@code true} if visit items are being sorted ascending; {@code false} if descending
     */
    public boolean isSortAscending() {
        return sortAscending;
    }

    /**
     * Returns the value being queried on.
     * <p>
     * This implementation does not append wildcards; all searches are 'contains'.
     *
     * @return the value. May be {@code null}
     */
    @Override
    public String getValue() {
        return StringUtils.trimToNull(getSearchField().getText());
    }

    /**
     * Invoked when the search field changes. Invokes {@link #onQuery} and resets the focus back to the search field.
     */
    @Override
    protected void onSearchFieldChanged() {
        super.onSearchFieldChanged();
        FocusHelper.setFocus(getSearchField());
    }

    /**
     * Returns the user preferences.
     *
     * @return the preferences. May be {@code null}
     */
    protected Preferences getPreferences() {
        return preferences;
    }

    /**
     * Returns the archetype selector.
     *
     * @return the archetype selector
     */
    protected SelectField getArchetypeSelector() {
        return archetypeSelector;
    }

    /**
     * Returns the sort button.
     *
     * @return the sort button
     */
    protected Button getSort() {
        if (sort == null) {
            sort = ButtonFactory.create(() -> {
                sortAscending = !sortAscending;
                setSortIcon();
                onQuery();
                FocusHelper.setFocus(sort);
            });
            setSortIcon();
        }
        return sort;
    }

    /**
     * Updates the archetypes to query.
     * <p>
     * This delegates to {@link #updateSelectedArchetypes(ShortNameListModel, int)}
     */
    protected void updateSelectedArchetypes() {
        updateSelectedArchetypes(model, archetypeSelector.getSelectedIndex());
    }

    /**
     * Updates the archetypes to query.
     *
     * @param model    the model
     * @param selected the selected index
     */
    protected abstract void updateSelectedArchetypes(ShortNameListModel model, int selected);

    /**
     * Determines if medical records within a visit are sorted on ascending or descending start time.
     *
     * @param preferences user preferences
     * @return the {@code true} if records are sorted on ascending start time, {@code false} if they are sorted
     * descending
     */
    protected boolean getSortAscending(Preferences preferences) {
        String sort = preferences.getString(PreferenceArchetypes.HISTORY, "sort", "ASC");
        return "ASC".equals(sort);
    }

    /**
     * Returns the available archetypes model.
     *
     * @return the model
     */
    ShortNameListModel getAvailableArchetypesModel() {
        return model;
    }

    /**
     * Sets the sort button icon.
     */
    private void setSortIcon() {
        String style;
        String toolTip;
        if (sortAscending) {
            style = "sort.ascending";
            toolTip = Messages.get("patient.record.query.sortAscending");
        } else {
            style = "sort.descending";
            toolTip = Messages.get("patient.record.query.sortDescending");
        }
        sort.setStyleName(style);
        sort.setToolTipText(toolTip);
    }

}
