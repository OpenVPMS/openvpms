/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.EntityQuery;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;

/**
 * ESCI administration workspace.
 *
 * @author Tim Anderson
 */
public class ESCIWorkspace extends ResultSetCRUDWorkspace<Party> {

    /**
     * Constructs an {@link ESCIWorkspace}.
     *
     * @param context the context
     */
    public ESCIWorkspace(Context context) {
        super("admin.esci", context);
        setArchetypes(Archetypes.create(SupplierArchetypes.SUPPLIER_ORGANISATION, Party.class));
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected EntityQuery<Party> createQuery() {
        EntityQuery<Party> query = new EntityQuery<>(new ESCIObjectSetQuery(), getContext());
        query.setAuto(true);
        return query;
    }

    /**
     * /**
     * Creates a new browser.
     *
     * @param query the query
     * @return a new browser
     */
    @Override
    protected ESCIBrowser createBrowser(Query<Party> query) {
        return new ESCIBrowser((EntityQuery<Party>) query, new DefaultLayoutContext(getContext(), getHelpContext()));
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<Party> createCRUDWindow() {
        ESCIBrowser browser = (ESCIBrowser) getBrowser();
        return new ESCICRUDWindow(getArchetypes(), browser, getContext(), getHelpContext());
    }
}
