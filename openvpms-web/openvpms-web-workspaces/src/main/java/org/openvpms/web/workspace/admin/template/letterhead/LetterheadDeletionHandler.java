/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template.letterhead;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.delete.AbstractEntityDeletionHandler;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Deletion handler for <em>entity.letterhead</em>
 * <p/>
 * This ensures that any associated logo is also deleted.
 *
 * @author Tim Anderson
 */
public class LetterheadDeletionHandler extends AbstractEntityDeletionHandler<Entity> {

    /**
     * The participations to check.
     */
    private final String[] participations;

    /**
     * Constructs a {@link LetterheadDeletionHandler}.
     *
     * @param object             the object to delete
     * @param factory            the editor factory
     * @param transactionManager the transaction manager
     * @param service            the archetype service
     */
    public LetterheadDeletionHandler(Entity object, IMObjectEditorFactory factory,
                                     PlatformTransactionManager transactionManager, IArchetypeRuleService service) {
        super(object, factory, transactionManager, service);
        String[] archetypes = DescriptorHelper.getShortNames(DEFAULT_PARTICIPATIONS, false, service);
        participations = ArrayUtils.removeElement(archetypes, "participation.logo");
    }

    /**
     * Returns the participation archetypes to check.
     *
     * @return the participation archetypes to check
     */
    @Override
    protected String[] getParticipations() {
        return participations;
    }
}
