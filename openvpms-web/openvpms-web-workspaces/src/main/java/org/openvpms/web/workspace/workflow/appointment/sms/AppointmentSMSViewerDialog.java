/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.sms;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.im.sms.SMSReplyUpdater;
import org.openvpms.web.component.im.sms.SMSViewerDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Viewer for appointment SMS.
 *
 * @author Tim Anderson
 */
public class AppointmentSMSViewerDialog extends SMSViewerDialog {

    /**
     * Constructs an {@link AppointmentSMSViewerDialog}.
     *
     * @param viewer the viewer
     */
    public AppointmentSMSViewerDialog(AppointmentSMSViewer viewer) {
        super(Messages.get("workflow.scheduling.appointment.sms.title"), viewer);
    }

    /**
     * Creates a dialog if the specified appointment has SMS.
     *
     * @param appointmentRef the appointment reference. May be {@code null}
     * @return a dialog to view SMS, or {@code null} if there is none
     */
    public static AppointmentSMSViewerDialog create(Reference appointmentRef) {
        AppointmentSMSViewerDialog result = null;
        if (appointmentRef != null) {
            ArchetypeService service = ServiceHelper.getArchetypeService();
            Act appointment = service.get(appointmentRef, Act.class);
            if (appointment != null) {
                IMObjectBean bean = service.getBean(appointment);
                List<Act> sms = bean.getTargets("appointmentSMS", Act.class);
                if (!sms.isEmpty()) {
                    AppointmentSMSViewer viewer = new AppointmentSMSViewer(appointment, sms, service,
                                                                           ServiceHelper.getBean(SMSReplyUpdater.class),
                                                                           ServiceHelper.getBean(ActionFactory.class));
                    result = new AppointmentSMSViewerDialog(viewer);
                }
            }
        }
        return result;
    }
}