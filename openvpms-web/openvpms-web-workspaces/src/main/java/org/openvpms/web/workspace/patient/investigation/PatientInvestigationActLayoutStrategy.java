/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.doc.DocumentEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationCollectionEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.LookupFilter;
import org.openvpms.web.component.im.lookup.LookupQuery;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.print.IMObjectReportPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.PatientDocumentActLayoutStrategy;
import org.openvpms.web.workspace.patient.mr.PatientInvestigationFormReporter;

import java.util.List;


/**
 * Layout strategy that includes a 'Print Form' button to print the act.
 *
 * @author Tim Anderson
 */
public class PatientInvestigationActLayoutStrategy extends PatientDocumentActLayoutStrategy {

    /**
     * Order id node name.
     */
    public static final String ORDER_ID = "orderId";

    /**
     * Report id node name.
     */
    public static final String REPORT_ID = "reportId";

    /**
     * Order status node name.
     */
    public static final String ORDER_STATUS = "status2";

    /**
     * Laboratory node name.
     */
    public static final String LABORATORY = "laboratory";

    /**
     * Device node name.
     */
    public static final String DEVICE = "device";

    /**
     * Summary node name.
     */
    public static final String SUMMARY = "summary";

    /**
     * Status node name.
     */
    public static final String STATUS = "status";

    /**
     * Tests node name.
     */
    public static final String TESTS = "tests";

    /**
     * Products node name.
     */
    public static final String PRODUCTS = "products";

    /**
     * Investigation type node name.
     */
    public static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Results node name.
     */
    public static final String RESULTS = "results";

    /**
     * Versions node name.
     */
    public static final String VERSIONS = "versions";

    /**
     * Message node name.
     */
    public static final String MESSAGE = "message";

    /**
     * Determines if printing should be enabled.
     */
    private boolean enablePrint = true;

    /**
     * Determines if the order and status message should be rendered inline.
     */
    private boolean inlineOrderStatusMessage = false;

    /**
     * Investigation id node name.
     */
    private static final String ID = "id";


    /**
     * Constructs a {@link PatientInvestigationActLayoutStrategy}.
     */
    public PatientInvestigationActLayoutStrategy() {
        this(null, null, null, null, false);
    }

    /**
     * Constructs a {@link PatientInvestigationActLayoutStrategy}.
     *
     * @param editor            the document reference editor. May be {@code null}
     * @param versionsEditor    the document version editor. May be {@code null}
     * @param deviceEditor      the device editor. May be {@code null}
     * @param orderStatusEditor the order status editor. If {@code null}, the order status is read-only
     * @param locked            determines if the record is locked
     */
    public PatientInvestigationActLayoutStrategy(DocumentEditor editor, ActRelationshipCollectionEditor versionsEditor,
                                                 ParticipationCollectionEditor deviceEditor,
                                                 PropertyComponentEditor orderStatusEditor, boolean locked) {
        super(editor, versionsEditor, locked);
        if (deviceEditor != null) {
            addComponent(new ComponentState(deviceEditor));
        }
        if (orderStatusEditor != null) {
            addComponent(new ComponentState(orderStatusEditor));
        }
    }

    /**
     * Determines if the button should be enabled.
     *
     * @param enable if {@code true}, enable the button
     */
    public void setEnableButton(boolean enable) {
        enablePrint = enable;
    }

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        ArchetypeNodes nodes = new ArchetypeNodes(EDIT_NODES)
                .excludeIfEmpty(ORDER_ID, REPORT_ID, MESSAGE, TESTS, PRODUCTS, RESULTS, VERSIONS, LABORATORY)
                .exclude(SUMMARY);
        if (!context.isEdit()) {
            nodes.excludeIfEmpty(DEVICE);
        }

        if (enablePrint || !object.isNew()) {
            // force the id node to display, overriding any filter
            nodes.simple(ID);
        }

        IMObjectBean bean = getBean(object);

        if (enablePrint) {
            // display the print button to the right of the id
            Button print = ButtonFactory.create("button.printform", () -> printForm((DocumentAct) object, context));
            ComponentState id = createComponent(properties.get(ID), object, context);
            Row row = RowFactory.create(Styles.WIDE_CELL_SPACING, id.getComponent(),
                                        RowFactory.create(RowFactory.rightAlign(), print));
            addComponent(new ComponentState(row, id.getProperty()));
        }
        Entity laboratory = (Entity) context.getCache().get(bean.getTargetRef(LABORATORY));
        IMObjectComponentFactory factory = context.getComponentFactory();
        if (context.isEdit()) {
            boolean hasLaboratory = laboratory != null;
            if (hasLaboratory || isLocked()) {
                // note that this replaces any prior product registration
                if (hasLaboratory) {
                    // the laboratory is responsible for the status (with the exception of POSTED, which is done
                    // via record locking)
                    addComponent(factory.create(createReadOnly(properties.get(STATUS)), object));
                } else {
                    addComponent(createStatus(object, properties));
                }
                addComponent(factory.create(createReadOnly(properties.get(INVESTIGATION_TYPE)), object));
            }
            boolean editDevice = false;
            if (hasLaboratory) {
                Property orderStatus = properties.get(ORDER_STATUS);
                editDevice = InvestigationActStatus.PENDING.equals(orderStatus.getString());
            }
            if (!editDevice) {
                if (bean.getTargetRef(DEVICE) != null) {
                    addComponent(factory.create(createReadOnly(properties.get(DEVICE)), object));
                } else {
                    nodes.exclude(DEVICE);
                }
            }
        } else {
            addComponent(createDocumentViewer(object, properties, context));
        }

        Property message = properties.get(MESSAGE);
        if (message != null && message.getString() != null) {
            // if there is a message, display the order status and message next to each other, with the message
            // displaying over multiple lines if required.
            inlineOrderStatusMessage = true;
            ComponentState orderStatus = getComponent(ORDER_STATUS);
            if (orderStatus == null) {
                orderStatus = createComponent(properties.get(ORDER_STATUS), object, context);
            }

            ComponentState text = createMultiLineText(message, 1, 20, Styles.FULL_WIDTH, context);
            text.getComponent().setLayoutData(RowFactory.layout(Styles.FULL_WIDTH));

            addComponent(createComponentPair(orderStatus, text));
            nodes.exclude(PatientInvestigationActLayoutStrategy.ORDER_STATUS, MESSAGE);
            // need to render order status explicitly. See doSimpleLayout()
        } else {
            inlineOrderStatusMessage = false;
        }

        Property summary = properties.get(SUMMARY);
        if (summary.getString() != null) {
            // will display as a single line string if short
            addComponent(createMultiLineText(summary, 1, 20, Styles.FULL_WIDTH, context));
        }
        setArchetypeNodes(nodes);
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out child components in a grid.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doSimpleLayout(final IMObject object, IMObject parent, List<Property> properties,
                                  Component container, final LayoutContext context) {
        ComponentGrid grid = createGrid(object, properties, context);
        if (inlineOrderStatusMessage) {
            // the order status has been pre-created in order to include a message. Display this over both column groups
            ComponentState orderStatus = getComponent(ORDER_STATUS);
            if (orderStatus != null) {
                grid.add(orderStatus, 2);
            }
        }
        addAuditInfo(object, grid, context);
        ComponentState summary = getComponent(SUMMARY);
        if (summary != null) {
            grid.add(summary, 2);
        }
        container.add(ColumnFactory.create(Styles.INSET, createGrid(grid)));
    }

    /**
     * Determines if a property should be made read-only when the act is locked.
     *
     * @param property the property
     * @return {@code true} if the property should be made read-only
     */
    @Override
    protected boolean makeReadOnly(Property property) {
        String name = property.getName();
        return !property.isReadOnly() && !ORDER_STATUS.equals(name);
    }

    /**
     * Returns a component to view the document, if any.
     *
     * @param object  the investigation
     * @param context the layout context
     * @return the document viewer
     */
    private ComponentState createDocumentViewer(IMObject object, PropertySet properties, LayoutContext context) {
        Property property = properties.get(DOCUMENT);
        InvestigationViewer viewer = new InvestigationViewer((DocumentAct) object, true, context);
        return new ComponentState(viewer.getComponent(), property);
    }

    /**
     * Creates a component for the status node.
     * <p/>
     * If the act is POSTED or CANCELLED, this restricts the status to either of those values.
     *
     * @param object     the object
     * @param properties the properties
     * @return the status component
     */
    private ComponentState createStatus(IMObject object, PropertySet properties) {
        Property property = properties.get(STATUS);
        LookupQuery query = new NodeLookupQuery(object, property);
        String status = property.getString();
        if (ActStatus.POSTED.equals(status) || ActStatus.CANCELLED.equals(status)) {
            query = new LookupFilter(query, true, ActStatus.POSTED, ActStatus.CANCELLED);
        }
        LookupField field = LookupFieldFactory.create(property, query);
        return new ComponentState(field, property);
    }

    /**
     * Prints the form associated with an investigation, if any.
     *
     * @param investigation the investigation
     * @param layoutContext the layout context
     */
    private static void printForm(DocumentAct investigation, LayoutContext layoutContext) {
        ReporterFactory reporterFactory = ServiceHelper.getBean(ReporterFactory.class);
        Context context = layoutContext.getContext();
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(investigation, context);
        PatientInvestigationFormReporter reporter = reporterFactory.create(investigation, locator,
                                                                           PatientInvestigationFormReporter.class);
        if (reporter.hasDocument()) {
            IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
            IMObjectReportPrinter<DocumentAct> printer = factory.createIMObjectReportPrinter(reporter, context);
            InteractiveIMPrinter<DocumentAct> interactive = new InteractiveIMPrinter<>(printer, context,
                                                                                       layoutContext.getHelpContext());
            interactive.setMailContext(layoutContext.getMailContext());
            interactive.print();
        } else {
            InformationDialog.show(Messages.get("printdialog.title"), Messages.get("investigation.print.noform"));
        }
    }
}