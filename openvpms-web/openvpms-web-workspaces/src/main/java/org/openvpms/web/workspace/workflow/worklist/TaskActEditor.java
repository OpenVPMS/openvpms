/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import nextapp.echo2.app.Extent;
import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.workflow.scheduling.AbstractScheduleActEditor;

import java.util.Calendar;
import java.util.Date;

import static org.openvpms.archetype.rules.act.ActStatus.CANCELLED;
import static org.openvpms.archetype.rules.act.ActStatus.COMPLETED;
import static org.openvpms.archetype.rules.act.ActStatus.IN_PROGRESS;

/**
 * Base class for <em>act.customerTask</em> editors.
 *
 * @author Tim Anderson
 */
public abstract class TaskActEditor extends AbstractScheduleActEditor {

    /**
     * The work list node name.
     */
    protected static final String WORK_LIST = "worklist";

    /**
     * The task type node name.
     */
    protected static final String TASK_TYPE = "taskType";

    /**
     * The status node name.
     */
    protected static final String STATUS = "status";

    /**
     * The consult start time node name.
     */
    protected static final String CONSULT_START_TIME = "consultStartTime";

    /**
     * Constructs a {@link TaskActEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public TaskActEditor(Act act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        if (act.isNew()) {
            initParticipant(CUSTOMER, context.getContext().getCustomer());
            initParticipant(PATIENT, context.getContext().getPatient());
        }

        if (getStartTime() == null) {
            Date date = context.getContext().getWorkListDate();
            if (date != null) {
                setStartTime(getDefaultStartTime(date), true);
            }
        }

        addStartEndTimeListeners();

        getProperty(STATUS).addModifiableListener(modifiable -> onStatusChanged());
    }

    /**
     * Sets the work list.
     *
     * @param workList the work list
     */
    public void setWorkList(Entity workList) {
        setParticipant(WORK_LIST, workList);
        TaskTypeParticipationEditor editor = getTaskTypeEditor();
        if (editor != null) {
            editor.setWorkList(workList);
        }
    }

    /**
     * Returns the work list.
     *
     * @return the work list. May be {@code null}
     */
    public Entity getWorkList() {
        return getParticipant(WORK_LIST);
    }

    /**
     * Sets the task type.
     *
     * @param taskType the task type
     */
    public void setTaskType(Entity taskType) {
        setParticipant(TASK_TYPE, taskType);
    }

    /**
     * Returns the task type.
     *
     * @return the task type. May be {@code null}
     */
    public Entity getTaskType() {
        return getParticipant(TASK_TYPE);
    }

    /**
     * Returns the clinician.
     *
     * @return the clinician. May be {@code null}
     */
    public User getClinician() {
        return (User) getParticipant("clinician");
    }

    /**
     * Validates the object.
     * <p/>
     * This extends validation by ensuring that there are not too many tasks.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean result = super.doValidation(validator);
        if (result) {
            result = checkMaxSlots();
        }
        return result;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Invoked when layout has completed. All editors have been created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();

        Party workList = (Party) getParticipant(WORK_LIST);
        TaskTypeParticipationEditor editor = getTaskTypeEditor();
        if (editor != null) {
            editor.setWorkList(workList);
        }
    }

    /**
     * Returns the task type editor.
     *
     * @return the task type editor
     */
    protected TaskTypeParticipationEditor getTaskTypeEditor() {
        return (TaskTypeParticipationEditor) getParticipationEditor(TASK_TYPE, true);
    }

    /**
     * Invoked when the status changes. Sets the end time to today if the
     * status is 'Completed' or 'Cancelled', or {@code null} if it is 'Pending'
     */
    protected void onStatusChanged() {
        Property status = getProperty(STATUS);
        Date time = null;
        String value = (String) status.getValue();
        if (COMPLETED.equals(value) || CANCELLED.equals(value)) {
            time = new Date();
        }
        setEndTime(time, false);

        if (IN_PROGRESS.equals(value)) {
            getProperty(CONSULT_START_TIME).setValue(new Date());
        }
    }

    /**
     * Calculates the default start time of a task. If the date is today, this returns
     * the current time truncated to minutes (as editing is only supported to minutes), otherwise it returns the
     * supplied date and time 00:00.
     *
     * @param date the start date
     * @return the start time
     */
    protected Date getDefaultStartTime(Date date) {
        if (DateRules.isToday(date)) {
            date = DateUtils.truncate(new Date(), Calendar.MINUTE);
        } else {
            date = DateRules.getDate(date);
        }
        return date;
    }

    /**
     * Determines if there are enough slots available to save the task.
     *
     * @return {@code true} if there are less than maxSlots tasks, otherwise {@code false}
     */
    private boolean checkMaxSlots() {
        boolean result;
        Act act = getObject();
        if (TaskQueryHelper.tooManyTasks(act)) {
            String title = Messages.get("workflow.worklist.toomanytasks.title");
            String message = Messages.get("workflow.worklist.toomanytasks.message");
            ErrorDialog.show(title, message);
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    protected class LayoutStrategy extends AbstractLayoutStrategy {

        /**
         * Constructs an {@link AbstractLayoutStrategy}.
         */
        public LayoutStrategy() {
            addComponent(new ComponentState(getStartTimeEditor()));
            addComponent(new ComponentState(getEndTimeEditor()));
        }

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            Property notes = properties.get("notes");
            if (notes != null) {
                addComponent(createMultiLineText(notes, 2, 5, new Extent(50, Extent.EX), context));
            }
            return super.apply(object, properties, parent, context);
        }
    }
}
