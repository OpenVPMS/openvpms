/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.settings;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.report.openoffice.OpenOfficeService;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.openoffice.OpenOfficeStatus;

/**
 * OpenOffice settings.
 *
 * @author Tim Anderson
 */
public class OpenOfficeSettings extends SettingsTab {

    /**
     * The OpenOffice status.
     */
    private final OpenOfficeStatus status;

    /**
     * Refresh button identifier.
     */
    private static final String REFRESH_ID = "button.refresh";

    /**
     * Restart button identifier.
     */
    private static final String RESTART_ID = "button.restartopenoffice";

    /**
     * The buttons to display.
     */
    private static final String[] BUTTONS = {REFRESH_ID, RESTART_ID};

    /**
     * Constructs an {@link OpenOfficeSettings}.
     *
     * @param help the help context
     */
    public OpenOfficeSettings(HelpContext help) {
        super(SettingsArchetypes.OPEN_OFFICE, help, BUTTONS);
        status = new OpenOfficeStatus(ServiceHelper.getBean(OpenOfficeService.class),
                                      ServiceHelper.getArchetypeService());
    }

    /**
     * Invoked when the tab is displayed.
     */
    @Override
    public void show() {
        Component component = getComponent();
        component.removeAll();
        component.add(ColumnFactory.create(Styles.LARGE_INSET, status.getComponent()));
    }

    /**
     * Invoked when a button is pressed.
     *
     * @param button the button identifier
     */
    @Override
    public void onButton(String button) {
        if (REFRESH_ID.equals(button)) {
            status.refresh();
        } else if (RESTART_ID.equals(button)) {
            restart();
        }
    }

    /**
     * Determines if OpenOffice is configured.
     *
     * @return {@code true} if OpenOffice is configured, otherwise {@code false}
     */
    public static boolean isAvailable() {
        return ServiceHelper.getContext().getBeanNamesForType(OpenOfficeService.class).length > 0;
    }

    /**
     * Invoked when the settings have changed.
     * <p/>
     * This displays the new settings and prompts to restart OpenOffice.
     *
     * @param settings the updated settings
     */
    @Override
    protected void onChanged(Entity settings) {
        super.onChanged(settings);
        restart();
    }

    /**
     * Prompts to restart OpenOffice.
     */
    private void restart() {
        ConfirmationDialog.newDialog()
                .title(Messages.get("admin.system.openoffice.title"))
                .message(Messages.get("admin.system.openoffice.restart"))
                .yesNo()
                .yes(() -> {
                    ServiceHelper.getBean(OpenOfficeService.class).restart();
                    show();
                })
                .show();
    }
}
