/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.CheckBox;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.table.act.ActAmountTableModel;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.echo.table.TableColumnFactory;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;

/**
 * Customer account table model.
 * <p/>
 * Optionally includes a column for the 'hide' node.
 *
 * @author Tim Anderson
 */
public class CustomerAccountActTableModel<T extends Act> extends ActAmountTableModel<T> {

    /**
     * The notes node.
     */
    private static final String NOTES = "notes";

    /**
     * The hide node.
     */
    private static final String HIDE = "hide";

    /**
     * The notes column model index.
     */
    private static final int NOTES_INDEX = AMOUNT_INDEX + 1;

    /**
     * The hide column model index.
     */
    private static final int HIDE_INDEX = NOTES_INDEX + 1;

    /**
     * Constructs a {@link CustomerAccountActTableModel}.
     */
    public CustomerAccountActTableModel() {
        this(false, true);
    }

    /**
     * Constructs a {@link CustomerAccountActTableModel}.
     *
     * @param showStatus if {@code true},  show the status column
     * @param showHide   if {@code true}, show the hide column
     */
    public CustomerAccountActTableModel(boolean showStatus, boolean showHide) {
        super(true, showStatus, true, false, false);
        if (showHide) {
            getColumnModel().addColumn(createTableColumn(HIDE_INDEX, "customer.account.table.hide"));
        }
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param act    the act
     * @param column the table column
     * @param row    the table row
     * @return the value at the given coordinate
     */
    @Override
    protected Object getValue(T act, TableColumn column, int row) {
        Object result = null;
        if (column.getModelIndex() == NOTES_INDEX) {
            IMObjectBean bean = getBean(act);
            if (bean.hasNode(NOTES)) {
                result = StringUtils.abbreviate(bean.getString(NOTES), 80);
            }
        } else if (column.getModelIndex() == HIDE_INDEX) {
            IMObjectBean bean = getBean(act);
            if (bean.hasNode(HIDE) && bean.getBoolean(HIDE)) {
                CheckBox checkBox = CheckBoxFactory.create(true);
                checkBox.setEnabled(false);
                result = checkBox;
            }
        } else {
            result = super.getValue(act, column, row);
        }
        return result;
    }

    /**
     * Helper to create a column model.
     * <p/>
     * Adds a hide column to indicate if an act is being suppressed in statements.
     *
     * @param showArchetype   determines if the archetype column should be displayed
     * @param showStatus      determines if the status column should be displayed
     * @param showAmount      determines if the credit/debit amount should be displayed
     * @param showDescription determines if the description column should be displayed
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(boolean showArchetype, boolean showStatus, boolean showAmount,
                                                 boolean showDescription) {
        TableColumnModel model = super.createColumnModel(showArchetype, showStatus, showAmount, showDescription);
        model.addColumn(TableColumnFactory.create(NOTES_INDEX, getDisplayName(INVOICE, NOTES)));
        return model;
    }
}
