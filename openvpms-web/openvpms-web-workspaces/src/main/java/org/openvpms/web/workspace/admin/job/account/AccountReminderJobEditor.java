/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.account;

import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * An editor for <em>entity.jobAccountReminder</em>.
 *
 * @author Tim Anderson
 */
public class AccountReminderJobEditor extends AbstractIMObjectEditor {

    /**
     * The ignore interval node name.
     */
    static final String IGNORE_INTERVAL = "ignoreInterval";

    /**
     * The ignore interval units node name.
     */
    static final String IGNORE_INTERVAL_UNITS = "ignoreIntervalUnits";

    /**
     * The reminders node name.
     */
    private static final String REMINDERS = "reminders";

    /**
     * Constructs a {@link AccountReminderJobEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AccountReminderJobEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        addEditor(new AccountReminderCollectionEditor(getCollectionProperty(REMINDERS), object, layoutContext));
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new AccountReminderJobEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        AccountReminderCollectionEditor reminders = getReminders();
        strategy.addComponent(new ComponentState(reminders));
        return strategy;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateIntervals(validator);
    }

    /**
     * Ensures that the intervals are valid.
     *
     * @param validator the validator
     * @return {@code true} if the intervals are valid, otherwise {@code false}
     */
    protected boolean validateIntervals(Validator validator) {
        boolean valid = true;
        Date now = DateRules.getToday();
        Period ignore = PeriodHelper.getPeriod(getBean(getObject()), IGNORE_INTERVAL, IGNORE_INTERVAL_UNITS);
        Date ignoreFrom = DateRules.minus(now, ignore);
        AccountReminderCollectionEditor reminders = getReminders();
        Set<Date> dates = new HashSet<>();
        for (IMObject reminder : reminders.getObjects()) {
            IMObjectEditor editor = reminders.getEditor(reminder);
            int interval = editor.getProperty("interval").getInt();
            DateUnits units = DateUnits.fromString(editor.getProperty("units").getString(), DateUnits.DAYS);
            Date date = DateRules.getDate(now, -interval, units);
            if (!dates.add(date)) {
                valid = false;
                validator.add(this, new ValidatorError(Messages.format("accountreminder.duplicateinterval",
                                                                       DateFormatter.formatInterval(interval, units))));
                break;
            } else if (date.compareTo(ignoreFrom) < 0) {
                valid = false;
                String message = Messages.format("accountreminder.ignoreintervalmustbegreater",
                                                 getProperty(IGNORE_INTERVAL).getDisplayName(),
                                                 DateFormatter.formatInterval(interval, units));
                validator.add(this, new ValidatorError(message));
                break;
            }
        }
        return valid;
    }

    /**
     * Returns the reminder collection editor.
     *
     * @return the editor
     */
    private AccountReminderCollectionEditor getReminders() {
        return (AccountReminderCollectionEditor) getEditor(REMINDERS, false);
    }
}