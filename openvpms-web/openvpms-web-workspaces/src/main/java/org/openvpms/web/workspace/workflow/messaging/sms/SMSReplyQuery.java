/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.LookupFilter;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.ActStatuses;

/**
 * Query for <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class SMSReplyQuery extends AbstractSMSQuery {

    /**
     * The act statuses. Exclude the <em>READ</em> status, as it will be handled explicitly whenever <em>PENDING</em>
     * is selected.
     */
    private static final ActStatuses STATUSES = new ActStatuses(
            new LookupFilter(new NodeLookupQuery(SMSArchetypes.REPLY, "status"), false, MessageStatus.READ), null);

    /**
     * The default statuses to query.
     */
    private static final String[] DEFAULT_STATUSES = {MessageStatus.PENDING, MessageStatus.READ};

    /**
     * Constructs a {@link SMSReplyQuery}.
     *
     * @param context the layout context
     */
    public SMSReplyQuery(LayoutContext context) {
        super(SMSArchetypes.REPLY, STATUSES, context);
        setStatus(MessageStatus.PENDING);
    }

    /**
     * Returns the act statuses to query.
     * <p/>
     * If the status is <em>PENDING</em>, this also includes <em>READ</em> acts.
     *
     * @return the act statuses to query
     */
    @Override
    protected String[] getStatuses() {
        String[] statuses = super.getStatuses();
        if (statuses.length == 1 && statuses[0].equals(MessageStatus.PENDING)) {
            statuses = DEFAULT_STATUSES;
        }
        return statuses;
    }

}
