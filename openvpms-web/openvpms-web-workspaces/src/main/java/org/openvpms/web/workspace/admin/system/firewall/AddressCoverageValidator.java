/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.firewall;

import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.component.business.dao.im.security.IUserDAO;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.echo.servlet.SessionMonitor;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validator to ensure that allowed addresses cover the current logged-in users.
 * <p/>
 * This is to avoid configuring allowed addresses that exclude current users.
 *
 * @author Tim Anderson
 */
class AddressCoverageValidator {

    /**
     * Denotes an address of a current session that is excluded by the list of allowed addresses.
     */
    public static class Excluded {

        private final String address;

        private final String user;

        /**
         * Constructs an {@link Excluded}.
         *
         * @param address the address
         * @param user    the logged-in username
         */
        public Excluded(String address, String user) {
            this.address = address;
            this.user = user;
        }

        /**
         * Returns the address.
         *
         * @return the address
         */
        public String getAddress() {
            return address;
        }

        /**
         * Returns the logged-in username.
         *
         * @return the username
         */
        public String getUser() {
            return user;
        }
    }

    /**
     * The session monitor.
     */
    private final SessionMonitor sessionMonitor;

    /**
     * The users service.
     */
    private final IUserDAO users;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Cache of connectFromAnywhere flags, keyed on username.
     */
    private final Map<String, Boolean> connectFromAnywhere = new HashMap<>();

    /**
     * Constructs an {@link AddressCoverageValidator}.
     *
     * @param sessionMonitor the session monitor
     * @param users          the users service
     * @param service        the archetype service
     */
    public AddressCoverageValidator(SessionMonitor sessionMonitor, IUserDAO users, ArchetypeService service) {
        this.sessionMonitor = sessionMonitor;
        this.users = users;
        this.service = service;
    }

    /**
     * Finds the first address of current sessions that is excluded by the list of allowed addresses.
     *
     * @param addresses  the allowed addresses
     * @param accessType the firewall accessType. Only valid for {@code ALLOWED_ONLY} and {@code ALLOWED_USER}
     * @return the first excluded address or {@code null} if no address is excluded
     */
    public Excluded getFirstExcludedAddress(List<String> addresses, FirewallSettings.AccessType accessType) {
        Excluded result = null;
        boolean connectFromAnywhere = accessType == FirewallSettings.AccessType.ALLOWED_USER;
        List<IpAddressMatcher> matchers = getMatchers(addresses);
        for (SessionMonitor.Session session : sessionMonitor.getSessions()) {
            String host = session.getHost();
            if (host != null && (!connectFromAnywhere || !userCanConnectFromAnywhere(session.getName()))) {
                boolean found = false;
                for (IpAddressMatcher matcher : matchers) {
                    if (matcher.matches(host)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    result = new Excluded(host, session.getName());
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Determines if a user can connect from anywhere.
     *
     * @param name the username
     * @return {@code true} if the user can connect from anywhere, otherwise {@code false}
     */
    private boolean userCanConnectFromAnywhere(String name) {
        return connectFromAnywhere.computeIfAbsent(name, username -> {
            boolean result = false;
            User user = users.getUser(username);
            if (user != null) {
                IMObjectBean bean = service.getBean(user);
                result = bean.getBoolean("connectFromAnywhere");
            }
            return result;
        });
    }

    /**
     * Returns address matchers for each address.
     *
     * @param addresses the allowed addresses
     * @return the corresponding address matchers
     */
    private List<IpAddressMatcher> getMatchers(List<String> addresses) {
        List<IpAddressMatcher> result = new ArrayList<>();
        for (String address : addresses) {
            result.add(new IpAddressMatcher(address));
        }
        return result;
    }
}
