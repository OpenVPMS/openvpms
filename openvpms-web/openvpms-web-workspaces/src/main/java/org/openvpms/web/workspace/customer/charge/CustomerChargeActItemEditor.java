/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.tax.TaxRuleException;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.bound.BoundProperty;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.Quantity;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.edit.act.TemplateProduct;
import org.openvpms.web.component.im.edit.reminder.ReminderEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientActEditor;
import org.openvpms.web.component.im.product.BatchParticipationEditor;
import org.openvpms.web.component.im.product.FixedPriceEditor;
import org.openvpms.web.component.im.product.ProductParticipationEditor;
import org.openvpms.web.component.im.util.LookupNameHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.PriceActItemEditor;
import org.openvpms.web.workspace.customer.charge.TaskActRelationshipCollectionEditor.TaskType;
import org.openvpms.web.workspace.customer.charge.department.DepartmentListener;
import org.openvpms.web.workspace.patient.PatientIdentityEditor;
import org.openvpms.web.workspace.patient.investigation.PatientInvestigationActEditor;
import org.openvpms.web.workspace.patient.mr.PatientAlertEditor;
import org.openvpms.web.workspace.patient.mr.PatientMedicationActEditor;
import org.openvpms.web.workspace.patient.mr.PrescriptionMedicationActEditor;
import org.openvpms.web.workspace.patient.mr.Prescriptions;
import org.openvpms.web.workspace.workflow.worklist.TaskActEditor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static java.math.BigDecimal.ZERO;
import static org.openvpms.archetype.rules.math.MathRules.ONE_HUNDRED;
import static org.openvpms.archetype.rules.product.ProductArchetypes.MEDICATION;
import static org.openvpms.archetype.rules.product.ProductArchetypes.MERCHANDISE;
import static org.openvpms.web.echo.style.Styles.CELL_SPACING;
import static org.openvpms.web.echo.style.Styles.WIDE_CELL_SPACING;


/**
 * An editor for {@link Act}s which have an archetype of
 * <em>act.customerAccountInvoiceItem</em>,
 * <em>act.customerAccountCreditItem</em>
 * or <em>act.customerAccountCounterItem</em>.
 *
 * @author Tim Anderson
 */
public abstract class CustomerChargeActItemEditor extends PriceActItemEditor {

    /**
     * The product participation editor.
     * <p>
     * This needs to be created outside of the automatic layout, in order to ensure events
     * aren't lost when the layout changes.
     */
    private final ParticipationCollectionEditor productCollectionEditor;

    /**
     * Listener for changes to the quantity.
     */
    private final ModifiableListener quantityListener;

    /**
     * Listener for changes to the fixed price, quantity and unit price, to update the discount and total.
     */
    private final ModifiableListener calculationListener;

    /**
     * Listener for changes to the discount, to update the total.
     */
    private final ModifiableListener discountListener;

    /**
     * Listener for changes to the total, so the tax amount can be recalculated, and print flag enabled/disabled.
     */
    private final ModifiableListener totalListener;

    /**
     * Listener for changes to the batch.
     */
    private final ModifiableListener batchListener;

    /**
     * The quantity.
     */
    private final Quantity quantity;

    /**
     * The stock on hand.
     */
    private final StockQuantity stockQuantity;

    /**
     * The investigations.
     */
    private final InvestigationManager investigations;

    /**
     * Dispensing act editor. May be {@code null}
     */
    private final DispensingActRelationshipCollectionEditor dispensing;

    /**
     * Reminders act editor. May be {@code null}
     */
    private final ActRelationshipCollectionEditor reminders;

    /**
     * Alerts act editor. May be {@code null}
     */
    private final ActRelationshipCollectionEditor alerts;

    /**
     * Tasks act editor. May be {@code null}
     */
    private final TaskActRelationshipCollectionEditor tasks;

    /**
     * Documents manager. May be {@code null}
     */
    private final ChargeItemDocumentManager documents;

    /**
     * Selling units label.
     */
    private final Label sellingUnits;

    /**
     * The previous quantity. This is used to ensure that a quantity cannot be changed from positive to negative
     * and vice-versa if the charge item has been saved.
     */
    private BigDecimal previousQuantity;

    /**
     * Button to launch the medication editor.
     */
    private Button medicationButton;

    /**
     * If {@code true}, prompt to use prescriptions.
     */
    private boolean promptForPrescription = true;

    /**
     * If {@code true}, enable medication editing to be cancelled when it is being dispensed from a prescription.
     */
    private boolean cancelPrescription;

    /**
     * Listener for department changes. May be {@code null}
     */
    private DepartmentListener departmentListener;

    /**
     * The logger.
     */
    private static final Log log = LogFactory.getLog(CustomerChargeActItemEditor.class);

    /**
     * Department node name.
     */
    private static final String DEPARTMENT = "department";


    /**
     * Dispensing node name.
     */
    private static final String DISPENSING = "dispensing";

    /**
     * Reminders node name.
     */
    private static final String REMINDERS = "reminders";

    /**
     * Alerts node name.
     */
    private static final String ALERTS = "alerts";

    /**
     * Tasks node name.
     */
    private static final String TASKS = "tasks";

    /**
     * Returned quantity node name.
     */
    private static final String RETURNED_QUANTITY = "returnedQuantity";

    /**
     * Received quantity node name.
     */
    private static final String RECEIVED_QUANTITY = "receivedQuantity";

    /**
     * Pharmacy order nodes.
     */
    private static final String[] ORDER_NODES = {RECEIVED_QUANTITY, RETURNED_QUANTITY};

    /**
     * Investigations node name.
     */
    private static final String INVESTIGATIONS = "investigations";

    /**
     * The documents node name.
     */
    private static final String DOCUMENTS = "documents";

    /**
     * Fixed cost node name.
     */
    private static final String FIXED_COST = "fixedCost";

    /**
     * Unit cost node name.
     */
    private static final String UNIT_COST = "unitCost";

    /**
     * Tax node name.
     */
    private static final String TAX = "tax";

    /**
     * Total node name.
     */
    private static final String TOTAL = "total";

    /**
     * The print aggregate node name.
     */
    private static final String PRINT_AGGREGATE = "printAggregate";

    /**
     * The stock location node name.
     */
    private static final String STOCK_LOCATION = "stockLocation";

    /**
     * The batch node name.
     */
    private static final String BATCH = "batch";

    /**
     * Nodes to use when a product template is selected.
     */
    private static final ArchetypeNodes TEMPLATE_NODES = new ArchetypeNodes().exclude(
            QUANTITY, FIXED_PRICE, UNIT_PRICE, DISCOUNT, CLINICIAN, TOTAL, DISPENSING,
            REMINDERS, ALERTS, TASKS, BATCH);

    /**
     * Patient identity node name.
     */
    private static final String PATIENT_IDENTITY = "patientIdentity";

    /**
     * Constructs a {@link CustomerChargeActItemEditor}.
     * <p>
     * This recalculates the tax amount.
     *
     * @param act           the act to edit
     * @param parent        the parent act
     * @param context       the edit context
     * @param layoutContext the layout context
     */
    public CustomerChargeActItemEditor(FinancialAct act, Act parent, CustomerChargeEditContext context,
                                       LayoutContext layoutContext) {
        super(act, parent, context, layoutContext);
        if (!act.isA(CustomerAccountArchetypes.INVOICE_ITEM, CustomerAccountArchetypes.CREDIT_ITEM,
                     CustomerAccountArchetypes.COUNTER_ITEM)) {
            throw new IllegalArgumentException("Invalid act type:" + act.getArchetype());
        }

        Property quantityProperty = getProperty(QUANTITY);
        LayoutContext layout = getLayoutContext();
        previousQuantity = quantityProperty.getBigDecimal(ZERO);
        quantity = new Quantity(quantityProperty, act, layout);
        stockQuantity = new StockQuantity(act, context.getStock(), layout);
        productCollectionEditor = createProductCollectionEditor();
        dispensing = createDispensingCollectionEditor();
        reminders = createRemindersCollectionEditor();
        alerts = createAlertsCollectionEditor();
        tasks = createTaskCollectionEditor();
        documents = createDocumentsManager(context.getSaveContext());
        investigations = act.isA(CustomerAccountArchetypes.INVOICE_ITEM) ? context.getInvestigations() : null;

        quantityListener = modifiable -> onQuantityChanged();
        if (dispensing != null) {
            dispensing.setPrescriptions(context.getPrescriptions());
            medicationButton = ButtonFactory.text(dispensing.getProperty().getDisplayName(), this::editMedication);
        }

        // register the context to delete acts after everything else has been saved to avoid Hibernate errors.
        ChargeSaveContext saveContext = context.getSaveContext();
        if (dispensing != null) {
            dispensing.getEditor().setRemoveHandler(saveContext);
        }
        if (reminders != null) {
            reminders.getEditor().setRemoveHandler(saveContext);
        }
        if (alerts != null) {
            alerts.getEditor().setRemoveHandler(saveContext);
        }
        if (tasks != null) {
            tasks.getEditor().setRemoveHandler(saveContext);
        }

        sellingUnits = LabelFactory.create();

        if (act.isNew()) {
            // default the act start time to today
            act.setActivityStartTime(new Date());
        }

        calculateTotal();
        calculateTax();

        Product product = getProduct();
        if (product != null) {
            initStockLocation(product);
        }
        ArchetypeNodes nodes = getFilterForProduct(product, getQuantity(), updatePrint(product));
        setArchetypeNodes(nodes);

        // add a listener to update the total when the discount changes
        discountListener = modifiable -> calculateTotal();
        getProperty(DISCOUNT).addModifiableListener(discountListener);

        // add a listener to update the tax amount when the total changes
        totalListener = modifiable -> onTotalChanged();
        getProperty(TOTAL).addModifiableListener(totalListener);

        // add a listener to update the discount and total when the quantity, fixed or unit price changes
        calculationListener = this::recalculate;
        getProperty(FIXED_PRICE).addModifiableListener(calculationListener);
        quantityProperty.addModifiableListener(calculationListener);
        getProperty(UNIT_PRICE).addModifiableListener(calculationListener);
        quantityProperty.addModifiableListener(quantityListener);

        batchListener = modifiable -> updateMedicationBatch(getStockLocationRef());
        updateOnHandQuantity();
        addStartEndTimeListeners();
    }

    /**
     * Returns the object being edited.
     *
     * @return the object being edited
     */
    @Override
    public FinancialAct getObject() {
        return (FinancialAct) super.getObject();
    }

    /**
     * Sets a product included from a template.
     *
     * @param product  the product. May be {@code null}
     * @param template the template that included the product. May be {@code null}
     * @param group    the template group, used to group acts generated from the same template. Ignored if there is no
     *                 template
     */
    @Override
    public void setProduct(TemplateProduct product, Product template, int group) {
        super.setProduct(product, template, group);
        if (product != null) {
            if (!product.getPrint() && MathRules.isZero(getTotal())) {
                setPrint(false);
            }
        }
    }

    /**
     * Returns the batch.
     *
     * @return the batch. May be {@code null}
     */
    public Entity getBatch() {
        return getParticipant(BATCH);
    }

    /**
     * Sets the batch.
     *
     * @param batch the batch. May be {@code null}
     */
    public void setBatch(Entity batch) {
        setParticipant(BATCH, batch);
    }

    /**
     * Determines if the quantity is a default for a product based on the patient's weight.
     *
     * @return {@code true} if the quantity is a default
     */
    public boolean isDefaultQuantity() {
        return quantity.isDefault();
    }

    /**
     * Returns the department.
     *
     * @return the department. May be {@code null}
     */
    public Entity getDepartment() {
        return getParticipant(DEPARTMENT);
    }

    /**
     * Sets the department.
     *
     * @param department the department. May be {@code null}
     */
    public void setDepartment(Entity department) {
        setParticipant(DEPARTMENT, department);
    }

    /**
     * Returns the department reference.
     *
     * @return the department reference. May be {@code null}
     */
    public Reference getDepartmentRef() {
        return getParticipantRef(DEPARTMENT);
    }

    /**
     * Returns the total.
     *
     * @return the total
     */
    public BigDecimal getTotal() {
        return getProperty(TOTAL).getBigDecimal(ZERO);
    }

    /**
     * Determines if an order has been placed for the item.
     * <p/>
     * This is determined from the status. If non-null, an order has been placed.
     *
     * @return {@code true} if an order has been placed
     */
    public boolean isOrdered() {
        return getStatus() != null;
    }

    /**
     * Returns the received quantity.
     *
     * @return the received quantity
     */
    public BigDecimal getReceivedQuantity() {
        Property property = getProperty(RECEIVED_QUANTITY);
        return property != null ? property.getBigDecimal(ZERO) : ZERO;
    }

    /**
     * Sets the received quantity.
     *
     * @param quantity the received quantity
     */
    public void setReceivedQuantity(BigDecimal quantity) {
        Property property = getProperty(RECEIVED_QUANTITY);
        if (property != null) {
            property.setValue(quantity);
        }
    }

    /**
     * Returns the returned quantity.
     *
     * @return the returned quantity
     */
    public BigDecimal getReturnedQuantity() {
        Property property = getProperty(RETURNED_QUANTITY);
        return property != null ? property.getBigDecimal(ZERO) : ZERO;
    }

    /**
     * Sets the returned quantity.
     *
     * @param quantity the returned quantity
     */
    public void setReturnedQuantity(BigDecimal quantity) {
        Property property = getProperty(RETURNED_QUANTITY);
        if (property != null) {
            property.setValue(quantity);
        }
    }

    /**
     * Returns any medication act associated with the charge item.
     *
     * @return the medication act, or {@code null} if none is found
     */
    public Act getMedication() {
        Act result = null;
        if (dispensing != null) {
            List<Act> acts = dispensing.getActs();
            if (!acts.isEmpty()) {
                result = acts.get(0);
            }
        }
        return result;
    }

    /**
     * Return any investigations associated with the charge item.
     *
     * @return the investigations
     */
    public List<DocumentAct> getInvestigations() {
        return (investigations != null) ? investigations.getInvestigations(getObject()) : Collections.emptyList();
    }

    /**
     * Returns any documents associated with the charge item.
     *
     * @return the documents
     */
    public List<Act> getDocuments() {
        return (documents != null) ? documents.getDocuments() : Collections.emptyList();
    }

    /**
     * Returns the investigation editor given the investigation reference.
     *
     * @param investigationRef the investigation reference
     * @return the corresponding editor, or {@code null} if none is found
     */
    public PatientInvestigationActEditor getInvestigation(Reference investigationRef) {
        return (investigations != null) ? investigations.getEditor(investigationRef) : null;
    }

    /**
     * Returns the available stock for the product and stock location.
     *
     * @return the available stock, or {@code null} if stock is not being tracked or the product or stock location are
     * unset
     */
    public BigDecimal getStock() {
        return getEditContext().getStock().getAvailableStock(getObject());
    }

    /**
     * Disposes of the editor.
     * <br/>
     * Once disposed, the behaviour of invoking any method is undefined.
     */
    @Override
    public void dispose() {
        super.dispose();
        getProperty(TOTAL).removeModifiableListener(totalListener);
        getProperty(DISCOUNT).removeModifiableListener(discountListener);
        getProperty(FIXED_PRICE).removeModifiableListener(calculationListener);
        getProperty(QUANTITY).removeModifiableListener(calculationListener);
        getProperty(UNIT_PRICE).removeModifiableListener(calculationListener);
        getProperty(QUANTITY).removeModifiableListener(quantityListener);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    public boolean validate(Validator validator) {
        EditorQueue queue = getEditorQueue();
        return (queue == null || queue.isComplete()) && super.validate(validator) && validateProduct(validator);
    }

    /**
     * Sets the popup editor manager.
     *
     * @param queue the popup editor manager
     */
    public void setEditorQueue(EditorQueue queue) {
        getEditContext().setEditorQueue(queue);
    }

    /**
     * Returns the popup editor manager.
     *
     * @return the popup editor manager
     */
    public EditorQueue getEditorQueue() {
        return getEditContext().getEditorQueue();
    }

    /**
     * Returns the prescriptions.
     *
     * @return the prescriptions. May be {@code null}
     */
    public Prescriptions getPrescriptions() {
        return getEditContext().getPrescriptions();
    }

    /**
     * Determines if prescriptions should be prompted for.
     *
     * @param prompt if {@code true}, prompt for prescriptions, otherwise use them automatically
     */
    public void setPromptForPrescriptions(boolean prompt) {
        promptForPrescription = prompt;
    }

    /**
     * Determines if prescription editing may be cancelled.
     *
     * @param cancel if {@code true}, prescription editing may be cancelled
     */
    public void setCancelPrescription(boolean cancel) {
        cancelPrescription = cancel;
    }

    /**
     * Returns the reminders.
     *
     * @return the reminders
     */
    public List<Act> getReminders() {
        return (reminders != null) ? reminders.getCurrentActs() : Collections.emptyList();
    }

    /**
     * Returns a reference to the stock location.
     *
     * @return the stock location reference, or {@code null} if there is none
     */
    public Reference getStockLocationRef() {
        return getParticipantRef(STOCK_LOCATION);
    }

    /**
     * Notifies the editor that the product has been ordered via a pharmacy.
     * <p>
     * This refreshes the display to make the patient and product read-only, and display the received and returned
     * nodes if required.
     */
    public void ordered() {
        ProductParticipationEditor productEditor = getProductEditor();
        if (productEditor != null) {
            productEditor.setReadOnly(true);
        }

        // need to force redisplay in order to make patient read-only
        updateLayout(true);
    }

    /**
     * Returns the edit context.
     *
     * @return the edit context
     */
    @Override
    public CustomerChargeEditContext getEditContext() {
        return (CustomerChargeEditContext) super.getEditContext();
    }

    /**
     * Returns the save context.
     *
     * @return the save context
     */
    public ChargeSaveContext getSaveContext() {
        return getEditContext().getSaveContext();
    }

    /**
     * Updates the on-hand quantity.
     */
    public void updateOnHandQuantity() {
        stockQuantity.refresh();
    }

    /**
     * Adds a relationship to an investigation, if no relationship already exists.
     *
     * @param editor the investigation editor
     */
    public void addInvestigation(PatientInvestigationActEditor editor) {
        Act investigation = editor.getObject();
        Reference investigationRef = investigation.getObjectReference();
        CollectionProperty property = getCollectionProperty(INVESTIGATIONS);
        boolean found = false;
        for (ActRelationship relationship : property.getValues(ActRelationship.class)) {
            if (Objects.equals(investigationRef, relationship.getTarget())) {
                found = true;
                break;
            }
        }
        if (!found) {
            ActRelationship relationship = create("actRelationship.invoiceItemInvestigation", ActRelationship.class);
            relationship.setSource(getObject().getObjectReference());
            relationship.setTarget(investigationRef);
            property.add(relationship);
            editor.getCollectionProperty("invoiceItems").add(relationship);
        }
    }

    /**
     * Sets a listener for department changes.
     *
     * @param listener the listener. May be {@code null}
     */
    public void setDepartmentListener(DepartmentListener listener) {
        departmentListener = listener;
    }

    /**
     * Returns an editor for a reminder.
     *
     * @param reminder the reminder reference
     * @return the corresponding editor or {@code null} if none is found
     */
    protected ReminderEditor getReminderEditor(Reference reminder) {
        if (reminders != null) {
            for (ReminderEditor editor : getReminderEditors()) {
                if (editor.getObject().getObjectReference().equals(reminder)) {
                    return editor;
                }
            }
        }
        return null;
    }

    /**
     * Updates the discount and checks that it isn't less than the total cost.
     * <p>
     * If so, gives the user the opportunity to remove the discount.
     * <p/>
     * If the calculated discount is zero, and the quantity is non-zero, the update is ignored. This is prevent
     * user-entered discounts from being removed.
     *
     * @return {@code true} if the discount was updated
     */
    @Override
    protected boolean updateDiscount() {
        boolean updated = super.updateDiscount();
        BigDecimal discount = getProperty(DISCOUNT).getBigDecimal(ZERO);
        if (updated && discount.compareTo(ZERO) != 0) {
            BigDecimal fixedPriceMaxDiscount = getFixedPriceMaxDiscount(null);
            BigDecimal unitPriceMaxDiscount = getUnitPriceMaxDiscount(null);
            if ((fixedPriceMaxDiscount != null && !MathRules.equals(fixedPriceMaxDiscount, ONE_HUNDRED))
                || (unitPriceMaxDiscount != null && !MathRules.equals(unitPriceMaxDiscount, ONE_HUNDRED))) {
                // if there is a fixed and/or unit price maximum discount present, and it is not 100%, check if the
                // sale price is less than the cost price

                BigDecimal quantity = getQuantity();
                BigDecimal fixedCost = getFixedCost();
                BigDecimal fixedPrice = getFixedPrice();
                BigDecimal unitCost = getUnitCost();
                BigDecimal unitPrice = getUnitPrice();
                BigDecimal costPrice = fixedCost.add(unitCost.multiply(quantity));
                BigDecimal salePrice = fixedPrice.add(unitPrice.multiply(quantity));
                if (costPrice.compareTo(salePrice.subtract(discount)) > 0) {
                    ConfirmationDialog dialog = new ConfirmationDialog(Messages.get("customer.charge.discount.title"),
                                                                       Messages.get("customer.charge.discount.message"),
                                                                       ConfirmationDialog.YES_NO);
                    dialog.addWindowPaneListener(new PopupDialogListener() {
                        @Override
                        public void onYes() {
                            getProperty(DISCOUNT).setValue(ZERO);
                            super.onYes();
                        }
                    });
                    getEditorQueue().queue(getObject(), dialog);
                }
            }
        }
        return updated;
    }

    /**
     * Save any edits.
     * <p>
     * This implementation saves the current object before children, to ensure deletion of child acts
     * don't result in StaleObjectStateException exceptions.
     * <p>
     * This implementation will throw an exception if the product is an <em>product.template</em>.
     * Ideally, the act would be flagged invalid if this is the case, but template expansion only works for valid
     * acts. TODO
     *
     * @throws OpenVPMSException     if the save fails
     * @throws IllegalStateException if the product is a template
     */
    @Override
    protected void doSave() {
        if (log.isDebugEnabled()) {
            log.debug("doSave: state=" + debugString());
        }
        if (getObject().isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            ChargeSaveContext saveContext = getSaveContext();
            if (saveContext.getHistoryChanges() == null) {
                throw new IllegalStateException("PatientHistoryChanges haven't been registered");
            }
        }
        super.doSave();
        getEditContext().getStock().remove(getObject());
    }

    /**
     * Returns the dispensing node editor.
     *
     * @return the editor. May be {@code null}
     */
    protected ActRelationshipCollectionEditor getDispensingEditor() {
        return dispensing;
    }

    /**
     * Creates the layout strategy.
     *
     * @param fixedPrice         the fixed price editor
     * @param serviceRatioEditor the service ratio editor
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy(FixedPriceEditor fixedPrice,
                                                          ServiceRatioEditor serviceRatioEditor) {
        return new CustomerChargeItemLayoutStrategy(fixedPrice, serviceRatioEditor);
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();

        // add a listener to update the dispensing, investigation and reminder acts when the patient changes
        monitorParticipation(PATIENT, this::updatePatientActsPatient);

        // add a listener to update the dispensing, investigation and reminder acts when the clinician changes
        monitorParticipation(CLINICIAN, this::updatePatientActsClinician);

        if (getEditContext().useDepartments()) {
            monitorParticipation(DEPARTMENT, this::onDepartmentChanged);
        }

        Product product = getProduct();
        updateBatch(product, getStockLocationRef());

        Prescriptions prescriptions = getPrescriptions();
        PatientMedicationActEditor medicationEditor = getMedicationEditor();
        if (prescriptions != null && medicationEditor != null) {
            medicationEditor.setPrescriptions(prescriptions);
        }
        showMedicationButton(product);
    }

    /**
     * Invoked when the product is changed, to update prices, dispensing and reminder acts.
     *
     * @param product the product. May be {@code null}
     */
    @Override
    protected void productModified(Product product) {
        getProperty(FIXED_PRICE).removeModifiableListener(calculationListener);
        getProperty(QUANTITY).removeModifiableListener(calculationListener);
        getProperty(UNIT_PRICE).removeModifiableListener(calculationListener);
        getProperty(DISCOUNT).removeModifiableListener(discountListener);
        getProperty(TOTAL).removeModifiableListener(totalListener);
        super.productModified(product);

        boolean clearDefault = true;
        if (TypeHelper.isA(product, ProductArchetypes.MEDICATION)) {
            Party patient = getPatient();
            if (patient != null) {
                BigDecimal dose = getDose(product, patient);
                if (!MathRules.isZero(dose)) {
                    quantity.setValue(dose, true);
                    clearDefault = false;
                }
            }
        }
        if (clearDefault) {
            // the quantity is not a default for the product, so turn off any highlighting
            quantity.clearDefault();
        }
        Property discount = getProperty(DISCOUNT);
        discount.setValue(ZERO);
        updatePrices(product);

        if (product != null && product.isA(ProductArchetypes.TEMPLATE)) {
            updateSellingUnits(null);
        } else {
            Reference stockLocation = updateStockLocation(product);
            updateSellingUnits(product);
            updateBatch(product, stockLocation);
        }

        if (log.isDebugEnabled()) {
            log.debug("productModified: " + debugString());
        }

        getProperty(FIXED_PRICE).addModifiableListener(calculationListener);
        getProperty(QUANTITY).addModifiableListener(calculationListener);
        getProperty(UNIT_PRICE).addModifiableListener(calculationListener);
        getProperty(DISCOUNT).addModifiableListener(discountListener);
        getProperty(TOTAL).addModifiableListener(totalListener);

        updateDependencies(product);

        ProductParticipationEditor productEditor = getProductEditor();
        if (productEditor != null) {
            productEditor.setReadOnly(isProductReadOnly());
        }

        // update the layout if nodes require filtering
        updateLayout(product, false);

        notifyProductListener(product);
    }

    /**
     * Invoked when the start time changes.
     * This:
     * <ul>
     *     <li>Sets the value to end time if start time > end time.</li>
     *     <li>Updates the service ratio</li>
     *     <li>Updates the date/time of patient arcts</li>
     * </ul>
     */
    @Override
    protected void onStartTimeChanged() {
        super.onStartTimeChanged();
        updateServiceRatio();
        updateActsStartTime();
    }

    /**
     * Invoked when the service ratio is modified.
     */
    @Override
    protected void serviceRatioModified() {
        super.serviceRatioModified();
        updatePrices(getProduct());
    }

    /**
     * Updates prices.
     *
     * @param product the product. May be {@code null}
     */
    protected void updatePrices(Product product) {
        Property fixedPrice = getProperty(FIXED_PRICE);
        Property unitPrice = getProperty(UNIT_PRICE);
        Property fixedCost = getProperty(FIXED_COST);
        Property unitCost = getProperty(UNIT_COST);
        ProductPrice fixedProductPrice = null;
        ProductPrice unitProductPrice = null;
        BigDecimal fixedPriceValue = BigDecimal.ZERO;
        BigDecimal fixedCostValue = BigDecimal.ZERO;
        BigDecimal unitPriceValue = BigDecimal.ZERO;
        BigDecimal unitCostValue = BigDecimal.ZERO;
        if (TypeHelper.isA(product, ProductArchetypes.TEMPLATE)) {
            fixedPrice.setValue(fixedPriceValue);
            unitPrice.setValue(unitPriceValue);
            fixedCost.setValue(fixedCostValue);
            unitCost.setValue(unitCostValue);
        } else {
            if (product != null) {
                fixedProductPrice = getDefaultFixedProductPrice(product);
                unitProductPrice = getDefaultUnitProductPrice(product);
            }

            BigDecimal serviceRatio = getServiceRatio();
            if (fixedProductPrice != null) {
                fixedPriceValue = getPrice(product, fixedProductPrice, serviceRatio);
                fixedCostValue = getCost(fixedProductPrice);
            }
            fixedPrice.setValue(fixedPriceValue);
            fixedCost.setValue(fixedCostValue);

            if (unitProductPrice != null) {
                unitPriceValue = getPrice(product, unitProductPrice, serviceRatio);
                unitCostValue = getCost(unitProductPrice);
            }
            unitPrice.setValue(unitPriceValue);
            unitCost.setValue(unitCostValue);
            updateDiscount();
        }
        calculateTotal();
        updateTaxAmount();

        if (log.isDebugEnabled()) {
            String fixedStr = fixedProductPrice != null ? "id=" + fixedProductPrice.getId()
                                                          + ", price=" + fixedProductPrice.getPrice() : null;
            String unitStr = unitProductPrice != null ? "id=" + unitProductPrice.getId()
                                                        + ", price=" + unitProductPrice.getPrice() : null;
            log.debug("fixedProductPrice=[" + fixedStr + "]" + ", unitProductPrice=[" + unitStr + "]");
        }
    }

    /**
     * Calculates the total.
     */
    protected void calculateTotal() {
        BigDecimal fixedPrice = getFixedPrice();
        BigDecimal discount = getDiscount();
        BigDecimal quantity = getQuantity();
        BigDecimal unitPrice = getUnitPrice();
        BigDecimal total = getEditContext().calculateTotal(fixedPrice, unitPrice, quantity, discount);
        getProperty("total").setValue(total);
    }

    /**
     * Calculates the tax amount.
     *
     * @throws ArchetypeServiceException for any archetype service error
     * @throws TaxRuleException          for any tax error
     */
    protected void calculateTax() {
        Party customer = getCustomer();
        if (customer != null && getProductRef() != null) {
            FinancialAct act = getObject();
            BigDecimal previousTax = act.getTaxAmount();
            BigDecimal tax = calculateTax(customer);
            if (tax.compareTo(previousTax) != 0) {
                Property property = getProperty("tax");
                property.refresh();
            }
        }
    }

    /**
     * Returns the product batch participation editor.
     *
     * @return the product batch participation, or {@code null}  if none exists
     */
    protected BatchParticipationEditor getBatchEditor() {
        return getBatchEditor(true);
    }

    /**
     * Returns the product batch participation editor.
     *
     * @param create if {@code true} force creation of the edit components if it hasn't already been done
     * @return the product batch participation, or {@code null} if none exists
     */
    protected BatchParticipationEditor getBatchEditor(boolean create) {
        ParticipationEditor<Entity> editor = getParticipationEditor(BATCH, create);
        return (BatchParticipationEditor) editor;
    }

    /**
     * Determines if the product is read-only.
     *
     * @return {@code true} if the item has been ordered via an HL7 service, or its a service product with a
     * minimum quantity
     */
    @Override
    protected boolean isProductReadOnly() {
        return isOrdered() || super.isProductReadOnly();
    }

    /**
     * Returns the available service ratio for a product.
     * <p/>
     * This is the service ratio available to the user as at {@link #getStartTime()}, using the current department,
     * if departments are enabled.
     *
     * @param product the product. May be {@code null}
     * @return the service ratio, or {@code null} if none exists
     */
    @Override
    protected BigDecimal getAvailableServiceRatio(Product product) {
        Entity department = null;
        if (getEditContext().useDepartments()) {
            department = getDepartment();
            if (department == null && getObject().isNew()) {
                department = getLayoutContext().getContext().getDepartment();
            }
        }
        return getServiceRatio(product, department);
    }

    /**
     * Returns the service ratio for a product.
     * <p/>
     * This is the service ratio available to the user as at {@link #getStartTime()}, using the selected department,
     * if departments are enabled.
     *
     * @param product the product. May be {@code null}
     * @return the service ratio, or {@code null} if none exists
     */
    @Override
    protected BigDecimal getServiceRatio(Product product) {
        return getServiceRatio(product, getDepartment());
    }

    /**
     * Verifies that restricted products aren't being sold over the counter when the practice option
     * <em>sellRestrictedDrugsOTC</em> is {@code false}.
     *
     * @param validator the validator
     * @return {@code true} if the product is valid, otherwise {@code false}
     */
    private boolean validateProduct(Validator validator) {
        boolean result = true;
        CustomerChargeEditContext editContext = getEditContext();
        if (getObject().isA(CustomerAccountArchetypes.COUNTER_ITEM) && !editContext.sellRestrictedDrugsOTC()) {
            Product product = getProduct();
            if (product != null && editContext.isRestricted(product)) {
                validator.add(this, Messages.format("customer.charge.medication.nosaleOTC", product.getName()));
                result = false;
            }
        }
        return result;
    }

    /**
     * Shows/hides the medication button.
     * <p/>
     * It is shown for medication products when invoicing positive quantities, otherwise it is hidden.
     *
     * @param product the product. May be {@code null}
     */
    private void showMedicationButton(Product product) {
        if (medicationButton != null) {
            boolean visible = product != null && product.isA(MEDICATION) && !quantity.isNegative();
            medicationButton.setVisible(visible);
        }
    }

    /**
     * Updates dependencies, when the product changes, or the quantity goes from negative to positive, or vice-versa.
     *
     * @param product the product. May be {@code null}
     */
    private void updateDependencies(Product product) {
        updatePatientMedication(product);
        updateInvestigations(product);
        updateReminders(product);
        updateAlerts(product);
        updateTasks(product);
        updateOnHandQuantity();
        if (product != null) {
            addPatientIdentity(product);
        }
        updateDocuments();
    }

    /**
     * Updates the discount and total when a property changes.
     *
     * @param modifiable the property
     */
    private void recalculate(Modifiable modifiable) {
        getProperty(DISCOUNT).removeModifiableListener(discountListener);
        if (log.isDebugEnabled() && modifiable instanceof Property) {
            Property property = (Property) modifiable;
            Object value = property.getValue();
            updateDiscount();
            log.debug("Property updated, name=" + property.getName() + ", value=" + value + ", state=" + debugString());
        } else {
            updateDiscount();
        }
        calculateTotal();
        getProperty(DISCOUNT).addModifiableListener(discountListener);
    }

    /**
     * Invoked when the total changes.
     */
    private void onTotalChanged() {
        updateTaxAmount();
        updateLayout(false);
    }

    /**
     * Calculates the tax amount.
     */
    private void updateTaxAmount() {
        try {
            calculateTax();
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception);
        }
    }

    /**
     * Updates any related acts with the start time.
     */
    private void updateActsStartTime() {
        FinancialAct parent = getObject();
        PatientActEditor medication = getMedicationEditor();
        if (medication != null) {
            medication.setStartTime(parent.getActivityStartTime());
        }
        if (investigations != null) {
            investigations.updateTime(parent);
        }
        for (ReminderEditor editor : getReminderEditors()) {
            // the reminder due date is mapped to the startTime node, for performance reasons.
            // The due date is calculated relative to the initialTime node
            editor.setInitialTime(parent.getActivityStartTime());
        }
        if (tasks != null) {
            Product product = getProduct();
            if (product != null) {
                tasks.updateStartTimes(parent.getActivityStartTime(), product);
                tasks.refresh();
            }
        }
        if (reminders != null) {
            reminders.refresh();
        }
        if (alerts != null) {
            alerts.refresh();
        }
    }

    /**
     * Invoked when the product changes to update patient medications.
     * <p>
     * If the new product is a medication and there is:
     * <ul>
     * <li>an existing act, the existing act will be updated.
     * <li>no existing act, a new medication will be created
     * </ul>
     * <p>
     * If the product is null, any existing act will be removed
     *
     * @param product the product. May be {@code null}
     */
    private void updatePatientMedication(Product product) {
        if (dispensing != null) {
            if (TypeHelper.isA(product, ProductArchetypes.MEDICATION)) {
                PatientMedicationActEditor editor = getMedicationEditor();
                if (editor != null) {
                    // set the product on the existing acts
                    editor.setProduct(product);
                    changePrescription(editor);
                } else {
                    // add a new medication act
                    Act act = (Act) dispensing.create();
                    if (act != null) {
                        Act prescription = getPrescription();
                        if (prescription != null) {
                            checkUsePrescription(prescription, product, act);
                        } else {
                            createMedicationEditor(product, act);
                        }
                    }
                }
            } else {
                // product is not a medication or is null. Remove any existing act
                removeAll(dispensing);
            }
        }
        showMedicationButton(product);
    }

    /**
     * Returns the prescription for the current patient and product, if one exists.
     *
     * @return the prescription, or {@code null} if none exists
     */
    private Act getPrescription() {
        Party patient = getPatient();
        Product product = getProduct();
        Prescriptions prescriptions = getPrescriptions();
        return prescriptions != null && patient != null && product != null ?
               prescriptions.getPrescription(patient, product) : null;
    }

    /**
     * Determines if a prescription should be dispensed.
     *
     * @param prescription the prescription
     * @param product      the product being dispensed
     * @param medication   the medication act
     */
    private void checkUsePrescription(final Act prescription, final Product product, final Act medication) {
        if (promptForPrescription) {
            ConfirmationDialog dialog = new ConfirmationDialog(Messages.get("customer.charge.prescription.title"),
                                                               Messages.format("customer.charge.prescription.message",
                                                                               product.getName()));
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    createPrescriptionMedicationEditor(medication, prescription);
                }

                @Override
                public void onCancel() {
                    createMedicationEditor(product, medication);
                }
            });
            getEditorQueue().queue(getObject(), dialog);
        } else {
            createPrescriptionMedicationEditor(medication, prescription);
        }
    }

    /**
     * Creates an editor for an <em>act.patientMedication</em> that dispenses a prescription.
     *
     * @param medication   the medication
     * @param prescription the prescription
     */
    private void createPrescriptionMedicationEditor(Act medication, Act prescription) {
        PrescriptionMedicationActEditor editor = dispensing.createEditor(medication, createLayoutContext(medication));
        editor.setPrescription(prescription);
        setQuantity(editor.getQuantity());
        dispensing.addEdited(editor);
        queueMedicationEditor(editor, cancelPrescription); // queue editing of the act
    }

    /**
     * Creates an editor for an <em>act.patientMedication</em>.
     *
     * @param product the product
     * @param act     the medication act
     */
    private void createMedicationEditor(Product product, Act act) {
        boolean dispensingLabel = hasDispensingLabel(product);
        IMObjectEditor editor = createEditor(act, dispensing);
        if (editor instanceof PatientMedicationActEditor) {
            PatientMedicationActEditor medicationEditor = (PatientMedicationActEditor) editor;
            medicationEditor.setQuantity(quantity);
            medicationEditor.setPrescriptions(getPrescriptions());
        }
        editor.getComponent();
        dispensing.addEdited(editor);
        if (dispensingLabel) {
            // queue editing of the act
            queueMedicationEditor(editor, false);
        }
    }

    /**
     * Queues a medication editor.
     * <p/>
     * If the quantity, batch or clinician is changed, this propagates back to the invoice item.
     *
     * @param editor the editor to queue
     * @param cancel if {@code true}, display a cancel button
     */
    private void queueMedicationEditor(IMObjectEditor editor, boolean cancel) {
        Act medication = (Act) editor.getObject();
        IMObjectBean bean = getBean(medication);
        Reference oldClinician = bean.getTargetRef(CLINICIAN);
        Reference oldBatch = bean.getTargetRef(BATCH);
        BigDecimal oldQuantity = bean.getBigDecimal(QUANTITY, ZERO);
        queuePatientActEditor(editor, false, cancel, dispensing.getEditor(), () -> {
            Reference newClinician = bean.getTargetRef(CLINICIAN);
            Reference newBatch = bean.getTargetRef(BATCH);
            BigDecimal newQuantity = bean.getBigDecimal(QUANTITY, ZERO);
            if (!Objects.equals(oldClinician, newClinician)) {
                setClinicianRef(newClinician);
            }
            if (!Objects.equals(oldBatch, newBatch)) {
                updateBatch(newBatch);
            }
            if (oldQuantity.compareTo(newQuantity) != 0) {
                updateQuantity(newQuantity);
            }
        });
    }

    /**
     * Displays the medication editor, if there is a current medication act.
     */
    private void editMedication() {
        Act medication = getMedication();
        if (medication != null) {
            IMObjectEditor editor = dispensing.getEditor(medication);
            queueMedicationEditor(editor, false);
        }
    }

    /**
     * Invoked when the product changes to update investigation acts.
     * <p>
     * This removes any existing investigations, and creates new ones, if required.
     *
     * @param product the product. May be {@code null}
     */
    private void updateInvestigations(Product product) {
        if (investigations != null) {
            List<PatientInvestigationActEditor> editors = investigations.updateProduct(this, product);
            EditorQueue queue = getEditorQueue();
            for (PatientInvestigationActEditor editor : editors) {
                if (!queue.isQueued(editor)) {
                    // only queue the investigation if it isn't already. This can occur if a product charges multiple
                    // tests, or multiple template inclusions update the one investigation
                    queuePatientActEditor(editor, true, false, investigations.getEditor(), null);
                }
            }
        }
    }

    /**
     * Invoked when the product changes, to update reminders acts.
     * <p>
     * This removes any existing reminders, and creates new ones, if required.
     *
     * @param product the product. May be {@code null}
     */
    private void updateReminders(Product product) {
        if (reminders != null) {
            removeAll(reminders);
            if (product != null) {
                Party patient = getPatient();
                Map<Entity, Relationship> reminderTypes = getEditContext().getReminderTypes(product, patient);
                for (Map.Entry<Entity, Relationship> entry : reminderTypes.entrySet()) {
                    Entity reminderType = entry.getKey();
                    Relationship relationship = entry.getValue();
                    Act act = (Act) reminders.create();
                    if (act != null) {
                        IMObjectEditor editor = createEditor(act, reminders);
                        if (editor instanceof ReminderEditor) {
                            ReminderEditor reminder = (ReminderEditor) editor;
                            Date startTime = getStartTime();
                            reminder.setInitialTime(startTime);
                            reminder.setReminderType(reminderType);
                            reminder.setPatient(patient);
                            reminder.setProduct(product);

                            // marking matching reminders completed are handled via CustomerChargeActEditor.
                            // Need to disable it here to avoid the rule updating other reminders in the invoice.
                            reminder.setMarkMatchingRemindersCompleted(false);

                            // override the due date calculated from the reminder type
                            Date dueDate = getEditContext().getReminderDueDate(startTime, relationship);
                            reminder.setEndTime(dueDate);
                        }
                        reminders.addEdited(editor);
                        IMObjectBean bean = getBean(relationship);
                        boolean interactive = bean.getBoolean("interactive");
                        if (interactive) {
                            // queue editing of the act
                            queuePatientActEditor(editor, true, false, reminders.getEditor(), null);
                        }
                    }
                }
            }
        }
    }

    /**
     * Invoked when the product changes, to update alert acts.
     * <p>
     * This removes any existing alerts, and creates new ones, if required.
     *
     * @param product the product. May be {@code null}
     */
    private void updateAlerts(Product product) {
        Alerts allAlerts = getEditContext().getAlerts();
        if (alerts != null) {
            for (Act act : alerts.getCurrentActs()) {
                alerts.remove(act);
                allAlerts.remove(act);
            }
            if (product != null) {
                List<Entity> alertTypes = allAlerts.getAlertTypes(product);
                for (Entity alertType : alertTypes) {
                    Party patient = getPatient();
                    if (patient != null && !allAlerts.hasAlert(patient, alertType)) {
                        Act act = (Act) alerts.create();
                        if (act != null) {
                            IMObjectEditor editor = createEditor(act, alerts);
                            if (editor instanceof PatientAlertEditor) {
                                PatientAlertEditor alert = (PatientAlertEditor) editor;
                                alert.getComponent();
                                Date startTime = getStartTime();
                                alert.setStartTime(startTime);
                                alert.setPatient(patient);
                                alert.setAlertType(alertType);
                                alert.setProduct(product);

                                // marking matching alerts completed are handled via CustomerChargeActEditor.
                                // Need to disable it here to avoid the rule updating other alerts in the invoice.
                                alert.setMarkMatchingAlertsCompleted(false);
                            }
                            alerts.addEdited(editor);
                            IMObjectBean bean = getBean(alertType);
                            boolean interactive = bean.getBoolean("interactive");
                            if (interactive) {
                                // queue editing of the act
                                queuePatientActEditor(editor, true, false, alerts.getEditor(), null);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Invoked when the product or patient changes, to update task acts.
     * <p>
     * This removes any existing tasks, and creates new ones, if required.
     *
     * @param product the product. May be {@code null}
     */
    private void updateTasks(Product product) {
        if (tasks != null) {
            removeAll(tasks);
            Party patient = getPatient();
            Date startTime = getStartTime();
            if (product != null && patient != null && startTime != null) {
                List<TaskType> tasksTypes = tasks.getTasksTypes(product);
                for (TaskType taskType : tasksTypes) {
                    TaskActEditor editor = tasks.createEditor(taskType, patient, startTime);
                    if (editor != null && (taskType.isInteractive() || !editor.isValid())) {
                        // queue editing of the act
                        queuePatientActEditor(editor, true, false, tasks.getEditor(), null);
                    }
                }
            }
        }
    }

    /**
     * Updates documents.
     */
    private void updateDocuments() {
        if (documents != null) {
            documents.update();
        }
    }

    /**
     * Creates an act editor, with a new help context.
     *
     * @param act     the act to editor
     * @param editors the editor collection
     * @return the editor
     */
    private IMObjectEditor createEditor(Act act, ActRelationshipCollectionEditor editors) {
        return editors.createEditor(act, createLayoutContext(act));
    }

    /**
     * Creates a layout context for editing an act.
     *
     * @param act the act being edited
     * @return a new layout context
     */
    private LayoutContext createLayoutContext(Act act) {
        LayoutContext context = getLayoutContext();
        return new DefaultLayoutContext(context, context.getHelpContext().topic(act, "edit"));
    }

    /**
     * Updates the layout.
     *
     * @param force if {@code true}, force a refresh of the display
     */
    private void updateLayout(boolean force) {
        Product product = getProduct();
        updateLayout(product, force);
    }

    /**
     * Updates the layout.
     *
     * @param product the product. May be {@code null}
     * @param force   if {@code true}, force a refresh of the display
     */
    private void updateLayout(Product product, boolean force) {
        boolean showPrint = updatePrint(product);
        updateLayout(product, getQuantity(), showPrint, force);
    }

    /**
     * Invoked when the product or quantity changes to update the layout, if required.
     *
     * @param product   the product. May be {@code null}
     * @param quantity  the quantity. For negative quantities, medication, investigation and reminder nodes are
     *                  suppressed
     * @param showPrint if {@code true}, show the print node
     * @param force     if {@code true}, force a refresh of the display
     */
    private void updateLayout(Product product, BigDecimal quantity, boolean showPrint, boolean force) {
        ArchetypeNodes currentNodes = getArchetypeNodes();
        ArchetypeNodes expectedFilter = getFilterForProduct(product, quantity, showPrint);
        if (force || !Objects.equals(currentNodes, expectedFilter)) {
            EditorQueue queue = getEditorQueue();
            Component popupFocus = null;
            Component focus = FocusHelper.getFocus();
            Property focusProperty = null;
            if (queue != null && !queue.isComplete()) {
                popupFocus = focus;
            } else if (focus instanceof BoundProperty) {
                focusProperty = ((BoundProperty) focus).getProperty();
            }
            changeLayout(expectedFilter);  // this can move the focus away from the popups, if any
            if (queue != null && queue.isComplete()) {
                // no current popups, so move focus to the original property if possible, otherwise move it to the
                // product
                if (focusProperty != null) {
                    if (!setFocus(focusProperty)) {
                        moveFocusToProduct();
                    }
                } else {
                    moveFocusToProduct();
                }
            } else {
                // move the focus back to the popup
                FocusHelper.setFocus(popupFocus);
            }
        }
    }

    /**
     * Updates the selling units label.
     *
     * @param product the product. May be {@code null}
     */
    private void updateSellingUnits(Product product) {
        String units = "";
        if (product != null) {
            IMObjectBean bean = getBean(product);
            String node = "sellingUnits";
            if (bean.hasNode(node)) {
                units = LookupNameHelper.getName(product, node);
            }
        }
        sellingUnits.setText(units);
    }

    /**
     * Updates the batch.
     *
     * @param product       the product. May be {@code null}
     * @param stockLocation the stock location. May be {@code null}
     */
    private void updateBatch(Product product, Reference stockLocation) {
        BatchParticipationEditor batchEditor = getBatchEditor();
        if (batchEditor != null) {
            try {
                batchEditor.removeModifiableListener(batchListener);
                batchEditor.setStockLocation(stockLocation);
                batchEditor.setProduct(product);
                updateMedicationBatch(stockLocation);
            } finally {
                batchEditor.addModifiableListener(batchListener);
            }
        }
    }

    /**
     * Queues an editor for display in a popup dialog.
     * Use this when there may be multiple editors requiring display.
     * <p>
     * NOTE: all objects should be added to the collection prior to them being edited. If they are skipped,
     * they will subsequently be removed. This is necessary as the layout excludes nodes based on elements being
     * present.
     *
     * @param editor     the editor to queue
     * @param skip       if {@code true}, indicates that the editor may be skipped
     * @param cancel     if {@code true}, indicates that the editor may be cancelled
     * @param collection the collection to remove the object from, if the editor is skipped
     * @param callback   invoked if editing wasn't skipped or cancelled. May be {@code null}
     */
    private void queuePatientActEditor(IMObjectEditor editor, boolean skip, boolean cancel,
                                       CollectionPropertyEditor collection, Runnable callback) {
        EditorQueue queue = getEditorQueue();
        if (queue != null) {
            queue.queue(getObject(), editor, skip, cancel, (skipped, cancelled) -> {
                if (skipped || cancelled) {
                    collection.remove(editor.getObject());
                    if (collection.getProperty().isEmpty()) {
                        // update the layout if the node needs to be suppressed
                        updateLayout(false);
                    }
                } else if (callback != null) {
                    callback.run();
                }
                onEditCompleted();
            });
        }
    }

    /**
     * Removes all acts from a collection.
     *
     * @param collection the collection. May be {@code null}
     */
    private void removeAll(ActRelationshipCollectionEditor collection) {
        if (collection != null) {
            EditorQueue queue = getEditorQueue();
            for (Act act : collection.getCurrentActs()) {
                collection.remove(act);
                if (queue != null) {
                    queue.cancel(act); // cancel any queued edits
                }
            }
        }
    }

    /**
     * Invoked when the quantity changes.
     * <p/>
     * If the quantity changes sign (i.e. from positive to negative or vice versa), and the act has been saved,
     * the quantity will be reset to its prior value.
     * <p/>
     * This is to because positive quantities need to trigger medication and investigation ordering, and swapping
     * from one to the other could cause orders to be sent out that have the same identifiers as prior orders for
     * the same line item.
     */
    private void onQuantityChanged() {
        BigDecimal newQuantity = getQuantity();
        if (!getObject().isNew()) {
            // if the item has been saved do not let the quantity change from -ve to +ve and vice versa.
            if ((previousQuantity.signum() == -1 && newQuantity.signum() != -1)
                || previousQuantity.signum() != -1 && newQuantity.signum() == -1) {
                setQuantity(previousQuantity);
                return;
            }
        }

        if (quantity.getProperty().isValid()) {
            if (newQuantity.signum() < 0) {
                // negative quantity, so remove any acts not valid when crediting
                removeAll(dispensing);
                if (investigations != null) {
                    investigations.removeInvoiceItem(this);
                }
                removeAll(reminders);
                removeAll(alerts);
                removeAll(tasks);
                if (documents != null) {
                    documents.removeAll();
                }
                updateLayout(false);
            } else if (dispensing != null) {
                if (previousQuantity.signum() < 0) {
                    // change from negative to positive. Need to add dispensing, investigation, reminders, and alerts
                    updateDependencies(getProduct());
                } else {
                    PatientMedicationActEditor editor = getMedicationEditor();
                    if (editor != null) {
                        editor.setQuantity(newQuantity);
                    }
                }
            }

            previousQuantity = newQuantity;
            if (getEditContext().useMinimumQuantities() && getEditContext().overrideMinimumQuantities()) {
                // when the quantity is valid, and minimum quantities are being used, and the user can override
                // minimum quantities, adjust the minimum quantity if required.
                // Setting the quantity to zero disables the minimum
                BigDecimal minimumQuantity = getMinimumQuantity();
                if (!MathRules.isZero(minimumQuantity)
                    && (newQuantity.compareTo(minimumQuantity) < 0 || MathRules.isZero(newQuantity))) {
                    setMinimumQuantity(newQuantity);
                }
            }
        }
        updateOnHandQuantity();
    }

    /**
     * Updates the invoice quantity when a medication act changes.
     *
     * @param newQuantity the new quantity
     */
    private void updateQuantity(BigDecimal newQuantity) {
        setQuantity(newQuantity);
        updateOnHandQuantity();
    }

    /**
     * Updates the medication batch from the invoice.
     *
     * @param stockLocation the stock location. May be {@code null}
     */
    private void updateMedicationBatch(Reference stockLocation) {
        BatchParticipationEditor batchEditor = getBatchEditor();
        if (batchEditor != null) {
            PatientMedicationActEditor editor = getMedicationEditor();
            if (editor != null) {
                editor.setBatch(batchEditor.getEntity());
                editor.setStockLocation(stockLocation);
            }
        }
    }

    /**
     * Updates the batch if the medication batch changes.
     *
     * @param batch the new batch reference. May be {@code null}
     */
    private void updateBatch(Reference batch) {
        BatchParticipationEditor batchEditor = getBatchEditor();
        if (batchEditor != null) {
            batchEditor.removeModifiableListener(batchListener);
            try {
                batchEditor.setEntityRef(batch);
            } finally {
                batchEditor.addModifiableListener(batchListener);
            }
        }
    }

    /**
     * Updates any child patient acts with the patient.
     *
     * @param patient the patient. May be {@code null}
     */
    private void updatePatientActsPatient(Party patient) {
        PatientMedicationActEditor medication = getMedicationEditor();
        if (medication != null) {
            medication.setPatient(patient);
            changePrescription(medication);
        }
        updateInvestigations(patient);

        // force the documents to be recreated
        updateDocuments();

        Product product = getProduct();
        updateReminders(product); // need to recreate the reminders, as they may be for a different species

        updateAlerts(product);    // need to create alerts

        updateTasks(product);     // need to recreate tasks
    }

    /**
     * Updates investigations when the patient changes.
     *
     * @param patient the patient. May be {@code null}
     */
    private void updateInvestigations(Party patient) {
        if (investigations != null) {
            List<PatientInvestigationActEditor> editors = investigations.updatePatient(this, patient);
            for (PatientInvestigationActEditor editor : editors) {
                queuePatientActEditor(editor, true, false, investigations.getEditor(), null);
            }
        }
    }

    /**
     * Changes the prescription for an editor, if one is available.
     *
     * @param editor the editor
     */
    private void changePrescription(PatientMedicationActEditor editor) {
        Act prescription = getPrescription();
        if (prescription != null) {
            final EditorQueue queue = getEditorQueue();
            if (promptForPrescription) {
                Product product = getProduct();
                String title = Messages.get("customer.charge.prescription.title");
                String message = Messages.format("customer.charge.prescription.message", product.getName());
                ConfirmationDialog dialog = new ConfirmationDialog(title, message);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onOK() {
                        queue.queue(getObject(), editor, true, cancelPrescription, null);
                    }
                });
                queue.queue(getObject(), dialog);
            } else {
                queue.queue(getObject(), editor, true, cancelPrescription, null);
            }
        }
    }

    /**
     * Updates any child patient acts with the clinician.
     *
     * @param clinician the clinician. May be {@code null}
     */
    private void updatePatientActsClinician(User clinician) {
        PatientMedicationActEditor medication = getMedicationEditor();
        if (medication != null) {
            medication.setClinician(clinician);
        }
        if (investigations != null) {
            investigations.updateClinician(getObject(), clinician);
        }
        for (ReminderEditor editor : getReminderEditors()) {
            editor.setClinician(clinician);
        }
        updateDocuments();
    }

    /**
     * Invoked when the department changes.
     * <p/>
     * This updates the service ratio and prompts to update other line items if the line item was from a template.
     *
     * @param department the department. May be {@code null}
     */
    private void onDepartmentChanged(Entity department) {
        updateServiceRatio();
        if (departmentListener != null) {
            departmentListener.departmentChanged(this, department);
        }
    }

    private void updateServiceRatio() {
        Product product = getProduct();
        if (updateServiceRatio(product)) {
            updatePrices(product);
        }
    }

    /**
     * Returns the medication editor.
     *
     * @return the medication editor, or {@code null} if none exists
     */
    private PrescriptionMedicationActEditor getMedicationEditor() {
        PrescriptionMedicationActEditor editor = null;
        if (dispensing != null) {
            editor = (PrescriptionMedicationActEditor) dispensing.getCurrentEditor();
            if (editor == null) {
                List<Act> acts = dispensing.getActs();
                if (!acts.isEmpty()) {
                    editor = (PrescriptionMedicationActEditor) dispensing.getEditor(acts.get(0));
                }
            }
        }
        return editor;
    }

    /**
     * Returns the editors for each of the <em>act.patientReminder</em> acts.
     *
     * @return the editors
     */
    private Set<ReminderEditor> getReminderEditors() {
        return getActEditors(reminders);
    }

    /**
     * Returns the act editors for the specified collection editor.
     *
     * @param editors the collection editor. May be {@code null}
     * @return a set of editors
     */
    @SuppressWarnings("unchecked")
    private <T extends IMObjectEditor> Set<T> getActEditors(ActRelationshipCollectionEditor editors) {
        Set<T> result = new HashSet<>();
        if (editors != null) {
            for (Act act : editors.getCurrentActs()) {
                T editor = (T) editors.getEditor(act);
                result.add(editor);
            }
        }
        return result;
    }

    /**
     * Determines if a medication product requires a dispensing label.
     *
     * @param product the product
     * @return {@code true} if the product requires a dispensing label
     */
    private boolean hasDispensingLabel(Product product) {
        IMObjectBean bean = getBean(product);
        return bean.getBoolean("label");
    }

    /**
     * Initialises the stock location if one isn't present and there is a medication or merchandise product.
     * <p>
     * This is required due to a bug where charge quantities of zero would remove the stock location relationship,
     * and prevent subsequent quantity changes would not be reflected in the stock.
     *
     * @param product the product
     */
    private void initStockLocation(Product product) {
        if (product.isA(MEDICATION, MERCHANDISE)) {
            IMObjectBean bean = getBean(getObject());
            if (bean.getTargetRef(STOCK_LOCATION) == null) {
                updateStockLocation(product);
            }
        }
    }

    /**
     * Updates the stock location associated with the product.
     *
     * @param product the new product. May be {@code null}
     * @return the stock location. May be {@code null}
     */
    private Reference updateStockLocation(Product product) {
        Party stockLocation = null;
        if (TypeHelper.isA(product, MEDICATION, MERCHANDISE)) {
            stockLocation = getEditContext().getStockLocation(product);
        }
        IMObjectBean bean = getBean(getObject());
        if (stockLocation != null) {
            bean.setTarget(STOCK_LOCATION, stockLocation);
        } else {
            bean.removeValues(STOCK_LOCATION);
        }
        return stockLocation != null ? stockLocation.getObjectReference() : null;
    }

    /**
     * Adds a patient identity for a product, if one is configured.
     * <p>
     * Only one identity will be added, i.e. the quantity is ignored.
     *
     * @param product the product
     */
    private void addPatientIdentity(Product product) {
        Party patient = getPatient();
        if (patient != null && getObject().isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            IMObjectBean bean = getBean(product);
            if (bean.hasNode(PATIENT_IDENTITY)) {
                String archetype = bean.getString(PATIENT_IDENTITY);
                if (archetype != null) {
                    Context context = getLayoutContext().getContext();
                    HelpContext help = getHelpContext();
                    PatientIdentityEditor editor = PatientIdentityEditor.create(patient, archetype, context, help);
                    if (editor != null) {
                        EditorQueue queue = getEditorQueue();
                        EditDialog dialog = editor.edit(true);
                        queue.queue(getObject(), dialog, this::onEditCompleted);
                    }
                }
            }
        }
    }

    /**
     * Invoked after a queued editor has completed.
     * <p/>
     * If the queue is complete, the focus will be moved to the product.
     */
    private void onEditCompleted() {
        EditorQueue queue = getEditorQueue();
        if (queue != null && queue.isComplete()) {
            moveFocusToProduct();
            // force the parent collection editor to re-check the validation status of
            // this editor, in order for the Add button to be enabled.
            getListeners().notifyListeners(CustomerChargeActItemEditor.this);
        }
    }

    /**
     * Returns the value of the cost node of a price.
     *
     * @param price the product price
     * @return the cost
     */
    private BigDecimal getCost(ProductPrice price) {
        IMObjectBean bean = getBean(price);
        return bean.getBigDecimal("cost", ZERO);
    }

    /**
     * Returns a node filter for the specified product reference.
     * <p>
     * This excludes:
     * <ul>
     * <li>the investigations node if the product isn't a <em>product.medication</em>, <em>product.merchandise</em>,
     * or <em>product.service</em>
     * <li>the reminders node is excluded if there are no reminders present.
     * <li>the alerts node is excluded if there are no alerts present.
     * <li>the tasks node is excluded if there are no tasks present.
     * <li>the discount node, if discounts are disabled</li>
     * </ul>
     *
     * @param product   the product. May be {@code null}
     * @param quantity  the current quantity
     * @param showPrint if {@code true} show the print node
     * @return a node filter for the product. If {@code null}, no nodes require filtering
     */
    private ArchetypeNodes getFilterForProduct(Product product, BigDecimal quantity, boolean showPrint) {
        boolean negative = quantity.signum() == -1;
        ArchetypeNodes result;
        if (TypeHelper.isA(product, ProductArchetypes.TEMPLATE)) {
            result = TEMPLATE_NODES;
        } else {
            List<String> filter = new ArrayList<>();
            filter.add(DISPENSING);
            filter.add(REMINDERS);
            filter.add(ALERTS);
            filter.add(TASKS);
            if (disableDiscounts()) {
                filter.add(DISCOUNT);
            }
            if (!negative) {
                if (reminders != null && !reminders.getCollection().isEmpty()) {
                    filter.remove(REMINDERS);
                }
                if (alerts != null && !alerts.getCollection().isEmpty()) {
                    filter.remove(ALERTS);
                }
                if (tasks != null && !tasks.getCollection().isEmpty()) {
                    filter.remove(TASKS);
                }
            }
            result = new ArchetypeNodes().exclude(filter);
            if (showPrint) {
                result.simple(PRINT).order(PRINT, TAX);
            }
            if (isOrdered() && hasReceivedOrReturnedQuantity()) {
                if (result == null) {
                    result = new ArchetypeNodes();
                }
                result.simple(ORDER_NODES);
            }
            if (showServiceRatio()) {
                if (result == null) {
                    result = new ArchetypeNodes();
                }
                result.hidden(true).simple(SERVICE_RATIO);
            }
        }
        return result;
    }

    /**
     * Determines if there is a received or returned quantity set.
     *
     * @return {@code true} if there is a received or returned quantity set
     */
    private boolean hasReceivedOrReturnedQuantity() {
        Property received = getProperty(RECEIVED_QUANTITY);
        Property returned = getProperty(RETURNED_QUANTITY);
        return (received != null && received.getBigDecimal() != null)
               || (returned != null && returned.getBigDecimal() != null);
    }

    /**
     * Updates the print flag when the product or total changes.
     *
     * @param product the product. May be {@code null}
     * @return {@code true} if the print flag should be shown
     */
    private boolean updatePrint(Product product) {
        boolean result = false;
        if (product != null) {
            BigDecimal total = getTotal();
            result = MathRules.equals(total, ZERO);
            if (result) {
                Product template = getTemplate();
                if (template != null) {
                    IMObjectBean bean = getBean(template);
                    result = !bean.getBoolean(PRINT_AGGREGATE);
                }
            } else {
                setPrint(true);
            }
        }
        return result;
    }

    /**
     * Creates an editor for the "product" node.
     *
     * @return a new editor
     */
    private ParticipationCollectionEditor createProductCollectionEditor() {
        ParticipationCollectionEditor editor = new ParticipationCollectionEditor(
                getCollectionProperty(PRODUCT), getObject(), getLayoutContext());
        editor.getComponent();
        addEditor(editor);
        if (isProductReadOnly()) {
            ProductParticipationEditor productEditor = getProductEditor(false);
            if (productEditor != null) {
                productEditor.setReadOnly(true);
            }
        }
        return editor;
    }

    /**
     * Creates an editor for the "dispensing" node.
     *
     * @return a new editor
     */
    private DispensingActRelationshipCollectionEditor createDispensingCollectionEditor() {
        DispensingActRelationshipCollectionEditor editor = null;
        CollectionProperty collection = (CollectionProperty) getProperty(DISPENSING);
        if (collection != null && !collection.isHidden()) {
            editor = new DispensingActRelationshipCollectionEditor(collection, getObject(),
                                                                   new DefaultLayoutContext(getLayoutContext()));
            editor.getComponent();
            addEditor(editor);
        }
        return editor;
    }

    /**
     * Creates an editor for the "reminders" node.
     *
     * @return a new editor. May be {@code null}
     */
    private ActRelationshipCollectionEditor createRemindersCollectionEditor() {
        ActRelationshipCollectionEditor editor = null;
        CollectionProperty collection = (CollectionProperty) getProperty(REMINDERS);
        if (collection != null && !collection.isHidden()) {
            editor = new ReminderActRelationshipCollectionEditor(collection, getObject(),
                                                                 new DefaultLayoutContext(getLayoutContext()));
            addEditor(editor);
        }
        return editor;
    }

    /**
     * Creates an editor for the "alerts" node.
     *
     * @return a new editor. May be {@code null}
     */
    private ActRelationshipCollectionEditor createAlertsCollectionEditor() {
        AlertActRelationshipCollectionEditor editor = null;
        CollectionProperty collection = (CollectionProperty) getProperty(ALERTS);
        if (collection != null && !collection.isHidden()) {
            editor = new AlertActRelationshipCollectionEditor(collection, getObject(),
                                                              new DefaultLayoutContext(getLayoutContext()));
            editor.setAlerts(getEditContext().getAlerts());
            addEditor(editor);
        }
        return editor;
    }

    /**
     * Creates an editor for the "tasks" node.
     *
     * @return a new editor. May be {@code null}
     */
    private TaskActRelationshipCollectionEditor createTaskCollectionEditor() {
        TaskActRelationshipCollectionEditor editor = null;
        CollectionProperty collection = (CollectionProperty) getProperty(TASKS);
        if (collection != null) {
            editor = new TaskActRelationshipCollectionEditor(collection, getObject(),
                                                             new DefaultLayoutContext(getLayoutContext()));
            addEditor(editor);
        }
        return editor;
    }

    /**
     * Creates a documents manager.
     *
     * @param saveContext the save context
     * @return a new documents manager
     */
    private ChargeItemDocumentManager createDocumentsManager(ChargeSaveContext saveContext) {
        ChargeItemDocumentManager result = null;
        CollectionProperty property = getCollectionProperty(DOCUMENTS);
        if (property != null) {
            result = new ChargeItemDocumentManager(this, property, saveContext, getLayoutContext());
            addEditor(result);
        }
        return result;
    }

    /**
     * Returns a string containing the current state, for debugging purposes.
     *
     * @return a debug string
     */
    private String debugString() {
        Product product = getProduct();
        Party patient = getPatient();
        String productStr = product != null ? "id=" + product.getId() + ", name=" + product.getName() : null;
        User user = getLayoutContext().getContext().getUser();
        String patientStr = (patient != null) ? "id=" + patient.getId() + ", name=" + patient.getName() : null;
        User clinician = getClinician();
        return "product=[" + productStr + "]"
               + ", fixedCost=" + getFixedCost() + ", fixedPrice=" + getFixedPrice()
               + ", unitCost=" + getUnitCost() + ", unitPrice=" + getUnitPrice()
               + ", serviceRatio=" + getServiceRatio() + ", quantity=" + getQuantity()
               + ", discount=" + getProperty(DISCOUNT).getBigDecimal()
               + ", tax=" + getProperty(TAX).getBigDecimal() + ", total=" + getTotal()
               + ", clinician=" + (clinician != null ? clinician.getUsername() : null)
               + ", patient=[" + patientStr + "]"
               + ", user=" + (user != null ? user.getUsername() : null)
               + ", act=" + getObject().getObjectReference()
               + ", parent=" + ((getParent() != null) ? getParent().getObjectReference() : null);
    }

    protected class CustomerChargeItemLayoutStrategy extends PriceItemLayoutStrategy {

        public CustomerChargeItemLayoutStrategy(FixedPriceEditor fixedPrice, ServiceRatioEditor serviceRatio) {
            super(fixedPrice, serviceRatio);
        }

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            if (!getEditContext().useDepartments()) {
                ArchetypeNodes nodes = new ArchetypeNodes(super.getArchetypeNodes()).exclude(DEPARTMENT);
                setArchetypeNodes(nodes);
            }
            Row onHand = RowFactory.create(CELL_SPACING, RowFactory.rightAlign(),
                                           stockQuantity.getComponent().getLabel(),
                                           stockQuantity.getComponent().getComponent());
            Row row = RowFactory.create(WIDE_CELL_SPACING,
                                        RowFactory.create(CELL_SPACING, quantity.getComponent(), sellingUnits), onHand);
            addComponent(new ComponentState(productCollectionEditor));
            addComponent(new ComponentState(row, quantity.getProperty()));
            if (reminders != null) {
                addComponent(new ComponentState(reminders));
            }
            if (alerts != null) {
                addComponent(new ComponentState(alerts));
            }
            if (tasks != null) {
                addComponent(new ComponentState(tasks));
            }
            if (isOrdered()) {
                // the item has been ordered via an HL7 service, the patient cannot be changed
                addComponent(createComponent(createReadOnly(properties.get(PATIENT)), parent, context));
            }
            return super.apply(object, properties, parent, context);
        }

        /**
         * Lays out components in a grid.
         *
         * @param object     the object to lay out
         * @param properties the properties
         * @param context    the layout context
         */
        @Override
        protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
            ComponentGrid grid = super.createGrid(object, properties, context);
            if (medicationButton != null) {
                grid.set(0, grid.getColumns(), ColumnFactory.create(medicationButton));
            }
            return grid;
        }
    }

}
