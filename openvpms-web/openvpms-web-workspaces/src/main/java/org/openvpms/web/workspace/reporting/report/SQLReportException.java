/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.report;

import org.apache.commons.resources.Messages;
import org.openvpms.component.exception.OpenVPMSException;


/**
 * Exception class for {@link SQLReportPrinter}.
 *
 * @author Tim Anderson
 */
public class SQLReportException extends OpenVPMSException {

    /**
     * An enumeration of error codes.
     */
    public enum ErrorCode {
        NoQuery,
        ConnectionError
    }

    /**
     * The error code.
     */
    private final ErrorCode errorCode;


    /**
     * The appropriate resource file is loaded cached into memory when this class is loaded.
     */
    private static final Messages messages 
            = Messages.getMessages("org.openvpms.web.workspace.reporting.report.errmessages");


    /**
     * Constructs a new {@link SQLReportException}.
     *
     * @param errorCode the error code
     * @param cause     the cause. May be {@link null}
     */
    public SQLReportException(ErrorCode errorCode, Throwable cause) {
        super(org.openvpms.component.i18n.Messages.create(messages.getMessage(errorCode.toString())), cause);
        this.errorCode = errorCode;
    }

    /**
     * Constructs a new {@link SQLReportException}.
     *
     * @param errorCode the error code
     */
    public SQLReportException(ErrorCode errorCode, Object... args) {
        super(org.openvpms.component.i18n.Messages.create(messages.getMessage(errorCode.toString(), args)));
        this.errorCode = errorCode;
    }

    /**
     * Returns the error code.
     *
     * @return the error code
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
