/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.order;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.FinancialActions;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.print.BasicPrinterListener;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.order.OrderCharger;
import org.openvpms.web.workspace.customer.order.OrderChargerFactory;

import static org.openvpms.archetype.rules.finance.order.OrderArchetypes.INVESTIGATION_RETURN;

/**
 * CRUD window for customer orders.
 *
 * @author Tim Anderson
 */
public class CustomerOrderCRUDWindow extends ResultSetCRUDWindow<FinancialAct> {

    /**
     * Invoice button identifier.
     */
    private static final String INVOICE_ID = "button.invoice";

    /**
     * Constructs a {@link CustomerOrderCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param query      the query. May be {@code null}
     * @param set        the result set. May be {@code null}
     * @param context    the context
     * @param help       the help context
     */
    public CustomerOrderCRUDWindow(Archetypes<FinancialAct> archetypes, Query<FinancialAct> query,
                                   ResultSet<FinancialAct> set, Context context, HelpContext help) {
        super(filter(archetypes), OrderActions.INSTANCE, query, set, context, help);
    }


    /**
     * Returns the customer associated with the selected order/return.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        Party result = null;
        Act object = getObject();
        if (object != null) {
            IMObjectBean bean = getBean(object);
            result = bean.getTarget("customer", Party.class);
        }
        return result;
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(INVOICE_ID, action(this::charge, act -> getActions().canInvoice(act), true,
                                       "customer.order.invoice.title"));
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        OrderActions actions = getActions();
        FinancialAct order = getObject();
        buttons.setEnabled(VIEW_ID, enable);
        buttons.setEnabled(EDIT_ID, enable && actions.canEdit(order));
        buttons.setEnabled(DELETE_ID, enable && actions.canDelete(order));
        buttons.setEnabled(INVOICE_ID, enable && actions.canInvoice(order));
        buttons.setEnabled(PRINT_ID, enable);
        buttons.setEnabled(MAIL_ID, enable);
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected OrderActions getActions() {
        return (OrderActions) super.getActions();
    }

    /**
     * Charges out an order or return to the customer.
     *
     * @param act the order/return
     */
    protected void charge(FinancialAct act) {
        IMObjectBean bean = getBean(act);
        Party customer = bean.getTarget("customer", Party.class);
        if (customer != null) {
            OrderChargerFactory factory = ServiceHelper.getBean(OrderChargerFactory.class);
            Context context = getContext();
            OrderCharger charger = factory.create(customer, context, getHelpContext().subtopic("order"));
            charger.charge(act, () -> onRefresh(act));
        } else {
            ErrorDialog.newDialog()
                    .titleKey("customer.order.invalid", getDisplayName(act))
                    .messageKey("property.error.required", getDisplayName(act, "customer"))
                    .show();
        }
    }

    /**
     * Creates a new printer.
     *
     * @param object the object to print
     * @return an instance of {@link InteractiveIMPrinter}.
     * @throws OpenVPMSException for any error
     */
    @Override
    protected IMPrinter<FinancialAct> createPrinter(FinancialAct object) {
        InteractiveIMPrinter<FinancialAct> printer = (InteractiveIMPrinter<FinancialAct>) super.createPrinter(object);
        printer.setListener(new BasicPrinterListener() {
            public void printed(DocumentPrinter printer) {
                FinancialAct saved = getActions().setPrinted(object);
                if (saved != null) {
                    onSaved(saved, false);
                }
            }
        });
        return printer;
    }

    /**
     * Excludes <em>act.customerReturnInvestigation</em> from the list of archetypes that may be created.
     * This is required as investigation returns can only be used to cancel an existing investigation linked to
     * an invoice item.
     *
     * @param archetypes the archetypes
     * @return the archetypes with <em>act.customerReturnInvestigation</em> removed
     */
    private static Archetypes<FinancialAct> filter(Archetypes<FinancialAct> archetypes) {
        Archetypes<FinancialAct> result;
        if (archetypes.contains(INVESTIGATION_RETURN)) {
            String[] shortNames = ArrayUtils.removeElement(archetypes.getShortNames(), INVESTIGATION_RETURN);
            result = new Archetypes<>(shortNames, FinancialAct.class, archetypes.getDefaultShortName(),
                                      archetypes.getDisplayName());
        } else {
            result = archetypes;
        }

        return result;
    }


    private static class OrderActions extends FinancialActions<FinancialAct> {

        public static final OrderActions INSTANCE = new OrderActions();

        public boolean canInvoice(FinancialAct order) {
            return ActStatus.IN_PROGRESS.equals(order.getStatus());
        }
    }

}
