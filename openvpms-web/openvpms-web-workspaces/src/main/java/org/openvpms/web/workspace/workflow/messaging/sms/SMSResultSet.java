/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.OrConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;

import java.util.Date;
import java.util.List;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.idEq;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.leftJoin;
import static org.openvpms.component.system.common.query.Constraints.notExists;
import static org.openvpms.component.system.common.query.Constraints.or;
import static org.openvpms.component.system.common.query.Constraints.subQuery;

/**
 * Result set for <em>act.smsMessage</em> and <em>act.smsReply</em> acts.
 *
 * @author Tim Anderson
 */
public class SMSResultSet extends ActResultSet<Act> {

    /**
     * The selected location.
     */
    private final Party location;

    /**
     * The locations to query.
     */
    private final List<Party> locations;

    /**
     * Constructs an {@link SMSResultSet}.
     *
     * @param archetypes the act archetype constraint
     * @param value      the value being searched on. If non-null, can be used to search on message identifier,
     *                   party, customer, or patient name
     * @param from       the act start-from date. May be {@code null}
     * @param to         the act start-to date. May be {@code null}
     * @param statuses   the statuses. If empty, indicates all acts
     * @param pageSize   the maximum no. of results per page
     * @param sort       the sort criteria. May be {@code null}
     */
    public SMSResultSet(ShortNameConstraint archetypes, String value, Party location, List<Party> locations, Date from,
                        Date to, String[] statuses, int pageSize, SortConstraint[] sort) {
        super(archetypes, value, (ParticipantConstraint[]) null, from, to, statuses, false, null, pageSize, sort);
        this.location = location;
        this.locations = locations;
    }

    /**
     * Creates a new archetype query.
     *
     * @return a new archetype query
     */
    @Override
    protected ArchetypeQuery createQuery() {
        ArchetypeQuery query = super.createQuery();
        query.getArchetypeConstraint().setAlias("act");
        String value = getValue();
        if (!StringUtils.isEmpty(value) && getId(value) == null) {
            query.add(leftJoin("contact").add(leftJoin("entity", "contacte")));
            query.add(leftJoin("customer").add(leftJoin("entity", "customere")));
            query.add(leftJoin("patient").add(leftJoin("entity", "patiente")));
            query.add(or(eq("contacte.name", value), eq("customere.name", value), eq("patiente.name", value)));
        }
        if (location != null || !locations.isEmpty()) {
            query.add(Constraints.leftJoin("location", "location"));
            OrConstraint or = new OrConstraint();
            if (location != null) {
                or.add(eq("location.entity", location));
            } else {
                for (Party location : locations) {
                    or.add(eq("location.entity", location));
                }
            }
            // include messages that have no location
            or.add(notExists(subQuery(getArchetypes().getShortNames(), "i2").add(
                    join("location").add(join("entity").add(idEq("act", "i2"))))));
            query.add(or);
        }
        return query;
    }

}
