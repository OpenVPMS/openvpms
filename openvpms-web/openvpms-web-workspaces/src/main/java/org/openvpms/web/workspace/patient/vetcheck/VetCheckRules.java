/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.vetcheck;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.openvpms.archetype.rules.patient.MedicalRecordRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.math.Weight;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Address;
import org.openvpms.domain.party.Email;
import org.openvpms.domain.party.Phone;
import org.openvpms.domain.patient.Microchip;
import org.openvpms.domain.patient.Patient;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * VetCheck rules.
 *
 * @author Tim Anderson
 */
public class VetCheckRules {

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The medical record rules.
     */
    private final MedicalRecordRules rules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The singleton service.
     */
    private final SingletonService singletonService;

    /**
     * The VetCheck base url.
     */
    private static final String URL = "https://plugin.vetcheck.it";

    /**
     * VetCheck provider archetype, used to populate links.
     */
    private static final String VETCHECK = "entity.linkProviderVetCheck";


    /**
     * Constructs a {@link VetCheckRules}.
     *
     * @param domainService      the domain service
     * @param rules              the medical record rules
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param practiceService    the practice service
     * @param singletonService         the singleton service
     */
    public VetCheckRules(DomainService domainService, MedicalRecordRules rules, ArchetypeService service,
                         PlatformTransactionManager transactionManager, PracticeService practiceService,
                         SingletonService singletonService) {
        this.domainService = domainService;
        this.rules = rules;
        this.service = service;
        this.transactionManager = transactionManager;
        this.practiceService = practiceService;
        this.singletonService = singletonService;
    }

    /**
     * Determines if VetCheck is enabled.
     *
     * @return {@code true} if VetCheck is enabled, otherwise {@code false}
     */
    public boolean isVetCheckEnabled() {
        boolean result = false;
        Party practice = practiceService.getPractice();
        if (practice != null) {
            IMObjectBean bean = service.getBean(practice);
            result = bean.getBoolean("enableVetCheck");
        }
        return result;
    }

    /**
     * Returns the base VetCheck URI.
     *
     * @return the URI
     */
    public String getURL() {
        return getURL(null);
    }

    /**
     * Builds a VetCheck URI.
     *
     * @param patient the patient. May be {@code null}
     * @return the URI
     */
    public String getURL(Party patient) {
        Patient pet = null;
        Customer customer = null;
        if (patient != null) {
            pet = domainService.create(patient, Patient.class);
            customer = pet.getOwner();
        }
        return getURL(customer, pet);
    }

    /**
     * Adds a link to a patient's history.
     *
     * @param patient   the patient
     * @param clinician the clinician. May be {@code null}
     * @param data      the VetCheck link data
     * @return the link, or {@code null} if the uri couldn't be parsed
     */
    public VetCheckLink addLink(Party patient, User clinician, String data) {
        VetCheckLink result = null;
        List<NameValuePair> parameters = URLEncodedUtils.parse(data, StandardCharsets.UTF_8);
        String title = getValue("title", parameters);
        String url = getValue("share_link", parameters);
        if (url != null && title != null) {
            TransactionTemplate template = new TransactionTemplate(transactionManager);
            Act act = template.execute(transactionStatus -> {
                Act link = createLink(patient, clinician, url, title);
                Act event = rules.getEventForAddition(patient, new Date(), null);
                IMObjectBean bean = service.getBean(event);
                bean.addTarget("items", link, "event");
                bean.save(link);
                return link;
            });
            result = new VetCheckLink(title, url, act);
        }
        return result;
    }

    /**
     * Returns the most recent VetCheck links for a patient.
     *
     * @param patient the patient
     * @param count   the maximum number of links to return
     * @return the <em>act.patientClinicalLink</em>s
     */
    public List<Act> getMostRecentLinks(Party patient, int count) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Act> query = builder.createQuery(Act.class);
        Root<Act> root = query.from(Act.class, PatientArchetypes.CLINICAL_LINK);
        Join<Act, IMObject> join = root.join("patient");
        join.on(builder.equal(join.get("entity"), patient.getObjectReference()));
        root.join("provider").join("entity", VETCHECK);
        query.orderBy(builder.desc(root.get("startTime")), builder.desc(root.get("id")));
        return service.createQuery(query).setMaxResults(count).getResultList();
    }

    /**
     * Builds a VetCheck URI.
     *
     * @param patient  the patient. May be {@code null}
     * @param customer the customer. May be {@code null}
     * @return the URI
     */
    private String getURL(Customer customer, Patient patient) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL);
        if (customer != null && patient != null) {
            Address address = customer.getAddress();
            Phone phone = customer.getPhone();
            Email email = customer.getEmail();
            Weight weight = patient.getWeight();
            String sex = "";
            if (patient.isDesexed()) {
                sex = patient.getSex() + " " + Messages.get("patient.desexed");
            } else {
                sex = patient.getSex() + " " + Messages.get("patient.entire");
            }
            Microchip microchip = patient.getMicrochip();
            builder.queryParam("client_first_name", customer.getFirstName())
                    .queryParam("client_last_name", customer.getLastName())
                    .queryParam("client_address", address != null ? address.format(true) : null)
                    .queryParam("client_phone", phone != null ? phone.getPhoneNumber() : null)
                    .queryParam("client_email_address", (email != null) ? email.getEmailAddress() : null)
                    .queryParam("patient_id", patient.getId())
                    .queryParam("pet_name", patient.getName())
                    .queryParam("pet_species", patient.getSpeciesName())
                    .queryParam("pet_breed", patient.getBreedName())
                    .queryParam("pet_sex", sex)
                    .queryParam("pet_dob", patient.getDateOfBirth())
                    .queryParam("pet_weight", !weight.isZero() ? weight : null)
                    .queryParam("pet_color", patient.getColourName())
                    .queryParam("pet_microchip", microchip != null ? microchip.getIdentity() : null);
        }
        return builder.toUriString();
    }

    /**
     * Creates a new link.
     *
     * @param patient     the patient
     * @param clinician   the clinician. May be {@code null}
     * @param url         the url
     * @param description the url description
     * @return a new act
     */
    private Act createLink(Party patient, User clinician, String url, String description) {
        Act act = service.create(PatientArchetypes.CLINICAL_LINK, Act.class);
        IMObjectBean bean = service.getBean(act);
        bean.setTarget("patient", patient);
        bean.setTarget("clinician", clinician);
        bean.setValue("url", url);
        bean.setValue("description", StringUtils.abbreviate(description, bean.getMaxLength("description")));
        bean.setTarget("provider", getVetCheck());
        return act;
    }

    /**
     * Returns the active <em>entity.linkProviderVetCheck</em> instance, creating one if it doesn't exist.
     *
     * @return the VetCheck instance
     */
    private Entity getVetCheck() {
        return singletonService.get(VETCHECK, Entity.class, true);
    }

    /**
     * Returns the named value from a set of parameters.
     *
     * @param name       the parameter name
     * @param parameters the parameters
     * @return the corresponding value, or {@code null} if none can be found
     */
    private String getValue(String name, List<NameValuePair> parameters) {
        String result = null;
        for (NameValuePair parameter : parameters) {
            if (name.equals(parameter.getName())) {
                result = parameter.getValue();
                break;
            }
        }
        return result;
    }
}