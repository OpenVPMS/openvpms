/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;

/**
 * Layout strategy for <em>act.EFTPOSPayment</em> and <em>act.EFTPOSRefund</em>.
 *
 * @author Tim Anderson
 */
public class EFTPOSTransactionLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The message node.
     */
    private static final String MESSAGE = "message";

    /**
     * The response node.
     */
    private static final String RESPONSE = "response";

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes nodes = ArchetypeNodes.all()
            .excludeIfEmpty("transactionId", MESSAGE, RESPONSE, "receipts");


    /**
     * Constructs an {@link EFTPOSTransactionLayoutStrategy}.
     */
    public EFTPOSTransactionLayoutStrategy() {
        super(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        Property message = properties.get(MESSAGE);
        if (message != null && message.getString() != null) {
            addComponent(createMultiLineText(message, 1, 10, null, context));
        }
        Property response = properties.get(RESPONSE);
        if (response != null && response.getString() != null) {
            addComponent(createMultiLineText(response, 1, 10, null, context));
        }
        return super.apply(object, properties, parent, context);
    }
}
