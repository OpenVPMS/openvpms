/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import org.openvpms.archetype.rules.supplier.OrderRules;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.FinancialActEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.workspace.supplier.SupplierHelper;


/**
 * An editor for <em>act.supplierDelivery</em> and <em>act.supplierReturn</em> acts.
 *
 * @author Tim Anderson
 */
public class DeliveryEditor extends FinancialActEditor {

    /**
     * The order rules.
     */
    private final OrderRules rules;

    /**
     * Constructs a {@code DeliveryEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context. May be {@code null}
     */
    public DeliveryEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        rules = SupplierHelper.createOrderRules(context.getContext().getPractice());
        initialise();
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        return new DeliveryEditor(reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Creates a delivery item linked to an order item.
     *
     * @param orderItem the order item
     * @return the delivery item
     */
    public FinancialAct createItem(FinancialAct orderItem) {
        boolean delivery = getObject().isA(SupplierArchetypes.DELIVERY);
        FinancialAct deliveryItem = (delivery) ? rules.createDeliveryItem(orderItem)
                                               : rules.createReturnItem(orderItem);
        ActRelationshipCollectionEditor items = getItems();
        items.add(deliveryItem);
        DeliveryItemEditor itemEditor = (DeliveryItemEditor) getItems().getEditor(deliveryItem);
        itemEditor.setOrderItem(orderItem);

        items.setModified(deliveryItem, true);
        // need to explicitly flag the item as modified, else it can be excluded as a default value object

        items.refresh();
        return deliveryItem;
    }

    /**
     * Returns the items collection editor.
     *
     * @return the items collection editor. May be {@code null}
     */
    @Override
    public ActRelationshipCollectionEditor getItems() {
        return super.getItems();
    }

    /**
     * Save any edits.
     * <p>
     * This uses {@link #saveChildren()} to save the children prior to invoking {@link #saveObject()}.
     * <p>
     * This order is necessary to ensure that the <em>archetypeService.save.act.supplierDelivery.before</em> rule
     * has the most recent list of items when triggered.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        saveChildren();
        saveObject();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new DeliveryLayoutStrategy(getItems());
    }

}
