/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.workflow.TaskRules;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A task editor that enables the work list to be selected, for the purposes of follow-up tasks.
 *
 * @author Tim Anderson
 */
public class FollowUpTaskEditor extends RestrictedWorkListTaskEditor {

    /**
     * The available work lists, ordered on name.
     */
    private List<Entity> workLists;

    /**
     * The default work list. May be {@code null}
     */
    private Entity defaultWorkList;

    /**
     * Constructs a {@link FollowUpTaskEditor}.
     *
     * @param act       the act to edit
     * @param workLists the work lists for follow-up tasks, in order of preference
     * @param context   the layout context
     */
    public FollowUpTaskEditor(Act act, List<Entity> workLists, LayoutContext context) {
        super(act, null, context);
        initWorkLists(workLists);
        setStartTime(new Date());
        initWorkListEditor();
    }

    /**
     * Returns a display name for the object being edited.
     *
     * @return a display name for the object
     */
    @Override
    public String getDisplayName() {
        return Messages.get("patient.followup.task");
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new FollowUpTaskEditor(reload(getObject()), workLists, getLayoutContext());
    }

    /**
     * Returns the follow-up work lists.
     *
     * @param context the context
     * @return the follow-up work lists
     */
    public static List<Entity> getWorkLists(Context context) {
        return getWorkLists(context.getClinician(), context.getUser(), context.getLocation());
    }

    /**
     * Returns the available follow-up work lists for a clinician, user and practice location.
     *
     * @param clinician the clinician. May be {@code null}
     * @param user      the user. May be {@code null}
     * @param location  the practice location. May be {@code null}
     * @return the follow-up work lists
     */
    public static List<Entity> getWorkLists(User clinician, User user, Party location) {
        Set<Entity> matches = new LinkedHashSet<>();
        UserRules userRules = ServiceHelper.getBean(UserRules.class);
        LocationRules locationRules = ServiceHelper.getBean(LocationRules.class);
        if (clinician != null) {
            matches.addAll(userRules.getFollowupWorkLists(clinician));
        }
        if (user != null && !Objects.equals(user, clinician)) {
            matches.addAll(userRules.getFollowupWorkLists(user));
        }
        if (location != null) {
            matches.addAll(locationRules.getFollowupWorkLists(location));
        }
        return new ArrayList<>(matches);
    }

    /**
     * Returns follow-up work lists that have the task type.
     *
     * @param context  the context
     * @param taskType the task type
     * @return the follow-up work lists
     */
    public static List<Entity> getWorkLists(Context context, Entity taskType) {
        List<Entity> worklists = getWorkLists(context);
        TaskRules rules = ServiceHelper.getBean(TaskRules.class);
        return worklists.stream()
                .filter(worklist -> rules.hasTaskType(worklist, taskType))
                .collect(Collectors.toList());
    }

    /**
     * Invoked when layout has completed. All editors have been created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        getParticipationEditor("clinician", false).addModifiableListener(modifiable -> onClinicianChanged());
    }

    /**
     * Returns a default work list.
     *
     * @return a default work list, or {@code null} if there is no default
     */
    @Override
    protected Entity getDefaultWorkList() {
        return defaultWorkList;
    }

    /**
     * Creates an editor to edit a work list participation.
     *
     * @param participation the participation to edit
     * @return a new editor
     */
    @Override
    protected ParticipationEditor<Entity> createWorkListEditor(Participation participation) {
        return new RestrictedWorkListParticipationEditor(participation, getObject(), getLayoutContext()) {
            @Override
            protected Query<Entity> createWorkListQuery(String name) {
                RestrictedWorkListQuery query = new RestrictedWorkListQuery(workLists);
                if (name != null) {
                    Entity entity = getEntity();
                    if (entity == null || !StringUtils.equals(entity.getName(), name)) {
                        query.setValue(name);
                    }
                }
                return query;
            }
        };
    }

    /**
     * Creates the property set.
     * <p/>
     * This makes the customer and patient read-only.
     *
     * @param object    the object being edited
     * @param archetype the object archetype
     * @param variables the variables for macro expansion. May be {@code null}
     * @return the property set
     */
    @Override
    protected PropertySet createPropertySet(IMObject object, ArchetypeDescriptor archetype, Variables variables) {
        return new PropertySetBuilder(object, archetype, variables)
                .setReadOnly(CUSTOMER)
                .setReadOnly(PATIENT)
                .build();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new FollowUpLayoutStrategy();
    }

    /**
     * Initialises the work lists.
     *
     * @param workLists the work lists
     */
    private void initWorkLists(List<Entity> workLists) {
        defaultWorkList = (!workLists.isEmpty()) ? workLists.get(0) : null;
        this.workLists = new ArrayList<>(workLists);
        IMObjectSorter.sort(workLists, "name");
    }

    /**
     * Invoked when the clinician changes.
     * <p/>
     * Updates the available follow-up work lists.
     */
    private void onClinicianChanged() {
        Context context = getLayoutContext().getContext();
        initWorkLists(getWorkLists(getClinician(), context.getUser(), context.getLocation()));
    }

    private class FollowUpLayoutStrategy extends LayoutStrategy {

        public FollowUpLayoutStrategy() {
            setArchetypeNodes(new ArchetypeNodes(true, true).simple(WORK_LIST).order(WORK_LIST, START_TIME)
                                      .order(TASK_TYPE, START_TIME).hidden(true));
            addComponent(new ComponentState(getWorkListCollectionEditor()));
        }
    }
}
