/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.roster;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.help.HelpContext;

import java.util.Date;

/**
 * Roster CRUD window for the {@link AreaRosterBrowser} roster view.
 *
 * @author Tim Anderson
 */
class AreaRosterCRUDWindow extends RosterCRUDWindow {

    /**
     * Constructs an {@link AreaRosterCRUDWindow}.
     *
     * @param browser the browser
     * @param context the context
     * @param help    the help context
     */
    AreaRosterCRUDWindow(AreaRosterBrowser browser, Context context, HelpContext help) {
        super(browser, context, help);
    }

    /**
     * Invoked when a new object has been created.
     *
     * @param object the new object
     */
    @Override
    protected void onCreated(Act object) {
        IMObjectBean bean = getBean(object);
        RosterBrowser browser = getBrowser();
        bean.setTarget("schedule", browser.getSelectedEntity());
        Date date = browser.getSelectedDate();
        bean.setValue("startTime", DateRules.getDate(date, 9, DateUnits.HOURS));
        super.onCreated(object);
    }

    /**
     * Populates an entity, copying or moving an event.
     *
     * @param editor the editor
     * @param entity the entity
     */
    @Override
    protected void setEntity(RosterEventEditor editor, Entity entity) {
        editor.setSchedule(entity);
    }
}
