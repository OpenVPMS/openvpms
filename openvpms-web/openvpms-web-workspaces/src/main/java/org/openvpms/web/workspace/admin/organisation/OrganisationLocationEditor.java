/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityLinkCollectionTargetPropertyEditor;
import org.openvpms.web.component.im.relationship.MultipleRelationshipCollectionTargetEditor;
import org.openvpms.web.component.im.table.BaseIMObjectTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.print.BoundPrinterField;
import org.openvpms.web.component.print.PrintHelper;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.admin.organisation.printer.PrinterEditor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Editor for <em>party.organisationLocation</em>
 * <p>
 * This:
 * <ul>
 * <li>displays a list of a available printers for the "defaultPrinter" node
 * <li>displays an editor for the practice location logo</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class OrganisationLocationEditor extends AbstractOrganisationEditor {

    /**
     * The printer collection.
     */
    private final PrinterCollectionEditor printers;

    /**
     * All available printer references.
     */
    private final List<PrinterReference> printerReferences;

    /**
     * Constructs an {@link OrganisationLocationEditor}
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public OrganisationLocationEditor(Party object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        printerReferences = PrintHelper.getPrinters();
        printers = new PrinterCollectionEditor(getCollectionProperty("printers"), object, layoutContext);
        addEditor(printers);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LocationLayoutStrategy();
    }

    private class PrinterCollectionEditor extends MultipleRelationshipCollectionTargetEditor {

        /**
         * Constructs a {@link PrinterCollectionEditor}.
         *
         * @param property the collection property
         * @param entity   the parent entity
         * @param context  the layout context
         */
        PrinterCollectionEditor(CollectionProperty property, Entity entity, LayoutContext context) {
            super(new EntityLinkCollectionTargetPropertyEditor(property, entity, context.getCache()), entity, context);
        }

        /**
         * Creates a new editor.
         *
         * @param object  the object to edit
         * @param context the layout context
         * @return an editor to edit {@code object}
         */
        @Override
        public IMObjectEditor createEditor(IMObject object, LayoutContext context) {
            return new PrinterEditor(object, getObject(), getAvailablePrinters(object), context);
        }

        /**
         * Sets the current editor.
         *
         * @param editor the editor. May be {@code null}
         */
        @Override
        protected void setCurrentEditor(IMObjectEditor editor) {
            if (editor instanceof PrinterEditor) {
                ((PrinterEditor) editor).setAvailablePrinters(getAvailablePrinters(editor.getObject()));
            }
            super.setCurrentEditor(editor);
        }

        /**
         * Create a new table model.
         *
         * @param context the layout context
         * @return a new table model
         */
        @Override
        protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
            return new PrinterTableModel();
        }

        /**
         * Enable/disables the buttons.
         * <p>
         * This only enables the add button if there are printers available.
         *
         * @param enable if {@code true} enable buttons (subject to criteria), otherwise disable them
         */
        @Override
        protected void enableNavigation(boolean enable) {
            boolean enableAdd = enable;
            if (enable) {
                List<PrinterReference> available = getAvailablePrinters(null);
                enableAdd = !available.isEmpty();
            }
            super.enableNavigation(enable, enableAdd);
        }

        /**
         * Returns printer references that haven't been allocated to other <em>entity.printer</em>> instances.
         *
         * @param current the current object. May be {@code null}
         * @return the available printer names
         */
        List<PrinterReference> getAvailablePrinters(IMObject current) {
            Set<PrinterReference> allocated = new HashSet<>();
            for (IMObject object : getCurrentObjects()) {
                if (current != null && !object.equals(current)) {
                    IMObjectBean bean = getBean(object);
                    PrinterReference reference = PrinterReference.fromString(bean.getString("printer"));
                    if (reference != null) {
                        allocated.add(reference);
                    }
                }
            }
            List<PrinterReference> available = new ArrayList<>(printerReferences);
            available.removeAll(allocated);
            return available;
        }
    }

    private class PrinterTableModel extends BaseIMObjectTableModel<IMObject> {

        private final int statusIndex;

        /**
         * Constructs a new {@code BaseIMObjectTableModel}, using
         * a new column model created by {@link #createTableColumnModel}.
         */
        public PrinterTableModel() {
            super(null);
            DefaultTableColumnModel model = (DefaultTableColumnModel) createTableColumnModel(false, false, true, false,
                                                                                             false);
            statusIndex = getNextModelIndex(model);
            model.addColumn(createTableColumn(statusIndex, "printer.status"));
            setTableColumnModel(model);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate
         */
        @Override
        protected Object getValue(IMObject object, TableColumn column, int row) {
            Object result;
            if (column.getModelIndex() == statusIndex) {
                IMObjectBean bean = getBean(object);
                PrinterReference reference = PrinterReference.fromString(bean.getString("printer"));
                if (printerReferences.contains(reference)) {
                    result = Messages.get("printer.status.available");
                } else {
                    result = Messages.get("printer.status.unknown");
                }
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }
    }

    private class LocationLayoutStrategy extends OrganisationLocationLayoutStrategy {

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            Property defaultPrinter = properties.get("defaultPrinter");
            SelectField field = new BoundPrinterField(defaultPrinter, printerReferences);
            addComponent(new ComponentState(field, defaultPrinter));
            addComponent(new ComponentState(printers));
            return super.apply(object, properties, parent, context);
        }

        /**
         * Returns a component representing the practice location logo.
         *
         * @param object  the practice location
         * @param context the layout context
         * @return a new component
         */
        @Override
        protected ComponentState getLogo(IMObject object, LayoutContext context) {
            return getLogo(getLogoEditor());
        }
    }
}