/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;

import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * Abstract implementation of the {@link AppointmentGrid} interface.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAppointmentGrid extends AbstractScheduleEventGrid implements AppointmentGrid {

    /**
     * Appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The roster service.
     */
    private final RosterService rosterService;

    /**
     * The grid start time, as minutes since midnight.
     */
    private int startMins;

    /**
     * The grid end time, as minutes since midnight.
     */
    private int endMins;

    /**
     * Determines if the roster should be displayed.
     */
    private boolean showRoster;

    /**
     * The slot size, in minutes.
     */
    private int slotSize = DEFAULT_SLOT_SIZE;

    /**
     * The default slot size, in minutes.
     */
    static final int DEFAULT_SLOT_SIZE = 15;

    /**
     * The default start time, as minutes from midnight.
     */
    static final int DEFAULT_START = 8 * 60;

    /**
     * The default end time, as minutes from midnight.
     */
    static final int DEFAULT_END = 18 * 60;

    /**
     * The maximum end time, in minutes.
     */
    private static final int MAX_TIME = 24 * 60;


    /**
     * Constructs an {@link AbstractAppointmentGrid}.
     *
     * @param scheduleView  the schedule view. May be {@code null}
     * @param date          the grid start and end date
     * @param startMins     the grid start time, as minutes from midnight
     * @param endMins       the grid end time, as minutes from midnight
     * @param rules         the appointment rules
     * @param rosterService the roster service
     */
    public AbstractAppointmentGrid(Entity scheduleView, Date date, int startMins, int endMins, AppointmentRules rules,
                                   RosterService rosterService) {
        super(scheduleView, date, rules);
        this.startMins = startMins;
        this.endMins = endMins;
        this.rules = rules;
        this.rosterService = rosterService;
    }

    /**
     * Returns the no. of minutes from midnight that the grid starts at.
     *
     * @return the minutes from midnight that the grid starts at
     */
    @Override
    public int getStartMins() {
        return startMins;
    }

    /**
     * Returns the no. of minutes from midnight that the grid ends at.
     *
     * @return the minutes from midnight that the grid ends at
     */
    @Override
    public int getEndMins() {
        return endMins;
    }

    /**
     * Returns the no. of slots in the grid.
     *
     * @return the no. of slots
     */
    @Override
    public int getSlots() {
        return getSlotTimes().size();
    }

    /**
     * Returns the size of each slot, in minutes.
     *
     * @return the slot size, in minutes
     */
    @Override
    public int getSlotSize() {
        return slotSize;
    }

    /**
     * Returns the no. of slots that an event occupies, from the specified slot.
     * <p>
     * If the event begins prior to the slot, the remaining slots will be returned.
     *
     * @param event    the event
     * @param schedule the schedule
     * @param slot     the starting slot
     * @return the no. of slots that the event occupies
     */
    @Override
    public int getSlots(PropertySet event, Schedule schedule, int slot) {
        Date startTime = getStartTime(slot);
        Date endTime = event.getDate(ScheduleEvent.ACT_END_TIME);
        int endSlot = getSlot(endTime, true);
        int slots = endSlot - slot;
        if (slots > 1 && Schedule.isBlockingEvent(event)) {
            PropertySet next = schedule.getEventAfter(event, startTime);
            if (next != null) {
                Date nextStartTime = next.getDate(ScheduleEvent.ACT_START_TIME);
                int nextStartSlot = getSlot(nextStartTime);
                if (nextStartSlot < endSlot) {
                    slots = nextStartSlot - slot;
                }
            }
        }
        return slots;
    }

    /**
     * Returns the time that the specified slot starts at.
     *
     * @param slot the slot
     * @return the start time of the specified slot
     */
    @Override
    public Date getStartTime(int slot) {
        return Date.from(getSlot(slot).getStart().toInstant());
    }

    /**
     * Returns the time that the specified slot ends at.
     *
     * @param slot the slot
     * @return the end time of the specified slot
     */
    @Override
    public Date getEndTime(int slot) {
        return Date.from(getSlot(slot).getEnd().toInstant());
    }

    /**
     * Returns the time that the specified slot starts at.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the start time of the specified slot
     */
    @Override
    public Date getStartTime(Schedule schedule, int slot) {
        return getStartTime(slot);
    }

    /**
     * Returns the time that the specified slot ends at.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the end time of the specified slot. May be {@code null}
     */
    @Override
    public Date getEndTime(Schedule schedule, int slot) {
        return getEndTime(slot);
    }

    /**
     * Returns the hour of the specified slot.
     *
     * @param slot the slot
     * @return the hour, in the range 0..23
     */
    @Override
    public int getHour(int slot) {
        return getSlotTimes().get(slot).getStart().getHour();
    }

    /**
     * Determines the availability of a slot for the specified schedule.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the availability of the schedule
     */
    @Override
    public Availability getAvailability(Schedule schedule, int slot) {
        if (getEvent(schedule, slot) != null) {
            return Availability.BUSY;
        }
        ZonedDateTime time = getSlot(slot).getStart();
        if (time.compareTo(getSlotTimeForMinutes(schedule.getStartMins())) >= 0 &&
            time.compareTo(getSlotTimeForMinutes(schedule.getEndMins())) < 0) {
            return Availability.FREE;
        }
        return Availability.UNAVAILABLE;
    }

    /**
     * Determines how many slots are unavailable from the specified slot, for
     * a schedule.
     *
     * @param schedule the schedule
     * @param slot     the starting slot
     * @return the no. of concurrent slots that are unavailable
     */
    @Override
    public int getUnavailableSlots(Schedule schedule, int slot) {
        int slots = getSlots();
        int i = slot;
        while (i < slots && getAvailability(schedule, i) == Availability.UNAVAILABLE) {
            ++i;
        }
        return i - slot;
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time the time
     * @return the slot, or {@code -1} if the time is before the first slot, or {@link #getSlots()} if it is after the
     * last slot
     */
    @Override
    public int getSlot(Date time) {
        return getSlot(toZonedDateTime(time));
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time the time
     * @return the slot, or {@code -1} if the time is before the first slot, or {@link #getSlots()} if it is after the
     * last slot
     */
    public int getSlot(Date time, boolean roundUp) {
        return getSlot(toZonedDateTime(time), roundUp);
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time the time
     * @return the slot, or {@code -1} if the time is before the first slot, or {@link #getSlots()} if it is after the
     * last slot
     */
    @Override
    public int getSlot(ZonedDateTime time) {
        return getSlot(time, false);
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time    the time
     * @param roundUp if {@code true} round up to the nearest slot, otherwise round down
     * @return the slot, or {@code -1} if the time is before the first slot, or {@link #getSlots()} if it is after the
     * last slot
     */
    @Override
    public int getSlot(ZonedDateTime time, boolean roundUp) {
        List<Slot> slots = getSlotTimes();
        Slot key = new Slot(time, time);
        int index = Collections.binarySearch(slots, key, (o1, o2) -> {
            // handle case where a single timestamp is used to find an intersecting slot
            ZonedDateTime start1 = o1.getStart();
            ZonedDateTime end1 = o1.getEnd();
            ZonedDateTime start2 = o2.getStart();
            ZonedDateTime end2 = o2.getEnd();

            if (start1.equals(end1)) {
                if (start1.compareTo(start2) >= 0 && start1.compareTo(end2) < 0) {
                    return 0;
                }
            } else if (start2.equals(end2)) {
                if (start2.compareTo(start1) >= 0 && start2.compareTo(end1) < 0) {
                    return 0;
                }
            }
            if (end1.compareTo(start2) <= 0) {
                return -1;
            }
            if (start1.compareTo(end2) >= 0) {
                return 1;
            }
            return 0;
        });
        if (index < 0) {
            // if the index == -1, the time is before the first slot, else it is after the last slot
            return (index == -1) ? index : slots.size();
        } else if (roundUp) {
            Slot slot = slots.get(index);
            if (time.compareTo(slot.getStart()) > 0) {
                index++;
            }
        }
        return index;
    }

    /**
     * Determines if the roster should be displayed.
     *
     * @return {@code true} if the roster should be displayed
     */
    @Override
    public boolean showRoster() {
        return showRoster;
    }

    /**
     * Determines if the roster should be displayed.
     *
     * @param show if {@code true}, show the roster
     */
    @Override
    public void setShowRoster(boolean show) {
        this.showRoster = show;
    }

    /**
     * Returns the slot size of a schedule.
     *
     * @param schedule the schedule
     * @param rules    the appointment rules
     * @return the slot size
     */
    public static int getSlotSize(Entity schedule, AppointmentRules rules) {
        int slotSize = rules.getSlotSize(schedule);
        if (slotSize <= 0) {
            slotSize = DEFAULT_SLOT_SIZE;
        }
        return slotSize;
    }

    /**
     * Sets the no. of minutes from midnight that the grid starts at.
     *
     * @param startMins the minutes from midnight that the grid starts at
     */
    protected void setStartMins(int startMins) {
        this.startMins = startMins;
    }

    /**
     * Sets the no. of minutes from midnight that the grid ends at.
     *
     * @param endMins the minutes from midnight that the grid ends at
     */
    protected void setEndMins(int endMins) {
        this.endMins = endMins;
    }

    /**
     * Sets the size of each slot, in minutes.
     *
     * @param slotSize the slot size, in minutes
     */
    protected void setSlotSize(int slotSize) {
        this.slotSize = slotSize;
    }

    /**
     * Returns the minutes from midnight for the specified time, rounded up or down to the nearest slot.
     *
     * @param time    the time
     * @param roundUp if {@code true} round up to the nearest slot, otherwise round down
     * @return the minutes from midnight for the specified time
     */
    protected int getSlotMinutes(Date time, boolean roundUp) {
        return rules.getSlotMinutes(time, slotSize, roundUp);
    }

    /**
     * Creates a new {@link Schedule}.
     *
     * @param schedule the schedule
     * @return a new schedule
     */
    protected Schedule createSchedule(Entity schedule) {
        IMObjectBean bean = IMObjectHelper.getBean(schedule);
        Date start = bean.getDate("startTime");
        int startMins;
        int endMins;
        int slotSize = getSlotSize(schedule, rules);

        if (start != null) {
            startMins = getGridMinutes(start);
        } else {
            startMins = DEFAULT_START;
        }

        Date end = bean.getDate("endTime");
        if (end != null) {
            endMins = getGridMinutes(end);
        } else {
            endMins = DEFAULT_END;
        }
        if (endMins < startMins) {
            endMins = startMins;
        }

        List<Entity> areas = rosterService.getAreas(schedule.getObjectReference());
        return new Schedule(schedule, null, startMins, endMins, slotSize, areas, rules);
    }

    /**
     * Returns a slot time for a number of minutes relative to the start date.
     *
     * @param startMins the minutes
     * @return the corresponding slot time
     */
    ZonedDateTime getSlotTimeForMinutes(int startMins) {
        ZonedDateTime date = getStartZonedDateTime();
        return getTimePlusMinutes(date, startMins);
    }

    /**
     * Returns a slot given its index.
     *
     * @param slot the slot index
     * @return the corresponding sot
     */
    private Slot getSlot(int slot) {
        List<Slot> slots = getSlotTimes();
        if (slot >= slots.size()) {
            throw new IllegalArgumentException("Argument 'slot'=" + slot + " exceeds slots: " + slots.size());
        }
        return slots.get(slot);
    }

    /**
     * Returns a {@code ZonedDateTime} relative to the start date.
     *
     * @param minutes the number of minutes to add. Must not exceed 24 * 60.
     * @return the time relative to the start date
     */
    private ZonedDateTime getTimePlusMinutes(ZonedDateTime start, int minutes) {
        return (minutes < 24 * 60) ? start.withHour(minutes / 60).withMinute(minutes % 60)
                                   : start.plusDays(1);
    }

    /**
     * Determines an appointment grid boundary, in minutes from midnight.
     * <p>
     * This supports minutes in the range 00:00..24:00
     *
     * @param time the time
     * @return the minutes from midnight for {@code time}
     */
    private int getGridMinutes(Date time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        int day = calendar.get(Calendar.DAY_OF_MONTH) - 1;
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int mins = calendar.get(Calendar.MINUTE);
        int result = (day * 24 * 60) + (hour * 60) + mins;
        if (result < 0 || result > MAX_TIME) {
            result = DEFAULT_START;
        }
        return result;
    }


}
