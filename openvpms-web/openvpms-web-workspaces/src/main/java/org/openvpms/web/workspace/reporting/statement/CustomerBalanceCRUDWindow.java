/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import org.openvpms.archetype.component.processor.BatchProcessorListener;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceSummaryQuery;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.print.PrinterContextFactory;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workspace.AbstractCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.customer.account.InteractiveStatementPrinter;
import org.openvpms.web.workspace.customer.account.StatementPrinter;

import java.util.Date;

/**
 * .
 *
 * @author Tim Anderson
 */
public class CustomerBalanceCRUDWindow extends AbstractCRUDWindow<Act> {

    /**
     * The query.
     */
    private final CustomerBalanceQuery query;

    /**
     * The browser.
     */
    private final CustomerBalanceBrowser browser;

    /**
     * Constructs a {@link CustomerBalanceCRUDWindow}.
     *
     * @param query   the query
     * @param browser the browser
     * @param context the context
     * @param help    the help context
     */
    public CustomerBalanceCRUDWindow(CustomerBalanceQuery query, CustomerBalanceBrowser browser, Context context,
                                     HelpContext help) {
        super(Archetypes.create(CustomerAccountArchetypes.ACCOUNT_ACTS, Act.class), ActActions.view(), context, help);
        this.query = query;
        this.browser = browser;
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button set
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        UserRules rules = ServiceHelper.getBean(UserRules.class);
        boolean auth = rules.canEdit(getContext().getUser(), CustomerAccountArchetypes.OPENING_BALANCE);
        if (auth) {
            buttons.add("sendAll", this::onSendAll);
        }
        buttons.add("print", this::onPrint);
        buttons.add("report", this::onReport);
        if (auth) {
            buttons.add("endPeriod", this::onEndPeriod);
        }
    }

    /**
     * Invoked when the 'print' button is pressed to print the selected statement.
     */
    @Override
    protected void onPrint() {
        ObjectSet selected = browser.getSelected();
        if (selected != null) {
            Reference ref = selected.getReference(CustomerBalanceSummaryQuery.CUSTOMER_REFERENCE);
            Party customer = (Party) IMObjectHelper.getObject(ref, getContext());
            if (customer != null) {
                HelpContext help = getHelpContext().subtopic("print");
                Context context = LocalContext.copy(getContext());
                context.setPatient(null);
                context.setCustomer(customer);
                MailContext mailContext = new CustomerMailContext(context, help);
                PrinterContext printerContext = ServiceHelper.getBean(PrinterContextFactory.class).create();
                StatementPrinter printer = new StatementPrinter(
                        context, ServiceHelper.getBean(CustomerAccountRules.class), printerContext,
                        ServiceHelper.getBean(ReporterFactory.class));
                InteractiveStatementPrinter interactive = new InteractiveStatementPrinter(printer, context, help);
                interactive.setMailContext(mailContext);
                interactive.print();
            }
        }
    }

    /**
     * Invoked when the 'send all' button is pressed.
     */
    private void onSendAll() {
        if (checkStatementDate("reporting.statements.run.invalidDate")) {
            String title = Messages.get("reporting.statements.run.title");
            String message = Messages.get("reporting.statements.run.message");
            HelpContext help = getHelpContext().subtopic("confirmsend");
            SendStatementsDialog dialog = new SendStatementsDialog(title, message, help);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    doSendAll(dialog.reprint());
                }
            });
            dialog.show();
        }
    }

    /**
     * Processes all customers matching the criteria.
     *
     * @param reprint if {@code true}, process statements that have been printed.
     */
    private void doSendAll(boolean reprint) {
        HelpContext help = getHelpContext().subtopic("send");
        StatementGenerator generator = new StatementGenerator(query, getContext(), getMailContext(), help);
        generator.setReprint(reprint);
        generateStatements(generator, true);
    }

    /**
     * Invoked when the 'end period' button is pressed.
     */
    private void onEndPeriod() {
        if (checkStatementDate("reporting.statements.eop.invalidDate")) {
            String title = Messages.get("reporting.statements.eop.title");
            String message = Messages.get("reporting.statements.eop.message");
            HelpContext help = getHelpContext().subtopic("endperiod");
            EndOfPeriodDialog dialog = new EndOfPeriodDialog(title, message, help);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    doEndPeriod(dialog.postCompletedInvoices(), help);
                }
            });
            dialog.show();
        }
    }

    /**
     * Runs end period.
     *
     * @param postCompletedInvoices if {@code true}, post completed invoices
     * @param help                  the help context
     */
    private void doEndPeriod(boolean postCompletedInvoices, HelpContext help) {
        Date date = DateRules.getDate(query.getDate());
        EndOfPeriodGenerator generator = new EndOfPeriodGenerator(date, postCompletedInvoices, getContext(), help);
        generator.setListener(new BatchProcessorListener() {
            public void completed() {
                browser.query();
            }

            public void error(Throwable exception) {
                ErrorHelper.show(exception);
            }
        });
        generator.process();
    }

    /**
     * Generates statements.
     *
     * @param generator the statement generator
     * @param refresh   if {@code true}, refresh the browser on completion
     */
    private void generateStatements(StatementGenerator generator, boolean refresh) {
        generator.setListener(new BatchProcessorListener() {
            public void completed() {
                if (refresh) {
                    browser.query();
                }
            }

            public void error(Throwable exception) {
                generator.getProcessor().setCancel(true);
                ErrorHelper.show(exception);
            }
        });
        generator.process();
    }

    /**
     * Verifies that the statement date is at least a day prior to the current
     * date.
     *
     * @param errorKey the error message key, if the date is invalid
     * @return {@code true} if the statement date is less than today
     */
    private boolean checkStatementDate(String errorKey) {
        Date statementDate = query.getDate();
        Date date = DateRules.getYesterday();
        if (date.compareTo(statementDate) < 0) {
            ErrorDialog.show(Messages.get(errorKey));
            return false;
        }
        return true;
    }

    /**
     * Invoked when the 'Report' button is pressed.
     */
    private void onReport() {
        IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
        Context context = getContext();
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator("CUSTOMER_BALANCE", context);
        IMPrinter<ObjectSet> printer = factory.createObjectSetReportPrinter(query.getObjects(), locator, context);
        String type;
        if (query.queryAllBalances()) {
            type = Messages.get("reporting.statements.print.all");
        } else if (query.queryOverduebalances()) {
            type = Messages.get("reporting.statements.print.overdue");
        } else {
            type = Messages.get("reporting.statements.print.nonOverdue");
        }
        String title = Messages.format("imobject.print.title", type);
        HelpContext help = getHelpContext().subtopic("report");
        InteractiveIMPrinter<ObjectSet> iPrinter = new InteractiveIMPrinter<>(title, printer, context, help);
        iPrinter.setMailContext(getMailContext());
        iPrinter.print();
    }

}
