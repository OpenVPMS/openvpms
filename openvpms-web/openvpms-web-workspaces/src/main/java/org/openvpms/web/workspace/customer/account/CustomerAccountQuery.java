/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientQuery;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.select.AbstractSelectorListener;
import org.openvpms.web.component.im.select.IMObjectSelector;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.exists;
import static org.openvpms.component.system.common.query.Constraints.idEq;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.subQuery;

/**
 * Query for customer account acts.
 * <p/>
 * This allows acts to be restricted to those containing items for a particular patient.
 *
 * @author Tim Anderson
 */
public class CustomerAccountQuery<T extends Act> extends DateRangeActQuery<T> {

    /**
     * The patient selector.
     */
    private final IMObjectSelector<Party> patientSelector;

    /**
     * The act statuses.
     */
    private static final ActStatuses STATUSES;

    /**
     * The account act archetypes.
     */
    private static final String[] ACCOUNT_ACTS = CustomerAccountArchetypes.ACCOUNT_ACTS.toArray(new String[0]);

    static {
        STATUSES = new ActStatuses(INVOICE);
        STATUSES.setDefault((String) null);
    }

    /**
     * Constructs a {@link CustomerAccountQuery}.
     *
     * @param customer the customer
     * @param context  the context
     */
    @SuppressWarnings("unchecked")
    public CustomerAccountQuery(Party customer, LayoutContext context) {
        super(customer, "customer", CustomerArchetypes.CUSTOMER_PARTICIPATION, ACCOUNT_ACTS, STATUSES,
              (Class<T>) FinancialAct.class);
        patientSelector = createPatientSelector(customer, context);
    }

    /**
     * Constructs a {@link CustomerAccountQuery}.
     *
     * @param customer the customer
     * @param statuses the statuses to query
     * @param context  the layout context
     */
    @SuppressWarnings("unchecked")
    public CustomerAccountQuery(Party customer, String[] statuses, LayoutContext context) {
        super(customer, "customer", CustomerArchetypes.CUSTOMER_PARTICIPATION, ACCOUNT_ACTS, statuses,
              (Class<T>) FinancialAct.class);
        patientSelector = createPatientSelector(customer, context);
    }

    /**
     * Constructs a {@link CustomerAccountQuery}.
     *
     * @param customer   the customer
     * @param archetypes the archetypes to query
     * @param statuses   the statuses to query
     * @param context    the layout context
     */
    @SuppressWarnings("unchecked")
    public CustomerAccountQuery(Party customer, String[] archetypes, ActStatuses statuses, LayoutContext context) {
        super(customer, "customer", CustomerArchetypes.CUSTOMER_PARTICIPATION, archetypes,
              statuses, (Class<T>) FinancialAct.class);

        patientSelector = createPatientSelector(customer, context);
    }

    /**
     * Sets the patient to filter acts by.
     *
     * @param patient the patient. May be {@code null}
     */
    public void setPatient(Party patient) {
        patientSelector.setObject(patient);
    }

    /**
     * Creates a patient selector.
     *
     * @param customer the customer to restrict patients to
     * @param context  the layout context
     * @return the patient selector
     */
    protected IMObjectSelector<Party> createPatientSelector(Party customer, LayoutContext context) {
        IMObjectSelector<Party> selector = new IMObjectSelector<Party>(
                DescriptorHelper.getDisplayName(PatientArchetypes.PATIENT, ServiceHelper.getArchetypeService()),
                context, PatientArchetypes.PATIENT) {
            @Override
            protected Query<Party> createQuery(String value) {
                PatientQuery query = new PatientQuery(customer);
                query.setValue(value);
                return query;
            }
        };
        selector.getTextField().setWidth(new Extent(20, Extent.EX));
        selector.setListener(new AbstractSelectorListener<Party>() {
            @Override
            public void selected(Party object) {
                onQuery();
            }
        });
        return selector;
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        super.doLayout(container);
        String displayName = DescriptorHelper.getDisplayName(CustomerAccountArchetypes.INVOICE_ITEM, "patient",
                                                             getService());
        container.add(LabelFactory.text(displayName));
        container.add(patientSelector.getComponent());
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<T> createResultSet(SortConstraint[] sort) {
        ResultSet<T> result = null;
        ShortNameConstraint constraint = getArchetypeConstraint();
        Party patient = patientSelector.getObject();
        if (patient != null) {
            // if a patient is present, restrict the archetypes to those with patients on their items
            List<String> original = Arrays.asList(constraint.getShortNames());
            List<String> archetypes = new ArrayList<>(original);
            archetypes.removeIf(archetype -> !archetype.equals(INVOICE) && !archetype.equals(CREDIT));
            if (archetypes.isEmpty()) {
                // the selected archetypes don't support patients
                result = new ListResultSet<>(Collections.emptyList(), getMaxResults());
            } else if (!original.equals(archetypes)) {
                constraint = new ShortNameConstraint(constraint.getAlias(),
                                                     archetypes.toArray(new String[0]), true, true);
            }
        }
        if (result == null) {
            result = new AccountActResultSet(constraint, getParticipantConstraint(), getFrom(), getTo(),
                                             getStatuses(), excludeStatuses(), getConstraints(), getMaxResults(), sort);
        }
        return result;
    }

    private class AccountActResultSet extends ActResultSet<T> {

        /**
         * Constructs an {@link AccountActResultSet}.
         *
         * @param archetypes   the act archetype constraint
         * @param participants the participant constraints. May be {@code null}
         * @param from         the act start-from date. May be {@code null}
         * @param to           the act start-to date. May be {@code null}
         * @param statuses     the act statuses. If empty, indicates all acts
         * @param exclude      if {@code true} exclude acts with status in {@code statuses}; otherwise include them
         * @param constraints  additional query constraints. May be {@code null}
         * @param pageSize     the maximum no. of results per page
         * @param sort         the sort criteria. May be {@code null}
         */
        public AccountActResultSet(ShortNameConstraint archetypes, ParticipantConstraint participants,
                                   Date from, Date to, String[] statuses, boolean exclude, IConstraint constraints,
                                   int pageSize, SortConstraint[] sort) {
            super(archetypes, participants, from, to, statuses, exclude, constraints, pageSize, sort);
        }

        /**
         * Creates a new archetype query.
         *
         * @return a new archetype query
         */
        @Override
        protected ArchetypeQuery createQuery() {
            ShortNameConstraint archetypes = getArchetypes();
            IArchetypeRuleService service = ServiceHelper.getArchetypeService();
            String[] relationships = DescriptorHelper.getNodeShortNames(archetypes.getShortNames(), "items", service);

            ArchetypeQuery query = super.createQuery();
            Party patient = patientSelector.getObject();
            if (patient != null) {
                // only return those acts with items linked to the patient
                query.add(exists(subQuery(relationships, "items")
                                         .add(idEq("act", "source")).add(join("target").add(join("patient").add(
                                eq("entity", patient))))));
            }
            return query;
        }
    }
}
