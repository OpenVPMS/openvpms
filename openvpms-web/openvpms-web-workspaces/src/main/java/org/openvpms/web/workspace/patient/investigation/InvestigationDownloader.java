/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentActDownloader;
import org.openvpms.web.component.im.doc.Downloader;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.report.InvestigationTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;

/**
 * A {@link Downloader} for patient investigations.
 *
 * @author Tim Anderson
 */
public class InvestigationDownloader extends DocumentActDownloader {

    /**
     * Constructs a {@link DocumentActDownloader}.
     *
     * @param act           the act
     * @param context       the context
     * @param formatter     the file name formatter
     * @param reportFactory the report factory
     */
    public InvestigationDownloader(DocumentAct act, Context context, FileNameFormatter formatter,
                                   ReportFactory reportFactory) {
        super(act, context, formatter, reportFactory);
    }

    /**
     * Returns the template locator.
     *
     * @return the template locator
     */
    @Override
    protected DocumentTemplateLocator getTemplateLocator() {
        return new InvestigationTemplateLocator(getService());
    }
}