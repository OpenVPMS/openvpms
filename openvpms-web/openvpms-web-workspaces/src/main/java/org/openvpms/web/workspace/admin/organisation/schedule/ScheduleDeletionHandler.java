/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.schedule;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.delete.AbstractEntityDeletionHandler;
import org.openvpms.web.component.im.delete.IMObjectDeletionHandler;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * An {@link IMObjectDeletionHandler} for <em>party.organisationSchedule</em>.
 *
 * @author Tim Anderson
 */
public class ScheduleDeletionHandler extends AbstractEntityDeletionHandler<Entity> {

    /**
     * The relationship archetypes to exclude when determining if this can be deleted.
     */
    private static final String[] ARCHETYPES = {ScheduleArchetypes.SCHEDULE_APPOINTMENT_TYPE_RELATIONSHIP,
                                                ScheduleArchetypes.SCHEDULE_WORKLIST_RELATIONSHIP,
                                                ScheduleArchetypes.SCHEDULE_TEMPLATE_RELATIONSHIP};

    /**
     * Constructs a {@link ScheduleDeletionHandler}.
     *
     * @param object             the object to delete
     * @param factory            the editor factory
     * @param transactionManager the transaction manager
     * @param service            the archetype service
     */
    public ScheduleDeletionHandler(Entity object, IMObjectEditorFactory factory,
                                   PlatformTransactionManager transactionManager, IArchetypeRuleService service) {
        super(object, ARCHETYPES, factory, transactionManager, service);
    }
}