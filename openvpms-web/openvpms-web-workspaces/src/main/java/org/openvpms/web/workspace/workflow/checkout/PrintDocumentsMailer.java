/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Used by {@link PrintDocumentsTask} to email attachments.
 *
 * @author Tim Anderson
 */
class PrintDocumentsMailer {

    /**
     * The task context.
     */
    private final TaskContext context;

    /**
     * The mail dialog factory.
     */
    private final MailDialogFactory factory;

    /**
     * The reporter factory.
     */
    private final ReporterFactory reporterFactory;

    /**
     * The document job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * Constructs a {@link PrintDocumentsMailer}.
     *
     * @param context the task context
     */
    public PrintDocumentsMailer(TaskContext context) {
        this.context = context;
        factory = ServiceHelper.getBean(MailDialogFactory.class);
        reporterFactory = ServiceHelper.getBean(ReporterFactory.class);
        jobManager = ServiceHelper.getBean(DocumentJobManager.class);
    }

    /**
     * Mails attachments.
     *
     * @param attachments the attachments to mail
     * @param listener    the listener to notify if mail was sent
     */
    public <T extends IMObject> void mail(List<T> attachments, Runnable listener) {
        HelpContext email = context.getHelpContext().subtopic("email");
        MailContext mailContext = new CustomerMailContext(context, email);
        MailDialog dialog = factory.create(mailContext, new DefaultLayoutContext(context, email));
        if (attachments.size() == 1) {
            mailSingleAttachment(dialog, attachments.get(0), listener);
        } else {
            mailMultipleAttachments(dialog, attachments, listener);
        }
    }

    /**
     * Mails a single attachment.
     * <p/>
     * This uses the template associated with the attachment to generate the email content, if any.
     *
     * @param dialog     the mail dialog
     * @param attachment the attachment
     * @param listener   the listener to notify if mail was sent
     */
    private void mailSingleAttachment(MailDialog dialog, IMObject attachment, Runnable listener) {
        MailEditor editor = dialog.getMailEditor();
        addAttachment(editor, attachment, template -> {
            setContent(editor, attachment, template);
            show(dialog, listener);
        });
    }

    /**
     * Mails multiple attachments.
     * <p/>
     * The most recent invoice will be used to populate the email content, if one is present, and there is an invoice
     * email template.
     *
     * @param dialog      the mail dialog
     * @param attachments the attachments
     * @param listener    the listener to notify if mail was sent
     */
    private <T extends IMObject> void mailMultipleAttachments(MailDialog dialog, List<T> attachments, Runnable listener) {
        MostRecentInvoice state = new MostRecentInvoice();
        MailEditor editor = dialog.getMailEditor();
        addAttachments(editor, attachments.iterator(), state, () -> {
            if (state.getInvoice() != null) {
                setContent(editor, state.getInvoice(), state.getTemplate());
            }
            show(dialog, listener);
        });
    }

    /**
     * Shows the mail dialog.
     *
     * @param dialog   the dialog
     * @param listener    the listener to notify if mail was sent
     */
    private static void show(MailDialog dialog, Runnable listener) {
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                if (MailDialog.SEND_ID.equals(dialog.getAction())) {
                    listener.run();
                }
            }
        });
        dialog.show();
    }

    /**
     * Recursively processes attachments, adding them to the supplied editor. This is necessary as attachments
     * are processed asynchronously to support cancellation.
     *
     * @param editor      the mail editor
     * @param attachments the attachments
     * @param state       tracks the most recent invoice amongst the attachments
     * @param listener    the listener to notify when all attachments are processed
     */
    private <T extends IMObject> void addAttachments(MailEditor editor, Iterator<T> attachments, MostRecentInvoice state,
                                Runnable listener) {
        if (attachments.hasNext()) {
            IMObject attachment = attachments.next();
            addAttachment(editor, attachment, template -> {
                state.update(attachment, template);
                addAttachments(editor, attachments, state, listener);
            });
        } else {
            listener.run();
        }
    }

    /**
     * Adds an attachment generated from the supplied object.
     * <p/>
     * The attachment is generated asynchronously. The supplied listener will be notified on completion.
     *
     * @param editor   the mail editor
     * @param object   the object to generate the attachment from
     * @param listener the listener to notify on successful completion, with the template used to generate the
     *                 attachment if any.
     */
    private void addAttachment(MailEditor editor, IMObject object, Consumer<DocumentTemplate> listener) {
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(object, context);
        Reporter<IMObject> reporter = reporterFactory.create(object, locator, Reporter.class);
        reporter.setFields(ReportContextFactory.create(context));
        Job<Document> job = JobBuilder.<Document>newJob("Mail Attachment", context.getUser())
                .get(() -> reporter.getDocument(Reporter.DEFAULT_MIME_TYPE, true))
                .completed(document -> {
                    editor.addAttachment(document);
                    listener.accept(reporter.getTemplate());
                })
                .build();
        jobManager.runInteractive(job, Messages.get("document.generateattachment.title"),
                                  Messages.get("document.generateattachment.cancel"));
    }

    /**
     * Sets the content of the email, generated from the supplied object, if there is an associated email template.
     *
     * @param editor   the editor
     * @param object   the object to generate the content from
     * @param template the document template. May be {@code null}
     */
    private void setContent(MailEditor editor, IMObject object, DocumentTemplate template) {
        if (template != null) {
            EmailTemplate emailTemplate = template.getEmailTemplate();
            if (emailTemplate != null) {
                editor.setObject(object);
                editor.setContent(emailTemplate);
            }
        }
    }

    /**
     * Tracks the most recent invoice in the list of attachments being printed.
     */
    private static class MostRecentInvoice {

        /**
         * The most recent invoice.
         */
        private Act invoice;

        /**
         * The invoice template.
         */
        private DocumentTemplate invoiceTemplate;

        /**
         * Updates the state.
         *
         * @param attachment the attachment
         * @param template   the attachment template. May be {@code null}
         */
        public void update(IMObject attachment, DocumentTemplate template) {
            if (attachment.isA(CustomerAccountArchetypes.INVOICE)) {
                Act act = (Act) attachment;
                if (invoice == null || DateRules.compareTo(invoice.getActivityStartTime(),
                                                           act.getActivityStartTime()) < 0) {
                    invoice = act;
                    invoiceTemplate = template;
                }
            }
        }

        /**
         * Returns the most recent invoice.
         *
         * @return the invoice. May be {@code null}
         */
        public Act getInvoice() {
            return invoice;
        }

        /**
         * Returns the invoice template.
         *
         * @return the invoice template. May be {@code null}
         */
        public DocumentTemplate getTemplate() {
            return invoiceTemplate;
        }
    }
}
