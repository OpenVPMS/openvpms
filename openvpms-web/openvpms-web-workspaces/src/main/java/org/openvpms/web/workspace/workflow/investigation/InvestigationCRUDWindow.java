/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.charge.OrderServices;
import org.openvpms.web.workspace.patient.history.PatientHistoryActions;
import org.openvpms.web.workspace.patient.investigation.ResultChecker;

/**
 * CRUD Window for <em>act.patientInvestigation</em> acts.
 *
 * @author Tim Anderson
 */
class InvestigationCRUDWindow extends ResultSetCRUDWindow<Act> {

    /**
     * Results received button identifier.
     */
    private static final String CHECK_RESULTS = "button.checkResults";

    /**
     * Mark reviewed button identifier.
     */
    private static final String MARK_REVIEWED = "button.markReviewed";

    /**
     * Unmark reviewed button identifier.
     */
    private static final String UNMARK_REVIEWED = "button.unmarkReviewed";

    /**
     * Confirm order button identifier.
     */
    private static final String CONFIRM_ORDER = "button.confirmOrder";

    /**
     * Cancel investigation button identifier.
     */
    private static final String CANCEL_INVESTIGATION = "button.cancelInvestigation";

    /**
     * Constructs an {@link InvestigationCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param query      the query. May be {@code null}
     * @param set        the result set. May be {@code null}
     * @param context    the context
     * @param help       the help context
     */
    public InvestigationCRUDWindow(Archetypes<Act> archetypes, Query<Act> query, ResultSet<Act> set, Context context,
                                   HelpContext help) {
        super(archetypes, InvestigationActions.INSTANCE, query, set, context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(createViewButton());
        buttons.add(createEditButton());
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
        buttons.add(CONFIRM_ORDER, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                          this::confirmOrder, "investigation.order.confirm.title"));
        buttons.add(CHECK_RESULTS, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                          this::checkResults, "investigation.check.title"));
        buttons.add(MARK_REVIEWED, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                          this::markReviewed, "investigation.reviewed.title"));
        buttons.add(UNMARK_REVIEWED, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                            this::unmarkReviewed, "investigation.unreviewed.title"));
        buttons.add(CANCEL_INVESTIGATION, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                                 this::cancelInvestigation, "investigation.cancel.title"));
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        Act object = getObject();
        buttons.setEnabled(PRINT_ID, enable);
        buttons.setEnabled(MAIL_ID, enable);
        buttons.setEnabled(CONFIRM_ORDER, enable && InvestigationActions.INSTANCE.canConfirmOrder(object));
        buttons.setEnabled(CHECK_RESULTS, enable && InvestigationActions.INSTANCE.canCheck(object));
        buttons.setEnabled(MARK_REVIEWED, enable && InvestigationActions.INSTANCE.canReview(object));
        buttons.setEnabled(UNMARK_REVIEWED, enable && InvestigationActions.INSTANCE.canUnreview(object));
        buttons.setEnabled(CANCEL_INVESTIGATION, enable && InvestigationActions.INSTANCE.canCancel(object));
    }

    /**
     * Confirms an investigation with CONFIRM status.
     *
     * @param investigation the investigation.
     */
    private void confirmOrder(Act investigation) {
        OrderConfirmationManager manager = new OrderConfirmationManager();
        manager.confirm(investigation, getHelpContext(), () -> onRefresh(investigation));
    }

    /**
     * Checks if an investigation has results.
     */
    private void checkResults(Act investigation) {
        CheckResultsCommand command = new CheckResultsCommand(investigation);
        command.run();
        onRefresh(investigation);
    }

    /**
     * Marks an investigation as reviewed.
     *
     * @param investigation the investigation
     */
    private void markReviewed(Act investigation) {
        if (InvestigationActions.INSTANCE.canReview(investigation)) {
            investigation.setStatus2(InvestigationActStatus.REVIEWED);
            SaveHelper.save(investigation);
        }
        onRefresh(investigation);
    }

    /**
     * Unmarks an investigation as reviewed.
     *
     * @param investigation the investigation
     */
    private void unmarkReviewed(Act investigation) {
        if (InvestigationActions.INSTANCE.canUnreview(investigation)) {
            // TODO - the investigation could previously have had PARTIAL_RESULTS status
            investigation.setStatus2(InvestigationActStatus.RECEIVED);
            SaveHelper.save(investigation);
        }
        onRefresh(investigation);
    }

    /**
     * Cancels an investigation.
     *
     * @param investigation the investigation to cancel
     */
    private void cancelInvestigation(Act investigation) {
        if (InvestigationActions.INSTANCE.canCancel(investigation)) {
            Context context = getContext();
            User user = context.getUser();
            Party practice = context.getPractice();
            OrderServices orderServices = ServiceHelper.getBean(OrderServices.class);
            IArchetypeService service = ServiceHelper.getArchetypeService();
            PatientRules patientRules = ServiceHelper.getBean(PatientRules.class);
            InvestigationCanceller canceller = new InvestigationCanceller(investigation, user, practice, orderServices,
                                                                          patientRules, service);
            String message;
            if (canceller.isCharged()) {
                message = Messages.get("investigation.cancel.charged");
            } else {
                message = Messages.get("investigation.cancel.message");
            }
            ConfirmationDialog.show(Messages.get("investigation.cancel.title"),
                                    message, ConfirmationDialog.YES_NO,
                                    new PopupDialogListener() {
                                        @Override
                                        public void onYes() {
                                            canceller.cancel();
                                            onRefresh(investigation);
                                        }
                                    });
        } else {
            onRefresh(investigation);
        }
    }

    private static class InvestigationActions extends PatientHistoryActions {

        public static final InvestigationActions INSTANCE = new InvestigationActions();

        /**
         * Determines if objects can be created.
         *
         * @return {@code false}
         */
        @Override
        public boolean canCreate() {
            return false;
        }

        /**
         * Determines if an object can be deleted.
         *
         * @param object the object to check
         * @return {@code true} if the object can be deleted
         */
        @Override
        public boolean canDelete(Act object) {
            return false;
        }

        /**
         * Determines if an investigation order can be confirmed.
         *
         * @param object the object to check
         * @return {@code true} if the investigation order can be confirmed
         */
        public boolean canConfirmOrder(Act object) {
            return ActStatus.IN_PROGRESS.equals(object.getStatus())
                   && (InvestigationActStatus.CONFIRM.equals(object.getStatus2())
                       || (InvestigationActStatus.CONFIRM_DEFERRED.equals(object.getStatus2())));

        }

        /**
         * Determines if an investigation can be checked for updates.
         *
         * @param object the investigation
         * @return {@code true} if the investigation can be flagged as reviewed
         */
        boolean canCheck(Act object) {
            return new CheckResultsCommand(object).canRun();
        }

        /**
         * Determines if an investigation can cancelled.
         *
         * @param object the investigation
         * @return {@code true} if the investigation can be cancelled
         */
        boolean canCancel(Act object) {
            return !WorkflowStatus.CANCELLED.equals(object.getStatus())
                   && !(InvestigationActStatus.RECEIVED.equals(object.getStatus2())
                        || InvestigationActStatus.REVIEWED.equals(object.getStatus2()));
        }
    }

    private static class CheckResultsCommand {

        private final ResultChecker checker;

        public CheckResultsCommand(Act investigation) {
            checker = new ResultChecker((DocumentAct) investigation);
        }

        public boolean canRun() {
            return checker.canCheckResults();
        }

        public void run() {
            if (canRun()) {
                checker.checkResults();
            }
        }
    }
}
