/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.insurance.claim.Attachment;
import org.openvpms.report.DocFormats;
import org.openvpms.report.DocumentConverter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.report.TemplatedReporter;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.job.JobManager;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.history.PatientHistoryFilter;
import org.openvpms.web.workspace.patient.history.PatientHistoryIterator;
import org.openvpms.web.workspace.patient.history.PatientHistoryQuery;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;

import static org.openvpms.component.business.service.archetype.helper.DescriptorHelper.getDisplayName;
import static org.openvpms.component.system.common.query.Constraints.gt;

/**
 * Generates attachments for a claim.
 *
 * @author Tim Anderson
 */
class ClaimAttachmentGenerator {

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * The charges being claimed.
     */
    private final Charges charges;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The reporter factory.
     */
    private final ReporterFactory factory;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The reporter factory.
     */
    private final ReporterFactory reporterFactory;

    /**
     * The document converter.
     */
    private final DocumentConverter converter;

    /**
     * The document job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    private final DocumentHandlers documentHandlers;

    /**
     * The document template type for insurnace claim invoices.
     */
    static final String INSURANCE_CLAIM_INVOICE = "INSURANCE_CLAIM_INVOICE";

    /**
     * The document template type for insurance claim medical records.
     */
    static final String INSURANCE_CLAIM_MEDICAL_RECORDS = "INSURANCE_CLAIM_MEDICAL_RECORDS";


    /**
     * Constructs a {@link ClaimAttachmentGenerator}.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param charges  the charges
     * @param context  the context
     */
    public ClaimAttachmentGenerator(Party customer, Party patient, Charges charges, Context context) {
        this(customer, patient, charges, context, ServiceHelper.getBean(DocumentConverter.class));
    }

    /**
     * Constructs a {@link ClaimAttachmentGenerator}.
     *
     * @param customer  the customer
     * @param patient   the patient
     * @param charges   the charges
     * @param context   the context
     * @param converter the document converter
     */
    public ClaimAttachmentGenerator(Party customer, Party patient, Charges charges, Context context,
                                    DocumentConverter converter) {
        this.patient = patient;
        this.charges = charges;
        LocalContext local = new LocalContext(context);
        local.setCustomer(customer);
        local.setPatient(patient);
        this.context = local;
        insuranceRules = ServiceHelper.getBean(InsuranceRules.class);
        factory = ServiceHelper.getBean(ReporterFactory.class);
        service = ServiceHelper.getArchetypeService();
        reporterFactory = ServiceHelper.getBean(ReporterFactory.class);
        this.converter = converter;
        jobManager = ServiceHelper.getBean(DocumentJobManager.class);
        transactionManager = ServiceHelper.getBean(PlatformTransactionManager.class);
        documentHandlers = ServiceHelper.getBean(DocumentHandlers.class);
    }

    /**
     * Generates attachments.
     * <p/>
     * This is done asynchronously, as document generation and conversion are run as
     * {@link JobManager#runInteractive(Job, String, String) jobs} to support cancellation.
     * <p/>
     * The listener will be invoked on completion with {@code true} to indicate success, or {@code false} to indicate
     * failure.
     *
     * @param claim       the claim
     * @param insurer the insurer. May be {@code null}
     * @param attachments the attachments editor
     * @param location    the location to use
     * @param listener    the listener to notify if attachments were successfully generated or not
     */
    public void generate(Act claim, Party insurer, AttachmentCollectionEditor attachments, Party location,
                         Consumer<Boolean> listener) {
        context.setLocation(location);
        updateHistory(attachments);
        updateInvoices(attachments);

        Iterator<Act> iterator = attachments.getCurrentActs().iterator();
        generate(iterator, claim, insurer, attachments, success -> {
            attachments.save();
            attachments.refresh();
            listener.accept(success);
        });
    }

    /**
     * Runs a job.
     *
     * @param job        the job
     * @param titleKey   the title resource bundle key
     * @param messageKey the message resource bundle key
     */
    protected void rubJob(Job<Document> job, String titleKey, String messageKey) {
        jobManager.runInteractive(job, Messages.get(titleKey), Messages.get(messageKey));
    }

    /**
     * Generates the next attachment returned by an iterator, if any.
     *
     * @param iterator    the attachment iterator.
     * @param claim       the claim
     * @param insurer the insurer. May be {@code null}
     * @param attachments the attachments collection
     * @param listener    the listener to notify on success or failure
     */
    private void generate(Iterator<Act> iterator, Act claim, Party insurer, AttachmentCollectionEditor attachments,
                          Consumer<Boolean> listener) {
        if (iterator.hasNext()) {
            Act attachment = iterator.next();
            generate(attachment, claim, insurer, success -> {
                if (success != null && success) {
                    attachments.refresh((DocumentAct) attachment);
                    generate(iterator, claim, insurer, attachments, listener);
                } else {
                    listener.accept(false);
                }
            });
        } else {
            listener.accept(true);
        }
    }

    /**
     * Updates the patient history.
     * <p/>
     * Any existing history attachment is regenerated, to ensure it contains the latest history.
     *
     * @param attachments the attachments editor
     */
    private void updateHistory(AttachmentCollectionEditor attachments) {
        Act history = attachments.getHistory();
        if (history != null) {
            attachments.remove(history);
        }
        attachments.add(attachments.createHistory());
    }

    /**
     * Updates existing invoice attachments, and removes those that are no longer relevant.
     * <p/>
     * Existing attachments are regenerated to ensure that they reflect the current claimed items and customer balance.
     *
     * @param attachments the attachments editor
     */
    private void updateInvoices(AttachmentCollectionEditor attachments) {
        Set<Reference> expectedInvoices = charges.getInvoiceRefs();
        for (Act attachment : attachments.getCurrentActs()) {
            if (isInvoice(attachment)) {
                // for invoices that are in the final claim, it is easiest to just remove the attachment and
                // recreate, as it handles document deletion
                attachments.remove(attachment);
            }
        }
        for (Reference ref : expectedInvoices) {
            FinancialAct invoice = (FinancialAct) IMObjectHelper.getObject(ref);
            if (invoice != null) {
                attachments.addInvoice(invoice);
            }
        }
    }

    /**
     * Generates an attachment.
     *
     * @param attachment the attachment
     * @param claim      the claim
     * @param insurer the insurer. May be {@code null}
     * @param listener   the listener to notify if the attachment was successfully generated
     */
    private void generate(Act attachment, Act claim, Party insurer, Consumer<Boolean> listener) {
        IMObjectBean bean = service.getBean(attachment);
        String type = bean.getString("type");
        if (PatientArchetypes.CLINICAL_EVENT.equals(type)) {
            generateHistory(claim, insurer, bean, listener);
        } else if (bean.getReference("document") == null) {
            Act original = bean.getTarget("original", Act.class);
            if (TypeHelper.isA(original, CustomerAccountArchetypes.INVOICE)) {
                generateInvoice(bean, original, claim, listener);
            } else if (TypeHelper.isA(original, InvestigationArchetypes.PATIENT_INVESTIGATION)) {
                generateInvestigation(bean, (DocumentAct) original, listener);
            } else if (original instanceof DocumentAct) {
                generateDocument(bean, original, listener);
            } else {
                noDocument(bean, original, listener);
            }
        } else {
            listener.accept(true);
        }
    }

    /**
     * Generates an investigation, storing it as an attachment.
     *
     * @param bean          the attachment
     * @param investigation the investigation
     * @param listener      the listener to notify on completion or failure
     */
    private void generateInvestigation(IMObjectBean bean, DocumentAct investigation, Consumer<Boolean> listener) {
        convertOrCopy(investigation.getDocument(), bean, investigation, listener);
    }

    /**
     * Generates a document, storing it as an attachment.
     *
     * @param bean     the attachment
     * @param original the original document
     * @param listener the listener to notify on completion or failure
     */
    private void generateDocument(IMObjectBean bean, Act original, Consumer<Boolean> listener) {
        IMObjectBean source = service.getBean(original);
        Reference docRef = source.hasNode("document") ? source.getReference("document") : null;
        if (docRef == null) {
            if (original.isA(CustomerArchetypes.DOCUMENT_ATTACHMENT, PatientArchetypes.DOCUMENT_ATTACHMENT)) {
                noDocument(bean, original, listener);
            } else {
                ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(original, context);
                Reporter<Act> reporter = factory.create(original, locator, TemplatedReporter.class);
                save(bean, reporter, listener);
            }
        } else {
            convertOrCopy(docRef, bean, original, listener);
        }
    }

    /**
     * Converts or copies a document.
     *
     * @param docRef   the document reference. May be {@code null}
     * @param bean     the attachment bean
     * @param original the original record. May be {@code null}
     * @param listener the listener to notify on completion or failure
     */
    private void convertOrCopy(Reference docRef, IMObjectBean bean, Act original, Consumer<Boolean> listener) {
        Document document = (docRef != null) ? service.get(docRef, Document.class) : null;
        if (document == null) {
            noDocument(bean, original, listener);
        } else {
            String mimeType = document.getMimeType();
            if (!StringUtils.isEmpty(mimeType) && !DocFormats.PDF_TYPE.equals(mimeType)) {
                if (converter.canConvert(document, DocFormats.PDF_TYPE)) {
                    convertToPDF(bean, document, listener);
                } else {
                    copy(bean, document, listener);
                }
            } else {
                copy(bean, document, listener);
            }
        }
    }

    /**
     * Invoked when an attachment for a claim has no document.
     *
     * @param bean     the attachment bean
     * @param original the original attachment. May be {@code null}
     * @param listener the listener not notify
     */
    private void noDocument(IMObjectBean bean, Act original, Consumer<Boolean> listener) {
        String message = (original != null)
                         ? Messages.format("patient.insurance.nodocument", getDisplayName(original, service))
                         : Messages.get("patient.insurance.attachmentdeleted");
        setStatus(bean, Attachment.Status.ERROR, message);
        listener.accept(false);
    }

    /**
     * Converts a document to PDF, saving it as an attachment.
     * <p/>
     * This is run as a job, to support cancellation.
     *
     * @param bean     the attachment
     * @param document the document to convert
     * @param listener the listener to notify on completion or failure
     */
    private void convertToPDF(IMObjectBean bean, Document document, Consumer<Boolean> listener) {
        Job<Document> job = JobBuilder.<Document>newJob(document.getName(), context.getUser())
                .get(() -> converter.convert(document, DocFormats.PDF_TYPE, true))
                .completed(converted -> save(bean, converted, listener))
                .cancelled(() -> listener.accept(false))
                .failed(exception -> {
                    setStatus(bean, Attachment.Status.ERROR, exception.getMessage());
                    listener.accept(false);
                })
                .build();
        rubJob(job, "document.convert.title", "document.convert.cancel");
    }

    /**
     * Determines if an attachment is an invoice attachment.
     *
     * @param attachment the attachment
     * @return {@code true} if the attachment is a invoice attachment
     */
    private boolean isInvoice(Act attachment) {
        return CustomerAccountArchetypes.INVOICE.equals(service.getBean(attachment).getString("type"));
    }

    /**
     * Generates an invoice, storing it in an attachment.
     *
     * @param bean     the attachment
     * @param original the invoice
     * @param claim    the claim
     * @param listener the listener to notify on completion or failure
     */
    private void generateInvoice(IMObjectBean bean, Act original, Act claim, Consumer<Boolean> listener) {
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(INSURANCE_CLAIM_INVOICE, context);
        Reporter<Act> reporter = reporterFactory.create(original, locator, TemplatedReporter.class);
        reporter.getParameters().put("claim", claim);
        save(bean, reporter, listener);
    }

    /**
     * Generates patient history, storing it in an attachment.
     * <br/>
     * If the insurer is linked to an insurance service, and a claim for the patient has been previously submitted,
     * this excludes previously submitted history.
     *
     * @param claim the claim
     * @param insurer the insurer. May be {@code null}
     * @param bean     the attachment
     * @param listener the listener to notify on completion or failure
     */
    private void generateHistory(Act claim, Party insurer, IMObjectBean bean, Consumer<Boolean> listener) {
        Date from = null;
        if (insurer != null) {
            from = insuranceRules.getMostRecentTreatmentDateForService(patient, insurer, claim);
        }
        PatientHistoryQuery query = new ClaimHistoryQuery(patient, from);
        PatientHistoryFilter filter = new PatientHistoryFilter(query.getSelectedArchetypes(), service);
        PatientHistoryIterator summary = new PatientHistoryIterator(query, filter, 3);
        DocumentTemplateLocator locator = new ContextDocumentTemplateLocator(INSURANCE_CLAIM_MEDICAL_RECORDS,
                                                                             context);
        Reporter<Act> reporter = factory.create(summary, locator, TemplatedReporter.class);
        save(bean, reporter, listener);
    }

    /**
     * Generates a document, storing it in an attachment.
     * <p/>
     * This is run as a job, to support cancellation.
     *
     * @param bean     the attachment
     * @param reporter the reporter used to generate the document
     * @param listener the listener to notify on completion or failure
     */
    private void save(IMObjectBean bean, Reporter<Act> reporter, Consumer<Boolean> listener) {
        reporter.setFields(ReportContextFactory.create(context));
        Job<Document> job = JobBuilder.<Document>newJob("Claim Attachment Generator", context.getUser())
                .get(() -> reporter.getDocument(DocFormats.PDF_TYPE, true)) // ensure letterhead is included
                .completed(document -> save(bean, document, listener))
                .cancelled(() -> listener.accept(false))
                .failed(exception -> {
                    setStatus(bean, Attachment.Status.ERROR, exception.getMessage());
                    listener.accept(false);
                })
                .build();
        rubJob(job, "document.generateattachment.title", "document.generateattachment.cancel");
    }

    /**
     * Copies a document to an attachment.
     *
     * @param bean     the attachment
     * @param document the document to copy
     * @param listener the listener to notify on completion or failure
     */
    private void copy(IMObjectBean bean, Document document, Consumer<Boolean> listener) {
        DocumentHandler handler = documentHandlers.get(document);
        InputStream content = handler.getContent(document);
        Document copy = handler.create(document.getName(), content, document.getMimeType(), document.getSize());
        save(bean, copy, listener);
    }

    /**
     * Saves an attachment.
     *
     * @param bean     the attachment
     * @param document the document
     * @param listener the listener to notify on completion or failure
     */
    private void save(IMObjectBean bean, Document document, Consumer<Boolean> listener) {
        boolean result = false;
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        try {
            template.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    // need to remove any existing document
                    Document existing = bean.getObject("document", Document.class);
                    if (existing != null) {
                        bean.setValue("document", null);
                        bean.save();
                        service.remove(existing);
                    }
                    bean.setValue("status", Attachment.Status.PENDING.name());
                    bean.setValue("error", null);
                    bean.setValue("document", document.getObjectReference());
                    bean.setValue("fileName", document.getName());
                    bean.setValue("mimeType", document.getMimeType());
                    bean.save(document);
                }
            });
            result = true;
        } catch (Throwable exception) {
            setStatus(bean, Attachment.Status.ERROR, exception.getMessage());
        }
        listener.accept(result);
    }

    /**
     * Sets the status and message of an attachment.
     *
     * @param bean    the attachment
     * @param status  the status
     * @param message the message. May be {@code null}
     */
    private void setStatus(IMObjectBean bean, Attachment.Status status, String message) {
        bean.setValue("status", status.name());
        if (message != null) {
            message = StringUtils.abbreviate(message, NodeDescriptor.DEFAULT_MAX_LENGTH);
        }
        bean.setValue("error", message);
        bean.save();
    }

    /**
     * History query for claims that supports excluding previously submitted visits.
     */
    private static class ClaimHistoryQuery extends PatientHistoryQuery {

        /**
         * If non-null, exclude all visits on or prior to the date/time as these have been submitted previously.
         * Note that the date range can't be used, as it is limited to dates, not date/times.
         */
        private final Date excludeOnOrPrior;

        /**
         * Constructs a {@link ClaimHistoryQuery}.
         * <p/>
         * This excludes communications.
         *
         * @param patient the patient to query
         * @param excludeOnOrPrior if non-null, exclude all visits on or prior to the date/time as these have been
         *                         submitted previously
         */
        public ClaimHistoryQuery(Party patient, Date excludeOnOrPrior) {
            super(patient, true);
            this.excludeOnOrPrior = excludeOnOrPrior;
            if (excludeOnOrPrior != null) {
                setAllDates(false);
                setFrom(excludeOnOrPrior);
            }
        }

        /**
         * Creates a new result set.
         *
         * @param sort the sort constraint. May be {@code null}
         * @return a new result set
         */
        @Override
        protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
            IConstraint times = null;
            if (excludeOnOrPrior != null) {
                // exclude visits on or prior to from
                times = gt("startTime", excludeOnOrPrior);
            }
            ParticipantConstraint[] participantConstraint = {getParticipantConstraint()};
            return new ActResultSet<>(getArchetypeConstraint(), participantConstraint, times,
                                      getStatuses(), excludeStatuses(), getConstraints(), getMaxResults(), sort);
        }
    }
}
