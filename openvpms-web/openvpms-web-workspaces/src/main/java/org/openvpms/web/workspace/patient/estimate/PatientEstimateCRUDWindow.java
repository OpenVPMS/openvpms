/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.estimate;

import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.estimate.EstimateCRUDWindow;

/**
 * Patient estimate CRUD window.
 *
 * @author Tim Anderson
 */
public class PatientEstimateCRUDWindow extends EstimateCRUDWindow {

    /**
     * Constructs a {@link PatientEstimateCRUDWindow}.
     *
     * @param context the context
     * @param help    the help context
     */
    public PatientEstimateCRUDWindow(Context context, HelpContext help) {
        super(Archetypes.create(EstimateArchetypes.ESTIMATE, Act.class), context, help);
    }

    /**
     * Creates and edits a new object.
     */
    @Override
    public void create() {
        if (getContext().getCustomer() == null) {
            ErrorHelper.show(Messages.get("patient.estimate.nocustomer"));
        } else {
            super.create();
        }
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit.
     * @param context the layout context
     * @return a new editor
     */
    @Override
    protected IMObjectEditor createEditor(Act object, LayoutContext context) {
        return new PatientEstimateEditor(object, null, context);
    }
}