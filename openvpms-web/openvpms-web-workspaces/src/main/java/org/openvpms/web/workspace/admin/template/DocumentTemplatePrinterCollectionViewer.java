/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.IMObjectRelationshipCollectionViewer;
import org.openvpms.web.component.im.relationship.RelationshipDescriptorTableModel;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.print.PrinterViewer;
import org.openvpms.web.component.property.CollectionProperty;

/**
 * Viewer for collections of <em>entityRelationship.documentTemplatePrinter</em>.
 *
 * @author Tim Anderson
 */
public class DocumentTemplatePrinterCollectionViewer extends IMObjectRelationshipCollectionViewer {

    /**
     * Constructs a {@link DocumentTemplatePrinterCollectionViewer}.
     *
     * @param property the collection to view
     * @param parent   the parent object
     * @param layout   the layout context. May be {@code null}
     */
    public DocumentTemplatePrinterCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext layout) {
        super(property, parent, layout);
    }

    /**
     * Browse an object.
     *
     * @param object the object to browse.
     */
    @Override
    protected void browse(IMObject object) {
        if (getObject().isA(PracticeArchetypes.PRACTICE)) {
            super.browse(object);
        }
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        boolean displayTarget = getObject().isA(DocumentArchetypes.DOCUMENT_TEMPLATE);
        DocumentTemplatePrinterTableModel relationship = new DocumentTemplatePrinterTableModel(
                getProperty().getArchetypeRange(), context, displayTarget);
        return (IMTableModel) relationship;
    }

    private static class DocumentTemplatePrinterTableModel
            extends RelationshipDescriptorTableModel<EntityRelationship> {

        /**
         * Constructs a {@link DocumentTemplatePrinterTableModel}.
         * <p/>
         * Enables selection if the context is in edit mode.
         *
         * @param shortNames    the archetype short names
         * @param context       the layout context
         * @param displayTarget if {@code true} display the target node, otherwise display the source node
         */
        public DocumentTemplatePrinterTableModel(String[] shortNames, LayoutContext context, boolean displayTarget) {
            super(shortNames, context, displayTarget);
        }

        /**
         * Returns a list of node descriptor names to include in the table.
         *
         * @return the list of node descriptor names to include in the table
         */
        @Override
        protected String[] getNodeNames() {
            return new String[]{"printer", "paperTray", "sides", "interactive"};
        }

        /**
         * Returns a value for a given column.
         *
         * @param object the object to operate on
         * @param column the column
         * @param row    the row
         * @return the value for the column
         */
        @Override
        protected Object getValue(EntityRelationship object, DescriptorTableColumn column, int row) {
            if (column.getName().equals("printer")) {
                Object value = column.getValue(object);
                return value != null ? new PrinterViewer(value.toString(), getLayoutContext()).getComponent() : null;
            }
            return super.getValue(object, column, row);
        }
    }
}