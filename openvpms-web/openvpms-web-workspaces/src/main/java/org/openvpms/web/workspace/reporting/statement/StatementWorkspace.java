/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.workspace.BrowserCRUDWindowTab;
import org.openvpms.web.component.workspace.TabComponent;
import org.openvpms.web.component.workspace.TabbedWorkspace;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.tabpane.ObjectTabPaneModel;
import org.openvpms.web.workspace.reporting.statement.reminder.AccountReminderBrowser;
import org.openvpms.web.workspace.reporting.statement.reminder.AccountReminderCRUDWindow;
import org.openvpms.web.workspace.reporting.statement.reminder.AccountReminderObjectSetQuery;
import org.openvpms.web.workspace.reporting.statement.reminder.AccountReminderQuery;


/**
 * Statement workspace.
 *
 * @author Tim Anderson
 */
public class StatementWorkspace extends TabbedWorkspace<Act> {

    /**
     * Constructs a {@link StatementWorkspace}.
     *
     * @param context the context
     */
    public StatementWorkspace(Context context) {
        super("reporting.statement", context);
    }

    /**
     * Returns the class type that this operates on.
     *
     * @return the class type that this operates on
     */
    @Override
    protected Class<Act> getType() {
        return Act.class;
    }

    /**
     * Adds tabs to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    @Override
    protected void addTabs(ObjectTabPaneModel<TabComponent> model) {
        addAccountTab(model);
    }

    /**
     * Adds an account tab to the tab pane.
     *
     * @param model the tab pane model
     */
    private void addAccountTab(ObjectTabPaneModel<TabComponent> model) {
        Context context = getContext();
        addTab("reporting.statements.account", model, new AccountTab(context, getHelpContext()));

        addReminderTab(model, context);
    }

    /**
     * Adds a reminder tab to the tab pane.
     *
     * @param model the tab pane model
     */
    private void addReminderTab(ObjectTabPaneModel<TabComponent> model, Context context) {
        HelpContext help = subtopic("reminder");
        AccountReminderQuery query = new AccountReminderQuery(new AccountReminderObjectSetQuery(context));
        AccountReminderBrowser browser = new AccountReminderBrowser(query, new DefaultLayoutContext(context, help));
        AccountReminderCRUDWindow window = new AccountReminderCRUDWindow(context, help);
        addTab("reporting.statements.reminder", model, new BrowserCRUDWindowTab<>(browser, window, true, false));
    }
}
