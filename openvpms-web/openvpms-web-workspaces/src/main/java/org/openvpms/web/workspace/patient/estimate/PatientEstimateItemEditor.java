/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.estimate;

import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.product.FixedPriceEditor;
import org.openvpms.web.component.im.product.ProductParticipationEditor;
import org.openvpms.web.workspace.customer.charge.ChargeEditContext;
import org.openvpms.web.workspace.customer.estimate.EstimateItemEditor;

/**
 * An {@link EstimateItemEditor} for patients.
 *
 * @author Tim Anderson
 */
public class PatientEstimateItemEditor extends EstimateItemEditor {

    /**
     * Constructs an {@link PatientEstimateItemEditor}.
     *
     * @param act           the act to edit
     * @param parent        the parent act
     * @param context       the edit context
     * @param layoutContext the layout context
     */
    public PatientEstimateItemEditor(Act act, Act parent, ChargeEditContext context, LayoutContext layoutContext) {
        super(act, parent, context, layoutContext);
        if (act.isNew()) {
            initParticipant("patient", layoutContext.getContext().getPatient());
        }
    }

    /**
     * Creates the layout strategy.
     *
     * @param fixedPrice         the fixed price editor
     * @param serviceRatioEditor the service ration editor
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy(FixedPriceEditor fixedPrice, ServiceRatioEditor serviceRatioEditor) {
        return new PatientEstimateItemLayoutStrategy(fixedPrice, serviceRatioEditor);
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        // the patient node is hidden, so need to update the product with the current patient to restrict
        // product searches by species
        ProductParticipationEditor product = getProductEditor();
        if (product != null) {
            product.setPatient(getPatient());
        }
    }

    /**
     * A layout strategy that filters the patient node.
     */
    private class PatientEstimateItemLayoutStrategy extends EstimateItemLayoutStrategy {

        public PatientEstimateItemLayoutStrategy(FixedPriceEditor fixedPrice, ServiceRatioEditor serviceRatioEditor) {
            super(fixedPrice, serviceRatioEditor);
            getArchetypeNodes().exclude(PATIENT);
        }
    }

}
