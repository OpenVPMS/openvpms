/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.firewall;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.property.AbstractEditor;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Editor for firewall allowed addresses.
 *
 * @author Tim Anderson
 */
class AllowedAddressCollectionEditor extends AbstractEditor {

    /**
     * The firewall settings.
     */
    private final FirewallSettings settings;

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * The editors.
     */
    private final Editors editors;

    /**
     * The allowed addresses.
     */
    private final List<FirewallEntryEditor> addresses = new ArrayList<>();

    /**
     * The collection editor component.
     */
    private final Component component;

    /**
     * The table to display allowed addresses.
     */
    private final PagedIMTable<FirewallEntryEditor> table;

    /**
     * The container for the current editor.
     */
    private final Component editorContainer = ColumnFactory.create();

    /**
     * The buttons.
     */
    private final ButtonRow buttons;

    /**
     * Listener for current editor changes.
     */
    private final ModifiableListener currentListener;

    /**
     * The current editor.
     */
    private FirewallEntryEditor current;

    /**
     * The add button identifier.
     */
    private static final String ADD_ID = "button.add";

    /**
     * The delete button identifier.
     */
    private static final String DELETE_ID = "button.delete";

    /**
     * Constructs an {@link AllowedAddressCollectionEditor}.
     *
     * @param settings      the firewall settings to populate
     * @param layoutContext the layout context
     */
    public AllowedAddressCollectionEditor(FirewallSettings settings, LayoutContext layoutContext) {
        this.settings = settings;
        this.layoutContext = layoutContext;
        editors = new Editors(getListeners());
        currentListener = modifiable -> changed();
        for (FirewallEntry entry : settings.getAllowedAddresses()) {
            FirewallEntryEditor addressEditor = new FirewallEntryEditor(entry, layoutContext);
            addresses.add(addressEditor);
            editors.add(addressEditor);
        }
        FocusGroup group = getFocusGroup();
        buttons = new ButtonRow(group);
        buttons.addButton(ADD_ID, this::onAdd);
        buttons.addButton(DELETE_ID, this::onDelete);
        table = new PagedIMTable<>(new AddressTableModel());
        table.getTable().addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onSelected();
            }
        });
        component = ColumnFactory.create(Styles.CELL_SPACING, buttons, table.getComponent(), editorContainer);
        refreshTable();
        enableDelete();
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        return component;
    }

    /**
     * Determines if the object has been modified.
     *
     * @return {@code true} if the object has been modified
     */
    @Override
    public boolean isModified() {
        return super.isModified() || editors.isModified() || (current == null || current.isModified());
    }

    /**
     * Clears the modified status of the object.
     */
    @Override
    public void clearModified() {
        super.clearModified();
        editors.clearModified();
        if (current != null) {
            current.clearModified();
        }
    }

    /**
     * Determines if the object is valid.
     *
     * @return {@code true} if the object is valid; otherwise {@code false}
     */
    @Override
    public boolean isValid() {
        return super.isValid() && editors.isValid()
               && (current == null || current.isEmpty() || current.isValid());
    }

    /**
     * Returns the active addresses.
     *
     * @return the active addresses
     */
    public List<String> getActiveAddresses() {
        return addresses.stream()
                .filter(FirewallEntryEditor::getActive)
                .map(FirewallEntryEditor::getAddress)
                .collect(Collectors.toList());
    }

    /**
     * Resets the cached validity state of the object.
     *
     * @param descendants if {@code true} reset the validity state of any descendants as well.
     */
    @Override
    protected void resetValid(boolean descendants) {
        super.resetValid(descendants);
        if (descendants) {
            editors.resetValid();
            if (current != null) {
                current.resetValid();
            }
        }
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean result = true;
        if (current != null) {
            result = addCurrentEdits(validator);
        }
        if (result) {
            result = editors.validate(validator);
        }
        if (result) {
            result = populateAllowedAddressesSettings(validator);
        }
        return result;
    }

    /**
     * Refreshes the allowed addresses table.
     */
    private void refreshTable() {
        ListResultSet<FirewallEntryEditor> set = new ListResultSet<>(addresses, 10);
        table.setResultSet(set);
    }

    /**
     * Enables the delete button if there is a current editor, or an address is selected in the table, else disables it.
     */
    private void enableDelete() {
        boolean enableDelete = current != null || table.getSelected() != null;
        buttons.getButtons().setEnabled(DELETE_ID, enableDelete);
    }

    /**
     * Populates the settings with allowed addresses.
     *
     * @param validator the validator
     * @return {@code true} if the settings could be populated, otherwise {@code false}
     */
    private boolean populateAllowedAddressesSettings(Validator validator) {
        boolean result;
        List<FirewallEntry> entries = addresses.stream()
                .map(FirewallEntryEditor::getEntry)
                .collect(Collectors.toList());
        try {
            settings.setAllowedAddresses(entries);
            result = true;
        } catch (IllegalArgumentException exception) {
            // too many addresses to encode onto the settings
            result = false;
            validator.add(this, Messages.get("admin.system.firewall.toomanyaddresses"));
        }
        return result;
    }

    /**
     * Adds a new allowed address.
     */
    private void onAdd() {
        if (addCurrentEdits(new DefaultValidator())) {
            editorContainer.removeAll();
            setEditor(new FirewallEntryEditor(layoutContext));
            enableDelete();
        }
    }

    /**
     * Deletes the selected allowed address.
     */
    private void onDelete() {
        FirewallEntryEditor editor = current;
        if (editor == null) {
            editor = table.getSelected();
        }
        if (editor != null) {
            editors.remove(editor);
            editor.removeModifiableListener(currentListener);
            getFocusGroup().remove(editor.getFocusGroup());
            if (addresses.remove(editor)) {
                refreshTable();
            }
            current = null;
            editorContainer.removeAll();
            changed();
        }
        enableDelete();
    }

    /**
     * Invoked when an address is selected.
     * <p/>
     * Displays it for editing.
     */
    private void onSelected() {
        FirewallEntryEditor selected = table.getSelected();
        if (addCurrentEdits(new DefaultValidator())) {
            // only edit the selected address if the existing address is valid
            editorContainer.removeAll();
            if (selected != null) {
                setEditor(selected);
            }
            enableDelete();
        }
    }

    /**
     * Sets the current editor.
     *
     * @param editor the editor
     */
    private void setEditor(FirewallEntryEditor editor) {
        FocusGroup focusGroup = getFocusGroup();
        if (current != null) {
            current.removeModifiableListener(currentListener);
            focusGroup.remove(current.getFocusGroup());
        }

        current = editor;
        current.addModifiableListener(currentListener);
        focusGroup.add(editor.getFocusGroup());
        editorContainer.add(current.getComponent());
        current.getFocusGroup().setFocus();
    }

    /**
     * Adds current edits, if any.
     *
     * @param validator the validator
     * @return {@code true} there is no current editor, or the current editor is valid, otherwise {@code false}
     */
    private boolean addCurrentEdits(Validator validator) {
        boolean valid = true;
        if (current != null) {
            if (current.isEmpty()) {
                editors.remove(current);
                addresses.remove(current);
                refreshTable();
                changed();
            } else {
                valid = current.validate(validator);
                if (valid) {
                    valid = validateUnique(validator);
                }
                if (valid) {
                    if (!editors.getEditors().contains(current)) {
                        editors.add(current);
                    }
                    if (!addresses.contains(current)) {
                        addresses.add(current);
                    }
                    if (current.isModified()) {
                        refreshTable();
                        current.clearModified();
                    }
                }
            }
        }
        return valid;
    }

    /**
     * Determines if the current address is unique.
     *
     * @param validator the validator
     * @return {@code true} if the current address is unique, otherwise {@code false}
     */
    private boolean validateUnique(Validator validator) {
        boolean valid = true;
        String address = current.getAddress();
        for (FirewallEntryEditor existing : addresses) {
            if (existing != current && address.equals(existing.getAddress())) {
                valid = false;
                validator.add(current, Messages.format("admin.system.firewall.duplicate", address));
                break;
            }
        }
        return valid;
    }

    private static class AddressTableModel extends AbstractIMTableModel<FirewallEntryEditor> {

        private static final int ADDRESS_INDEX = 0;

        private static final int ACTIVE_INDEX = 1;

        private static final int DESCRIPTION_INDEX = 2;

        private static final int RANGE_INDEX = 3;

        public AddressTableModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(ADDRESS_INDEX, "admin.system.firewall.address"));
            model.addColumn(createTableColumn(ACTIVE_INDEX, ACTIVE));
            model.addColumn(createTableColumn(DESCRIPTION_INDEX, DESCRIPTION));
            model.addColumn(createTableColumn(RANGE_INDEX, "admin.system.firewall.range"));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return new SortConstraint[0];
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(FirewallEntryEditor object, TableColumn column, int row) {
            Object result = null;
            switch (column.getModelIndex()) {
                case ADDRESS_INDEX:
                    result = object.getAddress();
                    break;
                case ACTIVE_INDEX:
                    result = getCheckBox(object.getActive());
                    break;
                case DESCRIPTION_INDEX:
                    result = object.getDescription();
                    break;
                case RANGE_INDEX:
                    result = FirewallEntryEditor.getRange(object.getAddress());
                    break;
            }
            return result;
        }
    }

    /**
     * Marks this as changed, and notifies registered listeners.
     */
    private void changed() {
        setModified();
        resetValid();
        notifyListeners();
    }
}