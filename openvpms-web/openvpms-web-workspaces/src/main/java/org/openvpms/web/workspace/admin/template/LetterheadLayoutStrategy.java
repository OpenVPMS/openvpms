/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.organisation.LogoComponentFactory;

import java.util.List;

/**
 * Layout strategy for <em>entity.letterhead</em>.
 *
 * @author Tim Anderson
 */
public class LetterheadLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The logo factory.
     */
    private final LogoComponentFactory logoFactory;

    /**
     * Constructs a {@link LetterheadLayoutStrategy}.
     */
    public LetterheadLayoutStrategy() {
        logoFactory = ServiceHelper.getBean(LogoComponentFactory.class);
    }

    /**
     * Creates a set of components to be rendered from the supplied descriptors.
     *
     * @param object     the parent object
     * @param properties the properties
     * @param context    the layout context
     * @return the components
     */
    @Override
    protected ComponentSet createComponentSet(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentSet set = super.createComponentSet(object, properties, context);
        int index = set.indexOf("logoFile");
        ComponentState logo = getLogo(object, context);
        set.add(index, logo);
        return set;
    }

    /**
     * Returns a component to display the logo.
     *
     * @param object  the letterhead
     * @param context the layout context
     * @return the logo component
     */
    protected ComponentState getLogo(IMObject object, LayoutContext context) {
        return logoFactory.getLogo((Entity) object, context);
    }
}