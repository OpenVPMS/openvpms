/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.settings;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.settings.SettingsCache;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.singleton.SingletonService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.IMObjectViewer;
import org.openvpms.web.component.workspace.TabComponent;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * Settings tab.
 *
 * @author Tim Anderson
 */
class SettingsTab implements TabComponent {

    /**
     * The settings archetype.
     */
    private final String archetype;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The component container.
     */
    private final Column container = ColumnFactory.create();

    /**
     * The tab title.
     */
    private final String title;

    /**
     * Identifiers of buttons associated with the tab.
     */
    private final String[] buttons;

    /**
     * Constructs a {@link SettingsTab}.
     *
     * @param archetype the settings archetype
     * @param help      the help context
     * @param buttons   identifiers of buttons to display
     */
    public SettingsTab(String archetype, HelpContext help, String... buttons) {
        this.archetype = archetype;
        this.help = help.topic(archetype + "/edit");
        this.buttons = buttons;
        title = DescriptorHelper.getDisplayName(archetype, ServiceHelper.getArchetypeService());
    }

    /**
     * Returns the tab title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the help context for the tab.
     *
     * @return the help context
     */
    @Override
    public HelpContext getHelpContext() {
        return help;
    }

    /**
     * Invoked when the tab is displayed.
     */
    @Override
    public void show() {
        SettingsCache cache = ServiceHelper.getBean(SettingsCache.class);
        IMObjectBean group = cache.getGroup(archetype, true);
        IMObject settings = group.getObject();
        HelpContext view = help.topic(settings, "view");
        LayoutContext context = new DefaultLayoutContext(true, new LocalContext(), view);
        IMObjectViewer viewer = new IMObjectViewer(settings, context);
        container.removeAll();
        container.add(viewer.getComponent());
    }

    /**
     * Returns the tab component.
     *
     * @return the tab component
     */
    @Override
    public Component getComponent() {
        return container;
    }

    /**
     * Edits the settings.
     */
    public void edit() {
        SingletonService service = ServiceHelper.getBean(SingletonService.class);
        Entity settings = service.get(archetype, Entity.class, true);
        long version = settings.getVersion();
        HelpContext edit = help.topic(settings, "edit");
        LayoutContext context = new DefaultLayoutContext(true, new LocalContext(), edit);
        IMObjectEditor editor = ServiceHelper.getBean(IMObjectEditorFactory.class).create(settings, context);
        editor.getComponent();
        EditDialog dialog = ServiceHelper.getBean(EditDialogFactory.class).create(editor, context.getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                if (version != settings.getVersion()) {
                    onChanged(settings);
                }
            }
        });
        dialog.show();
    }

    /**
     * Returns the identifiers of buttons to display with this tab.
     *
     * @return the button identifiers
     */
    public String[] getButtons() {
        return buttons;
    }

    /**
     * Invoked when a button is pressed.
     *
     * @param button the button identifier
     */
    public void onButton(String button) {
    }

    /**
     * Invoked when the settings have changed.
     * <p/>
     * This displays the new settings.
     *
     * @param settings the updated settings
     */
    protected void onChanged(Entity settings) {
        show();
    }
}
