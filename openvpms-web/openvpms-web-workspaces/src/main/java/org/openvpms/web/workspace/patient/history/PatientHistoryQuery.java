/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.CheckBox;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.customer.CommunicationArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.list.ShortNameListModel;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.PageLocator;
import org.openvpms.web.component.im.query.ParticipantConstraint;
import org.openvpms.web.component.im.query.QueryHelper;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.relationship.RelationshipHelper;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * Patient medical record history query.
 * <p>
 * This returns <em>act.patientClinicalEvent</em> and <em>act.customerCommunication*</em> acts within a date range.
 * <br/>
 * It provides a selector to filter acts items; filtering must be performed by the caller.
 *
 * @author Tim Anderson
 */
public class PatientHistoryQuery extends AbstractPatientHistoryQuery {

    /**
     * Determines if the history is sorted on ascending or descending start time.
     */
    private final boolean sortHistoryAscending;

    /**
     * Determines if communications are included.
     */
    private final boolean includeCommunications;

    /**
     * Determines if charges are included.
     */
    private CheckBox includeCharges;

    /**
     * The product type selector.
     */
    private ProductTypeSelector productType;

    /**
     * The archetypes to query.
     */
    private static final String[] ARCHETYPES = new String[]{PatientArchetypes.CLINICAL_EVENT,
                                                            CommunicationArchetypes.ACTS,
                                                            SMSArchetypes.MESSAGE,
                                                            SMSArchetypes.REPLY};

    /**
     * Document act version archetypes.
     */
    private static final String[] DOC_VERSION_ARCHETYPES = new String[]{
            InvestigationArchetypes.PATIENT_INVESTIGATION_VERSION,
            PatientArchetypes.DOCUMENT_ATTACHMENT_VERSION,
            PatientArchetypes.DOCUMENT_IMAGE_VERSION,
            PatientArchetypes.DOCUMENT_LETTER_VERSION};

    /**
     * Constructs a {@link PatientHistoryQuery}.
     *
     * @param patient     the patient to query
     * @param preferences user preferences
     */
    public PatientHistoryQuery(Party patient, Preferences preferences) {
        this(patient, preferences.getBoolean(PreferenceArchetypes.HISTORY, "showCharges", true),
             preferences.getBoolean(PreferenceArchetypes.HISTORY, "showCommunications", true),
             getSortHistoryAscending(preferences));
        setSortAscending(getSortAscending(preferences));
    }

    /**
     * Constructs a {@link PatientHistoryQuery}.
     * <p/>
     * This excludes communications.
     *
     * @param patient the patient to query
     * @param charges if {@code true}, include charges
     */
    public PatientHistoryQuery(Party patient, boolean charges) {
        this(patient, charges, false);
    }

    /**
     * Constructs a {@link PatientHistoryQuery}.
     *
     * @param patient        the patient to query
     * @param charges        if {@code true}, include charges
     * @param communications if {@code true}, include communications, otherwise exclude them
     */
    public PatientHistoryQuery(Party patient, boolean charges, boolean communications) {
        this(patient, charges, communications, false);
    }

    /**
     * Constructs a {@link PatientHistoryQuery}.
     *
     * @param patient        the patient to query
     * @param charges        if {@code true}, include charges
     * @param communications if {@code true}, include communications, otherwise exclude them
     */
    public PatientHistoryQuery(Party patient, boolean charges, boolean communications, boolean sortHistoryAscending) {
        super(patient, (communications) ? ARCHETYPES : new String[]{PatientArchetypes.CLINICAL_EVENT}, null);
        this.sortHistoryAscending = sortHistoryAscending;
        this.includeCommunications = communications;
        init(charges);
    }

    /**
     * Determines if charges should be included.
     *
     * @param include if {@code true}, include charges, else exclude them
     */
    public void setIncludeCharges(boolean include) {
        includeCharges.setSelected(include);
        onIncludeChargesChanged();
    }

    /**
     * Determines if charges should be included.
     *
     * @return if {@code true}, include charges, else exclude them
     */
    public boolean getIncludeCharges() {
        return includeCharges.isSelected();
    }

    /**
     * Determines if visits and communications are sorted in ascending order.
     *
     * @return {@code true} if they are being sorted ascending; {@code false} if descending
     */
    public boolean getSortHistoryAscending() {
        return sortHistoryAscending;
    }

    /**
     * Determines the page that an event falls on, excluding any date range constraints.
     *
     * @param object the event
     * @return the page that the event would fall on, if present
     */
    public int getPage(Act object) {
        int page = 0;
        IMObjectBean bean = IMObjectHelper.getBean(object);
        Reference patient = bean.getTargetRef("patient");
        if (patient != null && Objects.equals(patient, getEntityId())) {
            ArchetypeQuery query = new ArchetypeQuery(getShortNames(), false, false);
            query.add(new ParticipantConstraint("patient", PatientArchetypes.PATIENT_PARTICIPATION, patient));
            page = QueryHelper.getPage(object, query, getMaxResults(), "startTime", sortHistoryAscending,
                                       PageLocator.DATE_COMPARATOR);
        }
        return page;
    }

    /**
     * Returns the product types to select.
     *
     * @return the product type references, or an empty list if all product types are being selected
     */
    public Set<Reference> getProductTypes() {
        Set<Reference> result;
        ProductTypeSelector selector = getProductTypeSelector();
        if (selector.isAll()) {
            result = Collections.emptySet();
        } else {
            result = new HashSet<>();
            for (Entity entity : selector.getSelected()) {
                result.add(entity.getObjectReference());
            }
        }
        return result;
    }

    /**
     * Sets the product types to select.
     *
     * @param productTypes the product types
     */
    public void setProductTypes(Set<Reference> productTypes) {
        ProductTypeSelector selector = getProductTypeSelector();
        selector.setSelected(productTypes);
    }

    /**
     * Returns the preferred height of the query when rendered.
     *
     * @return the preferred height, or {@code null} if it has no preferred height
     */
    @Override
    public Extent getHeight() {
        return super.getHeight(2);
    }

    /**
     * The archetype short names being queried.
     * Any wildcards are expanded.
     *
     * @return the short names being queried
     */
    @Override
    public String[] getShortNames() {
        if (excludeCommunications()) {
            return new String[]{PatientArchetypes.CLINICAL_EVENT};
        }
        return super.getShortNames();
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        ResultSet<Act> result;
        if (excludeCommunications()) {
            // communications have been excluded by the type selector, so don't query them
            ShortNameConstraint archetypes = new ShortNameConstraint(PatientArchetypes.CLINICAL_EVENT, true, true);
            result = new ActResultSet<>(archetypes, getParticipantConstraint(), getFrom(), getTo(),
                                        getStatuses(), excludeStatuses(), getConstraints(), getMaxResults(), sort);
        } else {
            result = super.createResultSet(sort);
        }
        return result;
    }

    /**
     * Initialises the query.
     *
     * @param charges if {@code true}, display charges
     */
    protected void init(boolean charges) {
        String[] itemArchetypes = RelationshipHelper.getTargetShortNames(ServiceHelper.getArchetypeService(),
                                                                         PatientArchetypes.CLINICAL_EVENT_ITEM);
        setAvailableArchetypes(itemArchetypes);

        String[] archetypes = ArrayUtils.addAll(itemArchetypes, DOC_VERSION_ARCHETYPES);
        if (includeCommunications) {
            // add a single entry for all communication records
            ShortNameListModel model = getAvailableArchetypesModel();
            model.add(CommunicationArchetypes.ACTS, Messages.get("patient.record.query.communications"), false);
            archetypes = ArrayUtils.add(archetypes, CommunicationArchetypes.ACTS);
        }
        if (charges) {
            setSelectedArchetypes(ArrayUtils.add(archetypes, CustomerAccountArchetypes.INVOICE_ITEM));
        } else {
            setSelectedArchetypes(archetypes);
        }
        includeCharges = CheckBoxFactory.create(charges);

        includeCharges.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onIncludeChargesChanged();
                onQuery();
            }
        });

        if (sortHistoryAscending) {
            setDefaultSortConstraint(ASCENDING_START_TIME);
        } else {
            setDefaultSortConstraint(DESCENDING_START_TIME);
        }
    }

    /**
     * Lays out the component in a container, and sets focus on the instance name.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        FocusGroup focusGroup = getFocusGroup();
        SelectField shortNameSelector = getArchetypeSelector();
        Button sort = getSort();
        ProductTypeSelector selector = getProductTypeSelector();

        container.add(LabelFactory.create("query.type"));
        container.add(shortNameSelector);
        focusGroup.add(shortNameSelector);
        Row subrow = RowFactory.create(Styles.CELL_SPACING, LabelFactory.create("patient.record.query.includeCharges"),
                                       includeCharges, sort);
        container.add(subrow);
        focusGroup.add(includeCharges);
        focusGroup.add(sort);
        addSearchField(subrow);
        container.add(LabelFactory.create("patient.record.query.productType"));
        container.add(selector);
        focusGroup.add(selector.getTarget());
        addDateRange(container);
    }

    /**
     * Creates a container component to lay out the query component in.
     *
     * @return a new container
     * @see #doLayout(Component)
     */
    @Override
    protected Component createContainer() {
        return GridFactory.create(3);
    }

    /**
     * Updates the short names to query.
     *
     * @param model    the model
     * @param selected the selected index
     */
    protected void updateSelectedArchetypes(ShortNameListModel model, int selected) {
        String[] archetypes;
        if (model.isAll(selected)) {
            archetypes = model.getShortNames();
            archetypes = ArrayUtils.addAll(archetypes, DOC_VERSION_ARCHETYPES);
        } else {
            String archetype = model.getShortName(selected);
            archetypes = getSelectedArchetypes(archetype);
        }
        if (includeCharges.isSelected()) {
            archetypes = ArrayUtils.add(archetypes, CustomerAccountArchetypes.INVOICE_ITEM);
        }
        setSelectedArchetypes(archetypes);
    }

    /**
     * Determines if visits are sorted on ascending or descending start time.
     *
     * @param preferences user preferences
     * @return the {@code true} if visits are sorted on ascending start time, {@code false} if they are sorted
     * descending
     */
    protected static boolean getSortHistoryAscending(Preferences preferences) {
        String sort = preferences.getString(PreferenceArchetypes.HISTORY, "historySort", "DESC");
        return "ASC".equals(sort);
    }

    /**
     * Determines if communications were included in the query, but excluded by the type selector.
     *
     * @return {@code true} if communications are excluded
     */
    private boolean excludeCommunications() {
        return includeCommunications && !ArrayUtils.contains(getSelectedArchetypes(), CommunicationArchetypes.ACTS);
    }

    /**
     * Returns the selected archetypes.
     *
     * @param archetype the archetype
     * @return the corresponding archetypes
     */
    private String[] getSelectedArchetypes(String archetype) {
        if (InvestigationArchetypes.PATIENT_INVESTIGATION.equals(archetype)) {
            return new String[]{archetype, InvestigationArchetypes.PATIENT_INVESTIGATION_VERSION};
        } else if (PatientArchetypes.DOCUMENT_ATTACHMENT.equals(archetype)) {
            return new String[]{archetype, PatientArchetypes.DOCUMENT_ATTACHMENT_VERSION};
        } else if (PatientArchetypes.DOCUMENT_IMAGE.equals(archetype)) {
            return new String[]{archetype, PatientArchetypes.DOCUMENT_IMAGE_VERSION};
        } else if (PatientArchetypes.DOCUMENT_LETTER.equals(archetype)) {
            return new String[]{archetype, PatientArchetypes.DOCUMENT_LETTER_VERSION};
        }
        return new String[]{archetype};
    }

    /**
     * Invoked when the include charges flag is changed.
     * <p>
     * This updates the selected short names, and the session preferences.
     */
    private void onIncludeChargesChanged() {
        updateSelectedArchetypes();

        // update session preferences
        Preferences preferences = getPreferences();
        if (preferences != null) {
            preferences.setPreference(PreferenceArchetypes.HISTORY, "showCharges", includeCharges.isSelected());
        }
    }

    /**
     * Returns the product type selector.
     *
     * @return the product type selector
     */
    private ProductTypeSelector getProductTypeSelector() {
        if (productType == null) {
            productType = new ProductTypeSelector();
            productType.setListener(this::onQuery);
        }
        return productType;
    }
}
