/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Helper to retrieve and display logos.
 *
 * @author Tim Anderson
 */
public class LogoHelper {

    /**
     * Returns the logo act associated with an entity.
     *
     * @param object the entity
     * @return the logo act, or {@code null} if none exists
     */
    public static DocumentAct getLogo(Entity object, ArchetypeService service) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<DocumentAct> query = builder.createQuery(DocumentAct.class);
        Root<DocumentAct> from = query.from(DocumentAct.class, DocumentArchetypes.LOGO_ACT);
        Join<DocumentAct, IMObject> owner = from.join("owner");
        owner.on(builder.equal(owner.get("entity"), object.getObjectReference()));
        builder.asc(from.get("id"));
        return service.createQuery(query).getFirstResult();
    }


}