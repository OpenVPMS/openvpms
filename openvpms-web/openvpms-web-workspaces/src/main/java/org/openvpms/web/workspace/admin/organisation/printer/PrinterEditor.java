/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.printer;

import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.list.ListModel;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.print.BoundPrinterField;
import org.openvpms.web.component.print.PrinterListModel;
import org.openvpms.web.component.property.Property;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Editor for <em>entity.printer</em>.
 *
 * @author Tim Anderson
 */
public class PrinterEditor extends AbstractIMObjectEditor {

    /**
     * The printer selector.
     */
    private final PropertyComponentEditor printer;

    /**
     * The printer selector.
     */
    private final SelectField printerSelector;

    /**
     * Constructs a {@link PrinterEditor}.
     *
     * @param object            the object to edit
     * @param parent            the parent object. May be {@code null}
     * @param availablePrinters the available printers
     * @param layoutContext     the layout context
     */
    public PrinterEditor(IMObject object, IMObject parent, List<PrinterReference> availablePrinters,
                         LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        Property property = getProperty("printer");
        printerSelector = new BoundPrinterField(property, availablePrinters);
        printer = new PropertyComponentEditor(property, printerSelector);
        printer.addModifiableListener(modifiable -> onPrinterChanged());
        addEditor(printer);
    }

    /**
     * Sets the available printer names.
     *
     * @param references the available printer references
     */
    public void setAvailablePrinters(List<PrinterReference> references) {
        ((SelectField) printer.getComponent()).setModel(createModel(references));
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = new PrinterLayoutStrategy();
        strategy.addComponent(new ComponentState(printer));
        return strategy;
    }

    /**
     * Creates a model of available printers.
     *
     * @param references the available printer references
     * @return a new model
     */
    private ListModel createModel(Collection<PrinterReference> references) {
        PrinterReference reference = PrinterReference.fromString(getProperty("printer").getString());
        if (reference != null) {
            references = new HashSet<>(references);
            references.add(reference);
        }
        return new PrinterListModel(references);
    }

    /**
     * Invoked when the printer changes. Sets the name to the printer name.
     */
    private void onPrinterChanged() {
        PrinterReference reference = (PrinterReference) printerSelector.getSelectedItem();
        String name = (reference != null) ? reference.getName() : null;
        getProperty("name").setValue(name);
    }

}