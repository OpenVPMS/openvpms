/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.DefaultEditableComponentFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.sms.BoundSMSTextArea;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * A component to test the expression evaluation of SMS templates.
 *
 * @author Tim Anderson
 */
public abstract class SMSTemplateSampler {

    /**
     * The template.
     */
    private final Entity template;

    /**
     * The maximum no. of parts in SMS messages.
     */
    private final int maxParts;

    /**
     * The layout context.
     */
    private final LayoutContext layoutContext;

    /**
     * The property to hold the generated message.
     */
    private final SimpleProperty message = new SimpleProperty("message", null, String.class,
                                                              Messages.get("sms.message"));

    /**
     * The evaluation status.
     */
    private final Label status;

    /**
     * The focus group.
     */
    private final FocusGroup group = new FocusGroup("Sampler");

    /**
     * Constructs an {@link SMSTemplateSampler}.
     *
     * @param template      the template
     * @param layoutContext the layout context
     */
    public SMSTemplateSampler(Entity template, LayoutContext layoutContext) {
        this.template = template;

        // create a local context so changes aren't propagated
        Context local = new LocalContext(layoutContext.getContext());
        this.layoutContext = new DefaultLayoutContext(true, local, layoutContext.getHelpContext());
        this.layoutContext.setComponentFactory(new DefaultEditableComponentFactory(this.layoutContext));
        status = LabelFactory.create(true, true);

        SMSService smsService = ServiceHelper.getSMSService();
        maxParts = smsService.getMaxParts();
        message.setMaxLength(-1); // don't limit message length
    }

    /**
     * Evaluates the template.
     */
    public void evaluate() {
        String value;
        String statusText = null;
        try {
            value = evaluate(template, layoutContext.getContext());
            if (value != null) {
                int maxLength = SMSLengthCalculator.getMaxLength(maxParts, value);
                if (value.length() > maxLength) {
                    value = value.substring(0, maxLength);
                    statusText = Messages.get("sms.truncated");
                }
            }
        } catch (Throwable exception) {
            value = null;
            statusText = (exception.getCause() != null) ? exception.getCause().getMessage()
                                                        : exception.getMessage();
        }
        message.setValue(value);
        status.setText(statusText);
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group
     */
    public FocusGroup getFocusGroup() {
        return group;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        FocusGroup group = getFocusGroup();
        BoundSMSTextArea message = new BoundSMSTextArea(this.message, 40, 15);
        message.setMaxParts(maxParts);
        message.setStyleName(Styles.DEFAULT);
        message.setEnabled(false);

        Label messageLabel = LabelFactory.create();
        messageLabel.setText(this.message.getDisplayName());

        ComponentGrid grid = new ComponentGrid();
        layoutFields(grid, group, layoutContext);
        grid.add(LabelFactory.create("sms.title", Styles.BOLD), LabelFactory.create(),
                 LabelFactory.create("sms.appointment.status", Styles.BOLD));
        status.setTextAlignment(Alignment.ALIGN_TOP);
        status.setLayoutData(ComponentGrid.layout(new Alignment(Alignment.LEFT, Alignment.TOP)));
        grid.add(LabelFactory.create("sms.message"), RowFactory.create(message));
        grid.set(2, 2, 2, status);
        group.add(message);
        return ColumnFactory.create(Styles.INSET, grid.createGrid());
    }

    /**
     * Lays out the editable fields in a grid.
     *
     * @param grid    the grid
     * @param group   the focus group
     * @param context the layout context
     */
    protected abstract void layoutFields(ComponentGrid grid, FocusGroup group, LayoutContext context);

    /**
     * Evaluates the template.
     *
     * @param template the template
     * @param context  the context
     * @return the result of the evaluation. May be {@code null}
     */
    protected abstract String evaluate(Entity template, Context context);

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return layoutContext;
    }

}
