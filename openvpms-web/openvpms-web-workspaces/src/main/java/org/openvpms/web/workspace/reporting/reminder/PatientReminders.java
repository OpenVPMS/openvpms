/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderType.GroupBy;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Patient reminders prepared for processing by an {@link PatientReminderProcessor}.
 *
 * @author Tim Anderson
 */
public class PatientReminders {

    /**
     * The reminders to process.
     */
    private final List<ReminderEvent> reminders = new ArrayList<>();

    /**
     * The reminder grouping policy. This is used to determine which document template, if any, is selected to process
     * reminders.
     */
    private final GroupBy groupBy;

    /**
     * Cancelled reminders.
     */
    private final List<ReminderEvent> cancelled = new ArrayList<>();

    /**
     * Reminders in error.
     */
    private final List<ReminderEvent> errors = new ArrayList<>();

    /**
     * Reminders or their items that have been updated.
     */
    private final Map<ReminderEvent, List<Act>> updated = new LinkedHashMap<>();

    /**
     * Reminders that were successfully saved.
     */
    private final List<ReminderEvent> saved = new ArrayList<>();

    /**
     * Determines if reminders are being resent.
     */
    private final boolean resend;

    /**
     * Constructs a {@link PatientReminders}.
     *
     * @param groupBy the reminder grouping policy. This is used to determine which document template, if any, is
     *                selected to process reminders.
     * @param resend  determines if reminders are being resent
     */
    public PatientReminders(GroupBy groupBy, boolean resend) {
        this.groupBy = groupBy;
        this.resend = resend;
    }

    /**
     * Adds a reminder to send.
     *
     * @param reminder the reminder
     */
    public void addReminder(ReminderEvent reminder) {
        reminders.add(reminder);
    }

    /**
     * Returns the reminders.
     *
     * @return the reminders
     */
    public List<ReminderEvent> getReminders() {
        return new ArrayList<>(reminders);
    }

    /**
     * Returns the first reminder to send.
     *
     * @return the first reminder, or {@code null} if there are none
     */
    public ReminderEvent getFirst() {
        return !reminders.isEmpty() ? reminders.get(0) : null;
    }

    /**
     * Determines if there are reminders to send.
     *
     * @return {@code true} if there are reminders to send, otherwise {@code false}
     */
    public boolean canSend() {
        return getFirst() != null;
    }

    /**
     * Returns the reminder grouping policy.
     *
     * @return the reminder grouping policy
     */
    public GroupBy getGroupBy() {
        return groupBy;
    }

    /**
     * Adds updated acts, to be saved on completion of processing.
     *
     * @param reminder the reminder the acts are associated with
     * @param acts     the updated acts
     */
    public void addUpdated(ReminderEvent reminder, Act... acts) {
        addUpdated(reminder, Arrays.asList(acts));
    }

    /**
     * Adds updated acts, to be saved on completion of processing.
     *
     * @param reminder the reminder the acts are associated with
     * @param acts     the updated acts
     */
    public void addUpdated(ReminderEvent reminder, List<Act> acts) {
        updated.put(reminder, acts);
    }

    /**
     * Returns the updated acts.
     * <p/>
     * Each collection represents one or more reminder/reminder item acts that should be saved together.
     *
     * @return the updated acts
     */
    public Map<ReminderEvent, List<Act>> getUpdated() {
        return updated;
    }

    /**
     * Adds a cancelled reminder.
     *
     * @param reminder the cancelled reminder
     */
    public void addCancelled(ReminderEvent reminder) {
        cancelled.add(reminder);
        reminders.remove(reminder);
    }

    /**
     * Returns the cancelled reminders.
     *
     * @return the cancelled reminders
     */
    public List<ReminderEvent> getCancelled() {
        return cancelled;
    }

    /**
     * Adds a reminder that is in error.
     *
     * @param reminder the reminder
     */
    public void addError(ReminderEvent reminder) {
        errors.add(reminder);
        reminders.remove(reminder);
        cancelled.remove(reminder);
    }

    /**
     * Returns the reminders in error.
     *
     * @return the reminders in error
     */
    public List<ReminderEvent> getErrors() {
        return errors;
    }

    /**
     * Adds a reminder that was successfully saved.
     *
     * @param reminder the reminder
     */
    public void addSaved(ReminderEvent reminder) {
        saved.add(reminder);
    }

    /**
     * Returns all reminders that have not been saved.
     *
     * @return the unsaved reminders
     */
    public List<ReminderEvent> getUnsaved() {
        Set<ReminderEvent> result = new LinkedHashSet<>(reminders);
        result.addAll(updated.keySet()); // may include cancelled or error reminders
        result.removeAll(saved);
        return new ArrayList<>(result);
    }

    /**
     * Determines if reminders are being resent.
     *
     * @return {@code true} if reminders are being resent
     */
    public boolean getResend() {
        return resend;
    }

    /**
     * Creates a context for the reminders.
     *
     * @param practice the practice
     * @return a new context
     */
    public Context createContext(Party practice) {
        Context context = new LocalContext();
        context.setPractice(practice);
        return context;
    }

    /**
     * Returns the number of processed reminder items.
     *
     * @return the number of processed reminder items
     */
    public int getProcessed() {
        return reminders.size() + cancelled.size() + errors.size();
    }

    /**
     * Returns the supplied reminders as a collection of {@link ObjectSet}s.
     *
     * @param reminders the reminders
     * @return the reminders as {@link ObjectSet}s
     */
    public List<ObjectSet> getObjectSets(List<ReminderEvent> reminders) {
        return new ArrayList<>(reminders);
    }
}
