/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.print.DefaultBatchPrinter;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;
import java.util.function.Consumer;

/**
 * A batch printer for claims.
 *
 * @author Tim Anderson
 */
class ClaimBatchPrinter extends DefaultBatchPrinter<IMObject> {

    /**
     * The claim.
     */
    private final Claim claim;

    /**
     * The listener to notify on completion.
     */
    private final Consumer<Throwable> listener;

    /**
     * Constructs a {@link ClaimBatchPrinter}.
     *
     * @param claim    the claim
     * @param selected the objects to print
     * @param context  the context
     * @param help     the help context
     * @param listener the listener to notify when the printing completes
     */
    public ClaimBatchPrinter(Claim claim, List<IMObject> selected, Context context, HelpContext help,
                             Consumer<Throwable> listener) {
        super(selected, context, help);
        this.claim = claim;
        this.listener = listener;
    }

    /**
     * Creates a new document template locator to locate the template for the object being printed.
     *
     * @param object  the object to print
     * @param context the context
     * @return a new document template locator
     */
    @Override
    protected DocumentTemplateLocator createDocumentTemplateLocator(IMObject object, Context context) {
        ClaimHelper helper = new ClaimHelper(ServiceHelper.getArchetypeService());
        return helper.getDocumentTemplateLocator(claim, object, context);
    }

    /**
     * Invoked when printing completes.
     */
    @Override
    protected void completed() {
        listener.accept(null);
    }
}
