/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.HttpImageReference;
import nextapp.echo2.app.Label;
import org.apache.commons.lang.StringUtils;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.DocumentBackedTextProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.servlet.DownloadServlet;
import org.openvpms.web.echo.style.Styles;

import java.util.List;

import static org.openvpms.web.component.im.view.Hint.height;

/**
 * Layout strategy for <em>act.patientInvestigationResultItem</em>.
 *
 * @author Tim Anderson
 */
public class ResultLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The result node name.
     */
    private static final String RESULT = "result";

    /**
     * The notes node name.
     */
    private static final String NOTES = "notes";

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes NODES = ArchetypeNodes.all().excludeIfEmpty(
            "value", "units", "qualifier", "extremeLowRange", "lowRange", "highRange", "extremeHighRange",
            "outOfRange", "referenceRange").exclude(NOTES);

    /**
     * Constructs a {@link ResultLayoutStrategy}.
     */
    public ResultLayoutStrategy() {
        setArchetypeNodes(new ArchetypeNodes(NODES));
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        ComponentState result = getResult(object, properties, context);
        if (result != null) {
            addComponent(result);
            getArchetypeNodes().exclude(RESULT);
        }
        Property notes = new DocumentBackedTextProperty(object, properties.get(NOTES), "longNotes");
        if (!StringUtils.isEmpty(notes.getString())) {
            // will display as a single line if short
            addComponent(createMultiLineText(notes, 1, 20, Styles.FULL_WIDTH, context));
            getArchetypeNodes().exclude(NOTES);
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out child components in a grid.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doSimpleLayout(IMObject object, IMObject parent, List<Property> properties, Component container,
                                  LayoutContext context) {
        ComponentState result = getComponent(RESULT);
        if (result == null) {
            // no component to render the result, so create one that forces to render it on a single line.
            ComponentState state = context.getComponentFactory().create(ArchetypeNodes.find(properties, RESULT), object,
                                                                        height(1));
            addComponent(state);
        }
        ComponentState notes = getComponent(NOTES);
        ComponentGrid grid = createGrid(object, properties, context, 2);
        addAuditInfo(object, grid, context);
        if (result != null) {
            grid.add(result, 2);
        }
        if (notes != null) {
            grid.add(notes, 2);
        }
        Grid component = createGrid(grid);
        component.setWidth(Styles.FULL_WIDTH);
        container.add(ColumnFactory.create(Styles.INSET, component));
    }

    /**
     * Creates a component to render the text result and image in a larger area beneath the simple properties,
     * but only if one or both are present. If only the text result is present, and the text is short,
     * don't create a component - this will be handled as part of the normal layout.
     * <p/>
     * If there is no text result nor image, the result is suppressed
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     * @return a component to render the result, or {@code null} if no special handling is required
     */
    private ComponentState getResult(IMObject object, PropertySet properties, LayoutContext context) {
        ComponentState state = null;
        Property property = new DocumentBackedTextProperty(object, properties.get(RESULT), "longResult");
        Label image = getImage(object);
        if (getLines(property) > 1 || image != null) {
            boolean empty = StringUtils.isEmpty(property.getString());
            ComponentState text = (!empty) ? createMultiLineText(property, 2, 20, Styles.FULL_WIDTH, context) : null;

            if (text != null && image != null) {
                state = new ComponentState(ColumnFactory.create(Styles.CELL_SPACING, image, text.getComponent()),
                                           property);
            } else if (text != null) {
                state = text;
            } else {
                state = new ComponentState(image, property);
            }
        }
        return state;
    }

    private Label getImage(IMObject object) {
        Label result = null;
        IMObjectBean bean = getBean(object);
        DocumentAct image = bean.getTarget("image", DocumentAct.class);
        if (image != null && image.getDocument() != null) {
            String uri = DownloadServlet.getDocumentURI(image.getDocument());
            result = LabelFactory.create(new HttpImageReference(uri));
        }
        return result;
    }
}
