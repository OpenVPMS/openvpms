/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.ContextException;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.system.ServiceHelper;


/**
 * Task to query the most recent <em>act.customerAccountChargesInvoice</em>
 * with IN_PROGRESS or COMPLETED status for the context customer.
 * If one is present, adds it to the context.
 *
 * @author Tim Anderson
 */
public class GetInvoiceTask extends SynchronousTask {

    /**
     * The rules.
     */
    private final CustomerAccountRules rules;

    /**
     * Constructs a {@link GetInvoiceTask}.
     */
    public GetInvoiceTask() {
        rules = ServiceHelper.getBean(CustomerAccountRules.class);
    }

    /**
     * Executes the task.
     *
     * @param context the context
     * @throws OpenVPMSException for any error
     */
    public void execute(TaskContext context) {
        FinancialAct invoice = getInvoice(context);
        if (invoice != null) {
            context.addObject(invoice);
        }
    }

    /**
     * Returns the latest {@code IN_PROGRESS} or {@code COMPLETED} invoice for a customer.
     * <p>
     * Invoices with {@code IN_PROGRESS} will be returned in preference to {@code COMPLETED} ones.
     *
     * @param context the context
     * @return the customer invoice, or {@code null} if none is found
     */
    protected FinancialAct getInvoice(TaskContext context) {
        Party customer = context.getCustomer();
        if (customer == null) {
            throw new ContextException(ContextException.ErrorCode.NoCustomer);
        }
        return rules.getInvoice(customer);
    }
}
