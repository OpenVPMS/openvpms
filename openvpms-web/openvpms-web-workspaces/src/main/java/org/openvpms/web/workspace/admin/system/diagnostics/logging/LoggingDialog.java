/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics.logging;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.list.DefaultListModel;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.bound.BoundSelectFieldFactory;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.component.im.util.VirtualNodeSortConstraint;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Configures logging.
 *
 * @author Tim Anderson
 */
public class LoggingDialog extends ModalDialog {

    /**
     * The logging configuration.
     */
    private final LoggingConfig config;

    /**
     * The table displaying the added categories.
     */
    private final PagedIMTable<Pair<String, Level>> table;

    /**
     * The logging category.
     */
    private final Property category = SimpleProperty.newProperty().name("category").type(String.class)
            .displayNameKey("admin.system.logging.category")
            .build();

    /**
     * The logging level.
     */
    private final Property level = SimpleProperty.newProperty().name("level").type(Level.class)
            .required()
            .value(Level.INFO)
            .displayNameKey("admin.system.logging.level")
            .build();

    /**
     * Logging category table container.
     */
    private final Column container = new Column();

    /**
     * Reset button identifier.
     */
    private static final String RESET_ID = "button.reset";

    /**
     * Constructs a {@link LoggingDialog}.
     */
    public LoggingDialog() {
        super(Messages.get("admin.system.diagnostic.logging.title"), "MessageDialog",
              new String[]{CLOSE_ID, RESET_ID});
        config = ServiceHelper.getBean(LoggingConfig.class);
        table = new PagedIMTable<>(new Model());
        table.getTable().addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                Pair<String, Level> selected = table.getSelected();
                if (selected != null) {
                    category.setValue(selected.getLeft());
                    level.setValue(selected.getRight());
                }
            }
        });
        resize("LoggingDialog.size");
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (RESET_ID.equals(button)) {
            onReset();
        } else {
            super.onButton(button);
        }
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        refresh();
        Level[] levels = new Level[]{Level.TRACE, Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR, Level.FATAL};
        SelectField field = BoundSelectFieldFactory.create(level, new DefaultListModel(levels));
        Button add = ButtonFactory.create("button.add", this::onAdd);
        Button remove = ButtonFactory.create("button.delete", this::onRemove);
        Row row = RowFactory.create(Styles.CELL_SPACING,
                                    LabelFactory.create("admin.system.diagnostic.logging.category"),
                                    BoundTextComponentFactory.create(category, 70), field, add, remove);

        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, row, container);
        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
    }

    /**
     * Adds the entered category.
     */
    private void onAdd() {
        String c = category.getString();
        Level l = (Level) level.getValue();
        if (c != null && l != null) {
            config.add(c, l);
            category.setValue(null);
            refresh();
        }
    }

    /**
     * Removes the selected category.
     */
    private void onRemove() {
        String c = category.getString();
        if (c != null) {
            config.remove(c);
            category.setValue(null);
            refresh();
        }
    }

    /**
     * Refreshes the display.
     */
    private void refresh() {
        int page = table.getPage();
        SortConstraint[] sort = (table.getResultSet() != null) ? table.getResultSet().getSortConstraints() : null;
        List<Pair<String, Level>> list = new ArrayList<>();
        Map<String, Level> categories = this.config.getCategories();
        if (sort == null) {
            categories = new TreeMap<>(categories);
        }
        categories.forEach((key, value) -> list.add(Pair.of(key, value)));
        ListResultSet<Pair<String, Level>> set = new ListResultSet<Pair<String, Level>>(list, 15) {
            @Override
            public void sort(SortConstraint[] sort) {
                IMObjectSorter.sort(getObjects(), sort, input -> input);
                super.sort(sort);
            }
        };
        if (sort != null) {
            set.sort(sort);
        }
        table.setResultSet(set);
        if (table.getPage() != page) {
            table.getNavigator().setPage(page);
        }
        container.removeAll();
        if (set.getResults() != 0) {
            container.add(table.getComponent());
        } else {
            Label label = LabelFactory.create("admin.system.diagnostic.logging.none", Styles.BOLD);
            label.setLayoutData(ColumnFactory.layout(Alignment.ALIGN_CENTER));

            Column wrapper = ColumnFactory.create(Styles.LARGE_INSET, label);
            wrapper.setLayoutData(ColumnFactory.layout(Alignment.ALIGN_CENTER));
            container.add(wrapper);
        }
    }

    /**
     * Displays a confirmation dialog to resets logging back to the default.
     */
    private void onReset() {
        ConfirmationDialog.newDialog().titleKey("admin.system.diagnostic.logging.title")
                .messageKey("admin.system.diagnostic.logging.reset")
                .yesNo()
                .yes(() -> {
                    config.reset();
                    refresh();
                })
                .show();
    }

    private static class Model extends AbstractIMTableModel<Pair<String, Level>> {

        private static final int CATEGORY_INDEX = 0;

        private static final int LEVEL_INDEX = CATEGORY_INDEX + 1;

        /**
         * Constructs a {@link Model}.
         */
        public Model() {
            TableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(CATEGORY_INDEX, "admin.system.diagnostic.logging.category"));
            model.addColumn(createTableColumn(LEVEL_INDEX, "admin.system.diagnostic.logging.level"));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            if (column == CATEGORY_INDEX) {
                return new SortConstraint[]{new VirtualNodeSortConstraint("category", ascending, object -> {
                    Pair<String, Level> pair = (Pair<String, Level>) object;
                    return pair.getLeft();
                })};
            } else if (column == LEVEL_INDEX) {
                return new SortConstraint[]{new VirtualNodeSortConstraint("level", ascending, object -> {
                    Pair<String, Level> pair = (Pair<String, Level>) object;
                    return pair.getRight();
                })};
            }
            return null;
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Pair<String, Level> object, TableColumn column, int row) {
            int index = column.getModelIndex();
            if (index == CATEGORY_INDEX) {
                return object.getLeft();
            } else if (index == LEVEL_INDEX) {
                return object.getRight();
            }
            return null;
        }
    }
}