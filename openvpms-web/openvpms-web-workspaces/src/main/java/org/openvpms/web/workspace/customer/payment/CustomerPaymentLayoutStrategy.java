/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.workspace.customer.account.CustomerAccountLayoutStrategy;

import java.util.List;

/**
 * Layout strategy for <em>act.customerAccountPayment</em>.
 * <p/>
 * This suppresses the audit node if it is empty.
 *
 * @author Tim Anderson
 */
public class CustomerPaymentLayoutStrategy extends CustomerAccountLayoutStrategy {

    /**
     * The audit node name.
     */
    private static final String AUDIT = "audit";


    /**
     * Constructs a {@link CustomerPaymentLayoutStrategy}.
     */
    public CustomerPaymentLayoutStrategy() {
        super();
    }

    /**
     * Constructs a {@link CustomerPaymentLayoutStrategy}.
     *
     * @param editor the act items editor
     * @param status the status editor
     */
    public CustomerPaymentLayoutStrategy(IMObjectCollectionEditor editor, PaymentStatus status) {
        addComponent(new ComponentState(editor));
        if (!status.isReadOnly()) {
            addComponent(new ComponentState(status.getEditor()));
        }
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (context.isEdit()) {
            getArchetypeNodes().exclude(AUDIT);
        } else {
            getArchetypeNodes().excludeIfEmpty("notes", AUDIT);
            Property audit = properties.get(AUDIT);
            if (audit.getString() != null) {
                // will display as a single line string if short
                addComponent(createMultiLineText(audit, 1, 5, new Extent(80, Extent.EX), context));
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out child components in a grid.
     *
     * @param object     the object to lay out
     * @param parent     the parent object. May be {@code null}
     * @param properties the properties
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doSimpleLayout(IMObject object, IMObject parent, List<Property> properties, Component container,
                                  LayoutContext context) {
        super.doSimpleLayout(object, parent, properties, container, context);
    }
}
