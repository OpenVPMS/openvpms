/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import echopointng.LabelEx;
import echopointng.layout.TableLayoutDataEx;
import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.workspace.workflow.appointment.sms.AppointmentSMSViewerDialog;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleTableCellRenderer;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleTableModel;

import java.util.Date;

import static org.openvpms.web.echo.style.Styles.FULL_WIDTH;

/**
 * Cell renderer for appointments.
 *
 * @author Tim Anderson
 */
public abstract class AbstractAppointmentTableCellRender extends ScheduleTableCellRenderer {

    /**
     * Constructs a {@link AbstractAppointmentTableCellRender}.
     *
     * @param model the table model
     */
    public AbstractAppointmentTableCellRender(ScheduleTableModel model) {
        super(model);
    }

    /**
     * Helper to create a label indicating the reminder status of an appointment.
     *
     * @param reminderSent  the date a reminder was sent. May be {@code null}
     * @param reminderError the reminder error, if the reminder couldn't be sent. May be {@code null}
     * @return a new label
     */
    public static Label createReminderIcon(Date reminderSent, String reminderError) {
        String style;
        String tooltip = null;
        if (!StringUtils.isEmpty(reminderError)) {
            style = "AppointmentReminder.error";
        } else if (reminderSent != null) {
            style = "AppointmentReminder.sent";
            tooltip = Messages.format("workflow.scheduling.appointment.table.reminded",
                                      DateFormatter.formatDateTimeAbbrev(reminderSent));
        } else {
            style = "AppointmentReminder.unsent";
        }
        Label label = LabelFactory.create(null, style);
        label.setToolTipText(tooltip);
        return label;
    }

    /**
     * Renders a component representing appointment details, along with icons indicating the appointment status.
     *
     * @param component the component
     * @param event     the appointment event
     * @return the component, with any icons
     */
    public static Component addAppointmentIcons(Component component, PropertySet event) {
        boolean sendReminder = event.exists(ScheduleEvent.SEND_REMINDER)
                               && event.getBoolean(ScheduleEvent.SEND_REMINDER);
        Date reminderSent = event.exists(ScheduleEvent.REMINDER_SENT) ?
                            event.getDate(ScheduleEvent.REMINDER_SENT) : null;
        String reminderError = event.exists(ScheduleEvent.REMINDER_ERROR) ?
                               event.getString(ScheduleEvent.REMINDER_ERROR) : null;
        Date confirmedTime = event.exists(ScheduleEvent.CONFIRMED_TIME) ?
                             event.getDate(ScheduleEvent.CONFIRMED_TIME) : null;
        boolean onlineBooking = event.exists(ScheduleEvent.ONLINE_BOOKING)
                                && event.getBoolean(ScheduleEvent.ONLINE_BOOKING);
        String smsStatus = event.exists(ScheduleEvent.SMS_STATUS) ? event.getString(ScheduleEvent.SMS_STATUS) : null;

        boolean hasReminder = sendReminder || reminderSent != null || reminderError != null;
        if (hasReminder || smsStatus != null || confirmedTime != null || onlineBooking) {
            if (!(component instanceof Row)) {
                component = RowFactory.create(Styles.CELL_SPACING, component);
            }
            if (hasReminder) {
                Label reminder = createReminderIcon(reminderSent, reminderError);
                reminder.setLayoutData(RowFactory.layout(new Alignment(Alignment.RIGHT, Alignment.TOP), FULL_WIDTH));
                component.add(reminder);
            }
            if (smsStatus != null) {
                Button sms = ButtonFactory.create();
                if (AppointmentStatus.SMS_RECEIVED.equals(smsStatus)) {
                    sms.setStyleName("Appointment.SMS.Unread");
                } else {
                    sms.setStyleName("Appointment.SMS.Default");
                }
                sms.setToolTipText(Messages.get("workflow.scheduling.appointment.table.viewsms"));
                sms.setLayoutData(RowFactory.layout(new Alignment(Alignment.RIGHT, Alignment.TOP), FULL_WIDTH));
                component.add(sms);
                sms.addActionListener(new ActionListener() {
                    @Override
                    public void onAction(ActionEvent actionEvent) {
                        Reference reference = event.getReference(ScheduleEvent.ACT_REFERENCE);
                        AppointmentSMSViewerDialog dialog = AppointmentSMSViewerDialog.create(reference);
                        if (dialog != null) {
                            dialog.addWindowPaneListener(new WindowPaneListener() {
                                @Override
                                public void onClose(WindowPaneEvent event) {
                                    sms.setStyleName("Appointment.SMS.Default");
                                }
                            });
                            dialog.show();
                        }
                    }
                });
                component.add(sms);
            }
            if (confirmedTime != null) {
                Label confirmed = new LabelEx(); // needs to be LabelEx to support sizing
                confirmed.setStyleName("Appointment.Confirmed");
                confirmed.setLayoutData(RowFactory.layout(new Alignment(Alignment.RIGHT, Alignment.TOP), FULL_WIDTH));
                confirmed.setToolTipText(Messages.format("workflow.scheduling.appointment.table.confirmed",
                                                         DateFormatter.formatDateTimeAbbrev(confirmedTime)));
                component.add(confirmed);
            }
            if (onlineBooking) {
                Label booking = LabelFactory.create(null, "Appointment.OnlineBooking");
                booking.setLayoutData(RowFactory.layout(new Alignment(Alignment.RIGHT, Alignment.TOP), FULL_WIDTH));
                component.add(booking);
            }
        }
        return component;
    }

    /**
     * Returns a component representing an event.
     *
     * @param table  the table
     * @param event  the event
     * @param column the column
     * @param row    the row
     * @return the component
     */
    @Override
    protected Component getEvent(Table table, PropertySet event, int column, int row) {
        Component result;
        if (Schedule.isBlockingEvent(event)) {
            result = getBlock(event);
        } else {
            result = getAppointment(event);
        }
        return result;
    }

    /**
     * Helper to create a multiline label with optional notes popup, if the supplied notes are non-null and
     * {@code displayNotes} is {@code true}.
     * <p/>
     * This includes icons for the reminder, confirmation time and online booking if present.
     *
     * @param text  the label text
     * @param notes the notes. May be {@code null}
     * @param event the appointment
     * @return a component representing the label with optional popup
     */
    protected Component createLabelWithNotes(String text, String notes, PropertySet event) {
        Component result = createLabelWithNotes(text, notes);
        return addAppointmentIcons(result, event);
    }

    /**
     * Returns the table layout data for an event .
     *
     * @param event     the event
     * @param highlight the highlight setting
     * @return layout data for the event, or {@code null} if no style information exists
     */
    @Override
    protected TableLayoutDataEx getEventLayoutData(PropertySet event, ScheduleTableModel.Highlight highlight) {
        TableLayoutDataEx result = null;
        if (Schedule.isBlockingEvent(event)) {
            Reference reference = event.getReference(ScheduleEvent.SCHEDULE_TYPE_REFERENCE);
            Color colour = getColour(reference);
            if (colour != null) {
                result = new TableLayoutDataEx();
                result.setBackground(colour);
            }
        } else {
            result = super.getEventLayoutData(event, highlight);
        }
        return result;
    }

    /**
     * Formats an appointment.
     *
     * @param event the event
     * @return the appointment component
     */
    private Component getAppointment(PropertySet event) {
        Component result;
        String text = evaluate(event);
        if (text == null) {
            String customer = event.getString(ScheduleEvent.CUSTOMER_NAME);
            String patient = event.getString(ScheduleEvent.PATIENT_NAME);
            String status = getModel().getStatus(event);
            String reason = event.getString(ScheduleEvent.ACT_REASON_NAME);
            if (reason == null) {
                // fall back to the code
                reason = event.getString(ScheduleEvent.ACT_REASON);
            }
            if (reason == null) {
                reason = Messages.get("workflow.scheduling.appointment.table.noreason");
            }

            if (patient == null) {
                if (customer == null) {
                    customer = Messages.get("workflow.scheduling.appointment.table.nocustomer");
                }
                text = Messages.format("workflow.scheduling.appointment.table.customer",
                                       customer, reason, status);
            } else {
                text = Messages.format("workflow.scheduling.appointment.table.customerpatient",
                                       customer, patient, reason, status);
            }
        }
        String notes = event.getString(ScheduleEvent.NOTES);
        result = createLabelWithNotes(text, notes, event);
        return result;
    }

    /**
     * Formats a block.
     *
     * @param event the blocking event
     * @return the block component
     */
    private Component getBlock(PropertySet event) {
        String text = event.getString(ScheduleEvent.ACT_NAME);
        if (text == null) {
            text = event.getString(ScheduleEvent.SCHEDULE_TYPE_NAME);
        }
        String notes = event.getString(ScheduleEvent.NOTES);
        return createLabelWithNotes(text, notes);
    }

}
