/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.SelectField;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityRelationshipEditor;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.print.BoundPrinterField;
import org.openvpms.web.component.property.Property;


/**
 * Editor for <em>entityRelationship.documentTemplatePrinters</em>.
 *
 * @author Tim Anderson
 */
public class DocumentTemplatePrinterEditor extends EntityRelationshipEditor {

    /**
     * The printer property.
     */
    private final Property printer;

    /**
     * The printer selector.
     */
    private final SelectField printerSelector;

    /**
     * Constructs a {@link DocumentTemplatePrinterEditor}.
     *
     * @param relationship the relationship
     * @param parent       the parent object
     * @param context      the layout context
     */
    public DocumentTemplatePrinterEditor(EntityRelationship relationship, IMObject parent, LayoutContext context) {
        super(relationship, parent, context);
        printer = getProperty("printer");
        printer.addModifiableListener(modifiable -> {
            onPrinterChanged();
        });
        printerSelector = new BoundPrinterField(printer);
        addEditor(new PropertyComponentEditor(printer, printerSelector));
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        strategy.addComponent(new ComponentState(printerSelector, printer));
        return strategy;
    }

    /**
     * Invoked when the printer changes. Updates the underlying property.
     */
    private void onPrinterChanged() {
        PrinterReference reference = (PrinterReference) printerSelector.getSelectedItem();
        printer.setValue(reference != null ? reference.toString() : null);
    }

}
