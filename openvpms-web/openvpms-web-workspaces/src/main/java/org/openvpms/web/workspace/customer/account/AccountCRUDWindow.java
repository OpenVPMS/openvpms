/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRuleException;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.account.CustomerBalanceUpdater;
import org.openvpms.archetype.tools.account.AccountBalanceTool;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.print.PrinterContext;
import org.openvpms.web.component.im.print.PrinterContextFactory;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.customer.account.payment.AdminCustomerPaymentEditDialog;
import org.openvpms.web.workspace.customer.account.payment.AdminCustomerPaymentEditor;
import org.openvpms.web.workspace.customer.charge.AbstractChargeCRUDWindow;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;


/**
 * CRUD window for invoices.
 *
 * @author Tim Anderson
 */
public class AccountCRUDWindow extends AbstractChargeCRUDWindow {

    /**
     * Reverse button identifier.
     */
    private static final String REVERSE_ID = "button.reverse";

    /**
     * Reverse button identifier.
     */
    private static final String STATEMENT_ID = "button.statement";

    /**
     * Adjust button identifier.
     */
    private static final String ADJUST_ID = "button.adjust";

    /**
     * Check button identifier.
     */
    private static final String CHECK_ID = "button.check";

    /**
     * Hide button identifier.
     */
    private static final String HIDE_ID = "button.hide";

    /**
     * Unhide button identifier.
     */
    private static final String UNHIDE_ID = "button.unhide";


    /**
     * Constructs an {@link AccountCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create
     * @param context    the context
     * @param help       the help context
     */
    public AccountCRUDWindow(Archetypes<FinancialAct> archetypes, Context context, HelpContext help) {
        super(archetypes, context, help);
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(FinancialAct object) {
        super.setObject(object);
        updateContext(CustomerAccountArchetypes.INVOICE, object);
    }

    /**
     * Print an object.
     *
     * @param object the object to print
     */
    @Override
    protected void print(FinancialAct object) {
        IMObjectBean bean = getBean(object);
        if (bean.hasNode("hide") && bean.getBoolean("hide")) {
            String displayName = getDisplayName(object);
            String title = Messages.format("customer.account.printhidden.title", displayName);
            String message = Messages.format("customer.account.printhidden.message", displayName, object.getId());
            ConfirmationDialog.show(title, message, ConfirmationDialog.YES_NO, new PopupDialogListener() {
                @Override
                public void onYes() {
                    AccountCRUDWindow.super.print(object);
                }
            });
        } else {
            super.print(object);
        }
    }

    /**
     * Mail an object.
     *
     * @param object the object to mail
     */
    @Override
    protected void mail(FinancialAct object) {
        IMObjectBean bean = getBean(object);
        if (bean.hasNode("hide") && bean.getBoolean("hide")) {
            String displayName = getDisplayName(object);
            String title = Messages.format("customer.account.mailhidden.title", displayName);
            String message = Messages.format("customer.account.mailhidden.message", displayName, object.getId());
            ConfirmationDialog.show(title, message, ConfirmationDialog.YES_NO, new PopupDialogListener() {
                @Override
                public void onYes() {
                    AccountCRUDWindow.super.mail(object);
                }
            });
        } else {
            super.mail(object);
        }
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        boolean admin = UserHelper.isAdmin(getContext().getUser());

        if (admin) {
            // If the logged in user is an administrator, can edit payments
            buttons.add(EDIT_ID, action(CustomerAccountArchetypes.PAYMENT, this::administerPayment,
                                        act -> getActions().canAdminPayment(act, getContext().getUser()),
                                        "customer.account.payment.edit"));
        }

        buttons.add(STATEMENT_ID, this::onStatement);
        buttons.add(ADJUST_ID, this::onAdjust);
        buttons.add(REVERSE_ID, this::onReverse);
        buttons.add(createPrintButton());
        buttons.add(createMailButton());

        if (admin) {
            // If the logged in user is an administrator, show the Check, Hide, Unhide buttons
            buttons.add(CHECK_ID, this::onCheck);
            buttons.add(HIDE_ID, this::onHide);
            buttons.add(UNHIDE_ID, this::onUnhide);
        }
        buttons.add(createEnableRemindersButton());
        buttons.add(createDisableRemindersButton());

        enableButtons(buttons, true);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        User user = getContext().getUser();
        buttons.setEnabled(EDIT_ID, enable && getActions().canAdminPayment(getObject(), user));
        buttons.setEnabled(REVERSE_ID, enable);
        enablePrintPreview(buttons, enable);
        buttons.setEnabled(CHECK_ID, enable);
        enableDisableReminderButtons(buttons, enable);
    }

    /**
     * Prints a statement.
     */
    protected void onStatement() {
        Context context = getContext();
        Party customer = context.getCustomer();
        if (customer != null) {
            PrinterContext printerContext = ServiceHelper.getBean(PrinterContextFactory.class).create();
            StatementPrinter printer = new StatementPrinter(context, ServiceHelper.getBean(CustomerAccountRules.class),
                                                            printerContext,
                                                            ServiceHelper.getBean(ReporterFactory.class));
            HelpContext help = getHelpContext().subtopic("statement");
            InteractiveStatementPrinter interactive = new InteractiveStatementPrinter(printer, context, help);
            interactive.setMailContext(new CustomerMailContext(context, true, help));
            interactive.print();
        }
    }

    /**
     * Invoked when the 'reverse' button is pressed.
     */
    protected void onReverse() {
        FinancialAct act = IMObjectHelper.reload(getObject());
        if (act != null) {
            String status = act.getStatus();
            if (act.isA(CustomerAccountArchetypes.OPENING_BALANCE, CustomerAccountArchetypes.CLOSING_BALANCE)
                || !FinancialActStatus.POSTED.equals(status)) {
                showStatusError(act, "customer.account.noreverse.title", "customer.account.noreverse.message");
            } else {
                Reverser reverser = new Reverser(getContext().getPractice(), getHelpContext().subtopic("reverse"));
                reverser.reverse(act, () -> onRefresh(act));
            }
        }
    }

    /**
     * Invoked when the 'adjust' button is pressed.
     */
    protected void onAdjust() {
        Party customer = getContext().getCustomer();
        CustomerAccountRules rules = ServiceHelper.getBean(CustomerAccountRules.class);
        if (customer != null) {
            String[] shortNames = {CustomerAccountArchetypes.DEBIT_ADJUST,
                                   CustomerAccountArchetypes.CREDIT_ADJUST,
                                   CustomerAccountArchetypes.BAD_DEBT};
            if (!rules.hasAccountActs(customer)) {
                // only allow Initial Balance acts if there are no other customer account acts
                shortNames = ArrayUtils.insert(0, shortNames, CustomerAccountArchetypes.INITIAL_BALANCE);
            }
            Archetypes<FinancialAct> archetypes = Archetypes.create(shortNames, FinancialAct.class,
                                                                    Messages.get("customer.account.createtype"));
            onCreate(archetypes);
        }
    }

    /**
     * Invoked when the 'check' button is pressed.
     */
    protected void onCheck() {
        final Party customer = getContext().getCustomer();
        if (customer != null) {
            try {
                CustomerAccountRules rules = getRules();
                BigDecimal expected = rules.getDefinitiveBalance(customer);
                BigDecimal actual = rules.getBalance(customer);
                if (expected.compareTo(actual) == 0) {
                    String title = Messages.get("customer.account.balancecheck.title");
                    String message = Messages.get("customer.account.balancecheck.ok");
                    InformationDialog.show(title, message);
                } else {
                    String message = Messages.format("customer.account.balancecheck.error",
                                                     NumberFormatter.formatCurrency(expected),
                                                     NumberFormatter.formatCurrency(actual));
                    confirmRegenerate(message, customer);
                }
            } catch (CustomerAccountRuleException exception) {
                String message = Messages.format("customer.account.balancecheck.acterror", exception.getMessage());
                confirmRegenerate(message, customer);
            }
        }
    }

    /**
     * Invoked when the adjustment editor is closed.
     *
     * @param editor the editor
     * @param isNew  determines if the object is a new instance
     */
    @Override
    protected void onEditCompleted(IMObjectEditor editor, boolean isNew) {
        if (editor.isSaved()) {
            onRefresh(getObject());
        }
    }

    /**
     * Invoked when the 'edit' button is pressed for a payment.
     */
    protected void administerPayment(FinancialAct payment) {
        if (getRules().hasClearedTillBalance(payment)) {
            // can't edit payments associated with a cleared till balance
            ErrorDialog.show(Messages.get("customer.account.payment.clearedtill.title"),
                             Messages.get("customer.account.payment.clearedtill.message"));
        } else {
            HelpContext edit = createEditTopic(payment);
            LayoutContext context = createLayoutContext(edit);
            AdminCustomerPaymentEditor editor = new AdminCustomerPaymentEditor(payment, null, context);
            editor.getComponent();

            EditDialog dialog = new AdminCustomerPaymentEditDialog(editor, context.getContext());
            dialog.addWindowPaneListener(new WindowPaneListener() {
                public void onClose(WindowPaneEvent event) {
                    onEditCompleted(editor, false);
                }
            });
            dialog.show();
        }
    }

    /**
     * Invoked to hide a transaction.
     */
    private void onHide() {
        getRules().setHidden(getObject(), true);
        onRefresh(getObject());
    }

    /**
     * Invoked to unhide a transaction.
     */
    private void onUnhide() {
        getRules().setHidden(getObject(), false);
        onRefresh(getObject());
    }

    /**
     * Confirms if regeneration of a customer account balance should proceed.
     *
     * @param message  the confirmation message
     * @param customer the customer
     */
    private void confirmRegenerate(String message, final Party customer) {
        String title = Messages.get("customer.account.balancecheck.title");
        HelpContext check = getHelpContext().subtopic("check");
        final ConfirmationDialog dialog = new ConfirmationDialog(title, message, check);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                regenerate(customer);
            }
        });
        dialog.show();
    }

    /**
     * Regenerates the balance for a customer.
     *
     * @param customer the customer
     */
    private void regenerate(Party customer) {
        try {
            IArchetypeService service = ServiceHelper.getArchetypeService(false);
            CustomerBalanceUpdater updater = ServiceHelper.getBean(CustomerBalanceUpdater.class);
            PlatformTransactionManager transactionManager = ServiceHelper.getBean(PlatformTransactionManager.class);
            AccountBalanceTool tool = new AccountBalanceTool(service, updater, transactionManager);
            tool.generate(customer);
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        } finally {
            onRefresh(getObject());
        }
    }
}