/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.table.DescriptorTableModel;

/**
 * Table model for <em>entity.documentTemplateEmail*</em> objects for administrators.
 *
 * @author Tim Anderson
 */
public class AdminEmailDocumentTemplateTableModel extends DescriptorTableModel<Entity> {

    /**
     * The nodes to display.
     */
    private final String[] nodes;

    /**
     * The available nodes to display.
     */
    private static final String[] NODES = {"id", "name", "description", "active", "revision"};

    /**
     * Constructs a {@link AdminDocumentTemplateTableModel}.
     *
     * @param context the layout context
     * @param query   the query. If both active and inactive results are being queried, an Active column will be
     *                displayed
     */
    public AdminEmailDocumentTemplateTableModel(Query<Entity> query, LayoutContext context) {
        super(query.getShortNames(), context);
        boolean active = (query.getActive() == BaseArchetypeConstraint.State.BOTH);
        this.nodes = (active) ? NODES : ArrayUtils.removeElement(NODES, "active");
        setTableColumnModel(createColumnModel(query.getShortNames(), context));
    }

    /**
     * Returns a list of node descriptor names to include in the table.
     * This implementation returns {@code null} to indicate that the
     * intersection should be calculated from all descriptors.
     *
     * @return the list of node descriptor names to include in the table
     */
    @Override
    protected String[] getNodeNames() {
        return nodes;
    }
}
