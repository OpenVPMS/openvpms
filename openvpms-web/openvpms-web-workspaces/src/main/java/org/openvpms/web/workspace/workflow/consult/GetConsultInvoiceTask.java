/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.consult;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.functor.ActComparator;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.GetInvoiceTask;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * Task to query the most recent <em>act.customerAccountChargesInvoice</em>, for the context customer.
 * If the context has an <em>act.patientClinicalEvent</em> then the invoice associated with this will be returned.
 * <p/>
 * The invoice will be added to the context.
 * <p/>
 * The invoice may be {@code POSTED} if no {@code IN_PROGRESS} or {@code COMPLETED} invoice is present.
 *
 * @author Tim Anderson
 */
public class GetConsultInvoiceTask extends GetInvoiceTask {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Constructs a {@link GetConsultInvoiceTask}.
     */
    public GetConsultInvoiceTask() {
        this.service = ServiceHelper.getArchetypeService();
    }

    /**
     * Executes the task.
     * <p/>
     * This adds the current invoice for a customer to the context, if any. It selects:
     * <ul>
     *     <li>the invoice linked to the event, if it is {@code IN_PROGRESS} or {@code COMPLETED}; else </li>
     *     <li>the most recent {@code IN_PROGRESS} or {@code COMPLETED} invoice for the customer; else</li>
     *     <li>the most recent POSTED invoice linked to the event</li>
     * </ul>
     *
     * @throws OpenVPMSException for any error
     */
    @Override
    public void execute(TaskContext context) {
        FinancialAct result;
        FinancialAct eventInvoice = getInvoiceForEvent(context);
        if (eventInvoice == null) {
            // no invoice linked to the event so fall back to a recent invoice for the customer
            result = getInvoice(context);
        } else if (ActStatus.POSTED.equals(eventInvoice.getStatus())) {
            // see if there is an IN_PROGRESS or COMPLETED invoice not linked to the event
            FinancialAct invoice = getInvoice(context);
            if (invoice != null) {
                result = invoice;
            } else {
                result = eventInvoice;
            }
        } else {
            result = eventInvoice;
        }
        if (result != null) {
            context.addObject(result);
        }
    }

    /**
     * Returns an invoice associated with the current event.
     * <p/>
     * This will select non-POSTED invoices in preference to POSTED ones, if available.
     *
     * @param context the context
     * @return an invoice linked to the event, or {@code null} if none is found.
     */
    protected FinancialAct getInvoiceForEvent(TaskContext context) {
        Act event = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
        FinancialAct invoice = null;
        if (event != null) {
            invoice = getInvoiceForEvent(event, context);
        }
        return invoice;
    }

    /**
     * Returns the first {@code IN_PROGRESS} or {@code COMPLETED} invoice associated with an event, if it belongs to
     * the current customer.
     *
     * @param event   the event
     * @param context the context
     * @param seen    the set of invoice references that have already been seen, to avoid redundant processing
     * @param posted  collects {@code POSTED} invoices linked to the event
     * @return the first {@code IN_PROGRESS} or {@code COMPLETED}invoice associated with the event, or {@code null}
     * if none is present
     */
    protected FinancialAct getInvoiceForEvent(Act event, TaskContext context, Set<Reference> seen,
                                              List<FinancialAct> posted) {
        FinancialAct invoice = null;
        Party customer = context.getCustomer();
        if (customer == null) {
            throw new IllegalStateException("Context has no customer");
        }
        Reference customerRef = customer.getObjectReference();
        IMObjectBean bean = service.getBean(event);
        for (ActRelationship relationship : bean.getValues("chargeItems", ActRelationship.class)) {
            FinancialAct item = service.get(relationship.getTarget(), FinancialAct.class);
            if (item != null) {
                IMObjectBean itemBean = service.getBean(item);
                Reference invoiceRef = itemBean.getSourceRef("invoice");
                if (invoiceRef != null && !seen.contains(invoiceRef)) {
                    FinancialAct act = (FinancialAct) service.get(invoiceRef);
                    if (act != null) {
                        seen.add(invoiceRef);
                        IMObjectBean invoiceBean = service.getBean(act);
                        if (Objects.equals(customerRef, invoiceBean.getTargetRef("customer"))) {
                            if (!ActStatus.POSTED.equals(act.getStatus())) {
                                invoice = act;
                                // if there are multiple non-POSTED invoices, which one to select? TODO
                                break;
                            } else {
                                posted.add(act);
                            }
                        }
                    }
                }
            }
        }
        return invoice;
    }

    /**
     * Selects the most recent invoice from a list.
     *
     * @param invoices the invoices
     * @return the most recent invoice, or {@code null} if the list is empty
     */
    protected FinancialAct selectMostRecent(List<FinancialAct> invoices) {
        FinancialAct result = null;
        if (!invoices.isEmpty()) {
            if (invoices.size() > 1) {
                invoices.sort(ActComparator.descending());
            }
            result = invoices.get(0);
        }
        return result;
    }

    /**
     * Returns the invoice associated with an event, if it belongs to the current customer.
     *
     * @param event   the event
     * @param context the context
     * @return the invoice associated with the event, or {@code null} if none is present. The invoice may be POSTED
     */
    private FinancialAct getInvoiceForEvent(Act event, TaskContext context) {
        Set<Reference> seen = new HashSet<>();
        List<FinancialAct> posted = new ArrayList<>();
        FinancialAct invoice = getInvoiceForEvent(event, context, seen, posted);
        return (invoice != null) ? invoice : selectMostRecent(posted);
    }

}
