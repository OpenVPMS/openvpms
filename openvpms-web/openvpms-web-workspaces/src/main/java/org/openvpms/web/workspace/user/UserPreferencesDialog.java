/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.user;

import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.prefs.PreferencesDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.security.firewall.FirewallService;
import org.openvpms.web.system.ServiceHelper;

/**
 * User preferences dialog.
 *
 * @author Tim Anderson
 */
public class UserPreferencesDialog extends PreferencesDialog {

    /**
     * The user.
     */
    private final User user;

    /**
     * Constructs a {@link UserPreferencesDialog}.
     *
     * @param user    the user to edit preferences for
     * @param source  if non-null, specifies the source to copy preferences from if the user has none
     * @param context the layout context
     */
    public UserPreferencesDialog(User user, Party source, Context context) {
        super(user, source, false, context);
        this.user = user;
        FirewallService service = ServiceHelper.getBean(FirewallService.class);
        if (service.isMultifactorAuthenticationEnabled(user)) {
            addButton("button.configureTOTP", this::configureTOTP);
        }
    }

    /**
     * Configures TOTP for the user.
     */
    private void configureTOTP() {
        PasswordDialog dialog = new PasswordDialog();
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                PopupDialog configurationDialog = new TOTPConfigurationDialog(user);
                configurationDialog.show();
            }
        });
        dialog.show();
    }
}
