/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.patient.reminder.PagedReminderItemIterator;
import org.openvpms.archetype.rules.patient.reminder.ReminderItemQueryFactory;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.processor.ProgressBarProcessor;
import org.openvpms.web.system.ServiceHelper;

/**
 * An {@link ProgressBarProcessor} for processing reminder items returned by an {@link ReminderItemQueryFactory}.
 *
 * @author Tim Anderson
 */
abstract class ReminderItemProgressBarProcessor extends ProgressBarProcessor<ObjectSet> {

    /**
     * Constructs a {@link ReminderItemProgressBarProcessor }.
     */
    public ReminderItemProgressBarProcessor(ReminderItemQueryFactory factory) {
        super(null);
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        ArchetypeQuery query = factory.createQuery();
        query.setMaxResults(0);
        query.setCountResults(true);
        int size = service.get(query).getTotalResults();
        setItems(new PagedReminderItemIterator(factory, 100, service), size);
    }

    /**
     * Processes an object.
     *
     * @param object the object to process
     */
    @Override
    protected void process(ObjectSet object) {
        Act item = (Act) object.get("item");
        Act reminder = (Act) object.get("reminder");
        process(item, reminder);
        processCompleted(object);
    }

    /**
     * Processes a reminder item.
     *
     * @param item     the reminder item
     * @param reminder the reminder
     */
    protected abstract void process(Act item, Act reminder);

    /**
     * Notifies the iterator that the iteration has updated.
     */
    protected void updated() {
        getIterator().updated();
    }

    /**
     * Returns the iterator.
     *
     * @return the iterator
     */
    @Override
    protected PagedReminderItemIterator getIterator() {
        return (PagedReminderItemIterator) super.getIterator();
    }
}
