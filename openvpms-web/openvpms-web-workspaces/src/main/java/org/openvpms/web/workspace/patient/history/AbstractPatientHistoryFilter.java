/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.act.ActHierarchyFilter;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 * Filters patient history.
 * <p>
 * This:
 * <ul>
 * <li>enables specific event items to by included by archetype</li>
 * <li>excludes charge items if they are linked to by an included medication</li>
 * <li>merges customer communications so they appear inline with a visit</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class AbstractPatientHistoryFilter extends ActHierarchyFilter<Act> {

    /**
     * The primary archetype (i.e. event/problem)
     */
    private final String primaryArchetype;

    /**
     * Any communications to merge with the history.
     */
    private final List<Act> communications;

    /**
     * Constructs a {@link AbstractPatientHistoryFilter}.
     *
     * @param primaryArchetype the primary archetype (i.e. event/problem)
     * @param archetypes       the archetypes to include
     * @param communications   any communications to merge with the history. If specified, the elements from this list
     *                         are removed if they are merged
     */
    public AbstractPatientHistoryFilter(String primaryArchetype, String[] archetypes, List<Act> communications) {
        super(archetypes, true);
        this.primaryArchetype = primaryArchetype;
        this.communications = communications;
    }

    /**
     * Constructs a {@link AbstractPatientHistoryFilter}.
     *
     * @param archetype      the primary archetype (i.e. event/problem)
     * @param communications any communications to merge with the history. If specified, the elements from this list
     *                       are removed if they are merged
     */
    public AbstractPatientHistoryFilter(String archetype, List<Act> communications) {
        super();
        this.primaryArchetype = archetype;
        this.communications = communications;
    }

    /**
     * Returns the immediate children of an act.
     * <p/>
     * This implementation includes any communications that fall in the time range of the specified act.
     *
     * @param act  the parent act
     * @param acts a cache of the visited acts, keyed on reference
     * @return the immediate children of {@code act}
     */
    @Override
    protected Set<Act> getChildren(Act act, Map<Reference, Act> acts) {
        Set<Act> children = super.getChildren(act, acts);
        if ((communications != null && !communications.isEmpty()) && act.isA(primaryArchetype)) {
            ListIterator<Act> iterator = communications.listIterator();
            Date startTime = act.getActivityStartTime();
            Date endTime = act.getActivityEndTime();
            while (iterator.hasNext()) {
                Act communication = iterator.next();
                if (DateRules.between(communication.getActivityStartTime(), startTime, endTime)) {
                    children.add(communication);
                    iterator.remove();
                }
            }
        }
        return children;
    }
}
