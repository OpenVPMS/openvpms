/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import nextapp.echo2.app.Button;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextSwitchListener;
import org.openvpms.web.component.app.DefaultContextSwitchListener;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.doc.DocumentGenerator;
import org.openvpms.web.component.im.doc.DocumentGeneratorFactory;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.act.AbstractActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.RelationshipHelper;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.IMObjectViewerDialog;
import org.openvpms.web.component.im.view.Selection;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workspace.AbstractCRUDWindow;
import org.openvpms.web.component.workspace.DocumentActActions;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.DialogManager;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.PatientMedicalRecordLinker;
import org.openvpms.web.workspace.patient.PatientRecordCRUDWindow;

import java.util.List;

/**
 * CRUD Window for patient history.
 *
 * @author Tim Anderson
 */
public class AbstractPatientHistoryCRUDWindow extends AbstractCRUDWindow<Act> implements PatientRecordCRUDWindow {

    /**
     * Determines the operations that can be performed on document acts.
     */
    private final DocumentActActions documentActions = new DocumentActActions();

    /**
     * The current <em>act.patientClinicalEvent</em>.
     */
    private Act event;

    /**
     * View button identifier.
     */
    private static final String VIEW_ID = "button.view2";

    /**
     * Mark reviewed button identifier.
     */
    private static final String MARK_REVIEWED_ID = "button.markReviewed";

    /**
     * Unmark reviewed button identifier.
     */
    private static final String UNMARK_REVIEWED_ID = "button.unmarkReviewed";

    /**
     * External edit button identifier.
     */
    private static final String EXTERNAL_EDIT_ID = "button.externaledit";

    /**
     * Constructs a {@link AbstractPatientHistoryCRUDWindow}.
     *
     * @param context the context
     * @param help    the help context
     */
    public AbstractPatientHistoryCRUDWindow(Archetypes<Act> archetypes, PatientHistoryActions actions, Context context,
                                            HelpContext help) {
        super(archetypes, actions, context, help);
    }

    /**
     * Views a record.
     */
    public void view() {
        Act act = IMObjectHelper.reload(getObject());
        if (act != null) {
            try {
                IMObjectViewerDialog dialog = new IMObjectViewerDialog(getContext(), getMailContext(),
                                                                       getHelpContext());
                if (!DialogManager.isWindowDisplayed()) {
                    // if this is the root window, support context switching
                    dialog.setContextSwitchListener(new ContextSwitchListener() {
                        @Override
                        public void switchTo(IMObject object) {
                            DefaultContextSwitchListener.INSTANCE.switchTo(object);
                        }

                        @Override
                        public void switchTo(String shortName) {
                            // no-op
                        }
                    });
                } else {
                    dialog.disableHyperlinks();
                }
                dialog.setObject(act);
                dialog.show();
            } catch (OpenVPMSException exception) {
                ErrorHelper.show(exception);
            }
        }
    }

    /**
     * Sets the current patient clinical event.
     *
     * @param event the current event
     */
    public void setEvent(Act event) {
        this.event = event;
    }

    /**
     * Returns the current patient clinical event.
     *
     * @return the current event. May be {@code null}
     */
    public Act getEvent() {
        return event;
    }

    /**
     * Creates a button to view an act.
     */
    public Button createViewButton() {
        return ButtonFactory.create(VIEW_ID, this::view);
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected PatientHistoryActions getActions() {
        return (PatientHistoryActions) super.getActions();
    }

    /**
     * Invoked when a new object has been created.
     *
     * @param object the new object
     */
    @Override
    protected void onCreated(final Act object) {
        if (object.isA(PatientArchetypes.PATIENT_MEDICATION)) {
            confirmCreation(object, "patient.record.create.medication.title",
                            "patient.record.create.medication.message", "newMedication");
        } else if (object.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            confirmCreation(object, "patient.record.create.investigation.title",
                            "patient.record.create.investigation.message", "newInvestigation");
        } else {
            super.onCreated(object);
        }
    }

    /**
     * Edits an object.
     *
     * @param object the object to edit
     * @param path   the selection path. May be {@code null}
     */
    @Override
    protected void edit(Act object, List<Selection> path) {
        if (!object.isNew() && object.isA(PatientArchetypes.PATIENT_MEDICATION)) {
            confirmEdit(object, path, "patient.record.edit.medication.title",
                        "patient.record.edit.medication.message");
        } else {
            super.edit(object, path);
        }
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(createViewButton(), 1); // add after new, before edit
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(VIEW_ID, enable);
        buttons.setEnabled(MARK_REVIEWED_ID, enable && getActions().canReview(getObject()));
        buttons.setEnabled(UNMARK_REVIEWED_ID, enable && getActions().canUnreview(getObject()));
        buttons.setEnabled(EXTERNAL_EDIT_ID, enable && documentActions.canExternalEdit(getObject()));
    }

    /**
     * Creates a button to mark an investigation as reviewed.
     *
     * @return a new button
     */
    protected Button createMarkReviewedButton() {
        return ButtonFactory.create(MARK_REVIEWED_ID, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                                             this::markReviewed, "investigation.reviewed.title"));
    }

    /**
     * Creates a button to unmark an investigation as reviewed.
     *
     * @return a new button
     */
    protected Button createUnmarkReviewedButton() {
        return ButtonFactory.create(UNMARK_REVIEWED_ID, action(InvestigationArchetypes.PATIENT_INVESTIGATION,
                                                               this::unmarkReviewed, "investigation.unreviewed.title"));
    }

    /**
     * Marks an investigation as reviewed.
     *
     * @param investigation the investigation
     */
    protected void markReviewed(Act investigation) {
        getActions().review(investigation);
        onRefresh(investigation);
    }

    /**
     * Unmarks an investigation as reviewed.
     *
     * @param investigation the investigation
     */
    protected void unmarkReviewed(Act investigation) {
        getActions().unreview(investigation);
        onRefresh(investigation);
    }

    /**
     * Creates a button to externally edit a a document.
     *
     * @return a new button
     */
    protected Button createExternalEditButton() {
        return ButtonFactory.create(EXTERNAL_EDIT_ID, this::onExternalEdit);
    }

    /**
     * Launches an external editor to edit a document, if external editing of the document is supported.
     */
    protected void onExternalEdit() {
        final DocumentAct act = (DocumentAct) IMObjectHelper.reload(getObject());
        if (act == null) {
            ErrorDialog.show(Messages.format("imobject.noexist", getArchetypes().getDisplayName()));
        } else if (act.getDocument() != null) {
            documentActions.externalEdit(act);
        } else {
            // the act has no document attached. Try and generate it first.
            DocumentGeneratorFactory factory = ServiceHelper.getBean(DocumentGeneratorFactory.class);
            DocumentGenerator generator = factory.create(act, getContext(), getHelpContext(),
                                                         new DocumentGenerator.AbstractListener() {
                                                             @Override
                                                             public void generated(Document document) {
                                                                 onSaved(act, false);
                                                                 if (documentActions.canExternalEdit(act)) {
                                                                     documentActions.externalEdit(act);
                                                                 }
                                                             }
                                                         });
            generator.generate(true, false);
        }
    }

    /**
     * Creates a {@link PatientMedicalRecordLinker} to link medical records.
     *
     * @param event    the patient clinical event. May be {@code null}
     * @param problem  the patient clinical problem. May be {@code null}
     * @param item     a medication or clinical note. May be {@code null}
     * @param addendum the addendum. May be {@code null}
     * @return a new medical record linker
     */
    protected PatientMedicalRecordLinker createMedicalRecordLinker(Act event, Act problem, Act item, Act addendum) {
        return new PatientMedicalRecordLinker(event, problem, item, addendum);
    }

    /**
     * Creates a {@link PatientMedicalRecordLinker} to link medical records.
     *
     * @param event the patient clinical event
     * @param item  the patient record item
     * @return a new medical record linker
     */
    protected PatientMedicalRecordLinker createMedicalRecordLinker(Act event, Act item) {
        return new PatientMedicalRecordLinker(event, item);
    }

    /**
     * Creates a new event, making it the current event.
     *
     * @return the event
     */
    protected Act createEvent() {
        Act event = (Act) IMObjectCreator.create(PatientArchetypes.CLINICAL_EVENT);
        if (event == null) {
            throw new IllegalStateException("Failed to create " + PatientArchetypes.CLINICAL_EVENT);
        }
        LayoutContext layoutContext = createLayoutContext(getHelpContext());
        IMObjectEditor editor = ServiceHelper.getBean(IMObjectEditorFactory.class).create(event, layoutContext);
        editor.getComponent();
        if (editor instanceof AbstractActEditor) {
            ((AbstractActEditor) editor).setStatus(ActStatus.COMPLETED);
        }
        editor.save();
        setEvent(event);
        return event;
    }

    /**
     * Helper to concatenate the short names for the target of a relationship with those supplied.
     *
     * @param relationship    the relationship archetype short name
     * @param includeAddendum if {@code true}, include the <em>act.patientClinicalAddendum</em> if present, otherwise
     *                        exclude it
     * @param shortNames      the short names to add
     * @return the archetype shortnames
     */
    protected String[] getShortNames(String relationship, boolean includeAddendum, String... shortNames) {
        String[] targets = RelationshipHelper.getTargetShortNames(ServiceHelper.getArchetypeService(), relationship);
        if (!includeAddendum) {
            targets = ArrayUtils.removeElement(targets, PatientArchetypes.CLINICAL_ADDENDUM);
        }
        return ArrayUtils.addAll(targets, shortNames);
    }

    /**
     * Confirms creation of an object.
     *
     * @param object  the object
     * @param title   the dialog title key
     * @param message the dialog message key
     * @param help    the help key
     */
    private void confirmCreation(final Act object, String title, String message, String help) {
        ConfirmationDialog dialog = new ConfirmationDialog(Messages.get(title), Messages.get(message),
                                                           getHelpContext().subtopic(help));
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                AbstractPatientHistoryCRUDWindow.super.onCreated(object);
            }
        });
        dialog.show();
    }

    /**
     * Confirms editing of an object.
     *
     * @param object  the object
     * @param path    the selection path. May be {@code null}
     * @param title   the dialog title key
     * @param message the dialog message key
     */
    private void confirmEdit(final Act object, List<Selection> path, String title, String message) {
        ConfirmationDialog dialog = new ConfirmationDialog(Messages.get(title), Messages.get(message));
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                AbstractPatientHistoryCRUDWindow.super.edit(object, path);
            }
        });
        dialog.show();
    }

}
