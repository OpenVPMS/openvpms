/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.cache;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionListener;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;

/**
 * Browser for appointment, task, calendar, roster and lookup caches.
 *
 * @author Tim Anderson
 */
public class CacheBrowser {

    /**
     * The memory use.
     */
    private final Memory memory;

    /**
     * The caches.
     */
    private final Caches caches;

    /**
     * The table of caches.
     */
    private final PagedIMTable<CacheState> table;

    /**
     * Constructs a {@link CacheBrowser}.
     */
    public CacheBrowser() {
        memory = new Memory();
        caches = new Caches();
        table = new PagedIMTable<>(new CacheTableModel());
        ListResultSet<CacheState> set = new ListResultSet<CacheState>(caches.getCaches(), 20) {
            @Override
            public void sort(SortConstraint[] sort) {
                super.sort(sort);
                IMObjectSorter.sort(getObjects(), sort, input -> input);
            }
        };
        table.setResultSet(set);
    }

    /**
     * Returns the selected cache.
     *
     * @return the selected cache, or {@code null} if none is selected
     */
    public CacheState getSelected() {
        return table.getSelected();
    }

    /**
     * Adds a listener to be notified when a cache is selected.
     *
     * @param listener the listener to add
     */
    public void addActionListener(ActionListener listener) {
        table.getTable().addActionListener(listener);
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        Button calc = ButtonFactory.create("button.calculatesize", this::calculateSizes);
        calc.setLayoutData(RowFactory.layout(Alignment.ALIGN_TOP));
        return ColumnFactory.create(Styles.WIDE_CELL_SPACING, memory.getComponent(),
                                    RowFactory.create(Styles.CELL_SPACING, table.getComponent(), calc));
    }

    /**
     * Returns the focus group.
     *
     * @return the focus group
     */
    public FocusGroup getFocusGroup() {
        return table.getFocusGroup();
    }

    /**
     * Refreshes statistics for each of the caches
     * and updates memory use.
     */
    public void refresh() {
        for (CacheState cache : caches.getCaches()) {
            cache.refreshStatistics();
        }
        updateTable();
        memory.refresh();
    }

    /**
     * Resets statistics for each of the caches.
     */
    public void resetStatistics() {
        for (CacheState cache : caches.getCaches()) {
            cache.resetStatistics();
        }
        refresh();
    }

    /**
     * Clears the selected cache.
     */
    public void clear() {
        CacheState cache = table.getSelected();
        if (cache != null) {
            cache.clear();
            cache.resetStatistics();
            cache.refreshStatistics();
            updateTable();
        }
    }

    /**
     * Calculates a suggested cache size.
     *
     * @param cache the cache
     * @return the suggested cache size
     */
    public long getSuggestedSize(CacheState cache) {
        return caches.getSuggestedSize(cache);
    }

    /**
     * Calculates the cache sizes.
     * <p/>
     * With large caches, this is an expensive operation, so it is only performed on demand.
     */
    private void calculateSizes() {
        for (CacheState cache : caches.getCaches()) {
            cache.refreshCacheSize();
        }
        updateTable();
        memory.refresh();
    }

    /**
     * Updates the table.
     */
    private void updateTable() {
        ResultSet<CacheState> set = table.getResultSet();
        set.sort(set.getSortConstraints());
        table.getModel().refresh();
    }

}