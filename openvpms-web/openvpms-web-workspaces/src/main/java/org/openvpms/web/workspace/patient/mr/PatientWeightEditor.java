/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientActEditor;
import org.openvpms.web.system.ServiceHelper;

/**
 * Editor for <em>act.patientWeight</em> acts.
 *
 * @author Tim Anderson
 */
public class PatientWeightEditor extends PatientActEditor {

    /**
     * Constructs a {@link PatientWeightEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public PatientWeightEditor(Act act, Act parent, LayoutContext context) {
        super(act, parent, context);
        if (act.isNew()) {
            // initialise the weight units to the practice defaults
            PracticeService service = ServiceHelper.getBean(PracticeService.class);
            WeightUnits units = service.getDefaultWeightUnits();
            getProperty("units").setValue(units.toString());
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new editor
     */
    @Override
    public IMObjectEditor newInstance() {
        return new PatientWeightEditor(reload(getObject()), (Act) getParent(), getLayoutContext());
    }
}
