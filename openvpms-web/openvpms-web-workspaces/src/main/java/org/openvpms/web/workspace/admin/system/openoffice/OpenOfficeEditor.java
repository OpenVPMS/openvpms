/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.openoffice;

import org.apache.commons.lang3.StringUtils;
import org.jodconverter.local.office.LocalOfficeUtils;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.report.openoffice.OpenOfficeConfig;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

import java.io.File;

import static org.openvpms.report.openoffice.OpenOfficeConfig.MAX_TASKS;
import static org.openvpms.report.openoffice.OpenOfficeConfig.PATH;
import static org.openvpms.report.openoffice.OpenOfficeConfig.PORTS;

/**
 * Editor for <em>entity.settingsGlobalOpenOffice</em>.
 *
 * @author Tim Anderson
 */
public class OpenOfficeEditor extends AbstractIMObjectEditor {

    /**
     * Constructs an {@link OpenOfficeEditor}.
     *
     * @param settings the settings
     * @param parent        the parent object. May be {@code null}
     * @param context  the layout context
     */
    public OpenOfficeEditor(Entity settings, IMObject parent, LayoutContext context) {
        super(settings, parent, context);
        Property home = getProperty(PATH);
        if (StringUtils.isEmpty(home.getString())) {
            home.setValue(OpenOfficeConfig.getDefaultPath());
        }
        Property ports = getProperty(PORTS);
        if (StringUtils.isEmpty(ports.getString())) {
            ports.setValue(OpenOfficeConfig.DEFAULT_PORT);
        }
        Property maxTasks = getProperty(MAX_TASKS);
        if (maxTasks.getValue() == null) {
            maxTasks.setValue(OpenOfficeConfig.DEFAULT_MAX_TASKS_PER_PROCESS);
        }
    }

    /**
     * Returns a title for the editor.
     *
     * @return a title for the editor
     */
    @Override
    public String getTitle() {
        return Messages.get("admin.system.openoffice.config");
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new OpenOfficeEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateHome(validator) && validatePorts(validator);
    }

    /**
     * Verify that the OpenOffice home directory is valid.
     *
     * @param validator the validator
     * @return {@code true} if  the directory is valid, otherwise {@code false}
     */
    protected boolean validateHome(Validator validator) {
        boolean result = false;
        Property home = getProperty(PATH);
        String string = home.getString();
        if (StringUtils.isEmpty(string)) {
            validator.add(home, Messages.format("admin.openoffice.home.invalid", home.getDisplayName()));
        } else {
            File file = new File(string);
            if (!file.isDirectory()) {
                validator.add(home, Messages.format("admin.openoffice.home.invalid", home.getDisplayName()));
            } else {
                File executable = LocalOfficeUtils.getOfficeExecutable(file);
                if (executable.isFile()) {
                    result = true;
                } else {
                    validator.add(home, Messages.format("admin.openoffice.home.noexecutable",
                                                        home.getDisplayName()));
                }
            }
        }
        return result;
    }

    /**
     * Verify that the ports property is a comma separated string of numbers.
     *
     * @param validator the ports
     * @return {@code true} if the ports are valid, otherwise {@code false}
     */
    protected boolean validatePorts(Validator validator) {
        boolean result = false;
        Property property = getProperty(PORTS);
        String string = property.getString();
        if (!StringUtils.isEmpty(string) && string.matches("^\\d+(\\s*,\\s*\\d+)*$")) {
            result = true;
        } else {
            validator.add(property, Messages.get("admin.openoffice.ports.invalid"));
        }
        return result;
    }
}
