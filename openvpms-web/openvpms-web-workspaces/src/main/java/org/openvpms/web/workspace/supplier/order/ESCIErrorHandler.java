/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.order;

import org.openvpms.esci.adapter.dispatcher.ESCIDispatcher;
import org.openvpms.esci.adapter.dispatcher.ErrorHandler;
import org.openvpms.web.component.error.ErrorFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper to collect errors from {@link ESCIDispatcher#dispatch(ErrorHandler)}.
 *
 * @author Tim Anderson
 */
public class ESCIErrorHandler implements ErrorHandler {

    /**
     * The errors.
     */
    private List<Throwable> errors = new ArrayList<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ESCIErrorHandler.class);

    /**
     * Determines if the dispatcher should terminate on error.
     *
     * @return {@code false}
     */
    @Override
    public boolean terminateOnError() {
        return false;
    }

    /**
     * Invoked when an error occurs.
     *
     * @param exception the error
     */
    @Override
    public void error(Throwable exception) {
        errors.add(exception);
        log.error(exception.getMessage(), exception);
    }

    /**
     * Returns the number of errors detected.
     *
     * @return the number of errors
     */
    public int getErrors() {
        return errors.size();
    }

    /**
     * Formats any errors encountered during inbox processing.
     *
     * @return the formatted errors
     */
    public String formatErrors() {
        StringBuilder builder = new StringBuilder();
        for (Throwable error : errors) {
            if (builder.length() != 0) {
                builder.append("\n");
            }
            builder.append(ErrorFormatter.format(error));
        }
        return builder.toString();
    }
}
