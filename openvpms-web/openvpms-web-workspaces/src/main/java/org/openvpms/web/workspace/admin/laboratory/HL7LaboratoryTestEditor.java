/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.EntityIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for <em>entity.laboratoryTestHL7</em>.
 *
 * @author Tim Anderson
 */
public class HL7LaboratoryTestEditor extends AbstractLaboratoryTestEditor {

    /**
     * Constructs an {@link HL7LaboratoryTestEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public HL7LaboratoryTestEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);

        IMObjectBean bean = getBean(object);
        EntityIdentity identity = bean.getObject("code", EntityIdentity.class);
        if (identity == null) {
            identity = create(LaboratoryArchetypes.TEST_CODE, EntityIdentity.class);
            bean.addValue("code", identity);
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new HL7LaboratoryTestEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Ensures that the investigation type is valid for the test.
     *
     * @param validator the validator
     * @return {@code true} if the investigation type is valid
     */
    @Override
    protected boolean validateInvestigationType(Validator validator) {
        Entity investigationType = getInvestigationType();
        if (investigationType != null) {
            IMObjectBean bean = getBean(investigationType);
            Reference laboratory = bean.getTargetRef("laboratory");
            if (laboratory == null || !laboratory.isA(LaboratoryArchetypes.HL7_LABORATORY,
                                                      LaboratoryArchetypes.HL7_LABORATORY_GROUP)) {
                validator.add(this, Messages.format("hl7test.invalidInvestigationType", investigationType.getName()));
            }
        }
        return validator.isValid();
    }
}
