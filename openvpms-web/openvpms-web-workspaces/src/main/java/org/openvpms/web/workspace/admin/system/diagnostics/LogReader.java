/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * Supports efficient pagination through text log files by tracking the offset of each page.
 *
 * @author Tim Anderson
 */
public class LogReader {

    public static class Page {

        /**
         * The lines.
         */
        private final List<String> lines;

        /**
         * The page state.
         */
        private final PageState state;

        /**
         * Constructs a {@link Page}.
         *
         * @param lines the lines
         * @param state the page state
         */
        Page(List<String> lines, PageState state) {
            this.lines = lines;
            this.state = state;
        }

        /**
         * Returns the page number.
         *
         * @return the page number, zero based
         */
        public int getPage() {
            return state.page;
        }

        /**
         * Indicates if the page was partially read.
         * <p/>
         * This could be that {@code lines != maxLines}, or the last line was incomplete.
         *
         * @return {@code true} if the page was partially read
         */
        public boolean isPartial() {
            return state.partial;
        }

        /**
         * Returns the first line number.
         *
         * @return the first line number (0-based)
         */
        public int getFirstLine() {
            return state.first;
        }

        /**
         * Returns the lines.
         *
         * @return the lines
         */
        public List<String> getLines() {
            return lines;
        }

        /**
         * Determine if the page contains the specified text.
         *
         * @param search the search criteria
         */
        public boolean contains(Search search) {
            for (String line : lines) {
                if (search.matches(line)) {
                    return true;
                }
            }
            return false;
        }
    }


    public static class Search {

        /**
         * The search criteria.
         */
        private final String criteria;

        /**
         * Determines if its a case-sensitive or case-insensitive search.
         */
        private final boolean caseSensitive;

        public Search(String criteria, boolean caseSensitive) {
            this.criteria = caseSensitive ? criteria : criteria.toLowerCase();
            this.caseSensitive = caseSensitive;
        }

        /**
         * Detemines if a line matches the search criteria.
         *
         * @param line the line
         * @return {@code true} if the line matches the search criteria, otherwise {@code false}
         */
        public boolean matches(String line) {
            return indexOf(line, 0) != -1;
        }

        /**
         * Returns the index of the search criteria in the line.
         *
         * @param line      the line
         * @param fromIndex the index to search from
         * @return the index, if there is a match, otherwise {@code -1}
         */
        public int indexOf(String line, int fromIndex) {
            if (!caseSensitive) {
                line = line.toLowerCase();
            }
            return line.indexOf(criteria, fromIndex);
        }

        /**
         * Returns the length of the search criteria.
         *
         * @return the length
         */
        public int length() {
            return criteria.length();
        }
    }


    /**
     * The file path.
     */
    private final String path;

    /**
     * The maximum no. of lines per page.
     */
    private final int maxLines;

    /**
     * Read buffer.
     */
    private final byte[] buffer = new byte[8192];

    /**
     * The pages.
     */
    private final ArrayList<PageState> pages = new ArrayList<>();

    /**
     * The file length.
     */
    private long length = -1;

    /**
     * Constructs a {@link LogReader}.
     *
     * @param file     the file to read
     * @param maxLines the maximum no. of lines per page
     */
    LogReader(File file, int maxLines) {
        this(file.getPath(), maxLines);
    }

    /**
     * Constructs a {@link LogReader}.
     *
     * @param path     the path of the file to read
     * @param maxLines the maximum no. of lines per page
     */
    LogReader(String path, int maxLines) {
        this.path = path;
        this.maxLines = maxLines;
    }

    /**
     * Returns the log file path.
     *
     * @return the log file path
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns the file length.
     *
     * @return the file length
     * @throws IOException for any I/O error
     */
    public long getLength() throws IOException {
        long result;
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            result = checkLength(file);
        }
        return result;
    }

    /**
     * Reads a page.
     * <p/>
     * If the file has been truncated since the last read, {@code null} will be returned, and pagination should
     * be restarted.
     * <p/>
     * Truncation detection is determined by comparing the file size from the last read with the current size.<br/>
     * It will not detect if the file has been rewritten.
     *
     * @param page the page, zero-based
     * @return the corresponding page, or {@code null} if none is found or the file has been truncated
     * @throws IOException for any I/O error
     */
    public Page read(int page) throws IOException {
        Page result = null;
        Page current = null;
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            boolean done = false;
            while (!done) {
                if (page < pages.size()) {
                    current = reread(file, page, pages.get(page));
                } else {
                    current = readNext(file);
                }
                if (current == null || current.getPage() == page || current.isPartial()) {
                    done = true;
                }
            }
        }
        if (current != null && current.getPage() == page) {
            result = current;
        }
        return result;
    }

    /**
     * Returns the last page.
     *
     * @return the last page, or {@code null} if the file has been truncated since it was last read
     * @throws IOException for any I/O error
     */
    public Page readLast() throws IOException {
        Page result = null;
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            Page page;
            while ((page = readNext(file)) != null) {
                result = page;
                if (result.isPartial()) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Re-reads a page.
     *
     * @param file  the file
     * @param page  the page to re-read
     * @param state the state
     * @return the page, or {@code null} if the offset could not be seeked to
     * @throws IOException for any I/O error
     */
    private Page reread(RandomAccessFile file, int page, PageState state) throws IOException {
        return readFromOffset(file, page, state.startOffset, state.first);
    }

    /**
     * Reads the next unread page.
     *
     * @param file the file
     * @return the next page, or {@code null} if it doesn't exist
     * @throws IOException for any I/O error
     */
    private Page readNext(RandomAccessFile file) throws IOException {
        Page result;
        if (pages.isEmpty()) {
            result = readAtCurrentPosition(file, 0, 0);
        } else {
            int lastPage = pages.size() - 1;
            PageState state = pages.get(lastPage);
            if (state.partial) {
                // last page was only partially read, so re-read
                Page last = reread(file, lastPage, state);
                if (last == null) {
                    // truncated
                    result = null;
                } else if (last.isPartial()) {
                    // no additional page
                    result = last;
                } else {
                    result = readNext(file, last.state);
                }
            } else {
                result = readNext(file, state);
            }
        }
        return result;
    }

    /**
     * Reads the page from the specified offset.
     *
     * @param file   the file
     * @param page   the page being read
     * @param offset the offset
     * @param first  the first line
     * @return the page, or {@code null} if the offset could not be seeked to
     * @throws IOException for any I/O error
     */
    private Page readFromOffset(RandomAccessFile file, int page, long offset, int first) throws IOException {
        Page result = null;
        if (seek(file, offset)) {
            result = readAtCurrentPosition(file, page, first);
        }
        return result;
    }

    /**
     * Reads the page from the current position in the file.
     *
     * @param file  the file
     * @param page  the page being read
     * @param first the first line
     * @return the page
     * @throws IOException for any I/O error
     */
    private Page readAtCurrentPosition(RandomAccessFile file, int page, int first) throws IOException {
        PageState state;
        List<String> lines = new ArrayList<>();
        Reader reader = new Reader(file, buffer);
        long startOffset = reader.getFilePointer();
        long endOffset = startOffset;
        for (int i = 0; i < maxLines && !reader.eof(); ++i) {
            String line = reader.readLine();
            if (line != null) {
                lines.add(line);
            }
            endOffset = reader.getFilePointer();
        }
        boolean partial = reader.eof() || lines.size() != maxLines;
        state = new PageState(page, startOffset, endOffset, first, lines.size(), partial);
        if (page < pages.size()) {
            pages.set(page, state);
        } else {
            pages.add(state);
        }
        return new Page(lines, state);
    }

    /**
     * Reads the next page.
     *
     * @param file  the file
     * @param state the current page
     * @return the next page, or {@code null} if the offset could not be seeked to
     * @throws IOException for any I/O error
     */
    private Page readNext(RandomAccessFile file, PageState state) throws IOException {
        return readFromOffset(file, state.page + 1, state.endOffset, state.first + state.count);
    }

    /**
     * Seeks to an offset in a file.
     *
     * @param file   the file
     * @param offset the offset to move to
     * @return {@code true} if the seek was successful
     * @throws IOException for any I/O error
     */
    private boolean seek(RandomAccessFile file, long offset) throws IOException {
        boolean result = false;
        long newLength = checkLength(file);
        if (newLength > offset) {
            file.seek(offset);
            result = true;
        }
        return result;
    }

    /**
     * Checks the file length.
     * <p/>
     * If the new length is less than that seen previously, the page state is cleared as the file has been truncated.
     *
     * @param file the file
     * @return the file length
     * @throws IOException for any I/O error
     */
    private long checkLength(RandomAccessFile file) throws IOException {
        long newLength = file.length();
        if (length != -1 && newLength < length) {
            pages.clear();
        }
        length = newLength;
        return length;
    }

    /**
     * Provides buffered access to a {@link RandomAccessFile}.
     */
    private static class Reader {

        private final RandomAccessFile file;

        private final byte[] buffer;

        private int count;

        private int index;

        private long pointer;

        private boolean eof;

        Reader(RandomAccessFile file, byte[] buffer) throws IOException {
            this.file = file;
            this.buffer = buffer;
            pointer = file.getFilePointer();
        }

        /**
         * Reads a line from a file.
         * This strips any '\r\n' or '\n'
         *
         * @return the line, or {@code null} if end-of-file was reached
         * @throws IOException for any I/O error
         */
        String readLine() throws IOException {
            StringBuilder line = new StringBuilder();
            int ch = -1;
            eof = false;
            boolean eol = false;
            while (!eol) {
                ch = readNext();
                switch (ch) {
                    case -1:
                    case '\n':
                        eol = true;
                        break;
                    case '\r':
                        eol = true;
                        long pos = pointer;
                        int currentIndex = index;
                        if ((readNext()) != '\n') {
                            pointer = pos;
                            if (currentIndex > index) {
                                // new buffer was read, so discard this one on next read
                                index = count;
                            } else {
                                index = currentIndex;
                            }
                        }
                        break;
                    default:
                        line.append((char) ch);
                        break;
                }
            }
            eof = (ch == -1);
            return eof && line.length() == 0 ? null : line.toString();
        }

        boolean eof() {
            return eof;
        }

        long getFilePointer() {
            return pointer;
        }

        private int readNext() throws IOException {
            int ch;
            if (index == count) {
                index = 0;
                count = file.read(buffer);
                if (count <= 0) {
                    ch = -1;
                } else {
                    ch = buffer[index] & 0xFF;
                }
            } else {
                ch = buffer[index] & 0xFF;
            }
            if (ch != -1) {
                pointer++;
                index++;
            }
            return ch;
        }

    }

    private static final class PageState {

        private final int page;

        private final long startOffset;

        private final long endOffset;

        private final int first;

        private final int count;

        private final boolean partial;

        PageState(int page, long startOffset, long endOffset, int first, int count, boolean partial) {
            this.page = page;
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.first = first;
            this.count = count;
            this.partial = partial;
        }

    }
}
