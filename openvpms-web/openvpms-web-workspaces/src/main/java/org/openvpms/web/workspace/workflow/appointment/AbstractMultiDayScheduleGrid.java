/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.component.system.common.cache.SoftRefIMObjectCache;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Multiple-day schedule grid.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMultiDayScheduleGrid extends AbstractScheduleEventGrid {

    /**
     * The number of days to display.
     */
    private final int days;

    private static final int MINUTES_PER_DAY = 24 * 60;

    /**
     * Constructs an {@link AbstractMultiDayScheduleGrid}.
     *
     * @param scheduleView the schedule view
     * @param date         the date
     * @param days         the number of days to display
     * @param events       the events
     * @param rules        the appointment rules
     */
    public AbstractMultiDayScheduleGrid(Entity scheduleView, Date date, int days,
                                        Map<Entity, ScheduleEvents> events, AppointmentRules rules) {
        super(scheduleView, date, DateRules.getDate(date, days - 1, DateUnits.DAYS), rules);
        this.days = days;
        setEvents(events);
    }

    /**
     * Returns the no. of slots in the grid.
     *
     * @return the no. of slots
     */
    @Override
    public int getSlots() {
        return days;
    }

    /**
     * Returns the size of each slot, in minutes.
     *
     * @return the slot size, in minutes
     */
    @Override
    public int getSlotSize() {
        return MINUTES_PER_DAY;
    }

    /**
     * Returns the event for the specified schedule and slot.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the corresponding event, or {@code null} if none is found
     */
    @Override
    public PropertySet getEvent(Schedule schedule, int slot) {
        return getEvent(schedule, slot, true);
    }

    /**
     * Returns the event for the specified schedule and slot.
     *
     * @param schedule              the schedule
     * @param slot                  the slot
     * @param includeBlockingEvents if {@code true}, look for blocking events if there are no non-blocking events
     * @return the corresponding event, or {@code null} if none is found
     */
    public PropertySet getEvent(Schedule schedule, int slot, boolean includeBlockingEvents) {
        PropertySet result = null;
        if (slot < getSlots()) {
            Date time = getStartTime(schedule, slot);
            result = schedule.getEvent(time, MINUTES_PER_DAY, includeBlockingEvents);
            if (result == null) {
                result = schedule.getIntersectingEvent(time, time, MINUTES_PER_DAY, includeBlockingEvents);
            }
        }
        return result;
    }

    /**
     * Returns the time that the specified slot starts at.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the start time of the specified slot. May be {@code null}
     */
    @Override
    public Date getStartTime(Schedule schedule, int slot) {
        Date date = DateRules.getDate(getStartDate(), slot, DateUnits.DAYS);
        return DateRules.getDate(date, schedule.getStartMins(), DateUnits.MINUTES);
    }

    /**
     * Returns the time that the specified slot ends at.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the end time of the specified slot. May be {@code null}
     */
    @Override
    public Date getEndTime(Schedule schedule, int slot) {
        Date date = DateRules.getDate(getStartDate(), slot, DateUnits.DAYS);
        if (schedule.getEndMins() == 24 * 60) {
            // avoids daylight savings issues.
            return DateRules.getDate(date, 1, DateUnits.DAYS);
        }
        return DateRules.getDate(date, schedule.getEndMins(), DateUnits.MINUTES);
    }

    /**
     * Determines the availability of a slot for the specified schedule.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the availability of the schedule
     */
    @Override
    public Availability getAvailability(Schedule schedule, int slot) {
        PropertySet event = getEvent(schedule, slot, false);
        return (event != null) ? Availability.BUSY : Availability.FREE;
    }

    /**
     * Determines how many slots are unavailable from the specified slot, for
     * a schedule.
     *
     * @param schedule the schedule
     * @param slot     the starting slot
     * @return the no. of concurrent slots that are unavailable
     */
    @Override
    public int getUnavailableSlots(Schedule schedule, int slot) {
        return 0;
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time the time
     * @return the slot, or {@code -1} if the time doesn't intersect any slot
     */
    @Override
    public int getSlot(Date time) {
        return Days.daysBetween(new DateTime(getStartDate()), new DateTime(time)).getDays();
    }

    /**
     * Returns the no. of slots an event occupies, from the specified slot.
     * <p>
     * If the event begins prior to the slot, the remaining slots will be returned.
     *
     * @param event the event
     * @param slot  the starting slot
     * @return the no. of slots that the event occupies
     */
    public int getSlots(PropertySet event, int slot) {
        DateTime endTime = new DateTime(event.getDate(ScheduleEvent.ACT_END_TIME));
        int endSlot = Days.daysBetween(new DateTime(getStartDate()), endTime).getDays();
        if (endTime.getHourOfDay() > 0 || endTime.getMinuteOfHour() > 0) {
            ++endSlot;
        }
        return endSlot - slot;
    }

    /**
     * Returns the date of a slot.
     *
     * @param slot the slot
     * @return the start time of the specified slot
     */
    public Date getDate(int slot) {
        return DateRules.getDate(getStartDate(), slot, DateUnits.DAYS);
    }

    /**
     * Sets the events.
     *
     * @param events the events, keyed on schedule
     */
    protected void setEvents(Map<Entity, ScheduleEvents> events) {
        List<Schedule> schedules = new ArrayList<>();
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        IMObjectCache cageTypes = new SoftRefIMObjectCache(service);

        int index = -1;
        Entity last = null;
        AppointmentRules rules = getAppointmentRules();
        for (Entity entity : events.keySet()) {
            IMObjectBean bean = service.getBean(entity);
            Entity cageType = (Entity) cageTypes.get(bean.getTargetRef("cageType"));
            Schedule schedule = new Schedule(entity, cageType, 0, 24 * 60, 24 * 60, Collections.emptyList(), rules);
            if (!Objects.equals(last, entity)) {
                index++;
            }
            last = entity;
            schedule.setRenderEven(index % 2 == 0);
            schedules.add(schedule);
        }
        setSchedules(schedules);

        // add the events
        for (Map.Entry<Entity, ScheduleEvents> entry : events.entrySet()) {
            Entity schedule = entry.getKey();
            List<PropertySet> sets = entry.getValue().getEvents();

            for (PropertySet set : sets) {
                addEvent(schedule, set);
            }
        }
    }

    /**
     * Adds an event or blocking event.
     * <p>
     * If the event is not a blocking event, and the corresponding Schedule already has an event that intersects
     * it, a new Schedule will be created with the same start and end times, and the event added to that.
     *
     * @param schedule the schedule to add the appointment to
     * @param event    the event
     */
    @Override
    protected void addEvent(Entity schedule, PropertySet event) {
        int index = -1;
        boolean found = false;
        Schedule row = null;
        Schedule match = null;

        // try and find a corresponding Schedule. If the event is non-blocking, try and find one that has no event
        // that intersects the supplied one.
        List<Schedule> rows = getSchedules();
        Reference eventRef = event.getReference(ScheduleEvent.ACT_REFERENCE);
        for (int i = 0; i < rows.size(); ++i) {
            row = rows.get(i);
            if (row.getSchedule().equals(schedule)) {
                if (row.indexOf(eventRef) != -1) {
                    // multi-day appointments are duplicated on subsequent days, so skip it if it has already been added
                    return;
                }
                if (row.hasIntersectingEvent(event, false, MINUTES_PER_DAY)) {
                    match = row;
                    index = i;
                } else {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            // event intersects an existing one, so create a new Schedule. Any blocking event will be shared.
            row = new Schedule(match, false, getAppointmentRules());
            rows.add(index + 1, row);
        }
        row.addEvent(event);
    }
}
