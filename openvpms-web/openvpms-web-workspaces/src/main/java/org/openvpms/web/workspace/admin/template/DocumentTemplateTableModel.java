/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Query;

/**
 * Table model for <em>entity.documentTemplate</em> objects.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateTableModel extends AbstractDocumentTemplateTableModel {

    /**
     * The nodes to display.
     */
    private static final String[] NODES = {"id", "name", "description", "type", "reportType", "active"};

    /**
     * Constructs a {@link DocumentTemplateTableModel}.
     *
     * @param context the layout context
     */
    public DocumentTemplateTableModel(LayoutContext context) {
        this(context, true);
    }

    /**
     * Constructs a {@link DocumentTemplateTableModel}.
     *
     * @param context the layout context
     * @param query   the query. If both active and inactive results are being queried, an Active column will be
     *                displayed
     */
    public DocumentTemplateTableModel(Query<Entity> query, LayoutContext context) {
        this(context, query.getActive() == BaseArchetypeConstraint.State.BOTH);
    }

    /**
     * Constructs a {@link DocumentTemplateTableModel}.
     *
     * @param context the layout context
     * @param active  determines if the active column should be displayed
     */
    protected DocumentTemplateTableModel(LayoutContext context, boolean active) {
        super(NODES, context, active);
    }
}
