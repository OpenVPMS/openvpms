/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.mapping.service.MappingProvider;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.plugin.AbstractMappingPluginEditor;

/**
 * An editor for <em>entity.laboratoryService*</em> plugin configurations.
 * <p/>
 * This supports plugins that implement the {@link MappingProvider} interface; a mapping tab will
 * be added when the laboratory service is configured.
 *
 * @author Tim Anderson
 */
public class LaboratoryServiceEditor extends AbstractMappingPluginEditor {

    /**
     * Constructs a {@link LaboratoryServiceEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public LaboratoryServiceEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance of the editor
     */
    @Override
    public IMObjectEditor newInstance() {
        return new LaboratoryServiceEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Returns the service associated with a plugin configuration.
     *
     * @param config the configuration
     * @return the corresponding service, or {@code null} if none is found
     */
    @Override
    protected Object getPluginService(IMObject config) {
        return ServiceHelper.getBean(LaboratoryServices.class).getService((Entity) config);
    }

}
