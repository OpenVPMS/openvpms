/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.sms;

import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.im.sms.SMSReplyUpdater;
import org.openvpms.web.component.im.sms.SMSViewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Viewer for appointment SMS.
 *
 * @author Tim Anderson
 */
public class AppointmentSMSViewer extends SMSViewer {

    /**
     * The appointment.
     */
    private final Act appointment;

    /**
     * The action factory.
     */
    @Autowired
    private ActionFactory actionFactory;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AppointmentSMSViewer.class);

    /**
     * The SMS status node.
     */
    private static final String SMS_STATUS = "smsStatus";

    /**
     * Constructs an {@link AppointmentSMSViewer}.
     *
     * @param appointment   the appointment
     * @param sms           the SMS to display
     * @param service       the archetype service
     * @param replyUpdater  the reply updater
     * @param actionFactory the action factory
     */
    public AppointmentSMSViewer(Act appointment, List<Act> sms, ArchetypeService service, SMSReplyUpdater replyUpdater,
                                ActionFactory actionFactory) {
        super(sms, service, replyUpdater);
        this.appointment = appointment;
        this.actionFactory = actionFactory;
    }

    /**
     * Marks all replies as being read, and updates the appointment SMS status.
     */
    @Override
    public void markAsRead() {
        super.markAsRead();
        actionFactory.newAction()
                .backgroundOnly()
                .withObject(appointment)
                .useLatestInstanceOnRetry()
                .skipIfMissing()
                .asBean(bean -> {
                    if (!AppointmentStatus.SMS_READ.equals(bean.getString(SMS_STATUS))) {
                        bean.setValue(SMS_STATUS, AppointmentStatus.SMS_READ);
                        bean.save();
                    }
                })
                .onFailure(status -> log.warn("Failed to update appointment SMS status: {}", status.getReason()))
                .run();
    }
}
