/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.PropertySetBuilder;
import org.openvpms.web.workspace.workflow.worklist.FollowUpTaskEditor;
import org.openvpms.web.workspace.workflow.worklist.TaskActEditor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * An editor for <em>actRelationship.invoiceItemTask</em> collections.
 *
 * @author Tim Anderson
 */
class TaskActRelationshipCollectionEditor extends ActRelationshipCollectionEditor {

    public static class TaskType {

        /**
         * The task type.
         */
        private final Entity taskType;

        /**
         * The product-task relationship.
         */
        private final IMObjectBean relationship;

        /**
         * Constructs a {@link TaskType}.
         *
         * @param taskType     the task type
         * @param relationship the product-task relationship
         */
        private TaskType(Entity taskType, IMObjectBean relationship) {
            this.relationship = relationship;
            this.taskType = taskType;
        }

        /**
         * Returns the task type.
         *
         * @return the task type
         */
        public Entity getType() {
            return taskType;
        }

        /**
         * Determines if the task editor should be displayed.
         *
         * @return {@code true} if the task editor should be displayed, otherwise {@code false}
         */
        public boolean isInteractive() {
            return relationship.getBoolean("interactive");
        }

        /**
         * Determines if a follow-up work list should be used.
         *
         * @return {@code true} if a follow-up work list should be used, {@code false} if a specific work list should
         * be used
         */
        public boolean useFollowup() {
            return getWorklist() == null;
        }

        /**
         * Returns the work list to use.
         *
         * @return the work list, or {@code null} if a follow-up work list should be used
         */
        public Reference getWorklist() {
            return relationship.getReference("worklist");
        }

        /**
         * Returns the period from the invoice item time to start the task.
         *
         * @return the period
         */
        public Period getStart() {
            Period period = PeriodHelper.getPeriod(relationship, "start", "startUnits");
            return period != null ? period : Period.ZERO;
        }
    }

    /**
     * Constructs a {@link TaskActRelationshipCollectionEditor}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public TaskActRelationshipCollectionEditor(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
        setExcludeDefaultValueObject(false);
    }

    /**
     * Returns the active task types for a product.
     *
     * @param product the product
     * @return the task types
     */
    public List<TaskType> getTasksTypes(Product product) {
        List<TaskType> result = new ArrayList<>();
        IMObjectBean bean = getBean(product);
        if (bean.hasNode("tasks")) {
            RelatedIMObjects<Entity, Relationship> tasks = bean.getRelated("tasks", Entity.class);
            for (ObjectRelationship<Entity, Relationship> item : tasks.active().getObjectRelationships()) {
                result.add(new TaskType(item.getObject(), getBean(item.getRelationship())));
            }
        }
        return result;
    }

    /**
     * Creates an act and editor for a task type.
     *
     * @param taskType  the task type
     * @param patient   the patient
     * @param startTime the time the task starts. If the task type has a non-zero period, this will be added
     * @return a new editor, or {@code null} if one couldn't be created
     */
    public TaskActEditor createEditor(TaskType taskType, Party patient, Date startTime) {
        TaskActEditor editor = null;
        Act act = (Act) create();
        if (act != null) {
            if (taskType.useFollowup()) {
                List<Entity> worklists = FollowUpTaskEditor.getWorkLists(getContext().getContext(), taskType.getType());
                editor = new FollowUpTaskEditor(act, worklists, createLayoutContext(act));
            } else {
                Entity worklist = (Entity) getObject(taskType.getWorklist());
                if (worklist != null && worklist.isActive()) {
                    editor = (TaskActEditor) createEditor(act, createLayoutContext(act));
                    editor.setWorkList(worklist);
                }
            }
            if (editor != null) {
                editor.setPatient(patient);
                editor.setTaskType(taskType.getType());
                editor.setStartTime(DateRules.plus(startTime, taskType.getStart()));
            }
        }
        if (editor != null) {
            addEdited(editor);
        }
        return editor;
    }

    /**
     * Updates the start times for tasks.
     *
     * @param startTime the time the task starts. If the task type has a non-zero period, this will be added
     * @param product   the product
     */
    public void updateStartTimes(Date startTime, Product product) {
        for (Act act : getCurrentActs()) {
            TaskActEditor editor = (TaskActEditor) getEditor(act);
            Period period = getTaskTypePeriod(product, editor.getTaskType());
            if (period != null) {
                editor.setStartTime(DateRules.plus(startTime, period));
            }
        }
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit
     * @param context the layout context
     * @return an editor to edit {@code object}
     */
    @Override
    public IMObjectEditor createEditor(IMObject object, LayoutContext context) {
        return new ChargeTaskEditor((Act) object, getObject(), context);
    }

    /**
     * Determines if items can be added to the collection.
     *
     * @return {@code false} - tasks cannot be directly added by users
     */
    @Override
    protected boolean canAdd() {
        return false;
    }

    /**
     * Invoked when the collection or an editor changes. Resets the cached valid status and notifies registered
     * listeners.
     *
     * @param modifiable the modifiable to pass to the listeners
     */
    @Override
    protected void onModified(Modifiable modifiable) {
        super.onModified(modifiable);
        refresh();  // force a refresh of the table
    }

    /**
     * Returns the period for a task type.
     *
     * @param product  the product
     * @param taskType the task type
     * @return the period, or {@code null} if it cannot be determined
     */
    private Period getTaskTypePeriod(Product product, Entity taskType) {
        Period result = null;
        IMObjectBean bean = getBean(product);
        if (bean.hasNode("tasks")) {
            Relationship relationship = bean.getValue("tasks", Relationship.class, Predicates.targetEquals(taskType));
            if (relationship != null) {
                TaskType type = new TaskType(taskType, getBean(relationship));
                result = type.getStart();
            }
        }
        return result;
    }

    /**
     * Creates a new layout context for a task.
     *
     * @param task the task
     * @return a new layout context
     */
    private LayoutContext createLayoutContext(Act task) {
        LayoutContext context = getContext();
        return new DefaultLayoutContext(context, context.getHelpContext().topic(task, "edit"));
    }

    private static class ChargeTaskEditor extends TaskActEditor {

        /**
         * Constructs a {@link ChargeTaskEditor}.
         *
         * @param act     the act to edit
         * @param parent  the parent object. May be {@code null}
         * @param context the layout context
         */
        public ChargeTaskEditor(Act act, IMObject parent, LayoutContext context) {
            super(act, parent, context);
        }

        /**
         * Creates a new instance of the editor, with the latest instance of the object to edit.
         *
         * @return {@code null}. Not implemented as it is not a top-level object
         */
        @Override
        public IMObjectEditor newInstance() {
            return null;
        }

        /**
         * Sets the task type.
         *
         * @param taskType the task type
         */
        @Override
        public void setTaskType(Entity taskType) {
            super.setTaskType(taskType);
            onLayout(); // need to refresh as the task type is read-only
        }

        /**
         * Creates the property set.
         *
         * @param object    the object being edited
         * @param archetype the object archetype
         * @param variables the variables for macro expansion. May be {@code null}
         * @return the property set
         */
        @Override
        protected PropertySet createPropertySet(IMObject object, ArchetypeDescriptor archetype, Variables variables) {
            return new PropertySetBuilder(object, archetype, variables)
                    .setReadOnly(TASK_TYPE)
                    .setReadOnly(WORK_LIST)
                    .setReadOnly(CUSTOMER)
                    .setReadOnly(PATIENT)
                    .build();
        }

        /**
         * Creates the layout strategy.
         *
         * @return a new layout strategy
         */
        @Override
        protected IMObjectLayoutStrategy createLayoutStrategy() {
            return new LayoutStrategy() {
                /**
                 * Apply the layout strategy.
                 * <p>
                 * This renders an object in a {@code Component}, using a factory to create the child components.
                 *
                 * @param object     the object to apply
                 * @param properties the object's properties
                 * @param parent     the parent object. May be {@code null}
                 * @param context    the layout context
                 * @return the component containing the rendered {@code object}
                 */
                @Override
                public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
                    setArchetypeNodes(new ArchetypeNodes().simple(WORK_LIST));
                    return super.apply(object, properties, parent, context);
                }
            };
        }
    }
}