/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.AbstractRemoveConfirmationHandler;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.RemoveConfirmationHandler;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.PriceActItemEditor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * Implementation of {@link RemoveConfirmationHandler} for {@link AbstractChargeItemRelationshipCollectionEditor}.
 * <p/>
 * If the charge item being removed has minimum quantities, this displays a confirmation. If not, it falls back
 * to the default remove confirmation.
 *
 * @author Tim Anderson
 */
public abstract class ChargeRemoveConfirmationHandler extends AbstractRemoveConfirmationHandler {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * Constructs a {@link ChargeRemoveConfirmationHandler}.
     *
     * @param context the context
     * @param help    the help context
     */
    public ChargeRemoveConfirmationHandler(Context context, HelpContext help) {
        this.context = context;
        this.help = help;
    }

    /**
     * Confirms removal of an object from a collection.
     * <p/>
     * If approved, it performs the removal.
     *
     * @param object     the object to remove
     * @param collection the collection to remove the object from, if approved
     */
    @Override
    public void remove(IMObject object, IMObjectCollectionEditor collection) {
        AbstractChargeItemRelationshipCollectionEditor chargeCollection
                = (AbstractChargeItemRelationshipCollectionEditor) collection;
        ChargeRemovalStates states = getRemovalStates(Collections.singletonList(object), chargeCollection);
        if (states.prompt()) {
            promptRemoval(states, chargeCollection);
        } else {
            super.remove(object, collection);
        }
    }

    /**
     * Removes a collection of items.
     *
     * @param objects    the objects to remove
     * @param collection the collection the objects belong to
     */
    public void remove(List<IMObject> objects, AbstractChargeItemRelationshipCollectionEditor collection) {
        if (objects.size() == 1) {
            remove(objects.get(0), collection);
        } else if (objects.size() > 1) {
            ChargeRemovalStates states = getRemovalStates(objects, collection);
            if (states.prompt()) {
                promptRemoval(states, collection);
            } else {
                String displayName = collection.getProperty().getDisplayName();
                String title = Messages.format("imobject.collection.deletes.title", displayName);
                String message = Messages.format("imobject.collection.deletes.message", objects.size(), displayName);
                ConfirmationDialog dialog = new ConfirmationDialog(title, message, ConfirmationDialog.YES_NO);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onYes() {
                        removeAll(objects, collection);
                    }

                    @Override
                    public void onNo() {
                        cancelRemove(collection);
                    }
                });
                dialog.show();
            }
        }
    }

    /**
     * Returns the removal states for a list of objects.
     *
     * @param objects    the objects to remove
     * @param collection the collection to remove them from
     * @return the removal states
     */
    protected ChargeRemovalStates getRemovalStates(List<IMObject> objects,
                                                   AbstractChargeItemRelationshipCollectionEditor collection) {
        ChargeRemovalStates result = new ChargeRemovalStates();
        for (IMObject object : objects) {
            checkRemovalState((Act) object, collection, result);
        }
        return result;
    }

    /**
     * Checks the removal state for an object.
     *
     * @param object     the object being removed
     * @param collection the collection the object belongs to
     * @param states     all removal states
     * @return {@code true} if the object can be removed
     */
    protected boolean checkRemovalState(Act object, AbstractChargeItemRelationshipCollectionEditor collection,
                                        ChargeRemovalStates states) {
        ChargeRemovalStates.Status status = ChargeRemovalStates.Status.OK;

        PriceActEditContext editContext = collection.getEditContext();
        if (editContext.useMinimumQuantities()) {
            IMObjectEditor chargeEditor = collection.getEditor(object);
            if (chargeEditor instanceof PriceActItemEditor) {
                PriceActItemEditor editor = (PriceActItemEditor) chargeEditor;
                BigDecimal quantity = editor.getMinimumQuantity();
                if (quantity.compareTo(BigDecimal.ZERO) > 0) {
                    String message;
                    String name = getDisplayName(editor);
                    if (editContext.overrideMinimumQuantities()) {
                        status = ChargeRemovalStates.Status.OVERRIDE;
                        message = Messages.format("customer.charge.delete.minquantity", name, quantity);
                    } else {
                        status = ChargeRemovalStates.Status.FORBIDDEN;
                        message = Messages.format("customer.charge.delete.minquantity.forbidden", name, quantity);
                    }
                    states.addStatus(object, status, message);
                }
            }
        }
        return status != ChargeRemovalStates.Status.FORBIDDEN;
    }

    /**
     * Invoked when removal is cancelled.
     * <p/>
     * This unmarks all marked objects,
     *
     * @param collection the collection
     */
    @Override
    protected void cancelRemove(IMObjectCollectionEditor collection) {
        ((AbstractChargeItemRelationshipCollectionEditor) collection).unmarkAll();
    }

    /**
     * Returns the display name for an object, used for display.
     *
     * @param object     the object
     * @param collection the collection the object is in
     * @return the display name
     */
    @Override
    protected String getDisplayName(IMObject object, IMObjectCollectionEditor collection) {
        String result;
        AbstractChargeItemRelationshipCollectionEditor chargeCollection
                = (AbstractChargeItemRelationshipCollectionEditor) collection;
        IMObjectEditor chargeEditor = chargeCollection.getEditor(object);
        if (chargeEditor instanceof PriceActItemEditor) {
            result = getDisplayName((PriceActItemEditor) chargeEditor);
        } else {
            result = super.getDisplayName(object, collection);
        }
        return result;
    }

    /**
     * Prompts for removal.
     *
     * @param states           the charge states
     * @param chargeCollection the collection to remove from
     */
    private void promptRemoval(ChargeRemovalStates states,
                               AbstractChargeItemRelationshipCollectionEditor chargeCollection) {
        Consumer<Collection<Act>> remover = acts -> {
            removeAll(new ArrayList<>(acts), chargeCollection);
            cancelRemove(chargeCollection);  // cancel removal of any remaining selections
        };
        Runnable canceller = () -> cancelRemove(chargeCollection);
        states.show(chargeCollection.getProperty().getDisplayName(), remover, canceller,
                    new DefaultLayoutContext(context, help));
    }

    /**
     * Removes a collection of items.
     *
     * @param objects    the objects to remove
     * @param collection the collection the objects belong to
     */
    private void removeAll(List<IMObject> objects, AbstractChargeItemRelationshipCollectionEditor collection) {
        for (IMObject object : objects) {
            apply(object, collection);
        }
    }

    /**
     * Returns the display name for an object.
     *
     * @param editor the object editor
     * @return a display name for the object
     */
    private String getDisplayName(PriceActItemEditor editor) {
        String result;
        Product product = editor.getProduct();
        if (product != null) {
            result = product.getName();
        } else {
            result = DescriptorHelper.getDisplayName(editor.getObject(), ServiceHelper.getArchetypeService());
        }
        return result;
    }

}
