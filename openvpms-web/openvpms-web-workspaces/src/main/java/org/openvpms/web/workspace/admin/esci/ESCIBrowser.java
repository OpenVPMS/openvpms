/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.AbstractEntityBrowser;
import org.openvpms.web.component.im.query.EntityQuery;
import org.openvpms.web.component.im.table.NameDescObjectSetTableModel;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.echo.table.TableColumnFactory;

import static org.openvpms.archetype.rules.stock.StockArchetypes.STOCK_LOCATION;

/**
 * Browses ESCI suppliers.
 *
 * @author Tim Anderson
 */
class ESCIBrowser extends AbstractEntityBrowser<Party> {

    /**
     * Constructs an {@link ESCIBrowser}.
     *
     * @param query   the query
     * @param context the layout context
     */
    public ESCIBrowser(EntityQuery<Party> query, LayoutContext context) {
        super(query, context, new ESCITableModel());
    }

    /**
     * Returns the stock location associated with e the selected row.
     *
     * @return the stock location. May be {@code null}
     */
    public Party getStockLocation() {
        ObjectSet selected = getBrowser().getSelected();
        return (selected != null) ? (Party) IMObjectHelper.getObject(selected.getReference("stockLocation.reference"))
                                  : null;
    }

    /**
     * Returns the ESCI configuration.
     *
     * @return the ESCI configuration
     */
    public EntityRelationship getConfig() {
        ObjectSet selected = getBrowser().getSelected();
        return (selected != null) ? (EntityRelationship) IMObjectHelper.getObject(
                selected.getReference("config.reference")) : null;
    }

    private static class ESCITableModel extends NameDescObjectSetTableModel {

        /**
         * Stock location column index.
         */
        private static final int LOCATION_INDEX = ACTIVE_INDEX + 1;

        /**
         * Constructs an {@link ESCITableModel}.
         */
        public ESCITableModel() {
            super("entity", false, false);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param set    the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(ObjectSet set, TableColumn column, int row) {
            Object result;
            if (column.getModelIndex() == LOCATION_INDEX) {
                result = set.getString("stockLocation.name");
            } else {
                result = super.getValue(set, column, row);
            }
            return result;
        }

        /**
         * Creates the column model.
         *
         * @param showArchetype if {@code true} show the archetype
         * @return a new column model
         */
        @Override
        protected TableColumnModel createTableColumnModel(boolean showArchetype, boolean showActive) {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(ID_INDEX, ID));
            model.addColumn(TableColumnFactory.create(NAME_INDEX,
                                                      getDisplayName(SupplierArchetypes.SUPPLIER_ORGANISATION)));
            if (showActive) {
                model.addColumn(createTableColumn(ACTIVE_INDEX, ACTIVE));
            }
            model.addColumn(TableColumnFactory.create(LOCATION_INDEX, getDisplayName(STOCK_LOCATION)));
            return model;
        }
    }

}
