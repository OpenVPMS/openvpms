/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier;

import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.archetype.rules.product.ProductSupplier;
import org.openvpms.archetype.rules.supplier.DeliveryProcessor;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.product.ProductParticipationEditor;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * An editor for supplier orders, deliveries and returns, that:
 * <ul>
 * <li>calculates tax.</li>
 * <li>defaults values to the the associated {@link ProductSupplier} for the selected product and
 * supplier.</li>
 * <li>for orders and returns, updates the {@link ProductSupplier} on save, creating one if none exists.<br/>
 * NOTE: deliveries are excluded from this as deliveries update the relationship when finalised via the
 * {@link DeliveryProcessor}. Updating within the save would prevent the party.organisationPractice
 * ignoreListPriceDecreases flag from working.
 * </li>
 * </ul>
 *
 * @author Tim Anderson
 */
public abstract class SupplierStockItemEditor extends SupplierActItemEditor {

    /**
     * The reorder code node name.
     */
    protected static final String REORDER_CODE = "reorderCode";

    /**
     * The reorder description node name.
     */
    protected static final String REORDER_DESCRIPTION = "reorderDescription";

    /**
     * The package size node name.
     */
    protected static final String PACKAGE_SIZE = "packageSize";

    /**
     * The package units node name.
     */
    protected static final String PACKAGE_UNITS = "packageUnits";

    /**
     * The list price node name.
     */
    protected static final String LIST_PRICE = "listPrice";

    /**
     * The product rules.
     */
    private final ProductRules productRules;

    /**
     * Order detail nodes.
     */
    private static final String[] ORDER_DETAILS = {REORDER_CODE, REORDER_DESCRIPTION, PACKAGE_SIZE, PACKAGE_UNITS,
                                                   UNIT_PRICE, LIST_PRICE};

    /**
     * Constructs a {@link SupplierStockItemEditor}.
     *
     * @param act     the act
     * @param parent  the parent act
     * @param context the layout context
     * @throws ArchetypeServiceException for any archetype service error
     */
    public SupplierStockItemEditor(FinancialAct act, Act parent, LayoutContext context) {
        super(act, parent, context);
        productRules = ServiceHelper.getBean(ProductRules.class);
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    public Party getSupplier() {
        IMObjectBean bean = getBean(getParent());
        return (Party) getObject(bean.getTargetRef("supplier"));
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location. May be {@code null}
     */
    public Party getStockLocation() {
        return (Party) getObject(getStockLocationRef());
    }

    /**
     * Returns the stock location reference.
     *
     * @return the stock location reference. May be {@code null}
     */
    public Reference getStockLocationRef() {
        IMObjectBean bean = getBean(getParent());
        return bean.getTargetRef("stockLocation");
    }

    /**
     * Sets the stock location.
     *
     * @param location the stock location. May be {@code null}
     */
    public void setStockLocation(Party location) {
        ProductParticipationEditor editor = getProductEditor();
        if (editor != null) {
            editor.setStockLocation(location);
        }
    }

    /**
     * Returns the reorder code.
     *
     * @return the reorder code. May be {@code null}
     */
    public String getReorderCode() {
        return (String) getProperty(REORDER_CODE).getValue();
    }

    /**
     * Sets the reorder code.
     *
     * @param reorderCode the reorder code. May be {@code null}
     */
    public void setReorderCode(String reorderCode) {
        getProperty(REORDER_CODE).setValue(reorderCode);
    }

    /**
     * Returns the package units.
     *
     * @return the package units. May be {@code null}
     */
    public String getPackageUnits() {
        return (String) getProperty(PACKAGE_UNITS).getValue();
    }

    /**
     * Returns the reorder description.
     *
     * @return the reorder description. May be {@code null}
     */
    public String getReorderDescription() {
        return (String) getProperty(REORDER_DESCRIPTION).getValue();
    }

    /**
     * Sets the reorder description.
     *
     * @param description the description. May be {@code null}
     */
    public void setReorderDescription(String description) {
        getProperty(REORDER_DESCRIPTION).setValue(description);
    }

    /**
     * Sets the package size.
     *
     * @param packageSize the package size
     */
    public void setPackageSize(Integer packageSize) {
        getProperty(PACKAGE_SIZE).setValue(packageSize);
    }

    /**
     * Returns the package size.
     *
     * @return the package size
     */
    public int getPackageSize() {
        Integer value = (Integer) getProperty(PACKAGE_SIZE).getValue();
        return (value != null) ? value : 0;
    }

    /**
     * Sets the package units.
     *
     * @param packageUnits the package units. May be {@code null}
     */
    public void setPackageUnits(String packageUnits) {
        getProperty(PACKAGE_UNITS).setValue(packageUnits);
    }

    /**
     * Sets the list price.
     *
     * @param price the list price
     */
    public void setListPrice(BigDecimal price) {
        getProperty(LIST_PRICE).setValue(price);
    }

    /**
     * Returns the list price.
     *
     * @return the list price
     */
    public BigDecimal getListPrice() {
        return (BigDecimal) getProperty(LIST_PRICE).getValue();
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        // propagate the parent's supplier and stock location. Can only do this once the product editor is created
        ProductParticipationEditor productEditor = getProductEditor();
        if (productEditor != null) {
            productEditor.setSupplier(getSupplier());
            productEditor.setStockLocation(getStockLocation());
        }
    }

    /**
     * Invoked when the product is changed, to update prices.
     *
     * @param product the product. May be {@code null}
     */
    @Override
    protected void productModified(Product product) {
        ProductSupplier ps = getProductSupplier();
        if (ps != null) {
            setReorderCode(ps.getReorderCode());
            setReorderDescription(ps.getReorderDescription());
            setPackageSize(ps.getPackageSize());
            setPackageUnits(ps.getPackageUnits());
            setListPrice(ps.getListPrice());
            setUnitPrice(ps.getNettPrice());
        } else {
            setReorderCode(null);
            setReorderDescription(null);
            setPackageSize(null);
            setPackageUnits(null);
            setListPrice(null);
            setUnitPrice(null);
        }
        notifyProductListener(product);
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave() {
        Act object = getObject();
        if (!object.isA(SupplierArchetypes.DELIVERY_ITEM) && orderDetailsChanged()) {
            // don't update the product-supplier relationship for deliveries. This is handled when the delivery
            // is finalised by DeliveryProcessor in order to avoid auto-updating prices when
            // ignoreListPriceDecreases is true.
            Party supplier = getSupplier();
            Reference productRef = getProductRef();
            Product product = (productRef != null) ? getService().get(productRef, Product.class) : null;
            if (supplier != null && product != null) {
                // getProduct() will return a cached instance. Make sure the latest version of the product is being
                // used to avoid stale object exceptions.
                getLayoutContext().getCache().add(product);
                checkProductSupplier(product, supplier);
            }
        }
        super.doSave();
    }

    /**
     * Returns the product supplier relationship.
     *
     * @return the relationship. May be {@code null}
     */
    protected ProductSupplier getProductSupplier() {
        ProductParticipationEditor editor = getProductEditor();
        return (editor != null) ? editor.getProductSupplier() : null;
    }

    /**
     * Returns the product-supplier relationship matching the current details.
     *
     * @return the product-supplier relationship, or {@code null} if there is no match
     */
    protected ProductSupplier getMatchingProductSupplier() {
        ProductSupplier result = null;
        Product product = getProduct();
        Party supplier = getSupplier();
        if (product != null && supplier != null) {
            String reorderCode = getReorderCode();
            int size = getPackageSize();
            String units = getPackageUnits();
            ProductSupplier ps = productRules.getProductSupplier(product, supplier, reorderCode, size, units);
            if (ps != null) {
                String reorderDesc = getReorderDescription();
                BigDecimal listPrice = getListPrice();
                BigDecimal nettPrice = getUnitPrice();
                if (size == ps.getPackageSize()
                    && Objects.equals(units, ps.getPackageUnits())
                    && MathRules.equals(listPrice, ps.getListPrice())
                    && MathRules.equals(nettPrice, ps.getNettPrice())
                    && Objects.equals(ps.getReorderCode(), reorderCode)
                    && Objects.equals(ps.getReorderDescription(), reorderDesc)) {
                    result = ps;
                }
            }
        }
        return result;
    }

    /**
     * Returns the product rules.
     *
     * @return the product rules
     */
    protected ProductRules getProductRules() {
        return productRules;
    }

    /**
     * Determines if the order details have changed.
     *
     * @return {@code true} if the order details have changed, otherwise {@code false}
     */
    private boolean orderDetailsChanged() {
        for (String name : ORDER_DETAILS) {
            if (getProperty(name).isModified()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the product-supplier relationship for the specified product and supplier.
     * <p/>
     * If no relationship exists with the same package size and units, a new one will be added.
     * <p/>
     * If a relationship exists but the list price, nett price, reorder code or description have changed, it will be
     * updated.
     *
     * @param product  the product
     * @param supplier the supplier
     */
    private void checkProductSupplier(Product product, Party supplier) {
        ProductSupplier ps = getProductSupplier();
        String reorderCode = getReorderCode();
        int size = getPackageSize();
        String units = getPackageUnits();
        if (ps == null) {
            ps = productRules.getProductSupplier(product, supplier, reorderCode, size, units);
        }
        boolean save = true;
        String reorderDesc = getReorderDescription();
        BigDecimal listPrice = getListPrice();
        BigDecimal unitPrice = getUnitPrice();
        if (ps == null) {
            // no product-supplier relationship, so create a new one
            ps = productRules.createProductSupplier(product, supplier);
            ps.setPackageSize(size);
            ps.setPackageUnits(units);
            ps.setReorderCode(reorderCode);
            ps.setReorderDescription(reorderDesc);
            ps.setListPrice(listPrice);
            ps.setNettPrice(unitPrice);
            if (productRules.getProductSuppliers(product, supplier).isEmpty()) {
                // if there are no relationships for the supplier, mark the new one as the preferred
                ps.setPreferred(true);
            }
        } else if (size != ps.getPackageSize()
                   || !Objects.equals(units, ps.getPackageUnits())
                   || !MathRules.equals(listPrice, ps.getListPrice())
                   || !MathRules.equals(unitPrice, ps.getNettPrice())
                   || !Objects.equals(ps.getReorderCode(), reorderCode)
                   || !Objects.equals(ps.getReorderDescription(), reorderDesc)) {
            // properties are different to an existing relationship
            ps.setPackageSize(size);
            ps.setPackageUnits(units);
            ps.setReorderCode(reorderCode);
            ps.setReorderDescription(reorderDesc);
            ps.setListPrice(listPrice);
            ps.setNettPrice(unitPrice);
        } else {
            save = false;
        }
        if (save) {
            // NOTE: don't really want to save the entire product, but saving the relationship can lead to lost updates
            // as they don't have a version
            getService().save(product);
        }
    }

}
