/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.workflow.investigation.OrderConfirmationDialog;
import org.openvpms.web.workspace.workflow.investigation.OrderConfirmationManager;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Manages submission and confirmation of interactive laboratory orders created during invoicing.
 *
 * @author Tim Anderson
 */
class InvestigationSubmitter {

    public interface Listener {

        void submit(List<DocumentAct> investigations);

        void skip();

        void cancel();

    }

    /**
     * The investigation manager.
     */
    private final InvestigationManager investigationManager;

    /**
     * The layout context.
     */
    private final LayoutContext context;


    /**
     * Constructs a {@link InvestigationSubmitter}.
     *
     * @param manager the investigation manager
     * @param context the layout context
     */
    InvestigationSubmitter(InvestigationManager manager, LayoutContext context) {
        this.investigationManager = manager;
        this.context = context;
    }

    /**
     * Displays a dialog to select investigations for submission.
     *
     * @param listener the listener to notify
     */
    public void select(Consumer<List<DocumentAct>> listener) {
        Listener delegate = new Listener() {
            @Override
            public void submit(List<DocumentAct> investigations) {
                listener.accept(investigations);
            }

            @Override
            public void skip() {
            }

            @Override
            public void cancel() {
            }
        };
        InvestigationOrderDialog dialog = createDialog(delegate, false);
        dialog.show();
    }

    /**
     * Queues a dialog to select investigations for submission.
     *
     * @param queue    the queue
     * @param listener the listener to notify
     */
    public void select(EditorQueue queue, Listener listener) {
        InvestigationOrderDialog dialog = createDialog(listener, true);
        queue.queue(dialog);
        // NOTE - this will register its own WindowPaneListener. Order of registration is important
    }

    /**
     * Processes all investigations that need order confirmation.
     *
     * @param investigations the investigations
     * @param queue          the queue used to queue confirmation dialogs.
     */
    public void confirm(List<DocumentAct> investigations, EditorQueue queue) {
        List<DocumentAct> acts = new ArrayList<>();
        for (DocumentAct investigation : investigations) {
            if (investigation != null && canConfirm(investigation)) {
                acts.add(investigation);
            }
        }
        if (!acts.isEmpty()) {
            OrderConfirmationManager manager = new OrderConfirmationManager();
            List<OrderConfirmationDialog> dialogs = new ArrayList<>();
            for (DocumentAct investigation : acts) {
                OrderConfirmationDialog dialog = manager.createConfirmationDialog(investigation,
                                                                                  context.getHelpContext());
                if (dialog != null) {
                    dialog.addWindowPaneListener(new WindowPaneListener() {
                        @Override
                        public void onClose(WindowPaneEvent event) {
                            investigationManager.reload(investigation);
                        }
                    });
                    dialogs.add(dialog);
                }
            }
            int count = 1;
            for (OrderConfirmationDialog dialog : dialogs) {
                String title = Messages.format("customer.charge.laboratory.order.title", count, dialogs.size());
                dialog.setTitle(title);
                queue.queue(dialog);
                count++;
            }
        }
    }

    /**
     * Determines if there are investigations to submit.
     *
     * @return {@code true} if there are investigations to submit
     */
    public boolean canSubmit() {
        List<DocumentAct> investigations = investigationManager.getUnconfirmedLaboratoryInvestigations();
        if (!investigations.isEmpty()) {
            for (DocumentAct investigation : investigations) {
                // ensure the latest instance of the investigation is being used
                investigationManager.reload(investigation);
            }
            investigations = investigationManager.getUnconfirmedLaboratoryInvestigations();
        }
        return !investigations.isEmpty();
    }

    /**
     * Creates a dialog to select investigations to submit.
     *
     * @param listener the listener to notify
     * @param skip     if {@code true}, display a button to skip selection
     * @return a new dialog
     */
    protected InvestigationOrderDialog createDialog(Listener listener, boolean skip) {
        InvestigationOrderDialog dialog = new InvestigationOrderDialog(investigationManager, context, skip);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onAction(String action) {
                if (action.equals(InvestigationOrderDialog.SUBMIT_ID)) {
                    listener.submit(dialog.getSelections());
                }
                super.onAction(action);
            }

            @Override
            public void onSkip() {
                listener.skip();
            }

            @Override
            public void onCancel() {
                listener.cancel();
            }
        });
        return dialog;
    }

    /**
     * Determines if an order for an investigation can be confirmed.
     *
     * @param investigation the investigation
     * @return {@code true} if the order for the investigation can be confirmed
     */
    private boolean canConfirm(Act investigation) {
        String orderStatus = investigation.getStatus2();
        return !InvestigationActStatus.CANCELLED.equals(investigation.getStatus())
               && (InvestigationActStatus.CONFIRM.equals(orderStatus) ||
                   InvestigationActStatus.CONFIRM_DEFERRED.equals(orderStatus));
    }

}
