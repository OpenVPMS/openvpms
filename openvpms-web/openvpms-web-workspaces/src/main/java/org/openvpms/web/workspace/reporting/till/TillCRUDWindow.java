/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.till;

import nextapp.echo2.app.Extent;
import nextapp.echo2.app.ListBox;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.rules.finance.till.TillBalanceQuery;
import org.openvpms.archetype.rules.finance.till.TillBalanceStatus;
import org.openvpms.archetype.rules.finance.till.TillRules;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.IMObjectListCellRenderer;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.till.CashDrawer;
import org.openvpms.web.component.im.till.CashDrawerFactory;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectViewer;
import org.openvpms.web.component.print.BasicPrinterListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.dialog.SelectionDialog;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ComponentFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.account.Reverser;
import org.openvpms.web.workspace.customer.account.payment.AdminCustomerPaymentEditDialog;
import org.openvpms.web.workspace.customer.account.payment.AdminCustomerPaymentEditor;
import org.openvpms.web.workspace.reporting.FinancialActCRUDWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ZERO;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND;
import static org.openvpms.archetype.rules.finance.till.TillArchetypes.TILL_BALANCE_ADJUSTMENT;
import static org.openvpms.archetype.rules.finance.till.TillBalanceStatus.IN_PROGRESS;
import static org.openvpms.archetype.rules.finance.till.TillBalanceStatus.UNCLEARED;


/**
 * CRUD window for till balances.
 * <p/>
 * This supports:
 * <ul>
 *   <li>starting to clear the selected till balance.<br/>
 *       This sets the status of the selected till balance to IN_PROGRESS, so that any new payments or refunds don't
 *      affect it.
 *   </li>
 *   <li>clearing the selected till balance.<br/>
 *      This sets the status of the selected till balance to CLEARED, and deposits the balance.
 *   </li>
 *   <li>adding payments and refunds to Clear In Progress balances, using {@link TillPaymentEditor}. <br/>
 *    This adds a finalised payment/refund to the selected till balance</li>
 *   <li>administratively editing POSTED payments in an Uncleared or Clear In Progress till balance, using
 *     {@link AdminCustomerPaymentEditor}.
 *   <li>transferring the selected payment/refund from one till to another.
 *    </li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class TillCRUDWindow extends FinancialActCRUDWindow {

    /**
     * The till rules.
     */
    private final TillRules rules;

    /**
     * Customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The selected child act.
     */
    private FinancialAct childAct;

    /**
     * Start clear button identifier.
     */
    static final String START_CLEAR_ID = "button.startClear";

    /**
     * Clear button identifier.
     */
    static final String CLEAR_ID = "button.clear";

    /**
     * Adjust button identifier.
     */
    static final String ADJUST_ID = "button.adjust";

    /**
     * Reverse button identifier.
     */
    static final String REVERSE_ID = "button.reverse";

    /**
     * Transfer button identifier.
     */
    static final String TRANSFER_ID = "button.transfer";

    /**
     * Open drawer button identifier.
     */
    private static final String OPEN_DRAWER_ID = "button.openDrawer";

    /**
     * Till balance short name.
     */
    private static final String TILL_BALANCE = "act.tillBalance";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TillCRUDWindow.class);


    /**
     * Constructs a {@link TillCRUDWindow}.
     *
     * @param context the context
     * @param help    the help context
     */
    public TillCRUDWindow(Context context, HelpContext help) {
        super(new Archetypes<>(TILL_BALANCE_ADJUSTMENT, FinancialAct.class), context, help);
        rules = ServiceHelper.getBean(TillRules.class);
        accountRules = ServiceHelper.getBean(CustomerAccountRules.class);
        userRules = ServiceHelper.getBean(UserRules.class);
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(FinancialAct object) {
        childAct = null;
        super.setObject(object);
    }

    /**
     * Invoked when the edit button is pressed. This popups up an {@link EditDialog} if the act is an
     * <em>act.tillBalanceAdjustment</em> or an <em>act.customerAccountPayment</em>
     * <p/>
     * Note that this is only enabled for:
     * <ul>
     *     <li>uncleared balances where the selected child act is a till adjustment or payment</li>
     *     <li>in progress balances where the selected child act is a payment</li>     *
     * </ul>
     */
    @Override
    public void edit() {
        if (TypeHelper.isA(childAct, TILL_BALANCE_ADJUSTMENT)) {
            editAdjustment(childAct);
        } else if (TypeHelper.isA(childAct, PAYMENT) && canAdministerPayment()) {
            administerPayment(childAct);
        }
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(START_CLEAR_ID, action(TillArchetypes.TILL_BALANCE, this::onStartClear, "till.clear.title"));
        buttons.add(CLEAR_ID, action(TillArchetypes.TILL_BALANCE, this::onClear, "till.clear.title"));
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
        buttons.add(ADJUST_ID, this::onAdjust);
        buttons.add(REVERSE_ID, this::onReverse);
        buttons.add(NEW_ID, this::onNew);
        buttons.add(createEditButton());
        buttons.add(TRANSFER_ID, this::onTransfer);
        buttons.add(OPEN_DRAWER_ID, this::onOpenDrawer);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        boolean uncleared = false;
        boolean inProgress = false;
        boolean reverse = false;
        boolean enableEdit = false;
        boolean enableTransfer = false;
        if (enable) {
            Act act = getObject();
            if (TypeHelper.isA(act, TILL_BALANCE)) {
                uncleared = UNCLEARED.equals(act.getStatus());
                inProgress = IN_PROGRESS.equals(act.getStatus());
            }
            if (uncleared) {
                if (TypeHelper.isA(childAct, TILL_BALANCE_ADJUSTMENT)) {
                    enableEdit = true;
                } else if (TypeHelper.isA(childAct, PAYMENT) && canAdministerPayment()) {
                    // can administratively edit payments
                    enableEdit = true;
                }
                if (TypeHelper.isA(childAct, PAYMENT, REFUND)) {
                    enableTransfer = true;
                }
            }
            if (inProgress) {
                if (TypeHelper.isA(childAct, PAYMENT) && canAdministerPayment()) {
                    // can administratively edit payments
                    enableEdit = true;
                }
                reverse = TypeHelper.isA(childAct, PAYMENT, REFUND);
            }
        }
        buttons.setEnabled(START_CLEAR_ID, uncleared);
        buttons.setEnabled(CLEAR_ID, uncleared || inProgress);
        enablePrintPreview(buttons, enable);
        buttons.setEnabled(ADJUST_ID, uncleared || inProgress);
        buttons.setEnabled(REVERSE_ID, reverse);
        buttons.setEnabled(NEW_ID, inProgress);
        buttons.setEnabled(EDIT_ID, enableEdit);
        buttons.setEnabled(TRANSFER_ID, enableTransfer);
        buttons.setEnabled(OPEN_DRAWER_ID, canOpenDrawer());
    }

    /**
     * Invoked when the 'start clear' button is pressed.
     * <p/>
     * This sets the status of the selected till balance to IN_PROGRESS, so that any new payments or refunds don't
     * affect it.
     * <p/>
     * If the cash float is different to the existing cash float for the till, an adjustment will be created     *
     *
     * @param act the till balance
     */
    protected void onStartClear(FinancialAct act) {
        IMObjectBean balanceBean = getBean(act);
        Entity till = balanceBean.getTarget("till", Entity.class);
        if (!rules.isClearInProgress(till)) {
            IMObjectBean tillBean = getBean(till);
            BigDecimal lastFloat = tillBean.getBigDecimal("tillFloat", ZERO);
            HelpContext help = getHelpContext().subtopic("startClear");
            StartClearTillDialog dialog = new StartClearTillDialog(help);
            dialog.setCashFloat(lastFloat);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    rules.startClearTill(act, dialog.getCashFloat());
                    onRefresh(act);
                }
            });
            dialog.show();
        } else {
            ErrorDialog.show(Messages.get("till.clear.title"),
                             Messages.get("till.clear.error.clearInProgress"));
        }
    }

    /**
     * Invoked when the 'clear' button is pressed.
     *
     * @param act the till balance
     */
    protected void onClear(FinancialAct act) {
        IMObjectBean actBean = getBean(act);
        Party till = actBean.getTarget("till", Party.class);
        Party location = getContext().getLocation();
        if (till != null && location != null) {
            boolean uncleared = UNCLEARED.equals(act.getStatus());
            boolean inProgress = IN_PROGRESS.equals(act.getStatus());
            HelpContext help = getHelpContext().subtopic("clear");
            if (uncleared) {
                if (rules.isClearInProgress(till)) {
                    ErrorDialog.show(Messages.get("till.clear.title"),
                                     Messages.get("till.clear.error.clearInProgress"));
                } else {
                    IMObjectBean bean = getBean(till);
                    BigDecimal lastFloat = bean.getBigDecimal("tillFloat", ZERO);
                    ClearTillDialog dialog = new ClearTillDialog(location, true, getContext(), help);
                    dialog.setCashFloat(lastFloat);
                    dialog.addWindowPaneListener(new PopupDialogListener() {
                        @Override
                        public void onOK() {
                            rules.clearTill(act, dialog.getCashFloat(), dialog.getAccount());
                            onRefresh(act);
                        }
                    });
                    dialog.show();
                }
            } else if (inProgress) {
                ClearTillDialog dialog = new ClearTillDialog(location, false, getContext(), help);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onOK() {
                        rules.clearTill(act, dialog.getAccount());
                        onRefresh(act);
                    }
                });
                dialog.show();
            }
        }
    }

    /**
     * Prints the till balance.
     */
    @Override
    protected void onPrint() {
        FinancialAct object = IMObjectHelper.reload(getObject());
        if (object != null) {
            try {
                IMPrinter<ObjectSet> printer = createTillBalanceReport(object);
                String displayName = getDisplayName(TILL_BALANCE);
                String title = Messages.format("imobject.print.title", displayName);
                HelpContext help = getHelpContext().subtopic("print");
                InteractiveIMPrinter<ObjectSet> iPrinter =
                        new InteractiveIMPrinter<>(title, printer, getContext(), help);
                iPrinter.setMailContext(getMailContext());
                iPrinter.setListener(new BasicPrinterListener() {
                    @Override
                    public void printed(DocumentPrinter printer) {
                        FinancialAct saved = getActions().setPrinted(object);
                        if (saved != null) {
                            onSaved(saved, false);
                        }
                    }
                });
                iPrinter.print();
            } catch (OpenVPMSException exception) {
                ErrorHelper.show(exception);
            }
        }
    }

    /**
     * Invoked when the 'mail' button is pressed.
     */
    @Override
    protected void onMail() {
        FinancialAct object = IMObjectHelper.reload(getObject());
        if (object != null) {
            IMPrinter<ObjectSet> printer = createTillBalanceReport(object);
            mail(printer);
        }
    }

    /**
     * Previews an object.
     *
     * @param object the object to preview
     */
    @Override
    protected void preview(FinancialAct object) {
        IMPrinter<ObjectSet> printer = createTillBalanceReport(object);
        preview(printer);
    }

    /**
     * Invoked when the 'new' button is pressed.
     */
    protected void onNew() {
        String[] shortNames = {PAYMENT, REFUND};
        Archetypes<FinancialAct> archetypes = Archetypes.create(shortNames, FinancialAct.class,
                                                                Messages.get("customer.account.createtype"));
        onCreate(archetypes);
    }

    /**
     * Invoked when the 'adjust' button is pressed.
     */
    protected void onAdjust() {
        onCreate(getArchetypes());
    }

    /**
     * Invoked when the 'reverse' button is pressed.
     */
    protected void onReverse() {
        FinancialAct balance = IMObjectHelper.reload(getObject());
        FinancialAct act = IMObjectHelper.reload(childAct);
        if (balance != null && IN_PROGRESS.equals(balance.getStatus()) && TypeHelper.isA(childAct, PAYMENT, REFUND)) {
            Reverser reverser = new Reverser(getContext().getPractice(), getHelpContext().subtopic("reverse"));
            reverser.reverse(act, balance, () -> onRefresh(balance));
        }
    }

    /**
     * Invoked when the 'transfer' button is pressed.
     */
    protected void onTransfer() {
        FinancialAct act = getObject();
        IMObjectBean bean = getBean(act);
        Party till = bean.getTarget("till", Party.class);
        List<IMObject> tills = getTillsExcluding(till);

        String title = Messages.get("till.transfer.title");
        String message = Messages.get("till.transfer.message");
        ListBox list = new ListBox(tills.toArray());
        list.setStyleName("default");
        list.setHeight(new Extent(10, Extent.EM));
        list.setCellRenderer(IMObjectListCellRenderer.NAME);

        SelectionDialog dialog = new SelectionDialog(title, message, list, getHelpContext().subtopic("transfer"));
        dialog.addWindowPaneListener(new WindowPaneListener() {
            public void onClose(WindowPaneEvent e) {
                Party selected = (Party) dialog.getSelected();
                if (selected != null) {
                    doTransfer(act, childAct, selected);
                }
            }
        });
        dialog.show();
    }

    /**
     * Invoked when the 'open drawer' button is pressed.
     */
    protected void onOpenDrawer() {
        CashDrawer drawer = getCashDrawer();
        if (drawer != null && drawer.canOpen()) {
            drawer.open();
        }
    }

    /**
     * Invoked when an act has been created.
     *
     * @param act the new act
     */
    @Override
    protected void onCreated(FinancialAct act) {
        // populate the adjust with the current till
        FinancialAct balance = getObject();
        if (TypeHelper.isA(act, TILL_BALANCE_ADJUSTMENT)) {
            act.setDescription(Messages.get("till.adjustment.description"));
        }
        IMObjectBean actBean = getBean(balance);
        Reference till = actBean.getTargetRef("till");
        IMObjectBean adjBean = getBean(act);
        if (till != null) {
            adjBean.setTarget("till", till);
        }
        super.onCreated(act);
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit.
     * @param context the layout context
     * @return a new editor
     */
    @Override
    protected IMObjectEditor createEditor(FinancialAct object, LayoutContext context) {
        IMObjectEditor result;
        // pass in the parent balance, if it isn't CLEARED
        FinancialAct balance = getObject();
        if (TillBalanceStatus.CLEARED.equals(balance.getStatus())) {
            balance = null;
        }
        if (TypeHelper.isA(object, TILL_BALANCE_ADJUSTMENT)) {
            result = new TillBalanceAdjustmentEditor(object, balance, context);
        } else if (TypeHelper.isA(object, PAYMENT, REFUND)) {
            result = new TillPaymentEditor(object, balance, context);
        } else {
            result = super.createEditor(object, context);
        }
        return result;
    }

    /**
     * Invoked when the editor is closed.
     *
     * @param editor the editor
     * @param isNew  determines if the object is a new instance
     */
    @Override
    protected void onEditCompleted(IMObjectEditor editor, boolean isNew) {
        FinancialAct act = IMObjectHelper.reload(getObject());
        setObject(act);
    }

    /**
     * Invoked when a child act is selected/deselected.
     *
     * @param child the child act. May be {@code null}
     */
    @Override
    protected void onChildActSelected(FinancialAct child) {
        childAct = child;
        enableButtons(getButtons(), getObject() != null);
    }

    /**
     * Creates a new {@link IMObjectViewer} for an object.
     *
     * @param object the object to view
     */
    @Override
    protected IMObjectViewer createViewer(IMObject object) {
        LayoutContext context = createViewLayoutContext();
        LayoutStrategy layout = new TillBalanceActLayoutStrategy();
        return new IMObjectViewer(object, null, layout, context);
    }

    /**
     * Returns a {@link CashDrawer} for the current till.
     *
     * @return the drawer corresponding to the till, or {@code null} if none is present
     */
    private CashDrawer getCashDrawer() {
        CashDrawer result = null;
        Entity till = getContext().getTill();
        if (till != null) {
            CashDrawerFactory factory = ServiceHelper.getBean(CashDrawerFactory.class);
            result = factory.create(till);
        }
        return result;
    }

    /**
     * Determines if the current user can administratively edit payments.
     *
     * @return {@code true} if the user can administratively edit payments
     */
    private boolean canAdministerPayment() {
        User user = getContext().getUser();
        return UserHelper.isAdmin(user) || userRules.canEdit(user, TILL_BALANCE_ADJUSTMENT);
    }

    /**
     * Edits a till balance adjustment.
     *
     * @param adjustment the till balance adjustment
     */
    private void editAdjustment(FinancialAct adjustment) {
        LayoutContext context = createLayoutContext(createEditTopic(adjustment));
        IMObjectEditor editor = createEditor(adjustment, context);
        EditDialog dialog = new EditDialog(editor, getContext());
        dialog.addWindowPaneListener(new WindowPaneListener() {
            public void onClose(WindowPaneEvent event) {
                onEditCompleted(editor, false);
            }
        });
        dialog.show();
    }

    /**
     * Administratively edit a POSTED payment.
     *
     * @param payment the payment
     */
    private void administerPayment(FinancialAct payment) {
        if (canAdministerPayment()) {
            if (accountRules.hasClearedTillBalance(payment)) {
                // can't edit payments associated with a cleared till balance
                ErrorDialog.show(Messages.get("customer.account.payment.clearedtill.title"),
                                 Messages.get("customer.account.payment.clearedtill.message"));
            } else {
                HelpContext edit = createEditTopic(payment);
                LayoutContext context = createLayoutContext(edit);
                AdminCustomerPaymentEditor editor = new AdminCustomerPaymentEditor(payment, null, context);
                editor.getComponent();

                EditDialog dialog = new AdminCustomerPaymentEditDialog(editor, context.getContext());
                dialog.addWindowPaneListener(new WindowPaneListener() {
                    public void onClose(WindowPaneEvent event) {
                        onEditCompleted(editor, false);
                    }
                });
                dialog.show();
            }
        }
    }

    /**
     * Transfers the selected payment/refund to a different till.
     *
     * @param balance the original balance
     * @param act     the act to transfer
     * @param till    the till to transfer to
     */
    private void doTransfer(FinancialAct balance, FinancialAct act, Party till) {
        try {
            TillRules rules = ServiceHelper.getBean(TillRules.class);
            rules.transfer(balance, act, till);
        } catch (OpenVPMSException exception) {
            ErrorHelper.show(exception.getMessage(), exception);
        }
        onRefresh(getObject());
    }

    /**
     * Returns all tills except that supplied.
     *
     * @param till the till to exclude
     * @return the list of available tills
     */
    private List<IMObject> getTillsExcluding(Party till) {
        IArchetypeService service = ServiceHelper.getArchetypeService();
        ArchetypeQuery query = new ArchetypeQuery(TillArchetypes.TILL, true).setMaxResults(ArchetypeQuery.ALL_RESULTS);
        List<IMObject> tills = new ArrayList<>(service.get(query).getResults());
        tills.remove(till);
        return tills;
    }

    /**
     * Determines if the cash drawer can be opened for the current till.
     *
     * @return {@code true} if the cash drawer can be opened
     */
    private boolean canOpenDrawer() {
        boolean result = false;
        try {
            CashDrawer drawer = getCashDrawer();
            result = drawer != null && drawer.canOpen();
        } catch (Exception exception) {
            log.warn("Failed to determine if the drawer can be opened for the till: {}",
                     exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Creates a printer to generate a till balance report.
     *
     * @param object the till balance
     * @return a new printer
     */
    private IMPrinter<ObjectSet> createTillBalanceReport(FinancialAct object) {
        IPage<ObjectSet> set = new TillBalanceQuery(object, ServiceHelper.getArchetypeService()).query();
        IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
        Context context = getContext();
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(TILL_BALANCE, context);
        return factory.createObjectSetReportPrinter(set.getResults(), locator, context);
    }

    /**
     * Layout strategy for till balance acts that displays the start and end times as date/times.
     */
    private class TillBalanceActLayoutStrategy extends LayoutStrategy {

        /**
         * Apply the layout strategy.
         * <p>
         * This renders an object in a {@code Component}, using a factory to create the child components.
         *
         * @param object     the object to apply
         * @param properties the object's properties
         * @param parent     the parent object. May be {@code null}
         * @param context    the layout context
         * @return the component containing the rendered {@code object}
         */
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
            if (!context.isEdit()) {
                DateFormat format = DateFormatter.getDateTimeFormat(false);
                int maxColumns = DateFormatter.getLength(format);
                addComponent(createDate("startTime", properties, maxColumns, format));
                addComponent(createDate("endTime", properties, maxColumns, format));
            }
            return super.apply(object, properties, parent, context);
        }

        /**
         * Creates a date component for a property.
         *
         * @param name       the property name
         * @param properties the properties
         * @param maxColumns the maximum columns to display
         * @param format     the date format
         * @return a new component
         */
        private ComponentState createDate(String name, PropertySet properties, int maxColumns, DateFormat format) {
            Property property = properties.get(name);
            TextField result = BoundTextComponentFactory.create(property, maxColumns, format);
            result.setEnabled(false);
            ComponentFactory.setStyle(result, Styles.DEFAULT);
            return new ComponentState(result, property);
        }
    }
}
