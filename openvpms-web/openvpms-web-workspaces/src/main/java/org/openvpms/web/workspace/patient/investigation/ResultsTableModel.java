/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableModel;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Table model for <em>act.patientInvestigationResults</em>.
 *
 * @author Tim Anderson
 */
public class ResultsTableModel extends DescriptorTableModel<Act> {

    /**
     * Determines if the test column is displayed.
     */
    private boolean showTest = true;

    /**
     * Determines if the category column is displayed.
     */
    private boolean showCategory = true;

    /**
     * The nodes to display.
     */
    private ArchetypeNodes nodes = ArchetypeNodes.nodes("date", "test", "categoryName", "status");

    /**
     * Constructs a {@link ResultsTableModel}.
     *
     * @param shortNames the archetype short names. May contain wildcards
     * @param context    the layout context
     */
    public ResultsTableModel(String[] shortNames, LayoutContext context) {
        super(context);
        setTableColumnModel(createColumnModel(shortNames, context));
    }

    /**
     * Sets the objects to display.
     * <p/>
     * This shows/hides the test and/or category columns, based on the presence of data.
     *
     * @param objects the objects to display
     */
    @Override
    public void setObjects(List<Act> objects) {
        boolean newShowTest = false;
        boolean newShowCategory = false;
        ArchetypeService service = ServiceHelper.getArchetypeService();
        for (Act act : objects) {
            IMObjectBean bean = service.getBean(act);
            if (!newShowTest && bean.getTargetRef("test") != null) {
                newShowTest = true;
            }
            if (!newShowCategory && bean.getString("categoryName") != null) {
                newShowCategory = true;
            }
            if (newShowTest && newShowCategory) {
                break;
            }
        }
        if (newShowTest != showTest || newShowCategory != showCategory) {
            // oh for a table model that can hide columns...
            nodes = ArchetypeNodes.nodes("startTime");
            showTest = newShowTest;
            showCategory = newShowCategory;
            if (showTest) {
                nodes.list("test");
            }
            if (showCategory) {
                nodes.list("categoryName");
            }
            nodes.list("status");
            setTableColumnModel(createColumnModel(new String[]{InvestigationArchetypes.RESULTS}, getLayoutContext()));
        }
        super.setObjects(objects);
    }

    /**
     * Returns an {@link ArchetypeNodes} that determines what nodes appear in the table.
     *
     * @return the nodes to include
     */
    @Override
    protected ArchetypeNodes getArchetypeNodes() {
        return nodes;
    }
}
