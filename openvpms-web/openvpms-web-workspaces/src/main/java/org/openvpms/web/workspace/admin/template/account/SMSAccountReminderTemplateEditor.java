/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template.account;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.workspace.admin.template.SMSTemplateEditor;
import org.openvpms.web.workspace.admin.template.SMSTemplateSampler;

/**
 * An editor for <em>entity.documentTemplateSMSAccount</em>.
 *
 * @author Tim Anderson
 */
public class SMSAccountReminderTemplateEditor extends SMSTemplateEditor {

    /**
     * Constructs an {@link SMSAccountReminderTemplateEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public SMSAccountReminderTemplateEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        evaluate();
    }

    /**
     * Creates a new sampler.
     *
     * @param template      the template
     * @param layoutContext the sampler
     * @return a new sampler
     */
    @Override
    protected SMSTemplateSampler createSampler(Entity template, LayoutContext layoutContext) {
        return new SMSAccountReminderTemplateSampler(template, layoutContext);
    }
}