/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.layout.GridLayoutData;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.clinician.ClinicianSelectField;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.location.LocationSelectField;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.LookupFilter;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.ParticipantConstraint;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.select.AbstractSelectorListener;
import org.openvpms.web.component.im.select.IMObjectSelector;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes.INVESTIGATION_TYPE;
import static org.openvpms.archetype.rules.patient.InvestigationArchetypes.PATIENT_INVESTIGATION;
import static org.openvpms.web.component.im.query.QueryHelper.addParticipantConstraint;


/**
 * Query for patient investigations.
 *
 * @author Tim Anderson
 */
public class InvestigationsQuery extends DateRangeActQuery<Act> {

    /**
     * The archetypes.
     */
    public static final String[] ARCHETYPES = new String[]{InvestigationArchetypes.PATIENT_INVESTIGATION};

    /**
     * Dummy incomplete status. Finds all investigations that aren't CANCELLED and don't have a REVIEWED result status.
     */
    public static final String INCOMPLETE = "INCOMPLETE";

    /**
     * The location selector.
     */
    private final LocationSelectField location;

    /**
     * The clinician selector.
     */
    private final SelectField clinician;


    /**
     * The investigation type selector.
     */
    private final IMObjectSelector<Entity> investigationType;

    /**
     * The result status dropdown.
     */
    private final LookupField orderStatusSelector;

    /**
     * Dummy incomplete status, used in the result status selector.
     */
    private static final Lookup INCOMPLETE_STATUS
            = new org.openvpms.component.business.domain.im.lookup.Lookup(new ArchetypeId("lookup.local"), INCOMPLETE,
                                                                          Messages.get("investigation.incomplete"));

    /**
     * The default sort constraint.
     */
    private static final SortConstraint[] DEFAULT_SORT = {new NodeSortConstraint("startTime", false)};

    /**
     * The act statuses to query.
     */
    private static final ActStatuses STATUSES;

    /**
     * The order statuses to query.
     */
    private static final ActStatuses ORDER_STATUS = new ActStatuses(new OrderStatusQuery(), null);

    /**
     * The reviewed investigation status.
     */
    private static final String[] REVIEWED_STATUS = new String[]{"REVIEWED"};

    static {
        STATUSES = new ActStatuses(PATIENT_INVESTIGATION);
        STATUSES.setDefault((String) null);
    }

    /**
     * Constructs an {@link InvestigationsQuery}.
     *
     * @param context the context
     */
    public InvestigationsQuery(LayoutContext context) {
        super(null, null, null, ARCHETYPES, STATUSES, Act.class);
        setDefaultSortConstraint(DEFAULT_SORT);
        setAuto(true);
        setContains(true);

        location = createLocationSelector(context.getContext());
        clinician = createClinicianSelector();
        investigationType = createInvestigationTypeSelector(context);
        orderStatusSelector = LookupFieldFactory.create(ORDER_STATUS, true);
        orderStatusSelector.addActionListener(new ActionListener() {
            public void onAction(ActionEvent e) {
                onStatusChanged();
            }
        });
    }

    /**
     * Sets the practice location.
     *
     * @param location the practice location. May be {@code null}
     */
    public void setLocation(Party location) {
        this.location.setSelectedItem(location);
    }

    /**
     * Returns the selected practice location.
     *
     * @return the selected location. May be {@code null}
     */
    public Party getLocation() {
        return (Party) location.getSelectedItem();
    }

    /**
     * Sets the investigation type.
     *
     * @param type the investigation type. May be {@code null}
     */
    public void setInvestigationType(Entity type) {
        investigationType.setObject(type);
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician. May be {@code null}
     */
    public void setClinician(User clinician) {
        this.clinician.setSelectedItem(clinician);
    }

    /**
     * Sets the result status to filter on.
     * <p/>
     * The {@link #INCOMPLETE} result status code can be used to return all incomplete investigations that haven't
     * been cancelled nor reviewed.
     *
     * @param status the result status, or {@code null} to include all result statuses
     */
    public void setOrderStatus(String status) {
        orderStatusSelector.setSelected(status);
    }

    /**
     * Returns the preferred height of the query when rendered.
     *
     * @return the preferred height, or {@code null} if it has no preferred height
     */
    @Override
    public Extent getHeight() {
        return getHeight(3);
    }

    /**
     * Creates a container component to lay out the query component in.
     *
     * @return a new container
     * @see #doLayout(Component)
     */
    @Override
    protected Component createContainer() {
        return GridFactory.create(6);
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addSearchField(container);
        addStatusSelector(container);
        addOrderStatusSelector(container);
        addDateRange(container);
        addLocation(container);
        addClinician(container);
        addInvestigationType(container);
    }

    /**
     * Adds a result status selector to the container.
     *
     * @param container the container
     */
    protected void addOrderStatusSelector(Component container) {
        Label label = LabelFactory.create();
        label.setText(DescriptorHelper.getDisplayName(PATIENT_INVESTIGATION, "status2", getService()));
        container.add(label);
        container.add(orderStatusSelector);
        getFocusGroup().add(orderStatusSelector);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        List<ParticipantConstraint> list = new ArrayList<>();
        ParticipantConstraint supplier = getParticipantConstraint();
        if (supplier != null) {
            list.add(supplier);
        }
        addParticipantConstraint(list, "investigationType", InvestigationArchetypes.INVESTIGATION_TYPE_PARTICIPATION,
                                 investigationType.getObject());
        addParticipantConstraint(list, "clinician", UserArchetypes.CLINICIAN_PARTICIPATION,
                                 (Entity) clinician.getSelectedItem());
        ParticipantConstraint[] participants = list.toArray(new ParticipantConstraint[0]);

        Party location = (Party) this.location.getSelectedItem();
        String[] statuses = getStatuses();
        String[] orderStatus;
        boolean exclude = false;
        Lookup selected = orderStatusSelector.getSelected();
        if (selected == INCOMPLETE_STATUS) {
            orderStatus = REVIEWED_STATUS;
            exclude = true;
            if (statuses.length == 0) {
                // if 'All' status and 'Incomplete' result status is selected, exclude cancelled investigations,
                // as these are effectively complete
                statuses = new String[]{ActStatus.IN_PROGRESS, ActStatus.POSTED};
            }
        } else if (selected != null) {
            if (InvestigationActStatus.CONFIRM.equals(selected.getCode())) {
                // CONFIRM and CONFIRM_DEFERRED are treated as the same from a query perspective
                orderStatus = new String[]{InvestigationActStatus.CONFIRM, InvestigationActStatus.CONFIRM_DEFERRED};
                if (statuses.length == 0) {
                    // All is selected - exclude CANCELLED investigations
                    statuses = new String[]{ActStatus.IN_PROGRESS, ActStatus.POSTED};
                }
            } else {
                orderStatus = new String[]{selected.getCode()};
            }
        } else {
            orderStatus = new String[0];
        }
        return new InvestigationResultSet(getArchetypeConstraint(), getValue(), participants, location,
                                          this.location.getLocations(), getFrom(), getTo(), statuses,
                                          orderStatus, exclude, getMaxResults(), sort);
    }

    /**
     * Returns the act statuses to query.
     *
     * @return the act statuses to query
     */
    @Override
    protected String[] getStatuses() {
        String[] statuses = super.getStatuses();
        if (ArrayUtils.contains(statuses, InvestigationActStatus.CONFIRM)) {
            statuses = ArrayUtils.add(statuses, InvestigationActStatus.CONFIRM_DEFERRED);
        }
        return statuses;
    }

    /**
     * Adds the investigation selector to a container.
     *
     * @param container the container
     */
    private void addInvestigationType(Component container) {
        Label label = LabelFactory.create();
        label.setText(investigationType.getType());
        container.add(label);
        Component component = investigationType.getComponent();
        GridLayoutData layoutData = new GridLayoutData();
        layoutData.setColumnSpan(3);
        component.setLayoutData(layoutData);
        container.add(component);
        investigationType.getSelect().setFocusTraversalParticipant(false);
        getFocusGroup().add(investigationType.getTextField());
    }

    /**
     * Creates a field to select the investigation type.
     *
     * @param context the layout context
     * @return a new selector
     */
    private IMObjectSelector<Entity> createInvestigationTypeSelector(LayoutContext context) {
        IMObjectSelector<Entity> selector = new IMObjectSelector<>(
                DescriptorHelper.getDisplayName(INVESTIGATION_TYPE, getService()), context, INVESTIGATION_TYPE);
        AbstractSelectorListener<Entity> listener = new AbstractSelectorListener<Entity>() {
            public void selected(Entity object) {
                onQuery();
            }
        };
        selector.setListener(listener);
        return selector;
    }

    /**
     * Adds the clinician selector to a container.
     *
     * @param container the container
     */
    private void addClinician(Component container) {
        Label label = LabelFactory.create();
        label.setText(Messages.get("label.clinician"));
        container.add(label);
        container.add(clinician);
        getFocusGroup().add(clinician);
    }

    /**
     * Adds the location selector to a container.
     *
     * @param container the container
     */
    private void addLocation(Component container) {
        Label label = LabelFactory.create();
        label.setText(DescriptorHelper.getDisplayName(PATIENT_INVESTIGATION, "location", getService()));
        container.add(label);
        container.add(location);
        getFocusGroup().add(location);
    }

    /**
     * Creates a field to select the location.
     *
     * @param context the context
     * @return a new selector
     */
    private LocationSelectField createLocationSelector(Context context) {
        LocationSelectField result = new LocationSelectField(context.getUser(), context.getPractice(), true);
        result.setSelectedItem(context.getLocation());
        result.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        return result;
    }

    /**
     * Creates a new dropdown to select clinicians.
     *
     * @return a new clinician selector
     */
    private SelectField createClinicianSelector() {
        SelectField result = new ClinicianSelectField();
        result.addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        return result;
    }

    /**
     * Order status query.
     * <p/>
     * This:
     * <ul>
     *     <li>merges the {@link InvestigationActStatus#CONFIRM} and {@link InvestigationActStatus#CONFIRM_DEFERRED}.
     *     <br/>
     *         The latter is used during invoicing to defer confirmation of orders where the laboratory is not available.
     *     </li>
     *     <li>adds an INCOMPLETE status, to indicate all investigations that have not been reviewed</li>
     * </ul>
     * <p/>
     */
    private static class OrderStatusQuery extends LookupFilter {

        /**
         * Constructs an {@link OrderStatusQuery}.
         */
        public OrderStatusQuery() {
            super(new NodeLookupQuery(PATIENT_INVESTIGATION, "status2"), false,
                  InvestigationActStatus.CONFIRM_DEFERRED);
        }

        /**
         * Returns the default lookup.
         *
         * @return {@link #INCOMPLETE_STATUS}
         */
        @Override
        public Lookup getDefault() {
            return INCOMPLETE_STATUS;
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<Lookup> getLookups() {
            List<Lookup> lookups = super.getLookups();
            lookups.add(0, INCOMPLETE_STATUS);
            return lookups;
        }
    }
}
