/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge.department;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.query.AbstractEntityQuery;
import org.openvpms.web.component.im.query.EntityResultSet;
import org.openvpms.web.component.im.query.QueryHelper;
import org.openvpms.web.component.im.query.ResultSet;

import java.util.List;

/**
 * Query for departments (i.e. <em>entity.department</em>) instances.
 * <p/>
 * This limits departments to those assigned to the current user, or all departments if none are assigned.
 *
 * @author Tim Anderson
 */
public class DepartmentQuery extends AbstractEntityQuery<Entity> {

    /**
     * The context.
     */
    private final Context context;

    /**
     * Constructs a {@link DepartmentQuery}.
     *
     * @param context the context
     */
    public DepartmentQuery(Context context) {
        super(new String[]{PracticeArchetypes.DEPARTMENT});
        this.context = context;
    }

    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
        ShortNameConstraint archetypes = getArchetypeConstraint();
        User user = context.getUser();
        IConstraint constraints = null;
        if (user != null) {
            IMObjectBean bean = getBean(user);
            List<Reference> departments = bean.getTargetRefs("departments");
            if (!departments.isEmpty()) {
                // limit departments to those assigned to the user
                constraints = Constraints.in("id", (Object[]) QueryHelper.getIds(departments));
            }
        }
        return new EntityResultSet<>(archetypes, getValue(), isIdentitySearch(), isSearchAll(),
                                     constraints, sort, getMaxResults(), isDistinct());
    }
}
