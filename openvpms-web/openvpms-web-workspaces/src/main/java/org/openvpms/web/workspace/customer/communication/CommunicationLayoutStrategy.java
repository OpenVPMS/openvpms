/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.IMObjectTable;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.DelegatingProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.popup.DropDown;
import org.openvpms.web.echo.text.TextArea;

import java.util.ArrayList;
import java.util.List;


/**
 * Layout strategy for <em>act.customerCommunication*</em> acts.
 *
 * @author Tim Anderson
 */
public class CommunicationLayoutStrategy extends AbstractCommunicationLayoutStrategy {

    /**
     * The contact archetype short name.
     */
    private final String contacts;

    /**
     * Constructs a {@link CommunicationLayoutStrategy}.
     */
    public CommunicationLayoutStrategy() {
        this(null, null, true);
    }

    /**
     * Constructs a {@link CommunicationLayoutStrategy}.
     *
     * @param message     the message property. May be {@code null}
     * @param contacts    contact archetype short names. May be {@code null}
     * @param showPatient determines if the patient node should be displayed when editing
     */
    public CommunicationLayoutStrategy(Property message, String contacts, boolean showPatient) {
        super(showPatient, message, START_TIME);
        this.contacts = contacts;
    }

    /**
     * Apply the layout strategy.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (!context.isEdit()) {
            addContactViewers(properties, object, context);
        } else if (contacts != null) {
            Party customer = getBean(object).getTarget("customer", Party.class);
            if (customer != null) {
                addContactEditors(object, properties, customer, context);
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Add viewers to display contacts.
     *
     * @param properties the properties
     * @param object     the communication object
     * @param context    the layout context
     */
    protected void addContactViewers(PropertySet properties, IMObject object, LayoutContext context) {
        Property address = properties.get(ADDRESS);
        if (address != null) {
            addContactViewer(address, object, context, false);
        }
    }

    /**
     * Adds a contact viewer.
     *
     * @param property the property
     * @param object   the communication object
     * @param context  the layout context
     * @param optional if {@code true} and the property is empty, don't add a viewer
     */
    protected void addContactViewer(Property property, IMObject object, LayoutContext context, boolean optional) {
        String value = property.getString();
        if (!optional || !StringUtils.isEmpty(value)) {
            ComponentState state;
            int lines = StringUtils.countMatches(value, "\n") + 1;
            if (lines > 1) {
                state = createComponent(property, object, context);
                Component component = state.getComponent();
                if (component instanceof TextArea) {
                    TextArea text = (TextArea) component;
                    text.setHeight(new Extent(lines + 1, Extent.EM));
                }
            } else {
                // force it to display in one line
                DelegatingProperty p = new DelegatingProperty(property) {
                    @Override
                    public int getMaxLength() {
                        return 255;
                    }
                };
                state = createComponent(p, object, context);
            }
            addComponent(state);
        }
    }

    /**
     * Adds contact editors.
     *
     * @param properties the properties
     * @param object     the communication object
     * @param customer   the customer
     * @param context    the layout context
     */
    protected void addContactEditors(IMObject object, PropertySet properties, Party customer, LayoutContext context) {
        Property address = properties.get("address");
        if (address != null) {
            addContactSelector(address, object, customer, context);
        }
    }

    /**
     * Adds a contact editor.
     *
     * @param property the contact property
     * @param object   the communication object
     * @param customer the customer
     * @param context  the layout context
     */
    protected void addContactSelector(Property property, IMObject object, Party customer, LayoutContext context) {
        List<Contact> contacts = getContacts(customer);
        addContactSelector(property, object, contacts, context);
    }

    /**
     * Adds a contact selector.
     *
     * @param property the contact property
     * @param object   the communication object
     * @param contacts the available contacts
     * @param context  the layout context
     */
    protected void addContactSelector(final Property property, IMObject object, List<Contact> contacts,
                                      LayoutContext context) {
        IMObjectComponentFactory factory = context.getComponentFactory();
        ComponentState address = factory.create(property, object);
        Component addressComponent = address.getComponent();
        if (addressComponent instanceof TextArea) {
            ((TextArea) addressComponent).setHeight(new Extent(2, Extent.EM));
        }
        if (!contacts.isEmpty()) {
            DropDown contactDropDown = new DropDown();
            IMObjectTable<Contact> table = new IMObjectTable<>();
            table.setObjects(contacts);
            table.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    Contact contact = table.getSelected();
                    if (contact != null) {
                        property.setValue(formatContact(contact));
                    }
                    contactDropDown.setExpanded(false);
                }
            });
            contactDropDown.setTarget(addressComponent);
            contactDropDown.setPopUpAlwaysOnTop(true);
            contactDropDown.setFocusOnExpand(true);
            contactDropDown.setPopUp(table);
            contactDropDown.setFocusComponent(table);
            addComponent(new ComponentState(contactDropDown, property));
        } else {
            addComponent(address);
        }
    }

    /**
     * Formats a contact.
     * <p>
     * This version returns the contact description
     *
     * @param contact the contact to format
     * @return the formatted contact
     */
    protected String formatContact(Contact contact) {
        return contact.getDescription();
    }

    /**
     * Returns the contacts for a party.
     *
     * @param party the party
     * @return the contacts
     */
    protected List<Contact> getContacts(Party party) {
        List<Contact> result = new ArrayList<>();
        for (Contact contact : party.getContacts()) {
            if (contact.isA(contacts)) {
                result.add(contact);
            }
        }
        return result;
    }


}
