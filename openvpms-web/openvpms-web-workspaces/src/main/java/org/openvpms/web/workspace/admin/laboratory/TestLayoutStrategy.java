/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.style.Styles;

import java.util.List;

/**
 * Layout strategy for <em>entity.laboratoryTest</em>.
 *
 * @author Tim Anderson
 */
public class TestLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Code node.
     */
    public static final String CODE = "code";

    /**
     * Name node.
     */
    public static final String NAME = "name";

    /**
     * Description node.
     */
    public static final String DESCRIPTION = "description";

    /**
     * Investigation type node.
     */
    public static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Group node.
     */
    public static final String GROUP = "group";

    /**
     * Use device node.
     */
    public static final String USE_DEVICE = "useDevice";

    /**
     * Turnaround node.
     */
    public static final String TURNAROUND = "turnaround";

    /**
     * Specimen node.
     */
    public static final String SPECIMEN = "specimen";

    /**
     * Constructs a {@link TestLayoutStrategy}.
     */
    public TestLayoutStrategy() {
        super();
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        ArchetypeNodes nodes = ArchetypeNodes.all().excludeIfEmpty(CODE).exclude(SPECIMEN);

        IMObjectBean bean = getBean(object);
        boolean labTest = bean.getObject(CODE) != null;

        if (context.isEdit()) {
            if (labTest) {
                // this test is provided by a lab, so mark all those fields populated by the lab read-only
                MutablePropertySet set = new MutablePropertySet(properties);
                set.setReadOnly(NAME);
                set.setReadOnly(DESCRIPTION);
                set.setReadOnly(INVESTIGATION_TYPE);
                set.setReadOnly(TURNAROUND);
                set.setReadOnly(GROUP);
                set.setReadOnly(USE_DEVICE);
                set.setReadOnly(SPECIMEN);
                properties = set;
            } else {
                nodes.exclude(GROUP, USE_DEVICE);
            }
        } else {
            nodes.excludeIfEmpty(TURNAROUND);
            if (!labTest) {
                nodes.exclude(GROUP, USE_DEVICE);
            }
        }
        Property specimen = properties.get(SPECIMEN);
        if (specimen != null && (context.isEdit() || specimen.getString() != null)) {
            addComponent(createMultiLineText(specimen, object, 5, 20, Styles.FULL_WIDTH, context));
        }

        setArchetypeNodes(nodes);
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out components in a grid.
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     */
    @Override
    protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentGrid grid = super.createGrid(object, properties, context);
        ComponentState specimen = getComponent(SPECIMEN);
        if (specimen != null) {
            grid.add(specimen, 2);
        }
        return grid;
    }

}
