/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.practice.Location;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.clinician.ClinicianSelectField;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.location.LocationSelectField;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.LocationActResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

import static org.openvpms.archetype.rules.user.UserArchetypes.CLINICIAN_PARTICIPATION;

/**
 * Charges query.
 *
 * @author Tim Anderson
 */
public abstract class AbstractChargesQuery extends DateRangeActQuery<FinancialAct> {

    /**
     * The act short names.
     */
    public static final String[] SHORT_NAMES = new String[]{
            CustomerAccountArchetypes.INVOICE,
            CustomerAccountArchetypes.CREDIT,
            CustomerAccountArchetypes.COUNTER};

    /**
     * The location selector.
     */
    private final LocationSelectField locationSelector;

    /**
     * Clinician selector.
     */
    private final SelectField clinicianSelector;

    /**
     * Constructs an {@link AbstractChargesQuery}.
     *
     * @param statuses the statuses to query
     * @param location the initial location to filter on. May be {@code null}
     * @param context  the layout context
     */
    public AbstractChargesQuery(ActStatuses statuses, Party location, LayoutContext context) {
        super(null, null, null, SHORT_NAMES, statuses, FinancialAct.class);
        locationSelector = createLocationSelector(context.getContext(), location);
        clinicianSelector = createClinicianSelector();
    }

    /**
     * Sets the selected location.
     *
     * @param location the location. May be {@code null}
     */
    public void setLocation(Party location) {
        locationSelector.setSelected(location != null ? new Location(location) : Location.ALL);
    }

    /**
     * Returns the selected clinician.
     *
     * @return the selected clinician. May be {@code null}
     */
    public User getClinician() {
        return (User) clinicianSelector.getSelectedItem();
    }

    /**
     * Sets the clinician.
     *
     * @param clinician the clinician. May be {@code null}
     */
    public void setClinician(User clinician) {
        clinicianSelector.setSelectedItem(clinician);
    }

    /**
     * Returns the preferred height of the query when rendered.
     *
     * @return the preferred height, or {@code null} if it has no preferred height
     */
    @Override
    public Extent getHeight() {
        return super.getHeight(2);
    }

    /**
     * Creates a container component to lay out the query component in.
     *
     * @return a new container
     * @see #doLayout(Component)
     */
    @Override
    protected Component createContainer() {
        return GridFactory.create(6);
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addShortNameSelector(container);
        addStatusSelector(container);
        addDateRange(container);
        addLocation(container);
        addClinician(container);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<FinancialAct> createResultSet(SortConstraint[] sort) {
        Party location = getLocation();
        ParticipantConstraint clinician = getClinicianConstraint();
        ParticipantConstraint[] participants = (clinician != null) ? new ParticipantConstraint[]{clinician} : null;
        return new LocationActResultSet<>(getArchetypeConstraint(), participants, location, getLocations(),
                                          getFrom(), getTo(), getStatuses(), excludeStatuses(), getMaxResults(),
                                          sort);
    }

    /**
     * Returns the available locations.
     *
     * @return the available locations
     */
    protected List<Party> getLocations() {
        return locationSelector.getLocations();
    }

    /**
     * Returns the selected location.
     *
     * @return the selected location. May be {@code null}
     */
    protected Party getLocation() {
        return (Party) locationSelector.getSelectedItem();
    }

    /**
     * Creates a field to select the location.
     *
     * @param context  the context
     * @param location the initial location. May  be {@code null}
     * @return a new selector
     */
    protected LocationSelectField createLocationSelector(Context context, Party location) {
        LocationSelectField result = new LocationSelectField(context.getUser(), context.getPractice(), true);
        if (location != null) {
            result.setSelectedItem(location);
        }
        result.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        return result;
    }

    /**
     * Adds the location selector to a container.
     *
     * @param container the container
     */
    protected void addLocation(Component container) {
        Label label = LabelFactory.create();
        label.setText(DescriptorHelper.getDisplayName(CustomerAccountArchetypes.INVOICE, "location",
                                                      ServiceHelper.getArchetypeService()));
        container.add(label);
        container.add(locationSelector);
        getFocusGroup().add(locationSelector);
    }

    /**
     * Adds the clinician selector to a container.
     *
     * @param container the container
     */
    protected void addClinician(Component container) {
        Label label = LabelFactory.create();
        label.setText(Messages.get("label.clinician"));
        container.add(label);
        container.add(clinicianSelector);
        getFocusGroup().add(clinicianSelector);
    }

    /**
     * Returns the clinician constraint.
     *
     * @return the clinician constraint, or {@code null} if not filtering by clinician
     */
    private ParticipantConstraint getClinicianConstraint() {
        User clinician = getClinician();
        return clinician != null ? new ParticipantConstraint("clinician", CLINICIAN_PARTICIPATION, clinician) : null;
    }

    /**
     * Creates a new dropdown to select clinicians.
     *
     * @return a new clinician selector
     */
    private SelectField createClinicianSelector() {
        SelectField result = new ClinicianSelectField();
        result.addActionListener(new ActionListener() {
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        return result;
    }

}
