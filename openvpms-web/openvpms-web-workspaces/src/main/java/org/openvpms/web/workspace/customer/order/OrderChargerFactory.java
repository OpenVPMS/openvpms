/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.order;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.order.OrderRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Factory for {@link OrderCharger} instances.
 *
 * @author Tim Anderson
 */
public class OrderChargerFactory {

    /**
     * The order rules.
     */
    private final OrderRules orderRules;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules customerAccountRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The rule-based archetype service.
     */
    private final IArchetypeRuleService ruleService;

    /**
     * Constructs an {@link OrderChargerFactory}.
     *
     * @param orderRules           the order rules
     * @param customerAccountRules the customer account rules
     * @param service              the archetype service
     * @param ruleService          the rule-based archetype service
     */
    public OrderChargerFactory(OrderRules orderRules, CustomerAccountRules customerAccountRules,
                               IArchetypeService service, IArchetypeRuleService ruleService) {
        this.orderRules = orderRules;
        this.customerAccountRules = customerAccountRules;
        this.service = service;
        this.ruleService = ruleService;
    }

    /**
     * Creates an {@link OrderCharger}.
     *
     * @param customer the customer
     * @param context  the context
     * @param help     the help context
     */
    public OrderCharger create(Party customer, Context context, HelpContext help) {
        return new OrderCharger(customer, orderRules, customerAccountRules, service, ruleService, context, help);
    }

    /**
     * Creates an {@link OrderCharger}.
     *
     * @param customer the customer
     * @param patient  the patient. May be {@code null}
     * @param context  the context
     * @param help     the help context
     */
    public OrderCharger create(Party customer, Party patient, Context context, HelpContext help) {
        return new OrderCharger(customer, patient, orderRules, customerAccountRules, service, ruleService, context,
                                help);
    }

}
