/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.table.DefaultTableModel;
import nextapp.echo2.app.table.TableModel;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.factory.TableFactory;
import org.openvpms.web.echo.table.DefaultTableHeaderRenderer;
import org.openvpms.web.echo.table.KeyTable;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.plugin.BundleHelper;
import org.osgi.framework.Bundle;

/**
 * Displays the current plugins.
 *
 * @author Tim Anderson
 */
class PluginViewer extends AbstractDiagnosticTab {

    /**
     * The table columns.
     */
    private final String[] columns;

    /**
     * A snapshot of the plugins.
     */
    private String[][] snapshot;

    /**
     * Constructs a  {@link PluginViewer}.
     */
    PluginViewer() {
        super("admin.system.diagnostic.plugin");
        columns = new String[]{Messages.get("admin.system.plugin.id"), Messages.get("admin.system.plugin.name"),
                               Messages.get("admin.system.plugin.version"),
                               Messages.get("admin.system.plugin.status")};
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String[][] data = getData(false);
        if (data != null) {
            result = toCSV("plugins.csv", columns, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result = null;
        String[][] data = getData(true);
        if (data != null) {
            TableModel model = new DefaultTableModel(data, columns);
            KeyTable table = TableFactory.create(model);
            table.setDefaultHeaderRenderer(DefaultTableHeaderRenderer.DEFAULT);
            table.setHeaderFixed(true);
            result = table;
        }
        return result;
    }


    /**
     * Returns the plugin data.
     *
     * @param refresh if {@code true}, refresh the data if it has been collected previously
     * @return the property data
     */
    private String[][] getData(boolean refresh) {
        if (snapshot == null || refresh) {
            try {
                PluginManager pluginManager = ServiceHelper.getBean(PluginManager.class);
                Bundle[] bundles = pluginManager.getBundles();
                snapshot = new String[bundles.length][];
                for (int i = 0; i < bundles.length; ++i) {
                    Bundle bundle = bundles[i];
                    String[] row = new String[]{Long.toString(bundle.getBundleId()), BundleHelper.getName(bundle),
                                                bundle.getVersion().toString(), BundleHelper.getState(bundle)};
                    snapshot[i] = row;
                }
            } catch (Throwable exception) {
                snapshot = null;
                ErrorHelper.show(exception);
            }
        }
        return snapshot;
    }
}
