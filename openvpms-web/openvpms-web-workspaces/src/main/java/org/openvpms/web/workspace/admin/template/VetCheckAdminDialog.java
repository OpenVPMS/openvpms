/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import echopointng.HttpPaneEx;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.vetcheck.AbstractVetCheckDialog;
import org.openvpms.web.workspace.patient.vetcheck.VetCheckRules;

/**
 * VetCheck admin dialog.
 *
 * @author Tim Anderson
 */
public class VetCheckAdminDialog extends AbstractVetCheckDialog {

    /**
     * Constructs a {@link VetCheckAdminDialog}.
     *
     * @param help the help context
     */
    public VetCheckAdminDialog(HelpContext help) {
        super(help);
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        super.doLayout();
        VetCheckRules rules = ServiceHelper.getBean(VetCheckRules.class);
        String uri = rules.getURL();
        HttpPaneEx frame = new HttpPaneEx(uri);
        getLayout().add(frame);
    }
}