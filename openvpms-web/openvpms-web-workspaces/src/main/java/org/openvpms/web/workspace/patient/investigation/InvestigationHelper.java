/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.laboratory.service.Orders;
import org.openvpms.web.system.ServiceHelper;

/**
 * Investigation helper methods.
 *
 * @author Tim Anderson
 */
class InvestigationHelper {

    /**
     * Returns an order for an investigation.
     *
     * @param investigationId the investigation identifier
     * @return the order or {@code null} if none is found
     */
    public static Order getOrder(long investigationId) {
        Orders orders = ServiceHelper.getBean(Orders.class);
        return orders.getOrder(investigationId);
    }

    /**
     * Returns the laboratory service for an order.
     *
     * @param order the order
     * @return the laboratory service
     * @throws LaboratoryException if the service is unavailable
     */
    public static LaboratoryService getLaboratoryService(Order order) {
        LaboratoryServices services = ServiceHelper.getBean(LaboratoryServices.class);
        Laboratory laboratory = order.getLaboratory();
        if (laboratory == null) {
            throw new IllegalStateException("Order for investigation=" + order.getInvestigationId()
                                            + " has no laboratory");
        }
        return services.getService(laboratory);
    }

}
