/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.practice;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.bound.BoundSelectFieldFactory;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.ShortNameListCellRenderer;
import org.openvpms.web.component.im.list.ShortNameListModel;
import org.openvpms.web.component.im.relationship.RelationshipHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.subscription.SubscriptionHelper;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.admin.organisation.AbstractOrganisationLayoutStrategy;

import java.util.List;

import static org.openvpms.web.component.im.view.ReadOnlyComponentFactory.MAX_DISPLAY_LENGTH;
import static org.openvpms.web.component.im.view.ReadOnlyComponentFactory.getText;


/**
 * Layout strategy for <em>party.organisationPractice</em>.
 *
 * @author Tim Anderson
 */
public class PracticeLayoutStrategy extends AbstractOrganisationLayoutStrategy {

    /**
     * The subscription component.
     */
    private ComponentState subscription;

    /**
     * Minimum quantities override.
     */
    private static final String MINIMUM_QUANTITIES_OVERRIDE = "minimumQuantitiesOverride";

    /**
     * The archetype nodes. This excludes nodes rendered alongside others.
     */
    private static final ArchetypeNodes NODES = new ArchetypeNodes().exclude(MINIMUM_QUANTITIES_OVERRIDE);

    /**
     * Default constructor.
     */
    public PracticeLayoutStrategy() {
        super(NODES);
    }

    /**
     * Constructs a {@link PracticeLayoutStrategy}.
     *
     * @param subscription the component representing the subscription
     * @param focusGroup   the subscription component's focus group. May be {@code null}
     */
    public PracticeLayoutStrategy(Component subscription, FocusGroup focusGroup) {
        super(NODES);
        this.subscription = new ComponentState(subscription, focusGroup);
    }

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        IMObjectComponentFactory factory = context.getComponentFactory();
        addDefaultPaymentType(properties, context);
        addMinimumQuantities(object, properties, factory);
        if (subscription == null) {
            Participation participation = SubscriptionHelper.getSubscriptionParticipation((Party) object, getService());
            SubscriptionViewer viewer = new SubscriptionViewer(context);
            if (participation != null) {
                DocumentAct act = (DocumentAct) context.getCache().get(participation.getAct());
                viewer.setSubscription(act);
            }
            subscription = new ComponentState(viewer.getComponent());
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out child components in a tab model.
     *
     * @param object     the parent object
     * @param properties the properties
     * @param model      the tab model
     * @param context    the layout context
     * @param shortcuts  if {@code true} include short cuts
     */
    @Override
    protected void doTabLayout(IMObject object, List<Property> properties, IMObjectTabPaneModel model,
                               LayoutContext context, boolean shortcuts) {
        super.doTabLayout(object, properties, model, context, shortcuts);
        addTab(Messages.get("admin.practice.subscription"), model,
               ColumnFactory.create(Styles.INSET, subscription.getComponent()));
    }

    /**
     * Registers a component to render the default payment type.
     *
     * @param properties the properties
     * @param context    the layout context
     */
    private void addDefaultPaymentType(PropertySet properties, LayoutContext context) {
        Component component;
        Property property = properties.get("defaultPaymentType");
        if (context.isEdit()) {
            String[] archetypes = RelationshipHelper.getTargetShortNames(
                    getService(), CustomerAccountArchetypes.PAYMENT_ITEM_RELATIONSHIP);
            ShortNameListModel model = new ShortNameListModel(archetypes);
            SelectField field = BoundSelectFieldFactory.create(property, model);
            field.setCellRenderer(new ShortNameListCellRenderer());
            component = field;
        } else {
            String value = property.getString();
            if (value != null) {
                value = getDisplayName(value);
            }
            component = getText(value, property.getMaxLength(), MAX_DISPLAY_LENGTH, Styles.DEFAULT);
        }
        addComponent(new ComponentState(component, property));
    }

    /**
     * Registers a component to render the minimum quantities and overrides properties.
     *
     * @param object     the practice object
     * @param properties the properties
     * @param factory    the component factory
     */
    private void addMinimumQuantities(IMObject object, PropertySet properties, IMObjectComponentFactory factory) {
        Property minimumQuantities = properties.get("minimumQuantities");
        Property minimumQuantitiesOverride = properties.get(MINIMUM_QUANTITIES_OVERRIDE);

        ComponentState quantities = factory.create(minimumQuantities, object);
        ComponentState override = factory.create(minimumQuantitiesOverride, object);
        Row row = RowFactory.create(Styles.CELL_SPACING, quantities.getComponent(), override.getLabel(),
                                    override.getComponent());
        FocusGroup group = new FocusGroup(minimumQuantities.getName());
        group.add(quantities.getComponent());
        group.add(override.getComponent());
        addComponent(new ComponentState(row, minimumQuantities, group));
    }
}
