/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.DocumentActEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;


/**
 * Editor for <em>act.customerDocument*</em> acts.
 *
 * @author Tony De Keizer
 */
public class CustomerDocumentActEditor extends DocumentActEditor {

    /**
     * Constructs a {@link CustomerDocumentActEditor}.
     *
     * @param act     the act
     * @param parent  the parent
     * @param context the layout context
     */
    public CustomerDocumentActEditor(DocumentAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        initParticipant("customer", context.getContext().getCustomer());
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        // TODO - could leave hanging documents
        return new CustomerDocumentActEditor(reload(getObject()), reload(getParent()), getLayoutContext());
    }
}
