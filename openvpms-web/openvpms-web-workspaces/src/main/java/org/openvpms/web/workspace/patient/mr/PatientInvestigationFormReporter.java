/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.TemporaryDocumentHandler;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.laboratory.internal.service.OrdersImpl;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.report.IMReport;
import org.openvpms.report.ReportException;
import org.openvpms.report.ReportFactory;
import org.openvpms.report.i18n.ReportMessages;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.TemplatedReporter;
import org.openvpms.web.system.ServiceHelper;

import java.io.IOException;


/**
 * A {@link Reporter} for <em>act.patientInvestigation</em> acts to print laboratory request forms.
 * <p/>
 * This uses the template associated with the investigation type, if present.<br/>
 * If not, it attempts to locate a document returned by the {@link LaboratoryService#getRequestForm(Order)}, if
 * the investigation has an order.
 *
 * @author Tim Anderson
 */
public class PatientInvestigationFormReporter extends TemplatedReporter<DocumentAct> {

    /**
     * The report factory.
     */
    private final ReportFactory reportFactory;

    /**
     * The laboratory order service.
     */
    private final OrdersImpl orders;

    /**
     * The laboratory services.
     */
    private final LaboratoryServices laboratoryServices;

    /**
     * The report.
     */
    private IMReport<DocumentAct> report;

    /**
     * Constructs a {@link PatientInvestigationFormReporter}.
     *
     * @param act       the act
     * @param formatter the file name formatter
     * @param service   the archetype service
     * @param lookups   the lookup service
     */
    public PatientInvestigationFormReporter(DocumentAct act, FileNameFormatter formatter, ArchetypeService service,
                                            LookupService lookups, ReportFactory reportFactory) {
        super(act, new InvestigationTypeTemplateLocator(act, service), formatter, service, lookups);
        this.reportFactory = reportFactory;
        orders = ServiceHelper.getBean(OrdersImpl.class);
        laboratoryServices = ServiceHelper.getBean(LaboratoryServices.class);
    }

    /**
     * Determines if the investigation has a document.
     *
     * @return {@code true} if the investigation has a document, otherwise {@code false}
     */
    public boolean hasDocument() {
        try {
            getReport();
        } catch (ReportException exception) {
            // do nothing
        }
        return report != null;
    }

    /**
     * Returns the report.
     *
     * @return the report
     * @throws OpenVPMSException         for any error
     * @throws ReportException           for any report error
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    protected IMReport<DocumentAct> getReport() {
        if (report == null) {
            report = createReport();
        }
        return report;
    }

    /**
     * Creates a report.
     * <p/>
     * This implementation uses the template associated with the investigation type, if present.
     * If not, attempts to locate a document returned by the {@link LaboratoryService#getRequestForm(Order)}, if
     * the investigation has an order.
     *
     * @return a new report
     * @throws ReportException if no template exists
     */
    @SuppressWarnings("unchecked")
    protected IMReport<DocumentAct> createReport() {
        IMReport<IMObject> result;
        DocumentTemplate template = getTemplate();
        if (template != null) {
            result = reportFactory.isIMObjectReport(template)
                     ? reportFactory.createIMObjectReport(template)
                     : reportFactory.createStaticContentReport(template);
        } else {
            result = createLaboratoryReport();
        }
        if (result == null) {
            String displayName = DescriptorHelper.getDisplayName(getObject(), getService());
            throw new ReportException(ReportMessages.noDocument(displayName));
        }
        return (IMReport<DocumentAct>) (IMReport<?>) result;
    }

    /**
     * Creates a report using the document provided by the laboratory, if any.
     *
     * @return a new report or {@link null} if none exists
     */
    private IMReport<IMObject> createLaboratoryReport() {
        IMReport<IMObject> result = null;
        ArchetypeService service = getService();
        DocumentAct investigation = getObject();
        Entity laboratory = service.getBean(investigation).getTarget("laboratory", Entity.class);
        if (laboratory != null) {
            Order order = orders.getOrder(investigation);
            if (order != null) {
                LaboratoryService laboratoryService = laboratoryServices.getService(laboratory);
                org.openvpms.laboratory.order.Document document = laboratoryService.getRequestForm(order);
                if (document != null) {
                    result = reportFactory.createStaticContentReport(convert(document));
                }
            }
        }
        return result;
    }

    /**
     * Converts a laboratory document.
     *
     * @param document the document to convert
     * @return the converted document
     */
    private Document convert(org.openvpms.laboratory.order.Document document) {
        try {
            TemporaryDocumentHandler handler = new TemporaryDocumentHandler(getService());
            return handler.create(document.getName(), document.getInputStream(), document.getMimeType(), -1);
        } catch (IOException exception) {
            throw new ReportException(ReportMessages.failedToCreateReport(document.getName(), exception.getMessage()),
                                      exception);
        }
    }

    /**
     * Locates the template associated with an investigation's investigation type.
     */
    private static class InvestigationTypeTemplateLocator implements DocumentTemplateLocator {

        /**
         * The cached template.
         */
        DocumentTemplate template;

        /**
         * The investigation.
         */
        private final DocumentAct investigation;

        /**
         * The archetype service.
         */
        private final ArchetypeService service;

        public InvestigationTypeTemplateLocator(DocumentAct investigation, ArchetypeService service) {
            this.investigation = investigation;
            this.service = service;
        }

        /**
         * Returns the document template.
         *
         * @return the document template, or {@code null} if the template cannot be located
         */
        @Override
        public DocumentTemplate getTemplate() {
            if (template == null) {
                IMObjectBean act = service.getBean(investigation);
                Entity investigationType = act.getTarget("investigationType", Entity.class);
                if (investigationType != null) {
                    IMObjectBean bean = service.getBean(investigationType);
                    Entity entity = bean.getTarget("template", Entity.class);
                    if (entity != null) {
                        template = new DocumentTemplate(entity, service);
                    }
                }
            }
            return template;
        }

        /**
         * Returns the type that the template applies to.
         *
         * @return the type. Corresponds to an <em>lookup.documentTemplateType</em> code
         */
        @Override
        public String getType() {
            return template != null ? template.getType() : PatientArchetypes.DOCUMENT_FORM;
        }
    }
}
