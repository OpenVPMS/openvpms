/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.eftpos.transaction.Payment;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.web.resource.i18n.Messages;

/**
 * EFT helper methods.
 *
 * @author Tim Anderson
 */
class EFTHelper {

    /**
     * Default constructor.
     */
    private EFTHelper() {
        // no-op
    }

    /**
     * Returns the appropriate EFT dialog title for a transaction.
     *
     * @param transaction the transaction
     * @return the dialog title
     */
    public static String getDialogTitle(IMObject transaction) {
        return getDialogTitle(transaction.isA(EFTPOSArchetypes.PAYMENT, CustomerAccountArchetypes.PAYMENT_EFT));
    }

    /**
     * Returns the appropriate EFT dialog title for a transaction.
     *
     * @param transaction the transaction
     * @return the dialog title
     */
    public static String getDialogTitle(Transaction transaction) {
        return getDialogTitle(transaction instanceof Payment);
    }

    /**
     * Returns the appropriate EFT dialog title for a transaction.
     *
     * @param payment if {@code true}, the transaction is a payment, else it is a refund
     * @return the dialog title
     */
    public static String getDialogTitle(boolean payment) {
        return payment ? Messages.get("customer.payment.eft.title") : Messages.get("customer.refund.eft.title");
    }
}
