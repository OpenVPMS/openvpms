/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.sms;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.sms.SMSDialog;
import org.openvpms.web.component.im.sms.SMSEditor;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.macro.MacroVariables;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.appointment.reminder.AppointmentReminderEvaluator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Date;
import java.util.List;

/**
 * Dialog for sending SMS associated with an appointment.
 *
 * @author Tim Anderson
 */
public class AppointmentSMSDialog extends SMSDialog {

    /**
     * The appointment.
     */
    private final Act appointment;

    /**
     * Constructs an {@link AppointmentSMSDialog}.
     *
     * @param appointment the appointment
     * @param phones      the phone numbers to select from. May be {@code null}
     * @param customer    the customer
     * @param patient     the patient
     * @param location    the practice location
     * @param context     the context
     * @param help        the help context
     */
    public AppointmentSMSDialog(Act appointment, List<Contact> phones, Party customer, Party patient, Party location,
                                Context context, HelpContext help) {
        super(phones, createContext(customer, patient, context), help);
        this.appointment = appointment;
        SMSEditor editor = getEditor();
        editor.setSource(appointment);

        Entity template = SMSHelper.getAppointmentTemplate(location);
        if (template != null) {
            try {
                AppointmentReminderEvaluator evaluator
                        = ServiceHelper.getBean(AppointmentReminderEvaluator.class);
                String message = evaluator.evaluate(template, appointment, location, context.getPractice());
                setMessage(message);
            } catch (Throwable exception) {
                ErrorHelper.show(exception);
            }
        }

        resize("AppointmentSMSDialog.size");
    }

    /**
     * Returns the appointment.
     * <p/>
     * This returns the latest instance.
     *
     * @return the appointment
     */
    public Act getAppointment() {
        Act result = IMObjectHelper.reload(appointment);
        return result != null ? result : appointment;
    }

    /**
     * Sends the message.
     *
     * @return {@code true} if the message was sent
     */
    @Override
    protected boolean send() {
        boolean send = super.send();
        if (send) {
            PlatformTransactionManager transactionManager = ServiceHelper.getTransactionManager();
            TransactionTemplate template = new TransactionTemplate(transactionManager);
            template.executeWithoutResult(transactionStatus -> updateAppointment());
        }
        return send;
    }

    /**
     * Creates an SMS editor.
     *
     * @param phones     the phones
     * @param variables  the variables
     * @param context    the context
     * @param smsService the SMS service
     * @return the editor
     */
    @Override
    protected SMSEditor createEditor(List<Contact> phones, MacroVariables variables, Context context,
                                     SMSService smsService) {
        return new AppointmentSMSEditor(phones, variables, context, smsService);
    }

    /**
     * Returns the editor.
     *
     * @return the editor
     */
    @Override
    protected AppointmentSMSEditor getEditor() {
        return (AppointmentSMSEditor) super.getEditor();
    }

    /**
     * Updates the appointment to indicate that a reminder has been sent.
     */
    private void updateAppointment() {
        Act reloaded = IMObjectHelper.reload(appointment);
        if (reloaded != null) {
            AppointmentRules rules = ServiceHelper.getBean(AppointmentRules.class);
            Date date = getEditor().disableReminder() ? new Date() : null;
            rules.setSMSReminderSent(reloaded, date);
        }
    }

    /**
     * Creates a local context with the specified customer and patient.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param parent   the parent context to inherit from
     * @return a new local context
     */
    private static Context createContext(Party customer, Party patient, Context parent) {
        LocalContext context = new LocalContext(parent);
        context.setCustomer(customer);
        context.setPatient(patient);
        return context;
    }
}
