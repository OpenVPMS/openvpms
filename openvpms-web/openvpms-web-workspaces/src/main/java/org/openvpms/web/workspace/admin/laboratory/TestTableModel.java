/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.table.DescriptorTableColumn;

/**
 * Table model for <em>entity.laboratoryTest</em>.
 *
 * @author Tim Anderson
 */
public class TestTableModel extends DefaultDescriptorTableModel<Entity> {

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes NODES = ArchetypeNodes.nodes("id", "code", "name", "investigationType",
                                                                     "turnaround", "specimen");

    /**
     * Constructs a {@link TestTableModel}.
     *
     * @param archetypes the archetypes. May contain wildcards
     * @param context    the layout context
     */
    public TestTableModel(String[] archetypes, LayoutContext context) {
        super(archetypes, context, NODES);
    }

    /**
     * Returns a value for a given column.
     *
     * @param object the object to operate on
     * @param column the column
     * @param row    the row
     * @return the value for the column
     */
    @Override
    protected Object getValue(Entity object, DescriptorTableColumn column, int row) {
        String name = column.getName();
        if (name.equals("turnaround")) {
            return getTruncatedValue(object, column, 40);
        } else if (name.equals("specimen")) {
            return getTruncatedValue(object, column, 80);
        }
        return super.getValue(object, column, row);
    }
}
