/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.search;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.reporting.charge.AbstractChargesCRUDWindow;

/**
 * The charges CRUD window.
 *
 * @author Tim Anderson
 */
public class ChargesCRUDWindow extends AbstractChargesCRUDWindow {

    /**
     * Constructs a {@link ChargesCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param browser    the query browser
     * @param context    the context
     * @param help       the help context
     */
    public ChargesCRUDWindow(Archetypes<FinancialAct> archetypes, QueryBrowser<FinancialAct> browser, Context context,
                             HelpContext help) {
        super(archetypes, browser, "CHARGES", context, help);
    }

}
