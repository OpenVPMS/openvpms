/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.CollectionResultSetFactory;
import org.openvpms.web.component.im.edit.DefaultCollectionResultSetFactory;
import org.openvpms.web.component.im.edit.DefaultRemoveConfirmationHandler;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.ProductTemplateExpander;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.table.ListMarkModel;
import org.openvpms.web.component.im.table.MarkablePagedIMObjectTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.button.CheckBox;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.workspace.customer.PriceActItemEditor;
import org.openvpms.web.workspace.patient.mr.DoseManager;

import java.util.List;

import static org.openvpms.archetype.rules.prefs.PreferenceArchetypes.CHARGE;

/**
 * Editor for collections of {@link ActRelationship}s belonging to charges and estimates.
 * <p/>
 * This provides an {@link DoseManager} to {@link PriceActItemEditor} instances.
 *
 * @author Tim Anderson
 */
public abstract class AbstractChargeItemRelationshipCollectionEditor extends ActRelationshipCollectionEditor {

    /**
     * The edit context.
     */
    private final PriceActEditContext editContext;

    /**
     * Show batch node.
     */
    static final String SHOW_BATCH = "showBatch";

    /**
     * Show template node.
     */
    static final String SHOW_TEMPLATE = "showTemplate";

    /**
     * Show product type node.
     */
    static final String SHOW_PRODUCT_TYPE = "showProductType";

    /**
     * Show department node.
     */
    private static final String SHOW_DEPARTMENT = "showDepartment";

    /**
     * Constructs an {@link AbstractChargeItemRelationshipCollectionEditor}
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public AbstractChargeItemRelationshipCollectionEditor(CollectionProperty property, Act act, LayoutContext context,
                                                          PriceActEditContext editContext) {
        this(property, act, context, DefaultCollectionResultSetFactory.INSTANCE, editContext);
    }

    /**
     * Constructs an {@link AbstractChargeItemRelationshipCollectionEditor}
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     * @param factory  the result set factory
     */
    public AbstractChargeItemRelationshipCollectionEditor(CollectionProperty property, Act act,
                                                          LayoutContext context, CollectionResultSetFactory factory,
                                                          PriceActEditContext editContext) {
        super(property, act, context, factory);
        this.editContext = editContext;
    }

    /**
     * Unmarks all charge items.
     */
    public void unmarkAll() {
        MarkablePagedIMObjectTableModel<IMObject> model
                = (MarkablePagedIMObjectTableModel<IMObject>) getTable().getModel();
        model.unmarkAll();
    }

    /**
     * Invoked when the "Add" button is pressed. Creates a new instance of the selected archetype, and displays it in
     * an editor.
     *
     * @return the new editor, or {@code null} if one could not be created
     */
    @Override
    protected IMObjectEditor onAdd() {
        unmarkAll();
        return super.onAdd();
    }

    /**
     * Returns the edit context.
     *
     * @return the edit context
     */
    protected PriceActEditContext getEditContext() {
        return editContext;
    }

    /**
     * Creates a new paged table.
     *
     * @param model the table model
     * @return a new paged table
     */
    @Override
    protected PagedIMTable<IMObject> createTable(IMTableModel<IMObject> model) {
        return new PagedIMTable<>(new MarkablePagedIMObjectTableModel<>((IMObjectTableModel<IMObject>) model));
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    @SuppressWarnings("unchecked")
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        context = new DefaultLayoutContext(context);
        TableComponentFactory factory = new TableComponentFactory(context);
        factory.setTruncateLongText(true);
        context.setComponentFactory(factory);
        ChargeItemTableModel<?> model = createChargeItemTableModel(context);
        ListMarkModel rowMarkModel = model.getRowMarkModel();
        if (rowMarkModel != null) {
            rowMarkModel.addListener(new ListMarkModel.Listener() {
                @Override
                public void changed(int index, boolean marked) {
                    enableDelete();
                }

                @Override
                public void cleared() {
                    enableDelete();
                }
            });
        }
        return (IMTableModel<IMObject>) model;
    }

    /**
     * Creates a new {@link ChargeItemTableModel}.
     *
     * @param context the layout context
     * @return a new table model
     */
    protected ChargeItemTableModel<Act> createChargeItemTableModel(LayoutContext context) {
        return new ChargeItemTableModel<>(getCollectionPropertyEditor().getArchetypeRange(), null, context);
    }

    /**
     * Creates the row of controls.
     * <p/>
     * This provides checkboxes to show/hide the template, product type and batch columns.
     *
     * @param focus the focus group
     * @return the row of controls
     */
    @Override
    protected ButtonRow createControls(FocusGroup focus) {
        ButtonRow controls = super.createControls(focus);
        Preferences preferences = getContext().getPreferences();
        boolean showBatch = preferences.getBoolean(CHARGE, SHOW_BATCH, false);
        boolean showTemplate = preferences.getBoolean(CHARGE, SHOW_TEMPLATE, false);
        boolean showProductType = preferences.getBoolean(CHARGE, SHOW_PRODUCT_TYPE, false);
        boolean showDepartment = preferences.getBoolean(CHARGE, SHOW_DEPARTMENT, false);

        ChargeItemTableModel<Act> model = getModel();
        if (model.hasBatch()) {
            CheckBox batch = CheckBoxFactory.create("customer.charge.show.batch", showBatch);
            batch.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    boolean selected = batch.isSelected();
                    preferences.setPreference(CHARGE, SHOW_BATCH, selected);
                    getModel().setShowBatch(preferences.getBoolean(CHARGE, SHOW_BATCH, false));
                }
            });
            controls.add(batch);
        }

        CheckBox template = CheckBoxFactory.create("customer.charge.show.template", showTemplate);
        CheckBox productType = CheckBoxFactory.create("customer.charge.show.productType", showProductType);
        template.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                boolean selected = template.isSelected();
                preferences.setPreference(CHARGE, SHOW_TEMPLATE, selected);
                getModel().setShowTemplate(selected);
            }
        });
        productType.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                boolean selected = productType.isSelected();
                preferences.setPreference(CHARGE, SHOW_PRODUCT_TYPE, selected);
                getModel().setShowProductType(selected);
            }
        });
        controls.add(template);
        controls.add(productType);
        if (editContext.useDepartments()) {
            CheckBox department = CheckBoxFactory.create("customer.charge.show.department", showDepartment);
            department.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    boolean selected = department.isSelected();
                    preferences.setPreference(CHARGE, SHOW_DEPARTMENT, selected);
                    getModel().setShowDepartment(preferences.getBoolean(CHARGE, SHOW_DEPARTMENT, false));
                }
            });
            controls.add(department);
        }
        return controls;
    }

    /**
     * Determines if the delete button should be enabled.
     *
     * @return {@code true} if the delete button should be enabled, {@code false} if it should be disabled
     */
    @Override
    protected boolean getEnableDelete() {
        boolean result = super.getEnableDelete();
        if (!result) {
            result = ((MarkablePagedIMObjectTableModel<IMObject>) getTable().getModel()).isMarked();
        }
        return result;
    }

    /**
     * Creates a new product template expander.
     * <p/>
     * This implementation will restrict products to those of the location and stock location,
     * if {@link PriceActEditContext#useLocationProducts()} is {@code true}.
     *
     * @return a new product template expander
     */
    @Override
    protected ProductTemplateExpander getProductTemplateExpander() {
        PriceActEditContext context = getEditContext();
        return new ProductTemplateExpander(context.useLocationProducts(), context.getLocation(),
                                           context.getStockLocation());
    }

    /**
     * Invoked when the 'delete' button is pressed.
     * If the selected object has been saved, delegates to the registered
     * {@link #getRemoveConfirmationHandler() RemoveConfirmationHandler},
     * or uses {@link DefaultRemoveConfirmationHandler} if none is registered.
     */
    protected void onDelete() {
        List<IMObject> items = getMarked();
        if (!items.isEmpty()) {
            ChargeRemoveConfirmationHandler handler = (ChargeRemoveConfirmationHandler) getRemoveConfirmationHandler();
            handler.remove(items, this);
        } else {
            super.onDelete();
        }
    }

    /**
     * Returns the marked objects.
     *
     * @return the marked objects
     */
    protected List<IMObject> getMarked() {
        MarkablePagedIMObjectTableModel<IMObject> model
                = (MarkablePagedIMObjectTableModel<IMObject>) getTable().getModel();
        return model.getMarked(getCurrentObjects());
    }

    /**
     * Returns the table.
     *
     * @return the table
     */
    @Override
    protected PagedIMTable<IMObject> getTable() {
        return super.getTable();
    }

    /**
     * Returns the underlying table model.
     *
     * @return the model
     */
    @SuppressWarnings("unchecked")
    protected ChargeItemTableModel<Act> getModel() {
        IMTableModel<?> model = getTable().getModel().getModel();
        return (ChargeItemTableModel<Act>) model;
    }
}
