/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;

import java.util.Date;
import java.util.List;


/**
 * Provides a time-range view of a set of appointments.
 *
 * @author Tim Anderson
 */
class AppointmentGridView extends AbstractAppointmentGrid {

    /**
     * The grid to filter appointments from.
     */
    private final AppointmentGrid grid;

    /**
     * The slots.
     */
    private final List<Slot> slots;

    /**
     * The starting slot to view from.
     */
    private int startSlot;

    /**
     * Constructs an {@link AppointmentGridView}.
     *
     * @param grid          the grid to filter appointments from
     * @param startMins     the start time of the view, as minutes from midnight
     * @param endMins       the end time of the view, as minutes from midnight
     * @param rules         the appointment rules
     * @param rosterService the roster service
     */
    public AppointmentGridView(AppointmentGrid grid, int startMins, int endMins, AppointmentRules rules,
                               RosterService rosterService) {
        super(grid.getScheduleView(), grid.getStartDate(), startMins, endMins, rules, rosterService);
        this.grid = grid;
        startSlot = grid.getSlot(getSlotTimeForMinutes(startMins));
        if (startSlot == -1) {
            if (grid.getSlots() > 0) {
                startSlot = grid.getSlots() - 1;
            } else {
                startSlot = 0;
            }
        }
        int endSlot = grid.getSlot(getSlotTimeForMinutes(endMins));
        if (endSlot >= grid.getSlots()) {
            endSlot = grid.getSlots() - 1;
        }
        slots = grid.getSlotTimes().subList(startSlot, endSlot + 1);
        setSlotSize(grid.getSlotSize());
    }

    /**
     * Returns the schedules.
     *
     * @return the schedules
     */
    public List<Schedule> getSchedules() {
        return grid.getSchedules();
    }

    /**
     * Returns the appointment for the specified schedule and slot.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the corresponding appointment, or {@code null} if none is found
     */
    public PropertySet getEvent(Schedule schedule, int slot) {
        PropertySet result = null;
        if (slot <= getSlots()) {
            result = grid.getEvent(schedule, startSlot + slot);
            if (result == null && slot == 0) {
                // see if there is an event in an earlier slot that intersects this one
                Date start = getStartTime(schedule, slot);
                Date end = getEndTime(schedule, slot);
                result = schedule.getIntersectingEvent(start, end, getSlotSize());
            }
        }
        return result;
    }

    /**
     * Returns the no. of slots that an event occupies, from the specified slot.
     * <p/>
     * If the event begins prior to the slot, the remaining slots will be returned.
     *
     * @param event    the event
     * @param schedule the schedule
     * @param slot     the starting slot  @return the no. of slots that the event occupies
     */
    @Override
    public int getSlots(PropertySet event, Schedule schedule, int slot) {
        return grid.getSlots(event, schedule, startSlot + slot);
    }

    /**
     * Returns the no. of slots in the grid.
     *
     * @return the no. of slots
     */
    @Override
    public int getSlots() {
        return slots.size();
    }

    /**
     * Returns the time that the specified slot starts at.
     *
     * @param slot the slot
     * @return the start time of the specified slot
     */
    @Override
    public Date getStartTime(int slot) {
        return grid.getStartTime(startSlot + slot);
    }

    /**
     * Returns the time that the specified slot ends at.
     *
     * @param slot the slot
     * @return the end time of the specified slot
     */
    @Override
    public Date getEndTime(int slot) {
        return grid.getEndTime(startSlot + slot);
    }

    /**
     * Returns the time that the specified slot starts at.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the start time of the specified slot
     */
    @Override
    public Date getStartTime(Schedule schedule, int slot) {
        return grid.getStartTime(schedule, startSlot + slot);
    }

    /**
     * Returns the hour of the specified slot.
     *
     * @param slot the slot
     * @return the hour, in the range 0..23
     */
    @Override
    public int getHour(int slot) {
        return grid.getHour(startSlot + slot);
    }

    /**
     * Returns the slot that a time falls in.
     *
     * @param time the time
     * @return the slot, or {@code -1} if the time doesn't intersect any slot
     */
    @Override
    public int getSlot(Date time) {
        int slot = grid.getSlot(time);
        return (slot >= startSlot) ? slot - startSlot : -1;
    }

    /**
     * Determines the availability of a slot for the specified schedule.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the availability of the schedule
     */
    @Override
    public Availability getAvailability(Schedule schedule, int slot) {
        return grid.getAvailability(schedule, startSlot + slot);
    }

    /**
     * Returns the slot times.
     *
     * @return the slot time
     */
    @Override
    public List<Slot> getSlotTimes() {
        return slots;
    }

    /**
     * Determines if the roster should be displayed.
     *
     * @param show if {@code true}, show the roster
     */
    @Override
    public void setShowRoster(boolean show) {
        grid.setShowRoster(show);
    }

    /**
     * Determines if the roster should be displayed.
     *
     * @return {@code true} if the roster should be displayed
     */
    @Override
    public boolean showRoster() {
        return grid.showRoster();
    }
}
