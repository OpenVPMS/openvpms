/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.document;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.DocumentActEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;


/**
 * Editor for <em>act.supplierDocument*</em> acts.
 *
 * @author Tim Anderson
 */
public class SupplierDocumentActEditor extends DocumentActEditor {

    /**
     * Constructs a {@link SupplierDocumentActEditor}.
     *
     * @param act     the act
     * @param parent  the parent
     * @param context the layout context
     */
    public SupplierDocumentActEditor(DocumentAct act, IMObject parent,
                                     LayoutContext context) {
        super(act, parent, context);
        initParticipant("supplier", context.getContext().getSupplier());
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        // TODO this can leave documents hanging
        return new SupplierDocumentActEditor(reload(getObject()), getParent(), getLayoutContext());
    }
}
