/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.account;

import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.PeriodHelper;
import org.openvpms.component.business.service.archetype.helper.IMObjects;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.edit.Deletable;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityLinkCollectionTargetPropertyEditor;
import org.openvpms.web.component.im.relationship.MultipleRelationshipCollectionTargetEditor;
import org.openvpms.web.component.im.relationship.SequencedCollectionResultSetFactory;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Editor for a collection of <em>entity.accountReminderCount</em>.
 * <p/>
 * This displays the objects on increasing interval.
 *
 * @author Tim Anderson
 */
class AccountReminderCollectionEditor extends MultipleRelationshipCollectionTargetEditor implements Deletable {

    /**
     * Constructs an {@link AccountReminderCollectionEditor}.
     *
     * @param property the collection property
     * @param object   the parent object
     * @param context  the layout context
     */
    public AccountReminderCollectionEditor(CollectionProperty property, Entity object, LayoutContext context) {
        super(new AccountReminderPropertyEditor(property, object, context.getCache()), object, context,
              SequencedCollectionResultSetFactory.INSTANCE);
    }

    /**
     * Adds an object to the collection, if it doesn't exist.
     *
     * @param object the object to add
     * @return {@code true} if the object was added, otherwise {@code false}
     */
    @Override
    public boolean add(IMObject object) {
        boolean result = super.add(object);
        resequence();
        return result;
    }

    /**
     * Removes an object from the collection.
     *
     * @param object the object to remove
     */
    @Override
    public void remove(IMObject object) {
        super.remove(object);
        resequence();
    }

    /**
     * Perform deletion.
     *
     * @throws OpenVPMSException if the delete fails
     */
    @Override
    public void delete() {
        IMObjectBean bean = getBean(getObject());
        // need to remove relationships to the parent object and save it, before removing the counts
        boolean changed = false;
        Collection<IMObject> counts = getCurrentObjects();
        for (IMObject count : counts) {
            Relationship relationship = bean.removeTarget("reminders", count);
            if (relationship != null) {
                changed = true;
            }
        }
        if (changed) {
            // need to disable validation in order to delete as at least one reminder count is required to save
            ServiceHelper.getArchetypeService().save(getObject(), false);
            // remove the counts
            for (IMObject count : counts) {
                IMObjectEditor editor = getEditor(count);
                if (editor != null) {
                    editor.delete();
                }
            }
        }
    }

    /**
     * Returns the interval of an <em>entity.accountReminderCount</em>.
     *
     * @param object the count
     * @return the interval
     */
    protected static Period getInterval(IMObject object) {
        return PeriodHelper.getPeriod(ServiceHelper.getArchetypeService().getBean(object), "interval", "units",
                                      Period.days(0));
    }

    /**
     * Resequences the relationships on increasing interval.
     */
    private void resequence() {
        AccountReminderPropertyEditor editor = (AccountReminderPropertyEditor) getCollectionPropertyEditor();
        editor.sequence();
    }

    private static class AccountReminderPropertyEditor extends EntityLinkCollectionTargetPropertyEditor {

        /**
         * Constructs an {@link AccountReminderPropertyEditor}.
         *
         * @param property the property to edit
         * @param parent   the parent object
         * @param objects  the objects service
         */
        public AccountReminderPropertyEditor(CollectionProperty property, Entity parent, IMObjects objects) {
            super(property, parent, objects);
        }

        /**
         * Sequences the relationships.
         */
        @Override
        protected void sequence() {
            Map<IMObject, EntityLink> map = getTargets();
            List<Map.Entry<IMObject, EntityLink>> entries = new ArrayList<>(map.entrySet());
            Date now = new Date();
            entries.sort((o1, o2) -> {
                Period period1 = getInterval(o1.getKey());
                Period period2 = getInterval(o2.getKey());
                return DateRules.plus(now, period1).compareTo(DateRules.plus(now, period2));
            });
            int sequence = 0;
            for (Map.Entry<IMObject, EntityLink> entry : entries) {
                EntityLink link = entry.getValue();
                link.setSequence(sequence++);
            }
        }
    }
}
