/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.Extent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectReferenceViewer;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

import static org.openvpms.web.component.im.layout.ArchetypeNodes.find;

/**
 * Layout strategy for <em>act.smsMessage</em>.
 *
 * @author Tim Anderson
 */
public class SMSMessageLayoutStrategy extends AbstractSMSMessageLayoutStrategy {

    /**
     * The status node.
     */
    private static final String STATUS = "status";

    /**
     * The status message node.
     */
    private static final String STATUS_MESSAGE = "statusMessage";

    /**
     * The expiry time node.
     */
    private static final String EXPIRY_TIME = "expiryTime";

    /**
     * The replies node.
     */
    private static final String REPLIES = "replies";

    /**
     * The source node.
     */
    private static final String SOURCE = "source";


    /**
     * Constructs an {@link SMSMessageLayoutStrategy}.
     */
    public SMSMessageLayoutStrategy() {
        this(true);
    }

    /**
     * Constructs an {@link SMSMessageLayoutStrategy}.
     *
     * @param showPatient if {@code true}, show the patient
     */
    public SMSMessageLayoutStrategy(boolean showPatient) {
        super(showPatient, CREATED_TIME);
        ArchetypeNodes nodes = ArchetypeNodes.all()
                .exclude(CONTACT, STATUS_MESSAGE)
                .excludeIfEmpty(EXPIRY_TIME, REPLIES);
        setArchetypeNodes(nodes);
    }

    /**
     * Apply the layout strategy.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        // render the status and status message together, if there is a status message
        Property statusMessage = properties.get(STATUS_MESSAGE);
        if (!StringUtils.isEmpty(statusMessage.getString())) {
            addComponent(createMultiLineText(statusMessage, 1, 3, new Extent(50, Extent.EX), context));
            addComponent(createComponentPair(STATUS, STATUS_MESSAGE, object, properties, context));
        }
        getArchetypeNodes().exclude(SOURCE);
        if (!context.isEdit() && context.getContextSwitchListener() != null) {
            Reference reference = getBean(object).getSourceRef(SOURCE);
            if (reference != null && reference.isA(ScheduleArchetypes.APPOINTMENT)) {
                String name = Messages.format("imobject.link", getDisplayName(reference.getArchetype()));
                IMObjectReferenceViewer viewer = new IMObjectReferenceViewer(reference, name,
                                                                             context.getContextSwitchListener(),
                                                                             context.getContext());
                addComponent(new ComponentState(viewer.getComponent(), properties.get(SOURCE)));
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns the text properties.
     * <p>
     * These are rendered under each other.
     * <p/>
     * If the statusMessage is present, this implementation includes the status and status message to ensure that
     * they don't take up too much horizontal space.
     *
     * @param properties the properties
     * @param message    the message property
     * @return the text properties
     */
    @Override
    protected List<Property> getTextProperties(List<Property> properties, Property message) {
        List<Property> result = super.getTextProperties(properties, message);
        if (getComponent(STATUS) != null) {
            // if the status and statusMessage have been rendered to together, include them
            Property status = find(properties, STATUS);
            if (status != null) {
                result.add(0, status);
            }
        }
        return result;
    }

    /**
     * Adds text components to the grid.
     * <p/>
     * This implementation adds the source, if present.
     *
     * @param grid       the grid
     * @param components the components to add
     */
    @Override
    protected void addTextComponents(ComponentGrid grid, ComponentSet components) {
        super.addTextComponents(grid, components);
        ComponentState source = getComponent(SOURCE);
        if (source != null) {
            grid.add(source.getComponent());
        }
    }
}
