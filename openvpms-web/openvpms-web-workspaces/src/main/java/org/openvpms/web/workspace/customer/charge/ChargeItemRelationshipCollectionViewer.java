/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DelegatingIMTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.view.act.ActRelationshipCollectionViewer;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.echo.button.CheckBox;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.archetype.rules.prefs.PreferenceArchetypes.CHARGE;


/**
 * Viewer for <em>actRelationship.customerAccountInvoiceItem</em> and
 * <em>actRelationship.customerAccountCreditItem</em> act relationships.
 * Sorts the items on descending start time.
 *
 * @author Tim Anderson
 */
public class ChargeItemRelationshipCollectionViewer extends ActRelationshipCollectionViewer {

    /**
     * Show batch node.
     */
    static final String SHOW_BATCH = "showBatch";

    /**
     * Show template node.
     */
    static final String SHOW_TEMPLATE = "showTemplate";

    /**
     * Show product type node.
     */
    static final String SHOW_PRODUCT_TYPE = "showProductType";

    /**
     * Show department node.
     */
    private static final String SHOW_DEPARTMENT = "showDepartment";

    /**
     * Constructs a {@link ChargeItemRelationshipCollectionViewer}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public ChargeItemRelationshipCollectionViewer(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
    }

    /**
     * Lays out the component.
     *
     * @return a new component
     */
    @Override
    protected Component doLayout() {
        Component component = super.doLayout();
        component.add(createControls(), 0);
        return component;
    }

    /**
     * Create controls to show/hide columns.
     *
     * @return a row of controls
     */
    protected Row createControls() {
        Row row = RowFactory.create(Styles.CELL_SPACING);

        // TODO - this largely duplicates code from AbstractChargeItemRelationshipCollectionEditor
        Preferences prefs = getLayoutContext().getPreferences();
        boolean showBatch = prefs.getBoolean(PreferenceArchetypes.CHARGE, SHOW_BATCH, false);
        boolean showTemplate = prefs.getBoolean(PreferenceArchetypes.CHARGE, SHOW_TEMPLATE, false);
        boolean showProductType = prefs.getBoolean(PreferenceArchetypes.CHARGE, SHOW_PRODUCT_TYPE, false);
        boolean showDepartment = prefs.getBoolean(PreferenceArchetypes.CHARGE, SHOW_DEPARTMENT, false);

        ChargeItemTableModel<?> model = getModel();
        if (model != null) {
            if (model.hasBatch()) {
                CheckBox batch = CheckBoxFactory.create("customer.charge.show.batch", showBatch);
                batch.addActionListener(new ActionListener() {
                    @Override
                    public void onAction(ActionEvent event) {
                        boolean selected = batch.isSelected();
                        prefs.setPreference(PreferenceArchetypes.CHARGE, SHOW_BATCH, selected);
                        model.setShowBatch(prefs.getBoolean(PreferenceArchetypes.CHARGE, SHOW_BATCH, false));
                    }
                });
                row.add(batch);
            }

            CheckBox template = CheckBoxFactory.create("customer.charge.show.template", showTemplate);
            CheckBox productType = CheckBoxFactory.create("customer.charge.show.productType", showProductType);
            template.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    boolean selected = template.isSelected();
                    prefs.setPreference(PreferenceArchetypes.CHARGE, SHOW_TEMPLATE, selected);
                    model.setShowTemplate(selected);
                }
            });
            productType.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    boolean selected = productType.isSelected();
                    prefs.setPreference(PreferenceArchetypes.CHARGE, SHOW_PRODUCT_TYPE, selected);
                    model.setShowProductType(selected);
                }
            });
            row.add(template);
            row.add(productType);

            Party practice = getLayoutContext().getContext().getPractice();
            if (practice != null && ServiceHelper.getBean(PracticeRules.class).departmentsEnabled(practice)) {
                CheckBox department = CheckBoxFactory.create("customer.charge.show.department", showDepartment);
                department.addActionListener(new ActionListener() {
                    @Override
                    public void onAction(ActionEvent event) {
                        boolean selected = department.isSelected();
                        prefs.setPreference(CHARGE, SHOW_DEPARTMENT, selected);
                        getModel().setShowDepartment(prefs.getBoolean(CHARGE, SHOW_DEPARTMENT, false));
                    }
                });
                row.add(department);
            }
        }
        return row;
    }

    /**
     * Returns the table, creating it if it doesn't exist.
     *
     * @return the table
     */
    @Override
    protected PagedIMTable<IMObject> getTable() {
        return super.getTable();
    }

    /**
     * Returns the underlying table model.
     *
     * @return the underlying table model, or {@code null} if it is not of the expected type
     */
    protected ChargeItemTableModel<?> getModel() {
        IMTableModel<?> result = null;
        IMTableModel<?> relationshipModel = getTable().getModel().getModel();
        if (relationshipModel instanceof DelegatingIMTableModel) {
            result = ((DelegatingIMTableModel<?, ?>) relationshipModel).getModel();
        }
        return (result instanceof ChargeItemTableModel<?>) ? (ChargeItemTableModel<?>) result : null;
    }
}
