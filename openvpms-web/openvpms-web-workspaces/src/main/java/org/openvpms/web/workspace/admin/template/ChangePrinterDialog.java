/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.DefaultIMObjectTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.print.BoundPrinterField;
import org.openvpms.web.component.print.PrintHelper;
import org.openvpms.web.component.print.PrinterListModel;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.MessageDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Dialog to change or remove printers from practice locations and document templates.
 *
 * @author Tim Anderson
 */
public class ChangePrinterDialog extends MessageDialog {

    /**
     * The printer use manager.
     */
    private final PrinterUseManager manager;

    /**
     * The available printers.
     */
    private final List<PrinterReference> available;

    /**
     * The printer property.
     */
    private final Property printer;

    /**
     * The printer to change/remove.
     */
    private final BoundPrinterField printerField;

    /**
     * The property tracking the printer to change to.
     */
    private final Property to;

    /**
     * The printer to change to.
     */
    private final BoundPrinterField toPrinter;

    /**
     * If selected, the printer is being changed.
     */
    private final RadioButton change;

    /**
     * If selected, the printer is being removed.
     */
    private final RadioButton remove;

    /**
     * Table to display use of the printer.
     */
    private final PagedIMTable<Entity> table;

    /**
     * Container for the table.
     */
    private Column container;

    /**
     * Constructs a {@link ChangePrinterDialog}.
     *
     * @param help the help context. May be {@code null}
     */
    private ChangePrinterDialog(PrinterUseManager manager, List<PrinterReference> available, HelpContext help) {
        super(Messages.get("admin.changeprinter.title"), Messages.get("admin.changeprinter.message"), OK_CANCEL, help);
        this.manager = manager;
        this.available = available;
        ButtonGroup group = new ButtonGroup();
        printer = new SimpleProperty("printer", null, String.class, Messages.get("admin.changeprinter.select"));
        printerField = new BoundPrinterField(printer, manager.getPrinters());
        printerField.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onFromChanged();
            }
        });
        change = ButtonFactory.create("admin.changeprinter.change", group, this::onChangeSelected);
        remove = ButtonFactory.create("admin.changeprinter.remove", group, this::onRemoveSelected);
        to = new SimpleProperty("to", null, String.class);
        toPrinter = new BoundPrinterField(to, available);
        toPrinter.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onToPrinterSelected();
            }
        });
        table = new PagedIMTable<>(new DefaultIMObjectTableModel<>(true, true, false));
        change.setSelected(true);
        updateOK();
        resize("ChangePrinterDialog.size");
    }

    /**
     * Shows the change printer dialog.
     *
     * @param help the help context
     */
    public static void show(HelpContext help) {
        List<PrinterReference> available = PrintHelper.getPrinters();
        PrinterUseManager manager = new PrinterUseManager(ServiceHelper.getArchetypeService(),
                                                          ServiceHelper.getBean(PracticeService.class),
                                                          ServiceHelper.getBean(LocationRules.class),
                                                          ServiceHelper.getBean(PlatformTransactionManager.class));
        manager.refresh();
        if (manager.hasPrinters()) {
            ChangePrinterDialog dialog = new ChangePrinterDialog(manager, available, help.subtopic("changeprinter"));
            dialog.show();
        } else {
            InformationDialog.show(Messages.get("admin.changeprinter.title"),
                                   Messages.get("admin.changeprinter.noupdaterequired"));
        }
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        Label message = LabelFactory.create(true, true);
        message.setText(getMessage());
        ComponentGrid grid = new ComponentGrid();
        grid.add(LabelFactory.text(printer.getDisplayName()), printerField);
        grid.add(change, toPrinter);
        grid.add(remove);
        container = ColumnFactory.create(Styles.CELL_SPACING);
        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, message, grid.createGrid(), container);
        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
    }

    /**
     * Invoked when the 'OK' button is pressed.
     */
    @Override
    protected void onOK() {
        PrinterReference from = (PrinterReference) printerField.getSelectedItem();
        PrinterReference to = (PrinterReference) toPrinter.getSelectedItem();
        String title = Messages.get("admin.changeprinter.title");
        if (change.isSelected() && from != null && to != null) {
            ConfirmationDialog.show(title,
                                    Messages.format("admin.changeprinter.confirmchange", from.getName(), to.getName()),
                                    YES_NO, new PopupDialogListener() {
                        @Override
                        public void onYes() {
                            try {
                                manager.replace(from, to);
                            } catch (Exception exception) {
                                ErrorHelper.show(title, exception);
                                manager.refresh();
                            }
                            ChangePrinterDialog.super.onOK();
                        }

                        @Override
                        public void onNo() {
                            ChangePrinterDialog.this.onCancel();
                        }
                    });
        } else if (remove.isSelected() && from != null) {
            ConfirmationDialog.show(title, Messages.format("admin.changeprinter.confirmremove", from.getName()),
                                    YES_NO, new PopupDialogListener() {
                        @Override
                        public void onYes() {
                            try {
                                manager.remove(from);
                            } catch (Exception exception) {
                                ErrorHelper.show(title, exception);
                                manager.refresh();
                            }
                            ChangePrinterDialog.super.onOK();
                        }

                        @Override
                        public void onNo() {
                            ChangePrinterDialog.this.onCancel();
                        }
                    });
        }
    }

    /**
     * Invoked when the 'From' printer is changed.
     */
    @SuppressWarnings("unchecked")
    private void onFromChanged() {
        List<PrinterReference> list = new ArrayList<>(available);
        PrinterReference selected = (PrinterReference) printerField.getSelectedItem();
        if (selected != null) {
            list.remove(selected);
            List<Entity> use = (List<Entity>) (List<?>) manager.getUse(selected);
            use.sort((o1, o2) -> {
                int result = StringUtils.compare(o1.getArchetype(), o2.getArchetype());
                if (result == 0) {
                    result = StringUtils.compare(o1.getName(), o2.getName());
                }
                return result;
            });
            table.setResultSet(new ListResultSet<>(use, 5));
            Component component = table.getComponent();
            if (component.getParent() == null) {
                container.add(LabelFactory.create("admin.changeprinter.usedby"));
                container.add(component);
            }
        } else {
            container.removeAll();
        }
        toPrinter.setModel(new PrinterListModel(list));
        updateOK();
    }

    /**
     * Invoked when the Change button is selected.
     * <p/>
     * Enables the to-printer dropdown, and updates the OK button.
     */
    private void onChangeSelected() {
        toPrinter.setEnabled(true);
        updateOK();
    }

    /**
     * Invoked when the Remove button is selected.
     * <p/>
     * Disables the to-printer dropdown, and updates the OK button.
     */
    private void onRemoveSelected() {
        toPrinter.setEnabled(false);
        updateOK();
    }

    /**
     * Invoked when the to-printer is selected.
     * <p/>
     * Updates the OK button.
     */
    private void onToPrinterSelected() {
        updateOK();
    }

    /**
     * Enables the OK button if enough details are selected, else disables it.
     */
    private void updateOK() {
        boolean enabled = remove.isSelected()
                          || (change.isSelected() && printerField.getSelectedItem() != null
                              && toPrinter.getSelectedItem() != null);
        getButtons().setEnabled(OK_ID, enabled);
    }

}
