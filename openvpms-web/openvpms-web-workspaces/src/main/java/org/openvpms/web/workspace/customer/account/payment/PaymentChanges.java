/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.payment;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Tracks changes to payment items, for auditing purposes.
 *
 * @author Tim Anderson
 */
class PaymentChanges {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The changes. Map of old payment item to new payment item.
     */
    private final Map<FinancialAct, FinancialAct> changes = new LinkedHashMap<>();

    /**
     * Cosntructs a {@link PaymentChanges}.
     *
     * @param service the archetype service
     */
    public PaymentChanges(IArchetypeService service) {
        this.service = service;
    }

    /**
     * Adds a change.
     * <p/>
     * Note that this does not track unsaved changes.
     *
     * @param previousPayment the previous payment item
     * @param newPayment      the new payment item
     */
    public void addChange(FinancialAct previousPayment, FinancialAct newPayment) {
        if (!previousPayment.isNew()) {
            changes.put(newPayment, previousPayment);
        } else {
            // a new act is replacing another. The old entry will point to the persistent instance.
            changes.put(newPayment, changes.remove(previousPayment));
        }
    }

    /**
     * Generates an audit messages of changes.
     *
     * @param name      the user name
     * @param timestamp the timestamp to use for the message
     * @return an audit message, or {@code null} if there have been no auditable changes
     */
    public String getAuditMessage(String name, Date timestamp) {
        String result = null;
        if (!changes.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            String date = DateFormatter.formatDateTime(timestamp);
            for (Map.Entry<FinancialAct, FinancialAct> entry : changes.entrySet()) {
                FinancialAct persistent = entry.getValue();
                if (builder.length() != 0) {
                    builder.append('\n');
                }
                builder.append(Messages.format("customer.account.payment.audit", date, name,
                                               DescriptorHelper.getDisplayName(persistent, service),
                                               NumberFormatter.formatCurrency(persistent.getTotal()),
                                               DescriptorHelper.getDisplayName(entry.getKey(), service)));
            }
            result = builder.toString();
            changes.clear();
        }
        return result;
    }
}
