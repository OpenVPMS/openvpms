/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.IMObjectRelationshipCollectionTargetViewer;
import org.openvpms.web.component.im.table.BaseIMObjectTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.print.PrintHelper;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.resource.i18n.Messages;

import java.util.HashSet;
import java.util.Set;

/**
 * Displays printers associated with a practice location, including a status column that shows if they are
 * available/unknown.
 *
 * @author Tim Anderson
 */
public class LocationPrinterCollectionViewer extends IMObjectRelationshipCollectionTargetViewer {

    /**
     * The set of available printers.
     */
    private Set<PrinterReference> printers;

    /**
     * The print column model index.
     */
    private int printIndex;

    /**
     * Constructs a {@link LocationPrinterCollectionViewer}.
     *
     * @param property the collection to view
     * @param parent   the parent object
     * @param context  the layout context. May be {@code null}
     */
    public LocationPrinterCollectionViewer(CollectionProperty property, IMObject parent, LayoutContext context) {
        super(property, parent, context);
    }

    /**
     * Returns the available printers.
     *
     * @return the printers
     */
    protected Set<PrinterReference> getPrinters() {
        if (printers == null) {
            printers = new HashSet<>(PrintHelper.getPrinters());
        }
        return printers;
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        return new BaseIMObjectTableModel<IMObject>() {


            /**
             * Returns the value found at the given coordinate within the table.
             *
             * @param object the object
             * @param column the table column
             * @param row    the table row
             */
            @Override
            protected Object getValue(IMObject object, TableColumn column, int row) {
                Object result;
                if (column.getModelIndex() == printIndex) {
                    IMObjectBean bean = getBean(object);
                    PrinterReference printer = PrinterReference.fromString(bean.getString("printer"));
                    if (printer != null && getPrinters().contains(printer)) {
                        result = Messages.get("printer.status.available");
                    } else {
                        result = Messages.get("printer.status.unknown");
                    }
                } else {
                    result = super.getValue(object, column, row);
                }
                return result;
            }

            /**
             * Creates a new column model.
             *
             * @return a new column model
             */
            @Override
            protected TableColumnModel createTableColumnModel() {
                DefaultTableColumnModel model = new DefaultTableColumnModel();
                model.addColumn(createTableColumn(NAME_INDEX, "table.imobject.name"));
                printIndex = getNextModelIndex(model);
                model.addColumn(createTableColumn(printIndex, "printer.status"));
                return model;
            }
        };
    }
}
