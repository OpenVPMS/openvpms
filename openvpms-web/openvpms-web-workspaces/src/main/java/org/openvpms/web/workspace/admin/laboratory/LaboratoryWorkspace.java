/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;

/**
 * Laboratory workspace.
 *
 * @author Tim Anderson
 */
public class LaboratoryWorkspace extends ResultSetCRUDWorkspace<Entity> {

    /**
     * Constructs a {@link LaboratoryWorkspace}.
     *
     * @param context the context
     */
    public LaboratoryWorkspace(Context context) {
        super("admin.laboratory", context);
        setArchetypes(Entity.class, LaboratoryArchetypes.DEVICES, LaboratoryArchetypes.LABORATORY_SERVICES,
                      LaboratoryArchetypes.INVESTIGATION_TYPE, LaboratoryArchetypes.TEST);
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<Entity> createCRUDWindow() {
        QueryBrowser<Entity> browser = getBrowser();
        return new LaboratoryCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(),
                                        getContext(), getHelpContext());
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery() {
        Query<Entity> query = super.createQuery();
        query.setContains(true);
        return query;
    }

}
