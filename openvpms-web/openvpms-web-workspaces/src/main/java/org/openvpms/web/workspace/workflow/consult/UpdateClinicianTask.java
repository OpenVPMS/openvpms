/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.consult;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.workflow.AbstractUpdateActTask;
import org.openvpms.web.component.workflow.TaskContext;

/**
 * Task to update the clinician on an act, if it is not already set.
 * <p/>
 * The clinician is sourced from the context.
 *
 * @author Tim Anderson
 */
class UpdateClinicianTask extends AbstractUpdateActTask<Act> {

    /**
     * Constructs an {@link UpdateClinicianTask}.
     *
     * @param archetype the archetype of the object to update
     */
    public UpdateClinicianTask(String archetype) {
        this(archetype, true);
    }

    /**
     * Constructs an {@link UpdateClinicianTask}.
     *
     * @param archetype the archetype of the object to update
     * @param save      determines if the object should be saved
     */
    public UpdateClinicianTask(String archetype, boolean save) {
        super(archetype, save); // if save, retry if saving fails
    }

    /**
     * Determines if the object can be updated.
     *
     * @param object  the object
     * @param context the task context
     * @return {@code true}
     */
    @Override
    protected boolean canUpdate(Act object, TaskContext context) {
        boolean update = super.canUpdate(object, context);
        if (update) {
            update = context.getClinician() != null;
        }
        return update;
    }

    /**
     * Populates an object.
     *
     * @param object  the object to populate
     * @param context the task context
     */
    @Override
    protected void populate(Act object, TaskContext context) {
        User clinician = context.getClinician();
        if (clinician != null) {
            IMObjectBean bean = getBean(object);
            if (bean.getTargetRef("clinician") == null) {
                bean.setTarget("clinician", clinician);
            }
        }
    }
}