/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.component.business.service.archetype.functor.ActComparator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.practice.Location;
import org.openvpms.insurance.internal.InsuranceFactory;
import org.openvpms.insurance.service.GapInsuranceService;
import org.openvpms.insurance.service.InsuranceService;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.workflow.AbstractTask;
import org.openvpms.web.component.workflow.ConditionalTask;
import org.openvpms.web.component.workflow.ConfirmationTask;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.Tasks;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.insurance.claim.ClaimEditDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A task that prompts the user to claim against a patient's insurance policy if their provider supports it.
 *
 * @author Tim Anderson
 */
public class InsuranceClaimTask extends Tasks {

    /**
     * The active policies and their associated insurers, most recent policy first.
     */
    private final Map<Act, Party> activePolicies = new LinkedHashMap<>();

    /**
     * Determines if gap claims can be submitted.
     */
    private final boolean gapClaim;

    /**
     * The insurance rules.
     */
    private final InsuranceRules rules;

    /**
     * The selected policy.
     */
    private Act policy;

    /**
     * The selected insurer.
     */
    private Party insurer;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InsuranceClaimTask.class);

    /**
     * Constructs an {@link InsuranceClaimTask}.
     *
     * @param policies   the policies
     * @param gapClaim if {@code true}, make a gap claim
     * @param help     the help context
     */
    public InsuranceClaimTask(List<Act> policies, boolean gapClaim, HelpContext help) {
        super(help);
        rules = ServiceHelper.getBean(InsuranceRules.class);
        policies.sort(ActComparator.descending()); // most recent policy first
        for (Act policy : policies) {
            Party insurer = rules.getInsurer(policy);
            if (insurer != null) {
                activePolicies.put(policy, insurer);
            }
        }
        this.gapClaim = gapClaim;
        setRequired(false);
    }

    /**
     * Initialise any tasks.
     *
     * @param context the task context
     */
    @Override
    protected void initialise(TaskContext context) {
        if (!activePolicies.isEmpty()) {
            if (gapClaim) {
                // check if gap claims are available, and if so prompt to make a gap claim
                addTask(new GapClaimTask(context.getHelpContext()));
            } else {
                // prompt to make a standard claim
                ConfirmationTask confirm = createClaimConfirmationTask(context, activePolicies);
                addTask(new ConditionalTask(confirm, new EditClaimTask()));
            }
        }
    }

    private void setSelectedPolicy(Map.Entry<Act, Party> entry) {
        if (entry != null) {
            this.policy = entry.getKey();
            this.insurer = entry.getValue();
        } else {
            this.policy = null;
            this.insurer = null;
        }
    }

    /**
     * Task to edit a claim.
     */
    private class EditClaimTask extends EditIMObjectTask {

        /**
         * Constructs a new {@code EditIMObjectTask} to edit an object
         * in the {@link TaskContext}.
         */
        public EditClaimTask() {
            super(InsuranceArchetypes.CLAIM);
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         */
        @Override
        public void start(TaskContext context) {
            FinancialAct claim = rules.createClaim(policy);
            if (gapClaim) {
                IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(claim);
                bean.setValue("gapClaim", true);
            }
            context.setObject(InsuranceArchetypes.CLAIM, claim);
            super.start(context);
        }

        /**
         * Invoked when the edit dialog closes to complete the task.
         *
         * @param action  the dialog action
         * @param editor  the editor
         * @param context the task context
         */
        @Override
        protected void onDialogClose(String action, IMObjectEditor editor, TaskContext context) {
            if (ClaimEditDialog.SUBMIT_ID.equals(action)) {
                super.onEditCompleted();
            } else {
                super.onDialogClose(action, editor, context);
            }
        }

        /**
         * Invoked when editing fails due to error.
         *
         * @param object    the object being edited.
         * @param context   the  task context
         * @param exception the cause of the error
         */
        @Override
        protected void onError(IMObject object, TaskContext context, Throwable exception) {
            String preamble = Messages.format("patient.insurance.claim.editerror", insurer.getName());
            log.error(preamble + " " + exception.getMessage(), exception);
            ErrorDialog.newDialog()
                    .preamble(preamble)
                    .message(ErrorFormatter.format(exception))
                    .buttons(ErrorDialog.RETRY_ID, ErrorDialog.SKIP_ID, ErrorDialog.CANCEL_ID)
                    .listener(new PopupDialogListener() {
                        @Override
                        public void onRetry() {
                            start(context);
                        }

                        @Override
                        public void onSkip() {
                            notifySkipped();
                        }

                        @Override
                        public void onCancel() {
                            notifyCancelled();
                        }
                    })
                    .show();
        }
    }

    private ConfirmationTask createClaimConfirmationTask(TaskContext context, Map<Act, Party> policies) {
        String title = (gapClaim) ? Messages.get("patient.insurance.gapclaim.title")
                                  : Messages.get("patient.insurance.claim.title");
        String message = null;
        if (policies.size() > 1) {
            message = (gapClaim) ? Messages.get("patient.insurance.gapclaim.multiplepolicies")
                                 : Messages.get("patient.insurance.claim.multiplepolicies");
        } else {
            Party insurer = policies.values().stream().findFirst().orElse(null);
            if (insurer != null) {
                message = (gapClaim) ? Messages.format("patient.insurance.gapclaim.message", insurer.getName())
                                     : Messages.format("patient.insurance.claim.message", insurer.getName());
            }
        }
        return new ConfirmClaimTask(title, message, policies, context.getHelpContext());
    }

    /**
     * Task to determine if gap claims are available for the policies and if so, prompts to make a claim.
     */
    private class GapClaimTask extends Tasks {

        public GapClaimTask(HelpContext context) {
            super(context);
            setRequired(false);
        }

        /**
         * Initialise any tasks.
         *
         * @param context the task context
         */
        @Override
        protected void initialise(TaskContext context) {
            addTask(new AbstractTask() {
                @Override
                public void start(TaskContext context) {
                    run(context);
                    notifyCompleted();
                }
            });
        }

        /**
         * Runs the task.
         *
         * @param context the task context
         */
        private void run(TaskContext context) {
            Map<Act, Party> gapPolicies = new LinkedHashMap<>(); // ensure they are ordered most recent first
            boolean rerun = false;
            for (Map.Entry<Act, Party> entry : activePolicies.entrySet()) {
                Act policy = entry.getKey();
                Party insurer = entry.getValue();
                try {
                    if (isGapClaimAvailable(policy, insurer, context)) {
                        gapPolicies.put(policy, insurer);
                    }
                } catch (Throwable exception) {
                    String preamble = Messages.format("patient.insurance.gapclaim.notavailable", insurer.getName());
                    log.error(preamble + " " + exception.getMessage(), exception);
                    ErrorTask task = new ErrorTask(preamble, exception, () -> {
                        // re-run the task
                        initialise(context);
                    });
                    addTask(task);
                    rerun = true;
                    break;
                }
            }
            if (!rerun && !gapPolicies.isEmpty()) {
                ConfirmationTask confirm = createClaimConfirmationTask(context, gapPolicies);
                addTask(new ConditionalTask(confirm, new EditClaimTask()));
            }
        }

        /**
         * Determines if gap claims are available for a policy.
         *
         * @param policy  the policy
         * @param insurer the insurer
         * @param context the task context
         * @return {@code true} if gap claims are available, otherwise {@code false}
         */
        private boolean isGapClaimAvailable(Act policy, Party insurer, TaskContext context) {
            boolean result = false;
            String policyNumber = rules.getPolicyNumber(policy);
            InsuranceService insuranceService = ServiceHelper.getBean(InsuranceServices.class).getService(insurer);
            if (insuranceService instanceof GapInsuranceService) {
                InsuranceFactory factory = ServiceHelper.getBean(InsuranceFactory.class);
                GapInsuranceService gapService = (GapInsuranceService) insuranceService;
                Location location = factory.getLocation(context.getLocation());
                result = gapService.getGapClaimAvailability(insurer, policyNumber, location).isAvailable();
            }
            return result;
        }
    }

    private class ConfirmClaimTask extends ConfirmationTask {

        /**
         * The policies.
         */
        private final Map<Act, Party> policies;

        /**
         * Constructs a {@link ConfirmClaimTask}.
         *
         * @param title    the confirmation title
         * @param message  the message
         * @param policies the policies
         * @param help     the help context
         */
        public ConfirmClaimTask(String title, String message, Map<Act, Party> policies, HelpContext help) {
            super(title, message, help);
            this.policies = policies;
            setRequired(false);
        }

        /**
         * Creates a new confirmation dialog.
         *
         * @param title   the dialog title.
         * @param message the dialog message
         * @param buttons the buttons
         * @param help    the help context
         * @return a new confirmation dialog
         */
        @Override
        protected ConfirmationDialog createConfirmationDialog(String title, String message, String[] buttons,
                                                              HelpContext help) {
            return new ClaimConfirmationDialog(title, message, buttons, help);
        }

        private class ClaimConfirmationDialog extends ConfirmationDialog {
            public ClaimConfirmationDialog(String title, String message, String[] buttons, HelpContext help) {
                super(title, message, buttons, help);
            }

            /**
             * Invoked when the 'yes' button is pressed. This ensures a policy has been selected before closing.
             */
            @Override
            protected void onYes() {
                if (policy != null) {
                    super.onYes();
                }
            }

            /**
             * Lays out the component prior to display.
             */
            @Override
            protected void doLayout() {
                Label message = LabelFactory.text(getMessage());
                Column container = ColumnFactory.create(Styles.WIDE_CELL_SPACING, message);
                if (policies.size() > 1) {
                    ButtonGroup group = new ButtonGroup();
                    Column column = ColumnFactory.create(Styles.CELL_SPACING);
                    boolean first = true;
                    for (Map.Entry<Act, Party> entry : policies.entrySet()) {
                        RadioButton button = ButtonFactory.text(entry.getValue().getName(), group, () -> {
                            setSelectedPolicy(entry);
                        });
                        if (first) {
                            button.setSelected(true);
                            setSelectedPolicy(entry);
                            first = false;
                        }
                        column.add(button);
                    }
                    container.add(column);
                } else {
                    Map.Entry<Act, Party> entry = policies.entrySet().stream().findFirst().orElse(null);
                    setSelectedPolicy(entry);
                }
                getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, container));
            }
        }
    }

    /**
     * Tasks to display an error and prompt to retry.
     */
    private static class ErrorTask extends AbstractTask {

        /**
         * The message to display before the error.
         */
        private final String preamble;

        /**
         * The error.
         */
        private final Throwable error;

        /**
         * The listener to invoke to retry.
         */
        private final Runnable retryListener;

        /**
         * Constructs an {@link ErrorTask}.
         *
         * @param preamble      the message to display before the error
         * @param error         the error
         * @param retryListener the listener to invoke on retry
         */
        public ErrorTask(String preamble, Throwable error, Runnable retryListener) {
            this.preamble = preamble;
            this.error = error;
            this.retryListener = retryListener;
            setRequired(false); // to allow the task to be skipped
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         */
        @Override
        public void start(TaskContext context) {
            ErrorDialog.newDialog()
                    .preamble(preamble)
                    .message(ErrorFormatter.format(error))
                    .buttons(ErrorDialog.RETRY_ID, ErrorDialog.SKIP_ID, ErrorDialog.CANCEL_ID)
                    .listener(new PopupDialogListener() {
                        @Override
                        public void onRetry() {
                            retryListener.run();
                            notifyCompleted();
                        }

                        @Override
                        public void onSkip() {
                            notifySkipped();
                        }

                        @Override
                        public void onCancel() {
                            notifyCancelled();
                        }
                    })
                    .show();
        }
    }
}
