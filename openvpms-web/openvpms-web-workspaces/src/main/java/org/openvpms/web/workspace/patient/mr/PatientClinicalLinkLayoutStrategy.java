/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import nextapp.echo2.app.Component;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.Hint;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.history.PatientHistoryDatingPolicy;

/**
 * Layout strategy for <em>act.patientClinicalLink</em> .
 *
 * @author Tim Anderson
 */
public class PatientClinicalLinkLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Provider node name.
     */
    private static final String PROVIDER = "provider";

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes NODES = ArchetypeNodes.all().excludeIfEmpty(PROVIDER);

    /**
     * Constructs a {@link PatientClinicalLinkLayoutStrategy}.
     */
    public PatientClinicalLinkLayoutStrategy() {
        super(NODES);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        addComponent(getURL(object, properties, context));

        if (context.isEdit()) {
            PatientHistoryDatingPolicy policy = ServiceHelper.getBean(PatientHistoryDatingPolicy.class);
            if (!policy.canEditStartTime((Act) object)) {
                addComponent(createComponent(createReadOnly(properties.get(START_TIME)), object, context));
            }
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns a component for the URL.
     * <p/>
     * If in edit mode, and the link has a provider, the returned URL will be read-only.
     *
     * @param object     the link
     * @param properties the properties
     * @param context    the layout context
     * @return the URL component
     */
    private ComponentState getURL(IMObject object, PropertySet properties, LayoutContext context) {
        Property url = properties.get("url");
        if (context.isEdit() && hasProvider(object)) {
            url = createReadOnly(url);
        }
        Component field = context.getComponentFactory().create(url, Hint.width(100));
        return new ComponentState(field, url);
    }

    /**
     * Determines if the link has provider.
     *
     * @param object the link
     * @return {@code true} if the link has provider, otherwise {@code false}
     */
    private boolean hasProvider(IMObject object) {
        IMObjectBean bean = getBean(object);
        return bean.getTargetRef(PROVIDER) != null;
    }
}
