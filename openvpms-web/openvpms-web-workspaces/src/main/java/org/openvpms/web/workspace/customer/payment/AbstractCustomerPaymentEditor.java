/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.act.ActHelper;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.edit.payment.PaymentEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectCreationListener;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * An editor for {@link Act}s which have an archetype of
 * <em>act.customerAccountPayment</em> or <em>act.customerAccountRefund</em>.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCustomerPaymentEditor extends PaymentEditor {

    /**
     * The payment status.
     */
    private final PaymentStatus status;

    /**
     * The amount of the invoice that this payment relates to.
     */
    private final SimpleProperty invoiceAmount;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * Determines the expected payment amount. If {@code null}, there
     * is no limit on the payment amount. If non-null, validation will fail
     * if the act total is not that specified.
     */
    private BigDecimal expectedAmount;

    /**
     * The last till used to successfully make an EFT payment/refund.
     * <p/>
     * Once an EFT payment/refund has been made, the till cannot be changed.
     */
    private Entity eftTill;


    /**
     * Constructs an {@link AbstractCustomerPaymentEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public AbstractCustomerPaymentEditor(FinancialAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        status = new PaymentStatus(getProperty("status"), act);
        invoiceAmount = createProperty("invoiceAmount", "customer.payment.currentInvoice");
        rules = ServiceHelper.getBean(CustomerAccountRules.class);

        initParticipant("customer", context.getContext().getCustomer());
        initParticipant("location", context.getContext().getLocation());

        if (!status.isReadOnly()) {
            addEditor(status.getEditor());
        }

        getItems().setCreationListener(new IMObjectCreationListener() {
            public void created(IMObject object) {
                onCreated((FinancialAct) object);
            }
        });

        if (!act.isNew()) {
            Entity till = getTill();
            if (till != null) {
                for (EFTPaymentItemEditor itemEditor : getEFTItemEditors()) {
                    if (!itemEditor.canDelete()) {
                        // the till can't be changed if a transaction has been approved, or one is in progress
                        eftTill = till;
                    }
                }
            }
        }
    }

    /**
     * Determines if the current status is POSTED.
     *
     * @return {@code true} if the current status is POSTED.
     */
    public boolean isPosted() {
        return getStatus().equals(ActStatus.POSTED);
    }

    /**
     * If the current status is POSTED, sets it to IN_PROGRESS, and indicate that it should be set to POSTED on
     * completion of editing. This enables the payment to be saved without affecting the balance (once POSTED,
     * totals cannot change).
     * <p/>
     * The view will still indicate POSTED.
     */
    public void makeSaveableAndPostOnCompletion() {
        status.makeSaveableAndPostOnCompletion();
    }

    /**
     * Sets the invoice amount.
     *
     * @param amount the invoice amount
     */
    public void setInvoiceAmount(BigDecimal amount) {
        invoiceAmount.setValue(amount);
    }

    /**
     * Returns the invoice amount.
     *
     * @return the invoice amount
     */
    public BigDecimal getInvoiceAmount() {
        return invoiceAmount.getBigDecimal(BigDecimal.ZERO);
    }

    /**
     * Determines the expected amount of the payment. If {@code null}, there
     * is no limit on the payment amount. If non-null, validation will fail
     * if the act total is not that specified.
     *
     * @param amount the expected payment amount. May be {@code null}
     */
    public void setExpectedAmount(BigDecimal amount) {
        expectedAmount = amount;
    }

    /**
     * Returns the expected amount of the payment.
     *
     * @return the expected amount of the payment. May be {@code null}
     */
    public BigDecimal getExpectedAmount() {
        return expectedAmount;
    }

    /**
     * Sets the till.
     *
     * @param till the till. May be {@code null}
     */
    public void setTill(Entity till) {
        setParticipant("till", till);
    }

    /**
     * Returns the selected till.
     *
     * @return the till, or {@code null} if none has been selected
     */
    public Entity getTill() {
        return getParticipant("till");
    }

    /**
     * Determines if there are any EFT transactions that need to be performed.
     *
     * @return {@code true} if the payment has any outstanding EFT items requiring payment/refund,
     * otherwise {@code false}
     */
    public boolean requiresEFTTransaction() {
        for (EFTPaymentItemEditor editor : getEFTItemEditors()) {
            if (editor.requiresEFTTransaction()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Processes the first incomplete EFT transaction.
     * <p/>
     * This should be invoked when {@link #requiresEFTTransaction()} returns {@code true}
     *
     * @param listener the listener to notify on success
     */
    public void processEFTTransaction(Runnable listener) {
        for (EFTPaymentItemEditor editor : getEFTItemEditors()) {
            if (editor.requiresEFTTransaction()) {
                editor.eft(listener);
                break;
            }
        }
    }

    /**
     * Determines if the act should be posted on completion of the edit.
     *
     * @return {@code true} if the act should be posted on completion of the edit
     */
    public boolean postOnCompletion() {
        return status.postOnCompletion();
    }

    /**
     * Determines if the status can be changed.
     *
     * @return {@code true} if the status can be changed
     */
    public boolean canChangeStatus() {
        return !status.isReadOnly();
    }

    /**
     * Determines if the payment can be deleted.
     *
     * @return {@code true} if the payment can be deleted, otherwise {@code false}
     */
    public boolean canDelete() {
        boolean result = false;
        if (!getObject().isNew() && canChangeStatus()) {
            result = true;
            for (EFTPaymentItemEditor editor : getEFTItemEditors()) {
                if (!editor.canDelete()) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Determines if the payment only has EFT items.
     *
     * @return {@code true} if the payment only has EFT items
     */
    public boolean isEmptyOrOnlyHasEFTItems() {
        boolean result;
        List<Act> acts = getItems().getCurrentActs();
        if (acts.isEmpty()) {
            result = true;
        } else {
            result = true;
            for (Act act : acts) {
                if (!act.isA(CustomerAccountArchetypes.PAYMENT_EFT, CustomerAccountArchetypes.REFUND_EFT)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new CustomerPaymentLayoutStrategy(getItems(), getPaymentStatus());
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This can be used to perform processing that requires all editors to be created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        initItems();
        ParticipationEditor<Entity> till = getParticipationEditor("till", true);
        if (till != null) {
            till.addModifiableListener(modifiable -> onTillChanged());
        }
    }

    /**
     * Returns the payment status.
     *
     * @return the payment status
     */
    protected PaymentStatus getPaymentStatus() {
        return status;
    }

    /**
     * Validates the object.
     * <p/>
     * This extends validation by ensuring that the payment amount matches the expected amount, if present.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean requiresEFTTransaction = requiresEFTTransaction();
        if (requiresEFTTransaction && status.isPosted()) {
            // Cannot save the transaction as POSTED when EFT is incomplete
            status.makeSaveableAndPostOnCompletion();
        }
        boolean valid = super.doValidation(validator);
        if (valid && expectedAmount != null) {
            Property property = getProperty("amount");
            BigDecimal amount = (BigDecimal) property.getValue();
            if (amount.compareTo(expectedAmount) != 0) {
                valid = false;
                // need to pre-format the amounts as the Messages uses the browser's locale which may have different
                // currency format
                String msg = Messages.format("customer.payment.amountMismatch",
                                             NumberFormatter.formatCurrency(expectedAmount));
                validator.add(property, new ValidatorError(msg));
            }
        }
        if (valid) {
            if (eftTill != null && !eftTill.equals(getTill())) {
                validator.add(this, Messages.get("customer.payment.eft.cannotchangetill"));
                valid = false;
            } else if (requiresEFTTransaction && ActStatus.POSTED.equals(getStatus())) {
                // Shouldn't occur. Status has been overridden
                validator.add(this, "Cannot finalise payment until EFT transactions are complete");
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Deletes the object.
     *
     * @throws OpenVPMSException     if the delete fails
     * @throws IllegalStateException if the act is POSTED or has EFT payments
     */
    @Override
    protected void doDelete() {
        for (EFTPaymentItemEditor editor : getEFTItemEditors()) {
            if (!editor.canDelete()) {
                throw new IllegalStateException("Cannot delete EFT payment");
            }
        }
        super.doDelete();
    }

    /**
     * Returns the EFT item editors.
     *
     * @return the EFT item editors
     */
    protected List<EFTPaymentItemEditor> getEFTItemEditors() {
        List<EFTPaymentItemEditor> result = new ArrayList<>();
        ActRelationshipCollectionEditor items = getItems();
        for (Act item : items.getCurrentActs()) {
            if (item.isA(CustomerAccountArchetypes.PAYMENT_EFT, CustomerAccountArchetypes.REFUND_EFT)) {
                result.add((EFTPaymentItemEditor) items.getEditor(item));
            }
        }
        return result;
    }

    /**
     * Invoked when a child act is created. This sets the total to the:
     * <ul>
     * <li>outstanding balance +/- the running total, if there is no expected
     * amount; or</li>
     * <li>expected amount - the running total</li>
     * </ul>
     *
     * @param act the act
     */
    protected void onCreated(FinancialAct act) {
        Party customer = (Party) getParticipant("customer");
        if (customer != null) {
            BigDecimal runningTotal = getRunningTotal();
            BigDecimal balance;
            if (expectedAmount == null) {
                // default the amount to the outstanding balance +/- the running total.
                boolean payment = act.isA("act.customerAccountPayment*");
                balance = rules.getBalance(customer, runningTotal, payment);
                act.setTotal(new Money(balance));
            } else {
                // default the amount to the expected amount - the running total.
                balance = expectedAmount.subtract(runningTotal);
                if (balance.signum() >= 0) {
                    act.setTotal(new Money(balance));
                }
            }
            getItems().setModified(act, true);
        }
    }

    /**
     * Returns the invoice amount property.
     *
     * @return the property
     */
    protected Property getInvoiceAmountProperty() {
        return invoiceAmount;
    }

    /**
     * Helper to create a decimal property.
     *
     * @param name the property name
     * @param key  the resource bundle key
     * @return a new property
     */
    protected SimpleProperty createProperty(String name, String key) {
        SimpleProperty property = new SimpleProperty(name, BigDecimal.class);
        property.setDisplayName(Messages.get(key));
        property.setReadOnly(true);
        return property;
    }

    /**
     * Adds a default item if the object is new and there are no items present.
     */
    private void initItems() {
        if (getObject().isNew()) {
            ActRelationshipCollectionEditor items = getItems();
            CollectionProperty property = items.getCollection();
            if (property.getValues().isEmpty()) {
                addItem();
            }
        }
    }

    /**
     * Invoked when the till changes. Propagates it to the EFT item editors.
     */
    private void onTillChanged() {
        Entity till = getTill();
        if (eftTill == null || !eftTill.equals(till)) {
            for (EFTPaymentItemEditor itemEditor : getEFTItemEditors()) {
                itemEditor.setTill(till);
            }
        }
    }

    /**
     * Returns the running total. This is the current total of the act
     * minus any committed child acts which are already included in the balance.
     *
     * @return the running total
     */
    private BigDecimal getRunningTotal() {
        FinancialAct act = getObject();
        BigDecimal total = act.getTotal();
        BigDecimal committed = ActHelper.sum(act, "amount");
        return total.subtract(committed);
    }
}
