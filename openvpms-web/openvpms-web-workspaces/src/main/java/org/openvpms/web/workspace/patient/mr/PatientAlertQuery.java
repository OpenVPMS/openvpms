/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.LocalSortResultSet;
import org.openvpms.web.component.im.query.ResultSet;

import static org.openvpms.archetype.rules.patient.PatientArchetypes.PATIENT_PARTICIPATION;

/**
 * Query for <em>act.patientAlert</em> acts.
 * <p/>
 * NOTE: where a entity.patientAlertType has been deactivated, acts with this alert type will continue to be returned.
 * This is because this query is used to query both current and historical alerts.
 *
 * @author Tim Anderson
 */
public class PatientAlertQuery extends DateRangeActQuery<Act> {

    /**
     * The alert statuses to query.
     */
    private static final ActStatuses STATUSES = new ActStatuses(PatientArchetypes.ALERT);

    /**
     * Constructs a {@link PatientAlertQuery}.
     *
     * @param patient the entity to search for
     */
    public PatientAlertQuery(Party patient) {
        super(patient, "patient", PATIENT_PARTICIPATION, new String[]{PatientArchetypes.ALERT}, STATUSES, Act.class);
        setStatus(ActStatus.IN_PROGRESS);

        setDefaultSortConstraint(DESCENDING_START_TIME);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        // can sort on virtual nodes, so wrap in a LocalSortResultSet to support these when present
        return new LocalSortResultSet<>(super.createResultSet(sort));
    }
}
