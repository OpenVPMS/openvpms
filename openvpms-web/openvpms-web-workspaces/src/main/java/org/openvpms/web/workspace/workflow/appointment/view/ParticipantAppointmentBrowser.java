/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.view;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.ContextSwitchListener;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.scheduling.view.ParticipantScheduleEventBrowser;

/**
 * A browser for appointments returned by a {@link ParticipantAppointmentQuery} that jumps to the appointment in
 * the appointment workspace when selected.
 *
 * @author Tim Anderson
 */
public class ParticipantAppointmentBrowser extends ParticipantScheduleEventBrowser {

    /**
     * Constructs a {@link ParticipantAppointmentBrowser} that queries appointments using the specified query.
     *
     * @param query   the query
     * @param context the layout context
     */
    public ParticipantAppointmentBrowser(ParticipantAppointmentQuery query, IMTableModel<Act> model,
                                         LayoutContext context) {
        super(query, model, context);
    }

    /**
     * Switches to the appropriate workspace when an event is selected, but only if the event is for the specified
     * location.
     *
     * @param event    the event
     * @param location the practice location
     * @param listener the context switch listener
     */
    @Override
    protected void switchTo(Act event, Party location, ContextSwitchListener listener) {
        AppointmentRules rules = ServiceHelper.getBean(AppointmentRules.class);
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(event);
        Entity schedule = bean.getTarget("schedule", Entity.class);
        if (schedule != null && location != null) {
            Entity view = rules.getScheduleView(location, schedule);
            if (view != null) {
                listener.switchTo(event);
            } else {
                Party newLocation = rules.getLocation(schedule);
                String name = (newLocation != null) ? newLocation.getName() : Messages.get("imobject.none");
                InformationDialog.show(Messages.format("workflow.scheduling.appointment.view.wronglocation", name));
            }
        }
    }
}