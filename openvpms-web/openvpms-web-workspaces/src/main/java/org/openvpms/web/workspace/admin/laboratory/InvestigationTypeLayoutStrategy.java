/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.MutablePropertySet;
import org.openvpms.web.component.property.PropertySet;

/**
 * Layout strategy for <em>entity.investigationType</em>.
 *
 * @author Tim Anderson
 */
public class InvestigationTypeLayoutStrategy extends AbstractLayoutStrategy {

    private final ArchetypeNodes nodes = new ArchetypeNodes();

    static final String LABORATORY = "laboratory";

    private static final String DEVICES = "devices";

    private static final String TEMPLATE = "template";

    private static final String SUPPLIER = "supplier";

    private static final String TYPE_ID = "typeId";

    private static final String NAME = "name";

    private static final String DESCRIPTION = "description";

    /**
     * Constructs an {@link AbstractLayoutStrategy}.
     */
    public InvestigationTypeLayoutStrategy() {
        super();
        nodes.excludeIfEmpty(DEVICES); // devices only populated by laboratory plugins
        setArchetypeNodes(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (!context.isEdit()) {
            nodes.excludeIfEmpty(LABORATORY, TEMPLATE, SUPPLIER);
        } else if (getBean(object).getObject(TYPE_ID) != null) {
            // if the investigation type was created by a laboratory plugin, make the nodes read-only
            MutablePropertySet set = new MutablePropertySet(properties);
            set.setReadOnly(NAME);
            set.setReadOnly(DESCRIPTION);
            set.setReadOnly(LABORATORY);
            properties = set;
        }
        return super.apply(object, properties, parent, context);
    }
}
