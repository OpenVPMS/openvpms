/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Border;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.CheckBox;
import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Insets;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.DefaultTableModel;
import nextapp.echo2.app.table.TableCellRenderer;
import nextapp.echo2.app.table.TableColumn;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.component.bound.BoundCheckBox;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.SelectFieldFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.TableEx;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;


/**
 * Log viewer.
 *
 * @author Tim Anderson
 */
public class LogViewer extends AbstractDiagnosticTab {

    /**
     * Page selector.
     */
    private final Property pageSelector;

    /**
     * Page selector listener.
     */
    private final ModifiableListener pageListener;

    /**
     * Search.
     */
    private final Property search;

    /**
     * Match case property.
     */
    private final Property matchCase;

    /**
     * First page.
     */
    private final Button first;

    /**
     * Previous page.
     */
    private final Button previous;

    /**
     * Next page.
     */
    private final Button next;

    /**
     * Last page.
     */
    private final Button last;

    /**
     * Log file size.
     */
    private final Label size;

    /**
     * Page input.
     */
    private final TextField input;

    /**
     * Search forward.
     */
    private final Button forward;

    /**
     * Search backward.
     */
    private final Button backward;

    /**
     * The log table.
     */
    private final TableEx table;

    /**
     * The rows that match the search criteria on the current page.
     */
    private final List<Integer> matches = new ArrayList<>();

    /**
     * The current match being displayed.
     */
    private int currentMatch = -1;

    /**
     * The current reader.
     */
    private LogReader reader;

    /**
     * The current page.
     */
    private int currentPage = 0;

    /**
     * Maximum number of lines per page.
     */
    private static final int MAX_LINES = 100;

    /**
     * Search match text background colour.
     */
    private static final String MATCH_BACKGROUND = "#0078d7";

    /**
     * Search match text foreground colour.
     */
    private static final String MATCH_FOREGROUND = "#ffffff";

    /**
     * Constructs a {@link LogViewer}.
     */
    LogViewer() {
        super("admin.system.diagnostic.log");
        pageSelector = new SimpleProperty("page", this.currentPage, Integer.class);
        pageListener = modifiable -> show(pageSelector.getInt() - 1);
        pageSelector.addModifiableListener(pageListener);
        search = new SimpleProperty("search", String.class);
        ModifiableListener searchListener = modifiable -> onSearch(currentPage, true);
        search.addModifiableListener(searchListener);
        matchCase = new SimpleProperty("matchCase", false, Boolean.class,
                                       Messages.get("admin.system.diagnostic.log.matchcase"));
        matchCase.addModifiableListener(searchListener);

        first = ButtonFactory.create(null, "navigation.first", this::onFirst);

        input = BoundTextComponentFactory.create(pageSelector, 5);
        previous = ButtonFactory.create(null, "navigation.previous", this::onPrevious);
        next = ButtonFactory.create(null, "navigation.next", this::onNext);
        last = ButtonFactory.create(null, "navigation.last", this::onLast);
        size = LabelFactory.create();
        forward = ButtonFactory.create(null, "button.down", this::onSearchForward);
        forward.setToolTipText(Messages.get("admin.system.diagnostic.log.nextmatch"));
        backward = ButtonFactory.create(null, "button.up", this::onSearchBackward);
        backward.setToolTipText(Messages.get("admin.system.diagnostic.log.previousmatch"));

        DefaultTableModel model = new DefaultTableModel(new Object[0][2], new Object[2]);
        DefaultTableColumnModel columnModel = new DefaultTableColumnModel();
        columnModel.addColumn(new TableColumn(0));
        columnModel.addColumn(new TableColumn(1));
        table = new TableEx(model, columnModel);
        table.setBorder(new Border(0, Color.WHITE, Border.STYLE_NONE));
        table.setHeaderVisible(false);
        table.setInsets(new Insets(2));
        table.setDefaultRenderer(Object.class, (TableCellRenderer) (table, o, i, i1) -> {
            if (o instanceof Component) {
                return (Component) o;
            }
            if (o instanceof Integer) {
                return TableHelper.rightAlign(o + ": ");
            }
            return null;
        });
    }

    /**
     * Returns the log file paths.
     *
     * @return the paths
     */
    public List<String> getFiles() {
        List<String> files = new ArrayList<>();
        String fileName = getDefaultFileName();
        if (fileName != null) {
            if (Files.isReadable(Paths.get(fileName))) {
                files.add(fileName);
            }
        }
        boolean done = false;
        int suffix = 1;
        while (!done) {
            String path = fileName + "." + suffix;
            if (Files.isReadable(Paths.get(path))) {
                files.add(path);
                suffix++;
            } else {
                done = true;
            }
        }
        return files;
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        LogReader reader = getReader();
        if (reader != null) {
            result = getDocument(reader.getPath());
        }
        return result;
    }

    /**
     * Returns a document containing the diagnostics for the specified file.
     *
     * @param file the log file
     * @return the document, or {@code null} if one cannot be created
     */
    public Document getDocument(String file) {
        Document result = null;
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        try (GZIPOutputStream gzip = new GZIPOutputStream(bytes);
             FileInputStream in = new FileInputStream(file)) {

            int count;
            while ((count = in.read(buffer)) > 0) {
                gzip.write(buffer, 0, count);
            }

            in.close();

            gzip.finish();
            gzip.close();
            byte[] content = bytes.toByteArray();
            result = toDocument(file + ".gz", content, content.length, "application/gzip");
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        List<String> files = getFiles();
        Component log;

        if (!files.isEmpty()) {
            read(files.get(0));
            SelectField fileSelector = SelectFieldFactory.create(files.toArray());
            fileSelector.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    Object selectedItem = fileSelector.getSelectedItem();
                    if (selectedItem instanceof String) {
                        read(selectedItem.toString());
                        show(0);
                    }
                }
            });
            log = RowFactory.create(Styles.CELL_SPACING, fileSelector, size);
        } else {
            log = LabelFactory.create("admin.system.diagnostic.lognotfound");
        }
        CheckBox matchCaseCheckBox = new BoundCheckBox(matchCase);
        matchCaseCheckBox.setText(matchCase.getDisplayName());
        Row row = RowFactory.create(Styles.CELL_SPACING, first, previous, input, next, last,
                                    LabelFactory.create("query.search"), BoundTextComponentFactory.create(search, 20),
                                    forward, backward, matchCaseCheckBox);
        SplitPane pane = SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM, "LogViewer");
        pane.add(ColumnFactory.create(Styles.INSET, ColumnFactory.create(Styles.CELL_SPACING, log, row)));
        pane.add(ColumnFactory.create(Styles.INSET, table));

        enablePaging(reader != null);

        show(currentPage);
        return pane;
    }

    /**
     * Sets the file name to read,
     *
     * @param fileName the file name.
     */
    private void read(String fileName) {
        reader = new LogReader(fileName, MAX_LINES);
    }

    /**
     * Returns the reader.
     *
     * @return the reader. May be {@code null}
     */
    private LogReader getReader() {
        if (reader == null) {
            List<String> paths = getFiles();
            if (!paths.isEmpty()) {
                read(paths.get(0));
            }
        }
        return reader;
    }

    /**
     * Enables/disables paging.
     *
     * @param enable if {@code true} enable the buttons, otherwise disable them
     */
    private void enablePaging(boolean enable) {
        first.setEnabled(enable);
        last.setEnabled(enable);
        input.setEnabled(enable);
        previous.setEnabled(enable);
        next.setEnabled(enable);
    }

    /**
     * Enables/disables the search navigation buttons.
     */
    private void enableSearchControls() {
        if (reader != null && !StringUtils.isEmpty(search.getString())) {
            forward.setEnabled(true); // can always search forward as it will seek to the next page (if any)
            backward.setEnabled(currentPage > 0 || currentMatch > 0);
        } else {
            forward.setEnabled(false);
            backward.setEnabled(false);
        }
    }

    /**
     * Returns the default file name.
     *
     * @return the default file name
     */
    private String getDefaultFileName() {
        String fileName = null;
        Appender appender = LoggerContext.getContext().getConfiguration().getAppenders().get("fullout");
        if (appender instanceof RollingFileAppender) {
            fileName = ((RollingFileAppender) appender).getFileName();
        }
        return fileName;
    }

    /**
     * Displays the first page.
     */
    private void onFirst() {
        show(0);
    }

    /**
     * Displays the previous page.
     */
    private void onPrevious() {
        if (currentPage > 0) {
            currentPage--;
        }
        show(currentPage);
    }

    /**
     * Displays the next page.
     */
    private void onNext() {
        show(currentPage + 1);
    }

    /**
     * Displays the last page.
     */
    private void onLast() {
        LogReader.Page content = null;
        try {
            content = reader.readLast();
            if (content == null) {
                // truncated. Try and re-read
                content = reader.readLast();
            }
        } catch (IOException exception) {
            ErrorHelper.show(exception);
        }
        show(content);
    }

    /**
     * Show a page.
     *
     * @param pageNumber the page number (0-based)
     */
    private void show(int pageNumber) {
        LogReader.Page page = null;
        try {
            if (getReader() != null) {
            page = reader.read(pageNumber);
            if (page == null) {
                // page does not exist. Try and show the last page
                page = reader.readLast();
            }
            }
        } catch (IOException exception) {
            ErrorHelper.show(exception);
        }
        show(page);
    }

    /**
     * Show a page.
     *
     * @param page the page. May be {@code null}
     */
    private void show(LogReader.Page page) {
        show(page, getSearch(), true);
    }

    /**
     * Returns the search criteria.
     *
     * @return the search criteria, or {@code null} if there is none
     */
    private LogReader.Search getSearch() {
        String criteria = search.getString();
        return (criteria != null) ? new LogReader.Search(criteria, matchCase.getBoolean()) : null;
    }

    /**
     * Shows a page.
     * <p/>
     * NOTE: when performing case-insensitive searches, the search text should already be lowercase.
     *
     * @param page    the page. May be {@code null}
     * @param search  the search text to highlight. May  be {@code null}
     * @param forward if {@code true}, search forward, else search backward
     */
    private void show(LogReader.Page page, LogReader.Search search, boolean forward) {
        matches.clear();
        currentMatch = 0;
        size.setText(NumberFormatter.getSize(getLength()));
        if (page != null) {
            populate(page, search);
        } else {
            table.setModel(new DefaultTableModel());
        }
        pageSelector.removeModifiableListener(pageListener);
        pageSelector.setValue(page != null ? page.getPage() + 1 : null);
        pageSelector.addModifiableListener(pageListener);
        if (!forward && matches.size() > 0) {
            currentMatch = matches.size() - 1;
        }
        currentPage = (page != null) ? page.getPage() : 0;
        showCurrentMatch();
    }

    /**
     * Populates the table with the log page, with each line on a separate row.
     * <p/>
     * Text matching the search criteria is highlighted, and an embedded anchor can be used to locate it.
     *
     * @param page   the page to display
     * @param search the search criteria. May be {@code null}
     */
    private void populate(LogReader.Page page, LogReader.Search search) {
        int lineNo = page.getFirstLine();
        List<String> lines = page.getLines();
        Object[][] data = new Object[lines.size()][2];
        for (int i = 0; i < lines.size(); ++i) {
            StringBuilder builder = new StringBuilder();
            builder.append("<div style='white-space:pre-wrap'>");
            String line = lines.get(i);
            data[i][0] = ++lineNo;
            if (search != null) {
                int previous = 0;
                int start;
                while ((start = search.indexOf(line, previous)) != -1) {
                    if (start > previous) {
                        builder.append(escape(line.substring(previous, start)));
                    }
                    matches.add(i);
                    builder.append("<span style='background-color:" + MATCH_BACKGROUND + ";color:"
                                   + MATCH_FOREGROUND + "'>");
                    int end = start + search.length();
                    builder.append(escape(line.substring(start, end)));
                    builder.append("</span>");
                    previous = end;
                }
                if (previous < line.length()) {
                    builder.append(escape(line.substring(previous)));
                }
            } else {
                builder.append(escape(line));
            }
            builder.append("</div>");
            data[i][1] = LabelFactory.html(builder.toString(), true);
        }
        table.setModel(new DefaultTableModel(data, new Object[2]));
    }

    /**
     * Escapes characters in text.
     *
     * @param text the text to escape
     * @return the escaped text
     */
    private String escape(String text) {
        return StringEscapeUtils.escapeXml10(text);
    }

    /**
     * Shows the current line matching the search criteria, if any.
     */
    private void showCurrentMatch() {
        if (currentMatch >= 0 && currentMatch < matches.size()) {
            scrollToLine(currentMatch);
        }
        enableSearchControls();
    }

    /**
     * Scrolls the log to the specified match.
     *
     * @param match the match number
     */
    private void scrollToLine(int match) {
        if (match >= 0 && match < matches.size()) {
            table.setScrollToRow(matches.get(match));
        }
    }

    /**
     * Searches forwards for log entries that match the search text.
     */
    private void onSearchForward() {
        while (currentMatch + 1 < matches.size() && matches.get(currentMatch).equals(matches.get(currentMatch + 1))) {
            // if the next match is on the same line, skip it
            currentMatch++;
        }
        if (currentMatch + 1 >= matches.size()) {
            onSearch(currentPage + 1, true);
        } else {
            currentMatch++;
            showCurrentMatch();
        }
    }

    /**
     * Searches backwards for log entries that match the search text.
     */
    private void onSearchBackward() {
        while (currentMatch - 1 > 0 && matches.get(currentMatch).equals(matches.get(currentMatch - 1))) {
            // if the previous match is on the same line, skip it
            currentMatch--;
        }

        if (currentMatch > 0) {
            currentMatch--;
            showCurrentMatch();
        } else if (currentPage > 0) {
            onSearch(currentPage - 1, false);
        }
    }

    /**
     * Invoked to search for matches to the text in the search field.
     *
     * @param pageNumber the page number to search from
     * @param forward    if {@code true} search forward if there are no matches on the current page, else search backwards
     */
    private void onSearch(int pageNumber, boolean forward) {
        LogReader.Search search = getSearch();
        if (search != null && getReader() != null) {
            try {
                LogReader.Page page = reader.read(pageNumber);
                if (page == null) {
                    // page does not exist. Try and show the last page
                    page = reader.readLast();
                }
                if (page != null) {
                    pageNumber = page.getPage();
                    while (page != null && !page.contains(search)) {
                        if (forward) {
                            pageNumber++;
                            page = reader.read(pageNumber);
                        } else if (pageNumber > 0) {
                            pageNumber--;
                            page = reader.read(pageNumber);
                        } else {
                            page = null;
                        }
                    }
                    if (page != null) {
                        show(page, search, forward);
                    }
                }
            } catch (Throwable exception) {
                ErrorHelper.show(exception);
            }
        }
    }

    /**
     * Returns the log file length.
     *
     * @return the log file length
     */
    private long getLength() {
        try {
            return reader.getLength();
        } catch (Throwable exception) {
            return 0;
        }
    }

}
