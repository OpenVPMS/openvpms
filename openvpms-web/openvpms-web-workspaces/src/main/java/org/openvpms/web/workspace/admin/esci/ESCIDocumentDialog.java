/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.app.layout.GridLayoutData;
import org.openvpms.esci.adapter.dispatcher.ESCIDispatcher;
import org.openvpms.esci.adapter.dispatcher.Inbox;
import org.openvpms.esci.adapter.dispatcher.ProcessingConfig;
import org.openvpms.esci.adapter.map.invoice.LenientOrderResolver;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.io.UBLDocumentWriter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.AbstractBrowserListener;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextArea;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

/**
 * Dialog to manage documents in an ESCI inbox.
 *
 * @author Tim Anderson
 */
class ESCIDocumentDialog extends ModalDialog {

    /**
     * The inbox.
     */
    private final Inbox inbox;

    /**
     * The document browser.
     */
    private final ESCIDocumentBrowser browser;

    /**
     * The document dispatcher.
     */
    private final ESCIDispatcher dispatcher;

    /**
     * The text area to display the selected document.
     */
    private final TextArea text;

    /**
     * Refresh button identifier.
     */
    private static final String REFRESH_ID = "button.escirefresh";

    /**
     * Process button identifer.
     */
    private static final String PROCESS_ID = "button.esciprocess";

    /**
     * Delete button identifier.
     */
    private static final String DELETE_ID = "button.delete";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ESCIDocumentDialog.class);

    /**
     * Constructs an {@link ESCIDocumentDialog}.
     *
     * @param inbox the inbox
     * @param help  the help context. May be {@code null}
     */
    public ESCIDocumentDialog(Inbox inbox, Context context, HelpContext help) {
        super("Inbox", "BrowserDialog", new String[0], help);
        this.inbox = inbox;

        dispatcher = ServiceHelper.getBean(ESCIDispatcher.class);
        browser = new ESCIDocumentBrowser(inbox, new DefaultLayoutContext(context, help));
        browser.addBrowserListener(new AbstractBrowserListener<DocumentReference>() {
            @Override
            public void selected(DocumentReference object) {
                show(object);
            }
        });

        text = TextComponentFactory.createTextArea();
        text.setEnabled(false);

        addButton(REFRESH_ID, this::onRefresh);
        addButton(PROCESS_ID, this::onProcess);
        addButton(DELETE_ID, this::onDelete);
        addButton(CLOSE_ID);
        enableButtons();
    }

    /**
     * Show the window.
     */
    @Override
    public void show() {
        super.show();
        refresh(null);
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        // the following enables the text to fit the area of the split pane, and will grow/shrink with the pane.
        text.setWidth(Styles.FULL_WIDTH);
        text.setHeight(Styles.FULL_HEIGHT);
        Grid grid = GridFactory.create(1, text);
        grid.setInsets(StyleSheetHelper.getInsets(Column.class, Styles.INSET, Column.PROPERTY_INSETS));
        GridLayoutData data = new GridLayoutData();
        grid.setRowHeight(0, Styles.FULL_HEIGHT);
        grid.setWidth(Styles.FULL_WIDTH);
        grid.setHeight(Styles.FULL_HEIGHT);
        text.setLayoutData(data);

        SplitPane pane = SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM,
                                                 "BrowserCRUDWorkspace.Layout", browser.getComponent(), grid);
        getLayout().add(pane);
    }

    /**
     * Enables/disables buttons that require a selection.
     */
    private void enableButtons() {
        ButtonSet buttons = getButtons();
        boolean enable = browser.getSelected() != null;
        buttons.setEnabled(DELETE_ID, enable);
        buttons.setEnabled(PROCESS_ID, enable);
    }

    /**
     * Refreshes the browser.
     */
    private void onRefresh() {
        refresh(browser.getSelected());
    }

    /**
     * Invoked to process a document.
     */
    private void onProcess() {
        DocumentReference selected = browser.getSelected();
        if (selected != null) {
            if (!browser.isFirstDocumentSelected()) {
                ConfirmationDialog.newDialog()
                        .title(Messages.format("admin.esci.process.title", selected.getDocumentType()))
                        .message(Messages.format("admin.esci.process.outoforder", selected.getDocumentType()))
                        .yesNo()
                        .yes(() -> process(selected)).show();
            } else {
                process(selected);
            }
        } else {
            refresh(selected);
        }
    }

    /**
     * Processes a document.
     *
     * @param document the document reference
     */
    private void process(DocumentReference document) {
        try {
            dispatcher.process(inbox, document.getReference(), null);
        } catch (ESCIAdapterException exception) {
            if (DocumentReference.INVOICE.equals(document.getDocumentType())) {
                ConfirmationDialog.newDialog()
                        .title(Messages.get("admin.esci.retryprocessing.title"))
                        .preamble(Messages.format("admin.esci.process.failed", document.getDocumentType()))
                        .message(exception.getMessage())
                        .footer(Messages.format("admin.esci.retryprocessing.retry", document.getDocumentType()))
                        .yesNo()
                        .yes(() -> processIgnoringOrderErrors(document))
                        .show();
            } else {
                showError(exception, document);
            }
        } catch (Throwable exception) {
            showError(exception, document);
        }
    }

    /**
     * Processes a document, ignoring invalid order references.
     *
     * @param document the document reference
     */
    private void processIgnoringOrderErrors(DocumentReference document) {
        try {
            LenientOrderResolver resolver = new LenientOrderResolver(ServiceHelper.getArchetypeService());
            dispatcher.process(inbox, document.getReference(), new ProcessingConfig(resolver));
            if (!resolver.getErrors().isEmpty()) {
                StringBuilder builder = new StringBuilder();
                for (ESCIAdapterException exception : resolver.getErrors()) {
                    if (builder.length() != 0) {
                        builder.append("\n");
                    }
                    builder.append(exception.getMessage());
                }
                InformationDialog.newDialog()
                        .title(Messages.format("admin.esci.process.title", document.getDocumentType()))
                        .preamble(Messages.get("admin.esci.processedwitherrors"))
                        .message(builder.toString())
                        .show();
            }
        } catch (Throwable exception) {
            showError(exception, document);
        }
    }

    /**
     * Shows an error.
     *
     * @param exception the exception
     * @param document  the document
     */
    private void showError(Throwable exception, DocumentReference document) {
        log.error("Failed to process document: " + exception.getMessage(), exception);
        ErrorDialog.newDialog().title(Messages.get("admin.esci.process.failed.title"))
                .preamble(Messages.format("admin.esci.process.failed", document.getDocumentType()))
                .message(ErrorFormatter.format(exception))
                .show();
    }

    /**
     * Deletes the selected document.
     */
    private void onDelete() {
        DocumentReference selected = browser.getSelected();
        if (selected != null) {
            ConfirmationDialog.newDialog()
                    .title(Messages.format("imobject.delete.title", selected.getDocumentType()))
                    .message(Messages.format("imobject.delete.message", selected.getDocumentType()))
                    .yesNo()
                    .yes(() -> delete(selected))
                    .show();
        }
    }

    /**
     * Deletes a document.
     *
     * @param reference the document reference
     */
    private void delete(DocumentReference reference) {
        try {
            dispatcher.delete(inbox, reference.getReference());
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        refresh(null);
    }

    /**
     * Shows a document.
     *
     * @param reference the document reference
     */
    private void show(DocumentReference reference) {
        try {
            Document document = inbox.getDocument(reference.getReference());
            UBLDocumentContext context = new UBLDocumentContext();
            UBLDocumentWriter writer = context.createWriter();
            writer.setFormat(true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            writer.write(document.getAny(), stream, false);
            String string = new String(stream.toByteArray(), StandardCharsets.UTF_8);
            text.setText(string);
        } catch (Throwable exception) {
            text.setText(ErrorFormatter.format(exception));
        }
        enableButtons();
    }

    /**
     * Refreshes the display.
     *
     * @param reference the reference of the document to select, or {@code null} to select the first document if
     *                  available
     */
    private void refresh(DocumentReference reference) {
        browser.query();
        if (reference != null) {
            browser.setSelected(reference);
        }
        DocumentReference selected = browser.getSelected();
        if (selected != null) {
            show(selected);
        } else {
            text.setText(null);
        }
        enableButtons();
    }

}
