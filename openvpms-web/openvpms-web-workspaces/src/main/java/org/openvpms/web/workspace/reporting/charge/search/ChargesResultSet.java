/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.search;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.LocationActResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Charges result set.
 *
 * @author Tim Anderson
 */
public class ChargesResultSet extends LocationActResultSet<FinancialAct> {

    /**
     * The amount range start, or {@code null} to not filter by amount.
     */
    private final BigDecimal amountFrom;

    /**
     * The amount range end,  or {@code null} to not filter by amount.
     */
    private final BigDecimal amountTo;

    /**
     * Constructs a {@link ChargesResultSet}.
     *
     * @param archetypes the act archetype constraint
     * @param value      the value to query on. May be {@code null}
     * @param location   the location to restrict results to. May be {@code null}
     * @param locations  the locations to query
     * @param from       the act from date. May be {@code null}
     * @param to         the act to date, inclusive. May be {@code null}
     * @param clinician  the clinician. May be {@code null}
     * @param amountFrom the amount range start, or {@code null} to not filter by amount
     * @param amountTo   the amount range end,  or {@code null} to not filter by amount
     * @param statuses   the act statuses. If {@code null} or empty, indicates all acts
     * @param exclude    if {@code true} exclude acts with status in {@code statuses}; otherwise include them.
     * @param pageSize   the maximum no. of results per page
     * @param sort       the sort criteria. May be {@code null}
     */
    public ChargesResultSet(ShortNameConstraint archetypes, String value, Party location, List<Party> locations,
                            Date from, Date to, User clinician, BigDecimal amountFrom,
                            BigDecimal amountTo, String[] statuses,
                            boolean exclude, int pageSize, SortConstraint[] sort) {
        super(archetypes, value, getParticipants(clinician), location, locations, from, to, statuses, exclude,
              pageSize, sort);
        this.amountFrom = amountFrom;
        this.amountTo = amountTo;
    }

    /**
     * Creates a new archetype query.
     *
     * @return a new archetype query
     */
    @Override
    protected ArchetypeQuery createQuery() {
        ArchetypeQuery query = super.createQuery();
        if (amountFrom != null || amountTo != null) {
            if (amountFrom != null && amountTo != null) {
                if (amountFrom.compareTo(amountTo) == 0) {
                    query.add(Constraints.eq("amount", amountFrom));
                } else {
                    query.add(Constraints.between("amount", amountFrom, amountTo));
                }
            } else if (amountFrom != null) {
                query.add(Constraints.gte("amount", amountFrom));
            } else {
                query.add(Constraints.lte("amount", amountTo));
            }
        }
        return query;
    }

    /**
     * Adds constraints to query nodes on a value.
     * <p/>
     * This creates the constraints with {@link #createValueConstraints(String, List)}. If multiple constraints
     * are returned, an or constraint is used to add them to the query.
     *
     * @param query the query to add the constraints to
     */
    @Override
    protected void addValueConstraints(ArchetypeQuery query) {
        String value = getValue();
        if (value != null) {
            query.add(Constraints.leftJoin("customer").add(Constraints.leftJoin("entity", "cust")));
        }
        super.addValueConstraints(query);
    }

    /**
     * Creates constraints to query nodes on a value.
     *
     * @param value the value to query
     * @param nodes the nodes to query
     * @return the constraints
     */
    @Override
    protected List<IConstraint> createValueConstraints(String value, List<String> nodes) {
        List<IConstraint> result = super.createValueConstraints(value, nodes);
        result.add(Constraints.eq("cust.name", value));
        return result;
    }

    /**
     * Returns the participants constraint.
     *
     * @param clinician the clinician. May be {@code null}
     * @return the constraints, or {@code null} if the clinician is {@code null}
     */
    private static ParticipantConstraint[] getParticipants(User clinician) {
        return (clinician != null) ? new ParticipantConstraint[]{
                new ParticipantConstraint("clinician", UserArchetypes.CLINICIAN_PARTICIPATION, clinician)} : null;
    }
}
