/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkin;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.hl7.patient.PatientContext;
import org.openvpms.hl7.patient.PatientContextFactory;
import org.openvpms.hl7.patient.PatientInformationService;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.client.HospitalizationService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workflow.AbstractTask;
import org.openvpms.web.component.workflow.ConditionalCreateTask;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.DefaultTaskListener;
import org.openvpms.web.component.workflow.LocalTask;
import org.openvpms.web.component.workflow.PrintIMObjectTask;
import org.openvpms.web.component.workflow.ReloadTask;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.TaskEvent;
import org.openvpms.web.component.workflow.Tasks;
import org.openvpms.web.component.workflow.WorkflowImpl;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.workflow.EditVisitTask;
import org.openvpms.web.workspace.workflow.GetInvoiceTask;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;


/**
 * Check-in workflow.
 *
 * @author Tim Anderson
 */
public class CheckInWorkflow extends WorkflowImpl {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The initial context.
     */
    private TaskContext initial;

    /**
     * The external context to access and update.
     */
    private Context external;

    /**
     * The flow sheet service factory.
     */
    private FlowSheetServiceFactory flowSheetServiceFactory;

    /**
     * The check-in workflow help topic.
     */
    private static final String HELP_TOPIC = "workflow/checkin";

    /**
     * Constructs a {@code CheckInWorkflow}.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param context  the external context to access and update
     * @param help     the help context
     */
    public CheckInWorkflow(Party customer, Party patient, Context context, HelpContext help) {
        super(help.topic(HELP_TOPIC));
        this.service = ServiceHelper.getArchetypeService();
        User clinician = UserHelper.useLoggedInClinician(context) ? context.getUser() : null;
        initialise(null, customer, patient, clinician, context);
    }

    /**
     * Constructs a {@code CheckInWorkflow} from an appointment.
     *
     * @param appointment the appointment
     * @param context     the external context to access and update
     * @param help        the help context
     */
    public CheckInWorkflow(Act appointment, Context context, HelpContext help) {
        super(help.topic(HELP_TOPIC));
        this.service = ServiceHelper.getArchetypeService();
        initialise(appointment, context);
    }

    /**
     * Constructs a {@code CheckInWorkflow}.
     * <p/>
     * The workflow must be initialised via {@link #initialise} prior to use.
     *
     * @param help the help context
     */
    protected CheckInWorkflow(HelpContext help) {
        super(help);
        this.service = ServiceHelper.getArchetypeService();
    }

    /**
     * Starts the workflow.
     */
    @Override
    public void start() {
        super.start(initial);
    }

    /**
     * Initialises the workflow from an appointment.
     *
     * @param appointment the appointment
     * @param context     the external context to access and update
     */
    protected void initialise(Act appointment, Context context) {
        IMObjectBean bean = service.getBean(appointment);
        Party customer = bean.getTarget("customer", Party.class);
        Party patient = bean.getTarget("patient", Party.class);
        User clinician = UserHelper.useLoggedInClinician(context) ? context.getUser()
                                                                  : bean.getTarget("clinician", User.class);

        initialise(appointment, customer, patient, clinician, context);
    }

    /**
     * Returns the time that the customer arrived for the appointment.
     * <p/>
     * This is used to:
     * <ul>
     * <li>select a Visit</li>
     * <li>set the start time of a customer task</li>
     * <li>set the arrivalTime on the appointment</li>
     * </ul>
     *
     * @return the arrival time. Defaults to now.
     */
    protected Date getArrivalTime() {
        return new Date();
    }

    /**
     * Returns the initial context.
     *
     * @return the initial context
     */
    protected Context getInitialContext() {
        return initial;
    }

    /**
     * Creates a new {@link EditVisitTask}.
     *
     * @return a new task to edit the visit
     */
    protected EditVisitTask createEditVisitTask() {
        return new EditVisitTask();
    }

    /**
     * Initialise the workflow.
     *
     * @param appointment the appointment. May be {@code null}
     * @param customer    the customer
     * @param patient     the patient
     * @param clinician   the clinician. May be {@code null}
     * @param context     the external context to access and update
     */
    private void initialise(Act appointment, Party customer, Party patient, User clinician, Context context) {
        flowSheetServiceFactory = ServiceHelper.getBean(FlowSheetServiceFactory.class);
        if (context.getPractice() == null) {
            throw new IllegalStateException("Context has no practice");
        }
        external = context;
        HelpContext help = getHelpContext();
        initial = new DefaultTaskContext(help);
        if (appointment != null) {
            initial.addObject(appointment);
            IMObjectBean bean = service.getBean(appointment);
            Entity schedule = bean.getTarget("schedule", Entity.class);
            if (schedule != null) {
                initial.addObject(schedule);
            }
        }
        initial.setCustomer(customer);
        initial.setPatient(patient);
        initial.setClinician(clinician);
        initial.setUser(external.getUser());
        initial.setWorkListDate(new Date());
        initial.setScheduleDate(external.getScheduleDate());
        initial.setPractice(external.getPractice());
        initial.setLocation(external.getLocation());
        initial.setDepartment(external.getDepartment());

        Date arrivalTime = getArrivalTime();

        addTask(new CheckInDialogTask(getHelpContext()));

        // get the latest invoice, or create one if none is available
        addTask(new GetInvoiceTask());
        addTask(new ConditionalCreateTask(CustomerAccountArchetypes.INVOICE));

        // need to generate any admission messages prior to invoice editing
        addTask(new AdmissionTask());

        // edit the act.patientClinicalEvent in a local context, propagating the patient, customer and department
        // on completion.
        // If the task is cancelled, generate HL7 cancel admission messages
        EditVisitTask editVisitTask = createEditVisitTask();
        editVisitTask.addTaskListener(new DefaultTaskListener() {
            @Override
            public void taskEvent(TaskEvent event) {
                if (event.getType() == TaskEvent.Type.CANCELLED) {
                    CancelAdmissionTask task = new CancelAdmissionTask();
                    task.execute(getContext());
                }
            }
        });
        addTask(new LocalTask(editVisitTask, Context.PATIENT_SHORTNAME, Context.CUSTOMER_SHORTNAME,
                              PracticeArchetypes.DEPARTMENT));

        // Reload the task to refresh the context with any edits made
        addTask(new ReloadTask(PatientArchetypes.CLINICAL_EVENT));

        if (appointment != null) {
            addTask(new UpdateAppointmentTask(appointment, arrivalTime));
        }

        // add a task to update the global context at the end of the workflow
        addTask(new SynchronousTask() {
            public void execute(TaskContext context) {
                external.setPatient(context.getPatient());
                external.setCustomer(context.getCustomer());
                external.setDepartment(context.getDepartment());
            }
        });
    }

    class CheckInDialogTask extends Tasks {

        private CheckInDialog dialog;

        /**
         * Constructs a {@code Tasks}.
         *
         * @param help the help context
         */
        CheckInDialogTask(HelpContext help) {
            super(help);
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void start(TaskContext context) {
            dialog = new CheckInDialog(context.getCustomer(), context.getPatient(), context.getSchedule(),
                                       context.getClinician(), context.getPractice(), context.getLocation(),
                                       getArrivalTime(), context.getAppointment(), context.getUser(),
                                       context.getHelpContext());
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    context.setPatient(dialog.getPatient());
                    context.setClinician(dialog.getClinician());
                    context.addObject(dialog.getEvent());
                    context.setAppointment(dialog.getAppointment());
                    context.setTask(dialog.getTask());

                    FlowSheetInfo flowsheet = dialog.getFlowSheetInfo();
                    boolean completed = true;
                    if (flowsheet != null) {
                        addTask(new CreateFlowSheetTask(flowsheet));
                        completed = false;
                    }

                    Collection<Entity> templates = dialog.getTemplates();
                    if (!templates.isEmpty()) {
                        addPrintTasks(templates, context);
                        completed = false;
                    }
                    if (completed) {
                        notifyCompleted();
                    } else {
                        CheckInDialogTask.super.start(context);
                    }
                }

                @Override
                public void onAction(String action) {
                    notifyCancelled();
                }
            });
            dialog.show();
        }

        /**
         * Returns the dialog.
         *
         * @return the dialog, or {@code null} if it hasn't been displayed
         */
        public CheckInDialog getCheckInDialog() {
            return dialog;
        }

        /**
         * Generate documents from a list of templates, and queue tasks to print them.
         *
         * @param templates the templates
         * @param context   the context
         */
        private void addPrintTasks(Collection<Entity> templates, TaskContext context) {
            CustomerMailContext mailContext = new CustomerMailContext(context, context.getHelpContext());
            for (Entity entity : templates) {
                DocumentTemplate template = new DocumentTemplate(entity, service);
                String type = template.getType();
                if (type != null) {
                    Act document = service.create(type, Act.class);
                    IMObjectBean bean = service.getBean(document);
                    bean.setTarget("patient", context.getPatient());
                    bean.setTarget("documentTemplate", entity);
                    bean.setTarget("clinician", context.getClinician());
                    addTask(new PrintPatientActTask(document, mailContext, PrintIMObjectTask.PrintMode.BACKGROUND));
                }
            }
        }
    }

    /**
     * Task to create a flow sheet.
     */
    private class CreateFlowSheetTask extends AbstractTask {

        private final FlowSheetInfo info;

        /**
         * Creates a new {@link CreateFlowSheetTask}.
         */
        CreateFlowSheetTask(FlowSheetInfo info) {
            this.info = info;
            setRequired(false);
        }

        /**
         * Starts the task.
         *
         * @param context the task context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void start(TaskContext context) {
            boolean popup = false;
            Act visit = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
            Party patient = context.getPatient();
            Party location = context.getLocation();
            if (visit != null && location != null) {
                PatientContextFactory factory = ServiceHelper.getBean(PatientContextFactory.class);
                PatientContext patientContext = factory.createContext(patient, visit, location);
                try {
                    HospitalizationService service = flowSheetServiceFactory.getHospitalizationService(location);
                    service.add(patientContext, info.getExpectedStay(), info.getDepartmentId(), info.getTemplate());
                } catch (Throwable exception) {
                    popup = true;
                    ErrorHelper.show(exception, this::notifyCompleted);
                }
            }
            if (!popup) {
                notifyCompleted();
            }
        }
    }

    private class UpdateAppointmentTask extends SynchronousTask {

        /**
         * The appointment to update.
         */
        private final Act appointment;

        /**
         * The customer arrival time.
         */
        private final Date arrivalTime;

        /**
         * Constructs an {@link UpdateAppointmentTask}.
         *
         * @param appointment the appointment to update
         * @param arrivalTime the customer arrival time
         */
        UpdateAppointmentTask(Act appointment, Date arrivalTime) {
            this.appointment = appointment;
            this.arrivalTime = arrivalTime;
        }

        /**
         * Executes the task.
         *
         * @param context the task context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void execute(TaskContext context) {
            Act current = service.get(appointment.getObjectReference(), Act.class);
            if (current != null && current.getStatus().equals(appointment.getStatus())) {
                // only update the appointment if its status, customer and patient haven't changed
                IMObjectBean existingBean = service.getBean(appointment);
                IMObjectBean currentBean = service.getBean(current);
                if (sameRef(existingBean, currentBean, "customer") && sameRef(existingBean, currentBean, "patient")) {
                    current.setStatus(AppointmentStatus.CHECKED_IN);
                    currentBean.setValue("arrivalTime", arrivalTime);
                    Act task = context.getTask();
                    if (task != null) {
                        currentBean.setTarget("tasks", task);
                    }
                    if (!SaveHelper.save(current)) {
                        notifyCancelled();
                    }
                    context.setAppointment(current);
                }
            }
        }

        private boolean sameRef(IMObjectBean bean1, IMObjectBean bean2, String node) {
            return Objects.equals(bean1.getTargetRef(node), bean2.getTargetRef(node));
        }
    }

    private static class AdmissionTask extends SynchronousTask {

        /**
         * Executes the task.
         *
         * @throws OpenVPMSException for any error
         */
        @Override
        public void execute(TaskContext context) {
            PatientContextFactory factory = ServiceHelper.getBean(PatientContextFactory.class);
            Act visit = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
            PatientContext pc = factory.createContext(context.getPatient(), context.getCustomer(), visit,
                                                      context.getLocation(), context.getClinician());
            PatientInformationService service = ServiceHelper.getBean(PatientInformationService.class);
            service.admitted(pc);
        }
    }

    private static class CancelAdmissionTask extends SynchronousTask {

        /**
         * Executes the task.
         *
         * @throws OpenVPMSException for any error
         */
        @Override
        public void execute(TaskContext context) {
            PatientContextFactory factory = ServiceHelper.getBean(PatientContextFactory.class);
            Act visit = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
            PatientContext pc = factory.createContext(context.getPatient(), context.getCustomer(), visit,
                                                      context.getLocation(), context.getClinician());
            PatientInformationService service = ServiceHelper.getBean(PatientInformationService.class);
            service.admissionCancelled(pc);
        }
    }

}
