/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.RemoveConfirmationHandler;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.patient.investigation.PatientInvestigationActEditor;

import java.util.List;
import java.util.Map;

/**
 * Implementation if {@link RemoveConfirmationHandler} for {@link ChargeItemRelationshipCollectionEditor}.
 * <p/>
 * If the charge item being removed has minimum quantities or is associated with an investigation, this displays a
 * confirmation.
 *
 * @author Tim Anderson
 */
public class InvoiceRemoveConfirmationHandler extends ChargeRemoveConfirmationHandler {

    /**
     * The investigation manager.
     */
    private final InvestigationManager manager;

    /**
     * Constructs a {@link InvoiceRemoveConfirmationHandler}.
     *
     * @param manager the investigation manager
     * @param context the context
     * @param help    the help context
     */
    public InvoiceRemoveConfirmationHandler(InvestigationManager manager, Context context, HelpContext help) {
        super(context, help);
        this.manager = manager;
    }

    /**
     * Returns the removal states for a list of objects.
     *
     * @param objects    the objects to remove
     * @param collection the collection to remove them from
     * @return the removal states
     */
    @Override
    protected ChargeRemovalStates getRemovalStates(List<IMObject> objects,
                                                   AbstractChargeItemRelationshipCollectionEditor collection) {
        InvoiceItemRemovalStates result = new InvoiceItemRemovalStates(manager);
        for (IMObject object : objects) {
            FinancialAct item = (FinancialAct) object;
            if (checkRemovalState(item, collection, result)) {
                CustomerChargeActItemEditor itemEditor = (CustomerChargeActItemEditor) collection.getEditor(item);
                if (itemEditor != null) {
                    result.determineAffectedInvestigations(itemEditor);
                }
            }
        }
        for (Map.Entry<FinancialAct, List<PatientInvestigationActEditor>> entry
                : result.getAffectedEditorsByInvoiceItem().entrySet()) {
            FinancialAct item = entry.getKey();
            List<PatientInvestigationActEditor> editors = entry.getValue();
            for (PatientInvestigationActEditor editor : editors) {
                String displayName = getDisplayName(editor);
                if (editor.canEdit()) {
                    if (result.hasNoMoreTests(editor)) {
                        String message = Messages.format("customer.charge.delete.investigation.remove", displayName);
                        result.addStatus(item, ChargeRemovalStates.Status.OK, message);
                    } else {
                        String message = Messages.format("customer.charge.delete.investigation.removetest",
                                                         displayName);
                        result.addStatus(item, ChargeRemovalStates.Status.OK, message);
                    }
                } else {
                    String message = Messages.format("customer.charge.delete.investigation.cancel", displayName);
                    result.addStatus(item, ChargeRemovalStates.Status.OK, message);
                }
            }
        }
        return result;
    }

    /**
     * Returns a display name for an investigation.
     *
     * @param editor the investigation editor
     * @return the display name for the investigation
     */
    private String getDisplayName(PatientInvestigationActEditor editor) {
        Entity investigationType = editor.getInvestigationType();
        return (investigationType != null) ? investigationType.getName() : Messages.format("imobject.none");
    }

}
