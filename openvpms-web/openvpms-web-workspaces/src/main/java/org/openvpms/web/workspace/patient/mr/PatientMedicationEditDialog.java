/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.EditActions;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActEditDialog;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.print.BasicPrinterListener;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * Edit dialog for <em>act.patientMedication</em> that provides a Print & OK button to print and then close the dialog.
 *
 * @author Tim Anderson
 */
public class PatientMedicationEditDialog extends ActEditDialog {

    /**
     * The 'Print & OK' button.
     */
    public static final String PRINT_OK = "button.printOK";

    /**
     * Constructs a {@link PatientMedicationEditDialog}.
     *
     * @param editor  the editor
     * @param context the context
     */
    public PatientMedicationEditDialog(PatientMedicationActEditor editor, Context context) {
        this(editor, EditActions.applyOKCancel(), context);
    }

    /**
     * Constructs a {@link PatientMedicationEditDialog}.
     *
     * @param editor  the editor
     * @param actions the supported dialog actions
     * @param context the context
     */
    public PatientMedicationEditDialog(IMObjectEditor editor, EditActions actions, Context context) {
        super(editor, actions, context);
        ButtonSet buttons = getButtons();
        // add a Print & OK button before OK
        int index = buttons.indexOf(OK_ID);
        if (index == -1) {
            // make first button
            index = 0;
        }
        buttons.add(PRINT_OK, index).addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onPrintOK();
            }
        });
    }

    /**
     * Invoked when 'Print & OK' is pressed.
     * <br/>
     * Prints the label and closes the dialog.
     */
    private void onPrintOK() {
        IMObjectEditor editor = getEditor();
        if (editor != null) {
            IMObject object = editor.getObject();
            Context context = getContext();
            HelpContext help = getHelpContext();
            DocumentTemplateLocator locator = new ContextDocumentTemplateLocator(object, context);
            IMPrinterFactory factory = ServiceHelper.getBean(IMPrinterFactory.class);
            IMPrinter<IMObject> printer = factory.create(object, locator, context);
            InteractiveIMPrinter<IMObject> iPrinter = new InteractiveIMPrinter<>(printer, context, help);
            iPrinter.setInteractive(false);
            iPrinter.setListener(new BasicPrinterListener() {
                @Override
                public void printed(DocumentPrinter printer) {
                    onOK();
                }
            });
            iPrinter.print();
        }
    }
}