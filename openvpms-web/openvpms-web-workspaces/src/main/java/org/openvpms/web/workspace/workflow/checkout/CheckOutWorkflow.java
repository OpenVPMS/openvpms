/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.hl7.patient.PatientContext;
import org.openvpms.hl7.patient.PatientContextFactory;
import org.openvpms.hl7.patient.PatientInformationService;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.client.HospitalizationService;
import org.openvpms.smartflow.model.Hospitalization;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextException;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.retry.AbstractRetryable;
import org.openvpms.web.component.retry.Retryable;
import org.openvpms.web.component.retry.Retryer;
import org.openvpms.web.component.workflow.AbstractConfirmationTask;
import org.openvpms.web.component.workflow.AbstractTask;
import org.openvpms.web.component.workflow.ConditionalCreateTask;
import org.openvpms.web.component.workflow.ConditionalTask;
import org.openvpms.web.component.workflow.ConfirmationTask;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.EvalTask;
import org.openvpms.web.component.workflow.ReloadTask;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.Task;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.TaskListener;
import org.openvpms.web.component.workflow.TaskProperties;
import org.openvpms.web.component.workflow.Tasks;
import org.openvpms.web.component.workflow.UpdateIMObjectTask;
import org.openvpms.web.component.workflow.WorkflowImpl;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.charge.UndispensedOrderChecker;
import org.openvpms.web.workspace.customer.charge.UndispensedOrderDialog;
import org.openvpms.web.workspace.workflow.GetClinicalEventTask;
import org.openvpms.web.workspace.workflow.MandatoryCustomerAlertTask;
import org.openvpms.web.workspace.workflow.MandatoryPatientAlertTask;
import org.openvpms.web.workspace.workflow.payment.PaymentWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.openvpms.web.component.workflow.TaskFactory.eq;
import static org.openvpms.web.component.workflow.TaskFactory.ne;
import static org.openvpms.web.component.workflow.TaskFactory.when;


/**
 * Check-out workflow.
 *
 * @author Tim Anderson
 */
public class CheckOutWorkflow extends WorkflowImpl {

    public enum ClaimAtCheckout {
        ALL,            // prompt for both e-claims and gap claims
        NONE,           // no claim prompting
        STANDARD_CLAIM, // standard claims only
        GAP_CLAIM       // gap claims only
    }

    /**
     * The external context to access and update.
     */
    private final Context external;

    /**
     * The collected events.
     */
    private final Visits visits;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The appointment rules.
     */
    private final AppointmentRules appointmentRules;

    /**
     * The insurance rules.
     */
    private final InsuranceRules insuranceRules;

    /**
     * The flow sheet service factory.
     */
    private final FlowSheetServiceFactory flowSheetServiceFactory;

    /**
     * Claim at checkout configuration.
     */
    private final ClaimAtCheckout claimAtCheckout;

    /**
     * The initial context.
     */
    private TaskContext initial;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(CheckOutWorkflow.class);


    /**
     * Constructs a {@link CheckOutWorkflow} from an <em>act.customerAppointment</em> or <em>act.customerTask</em>.
     *
     * @param act     the appointment or task
     * @param context the external context to access and update
     * @param help    the help context
     */
    public CheckOutWorkflow(Act act, Context context, HelpContext help) {
        super(help.topic("workflow/checkout"));
        Party practice = context.getPractice();
        if (practice == null) {
            throw new IllegalStateException("Context has no practice");
        }
        external = context;
        service = ServiceHelper.getArchetypeService();
        appointmentRules = ServiceHelper.getBean(AppointmentRules.class);
        insuranceRules = ServiceHelper.getBean(InsuranceRules.class);
        visits = new Visits(context.getCustomer(), appointmentRules, ServiceHelper.getBean(PatientRules.class),
                            service);
        flowSheetServiceFactory = ServiceHelper.getBean(FlowSheetServiceFactory.class);

        claimAtCheckout = getClaimAtCheckout(practice);

        initialise(act, getHelpContext());
    }

    /**
     * Starts the workflow.
     */
    @Override
    public void start() {
        super.start(initial);
    }

    /**
     * Returns the events.
     *
     * @return the events
     */
    public Visits getVisits() {
        return visits;
    }

    /**
     * Creates a new task to performing charging.
     *
     * @param visits the events
     * @return a new task
     */
    protected CheckoutEditInvoiceTask createChargeTask(Visits visits) {
        return new CheckoutEditInvoiceTask(visits);
    }

    /**
     * Creates a new payment workflow.
     *
     * @param context the context
     * @return a new payment workflow
     */
    protected PaymentWorkflow createPaymentWorkflow(TaskContext context) {
        return new PaymentWorkflow(context, external, context.getHelpContext().subtopic("pay"));
    }

    /**
     * Returns a condition task to determine if the invoice should be posted.
     *
     * @return a new condition
     */
    protected EvalTask<Boolean> getPostCondition() {
        String invoiceTitle = Messages.get("workflow.checkout.postinvoice.title");
        String invoiceMsg = Messages.get("workflow.checkout.postinvoice.message");
        return new ConfirmationTask(invoiceTitle, invoiceMsg, getHelpContext().subtopic("post"));
    }

    /**
     * Initialise the workflow.
     *
     * @param act     the appointment or task
     * @param help the help context
     */
    private void initialise(Act act, HelpContext help) {
        IMObjectBean bean = service.getBean(act);
        Party customer = bean.getTarget("customer", Party.class);
        Party patient = bean.getTarget("patient", Party.class);
        List<Act> policies = Collections.emptyList();

        if (customer == null) {
            throw new ContextException(ContextException.ErrorCode.NoCustomer);
        }
        if (patient == null) {
            throw new ContextException(ContextException.ErrorCode.NoPatient);
        }

        User clinician;
        if (UserHelper.useLoggedInClinician(external)) {
            clinician = external.getUser();
        } else {
            clinician = bean.getTarget("clinician", User.class);
            if (clinician == null) {
                clinician = external.getClinician();
            }
        }

        initial = new DefaultTaskContext(help);
        initial.addObject(act);
        initial.setCustomer(customer);
        initial.setPatient(patient);
        initial.setClinician(clinician);

        initial.setUser(external.getUser());
        initial.setPractice(external.getPractice());
        initial.setLocation(external.getLocation());
        initial.setDepartment(external.getDepartment());

        addTask(new MandatoryCustomerAlertTask());
        addTask(new MandatoryPatientAlertTask());

        Act appointment = getAppointment(act, patient);
        if (appointment != null && appointmentRules.isBoardingAppointment(appointment)) {
            addTask(new GetBoardingAppointmentsTask(appointment, visits));
            // TODO - don't support claims for boarding appointments yet
        } else {
            // add the most recent clinical event to the context
            addTask(new GetClinicalEventTask(act.getActivityStartTime()));

            // copy the event to Events
            addTask(new SynchronousTask() {
                @Override
                public void execute(TaskContext context) {
                    Act event = (Act) context.getObject(PatientArchetypes.CLINICAL_EVENT);
                    if (event == null) {
                        throw new ContextException(ContextException.ErrorCode.NoClinicalEvent);
                    }
                    visits.add(event, appointment);
                }
            });

            if (claimAtCheckout != ClaimAtCheckout.NONE) {
                // prompt for insurance claims - check if the patient has a policy/policies
                policies = insuranceRules.getCurrentPolicies(customer, patient);
            }
        }

        if (flowSheetServiceFactory.isSmartFlowSheetEnabled(initial.getLocation())) {
            addTask(new BatchDischargeFromSmartFlowSheetTask(visits, help));
        }

        addTask(new GetCheckOutInvoiceTask(visits));
        // populate the context with an invoice, if one is present. This may return a POSTED invoice

        addTask(new ConditionalCreateTask(CustomerAccountArchetypes.INVOICE));
        // create a new invoice if no invoice is available

        addTask(new InvoiceTask(getHelpContext()));
        // perform invoicing

        // on save, determine if the user wants to post the invoice, but only if it's not already posted
        addTask(when(ne(CustomerAccountArchetypes.INVOICE, "status", ActStatus.POSTED), getPostTask()));

        Tasks tasks = new Tasks(help);
        if (!policies.isEmpty() && (claimAtCheckout == ClaimAtCheckout.ALL
                                    || claimAtCheckout == ClaimAtCheckout.GAP_CLAIM)) {
            // gap claims need to be submitted for unpaid invoices that haven't already been claimed
            tasks.addTask(when(new UnclaimedUnpaidInvoice(), new InsuranceClaimTask(policies, true, help)));
        }

        // if the invoice is posted and the customer has a positive balance, prompt to pay the account.
        // Need to reload the invoice to get the updated allocation
        PaymentWorkflow payWorkflow = createPaymentWorkflow(initial);
        payWorkflow.addTask(new ReloadTask(CustomerAccountArchetypes.INVOICE));
        payWorkflow.setRequired(false);

        tasks.addTask(when(new PositiveBalance(), payWorkflow));
        if (!policies.isEmpty() && (claimAtCheckout == ClaimAtCheckout.ALL
                                    || claimAtCheckout == ClaimAtCheckout.STANDARD_CLAIM)) {
            // standard claims need to be submitted for paid invoices
            tasks.addTask(when(new UnclaimedPaidInvoice(), new InsuranceClaimTask(policies, false, help)));
        }

        addTask(when(eq(CustomerAccountArchetypes.INVOICE, "status", ActStatus.POSTED), tasks));

        // print acts and documents created since the events or invoice was created
        addTask(new PrintTask(visits, help.subtopic("print")));

        // add a follow-up task, if it is configured for the practice location
        if (followUp()) {
            addTask(new FollowUpTask(help));
        }

        // update the appointments and events setting their status to COMPLETED and endTime to now.
        // If the invoice was posted, the events should already be completed.
        addTask(new UpdateEventTask(visits));
        addTask(new DischargeTask(visits));

        // update the act status if it is a task
        if (act.isA(ScheduleArchetypes.TASK)) {
            TaskProperties appProps = new TaskProperties();
            appProps.add("status", ActStatus.COMPLETED);
            appProps.add("endTime", Date::new);
            UpdateIMObjectTask<IMObject> task = new UpdateIMObjectTask<>(act.getArchetype(), appProps);
            task.skipIfMissing();   // if the task has been deleted, ignore it
            addTask(task);
        }

        // add a task to update the context at the end of the workflow
        addTask(new SynchronousTask() {
            public void execute(TaskContext context) {
                external.setCustomer(context.getCustomer());
                external.setPatient(context.getPatient());
                external.setTill(context.getTill());
                external.setTerminal(context.getTerminal());
                external.setClinician(context.getClinician());
                external.setDepartment(context.getDepartment());
            }
        });
    }

    /**
     * Returns the appointment associated with an act, falling back to the active appointment if none exists.
     *
     * @param act     the act
     * @param patient the patient
     * @return the appointment, or {@code null} if none exists
     */
    private Act getAppointment(Act act, Party patient) {
        Act result;
        if (act.isA(ScheduleArchetypes.APPOINTMENT)) {
            result = act;
        } else {
            IMObjectBean bean = service.getBean(act);
            result = bean.getSource("appointments", Act.class);
            if (result == null) {
                result = appointmentRules.getActivePatientAppointment(patient);
            }
        }
        return result;
    }

    /**
     * Returns a task to post the invoice.
     * <p>
     * This first confirms that the invoice should be posted, and then checks if there are any undispensed orders.
     * If so, displays a confirmation dialog before posting.
     *
     * @return a task to post the invoice
     */
    private Task getPostTask() {
        HelpContext help = getHelpContext().subtopic("post");
        Tasks postTasks = new Tasks(help);
        postTasks.addTask(new PostInvoiceTask(service));
        postTasks.setRequired(false);

        EvalTask<Boolean> confirmPost = getPostCondition();
        EvalTask<Boolean> checkUndispensed = new UndispensedOrderTask(help);
        ConditionalTask post = new ConditionalTask(confirmPost, when(checkUndispensed, postTasks));
        post.setRequired(false);
        return post;
    }

    /**
     * Determines if a follow-up task should be created.
     *
     * @return {@code true} if a follow-up task should be created
     */
    private boolean followUp() {
        boolean result = false;
        Party practice = external.getPractice();
        if (practice != null) {
            IMObjectBean bean = service.getBean(practice);
            result = bean.getBoolean("followUpAtCheckOut");
        }
        return result;
    }

    private CheckOutWorkflow.ClaimAtCheckout getClaimAtCheckout(Party practice) {
        String result = service.getBean(practice).getString("claimAtCheckout", "ALL");
        return CheckOutWorkflow.ClaimAtCheckout.valueOf(result);
    }

    /**
     * A task to perform invoicing. If the current invoice is finalised, it will be displayed in a dialog for viewing,
     * with a New button provided to create a new invoice.
     * If the current invoice isn't finalised, an edit dialog will be displayed.
     */
    private class InvoiceTask extends Tasks {

        InvoiceTask(HelpContext help) {
            super(help);
        }

        @Override
        public void start(TaskContext context) {
            FinancialAct invoice = (FinancialAct) context.getObject(CustomerAccountArchetypes.INVOICE);
            if (invoice == null) {
                notifyCancelled();
            } else if (ActStatus.POSTED.equals(invoice.getStatus())) {
                InvoiceViewerDialog dialog = new InvoiceViewerDialog(invoice, visits, context,
                                                                     context.getHelpContext());
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onAction(String action) {
                        if (InvoiceViewerDialog.NEW_ID.equals(action)) {
                            onNew(context);
                        }
                    }

                    @Override
                    public void onOK() {
                        notifyCompleted();
                    }

                    @Override
                    public void onCancel() {
                        notifyCancelled();
                    }
                });
                dialog.show();
            } else {
                addTask(createChargeTask(visits));
                super.start(context);
            }
        }

        /**
         * Create a new invoice and display an editor.
         *
         * @param context the task context
         */
        private void onNew(TaskContext context) {
            FinancialAct invoice = service.create(CustomerAccountArchetypes.INVOICE, FinancialAct.class);
            context.addObject(invoice);
            addTask(createChargeTask(visits));
            InvoiceTask.super.start(context);
        }
    }

    private class BatchDischargeFromSmartFlowSheetTask extends Tasks {

        private final Visits visits;

        public BatchDischargeFromSmartFlowSheetTask(Visits visits, HelpContext help) {
            super(help);
            setRequired(false);
            this.visits = visits;
        }

        /**
         * Initialise any tasks.
         *
         * @param context the task context
         */
        @Override
        protected void initialise(TaskContext context) {
            Party location = context.getLocation();
            boolean completed = true;
            if (location != null && !visits.isEmpty()) {
                PatientContextFactory factory = ServiceHelper.getBean(PatientContextFactory.class);
                HospitalizationService service = flowSheetServiceFactory.getHospitalizationService(location);
                for (Visit event : visits) {
                    Act act = event.getEvent();
                    PatientContext patientContext = factory.createContext(act, location);
                    Hospitalization hospitalization = service.getHospitalization(patientContext);
                    if (hospitalization != null && Hospitalization.ACTIVE_STATUS.equals(hospitalization.getStatus())) {
                        completed = false;
                        String title = Messages.get("workflow.checkout.flowsheet.discharge.title");
                        String message = Messages.format("workflow.checkout.flowsheet.discharge.message",
                                                         patientContext.getPatientFirstName());
                        ConfirmationTask confirm = new ConfirmationTask(title, message, true, getHelpContext());
                        DischargeFromSmartFlowSheetTask discharge
                                = new DischargeFromSmartFlowSheetTask(patientContext, service);
                        addTask(new ConditionalTask(confirm, discharge));
                    }
                }
            }
            if (completed) {
                notifyCompleted();
            }
        }
    }

    /**
     * Updates the patient clinical event, if it is not already completed.
     */
    private class UpdateEventTask extends SynchronousTask {

        /**
         * The events to update.
         */
        private final Visits visits;

        /**
         * Constructs an {@link UpdateEventTask}.
         */
        UpdateEventTask(Visits visits) {
            this.visits = visits;
        }

        /**
         * Executes the task.
         *
         * @param context the context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void execute(TaskContext context) {
            for (Visit event : visits) {
                Retryable action = new AbstractRetryable() {
                    @Override
                    protected boolean runFirst() {
                        return update(true, event);
                    }

                    @Override
                    protected boolean runAction() {
                        return update(false, event);
                    }
                };
                if (!Retryer.run(action)) {
                    notifyCancelled();
                }
            }
        }

        /**
         * Updates an event.
         *
         * @param visit  the visit
         * @param toSave collects objects to save
         * @return {@code true} if the event still exists, otherwise {@code false}
         */
        private boolean updateEvent(Visit visit, List<Act> toSave) {
            Act event = visit.reloadEvent();
            // reload the event, as it has most likely changed as a result of invoicing
            if (event != null && ActStatus.IN_PROGRESS.equals(event.getStatus())) {
                event.setStatus(ActStatus.COMPLETED);
                event.setActivityEndTime(new Date());
                toSave.add(event);
            }
            return event != null;
        }

        /**
         * Updates an object and saves it.
         *
         * @param first if {@code true}, this is the first invocation
         * @param event the event
         * @return {@code true} if the object exists, otherwise {@code false}
         */
        private boolean update(boolean first, Visit event) {
            List<Act> toSave = new ArrayList<>();
            boolean result = updateAppointment(first, event.getAppointment(), toSave);
            result |= updateEvent(event, toSave);
            if (!toSave.isEmpty()) {
                service.save(toSave);
            }
            return result;
        }

        /**
         * Updates an appointment.
         *
         * @param first       if {@code true} this is the first time it is being updated.
         * @param appointment the appointment
         * @param toSave      collects objects to save
         * @return {@code true} if the object exists, otherwise {@code false}
         */
        private boolean updateAppointment(boolean first, Act appointment, List<Act> toSave) {
            if (!first) {
                appointment = IMObjectHelper.reload(appointment);
            }
            if (appointment != null && !ActStatus.COMPLETED.equals(appointment.getStatus())) {
                appointment.setStatus(ActStatus.COMPLETED);
                toSave.add(appointment);
            }
            return appointment != null;
        }
    }

    /**
     * Displays a warning dialog if the invoice is to be finalised, but there are undispensed orders.
     */
    private static class UndispensedOrderTask extends AbstractConfirmationTask {

        /**
         * The undispensed order checker.
         */
        private UndispensedOrderChecker checker;

        /**
         * Constructs an {@link UndispensedOrderTask}.
         *
         * @param help the help context
         */
        UndispensedOrderTask(HelpContext help) {
            super(help);
        }

        /**
         * Starts the task.
         * <p>
         * The registered {@link TaskListener} will be notified on completion or failure.
         *
         * @param context the task context
         */
        @Override
        public void start(TaskContext context) {
            Act invoice = (Act) context.getObject(CustomerAccountArchetypes.INVOICE);
            if (invoice != null) {
                checker = new UndispensedOrderChecker(invoice);
                if (checker.hasUndispensedItems()) {
                    // display the confirmation dialog
                    super.start(context);
                } else {
                    setValue(true);
                }
            } else {
                setValue(false);
            }
        }

        /**
         * Creates a new confirmation dialog.
         *
         * @param context the context
         * @param help    the help context
         * @return a new confirmation dialog
         */
        @Override
        protected ConfirmationDialog createConfirmationDialog(TaskContext context, HelpContext help) {
            return new UndispensedOrderDialog(checker.getUndispensedItems(), help);
        }
    }

    /**
     * Task to determine if the invoice is POSTED, is not claimed, and is not fully paid.
     */
    private static class UnclaimedUnpaidInvoice extends EvalTask<Boolean> {
        @Override
        public void start(TaskContext context) {
            FinancialAct invoice = (FinancialAct) IMObjectHelper.reload(
                    context.getObject(CustomerAccountArchetypes.INVOICE));
            boolean value = false;
            if (invoice != null && ActStatus.POSTED.equals(invoice.getStatus())
                && !MathRules.isZero(invoice.getTotal())
                && invoice.getAllocatedAmount().compareTo(invoice.getTotal()) < 0) {
                InsuranceRules rules = ServiceHelper.getBean(InsuranceRules.class);
                value = !rules.isClaimed(invoice);
            }
            setValue(value);
        }
    }

    /**
     * Task to determine if the invoice is fully paid and unclaimed.
     */
    private static class UnclaimedPaidInvoice extends EvalTask<Boolean> {
        @Override
        public void start(TaskContext context) {
            FinancialAct invoice = (FinancialAct) IMObjectHelper.reload(
                    context.getObject(CustomerAccountArchetypes.INVOICE));
            boolean value = false;
            if (invoice != null && ActStatus.POSTED.equals(invoice.getStatus())
                && !MathRules.isZero(invoice.getTotal())
                && MathRules.equals(invoice.getTotal(), invoice.getAllocatedAmount())) {
                InsuranceRules rules = ServiceHelper.getBean(InsuranceRules.class);
                value = !rules.isClaimed(invoice);
            }
            setValue(value);
        }
    }

    private static class PositiveBalance extends EvalTask<Boolean> {

        /**
         * Starts the task.
         * <p>
         * The registered {@link TaskListener} will be notified on completion or failure.
         *
         * @param context the task context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void start(TaskContext context) {
            Party customer = context.getCustomer();
            CustomerAccountRules rules = ServiceHelper.getBean(CustomerAccountRules.class);
            BigDecimal balance = rules.getBalance(customer);
            if (balance.compareTo(BigDecimal.ZERO) > 0) {
                setValue(true);
            } else {
                setValue(false);
            }
        }
    }

    /**
     * Prints all unprinted acts and documents for the customer and patient.
     * This uses the minimum startTime of the <em>act.patientClinicalEvent</em>,
     * <em>act.customerAccountChargesInvoice</em> and time now to select the objects to print.
     */
    private static class PrintTask extends Tasks {

        private final Visits visits;

        /**
         * Constructs a {@link PrintTask}.
         *
         * @param visits the events
         * @param help   the help context
         */
        PrintTask(Visits visits, HelpContext help) {
            super(help);
            this.visits = visits;
        }

        /**
         * Initialise any tasks.
         *
         * @param context the task context
         */
        @Override
        protected void initialise(TaskContext context) {
            Date min = new Date();
            for (Visit event : visits) {
                min = getMin(min, event.getEvent().getActivityStartTime());
            }
            Act invoice = (Act) context.getObject(CustomerAccountArchetypes.INVOICE);
            if (invoice != null) {
                min = getMin(min, invoice.getActivityStartTime());
            }
            min = DateRules.getDate(min);  // print all documents done on or after the min date
            PrintDocumentsTask printDocs = new PrintDocumentsTask(visits.getPatients(), min, context.getHelpContext());
            printDocs.setRequired(false);
            addTask(printDocs);
        }


        /**
         * Returns the minimum of two dates.
         *
         * @param date1 the first date
         * @param date2 the second date
         * @return the minimum of the two dates
         */
        private Date getMin(Date date1, Date date2) {
            Date min = date1;
            if (date1.getTime() > date2.getTime()) {
                min = date2;
            }
            return min;
        }
    }

    private static class DischargeTask extends SynchronousTask {

        private final Visits visits;

        DischargeTask(Visits visits) {
            this.visits = visits;
        }

        /**
         * Executes the task.
         *
         * @throws OpenVPMSException for any error
         */
        @Override
        public void execute(TaskContext context) {
            PatientContextFactory factory = ServiceHelper.getBean(PatientContextFactory.class);
            PatientInformationService service = ServiceHelper.getBean(PatientInformationService.class);
            for (Visit event : visits) {
                Act act = event.getEvent();
                PatientContext pc = factory.createContext(event.getPatient(), context.getCustomer(), act,
                                                          context.getLocation(), context.getClinician());
                service.discharged(pc);
            }
        }
    }

    /**
     * Task to import flow sheet reports for a patient, if a patient has a Smart Flow Sheet hospitalisation.
     */
    private static class DischargeFromSmartFlowSheetTask extends AbstractTask {

        private final PatientContext patientContext;

        private final HospitalizationService service;

        public DischargeFromSmartFlowSheetTask(PatientContext context, HospitalizationService service) {
            this.patientContext = context;
            this.service = service;
        }

        /**
         * Starts the task.
         * <p>
         * The registered {@link TaskListener} will be notified on completion or failure.
         *
         * @param context the task context
         * @throws OpenVPMSException for any error
         */
        @Override
        public void start(final TaskContext context) {
            try {
                service.discharge(patientContext.getPatient(), patientContext.getVisit());
                notifyCompleted();
            } catch (Exception exception) {
                log.error(exception.getMessage(), exception);
                ErrorDialog.newDialog()
                        .title(Messages.get("workflow.checkout.flowsheet.discharge.title"))
                        .message(Messages.format("workflow.checkout.flowsheet.discharge.error", exception.getMessage()))
                        .yesNoCancel()
                        .yes(() -> start(context))
                        .no(this::notifyCompleted)
                        .cancel(this::notifyCancelled)
                        .show();
            }
        }
    }

}
