/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.query.BaseArchetypeConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.laboratory.Test.UseDevice;
import org.openvpms.laboratory.internal.service.OrderValidationService;
import org.openvpms.laboratory.internal.service.OrderValidationService.ValidationStatus;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.laboratory.service.OrderValidationStatus;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.edit.AbstractIMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.act.ParticipationCollectionEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.LookupFilter;
import org.openvpms.web.component.im.lookup.LookupQuery;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.AbstractEntityQuery;
import org.openvpms.web.component.im.query.FilteredResultSet;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.PatientDocumentActEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.openvpms.web.component.im.doc.DocumentActLayoutStrategy.DOCUMENT;


/**
 * An editor for <em>act.patientInvestigation</em> acts.
 *
 * @author Tim Anderson
 */
public class PatientInvestigationActEditor extends PatientDocumentActEditor {

    /**
     * The device participation editor.
     */
    private final ParticipationCollectionEditor deviceEditor;

    /**
     * The laboratory rules.
     */
    private final LaboratoryRules rules;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The tests.
     */
    private List<Entity> tests;

    /**
     * Flag to indicate if the Print Form button should be enabled or disabled.
     */
    private boolean enableButton;

    /**
     * Determines if the device can be edited.
     */
    private boolean showDevice;

    /**
     * Determines what states can be set for the order status node.
     */
    private OrderStatusPermission orderStatusPermission = OrderStatusPermission.READ_ONLY;

    /**
     * Determines if the order should be validated against a laboratory service. This is initially set,
     * and whenever the investigation type, device, or tests change to avoid redundant calls.
     */
    private boolean validateOrder = true;

    /**
     * The identifier of a laboratory alert returned by {@link AlertListener#onAlert(String)}, used for alert
     * cancellation.
     */
    private String labAlert;

    /**
     * Protected document node.
     */
    static final String PROTECTED_DOCUMENT = "protectedDocument";

    /**
     * Investigation type node.
     */
    private static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Group node.
     */
    private static final String GROUP = "group";

    /**
     * Products node.
     */
    private static final String PRODUCTS = "products";

    /**
     * Practice location node.
     */
    private static final String LOCATION = "location";

    /**
     * The laboratory node.
     */
    private static final String LABORATORY = "laboratory";

    /**
     * The laboratory device node.
     */
    private static final String DEVICE = "device";

    /**
     * The clinician node.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Order status node.
     */
    private static final String ORDER_STATUS = "status2";

    /**
     * Confirm order node.
     */
    private static final String CONFIRM_ORDER = "confirmOrder";

    /**
     * Tests node.
     */
    private static final String TESTS = "tests";

    /**
     * Description node.
     */
    private static final String DESCRIPTION = "description";

    /**
     * Constructs a {@link PatientInvestigationActEditor}.
     *
     * @param act     the act
     * @param parent  the parent act. May be {@code null}
     * @param context the layout context
     */
    public PatientInvestigationActEditor(DocumentAct act, Act parent, LayoutContext context) {
        super(act, parent, context);
        rules = ServiceHelper.getBean(LaboratoryRules.class);
        domainService = ServiceHelper.getBean(DomainService.class);

        if (act.isNew()) {
            initParticipant(LOCATION, context.getContext().getLocation());
        }
        deviceEditor = createDeviceCollectionEditor();
        addEditor(deviceEditor);

        updateOrderStatusPermission(getInvestigationType(), getLaboratory());

        getProperty(DOCUMENT).addModifiableListener(modifiable -> onDocumentChanged());
    }

    /**
     * Updates the investigation type, if it is not the same as the existing one.
     *
     * @param investigationType the investigation type. May be {@code null}
     */
    public void setInvestigationType(Entity investigationType) {
        checkEditable();
        setParticipant(INVESTIGATION_TYPE, investigationType);
    }

    /**
     * Adds a product, if it does not already exist.
     *
     * @param product the product to add
     */
    public void addProduct(Product product) {
        checkEditable();
        List<Reference> refs = getBean(getObject()).getTargetRefs(PRODUCTS);
        if (!refs.contains(product.getObjectReference())) {
            addParticipant(PRODUCTS, product);
            onLayout();   // required to force display of the product
        }
    }

    /**
     * Removes a product.
     *
     * @param product the product to remove
     */
    public void removeProduct(Product product) {
        checkEditable();
        if (removeParticipant(PRODUCTS, product)) {
            onLayout();
        }
    }

    /**
     * Sets the location.
     *
     * @param location the location. May be {@code null}
     */
    public void setLocation(Party location) {
        setParticipant(LOCATION, location);
    }

    /**
     * Returns the location.
     *
     * @return the location. May be {@code null}
     */
    public Party getLocation() {
        return (Party) getParticipant(LOCATION);
    }

    /**
     * Returns the location reference.
     *
     * @return the location reference. May be {@code null}
     */
    public Reference getLocationRef() {
        return getParticipantRef(LOCATION);
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    public void save() {
        boolean isNew = getObject().isNew();
        super.save();
        if (isNew) {
            // enable printing of the form if the act has been saved and was previously unsaved
            enableButton = true; // getObject().isNew() will be true until transaction commits, so need this flag
            onLayout();
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        return new PatientInvestigationActEditor(reload(getObject()), (Act) getParent(), getLayoutContext());
    }

    /**
     * Determines if a test can be added to this investigation.
     *
     * @param patient the patient
     * @param test    the test
     * @return {@code true} if the test can be added to this investigation, otherwise {@code false}
     */
    public boolean canAddTest(Party patient, Entity test) {
        boolean result = false;
        if (test.isActive() && canEdit()) {
            IMObjectBean bean = getBean(test);
            if (bean.getBoolean(GROUP) && Objects.equals(getPatientRef(), patient.getObjectReference())) {
                Reference ref = bean.getTargetRef(INVESTIGATION_TYPE);
                if (ref != null && ref.equals(getInvestigationTypeRef())) {
                    List<Entity> tests = getTests();
                    if (!tests.contains(test)) {
                        if (canGroup(tests)) {
                            result = true;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the tests.
     *
     * @return the tests
     */
    public List<Entity> getTests() {
        if (tests == null) {
            tests = getParticipants(TESTS);
        }
        return tests;
    }

    /**
     * Adds a test.
     *
     * @param test the test to add
     */
    public void addTest(Entity test) {
        checkEditable();
        addParticipant(TESTS, test);
        tests = null;
        onTestChanged();
    }

    /**
     * Removes a test.
     *
     * @param test the test to remove
     */
    public void removeTest(Entity test) {
        checkEditable();
        removeParticipant(TESTS, test);
        tests = null;
        onTestChanged();
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type. May be {@code null}
     */
    public Entity getInvestigationType() {
        return getParticipant(INVESTIGATION_TYPE);
    }

    /**
     * Returns the investigation type reference.
     *
     * @return the investigation type reference. May be {@code null}
     */
    public Reference getInvestigationTypeRef() {
        return getParticipantRef(INVESTIGATION_TYPE);
    }

    /**
     * Returns the laboratory.
     *
     * @return the laboratory. May be {@code null}
     */
    public Entity getLaboratory() {
        return getParticipant(LABORATORY);
    }

    /**
     * Returns the device.
     *
     * @return the device. May be {@code null}
     */
    public Entity getDevice() {
        return getParticipant(DEVICE);
    }

    /**
     * Sets the device.
     *
     * @param device the device. May be {@code null}
     */
    public void setDevice(Entity device) {
        setParticipant(DEVICE, device);
    }

    /**
     * Returns the device reference.
     *
     * @return the device reference. May be {@code null}
     */
    public Reference getDeviceRef() {
        return getParticipantRef(DEVICE);
    }

    /**
     * Determines if the investigation generates a laboratory order that needs to be confirmed.
     *
     * @return {@code true} if the order needs to be confirmed
     */
    public boolean confirmOrder() {
        boolean result = false;
        if (canEdit()) {
            result = getProperty(CONFIRM_ORDER).getBoolean(false);
        }
        return result;
    }

    /**
     * Returns the order status.
     *
     * @return the order status
     */
    public String getOrderStatus() {
        return getObject().getStatus2();
    }

    /**
     * Determines if the investigation is editable.
     *
     * @return {@code true} if the order status is {@code PENDING} and not locked
     */
    public boolean canEdit() {
        String status = getOrderStatus();
        return InvestigationActStatus.PENDING.equals(status) && !isLocked();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        PropertyComponentEditor orderStatusEditor = createOrderStatus();
        PatientInvestigationActLayoutStrategy strategy = new PatientInvestigationActLayoutStrategy(
                getDocumentEditor(), getVersionsEditor(), (showDevice) ? deviceEditor : null,
                orderStatusEditor, isLocked());
        strategy.setEnableButton(enablePrintForm());
        return strategy;
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        monitorParticipation(INVESTIGATION_TYPE, (entity) -> onInvestigationTypeChanged());
        monitorParticipation(DEVICE, (entity) -> setValidateOrder());
        monitorParticipation(CLINICIAN, (entity) -> setValidateOrder());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateTests(validator) && validateOrder(validator);
    }

    /**
     * Validates that the start and end times are valid.
     * <p/>
     * This implementation always returns {@code true}
     *
     * @param validator the validator
     * @return {@code true}
     */
    @Override
    protected boolean validateStartEndTimes(Validator validator) {
        return true;
    }

    /**
     * Deletes the object.
     *
     * @throws OpenVPMSException if the delete fails
     */
    @Override
    protected void doDelete() {
        deleteChildDocuments();
        super.doDelete();
    }

    /**
     * Returns the laboratory services.
     *
     * @return the laboratory services
     */
    protected LaboratoryServices getLaboratoryServices() {
        return ServiceHelper.getBean(LaboratoryServices.class);
    }

    /**
     * Returns the order validation service.
     *
     * @return the order validation service
     */
    protected OrderValidationService getValidationService() {
        return ServiceHelper.getBean(OrderValidationService.class);
    }

    /**
     * Invoked when the document changes.
     * <p/>
     * This:
     * <li>sets the <em>protectedDocument</em> flag to protect the document from being replaced by any lab API.</li>
     * <li>changes the order status from SENT to RECEIVED. See OVPMS-2447</li>
     */
    private void onDocumentChanged() {
        getProperty(PROTECTED_DOCUMENT).setValue(true);
        if (InvestigationActStatus.SENT.equals(getOrderStatus())) {
            getProperty(ORDER_STATUS).setValue(InvestigationActStatus.RECEIVED);
        }
    }

    /**
     * Ensures that the investigation is editable.
     */
    private void checkEditable() {
        if (!canEdit()) {
            throw new IllegalStateException("Cannot change investigation with status " + getObject().getStatus2());
        }
    }

    /**
     * Invoked when the tests change.
     */
    private void onTestChanged() {
        setValidateOrder();
        Entity laboratory = getLaboratory();
        if (laboratory != null) {
            List<Entity> tests = getTests();
            showDevice = showDevice(tests);
            if (!tests.isEmpty()) {
                boolean confirm = needsOrderConfirmation(laboratory, tests);
                Property confirmOrder = getProperty(CONFIRM_ORDER);
                if (confirm) {
                    confirmOrder.setValue(true);
                } else {
                    confirmOrder.setValue(null);
                }
            }
            if (showDevice) {
                if (getDeviceRef() == null) {
                    setDefaultDevice();
                }
            } else {
                setDevice(null);
            }
        }
        // tests aren't editable via the UI so need to layout again to refresh the test collection and display any
        // devices
        onLayout();
    }

    /**
     * Sets a default device, if one is present.
     */
    private void setDefaultDevice() {
        Entity investigationType = getInvestigationType();
        if (investigationType != null) {
            Reference locationRef = getLocationRef();
            IMObjectBean bean = getBean(investigationType);
            List<Entity> devices = bean.getTargets("devices", Entity.class, Policies.active());
            Entity match = null;
            for (Entity device : devices) {
                IMObjectBean deviceBean = getBean(device);
                List<Reference> locations = deviceBean.getTargetRefs("locations");
                if (locations.isEmpty() || locations.contains(locationRef)) {
                    if (match == null) {
                        match = device;
                    } else {
                        // multiple devices match, so don't set a default
                        match = null;
                        break;
                    }
                }
            }
            setDevice(match);
        }
    }

    /**
     * Determines if the device field should be displayed to select a device to perform tests on.
     *
     * @param tests the tests
     * @return {@code true} if the device should be displayed
     */
    private boolean showDevice(List<Entity> tests) {
        boolean result = false;
        for (Entity test : tests) {
            IMObjectBean bean = getBean(test);
            UseDevice use = getUseDevice(bean);
            if (use == UseDevice.YES || use == UseDevice.OPTIONAL) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Determines if tests can be grouped.
     *
     * @param tests the tests
     * @return {@code true} if the tests can be grouped, otherwise {@code false}
     */
    private boolean canGroup(List<Entity> tests) {
        for (Entity test : tests) {
            IMObjectBean bean = getBean(test);
            if (!bean.getBoolean(GROUP)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns a laboratory service given its configuration.
     *
     * @param laboratory the laboratory configuration
     * @return the laboratory service
     */
    private LaboratoryService getLaboratoryService(Entity laboratory) {
        return getLaboratoryServices().getService(laboratory);
    }

    /**
     * Determines if the investigation needs order confirmation.
     *
     * @param laboratory the laboratory
     * @return {@code true} if the investigation needs order confirmation
     */
    private boolean needsOrderConfirmation(Entity laboratory, List<Entity> tests) {
        boolean result = false;
        if (!tests.isEmpty() && !isHL7(tests)) {
            try {
                LaboratoryService service = getLaboratoryService(laboratory);
                if (service != null) {
                    // will only ever be null when testing with a mocked LaboratoryServices
                    for (Entity entity : tests) {
                        Test test = domainService.create(entity, Test.class);
                        if (service.confirmOrders(test)) {
                            result = true;
                            break;
                        }
                    }
                }
            } catch (Throwable exception) {
                // TODO - if the service is unavailable, but the order requires confirmation, the flag won't be set.
                // see OVPMS-2871
                ErrorHelper.show(exception);
            }
        }
        return result;
    }

    /**
     * Determines if one or mores tests are HL7 tests.
     *
     * @param tests the tests
     * @return {@code true} if at least one of the tests is an HL7 test
     */
    private boolean isHL7(List<Entity> tests) {
        for (Entity test : tests) {
            if (test.isA(LaboratoryArchetypes.HL7_TEST)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Invoked when the investigation type changes.
     * <p/>
     * This clears the products.
     */
    private void onInvestigationTypeChanged() {
        Entity investigationType = getInvestigationType();
        this.tests = null;
        setValidateOrder();
        CollectionProperty tests = getCollectionProperty(TESTS);
        boolean changed = false;
        for (Object value : tests.getValues()) {
            tests.remove(value);
            changed = true;
        }
        CollectionProperty products = getCollectionProperty(PRODUCTS);
        for (Object value : products.getValues()) {
            products.remove(value);
            changed = true;
        }
        boolean layout = updateLaboratory(investigationType);

        if (investigationType != null) {
            // copy the description from the investigation type, unless it is linked to a laboratory.
            // The latter tend to be generic, so exclude them
            String description = getLaboratory() == null ? investigationType.getDescription() : null;
            getProperty(DESCRIPTION).setValue(description);
        }

        if (layout || changed) {
            onLayout();  // force layout to occur, to refresh the tests, products and order status
        }
    }

    /**
     * Flag that the laboratory order needs to be validated.
     */
    private void setValidateOrder() {
        validateOrder = true;
    }

    /**
     * Validates tests.
     *
     * @param validator the validator
     * @return {@code true} if the tests are valid otherwise {@code false}
     */
    private boolean validateTests(Validator validator) {
        if (canEdit()) {
            // only check tests if the investigation can still be submitted. Once submitted, legacy data can change
            List<Entity> tests = getTests();
            Entity laboratory = getLaboratory();
            Party location = getLocation();
            Entity device = getDevice();
            if (!tests.isEmpty()) {
                Entity investigationType = getInvestigationType();
                if (investigationType == null) {
                    // this shouldn't occur, as prior validation will prevent it
                    validator.add(this, Messages.format("property.error.required",
                                                        getDescriptor(INVESTIGATION_TYPE).getDisplayName()));
                } else if (location == null) {
                    // location is required for tests
                    validator.add(this, Messages.format("property.error.required",
                                                        getDescriptor(LOCATION).getDisplayName()));
                } else {
                    Entity typeLaboratory = rules.getLaboratory(investigationType);
                    Entity locationLaboratory = getLaboratory(investigationType, location);
                    if (TypeHelper.isA(typeLaboratory, LaboratoryArchetypes.LABORATORY_SERVICES) &&
                        locationLaboratory == null) {
                        // there is a laboratory service associated with the investigation type, and there is
                        // configuration for at least one practice locations, but not that specified.
                        // NOTE: HL7 laboratories aren't subject to this check for historical reasons
                        String message = Messages.format("investigation.test.noLaboratoryForLocation",
                                                         investigationType.getName(), typeLaboratory.getName(),
                                                         location.getName());
                        validator.add(this, new ValidatorError(message));
                    } else if (!Objects.equals(locationLaboratory, laboratory)) {
                        String typeLabName = (locationLaboratory != null) ? locationLaboratory.getName()
                                                                          : Messages.get("imobject.none");
                        String labName = (laboratory != null) ? laboratory.getName()
                                                              : Messages.get("imobject.none");
                        String message = Messages.format("investigation.test.laboratoryInvestigationTypeMismatch",
                                                         typeLabName, investigationType.getName(), labName);
                        validator.add(this, new ValidatorError(message));
                    } else if (validateDevice(validator, device, laboratory, location)) {
                        validateTests(validator, tests, investigationType, laboratory, location, device);
                    }
                }
            } else if (laboratory != null) {
                String message = Messages.format("investigation.test.testRequiredForLab", laboratory.getName());
                validator.add(this, new ValidatorError(message));
            } else if (device != null) {
                String message = Messages.format("investigation.test.testRequiredForDevice");
                validator.add(this, new ValidatorError(message));
            }
        }
        return validator.isValid();
    }

    /**
     * Verifies the order for an investigation, if it can be submitted to  {@link LaboratoryService}.
     * <p/>
     * This can be an expensive operation, so validation only occurs when the {@code validateOrder} flag is
     * {@code true}.
     * <p/>
     * NOTE: if a {@link LaboratoryService} reports an error for an investigation, this doesn't generate a validation
     * error, as to do so would prevent the investigation being saved. It raises an alert instead.<br/>
     * This is so users can exit editing and correct issues (such as a patient without a date of birth), without having
     * to re-create the investigation.
     *
     * @param validator the validator
     * @return {@code true} if  the investigation is valid, otherwise {@code false}
     */
    private boolean validateOrder(Validator validator) {
        if (validateOrder && canEdit()) {
            // only validate the order if it can still be submitted. Once submitted, legacy data can change
            AlertListener listener = getAlertListener();
            if (listener != null && labAlert != null) {
                listener.cancel(labAlert);
            }
            ValidationStatus status = getValidationService().validate(getObject());
            if (status.getStatus() == OrderValidationStatus.Status.ERROR) {
                if (listener != null) {
                    String message = Messages.format("investigation.validation.error", status.getName(),
                                                     status.getMessage());
                    labAlert = listener.onAlert(message, AlertListener.Category.ERROR);
                }
            } else if (status.getStatus() == OrderValidationStatus.Status.WARNING) {
                if (listener != null) {
                    String message = Messages.format("investigation.validation.warning", status.getName(),
                                                     status.getMessage());
                    labAlert = listener.onAlert(message);
                }
            } else {
                validateOrder = false;
            }
        }
        return validator.isValid();
    }

    /**
     * Validates a device.
     *
     * @param validator  the validator
     * @param device     the device. May be {@code null}
     * @param laboratory the laboratory. May be {@code null}
     * @param location   the practice location
     * @return {@code true} if the device is valid
     */
    private boolean validateDevice(Validator validator, Entity device, Entity laboratory, Party location) {
        if (device != null) {
            IMObjectBean deviceBean = getBean(device);
            if (laboratory == null) {
                validator.add(this, Messages.format("investigation.test.laboratoryRequired", device.getName()));
            } else if (!Objects.equals(laboratory.getObjectReference(), deviceBean.getTargetRef(LABORATORY))) {
                validator.add(this, Messages.format("investigation.test.laboratoryDeviceMismatch",
                                                    device.getName(), laboratory.getName()));
            } else if (!rules.canUseDeviceAtLocation(device, location)) {
                validator.add(this, Messages.format("investigation.test.notAtLocation", device.getName(),
                                                    location.getName()));
            }
        }
        return validator.isValid();
    }

    /**
     * Validates tests.
     *
     * @param validator         the validator
     * @param tests             the tests
     * @param investigationType the investigation type
     * @param laboratory        the laboratory. May be {@code null}
     * @param location          the practice location
     * @param device            the device. May be {@code null}
     */
    private void validateTests(Validator validator, List<Entity> tests, Entity investigationType, Entity laboratory,
                               Party location, Entity device) {
        Reference investigationTypeRef = investigationType.getObjectReference();
        for (Entity test : tests) {
            if (!test.isActive()) {
                validator.add(this, Messages.format("investigation.test.invactive", test.getName()));
                break;
            } else {
                IMObjectBean bean = getBean(test);
                if (!Objects.equals(investigationTypeRef, bean.getTargetRef(INVESTIGATION_TYPE))) {
                    validator.add(this, Messages.format("investigation.test.unsupported", test.getName(),
                                                        investigationType.getName()));
                    break;
                }
                UseDevice use = getUseDevice(bean);
                if (use == UseDevice.NO && device != null) {
                    validator.add(this, Messages.format("investigation.test.unsupportedDevice", test.getName()));
                    break;
                } else if (use == UseDevice.YES && device == null) {
                    validator.add(this, Messages.format("investigation.test.deviceRequired", test.getName()));
                    break;
                } else if (tests.size() > 1 && !bean.getBoolean("group")) {
                    validator.add(this, Messages.format("investigation.test.noGroup", test.getName()));
                } else if (laboratory != null && !rules.canUseLaboratoryAtLocation(laboratory, location)) {
                    validator.add(this, Messages.format("investigation.test.notAtLocation", test.getName(),
                                                        location.getName()));
                    break;
                }
            }
        }
    }

    /**
     * Determines if the user has permission to edit the order status.
     *
     * @param investigationType the investigation type. May be {@code null}
     * @param laboratory        the laboratory. May be {@code null}
     * @return {@code true} if the order status permission changed, otherwise {@code false}
     */
    private boolean updateOrderStatusPermission(Entity investigationType, Entity laboratory) {
        OrderStatusPermission oldPermission = orderStatusPermission;
        if (investigationType != null && laboratory == null) {
            // can edit the status if the investigation isn't ordered via a laboratory
            orderStatusPermission = OrderStatusPermission.EDITABLE;
        } else {
            orderStatusPermission = OrderStatusPermission.READ_ONLY;
        }
        return orderStatusPermission != oldPermission;
    }

    /**
     * Updates the laboratory when the investigation type changes.
     *
     * @param investigationType the investigation type
     * @return {@code true} if the layout needs to be changed, otherwise {@code false}
     */
    private boolean updateLaboratory(Entity investigationType) {
        boolean result = false;
        Entity laboratory = null;
        if (investigationType != null) {
            laboratory = getLaboratory(investigationType);
        }
        boolean oldShowDevice = showDevice;
        setValidateOrder();

        setParticipant(LABORATORY, laboratory);
        setParticipant(DEVICE, (Reference) null);

        if (updateOrderStatusPermission(investigationType, laboratory) || oldShowDevice != showDevice) {
            // need to change layout
            result = true;
        }
        return result;
    }

    /**
     * Delete documents associated with results.
     * <p/>
     * This is because the parent-child relationships don't cascade to documents.
     */
    private void deleteChildDocuments() {
        IMObjectBean bean = getBean(getObject());
        for (Act results : bean.getTargets("results", Act.class)) {
            IMObjectBean resultsBean = getBean(results);
            deleteDocument(resultsBean, "longNotes");
            for (Act result : resultsBean.getTargets("items", Act.class)) {
                deleteResultDocuments(result);
            }
        }
    }

    /**
     * Deletes documents associated with a result.
     *
     * @param result the result
     */
    private void deleteResultDocuments(Act result) {
        IMObjectBean bean = getBean(result);
        deleteDocument(bean, "longResult");
        deleteDocument(bean, "longNotes");
        deleteDocument(bean, "image");
    }

    /**
     * Deletes a document act and document associated with a relationship node.
     *
     * @param bean the bean
     * @param node the relationship node name
     */
    private void deleteDocument(IMObjectBean bean, String node) {
        DocumentAct act = bean.getTarget(node, DocumentAct.class);
        if (act != null) {
            ArchetypeService service = getService();
            Reference document = act.getDocument();
            if (document != null) {
                act.setDocument(null);
                service.save(act);
                service.remove(document);
            }
        }
    }

    /**
     * Returns the laboratory associated with an investigation type, at the practice location.
     *
     * @param investigationType the investigation type
     * @return the laboratory, or {@code null} if there is none
     */
    private Entity getLaboratory(Entity investigationType) {
        return getLaboratory(investigationType, getLocation());
    }

    /**
     * Returns the laboratory associated with an investigation type, at the specified practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location. May be {@code null}
     * @return the laboratory, or {@code null} if there is none
     */
    private Entity getLaboratory(Entity investigationType, Party location) {
        return (location != null) ? rules.getLaboratory(investigationType, location) : null;
    }

    /**
     * Determines if the Print Form button should be displayed.
     * <p/>
     * Note that getObject().isNew() returns true until the transaction commits
     *
     * @return {@code true} if it should be displayed, otherwise {@code false}
     */
    private boolean enablePrintForm() {
        return enableButton || !getObject().isNew();
    }

    /**
     * Creates the device editor.
     *
     * @return a new device editor
     */
    private ParticipationCollectionEditor createDeviceCollectionEditor() {
        CollectionProperty property = getCollectionProperty(DEVICE);
        return new ParticipationCollectionEditor(property, getObject(), getLayoutContext()) {
            @Override
            protected IMObjectEditor createEditor(IMObject object, LayoutContext context) {
                return new ParticipationEditor<Entity>((Participation) object, (Act) getObject(), context) {

                    @Override
                    protected IMObjectReferenceEditor<Entity> createEntityEditor(Property property) {
                        return new DeviceReferenceEditor(property, context);
                    }
                };
            }
        };
    }

    /**
     * Determines if a test uses a device.
     *
     * @param test the test
     * @return the {@link UseDevice} configuration for the test
     */
    private UseDevice getUseDevice(IMObjectBean test) {
        String useDevice = test.hasNode("useDevice") ? test.getString("useDevice") : null;
        if (useDevice == null) {
            return UseDevice.NO;
        }
        return UseDevice.valueOf(useDevice);
    }

    /**
     * Returns a component to edit the order status.
     * <p/>
     * For investigations associated with a laboratory, the laboratory manages the status.
     *
     * @return the order status component, or {@code null} if it isn't editable
     */
    private PropertyComponentEditor createOrderStatus() {
        PropertyComponentEditor result = null;
        Property property = getProperty(PatientInvestigationActLayoutStrategy.ORDER_STATUS);
        if (orderStatusPermission != OrderStatusPermission.READ_ONLY) {
            LookupQuery query = new NodeLookupQuery(getObject(), property);
            // can't select CONFIRM, CONFIRM_DEFERRED or ERROR, as these are solely used by laboratory services
            query = new LookupFilter(query, false, InvestigationActStatus.CONFIRM,
                                     InvestigationActStatus.CONFIRM_DEFERRED, InvestigationActStatus.ERROR);
            LookupField field = LookupFieldFactory.create(property, query);
            result = new PropertyComponentEditor(property, field);
        }
        return result;
    }

    private class DeviceReferenceEditor extends AbstractIMObjectReferenceEditor<Entity> {
        DeviceReferenceEditor(Property property, LayoutContext context) {
            super(property, getParent(), context);
        }

        protected Query<Entity> createQuery(String name) {
            DeviceQuery query = new DeviceQuery();
            query.setValue(name);
            return query;
        }

        /**
         * Determines if a reference is valid.
         *
         * @param reference the reference to check
         * @return {@code true}
         */
        @Override
        protected boolean isValidReference(Reference reference) {
            return true;
        }

        private class DeviceQuery extends AbstractEntityQuery<Entity> {

            /**
             * Constructs a {DeviceQuery}.
             */
            DeviceQuery() {
                super(new String[]{LaboratoryArchetypes.DEVICES});
                setAuto(true);
            }

            /**
             * Creates the result set.
             *
             * @param sort the sort criteria. May be {@code null}
             * @return a new result set
             */
            @Override
            protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
                List<Entity> matches = new ArrayList<>();
                Entity investigationType = getInvestigationType();
                if (investigationType != null) {
                    Policy<Relationship> policy;
                    BaseArchetypeConstraint.State active = getActive();
                    if (active == BaseArchetypeConstraint.State.ACTIVE) {
                        policy = Policies.active();
                    } else if (active == BaseArchetypeConstraint.State.INACTIVE) {
                        policy = Policies.newPolicy(Relationship.class).inactiveObjects().build();
                    } else {
                        policy = Policies.all();
                    }
                    matches = getBean(investigationType).getTargets("devices", Entity.class, policy);
                }
                Reference location = getLocationRef();
                // filter the result set to only include those devices available at the location
                return new FilteredResultSet<Entity>(new ListResultSet<>(matches, getMaxResults())) {
                    @Override
                    protected void filter(Entity object, List<Entity> results) {
                        if (location != null) {
                            IMObjectBean bean = getBean(object);
                            List<Reference> locations = bean.getTargetRefs("locations");
                            if (locations.isEmpty() || locations.contains(location)) {
                                results.add(object);
                            }
                        }
                    }
                };
            }
        }
    }

    private enum OrderStatusPermission {
        EDITABLE,   // the order status can be changed, with the exception of the Confirm states,
        // which are laboratory-service only. This only applies when the investigation type is not
        // associated with a laboratory
        READ_ONLY,  // the order status cannot be changed, as it managed by the laboratory service
    }

}
