/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

import static org.openvpms.web.workspace.admin.laboratory.InvestigationTypeLayoutStrategy.LABORATORY;

/**
 * Editor for <em>entity.investigationType</em>.
 * <p>
 * This ensures that user created investigation types cannot be assigned provider managed laboratories.
 *
 * @author Tim Anderson
 */
public class InvestigationTypeEditor extends AbstractIMObjectEditor {

    /**
     * Determines if the investigation type is laboratory provided.
     */
    private final boolean provided;

    /**
     * Investigation type id node.
     */
    private static final String TYPE_ID = "typeId";

    /**
     * Constructs an {@link InvestigationTypeEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public InvestigationTypeEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        provided = !getCollectionProperty(TYPE_ID).isEmpty();
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new InvestigationTypeEditor(reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Returns the laboratory.
     *
     * @return the laboratory. May be {@code null}
     */
    public Entity getLaboratory() {
        return (Entity) getTarget(LABORATORY);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateLaboratory(validator);
    }

    /**
     * Ensures that user-created investigation types cannot be submitted to laboratory services by preventing
     * laboratory services being assigned to them.
     *
     * @param validator the validator
     * @return {@code true} if the investigation type is valid
     */
    private boolean validateLaboratory(Validator validator) {
        if (!provided) {
            Entity laboratory = getLaboratory();
            if (laboratory != null && laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES)) {
                validator.add(this, Messages.format("investigationType.invalidLaboratory", laboratory.getName()));
            }
        }
        return validator.isValid();
    }
}
