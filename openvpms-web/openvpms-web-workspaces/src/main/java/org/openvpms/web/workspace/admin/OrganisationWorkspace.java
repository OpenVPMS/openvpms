/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin;

import org.openvpms.archetype.rules.finance.till.TillArchetypes;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.admin.organisation.OrganisationCRUDWindow;


/**
 * Organisation workspace.
 *
 * @author Tim Anderson
 */
public class OrganisationWorkspace extends ResultSetCRUDWorkspace<Entity> {

    /**
     * Constructs an {@code OrganisationWorkspace}.
     *
     * @param context the context
     */
    public OrganisationWorkspace(Context context) {
        super("admin.organisation", context);
        String[] archetypes = {"party.organisation*", "entity.organisation*", PracticeArchetypes.DEPARTMENT,
                               "entity.rosterArea", SMSArchetypes.SMS_PROVIDERS, SMSArchetypes.EMAIL_CONFIGURATIONS,
                               ProductArchetypes.SERVICE_RATIO_CALENDAR, "entity.mailServer",
                               InsuranceArchetypes.INSURANCE_SERVICES, "entity.printerService*",
                               "entity.plugin*", "entity.EFTPOS*"};
        String[] exclude = {"entity.pluginConfiguration"};
        String displayName = Messages.get(getId() + ".type");
        setArchetypes(Archetypes.create(archetypes, exclude, Entity.class, null, displayName));
    }

    /**
     * Sets the current object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(Entity object) {
        super.setObject(object);
        // need to update the context in case organisations have changed.
        // May need to refine this so that the context is only updated if the
        // organisation is a newer version of that currently in the context
        // (i,e don't change for different organisations).
        Context context = getContext();
        if (TypeHelper.isA(object, ScheduleArchetypes.ORGANISATION_SCHEDULE)) {
            context.setSchedule(object);
        } else if (TypeHelper.isA(object, ScheduleArchetypes.ORGANISATION_WORKLIST)) {
            context.setWorkList(object);
        } else if (TypeHelper.isA(object, TillArchetypes.TILL)) {
            context.setTill(object);
        }
    }

    /**
     * Determines if the workspace can be updated with instances of the specified archetype.
     * <p/>
     * This implementation always returns {@code false}, largely to avoid receiving updates when changing practice
     * locations.
     *
     * @param shortName the archetype's short name
     * @return {@code false}
     */
    @Override
    public boolean canUpdate(String shortName) {
        return false;
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery() {
        Query<Entity> query = super.createQuery();
        query.setContains(true);
        return query;
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<Entity> createCRUDWindow() {
        QueryBrowser<Entity> browser = getBrowser();
        return new OrganisationCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(),
                                          getContext(), getHelpContext());
    }
}
