/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.openoffice.OpenOfficeService;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.openoffice.OpenOfficeStatus;

/**
 * OpenOffice diagnostics viewer.
 *
 * @author Tim Anderson
 */
public class OpenOfficeViewer extends AbstractDiagnosticTab {

    /**
     * The OpenOffice status.
     */
    private final OpenOfficeStatus status;

    /**
     * The data snapshot.
     */
    private String[][] snapshot;

    /**
     * The table columns.
     */
    private static final String[] COLUMNS = new String[]{"Name", "Value"};

    /**
     * Constructs an {@link OpenOfficeViewer}.
     */
    OpenOfficeViewer() {
        super("admin.system.openoffice.title");
        OpenOfficeService officeService = ServiceHelper.getBean(OpenOfficeService.class);
        ArchetypeService service = ServiceHelper.getArchetypeService();
        status = new OpenOfficeStatus(officeService, service);
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String[][] data = getData();
        if (data != null) {
            result = toCSV("openoffice.csv", COLUMNS, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component component = null;
        try {
            status.refresh();
            component = status.getComponent();
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        return component;
    }

    /**
     * Returns the data.
     *
     * @return the data, or {@code null} if it cannot be obtained
     */
    private String[][] getData() {
        if (snapshot == null) {
            try {
                snapshot = new String[4][];
                snapshot[0] = getProperty(status.getHome());
                snapshot[1] = getProperty(status.getPorts());
                snapshot[2] = getProperty(status.getMaxTasksPerProcess());
                snapshot[3] = getProperty(status.getRunning());
            } catch (Throwable exception) {
                ErrorHelper.show(exception);
            }
        }
        return snapshot;
    }

    /**
     * Helper to return a property as a pair of strings.
     *
     * @param property the property
     * @return an array containing the display name and property value
     */
    private String[] getProperty(Property property) {
        return new String[]{property.getDisplayName(), property.getString()};
    }
}
