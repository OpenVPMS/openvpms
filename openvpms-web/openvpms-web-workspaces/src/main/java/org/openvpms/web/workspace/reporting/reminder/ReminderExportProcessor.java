/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.ContactMatcher;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderExporter;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.echo.servlet.DownloadServlet;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;

import java.util.Date;
import java.util.List;


/**
 * Processor that exports reminders using the {@link ReminderExporter}.
 *
 * @author Tim Anderson
 */
public class ReminderExportProcessor extends PatientReminderProcessor<PatientReminders> {

    /**
     * The practice location.
     */
    private final Party location;

    /**
     * Constructs a {@link ReminderExportProcessor}.
     *
     * @param reminderTypes the reminder types
     * @param rules         the reminder rules
     * @param patientRules  the patient rules
     * @param location      the current practice location
     * @param practice      the practice
     * @param service       the archetype service
     * @param config        the reminder configuration
     * @param logger        the communication logger. May be {@code null}
     * @param actionFactory the action factory
     */
    public ReminderExportProcessor(ReminderTypes reminderTypes, ReminderRules rules, PatientRules patientRules,
                                   Party location, Party practice, IArchetypeService service,
                                   ReminderConfiguration config, CommunicationLogger logger,
                                   ActionFactory actionFactory) {
        super(reminderTypes, rules, patientRules, practice, service, config, logger, actionFactory);
        this.location = location;
    }

    /**
     * Returns the reminder item archetype that this processes.
     *
     * @return the archetype
     */
    @Override
    public String getArchetype() {
        return ReminderArchetypes.EXPORT_REMINDER;
    }

    /**
     * Processes reminders.
     *
     * @param reminders the reminders
     */
    @Override
    public void process(PatientReminders reminders) {
        export(reminders.getReminders());
    }

    /**
     * Determines if reminder processing is performed asynchronously.
     *
     * @return {@code true} if reminder processing is performed asynchronously
     */
    @Override
    public boolean isAsynchronous() {
        return false;
    }

    /**
     * Exports reminders.
     *
     * @param reminders the reminders to export
     */
    protected void export(List<ReminderEvent> reminders) {
        ReminderExporter exporter = ServiceHelper.getBean(ReminderExporter.class);
        Document document = exporter.export(reminders, new Date());
        DownloadServlet.startDownload(document);
    }

    /**
     * Creates a {@link PatientReminders}.
     *
     * @param groupBy the reminder grouping policy. This determines which document template is selected
     * @param resend  if {@code true}, reminders are being resent
     * @return a new {@link PatientReminders}
     */
    @Override
    protected PatientReminders createPatientReminders(ReminderType.GroupBy groupBy, boolean resend) {
        return new PatientReminders(groupBy, resend);
    }

    /**
     * Prepares reminders for processing.
     * <p>
     * This:
     * <ul>
     * <li>filters out any reminders that can't be processed due to missing data</li>
     * <li>adds meta-data for subsequent calls to {@link #process}</li>
     * </ul>
     *
     * @param reminders the reminders to prepare
     */
    @Override
    protected void prepare(PatientReminders reminders) {
        for (ReminderEvent reminder : reminders.getReminders()) {
            Party customer = reminder.getCustomer();
            ContactMatcher matcher = createContactMatcher(ContactArchetypes.LOCATION);
            Contact contact = getContact(customer, matcher, reminder.getContact());
            if (contact != null) {
                populate(reminder, contact, getLocation(customer));
            } else {
                String message = Messages.format("reporting.reminder.nocontact", DescriptorHelper.getDisplayName(
                        ContactArchetypes.LOCATION, getService()));
                error(reminder, message, reminders);
            }
        }
    }

    /**
     * Logs reminder communications.
     *
     * @param state  the reminder state
     * @param logger the communication logger
     */
    @Override
    protected void log(PatientReminders state, CommunicationLogger logger) {
        List<ReminderEvent> reminders = state.getReminders();
        String subject = Messages.get("reminder.log.export.subject");
        for (ReminderEvent reminder : reminders) {
            Contact contact = reminder.getContact();
            String notes = getNote(reminder);
            logger.logMail(reminder.getCustomer(), reminder.getPatient(), contact.getDescription(),
                           subject, COMMUNICATION_REASON, null, notes, location);
        }
    }
}
