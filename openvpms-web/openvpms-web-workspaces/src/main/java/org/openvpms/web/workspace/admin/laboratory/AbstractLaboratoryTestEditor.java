/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityLinkCollectionEditor;
import org.openvpms.web.component.im.relationship.EntityLinkEditor;
import org.openvpms.web.component.property.Validator;

/**
 * Editor for <em>entity.laboratoryTest</em> and <em></em>.
 * <p/>
 * This prevents investigation types managed by laboratories from being assigned to manually constructed tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractLaboratoryTestEditor extends AbstractIMObjectEditor {

    /**
     * Investigation type node.
     */
    private static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Constructs an {@link AbstractLaboratoryTestEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AbstractLaboratoryTestEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type. May be {@code null}
     */
    public Entity getInvestigationType() {
        return (Entity) getTarget(INVESTIGATION_TYPE);
    }

    /**
     * Sets the investigation type.
     *
     * @param investigationType the investigation type
     */
    public void setInvestigationType(Entity investigationType) {
        EntityLinkCollectionEditor collectionEditor = (EntityLinkCollectionEditor) getEditor(INVESTIGATION_TYPE);
        EntityLinkEditor editor = (EntityLinkEditor) collectionEditor.getFirstEditor(true);
        editor.setTarget(investigationType);
    }

    /**
     * Ensures that the investigation type is valid for the test.
     *
     * @param validator the validator
     * @return {@code true} if the investigation type is valid
     */
    protected abstract boolean validateInvestigationType(Validator validator);

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateInvestigationType(validator);
    }
}