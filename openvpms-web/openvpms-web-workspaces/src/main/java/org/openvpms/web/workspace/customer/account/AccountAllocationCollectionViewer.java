/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.RelationshipHelper;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.IMObjectTableCollectionViewer;
import org.openvpms.web.component.im.view.IMObjectViewer;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.system.ServiceHelper;

/**
 * Viewer for customer account allocations.
 *
 * @author Tim Anderson
 */
public class AccountAllocationCollectionViewer extends IMObjectTableCollectionViewer {

    /**
     * Constructs an {@link AccountAllocationCollectionViewer}
     *
     * @param property the collection to view
     * @param parent   the parent object
     * @param context  the layout context. May be {@code null}
     */
    public AccountAllocationCollectionViewer(CollectionProperty property, FinancialAct parent, LayoutContext context) {
        super(property, parent, context);
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        return new AccountAllocationRelationshipTableModel((FinancialAct) getObject());
    }

    /**
     * Creates a viewer for an object.
     *
     * @param object  the object
     * @param context the layout context
     * @return a new viewer
     */
    @Override
    protected IMObjectViewer createViewer(IMObject object, LayoutContext context) {
        IMObjectViewer viewer = super.createViewer(object, context);
        if (object instanceof Relationship) {
            Reference related = RelationshipHelper.getRelated(getObject(), (Relationship) object);
            if (related != null) {
                // use the target archetype rather than the relationship for the display name
                viewer.setTitle(DescriptorHelper.getDisplayName(related.getArchetype(),
                                                                ServiceHelper.getArchetypeService()));
            }
        }
        return viewer;
    }
}