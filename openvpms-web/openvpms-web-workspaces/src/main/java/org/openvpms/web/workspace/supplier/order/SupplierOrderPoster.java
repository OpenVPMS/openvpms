/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.order;

import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.supplier.OrderRules;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.action.ActionStatus;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.workspace.ActPoster;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.supplier.SupplierHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

/**
 * An {@link ActPoster} for supplier orders.
 * <p/>
 * This prevents non-clinicians from posting orders or if the order has restricted products and ordering
 * is restricted as per {@link PracticeRules#isOrderingRestricted(Party)}.
 *
 * @author Tim Anderson
 */
public class SupplierOrderPoster extends ActPoster<FinancialAct> {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SupplierOrderPoster.class);

    /**
     * Constructs a {@link SupplierOrderPoster}.
     *
     * @param act     the act to post
     * @param actions the actions that determine if the act can be posted or not
     * @param help    the help context
     */
    public SupplierOrderPoster(FinancialAct act, OrderActions actions, Context context, HelpContext help) {
        super(act, actions, help);
        this.context = context;
    }

    /**
     * Confirms that the user wants to post the act, posting it if accepted.
     *
     * @param act      the act to post
     * @param help     the help context
     * @param listener the listener to notify on completion
     */
    @Override
    protected void confirmPost(FinancialAct act, HelpContext help, Consumer<ActionStatus> listener) {
        PracticeRules practiceRules = ServiceHelper.getBean(PracticeRules.class);
        OrderRules orderRules = SupplierHelper.createOrderRules(context.getPractice());

        boolean restricted = false;
        if (practiceRules.isOrderingRestricted(context.getPractice())) {
            UserRules userRules = ServiceHelper.getBean(UserRules.class);
            User user = context.getUser();
            if (!userRules.isClinician(user) && orderRules.hasRestrictedProducts(act)) {
                log.warn("A user has attempted to finalise a restricted order. User={}, order={}", user.getUsername(),
                         act.getId());
                restricted = true;
            }
        }
        if (restricted) {
            InformationDialog.newDialog()
                    .titleKey("supplier.order.restricted.title")
                    .messageKey("supplier.order.restricted.message")
                    .ok(() -> listener.accept(ActionStatus.success()))
                    .show();
        } else {
            super.confirmPost(act, help, listener);
        }
    }
}
