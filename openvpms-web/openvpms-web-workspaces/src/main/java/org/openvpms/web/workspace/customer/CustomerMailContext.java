/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import org.apache.commons.collections4.ComparatorUtils;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextMailContext;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.contact.ContactHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.mail.AddressFormatter;
import org.openvpms.web.component.mail.AttachmentBrowser;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.ToAddressFormatter;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.document.CustomerPatientDocumentBrowser;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_VET;
import static org.openvpms.archetype.rules.supplier.SupplierArchetypes.SUPPLIER_VET_PRACTICE;
import static org.openvpms.component.model.bean.Policies.active;


/**
 * An {@link MailContext} that uses an {@link Context} to returns 'from' addresses from the practice location or
 * practice, and 'to' addresses from the current customer and the current patient's referrals.
 *
 * @author Tim Anderson
 */
public class CustomerMailContext extends ContextMailContext {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * If {@code true}, use a default to-address.
     */
    private final boolean useDefaultToAddress;

    /**
     * The to-address state. This is cached, as determining referral addresses can be expensive.
     * It is created on demand, and recreated if the underlying context changes.
     */
    private AddressState toAddressState;

    /**
     * Constructs a {@link CustomerMailContext}.
     *
     * @param context the context
     */
    public CustomerMailContext(Context context) {
        this(context, false);
    }

    /**
     * Constructs a {@link CustomerMailContext}.
     *
     * @param context             the context
     * @param useDefaultToAddress if {@code true}, use a default to-address
     */
    public CustomerMailContext(Context context, boolean useDefaultToAddress) {
        super(context);
        this.useDefaultToAddress = useDefaultToAddress;
        this.service = ServiceHelper.getArchetypeService();
    }

    /**
     * Constructs a {@link CustomerMailContext} that registers an attachment browser.
     *
     * @param context the context
     * @param help    the help context
     */
    public CustomerMailContext(Context context, HelpContext help) {
        this(context, false, help);
    }

    /**
     * Constructs a {@link CustomerMailContext} that registers an attachment browser.
     *
     * @param context             the context
     * @param useDefaultToAddress if {@code true}, use a default to-address
     * @param help                the help context
     */
    public CustomerMailContext(Context context, boolean useDefaultToAddress, HelpContext help) {
        this(context, useDefaultToAddress);
        DefaultLayoutContext layout = new DefaultLayoutContext(context, help);

        setAttachmentBrowserFactory(mailContext -> {
            AttachmentBrowser result = null;
            Party customer = getCustomer();
            Party patient = getPatient();
            if (customer != null || patient != null) {
                result = new CustomerPatientDocumentBrowser(customer, patient, layout);
            }
            return result;
        });
    }

    /**
     * Returns the customer.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        return getContext().getCustomer();
    }

    /**
     * Returns the patient.
     *
     * @return the patient. May be {@code null}
     */
    public Party getPatient() {
        return getContext().getPatient();
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location. May be {@code null}
     */
    public Party getLocation() {
        return getContext().getLocation();
    }

    /**
     * Returns the available 'to' email addresses.
     *
     * @return the 'to' email addresses
     */
    @Override
    public Addresses getToAddresses() {
        return getToAddressState().getAddresses();
    }

    /**
     * Returns a formatter to format 'to' addresses.
     *
     * @return the 'to' address formatter
     */
    @Override
    public AddressFormatter getToAddressFormatter() {
        return new ReferringAddressFormatter();
    }

    /**
     * Creates a new mail context if the specified act has a customer or patient participation.
     * <p/>
     * NOTE: this copies rather than inherits the specified context.
     *
     * @param act     the act
     * @param context the context source the practice and location
     * @param help    the help context
     * @return a new mail context, or {@code null} if the act has no customer no patient participation
     */
    public static CustomerMailContext create(Act act, Context context, HelpContext help) {
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(act);
        Party customer = getParty(bean, "customer", context);
        Party patient = getParty(bean, "patient", context);
        return create(customer, patient, context, help);
    }

    /**
     * Creates a new mail context if either the customer or patient is non-null.
     * <p/>
     * NOTE: this copies rather than inherits the specified context.
     *
     * @param customer the customer. May be {@code null}
     * @param patient  the patient. May be {@code null}
     * @param context  the context to source the practice and location from
     * @param help     the help context
     * @return a new mail context, or {@code null} if both customer and patient are null
     */
    public static CustomerMailContext create(Party customer, Party patient, Context context, HelpContext help) {
        CustomerMailContext result = null;
        if (customer != null || patient != null) {
            Context local = LocalContext.copy(context);
            // NOTE: cannot inherit from context as it if the customer or patient are null, a possibly unrelated
            // customer/patient will be inherited from the parent
            local.setCustomer(customer);
            local.setPatient(patient);
            result = new CustomerMailContext(local, help);
        }
        return result;
    }

    /**
     * Returns the to-address state, creating it if required.
     *
     * @return the mail context state
     */
    protected AddressState getToAddressState() {
        if (toAddressState == null || toAddressesOutOfDate(toAddressState)) {
            toAddressState = createToAddressState(new AddressStateBuilder());
        }
        return toAddressState;
    }

    /**
     * Determines if the to-address state is out of date.
     * <p/>
     * The state is considered out of date if the parties used to build it don't match, or have been changed.
     *
     * @param state the state
     * @return {@code true} if the state is out of date, otherwise {@code false}
     */
    protected boolean toAddressesOutOfDate(AddressState state) {
        return !state.matches(CustomerArchetypes.PERSON, getCustomer())
               || !state.matches(PatientArchetypes.PATIENT, getPatient());
    }

    /**
     * Creates the to-address state.
     *
     * @param builder the state builder
     * @return a new state
     */
    protected AddressState createToAddressState(AddressStateBuilder builder) {
        Party customer = getCustomer();
        builder.addParty(CustomerArchetypes.PERSON, customer);

        if (customer != null) {
            List<Contact> contacts = ContactHelper.getEmailContacts(customer, service);
            builder.addContacts(contacts);
            if (builder.getPreferred() == null && useDefaultToAddress && !contacts.isEmpty()) {
                builder.setPreferred(contacts.get(0));
            }

            Party patient = getPatient();
            builder.addParty(PatientArchetypes.PATIENT, patient);
            if (patient != null) {
                builder.addContacts(getReferralAddresses(patient));
            }
        }
        return builder.build();
    }

    /**
     * Returns referral email addresses associated with a patient.
     *
     * @param patient the patient
     * @return the referral addresses
     */
    protected List<Contact> getReferralAddresses(Party patient) {
        List<Contact> result = new ArrayList<>();
        IMObjectBean bean = service.getBean(patient);
        Set<Reference> practices = new HashSet<>();
        // tracks processed practices to avoid retrieving them more than once

        for (IMObject referral : bean.getTargets("referrals", active(new Date(), false))) {
            if (referral instanceof Party) {
                if (referral.isActive()) {
                    result.addAll(ContactHelper.getEmailContacts((Party) referral, service));
                }
                if (referral.isA(SUPPLIER_VET)) {
                    // add any contacts of the practices that the vet belongs to, but only if they are active
                    IMObjectBean vet = service.getBean(referral);
                    for (Reference ref : vet.getSourceRefs("practices")) {
                        if (!practices.contains(ref)) {
                            practices.add(ref);
                            Party practice = (Party) IMObjectHelper.getObject(ref, getContext());
                            if (practice != null && practice.isActive()) {
                                result.addAll(ContactHelper.getEmailContacts(practice, service));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the party associated with a node.
     *
     * @param bean    the bean
     * @param node    the node
     * @param context the context
     * @return the associated party, or {@code null} if none exists
     */
    private static Party getParty(IMObjectBean bean, String node, Context context) {
        if (bean.hasNode(node)) {
            return (Party) IMObjectHelper.getObject(bean.getTargetRef(node), context);
        }
        return null;
    }

    /**
     * Returns the party associated with a contact.
     *
     * @param contact the contact
     * @return the party
     */
    private static Party getParty(Contact contact) {
        return ((org.openvpms.component.business.domain.im.party.Contact) contact).getParty();
    }

    /**
     * Manages email contacts.
     */
    protected static class AddressState {

        /**
         * The parties used to build the state.
         */
        private final List<PartyContacts> parties;

        /**
         * The addresses.
         */
        private final Addresses addresses;

        /**
         * Constructs a {@link AddressState}.
         *
         * @param addresses the addresses
         * @param parties   the parties used to build the list of contacts
         */
        public AddressState(Addresses addresses, List<PartyContacts> parties) {
            this.addresses = addresses;
            this.parties = parties;
        }

        /**
         * Returns the addresses.
         *
         * @return the addresses
         */
        public Addresses getAddresses() {
            return addresses;
        }

        /**
         * Determines if a party matches that used to construct the state.
         *
         * @param archetype the party archetype
         * @param party     the party. May be {@code null}
         * @return {@code true} if
         */
        public boolean matches(String archetype, Party party) {
            boolean matches = false;
            for (PartyContacts handle : parties) {
                if (handle.archetype.equals(archetype)) {
                    matches = handle.matches(party);
                    break;
                }
            }
            return matches;
        }

        /**
         * Handle to a party, used to determine if contacts need refreshing.
         */
        static class PartyContacts {

            /**
             * The party. May be {@code null}
             */
            private final Party party;

            /**
             * The party version.
             */
            private final long version;

            /**
             * The archetype.
             */
            private final String archetype;

            /**
             * Constructs {@link PartyContacts}.
             *
             * @param party     the party. May be {@code null}
             * @param archetype the party archetype, used when no party is specified
             */
            public PartyContacts(Party party, String archetype) {
                this.party = party;
                this.version = getVersion(party);
                this.archetype = archetype;
            }

            /**
             * Determines if a party matches. Parties are equal, and have the same version.
             *
             * @param other the other party. May be {@code null}
             * @return {@code true} if the parties match, otherwise {@code false}
             */
            public boolean matches(Party other) {
                return Objects.equals(party, other) && version == getVersion(other);
            }

            /**
             * Returns the version of a party.
             *
             * @param party the party. May be {@code null}
             * @return the version
             */
            private long getVersion(Party party) {
                return party != null ? party.getVersion() : -1;
            }
        }
    }

    /**
     * Builds {@link AddressState} instances.
     */
    protected static class AddressStateBuilder {

        /**
         * The parties used to build the list of contacts.
         */
        private final List<AddressState.PartyContacts> parties = new ArrayList<>();

        /**
         * The email contacts.
         */
        private final List<Contact> contacts = new ArrayList<>();

        /**
         * The preferred contact.
         */
        private Contact preferred;

        /**
         * Adds a party.
         *
         * @param archetype the party archetype, used when the party isn't present
         * @param party     the party. May be {@code null}
         */
        public void addParty(String archetype, Party party) {
            parties.add(new AddressState.PartyContacts(party, archetype));
        }

        /**
         * Adds contacts.
         *
         * @param contacts the contacts
         */
        public void addContacts(List<Contact> contacts) {
            this.contacts.addAll(contacts);
        }

        /**
         * Sets the preferred contact.
         *
         * @param preferred the preferred contact. May be {@code null}
         */
        public void setPreferred(Contact preferred) {
            this.preferred = preferred;
        }

        /**
         * Returns the preferred contact.
         *
         * @return the preferred contact. May be {@code null}
         */
        public Contact getPreferred() {
            return preferred;
        }

        /**
         * Builds the state.
         *
         * @return a new state
         */
        public AddressState build() {
            if (contacts.size() > 1) {
                sort(contacts);
            }
            return new AddressState(new Addresses(contacts, preferred), parties);
        }

        /**
         * Sorts contacts by party name.
         *
         * @param contacts the contacts to sort
         */
        private void sort(List<Contact> contacts) {
            Comparator<Object> comparator = ComparatorUtils.nullHighComparator(null);
            // sort the contacts on party name
            contacts.sort((o1, o2) -> comparator.compare(getPartyName(o1), getPartyName(o2)));
        }

        /**
         * Returns the name of party associated with a contact.
         *
         * @param contact the contact
         * @return the party name
         */
        private String getPartyName(Contact contact) {
            return getParty(contact).getName();
        }
    }

    /**
     * An address formatter that indicates that a vet/vet practice is the referring vet/vet practice.
     */
    private static class ReferringAddressFormatter extends ToAddressFormatter {

        /**
         * Returns the type of a contact.
         *
         * @param contact the contact
         * @return the type of the contact. May be {@code null}
         */
        @Override
        public String getType(Contact contact) {
            String type = super.getType(contact);
            if (type != null) {
                Party party = getParty(contact);
                if (party != null && party.isA(SUPPLIER_VET, SUPPLIER_VET_PRACTICE)) {
                    type = Messages.format("mail.type.referring", type);
                }
            }
            return type;
        }
    }

}
