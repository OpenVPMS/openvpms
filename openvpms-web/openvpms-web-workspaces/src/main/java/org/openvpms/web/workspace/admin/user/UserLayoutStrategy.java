/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.user;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.report.ReportFactory;
import org.openvpms.web.component.im.doc.DocumentActDownloader;
import org.openvpms.web.component.im.doc.FileNameFormatter;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ColourNodeLayoutStrategy;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;


/**
 * Layout strategy that hides the 'password' node.
 *
 * @author Tim Anderson
 */
public class UserLayoutStrategy extends ColourNodeLayoutStrategy {

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        // when viewing, exclude the password node
        ArchetypeNodes nodes = context.isEdit() ? DEFAULT_NODES : new ArchetypeNodes().exclude("password");
        setArchetypeNodes(nodes);
        return super.apply(object, properties, parent, context);
    }

    /**
     * Creates a set of components to be rendered from the supplied descriptors.
     *
     * @param object     the parent object
     * @param properties the properties
     * @param context    the layout context
     * @return the components
     */
    @Override
    protected ComponentSet createComponentSet(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentSet set = super.createComponentSet(object, properties, context);
        set.add(getSignature((User) object, context));
        return set;
    }

    /**
     * Returns a component to display the user's signature.
     *
     * @param user    the user
     * @param context the layout context
     * @return the user's signature
     */
    protected ComponentState getSignature(User user, LayoutContext context) {
        DocumentAct signature = ServiceHelper.getBean(UserRules.class).getSignature(user);
        Component component;
        if (signature != null && signature.getDocument() != null) {
            FileNameFormatter formatter = ServiceHelper.getBean(FileNameFormatter.class);
            ReportFactory reportFactory = ServiceHelper.getBean(ReportFactory.class);
            DocumentActDownloader downloader = new DocumentActDownloader(signature, false, true,
                                                                         context.getContext(), formatter,
                                                                         reportFactory);

            // wrap in a row to left justify
            component = RowFactory.create(downloader.getComponent());
        } else {
            // no signature available
            component = LabelFactory.create("admin.user.nosignature");
        }
        return new ComponentState(component, null, null, Messages.get("admin.user.signature"));
    }

}
