/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.StaticDocumentTemplateLocator;

import static org.openvpms.component.model.bean.Policies.active;

/**
 * Claim helper methods.
 *
 * @author Tim Anderson
 */
class ClaimHelper {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a new {@link ClaimHelper}.
     *
     * @param service the archetype service
     */
    public ClaimHelper(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Determines if an invoice item has already been claimed by another insurance claim.
     *
     * @param item    the charge item
     * @param exclude the claim to exclude. May be {@code null}
     * @return the claim that the charge item has a relationship to, if it isn't CANCELLED
     */
    public Act getClaim(Act item, Act exclude) {
        Act result = null;
        IMObjectBean chargeBean = service.getBean(item);
        for (Act claimItem : chargeBean.getSources("claims", Act.class)) {
            IMObjectBean bean = service.getBean(claimItem);
            Act claim = bean.getSource("claim", Act.class);
            if (claim != null && !claim.equals(exclude)) {
                String status = claim.getStatus();
                if (!Claim.Status.CANCELLED.isA(status)) {
                    result = claim;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns a document template locator for an object.
     * <p/>
     * If the object is a claim, then the template associated with the insurer will be used, if any.
     *
     * @param claim   the claim
     * @param object  the object
     * @param context the context
     * @return a new template locator
     */
    public DocumentTemplateLocator getDocumentTemplateLocator(Claim claim, IMObject object, Context context) {
        if (object.isA(InsuranceArchetypes.CLAIM)) {
            Party insurer = claim.getPolicy().getInsurer();
            IMObjectBean bean = service.getBean(insurer);
            Entity template = bean.getTarget("template", Entity.class, active());
            if (template != null) {
                return new StaticDocumentTemplateLocator(new DocumentTemplate(template, service));
            }
        }
        return new ContextDocumentTemplateLocator(object, context);
    }

}
