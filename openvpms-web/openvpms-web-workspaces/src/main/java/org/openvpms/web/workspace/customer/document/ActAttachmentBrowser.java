/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.im.query.BrowserAdapter;
import org.openvpms.web.component.im.query.MultiSelectBrowser;
import org.openvpms.web.component.mail.ActAttachment;
import org.openvpms.web.component.mail.AttachmentBrowser;
import org.openvpms.web.component.mail.MailAttachment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Browser for attachments generated from acts.
 *
 * @author Tim Anderson
 */
public class ActAttachmentBrowser extends BrowserAdapter<Act, MailAttachment> implements AttachmentBrowser {

    /**
     * Creates an {@link ActAttachmentBrowser}.
     *
     * @param browser the browser to adapt from
     */
    public ActAttachmentBrowser(MultiSelectBrowser<Act> browser) {
        super(browser);
    }

    /**
     * Returns the selections.
     *
     * @return the selections
     */
    @Override
    public Collection<MailAttachment> getSelections() {
        List<MailAttachment> result = new ArrayList<>();
        for (Act act : getBrowser().getSelections()) {
            result.add(new ActAttachment(act));
        }
        return result;
    }

    /**
     * Clears the selections.
     */
    @Override
    public void clearSelections() {
        getBrowser().clearSelections();
    }

    /**
     * Returns the underlying browser.
     *
     * @return the underlying browser
     */
    @Override
    public MultiSelectBrowser<Act> getBrowser() {
        return (MultiSelectBrowser<Act>) super.getBrowser();
    }

    /**
     * Converts an object.
     *
     * @param object the object to convert. May be {@code null}
     * @return the converted object. May be {@code null}
     */
    @Override
    protected MailAttachment convert(Act object) {
        return new ActAttachment(object);
    }
}
