/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.LogoEditor;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * A layout strategy for o.
 *
 * @author Tim Anderson
 */
public abstract class AbstractOrganisationLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Constructs an {@link AbstractOrganisationLayoutStrategy}.
     */
    public AbstractOrganisationLayoutStrategy() {
        super();
    }

    /**
     * Constructs an {@link AbstractOrganisationLayoutStrategy}.
     *
     * @param nodes the nodes to render
     */
    public AbstractOrganisationLayoutStrategy(ArchetypeNodes nodes) {
        super(nodes);
    }

    /**
     * Lays out components in a grid.
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     */
    @Override
    protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentGrid grid = super.createGrid(object, properties, context);
        ComponentState logo = getLogo(object, context);
        grid.add(logo, 2);
        return grid;
    }

    /**
     * Returns a component representing the practice location logo.
     *
     * @param object  the practice location
     * @param context the layout context
     * @return a new component
     */
    protected ComponentState getLogo(IMObject object, LayoutContext context) {
        LogoComponentFactory logoFactory = ServiceHelper.getBean(LogoComponentFactory.class);
        return logoFactory.getLogo((Entity) object, context);
    }

    /**
     * Returns a component representing the practice location logo.
     *
     * @param editor the logo editor
     * @return a new component
     */
    protected ComponentState getLogo(LogoEditor editor) {
        return new ComponentState(editor.getComponent(), null, editor.getFocusGroup(),
                                  Messages.get("admin.practice.logo"));
    }
}
