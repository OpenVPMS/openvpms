/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.info;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.workspace.workflow.worklist.view.ParticipantTaskQuery;

/**
 * Queries task by patient.
 * <p/>
 * By default, queries instances that are incomplete i.e. don't have COMPLETED or CANCELLED status.
 *
 * @author Tim Anderson
 */
public class PatientTaskQuery extends ParticipantTaskQuery {

    /**
     * Constructs a {@link PatientTaskQuery}.
     *
     * @param patient the patient to search for
     */
    public PatientTaskQuery(Party patient) {
        super(patient, "patient", PatientArchetypes.PATIENT_PARTICIPATION);
    }
}