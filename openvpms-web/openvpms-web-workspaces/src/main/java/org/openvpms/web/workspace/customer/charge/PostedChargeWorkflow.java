/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.ContextHelper;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.PrintActTask;
import org.openvpms.web.component.workflow.ReloadTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.WorkflowImpl;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.workflow.payment.PaymentWorkflow;

import java.math.BigDecimal;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;

/**
 * Workflow for when a charge is POSTED.
 * <p/>
 * This prompts to pay the account if the customer has a positive balance, and pops up a dialog to print the charge.
 *
 * @author Tim Anderson
 */
public class PostedChargeWorkflow extends WorkflowImpl {

    /**
     * The context.
     */
    private final TaskContext initial;

    /**
     * Constructs a {@link PostedChargeWorkflow}.
     *
     * @param charge  the charge
     * @param context the context
     * @param help    the help context
     */
    public PostedChargeWorkflow(FinancialAct charge, Context context, HelpContext help) {
        super(help);

        // use a local copy of the context, with the customer set to that of the charge. Supply the patient,
        // if it has a relationship to the charge customer.
        LocalContext local = LocalContext.copy(context);
        IMObjectBean bean = getBean(charge);
        Party customer = bean.getTarget("customer", Party.class);
        local.setCustomer(customer);
        ContextHelper.setPatient(local, context.getPatient(), true);

        initial = new DefaultTaskContext(local, help);
        initial.addObject(charge);
        if (customer != null && charge.isA(INVOICE, COUNTER)) {
            // add a payment workflow if the customer has a positive balance
            if (hasPositiveBalance(customer)) {
                addPaymentWorkflow(charge, local, help);
            }
        }
        addPrintTask(charge, local);
    }

    /**
     * Starts the workflow.
     */
    @Override
    public void start() {
        start(initial);
    }

    /**
     * Adds a payment workflow.
     *
     * @param charge  the charge
     * @param context the context
     * @param help    the help context
     */
    private void addPaymentWorkflow(FinancialAct charge, Context context, HelpContext help) {
        BigDecimal total = charge.getTotal();
        PaymentWorkflow payment = new PaymentWorkflow(total, context, help);
        payment.setRequired(false);
        addTask(payment);
        // need to reload the act as it may be changed via the payment workflow as part of the CustomerAccountRules
        addTask(new ReloadTask(charge.getArchetype()));
    }

    /**
     * Adds a task to print the charge.
     *
     * @param charge  the charge
     * @param context the context
     */
    private void addPrintTask(FinancialAct charge, Context context) {
        MailContext mailContext = new CustomerMailContext(context);
        PrintActTask print = new PrintActTask(charge.getArchetype(), mailContext);
        print.setRequired(false);
        print.setEnableSkip(false);
        addTask(print);
    }

    /**
     * Determines if a customer has a positive balance.
     *
     * @param customer the customer
     * @return {@code true} if the customer has a positive balance, otherwise {@code false}
     */
    private boolean hasPositiveBalance(Party customer) {
        CustomerAccountRules rules = ServiceHelper.getBean(CustomerAccountRules.class);
        BigDecimal balance = rules.getBalance(customer);
        return balance.compareTo(BigDecimal.ZERO) > 0;
    }
}
