/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.style;

import nextapp.echo2.app.ListBox;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.Action;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.app.ContextApplicationInstance;
import org.openvpms.web.echo.dialog.SelectionDialog;
import org.openvpms.web.echo.style.StyleSheetCache;
import org.openvpms.web.echo.style.ThemeResources;
import org.openvpms.web.echo.style.UserStyleSheets;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.web.component.action.ActionBuilder.update;
import static org.openvpms.web.echo.factory.ComponentFactory.setDefaultStyle;

/**
 * Prompts to select from the list of available themes, and updates the practice with the selected theme.
 *
 * @author Tim Anderson
 */
public class ChangeThemeDialog extends SelectionDialog {

    /**
     * The practice to update.
     */
    private final Party practice;

    /**
     * The action factory.
     */
    private final ActionFactory factory;

    /**
     * The style sheet cache.
     */
    private final StyleSheetCache styleSheetCache;

    /**
     * The user style sheets.
     */
    private final UserStyleSheets userStyleSheets;

    /**
     * The theme node.
     */
    private static final String THEME = "theme";

    /**
     * Constructs a {@link SelectionDialog}.
     *
     * @param practice the practice to update
     * @param factory  the action factory
     */
    private ChangeThemeDialog(Party practice, ActionFactory factory) {
        super(Messages.get("stylesheet.theme.title"), Messages.get("stylesheet.theme.message"), getThemes(practice));
        setCloseOnSelection(false);
        this.practice = practice;
        this.factory = factory;
        styleSheetCache = ServiceHelper.getBean(StyleSheetCache.class);
        userStyleSheets = ServiceHelper.getBean(UserStyleSheets.class);
    }

    /**
     * Shows the dialog, if the practice is available.
     */
    public static void display() {
        PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
        ActionFactory factory = ServiceHelper.getBean(ActionFactory.class);
        factory.newAction()
                .withLatest(PracticeArchetypes.PRACTICE, practiceService::getPractice)
                .call(practice -> {
                    ChangeThemeDialog dialog = new ChangeThemeDialog(practice, factory);
                    dialog.show();
                })
                .runOnce();
    }

    /**
     * Get the selected object (if any), and close the window.
     */
    @Override
    protected void onSelected() {
        super.onSelected();
        String selected = (String) getSelected();
        if (selected != null) {
            updateTheme(selected);
        } else {
            close(OK_ID);
        }
    }

    /**
     * Returns the available themes in a list box, with the current theme pre-selected.
     *
     * @param practice the practice
     * @return the themes
     */
    private static ListBox getThemes(Party practice) {
        ArchetypeService service = ServiceHelper.getArchetypeService();
        ThemeResources themes = ServiceHelper.getBean(ThemeResources.class);
        themes.load();   // refresh
        ThemeListModel model = new ThemeListModel(themes);
        IMObjectBean bean = service.getBean(practice);
        String theme = bean.getString(THEME);
        ListBox listBox = new ListBox(model);
        if (theme != null) {
            int index = model.indexOf(theme);
            if (index != -1) {
                listBox.setSelectedIndex(index);
            }
        }
        listBox.setCellRenderer(new ThemeRenderer(themes));
        setDefaultStyle(listBox);
        return listBox;
    }

    /**
     * Updates the theme and closes the dialog.
     *
     * @param newTheme the new theme
     */
    private void updateTheme(String newTheme) {
        Action action = factory.newAction()
                .action(practice, update(THEME, newTheme))
                .onSuccess(() -> {
                    close(OK_ID);
                    styleSheetCache.reset();
                    userStyleSheets.reset();
                    ContextApplicationInstance.getInstance().setStyleSheet();
                })
                .onFailure(() -> close(CANCEL_ID))
                .build();
        action.run();
    }
}
