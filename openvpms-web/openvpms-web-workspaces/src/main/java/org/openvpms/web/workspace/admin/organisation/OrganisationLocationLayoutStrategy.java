/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.web.component.im.layout.ArchetypeNodes;

/**
 * Layout strategy for <em>party.organisationLocation<em>.
 *
 * @author Tim Anderson
 */
public abstract class OrganisationLocationLayoutStrategy extends AbstractOrganisationLayoutStrategy {

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes NODES = new ArchetypeNodes().simple("pricingGroup");


    /**
     * Constructs an {@link OrganisationLocationLayoutStrategy}.
     */
    public OrganisationLocationLayoutStrategy() {
        super(NODES);
    }

}