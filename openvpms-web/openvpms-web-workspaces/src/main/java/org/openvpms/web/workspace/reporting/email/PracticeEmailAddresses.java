/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.email;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.PurposeMatcher;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.contact.ContactHelper;
import org.openvpms.web.component.mail.EmailAddress;
import org.openvpms.web.workspace.reporting.ReportingException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Practice and practice location email addresses, used for reporting.
 *
 * @author Tim Anderson
 */
public class PracticeEmailAddresses {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Email contacts, keyed on practice/practice location reference.
     */
    private final Map<Reference, Contact> contacts = new HashMap<>();

    /**
     * EmailAddress addresses keyed on practice/practice location reference.
     */
    private final Map<Reference, EmailAddress> addresses = new HashMap<>();

    /**
     * The default email contact.
     */
    private final Contact defaultContact;

    /**
     * The default email address.
     */
    private final EmailAddress defaultAddress;

    /**
     * Constructs a {@link PracticeEmailAddresses}.
     *
     * @param practice  the practice
     * @param locations the practice locations
     * @param purpose   the contact purpose to locate email contacts
     * @param service   the archetype service
     * @throws ReportingException if the email address is invalid
     */
    public PracticeEmailAddresses(Party practice, List<Party> locations, String purpose, ArchetypeService service) {
        this.service = service;
        String defaultEmailName = ContactHelper.getDefaultEmailName(service);
        defaultContact = getPracticeEmailContact(practice, purpose);
        defaultAddress = getPracticeEmailAddress(defaultContact, practice, defaultEmailName);
        addLocations(purpose, defaultEmailName, locations);
    }

    /**
     * Returns the practice address to use when sending emails to a customer.
     * <p>
     * If the customer has a practice location, this will determine the address used.
     *
     * @param customer the customer
     * @return the from address
     */
    public EmailAddress getPracticeAddress(Party customer) {
        IMObjectBean bean = service.getBean(customer);
        Reference locationRef = bean.getTargetRef("practice");
        EmailAddress result = addresses.get(locationRef);
        if (result == null) {
            result = defaultAddress;
        }
        return result;
    }

    /**
     * Returns the email address for the given practice or location.
     *
     * @param location the practice or practice location
     * @return the email address. May be {@code null}
     */
    public EmailAddress getAddress(Party location) {
        return location.isA(PracticeArchetypes.LOCATION) ? addresses.get(location.getObjectReference())
                                                         : defaultAddress;
    }

    /**
     * Returns the email contact for the given practice or location.
     *
     * @param location the practice or practice location
     * @return the email address. May be {@code null}
     */
    public Contact getContact(Party location) {
        return location.isA(PracticeArchetypes.LOCATION) ? contacts.get(location.getObjectReference())
                                                         : defaultContact;
    }

    /**
     * Returns the practice email address.
     *
     * @return the practice email address. Never {@code null}
     */
    public EmailAddress getPracticeAddress() {
        return defaultAddress;
    }

    /**
     * Adds email addresses with the specified purpose for each of the locations, if they have one.
     *
     * @param purpose          the contact purpose
     * @param defaultEmailName the default email name
     * @param locations        the practice locations
     */
    private void addLocations(String purpose, String defaultEmailName, List<Party> locations) {
        for (Party location : locations) {
            PurposeMatcher matcher = new PurposeMatcher(ContactArchetypes.EMAIL, purpose, true, service);
            Contact contact = Contacts.find(location.getContacts(), matcher);
            if (contact != null) {
                EmailAddress address = getEmailAddress(contact, location, defaultEmailName);
                if (address != null) {
                    Reference reference = location.getObjectReference();
                    contacts.put(reference, contact);
                    addresses.put(reference, address);
                }
            }
        }
    }

    /**
     * Returns the practice email contact.
     *
     * @param practice the practice
     * @param purpose  the contact purpose
     * @return the corresponding contact
     * @throws ReportingException if there is no email contact
     */
    private Contact getPracticeEmailContact(Party practice, String purpose) {
        PurposeMatcher matcher = new PurposeMatcher(ContactArchetypes.EMAIL, purpose, false, service);
        Contact contact = Contacts.find(practice.getContacts(), matcher);
        if (contact == null) {
            throw new ReportingException(ReportingException.ErrorCode.NoEmailContact, practice.getName());
        }
        return contact;
    }

    /**
     * Returns an email address for a practice email contact.
     *
     * @param contact          the contact
     * @param practice         the practice
     * @param defaultEmailName the default email name
     * @return the corresponding email address
     * @throws ReportingException if the email address is invalid
     */
    private EmailAddress getPracticeEmailAddress(Contact contact, Party practice, String defaultEmailName) {
        EmailAddress result = getEmailAddress(contact, practice, defaultEmailName);
        if (result == null) {
            throw new ReportingException(ReportingException.ErrorCode.InvalidEmailAddress, practice.getName());
        }
        return result;
    }

    /**
     * Returns an email address for an email contact.
     *
     * @param contact          the contact
     * @param practice         the practice/practice location
     * @param defaultEmailName the default email name. If the contact doesn't have this as its name, the contact name
     *                         will be used, else the practice name will be used
     * @return the email address, or {@code null} if the contact is invalid
     */
    private EmailAddress getEmailAddress(Contact contact, Party practice, String defaultEmailName) {
        IMObjectBean bean = service.getBean(contact);
        String address = bean.getString("emailAddress");
        if (!StringUtils.isEmpty(address)) {
            String name;
            if (!StringUtils.isEmpty(contact.getName()) && !StringUtils.equals(defaultEmailName, contact.getName())) {
                name = contact.getName();
            } else {
                name = practice.getName();
            }
            return new EmailAddress(address, name);
        }
        return null;
    }

}
