/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.action.ActionStatus;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.EditDialogFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorSaver;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.workspace.ActPoster;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.system.ServiceHelper;

import java.util.function.Consumer;

/**
 * A {@link ActPoster} for supplier deliveries.
 *
 * @author Tim Anderson
 */
class DeliveryPoster extends ActPoster<FinancialAct> {

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * Constructs a {@link DeliveryPoster}.
     *
     * @param act     the act to post
     * @param context the layout context
     */
    public DeliveryPoster(FinancialAct act, LayoutContext context) {
        super(act, DeliveryActions.INSTANCE, context.getHelpContext());
        this.context = context;
    }

    /**
     * Posts the act.
     *
     * @param act      the act to post
     * @param listener the listener to notify on completion
     */
    @Override
    protected void post(FinancialAct act, Consumer<ActionStatus> listener) {
        DeliveryEditor editor = new DeliveryEditor(act, null, context);
        editor.setStatus(ActStatus.POSTED);
        Validator validator = new DefaultValidator();
        if (!editor.validate(validator)) {
            edit(editor, validator, listener);
        } else {
            IMObjectEditorSaver saver = new IMObjectEditorSaver();
            if (saver.save(editor, () -> listener.accept(ActionStatus.success()))) {
                listener.accept(ActionStatus.success());
            }
        }
    }

    /**
     * Edits the delivery, displaying validation errors.
     *
     * @param editor    the editor
     * @param validator the validator
     * @param listener  the listener to notify on completion
     */
    private void edit(DeliveryEditor editor, Validator validator, Consumer<ActionStatus> listener) {
        // pop up an editor for the delivery and display the errors
        EditDialog dialog = ServiceHelper.getBean(EditDialogFactory.class).create(editor, context.getContext());
        dialog.show();
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                listener.accept(ActionStatus.success());
            }
        });
        ValidationHelper.showError(validator);
    }
}
