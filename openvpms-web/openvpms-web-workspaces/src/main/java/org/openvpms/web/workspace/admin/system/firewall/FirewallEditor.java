/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.firewall;

import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.component.business.dao.im.security.IUserDAO;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.servlet.SessionMonitor;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.firewall.AddressCoverageValidator.Excluded;

import java.util.List;

/**
 * Editor for <em>entity.globalSettingsFirewall</em>.
 *
 * @author Tim Anderson
 */
public class FirewallEditor extends AbstractIMObjectEditor {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The contacts helper.
     */
    private final Contacts contacts;

    /**
     * The firewall settings.
     */
    private final FirewallSettings settings;

    /**
     * The address coverage validator.
     */
    private final AddressCoverageValidator coverage;

    /**
     * The collection of allowed addresses.
     */
    private final AllowedAddressCollectionEditor allowedAddresses;

    /**
     * Constructs a {@link FirewallEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public FirewallEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        practiceService = ServiceHelper.getBean(PracticeService.class);
        contacts = new Contacts(getService());
        settings = new FirewallSettings(object, getService());
        allowedAddresses = new AllowedAddressCollectionEditor(settings, getLayoutContext());
        coverage = new AddressCoverageValidator(ServiceHelper.getBean(SessionMonitor.class),
                                                ServiceHelper.getBean(IUserDAO.class),
                                                getService());
        getEditors().add(allowedAddresses);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new editor
     */
    @Override
    public IMObjectEditor newInstance() {
        return new FirewallEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        strategy.addComponent(new ComponentState(allowedAddresses.getComponent(),
                                                 getProperty(FirewallLayoutStrategy.FIRST_ALLOWED),
                                                 allowedAddresses.getFocusGroup()));
        return strategy;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateAddressCoverage(validator)
               && validateMultifactorAuthentication(validator);
    }

    /**
     * Verifies that logged-in users are covered by the active addresses, if the access type {@code <> ALL}.
     *
     * @param validator the validator
     * @return {@code true} if all addresses are covered, otherwise {@code false}
     */
    private boolean validateAddressCoverage(Validator validator) {
        boolean result = true;
        if (settings.getAccessType() != FirewallSettings.AccessType.UNRESTRICTED) {
            List<String> activeAddresses = allowedAddresses.getActiveAddresses();
            Excluded uncovered = coverage.getFirstExcludedAddress(activeAddresses, settings.getAccessType());
            if (uncovered != null) {
                result = false;
                validator.add(this, Messages.format("admin.system.firewall.addressnotcovered",
                                                    getProperty(FirewallLayoutStrategy.FIRST_ALLOWED).getDisplayName(),
                                                    uncovered.getAddress(), uncovered.getUser()));
            }
        }
        return result;
    }

    /**
     * Ensures that email is configured at the practice level, if multifactor authentication is enabled.
     *
     * @param validator the validator
     * @return {@code true} if the practice is valid, or multifactor authentication is disabled, otherwise {@code false}
     */
    private boolean validateMultifactorAuthentication(Validator validator) {
        boolean result = true;
        Property mfa = getProperty("enableMfa");
        if (mfa.getBoolean()) {
            Party practice = practiceService.getPractice();
            if (practice == null || contacts.getEmail(practice) == null || practiceService.getMailServer() == null) {
                validator.add(mfa, Messages.get("admin.system.firewall.emailnotconfigured"));
                result = false;
            }
        }
        return result;
    }

}