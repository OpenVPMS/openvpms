/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.EditActions;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.system.ServiceHelper;

/**
 * Dialog for editing individual contacts in the customer summary.
 *
 * @author Tim Anderson
 */
class SummaryContactEditDialog extends EditDialog {

    /**
     * The contact.
     */
    private final Contact contact;

    /**
     * The customer.
     */
    private Party customer;

    /**
     * Constructs a {@link SummaryContactEditDialog}.
     *
     * @param contact  the contact being edited
     * @param editor   the editor
     * @param customer the customer
     * @param context  the context
     */
    public SummaryContactEditDialog(Contact contact, IMObjectEditor editor, Party customer, Context context) {
        super(editor, EditActions.okCancel(), context);
        this.customer = customer;
        this.contact = contact;
    }

    /**
     * Returns the customer.
     *
     * @return the customer
     */
    public Party getCustomer() {
        return customer;
    }

    /**
     * Saves the current object.
     *
     * @param editor the editor
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void doSave(IMObjectEditor editor) {
        customer = IMObjectHelper.reload(customer, true);          // make sure we have the latest instance
        customer.removeContact(contact);                           // need to remove and re-add if already present
        customer.addContact(contact);
        ServiceHelper.getArchetypeService().deriveValues(contact); // update description
        super.doSave(editor);
    }
}