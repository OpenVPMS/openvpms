/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.web.component.im.doc.DocumentBackedTextNodeEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientActEditor;
import org.openvpms.web.component.property.Property;

/**
 * An editor for <em>act.patientClinicalNote</em>>.
 * <p/>
 * This stores short notes in the 'note' node. Notes longer than the maximum length supported by the node are
 * stored in a text document.
 *
 * @author Tim Anderson
 */
public class PatientNoteEditor extends PatientActEditor {

    /**
     * The note editor.
     */
    private final DocumentBackedTextNodeEditor editor;

    /**
     * The note node.
     */
    static final String NOTE = "note";

    /**
     * The document node.
     */
    static final String DOCUMENT = "document";

    /**
     * Constructs a {@link PatientNoteEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent act. May be {@code null}
     * @param context the layout context
     */
    public PatientNoteEditor(DocumentAct act, Act parent, LayoutContext context) {
        super(act, parent, context);

        // set up a proxy for the note. This will be saved as a document if it exceeds the character limit of the
        // underlying property
        Property note = getProperty(NOTE);
        Property document = getProperty(DOCUMENT);
        editor = new DocumentBackedTextNodeEditor(act, note, document, context);
    }

    /**
     * Returns the note.
     *
     * @return the note. May be {@code null}
     */
    public String getNote() {
        return editor.getText().getString();
    }

    /**
     * Sets the note.
     *
     * @param note the note. May be {@code null}
     */
    public void setNote(String note) {
        editor.getText().setValue(note);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new PatientNoteEditor(reload((DocumentAct) getObject()), (Act) getParent(), getLayoutContext());
    }

    /**
     * Determines if the object has been changed.
     *
     * @return {@code true} if the object has been changed
     */
    @Override
    public boolean isModified() {
        return super.isModified() || editor.isModified();
    }

    /**
     * Returns the text property.
     *
     * @return text property
     */
    protected Property getText() {
        return editor.getText();
    }

    /**
     * Saves the object.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void saveObject() {
        editor.save(() -> PatientNoteEditor.super.saveObject());
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new PatientClinicalNoteLayoutStrategy(editor.getText());
    }
}
