/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.estimate;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.estimate.EstimateArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;

/**
 * Customer estimate query.
 *
 * @author Tim Anderson
 */
public class EstimateQuery extends DateRangeActQuery<Act> {

    /**
     * The act statuses.
     */
    public static final ActStatuses STATUSES;

    /**
     * The archetypes.
     */
    private static final String[] ARCHETYPES = {EstimateArchetypes.ESTIMATE};

    static {
        STATUSES = new ActStatuses(EstimateArchetypes.ESTIMATE);
        STATUSES.setDefault((String) null);
    }

    /**
     * Constructs an {@link EstimateQuery}.
     *
     * @param customer the customer to search for
     */
    public EstimateQuery(Entity customer) {
        super(customer, "customer", CustomerArchetypes.CUSTOMER_PARTICIPATION, ARCHETYPES, STATUSES, Act.class);
    }
}
