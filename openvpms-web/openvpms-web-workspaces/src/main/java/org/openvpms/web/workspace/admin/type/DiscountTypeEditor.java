/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.type;

import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

import java.math.BigDecimal;

/**
 * Editor for <em>entity.discountType</em>.
 *
 * @author Tim Anderson
 */
public class DiscountTypeEditor extends AbstractIMObjectEditor {

    /**
     * Type node name.
     */
    private static final String TYPE = "type";

    /**
     * Discount fixed node name.
     */
    private static final String DISCOUNT_FIXED = "discountFixed";

    /**
     * Rate node name.
     */
    private static final String RATE = "rate";

    /**
     * Constructs an {@link DiscountTypeEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public DiscountTypeEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        getProperty(TYPE).addModifiableListener(modifiable -> {
            if (DiscountRules.FIXED.equals(getType())) {
                setDiscountFixed(false);
            }
        });
    }

    /**
     * Sets the discount type.
     *
     * @param type the type
     */
    public void setType(String type) {
        getProperty(TYPE).setValue(type);
    }

    /**
     * Returns the discount type.
     *
     * @return the discount type. May be {@code null}
     */
    public String getType() {
        return getProperty(TYPE).getString();
    }

    /**
     * Determines if the fixed amount should be included in the discount.
     *
     * @param discountFixed if {@code true}, the fixed amount should be included, else it should be excluded
     */
    public void setDiscountFixed(boolean discountFixed) {
        getProperty(DISCOUNT_FIXED).setValue(discountFixed);
    }

    /**
     * Sets the discount rate.
     *
     * @param rate the rate
     */
    public void setRate(BigDecimal rate) {
        getProperty(RATE).setValue(rate);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new DiscountTypeEditor(reload(getObject()), getParent(), newLayoutContext());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateFixed(validator);
    }

    /**
     * Verifies that if the discount type is {@code FIXED}, the <em>discountFixed</em> node is unset.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    private boolean validateFixed(Validator validator) {
        boolean valid = true;
        if (DiscountRules.FIXED.equals(getProperty(TYPE).getString())) {
            Property discountFixed = getProperty(DISCOUNT_FIXED);
            if (discountFixed.getBoolean()) {
                valid = false;
                String message = Messages.get("discount.fixedWithdiscountFixedUnsupported");
                validator.add(discountFixed, new ValidatorError(message));
            }
        }
        return valid;
    }
}
