/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.lookup;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for <em>lookup.oauth2ClientRegistration</em>.
 *
 * @author Tim Anderson
 */
public class OAuth2ClientRegistrationEditor extends AbstractLookupEditor {

    /**
     * Gmail client registration id.
     */
    public static final String GMAIL = "gmail";

    /**
     * Outlook client registration id.
     */
    public static final String OUTLOOK = "outlook";

    /**
     * The code component.
     */
    private ComponentState codeComponent;

    /**
     * Determines if the tenant id node is displayed.
     */
    private boolean tenantIdDisplayed;

    /**
     * The tenant id node name.
     */
    private static final String TENANT_ID = "tenantId";

    /**
     * Constructs an {@link OAuth2ClientRegistrationEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public OAuth2ClientRegistrationEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateTenantId(validator) && validateSecret(validator);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        if (codeComponent == null) {
            Property code = getProperty(CODE);
            codeComponent = getLayoutContext().getComponentFactory().create(code, getObject());
            code.addModifiableListener(modifiable -> onCodeChanged());
        }
        IMObjectLayoutStrategy layoutStrategy = new OAuth2ClientRegistrationLayoutStrategy();
        layoutStrategy.addComponent(codeComponent);
        return layoutStrategy;
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This can be used to perform processing that requires all editors to be created.
     */
    @Override
    protected void onLayoutCompleted() {
        tenantIdDisplayed = displayTenantId();
        if (tenantIdDisplayed && StringUtils.trimToNull(getProperty(TENANT_ID).getString()) == null) {
            // the tenantId is defined as an optional field, so focus will move to the first mandatory field.
            // Force the focus to the tenantId.
            getFocusGroup().setDefault(getEditor(TENANT_ID).getComponent());
            // TODO - add support to make optional fields mandatory prior to layout by making the PropertySet mutable
        }
    }

    /**
     * Verifies the tenantId is present, when Outlook is the provider.
     *
     * @param validator the validator
     * @return {@code true} if the tenantId is valid, otherwise {@code false}
     */
    private boolean validateTenantId(Validator validator) {
        boolean valid = true;
        Property tenantId = getProperty(TENANT_ID);
        if (StringUtils.trimToNull(tenantId.getString()) == null && OUTLOOK.equals(getCode())) {
            valid = false;
            validator.add(this, new ValidatorError(tenantId, Messages.format("property.error.required",
                                                                             tenantId.getDisplayName())));
        }
        return valid;
    }

    /**
     * Validates the client secret is present where required by a provider.
     *
     * @param validator the validator
     * @return {@code true} if the secret is valid, otherwise {@code false}
     */
    private boolean validateSecret(Validator validator) {
        boolean valid = true;
        String code = getCode();
        Property secret = getProperty("clientSecret");
        if (StringUtils.trimToNull(secret.getString()) == null && (GMAIL.equals(code) || OUTLOOK.equals(code))) {
            valid = false;
            validator.add(this, new ValidatorError(secret, Messages.format("property.error.required",
                                                                           secret.getDisplayName())));
        }
        return valid;
    }

    /**
     * Determines if the tenant id node should be displayed.
     *
     * @return {@code true} if the tenant id node should be displayed
     */
    private boolean displayTenantId() {
        return OUTLOOK.equals(getCode());
    }

    /**
     * Invoked when the code changes.
     */
    private void onCodeChanged() {
        if (tenantIdDisplayed != displayTenantId()) {
            // need to change the layout to include/exclude the tenant id
            onLayout();
        } else {
            OAuth2ClientRegistrationLayoutStrategy layout
                    = (OAuth2ClientRegistrationLayoutStrategy) getView().getLayout();
            if (layout != null) {
                layout.updateRedirectURI((Lookup) getObject());
            }
        }
    }
}