/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.button.ButtonGroup;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.web.component.im.edit.AbstractRemoveConfirmationHandler;
import org.openvpms.web.component.im.edit.IMObjectCollectionEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.AltModelActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.charge.DefaultEditorQueue;
import org.openvpms.web.workspace.customer.charge.EditorQueue;

/**
 * Editor for collections of payment and refund item {@link ActRelationship}s.
 * <p/>
 * This defaults the item archetype to the <em>defaultPaymentType</em> of <em>party.organisationPractice</em>.
 * i.e, if the defaultPayment type is <em>act.customerAccountPaymentEFT</em>, the default refund type is
 * <em>act.customerAccountRefundEFT</em>.
 *
 * @author Tim Anderson
 */
public class PaymentItemRelationshipCollectionEditor extends AltModelActRelationshipCollectionEditor {

    /**
     * The editor queue.
     */
    private final EditorQueue queue;

    /**
     * The default payment type node of party.organisationPractice.
     */
    private static final String DEFAULT_PAYMENT_TYPE = "defaultPaymentType";

    /**
     * Creates an {@link PaymentItemRelationshipCollectionEditor}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public PaymentItemRelationshipCollectionEditor(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
        queue = new DefaultEditorQueue(context.getContext());

        // register a handler to prevent removal of EFT items with in progress or approved transactions
        setRemoveConfirmationHandler(new RemoveHandler());
    }

    /**
     * Returns the editor queue.
     *
     * @return the queue
     */
    public EditorQueue getQueue() {
        return queue;
    }

    /**
     * Adds a new item to the collection, subject to the constraints of {@link #create(String)}.
     *
     * @param shortName the archetype to add
     * @return the editor for the item, or {@code null} a new item cannot be created.
     */
    @Override
    public IMObjectEditor add(String shortName) {
        IMObjectEditor editor = super.add(shortName);
        if (editor != null) {
            editor.getComponent();
            // clear any modified flags so the object can be replaced
            editor.clearModified();
        }
        return editor;
    }

    /**
     * Validates the object.
     * <p>
     * This validates the current object being edited, and if valid, the collection.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && queue.isComplete();
    }

    /**
     * Lays out the component.
     *
     * @param context the layout context
     * @return the component
     */
    @Override
    protected Component doLayout(LayoutContext context) {
        Component component = super.doLayout(context);
        String archetype = getDefaultArchetype();
        if (archetype != null) {
            setArchetype(archetype);
        }
        return component;
    }

    /**
     * Determines if the previous and next navigation buttons should be displayed.
     *
     * @return {@code false}
     */
    @Override
    protected boolean showPreviousNext() {
        return false;
    }

    /***
     * Adds an archetype selector to the button row, if there is more than one archetype.
     *
     * @param buttons the buttons
     * @param focus   the focus group
     */
    @Override
    protected void addArchetypeSelector(ButtonRow buttons, FocusGroup focus) {
        ArchetypeService service = getService();
        String[] range = DescriptorHelper.getShortNames(getCollectionPropertyEditor().getArchetypeRange(),
                                                        false, service);
        ButtonGroup group = new ButtonGroup();
        String defaultArchetype = getDefaultArchetype();
        if (defaultArchetype == null && range.length > 0) {
            defaultArchetype = range[0];
        }
        for (String archetype : range) {
            String displayName = DescriptorHelper.getDisplayName(archetype, service);
            RadioButton button = ButtonFactory.text(displayName, group, () -> onArchetypeSelected(archetype));
            if (archetype.equals(defaultArchetype)) {
                setArchetype(archetype);
                button.setSelected(true);
            }
            buttons.addButton(button);
        }
    }

    /**
     * Invoked when on archetype is selected.
     * <p/>
     * If the current object is new and unmodified, it will be replaced by an object of the specified archetype.
     * If not, it specifies the archetype that will be created when clicking Add.
     *
     * @param archetype the archetype
     */
    private void onArchetypeSelected(String archetype) {
        setArchetype(archetype);
        IMObjectEditor currentEditor = getCurrentEditor();
        IMObject object = (currentEditor != null) ? currentEditor.getObject() : null;
        if (object != null && object.isNew() && !currentEditor.isModified()) {
            // existing object hasn't been changed from its default values, so replace it
            remove(object);
            onAdd();
        }
    }

    /**
     * Returns the default item archetype.
     *
     * @return the default item archetype
     */
    private String getDefaultArchetype() {
        String archetype = null;
        PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
        Party practice = practiceService.getPractice();
        if (practice != null) {
            IMObjectBean bean = getBean(practice);
            archetype = bean.getString(DEFAULT_PAYMENT_TYPE);
            if (archetype != null) {
                if (getObject().isA(CustomerAccountArchetypes.REFUND)) {
                    archetype = ServiceHelper.getBean(CustomerAccountRules.class).getReversalArchetype(archetype);
                }
            }
        }
        return archetype;
    }

    private static class RemoveHandler extends AbstractRemoveConfirmationHandler {

        /**
         * Displays a confirmation dialog to confirm removal of an object from a collection.
         * <p>
         * If approved, it performs the removal.
         *
         * @param object     the object to remove
         * @param collection the collection to remove the object from, if approved
         */
        @Override
        protected void confirmRemove(IMObject object, IMObjectCollectionEditor collection) {
            if (object.isA(CustomerAccountArchetypes.PAYMENT_EFT, CustomerAccountArchetypes.REFUND_EFT)) {
                PaymentItemRelationshipCollectionEditor editor = (PaymentItemRelationshipCollectionEditor) collection;
                EFTPaymentItemEditor itemEditor = (EFTPaymentItemEditor) editor.getEditor(object);
                String status = itemEditor.getDeletionStatus();
                if (status == null) {
                    super.confirmRemove(object, collection);
                } else {
                    String displayName = getDisplayName(object, collection);
                    String title = Messages.format("imobject.collection.delete.title", displayName);
                    if (Transaction.Status.APPROVED.toString().equals(status)) {
                        InformationDialog.show(title, Messages.format("customer.payment.delete.approvedEFT",
                                                                      displayName));
                    } else {
                        InformationDialog.show(title, Messages.format("customer.payment.delete.outstandingEFT",
                                                                      displayName));
                    }
                    cancelRemove(collection);
                }
            } else {
                super.confirmRemove(object, collection);
            }
        }
    }
}
