/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist.view;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.workspace.workflow.scheduling.view.ParticipantScheduleEventQuery;

/**
 * Queries tasks by participant.
 * <p/>
 * By default, queries instances that are incomplete i.e. don't have COMPLETED or CANCELLED status.
 *
 * @author Tim Anderson
 */
public class ParticipantTaskQuery extends ParticipantScheduleEventQuery {

    /**
     * The act statuses to query..
     */
    private static final ActStatuses STATUSES = new ActStatuses(new StatusLookupQuery(ScheduleArchetypes.TASK));

    /**
     * Constructs a {@link ParticipantTaskQuery}.
     *
     * @param entity        the entity to search for
     * @param participant   the participant node name
     * @param participation the entity participation archetype
     */
    public ParticipantTaskQuery(Entity entity, String participant, String participation) {
        super(entity, participant, participation, ScheduleArchetypes.TASK, STATUSES);
    }

}