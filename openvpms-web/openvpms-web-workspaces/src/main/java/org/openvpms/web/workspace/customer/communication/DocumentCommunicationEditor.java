/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.DocumentBackedTextNodeEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;

/**
 * Editor for <em>act.customerCommunication</em> acts that have a document node.
 *
 * @author Tim Anderson
 */
public abstract class DocumentCommunicationEditor extends AbstractCommunicationEditor {

    /**
     * The message editor.
     * <p/>
     * This will populate the message node for short messages, or the document node for long messages.
     */
    private final DocumentBackedTextNodeEditor editor;

    /**
     * The supported contact archetype short names.
     */
    private final String contacts;


    /**
     * Constructs an {@link DocumentCommunicationEditor}.
     *
     * @param act      the act to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context
     * @param contacts the contact archetype short names. May be {@code null}
     */
    public DocumentCommunicationEditor(DocumentAct act, IMObject parent, LayoutContext context, String contacts) {
        super(act, parent, context);
        this.contacts = contacts;

        // set up a proxy for the message. This will be saved as a document if it exceeds the character limit of the
        // underlying property
        Property message = getProperty("message");
        Property document = getProperty("document");
        editor = new DocumentBackedTextNodeEditor(act, message, document, context);
    }

    /**
     * Returns the message.
     *
     * @return the message. May be {@code null}
     */
    public String getMessage() {
        return getProperty().getString();
    }

    /**
     * Sets the message.
     *
     * @param message the message. May be {@code null}
     */
    public void setMessage(String message) {
        getProperty().setValue(message);
    }


    /**
     * Determines if the object has been changed.
     *
     * @return {@code true} if the object has been changed
     */
    @Override
    public boolean isModified() {
        return super.isModified() || editor.isModified();
    }

    /**
     * Saves the object.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    protected void saveObject() {
        editor.save(() -> DocumentCommunicationEditor.super.saveObject());
    }

    /**
     * Returns the message property.
     *
     * @return the message property
     */
    protected Property getProperty() {
        return editor.getText();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new CommunicationLayoutStrategy(getProperty(), contacts, getShowPatient());
    }

}
