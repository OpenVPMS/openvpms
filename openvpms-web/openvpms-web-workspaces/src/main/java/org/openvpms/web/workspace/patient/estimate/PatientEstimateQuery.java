/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.estimate;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.button.CheckBox;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.workspace.customer.estimate.EstimateQuery;

/**
 * A query set for estimates for a particular patient, or all patients.
 * <p/>
 * If restricted to a specific patient, it only returns estimates that include the specified patient, and no other.
 *
 * @author Tim Anderson
 */
public class PatientEstimateQuery extends EstimateQuery {

    /**
     * The patient.
     */
    private final Party patient;

    /**
     * Determines if estimates for all patients should be displayed.
     */
    private final CheckBox allPatients;

    /**
     * Constructs a {@link PatientEstimateQuery}.
     *
     * @param customer the customer to search for
     * @param patient  the patient to search for
     */
    public PatientEstimateQuery(Party customer, Party patient) {
        this(customer, patient, false);
    }

    /**
     * Constructs a {@link PatientEstimateQuery}.
     *
     * @param customer        the customer to search for
     * @param patient         the patient to search for
     * @param showAllPatients if {@code true}, add a toggle to show estimates for all patients
     */
    public PatientEstimateQuery(Party customer, Party patient, boolean showAllPatients) {
        super(customer);
        this.patient = patient;
        if (showAllPatients) {
            allPatients = CheckBoxFactory.create("label.allpatients", false);
            allPatients.addActionListener(new ActionListener() {
                @Override
                public void onAction(ActionEvent event) {
                    onQuery();
                }
            });
        } else {
            allPatients = null;
        }
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        super.doLayout(container);
        if (allPatients != null) {
            container.add(allPatients);
        }
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        if (allPatients != null && allPatients.isSelected()) {
            return super.createResultSet(sort);
        }
        return new PatientEstimateResultSet(patient, getArchetypeConstraint(), getParticipantConstraint(),
                                            getFrom(), getTo(), getStatuses(), getMaxResults(), sort);
    }
}
