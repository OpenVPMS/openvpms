/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.AbstractDocumentTemplateQuery;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.IMObjectTableBrowser;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;


/**
 * Document template workspace.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateWorkspace extends ResultSetCRUDWorkspace<Entity> {

    /**
     * Constructs a {@link DocumentTemplateWorkspace}.
     */
    public DocumentTemplateWorkspace(Context context) {
        super("admin.documentTemplate", context);
        setArchetypes(Entity.class, DocumentArchetypes.DOCUMENT_TEMPLATE, DocumentArchetypes.EMAIL_TEMPLATES,
                      DocumentArchetypes.SMS_TEMPLATES, DocumentArchetypes.LETTERHEAD);
    }

    /**
     * Creates a new browser.
     *
     * @param query the query
     * @return a new browser
     */
    @Override
    protected Browser<Entity> createBrowser(Query<Entity> query) {
        return new TemplateBrowser(query, new DefaultLayoutContext(getContext(), getHelpContext()));
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery() {
        return new TemplateQuery(getArchetypes().getShortNames());
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<Entity> createCRUDWindow() {
        QueryBrowser<Entity> browser = getBrowser();
        return new DocumentTemplateCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(),
                                              getContext(), getHelpContext());
    }

    private static class TemplateQuery extends AbstractDocumentTemplateQuery {

        public TemplateQuery(String[] shortNames) {
            super(shortNames);
            setContains(true);
        }
    }

    private static class TemplateBrowser extends IMObjectTableBrowser<Entity> {

        /**
         * Constructs a {@link TemplateBrowser} that queries IMObjects using the specified query.
         *
         * @param query   the query
         * @param context the layout context
         */
        public TemplateBrowser(Query<Entity> query, LayoutContext context) {
            super(query, context);
        }

        /**
         * Creates a table model.
         *
         * @param archetypes the archetypes the table must display
         * @param query      the query
         * @param context    the layout context
         * @return a new table model
         */
        @Override
        protected IMObjectTableModel<Entity> createTableModel(String[] archetypes, Query<Entity> query,
                                                              LayoutContext context) {
            if (archetypes.length == 1) {
                String archetype = archetypes[0];
                if (DocumentArchetypes.DOCUMENT_TEMPLATE.equals(archetype)) {
                    return new AdminDocumentTemplateTableModel(query, context);
                } else if (DocumentArchetypes.USER_EMAIL_TEMPLATE.equals(archetype)
                           || DocumentArchetypes.SYSTEM_EMAIL_TEMPLATE.equals(archetype)) {
                    return new AdminEmailDocumentTemplateTableModel(query, context);
                }
            }
            return super.createTableModel(archetypes, query, context);
        }
    }
}
