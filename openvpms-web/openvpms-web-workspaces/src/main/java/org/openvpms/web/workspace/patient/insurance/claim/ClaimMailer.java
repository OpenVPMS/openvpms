/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.archetype.rules.insurance.InsuranceArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.doc.DocumentJobManager;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.job.Job;
import org.openvpms.web.component.job.JobBuilder;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailDialogFactory;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 * Emails claims.
 *
 * @author Tim Anderson
 */
public class ClaimMailer {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Claim helper.
     */
    private final ClaimHelper helper;

    /**
     * The reporter factory.
     */
    private final ReporterFactory reporterFactory;

    /**
     * The job manager.
     */
    private final DocumentJobManager jobManager;

    /**
     * Constructs a {@link ClaimMailer}.
     */
    public ClaimMailer() {
        service = ServiceHelper.getArchetypeService();
        reporterFactory = ServiceHelper.getBean(ReporterFactory.class);
        jobManager = ServiceHelper.getBean(DocumentJobManager.class);
        helper = new ClaimHelper(service);
    }

    /**
     * Asynchronously creates a {@link MailDialog} to email a claim.
     *
     * @param claim       the claim
     * @param act         the claim act
     * @param attachments the attachments
     * @param context     the context
     * @param help        the help context
     * @param listener    the listener to notify when the dialog is created
     */
    public void mail(Claim claim, Act act, List<IMObject> attachments, Context context, HelpContext help,
                     Consumer<MailDialog> listener) {
        MailContext mailContext = new InsurerMailContext(context, help);
        MailDialogFactory factory = ServiceHelper.getBean(MailDialogFactory.class);
        MailDialog dialog = factory.create(mailContext, new DefaultLayoutContext(context, help));
        dialog.getMailEditor().setObject(act); // make the claim available for template expansion

        // add attachments
        mail(claim, attachments.iterator(), dialog, context, listener);
    }

    /**
     * Recursively process attachments for mailing. On completion, the listener will be notified with the populated
     * mail dialog.
     *
     * @param claim       the claim
     * @param dialog      the dialog to populate
     * @param attachments the attachments to process
     * @param listener    the listener to notify on completion
     */
    private void mail(Claim claim, Iterator<IMObject> attachments, MailDialog dialog, Context context,
                      Consumer<MailDialog> listener) {
        if (attachments.hasNext()) {
            IMObject object = attachments.next();
            MailEditor editor = dialog.getMailEditor();
            if (object.isA(InsuranceArchetypes.ATTACHMENT)) {
                addAttachment(editor, (DocumentAct) object);
                mail(claim, attachments, dialog, context, listener); // process the next attachment
            } else {
                generateAttachment(object, claim, context, document -> {
                    editor.addAttachment(document);
                    mail(claim, attachments, dialog, context, listener); // process the next attachment
                });
            }
        } else {
            listener.accept(dialog);
        }
    }

    /**
     * Adds an attachment to the editor.
     *
     * @param editor the mail editor
     * @param act    the attachment
     */
    private void addAttachment(MailEditor editor, DocumentAct act) {
        Document document = act.getDocument() != null ? service.get(act.getDocument(), Document.class) : null;
        if (document != null) {
            editor.addAttachment(document);
        }
    }

    /**
     * Schedules attachment generation.
     *
     * @param object   the attachment
     * @param claim    the claim
     * @param context  the context
     * @param listener the listener for the generated attachment
     */
    private void generateAttachment(IMObject object, Claim claim, Context context, Consumer<Document> listener) {
        DocumentTemplateLocator locator = helper.getDocumentTemplateLocator(claim, object, context);
        Reporter<IMObject> reporter = reporterFactory.create(object, locator, Reporter.class);
        reporter.setFields(ReportContextFactory.create(context));
        Job<Document> job = JobBuilder.<Document>newJob("Claim attachment", context.getUser())
                .get(() -> reporter.getDocument(Reporter.DEFAULT_MIME_TYPE, true))
                .completed(listener)
                .build();
        jobManager.runInteractive(job, Messages.get("document.generateattachment.title"),
                                  Messages.get("document.generateattachment.cancel"));
    }
}