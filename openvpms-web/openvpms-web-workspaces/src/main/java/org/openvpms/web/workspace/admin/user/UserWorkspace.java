/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.app.LocationMonitor;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;
import org.openvpms.web.system.ServiceHelper;


/**
 * User workspace.
 *
 * @author Tim Anderson
 */
public class UserWorkspace extends ResultSetCRUDWorkspace<User> {

    /**
     * The location monitor.
     */
    private final LocationMonitor monitor;

    /**
     * Constructs an {@link UserWorkspace}.
     */
    public UserWorkspace(Context context) {
        super("admin.user", context);
        String displayName = DescriptorHelper.getDisplayName(UserArchetypes.USER, ServiceHelper.getArchetypeService());
        setArchetypes(Archetypes.create(new String[]{UserArchetypes.USER}, User.class, UserArchetypes.USER,
                                        displayName));
        monitor = new LocationMonitor((GlobalContext) context, party -> updateButtons());
    }

    /**
     * Invoked when the workspace is displayed.
     */
    @Override
    public void show() {
        super.show();
        monitor.register();
        updateButtons();
    }

    /**
     * Invoked when the workspace is hidden.
     */
    @Override
    public void hide() {
        monitor.unregister();
        super.hide();
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<User> createCRUDWindow() {
        QueryBrowser<User> browser = getBrowser();
        return new UserCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(), getContext(),
                                  getHelpContext());
    }

    /**
     * Updates the button display if the location has changed.
     * <p/>
     * This is required as the SFS button is displayed based on the current location
     */
    private void updateButtons() {
        if (monitor.changed()) {
            ResultSetCRUDWindow<User> window = getCRUDWindow();
            if (window != null) {
                window.refreshButtons();
            }
        }
    }
}
