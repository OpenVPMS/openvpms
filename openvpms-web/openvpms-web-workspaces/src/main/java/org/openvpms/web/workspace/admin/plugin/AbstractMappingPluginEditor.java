/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.plugin;

import echopointng.TabbedPane;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.service.MappingProvider;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.service.config.ConfigurableService;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectTabPane;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.TabbedPaneFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.tabpane.TabPaneModel;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.mapping.MappingTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * Base class for plugins that implement the {@link MappingProvider} interface.
 * <p/>
 * A mapping tab will be added when the plugin is configured.
 * <p/>
 * NOTE: mappings are validated and saved independently of the plugin.
 *
 * @author Tim Anderson
 */
public abstract class AbstractMappingPluginEditor extends AbstractIMObjectEditor {

    /**
     * The mapping container.
     */
    private final Component mappingContainer;

    /**
     * The mapping tab pane.
     */
    private TabbedPane pane;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AbstractMappingPluginEditor.class);


    /**
     * Constructs an {@link AbstractMappingPluginEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AbstractMappingPluginEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        mappingContainer = ColumnFactory.create(Styles.INSET_Y);
    }

    /**
     * Save any edits.
     */
    @Override
    protected void doSave() {
        super.doSave();
        refresh();
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This can be used to perform processing that requires all editors to be created.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        refresh();
    }

    /**
     * Refreshes the mapping display, if the plugin implements the {@link MappingProvider} interface.
     */
    protected void refresh() {
        int index = (pane != null) ? pane.getSelectedIndex() : -1;
        mappingContainer.removeAll();

        try {
            IMObject config = getObject();
            if (!config.isNew()) {
                List<Mappings<?>> mappingsList = getMappings();
                if (!mappingsList.isEmpty()) {
                    TabPaneModel model = new TabPaneModel(mappingContainer);
                    for (Mappings<?> mappings : mappingsList) {
                        PagedIMTable<Mapping> table = new PagedIMTable<>(new MappingTableModel<>(mappings));
                        table.setResultSet(new ListResultSet<>(mappings.getMappings(), 20));
                        ButtonRow row = new ButtonRow();
                        row.addButton("button.editmappings", new ActionListener() {
                            @Override
                            public void onAction(ActionEvent event) {
                                editMappings();
                            }
                        });
                        Column content = ColumnFactory.create(
                                Styles.INSET_Y, ColumnFactory.create(Styles.CELL_SPACING,
                                                                     RowFactory.create(Styles.INSET_X, row),
                                                                     table.getTable()));
                        model.addTab(mappings.getDisplayName(), content);
                    }
                    pane = TabbedPaneFactory.create(model);
                    if (index != -1 && index < pane.getModel().size()) {
                        pane.setSelectedIndex(index);
                    }
                    mappingContainer.add(ColumnFactory.create(Styles.INSET_Y, pane));
                } else {
                    Button button = ButtonFactory.create("button.refresh", this::refresh);
                    mappingContainer.add(ColumnFactory.create(
                            Styles.LARGE_INSET, RowFactory.create(Styles.CELL_SPACING, button,
                                                                  LabelFactory.create("mapping.notfound"))));
                }
            } else {
                String message = Messages.format("mapping.save", getDisplayName());
                mappingContainer.add(ColumnFactory.create(Styles.LARGE_INSET, LabelFactory.text(message)));
            }
        } catch (Throwable exception) {
            Label label = LabelFactory.create(true, true);
            label.setText(ErrorFormatter.format(exception));
            mappingContainer.add(label);
        }
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Determines if the plugin configuration is associated with a {@link MappingProvider}.
     *
     * @return the mapping provider, or {@code null} if the service doesn't implement the {@link MappingProvider}
     * interface
     */
    protected MappingProvider getMappingProvider() {
        MappingProvider result = null;
        IMObject config = getObject();
        Object pluginService = getPluginService(config);
        if (pluginService instanceof MappingProvider) {
            result = (MappingProvider) pluginService;
        }
        return result;
    }

    /**
     * Returns the service associated with a plugin configuration.
     * <p/>
     * This implementation looks for a {@link ConfigurableService} that accepts the configuration.
     *
     * @param config the configuration
     * @return the corresponding service, or {@code null} if none is found
     */
    protected Object getPluginService(IMObject config) {
        Object result = null;
        PluginManager manager = ServiceHelper.getBean(PluginManager.class);
        for (ConfigurableService service : manager.getServices(ConfigurableService.class)) {
            try {
                if (config.isA(service.getArchetype())) {
                    result = service;
                    break;
                }
            } catch (Throwable exception) {
                log.warn(exception.getMessage(), exception);
            }
        }
        return result;
    }

    private void editMappings() {
        List<Mappings<?>> mappingsList = getMappings();
        if (!mappingsList.isEmpty()) {
            int selectedIndex = pane != null ? pane.getSelectedIndex() : 0;
            MappingEditDialog dialog = new MappingEditDialog(mappingsList, selectedIndex, getLayoutContext());
            dialog.addWindowPaneListener(new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    refresh();
                }
            });
            dialog.show();
        }
    }

    private List<Mappings<?>> getMappings() {
        MappingProvider provider = getMappingProvider();
        return provider != null ? provider.getMappings() : Collections.emptyList();
    }

    private class LayoutStrategy extends AbstractLayoutStrategy {

        /**
         * Lays out each child component in a tabbed pane.
         *
         * @param object     the object to lay out
         * @param parent     the parent object. May be {@code null}
         * @param properties the properties
         * @param container  the container to use
         * @param context    the layout context
         */
        @Override
        protected void doComplexLayout(IMObject object, IMObject parent, List<Property> properties, Component container,
                                       LayoutContext context) {
            IMObjectTabPaneModel model = doTabLayout(object, properties, container, context, false);
            model.addTab(Messages.get("mapping.title"), mappingContainer);
            IMObjectTabPane pane = new IMObjectTabPane(model);
            pane.setSelectedIndex(0);
            container.add(pane);
        }
    }
}
