/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.filetransfer.UploadEvent;
import nextapp.echo2.app.filetransfer.UploadListener;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.web.component.im.doc.AbstractUploadListener;
import org.openvpms.web.component.im.doc.UploadDialog;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.OptionDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.plugin.BundleHelper;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Plugin browser.
 *
 * @author Tim Anderson
 */
public class PluginBrowser extends AbstractTabComponent {

    /**
     * The plugin manager.
     */
    private final PluginManager manager;

    /**
     * The container.
     */
    private final Component component;

    /**
     * Plugin status.
     */
    private final Label status;

    /**
     * The plugins table.
     */
    private final PagedIMTable<Bundle> plugins;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PluginBrowser.class);


    /**
     * Start plugin button identifier.
     */
    private static final String START_ID = "button.start";

    /**
     * Stop plugin button identifier.
     */
    private static final String STOP_ID = "button.stop";

    /**
     * Install plugin button identifier.
     */
    private static final String INSTALL_ID = "button.install";

    /**
     * Uninstall plugin button identifier.
     */
    private static final String UNINSTALL_ID = "button.uninstall";

    /**
     * Refresh display button identifier.
     */
    private static final String REFRESH_ID = "button.refresh";

    /**
     * Configure plugin manager button identifier.
     */
    private static final String CONFIGURE_ID = "button.configure";

    /**
     * Constructs a {@link PluginBrowser}.
     *
     * @param help the help context for the tab
     */
    public PluginBrowser(HelpContext help) {
        super(help);
        status = LabelFactory.create(Styles.BOLD);
        plugins = new PagedIMTable<>(new PluginTableModel());
        plugins.getTable().addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                enableButtons();
            }
        });
        component = ColumnFactory.create(Styles.INSET,
                                         ColumnFactory.create(Styles.WIDE_CELL_SPACING, status,
                                                              plugins.getComponent()));
        manager = ServiceHelper.getBean(PluginManager.class);
        ButtonSet buttons = getButtonSet();
        buttons.add(START_ID, this::onStart);
        buttons.add(STOP_ID, this::onStop);
        buttons.add(INSTALL_ID, this::onInstall);
        buttons.add(UNINSTALL_ID, this::onUninstall);
        buttons.add(REFRESH_ID, this::onRefresh);
        buttons.add(CONFIGURE_ID, this::onConfigure);
    }

    /**
     * Invoked when the tab is displayed.
     */
    @Override
    public void show() {
        refresh();
    }

    /**
     * Returns the tab component.
     *
     * @return the tab component
     */
    @Override
    public Component getComponent() {
        return SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP, "SplitPaneWithButtonRow",
                                       getButtons(), component);
    }

    /**
     * Enables/disables buttons.
     */
    private void enableButtons() {
        ButtonSet buttons = getButtonSet();
        Bundle selected = plugins.getSelected();
        boolean started = manager.isStarted();
        boolean uninstall = started && selected != null && manager.canUninstall(selected);
        boolean restart = started && selected != null && manager.canRestart(selected);
        buttons.setEnabled(INSTALL_ID, started);
        buttons.setEnabled(UNINSTALL_ID, uninstall);
        buttons.setEnabled(START_ID, restart);
        buttons.setEnabled(STOP_ID, restart);
        buttons.setEnabled(REFRESH_ID, started);
    }

    /**
     * Refreshes the display.
     */
    private void refresh() {
        int active = 0;
        Bundle selected = plugins.getSelected();
        Bundle[] bundles = manager.getBundles();
        for (Bundle bundle : bundles) {
            if (bundle.getState() == Bundle.ACTIVE) {
                active++;
            }
        }
        status.setText(Messages.format("admin.system.plugin.active", active, bundles.length));
        int pageSize = 20;
        ResultSet<Bundle> set = new ListResultSet<>(Arrays.asList(bundles), pageSize);
        plugins.setResultSet(set);
        if (selected != null) {
            for (int i = 0; i < bundles.length; ++i) {
                if (bundles[i].getBundleId() == selected.getBundleId()) {
                    plugins.getModel().setPage(i / pageSize);
                    plugins.setSelected(bundles[i]);
                    break;
                }
            }
        }

        enableButtons();
    }

    /**
     * Starts a plugin.
     */
    private void onStart() {
        Bundle selected = plugins.getSelected();
        if (selected != null && selected.getState() != Bundle.ACTIVE
            && selected.getBundleId() != Constants.SYSTEM_BUNDLE_ID) {
            String title = Messages.get("admin.system.plugin.start.title");
            String message = Messages.format("admin.system.plugin.start.message", BundleHelper.getName(selected));
            ConfirmationDialog.show(title, message, ConfirmationDialog.YES_NO, new PopupDialogListener() {
                @Override
                public void onYes() {
                    start(selected);
                }
            });
        }
        refresh();
    }

    /**
     * Scans for new plugins and refreshes the display.
     * <p/>
     * Scanning is required to pick up plugins that may have been deployed on the command line. Note that
     * it will not pick up those plugins that have been uninstalled; this requires a restart.
     */
    private void onRefresh() {
        try {
            manager.scanForNewPlugins();
        } catch (Exception exception) {
            ErrorHelper.show(exception);
        }
        refresh();
    }

    /**
     * Starts a plugin.
     *
     * @param plugin the plugin
     */
    private void start(Bundle plugin) {
        try {
            manager.start(plugin);
        } catch (Throwable exception) {
            log.warn("Failed to start bundle=" + BundleHelper.getName(plugin), exception);
            ErrorDialog.show(Messages.get("admin.system.plugin.start.title"),
                             Messages.format("admin.system.plugin.start.error", BundleHelper.getName(plugin),
                                             exception.getMessage()));
        }
    }

    /**
     * Stops a plugin.
     */
    private void onStop() {
        Bundle selected = plugins.getSelected();
        if (selected != null && selected.getState() != Bundle.RESOLVED
            && selected.getBundleId() != Constants.SYSTEM_BUNDLE_ID) {
            String title = Messages.get("admin.system.plugin.stop.title");
            String message = Messages.format("admin.system.plugin.stop.message", BundleHelper.getName(selected));
            ConfirmationDialog.show(title, message, ConfirmationDialog.YES_NO, new PopupDialogListener() {
                @Override
                public void onYes() {
                    stop(selected);
                }
            });
        } else {
            refresh();
        }
    }

    /**
     * Stops a plugin.
     *
     * @param plugin the plugin
     */
    private void stop(Bundle plugin) {
        try {
            manager.stop(plugin);
        } catch (Throwable exception) {
            log.warn("Failed to stop bundle=" + BundleHelper.getName(plugin), exception);
            ErrorDialog.show(Messages.get("admin.system.plugin.stop.title"),
                             Messages.format("admin.system.plugin.stop.error", BundleHelper.getName(plugin),
                                             exception.getMessage()));
        }
        refresh();
    }

    /**
     * Installs a plugin.
     */
    private void onInstall() {
        UploadListener listener = new AbstractUploadListener() {
            @Override
            public void fileUpload(UploadEvent uploadEvent) {
                File file = null;
                File dir = null;
                try {
                    dir = Files.createTempDirectory("openvpmsupload").toFile();
                    file = new File(dir, uploadEvent.getFileName());
                    try (FileOutputStream output = new FileOutputStream(file)) {
                        IOUtils.copyLarge(uploadEvent.getInputStream(), output);
                    }
                    manager.install(file);
                } catch (Throwable exception) {
                    ErrorHelper.show(exception);
                } finally {
                    if (file != null) {
                        if (file.delete()) {
                            if (!dir.delete()) {
                                log.warn("Failed to clean up temporary upload dir: " + dir);
                            }
                        } else {
                            log.warn("Failed to clean up temporary upload: " + file);
                        }
                    }
                }
                refresh();
            }
        };
        UploadDialog dialog = new UploadDialog(listener, getHelpContext());
        dialog.show();
    }

    /**
     * Uninstalls a plugin.
     */
    private void onUninstall() {
        Bundle selected = plugins.getSelected();
        if (selected != null && selected.getBundleId() != Constants.SYSTEM_BUNDLE_ID) {
            String title = Messages.get("admin.system.plugin.uninstall.title");
            String message = Messages.format("admin.system.plugin.uninstall.message", BundleHelper.getName(selected));
            ConfirmationDialog.show(title, message, ConfirmationDialog.YES_NO, new PopupDialogListener() {
                @Override
                public void onYes() {
                    uninstall(selected);
                }
            });
        } else {
            refresh();
        }
    }

    /**
     * Uninstall a plugin.
     *
     * @param plugin the plugin
     */
    private void uninstall(Bundle plugin) {
        try {
            manager.uninstall(plugin);
        } catch (Throwable exception) {
            log.warn("Failed to uninstall bundle=" + BundleHelper.getName(plugin), exception);
            ErrorDialog.show(Messages.get("admin.system.plugin.uninstall.title"),
                             Messages.format("admin.system.plugin.uninstall.error", BundleHelper.getName(plugin),
                                             exception.getMessage()));
        }
        refresh();
    }

    /**
     * Configures the plugin manager.
     */
    private void onConfigure() {
        PracticeService service = ServiceHelper.getBean(PracticeService.class);
        Party practice = service.getPractice();
        if (practice != null) {
            boolean enabled = service.pluginsEnabled();
            OptionDialog dialog = new OptionDialog(Messages.get("admin.system.plugin.configure.title"),
                                                   Messages.get("admin.system.plugin.enable.message"),
                                                   new String[]{Messages.get("admin.system.plugin.enable.yes"),
                                                                Messages.get("admin.system.plugin.enable.no")});
            dialog.setSelected(enabled ? 0 : 1);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    boolean newEnabled = dialog.getSelected() == 0;
                    if (enabled != newEnabled) {
                        configure(newEnabled, practice);
                    }
                }
            });
            dialog.show();
        }
    }

    /**
     * Configures the plugins.
     *
     * @param enable   if {@code true}, enable plugins, else disable them
     * @param practice the practice to update
     */
    private void configure(boolean enable, Party practice) {
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(practice);
        bean.setValue("enablePlugins", enable);
        bean.save();
        manager.stop();
        manager.start();
        refresh();
    }

    private static class PluginTableModel extends AbstractIMTableModel<Bundle> {

        private static final int ID_INDEX = 0;

        private static final int NAME_INDEX = ID_INDEX + 1;

        private static final int VERSION_INDEX = NAME_INDEX + 1;

        private static final int STATUS_INDEX = VERSION_INDEX + 1;

        PluginTableModel() {
            TableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(ID_INDEX, "admin.system.plugin.id"));
            model.addColumn(createTableColumn(NAME_INDEX, "admin.system.plugin.name"));
            model.addColumn(createTableColumn(VERSION_INDEX, "admin.system.plugin.version"));
            model.addColumn(createTableColumn(STATUS_INDEX, "admin.system.plugin.status"));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return null;
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Bundle object, TableColumn column, int row) {
            Object result = null;
            switch (column.getModelIndex()) {
                case ID_INDEX:
                    result = object.getBundleId();
                    break;
                case NAME_INDEX:
                    result = BundleHelper.getName(object);
                    break;
                case VERSION_INDEX:
                    result = object.getVersion().toString();
                    break;
                case STATUS_INDEX:
                    result = BundleHelper.getState(object);
                    break;
            }
            return result;
        }
    }
}
