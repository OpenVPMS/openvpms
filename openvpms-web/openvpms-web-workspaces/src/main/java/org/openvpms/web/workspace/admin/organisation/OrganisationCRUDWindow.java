/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.internal.sync.Change;
import org.openvpms.domain.internal.sync.DefaultChanges;
import org.openvpms.eftpos.internal.service.EFTPOSServices;
import org.openvpms.eftpos.internal.terminal.TerminalImpl;
import org.openvpms.eftpos.service.EFTPOSService;
import org.openvpms.eftpos.service.ManagedTerminalRegistrar;
import org.openvpms.eftpos.service.TerminalRegistrar;
import org.openvpms.eftpos.service.WebTerminalRegistrar;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.insurance.service.InsuranceService;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.eftpos.ManagedTerminalRegistrationDialog;
import org.openvpms.web.workspace.admin.eftpos.WebTerminalRegistrationDialog;

import java.util.List;

import static org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes.TERMINALS;
import static org.openvpms.archetype.rules.insurance.InsuranceArchetypes.INSURANCE_SERVICES;

/**
 * CRUD window for the Organisation workspace.
 *
 * @author Tim Anderson
 */
public class OrganisationCRUDWindow extends ResultSetCRUDWindow<Entity> {

    /**
     * Button identifier to register an EFTPOS terminal.
     */
    private static final String REGISTER_ID = "button.register";

    /**
     * Synchronise button identifier.
     */
    private static final String SYNC_DATA = "button.sync";

    /**
     * Constructs an {@link OrganisationCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param query      the query. May be {@code null}
     * @param set        the result set. May be {@code null}
     * @param context    the context
     * @param help       the help context
     */
    public OrganisationCRUDWindow(Archetypes<Entity> archetypes, Query<Entity> query, ResultSet<Entity> set,
                                  Context context, HelpContext help) {
        super(archetypes, query, set, context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(REGISTER_ID, this::onRegister);
        buttons.add(SYNC_DATA, this::synchroniseData);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(REGISTER_ID, enable && TypeHelper.isA(getObject(), TERMINALS));
        buttons.setEnabled(SYNC_DATA, enable && TypeHelper.isA(getObject(), INSURANCE_SERVICES));
    }

    /**
     * Synchronises service data.
     */
    protected void synchroniseData() {
        Entity object = getObject();
        if (TypeHelper.isA(object, INSURANCE_SERVICES)) {
            synchroniseInsurers(object);
        }
    }

    /**
     * Synchronises insurers associated with an insurance service.
     *
     * @param object the service configuration
     */
    protected void synchroniseInsurers(Entity object) {
        InsuranceServices insuranceServices = ServiceHelper.getBean(InsuranceServices.class);
        InsuranceService service = insuranceServices.getServiceForConfiguration(object);
        DefaultChanges<Party> changes = new DefaultChanges<>();
        service.synchroniseInsurers(changes);
        List<Change<Party>> list = changes.getChanges();
        if (list.isEmpty()) {
            InformationDialog.show(Messages.get("admin.organisation.insurer.sync.title"),
                                   Messages.format("admin.organisation.insurer.sync.nochanges",
                                                   object.getName()));
        } else {
            InsurerChanges popup = new InsurerChanges(list);
            popup.show();
        }
    }

    /**
     * Registers an EFTPOS terminal.
     */
    private void onRegister() {
        Entity config = IMObjectHelper.reload(getObject());
        if (config != null) {
            String title = Messages.get("admin.eftpos.register.title");
            try {
                Terminal terminal = new TerminalImpl(config, ServiceHelper.getArchetypeService());
                EFTPOSServices eftposServices = ServiceHelper.getBean(EFTPOSServices.class);
                EFTPOSService service = eftposServices.getService(terminal);
                TerminalStatus status = service.getTerminalStatus(terminal);
                if (status.isError()) {
                    ErrorDialog.show(title, status.getMessage());
                } else if (status.isRegistrationRequired()) {
                    if (status.canRegister()) {
                        register(service, terminal, title, config);
                    } else {
                        InformationDialog.show(title, Messages.get("admin.eftpos.register.registerexternal"));
                    }
                } else if (status.canRegister()) {
                    if (status.isRegistered()) {
                        ConfirmationDialog.show(title, Messages.get("admin.eftpos.register.reregister"),
                                                ConfirmationDialog.YES_NO, new PopupDialogListener() {
                                    @Override
                                    public void onYes() {
                                        register(service, terminal, title, config);
                                    }
                                });
                    } else {
                        register(service, terminal, title, config);
                    }
                } else if (status.isRegistered()) {
                    InformationDialog.show(title, Messages.get("admin.eftpos.register.alreadyregistered"));
                } else {
                    // not registered, and can't register, but registration apparently not required. Probably an error
                    InformationDialog.show(title, Messages.get("admin.eftpos.register.registerexternal"));
                }
            } catch (Exception exception) {
                ErrorHelper.show(title, exception);
            }
        }
    }

    /**
     * Registers a terminal.
     *
     * @param service  the EFTPOS service
     * @param terminal the terminal to register
     * @param title    the dialog title
     * @param config   the terminal configuration
     */
    private void register(EFTPOSService service, Terminal terminal, String title, Entity config) {
        try {
            TerminalRegistrar registration = service.register(terminal);
            if (registration instanceof WebTerminalRegistrar) {
                WebTerminalRegistrationDialog dialog
                        = new WebTerminalRegistrationDialog(terminal, (WebTerminalRegistrar) registration);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onClose(WindowPaneEvent event) {
                        onRefresh(config);
                    }
                });
                dialog.show();
            } else if (registration instanceof ManagedTerminalRegistrar) {
                ManagedTerminalRegistrationDialog dialog = new ManagedTerminalRegistrationDialog(
                        (ManagedTerminalRegistrar) registration, getHelpContext());
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onClose(WindowPaneEvent event) {
                        onRefresh(config);
                    }
                });
                dialog.show();
            } else {
                onRefresh(config);
            }
        } catch (Exception exception) {
            ErrorHelper.show(title, exception);
        }
    }

}
