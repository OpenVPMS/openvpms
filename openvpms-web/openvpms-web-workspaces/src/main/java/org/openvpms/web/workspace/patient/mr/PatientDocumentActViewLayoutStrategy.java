/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openvpms.web.workspace.patient.mr;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.PropertySet;

/**
 * Layout strategy for viewing <em>act.patientDocument*</em> acts.
 *
 * @author benjamincharlton
 */
public class PatientDocumentActViewLayoutStrategy extends PatientDocumentActLayoutStrategy {

    /**
     * The nodes to display.
     */
    private static final ArchetypeNodes VIEW_NODES = new ArchetypeNodes(EDIT_NODES).excludeIfEmpty(VERSIONS);

    /**
     * Constructs a {@link PatientDocumentActViewLayoutStrategy}
     */
    public PatientDocumentActViewLayoutStrategy() {
        super();
        setArchetypeNodes(VIEW_NODES);
    }

    /**
     * Apply the layout strategy.
     * <p/>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        addDocumentViewers((DocumentAct) object, properties, context);
        return super.apply(object, properties, parent, context);
    }

}
