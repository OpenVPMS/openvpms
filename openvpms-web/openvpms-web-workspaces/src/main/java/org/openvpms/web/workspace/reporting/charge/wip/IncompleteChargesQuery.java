/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.wip;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.NodeSortConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.query.AbstractQueryState;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.QueryState;
import org.openvpms.web.workspace.reporting.charge.AbstractChargesQuery;

import java.util.Date;


/**
 * Query for incomplete charges. i.e invoices, credit and counter acts that
 * are IN_PROGRESS, COMPLETE, or ON_HOLD.
 *
 * @author Tim Anderson
 */
public class IncompleteChargesQuery extends AbstractChargesQuery {

    /**
     * The act statuses, excluding POSTED.
     */
    private static final ActStatuses STATUSES = new ActStatuses(CustomerAccountArchetypes.INVOICE, ActStatus.POSTED);

    /**
     * The default sort constraint.
     */
    private static final SortConstraint[] DEFAULT_SORT = {new NodeSortConstraint("customer")};

    /**
     * Constructs an {@link IncompleteChargesQuery}.
     *
     * @param context the context
     */
    public IncompleteChargesQuery(LayoutContext context) {
        super(STATUSES, context.getContext().getLocation(), context);
        setDefaultSortConstraint(DEFAULT_SORT);
        setAuto(true);
    }

    /**
     * Returns the query state.
     *
     * @return the query state
     */
    @Override
    public QueryState getQueryState() {
        return new Memento(this);
    }

    /**
     * Sets the query state.
     *
     * @param state the query state
     */
    @Override
    public void setQueryState(QueryState state) {
        if (state instanceof Memento) {
            Memento memento = (Memento) state;
            setShortName(memento.archetype);
            setStatus(memento.status);
            getDateRange().setAllDates(memento.all);
            getDateRange().setFrom(memento.from);
            getDateRange().setTo(memento.to);
            setLocation(memento.location);
        }
    }

    private static class Memento extends AbstractQueryState {

        private final String archetype;

        private final String status;

        private final boolean all;

        private final Date from;

        private final Date to;

        private final Party location;

        /**
         * Constructs a {@link Memento}.
         *
         * @param query the query
         */
        public Memento(IncompleteChargesQuery query) {
            super(query);
            this.archetype = query.getShortName();
            LookupField status = query.getStatusSelector();
            this.status = (status != null) ? status.getSelectedCode() : null;
            this.all = query.getAllDates();
            this.from = query.getFrom();
            this.to = query.getTo();
            this.location = query.getLocation();
        }
    }
}
