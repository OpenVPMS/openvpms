/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.im.delete.ActDeletionHandler;
import org.openvpms.web.component.im.delete.Deletable;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.resource.i18n.Messages;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * A deletion handler for <em>act.customerAccountPayment</em> and <em>act.customerAccountRefund</em>
 * that prevents deletion if:
 * <ul>
 *     <li>the act is posted; or</li>
 *     <li>has an APPROVED or NO_TRANSACTION EFTPOS transaction; or</li>
 *     <li>has an outstanding EFTPOS transaction</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class PaymentDeletionHandler extends ActDeletionHandler<FinancialAct> {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * Constructs a {@link PaymentDeletionHandler}.
     *
     * @param object             the act to delete
     * @param factory            the editor factory
     * @param transactionManager the transaction manager
     * @param service            the archetype service
     */
    public PaymentDeletionHandler(FinancialAct object, IMObjectEditorFactory factory,
                                  PlatformTransactionManager transactionManager, IArchetypeRuleService service,
                                  CustomerAccountRules rules) {
        super(object, factory, transactionManager, service);
        this.rules = rules;
    }

    /**
     * Determines if the object can be deleted.
     *
     * @return {@link Deletable#yes()} if the object can be deleted, otherwise {@link Deletable#no}
     */
    @Override
    public Deletable getDeletable() {
        Deletable result = super.getDeletable();
        if (result.canDelete()) {
            FinancialAct object = getObject();
            if (rules.hasApprovedEFTPOSTransaction(object)) {
                result = Deletable.no(Messages.format("customer.payment.delete.approvedEFT", getDisplayName()));
            } else if (rules.hasOutstandingEFTPOSTransaction(object, true)) {
                result = Deletable.no(Messages.format("customer.payment.delete.outstandingEFT", getDisplayName()));
            }
        }
        return result;
    }
}
