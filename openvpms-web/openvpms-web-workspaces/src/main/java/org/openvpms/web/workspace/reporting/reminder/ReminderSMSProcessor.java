/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.party.ContactMatcher;
import org.openvpms.archetype.rules.party.SMSMatcher;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderType.GroupBy;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.openvpms.web.component.service.SimpleSMSService;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.openvpms.web.workspace.reporting.ReportingException;

import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.FailedToProcessReminder;
import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.SMSDisabled;
import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.SMSMessageEmpty;
import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.SMSMessageTooLong;
import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.TemplateMissingSMSText;


/**
 * Sends reminders via SMS.
 *
 * @author Tim Anderson
 */
public class ReminderSMSProcessor extends GroupedReminderProcessor<SMSReminders> {

    /**
     * The SMS service.
     */
    private final SimpleSMSService smsService;

    /**
     * The template evaluator.
     */
    private final ReminderSMSEvaluator evaluator;

    /**
     * Determines if SMS enabled. If not, any reminder will have an error logged against it.
     */
    private final boolean smsEnabled;

    /**
     * Constructs a {@link ReminderSMSProcessor}.
     *
     * @param smsService    the SMS service
     * @param evaluator     the SMS template evaluator
     * @param reminderTypes the reminder types
     * @param practice      the practice
     * @param reminderRules the reminder rules
     * @param patientRules  the patient rules
     * @param service       the archetype service
     * @param config        the reminder configuration
     * @param actionFactory the action factory
     */
    public ReminderSMSProcessor(SimpleSMSService smsService, ReminderSMSEvaluator evaluator,
                                ReminderTypes reminderTypes, Party practice, ReminderRules reminderRules,
                                PatientRules patientRules, ArchetypeService service, ReminderConfiguration config,
                                ActionFactory actionFactory) {
        super(reminderTypes, reminderRules, patientRules, practice, service, config, null, actionFactory);
        this.smsService = smsService;
        this.evaluator = evaluator;
        smsEnabled = smsService.isEnabled();
    }

    /**
     * Returns the reminder item archetype that this processes.
     *
     * @return the archetype
     */
    @Override
    public String getArchetype() {
        return ReminderArchetypes.SMS_REMINDER;
    }

    /**
     * Processes reminders.
     *
     * @param reminders the reminders
     */
    @Override
    public void process(SMSReminders reminders) {
        if (!smsEnabled) {
            throw new ReportingException(SMSDisabled);
        }
        String phoneNumber = reminders.getPhoneNumber();
        try {
            Party practice = getPractice();
            String text = reminders.getText(practice);
            if (StringUtils.isEmpty(text)) {
                throw new ReportingException(SMSMessageEmpty, reminders.getSMSTemplate().getName());
            } else {
                int parts = smsService.getParts(text);
                int maxParts = smsService.getMaxParts();
                if (parts > maxParts) {
                    throw new ReportingException(SMSMessageTooLong, reminders.getSMSTemplate().getName(), parts,
                                                 maxParts);
                }
            }
            Party patient = null;
            String notes = null;
            String subject = Messages.get("reminder.log.sms.subject");
            ReminderEvent event = reminders.getFirst();
            if (event != null) {
                patient = event.getPatient();
                notes = getNote(event);
            }
            smsService.send(phoneNumber, text, reminders.getCustomer(), patient, null, subject, COMMUNICATION_REASON,
                            notes, reminders.getLocation());
        } catch (OpenVPMSException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new ReportingException(FailedToProcessReminder, exception, exception.getMessage());
        }
    }

    /**
     * Determines if reminder processing is performed asynchronously.
     *
     * @return {@code true} if reminder processing is performed asynchronously
     */
    @Override
    public boolean isAsynchronous() {
        return false;
    }

    /**
     * Returns the contact to use.
     *
     * @param customer       the reminder
     * @param matcher        the contact matcher
     * @param defaultContact the default contact, or {@code null} to select one from the customer
     * @return the contact, or {@code null} if none is found
     */
    @Override
    protected Contact getContact(Party customer, ContactMatcher matcher, Contact defaultContact) {
        Contact contact = super.getContact(customer, matcher, defaultContact);
        if (contact != null && StringUtils.isEmpty(SMSHelper.getPhone(contact))) {
            contact = null;
        }
        return contact;
    }

    /**
     * Logs reminder communications.
     * <p/>
     * For SMS, this is a no-op, as the SMS message is the log.
     *
     * @param reminders  the reminder state
     * @param logger the communication logger
     */
    @Override
    protected void log(PatientReminders reminders, CommunicationLogger logger) {
        // do nothing.
    }

    /**
     * Prepares reminders for processing.
     * <p>
     * This:
     * <ul>
     * <li>filters out any reminders that can't be processed due to missing data</li>
     * <li>adds meta-data for subsequent calls to {@link #process}</li>
     * </ul>
     *
     * @param reminders the reminders to prepare
     * @throws ReportingException if the reminders cannot be prepared
     */
    @Override
    protected void prepare(SMSReminders reminders) {
        super.prepare(reminders);
        DocumentTemplate template = reminders.getTemplate();
        if (template != null && reminders.canSend()) {
            Entity smsTemplate = template.getSMSTemplate();
            if (smsTemplate == null) {
                throw new ReportingException(TemplateMissingSMSText, template.getName());
            }
            reminders.setSMSTemplate(smsTemplate);
        }
    }

    /**
     * Creates a contact matcher to locate the contact to send to.
     *
     * @return a new contact matcher
     */
    @Override
    protected ContactMatcher createContactMatcher() {
        return new SMSMatcher(CONTACT_PURPOSE, false, getService());
    }

    /**
     * Returns the contact archetype.
     *
     * @return the contact archetype
     */
    @Override
    protected String getContactArchetype() {
        return ContactArchetypes.PHONE;
    }

    /**
     * Creates a {@link PatientReminders}.
     *
     * @param groupBy the reminder grouping policy. This determines which document template is selected
     * @param resend  if {@code true}, reminders are being resent
     * @return a new {@link PatientReminders}
     */
    @Override
    protected SMSReminders createPatientReminders(GroupBy groupBy, boolean resend) {
        return new SMSReminders(groupBy, resend, evaluator);
    }
}
