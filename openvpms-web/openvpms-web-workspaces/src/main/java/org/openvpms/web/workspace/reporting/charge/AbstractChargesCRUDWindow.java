/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge;

import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.component.im.edit.IMObjectActions;
import org.openvpms.web.component.im.print.IMObjectReportPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.print.InteractiveIMPrinter;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;

/**
 * Abstract CRUD window for charges.
 *
 * @author Tim Anderson
 */
public abstract class AbstractChargesCRUDWindow extends ResultSetCRUDWindow<FinancialAct> {

    /**
     * The  query browser.
     */
    private final QueryBrowser<FinancialAct> browser;

    /**
     * The report type.
     */
    private final String reportType;

    /**
     * Constructs an {@link AbstractChargesCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param browser    the query browser
     * @param reportType the report type
     * @param context    the context
     * @param help       the help context
     */
    public AbstractChargesCRUDWindow(Archetypes<FinancialAct> archetypes, QueryBrowser<FinancialAct> browser,
                                     String reportType, Context context, HelpContext help) {
        super(archetypes, Actions.INSTANCE, browser.getQuery(), null, LocalContext.copy(context), help);
        // use a copy of the context, and clear the customer and patient. The customer will reflect the selected
        // object
        Context local = getContext();
        local.setCustomer(null);
        local.setPatient(null);
        setMailContext(new CustomerMailContext(local, help));
        this.reportType = reportType;
        this.browser = browser;
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(FinancialAct object) {
        super.setObject(object);
        Party customer = null;
        if (object != null) {
            IMObjectBean bean = getBean(object);
            if (bean.hasNode("customer")) {
                customer = bean.getTarget("customer", Party.class);
            }
        }
        getContext().setCustomer(customer);
    }

    /**
     * Returns the customer of the selected object.
     *
     * @return the customer. May be {@code null}
     */
    public Party getCustomer() {
        return getContext().getCustomer();
    }

    /**
     * Views the selected object.
     */
    @Override
    public void view() {
        setResultSet(browser.getResultSet());
        super.view();
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button set
     */
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(createViewButton());
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
        buttons.add("report", this::onReport);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        buttons.setEnabled(VIEW_ID, enable);
        buttons.setEnabled(PRINT_ID, enable);
        buttons.setEnabled(MAIL_ID, enable);
    }

    /**
     * Invoked when the 'Report' button is pressed.
     */
    private void onReport() {
        Context context = getContext();
        DocumentTemplateLocator locator = new ContextDocumentTemplateLocator(reportType, context);
        IMPrinterFactory printerFactory = ServiceHelper.getBean(IMPrinterFactory.class);

        IMObjectReportPrinter<FinancialAct> printer = printerFactory.createIMObjectReportPrinter(
                getQuery(), locator, context);
        InteractiveIMPrinter<FinancialAct> iPrinter = new InteractiveIMPrinter<>(
                Messages.get("reporting.charge.print"), printer, context, getHelpContext());
        iPrinter.setMailContext(getMailContext());
        iPrinter.print();
    }

    private static class Actions extends ActActions<FinancialAct> {

        public static final IMObjectActions<FinancialAct> INSTANCE = new Actions();

        /**
         * Determines if objects can be created.
         *
         * @return {@code false}
         */
        @Override
        public boolean canCreate() {
            return false;
        }

        /**
         * Determines if an act can be edited.
         *
         * @param act the act to check
         * @return {@code true} if the act status isn't {@code POSTED}
         */
        @Override
        public boolean canEdit(FinancialAct act) {
            return false;
        }

        /**
         * Determines if an object can be deleted.
         *
         * @param object the object to check
         * @return {@code true} if the object can be deleted
         */
        @Override
        public boolean canDelete(FinancialAct object) {
            return false;
        }
    }
}

