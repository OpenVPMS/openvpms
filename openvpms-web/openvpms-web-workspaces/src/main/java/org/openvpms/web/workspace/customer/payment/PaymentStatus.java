/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.edit.PropertyEditor;
import org.openvpms.web.component.im.lookup.DefaultLookupPropertyEditor;
import org.openvpms.web.component.im.lookup.LookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.LookupPropertyEditor;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;

import java.util.Objects;

/**
 * Used to manage the payment/refund status.
 * <p/>
 * By default, payments/refunds are POSTED so cannot be changed once Apply or OK is pressed.
 * <p/>
 * However, EFTPOS transactions require that the payment/refund is saved prior to the transaction going ahead,
 * which means they cannot be POSTED until the transaction is completed.
 * <p/>
 * This class allows the status to be displayed as POSTED, but managed internally as IN_PROGRESS so that the user
 * doesn't need to manually finalise the transaction on completion of EFTPOS transactions.
 * <p/>
 * The user is still able to change the status to IN_PROGRESS.
 * <p/>
 * NOTE: if an act has been saved and is POSTED, the payment status is treated as read-only, even if the status
 * was updated subsequent to it being saved.
 *
 * @author Tim Anderson
 */
public class PaymentStatus {

    /**
     * The real status property.
     */
    private final Property status;

    /**
     * Determines if the status is read-only.
     */
    private final boolean readOnly;

    /**
     * The property that provides the user view of the status.
     */
    private final SimpleProperty view;

    /**
     * The property editor, bound to the view property.
     */
    private final LookupPropertyEditor editor;

    /**
     * The listener for status property updates.
     */
    private ModifiableListener statusListener;

    /**
     * Determines if the status should be set to POSTED on completion of editing.
     */
    private boolean postOnCompletion = false;

    /**
     * Constructs a {@link PaymentStatus}.
     *
     * @param status the status property
     * @param parent the parent act
     */
    public PaymentStatus(Property status, FinancialAct parent) {
        this.status = status;
        readOnly = isPosted() && !parent.isNew();
        if (!readOnly) {
            statusListener = modifiable -> onPropertyModified();
            view = new SimpleProperty(status.getName(), status.getValue(), status.getType(),
                                      status.getDisplayName());
            NodeLookupQuery query = new NodeLookupQuery(parent, status);
            LookupField field = LookupFieldFactory.create(view, query);
            editor = new DefaultLookupPropertyEditor(view, field);

            this.status.addModifiableListener(statusListener);
            view.addModifiableListener(modifiable -> onViewModified());
        } else {
            editor = null;
            view = null;
        }
    }

    /**
     * Determines if the current status is {@link ActStatus#POSTED POSTED}.
     *
     * @return {@code true} if the current status is POSTED.
     */
    public boolean isPosted() {
        return ActStatus.POSTED.equals(status.getString());
    }

    /**
     * If the current status is POSTED, sets it to IN_PROGRESS, and indicate that it should be set to POSTED on
     * completion of editing. This enables the payment to be saved without affecting the balance (once POSTED,
     * totals cannot change).
     * <p/>
     * The view will still indicated POSTED.
     * <p/>
     *
     * @throws IllegalStateException if the status is read-only
     */
    public void makeSaveableAndPostOnCompletion() {
        if (readOnly) {
            throw new IllegalStateException("Cannot update status of POSTED act");
        }
        if (isPosted()) {
            postOnCompletion = true;
            try {
                status.removeModifiableListener(statusListener);
                status.setValue(FinancialActStatus.IN_PROGRESS);
            } finally {
                status.addModifiableListener(statusListener);
            }
        }
    }

    /**
     * Returns the status editor.
     *
     * @return the status editor, or {@code null} if the status cannot be edited
     */
    public PropertyEditor getEditor() {
        return editor;
    }

    /**
     * Determines if the act should be posted on completion of editing.
     *
     * @return {@code true} if the act should be posted on completion, otherwise {@code false}
     */
    public boolean postOnCompletion() {
        return postOnCompletion && !isPosted();
    }

    /**
     * Determine if the status is read-only.
     *
     * @return {@code true} if the act was previously saved with POSTED status, otherwise {@code false}
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Invoked when the status property is modified. Updates the view.
     */
    private void onPropertyModified() {
        String value = status.getString();
        if (!Objects.equals(value, view.getString())) {
            view.setValue(value);
        }
    }

    /**
     * Invoked when the view property is modified. Updates the status.
     * <p/>
     * As the user has explicitly changed the status, this turns off {@code postOnCompletion}.
     */
    private void onViewModified() {
        postOnCompletion = false;
        String value = view.getString();
        if (!Objects.equals(value, status.getString())) {
            status.setValue(value);
        }
    }
}
