/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Table;
import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.IMObjectTableBrowser;
import org.openvpms.web.component.im.sms.SMSReplyUpdater;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.EvenOddTableCellRenderer;
import org.openvpms.web.system.ServiceHelper;

/**
 * Browser for <act>act.smsReply</act>.
 *
 * @author Tim Anderson
 */
public class SMSReplyBrowser extends IMObjectTableBrowser<Act> {

    /**
     * Determines if the user has authority to update the status of a reply.
     */
    private final boolean canUpdateStatus;

    /**
     * The reply updater.
     */
    private final SMSReplyUpdater replyUpdater;

    /**
     * Constructs a {@link SMSReplyBrowser}.
     *
     * @param query         the query
     * @param layoutContext the layout context
     */
    public SMSReplyBrowser(SMSReplyQuery query, DefaultLayoutContext layoutContext) {
        super(query, null, new TopLevelSMSReplyTableModel(layoutContext), layoutContext);
        UserRules rules = ServiceHelper.getBean(UserRules.class);
        canUpdateStatus = rules.canSave(layoutContext.getContext().getUser(), SMSArchetypes.REPLY);
        replyUpdater = ServiceHelper.getBean(SMSReplyUpdater.class);
    }

    /**
     * Notifies listeners when an object is selected.
     * <p/>
     * This marks any <em>PENDING</em> message as <em>READ</em>.
     *
     * @param selected the selected object
     */
    @Override
    protected void notifySelected(Act selected) {
        markRead(selected);
        super.notifySelected(selected);
    }

    /**
     * Creates a new paged table.
     *
     * @param model the table model
     * @return a new paged table
     */
    @Override
    protected PagedIMTable<Act> createTable(IMTableModel<Act> model) {
        PagedIMTable<Act> table = super.createTable(model);
        if (canUpdateStatus) {
            table.getTable().setDefaultRenderer(Object.class, new EvenOddTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
                    Component result = super.getTableCellRendererComponent(table, value, column, row);
                    if (MessageStatus.PENDING.equals(model.getObjects().get(row).getStatus())) {
                        if (result instanceof Label) {
                            result.setStyleName(Styles.BOLD);
                        }
                    }
                    return result;
                }
            });
        }
        return table;
    }

    /**
     * Invoked when a message is selected.
     * <p/>
     * This updates the act status to <em>READ</em> if it is <em>PENDING</em>, and the current user can change the
     * status.
     *
     * @param message the selected object
     */
    private void markRead(Act message) {
        if (canUpdateStatus && MessageStatus.PENDING.equals(message.getStatus())) {
            replyUpdater.markRead(message);
            Act select = getSelected();
            getTable().getTable().getModel().refresh();
            setSelected(select);
        }
    }
}
