/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement.reminder;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.UpdatableQueryIterator;

/**
 * Iterator over results returned by a query created using {@link AccountReminderQueryFactory}.
 *
 * @author Tim Anderson
 */
class AccountReminderQueryIterator extends UpdatableQueryIterator<ObjectSet> {

    /**
     * Constructs an {@link AccountReminderQueryIterator}.
     *
     * @param query    the query
     * @param pageSize the page size
     * @param service  the archetype service
     */
    protected AccountReminderQueryIterator(ArchetypeQuery query, int pageSize, IArchetypeService service) {
        super(query, pageSize, service);
    }

    /**
     * Returns the next page.
     *
     * @param query   the query
     * @param service the archetype service
     * @return the next page
     */
    @Override
    protected IPage<ObjectSet> getNext(ArchetypeQuery query, IArchetypeService service) {
        return service.getObjects(query);
    }

    /**
     * Returns a unique identifier for the object.
     *
     * @param object the object
     * @return a unique identifier for the object
     */
    @Override
    protected long getId(ObjectSet object) {
        Act reminder = (Act) object.get("reminder");
        return reminder.getId();
    }
}
