/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.statement.Statement;
import org.openvpms.archetype.rules.finance.statement.StatementProcessorException;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.report.ReportContextFactory;
import org.openvpms.web.component.im.report.Reporter;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.im.report.TemplatedReporter;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.Mailer;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.openvpms.web.workspace.reporting.email.LocationMailerFactory;

import java.util.List;

import static org.openvpms.archetype.rules.finance.statement.StatementProcessorException.ErrorCode.FailedToProcessStatement;
import static org.openvpms.archetype.rules.finance.statement.StatementProcessorException.ErrorCode.InvalidConfiguration;
import static org.openvpms.archetype.rules.finance.statement.StatementProcessorException.ErrorCode.NoContact;


/**
 * Sends statement emails.
 *
 * @author Tim Anderson
 */
public class StatementEmailProcessor extends AbstractStatementProcessorListener {

    /**
     * The mailer factory.
     */
    private final LocationMailerFactory mailerFactory;

    /**
     * The email template evaluator.
     */
    private final EmailTemplateEvaluator evaluator;

    /**
     * The reporter factory.
     */
    private final ReporterFactory reporterFactory;

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The practice location to use, if a customer doesn't have a preferred location.
     */
    private final Party location;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The statement document template.
     */
    private final DocumentTemplate statementTemplate;

    /**
     * The statement email template.
     */
    private final EmailTemplate emailTemplate;

    /**
     * Constructs a {@link StatementEmailProcessor}.
     *
     * @param mailerFactory    the reporter factory
     * @param evaluator        the email template evaluator
     * @param reporterFactory  the reporter factory
     * @param practice         the practice
     * @param location         the practice location to use, if a customer doesn't have a preferred location
     * @param logger           the communication logger. May be {@code null}
     * @param passwordResolver the mail server password resolver
     * @throws ArchetypeServiceException   for any archetype service error
     * @throws StatementProcessorException for any statement processor error
     */
    public StatementEmailProcessor(MailerFactory mailerFactory, EmailTemplateEvaluator evaluator,
                                   ReporterFactory reporterFactory, Party practice, Party location,
                                   PracticeRules practiceRules, IArchetypeService service, CommunicationLogger logger,
                                   MailPasswordResolver passwordResolver) {
        super(practice, logger);
        this.mailerFactory = new LocationMailerFactory(practice, practiceRules, service, mailerFactory, "BILLING",
                                                       passwordResolver);
        this.evaluator = evaluator;
        this.reporterFactory = reporterFactory;
        this.practice = practice;
        this.location = location;
        this.service = service;
        Entity entity = getStatementTemplate(service);
        statementTemplate = new DocumentTemplate(entity, service);
        emailTemplate = statementTemplate.getEmailTemplate();
        if (emailTemplate == null) {
            throw new StatementProcessorException(InvalidConfiguration, "No email document template configured");
        }
    }

    /**
     * Processes a statement.
     *
     * @param statement the event to process
     * @throws OpenVPMSException for any error
     */
    public void process(Statement statement) {
        try {
            Party customer = statement.getCustomer();
            Party location = getLocation(customer);

            Context context = new LocalContext();
            context.setCustomer(customer);
            context.setLocation(location);
            context.setPractice(practice);

            Mailer mailer = mailerFactory.create(location, context);
            List<Contact> contacts = statement.getContacts();
            if (contacts.isEmpty()) {
                throw new StatementProcessorException(NoContact, customer);
            }
            Contact contact = contacts.get(0);
            IMObjectBean bean = service.getBean(contact);
            if (!bean.isA(ContactArchetypes.EMAIL)) {
                throw new StatementProcessorException(NoContact, customer);
            }
            String to = bean.getString("emailAddress");
            String[] toAddresses = {to};
            mailer.setTo(toAddresses);
            String subject = evaluator.getSubject(emailTemplate, customer, context);
            String text = evaluator.getMessage(emailTemplate, customer, context);
            mailer.setSubject(subject);
            mailer.setBody(text);
            Iterable<IMObject> objects = getActs(statement);
            Reporter<?> reporter = reporterFactory.create(objects, statementTemplate, TemplatedReporter.class);
            reporter.setParameters(getParameters(statement));
            reporter.setFields(ReportContextFactory.create(context));
            Document doc = reporter.getDocument(DocFormats.PDF_TYPE, true);
            mailer.addAttachment(doc);

            // add any attachments associated with the email template, ordered on template name.
            // This assumes that the template and document names are similar.
            for (DocumentTemplate template : emailTemplate.getAttachments(true)) {
                Document document = template.getDocument();
                if (document != null) {
                    mailer.addAttachment(document);
                }
            }

            mailer.send();
            if (!statement.isPreview() && !statement.isPrinted()) {
                setPrinted(statement);
            }

            CommunicationLogger logger = getLogger();
            if (logger != null) {
                logger.logEmail(customer, null, mailer.getFrom(), toAddresses, null, null, subject,
                                COMMUNICATION_REASON, text, null, doc.getName(), location);
            }
        } catch (ArchetypeServiceException | StatementProcessorException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new StatementProcessorException(exception, FailedToProcessStatement, exception.getMessage());
        }
    }

    /**
     * Returns the statement template.
     *
     * @param service the archetype service
     * @return the statement template
     * @throws StatementProcessorException if there is no template configured
     */
    protected Entity getStatementTemplate(IArchetypeService service) {
        TemplateHelper helper = new TemplateHelper(service);
        Entity entity = helper.getTemplateForType(CustomerAccountArchetypes.OPENING_BALANCE);
        if (entity == null) {
            throw new StatementProcessorException(InvalidConfiguration, "No document template configured");
        }
        return entity;
    }

    /**
     * Returns the practice location to use when sending reminders to a customer.
     *
     * @param customer the customer
     * @return the customer's preferred practice location, or the location passed at construction, if there is none
     */
    protected Party getLocation(Party customer) {
        Party result = service.getBean(customer).getTarget("practice", Party.class);
        return result != null ? result : location;
    }

    /**
     * Helper to return the statement acts as an Iterable<IMObject>.
     *
     * @param event the statement event
     * @return the statement acts
     */
    @SuppressWarnings("unchecked")
    private Iterable<IMObject> getActs(Statement event) {
        return (Iterable<IMObject>) (Iterable<?>) event.getActs();
    }

}
