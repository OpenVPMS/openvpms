/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.edit.DefaultIMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.appointment.reminder.AppointmentReminderEvaluator;

import java.util.Date;

import static org.openvpms.archetype.rules.workflow.ScheduleArchetypes.APPOINTMENT;

/**
 * A component to test the expression evaluation of an <em>entity.documentTemplateSMSAppointment</em>.
 *
 * @author Tim Anderson
 */
public class SMSAppointmentTemplateSampler extends CustomerPatientSMSTemplateSampler {

    /**
     * The template evaluator.
     */
    private final AppointmentReminderEvaluator evaluator;

    /**
     * The appointment type to test against.
     */
    private final Property appointmentType;


    /**
     * Constructs an {@link SMSAppointmentTemplateSampler}.
     *
     * @param template      the template
     * @param layoutContext the layout context
     */
    public SMSAppointmentTemplateSampler(Entity template, LayoutContext layoutContext) {
        super(template, layoutContext);
        ArchetypeService service = ServiceHelper.getArchetypeService();
        evaluator = ServiceHelper.getBean(AppointmentReminderEvaluator.class);

        appointmentType = SimpleProperty.newProperty()
                .name("appointmentType")
                .displayName(DescriptorHelper.getDisplayName(APPOINTMENT, "appointmentType", service))
                .type(Reference.class)
                .archetypeRange(ScheduleArchetypes.APPOINTMENT_TYPE)
                .build();
        appointmentType.addModifiableListener(modifiable -> evaluate());
    }

    /**
     * Lays out the editable fields in a grid.
     *
     * @param grid    the grid
     * @param group   the focus group
     * @param context the layout context
     */
    @Override
    protected void layoutFields(ComponentGrid grid, FocusGroup group, LayoutContext context) {
        super.layoutFields(grid, group, context);
        IMObjectReferenceEditor<Entity> selector = new DefaultIMObjectReferenceEditor<>(appointmentType, null, context);
        Label label = LabelFactory.text(appointmentType.getDisplayName());
        grid.add(label, selector.getComponent());
        group.add(selector.getFocusGroup());
    }

    /**
     * Evaluates the template.
     *
     * @param template the template
     * @param context  the context
     * @return the result of the evaluation. May be {@code null}
     */
    @Override
    protected String evaluate(Entity template, Context context) {
        Act act = (Act) IMObjectCreator.create(APPOINTMENT);
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(act);
        bean.setTarget("customer", getCustomer());
        bean.setTarget("patient", getPatient());
        bean.setTarget("appointmentType", appointmentType.getReference());
        act.setActivityStartTime(new Date());
        act.setActivityEndTime(new Date());
        return evaluator.evaluate(template, act, context.getLocation(), context.getPractice());
    }
}
