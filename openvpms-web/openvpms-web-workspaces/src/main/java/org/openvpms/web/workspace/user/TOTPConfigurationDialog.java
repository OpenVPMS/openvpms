/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.user;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.ImageReference;
import nextapp.echo2.app.Label;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.help.HelpTopics;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.image.ThumbnailImageReference;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.security.login.MfaService;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;

/**
 * Dialog to configure a TOTP secret for a user.
 *
 * @author Tim Anderson
 */
class TOTPConfigurationDialog extends ModalDialog {

    /**
     * The user to set up the secret for.
     */
    private final User user;

    /**
     * The multifactor authentication service.
     */
    private final MfaService service;

    /**
     * Component container.
     */
    private final Column container = ColumnFactory.create(Styles.WIDE_CELL_SPACING);

    /**
     * New TOTP button identifier.
     */
    private static final String NEW_TOTP = "button.newTOTP";

    /**
     * Disable TOTP button identifier.
     */
    private static final String DISABLE_TOTP = "button.disableTOTP";

    /**
     * The dialog buttons.
     */
    private static final String[] BUTTONS = {NEW_TOTP, DISABLE_TOTP, CLOSE_ID};

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TOTPConfigurationDialog.class);

    /**
     * Constructs a {@link TOTPConfigurationDialog}.
     *
     * @param user the user
     */
    public TOTPConfigurationDialog(User user) {
        super(Messages.get("login.mfa.totpconfig.title"), BUTTONS, null);
        this.user = user;
        service = ServiceHelper.getBean(MfaService.class);
        container.setFont(StyleSheetHelper.getDefaultFont()); // need to set font here, for html label used later
        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, container));
        resize("TOTPConfigurationDialog.size");
        display();
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (NEW_TOTP.equals(button)) {
            onNewTOTP();
        } else if (DISABLE_TOTP.equals(button)) {
            onDisableTOTP();
        } else {
            super.onButton(button);
        }
    }

    /**
     * Displays the current TOTP configuration.
     */
    private void display() {
        boolean hasTOTP = service.hasTOTP(user);
        container.removeAll();
        if (hasTOTP) {
            String url = service.getQRCodeURL(user);
            String baseURL = ServiceHelper.getBean(HelpTopics.class).getBaseURL();
            ImageReference qrCode = generateQRCode(url);
            container.add(LabelFactory.create("login.mfa.totpconfig.message", true, true));
            container.add(LabelFactory.html(Messages.format("login.mfa.totpconfig.apps", baseURL), true));
            container.add(new Label(qrCode));
        } else {
            container.add(LabelFactory.create("login.mfa.totpconfig.message", true, true));
            container.add(LabelFactory.create("login.mfa.totpconfig.new", true, true));
        }
        getButtons().setEnabled(DISABLE_TOTP, hasTOTP);
    }

    /**
     * Displays a confirmation dialog to create a TOTP secret if one already exists, else creates it.
     */
    private void onNewTOTP() {
        if (service.hasTOTP(user)) {
            ConfirmationDialog.newDialog()
                    .title(Messages.get("login.mfa.totpconfig.title"))
                    .message(Messages.get("login.mfa.totpconfig.replace"))
                    .yesNo()
                    .yes(this::createTOTP)
                    .show();
        } else {
            createTOTP();
        }
    }

    /**
     * Creates a new TOTP secret.
     */
    private void createTOTP() {
        service.configureTOTP(user);
        display();
    }

    /**
     * Displays a confirmation dialog to disable TOTP.
     */
    private void onDisableTOTP() {
        ConfirmationDialog.newDialog()
                .title(Messages.get("login.mfa.totpconfig.title"))
                .message(Messages.get("login.mfa.totpconfig.disable"))
                .yesNo()
                .yes(this::disableTOTP)
                .show();
    }

    /**
     * Disables TOTP.
     */
    private void disableTOTP() {
        service.removeTOTP(user);
        onClose();
    }

    /**
     * Generates a QR Code image for a TOTP secret.
     *
     * @param url the totp secret URL
     * @return the corresponding image, or {@code null} if an error occurs generating the image
     */
    private ImageReference generateQRCode(String url) {
        ImageReference result = null;
        try {
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, 200, 200);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "png", bos);
            result = new ThumbnailImageReference(bos.toByteArray(), "application/png");
        } catch (Exception exception) {
            log.error("Failed to generate QR code: {}", exception.getMessage(), exception);
        }
        return result;
    }
}
