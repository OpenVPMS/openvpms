/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.TemplateHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.table.DescriptorTableModel;

import java.util.List;

/**
 * Table model for <em>entity.documentTemplate</em> objects.
 *
 * @author Tim Anderson
 */
public class AbstractDocumentTemplateTableModel extends DescriptorTableModel<Entity> {

    /**
     * The nodes to display.
     */
    private final String[] nodes;

    /**
     * The template helper.
     */
    private final TemplateHelper templateHelper;

    /**
     * The content column index.
     */
    private int contentIndex;

    /**
     * The document template short names.
     */
    private static final String[] ARCHETYPES = new String[]{DocumentArchetypes.DOCUMENT_TEMPLATE};

    /**
     * Constructs an {@link AbstractDocumentTemplateTableModel}.
     *
     * @param nodes   the nodes to display
     * @param context the context
     * @param active  if {@code true}, include the active node, else suppress it
     */
    protected AbstractDocumentTemplateTableModel(String[] nodes, LayoutContext context, boolean active) {
        super(context);
        this.nodes = (active) ? nodes : ArrayUtils.removeElement(nodes, "active");
        templateHelper = new TemplateHelper(getService());
        setTableColumnModel(createColumnModel(ARCHETYPES, context));
    }

    /**
     * Returns a list of descriptor names to include in the table.
     *
     * @return the list of descriptor names to include in the table
     */
    @Override
    protected String[] getNodeNames() {
        return nodes;
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the table column
     * @param row    the table row
     */
    @Override
    protected Object getValue(Entity object, TableColumn column, int row) {
        if (column.getModelIndex() == contentIndex) {
            return getContentName(object);
        }
        return super.getValue(object, column, row);
    }

    /**
     * Returns a value for a given column.
     *
     * @param object the object to operate on
     * @param column the column
     * @param row    the row
     * @return the value for the column
     */
    @Override
    protected Object getValue(Entity object, DescriptorTableColumn column, int row) {
        Object result = null;
        if ("type".equals(column.getName())) {
            List<IMObject> values = column.getValues(object);
            if (values != null && !values.isEmpty()) {
                result = values.get(0).getName();
            }
        } else {
            result = super.getValue(object, column, row);
        }
        return result;
    }

    /**
     * Creates a column model for a set of archetypes.
     *
     * @param shortNames the archetype short names
     * @param context    the layout context
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(String[] shortNames, LayoutContext context) {
        DefaultTableColumnModel model = (DefaultTableColumnModel) super.createColumnModel(shortNames, context);
        contentIndex = getNextModelIndex(model);
        model.addColumn(createTableColumn(contentIndex, "document.template.content"));
        return model;
    }

    /**
     * Returns the name of the content associated with the template.
     *
     * @param object the template
     * @return the content name, or {@code null} if there is no content
     */
    private String getContentName(Entity object) {
        return templateHelper.getFileName(object);
    }
}