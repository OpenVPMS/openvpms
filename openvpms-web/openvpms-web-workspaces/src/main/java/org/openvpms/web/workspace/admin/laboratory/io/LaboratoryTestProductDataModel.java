/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory.io;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.laboratory.internal.io.LaboratoryTestProductData;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.product.PricingContextFactory;
import org.openvpms.web.component.im.product.ProductPricingContext;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.DefaultListMarkModel;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.format.NumberFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Table model for products affected by laboratory test price changes.
 *
 * @author Tim Anderson
 */
class LaboratoryTestProductDataModel extends AbstractIMTableModel<LaboratoryTestProductData> {

    /**
     * The pricing context.
     */
    private final ProductPricingContext pricingContext;

    /**
     * The date to use when determining prices.
     */
    private Date now;

    /**
     * The product id column.
     */
    private static final int ID = MARK_INDEX + 1;

    /**
     * The product name column.
     */
    private static final int NAME = ID + 1;

    /**
     * The associated test(s) column.
     */
    private static final int TESTS = NAME + 1;

    /**
     * The new cost column.
     */
    private static final int NEW_COST = TESTS + 1;

    /**
     * The new price column.
     */
    private static final int NEW_PRICE = NEW_COST + 1;

    /**
     * The old cost column.
     */
    private static final int OLD_COST = NEW_PRICE + 1;

    /**
     * The old price column.
     */
    private static final int OLD_PRICE = OLD_COST + 1;

    /**
     * Constructs an {@link LaboratoryTestProductDataModel}.
     *
     * @param context the layout context
     */
    public LaboratoryTestProductDataModel(LayoutContext context) {
        setRowMarkModel(new DefaultListMarkModel());
        DefaultTableColumnModel model = new DefaultTableColumnModel();
        addMarkColumn(model);
        model.addColumn(createTableColumn(ID, "table.imobject.id"));
        model.addColumn(createTableColumn(NAME, "table.imobject.name"));
        model.addColumn(createTableColumn(TESTS, "admin.laboratory.import.tests"));
        model.addColumn(createTableColumn(NEW_COST, "admin.laboratory.import.newCost"));
        model.addColumn(createTableColumn(NEW_PRICE, "admin.laboratory.import.newPrice"));
        model.addColumn(createTableColumn(OLD_COST, "admin.laboratory.import.oldCost"));
        model.addColumn(createTableColumn(OLD_PRICE, "admin.laboratory.import.oldPrice"));
        setTableColumnModel(model);

        Party practice = context.getContext().getPractice();
        PricingContextFactory factory = ServiceHelper.getBean(PricingContextFactory.class);
        pricingContext = factory.createProductPricingContext(practice, context.getContext().getLocation());
    }

    /**
     * Invoked prior to the table being rendered.
     */
    @Override
    public void preRender() {
        now = new Date();
    }

    /**
     * Returns the sort criteria.
     *
     * @param column    the primary sort column
     * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
     * @return the sort criteria, or {@code null} if the column isn't sortable
     */
    @Override
    public SortConstraint[] getSortConstraints(int column, boolean ascending) {
        return null;
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the column
     * @param row    the row
     * @return the value at the given coordinate.
     */
    @Override
    protected Object getValue(LaboratoryTestProductData object, TableColumn column, int row) {
        Object result;
        switch (column.getModelIndex()) {
            case MARK_INDEX:
                result = getRowMark(row);
                break;
            case ID:
                result = object.getNewProduct().getId();
                break;
            case NAME:
                result = object.getNewProduct().getName();
                break;
            case TESTS:
                result = getTests(object.getTests());
                break;
            case NEW_COST:
                result = getCurrency(object.getNewCost());
                break;
            case NEW_PRICE:
                result = getCurrency(pricingContext.getTaxExPrice(object.getNewProduct(), now));
                break;
            case OLD_COST:
                result = getCurrency(object.getOldCost());
                break;
            case OLD_PRICE:
                result = getCurrency(pricingContext.getTaxExPrice(object.getOldProduct(), now));
                break;
            default:
                result = null;
        }
        return result;
    }

    /**
     * Returns a component listing test names.
     *
     * @param tests the tests
     * @return a new component
     */
    private Component getTests(List<Entity> tests) {
        String text = tests.stream()
                .map(IMObject::getName)
                .sorted()
                .collect(Collectors.joining("\n"));
        return LabelFactory.text(text);
    }

    /**
     * Helper to format a currency value for display in a table.
     *
     * @param value the value
     * @return a right-aligned currency label
     */
    private Label getCurrency(BigDecimal value) {
        return TableHelper.rightAlign(NumberFormatter.formatCurrency(value));
    }
}
