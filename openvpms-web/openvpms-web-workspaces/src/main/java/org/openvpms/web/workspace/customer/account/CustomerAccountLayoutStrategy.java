/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import nextapp.echo2.app.Column;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.im.view.IMObjectComponentFactory;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.charge.ChargeArchetypeNodesFactory;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.COUNTER;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.CREDIT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;

/**
 * A layout strategy for customer account acts that displays the "hide" node, if {@code true}.
 *
 * @author Tim Anderson
 */
public class CustomerAccountLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Hide node.
     */
    private static final String HIDE = "hide";

    /**
     * Constructs a {@link CustomerAccountLayoutStrategy}.
     */
    public CustomerAccountLayoutStrategy() {
        setArchetypeNodes(ArchetypeNodes.all());
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        FinancialAct act = (FinancialAct) object;
        if (!context.isEdit() && act.getStatus().equals(FinancialActStatus.POSTED)) {
            // only display the allocation node for finalised acts
            getArchetypeNodes().complex("allocation");
            addComponent(getAllocation(act, properties, context));
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns {@link ArchetypeNodes} to determine which nodes will be displayed.
     *
     * @param object  the object to display
     * @param context the layout context
     * @return the archetype nodes
     */
    @Override
    protected ArchetypeNodes getArchetypeNodes(IMObject object, LayoutContext context) {
        ArchetypeNodes nodes = super.getArchetypeNodes(object, context);
        IMObjectBean bean = getBean(object);
        if (object.isA(INVOICE, COUNTER, CREDIT)) {
            ChargeArchetypeNodesFactory.apply(nodes);
        }
        if (bean.getBoolean(HIDE)) {
            nodes.simple(HIDE);
        }
        return nodes;
    }

    /**
     * Creates a component to display the allocation of credits/debits to an act.
     *
     * @param act        the act
     * @param properties the object's properties
     * @param context    the layout context
     * @return the component
     */
    private ComponentState getAllocation(FinancialAct act, PropertySet properties, LayoutContext context) {
        CollectionProperty allocation = (CollectionProperty) properties.get("allocation");

        // display the allocated and unallocated amounts
        Property allocated = properties.get("allocatedAmount");
        Property unallocated = new SimpleProperty("unallocatedAmount", null, Money.class,
                                                  Messages.get("customer.account.unallocated"), true);
        unallocated.setValue(act.getTotal().subtract(act.getAllocatedAmount()));

        IMObjectComponentFactory factory = context.getComponentFactory();
        ComponentGrid grid = new ComponentGrid();
        grid.add(factory.create(allocated, act), factory.create(unallocated, act));

        Column column = ColumnFactory.create(Styles.CELL_SPACING, grid.createGrid());

        // if there are debits/credits allocated to the act, display them
        if (!allocation.getValues().isEmpty()) {
            column.add(factory.create(allocation, act).getComponent());
        }
        return new ComponentState(column, allocation);
    }
}
