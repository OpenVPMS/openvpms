/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.printer;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.print.PrinterViewer;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;

/**
 * Layout strategy for <em>entity.printer</em> o suppress the name node. This is derived from the printer name.
 */
public class PrinterLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The nodes.
     */
    private static final ArchetypeNodes nodes = ArchetypeNodes.all().exclude("name");

    /**
     * Constructs a {@link PrinterLayoutStrategy}.
     */
    public PrinterLayoutStrategy() {
        setArchetypeNodes(nodes);
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (!context.isEdit()) {
            Property printer = properties.get("printer");
            addComponent(new ComponentState(new PrinterViewer(printer, context).getComponent(), printer));
        }
        return super.apply(object, properties, parent, context);
    }
}
