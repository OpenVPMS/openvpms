/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.location.LocationSelectField;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;

import java.util.List;

/**
 * Query for SMS acts.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSMSQuery extends DateRangeActQuery<Act> {

    /**
     * The location selector.
     */
    private final LocationSelectField location;

    /**
     * Constructs an {@link SMSQuery}.
     *
     * @param context the layout context
     */
    public AbstractSMSQuery(String archetype, ActStatuses statuses, LayoutContext context) {
        super(null, null, null, new String[]{archetype}, statuses, Act.class);
        location = createLocationSelector(context.getContext());
        setAuto(true);
        setContains(true);

        setDefaultSortConstraint(DESCENDING_START_TIME);
    }

    /**
     * Sets the selected location.
     *
     * @param location the selected location, or {@code null} to select all locations available to the user
     */
    public void setLocation(Party location) {
        this.location.setSelectedItem(location);
    }

    /**
     * Returns the preferred height of the query when rendered.
     *
     * @return the preferred height, or {@code null} if it has no preferred height
     */
    @Override
    public Extent getHeight() {
        return getHeight(2);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        Party location = (Party) this.location.getSelectedItem();
        String[] statuses = getStatuses();
        return new SMSResultSet(getArchetypeConstraint(), getValue(), location,
                                getLocations(), getFrom(), getTo(), statuses,
                                getMaxResults(), sort);
    }

    /**
     * Returns the available locations.
     *
     * @return the available locations
     */
    protected List<Party> getLocations() {
        return this.location.getLocations();
    }

    /**
     * Creates a container component to lay out the query component in.
     *
     * @return a new container
     * @see #doLayout(Component)
     */
    @Override
    protected Component createContainer() {
        return GridFactory.create(6);
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addSearchField(container);
        addStatusSelector(container);
        addDateRange(container);
        addLocation(container);
    }

    /**
     * Creates a field to select the location.
     *
     * @param context the context
     * @return a new selector
     */
    protected LocationSelectField createLocationSelector(Context context) {
        LocationSelectField result = new LocationSelectField(context.getUser(), context.getPractice(), true);
        result.setSelectedItem(context.getLocation());
        result.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        return result;
    }

    /**
     * Adds the location selector to a container.
     *
     * @param container the container
     */
    private void addLocation(Component container) {
        Label label = LabelFactory.text(DescriptorHelper.getDisplayName(SMSArchetypes.MESSAGE, "location",
                                                                        getService()));
        container.add(label);
        container.add(location);
        getFocusGroup().add(location);
    }
}
