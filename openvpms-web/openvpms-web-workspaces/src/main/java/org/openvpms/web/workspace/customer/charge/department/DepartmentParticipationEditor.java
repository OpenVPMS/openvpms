/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge.department;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.LayoutContext;

/**
 * Participation editor for <em>participation.department</em>.
 *
 * @author Tim Anderson
 */
public class DepartmentParticipationEditor extends ParticipationEditor<Entity> {

    /**
     * Constructs a {@link DepartmentParticipationEditor}.
     *
     * @param participation the object to edit
     * @param parent        the parent object
     * @param context       the layout context
     */
    public DepartmentParticipationEditor(Participation participation, Act parent, LayoutContext context) {
        super(participation, parent, context);
        if (participation.isNew() && getEntityRef() == null) {
            setEntity(context.getContext().getDepartment());
        }
    }

}
