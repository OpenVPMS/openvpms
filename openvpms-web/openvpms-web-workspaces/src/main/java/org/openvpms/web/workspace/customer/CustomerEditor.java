/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer;

import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.im.contact.ContactCollectionEditor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.relationship.EntityLinkEditor;
import org.openvpms.web.component.im.relationship.SingleEntityLinkCollectionEditor;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;


/**
 * Editor for <em>party.customer*</em> parties.
 *
 * @author Tim Anderson
 */
public class CustomerEditor extends AbstractIMObjectEditor {

    /**
     * Referral node name.
     */
    public static final String REFERRAL = "referral";

    /**
     * Referred by customer node name.
     */
    public static final String REFERRED_BY_CUSTOMER = "referredByCustomer";

    /**
     * Contacts node name.
     */
    public static final String CONTACTS = "contacts";

    /**
     * Patients node name.
     */
    public static final String PATIENTS = "patients";

    /**
     * <em>lookup.customerReferral</em> code indicating referral by another customer.
     */
    public static final String CUSTOMER_REFERRAL = "CUSTOMER";

    /**
     * The referred by customer editor.
     */
    private final SingleEntityLinkCollectionEditor referredByCustomerEditor;

    /**
     * Constructs a {@link CustomerEditor}.
     *
     * @param customer the object to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context. May be {@code null}.
     */
    public CustomerEditor(Party customer, IMObject parent, LayoutContext context) {
        super(customer, parent, context);

        if (customer.isNew()) {
            // initialise the practice location if one is not already present
            initPractice(context);
        }

        CollectionProperty contacts = getCollectionProperty(CONTACTS);
        if (contacts != null) {
            ContactCollectionEditor editor = createContactCollectionEditor(customer, context, contacts);
            addEditor(editor);

            if (contacts.getMinCardinality() == 0) {
                editor.setExcludeUnmodifiedContacts(true);
            }
            addContact(editor, ContactArchetypes.LOCATION);
            addContact(editor, ContactArchetypes.PHONE);
            addContact(editor, ContactArchetypes.EMAIL);
        }

        Property referral = getProperty(REFERRAL);
        CollectionProperty referredByCustomer = getCollectionProperty(REFERRED_BY_CUSTOMER);
        if (referral != null && referredByCustomer != null) {
            referredByCustomerEditor = new SingleEntityLinkCollectionEditor(referredByCustomer, customer, context);
            referredByCustomerEditor.getComponent(); // ensures the editor is created, and thus the focus group
            addEditor(referredByCustomerEditor);
            referral.addModifiableListener(modifiable -> {
                updateReferredByCustomer(referral);
            });
        } else {
            referredByCustomerEditor = null;
        }

        getLayoutContext().getContext().setCustomer(customer);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        return new CustomerEditor(reload((Party) getObject()), getParent(), getLayoutContext());
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        IMObjectLayoutStrategy strategy = super.createLayoutStrategy();
        ContactCollectionEditor contactEditor = getContacts();
        if (contactEditor != null) {
            strategy.addComponent(new ComponentState(contactEditor));
        }
        if (referredByCustomerEditor != null) {
            strategy.addComponent(new ComponentState(referredByCustomerEditor));
        }
        return strategy;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateReferral(validator);
    }

    /**
     * Creates a new editor for contacts.
     *
     * @param customer the customer
     * @param context  the context
     * @param contacts the contacts property
     * @return a new editor for contacts
     */
    protected ContactCollectionEditor createContactCollectionEditor(Party customer, LayoutContext context,
                                                                    CollectionProperty contacts) {
        return new ContactCollectionEditor(contacts, customer, context);
    }

    /**
     * Returns the contacts editor.
     *
     * @return the contacts editor, or {@code null} if none is registered
     */
    protected ContactCollectionEditor getContacts() {
        Editor editor = getEditor(CONTACTS, false);
        return (editor instanceof ContactCollectionEditor) ? (ContactCollectionEditor) editor : null;
    }

    /**
     * Add a contact if no instance currently exists.
     *
     * @param editor    the contact editor
     * @param shortName the contact archetype short name
     * @return the contact, or {@code null} if none was added
     */
    protected Contact addContact(ContactCollectionEditor editor, String shortName) {
        Contact contact = null;
        if (IMObjectHelper.getObject(shortName, editor.getCurrentObjects()) == null) {
            contact = (Contact) IMObjectCreator.create(shortName);
            if (contact != null) {
                ServiceHelper.getArchetypeService().deriveValues(contact);
                editor.add(contact);
            }
        }
        return contact;
    }

    /**
     * Sets the referral code.
     *
     * @param referral the <em>lookup.customerReferral</em> code. May be {@code null}
     */
    void setReferral(String referral) {
        getProperty(REFERRAL).setValue(referral);
    }

    /**
     * Sets the referred by customer.
     *
     * @param customer the customer
     */
    void setReferredByCustomer(Party customer) {
        EntityLinkEditor editor = getReferredByCustomerEditor();
        if (editor != null) {
            editor.setTarget(customer);
        }
    }

    /**
     * Returns the patient editor.
     *
     * @return the patient editor. May be {@code null}
     */
    PatientEntityRelationshipCollectionEditor getPatientCollectionEditor() {
        return (PatientEntityRelationshipCollectionEditor) getEditor(PATIENTS, false);
    }

    /**
     * Initialises the customer's practice location, if one isn't already present.
     *
     * @param context the layout context
     */
    private void initPractice(LayoutContext context) {
        Party location = context.getContext().getLocation();
        CollectionProperty property = getCollectionProperty("practice");
        if (location != null && property != null && property.size() == 0) {
            String[] range = property.getArchetypeRange();
            if (range.length == 1) {
                IMObject object = IMObjectCreator.create(range[0]);
                if (object instanceof Relationship) {
                    Relationship relationship = (Relationship) object;
                    relationship.setTarget(location.getObjectReference());
                    property.add(object);
                }
            }
        }
    }

    /**
     * Returns the reference to the referred-by customer.
     *
     * @return the reference. May be {@code null}
     */
    private Reference getReferredByCustomer() {
        EntityLinkEditor editor = getReferredByCustomerEditor();
        return editor != null ? editor.getTargetRef() : null;
    }

    private EntityLinkEditor getReferredByCustomerEditor() {
        return (referredByCustomerEditor != null) ? (EntityLinkEditor) referredByCustomerEditor.getCurrentEditor()
                                                  : null;
    }

    /**
     * Validates the referral property.
     * <p/>
     * This ensures that if "CUSTOMER" is selected, the <em>referredByCustomer</em> is populated.
     *
     * @param validator the validator
     * @return {@code true} if valid, otherwise {@code false}
     */
    private boolean validateReferral(Validator validator) {
        Property property = getProperty(REFERRAL);
        if (property != null) {
            String referral = property.getString();
            if (CUSTOMER_REFERRAL.equals(referral)) {
                Reference referredByCustomer = getReferredByCustomer();
                if (referredByCustomer == null) {
                    validator.add(property, Messages.format("property.error.required",
                                                            referredByCustomerEditor.getProperty().getDisplayName()));
                } else if (getObject().getObjectReference().equals(referredByCustomer)) {
                    // too many layers to get through to prevent the same customer being selected via query,
                    // so just validate that they aren't the same. TODO
                    validator.add(property, Messages.format("customer.sameReferralCustomer"));
                }
            }
        }
        return validator.isValid();
    }

    /**
     * Clears the referredByCustomer if the referred property is not "CUSTOMER".
     *
     * @param referred the referred property
     */
    private void updateReferredByCustomer(Property referred) {
        if (!CUSTOMER_REFERRAL.equals(referred.getString())) {
            IMObjectEditor editor = referredByCustomerEditor.getCurrentEditor();
            if (editor != null) {
                ((EntityLinkEditor) editor).setTarget(null);
            }
        }
    }
}
