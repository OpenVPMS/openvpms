/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.order;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.supplier.ProductOrder;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.util.LookupNameHelper;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.CellFormat;
import org.openvpms.web.echo.table.EvenOddTableCellRenderer;
import org.openvpms.web.echo.table.TableColumnFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

/**
 * Confirmation dialog to show existing orders for a product and prompt the user if they want to order again.
 *
 * @author Tim Anderson
 */
public class ProductOnOrderDialog extends ConfirmationDialog {

    /**
     * The order table.
     */
    private final PagedIMTable<ProductOrder> table;

    /**
     * Constructs a {@link ProductOnOrderDialog}.
     *
     * @param product the product
     * @param orders  the orders
     */
    public ProductOnOrderDialog(Product product, List<ProductOrder> orders) {
        super(Messages.get("supplier.order.onorder.title"),
              Messages.format("supplier.order.onorder.message", product.getName()),
              YES_NO);
        if (orders.size() > 0) {
            orders.sort((o1, o2) -> {
                int result = DateRules.compareTo(o1.getDate(), o2.getDate());
                if (result == 0) {
                    result = Long.compare(o1.getOrder().getId(), o2.getOrder().getId());
                }
                return result;
            });
        }
        table = new PagedIMTable<>(new ProductOrderTableModel());
        table.setResultSet(new ListResultSet<>(orders, 5));
        resize("ProductOnOrderDialog.size");
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        Label message = LabelFactory.create(true, true);
        message.setText(getMessage());
        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, message, table.getComponent());
        Row row = RowFactory.create(Styles.LARGE_INSET, column);
        getLayout().add(row);
    }

    private static class ProductOrderTableModel extends AbstractIMTableModel<ProductOrder> {

        private static final int ID_INDEX = 0;

        private static final int DATE_INDEX = 1;

        private static final int SUPPLIER_INDEX = 2;

        private static final int QUANTITY_INDEX = 3;

        private static final int PACKAGE_SIZE_INDEX = 4;

        private static final int PACKAGE_SIZE_UNITS = 5;

        /**
         * Constructs a {@link ProductOrderTableModel}.
         */
        public ProductOrderTableModel() {
            super();
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            EvenOddTableCellRenderer valueRender = new EvenOddTableCellRenderer(CellFormat.value());
            model.addColumn(createTableColumn(ID_INDEX, ID,
                                              new EvenOddTableCellRenderer(CellFormat.id())));
            model.addColumn(createTableColumn(DATE_INDEX, "table.act.date",
                                              new EvenOddTableCellRenderer(CellFormat.date())));
            model.addColumn(createTableColumn(SUPPLIER_INDEX, "supplier.type", valueRender));
            EvenOddTableCellRenderer integerRenderer = new EvenOddTableCellRenderer(CellFormat.integer());
            model.addColumn(createTableColumn(QUANTITY_INDEX, "supplier.order.onorder.quantity",
                                              integerRenderer));
            model.addColumn(TableColumnFactory.create(PACKAGE_SIZE_INDEX,
                                                      getDisplayName(SupplierArchetypes.ORDER_ITEM, "packageSize"),
                                                      integerRenderer));
            model.addColumn(TableColumnFactory.create(PACKAGE_SIZE_UNITS,
                                                      getDisplayName(SupplierArchetypes.ORDER_ITEM, "packageUnits"),
                                                      valueRender));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return null;
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(ProductOrder object, TableColumn column, int row) {
            Object result;
            switch (column.getModelIndex()) {
                case ID_INDEX:
                    result = object.getOrder().getId();
                    break;
                case DATE_INDEX:
                    result = object.getDate();
                    break;
                case SUPPLIER_INDEX:
                    result = object.getSupplierName();
                    break;
                case QUANTITY_INDEX:
                    result = object.getUndelivered();
                    break;
                case PACKAGE_SIZE_INDEX:
                    result = object.getPackageSize();
                    break;
                case PACKAGE_SIZE_UNITS:
                    result = getPackageUnits(object);
                    break;
                default:
                    result = null;
            }
            return result;
        }

        /**
         * Returns the package units for an order.
         *
         * @param object the order
         * @return the package units. May be {@code null}
         */
        private String getPackageUnits(ProductOrder object) {
            String units = object.getPackageUnits();
            return units != null ? LookupNameHelper.getName("lookup.uom", units, null) : null;
        }
    }
}