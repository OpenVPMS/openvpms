/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.party.ContactMatcher;
import org.openvpms.archetype.rules.party.Contacts;
import org.openvpms.archetype.rules.party.PurposeMatcher;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderType;
import org.openvpms.archetype.rules.patient.reminder.ReminderType.GroupBy;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.openvpms.web.workspace.reporting.ReportingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Patient reminder processor.
 *
 * @author Tim Anderson
 */
public abstract class PatientReminderProcessor<T extends PatientReminders> {

    /**
     * The reminder contact purpose.
     */
    protected static final String CONTACT_PURPOSE = "REMINDER";

    /**
     * The reason to use when logging communications.
     */
    protected static final String COMMUNICATION_REASON = "PATIENT_REMINDER";

    /**
     * The practice.
     */
    private final Party practice;

    /**
     * The reminder types.
     */
    private final ReminderTypes reminderTypes;

    /**
     * The reminder rules.
     */
    private final ReminderRules reminderRules;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The reminder configuration.
     */
    private final ReminderConfiguration config;

    /**
     * The communication logger.
     */
    private final CommunicationLogger logger;

    /**
     * The action factory.
     */
    private final ActionFactory actionFactory;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PatientReminderProcessor.class);

    /**
     * Constructs a {@link PatientReminderProcessor}.
     *
     * @param reminderTypes the reminder types
     * @param reminderRules the reminder rules
     * @param patientRules  the patient rules
     * @param practice      the practice
     * @param service       the archetype service
     * @param config        the reminder configuration
     * @param logger        the communication logger. May be {@code null}
     * @param actionFactory the action factory
     */
    public PatientReminderProcessor(ReminderTypes reminderTypes, ReminderRules reminderRules, PatientRules patientRules,
                                    Party practice, ArchetypeService service, ReminderConfiguration config,
                                    CommunicationLogger logger, ActionFactory actionFactory) {
        this.reminderTypes = reminderTypes;
        this.reminderRules = reminderRules;
        this.patientRules = patientRules;
        this.practice = practice;
        this.service = service;
        this.config = config;
        this.logger = logger;
        this.actionFactory = actionFactory;
    }

    /**
     * Prepares reminders for processing.
     * <p>
     * If reminders aren't being resent, this:
     * <ul>
     * <li>cancels any reminders that are no longer applicable</li>
     * <li>filters out any reminders that can't be processed due to missing data</li>
     * <li>adds meta-data for subsequent calls to {@link #process}</li>
     * </ul>
     * If reminders are being resent, due dates are ignored, and no cancellation will occur.
     * <p>
     * To specify the contact to use, pre-populate reminders via the {@link ReminderEvent#setContact(Contact)} method.
     *
     * @param reminders  the reminders
     * @param groupBy    the reminder grouping policy. This determines which document template is selected
     * @param cancelDate the date to use when determining if a reminder item should be cancelled
     * @param resend     if {@code true}, reminders are being resent
     * @return the reminders to process
     */
    public T prepare(List<ReminderEvent> reminders, GroupBy groupBy, Date cancelDate, boolean resend) {
        T result = createPatientReminders(groupBy, resend);
        for (ReminderEvent reminder : reminders) {
            Act item = reminder.getItem();
            if (!resend && isOutOfDate(item, cancelDate)) {
                // cancel out of date reminders, unless they are being resent
                cancel(reminder, Messages.get("reporting.reminder.outofdate"), false, result);
            } else if (isReminderTypeInactive(reminder)) {
                // cancel reminders for inactive reminder types
                cancel(reminder, Messages.get("reporting.reminder.remindertypeinactive"), resend, result);
            } else if (isDeceased(reminder)) {
                // cancel reminders for deceased patients
                cancel(reminder, Messages.get("reporting.reminder.deceased"), resend, result);
            } else if (isPatientInactive(reminder)) {
                // cancel reminders for inactive patients
                cancel(reminder, Messages.get("reporting.reminder.patientinactive"), resend, result);
            } else if (isCustomerInactive(reminder)) {
                // cancel reminders for inactive customers. If the reminder is being resent, don't update the reminder.
                cancel(reminder, Messages.get("reporting.reminder.customerinactive"), resend, result);
            } else {
                result.addReminder(reminder);
            }
        }
        prepare(result);
        return result;
    }

    /**
     * Returns the reminder item archetype that this processes.
     *
     * @return the archetype
     */
    public abstract String getArchetype();

    /**
     * Processes reminders.
     *
     * @param reminders the reminders
     */
    public abstract void process(T reminders);

    /**
     * Completes processing.
     *
     * @param reminders the reminders
     * @return {@code true} if any changes were saved
     */
    public boolean complete(T reminders) {
        boolean result;
        boolean resend = reminders.getResend();
        for (ReminderEvent reminder : reminders.getReminders()) {
            if (!resend) {
                complete(reminder, reminders);
            } else {
                // if it's a resend of a reminder, but the reminder item is new, complete it, if it isn't in error
                Act item = reminder.getItem();
                if (item.isNew() && !ReminderItemStatus.ERROR.equals(item.getStatus())) {
                    completeItem(reminder);
                    reminders.addUpdated(reminder, item);
                }
            }
        }
        result = save(reminders);
        CommunicationLogger logger = getLogger();
        if (logger != null && !reminders.getReminders().isEmpty()) {
            log(reminders, logger);
        }
        return result;
    }

    /**
     * Invoked when processing fails due to exception.
     * For reminders that are not being resent, are not in error or cancelled and haven't been saved, this sets the:
     * <ul>
     * <li>status of each reminder to be sent to {@link ReminderItemStatus#ERROR}</li>
     * <li>error node to the formatted message from the exception</li>
     * </ul>
     *
     * @param reminders the reminders
     * @param exception the exception
     * @return {@code true} if any reminders were updated
     */
    public boolean failed(PatientReminders reminders, Throwable exception) {
        MutableBoolean result = new MutableBoolean();
        boolean resend = reminders.getResend();
        log.error("Failed to send reminders: {}", exception.getMessage(), exception);
        if (!resend) {
            String error = ErrorFormatter.format(exception);
            for (ReminderEvent reminder : reminders.getUnsaved()) {
                reminders.addError(reminder);
                updateFailed(reminder, error);
                result.setValue(true);
            }
        }
        return result.booleanValue();
    }

    /**
     * Collects statistics.
     *
     * @param reminders  the reminders
     * @param statistics the statistics to update
     */
    public void addStatistics(PatientReminders reminders, Statistics statistics) {
        for (ReminderEvent event : reminders.getReminders()) {
            ReminderType reminderType = reminderTypes.get(event.getReminderType());
            if (reminderType != null) {
                statistics.increment(event, reminderType);
            }
        }
        statistics.addErrors(reminders.getErrors().size());
        statistics.addCancelled(reminders.getCancelled().size());
    }

    /**
     * Determines if reminder processing is performed asynchronously.
     *
     * @return {@code true} if reminder processing is performed asynchronously
     */
    public abstract boolean isAsynchronous();

    /**
     * Returns the reminder types.
     *
     * @return the reminder types
     */
    public ReminderTypes getReminderTypes() {
        return reminderTypes;
    }

    /**
     * Creates a {@link PatientReminders}.
     *
     * @param groupBy the reminder grouping policy. This determines which document template is selected
     * @param resend  if {@code true}, reminders are being resent
     * @return a new {@link PatientReminders}
     */
    protected abstract T createPatientReminders(GroupBy groupBy, boolean resend);

    /**
     * Prepares reminders for processing.
     * <p>
     * This:
     * <ul>
     * <li>filters out any reminders that can't be processed due to missing data</li>
     * <li>adds meta-data for subsequent calls to {@link #process}</li>
     * </ul>
     *
     * @param reminders the reminders to prepare
     * @throws ReportingException if the reminders cannot be prepared
     */
    protected abstract void prepare(T reminders);

    /**
     * Cancels a reminder item.
     * <p>
     * If the item is being resent, it is changed but not added to the set of updated acts. This is to ensure that
     * any original status and message is preserved, but that the status of the resend can be returned to the caller.
     *
     * @param reminder  the reminder
     * @param message   the cancellation reason
     * @param resend    determines if reminders are being resent. If {@code true}, the reminder itself isn't updated,
     *                  but is added to the set of cancelled reminders
     * @param reminders tracks updated and cancelled reminders
     */
    protected void cancel(ReminderEvent reminder, String message, boolean resend, PatientReminders reminders) {
        Act item = reminder.getItem();
        updateItem(item, ReminderItemStatus.CANCELLED, message);
        if (!resend) {
            List<Act> toSave = new ArrayList<>();
            toSave.add(item);
            Act act = updateReminder(reminder, item);
            if (act != null) {
                toSave.add(act);
            }
            reminders.addUpdated(reminder, toSave);
        }
        reminders.addCancelled(reminder);
    }

    /**
     * Determines if a reminder item should be cancelled as it is out of date
     *
     * @param item the item
     * @param from the date to cancel from
     * @return {@code true} if the reminder item should be cancelled
     */
    protected boolean isOutOfDate(Act item, Date from) {
        Date cancel = config.getCancelDate(item.getActivityStartTime(), getArchetype());
        return DateRules.compareDates(cancel, from) <= 0;
    }

    /**
     * Determines if the patient is deceased.
     *
     * @param reminder the reminder
     * @return {@code true} if the patient is deceased
     */
    protected boolean isDeceased(ReminderEvent reminder) {
        Party patient = reminder.getPatient();
        return patient != null && patientRules.isDeceased(patient);
    }

    /**
     * Determines if the patient is inactive.
     *
     * @param reminder the reminder
     * @return {@code true} if the patient is inactive
     */
    protected boolean isPatientInactive(ReminderEvent reminder) {
        Party patient = reminder.getPatient();
        return patient == null || !patient.isActive();
    }

    /**
     * Determines if the customer is inactive.
     *
     * @param reminder the reminder
     * @return {@code true} if the customer is inactive
     */
    protected boolean isCustomerInactive(ReminderEvent reminder) {
        Party customer = reminder.getCustomer();
        return customer == null || !customer.isActive();
    }

    /**
     * Logs reminder communications.
     *
     * @param reminders the reminders
     * @param logger    the communication logger
     */
    protected abstract void log(PatientReminders reminders, CommunicationLogger logger);

    /**
     * Saves the updated reminders.
     *
     * @param reminders the reminders
     * @return {@code true} if any changes were saved
     */
    protected boolean save(PatientReminders reminders) {
        boolean result = false;
        Map<ReminderEvent, List<Act>> updated = reminders.getUpdated();
        if (!updated.isEmpty()) {
            for (Map.Entry<ReminderEvent, List<Act>> entry : updated.entrySet()) {
                ReminderEvent reminder = entry.getKey();
                actionFactory.newAction()
                        .backgroundOnly()
                        .withObjects(entry.getValue())
                        .failIfChangedOrMissing()
                        .call(service::save)
                        .onSuccess(() -> reminders.addSaved(reminder))
                        .onFailure(status -> updateFailed(reminder, Messages
                                .format("reporting.reminder.sentnotupdated", status.getMessage())))
                        .run();
                result = true; // return true even on failure, as it may indicate paging has changed
            }
        }
        return result;
    }

    /**
     * Returns the practice location to use when sending reminders to a customer.
     *
     * @param customer the customer
     * @return the customer's preferred practice location, or the fallback reminder configuration location if the
     * customer has none. May be {@code null}
     */
    protected Party getLocation(Party customer) {
        Party location = service.getBean(customer).getTarget("practice", Party.class);
        if (location == null) {
            location = config.getLocation();
        }
        return location;
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     */
    protected Party getPractice() {
        return practice;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns the communication logger.
     *
     * @return the communication logger, or {@code null} if logging is disabled.
     */
    protected CommunicationLogger getLogger() {
        return logger;
    }

    /**
     * Returns the reminder configuration.
     *
     * @return the reminder configuration
     */
    protected ReminderConfiguration getConfig() {
        return config;
    }

    /**
     * Adds meta-data to reminders.
     *
     * @param reminders the reminders
     * @param contact   the customer contact. May be {@code null}
     * @param location  the customer location. May be {@code null}
     */
    protected void populate(PatientReminders reminders, Contact contact, Party location) {
        for (ReminderEvent reminder : reminders.getReminders()) {
            populate(reminder, contact, location);
        }
    }

    /**
     * Adds meta-data to a reminder.
     *
     * @param event    the reminder event
     * @param contact  the customer contact. May be {@code null}
     * @param location the customer location. May be {@code null}
     */
    protected void populate(ReminderEvent event, Contact contact, Party location) {
        Act reminder = event.getReminder();
        Act item = event.getItem();
        IMObjectBean bean = service.getBean(reminder);
        IMObjectBean itemBean = service.getBean(item);
        Reference reminderTypeRef = bean.getTargetRef("reminderType");
        ReminderType reminderType = reminderTypes.get(reminderTypeRef);
        event.setContact(contact);
        event.setReminderType(reminderType != null ? reminderType.getEntity() : null);
        event.setProduct(bean.getTarget("product", Product.class));
        event.setClinician(bean.getTarget("clinician", User.class));
        event.setReminderCount(itemBean.getInt("count"));
        event.setLocation(location);
    }

    /**
     * Creates a context for a reminder.
     *
     * @param reminder the reminder set
     * @param location the customer location. May be {@code null}
     * @return a new context
     */
    protected Context createContext(ReminderEvent reminder, Party location) {
        Context context = new LocalContext();
        context.setPatient(reminder.getPatient());
        context.setCustomer(reminder.getCustomer());
        context.setLocation(location);
        context.setPractice(practice);
        return context;
    }

    /**
     * Returns the contact to use.
     *
     * @param customer       the reminder
     * @param matcher        the contact matcher
     * @param defaultContact the default contact, or {@code null} to select one from the customer
     * @return the contact, or {@code null} if none is found
     */
    protected Contact getContact(Party customer, ContactMatcher matcher, Contact defaultContact) {
        Contact contact;
        if (defaultContact != null && matcher.isA(defaultContact)) {
            contact = defaultContact;
        } else {
            contact = Contacts.find(Contacts.sort(customer.getContacts()), matcher);
        }
        return contact;
    }

    /**
     * Creates a contact matcher to locate the contact to send to.
     *
     * @param shortName the contact archetype short name
     * @return a new contact matcher
     */
    protected ContactMatcher createContactMatcher(String shortName) {
        return new PurposeMatcher(shortName, CONTACT_PURPOSE, false, service);
    }

    /**
     * Returns the reminder type associated with a reminder set.
     *
     * @param reminder the reminder set
     * @return the reminder type, or {@code null} if none exists
     */
    protected ReminderType getReminderType(ReminderEvent reminder) {
        return reminderTypes.get(reminder.getReminderType());
    }

    /**
     * Returns a formatted note containing the reminder type and count.
     *
     * @param reminder the reminder
     * @return the note
     */
    protected String getNote(ReminderEvent reminder) {
        int reminderCount = reminder.getReminderCount();
        Entity reminderType = reminder.getReminderType();
        return getNote(reminderCount, reminderType);
    }

    /**
     * Returns a formatted note containing the reminder type and count.
     *
     * @param reminderCount the reminder count
     * @param reminderType  the reminder type. May be {@code null}
     * @return the note
     */
    protected String getNote(int reminderCount, Entity reminderType) {
        String name = (reminderType != null) ? reminderType.getName() : null;
        return Messages.format("reminder.log.note", name, reminderCount);
    }

    /**
     * Completes a reminder item, and updates the associated reminder if it has no PENDING or ERROR items.
     *
     * @param event the reminder event
     * @param state the processing state
     */
    protected void complete(ReminderEvent event, PatientReminders state) {
        Act item = completeItem(event);
        Act reminder = event.getReminder();
        updateReminder(reminder, item);
        IMObjectBean bean = service.getBean(reminder);
        bean.setValue("lastSent", new Date());
        state.addUpdated(event, reminder, item);
    }

    /**
     * Completes a reminder item.
     *
     * @param event the reminder event
     */
    protected Act completeItem(ReminderEvent event) {
        return updateItem(event, ReminderItemStatus.COMPLETED, null);
    }

    /**
     * Updates a reminder if it has no PENDING or ERROR items besides that supplied.
     *
     * @param event the reminder event
     * @param item  the reminder item
     * @return the updated reminder, or {@code null} if it wasn't updated
     */
    protected Act updateReminder(ReminderEvent event, Act item) {
        Act reminder = event.getReminder();
        if (updateReminder(reminder, item)) {
            return reminder;
        }
        return null;
    }

    /**
     * Updates a reminder item.
     *
     * @param event   the reminder event
     * @param status  the item status
     * @param message the error message. May be {@code null}
     * @return the reminder item
     */
    protected Act updateItem(ReminderEvent event, String status, String message) {
        Act item = event.getItem();
        updateItem(item, status, message);
        return item;
    }

    /**
     * Updates a reminder item.
     *
     * @param item    the item
     * @param status  the item status
     * @param message the error message. May be {@code null}
     */
    protected void updateItem(Act item, String status, String message) {
        item.setStatus(status);
        IMObjectBean bean = service.getBean(item);
        bean.setValue("processed", new Date());
        if (message != null) {
            // truncate the error if it is too long
            int maxLength = bean.getMaxLength("error");
            message = StringUtils.abbreviate(message, maxLength);
        }
        bean.setValue("error", message);
    }

    /**
     * Flags a reminder as being in error.
     *
     * @param reminder  the reminder
     * @param message   the error message
     * @param reminders the reminders
     */
    protected void error(ReminderEvent reminder, String message, T reminders) {
        Act item = updateItem(reminder, ReminderItemStatus.ERROR, message);
        reminders.addUpdated(reminder, item);
        reminders.addError(reminder);
    }

    /**
     * Flags each reminder item as being in error due to exception.
     *
     * @param reminders the reminders
     * @param exception the exception
     */
    protected void error(T reminders, Throwable exception) {
        String message = ErrorFormatter.format(exception);
        for (ReminderEvent event : reminders.getReminders()) {
            error(event, message, reminders);
        }
    }

    /**
     * Flags each reminder item as being in error.
     *
     * @param reminders the reminders
     * @param message   the error message
     */
    protected void error(T reminders, String message) {
        for (ReminderEvent event : reminders.getReminders()) {
            error(event, message, reminders);
        }
    }

    /**
     * Invoked if a reminder failed to be updated.
     * <p/>
     * If the reminder item exists and isn't in error or cancelled, it's status will be set to ERROR.
     *
     * @param reminder the reminder
     * @param error    the error
     */
    private void updateFailed(ReminderEvent reminder, String error) {
        actionFactory.newAction()
                .backgroundOnly()
                .withLatest(reminder.getItem())
                .call((item) -> {
                    if (!ReminderItemStatus.CANCELLED.equals(item.getStatus())
                        && !ReminderItemStatus.ERROR.equals(item.getStatus())) {
                        updateItem(item, ReminderItemStatus.ERROR, error);
                        service.save(item);
                    }
                })
                .onFailure(status -> {
                    log.error("Failed to update reminder item={} with error={}: {}",
                             reminder.getItem().getObjectReference(), error, status.getReason());
                })
                .run();
    }

    /**
     * Determines if the reminder type is inactive.
     *
     * @param event the reminder
     * @return {@code true} if the reminder type is inactive
     */
    private boolean isReminderTypeInactive(ReminderEvent event) {
        // NOTE: the reminder type is not populated on the event at this point
        IMObjectBean bean = service.getBean(event.getReminder());
        Reference reminderTypeRef = bean.getTargetRef("reminderType");
        ReminderType reminderType = reminderTypes.get(reminderTypeRef);
        return reminderType != null && !reminderType.isActive();
    }

    /**
     * Updates a reminder if it has no PENDING or ERROR items besides that supplied.
     * <p>
     * This increments the reminder count.
     * <p>
     * The caller is responsible for saving the reminder.
     *
     * @param reminder the reminder
     * @param item     the reminder item
     * @return {@code true} if the reminder was updated
     */
    private boolean updateReminder(Act reminder, Act item) {
        return reminderRules.updateReminder(reminder, item);
    }
}
