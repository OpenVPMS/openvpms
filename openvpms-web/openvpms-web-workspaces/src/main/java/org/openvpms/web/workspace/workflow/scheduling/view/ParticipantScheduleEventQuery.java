/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.scheduling.view;

import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * Queries schedule events by participant.
 * <p/>
 * By default, queries instances that are incomplete i.e. don't have COMPLETED or CANCELLED status.
 *
 * @author Tim Anderson
 */
public abstract class ParticipantScheduleEventQuery extends DateRangeActQuery<Act> {

    /**
     * Dummy incomplete status. Finds all events that aren't complete.
     */
    public static final String INCOMPLETE = "INCOMPLETE";

    /**
     * Statuses indicating an event is finished.
     */
    private final String[] COMPLETE_STATUSES = {WorkflowStatus.COMPLETED, WorkflowStatus.CANCELLED};


    /**
     * Dummy incomplete status, used in the status selector.
     */
    protected static final org.openvpms.component.business.domain.im.lookup.Lookup INCOMPLETE_STATUS
            = new org.openvpms.component.business.domain.im.lookup.Lookup(new ArchetypeId("lookup.local"), INCOMPLETE,
                                                                          Messages.get("workflow.scheduling.appointment.view.incomplete"));

    /**
     * Constructs a {@link ParticipantScheduleEventQuery}.
     *
     * @param entity        the entity to search for
     * @param participant   the participant node name
     * @param participation the entity participation archetype
     * @param archetype     the scheduling act archetype
     * @param statuses      the act status lookups
     */
    public ParticipantScheduleEventQuery(Entity entity, String participant, String participation, String archetype,
                                         ActStatuses statuses) {
        super(entity, participant, participation, new String[]{archetype}, statuses, Act.class);
    }

    /**
     * Returns the status codes representing completed events.
     *
     * @return the status codes
     */
    protected String[] getCompletedStatuses() {
        return COMPLETE_STATUSES;
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<Act> createResultSet(SortConstraint[] sort) {
        Lookup selected = getStatusSelector().getSelected();
        String[] statuses;

        if (selected == INCOMPLETE_STATUS) {
            List<String> available = new ArrayList<>(getStatusLookups().getCodes());
            for (String status : getCompletedStatuses()) {
                available.remove(status);
            }
            statuses = available.toArray(new String[0]);
        } else {
            statuses = super.getStatuses();
        }
        return new ActResultSet<>(getArchetypeConstraint(), getParticipantConstraint(), getFrom(), getTo(),
                                  statuses, excludeStatuses(), getConstraints(), getMaxResults(), sort);
    }

    protected static class StatusLookupQuery extends NodeLookupQuery {

        /**
         * Constructs a {@link StatusLookupQuery}.
         *
         * @param archetype the scheduling act archetype
         */
        public StatusLookupQuery(String archetype) {
            super(archetype, "status");
        }

        /**
         * Returns the default lookup.
         *
         * @return {@link #INCOMPLETE_STATUS}
         */
        @Override
        public org.openvpms.component.model.lookup.Lookup getDefault() {
            return INCOMPLETE_STATUS;
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<Lookup> getLookups() {
            List<Lookup> lookups = super.getLookups();
            lookups.add(0, INCOMPLETE_STATUS);
            return lookups;
        }
    }
}