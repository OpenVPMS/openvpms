/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Factory for creating logo components.
 *
 * @author Tim Anderson
 */
public class LogoComponentFactory {

    /**
     * The logo service.
     */
    private final LogoService logoService;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link LogoComponentFactory}.
     *
     * @param logoService the logo service
     * @param service     the archetype service
     */
    public LogoComponentFactory(LogoService logoService, ArchetypeService service) {
        this.logoService = logoService;
        this.service = service;
    }

    /**
     * Returns a component to display a logo for an entity.
     *
     * @param entity  the entity
     * @param context the layout context
     * @return the logo component
     */
    public ComponentState getLogo(Entity entity, LayoutContext context) {
        DocumentAct act = logoService.getLogo(entity);
        Participation participation = null;
        if (act != null) {
            IMObjectBean bean = service.getBean(act);
            participation = bean.getObject("owner", Participation.class);
        }
        ComponentState logo;
        if (participation != null) {
            logo = context.getComponentFactory().create(participation, entity);
        } else {
            logo = new ComponentState(LabelFactory.create("admin.practice.logo.none"));
        }
        logo.setDisplayName(Messages.get("admin.practice.logo"));
        return logo;
    }
}