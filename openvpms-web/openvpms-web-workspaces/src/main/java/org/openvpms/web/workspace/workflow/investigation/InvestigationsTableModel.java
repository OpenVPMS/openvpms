/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.doc.DocumentViewer;
import org.openvpms.web.component.im.doc.DocumentViewerFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableModel;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;
import java.util.List;


/**
 * Table model for <em>act.patientInvestigation</em> acts.
 *
 * @author Tim Anderson
 */
class InvestigationsTableModel extends DescriptorTableModel<Act> {

    /**
     * The patient rules.
     */
    private final PatientRules rules;

    /**
     * The document viewer factory.
     */
    private final DocumentViewerFactory factory;

    /**
     * The index of the start time column.
     */
    private int startTimeIndex;

    /**
     * The index of the endTime (Last Updated) column.
     */
    private int endTimeIndex;

    /**
     * The index of the patient column.
     */
    private int patientIndex;

    /**
     * The index of the customer column.
     */
    private int customerIndex;

    /**
     * The index of the document column.
     */
    private int documentIndex;

    /**
     * The index of the supplier column.
     */
    private int supplierIndex;

    /**
     * The index of the clinician column.
     */
    private int clinicianIndex;

    /**
     * The index of the location column.
     */
    private int locationIndex;

    /**
     * The current patient.
     */
    private Party patient;

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * End time node name.
     */
    private static final String END_TIME = "endTime";

    /**
     * Investigation type node name.
     */
    private static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Patient node name.
     */
    private static final String PATIENT = "patient";

    /**
     * Clinician node name.
     */
    private static final String CLINICIAN = "clinician";

    /**
     * Location node name.
     */
    private static final String LOCATION = "location";

    /**
     * Constructs an {@link InvestigationsTableModel}.
     *
     * @param context the layout context
     */
    public InvestigationsTableModel(LayoutContext context) {
        super(InvestigationsQuery.ARCHETYPES, context);
        rules = ServiceHelper.getBean(PatientRules.class);
        factory = ServiceHelper.getBean(DocumentViewerFactory.class);
    }

    /**
     * Invoked after the table has been rendered.
     */
    @Override
    public void postRender() {
        super.postRender();
        patient = null;
    }

    /**
     * Returns a list of descriptor names to include in the table.
     *
     * @return the list of descriptor names to include in the table
     */
    @Override
    protected String[] getNodeNames() {
        return new String[]{START_TIME, END_TIME, INVESTIGATION_TYPE, PATIENT, "id", "status", "status2",
                            CLINICIAN, LOCATION};
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param act    the object the object
     * @param column the table column
     * @param row    the table row
     * @return the value at the given coordinate
     */
    @Override
    protected Object getValue(Act act, TableColumn column, int row) {
        Object result;
        int index = column.getModelIndex();
        if (index == startTimeIndex) {
            result = formatDateTime(act.getActivityStartTime());
        } else if (index == endTimeIndex) {
            result = formatDateTime(act.getActivityEndTime());
        } else if (index == patientIndex) {
            result = getPatient(act);
        } else if (index == customerIndex) {
            result = getCustomer(act);
        } else if (index == supplierIndex) {
            result = getSupplier(act);
        } else if (index == documentIndex) {
            DocumentViewer viewer = factory.create((DocumentAct) act, true, getLayoutContext());
            viewer.setShowNoDocument(false);
            result = viewer.getComponent();
        } else if (index == clinicianIndex) {
            result = getClinician(act);
        } else if (index == locationIndex) {
            result = getLocation(act);
        } else {
            result = super.getValue(act, column, row);
        }
        return result;
    }

    /**
     * Creates a column model for a set of archetypes.
     *
     * @param shortNames the archetype short names
     * @param context    the layout context
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(String[] shortNames, LayoutContext context) {
        DefaultTableColumnModel model = (DefaultTableColumnModel) super.createColumnModel(shortNames, context);

        startTimeIndex = getModelIndex(model, START_TIME);
        endTimeIndex = getModelIndex(model, END_TIME);
        patientIndex = getModelIndex(model, PATIENT);
        customerIndex = getNextModelIndex(model);
        TableColumn customerColumn = createTableColumn(customerIndex, "investigationstablemodel.customer");
        model.addColumn(customerColumn);
        model.moveColumn(model.getColumnCount() - 1, getColumnOffset(model, PATIENT));

        supplierIndex = getNextModelIndex(model);
        TableColumn supplierColumnn = createTableColumn(supplierIndex, "investigationstablemodel.supplier");
        model.addColumn(supplierColumnn);

        documentIndex = getNextModelIndex(model);
        TableColumn documentColumn = new TableColumn(documentIndex);
        String displayName = getDisplayName(InvestigationArchetypes.PATIENT_INVESTIGATION, "document");
        documentColumn.setHeaderValue(displayName);
        model.addColumn(documentColumn);

        clinicianIndex = getModelIndex(model, CLINICIAN);
        locationIndex = getModelIndex(model, LOCATION);
        return model;
    }

    /**
     * Creates a new column for a node.
     *
     * @param archetypes the archetypes
     * @param name       the node name
     * @param index      the index to assign the column
     * @return a new column
     */
    @Override
    protected TableColumn createColumn(List<ArchetypeDescriptor> archetypes, String name, int index) {
        TableColumn column = super.createColumn(archetypes, name, index);
        if ("id".equals(name)) {
            column.setHeaderValue(Messages.get("investigationstablemodel.requestId"));
        }
        return column;
    }

    /**
     * Initialises the current object.
     *
     * @param object the object
     * @return the current object bean
     */
    @Override
    protected IMObjectBean initCurrent(IMObject object) {
        IMObjectBean bean = super.initCurrent(object);
        patient = (Party) getCached(bean.getTargetRef(PATIENT));
        return bean;
    }

    /**
     * Returns a component representing the owner of the patient at the time the investigation was done.
     *
     * @param act the act
     * @return the customer component. May be {@code null}
     */
    private Component getPatient(Act act) {
        getCurrent(act);
        return (patient != null) ? createViewer(patient.getObjectReference(), patient.getName(), true) : null;
    }

    /**
     * Returns a component representing the owner of the patient at the time the investigation was done.
     *
     * @param act the act
     * @return the customer component. May be {@code null}
     */
    private Component getCustomer(Act act) {
        Component result = null;
        getCurrent(act);
        if (patient != null) {
            Reference reference = rules.getOwnerReference(patient, act.getActivityStartTime());
            if (reference != null) {
                result = createViewer(reference, true);
            }
        }
        return result;
    }

    /**
     * Returns a component representing the supplier for the specified investigation type associated with the act.
     *
     * @param act the act
     * @return the supplier component. May be {@code null}
     */
    private Component getSupplier(Act act) {
        Component result = null;
        IMObjectBean bean = getCurrent(act);
        Entity investigationType = (Entity) getCached(bean.getTargetRef(INVESTIGATION_TYPE));
        if (investigationType != null) {
            IMObjectBean investigationTypeBean = getBean(investigationType);
            Reference ref = investigationTypeBean.getTargetRef("supplier");
            if (ref != null) {
                result = createViewer(ref, true);
            }
        }
        return result;
    }

    /**
     * Returns a component representing the clinician.
     *
     * @param act the investigation
     * @return the component, or {@code null} if there is no clinician
     */
    private Component getClinician(Act act) {
        return createViewer(act, CLINICIAN, false);
    }

    /**
     * Returns a component representing the location where the investigation was created.
     *
     * @param act the investigation
     * @return the component, or {@code null} if there is no location
     */
    private Component getLocation(Act act) {
        return createViewer(act, LOCATION, false);
    }

    /**
     * Returns an object given its reference, from the cache.
     *
     * @param reference the reference. May be {@code null}
     * @return the object. May be {@code null}
     */
    private IMObject getCached(Reference reference) {
        return reference != null ? getLayoutContext().getCache().get(reference) : null;
    }

    /**
     * Formats a date.
     *
     * @param date the date. May be {@code null}
     * @return a time if it is for today, or as a date/time for any other day
     */
    private String formatDateTime(Date date) {
        return (date != null) ? DateFormatter.formatDateTimeAbbrev(date) : null;
    }

}
