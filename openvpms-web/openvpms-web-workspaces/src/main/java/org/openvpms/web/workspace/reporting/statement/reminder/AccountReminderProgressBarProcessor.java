/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement.reminder;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.web.component.processor.ProgressBarProcessor;
import org.openvpms.web.system.ServiceHelper;

/**
 * An {@link ProgressBarProcessor} for processing account reminders returned by a {@link AccountReminderQueryFactory}.
 *
 * @author Tim Anderson
 */
abstract class AccountReminderProgressBarProcessor extends ProgressBarProcessor<ObjectSet> {

    /**
     * Constructs an {@link AccountReminderProgressBarProcessor }.
     */
    public AccountReminderProgressBarProcessor(AccountReminderQueryFactory factory) {
        super(null);
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        ArchetypeQuery query = factory.createQuery();
        query.setMaxResults(0);
        query.setCountResults(true);
        int size = service.get(query).getTotalResults();
        query.setCountResults(false);
        setItems(new AccountReminderQueryIterator(query, 100, service), size);
    }

    /**
     * Processes an object.
     *
     * @param object the object to process
     */
    @Override
    protected void process(ObjectSet object) {
        Act reminder = (Act) object.get("reminder");
        FinancialAct charge = (FinancialAct) object.get("charge");
        process(reminder, charge);
        processCompleted(object);
    }

    /**
     * Processes a reminder.
     *
     * @param reminder the reminder
     * @param charge   the charge
     */
    protected abstract void process(Act reminder, FinancialAct charge);

    /**
     * Notifies the iterator that the iteration has updated.
     */
    protected void updated() {
        getIterator().updated();
    }

    /**
     * Returns the iterator.
     *
     * @return the iterator
     */
    @Override
    protected AccountReminderQueryIterator getIterator() {
        return (AccountReminderQueryIterator) super.getIterator();
    }
}