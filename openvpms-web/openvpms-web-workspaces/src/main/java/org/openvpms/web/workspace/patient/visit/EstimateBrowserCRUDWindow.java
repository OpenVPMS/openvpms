/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.visit;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserFactory;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.patient.estimate.PatientEstimateCRUDWindow;
import org.openvpms.web.workspace.patient.estimate.PatientEstimateQuery;

/**
 * Links an estimates browser to a CRUD window.
 *
 * @author Tim Anderson
 */
public class EstimateBrowserCRUDWindow extends VisitBrowserCRUDWindow<Act> {

    /**
     * Constructs a {@link EstimateBrowserCRUDWindow}.
     *
     * @param customer the customer
     * @param patient  the patient
     * @param editor   the visit editor
     * @param context  the context
     * @param help     the help context
     */
    public EstimateBrowserCRUDWindow(Party customer, Party patient, VisitEditor editor, Context context,
                                     HelpContext help) {
        Query<Act> query = createQuery(customer, patient);
        Browser<Act> browser = BrowserFactory.create(query, new DefaultLayoutContext(context, help));
        setBrowser(browser);

        PatientEstimateCRUDWindow estimateWindow = createEstimateCRUDWindow(editor, context, help);
        setWindow(estimateWindow);
    }

    /**
     * Creates a new CRUD window for estimates.
     *
     * @param editor  the visit editor
     * @param context the context
     * @param help    the help context
     * @return a new CRUD window
     */
    protected PatientEstimateCRUDWindow createEstimateCRUDWindow(VisitEditor editor, Context context, HelpContext help) {
        VisitEstimateCRUDWindow estimateWindow = new VisitEstimateCRUDWindow(context, help);
        estimateWindow.setVisitEditor(editor);
        return estimateWindow;
    }

    /**
     * Creates a new query that returns all estimates for the specified customer and patient.
     *
     * @param customer the customer
     * @param patient  the patient
     * @return a new query
     */
    private Query<Act> createQuery(Party customer, final Party patient) {
        return new PatientEstimateQuery(customer, patient);
    }
}
