/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.CheckBox;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.ImageReference;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.doc.TemporaryDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.laboratory.resource.Content;
import org.openvpms.laboratory.resource.Resource;
import org.openvpms.laboratory.resource.Resources;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.DefaultListMarkModel;
import org.openvpms.web.component.im.table.IMTable;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.table.MapBasedMarkablePagedIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.image.ThumbnailImageReference;
import org.openvpms.web.echo.servlet.DownloadServlet;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Browser for external documents.
 *
 * @author Tim Anderson
 */
public class ExternalDocumentsDialog extends ModalDialog {

    /**
     * The name index.
     */
    public static final int NAME_INDEX = 1;

    /**
     * The page size.
     */
    private final int pageSize;

    /**
     * The tree navigation button row.
     */
    private final Row navigation;

    /**
     * The paged table model.
     */
    private final MapBasedMarkablePagedIMTableModel<String, Resource> pagedModel;

    /**
     * The resource table.
     */
    private final PagedIMTable<Resource> table;

    /**
     * Constructs an {@link ExternalDocumentsDialog}.
     *
     * @param set  the resources iterator
     * @param help the help context. May be {@code null}
     */
    public ExternalDocumentsDialog(ResourceIteratorResultSet set, HelpContext help) {
        super(Messages.get("document.attachexternal.title"), "BrowserDialog", OK_CANCEL, help);
        pageSize = set.getPageSize();
        IMTableModel<Resource> model = new ExternalDocumentTableModel();
        pagedModel = new MapBasedMarkablePagedIMTableModel<>(model, Resource::getId);
        table = new PagedIMTable<>(pagedModel);
        table.setResultSet(set);
        IMTable<Resource> child = table.getTable();
        navigation = RowFactory.create(Styles.CELL_SPACING);
        child.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                Resource selected = child.getSelected();
                if (selected != null) {
                    if (selected instanceof Resources) {
                        select((Resources) selected);
                    } else {
                        pagedModel.setMarked(selected, true);
                        onOK();
                    }
                }
            }
        });
        showNavigation(set.getResources());
        getLayout().add(ColumnFactory.create(Styles.INSET, navigation, table.getComponent()));
        resize();
    }

    /**
     * Returns the selected resources.
     *
     * @return the selected resources
     */
    public Collection<Resource> getSelected() {
        return pagedModel.getMarked();
    }

    /**
     * Resizes the dialog if required.
     */
    @Override
    protected void resize() {
        resize("ExternalDocumentsDialog.size");
    }

    /**
     * Invoked when a resource collection is selected.
     *
     * @param resources the resources
     */
    private void select(Resources resources) {
        ResourceIteratorResultSet child = new ResourceIteratorResultSet(resources, pageSize);
        showNavigation(resources);
        table.setResultSet(child);
    }

    /**
     * Displays the resource navigation buttons.
     *
     * @param resources the resources
     */
    private void showNavigation(Resources resources) {
        navigation.removeAll();
        for (Resources resource : getTree(resources)) {
            Button button = ButtonFactory.text(resource.getName(), () -> select(resource));
            navigation.add(button);
        }
    }

    /**
     * Returns the resources tree, with the root of the tree first.
     *
     * @param resources the resource to start from
     */
    private List<Resources> getTree(Resources resources) {
        List<Resources> result = new ArrayList<>();
        while (resources != null) {
            result.add(0, resources);
            resources = resources.getParent();
        }
        return result;
    }

    private static class ExternalDocumentTableModel extends AbstractIMTableModel<Resource> {
        public ExternalDocumentTableModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(new TableColumn(MARK_INDEX));
            model.addColumn(createTableColumn(NAME_INDEX, NAME));
            setTableColumnModel(model);
            setRowMarkModel(new DefaultListMarkModel());
        }

        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return new SortConstraint[0];
        }

        @Override
        protected Object getValue(Resource object, TableColumn column, int row) {
            Object result = null;
            switch (column.getModelIndex()) {
                case MARK_INDEX:
                    CheckBox mark = getRowMark(row);
                    if (!(object instanceof Content)) {
                        mark.setEnabled(false);
                    }
                    result = mark;
                    break;
                case NAME_INDEX:
                    if (object instanceof Content) {
                        Content content = (Content) object;
                        try {
                            if (content.canScale()) {
                                result = getImageAndLabel(content);
                            }
                        } catch (Throwable exception) {
                            // ignore
                        }
                    }
                    if (result == null) {
                        result = object.getName();
                    }
                    break;
            }
            return result;
        }

        /**
         * Attempts to render a thumbnail image and label for content.
         *
         * @param content the content
         * @return the image and label, or {@code null} if the content cannot be scaled
         */
        private Component getImageAndLabel(Content content) {
            Component result = null;
            int size = StyleSheetHelper.getProperty("thumbnail.size", 64);
            if (size < 10) {
                size = 10;
            }
            ImageReference reference = ThumbnailImageReference.create(content.getContent(size, size),
                                                                      content.getMimeType());
            if (reference != null) {
                Button button = ButtonFactory.create(null, (String) null, () -> {
                    TemporaryDocumentHandler handler
                            = new TemporaryDocumentHandler(ServiceHelper.getArchetypeService());
                    Document document = handler.create(content.getName(), content.getContent(),
                                                       content.getMimeType(), -1);
                    DownloadServlet.startDownload(document);
                });
                button.setIcon(reference);
                button.setToolTipText(Messages.get("document.fullsize"));
                result = RowFactory.create(Styles.CELL_SPACING, button, LabelFactory.text(content.getName()));
            }
            return result;
        }
    }
}
