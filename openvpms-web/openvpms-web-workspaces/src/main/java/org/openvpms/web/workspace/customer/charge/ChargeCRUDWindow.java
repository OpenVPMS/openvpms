/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;


import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.workflow.DefaultTaskListener;
import org.openvpms.web.component.workflow.TaskEvent;
import org.openvpms.web.component.workspace.ActPoster;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;


/**
 * CRUD window for customer charges.
 *
 * @author Tim Anderson
 */
public class ChargeCRUDWindow extends AbstractChargeCRUDWindow {

    /**
     * Constructs a {@link ChargeCRUDWindow}.
     * <p/>
     * This makes the default archetype {@link CustomerAccountArchetypes#INVOICE}.
     *
     * @param archetypes the archetypes that this may create
     * @param context    the context
     * @param help       the help context
     */
    public ChargeCRUDWindow(Archetypes<FinancialAct> archetypes, Context context, HelpContext help) {
        super(Archetypes.create(archetypes.getShortNames(), archetypes.getType(), INVOICE, archetypes.getDisplayName()),
              context, help);
    }

    /**
     * Sets the object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(FinancialAct object) {
        super.setObject(object);
        updateContext(CustomerAccountArchetypes.INVOICE, object);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(createPostButton());
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
        buttons.add(createEnableRemindersButton());
        buttons.add(createDisableRemindersButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(POST_ID, enable);
        enablePrintPreview(buttons, enable);
        enableDisableReminderButtons(buttons, enable);
    }

    /**
     * Returns a {@link ActPoster} to post an act.
     *
     * @param object the act to post
     * @return the {@link ActPoster}
     */
    @Override
    protected ActPoster<FinancialAct> getActPoster(FinancialAct object) {
        HelpContext help = getHelpContext().subtopic("post");
        return new ChargePoster(object, getActions(), createLayoutContext(help));
    }

    /**
     * Invoked when posting of an act is complete.
     * <p/>
     * This prompts to pay the account, and pops up a dialog to print the act.
     *
     * @param act the act
     */
    @Override
    protected void onPosted(FinancialAct act) {
        HelpContext help = getHelpContext().subtopic("post");
        PostedChargeWorkflow workflow = new PostedChargeWorkflow(act, getContext(), help);
        workflow.addTaskListener(new DefaultTaskListener() {
            public void taskEvent(TaskEvent event) {
                onRefresh(act);
            }
        });
        workflow.start();
    }
}
