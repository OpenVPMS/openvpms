/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.messages;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.act.DefaultActCopyHandler;
import org.openvpms.archetype.rules.message.MessageArchetypes;
import org.openvpms.component.business.service.archetype.helper.IMObjectCopier;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.AbstractIMObjectActions;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.workspace.AbstractViewCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerMailContext;


/**
 * Messaging CRUD window.
 *
 * @author Tim Anderson
 */
public class MessagingCRUDWindow extends AbstractViewCRUDWindow<Act> {

    /**
     * The reply button identifier.
     */
    static final String REPLY_ID = "reply";

    /**
     * The forward button identifier.
     */
    static final String FORWARD_ID = "forward";

    /**
     * The completed button identifier.
     */
    static final String COMPLETED_ID = "button.messagecompleted";

    /**
     * Message archetypes that may be created by this workspace.
     */
    private static final Archetypes<Act> MESSAGES = Archetypes.create(MessageArchetypes.USER, Act.class);


    /**
     * Constructs a {@link MessagingCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create
     * @param context    the context
     * @param help       the help context
     */
    public MessagingCRUDWindow(Archetypes<Act> archetypes, Context context, HelpContext help) {
        super(archetypes, Actions.INSTANCE, context, help);
    }

    /**
     * Creates and edits a new object.
     * <p/>
     * This implementation restricts creation to <em>act.userMessage</em> objects.
     */
    @Override
    public void create() {
        onCreate(MESSAGES);
    }

    /**
     * Returns the mail context.
     *
     * @return the mail context. May be {@code null}
     */
    @Override
    public MailContext getMailContext() {
        MailContext context = null;
        if (getObject() != null) {
            context = CustomerMailContext.create(getObject(), getContext(), getHelpContext());
        }
        if (context == null) {
            context = super.getMailContext();
        }
        return context;
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(createNewButton());
        buttons.add(REPLY_ID, this::onReply);
        buttons.add(FORWARD_ID, this::onForward);
        buttons.add(createDeleteButton());
        buttons.add(COMPLETED_ID, this::onCompleted);
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        boolean user = TypeHelper.isA(getObject(), MessageArchetypes.USER);
        buttons.setEnabled(REPLY_ID, enable && user);
        buttons.setEnabled(FORWARD_ID, enable && user);
        buttons.setEnabled(COMPLETED_ID, enable);
        enablePrintPreview(buttons, enable);
    }

    /**
     * Invoked when the object has been saved.
     *
     * @param object the object
     * @param isNew  determines if the object is a new instance
     */
    @Override
    protected void onSaved(Act object, boolean isNew) {
        onRefresh(object);
    }

    /**
     * Invoked when the 'reply' button is pressed.
     */
    private void onReply() {
        Act reply = copyObject();
        IMObjectBean bean = getBean(reply);
        bean.setTarget("to", reply.getCreatedBy());
        HelpContext help = getHelpContext().subtopic("reply").topic(reply, "edit");
        LayoutContext layoutContext = createLayoutContext(help);
        UserMessageEditor editor = new UserMessageEditor(reply, null, layoutContext);
        Message message = new Message(getBean(getObject()));

        editor.setFrom(layoutContext.getContext().getUser());
        String subject = Messages.format("workflow.messaging.reply.subject", message.getSubject());
        String text = Messages.format("workflow.messaging.reply.body", message.getSent(), message.getFromName(),
                                      message.getMessage());
        editor.setSubject(subject);
        editor.setMessage(text);
        edit(editor, Messages.get("workflow.messaging.reply.title"));
    }

    /**
     * Invoked when the 'forward' button is pressed.
     */
    private void onForward() {
        Act forward = copyObject();
        IMObjectBean bean = getBean(forward);
        bean.setTarget("to", (Reference) null);
        HelpContext help = getHelpContext().subtopic("forward").topic(forward, "edit");
        LayoutContext layoutContext = createLayoutContext(help);
        UserMessageEditor editor = new UserMessageEditor(forward, null, layoutContext);
        Message message = new Message(getBean(getObject()));

        String subject = Messages.format("workflow.messaging.forward.subject", message.getSubject());
        String text = Messages.format("workflow.messaging.forward.body", message.getSubject(), message.getSent(),
                                      message.getFromName(), message.getToName(), message.getMessage());
        editor.setSubject(subject);
        editor.setMessage(text);
        edit(editor, Messages.get("workflow.messaging.forward.title"));
    }

    /**
     * Displays a dialog to perform editing.
     *
     * @param editor the editor
     * @param title  the dialog title
     */
    private void edit(IMObjectEditor editor, String title) {
        EditDialog dialog = edit(editor);
        if (dialog != null) {
            dialog.setTitle(title);
        }
    }

    /**
     * Invoked when the 'completed' button is pressed.
     */
    private void onCompleted() {
        Act act = getObject();
        if (!ActStatus.COMPLETED.equals(act.getStatus())) {
            act = IMObjectHelper.reload(act);
            if (act != null) {
                act.setStatus(ActStatus.COMPLETED);
                SaveHelper.save(act);
            }
            onRefresh(act);
        }
    }

    /**
     * Copies the current act.
     *
     * @return a copy of the act
     */
    private Act copyObject() {
        DefaultActCopyHandler handler = new DefaultActCopyHandler();
        handler.setCopy(Act.class, Participation.class);
        IMObjectCopier copier = new IMObjectCopier(handler, ServiceHelper.getArchetypeService());
        return (Act) copier.apply(getObject()).get(0);
    }

    /**
     * Helper to extract message properties.
     */
    private static class Message {

        /**
         * The act.
         */
        private final IMObjectBean bean;

        /**
         * Constructs a {@link Message}.
         *
         * @param bean the message act
         */
        public Message(IMObjectBean bean) {
            this.bean = bean;
        }

        /**
         * Returns the 'from' user.
         *
         * @return the 'from' user. May be {@code null}
         */
        public User getFrom() {
            return (User) bean.getObject("from");
        }

        /**
         * Returns the 'to' user.
         *
         * @return the 'to' user. May be {@code null}
         */
        public User getTo() {
            return bean.getTarget("to", User.class);
        }

        /**
         * Returns the message subject.
         *
         * @return the message subject
         */
        public String getSubject() {
            return bean.getString("description", "");
        }

        /**
         * Returns the message body.
         *
         * @return the message body
         */
        public String getMessage() {
            return bean.getString("message", "");
        }

        /**
         * Returns the 'from' user name.
         *
         * @return the 'from' user name
         */
        public String getFromName() {
            User from = getFrom();
            return (from != null) ? from.getName() : "";
        }

        /**
         * Returns the 'to' user name.
         *
         * @return the 'to' user name
         */
        public String getToName() {
            User to = getTo();
            return (to != null) ? to.getName() : null;
        }

        /**
         * Returns the date when the message was sent.
         *
         * @return the date when the message was sent
         */
        public String getSent() {
            return DateFormatter.formatDateTime(bean.getDate("startTime"));
        }
    }

    private static class Actions extends AbstractIMObjectActions<Act> {

        public static final Actions INSTANCE = new Actions();

        /**
         * Determines if an object can be deleted.
         *
         * @param object the object to check
         * @return {@code true} if the object can be deleted
         */
        @Override
        public boolean canDelete(Act object) {
            return super.canDelete(object) && !TypeHelper.isA(object, MessageArchetypes.AUDIT);
        }
    }
}
