/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.settings;

import echopointng.TabbedPane;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.SplitPane;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.factory.TabbedPaneFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.tabpane.ObjectTabPaneModel;
import org.openvpms.web.resource.i18n.Messages;

/**
 * System settings dialog.
 *
 * @author Tim Anderson
 */
public class SettingsDialog extends ModalDialog {

    /**
     * Model for the settings tabs.
     */
    private final ObjectTabPaneModel<SettingsTab> model;

    /**
     * The tabbed pane.
     */
    private final TabbedPane pane;

    /**
     * The container for the tab row and tab content.
     */
    private final SplitPane container;

    /**
     * The current tab index.
     */
    private int currentIndex;

    /**
     * The current tab.
     */
    private SettingsTab currentTab;

    /**
     * The edit button identifier.
     */
    private static final String EDIT_ID = "button.edit";

    /**
     * Constructs a {@link SettingsDialog}.
     *
     * @param help the help context
     */
    public SettingsDialog(HelpContext help) {
        super(Messages.get("admin.system.settings"), new String[]{EDIT_ID, CLOSE_ID}, help);
        Column tabContainer = ColumnFactory.create(Styles.INSET_Y);
        container = SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM, "TabbedBrowser");
        container.add(tabContainer);
        model = new ObjectTabPaneModel<>(tabContainer);
        addTab(SettingsArchetypes.FIREWALL_SETTINGS);
        if (OpenOfficeSettings.isAvailable()) {
            addTab(new OpenOfficeSettings(help));
        }
        addTab(SettingsArchetypes.PASSWORD_POLICY);
        addTab(SettingsArchetypes.QUERY_SETTINGS);
        addTab(SettingsArchetypes.REPORT_SETTINGS);
        pane = TabbedPaneFactory.create(model);
        currentIndex = -1;
        pane.addPropertyChangeListener(evt -> onTabSelected());
        tabContainer.add(pane);
        getLayout().add(container);
        pane.setSelectedIndex(0);
        resize("SettingsDialog.size");
    }

    /**
     * Returns the help context.
     *
     * @return the help context. May be {@code null}
     */
    @Override
    public HelpContext getHelpContext() {
        HelpContext help = (currentTab != null) ? currentTab.getHelpContext() : null;
        return (help != null) ? help : super.getHelpContext();
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (EDIT_ID.equals(button)) {
            onEdit();
        } else if (!CLOSE_ID.equals(button)) {
            onSettingsButton(button);
        } else {
            super.onButton(button);
        }
    }

    /**
     * Invoked when a settings button is pressed.
     *
     * @param button the button identifier
     */
    private void onSettingsButton(String button) {
        if (currentTab != null) {
            currentTab.onButton(button);
        }
    }

    /**
     * Invoked when a tab is selected.
     */
    private void onTabSelected() {
        int index = pane.getSelectedIndex();
        if (index != currentIndex) {
            currentIndex = index;
            if (container.getComponentCount() == 2) {
                container.remove(1);
            }
            if (currentTab != null) {
                ButtonSet buttons = getButtons();
                for (String button : currentTab.getButtons()) {
                    buttons.remove(button);
                }
            }
            SettingsTab tab = model.getObject(index);
            if (tab != null) {
                if (tab.getButtons().length > 0) {
                    ButtonSet buttons = getButtons();
                    buttons.remove(CLOSE_ID);
                    for (String button : tab.getButtons()) {
                        addButton(button);
                    }
                    addButton(CLOSE_ID);
                }
                try {
                    container.add(tab.getComponent());
                    tab.show();
                    currentTab = tab;
                } catch (Throwable exception) {
                    currentTab = null;
                    ErrorHelper.show(exception);
                }
            }
        }
    }

    /**
     * Edits the settings for the displayed tab.
     */
    private void onEdit() {
        SettingsTab tab = model.getObject(currentIndex);
        if (tab != null) {
            tab.edit();
        }
    }

    /**
     * Adds a tab for a settings archetype.
     *
     * @param archetype the settings archetype
     */
    private void addTab(String archetype) {
        SettingsTab tab = new SettingsTab(archetype, getHelpContext());
        addTab(tab);
    }

    /**
     * Adds a settings tab.
     *
     * @param tab the tab
     */
    private void addTab(SettingsTab tab) {
        model.addTab(tab, tab.getTitle(), new Label());
    }
}
