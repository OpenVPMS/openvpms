/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.i18n.Messages;

/**
 * An exception for account reminders.
 *
 * @author Tim Anderson
 */
public class AccountReminderException extends OpenVPMSException {

    /**
     * Constructs an {@link AccountReminderException}.
     *
     * @param message the exception message
     * @param cause   the cause
     */
    protected AccountReminderException(String message, Throwable cause) {
        super(Messages.create(message), cause);
    }
}