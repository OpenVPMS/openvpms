/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.order;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.finance.order.OrderArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.DefaultContextSwitchListener;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserFactory;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerSummary;
import org.openvpms.web.workspace.patient.summary.CustomerPatientSummaryFactory;

/**
 * Customer order workspace.
 *
 * @author Tim Anderson
 */
public class CustomerOrderWorkspace extends ResultSetCRUDWorkspace<FinancialAct> {

    /**
     * THe user preferences.
     */
    private final Preferences preferences;

    /**
     * The archetypes that this workspace operates on.
     */
    private static final String[] SHORT_NAMES = {OrderArchetypes.ORDERS, OrderArchetypes.RETURNS};

    /**
     * Constructs a {@link CustomerOrderWorkspace}.
     * <p/>
     * The {@link #setArchetypes} method must be invoked to set archetypes that the workspace supports, before
     * performing any operations.
     *
     * @param context     the context
     * @param mailContext the mail context
     * @param preferences user preferences
     */
    public CustomerOrderWorkspace(Context context, MailContext mailContext, Preferences preferences) {
        super("workflow.order", context);
        this.preferences = preferences;
        setArchetypes(Archetypes.create(SHORT_NAMES, FinancialAct.class, Messages.get("workflow.order.type")));
        setMailContext(mailContext);
    }

    /**
     * Renders the workspace summary.
     *
     * @return the component representing the workspace summary, or {@code null} if there is no summary
     */
    @Override
    public Component getSummary() {
        Component result = null;
        CustomerOrderCRUDWindow window = (CustomerOrderCRUDWindow) getCRUDWindow();
        Party customer = (window != null) ? window.getCustomer() : null;
        if (customer != null) {
            CustomerPatientSummaryFactory factory = ServiceHelper.getBean(CustomerPatientSummaryFactory.class);
            CustomerSummary summary = factory.createCustomerSummary(getContext(), getHelpContext(), preferences);
            result = summary.getSummary(customer);
        }
        return result;
    }

    /**
     * Creates a new browser.
     *
     * @param query the query
     * @return a new browser
     */
    @Override
    protected Browser<FinancialAct> createBrowser(Query<FinancialAct> query) {
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(getContext(), getHelpContext());
        layoutContext.setContextSwitchListener(DefaultContextSwitchListener.INSTANCE);
        return BrowserFactory.create(query, layoutContext);
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected Query<FinancialAct> createQuery() {
        return new CustomerOrderQuery(SHORT_NAMES, new DefaultLayoutContext(getContext(), getHelpContext()));
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<FinancialAct> createCRUDWindow() {
        QueryBrowser<FinancialAct> browser = getBrowser();
        return new CustomerOrderCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(), getContext(),
                                           getHelpContext());
    }

    /**
     * Invoked when a browser object is selected.
     * <p/>
     * This implementation sets the object in the CRUD window and if it has been double-clicked:
     * <ul>
     * <li>pops up an editor, if editing is supported; otherwise
     * <li>pops up a viewer
     * </li>
     *
     * @param object the selected object
     */
    @Override
    protected void onBrowserSelected(FinancialAct object) {
        super.onBrowserSelected(object);
        if (updateSummaryOnChildUpdate()) {
            firePropertyChange(SUMMARY_PROPERTY, null, null);
        }
    }
}
