/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.PrintActTask;
import org.openvpms.web.component.workflow.ReloadTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.workspace.customer.CustomerMailContext;
import org.openvpms.web.workspace.workflow.payment.PaymentWorkflow;

import java.math.BigDecimal;

/**
 * Workflow to accept a gap payment or refund on a gap claim.
 *
 * @author Tim Anderson
 */
class GapClaimPaymentRefundWorkflow extends PaymentWorkflow {

    /**
     * Determines if a payment or refund is being created.
     */
    private final boolean payment;

    /**
     * The amount already paid.
     */
    private final BigDecimal paid;

    /**
     * The claim.
     */
    private final GapClaimImpl claim;

    /**
     * Construct a {@link GapClaimPaymentRefundWorkflow}.
     *
     * @param amount  the amount to pay or refund
     * @param payment if {@code true} create a payment, else create a refund
     * @param paid    the amount already paid
     * @param claim   the claim
     * @param context the context
     * @param help    the help context
     */
    public GapClaimPaymentRefundWorkflow(BigDecimal amount, boolean payment, BigDecimal paid, GapClaimImpl claim,
                                         Context context, HelpContext help) {
        super(amount, false, context, help);
        this.payment = payment;
        this.paid = paid;
        this.claim = claim;
    }

    /**
     * Starts the task.
     *
     * @param context the task context
     */
    @Override
    public void start(TaskContext context) {
        super.start(context);
        String archetype = (payment) ? CustomerAccountArchetypes.PAYMENT : CustomerAccountArchetypes.REFUND;
        addTask(new ReloadTask(archetype));  // make sure the latest version is available before printing

        PrintActTask print = new PrintActTask(archetype, new CustomerMailContext(getContext())) {
            @Override
            protected void notifyPrintCancelled() {
                notifySkipped();
            }

            @Override
            protected void notifyPrintCancelledOnError(Throwable cause) {
                // if the print fails, don't cancel the workflow
                ErrorHelper.show(cause);
                notifySkipped();
            }
        };
        print.setRequired(false);
        print.setEnableSkip(false); // don't want a skip button. Cancel will act as skip
        addTask(print);
    }

    /**
     * Creates a payment task.
     *
     * @param amount the charge amount that triggered the payment workflow
     * @return a new payment task
     */
    @Override
    protected EditIMObjectTask createPaymentTask(BigDecimal amount) {
        return new GapPaymentRefundEditTask(claim, amount, payment, paid);
    }
}
