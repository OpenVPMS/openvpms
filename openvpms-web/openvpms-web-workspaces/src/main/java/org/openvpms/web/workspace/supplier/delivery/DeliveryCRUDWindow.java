/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.supplier.delivery;

import org.openvpms.archetype.rules.supplier.OrderRules;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActEditDialog;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.workspace.ActPoster;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.supplier.SupplierHelper;
import org.openvpms.web.workspace.supplier.order.ESCISupplierCRUDWindow;


/**
 * CRUD window for supplier deliveries.
 *
 * @author Tim Anderson
 */
public class DeliveryCRUDWindow extends ESCISupplierCRUDWindow {

    /**
     * The order rules.
     */
    private final OrderRules rules;

    /**
     * Invoice button identifier.
     */
    private static final String INVOICE_ID = "invoice";

    /**
     * Reverse button identifier.
     */
    private static final String REVERSE_ID = "reverse";


    /**
     * Constructs a {@link DeliveryCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create
     * @param help       the help context
     */
    public DeliveryCRUDWindow(Archetypes<FinancialAct> archetypes, Context context, HelpContext help) {
        super(archetypes, DeliveryActions.INSTANCE, context, help);
        rules = SupplierHelper.createOrderRules(context.getPractice());
    }

    /**
     * Invoked when a new object has been created.
     *
     * @param act the new act
     */
    @Override
    protected void onCreated(final FinancialAct act) {
        boolean delivery = TypeHelper.isA(act, SupplierArchetypes.DELIVERY);
        LayoutContext context = new DefaultLayoutContext(getContext(), getHelpContext());
        final OrderTableBrowser browser = new OrderTableBrowser(delivery, context);
        String displayName = getDisplayName(act);
        String title = Messages.format("supplier.delivery.selectorders.title", displayName);
        String message = Messages.format("supplier.delivery.selectorders.message", displayName);
        HelpContext help = getHelpContext().subtopic("select");
        PopupDialog dialog = new OrderSelectionBrowserDialog(title, message, browser, help);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            public void onOK() {
                onCreated(act, browser);
            }
        });
        dialog.show();
    }

    /**
     * Invoked when an act is posted.
     *
     * @param act the act
     */
    @Override
    protected void onPosted(FinancialAct act) {
        if (act.isA(SupplierArchetypes.DELIVERY)) {
            onInvoice(act);
        } else {
            onCredit(act);
        }
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(createPostButton());
        buttons.add(createPrintButton());
        buttons.add(createMailButton());
        buttons.add(INVOICE_ID, () -> onInvoice(getObject()));
        buttons.add(REVERSE_ID, this::onReverse);
        buttons.add(createCheckInboxButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        boolean editEnabled = false;
        boolean deleteEnabled = false;
        boolean postEnabled = false;
        boolean invoiceEnabled = false;
        boolean reverseEnabled = false;
        if (enable) {
            FinancialAct object = getObject();
            DeliveryActions operations = (DeliveryActions) getActions();
            editEnabled = operations.canEdit(object);
            deleteEnabled = operations.canDelete(object);
            postEnabled = operations.canPost(object);
            invoiceEnabled = operations.canInvoice(object);
            reverseEnabled = !postEnabled; // can only reverse posted acts
        }
        buttons.setEnabled(EDIT_ID, editEnabled);
        buttons.setEnabled(DELETE_ID, deleteEnabled);
        buttons.setEnabled(POST_ID, postEnabled);
        enablePrintPreview(buttons, enable);
        buttons.setEnabled(INVOICE_ID, invoiceEnabled);
        buttons.setEnabled(REVERSE_ID, reverseEnabled);
    }

    /**
     * Creates a new edit dialog with Apply button disabled for <em>POSTED</em> acts, to workaround OVPMS-733.
     *
     * @param editor the editor
     */
    @Override
    protected EditDialog createEditDialog(IMObjectEditor editor) {
        return new ActEditDialog(editor, getContext());
    }

    /**
     * Returns a {@link ActPoster} to post an act.
     *
     * @param object the act to post
     * @return the {@link ActPoster}
     */
    @Override
    protected ActPoster<FinancialAct> getActPoster(FinancialAct object) {
        HelpContext help = getHelpContext().subtopic("post");
        return new DeliveryPoster(object, createLayoutContext(help));
    }

    /**
     * Invoked when a delivery or return is created.
     * <p/>
     * Creates delivery/return items for each of the selected order items, and adds them to the supplied act.
     *
     * @param act     the delivery/return
     * @param browser the order browser
     */
    private void onCreated(FinancialAct act, OrderTableBrowser browser) {
        addParticipations(act, browser.getSupplier(), browser.getStockLocation());
        HelpContext edit = createEditTopic(act);
        DeliveryEditor editor = new DeliveryEditor(act, null, createLayoutContext(edit));
        for (FinancialAct orderItem : browser.getSelectedOrderItems()) {
            editor.createItem(orderItem);
        }
        edit(editor);
    }

    /**
     * Invoked when the "Invoice" button is pressed.
     *
     * @param act the delivery act
     */
    private void onInvoice(Act act) {
        Act object = IMObjectHelper.reload(act);
        if (object != null) {
            String title = Messages.get("supplier.delivery.invoice.title");
            String message = Messages.get("supplier.delivery.invoice.message");
            ConfirmationDialog dialog = new ConfirmationDialog(title, message, getHelpContext().subtopic("invoice"));
            dialog.addWindowPaneListener(new PopupDialogListener() {
                public void onOK() {
                    invoice(object);
                }
            });
            dialog.show();
        }
    }

    /**
     * Invoked when a return is posted.
     *
     * @param act the return act
     */
    private void onCredit(Act act) {
        Act object = IMObjectHelper.reload(act);
        if (object != null) {
            String title = Messages.get("supplier.delivery.credit.title");
            String message = Messages.get("supplier.delivery.credit.message");
            ConfirmationDialog dialog = new ConfirmationDialog(title, message, getHelpContext().subtopic("credit"));
            dialog.addWindowPaneListener(new PopupDialogListener() {
                public void onOK() {
                    credit(object);
                }
            });
            dialog.show();
        }
    }

    /**
     * Reverse a delivery or return.
     */
    private void onReverse() {
        FinancialAct act = IMObjectHelper.reload(getObject());
        if (act != null) {
            String title;
            String message;
            if (TypeHelper.isA(act, SupplierArchetypes.DELIVERY)) {
                title = Messages.get("supplier.delivery.reverseDelivery.title");
                message = Messages.get("supplier.delivery.reverseDelivery.message");
            } else {
                title = Messages.get("supplier.delivery.reverseReturn.title");
                message = Messages.get("supplier.delivery.reverseReturn.message");
            }
            ConfirmationDialog dialog = new ConfirmationDialog(title, message, getHelpContext().subtopic("reverse"));
            dialog.addWindowPaneListener(new PopupDialogListener() {
                public void onOK() {
                    reverse(act);
                }
            });
            dialog.show();
        }
    }

    /**
     * Invoices a supplier.
     *
     * @param act the delivery
     */
    private void invoice(Act act) {
        if (rules.isInvoiced(act)) {
            InformationDialog.show(Messages.get("supplier.delivery.invoiced.message"));
        } else {
            rules.invoiceSupplier(act);
        }
    }

    /**
     * Credits a supplier from a return.
     *
     * @param act the return
     */
    private void credit(Act act) {
        if (rules.isCredited(act)) {
            InformationDialog.show(Messages.get("supplier.delivery.credited.message"));
        } else {
            rules.creditSupplier(act);
        }
    }

    /**
     * Reverse a delivery/return.
     *
     * @param act the delivery/return to reverse
     */
    private void reverse(FinancialAct act) {
        FinancialAct reversal;
        if (act.isA(SupplierArchetypes.DELIVERY)) {
            reversal = rules.reverseDelivery(act);
        } else {
            reversal = rules.reverseReturn(act);
        }
        onSaved(reversal, false);
    }
}
