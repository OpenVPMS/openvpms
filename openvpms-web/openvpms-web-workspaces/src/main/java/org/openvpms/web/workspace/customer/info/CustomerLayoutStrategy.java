/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.info;

import nextapp.echo2.app.Row;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.workspace.customer.CustomerEditor;

/**
 * Layout strategy for <em>party.customerperson</em>.
 *
 * @author Tim Anderson
 */
public class CustomerLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Constructs a {@link CustomerLayoutStrategy}.
     */
    public CustomerLayoutStrategy() {
        super(ArchetypeNodes.all().exclude(CustomerEditor.REFERRED_BY_CUSTOMER));
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        Property property = properties.get(CustomerEditor.REFERRAL);
        ComponentState referral = createComponent(property, object, context);
        ComponentState customer = createComponent(properties.get(CustomerEditor.REFERRED_BY_CUSTOMER), object, context);
        Row row = RowFactory.create(Styles.CELL_SPACING, referral.getComponent(), customer.getComponent());
        if (context.isEdit()) {
            property.addModifiableListener(modifiable -> {
                updateReferral(property, customer);
            });
        }
        updateReferral(property, customer);
        FocusGroup focus = new FocusGroup(property.getName());
        focus.add(referral.getComponent());
        if (customer.getFocusGroup() != null) {
            focus.add(customer.getFocusGroup());
        } else {
            focus.add(customer.getComponent());
        }
        addComponent(new ComponentState(row, property, focus));
        return super.apply(object, properties, parent, context);
    }

    /**
     * Makes the <em>referredByCustomer</em> node visible if the <em>referral</em> node is "CUSTOMER", else hides it.
     *
     * @param referral the referral node
     * @param customer the referredByCustomer node
     */
    private void updateReferral(Property referral, ComponentState customer) {
        customer.getComponent().setVisible("CUSTOMER".equals(referral.getString()));
    }
}
