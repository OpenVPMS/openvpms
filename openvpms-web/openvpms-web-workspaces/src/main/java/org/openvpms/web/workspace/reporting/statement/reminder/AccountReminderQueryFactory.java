/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement.reminder;

import org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes;
import org.openvpms.archetype.rules.practice.Location;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.JoinConstraint;
import org.openvpms.component.system.common.query.ObjectSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSet;

import java.util.Date;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.gte;
import static org.openvpms.component.system.common.query.Constraints.join;
import static org.openvpms.component.system.common.query.Constraints.shortName;
import static org.openvpms.component.system.common.query.Constraints.sort;

/**
 * A factory for creating queries operating on <em>act.customerChargeReminder*</em> archetypes.
 * <p>
 * The queries return {@link ObjectSet}s containing:
 * <ul>
 * <li>reminder - the <em>act.customerChargeReminder*</em></li>
 * <li>charge - the associated <em>act.customerAccountCharges*</em></li>
 * <li>customer - the customer linked to <em>charge</em></li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class AccountReminderQueryFactory {

    /**
     * The archetypes to query.
     */
    private final String[] archetypes;

    /**
     * The from date.
     */
    private Date from;

    /**
     * The to date.
     */
    private Date to;

    /**
     * The statuses.
     */
    private String[] statuses;

    /**
     * The practice location.
     */
    private Location location;

    /**
     * Status node name.
     */
    private static final String STATUS = "status";

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * Constructs an {@link AccountReminderQueryFactory}.
     */
    public AccountReminderQueryFactory() {
        archetypes = new String[]{AccountReminderArchetypes.CHARGE_REMINDER_SMS};
    }

    /**
     * Sets the from date.
     *
     * @param from the from date. May be {@code null}
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * Sets the to date.
     *
     * @param to the to date. May be {@code null}
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * Sets the statuses.
     *
     * @param statuses the statuses
     */
    public void setStatuses(String[] statuses) {
        this.statuses = statuses;
    }

    /**
     * Sets the location to query.
     * <p>
     * Defaults to {@link Location#ALL}.
     *
     * @param location the location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    public ArchetypeQuery createQuery() {
        final String reminderAlias = "reminder";
        final String chargeAlias = "charge";
        final String customerAlias = "customer";
        ArchetypeQuery query = new ArchetypeQuery(shortName(reminderAlias, archetypes, false));
        query.add(new ObjectSelectConstraint(reminderAlias));
        query.add(new ObjectSelectConstraint(chargeAlias));
        query.add(new ObjectSelectConstraint(customerAlias));
        if (from != null) {
            query.add(gte(START_TIME, from));
        }
        if (to != null) {
            query.add(Constraints.lt(START_TIME, to));
        }
        if (statuses != null && statuses.length != 0) {
            query.add(Constraints.in(STATUS, (Object[]) statuses));
        }
        JoinConstraint charge = join("source", chargeAlias);
        JoinConstraint customer = join("entity", customerAlias);
        charge.add(join("customer", "p_customer").add(customer));

        query.add(join("charge", "r_charge").add(charge));

        if (location != null && location.getPracticeLocation() != null) {
            charge.add(join("location", "l2").add(eq("entity", location.getPracticeLocation())));
        }

        query.add(sort(reminderAlias, START_TIME));
        query.add(sort(reminderAlias, "id"));
        return query;
    }
}