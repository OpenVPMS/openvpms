/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.apache.commons.lang.StringUtils;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableModel;

import java.util.List;

/**
 * Table model for <em>act.smsMessage</em>, <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class AbstractSMSTableModel extends DescriptorTableModel<Act> {

    /**
     * The index of the contact column.
     */
    private int contactIndex;

    /**
     * The index of the customer column.
     */
    private int customerIndex;

    /**
     * The index of the patient column.
     */
    private int patientIndex;

    /**
     * The index of the location column.
     */
    private int locationIndex;

    /**
     * The message index.
     */
    private int messageIndex;

    /**
     * Contact node name.
     */
    static final String CONTACT = "contact";

    /**
     * Message node name.
     */
    static final String MESSAGE = "message";

    /**
     * Customer node name.
     */
    static final String CUSTOMER = "customer";

    /**
     * Patient node name.
     */
    static final String PATIENT = "patient";

    /**
     * Location node name.
     */
    static final String LOCATION = "location";

    /**
     * Constructs an {@link AbstractSMSTableModel}.
     *
     * @param archetype the SMS archetype
     * @param context   the layout context
     */
    public AbstractSMSTableModel(String archetype, LayoutContext context) {
        super(new String[]{archetype}, context);
    }

    /**
     * Creates a column model for one or more archetypes.
     * If there are multiple archetypes, the intersection of the descriptors
     * will be used.
     *
     * @param archetypes the archetypes
     * @param context    the layout context
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(List<ArchetypeDescriptor> archetypes, LayoutContext context) {
        TableColumnModel model = super.createColumnModel(archetypes, context);
        contactIndex = getModelIndex(model, CONTACT);
        messageIndex = getModelIndex(model, MESSAGE);
        customerIndex = getModelIndex(model, CUSTOMER);
        patientIndex = getModelIndex(model, PATIENT);
        locationIndex = getModelIndex(model, LOCATION);
        return model;
    }

    /**
     * Returns an {@link ArchetypeNodes} that determines what nodes appear in the table.
     *
     * @return the nodes to include
     */
    @Override
    protected ArchetypeNodes getArchetypeNodes() {
        return ArchetypeNodes.nodes("startTime", CONTACT, "phone", "status", MESSAGE, CUSTOMER, PATIENT, "reason",
                                    LOCATION);
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the table column
     * @param row    the table row
     */
    @Override
    protected Object getValue(Act object, TableColumn column, int row) {
        Object result;
        int index = column.getModelIndex();
        if (index == contactIndex) {
            result = createViewer(object, CONTACT, true);
        } else if (index == messageIndex) {
            result = getMessage(object);
        } else if (index == customerIndex) {
            result = createViewer(object, CUSTOMER, true);
        } else if (index == patientIndex) {
            result = createViewer(object, PATIENT, true);
        } else if (index == locationIndex) {
            result = createViewer(object, LOCATION, false);
        } else {
            result = super.getValue(object, column, row);
        }
        return result;
    }

    private Object getMessage(Act object) {
        IMObjectBean bean = getCurrent(object);
        return StringUtils.abbreviate(bean.getString(MESSAGE), 20);
    }

}
