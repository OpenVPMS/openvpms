/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import echopointng.TabbedPane;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.SplitPane;
import nextapp.echo2.app.event.ChangeEvent;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.version.Version;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.PracticeMailContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.mail.MailEditor;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ChangeListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.factory.TabbedPaneFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.tabpane.TabPaneModel;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.system.diagnostics.logging.LoggingConfig;
import org.openvpms.web.workspace.admin.system.diagnostics.logging.LoggingDialog;
import org.openvpms.web.workspace.admin.system.settings.OpenOfficeSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.openvpms.web.component.workspace.AbstractCRUDWindow.MAIL_ID;

/**
 * Dialog to display diagnostics.
 *
 * @author Tim Anderson
 */
public class DiagnosticsDialog extends ModalDialog {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The tabs.
     */
    private final List<DiagnosticTab> tabs = new ArrayList<>();

    /**
     * The logging button.
     */
    private final Button logging;

    /**
     * The reload log4j button.
     */
    private final Button log4j;

    /**
     * The tabbed pane.
     */
    private final TabbedPane pane;

    /**
     * The container.
     */
    private final Component container;

    /**
     * The refresh button identifier.
     */
    private static final String REFRESH_ID = "button.refresh";

    /**
     * The logging button identifier.
     */
    private static final String LOGGING_ID = "button.logging";

    /**
     * The 'Reload Log4j configuration' button identifier.
     */
    private static final String RELOAD_LOG4J = "button.reloadlog4j";

    /**
     * Constructs a {@link DiagnosticsDialog}.
     *
     * @param context the context
     * @param help    the help context
     */
    public DiagnosticsDialog(Context context, HelpContext help) {
        super(Messages.get("admin.system.diagnostic.title"), "BrowserDialog", OK, help);

        this.context = context;
        this.help = help;
        container = SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_TOP_BOTTOM, "TabbedBrowser");
        Column tabContainer = ColumnFactory.create(Styles.INSET_Y);

        tabs.addAll(Arrays.asList(new MemoryViewer(), new CacheViewer(), new PropertiesViewer(), new ThreadViewer(),
                                  new DatabaseProcessViewer(), new InnoDBStatusViewer(), new PluginViewer()));
        if (OpenOfficeSettings.isAvailable()) {
            tabs.add(new OpenOfficeViewer());
        }
        tabs.add(new LogViewer());

        TabPaneModel model = new TabPaneModel(tabContainer);

        for (DiagnosticTab tab : tabs) {
            model.addTab(tab.getName(), new Label());
        }

        pane = TabbedPaneFactory.create(model);

        ButtonSet buttons = getButtons();
        buttons.add(REFRESH_ID, this::onRefresh);
        buttons.add(MAIL_ID, (Runnable) this::onMail);
        logging = buttons.add(LOGGING_ID, this::onLogging);
        log4j = buttons.add(RELOAD_LOG4J, this::onReloadLog);
        pane.getSelectionModel().addChangeListener(new ChangeListener() {
            @Override
            public void onChange(ChangeEvent event) {
                onTabSelected(pane.getSelectedIndex());
            }
        });
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        container.add(ColumnFactory.create(Styles.INSET_Y, pane));
        getLayout().add(container);
        onTabSelected(0);
    }

    /**
     * Changes tabs.
     *
     * @param index the tab index
     */
    private void onTabSelected(int index) {
        if (container.getComponentCount() == 2) {
            container.remove(1);
        }
        if (index >= 0 && index < tabs.size()) {
            DiagnosticTab tab = tabs.get(index);
            container.add(tab.getComponent());

            boolean loggingTab = tab instanceof LogViewer;
            logging.setVisible(loggingTab);
            log4j.setVisible(loggingTab);
        }
    }

    /**
     * Refreshes the selected tab.
     */
    private void onRefresh() {
        int index = pane.getSelectedIndex();
        if (index >= 0 && index < tabs.size()) {
            tabs.get(index).refresh();
            onTabSelected(index);
        }
    }

    /**
     * Emails the diagnostics.
     */
    private void onMail() {
        ConfirmationDialog.show(Messages.get("admin.system.diagnostic.mail.title"),
                                Messages.get("admin.system.diagnostic.mail.message"),
                                ConfirmationDialog.YES_NO, new PopupDialogListener() {
                    @Override
                    public void onYes() {
                        onMail(true);
                    }

                    @Override
                    public void onNo() {
                        onMail(false);
                    }
                });
    }

    /**
     * Emails the diagnostics.
     *
     * @param includeLogs if {@code true}, include the logs
     */
    private void onMail(boolean includeLogs) {
        MailDialog dialog = new MailDialog(new PracticeMailContext(context), null, new DefaultLayoutContext(context,
                                                                                                            help));
        MailEditor editor = dialog.getMailEditor();
        editor.setSubject("Diagnostics for OpenVPMS " + Version.VERSION + "(" + Version.REVISION + ")");
        dialog.show(); // show the dialog so any errors collecting attachments display on top.
        for (DiagnosticTab tab : tabs) {
            if (tab instanceof LogViewer) {
                if (includeLogs) {
                    LogViewer viewer = (LogViewer) tab;
                    for (String file : viewer.getFiles()) {
                        Document document = viewer.getDocument(file);
                        if (document != null) {
                            editor.addAttachment(document);
                        }
                    }
                }
            } else {
                Document document = tab.getDocument();
                if (document != null) {
                    editor.addAttachment(document);
                }
            }
        }
    }

    /**
     * Displays the logging configuration dialog.
     */
    private void onLogging() {
        LoggingDialog dialog = new LoggingDialog();
        dialog.show();
    }

    /**
     * Reloads the log4j configuration file.
     */
    private void onReloadLog() {
        ServiceHelper.getBean(LoggingConfig.class).reset();
        InformationDialog.show(Messages.get("admin.system.diagnostic.reloadlog4j"));
    }

}
