/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.im.util.LookupNameHelper;

import java.util.Map;

/**
 * Table model for <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class TopLevelSMSReplyTableModel extends AbstractSMSTableModel {

    /**
     * Cache of status names, keyed on code.
     */
    private final Map<String, String> statuses;

    /**
     * Status node.
     */
    private static final String STATUS = "status";

    /**
     * Constructs an {@link TopLevelSMSReplyTableModel}.
     *
     * @param context the layout context
     */
    public TopLevelSMSReplyTableModel(LayoutContext context) {
        super(SMSArchetypes.REPLY, context);
        statuses = LookupNameHelper.getLookupNames(SMSArchetypes.REPLY, STATUS);
    }

    /**
     * Returns an {@link ArchetypeNodes} that determines what nodes appear in the table.
     *
     * @return the nodes to include
     */
    @Override
    protected ArchetypeNodes getArchetypeNodes() {
        return ArchetypeNodes.nodes("startTime", CONTACT, "phone", MESSAGE, CUSTOMER, PATIENT, LOCATION, STATUS);
    }

    /**
     * Returns a value for a given column.
     *
     * @param object the object to operate on
     * @param column the column
     * @param row    the row
     * @return the value for the column
     */
    @Override
    protected Object getValue(Act object, DescriptorTableColumn column, int row) {
        if (STATUS.equals(column.getName())) {
            return getStatus(object);
        }
        return super.getValue(object, column, row);
    }

    /**
     * Returns the message status. This treats {@link MessageStatus#READ} as the same as {@link MessageStatus#PENDING}.
     *
     * @param object the message
     * @return the status
     */
    private String getStatus(Act object) {
        String status = object.getStatus();
        if (MessageStatus.READ.equals(status)) {
            status = MessageStatus.PENDING;
        }
        return statuses.get(status);
    }

}
