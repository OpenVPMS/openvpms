/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.openoffice;

import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.settings.SettingsArchetypes;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.openoffice.OpenOfficeConfig;
import org.openvpms.report.openoffice.OpenOfficeService;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

/**
 * OpenOffice status.
 *
 * @author Tim Anderson
 */
public class OpenOfficeStatus {

    /**
     * The OpenOffice service.
     */
    private final OpenOfficeService officeService;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The OpenOffice home directory.
     */
    private final SimpleProperty home;

    /**
     * The ports.
     */
    private final SimpleProperty ports;

    /**
     * The maximum no. of tasks per process before restart.
     */
    private final SimpleProperty maxTasks;

    /**
     * Property to indicate if OpenOffice is running.
     */
    private final SimpleProperty running;


    /**
     * The component.
     */
    private Component component;

    /**
     * Constructs an {@link OpenOfficeStatus}.
     *
     * @param officeService the OpenOffice service
     * @param service       the archetype service
     */
    public OpenOfficeStatus(OpenOfficeService officeService, ArchetypeService service) {
        this.officeService = officeService;
        this.service = service;
        home = new SimpleProperty("home", null, String.class, getDisplayName(OpenOfficeConfig.PATH), true);
        ports = new SimpleProperty("ports", null, String.class, getDisplayName(OpenOfficeConfig.PORTS), true);
        maxTasks = new SimpleProperty("maxTasks", null, Integer.class,
                                      getDisplayName(OpenOfficeConfig.MAX_TASKS), true);
        running = new SimpleProperty("running", null, String.class, Messages.get("admin.system.openoffice.running"),
                                     true);
    }

    /**
     * Returns the OpenOffice home directory property.
     *
     * @return the OpenOffice home directory property
     */
    public Property getHome() {
        return home;
    }

    /**
     * Returns the property indicating the ports OpenOffice is running on.
     *
     * @return the ports property
     */
    public Property getPorts() {
        return ports;
    }

    /**
     * Returns the property indicating the maximum no. of tasks per process before restarting OpenOffice.
     *
     * @return the max tasks property
     */
    public Property getMaxTasksPerProcess() {
        return maxTasks;
    }

    /**
     * Returns the property indicating if OpenOffice is running.
     *
     * @return the running property
     */
    public Property getRunning() {
        return running;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        if (component == null) {
            ComponentGrid grid = new ComponentGrid();
            grid.add(LabelFactory.text(getDisplayName(OpenOfficeConfig.PATH)),
                     BoundTextComponentFactory.create(home, 50));
            grid.add(LabelFactory.text(getDisplayName(OpenOfficeConfig.PORTS)),
                     BoundTextComponentFactory.create(ports, 20));
            grid.add(LabelFactory.text(getDisplayName(OpenOfficeConfig.MAX_TASKS)),
                     BoundTextComponentFactory.createNumeric(maxTasks, 5));
            grid.add(LabelFactory.create("admin.system.openoffice.running"),
                     BoundTextComponentFactory.create(running, 5));
            component = grid.createGrid();
            refresh();
        }
        return component;
    }

    /**
     * Refreshes the status.
     */
    public void refresh() {
        OpenOfficeConfig config = officeService.getConfig();
        home.setValue(config.getPath());
        ports.setValue(StringUtils.join(config.getPorts(), ", "));
        maxTasks.setValue(config.getMaxTasksPerProcess());
        running.setValue(Messages.format("yesOrNo", officeService.isRunning() ? 1 : 0));
    }

    /**
     * Returns the display name of a node.
     *
     * @param node the node
     * @return the display name
     */
    private String getDisplayName(String node) {
        return DescriptorHelper.getDisplayName(SettingsArchetypes.OPEN_OFFICE, node, service);
    }
}
