/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.history;

import org.apache.commons.collections4.CollectionUtils;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.act.ActHierarchyIterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/**
 * Flattens patient history.
 * <p/>
 * This merges communication acts into the patient history where they occur during a visit/problem.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPatientHistoryFlattener {

    /**
     * The primary archetype.
     */
    private final String archetype;

    /**
     * Determines if communications should be included.
     */
    private final boolean includeCommunications;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Communication reason node.
     */
    private static final String REASON = "reason";

    /**
     * Constructs an {@link AbstractPatientHistoryFlattener}.
     *
     * @param archetype             the primary archetype (e.g. visit or problem)
     * @param includeCommunications determines if communications should be included or excluded
     * @param service               the archetype service
     */
    public AbstractPatientHistoryFlattener(String archetype, boolean includeCommunications, ArchetypeService service) {
        this.archetype = archetype;
        this.includeCommunications = includeCommunications;
        this.service = service;
    }

    /**
     * Flattens an act hierarchy, only including those acts matching the supplied archetypes.
     *
     * @param objects             the top-level parent acts. These can contain be events/problems and communications
     * @param archetypes          the child archetypes to include
     * @param search              the search criteria. May be {@code null}
     * @param parentSortAscending if {@code true}, objects are sorted on ascending timestamp
     * @param childSortAscending  if {@code true} sort child acts on ascending timestamp; otherwise sort on descending
     *                            timestamp
     * @return the acts
     */
    public List<Act> flatten(List<Act> objects, String[] archetypes,
                             Predicate<org.openvpms.component.model.act.Act> search,
                             boolean parentSortAscending, boolean childSortAscending) {
        List<Act> events = new ArrayList<>();  // the primary events
        List<Act> communications = new ArrayList<>();
        for (Act object : objects) {
            if (object.isA(archetype)) {
                events.add(object);
            } else if (includeCommunication(object, search)) {
                communications.add(object);
            }
        }
        ActHierarchyIterator<Act> iterator = createIterator(events, archetypes, search, childSortAscending,
                                                            communications, service);
        List<Act> list = new ArrayList<>();
        CollectionUtils.addAll(list, iterator);
        if (!communications.isEmpty()) {
            // need to merge communications with the flattened list of acts. These have a mixed sort i.e.
            // top-level acts (communications, events) may be sorted one way, whereas child acts may be sorted
            // differently.

            Comparator<Act> comparator = (o1, o2) -> {
                int result = DateRules.compareTo(o1.getActivityStartTime(), o2.getActivityStartTime());
                if (result == 0) {
                    // same timestamps, so compare on id
                    result = Long.compare(o1.getId(), o2.getId());
                }
                if (!parentSortAscending) {
                    result = -result;
                }
                return result;
            };
            for (Act communication : communications) {
                int index = Collections.binarySearch(events, communication, comparator);
                if (index < 0) {
                    index = -index - 1;
                }
                int insert = -1;
                if (index < events.size()) {
                    Act event = events.get(index);
                    insert = list.indexOf(event);
                }
                if (insert == -1) {
                    list.add(communication);
                } else {
                    list.add(insert, communication);
                }
            }
        }
        return list;
    }

    /**
     * Creates an iterator over the act hierarchy.
     *
     * @param objects            the top-level parent acts, minus communications
     * @param archetypes         the child archetypes to include
     * @param search             the search criteria. May be {@code null}
     * @param childSortAscending if {@code true} sort child acts on ascending timestamp; otherwise sort on descending
     *                           timestamp
     * @param communications     communications to merge
     * @param service            the archetype service
     * @return a new iterator
     */
    protected abstract ActHierarchyIterator<Act> createIterator(List<Act> objects, String[] archetypes,
                                                                Predicate<org.openvpms.component.model.act.Act> search,
                                                                boolean childSortAscending, List<Act> communications,
                                                                ArchetypeService service);

    /**
     * Determines if a communication should be included.
     *
     * @param object the communication
     * @param search the search criteria. May be {@code null}
     * @return {@code true} if the communication should be included
     */
    private boolean includeCommunication(Act object, Predicate<org.openvpms.component.model.act.Act> search) {
        boolean result = includeCommunications;
        if (result) {
            IMObjectBean bean = service.getBean(object);
            if (bean.hasNode(REASON)) {
                Lookup lookup = bean.getLookup(REASON);
                if (lookup != null) {
                    result = showInPatientHistory(lookup);
                }
            }
            if (result) {
                result = (search == null || search.test(object));
            }
        }
        return result;
    }

    /**
     * Determines if the communications with the specified reason should be shown in patient history.
     *
     * @param reason the reason
     * @return {@code true} if the communication should be shown, {@code false} if it should be suppressed
     */
    private boolean showInPatientHistory(Lookup reason) {
        boolean result;
        IMObjectBean bean = service.getBean(reason);
        result = bean.getBoolean("showInPatientHistory");
        return result;
    }
}
