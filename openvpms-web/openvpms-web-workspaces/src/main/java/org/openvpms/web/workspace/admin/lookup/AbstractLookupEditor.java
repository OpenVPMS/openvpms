/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.lookup;

import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;


/**
 * Abstract editor for lookups.
 * <p/>
 * For lookups where there is both code and name nodes, and
 * the code is hidden, this derives the initial value of code from the name.
 * The derived value is the name with letters converted to uppercase, and
 * anything it is not in the range [A-Z,0-9] replaced with underscores.
 *
 * @author Tim Anderson
 */
public abstract class AbstractLookupEditor extends AbstractIMObjectEditor {

    /**
     * Lookup code node.
     */
    protected static final String CODE = "code";

    /**
     * Lookup name node.
     */
    protected static final String NAME = "name";

    /**
     * The code component.
     */
    private Component code;

    /**
     * Constructs an {@link AbstractLookupEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AbstractLookupEditor(IMObject object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);

        disableMacroExpansion(CODE);

        if (object.isNew() && getProperty(CODE) != null) {
            initCode();
        }

        Editor codeEditor = getEditor(CODE);
        if (codeEditor != null) {
            code = codeEditor.getComponent();
            code.setEnabled(object.isNew()); // only enable the code field for new objects
        }
    }

    /**
     * Returns the lookup code.
     *
     * @return the lookup code. May be {@code null}
     */
    public String getCode() {
        Property code = getProperty(CODE);
        return code != null ? code.getString() : null;
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendents are valid otherwise {@code false}
     */
    @Override
    public boolean validate(Validator validator) {
        boolean valid = super.validate(validator);
        if (valid) {
            valid = validateCode(validator);
        }
        return valid;
    }

    /**
     * Save any edits.
     *
     * @throws OpenVPMSException if the save fails
     */
    @Override
    public void save() {
        super.save();
        if (code != null) {
            code.setEnabled(false);
        }
    }

    /**
     * Updates the lookup code, if the object is new.
     * <p/>
     * This uses the code produced by {@link #createCode()}.
     */
    protected void updateCode() {
        if (getObject().isNew()) {
            Property property = getProperty(CODE);
            if (property != null) {
                String code = createCode();
                property.setValue(code);
            }
        }
    }

    /**
     * Initialises the code node.
     * <p/>
     * This is only invoked if the lookup is new.
     * <p/>
     * This implementation registers a listener to invoke {@link #updateCode()} when the name node changes.
     * If the lookup doesn't have name node, this implementation is a no-op.
     */
    protected void initCode() {
        Property name = getProperty(NAME);
        if (name != null) {
            Property code = getProperty(CODE);
            if (code.isHidden()) {
                // derive the code when the name changes
                name.addModifiableListener(new ModifiableListener() {
                    public void modified(Modifiable modifiable) {
                        updateCode();
                    }
                });
            }
        }
    }

    /**
     * Validates the lookup code.
     * <p/>
     * If the lookup is new, this implementation verifies that the code is unique within the lookup's archetype to
     * avoid duplicate lookup errors.
     *
     * @param validator the validator
     * @return {@code true} if the code is valid
     */
    protected boolean validateCode(Validator validator) {
        boolean result = true;

        Lookup lookup = (Lookup) getObject();
        if (lookup.isNew()) {
            String code = lookup.getCode();
            if (!StringUtils.isEmpty(code)) {
                String node = CODE;
                Property property = getProperty(node);
                String name = (property != null) ? property.getDisplayName() : node;
                String archetype = lookup.getArchetype();
                if (exists(archetype, code)) {
                    String message = Messages.format("lookup.validation.duplicate", getDisplayName(), name, code);
                    validator.add(this, new ValidatorError(archetype, node, message));
                    result = false;
                }
            }
        }
        return result;
    }

    /**
     * Creates a code for the lookup.
     * <p/>
     * This must be unique for lookups of the same archetype to avoid duplicate errors on save.
     * <p/>
     * This implementation creates a code from the name node.
     *
     * @return a new code
     */
    protected String createCode() {
        String code = null;
        String name = getProperty(NAME).getString();
        if (name != null) {
            code = name.toUpperCase();
            code = code.replaceAll("[^A-Z0-9]+", "_");
        }
        return code;
    }

    /**
     * Determines if a lookup exists.
     *
     * @param shortName the lookup archetype short name
     * @param code      the lookup code
     * @return {@code true} if it exists, otherwise {@code false}
     */
    private boolean exists(String shortName, String code) {
        return ServiceHelper.getLookupService().getLookup(shortName, code, false) != null;
    }

}
