/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.hl7;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.lookup.LookupRelationship;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Imports lookup mappings from a CSV document.
 *
 * @author Tim Anderson
 */
public class LookupMappingImporter {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The field separator.
     */
    private final char separator;

    /**
     * Constructs a {@link LookupMappingImporter}.
     *
     * @param service   the archetype service
     * @param lookups   the lookups
     * @param handlers  the document handlers
     * @param separator the field separator
     */
    public LookupMappingImporter(ArchetypeService service, LookupService lookups, DocumentHandlers handlers,
                                 char separator) {
        this.service = service;
        this.lookups = lookups;
        this.handlers = handlers;
        this.separator = separator;
    }

    /**
     * Imports lookup mappings.
     * <p/>
     * Any mapping already present will be ignored.
     *
     * @param document the CSV document to import
     * @return the loaded mappings, along with any errors encountered.
     */
    public LookupMappings load(Document document) {
        LookupMappingCSVReader reader = new LookupMappingCSVReader(handlers, separator);
        LookupMappings mappings = reader.read(document);
        if (mappings.getErrors().isEmpty()) {
            mappings = load(mappings);
        }
        return mappings;
    }

    /**
     * Loads mappings.
     *
     * @param mappings the mappings
     * @return the loaded mappings, along with any errors
     */
    private LookupMappings load(LookupMappings mappings) {
        List<LookupMapping> success = new ArrayList<>();
        List<LookupMapping> errors = new ArrayList<>();
        for (LookupMapping mapping : mappings.getMappings()) {
            Lookup from = getLookup(mapping.getFromType(), mapping.getFromCode(), mapping.getFromName(), mapping);
            Lookup to = getLookup(mapping.getToType(), mapping.getToCode(), mapping.getToName(), mapping);
            if (from == null || to == null) {
                errors.add(mapping);
                break;
            } else {
                try {
                    IMObjectBean bean = service.getBean(from);
                    if (!bean.getTargetRefs("mapping").contains(to.getObjectReference())) {
                        LookupRelationship relationship = (LookupRelationship) bean.addTarget("mapping", to);
                        to.addLookupRelationship(relationship);
                        service.save(Arrays.asList(from, to));
                        success.add(mapping);
                    }
                } catch (Throwable exception) {
                    mapping.setError(ErrorFormatter.format(exception));
                    errors.add(mapping);
                    break;
                }
            }
        }
        return new LookupMappings(success, errors);
    }

    /**
     * Returns a lookup given its archetype, code and name, creating it if required.
     *
     * @param shortName the lookup archetype
     * @param code      the lookup code
     * @param name      the lookup name
     * @param mapping   the mapping
     * @return the lookup, or {@code null} if it was not found or the arguments are invalid
     */
    private Lookup getLookup(String shortName, String code, String name, LookupMapping mapping) {
        Lookup result = null;
        String[] matches = DescriptorHelper.getShortNames(shortName, service);
        if (matches.length != 1 || !StringUtils.equals(matches[0], shortName)
            || !Objects.equals(service.getArchetypeDescriptor(shortName).getClassType(),
                               org.openvpms.component.business.domain.im.lookup.Lookup.class)) {
            mapping.setError(Messages.format("admin.hl7.mapping.import.invalidArch", shortName));
        } else {
            Lookup lookup = lookups.getLookup(shortName, code);
            if (lookup == null) {
                try {
                    lookup = service.create(shortName, Lookup.class);
                    lookup.setCode(code);
                    lookup.setName(name);
                    result = lookup;
                } catch (OpenVPMSException exception) {
                    // no-op
                }
            } else if (!StringUtils.equalsIgnoreCase(name, lookup.getName())) {
                mapping.setError(Messages.format("admin.hl7.mapping.import.invalidName", lookup.getName(),
                                                 lookup.getCode(), name));
            } else {
                result = lookup;
            }
        }
        return result;
    }

}