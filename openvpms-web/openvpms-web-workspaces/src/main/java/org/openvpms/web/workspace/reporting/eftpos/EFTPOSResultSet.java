/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.eftpos;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IConstraint;
import org.openvpms.component.system.common.query.OrConstraint;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.query.ActResultSet;
import org.openvpms.web.component.im.query.ParticipantConstraint;

import java.util.Date;
import java.util.List;

import static org.openvpms.component.system.common.query.Constraints.eq;
import static org.openvpms.component.system.common.query.Constraints.leftJoin;

/**
 * EFTPOS result set.
 *
 * @author Tim Anderson
 */
public class EFTPOSResultSet extends ActResultSet<Act> {

    /**
     * The selected location.
     */
    private final Party location;

    /**
     * The locations to query.
     */
    private final List<Party> locations;

    /**
     * Constructs an {@link EFTPOSResultSet}.
     *
     * @param archetypes   the act archetype constraint
     * @param value        the value being searched on. If non-null, can be used to search on message identifier,
     *                     party, customer, or patient name
     * @param participants the participant constraints. May be {@code null}
     * @param from         the act start-from date. May be {@code null}
     * @param to           the act start-to date. May be {@code null}
     * @param statuses     the statuses. If empty, indicates all acts
     * @param pageSize     the maximum no. of results per page
     * @param sort         the sort criteria. May be {@code null}
     */
    public EFTPOSResultSet(ShortNameConstraint archetypes, String value, ParticipantConstraint[] participants,
                           Party location, List<Party> locations, Date from, Date to, String[] statuses,
                           int pageSize, SortConstraint[] sort) {
        super(archetypes, value, participants, from, to, statuses, false, null, pageSize, sort);
        setDistinct(true);
        this.location = location;
        this.locations = locations;
    }

    /**
     * Creates a new archetype query.
     *
     * @param archetypes the archetypes
     * @return a new archetype query
     */
    @Override
    protected ArchetypeQuery createQuery(ShortNameConstraint archetypes) {
        ArchetypeQuery query = super.createQuery(archetypes);
        query.getArchetypeConstraint().setAlias("act");
        if (getValue() != null) {
            query.add(leftJoin("identities", "ids"));
            query.add(leftJoin("customer").add(leftJoin("entity", "customere")));
        }
        if (location != null || !locations.isEmpty()) {
            query.add(Constraints.leftJoin("location", "location"));
            if (location != null) {
                query.add(eq("location.entity", location));
            } else {
                OrConstraint or = new OrConstraint();
                for (Party location : locations) {
                    or.add(eq("location.entity", location));
                }
                query.add(or);
            }
        }
        return query;
    }

    /**
     * Creates constraints to query nodes on a value.
     *
     * @param value the value to query
     * @param nodes the nodes to query
     * @return the constraints
     */
    @Override
    protected List<IConstraint> createValueConstraints(String value, List<String> nodes) {
        List<IConstraint> result = super.createValueConstraints(value, nodes);
        result.add(eq("ids.identity", value));
        if (getId(value) == null) {
            result.add(eq("customere.name", value));
        }
        return result;
    }

}
