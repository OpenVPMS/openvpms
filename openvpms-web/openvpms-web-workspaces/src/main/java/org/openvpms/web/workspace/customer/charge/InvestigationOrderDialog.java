/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.laboratory.internal.service.OrderValidationService.ValidationStatus;
import org.openvpms.laboratory.service.OrderValidationStatus;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.IMObjectSelections;
import org.openvpms.web.component.im.query.ListQuery;
import org.openvpms.web.component.im.query.MultiSelectTableBrowser;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.openvpms.archetype.rules.patient.InvestigationArchetypes.PATIENT_INVESTIGATION;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.INVESTIGATION_TYPE;
import static org.openvpms.web.workspace.patient.investigation.PatientInvestigationActLayoutStrategy.ORDER_STATUS;

/**
 * Dialog to select investigations that require laboratory orders.
 * <p/>
 * This validates any orders prior to submission.
 *
 * @author Tim Anderson
 */
class InvestigationOrderDialog extends ModalDialog {

    /**
     * Submit button identifier.
     */
    public static final String SUBMIT_ID = "button.submit";

    /**
     * The investigation manager.
     */
    private final InvestigationManager manager;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The selections.
     */
    private final IMObjectSelections<DocumentAct> selections;

    /**
     * The investigation browser.
     */
    private MultiSelectTableBrowser<DocumentAct> browser;


    /**
     * Constructs an {@link InvestigationOrderDialog}.
     *
     * @param manager the investigation manager
     * @param context the layout context
     * @param skip    if {@code true}, display a 'Skip' button
     */
    public InvestigationOrderDialog(InvestigationManager manager, LayoutContext context, boolean skip) {
        super(Messages.get("customer.charge.investigation.submit.title"),
              (skip) ? new String[]{SUBMIT_ID, SKIP_ID, CANCEL_ID}
                     : new String[]{SUBMIT_ID, CANCEL_ID});
        setModal(true);

        this.manager = manager;
        this.context = new DefaultLayoutContext(context);
        context.setComponentFactory(new TableComponentFactory(this.context));
        selections = new IMObjectSelections<>();
        selections.setListener(this::updateSubmit);
        resize("InvestigationOrderDialog.size");
    }

    /**
     * Returns the selections.
     *
     * @return the selections
     */
    public List<DocumentAct> getSelections() {
        return new ArrayList<>(selections.getSelected());
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        List<DocumentAct> investigations = manager.getUnconfirmedLaboratoryInvestigations();
        for (DocumentAct act : investigations) {
            if (!InvestigationActStatus.CONFIRM_DEFERRED.equals(act.getStatus2())) {
                selections.setSelected(act, true);
            }
        }
        ListQuery<DocumentAct> query = new ListQuery<>(investigations, PATIENT_INVESTIGATION, DocumentAct.class);
        DefaultDescriptorTableModel<DocumentAct> model = new DefaultDescriptorTableModel<>(
                PATIENT_INVESTIGATION, context, "startTime", INVESTIGATION_TYPE, ORDER_STATUS, "clinician");
        browser = new MultiSelectTableBrowser<>(query, model, selections, context);
        getLayout().add(browser.getComponent());
        updateSubmit();
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (SUBMIT_ID.equals(button)) {
            validateInvestigations();
        } else {
            super.onButton(button);
        }
    }

    /**
     * Enables/disables the submit button.
     */
    private void updateSubmit() {
        getButtons().setEnabled(SUBMIT_ID, !selections.getSelected().isEmpty());
    }

    /**
     * Validates each of the selected investigations.
     */
    private void validateInvestigations() {
        validateInvestigations(getSelections().iterator());
    }

    /**
     * Recursively validates investigations, prompting the user if any problems are detected.
     *
     * @param iterator the investigations to validate
     */
    private void validateInvestigations(Iterator<DocumentAct> iterator) {
        if (iterator.hasNext()) {
            DocumentAct investigation = iterator.next();
            if (needsValidation(investigation)) {
                validate(investigation, iterator);
            } else {
                validateInvestigations(iterator);
            }
        } else if (!getSelections().isEmpty()) {
            super.onButton(SUBMIT_ID);
        } else {
            // none of the investigations are going to be submitted
            if (getButtons().isEnabled(SKIP_ID)) {
                onSkip();
            } else {
                onCancel();
            }
        }
    }

    /**
     * Determines if an investigation needs validation.
     *
     * @param investigation the investigation
     * @return {@code true} if the investigation needs validation, otherwise {@code false}
     */
    private boolean needsValidation(DocumentAct investigation) {
        return InvestigationActStatus.PENDING.equals(investigation.getStatus2());
    }

    /**
     * Validates an investigation.
     *
     * @param investigation the investigation to validate
     * @param iterator      the investigations to validate
     */
    private void validate(DocumentAct investigation, Iterator<DocumentAct> iterator) {
        ValidationStatus status = manager.validate(investigation);
        if (status.getStatus() == OrderValidationStatus.Status.ERROR) {
            String preamble = getMessage(investigation, status, "customer.charge.investigation.submit.error");
            ErrorDialog.newDialog()
                    .preamble(preamble)
                    .message(status.getMessage())
                    .ok(() -> {
                        browser.deselect(investigation);
                    })
                    .show();
        } else if (status.getStatus() == OrderValidationStatus.Status.WARNING) {
            String preamble = getMessage(investigation, status, "customer.charge.investigation.submit.warning");
            ConfirmationDialog.newDialog()
                    .titleKey("customer.charge.investigation.submit.confirm")
                    .preamble(preamble)
                    .message(status.getMessage())
                    .yesNoCancel()
                    .yes(() -> validateInvestigations(iterator))
                    .no(() -> {
                        browser.deselect(investigation);
                        validateInvestigations(iterator);
                    })
                    .cancel(InvestigationOrderDialog.this::onCancel)
                    .show();
        } else {
            validateInvestigations(iterator);
        }
    }

    /**
     * Formats a validation error or warning message for an investigation.
     *
     * @param investigation the investigation
     * @param status        the validation status
     * @param key           the resource bundle key
     * @return the message
     */
    private String getMessage(DocumentAct investigation, ValidationStatus status, String key) {
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(investigation);
        String patient = getPatient(bean);
        String investigationType = getInvestigationType(bean);
        return Messages.format(key, investigationType, patient, status.getName());
    }

    /**
     * Returns the name of the patient associated with an investigation.
     *
     * @param bean the investigation bean
     * @return the patient name
     */
    private String getPatient(IMObjectBean bean) {
        return IMObjectHelper.getName(bean.getTargetRef("patient"));
    }

    /**
     * Returns the name of the investigation type associated with an investigation.
     *
     * @param bean the investigation bean
     * @return the investigation type name
     */
    private String getInvestigationType(IMObjectBean bean) {
        return IMObjectHelper.getName(bean.getTargetRef("investigationType"));
    }
}
