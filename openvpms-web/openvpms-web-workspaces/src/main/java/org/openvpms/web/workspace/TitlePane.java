/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace;

import echopointng.ImageIcon;
import echopointng.image.URLImageReference;
import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.ImageReference;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SelectField;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.layout.RowLayoutData;
import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.ContextListener;
import org.openvpms.web.component.app.GlobalContext;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.list.IMObjectListCellRenderer;
import org.openvpms.web.component.im.list.IMObjectListModel;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.util.IMObjectSorter;
import org.openvpms.web.component.prefs.PreferencesDialog;
import org.openvpms.web.component.prefs.UserPreferences;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.SelectFieldFactory;
import org.openvpms.web.echo.pane.ContentPane;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.user.UserPreferencesDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * Title pane.
 *
 * @author Tim Anderson
 */
public class TitlePane extends ContentPane {

    /**
     * The practice rules.
     */
    private final PracticeRules practiceRules;

    /**
     * The user rules.
     */
    private final UserRules userRules;

    /**
     * The context.
     */
    private final GlobalContext context;

    /**
     * The preferences.
     */
    private final UserPreferences preferences;

    /**
     * The logo service.
     */
    private final LogoService logoService;

    /**
     * The logo.
     */
    private ImageIcon logo;

    /**
     * The location selector.
     */
    private SelectField locationSelector;

    /**
     * The department selector.
     */
    private SelectField departmentSelector;

    /**
     * Listener for context department changes.
     */
    private ContextListener departmentListener;

    /**
     * The style name.
     */
    private static final String STYLE = "TitlePane";

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(TitlePane.class);


    /**
     * Constructs a {@link TitlePane}.
     *
     * @param practiceRules the practice rules
     * @param userRules     the user rules
     * @param context       the context
     * @param preferences   the user preferences
     * @param logoService   the logo service
     */
    public TitlePane(PracticeRules practiceRules, UserRules userRules, GlobalContext context,
                     UserPreferences preferences, LogoService logoService) {
        this.practiceRules = practiceRules;
        this.userRules = userRules;
        this.context = context;
        this.preferences = preferences;
        this.logoService = logoService;
        doLayout();
    }

    /**
     * Disposes of this.
     */
    @Override
    public void dispose() {
        if (departmentListener != null) {
            context.removeListener(PracticeArchetypes.DEPARTMENT, departmentListener);
            departmentListener = null;
        }
        super.dispose();
    }

    /**
     * Lay out the component.
     */
    protected void doLayout() {
        setStyleName(STYLE);

        // use an ImageIcon for the logo, as it allows the image height to be set
        logo = new ImageIcon();
        logo.setStyleName("logo");
        logo.setLayoutData(RowFactory.layout(new Alignment(Alignment.DEFAULT, Alignment.CENTER)));

        Label user = LabelFactory.create(null, "small");
        user.setText(Messages.format("label.user", getUserName()));

        Row locationUserRow = RowFactory.create(Styles.CELL_SPACING, user);

        List<Party> locations = getLocations();
        if (!locations.isEmpty()) {
            Party defLocation = getDefaultLocation();
            Label location = LabelFactory.create("app.location", "small");
            IMObjectListModel model = new IMObjectListModel(locations, false, false);
            locationSelector = SelectFieldFactory.create(model);
            if (defLocation != null) {
                locationSelector.setSelectedItem(defLocation);
            }
            locationSelector.setCellRenderer(IMObjectListCellRenderer.NAME);
            locationUserRow.add(location);
            locationUserRow.add(locationSelector);
            locationSelector.addActionListener(new ActionListener() {
                public void onAction(ActionEvent e) {
                    changeLocation();
                }
            });
            setLogo(defLocation);
        }
        List<Entity> departments = getDepartments();
        if (!departments.isEmpty()) {
            Entity defDepartment = getDefaultDepartment(departments);
            Label department = LabelFactory.create("app.department", "small");
            IMObjectListModel model = new IMObjectListModel(departments, false, false);
            departmentSelector = SelectFieldFactory.create(model);
            if (defDepartment == null) {
                defDepartment = departments.get(0);
            }
            departmentSelector.setSelectedItem(defDepartment);
            context.setDepartment(defDepartment);
            departmentSelector.setCellRenderer(IMObjectListCellRenderer.NAME);
            locationUserRow.add(department);
            locationUserRow.add(departmentSelector);
            departmentSelector.addActionListener(new ActionListener() {
                public void onAction(ActionEvent e) {
                    changeDepartment();
                }
            });
            departmentListener = (key, value) -> onDepartmentChanged();
            context.addListener(PracticeArchetypes.DEPARTMENT, departmentListener);
        }
        if (canEditPreferences()) {
            Button preferences = ButtonFactory.create(null, "button.preferences", this::editPreferences);
            locationUserRow.add(preferences);
        }

        Row inset = RowFactory.create(Styles.INSET_X, locationUserRow);
        RowLayoutData right = new RowLayoutData();
        right.setAlignment(new Alignment(Alignment.RIGHT, Alignment.CENTER));
        right.setWidth(new Extent(100, Extent.PERCENT));
        inset.setLayoutData(right);
        Row row = RowFactory.create(logo, inset);
        add(row);
    }

    /**
     * Returns the user name for the current user.
     *
     * @return the user name
     */
    protected String getUserName() {
        User user = context.getUser();
        return (user != null) ? user.getName() : null;
    }

    /**
     * Changes the location.
     */
    private void changeLocation() {
        Party selected = (Party) locationSelector.getSelectedItem();
        context.setLocation(selected);
        setLogo(selected);
    }

    /**
     * Changes the department.
     */
    private void changeDepartment() {
        Entity selected = (Entity) departmentSelector.getSelectedItem();
        context.setDepartment(selected);
        Long id = selected != null ? selected.getId() : null;
        preferences.setPreference(PreferenceArchetypes.GENERAL, "department", id, true);
    }

    /**
     * Invoked when the department changes.
     */
    private void onDepartmentChanged() {
        Entity department = context.getDepartment();
        if (!Objects.equals(department, departmentSelector.getSelectedItem())) {
            departmentSelector.setSelectedItem(department);
        }
    }

    /**
     * Sets the logo for the practice location.
     *
     * @param location the location. May be {@code null}
     */
    private void setLogo(Party location) {
        ImageReference ref = null;
        try {
            URL url = logoService.getURL(location);
            if (url == null) {
                // fall back to the logo associated with the practice, if present
                Party practice = context.getPractice();
                if (practice != null) {
                    url = logoService.getURL(practice);
                }
            }
            if (url != null) {
                ref = new URLImageReference(url);
            }
        } catch (Exception exception) {
            log.warn("Failed to retrieve logo: {}", exception.getMessage(), exception);
        }
        logo.setIcon(ref);
    }

    /**
     * Returns the locations for the current user.
     *
     * @return the locations
     */
    private List<Party> getLocations() {
        List<Party> locations = Collections.emptyList();
        User user = context.getUser();
        Party practice = context.getPractice();
        if (user != null && practice != null) {
            locations = userRules.getLocations(user, practice);
            IMObjectSorter.sort(locations, "name");
        }
        return locations;
    }

    /**
     * Returns the departments for the current user.
     *
     * @return the departments, or an empty list if there are no departments or they are disabled
     */
    private List<Entity> getDepartments() {
        List<Entity> departments = Collections.emptyList();
        Party practice = context.getPractice();
        User user = context.getUser();
        if (practice != null && user != null && practiceRules.departmentsEnabled(practice)) {
            departments = userRules.getDepartments(user);
            IMObjectSorter.sort(departments, "name");
        }
        return departments;
    }

    /**
     * Returns the default department.
     * <p/>
     * This is the default department for the user if they have on, or the last selected department stored in
     * preferences.
     *
     * @param departments the available departments
     * @return the default department. May be {@code null}
     */
    private Entity getDefaultDepartment(List<Entity> departments) {
        Entity result = null;
        User user = context.getUser();
        if (user != null) {
            result = userRules.getDefaultDepartment(user, departments);
            if (result == null) {
                long id = preferences.getLong(PreferenceArchetypes.GENERAL, "department", -1);
                if (id != -1) {
                    result = departments.stream().filter(entity -> entity.getId() == id).findFirst().orElse(null);
                }
            }
        }
        return result;
    }

    /**
     * Returns the default location for the current user.
     *
     * @return the default location, or {@code null} if none is found
     */
    private Party getDefaultLocation() {
        Party location = null;
        User user = context.getUser();
        if (user != null) {
            location = userRules.getDefaultLocation(user);
            if (location == null) {
                Party practice = context.getPractice();
                if (practice != null) {
                    location = practiceRules.getDefaultLocation(practice);
                }
            }
        }
        return location;
    }

    /**
     * Determines if the user can edit preferences.
     *
     * @return {@code true} if the user can edit preferences
     */
    private boolean canEditPreferences() {
        boolean result = false;
        User user = context.getUser();
        if (user != null) {
            IMObjectBean bean = IMObjectHelper.getBean(user);
            result = bean.getBoolean("editPreferences");
        }
        return result;
    }

    /**
     * Edits user preferences.
     */
    private void editPreferences() {
        User user = IMObjectHelper.reload(context.getUser());
        if (user != null) {
            PreferencesDialog dialog = new UserPreferencesDialog(user, context.getPractice(),
                                                                 new LocalContext(context));
            dialog.show();
        }
    }

}
