/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.appointment.view;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.workspace.workflow.scheduling.view.ParticipantScheduleEventQuery;

import java.util.Date;

/**
 * Queries appointments by participant.
 * <p/>
 * By default, queries instances that are incomplete i.e. don't have COMPLETED, CANCELLED or NO_SHOW status.
 *
 * @author Tim Anderson
 */
public abstract class ParticipantAppointmentQuery extends ParticipantScheduleEventQuery {

    /**
     * The act statuses to query.
     */
    private static final ActStatuses STATUSES = new ActStatuses(new StatusLookupQuery(ScheduleArchetypes.APPOINTMENT));

    /**
     * Statuses indicating an event is finished.
     */
    private final String[] COMPLETE_STATUSES = {AppointmentStatus.COMPLETED, AppointmentStatus.CANCELLED,
                                                AppointmentStatus.NO_SHOW};


    /**
     * Constructs a {@link ParticipantAppointmentQuery}.
     *
     * @param entity        the entity to search for
     * @param participant   the participant node name
     * @param participation the entity participation archetype
     */
    protected ParticipantAppointmentQuery(Entity entity, String participant, String participation) {
        super(entity, participant, participation, ScheduleArchetypes.APPOINTMENT, STATUSES);
        getComponent(); // force creation of component to enable initialisation of fields

        // default the date range from a month in the past to 2 years into the future
        Date now = new Date();
        setFrom(DateRules.getDate(now, -1, DateUnits.MONTHS));
        setTo(DateRules.getDate(now, 2, DateUnits.YEARS));
    }

    /**
     * Returns the status codes representing completed events.
     *
     * @return the status codes
     */
    @Override
    protected String[] getCompletedStatuses() {
        return COMPLETE_STATUSES;
    }
}