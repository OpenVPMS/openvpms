/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.info;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.IMObjectTabPaneModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.workflow.appointment.view.ParticipantAppointmentBrowser;
import org.openvpms.web.workspace.workflow.appointment.view.ParticipantAppointmentTableModel;
import org.openvpms.web.workspace.workflow.worklist.view.ParticipantTaskBrowser;
import org.openvpms.web.workspace.workflow.worklist.view.ParticipantTaskTableModel;

import java.util.List;


/**
 * Layout strategy for patients that includes an appointment browser.
 *
 * @author Tim Anderson
 */
public class PatientViewLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * Lays out child components in a tab model.
     *
     * @param object     the parent object
     * @param properties the properties
     * @param model      the tab model
     * @param context    the layout context
     * @param shortcuts  if {@code true} include short cuts
     */
    @Override
    protected void doTabLayout(IMObject object, List<Property> properties, IMObjectTabPaneModel model,
                               LayoutContext context, boolean shortcuts) {
        super.doTabLayout(object, properties, model, context, shortcuts);
        Party patient = (Party) object;
        Browser<Act> appointments = getAppointments(patient, context);
        Browser<Act> tasks = getTasks(patient, context);

        addTab(Messages.get("patient.information.appointments"), model, appointments.getComponent());
        addTab(Messages.get("patient.information.tasks"), model, tasks.getComponent());
    }

    /**
     * Creates a new appointment browser.
     *
     * @param patient the customer
     * @param context the layout context
     * @return a new appointment browser
     */
    protected Browser<Act> getAppointments(Party patient, LayoutContext context) {
        IMTableModel<Act> model = new ParticipantAppointmentTableModel(false, context);
        return new ParticipantAppointmentBrowser(new PatientAppointmentQuery(patient), model, context);
    }

    /**
     * Creates a new task browser.
     *
     * @param patient the customer
     * @param context the layout context
     * @return a new task browser
     */
    protected Browser<Act> getTasks(Party patient, LayoutContext context) {
        IMTableModel<Act> model = new ParticipantTaskTableModel(false, context);
        return new ParticipantTaskBrowser(new PatientTaskQuery(patient), model, context);
    }
}