/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.archetype.rules.message.MessageStatus;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.edit.ViewActActions;
import org.openvpms.web.component.workspace.AbstractViewCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * CRUD window for <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class ReplyCRUDWindow extends AbstractViewCRUDWindow<Act> {

    /**
     * The completed button identifier.
     */
    static final String COMPLETED_ID = "button.messagecompleted";

    /**
     * Constructs a {@link ReplyCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param context    the context
     * @param help       the help context
     */
    public ReplyCRUDWindow(Archetypes<Act> archetypes, Context context, HelpContext help) {
        super(archetypes, new ReplyActions(), context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(COMPLETED_ID, action(this::onCompleted, (object) -> getActions().canComplete(object), true,
                                         "workflow.sms.complete.title"));
        super.layoutButtons(buttons);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(COMPLETED_ID, enable && getActions().canComplete(getObject()));
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected ReplyActions getActions() {
        return (ReplyActions) super.getActions();
    }

    /**
     * Invoked when the 'completed' button is pressed.
     */
    private void onCompleted(Act act) {
        act.setStatus(MessageStatus.COMPLETED);
        SaveHelper.save(act);
        onRefresh(act);
    }

    /**
     * Actions that can be performed on replies.
     */
    private static class ReplyActions extends ViewActActions<Act> {

        /**
         * Determines if a reply can be edited.
         * <p/>
         * A reply can only be edited if it has no parent SMS.
         *
         * @param reply the reply
         * @return {@code true} if the reply can be edited
         */
        @Override
        public boolean canEdit(Act reply) {
            boolean result = false;
            if (reply != null && reply.isA(SMSArchetypes.REPLY)) {
                IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(reply);
                if (bean.getValues("sms").isEmpty()) {
                    result = true;
                }
            }
            return result;
        }

        /**
         * Determines if a reply can be completed.
         *
         * @param reply the reply
         * @return {@code true} if the reply can be completed
         */
        public boolean canComplete(Act reply) {
            return reply != null && reply.isA(SMSArchetypes.REPLY)
                   && !MessageStatus.COMPLETED.equals(reply.getStatus());
        }
    }

}
