/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment.sms;

import nextapp.echo2.app.Column;
import org.openvpms.archetype.rules.workflow.AppointmentReminderConfig;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.system.common.util.Variables;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.sms.SMSEditor;
import org.openvpms.web.echo.button.CheckBox;
import org.openvpms.web.echo.factory.CheckBoxFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.List;

/**
 * Editor for appointment SMS.
 *
 * @author Tim Anderson
 */
public class AppointmentSMSEditor extends SMSEditor {

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The appointment reminder configuration. May be {@code null}
     */
    private final AppointmentReminderConfig config;

    /**
     * The reply status container.
     */
    private final Column replyContainer = ColumnFactory.create(Styles.CELL_SPACING);

    /**
     * Checkbox to disable that subsequent automatic reminder. Only displayed if an appointment reminder job is
     * configured, and the appointment has its <em>sendReminder</em> flag set.
     */
    private CheckBox disableReminder;


    /**
     * Constructs an {@link AppointmentSMSEditor}.
     * <p>
     * If no phone numbers are supplied, the phone number will be editable, otherwise it will be read-only.
     * If there are multiple phone numbers, they will be displayed in a dropdown, with the first no. as the default
     *
     * @param contacts   the available mobile contacts. May be {@code null}
     * @param variables  the variables for macro expansion. May be {@code null}
     * @param context    the context
     * @param smsService the SMS service
     */
    public AppointmentSMSEditor(List<Contact> contacts, Variables variables, Context context, SMSService smsService) {
        super(contacts, variables, context, smsService);

        rules = ServiceHelper.getBean(AppointmentRules.class);
        config = rules.getAppointmentReminderConfig();
        if (config != null && config.processReplies()) {
            addModifiableListener(modifiable -> showReplyInfo());
            showReplyInfo();
        }
    }

    /**
     * Sets the source of the SMS.
     *
     * @param source the source. May be {@code null}
     */
    @Override
    public void setSource(Act source) {
        super.setSource(source);
        if (source != null) {
            if (rules.sendReminder(source)) {
                disableReminder = CheckBoxFactory.create("sms.appointment.disablereminder");
            }
        }
    }

    /**
     * Determines if the automatic reminder for the appointment should be disabled.
     *
     * @return {@code true} if the automatic reminder for the appointment should be disabled, otherwise {@code false}
     */
    public boolean disableReminder() {
        return disableReminder != null && disableReminder.isSelected();
    }

    /**
     * Lays out the component in a grid.
     *
     * @param grid the grid
     */
    @Override
    protected void layout(ComponentGrid grid) {
        super.layout(grid);
        if (disableReminder != null) {
            grid.add(LabelFactory.create(), disableReminder);
            getFocusGroup().add(disableReminder);
        }
        if (config != null && config.processReplies()) {
            grid.add(ColumnFactory.create(Styles.INSET_Y, replyContainer), 2);
        }
    }

    /**
     * Displays details about SMS reply processing, to inform the user if the SMS contains text which will trigger
     * appointment confirmation or cancellation if included in a reply.
     */
    private void showReplyInfo() {
        String message = getMessage();
        replyContainer.removeAll();
        if (config.containsConfirmation(message)) {
            replyContainer.add(LabelFactory.text(Messages.format("sms.appointment.confirm", config.getConfirmText())));
        }
        if (config.containsCancellation(message)) {
            replyContainer.add(LabelFactory.text(Messages.format("sms.appointment.cancel", config.getCancelText())));
        }
    }
}