/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.component.model.act.Act;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.edit.ActActions;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.workspace.AbstractViewCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;

/**
 * CRUD window for <em>act.smsMessage</em>.
 *
 * @author Tim Anderson
 */
public class SMSCRUDWindow extends AbstractViewCRUDWindow<Act> {

    /**
     * Mark reviewed button identifier.
     */
    private static final String MARK_REVIEWED = "button.markReviewed";

    /**
     * Unmark reviewed button identifier.
     */
    private static final String UNMARK_REVIEWED = "button.unmarkReviewed";

    /**
     * Constructs an {@link SMSCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param context    the context
     * @param help       the help context
     */
    public SMSCRUDWindow(Archetypes<Act> archetypes, Context context,
                         HelpContext help) {
        super(archetypes, SMSActions.INSTANCE, context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(MARK_REVIEWED, action(SMSArchetypes.MESSAGE,
                                          this::markReviewed, "workflow.sms.reviewed.title"));
        buttons.add(UNMARK_REVIEWED, action(SMSArchetypes.MESSAGE,
                                            this::unmarkReviewed, "workflow.sms.unreviewed.title"));
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(MARK_REVIEWED, enable && SMSActions.INSTANCE.canReview(getObject()));
        buttons.setEnabled(UNMARK_REVIEWED, enable && SMSActions.INSTANCE.canUnreview(getObject()));
    }

    /**
     * Marks an SMS as reviewed.
     * <p>
     * This only applies to SMSes that have ERROR status.
     *
     * @param object the SMS
     */
    private void markReviewed(Act object) {
        if (SMSActions.INSTANCE.canReview(object)) {
            object.setStatus(OutboundMessage.Status.REVIEWED.toString());
            SaveHelper.save(object);
        }
        onRefresh(object);
    }

    /**
     * Unmarks an SMS as reviewed.
     * <p>
     * This only applies to SMSes that have REVIEWED status.
     * <p>
     * Un-reviewing them sets them back to ERROR status.
     *
     * @param object the SMS
     */
    private void unmarkReviewed(Act object) {
        if (SMSActions.INSTANCE.canUnreview(object)) {
            object.setStatus(OutboundMessage.Status.ERROR.toString());
            SaveHelper.save(object);
        }
        onRefresh(object);
    }

    private static class SMSActions extends ActActions<Act> {

        public static final SMSActions INSTANCE = new SMSActions();

        /**
         * Determines if objects can be created.
         *
         * @return {@code false}
         */
        @Override
        public boolean canCreate() {
            return false;
        }

        /**
         * Determines if an object can be deleted.
         *
         * @param object the object to check
         * @return {@code true} if the object can be deleted
         */
        @Override
        public boolean canDelete(Act object) {
            return OutboundMessage.Status.PENDING.toString().equals(object.getStatus());
        }

        /**
         * Determines if an SMS can be flagged as reviewed.
         *
         * @param object the SMS
         * @return {@code true} if the SMS can be flagged as reviewed
         */
        boolean canReview(Act object) {
            return OutboundMessage.Status.ERROR.toString().equals(object.getStatus());
        }

        /**
         * Determines if an SMS can be unflagged as reviewed.
         * <p>
         * This returns true for SMSes that have REVIEWED status.
         *
         * @param object the SMS
         * @return {@code true} if the SMS can be flagged as reviewed
         */
        boolean canUnreview(Act object) {
            return OutboundMessage.Status.REVIEWED.toString().equals(object.getStatus());
        }
    }

}
