/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.joda.time.Duration;
import org.joda.time.Period;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.invoice.InvoiceItemStatus;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.hl7.laboratory.LaboratoryOrderService;
import org.openvpms.hl7.patient.PatientContext;
import org.openvpms.hl7.patient.PatientContextFactory;
import org.openvpms.hl7.pharmacy.PharmacyOrderService;
import org.openvpms.laboratory.internal.dispatcher.OrderDispatcher;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Places orders with the {@link PharmacyOrderService}, if a product is dispensed via a pharmacy, or the
 * {@link LaboratoryOrderService}, if an investigation is processed by a laboratory.
 *
 * @author Tim Anderson
 */
public class OrderPlacer {

    /**
     * The customer.
     */
    private final Party customer;

    /**
     * The location.
     */
    private final Party location;

    /**
     * The user responsible for the orders.
     */
    private final User user;

    /**
     * The cache.
     */
    private final IMObjectCache cache;

    /**
     * The order services.
     */
    private final OrderServices services;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The pharmacy products helper.
     */
    private final PharmacyProducts pharmacies;

    private final boolean discontinueOnFinalisation;

    /**
     * The orders, keyed on act reference.
     */
    private final Map<Reference, Order> orders = new HashMap<>();

    /**
     * Constructs an {@link OrderPlacer}.
     *
     * @param customer the customer
     * @param location the location
     * @param user     the user responsible for the orders
     * @param practice the practice
     * @param cache    the object cache
     * @param services the order services
     * @param service  the archetype service
     */
    public OrderPlacer(Party customer, Party location, User user, Party practice, IMObjectCache cache,
                       OrderServices services, IArchetypeService service) {
        this.customer = customer;
        this.location = location;
        this.user = user;
        this.cache = cache;
        this.services = services;
        this.service = service;
        PracticeRules rules = services.getPracticeRules();
        Period period = rules.getPharmacyOrderDiscontinuePeriod(practice);
        discontinueOnFinalisation = period == null || period.toStandardDuration().compareTo(Duration.ZERO) < 0;
        this.pharmacies = new PharmacyProducts(services.getPharmacies(), location, cache);
    }

    /**
     * Initialises the order placer with an existing order.
     *
     * @param items the charge items and investigations
     */
    public void initialise(List<Act> items) {
        for (Act item : items) {
            initialise(item);
        }
    }

    /**
     * Initialises the order placer with an existing order.
     *
     * @param item the charge item or investigation
     */
    public void initialise(Act item) {
        Order order = getOrder(item);
        if (order != null) {
            orders.put(item.getObjectReference(), order);
        }
    }

    /**
     * Places any orders required by invoice items and investigations.
     * <p/>
     * If items have been removed since initialisation, they will be cancelled.
     *
     * @param items   the invoice items and investigations to order
     * @param changes patient history changes, used to obtain patient events
     * @return the set of updated charge and investigation items
     */
    public Set<Act> order(List<Act> items, PatientHistoryChanges changes) {
        return order(items, Collections.emptyList(), changes);
    }


    /**
     * Places any orders required by invoice items and investigations.
     * <p/>
     * If items have been removed since initialisation, they will be cancelled.
     *
     * @param items    the invoice items and investigations to order
     * @param excluded the items excluded from ordering
     * @param changes  patient history changes, used to obtain patient events
     * @return the set of updated charge and investigation items
     */
    public Set<Act> order(List<Act> items, List<Act> excluded, PatientHistoryChanges changes) {
        Map<Reference, Act> invoiceItemsByRef = getInvoiceItems(items);
        List<Reference> ids = new ArrayList<>(orders.keySet());
        Set<Act> updated = new HashSet<>();
        Set<Party> patients = new HashSet<>();
        for (Act item : items) {
            List<Act> invoiceItems = getInvoiceItems(item, invoiceItemsByRef);
            Reference id = item.getObjectReference();
            ids.remove(id);
            Order order = getOrder(item);
            Order existing = orders.get(id);
            if (order != null) {
                if (existing != null) {
                    existing.verifyOrder(order);  // sanity check
                    if (existing.needsUpdate(order)) {
                        updateOrder(order, invoiceItems, changes, patients, updated);
                    } else if (!order.isPlaced()) {
                        createOrder(order, invoiceItems, changes, patients, updated);
                    }
                } else {
                    createOrder(order, invoiceItems, changes, patients, updated);
                }
                orders.put(id, order);
            } else if (existing != null) {
                // new product is not ordered
                cancelOrder(existing, changes, patients, updated);
            }
        }
        for (Act exclude : excluded) {
            ids.remove(exclude.getObjectReference());
        }
        for (Reference id : ids) {
            Order existing = orders.remove(id);
            cancelOrder(existing, changes, patients, updated);
        }
        return updated;
    }

    /**
     * Cancel orders.
     *
     * @return the set of updated charge and investigation items
     */
    public Set<Act> cancel() {
        Set<Act> updated = new HashSet<>();
        Map<Reference, Act> events = new HashMap<>();
        for (Order order : orders.values()) {
            PatientContext context = getPatientContext(order, events);
            if (context != null) {
                order.cancel(context, services, user, updated);
            }
        }
        return updated;
    }

    /**
     * Cancels any orders where the associated invoice item has been deleted.
     *
     * @param current the set of current invoice items and investigations
     * @param changes patient history changes, used to obtain patient events
     * @return the set of updated charge and investigation items
     */
    public Set<Act> cancelDeleted(List<Act> current, PatientHistoryChanges changes) {
        Set<Act> updated = new HashSet<>();
        Map<Reference, Order> copy = new HashMap<>(orders);
        for (Act act : current) {
            copy.remove(act.getObjectReference());
        }
        if (!copy.isEmpty()) {
            Set<Party> patients = new HashSet<>();
            for (Map.Entry<Reference, Order> removed : copy.entrySet()) {
                cancelOrder(removed.getValue(), changes, patients, updated);
                orders.remove(removed.getKey());
            }
        }
        return updated;
    }

    /**
     * Discontinue orders.
     * <p/>
     * For pharmacy orders, this sends an HL7 message indicating that dispensing should no longer occur.<br/>
     * For laboratory orders, it is a no-op.<br/>
     * In both cases, the associated invoice items will have their statuses updated to
     * {@link InvoiceItemStatus#DISCONTINUED}, despite the fact that investigations will continue to be processed.
     * <p/>
     * This change in status for invoice items associated with investigations is required to ensure that the
     * {@code PharmacyOrderDiscontinuationJob} doesn't continually process the same POSTED invoices.
     *
     * @param items the invoice items and investigations. These will have their status updated if it was previously
     *              {@link InvoiceItemStatus#ORDERED}
     * @return the updated items
     */
    public Set<Act> discontinue(List<Act> items) {
        Set<Act> updated = new HashSet<>();
        Map<Reference, Act> invoiceItems = getInvoiceItems(items);
        Map<Reference, List<Act>> ordersToInvoiceItems = getOrdersToInvoices(items, invoiceItems);
        Map<Reference, Act> events = new HashMap<>();
        for (Map.Entry<Reference, Order> entry : orders.entrySet()) {
            Order order = entry.getValue();
            if (order instanceof PharmacyOrder) {
                PatientContext context = getPatientContext(order, events);
                if (context != null) {
                    order.discontinue(context, services, user);
                }
            }
            Reference id = entry.getKey();
            List<Act> itemsForOrder = ordersToInvoiceItems.get(id);
            if (itemsForOrder != null) {
                for (Act itemForOrder : itemsForOrder) {
                    if (InvoiceItemStatus.ORDERED.equals(itemForOrder.getStatus())) {
                        itemForOrder.setStatus(InvoiceItemStatus.DISCONTINUED);
                        updated.add(itemForOrder);
                    }
                }
            }
        }
        return updated;
    }

    /**
     * Determines if a product is dispensed via a pharmacy.
     *
     * @param product the product. May be {@code null}
     * @return {@code true} if the product is dispensed via a pharmacy
     */
    public boolean isPharmacyProduct(Product product) {
        return product != null && getPharmacy(product) != null;
    }

    /**
     * Determines if pharmacy orders should be discontinued on finalisation of the invoice.
     *
     * @return {@code true} if pharmacy orders should be discontinued on finalisation of the invoice, or {@code false}
     * if they should be discontinued after a delay
     */
    public boolean discontinueOnFinalisation() {
        return discontinueOnFinalisation;
    }

    /**
     * Helper to return a string representation of an {@link IMObject} for debugging purposes.
     *
     * @param object the object. May be {@code null}
     * @return the string
     */
    static String toString(IMObject object) {
        StringBuilder result = new StringBuilder();
        if (object != null) {
            result.append(object.getName());
            result.append(" (").append(object.getId()).append(")");
        } else {
            result.append("null");
        }
        return result.toString();
    }

    /**
     * Returns the references to the ordered acts with their corresponding invoice items.
     *
     * @param items        the invoice items and investigations
     * @param invoiceItems the invoice items, keyed on reference
     * @return references to the ordered acts with their corresponding invoice items
     */
    private Map<Reference, List<Act>> getOrdersToInvoices(List<Act> items, Map<Reference, Act> invoiceItems) {
        Map<Reference, List<Act>> result = new HashMap<>();
        for (Act item : items) {
            if (item.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
                result.put(item.getObjectReference(), Collections.singletonList(item));
            } else {
                result.put(item.getObjectReference(), getInvoiceItems(item, invoiceItems));
            }
        }
        return result;
    }

    /**
     * Returns the invoice item associated with an act.
     *
     * @param act          the act. If it is an invoice item, it will be returned
     * @param invoiceItems invoice items, keyed on reference
     * @return the invoice item
     * @throws IllegalStateException if the invoice item is not found
     */
    private List<Act> getInvoiceItems(Act act, Map<Reference, Act> invoiceItems) {
        List<Act> result;
        if (act.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            result = Collections.singletonList(act);
        } else {
            IMObjectBean bean = service.getBean(act);
            List<Reference> references = bean.getSourceRefs("invoiceItems");
            if (references.isEmpty()) {
                throw new IllegalStateException("No invoice item found for " + act);
            }
            result = new ArrayList<>();
            for (Reference reference : references) {
                Act item = invoiceItems.get(reference);
                if (item == null) {
                    throw new IllegalStateException("No invoice item found for " + act);
                }
                result.add(item);
            }
        }
        return result;
    }

    /**
     * Returns the invoice items from a list of invoice items and investigations.
     *
     * @param items the invoice items and investigation acts
     * @return the invoice items, keyed on reference
     */
    private Map<Reference, Act> getInvoiceItems(List<Act> items) {
        Map<Reference, Act> result = new HashMap<>();
        for (Act item : items) {
            if (item.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
                result.put(item.getObjectReference(), item);
            }
        }
        return result;
    }

    /**
     * Returns the order for an act.
     *
     * @param act the invoice item or investigation
     * @return the order. May be {@code null}
     */
    private Order getOrder(Act act) {
        Order result = null;
        if (act.isA(CustomerAccountArchetypes.INVOICE_ITEM)) {
            result = getPharmacyOrder(act);
        } else if (act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            result = getLaboratory(act);
        }
        return result;
    }

    /**
     * Returns a pharmacy order.
     *
     * @param act the invoice item
     * @return a pharmacy order, or {@code null} if the product isn't ordered via a pharmacy
     */
    private Order getPharmacyOrder(Act act) {
        Order order = null;
        IMObjectBean bean = service.getBean(act);
        Product product = (Product) getObject(bean.getTargetRef("product"));
        if (product != null && product.isA(ProductArchetypes.MEDICATION, ProductArchetypes.MERCHANDISE)) {
            Entity pharmacy = getPharmacy(product);
            if (pharmacy != null) {
                Party patient = (Party) getObject(bean.getTargetRef("patient"));
                if (patient != null) {
                    BigDecimal quantity = bean.getBigDecimal("quantity", BigDecimal.ZERO);
                    User clinician = (User) getObject(bean.getTargetRef("clinician"));
                    Reference event = bean.getSourceRef("event");
                    order = new PharmacyOrder(act, product, patient, quantity, clinician, pharmacy, event, service);
                }
            }
        }
        return order;
    }

    /**
     * Returns a laboratory order.
     *
     * @param act the investigation act
     * @return a laboratory order, or {@code null} if the investigation isn't ordered via a laboratory
     */
    private Order getLaboratory(Act act) {
        Order order = null;
        IMObjectBean bean = service.getBean(act);
        Entity investigationType = (Entity) getObject(bean.getTargetRef("investigationType"));
        if (investigationType != null) {
            Entity laboratory = (Entity) getObject(bean.getTargetRef("laboratory"));
            if (laboratory == null) {
                // Required for legacy investigations.
                laboratory = getLaboratory(investigationType);
            }
            if (laboratory != null) {
                Party patient = (Party) getObject(bean.getTargetRef("patient"));
                if (patient != null) {
                    User clinician = (User) getObject(bean.getTargetRef("clinician"));
                    Reference event = bean.getSourceRef("event");
                    if (laboratory.isA(LaboratoryArchetypes.HL7_LABORATORY)) {
                        Entity test = (Entity) getObject(bean.getTargetRef("tests")); // only one test for HL7 orders
                        if (test != null) {
                            IMObjectBean testBean = service.getBean(test);
                            Identity identity = testBean.getObject("code", Identity.class);
                            String testCode = identity != null ? identity.getIdentity() : null;
                            if (testCode != null) {
                                order = new HL7LaboratoryOrder(act, testCode, patient, clinician, laboratory, event);
                            }
                        }
                    } else {
                        order = new LaboratoryOrder(act, patient, clinician, laboratory, event);
                    }
                }
            }
        }
        return order;
    }

    /**
     * Returns the patient context associated with an order.
     *
     * @param order   the order
     * @param changes the changes
     * @return the patient context, or {@code null} if there is no corresponding patient event
     */
    private PatientContext getPatientContext(Order order, PatientHistoryChanges changes) {
        PatientContext result = null;
        Reference patient = order.getPatient().getObjectReference();
        List<Act> events = changes.getEvents(patient);
        Act event;
        if (events == null || events.isEmpty()) {
            event = changes.getEvent(order.getEvent());
        } else {
            event = Collections.max(events, (o1, o2)
                    -> DateRules.compareDateTime(o1.getActivityStartTime(), o2.getActivityStartTime(), true));
        }
        if (event != null) {
            result = services.getFactory().createContext(order.getPatient(), customer, event, location,
                                                         order.getClinician());
        }
        return result;
    }

    /**
     * Returns the patient context associated with an order.
     *
     * @param order  the order
     * @param events the cache of clinical events
     * @return the patient context, or {@code null} if there is no corresponding patient event
     */
    private PatientContext getPatientContext(Order order, Map<Reference, Act> events) {
        PatientContext result = null;
        Act event = events.get(order.getEvent());
        if (event == null) {
            event = (Act) getObject(order.getEvent());
        }
        if (event == null) {
            event = services.getRules().getEvent(order.getPatient(), order.getStartTime());
        }
        if (event != null) {
            events.put(order.getEvent(), event);
        }
        if (event != null) {
            PatientContextFactory factory = services.getFactory();
            result = factory.createContext(order.getPatient(), customer, event, location, order.getClinician());
        }
        return result;
    }

    /**
     * Creates an order.
     *
     * @param order        the order
     * @param invoiceItems the invoice items associated with the order
     * @param changes      the changes
     * @param patients     tracks patients that have had notifications sent
     * @param updated      the set of changed acts
     * @return {@code true} if an order was created (and invoice updated)
     */
    private boolean createOrder(Order order, List<Act> invoiceItems, PatientHistoryChanges changes, Set<Party> patients,
                                Set<Act> updated) {
        boolean result = false;
        PatientContext context = getPatientContext(order, changes);
        if (context != null) {
            notifyPatientInformation(context, changes, patients);
            if (order.create(context, services, user, invoiceItems, updated)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Updates an order.
     *
     * @param order        the order
     * @param invoiceItems the invoice items associated with the order
     * @param changes      the changes
     * @param patients     tracks patients that have had notifications sent
     * @param updated      the set of changed acts
     */
    private void updateOrder(Order order, List<Act> invoiceItems, PatientHistoryChanges changes, Set<Party> patients,
                             Set<Act> updated) {
        PatientContext context = getPatientContext(order, changes);
        if (context != null) {
            notifyPatientInformation(context, changes, patients);
            order.update(context, services, user, invoiceItems, updated);
        }
    }

    /**
     * Cancel an order.
     * <p/>
     * Note that if the charge was the result of an order being automatically charged and the charge is then removed,
     * there will be nothing to cancel.
     *
     * @param order    the order
     * @param changes  the changes
     * @param patients the patients, used to prevent duplicate patient update notifications being sent
     * @param updated  the set of changed acts
     */
    private void cancelOrder(Order order, PatientHistoryChanges changes, Set<Party> patients, Set<Act> updated) {
        if (!order.getActId().isNew()) {
            PatientContext context = getPatientContext(order, changes);
            if (context != null) {
                notifyPatientInformation(context, changes, patients);
                order.cancel(context, services, user, updated);
            }
        }
    }

    /**
     * Notifies registered listeners of patient visit information, when placing orders outside of a current visit.
     * <p/>
     * This is required for listeners that remove patient information when a patient is discharged.
     * <p/>
     * The {@code patients} variable is used to track if patient information has already been sent for a given patient,
     * to avoid multiple notifications being sent. This isn't kept across calls to {@link #order}, so
     * redundant notifications may be sent.
     *
     * @param context  the context
     * @param changes  the patient history changes
     * @param patients tracks patients that have had notifications sent
     */
    private void notifyPatientInformation(PatientContext context, PatientHistoryChanges changes, Set<Party> patients) {
        Act visit = context.getVisit();
        if (!patients.contains(context.getPatient())) {
            if (changes.isNew(visit)
                || (visit.getActivityEndTime() != null
                    && DateRules.compareTo(visit.getActivityEndTime(), new Date()) < 0)) {
                services.getInformationService().updated(context);
                patients.add(context.getPatient());
            }
        }
    }

    /**
     * Returns the pharmacy for a product and location.
     *
     * @param product the product. May be {@code null}
     * @return the pharmacy, or {@code null} if none is present
     */
    private Entity getPharmacy(Product product) {
        return pharmacies.getPharmacy(product);
    }

    /**
     * Returns the laboratory for an investigation and location.
     *
     * @param investigationType the investigation type
     * @return the pharmacy, or {@code null} if none is present
     */
    private Entity getLaboratory(Entity investigationType) {
        return services.getLaboratoryServices().getLaboratory(investigationType, location);
    }

    /**
     * Returns an object given its reference.
     *
     * @param reference the reference. May be {@code null}
     * @return the object, or {@code null} if none is found
     */
    private IMObject getObject(Reference reference) {
        return (reference != null) ? cache.get(reference) : null;
    }

    private static abstract class Order {

        private final Reference actId;

        private final Date startTime;

        private final Party patient;

        private final User clinician;

        private final Reference event;

        Order(Reference actId, Date startTime, Party patient, User clinician, Reference event) {
            this.actId = actId;
            this.startTime = startTime;
            this.patient = patient;
            this.clinician = clinician;
            this.event = event;
        }

        public Reference getActId() {
            return actId;
        }

        public long getPlacerOrderNumber() {
            return getActId().getId();
        }

        public Date getStartTime() {
            return startTime;
        }

        public Party getPatient() {
            return patient;
        }

        public User getClinician() {
            return clinician;
        }

        public Reference getEvent() {
            return event;
        }

        /**
         * Creates an order.
         *
         * @param context      the patient context
         * @param services     the order services
         * @param user         the user
         * @param invoiceItems the invoice item(s) associated with the order
         * @param updated      tracks acts that were updated creating the order
         * @return {@code true} if the order was created, otherwise {@code false}
         */
        public abstract boolean create(PatientContext context, OrderServices services, User user,
                                       List<Act> invoiceItems, Set<Act> updated);

        public abstract void cancel(PatientContext context, OrderServices services, User user, Set<Act> updated);

        public abstract void discontinue(PatientContext context, OrderServices services, User user);

        public abstract void update(PatientContext context, OrderServices services, User user, List<Act> invoiceItems, Set<Act> updated);

        /**
         * Verifies that immutable aspects of an order haven't changed.
         *
         * @param newOrder the new version of the order
         * @throws IllegalStateException if the order has been changed in an unsupported manner
         */
        public abstract void verifyOrder(Order newOrder);

        /**
         * Determines if the existing order needs updating.
         *
         * @param newOrder the new version of the order
         * @return {@code true} if the existing order needs updating
         */
        public abstract boolean needsUpdate(Order newOrder);

        /**
         * Determines if an order has been placed.
         *
         * @return {@code true} if the order has been placed
         */
        public abstract boolean isPlaced();

        /**
         * Verifies two objects are the same.
         *
         * @param displayName the display name, for error reporting
         * @param expected    the expected object
         * @param actual      the actual object
         * @throws IllegalStateException if they are different
         */
        protected void verifySame(String displayName, IMObject expected, IMObject actual) {
            if (!Objects.equals(expected, actual)) {
                throw new IllegalStateException(displayName + " cannot be changed on order: previous="
                                                + OrderPlacer.toString(expected)
                                                + ", current=" + OrderPlacer.toString(actual));
            }
        }

        /**
         * Verifies two objects are the same.
         *
         * @param displayName the display name, for error reporting
         * @param expected    the expected object
         * @param actual      the actual object
         * @throws IllegalStateException if they are different
         */
        protected void verifySame(String displayName, Object expected, Object actual) {
            if (!Objects.equals(expected, actual)) {
                throw new IllegalStateException(displayName + " cannot be changed on order: previous="
                                                + expected + ", current=" + actual);
            }
        }

        /**
         * Marks invoice items as ordered.
         *
         * @param invoiceItems the invoice items
         * @param updated      tracks changed acts
         */
        void markOrdered(List<Act> invoiceItems, Set<Act> updated) {
            for (Act item : invoiceItems) {
                if (!InvoiceItemStatus.ORDERED.equals(item.getStatus())) {
                    item.setStatus(InvoiceItemStatus.ORDERED);
                    updated.add(item);
                }
            }
        }
    }

    private static class PharmacyOrder extends Order {

        private final Act act;

        private final BigDecimal quantity;

        private final Entity pharmacy;

        private final Product product;

        private final ArchetypeService service;

        private final boolean oneway;

        private static final String RECEIVED_QUANTITY = "receivedQuantity";

        private static final String RETURNED_QUANTITY = "returnedQuantity";

        PharmacyOrder(Act act, Product product, Party patient, BigDecimal quantity, User clinician, Entity pharmacy,
                      Reference event, ArchetypeService service) {
            super(act.getObjectReference(), act.getActivityStartTime(), patient, clinician, event);
            this.act = act;
            this.product = product;
            this.quantity = quantity;
            this.pharmacy = pharmacy;
            this.service = service;
            oneway = service.getBean(pharmacy).getBoolean("oneway");
        }

        public Entity getPharmacy() {
            return pharmacy;
        }

        public Product getProduct() {
            return product;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        @Override
        public boolean create(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                              Set<Act> updated) {
            PharmacyOrderService service = services.getPharmacyService();
            boolean created = service.createOrder(context, product, quantity, getPlacerOrderNumber(), getStartTime(),
                                                  pharmacy);
            if (created) {
                markOrdered(invoiceItems, updated);
                updateInvoiceItems(invoiceItems, updated);
            }
            return created;
        }

        @Override
        public void update(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                           Set<Act> updated) {
            PharmacyOrderService service = services.getPharmacyService();
            service.updateOrder(context, product, quantity, getPlacerOrderNumber(), getStartTime(), pharmacy);
            updateInvoiceItems(invoiceItems, updated);
        }

        @Override
        public void cancel(PatientContext context, OrderServices services, User user, Set<Act> updated) {
            PharmacyOrderService service = services.getPharmacyService();
            service.cancelOrder(context, product, quantity, getPlacerOrderNumber(), getStartTime(), pharmacy);
        }

        @Override
        public void discontinue(PatientContext context, OrderServices services, User user) {
            PharmacyOrderService service = services.getPharmacyService();
            service.discontinueOrder(context, product, quantity, getPlacerOrderNumber(), getStartTime(), pharmacy);
        }

        /**
         * Determines if the existing order needs cancelling.
         *
         * @param newOrder the new version of the order
         */
        @Override
        public void verifyOrder(Order newOrder) {
            PharmacyOrder other = (PharmacyOrder) newOrder;
            verifySame("Patient", getPatient(), newOrder.getPatient());
            verifySame("Product", product, other.getProduct());
            verifySame("Pharmacy", pharmacy, other.getPharmacy());
        }

        public boolean needsUpdate(Order newOrder) {
            PharmacyOrder other = (PharmacyOrder) newOrder;
            return quantity.compareTo(other.getQuantity()) != 0
                   || !Objects.equals(getClinician(), other.getClinician());
        }

        /**
         * Determines if an order has been placed.
         *
         * @return {@code true} if the order has been placed
         */
        @Override
        public boolean isPlaced() {
            return InvoiceItemStatus.ORDERED.equals(act.getStatus());
        }

        /**
         * Updates invoice items to mark them received, if the pharmacy is one-way.
         *
         * @param invoiceItems the invoice items
         * @param updated      the updated acts
         */
        private void updateInvoiceItems(List<Act> invoiceItems, Set<Act> updated) {
            if (oneway) {
                for (Act item : invoiceItems) {
                    IMObjectBean bean = service.getBean(item);
                    BigDecimal quantity = bean.getBigDecimal("quantity", BigDecimal.ZERO);
                    if (!Objects.equals(quantity, bean.getBigDecimal(RECEIVED_QUANTITY, BigDecimal.ZERO))) {
                        bean.setValue(RECEIVED_QUANTITY, quantity);
                        updated.add(item);
                    }
                    if (bean.getValue(RETURNED_QUANTITY) != null) {
                        bean.setValue(RETURNED_QUANTITY, null);
                        updated.add(item);
                    }
                }
            }
        }
    }

    private static class LaboratoryOrder extends Order {
        private final Act investigation;

        private final Entity lab;

        LaboratoryOrder(Act act, Party patient, User clinician, Entity laboratory, Reference event) {
            super(act.getObjectReference(), act.getActivityStartTime(), patient, clinician, event);
            this.investigation = act;
            this.lab = laboratory;
        }

        @Override
        public boolean create(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                              Set<Act> updated) {
            boolean created = false;
            if (InvestigationActStatus.PENDING.equals(investigation.getStatus2())) {
                OrderDispatcher dispatcher = services.getOrderDispatcher();
                dispatcher.create(investigation);
                created = true;
            }
            return created;
        }

        @Override
        public void cancel(PatientContext context, OrderServices services, User user, Set<Act> updated) {
            OrderDispatcher dispatcher = services.getOrderDispatcher();
            // the laboratory could reject the cancellation, but at this point we don't care.
            dispatcher.cancel(investigation);
            investigation.setStatus(ActStatus.CANCELLED);
            updated.add(investigation);
        }

        @Override
        public void discontinue(PatientContext context, OrderServices services, User user) {
            // no-op
        }

        @Override
        public void update(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                           Set<Act> updated) {
            // no-op
        }

        /**
         * Verifies that immutable aspects of an order haven't changed.
         *
         * @param newOrder the new version of the order
         * @throws IllegalStateException if the order has been changed in an unsupported manner
         */
        @Override
        public void verifyOrder(Order newOrder) {
            LaboratoryOrder other = (LaboratoryOrder) newOrder;
            verifySame("Patient", getPatient(), newOrder.getPatient());
            verifySame("Laboratory", lab, other.lab);
        }

        /**
         * Determines if the existing order needs updating.
         *
         * @param newOrder the new version of the order
         * @return {@code true} if the existing order needs updating
         */
        @Override
        public boolean needsUpdate(Order newOrder) {
            return false;
        }

        /**
         * Determines if the order has been placed.
         *
         * @return {@code true} if the order has been placed
         */
        @Override
        public boolean isPlaced() {
            return !InvestigationActStatus.PENDING.equals(investigation.getStatus2());
        }
    }

    private static class HL7LaboratoryOrder extends Order {

        private final Act investigation;

        private final String test;

        private final Entity lab;

        public HL7LaboratoryOrder(Act act, String test, Party patient, User clinician, Entity device,
                                  Reference event) {
            super(act.getObjectReference(), act.getActivityStartTime(), patient, clinician, event);
            this.investigation = act;
            this.test = test;
            this.lab = device;
        }

        @Override
        public boolean create(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                              Set<Act> updated) {
            LaboratoryOrderService service = services.getLaboratoryService();
            boolean created = service.createOrder(context, getPlacerOrderNumber(), test, getStartTime(), lab);
            if (created) {
                investigation.setStatus2(InvestigationActStatus.SENT);
                updated.add(investigation);
                markOrdered(invoiceItems, updated);
            }
            return created;
        }

        @Override
        public void cancel(PatientContext context, OrderServices services, User user, Set<Act> updated) {
            LaboratoryOrderService service = services.getLaboratoryService();
            boolean sent = service.cancelOrder(context, getPlacerOrderNumber(), test, getStartTime(), lab);
            if (sent) {
                investigation.setStatus(ActStatus.CANCELLED);
                updated.add(investigation);
            }
        }

        @Override
        public void discontinue(PatientContext context, OrderServices services, User user) {
        }

        @Override
        public void update(PatientContext context, OrderServices services, User user, List<Act> invoiceItems,
                           Set<Act> updated) {
        }

        /**
         * Verifies that immutable aspects of an order haven't changed.
         *
         * @param newOrder the new version of the order
         * @throws IllegalStateException if the order has been changed in an unsupported manner
         */
        @Override
        public void verifyOrder(Order newOrder) {
            HL7LaboratoryOrder other = (HL7LaboratoryOrder) newOrder;
            verifySame("Patient", getPatient(), newOrder.getPatient());
            verifySame("Test", test, other.test);
            verifySame("Laboratory", lab, other.lab);
        }

        /**
         * Determines if the existing order needs updating.
         *
         * @param newOrder the new version of the order
         * @return {@code true} if the existing order needs updating
         */
        @Override
        public boolean needsUpdate(Order newOrder) {
            return false;
        }

        /**
         * Determines if the order has been placed.
         *
         * @return {@code true} if the order has been placed
         */
        @Override
        public boolean isPlaced() {
            return !InvestigationActStatus.PENDING.equals(investigation.getStatus2());
        }
    }
}
