/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for <em>entity.laboratoryTest</em>.
 * <p/>
 * This prevents investigation types managed by laboratories from being assigned to manually constructed tests.
 *
 * @author Tim Anderson
 */
public class TestEditor extends AbstractLaboratoryTestEditor {

    /**
     * Determines if the test is a laboratory provided test.
     */
    private final boolean provided;

    /**
     * Test code node.
     */
    private static final String CODE = "code";

    /**
     * Investigation type id node.
     */
    private static final String TYPE_ID = "typeId";

    /**
     * Constructs a {@link TestEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public TestEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        provided = !getCollectionProperty(CODE).isEmpty();
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new TestEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Determines if a node can be edited.
     *
     * @param name the node name
     * @return {@code true} if the node can be edited, otherwise {@code false}
     */
    public boolean canEdit(String name) {
        return getEditor(name) != null;
    }

    /**
     * Ensures that user-created tests cannot be submitted to laboratory services by preventing laboratory service
     * provided investigation types being assigned to them.
     *
     * @param validator the validator
     * @return {@code true} if the investigation type is valid
     */
    @Override
    protected boolean validateInvestigationType(Validator validator) {
        if (!provided) {
            Entity investigationType = getInvestigationType();
            if (investigationType != null) {
                IMObjectBean bean = getBean(investigationType);
                if (bean.getObject(TYPE_ID) != null) {
                    validator.add(this, Messages.format("test.invalidInvestigationType", investigationType.getName()));
                } else {
                    // ensure an HL7 laboratory isn't assigned to the test
                    Entity laboratory = (Entity) getObject(bean.getTargetRef("laboratory"));
                    if (laboratory != null && !laboratory.isA(LaboratoryArchetypes.LABORATORY_SERVICES)) {
                        validator.add(this, Messages.format("test.invalidLaboratoryService",
                                                            investigationType.getName(), laboratory.getName()));
                    }
                }
            }
        }
        return validator.isValid();
    }
}
