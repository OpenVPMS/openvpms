/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.type;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workspace.CRUDWindow;
import org.openvpms.web.component.workspace.ResultSetCRUDWorkspace;


/**
 * Type workspace.
 *
 * @author Tim Anderson
 */
public class TypeWorkspace extends ResultSetCRUDWorkspace<Entity> {

    /**
     * Constructs a {@link TypeWorkspace}.
     *
     * @param context the context
     */
    public TypeWorkspace(Context context) {
        super("admin.type", context);
        setArchetypes(Entity.class, "entity.*Type", "entity.PACSServer"); // TODO - remove PACS
    }

    /**
     * Creates a new query to populate the browser.
     *
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery() {
        Query<Entity> query = super.createQuery();
        query.setContains(true);
        return query;
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    @Override
    protected CRUDWindow<Entity> createCRUDWindow() {
        QueryBrowser<Entity> browser = getBrowser();
        return new TypeCRUDWindow(getArchetypes(), browser.getQuery(), browser.getResultSet(), getContext(),
                                  getHelpContext());
    }
}
