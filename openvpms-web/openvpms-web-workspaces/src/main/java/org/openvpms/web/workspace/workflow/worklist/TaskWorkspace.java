/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.worklist;

import org.openvpms.archetype.rules.prefs.PreferenceArchetypes;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.TaskRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleBrowser;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleCRUDWindow;
import org.openvpms.web.workspace.workflow.scheduling.SchedulingWorkspace;

import java.util.Date;
import java.util.Objects;


/**
 * Task workspace.
 *
 * @author Tim Anderson
 */
public class TaskWorkspace extends SchedulingWorkspace {

    /**
     * Constructs a {@link TaskWorkspace}.
     *
     * @param context     the context
     * @param preferences user preferences
     */
    public TaskWorkspace(Context context, Preferences preferences) {
        super("workflow.worklist", Archetypes.create(ScheduleArchetypes.WORK_LIST_VIEW, Entity.class), context,
              preferences, PreferenceArchetypes.WORK_LIST);
    }

    /**
     * Sets the current object.
     *
     * @param object the object. May be {@code null}
     */
    @Override
    public void setObject(Entity object) {
        getContext().setWorkListView(object);
        super.setObject(object);
    }

    /**
     * Determines if the workspace can be updated with instances of the specified archetype.
     *
     * @param shortName the archetype's short name
     * @return {@code true} if {@code shortName} is one of those in {@link #getArchetypes()}
     */
    @Override
    public boolean canUpdate(String shortName) {
        return super.canUpdate(shortName) || ScheduleArchetypes.TASK.equals(shortName);
    }

    /**
     * Updates the workspace with the specified object.
     *
     * @param object the object to update the workspace with. May be {@code null}
     */
    @Override
    public void update(IMObject object) {
        if (object != null) {
            if (object.isA(ScheduleArchetypes.WORK_LIST_VIEW)) {
                if (!Objects.equals(getObject(), object)) {
                    setObject((Entity) object);
                }
            } else if (object.isA(ScheduleArchetypes.TASK)) {
                Act act = (Act) object;
                if (!Objects.equals(getCRUDWindow().getObject(), act)) {
                    TaskRules rules = ServiceHelper.getBean(TaskRules.class);
                    Entity worklist = rules.getWorkList(act);
                    Party location = getContext().getLocation();
                    if (worklist != null && location != null) {
                        Entity view = rules.getWorkListView(location, worklist);
                        if (view != null) {
                            setScheduleView(view, act.getActivityStartTime(), act.getStatus());
                            ScheduleBrowser scheduleBrowser = getBrowser();
                            scheduleBrowser.setSelected(scheduleBrowser.getEvent(act));
                            getCRUDWindow().setObject(act);
                        }
                    }
                }
            }
        }
    }

    /**
     * Sets the schedule view and date.
     *
     * @param view the schedule view
     * @param date the date to view
     */
    @Override
    protected void setScheduleView(Entity view, Date date) {
        setScheduleView(view, date, null);
    }

    /**
     * Sets the schedule view and date and status.
     *
     * @param view   the schedule view
     * @param date   the date to view
     * @param status the status to include, or {@code null} to use the current status filter
     */
    protected void setScheduleView(Entity view, Date date, String status) {
        setScheduleView(view);
        TaskBrowser browser = (TaskBrowser) getBrowser();
        browser.setScheduleView(view);
        browser.setDate(date);
        browser.includeStatus(status);
        browser.query();
        onQuery();
    }

    /**
     * Creates a new browser.
     *
     * @return a new browser
     */
    protected ScheduleBrowser createBrowser() {
        return new TaskBrowser(getPreferences(), getContext());
    }

    /**
     * Creates a new CRUD window.
     *
     * @return a new CRUD window
     */
    protected ScheduleCRUDWindow createCRUDWindow() {
        return new TaskCRUDWindow(getContext(), getHelpContext());
    }

    /**
     * Invoked when events are queried.
     * <p/>
     * This implementation updates the context with the selected work list date and work list
     */
    @Override
    protected void onQuery() {
        Context context = getContext();
        ScheduleBrowser browser = getBrowser();
        context.setWorkListDate(browser.getDate());
        context.setWorkList((Party) browser.getSelectedSchedule());
        super.onQuery();
    }

    /**
     * Invoked when the object has been saved.
     *
     * @param object the object
     * @param isNew  determines if the object is a new instance
     */
    protected void onSaved(IMObject object, boolean isNew) {
        query();
        TaskBrowser browser = (TaskBrowser) getBrowser();
        browser.setSelected((Act) object);
        firePropertyChange(SUMMARY_PROPERTY, null, null);
    }

    /**
     * Invoked when the object needs to be refreshed.
     *
     * @param object the object. May be {@code null}
     */
    protected void onRefresh(IMObject object) {
        query();
        TaskBrowser browser = (TaskBrowser) getBrowser();
        if (!browser.setSelected((Act) object)) {
            // task no longer visible in the work list view, so remove it
            // from the CRUD window
            getCRUDWindow().setObject(null);
        }
        firePropertyChange(SUMMARY_PROPERTY, null, null);
    }

    /**
     * Invoked when an event is selected.
     *
     * @param event the event. May be {@code null}
     */
    @Override
    protected void eventSelected(PropertySet event) {
        // update the context work list
        updateContext();
        super.eventSelected(event);
        getContext().setTask(getCRUDWindow().getObject());
    }

    /**
     * Invoked to edit an event.
     *
     * @param event the event
     */
    @Override
    protected void onEdit(PropertySet event) {
        updateContext();
        super.onEdit(event);
    }

    /**
     * Returns the latest version of the current schedule view context object.
     *
     * @return the latest version of the schedule view context object, or {@link #getObject()} if they are the same
     */
    @Override
    protected Entity getLatest() {
        return getLatest(getContext().getWorkListView());
    }

    /**
     * Returns the default schedule view for the specified practice location.
     *
     * @param location the practice location
     * @param prefs    user preferences
     * @return the default schedule view, or {@code null} if there is no default
     */
    protected Entity getDefaultView(Party location, Preferences prefs) {
        TaskSchedules schedules = new TaskSchedules(location, prefs);
        return schedules.getDefaultScheduleView();
    }

    /**
     * Updates the global context with the selected work list.
     */
    private void updateContext() {
        Party workList = (Party) getBrowser().getSelectedSchedule();
        getContext().setWorkList(workList);
    }

}
