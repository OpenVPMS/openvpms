/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.Component;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.workspace.customer.communication.AbstractCommunicationLayoutStrategy;

import java.util.List;
import java.util.Objects;

import static org.openvpms.web.component.im.layout.ArchetypeNodes.include;
import static org.openvpms.web.echo.style.Styles.CELL_SPACING;

/**
 * Layout strategy for <em>act.smsMessage</em>, <em>act.smsReply</em>.
 *
 * @author Tim Anderson
 */
public class AbstractSMSMessageLayoutStrategy extends AbstractCommunicationLayoutStrategy {

    /**
     * The contact node.
     */
    protected static final String CONTACT = "contact";

    /**
     * The customer node.
     */
    protected static final String CUSTOMER = "customer";

    /**
     * The phone node.
     */
    protected static final String PHONE = "phone";

    /**
     * SMS created time. This is different to the created node as it is available to providers prior
     * to the SMS being saved.
     */
    protected static final String CREATED_TIME = "createdTime";

    /**
     * Constructs an {@link AbstractSMSMessageLayoutStrategy}.
     *
     * @param showPatient determines if the patient should be displayed
     * @param createdTime the name of the node holding the created time of the communication
     */
    public AbstractSMSMessageLayoutStrategy(boolean showPatient, String createdTime) {
        super(showPatient, null, createdTime);
    }

    /**
     * Apply the layout strategy.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        // render the contact and phone together
        Property recipient = properties.get(CONTACT);
        Property phone = properties.get(PHONE);
        if (!context.isEdit()) {
            IMObjectBean bean = getBean(object);
            if (Objects.equals(bean.getTargetRef(CONTACT), bean.getTargetRef(CUSTOMER))) {
                // don't re-display the customer
                getArchetypeNodes().exclude(CUSTOMER);
            }
        }
        Component partyComponent = createComponent(recipient, object, context).getComponent();
        Component phoneComponent = createComponent(phone, object, context).getComponent();
        addComponent(new ComponentState(RowFactory.create(CELL_SPACING, partyComponent, phoneComponent), phone));

        return super.apply(object, properties, parent, context);
    }

    /**
     * Returns the properties to display in the header.
     * <p>
     * Note; this does not exclude empty properties the returned properties are used to determine the other nodes
     * to display. Empty properties are excluded via {@link #excludeEmptyHeaderProperties(List)} instead.
     *
     * @param properties the properties
     * @return the header properties
     */
    @Override
    protected List<Property> getHeaderProperties(List<Property> properties) {
        return include(properties, PHONE, DESCRIPTION);
    }

    /**
     * Returns the patient-related properties.
     * <p/>
     * This implementation includes the location, customer and patient.
     *
     * @param properties the properties
     * @return the header properties
     */
    @Override
    protected List<Property> getPatientProperties(List<Property> properties) {
        return include(properties, LOCATION, CUSTOMER, PATIENT);
    }

    /**
     * Excludes empty fields.
     *
     * @param properties the fields
     * @return the properties to render
     */
    @Override
    protected List<Property> excludeEmptyFields(List<Property> properties) {
        return excludeIfEmpty(properties, REASON);
    }

    /**
     * Creates a component to display the message.
     *
     * @param property the message property
     * @param context  the layout context
     * @return a new component
     */
    @Override
    protected ComponentState createMessage(Property property, LayoutContext context) {
        return createMultiLineText(property, 3, 20, Styles.FULL_WIDTH, context);
    }
}
