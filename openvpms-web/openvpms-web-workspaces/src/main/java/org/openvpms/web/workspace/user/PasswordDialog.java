/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.user;

import echopointng.LabelEx;
import echopointng.xhtml.XhtmlFragment;
import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.echo.text.PasswordField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.OpenVPMSApp;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Dialog that prompts the logged-in user to enter their password.
 * <p/>
 * If too many attempts are made, logs out the session.
 *
 * @author Tim Anderson
 */
public class PasswordDialog extends ModalDialog {

    private final OpenVPMSApp app;

    /**
     * The message to display.
     */
    private final String message;

    /**
     * The password encoder.
     */
    private final PasswordEncoder encoder;

    /**
     * The password.
     */
    private final PasswordField password;

    /**
     * The message/password/error container.
     */
    private Column container;

    private int attempts = 0;

    private static final int MAX_ATTEMPTS = 5;

    /**
     * Constructs a {@link PasswordDialog}.
     * <p/>
     * Use this constructor when constructing a dialog while the {@link OpenVPMSApp} is active.
     */
    public PasswordDialog() {
        this(OpenVPMSApp.getInstance());
    }

    /**
     * Constructs a {@link PasswordDialog}.
     *
     * @param app the application instance
     */
    public PasswordDialog(OpenVPMSApp app) {
        this(app, Messages.get("password.message"), OK_CANCEL);
    }

    /**
     * Constructs a {@link PasswordDialog}.
     *
     * @param app     the application instance
     * @param message the message to display
     * @param buttons the buttons to display
     */
    public PasswordDialog(OpenVPMSApp app, String message, String[] buttons) {
        super(Messages.get("password.title"), buttons, null);
        this.app = app;
        this.message = message;
        password = TextComponentFactory.createPassword(20);
        password.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onOK();
            }
        });
        encoder = app.getApplicationContext().getBean(PasswordEncoder.class);
        resize("PasswordDialog.size");
    }

    /**
     * Processes a user request to close the window (via the close button).
     * <p/>
     * This restores the previous focus
     */
    @Override
    public void userClose() {
        if (getAction() == null) {
            // need an action, otherwise onOK() will be invoked as it is the default action
            setAction(CANCEL_ID);
        }
        super.userClose();
    }

    /**
     * Invoked when the 'OK' button is pressed.
     */
    @Override
    protected void onOK() {
        UserDetails user = getUser();
        if (user != null) {
            if (checkPassword(user)) {
                super.onOK();
            } else {
                if (++attempts < MAX_ATTEMPTS) {
                    password.setText(null);
                    if (container.getComponentCount() == 3) {
                        container.add(LabelFactory.create("password.error", "login.error"));
                    }
                } else {
                    // force logout
                    app.logout();
                }
            }
        }
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        Label loggedIn = LabelFactory.create("password.loggedin");
        Label name = LabelFactory.create(null, Styles.BOLD);
        UserDetails user = getUser();
        if (user != null) {
            name.setText(user.getUsername());
        }

        LabelEx space = new LabelEx(new XhtmlFragment(TableHelper.SPACER));
        container = ColumnFactory.create(Styles.WIDE_CELL_SPACING,
                                         RowFactory.create(loggedIn, space, name),
                                         LabelFactory.text(message), password);
        Row row = RowFactory.create(Styles.LARGE_INSET, container);
        getLayout().add(row);
    }

    /**
     * Verifies that the entered password equals that of the user.
     *
     * @param user the user
     * @return {@code true} if the passwords match
     */
    private boolean checkPassword(UserDetails user) {
        String text = StringUtils.trimToNull(password.getText());
        return (text != null && encoder.matches(text, user.getPassword()));
    }

    /**
     * Returns the current user.
     *
     * @return the current user
     */
    private UserDetails getUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return (principal instanceof UserDetails) ? (UserDetails) principal : null;
    }
}