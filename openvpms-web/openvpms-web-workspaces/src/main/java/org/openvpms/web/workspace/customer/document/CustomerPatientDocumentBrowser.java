/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.workspace.customer.account.CustomerAccountActTableModel;
import org.openvpms.web.workspace.customer.account.CustomerAccountQuery;
import org.openvpms.web.workspace.customer.estimate.EstimateQuery;
import org.openvpms.web.workspace.patient.estimate.PatientEstimateQuery;

import java.util.Date;

/**
 * A browser for customer and patient documents.
 * <p/>
 * This includes customer account acts and estimates.
 *
 * @author Tim Anderson
 */
public class CustomerPatientDocumentBrowser extends AbstractCustomerPatientDocumentBrowser {

    /**
     * Constructs a {@link CustomerPatientDocumentBrowser}.
     *
     * @param customer the customer. May be {@code null}
     * @param patient  the patient. May be {@code null}
     * @param context  the layout context
     */
    public CustomerPatientDocumentBrowser(Party customer, Party patient, LayoutContext context) {
        this(customer, patient, true, null, null, context);
    }

    /**
     * Constructs a {@link CustomerPatientDocumentBrowser}.
     *
     * @param customer      the customer. May be {@code null}
     * @param patient       the patient. May be {@code null}
     * @param customerFirst if {@code true} display the customer tab first, otherwise display the patient tab
     * @param from          the from date. May  be {@code null}
     * @param to            the to date. May be {@code null}
     * @param context       the layout context
     */
    public CustomerPatientDocumentBrowser(Party customer, Party patient, boolean customerFirst, Date from, Date to,
                                          LayoutContext context) {
        super(customer, patient, customerFirst, from, to, context);
    }

    /**
     * Adds customer document browsers.
     *
     * @param customer the customer
     * @param patient  the patient. May be {@code null}
     */
    @Override
    protected void addCustomerBrowsers(Party customer, Party patient) {
        super.addCustomerBrowsers(customer, patient);
        addBrowser("workspace.customer.account", new CustomerAccountQuery<>(customer, getContext()),
                   new CustomerAccountActTableModel<>(true, true));
        EstimateQuery estimate = (patient != null) ? new PatientEstimateQuery(customer, patient, true)
                                                   : new EstimateQuery(customer);
        addBrowser("workspace.customer.estimate", estimate);
    }

}
