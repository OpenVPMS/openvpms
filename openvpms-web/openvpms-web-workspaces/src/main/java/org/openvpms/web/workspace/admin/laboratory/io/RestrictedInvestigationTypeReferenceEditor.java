/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory.io;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.ShortNameConstraint;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.edit.AbstractIMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.AbstractEntityQuery;
import org.openvpms.web.component.im.query.AbstractEntityResultSet;
import org.openvpms.web.component.im.query.DefaultQueryExecutor;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.Property;

/**
 * An investigation type selector that restricts investigation types to those that are not linked to laboratories.
 *
 * @author Tim Anderson
 */
class RestrictedInvestigationTypeReferenceEditor extends AbstractIMObjectReferenceEditor<Entity> {

    /**
     * Constructs a {@link RestrictedInvestigationTypeReferenceEditor}.
     *
     * @param property the reference property
     * @param context  the layout context
     */
    public RestrictedInvestigationTypeReferenceEditor(Property property, LayoutContext context) {
        super(property, null, context);
        setAllowCreate(true);
    }

    /**
     * Creates a query to select objects.
     *
     * @param name the name to filter on. May be {@code null}
     * @return a new query
     */
    @Override
    protected Query<Entity> createQuery(String name) {
        InvestigationTypeQuery query = new InvestigationTypeQuery();
        query.setValue(name);
        return query;
    }

    private static class InvestigationTypeQuery extends AbstractEntityQuery<Entity> {
        /**
         * Constructs an {@link InvestigationTypeQuery}.
         */
        public InvestigationTypeQuery() {
            super(new String[]{LaboratoryArchetypes.INVESTIGATION_TYPE}, Entity.class);
        }

        /**
         * Creates the result set.
         *
         * @param sort the sort criteria. May be {@code null}
         * @return a new result set
         */
        @Override
        protected ResultSet<Entity> createResultSet(SortConstraint[] sort) {
            return new InvestigationTypeResultSet(getArchetypeConstraint(), getValue(), sort, getMaxResults());
        }
    }

    private static class InvestigationTypeResultSet extends AbstractEntityResultSet<Entity> {
        /**
         * Constructs an {@link AbstractEntityResultSet}.
         *
         * @param archetypes the archetypes to query
         * @param value      the value to query on. May be {@code null}
         * @param sort       the sort criteria. May be {@code null}
         * @param rows       the maximum no. of rows per page
         */
        public InvestigationTypeResultSet(ShortNameConstraint archetypes, String value, SortConstraint[] sort, int rows) {
            super(archetypes, value, false, null, sort, rows, false, new DefaultQueryExecutor<>());
        }

        /**
         * Adds constraints to query nodes on a value.
         *
         * @param query the query to add the constraints to
         */
        @Override
        protected void addValueConstraints(ArchetypeQuery query) {
            super.addValueConstraints(query);
            query.add(Constraints.leftJoin("laboratory", "lab"));
            query.add(Constraints.isNull("lab.target"));
        }
    }
}