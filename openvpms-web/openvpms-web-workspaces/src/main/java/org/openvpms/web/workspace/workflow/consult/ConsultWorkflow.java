/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.consult;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.component.workflow.AbstractConfirmationTask;
import org.openvpms.web.component.workflow.ConditionalCreateTask;
import org.openvpms.web.component.workflow.ConditionalTask;
import org.openvpms.web.component.workflow.DefaultTaskContext;
import org.openvpms.web.component.workflow.LocalTask;
import org.openvpms.web.component.workflow.NodeConditionTask;
import org.openvpms.web.component.workflow.ReloadTask;
import org.openvpms.web.component.workflow.SynchronousTask;
import org.openvpms.web.component.workflow.Task;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.component.workflow.TaskProperties;
import org.openvpms.web.component.workflow.UpdateActStatusTask;
import org.openvpms.web.component.workflow.UpdateActTask;
import org.openvpms.web.component.workflow.WorkflowImpl;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.workflow.EditVisitTask;
import org.openvpms.web.workspace.workflow.GetClinicalEventTask;
import org.openvpms.web.workspace.workflow.MandatoryCustomerAlertTask;
import org.openvpms.web.workspace.workflow.MandatoryPatientAlertTask;

import java.util.Date;

/**
 * Consult workflow.
 *
 * @author Tim Anderson
 */
public class ConsultWorkflow extends WorkflowImpl {

    /**
     * The initial context.
     */
    private final TaskContext initial;

    /**
     * Constructs a {@link ConsultWorkflow} from an <em>act.customerAppointment</em> or <em>act.customerTask</em>.
     *
     * @param act      the act
     * @param external the external context to access and update
     * @param help     the help context
     */
    public ConsultWorkflow(Act act, final Context external, HelpContext help) {
        super(help);
        initial = createContext(act, external, help);
        addTasks(act, external);
    }

    /**
     * Starts the workflow.
     */
    @Override
    public void start() {
        super.start(initial);
    }

    /**
     * Creates the context to run tasks with.
     *
     * @param act      the appointment/task
     * @param external the external context
     * @param help     the help context
     * @return a new context
     */
    protected TaskContext createContext(Act act, Context external, HelpContext help) {
        Party practice = external.getPractice();
        if (practice == null) {
            throw new IllegalStateException("Context has no practice");
        }
        IMObjectBean bean = getBean(act);
        Party customer = bean.getTarget("customer", Party.class);
        Party patient = bean.getTarget("patient", Party.class);
        User clinician;
        if (UserHelper.useLoggedInClinician(external)) {
            clinician = external.getUser();
        } else {
            clinician = bean.getTarget("clinician", User.class);
            if (clinician == null) {
                clinician = external.getClinician();
            }
        }

        TaskContext context = new DefaultTaskContext(help);
        context.setCustomer(customer);
        context.setPatient(patient);
        context.setClinician(clinician);
        context.setUser(external.getUser());
        context.setPractice(practice);
        context.setLocation(external.getLocation());
        context.setDepartment(external.getDepartment());
        context.addObject(act);
        return context;
    }

    /**
     * Adds the consult workflow tasks.
     *
     * @param act      the appointment/task
     * @param external the external context to update at the end of the workflow
     */
    protected void addTasks(Act act, final Context external) {
        if (!UserHelper.useLoggedInClinician(initial)) {
            addTask(new ClinicianSelectionTask(getHelpContext()));
        }

        // update the act status to IN_PROGRESS if it's not BILLED or COMPLETED
        addTask(createInProgressTask(act));
        addTask(new UpdateClinicianTask(act.getArchetype()));

        addTask(new MandatoryCustomerAlertTask());
        addTask(new MandatoryPatientAlertTask());

        Act appointment = TypeHelper.isA(act, ScheduleArchetypes.APPOINTMENT) ? act : null;
        addTask(new GetClinicalEventTask(act.getActivityStartTime(), appointment));
        addTask(new UpdateClinicianTask(PatientArchetypes.CLINICAL_EVENT));

        // get the latest invoice, possibly associated with the event. If none exists, creates a new one
        addTask(new GetConsultInvoiceTask());
        addTask(new ConditionalCreateTask(CustomerAccountArchetypes.INVOICE));
        addTask(new UpdateInvoiceClinicianTask());

        // edit the act.patientClinicalEvent in a local context, propagating the patient, customer, clinician and
        // department on completion
        addTask(new LocalTask(createEditVisitTask(), Context.PATIENT_SHORTNAME, Context.CUSTOMER_SHORTNAME,
                              Context.CLINICIAN_SHORTNAME, PracticeArchetypes.DEPARTMENT));

        // Reload the task to refresh the context with any edits made
        addTask(new ReloadTask(PatientArchetypes.CLINICAL_EVENT));

        // update the task/appointment status to BILLED if the invoice is COMPLETED
        addTask(createBilledTask(act));

        // add a task to update the global context at the end of the workflow
        addTask(new SynchronousTask() {
            public void execute(TaskContext context) {
                external.setCustomer(context.getCustomer());
                external.setPatient(context.getPatient());
                external.setClinician(context.getClinician());
                external.setDepartment(context.getDepartment());
            }
        });
    }

    /**
     * Creates a new {@link EditVisitTask}.
     *
     * @return a new task to edit the visit
     */
    protected EditVisitTask createEditVisitTask() {
        return new EditVisitTask();
    }

    /**
     * Creates a task to update the appointment/task act status to {@code IN_PROGRESS} if it is not {@code IN_PROGRESS},
     * {@code BILLED} or {@code COMPLETED}.
     * <p>
     * For task acts, this also sets the "arrivalTime" node to the current time.
     *
     * @param act the appointment or task act
     * @return a new task
     */
    private Task createInProgressTask(Act act) {
        String archetype = act.getArchetype();
        TaskProperties properties = new TaskProperties();
        properties.add("status", WorkflowStatus.IN_PROGRESS);
        if (act.isA(ScheduleArchetypes.TASK)) {
            properties.add("consultStartTime", Date::new);
        }
        UpdateActTask<Act> task = new UpdateActTask<>(archetype, properties);
        task.skipWhenStatusIn(WorkflowStatus.IN_PROGRESS, WorkflowStatus.BILLED, WorkflowStatus.COMPLETED);
        return task;
    }

    /**
     * Creates a task to update the appointment/task act status to {@code BILLED} if the invoice is {@code COMPLETED}.
     *
     * @param act the appointment or task act
     * @return a new task
     */
    private Task createBilledTask(Act act) {
        NodeConditionTask<String> invoiceCompleted
                = new NodeConditionTask<>(CustomerAccountArchetypes.INVOICE, "status", ActStatus.COMPLETED);
        UpdateActStatusTask billTask = new UpdateActStatusTask(act.getArchetype(), WorkflowStatus.BILLED);
        billTask.skipWhenStatusIn(WorkflowStatus.COMPLETED);
        return new ConditionalTask(invoiceCompleted, billTask);
    }

    /**
     * A task to update the invoice clinician. This doesn't save the invoice, and doesn't update if the invoice
     * is POSTED.
     */
    private static class UpdateInvoiceClinicianTask extends UpdateClinicianTask {

        /**
         * Constructs an {@link UpdateInvoiceClinicianTask}.
         */
        public UpdateInvoiceClinicianTask() {
            super(CustomerAccountArchetypes.INVOICE, false); // don't save the invoice. It may be invalid
            skipWhenStatusIn(ActStatus.POSTED);
        }
    }

    private static class ClinicianSelectionTask extends AbstractConfirmationTask {

        /**
         * Constructs a {@link ClinicianSelectionTask}.
         *
         * @param help the help context
         */
        public ClinicianSelectionTask(HelpContext help) {
            super(help);
        }

        /**
         * Creates a new confirmation dialog.
         *
         * @param context the task context
         * @param help    the help context
         * @return a new confirmation dialog
         */
        @Override
        protected ConfirmationDialog createConfirmationDialog(final TaskContext context, HelpContext help) {
            return new ClinicianSelectionDialog(context, Messages.get("consult.clinician.title"),
                                                Messages.get("consult.clinician.message"), help);
        }
    }
}
