/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory.io;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.laboratory.internal.io.LaboratoryTestData;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.TableColumnFactory;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

import static org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes.INVESTIGATION_TYPE;
import static org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes.TEST;

/**
 * Laboratory test data table model.
 *
 * @author Tim Anderson
 */
class LaboratoryTestDataModel extends AbstractIMTableModel<LaboratoryTestData> {

    /**
     * The id column.
     */
    private static final int ID = 0;

    /**
     * The provider column.
     */
    private static final int LABORATORY = ID + 1;

    /**
     * The test code column.
     */
    private static final int CODE = LABORATORY + 1;

    /**
     * The test name column.
     */
    private static final int NAME = CODE + 1;

    /**
     * The test description column.
     */
    private static final int DESCRIPTION = NAME + 1;

    /**
     * The old price column.
     */
    private static final int OLD_PRICE = DESCRIPTION + 1;

    /**
     * The new price column.
     */
    private static final int NEW_PRICE = OLD_PRICE + 1;

    /**
     * The specimen collection details column.
     */
    private static final int SPECIMEN = NEW_PRICE + 1;

    /**
     * The turnaround time details column.
     */
    private static final int TURNAROUND = SPECIMEN + 1;

    /**
     * Constructs a {@link LaboratoryTestDataModel}.
     */
    public LaboratoryTestDataModel() {
        DefaultTableColumnModel model = new DefaultTableColumnModel();
        model.addColumn(TableColumnFactory.create(ID, getDisplayName(TEST, "id")));
        model.addColumn(TableColumnFactory.create(LABORATORY, getDisplayName(INVESTIGATION_TYPE, "laboratory")));
        model.addColumn(TableColumnFactory.create(CODE, getDisplayName(TEST, "code")));
        model.addColumn(TableColumnFactory.create(NAME, getDisplayName(TEST, "name")));
        model.addColumn(TableColumnFactory.create(DESCRIPTION, getDisplayName(TEST, "description")));
        model.addColumn(createTableColumn(OLD_PRICE, "admin.laboratory.import.oldPrice"));
        model.addColumn(createTableColumn(NEW_PRICE, "admin.laboratory.import.newPrice"));
        model.addColumn(TableColumnFactory.create(SPECIMEN, getDisplayName(TEST, "specimen")));
        model.addColumn(TableColumnFactory.create(TURNAROUND, getDisplayName(TEST, "turnaround")));
        setTableColumnModel(model);
    }

    /**
     * Returns the sort criteria.
     *
     * @param column    the primary sort column
     * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
     * @return the sort criteria, or {@code null} if the column isn't sortable
     */
    @Override
    public SortConstraint[] getSortConstraints(int column, boolean ascending) {
        return null;
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the column
     * @param row    the row
     * @return the value at the given coordinate.
     */
    @Override
    protected Object getValue(LaboratoryTestData object, TableColumn column, int row) {
        Object result;
        switch (column.getModelIndex()) {
            case ID:
                result = getId(object);
                break;
            case LABORATORY:
                result = object.getLaboratory();
                break;
            case CODE:
                result = object.getCode();
                break;
            case NAME:
                result = object.getName();
                break;
            case DESCRIPTION:
                result = object.getDescription();
                break;
            case OLD_PRICE:
                result = TableHelper.rightAlign(NumberFormatter.formatCurrency(object.getOldPrice()));
                break;
            case NEW_PRICE:
                result = TableHelper.rightAlign(NumberFormatter.formatCurrency(object.getNewPrice()));
                break;
            case SPECIMEN:
                result = object.getSpecimen();
                break;
            case TURNAROUND:
                result = object.getTurnaround();
                break;
            default:
                result = null;
        }
        return result;
    }

    /**
     * Returns the test identifier.
     *
     * @param object the laboratory test data
     * @return the test identifier
     */
    private Object getId(LaboratoryTestData object) {
        Object result;
        if (object.getTest() == null || object.getTest().isNew()) {
            result = LabelFactory.text(Messages.format("editor.new.title", getDisplayName(TEST)), Styles.ITALIC);
        } else {
            result = TableHelper.rightAlign(Long.toString(object.getTest().getId()));
        }
        return result;
    }
}