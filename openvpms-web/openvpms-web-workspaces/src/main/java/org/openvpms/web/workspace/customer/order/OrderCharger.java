/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.order;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.RadioButton;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.button.ButtonGroup;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.order.OrderArchetypes;
import org.openvpms.archetype.rules.finance.order.OrderRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.QueryHelper;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.OptionDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditDialog;
import org.openvpms.web.workspace.customer.charge.CustomerChargeActEditor;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.component.business.service.archetype.helper.DescriptorHelper.getDisplayName;

/**
 * Charges customer orders.
 *
 * @author Tim Anderson
 */
public class OrderCharger {

    /**
     * The customer to query orders for.
     */
    private final Party customer;

    /**
     * The patient to query orders for.
     */
    private final Party patient;

    /**
     * The customer order rules.
     */
    private final OrderRules rules;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules accountRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The rule based archetype service.
     */
    private final IArchetypeRuleService ruleService;

    /**
     * The current context.
     */
    private final Context context;

    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The orders that have been charged, but not saved.
     */
    private final List<Act> charged = new ArrayList<>();

    /**
     * Constructs an {@link OrderCharger}.
     *
     * @param customer     the customer
     * @param rules        the order rules
     * @param accountRules the customer account rules
     * @param service      the archetype service
     * @param ruleService  the rule based archetype service
     * @param context      the context
     * @param help         the help context
     */
    OrderCharger(Party customer, OrderRules rules, CustomerAccountRules accountRules, IArchetypeService service,
                 IArchetypeRuleService ruleService, Context context, HelpContext help) {
        this(customer, null, rules, accountRules, service, ruleService, context, help);
    }

    /**
     * Constructs an {@link OrderCharger}.
     *
     * @param customer     the customer
     * @param patient      the patient. May be {@code null}
     * @param rules        the order rules
     * @param accountRules the customer account rules
     * @param service      the archetype service
     * @param ruleService  the rule based archetype service
     * @param context      the context
     * @param help         the help context
     */
    OrderCharger(Party customer, Party patient, OrderRules rules, CustomerAccountRules accountRules,
                 IArchetypeService service, IArchetypeRuleService ruleService, Context context, HelpContext help) {
        if (service instanceof IArchetypeRuleService) {
            throw new IllegalArgumentException("Argument 'service' must not implement IArchetypeRuleService");
        }
        this.customer = customer;
        this.patient = patient;
        this.rules = rules;
        this.accountRules = accountRules;
        this.service = service;
        this.ruleService = ruleService;

        // make sure the context reflects the customer and patient being charged
        LocalContext copy = LocalContext.copy(context);
        copy.setCustomer(customer);
        copy.setPatient(patient);
        this.context = copy;
        this.help = help;
    }

    /**
     * Returns the customer.
     *
     * @return the customer
     */
    public Party getCustomer() {
        return customer;
    }

    /**
     * Determines if the customer has pending orders.
     * <p>
     * If a patient was supplied at construction, this limits the query to those orders that are for the patient.
     *
     * @return {@code true} if the customer has pending orders
     */
    public boolean hasOrders() {
        if (charged.isEmpty()) {
            return rules.hasOrders(customer, patient);
        } else {
            PendingOrderQuery query = new PendingOrderQuery(customer, patient, charged);
            List<Act> orders = QueryHelper.query(query);
            return !orders.isEmpty();
        }
    }

    /**
     * Saves any charged orders.
     *
     * @throws OpenVPMSException for any error
     */
    public void save() {
        if (!charged.isEmpty()) {
            ruleService.save(charged);
        }
    }

    /**
     * Clears any charged orders.
     * <p>
     * This should be invoked after a successful {@link #save()}
     */
    public void clear() {
        charged.clear();
    }

    /**
     * Returns the number of charged orders.
     */
    public int getChargedOrders() {
        int result = 0;
        for (Act act : charged) {
            if (act.isA(OrderArchetypes.ORDERS)) {
                result++;
            }
        }
        return result;
    }

    /**
     * Returns the number of charged order returns.
     *
     * @return the number of charged order returns
     */
    public int getChargedReturns() {
        int result = charged.size();
        if (result != 0) {
            result -= getChargedOrders();
        }
        return result;
    }

    /**
     * Charges an order or return.
     *
     * @param act      the order/return
     * @param listener the listener to notify when charging completes
     */
    public void charge(FinancialAct act, Runnable listener) {
        OrderInvoicer invoicer = createInvoicer(act);
        Validator validator = new DefaultValidator();
        if (invoicer != null && invoicer.validate(validator)) {
            FinancialAct invoice = invoicer.getInvoice();
            if (invoice == null || ActStatus.POSTED.equals(invoice.getStatus())) {
                // the order/return doesn't relate to an invoice, or the invoice is POSTED
                if (invoicer.notFromInvoice()) {
                    invoiceOrCredit(act, invoicer, listener);
                } else if (invoicer.canInvoice()) {
                    invoice(act, invoicer, listener);
                } else if (invoicer.canCredit()) {
                    credit(act, invoicer, listener);
                } else {
                    show(Messages.format("customer.order.invoice.unsupported", getDisplayName(act, service)),
                         listener);
                }
            } else {
                if (invoicer.canInvoice()) {
                    doCharge(act, invoicer, invoice, new DefaultLayoutContext(context, help), listener);
                } else if (invoicer.canCredit()) {
                    credit(act, invoicer, listener);
                } else {
                    show(Messages.format("customer.order.invoice.unsupported", getDisplayName(act, service)),
                         listener);
                }
            }
        } else {
            String title = Messages.format("customer.order.invalid", getDisplayName(act, service));
            ValidationHelper.showError(title, validator, listener);
        }
    }

    /**
     * Displays a prompt to charge pending orders, if any.
     *
     * @param editor   the editor to add charges to
     * @param listener the listener to notify when charging completes
     */
    public void charge(CustomerChargeActEditor editor, Runnable listener) {
        PendingOrderQuery query = new PendingOrderQuery(customer, null, charged);
        ResultSet<Act> set = query.query();
        if (!set.hasNext()) {
            if (charged.isEmpty()) {
                show(Messages.format("customer.order.none", customer.getName()), listener);
            } else {
                show(Messages.format("customer.order.unsaved", customer.getName()), listener);
            }
        } else {
            PendingOrderBrowser browser = new PendingOrderBrowser(query, new DefaultLayoutContext(context, help));
            browser.query();
            PendingOrderDialog dialog = new PendingOrderDialog(Messages.get("customer.order.invoice.title"),
                                                               browser, new LocalContext(context), help);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    List<Act> orders = browser.getOrders();
                    charge(orders, editor, listener);
                }
            });
            dialog.show();
        }
    }

    /**
     * Charges any complete orders.
     *
     * @param editor the editor to add charges to
     */
    public void chargeComplete(CustomerChargeActEditor editor) {
        PendingOrderQuery query = new PendingOrderQuery(customer, null, charged);
        List<Act> orders = QueryHelper.query(query);
        for (Act order : orders) {
            OrderInvoicer invoicer = createInvoicer(order);
            if (invoicer != null && invoicer.isValid() && invoicer.canCharge(editor)
                && (patient == null || invoicer.canCharge(patient))) {
                invoicer.charge(editor);
                charged.add(order);
            }
        }
    }

    /**
     * Creates an invoicer for an order or return.
     *
     * @param act the order/return
     * @return a new invoicer, or {@code null} if the act is not supported
     */
    private OrderInvoicer createInvoicer(Act act) {
        if (TypeHelper.isA(act, OrderArchetypes.PHARMACY_ORDER, OrderArchetypes.PHARMACY_RETURN)) {
            return new PharmacyOrderInvoicer((FinancialAct) act, context.getClinician(), rules, service);
        } else if (TypeHelper.isA(act, OrderArchetypes.INVESTIGATION_RETURN)) {
            return new InvestigationOrderInvoicer((FinancialAct) act, context.getClinician(), rules, service);
        }
        return null;
    }

    /**
     * Charges orders.
     *
     * @param orders   the orders to charge
     * @param editor   the editor to add charges to
     * @param listener the listener to notify when charging completes
     */
    private void charge(List<Act> orders, CustomerChargeActEditor editor, Runnable listener) {
        StringBuilder messages = new StringBuilder();
        for (Act order : orders) {
            OrderInvoicer invoicer = createInvoicer(order);
            if (invoicer != null) {
                if (!invoicer.isValid()) {
                    append(messages, Messages.format("customer.order.incomplete", order.getId(),
                                                     getDisplayName(order, service)));
                } else if (patient != null && !invoicer.canCharge(patient)) {
                    append(messages, Messages.format("customer.order.multiplePatient", order.getId(),
                                                     getDisplayName(order, service)));
                } else {
                    OrderInvoicer.Status status = invoicer.getChargeStatus(editor);
                    if (!status.canCharge()) {
                        append(messages, status.getReason());
                    } else {
                        invoicer.charge(editor);
                        charged.add(order);
                    }
                }
            }
        }
        if (messages.length() != 0) {
            show(messages.toString(), listener);
        } else {
            listener.run();
        }
    }

    /**
     * Invoices an order.
     *
     * @param act      the order
     * @param invoicer the order invoicer
     * @param listener the listener to notify on completion
     */
    private void invoice(FinancialAct act, OrderInvoicer invoicer, Runnable listener) {
        if (invoicer.requiresEdit()) {
            FinancialAct current = accountRules.getInvoice(invoicer.getCustomer());
            charge(act, current, invoicer, listener);
        } else {
            invoicer.charge();
            listener.run();
        }
    }

    /**
     * Credits a return.
     *
     * @param act      the order return
     * @param invoicer the order invoicer
     * @param listener the listener to notify on completion
     */
    private void credit(FinancialAct act, OrderInvoicer invoicer, Runnable listener) {
        FinancialAct current = accountRules.getCredit(invoicer.getCustomer());
        charge(act, current, invoicer, listener);
    }

    /**
     * Invoices or credits an order/return that is unrelated to an existing invoice.
     *
     * @param act      the order/return
     * @param invoicer the order invoicer
     * @param listener the listener to notify on completion
     */
    private void invoiceOrCredit(FinancialAct act, OrderInvoicer invoicer, Runnable listener) {
        if (invoicer.isOrder()) {
            invoice(act, invoicer, listener);
        } else {
            String displayName = getDisplayName(act, service);
            String title = Messages.format("customer.order.return.credit.title", displayName);
            String message = Messages.format("customer.order.return.credit.message", displayName);
            String[] options = {DescriptorHelper.getDisplayName(CustomerAccountArchetypes.CREDIT, service),
                                DescriptorHelper.getDisplayName(CustomerAccountArchetypes.INVOICE, service)};
            OptionDialog dialog = new OptionDialog(title, message, options);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    boolean credit = dialog.getSelected() == 0;
                    applyReturn(act, credit, invoicer, listener);
                }
            });
            dialog.show();
        }
    }

    /**
     * Applies an order return.
     *
     * @param act      the order return
     * @param credit   if {@code true}, apply it to a credit, otherwise apply it to an invoice
     * @param invoicer the order invoicer
     * @param listener the listener to notify on completion
     */
    private void applyReturn(FinancialAct act, boolean credit, OrderInvoicer invoicer, Runnable listener) {
        if (credit) {
            credit(act, invoicer, listener);
        } else {
            FinancialAct current = accountRules.getInvoice(invoicer.getCustomer());
            if (current == null) {
                FinancialAct invoice = invoicer.createInvoice(customer);
                returnItems(act, invoice, invoicer, listener);
            } else {
                SelectChargeDialog dialog = createSelectChargeDialog(act, current, invoicer);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onOK() {
                        FinancialAct invoice;
                        if (dialog.createCharge()) {
                            invoice = invoicer.createInvoice(customer);
                        } else {
                            invoice = current;
                        }
                        returnItems(act, invoice, invoicer, listener);
                    }
                });
                dialog.show();
            }
        }
    }

    /**
     * Attempts to apply an order return to an invoice.
     *
     * @param act      the order return
     * @param invoice  the invoice
     * @param invoicer the invoicer
     * @param listener the listener to notify on completion
     */
    private void returnItems(FinancialAct act, FinancialAct invoice, OrderInvoicer invoicer, Runnable listener) {
        DefaultLayoutContext context = new DefaultLayoutContext(this.context, help);
        CustomerChargeActEditDialog dialog = invoicer.returnItems(invoice, this, context);
        if (dialog != null) {
            dialog.addWindowPaneListener(new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    listener.run();
                }
            });
            if (ActStatus.POSTED.equals(act.getStatus())) {
                charged.add(act);
                dialog.checkOrders();
            }
        }
    }

    /**
     * Charges an order or order return.
     * <p>
     * If there is no current invoice, the item will be charged and the invoice/credit displayed in an editor.
     * If there is an invoice, a dialog will be displayed to invoice the order to a the current invoice, or a new
     * invoice.
     *
     * @param act      the order/order return
     * @param current  the current invoice or credit. May be {@code null}
     * @param invoicer the order invoicer
     * @param listener the listener to notify on completion
     */
    private void charge(FinancialAct act, FinancialAct current, OrderInvoicer invoicer, Runnable listener) {
        DefaultLayoutContext context = new DefaultLayoutContext(this.context, help);
        context.setEdit(true);
        if (current == null) {
            // no current charge
            doCharge(act, invoicer, null, context, listener);
        } else {
            SelectChargeDialog dialog = createSelectChargeDialog(act, current, invoicer);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    if (dialog.createCharge()) {
                        doCharge(act, invoicer, null, context, listener);
                    } else {
                        doCharge(act, invoicer, current, context, listener);
                    }
                }
            });
            dialog.show();
        }
    }

    /**
     * Creates a dialog to select a charge.
     *
     * @param act      the order/order return
     * @param current  the current charge
     * @param invoicer the order invoicer
     * @return the dialog
     */
    private SelectChargeDialog createSelectChargeDialog(FinancialAct act, FinancialAct current,
                                                        OrderInvoicer invoicer) {
        String displayName = getDisplayName(current, service);
        String title = Messages.format("customer.order.currentcharge.title", displayName);
        String message;
        if (invoicer.getInvoice() != null && ActStatus.POSTED.equals(invoicer.getInvoice().getStatus())) {
            // original charge is posted
            message = Messages.format("customer.order.currentcharge.original",
                                      getDisplayName(act, service), displayName);
        } else {
            // no original charge
            message = Messages.format("customer.order.currentcharge.current", displayName);
        }
        return new SelectChargeDialog(title, message, displayName);
    }

    private void doCharge(FinancialAct act, OrderInvoicer invoicer, FinancialAct current,
                          DefaultLayoutContext context, Runnable listener) {
        CustomerChargeActEditDialog dialog = invoicer.charge(current, this, context);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                listener.run();
            }
        });
        if (ActStatus.POSTED.equals(act.getStatus())) {
            charged.add(act);
            dialog.checkOrders();
        }
    }

    private void show(String message, Runnable listener) {
        InformationDialog dialog = new InformationDialog(message);
        dialog.addWindowPaneListener(new WindowPaneListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                listener.run();
            }
        });
        dialog.show();
    }

    /**
     * Helper to append a message to a buffer, inserting a new-line if required.
     *
     * @param buffer  the buffer
     * @param message the message to append
     */
    private void append(StringBuilder buffer, String message) {
        if (buffer.length() != 0) {
            buffer.append("\n");
        }
        buffer.append(message);
    }

    private static class SelectChargeDialog extends ConfirmationDialog {

        /**
         * Selects the current charge.
         */
        private final RadioButton currentInvoice;

        /**
         * Selects a new charge.
         */
        private final RadioButton newInvoice;

        /**
         * Constructs a {@link SelectChargeDialog}.
         *
         * @param title       the window title
         * @param message     the message
         * @param displayName the charge display name
         */
        SelectChargeDialog(String title, String message, String displayName) {
            super(title, message);
            ButtonGroup group = new ButtonGroup();
            currentInvoice = create(Messages.format("customer.order.currentcharge", displayName), group);
            newInvoice = create(Messages.format("customer.order.newcharge", displayName), group);
            currentInvoice.setSelected(true);
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Label message = LabelFactory.create(true, true);
            message.setText(getMessage());
            Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING, message,
                                                 ColumnFactory.create(Styles.CELL_SPACING, currentInvoice, newInvoice));
            Row row = RowFactory.create(Styles.LARGE_INSET, column);
            getLayout().add(row);
        }

        /**
         * Determines if a new charge should be created.
         *
         * @return {@code true} if a new charge should be created
         */
        boolean createCharge() {
            return newInvoice.isSelected();
        }

        private RadioButton create(String label, ButtonGroup group) {
            RadioButton button = ButtonFactory.create(null, group);
            button.setText(label);
            return button;
        }
    }

}
