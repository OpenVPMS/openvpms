/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.SplitPane;
import org.openvpms.archetype.rules.prefs.Preferences;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.DefaultContextSwitchListener;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.BrowserFactory;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.im.table.act.AbstractActTableModel;
import org.openvpms.web.component.mail.MailContext;
import org.openvpms.web.component.workspace.AbstractCRUDWindow;
import org.openvpms.web.component.workspace.BrowserCRUDWindowTab;
import org.openvpms.web.component.workspace.TabComponent;
import org.openvpms.web.component.workspace.TabbedWorkspace;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.tabpane.ObjectTabPaneModel;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerSummary;
import org.openvpms.web.workspace.patient.summary.CustomerPatientSummaryFactory;
import org.openvpms.web.workspace.reporting.charge.search.ChargesCRUDWindow;
import org.openvpms.web.workspace.reporting.charge.search.ChargesQuery;
import org.openvpms.web.workspace.reporting.charge.wip.IncompleteChargesCRUDWindow;
import org.openvpms.web.workspace.reporting.charge.wip.IncompleteChargesQuery;


/**
 * Workspace to:
 * <ul>
 *     <li>detail customer charges that are works-in-progress, i.e not POSTED.</li>
 *     <li>query charges</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class ChargesWorkspace extends TabbedWorkspace<FinancialAct> {

    /**
     * The user preferences.
     */
    private final Preferences preferences;

    /**
     * Constructs a {@link ChargesWorkspace}.
     *
     * @param context     the context
     * @param mailContext the mail context
     * @param preferences the preferences
     */
    public ChargesWorkspace(Context context, MailContext mailContext, Preferences preferences) {
        super("reporting.charge", context);
        setMailContext(mailContext);
        this.preferences = preferences;
    }

    /**
     * Renders the workspace summary.
     *
     * @return the component representing the workspace summary, or {@code null} if there is no summary
     */
    @Override
    public Component getSummary() {
        Component result = null;
        Tab tab = (Tab) getSelected();
        Party customer = (tab != null) ? tab.getCustomer() : null;
        if (customer != null) {
            CustomerPatientSummaryFactory factory = ServiceHelper.getBean(CustomerPatientSummaryFactory.class);
            CustomerSummary summary = factory.createCustomerSummary(getContext(), getHelpContext(), preferences);
            result = summary.getSummary(customer);
        }
        return result;
    }

    /**
     * Returns the class type that this operates on.
     *
     * @return the class type that this operates on
     */
    @Override
    protected Class<FinancialAct> getType() {
        return FinancialAct.class;
    }

    /**
     * Adds tabs to the tabbed pane.
     *
     * @param model the tabbed pane model
     */
    @Override
    protected void addTabs(ObjectTabPaneModel<TabComponent> model) {
        addWorkInProgress(model);
        addSearch(model);
    }

    /**
     * Adds a browser for work-in-progress (i.e. not POSTED) charges.
     *
     * @param model the tab model
     */
    private void addWorkInProgress(ObjectTabPaneModel<TabComponent> model) {
        HelpContext help = subtopic("wip");
        IncompleteChargesQuery query = new IncompleteChargesQuery(new DefaultLayoutContext(getContext(), help));

        DefaultLayoutContext layoutContext = new DefaultLayoutContext(getContext(), help);
        layoutContext.setContextSwitchListener(DefaultContextSwitchListener.INSTANCE);
        QueryBrowser<FinancialAct> browser = (QueryBrowser<FinancialAct>) BrowserFactory.create(
                query, null, new ChargesTableModel(layoutContext), layoutContext);
        Archetypes<FinancialAct> archetypes = Archetypes.create(IncompleteChargesQuery.SHORT_NAMES, FinancialAct.class);
        IncompleteChargesCRUDWindow window = new IncompleteChargesCRUDWindow(archetypes, browser, getContext(), help);
        addTab("reporting.charge.wip", model, new Tab(browser, window));
    }

    /**
     * Adds a browser to search for charges.
     *
     * @param model the tab model
     */
    private void addSearch(ObjectTabPaneModel<TabComponent> model) {
        HelpContext help = subtopic("search");
        ChargesQuery query = new ChargesQuery(new DefaultLayoutContext(getContext(), help));
        DefaultLayoutContext layoutContext = new DefaultLayoutContext(getContext(), help);
        layoutContext.setContextSwitchListener(DefaultContextSwitchListener.INSTANCE);
        QueryBrowser<FinancialAct> browser = (QueryBrowser<FinancialAct>) BrowserFactory.create(
                query, null, new ChargesTableModel(layoutContext), layoutContext);
        Archetypes<FinancialAct> archetypes = Archetypes.create(IncompleteChargesQuery.SHORT_NAMES, FinancialAct.class);
        ChargesCRUDWindow window = new ChargesCRUDWindow(archetypes, browser, getContext(), help);
        addTab("reporting.charge.search", model, new Tab(browser, window, false));
    }

    private class Tab extends BrowserCRUDWindowTab<FinancialAct> {

        /**
         * Constructs a {@link Tab}.
         *
         * @param browser the browser
         * @param window  the window
         */
        public Tab(Browser<FinancialAct> browser, AbstractCRUDWindow<FinancialAct> window) {
            super(browser, window);
        }

        /**
         * Constructs a {@link Tab}.
         *
         * @param browser       the browser
         * @param window        the window
         * @param refreshOnShow determines if the browser should be refreshed when the tab is displayed.
         */
        public Tab(Browser<FinancialAct> browser, AbstractChargesCRUDWindow window, boolean refreshOnShow) {
            super(browser, window, refreshOnShow);
        }

        /**
         * Invoked when the tab is displayed.
         */
        @Override
        public void show() {
            super.show();
            firePropertyChange(SUMMARY_PROPERTY, null, null);
        }

        /**
         * Returns the tab component.
         *
         * @return the tab component
         */
        @Override
        public Component getComponent() {
            return SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP, "CRUDWindow",
                                           getWindow().getComponent(), getBrowser().getComponent());
        }

        /**
         * Returns the customer of the selected object.
         *
         * @return the customer, or {@code null} if there is none
         */
        public Party getCustomer() {
            return getWindow().getCustomer();
        }

        /**
         * Returns the CRUD window.
         *
         * @return the window
         */
        @Override
        public AbstractChargesCRUDWindow getWindow() {
            return (AbstractChargesCRUDWindow) super.getWindow();
        }

        /**
         * Selects the current object.
         *
         * @param object the selected object
         */
        @Override
        protected void select(FinancialAct object) {
            boolean refresh = refreshSummary(object);
            super.select(object);
            if (refresh) {
                firePropertyChange(SUMMARY_PROPERTY, null, null);
            }
        }

        /**
         * Invoked when an object is double-clicked.
         * <p/>
         * This views the object.
         */
        @Override
        protected void onDoubleClick() {
            getWindow().view();
        }

        /**
         * Refresh the browser.
         *
         * @param object the object to select. May be {@code null}
         */
        @Override
        protected void refreshBrowser(FinancialAct object) {
            boolean refresh = refreshSummary(object);
            super.refreshBrowser(object);
            if (refresh) {
                firePropertyChange(SUMMARY_PROPERTY, null, null);
            }
        }

        /**
         * Determines if the summary should be refreshed.
         *
         * @param object the object to select. May be {@code null}
         * @return {@code true} the current selected tab is the same as this, and {@code object} is different to the
         * existing selection or {@code null}
         */
        private boolean refreshSummary(FinancialAct object) {
            FinancialAct current = getWindow().getObject();
            return getSelected() == this && current != object || object == null;
        }
    }

    private static class ChargesTableModel extends AbstractActTableModel<FinancialAct> {

        public ChargesTableModel(LayoutContext context) {
            super(IncompleteChargesQuery.SHORT_NAMES, context);
        }

        /**
         * Returns a list of descriptor names to include in the table.
         *
         * @return the list of descriptor names to include in the table
         */
        @Override
        protected String[] getNodeNames() {
            return new String[]{"id", "startTime", "customer", "status", "amount", "notes"};
        }
    }
}
