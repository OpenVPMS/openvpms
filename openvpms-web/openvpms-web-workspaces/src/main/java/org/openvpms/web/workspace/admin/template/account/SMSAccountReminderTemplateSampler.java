/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template.account;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.customer.CustomerReferenceEditor;
import org.openvpms.web.component.im.edit.DefaultIMObjectReferenceEditor;
import org.openvpms.web.component.im.edit.IMObjectReferenceEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.template.SMSTemplateSampler;
import org.openvpms.web.workspace.customer.account.AccountReminderEvaluator;

import java.math.BigDecimal;
import java.util.Date;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;

/**
 * A component to test the expression evaluation of an <em>entity.documentTemplateSMSAccount</em>.
 *
 * @author Tim Anderson
 */
public class SMSAccountReminderTemplateSampler extends SMSTemplateSampler {

    /**
     * The template evaluator.
     */
    private final AccountReminderEvaluator evaluator;

    /**
     * The customer to test against.
     */
    private final SimpleProperty customer;

    /**
     * The location to test against.
     */
    private final SimpleProperty location;

    /**
     * The balance to test against.
     */
    private final SimpleProperty balance;

    private static final String LOCATION = "location";

    private static final String CUSTOMER = "customer";

    /**
     * Constructs an {@link SMSAccountReminderTemplateSampler}.
     *
     * @param template      the template
     * @param layoutContext the layout context
     */
    public SMSAccountReminderTemplateSampler(Entity template, LayoutContext layoutContext) {
        super(template, layoutContext);
        IArchetypeRuleService service = ServiceHelper.getArchetypeService();
        Context context = layoutContext.getContext();
        evaluator = new AccountReminderEvaluator(context.getPractice(),
                                                 ServiceHelper.getBean(SMSTemplateEvaluator.class), service,
                                                 ServiceHelper.getLookupService());

        ModifiableListener listener = modifiable -> evaluate();
        customer = new SimpleProperty(CUSTOMER, null, IMObjectReference.class,
                                      DescriptorHelper.getDisplayName(INVOICE, CUSTOMER, service));
        customer.setArchetypeRange(new String[]{CustomerArchetypes.PERSON});
        if (context.getCustomer() != null) {
            customer.setValue(context.getCustomer().getObjectReference());
        }
        location = new SimpleProperty(LOCATION, null, IMObjectReference.class,
                                      DescriptorHelper.getDisplayName(INVOICE, LOCATION, service));
        location.setArchetypeRange(new String[]{PracticeArchetypes.LOCATION});
        if (context.getLocation() != null) {
            location.setValue(context.getLocation().getObjectReference());
        }
        balance = new SimpleProperty("balance", new BigDecimal("100.25"), BigDecimal.class,
                                     Messages.get("admin.template.smsaccountreminder.balance"));

        customer.addModifiableListener(listener);
        location.addModifiableListener(listener);
        balance.addModifiableListener(listener);
    }

    /**
     * Lays out the editable fields in a grid.
     *
     * @param grid    the grid
     * @param group   the focus group
     * @param context the layout context
     */
    @Override
    protected void layoutFields(ComponentGrid grid, FocusGroup group, LayoutContext context) {
        CustomerReferenceEditor customerSelector = new CustomerReferenceEditor(customer, null, context);
        IMObjectReferenceEditor<Party> locationSelector = new DefaultIMObjectReferenceEditor<>(location, null, context);
        grid.add(LabelFactory.text(customer.getDisplayName()), customerSelector.getComponent(),
                 LabelFactory.text(location.getDisplayName()), locationSelector.getComponent());
        Component amount = context.getComponentFactory().create(balance);
        grid.add(LabelFactory.text(balance.getDisplayName()), amount);
        group.add(customerSelector.getFocusGroup());
        group.add(locationSelector.getFocusGroup());
        group.add(amount);
    }

    /**
     * Evaluates the template.
     *
     * @param template the template
     * @param context  the context
     * @return the result of the evaluation. May be {@code null}
     */
    @Override
    protected String evaluate(Entity template, Context context) {
        FinancialAct act = (FinancialAct) IMObjectCreator.create(INVOICE);
        IMObjectBean bean = ServiceHelper.getArchetypeService().getBean(act);
        bean.setTarget(CUSTOMER, customer.getReference());
        bean.setTarget(LOCATION, location.getReference());
        act.setStatus(FinancialActStatus.POSTED);
        act.setActivityStartTime(new Date());
        act.setActivityEndTime(new Date());
        act.setTotal(balance.getBigDecimal(BigDecimal.ZERO));
        act.setAllocatedAmount(BigDecimal.ZERO);
        return evaluator.evaluate(template, act, IMObjectHelper.getObject(customer.getReference(), Party.class),
                                  IMObjectHelper.getObject(location.getReference(), Party.class));
    }

}