/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.esci.adapter.client.SupplierServiceLocator;
import org.openvpms.esci.adapter.dispatcher.ESCIConfig;
import org.openvpms.esci.adapter.dispatcher.ESCIDispatcher;
import org.openvpms.esci.adapter.dispatcher.Inbox;
import org.openvpms.esci.service.InboxService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.im.view.Selection;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.supplier.order.ESCIErrorHandler;

import java.util.Arrays;
import java.util.Objects;

/**
 * ESCI administration CRUD window.
 *
 * @author Tim Anderson
 */
class ESCICRUDWindow extends ResultSetCRUDWindow<Party> {

    /**
     * The browser.
     */
    private final ESCIBrowser browser;

    /**
     * Check inbox button identifier.
     */
    private static final String CHECK_INBOX_ID = "button.checkInbox";

    /**
     * Check inbox button identifier.
     */
    private static final String CHECK_ALL_INBOXES_ID = "button.checkAllInboxes";

    /**
     * Manage inbox button identifier.
     */
    private static final String MANAGE_INBOX_ID = "button.manageinbox";

    /**
     * Constructs an {@link ESCICRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param browser    the browser
     * @param context    the context
     * @param help       the help context
     */
    public ESCICRUDWindow(Archetypes<Party> archetypes, ESCIBrowser browser, Context context, HelpContext help) {
        super(archetypes, browser.getQuery(), browser.getResultSet(), context, help);
        this.browser = browser;
    }

    /**
     * Edits the current object.
     */
    @Override
    public void edit() {
        Party object = getObject();
        if (object != null && Objects.equals(object, browser.getSelected())) {
            EntityRelationship config = browser.getConfig();
            if (config != null) {
                // pre-select the ESCI configuration in the editor
                setSelectionPath(Arrays.asList(new Selection("stockLocations", null),
                                               new Selection(null, config)));
            }
        }
        super.edit();
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        buttons.add(createViewButton());
        buttons.add(createEditButton());
        buttons.add(CHECK_INBOX_ID, this::checkInbox);
        buttons.add(CHECK_ALL_INBOXES_ID, this::checkAllInboxes);
        buttons.add(MANAGE_INBOX_ID, this::onManageInbox);
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(CHECK_INBOX_ID, enable);
        buttons.setEnabled(MANAGE_INBOX_ID, enable);
    }

    /**
     * Checks the selected inbox.
     */
    private void checkInbox() {
        Inbox inbox = getInbox();
        if (inbox != null) {
            ESCIErrorHandler handler = new ESCIErrorHandler();
            ESCIDispatcher dispatcher = ServiceHelper.getBean(ESCIDispatcher.class);
            int count = dispatcher.dispatch(inbox, handler);
            if (handler.getErrors() != 0) {
                showErrors(Messages.format("admin.esci.checkinbox.error", inbox.getSupplier().getName(),
                                           inbox.getStockLocation().getName()), handler);
            } else {
                InformationDialog.show(Messages.get("admin.esci.checkinbox.title"),
                                       Messages.format("admin.esci.checkinbox.message", count));
            }
        }
    }

    /**
     * Invoked to manage the inbox for the selected supplier and stock location.
     */
    private void onManageInbox() {
        Inbox inbox = getInbox();
        if (inbox != null) {
            ESCIDocumentDialog dialog = new ESCIDocumentDialog(inbox, getContext(), getHelpContext().subtopic("inbox"));
            dialog.show();
        }
    }

    /**
     * Checks all inboxes.
     */
    private void checkAllInboxes() {
        ESCIErrorHandler handler = new ESCIErrorHandler();
        ESCIDispatcher dispatcher = ServiceHelper.getBean(ESCIDispatcher.class);
        int count = dispatcher.dispatch(handler);
        if (handler.getErrors() != 0) {
            showErrors(Messages.get("admin.esci.checkallinboxes.error"), handler);
        } else {
            InformationDialog.show(Messages.get("admin.esci.checkallinboxes.title"),
                                   Messages.format("admin.esci.checkinbox.message", count));
        }
    }

    /**
     * Returns the selected inbox.
     *
     * @return the selected inbox, or {@code null} if none is selected
     */
    private Inbox getInbox() {
        Inbox result = null;
        ESCIConfig config = getConfig();
        if (config != null) {
            SupplierServiceLocator locator = ServiceHelper.getBean(SupplierServiceLocator.class);
            InboxService inboxService = locator.getInboxService(config);
            result = new Inbox(config, inboxService);
        }
        return result;
    }

    /**
     * Returns the selected ESCI configuration.
     *
     * @return the selected ESCI configuration, or {@code null} if none is selected
     */
    private ESCIConfig getConfig() {
        ESCIConfig result = null;
        Party supplier = IMObjectHelper.reload(browser.getSelected());
        Party stockLocation = browser.getStockLocation();
        if (supplier != null && stockLocation != null) {
            result = ESCIConfig.create(supplier, stockLocation, ServiceHelper.getArchetypeService());
        }
        return result;
    }

    /**
     * Displays errors from processing inboxes.
     *
     * @param context the error context
     * @param handler the error handler
     */
    private void showErrors(String context, ESCIErrorHandler handler) {
        ErrorDialog.newDialog()
                .preamble(context)
                .message(handler.formatErrors())
                .show();
    }

}
