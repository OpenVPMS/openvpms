/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.SplitPane;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.user.User;
import org.openvpms.eftpos.service.ManagedTransactionDisplay;
import org.openvpms.eftpos.service.Prompt;
import org.openvpms.eftpos.transaction.Transaction;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.print.DefaultBatchPrinter;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.factory.SplitPaneFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.servlet.SessionMonitor;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.util.PeriodicBackgroundTask;
import org.openvpms.web.system.ServiceHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes.MERCHANT_RECEIPT;
import static org.openvpms.component.model.bean.Predicates.targetIsA;

/**
 * EFTPOS payment dialog.
 *
 * @author Tim Anderson
 */
class EFTPaymentDialog extends ModalDialog {

    /**
     * The EFTPOS transaction.
     */
    private final IMObjectBean transaction;

    /**
     * The transaction display.
     */
    private final ManagedTransactionDisplay display;

    /**
     * The current user.
     */
    private final User user;

    /**
     * The context.
     */
    private final Context context;

    /**
     * The terminal buttons.
     */
    private final Row terminalButtons;

    /**
     * Determines if printing is required.
     */
    private final boolean print;

    /**
     * Displays messages from the provider.
     */
    private final Label content;

    /**
     * Runs the calls to the EFTPOS provider in the background.
     */
    private final PeriodicBackgroundTask task;

    /**
     * The ids of the printed merchant receipts, to avoid automatic reprints.
     */
    private final Set<Long> printed = new HashSet<>();

    /**
     * Used to prevent {@link #updateTransaction()} being invoked by multiple threads.
     */
    private final Semaphore waiter = new Semaphore(1);

    /**
     * Counts outstanding print jobs.
     */
    private int printJobs = 0;

    /**
     * Print merchant receipt button identifier.
     */
    private static final String PRINT_MERCHANT_RECEIPT_ID = "button.printMerchantReceipt";

    /**
     * The buttons to display.
     */
    private static final String[] BUTTONS = {OK_ID};

    /**
     * The frequency with which {@link #updateTransaction()} is invoked, in seconds.
     */
    private static final int POLL_INTERVAL = 6;

    /**
     * Constructs a {@link EFTPaymentDialog}.
     *
     * @param transaction the EFTPOS transaction
     * @param display     the transaction display
     * @param user        the current user
     * @param context     the context
     * @param help        the help context
     */
    public EFTPaymentDialog(Act transaction, ManagedTransactionDisplay display, User user, Context context,
                            HelpContext help) {
        super(EFTHelper.getDialogTitle(transaction), BUTTONS, help);
        this.transaction = ServiceHelper.getArchetypeService().getBean(transaction);
        this.display = display;
        this.user = user;
        this.context = context;
        terminalButtons = RowFactory.create(Styles.WIDE_CELL_SPACING);
        content = LabelFactory.create(true, true);
        print = !display.getTransaction().getTerminal().isReceiptPrinter();
        SplitPane container = SplitPaneFactory.create(SplitPane.ORIENTATION_VERTICAL_BOTTOM_TOP,
                                                      "SplitPaneWithButtonRow",
                                                      RowFactory.create(Styles.LARGE_INSET_X, terminalButtons),
                                                      ColumnFactory.create(Styles.LARGE_INSET, content));
        task = new PeriodicBackgroundTask(POLL_INTERVAL, TimeUnit.SECONDS, ServiceHelper.getBean(SessionMonitor.class),
                                          display::isComplete, this::backgroundUpdateTransaction, () -> refresh(true));
        ButtonSet buttons = getButtons();
        buttons.setEnabled(OK_ID, false);
        buttons.add(PRINT_MERCHANT_RECEIPT_ID, () -> printMerchantReceipts(true));
        buttons.setEnabled(PRINT_MERCHANT_RECEIPT_ID, false);
        getLayout().add(container);
        resize("EFTPaymentDialog.size");
    }

    /**
     * Show the window.
     */
    @Override
    public void show() {
        super.show();
        task.start();
    }

    /**
     * Disposes of the dialog.
     */
    @Override
    public void dispose() {
        task.dispose();
        super.dispose();
    }

    /**
     * Lays out the component prior to display.
     */
    @Override
    protected void doLayout() {
        refresh(false);
    }

    /**
     * Cancels the operation.
     * <p>
     * This implementation closes the dialog, setting the action to {@link #CANCEL_ID}.
     */
    @Override
    protected void doCancel() {
        Transaction transaction = display.getTransaction();
        Transaction.Status status = transaction.getStatus();
        if (status == Transaction.Status.PENDING || status == Transaction.Status.IN_PROGRESS) {
            display.cancel();
        }
        super.doCancel();
    }

    /**
     * Displays the current transaction status.
     *
     * @param printOutstanding if {@code true}, print outstanding receipts
     */
    protected void refresh(boolean printOutstanding) {
        terminalButtons.removeAll();

        try {
            if (print && printOutstanding) {
                // automatically print any unprinted merchant receipts
                printMerchantReceipts(false);
            }

            boolean complete = display.isComplete();
            if (complete && canClose()) {
                close();
            } else {
                StringBuilder buffer = new StringBuilder();
                List<String> messages = display.getMessages();
                for (String message : messages) {
                    if (buffer.length() > 0) {
                        buffer.append('\n');
                    }
                    buffer.append(message);
                }
                Prompt prompt = display.getPrompt();
                if (prompt != null) {
                    if (buffer.length() > 0) {
                        buffer.append('\n');
                    }
                    buffer.append(prompt.getMessage());
                    if (!complete) {
                        for (Prompt.Option option : prompt.getOptions()) {
                            Button button = ButtonFactory.text(option.getText(),
                                                               () -> pressTerminalButton(prompt, option));
                            terminalButtons.add(button);
                        }
                    }
                }
                content.setText(buffer.toString());
                if (complete) {
                    getButtons().setEnabled(OK_ID, true);
                }
                if (hasMerchantReceipts()) {
                    getButtons().setEnabled(PRINT_MERCHANT_RECEIPT_ID, true);
                }
            }
        } catch (Throwable exception) {
            content.setText(ErrorFormatter.format(exception));
        }
    }

    /**
     * Determines if the dialog can be closed.
     *
     * @return {@code true} if the transaction is approved and there are no outstanding prints
     */
    private boolean canClose() {
        return printJobs == 0 && display.getTransaction().getStatus() == Transaction.Status.APPROVED;
    }

    /**
     * Presses a terminal button.
     *
     * @param prompt the terminal prompt
     * @param option the button
     */
    private void pressTerminalButton(Prompt prompt, Prompt.Option option) {
        try {
            prompt.send(option);
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        updateTransaction();
        refresh(true);
    }

    /**
     * Prints any merchant receipts.
     *
     * @param all if {@code true}, print all receipts, otherwise print those that haven't been printed.
     */
    private void printMerchantReceipts(boolean all) {
        List<DocumentAct> toPrint = new ArrayList<>();
        for (Relationship relationship : getMerchantReceiptRelationships()) {
            long id = relationship.getTarget().getId();
            if (all || !printed.contains(id)) {
                printed.add(id);
                DocumentAct act = (DocumentAct) IMObjectHelper.getObject(relationship.getTarget());
                if (act != null && (all || !act.isPrinted())) {
                    toPrint.add(act);
                }
            }
        }
        if (!toPrint.isEmpty()) {
            printJobs++;
            ReceiptPrinter printer = new ReceiptPrinter(toPrint) {
                /**
                 * Invoked when printing completes.
                 */
                @Override
                protected void completed() {
                    printJobs--;
                }
            };
            printer.print();
        }
    }

    /**
     * Determines if the transaction has merchant receipts.
     *
     * @return {@code true} if the transaction has merchant receipts
     */
    private boolean hasMerchantReceipts() {
        boolean result = !printed.isEmpty();
        if (!result) {
            List<Relationship> receipts = getMerchantReceiptRelationships();
            result = !receipts.isEmpty();
        }
        return result;
    }

    /**
     * Returns the merchant receipt relationships.
     *
     * @return the merchant receipt relationships
     */
    private List<Relationship> getMerchantReceiptRelationships() {
        return transaction.getValues("receipts", Relationship.class, targetIsA(MERCHANT_RECEIPT));
    }

    /**
     * Invokes {@link ManagedTransactionDisplay#update()} with the current user.
     * <p/>
     * This is designed to be run in a background thread.
     *
     * @return {@code true} if the display needs to refresh, otherwise {@code false}
     */
    private boolean backgroundUpdateTransaction() {
        return RunAs.call(user, this::updateTransaction);
    }

    /**
     * Invokes {@link ManagedTransactionDisplay#update()} with the current user.
     * <p/>
     * This ensures only a single thread invokes the update at a time.
     *
     * @return {@code true} if the display needs to refresh, otherwise {@code false}
     */
    private boolean updateTransaction() {
        boolean result = false;
        if (waiter.tryAcquire()) {
            try {
                result = display.update();
            } finally {
                waiter.release();
            }
        }
        return result;
    }

    private class ReceiptPrinter extends DefaultBatchPrinter<DocumentAct> {

        public ReceiptPrinter(List<DocumentAct> toPrint) {
            super(toPrint, EFTPaymentDialog.this.context, EFTPaymentDialog.this.getHelpContext());
        }

        /**
         * Creates a new document template locator to locate the template for the object being printed.
         *
         * @param object  the object to print
         * @param context the context
         * @return a new document template locator
         */
        @Override
        protected DocumentTemplateLocator createDocumentTemplateLocator(DocumentAct object, Context context) {
            // merchant and customer receipts share the same template
            return new ContextDocumentTemplateLocator(EFTPOSArchetypes.CUSTOMER_RECEIPT, context);
        }
    }

}
