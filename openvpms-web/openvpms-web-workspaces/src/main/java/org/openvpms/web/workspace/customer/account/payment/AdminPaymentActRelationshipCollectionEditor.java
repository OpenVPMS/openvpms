/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account.payment;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.event.ActionEvent;
import nextapp.echo2.app.table.TableColumn;
import org.apache.commons.lang3.ArrayUtils;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.edit.payment.PaymentItemEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DescriptorTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.property.CollectionProperty;
import org.openvpms.web.echo.dialog.OptionDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;

/**
 * An {@link ActRelationshipCollectionEditor} that provides restricted editing of payment items, and tracks payment
 * changes for auditing purposes.
 *
 * @author Tim Anderson
 */
class AdminPaymentActRelationshipCollectionEditor extends ActRelationshipCollectionEditor {

    /**
     * Tracks payment changes for auditing purposes.
     */
    private final PaymentChanges audit;

    /**
     * Constructs an {@link AdminPaymentActRelationshipCollectionEditor}.
     *
     * @param property the collection property
     * @param act      the parent act
     * @param context  the layout context
     */
    public AdminPaymentActRelationshipCollectionEditor(CollectionProperty property, Act act, LayoutContext context) {
        super(property, act, context);
        audit = new PaymentChanges(ServiceHelper.getArchetypeService());
    }

    /**
     * Determines if items can be added or removed.
     *
     * @return {@code false} as items cannot be added or removed
     */
    @Override
    public boolean isCardinalityReadOnly() {
        return true;
    }

    /**
     * Generates an audit messages of changes.
     *
     * @return an audit message, or {@code null} if there have been no auditable changes
     */
    public String getAuditMessage() {
        User user = getContext().getContext().getUser();
        String name = (user != null) ? user.getUsername() : "unknown";
        return audit.getAuditMessage(name, new Date());
    }

    /**
     * Creates a new editor.
     *
     * @param object  the object to edit
     * @param context the layout context
     * @return an editor to edit {@code object}
     */
    @Override
    public IMObjectEditor createEditor(IMObject object, LayoutContext context) {
        IMObjectEditor editor = super.createEditor(object, context);
        if (editor instanceof PaymentItemEditor) {
            ((PaymentItemEditor) editor).setReadOnlyAmount(true);
        }
        return editor;
    }

    /**
     * Create a new table model.
     *
     * @param context the layout context
     * @return a new table model
     */
    @Override
    @SuppressWarnings("unchecked")
    protected IMTableModel<IMObject> createTableModel(LayoutContext context) {
        context = new DefaultLayoutContext(context);
        context.setComponentFactory(new TableComponentFactory(context));
        String[] archetypes = getCollectionPropertyEditor().getArchetypeRange();
        return (IMTableModel) new ItemTableModel(archetypes, context);
    }

    /**
     * Replaces a payment item with that of the specified archetype.
     *
     * @param act       the payment item
     * @param archetype the new payment item archetype
     */
    protected void change(FinancialAct act, String archetype) {
        if (!canChange(act)) {
            throw new IllegalStateException("Cannot change " + act.getId());
        }
        FinancialAct newAct = (FinancialAct) create(archetype);
        if (newAct != null) {
            newAct.setTotal(act.getTotal());
            remove(act);
            add(newAct);
            audit.addChange(act, newAct);
            refresh();
            setSelected(newAct);
            onSelected(); // edit it
        }
    }

    /**
     * Invoked to change a payment item.
     *
     * @param act the payment item
     */
    private void onChange(FinancialAct act) {
        String[] archetypeRange = getCollectionPropertyEditor().getArchetypeRange();
        String[] archetypes = ArrayUtils.removeElement(archetypeRange, act.getArchetype());
        String[] displayNames = new String[archetypes.length];
        for (int i = 0; i < archetypes.length; ++i) {
            displayNames[i] = DescriptorHelper.getDisplayName(archetypes[i], ServiceHelper.getArchetypeService());
        }
        OptionDialog dialog = new OptionDialog(Messages.get("customer.account.payment.change.title"),
                                               Messages.get("customer.account.payment.change.message"),
                                               displayNames, OptionDialog.CANCEL);
        dialog.setCloseOnSelection(true);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                int selected = dialog.getSelected();
                if (selected >= 0 && selected < archetypes.length) {
                    change(act, archetypes[selected]);
                }
            }
        });
        dialog.show();
    }

    /**
     * Determines if a payment item can be changed.
     * <p/>
     * EFT items with associated transactions cannot be changed.
     *
     * @param act the payment item
     * @return {@code true} if the payment item can be changed, otherwise {@code false}
     */
    private boolean canChange(FinancialAct act) {
        boolean result = true;
        if (act.isA(CustomerAccountArchetypes.PAYMENT_EFT)) {
            IMObjectBean bean = getBean(act);
            if (!bean.getValues("eft").isEmpty()) {
                result = false;
            }
        }
        return result;
    }

    private class ItemTableModel extends DescriptorTableModel<FinancialAct> {

        /**
         * Constructs an {@link ItemTableModel}.
         *
         * @param shortNames the archetype short names. May contain wildcards
         * @param context    the layout context
         */
        public ItemTableModel(String[] shortNames, LayoutContext context) {
            super(shortNames, context);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the table column
         * @param row    the table row
         */
        @Override
        protected Object getValue(FinancialAct object, TableColumn column, int row) {
            Object result;
            if (column.getModelIndex() == ARCHETYPE_INDEX) {
                Label label = LabelFactory.text(DescriptorHelper.getDisplayName(object, getService()));
                Button change = ButtonFactory.create("button.change");
                if (canChange(object)) {
                    change.addActionListener(new ActionListener() {
                        @Override
                        public void onAction(ActionEvent event) {
                            onChange(object);
                        }
                    });
                } else {
                    change.setEnabled(false);
                }
                Row wrapper = RowFactory.create(change);
                wrapper.setLayoutData(RowFactory.rightAlign());
                result = RowFactory.create(Styles.CELL_SPACING, label, wrapper);
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }
    }
}
