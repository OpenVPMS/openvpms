/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.schedule;

import org.openvpms.archetype.i18n.time.DateDurationFormatter;
import org.openvpms.archetype.i18n.time.DurationFormatter;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;

import java.util.Date;

/**
 * An editor for <em>party.organisationSchedule</em>.
 * <p>
 * This defaults the location to the current practice location for new schedules.
 *
 * @author Tim Anderson
 */
public class ScheduleEditor extends AbstractIMObjectEditor {

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The saved maximum duration.
     */
    private int savedMaxDuration = -1;

    /**
     * The saved maximum duration units.
     */
    private DateUnits savedMaxDurationUnits;

    /**
     * Determines if a confirmation prompt should be been displayed for maxDuration reductions. This is only
     * displayed once per edit.
     */
    private boolean confirmMaxDurationReduction = true;

    /**
     * Determines if the maxDuration reduction confirmation is currently being displayed.
     */
    private boolean showingMaxDurationConfirmationReduction = false;

    /**
     * The duration formatter.
     */
    private static final DurationFormatter formatter
            = DateDurationFormatter.create(false, false, true, true, true, true);

    /**
     * The <em>maxDuration</em> node.
     */
    private static final String MAX_DURATION = "maxDuration";

    /**
     * The <em>maxDurationUnits</em> node.
     */
    private static final String MAX_DURATION_UNITS = "maxDurationUnits";

    /**
     * Constructs a {@link ScheduleEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public ScheduleEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        rules = ServiceHelper.getBean(AppointmentRules.class);
        if (object.isNew()) {
            Party location = layoutContext.getContext().getLocation();
            if (location != null) {
                IMObjectBean bean = getBean(object);
                bean.setTarget("location", location);
            }
        } else {
            updateSavedDuration();
        }
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new ScheduleEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Sets the maximum duration for events.
     *
     * @param duration the duration, or {@code <= 0} to not limit event durations
     * @param units    the duration units. May be {@code null}
     */
    public void setMaxDuration(int duration, DateUnits units) {
        getProperty(MAX_DURATION).setValue(duration > 0 ? duration : null);
        getProperty(MAX_DURATION_UNITS).setValue(units != null ? units.toString() : null);
    }

    /**
     * Returns the maximum duration.
     *
     * @return the maximum duration, or {@code -1} if it's not set
     */
    public int getMaxDuration() {
        return getProperty(MAX_DURATION).getInt(-1);
    }

    /**
     * Returns the maximum duration units.
     *
     * @return the maximum duration, or {@code null} if it's not set
     */
    public DateUnits getMaxDurationUnits() {
        return DateUnits.fromString(getProperty(MAX_DURATION_UNITS).getString());
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This can be used to perform processing that requires all editors to be created.
     */
    @Override
    protected void onLayoutCompleted() {
        ModifiableListener listener = modifiable -> onMaxDurationChanged();
        getProperty(MAX_DURATION).addModifiableListener(listener);
        getProperty(MAX_DURATION_UNITS).addModifiableListener(listener);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateTimes(validator) && validateMaxDuration(validator);
    }

    /**
     * Save any edits.
     */
    @Override
    protected void doSave() {
        super.doSave();
        updateSavedDuration();
    }

    /**
     * Determines if the <em>maxDuration</em> length has been reduced from its saved value.
     *
     * @return {@code true} if it has been reduced, otherwise {@code false}
     */
    private boolean maxDurationReduced() {
        return checkDuration(getMaxDuration(), getMaxDurationUnits());
    }

    /**
     * Invoked when the maximum duration changes.
     * <p/>
     * If the duration is being reduced, this display a prompt warning of maxDuration reduction, and provides an option
     * to revert to saved.
     */
    private void onMaxDurationChanged() {
        if (!showingMaxDurationConfirmationReduction && maxDurationReduced() && confirmMaxDurationReduction) {
            // only display if there isn't a confirmation already being displayed
            showingMaxDurationConfirmationReduction = true;
            ConfirmationDialog.newDialog()
                    .title(Messages.get("workflow.scheduling.durationreduction.title"))
                    .message(Messages.get("workflow.scheduling.durationreduction.message"))
                    .yesNo()
                    .yes(() -> {
                        confirmMaxDurationReduction = false;
                        showingMaxDurationConfirmationReduction = false;
                    })
                    .no(() -> {
                        setMaxDuration(savedMaxDuration, savedMaxDurationUnits);
                        confirmMaxDurationReduction = false;
                        showingMaxDurationConfirmationReduction = false;
                    })
                    .show();

        }
    }

    /**
     * Updates the saved duration.
     */
    private void updateSavedDuration() {
        savedMaxDuration = getMaxDuration();
        savedMaxDurationUnits = getMaxDurationUnits();
    }

    /**
     * Validates the maximum duration.
     * <p/>
     * If the maximum duration changes, this ensures that the new duration encompasses all current and future events.
     * <p/>
     * This is necessary, as the maximum duration is used to determine the lower and upper bound of queries; failure
     * to set it correctly will result in events being excluded from retrieval.
     *
     * @param validator the validator
     * @return {@code true} if the maximum duration is valid, otherwise {@code false}
     */
    private boolean validateMaxDuration(Validator validator) {
        boolean result = false;
        if (getObject().isNew()) {
            // new schedule, so no events to check
            result = true;
        } else if (!showingMaxDurationConfirmationReduction) {
            // if validating while the confirmation dialog is displayed, treat as invalid
            int duration = getMaxDuration();
            DateUnits units = getMaxDurationUnits();
            if (checkDuration(duration, units)) {
                Act event = rules.getAppointmentEventLongerThanDuration((Entity) getObject(), DateRules.getToday(),
                                                                        duration, units);
                if (event != null) {
                    String date = DateFormatter.formatDate(event.getActivityStartTime(), false);
                    String time = DateFormatter.formatTime(event.getActivityStartTime(), false);
                    String eventDuration = formatter.format(event.getActivityStartTime(), event.getActivityEndTime());
                    validator.add(this, Messages.format("workflow.scheduling.eventexceedsduration", eventDuration,
                                                        getDisplayName(event), date, time));
                } else {
                    result = true;
                }
            } else {
                result = true;
            }
        }
        return result;
    }

    /**
     * Determines if events need to be checked to ensure they fall within the maximum duration.
     *
     * @param duration the duration
     * @param units    the duration units. May be {@code null}
     * @return {@code true} if events need to be checked, otherwise {@code false}
     */
    private boolean checkDuration(int duration, DateUnits units) {
        boolean result = true;
        if (duration > 0 && units != null && (duration != savedMaxDuration || units != savedMaxDurationUnits)) {
            if (savedMaxDuration > 0 && savedMaxDurationUnits != null) {
                Date today = new Date();
                Date saved = DateRules.getDate(today, savedMaxDuration, savedMaxDurationUnits);
                Date current = DateRules.getDate(today, duration, units);
                if (current.compareTo(saved) >= 0) {
                    // no decrease in duration, so don't need to verify
                    result = false;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Verifies that the Start Time and End Time are set correctly.
     *
     * @param validator the validator
     * @return {@code true} if the times are valid
     */
    private boolean validateTimes(Validator validator) {
        boolean valid = false;
        Date startTime = getProperty("startTime").getDate();
        Date endTime = getProperty("endTime").getDate();
        if ((startTime == null && endTime != null) || (startTime != null && endTime == null)) {
            validator.add(this, new ValidatorError(Messages.get("schedule.time.bothRequired")));
        } else if (startTime != null && DateRules.compareTo(startTime, endTime) >= 0) {
            validator.add(this, new ValidatorError(Messages.get("schedule.time.less")));
        } else {
            valid = true;
        }
        return valid;
    }
}