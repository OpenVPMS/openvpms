/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.action.ActionStatus;
import org.openvpms.web.component.im.edit.IMObjectEditorFactory;
import org.openvpms.web.component.im.edit.IMObjectEditorSaver;
import org.openvpms.web.component.im.edit.act.ActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.workspace.ActPoster;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.account.AccountActActions;

import java.util.Date;
import java.util.function.Consumer;

/**
 * An {@link ActPoster} for customer charges.
 * <p/>
 * This:
 * <ul>
 *     <li>checks for undispensed orders, if the charge is an invoice</li>
 *     <li>sets the date of the charge to now (see OVPMS-734)</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public class ChargePoster extends ActPoster<FinancialAct> {

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * Constructs an {@link ActPoster}.
     *
     * @param act     the act to post
     * @param actions the actions that determine if the act can be posted or not
     * @param context the layout context
     */
    public ChargePoster(FinancialAct act, AccountActActions actions, LayoutContext context) {
        super(act, actions, context.getHelpContext());
        this.context = context;
    }

    /**
     * Confirms that the user wants to post the act, posting it if accepted.
     *
     * @param act      the act to post
     * @param help     the help context
     * @param listener the listener to notify on completion
     */
    @Override
    protected void confirmPost(FinancialAct act, HelpContext help, Consumer<ActionStatus> listener) {
        if (act.isA(CustomerAccountArchetypes.INVOICE)) {
            UndispensedOrderChecker checker = new UndispensedOrderChecker(act);
            if (checker.hasUndispensedItems()) {
                checker.confirm(help, () -> postConfirmed(act, listener),
                                () -> listener.accept(ActionStatus.success()));
            } else {
                super.confirmPost(act, help, listener);
            }
        } else {
            super.confirmPost(act, help, listener);
        }
    }

    /**
     * Posts the act.
     * <p/>
     * This uses an editor to ensure that any HL7 Pharmacy Orders associated with invoices are discontinued.
     *
     * @param act      the act to post
     * @param listener the listener to notify on completion
     */
    @Override
    protected void post(FinancialAct act, Consumer<ActionStatus> listener) {
        ActEditor editor = (ActEditor) ServiceHelper.getBean(IMObjectEditorFactory.class).create(act, context);
        editor.setStatus(ActStatus.POSTED);
        editor.setStartTime(new Date()); // for OVPMS-734
        IMObjectEditorSaver saver = new IMObjectEditorSaver();
        if (saver.save(editor, () -> listener.accept(ActionStatus.success()))) {
            listener.accept(ActionStatus.success());
        }
    }
}