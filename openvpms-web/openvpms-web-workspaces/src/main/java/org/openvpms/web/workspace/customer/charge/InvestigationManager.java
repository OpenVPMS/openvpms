/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.laboratory.internal.service.OrderValidationService.ValidationStatus;
import org.openvpms.web.component.edit.AlertListener;
import org.openvpms.web.component.edit.Saveable;
import org.openvpms.web.component.im.edit.CollectionPropertyEditor;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.workspace.patient.investigation.PatientInvestigationActEditor;

import java.util.List;

/**
 * Manages investigations.
 * <p/>
 * Investigations are created through charging products with tests, and and may be associated with multiple invoice
 * items if the tests:
 * <ul>
 *     <li>have the same investigation type</li>
 *     <li>can be grouped</li>
 * </ul>
 *
 * @author Tim Anderson
 */
public interface InvestigationManager extends Modifiable, Saveable {

    /**
     * Adds an invoice item.
     *
     * @param item the invoice item
     */
    void addInvoiceItem(FinancialAct item);

    /**
     * Removes an invoice item. Relationships to existing investigations will be removed.
     * <p/>
     * If there are no invoice item relationships, and the investigation can be removed, it will be removed.
     *
     * @param item the invoice item
     */
    void removeInvoiceItem(FinancialAct item);

    /**
     * Removes an invoice item. Relationships to existing investigations will be removed.
     * <p/>
     * If there are no invoice item relationships, and the investigation can be removed, it will be removed.
     *
     * @param editor the invoice item editor
     */
    void removeInvoiceItem(CustomerChargeActItemEditor editor);

    /**
     * Invoked when an invoice item patient changes.
     *
     * @param editor  the invoice item editor
     * @param patient the patient. May be {@code null}
     * @return any new investigations created as a result of the patient change
     */
    List<PatientInvestigationActEditor> updatePatient(CustomerChargeActItemEditor editor, Party patient);

    /**
     * Invoked when an invoice item product changes.
     *
     * @param editor  the invoice item editor
     * @param product the product. May be {@code null}
     * @return any new investigations created as a result of the product change
     */
    List<PatientInvestigationActEditor> updateProduct(CustomerChargeActItemEditor editor, Product product);

    /**
     * Invoked when an invoice item clinician changes.
     *
     * @param item      the invoice item
     * @param clinician the clinician. May be {@code null}
     */
    void updateClinician(FinancialAct item, User clinician);

    /**
     * Invoked when an invoice item start time changes.
     *
     * @param item the invoice item
     */
    void updateTime(FinancialAct item);

    /**
     * Returns all investigations.
     *
     * @return the investigations
     */
    List<DocumentAct> getInvestigations();

    /**
     * Returns investigations.
     *
     * @param ignorePending if {@code true}, ignore investigations with a PENDING order status
     * @return the investigations
     */
    List<DocumentAct> getInvestigations(boolean ignorePending);

    /**
     * Returns all investigations for an invoice item.
     *
     * @param item the invoice item
     * @return the corresponding investigations
     */
    List<DocumentAct> getInvestigations(FinancialAct item);

    /**
     * Returns all investigations that are associated with a laboratory, that either haven't been submitted,
     * or need to be confirmed.
     *
     * @return the investigations
     */
    List<DocumentAct> getUnconfirmedLaboratoryInvestigations();

    /**
     * Returns all investigation editors.
     *
     * @return the investigation editors
     */
    List<PatientInvestigationActEditor> getEditors();

    /**
     * Returns the editor for an investigation, given its reference.
     *
     * @param investigationRef the investigation reference
     * @return the corresponding editor, or {@code null} if none exists
     */
    PatientInvestigationActEditor getEditor(Reference investigationRef);

    /**
     * Removes an investigation.
     *
     * @param investigation the investigation to remove
     * @return {@code true} if the investigation was removed
     */
    boolean removeInvestigation(DocumentAct investigation);

    /**
     * Reloads an investigation.
     *
     * @param investigation the investigation to reload
     * @return the reloaded investigation or {@code null} if it doesn't exist
     */
    DocumentAct reload(DocumentAct investigation);

    /**
     * Adds a listener to be notified when an investigation is reloaded.
     * <p/>
     * This operates outside of the {@link ModifiableListener} API, as this affects validation.
     *
     * @param callback the callback to invoke when an investigation is reloaded
     */
    void setReloadListener(Runnable callback);

    /**
     * Returns the underlying collection property editor.
     *
     * @return the collection property editor
     */
    CollectionPropertyEditor getEditor();

    /**
     * Registers an alert listener to pass to investigation editors.
     *
     * @param listener the listener. May be {@code null}
     */
    void setAlertListener(AlertListener listener);

    /**
     * Validates the order for an investigation, prior to its submission.
     *
     * @param investigation the investigation
     * @return the order validation status
     */
    ValidationStatus validate(DocumentAct investigation);
}
