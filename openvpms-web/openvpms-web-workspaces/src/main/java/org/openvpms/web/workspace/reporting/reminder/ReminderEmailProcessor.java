/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.reminder;

import org.openvpms.archetype.rules.doc.DocumentTemplate;
import org.openvpms.archetype.rules.doc.EmailTemplate;
import org.openvpms.archetype.rules.party.ContactArchetypes;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderConfiguration;
import org.openvpms.archetype.rules.patient.reminder.ReminderEvent;
import org.openvpms.archetype.rules.patient.reminder.ReminderRules;
import org.openvpms.archetype.rules.patient.reminder.ReminderType.GroupBy;
import org.openvpms.archetype.rules.patient.reminder.ReminderTypes;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.action.ActionFactory;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.report.ReporterFactory;
import org.openvpms.web.component.mail.EmailTemplateEvaluator;
import org.openvpms.web.component.mail.Mailer;
import org.openvpms.web.component.mail.MailerFactory;
import org.openvpms.web.security.mail.MailPasswordResolver;
import org.openvpms.web.workspace.customer.communication.CommunicationHelper;
import org.openvpms.web.workspace.customer.communication.CommunicationLogger;
import org.openvpms.web.workspace.reporting.ReportingException;
import org.openvpms.web.workspace.reporting.email.LocationMailerFactory;

import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.FailedToProcessReminder;
import static org.openvpms.web.workspace.reporting.ReportingException.ErrorCode.TemplateMissingEmailText;


/**
 * Sends reminder emails.
 *
 * @author Tim Anderson
 */
public class ReminderEmailProcessor extends GroupedReminderProcessor<EmailReminders> {

    /**
     * The mail services.
     */
    private final LocationMailerFactory mailServices;

    /**
     * The email template evaluator.
     */
    private final EmailTemplateEvaluator evaluator;

    /**
     * The reporter factory.
     */
    private final ReporterFactory reporterFactory;

    /**
     * Constructs a {@link ReminderEmailProcessor}.
     *
     * @param factory          the mailer factory
     * @param evaluator        the email template evaluator
     * @param reporterFactory  the reporter factory
     * @param reminderTypes    the reminder types
     * @param practice         the practice
     * @param reminderRules    the reminder rules
     * @param patientRules     the patient rules
     * @param practiceRules    the practice rules
     * @param service          the archetype service
     * @param config           the reminder configuration
     * @param logger           the communication logger. May be {@code null}
     * @param passwordResolver the mail server password resolver
     * @param actionFactory    the action factory
     */
    public ReminderEmailProcessor(MailerFactory factory, EmailTemplateEvaluator evaluator,
                                  ReporterFactory reporterFactory, ReminderTypes reminderTypes,
                                  Party practice, ReminderRules reminderRules, PatientRules patientRules,
                                  PracticeRules practiceRules, IArchetypeService service, ReminderConfiguration config,
                                  CommunicationLogger logger, MailPasswordResolver passwordResolver,
                                  ActionFactory actionFactory) {
        super(reminderTypes, reminderRules, patientRules, practice, service, config, logger, actionFactory);
        this.evaluator = evaluator;
        this.reporterFactory = reporterFactory;
        mailServices = new LocationMailerFactory(practice, practiceRules, service, factory,
                                                 ContactArchetypes.REMINDER_PURPOSE, passwordResolver);
    }

    /**
     * Returns the reminder item archetype that this processes.
     *
     * @return the archetype
     */
    @Override
    public String getArchetype() {
        return ReminderArchetypes.EMAIL_REMINDER;
    }

    /**
     * Determines if reminder processing is performed asynchronously.
     *
     * @return {@code true} if reminder processing is performed asynchronously
     */
    @Override
    public boolean isAsynchronous() {
        return false;
    }

    /**
     * Processes reminders.
     *
     * @param reminders the reminder state
     */
    @Override
    public void process(EmailReminders reminders) {
        Mailer mailer;

        try {
            mailer = send(reminders);
        } catch (OpenVPMSException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new ReportingException(FailedToProcessReminder, exception, exception.getMessage());
        }
        if (getLogger() != null) {
            String from = mailer.getFrom();
            String[] to = mailer.getTo();
            String[] cc = mailer.getCc();
            String[] bcc = mailer.getBcc();
            String subject = mailer.getSubject();
            String body = mailer.getBody();
            String attachments = CommunicationHelper.getAttachments(mailer.getAttachments());
            for (ReminderEvent reminder : reminders.getReminders()) {
                reminder.set("from", from);
                reminder.set("to", to);
                reminder.set("cc", cc);
                reminder.set("bcc", bcc);
                reminder.set("subject", subject);
                reminder.set("body", body);
                reminder.set("attachments", attachments);
            }
        }
    }

    /**
     * Prepares reminders for processing.
     * <p>
     * This:
     * <ul>
     * <li>filters out any reminders that can't be processed due to missing data</li>
     * <li>adds meta-data for subsequent calls to {@link #process}</li>
     * </ul>
     *
     * @param reminders the reminders to prepare
     * @throws ReportingException if the reminders cannot be prepared
     */
    @Override
    protected void prepare(EmailReminders reminders) {
        super.prepare(reminders);
        DocumentTemplate template = reminders.getTemplate();
        if (template != null) {
            EmailTemplate emailTemplate = template.getEmailTemplate();
            if (emailTemplate == null) {
                throw new ReportingException(TemplateMissingEmailText, template.getName());
            }
            reminders.setEmailTemplate(emailTemplate);
        }
    }

    /**
     * Creates a {@link PatientReminders}.
     *
     * @param groupBy the reminder grouping policy. This determines which document template is selected
     * @param resend  if {@code true}, reminders are being resent
     * @return a new {@link PatientReminders}
     */
    @Override
    protected EmailReminders createPatientReminders(GroupBy groupBy, boolean resend) {
        return new EmailReminders(groupBy, resend, evaluator, reporterFactory);
    }

    /**
     * Returns the contact archetype.
     *
     * @return the contact archetype
     */
    @Override
    protected String getContactArchetype() {
        return ContactArchetypes.EMAIL;
    }

    /**
     * Logs reminder communications.
     *
     * @param reminders the reminder state
     * @param logger    the communication logger
     */
    @Override
    protected void log(PatientReminders reminders, CommunicationLogger logger) {
        Party location = ((EmailReminders) reminders).getLocation();
        for (ReminderEvent event : reminders.getReminders()) {
            String notes = getNote(event);
            String from = event.getString("from");
            String[] to = (String[]) event.get("to");
            String[] cc = (String[]) event.get("cc");
            String[] bcc = (String[]) event.get("bcc");
            String subject = event.getString("subject");
            String body = event.getString("body");
            String attachments = event.getString("attachments");

            logger.logEmail(event.getCustomer(), event.getPatient(), from, to, cc, bcc, subject, COMMUNICATION_REASON,
                            body, notes, attachments, location);
        }
    }

    /**
     * Emails reminders.
     *
     * @param reminders the reminders to email
     */
    protected Mailer send(EmailReminders reminders) {
        ReminderEvent event = reminders.getReminders().get(0);
        Party location = reminders.getLocation();
        if (location == null) {
            throw new IllegalStateException("Practice location cannot be null, reminder="
                                            + event.getReminder().getId());
        }
        Context context = createContext(event, location);
        Mailer mailer = mailServices.create(location, context);

        String body = reminders.getMessage(context);
        String to = reminders.getEmailAddress();
        mailer.setTo(new String[]{to});

        String subject = reminders.getSubject(context);
        mailer.setSubject(subject);
        mailer.setBody(body);

        ReminderConfiguration config = getConfig();
        if (config.getEmailAttachments()) {
            // add the reminders as an attachment
            Document document = reminders.createAttachment(context);
            mailer.addAttachment(document);
        }

        // add any attachments associated with the email template, ordered on template name.
        // This assumes that the template and document names are similar.
        for (DocumentTemplate template : reminders.getEmailTemplate().getAttachments(true)) {
            Document document = template.getDocument();
            if (document != null) {
                mailer.addAttachment(document);
            }
        }

        mailer.send();
        return mailer;
    }

}
