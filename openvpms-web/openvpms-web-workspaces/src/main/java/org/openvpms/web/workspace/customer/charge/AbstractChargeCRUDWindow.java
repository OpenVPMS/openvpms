/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Button;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.CustomerActCRUDWindow;
import org.openvpms.web.workspace.customer.account.AccountActActions;

/**
 * .
 *
 * @author Tim Anderson
 */
public class AbstractChargeCRUDWindow extends CustomerActCRUDWindow<FinancialAct> {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * The reminder rules.
     */
    private final AccountReminderRules reminderRules;

    /**
     * Enable reminders button identifier.
     */
    private static final String ENABLE_REMINDERS_ID = "button.enableReminders";

    /**
     * Disable reminders button identifier.
     */
    private static final String DISABLE_REMINDERS_ID = "button.disableReminders";

    /**
     * Constructs an {@link AbstractChargeCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create
     * @param context    the context
     * @param help       the help context
     */
    public AbstractChargeCRUDWindow(Archetypes<FinancialAct> archetypes, Context context, HelpContext help) {
        super(archetypes, null, context, help);

        rules = ServiceHelper.getBean(CustomerAccountRules.class);
        reminderRules = ServiceHelper.getBean(AccountReminderRules.class);
        PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
        setActions(new AccountActActions(rules, reminderRules, practiceService));
    }

    /**
     * Returns the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected AccountActActions getActions() {
        return (AccountActActions) super.getActions();
    }

    /**
     * Enables/disables reminder buttons.
     *
     * @param buttons the buttons
     * @param enable  if {@code true} enable buttons, else disable them
     */
    protected void enableDisableReminderButtons(ButtonSet buttons, boolean enable) {
        buttons.setEnabled(ENABLE_REMINDERS_ID, enable && getActions().canEnableReminders(getObject()));
        buttons.setEnabled(DISABLE_REMINDERS_ID, enable && getActions().canDisableReminders(getObject()));
    }

    /**
     * Creates a button to enable reminders.
     *
     * @return a new button
     */
    protected Button createEnableRemindersButton() {
        return ButtonFactory.create(ENABLE_REMINDERS_ID,
                                    action(this::enableReminders, reminderRules::canEnableReminders, true,
                                           Messages.get("customer.charge.enableReminders")));
    }

    /**
     * Creates a button to disable reminders.
     *
     * @return a new button
     */
    protected Button createDisableRemindersButton() {
        return ButtonFactory.create(DISABLE_REMINDERS_ID,
                                    action(this::disableReminders, reminderRules::canDisableReminders, true,
                                           Messages.get("customer.charge.disableReminders")));
    }

    /**
     * Returns the customer account rules.
     *
     * @return the rules
     */
    protected CustomerAccountRules getRules() {
        return rules;
    }

    /**
     * Enables reminders for an act.
     *
     * @param act the act
     */
    private void enableReminders(FinancialAct act) {
        reminderRules.enableReminders(act);
        onRefresh(act);
    }

    /**
     * Disables reminders for an act.
     *
     * @param act the act
     */
    private void disableReminders(FinancialAct act) {
        reminderRules.disableReminders(act);
        onRefresh(act);
    }
}