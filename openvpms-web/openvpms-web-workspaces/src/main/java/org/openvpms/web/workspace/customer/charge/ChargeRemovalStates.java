/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.component.model.act.Act;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListQuery;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.table.DefaultListMarkModel;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.im.table.ListMarkModel;
import org.openvpms.web.component.im.table.MarkablePagedIMObjectTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.openvpms.web.echo.style.Styles.CELL_SPACING;

/**
 * Tracks charge removal states.
 * <p/>
 * This determines if a charge:
 * <ul>
 *     <li>can be removed without prompting</li>
 *     <li>can only be removed with override permissions</li>
 *     <li>cannot be removed</li>
 * </ul
 *
 * @author Tim Anderson
 */
public class ChargeRemovalStates {

    public enum Status {
        OK,        // the charge is OK to be removed
        OVERRIDE,  // the charge requires override authority to be removed
        FORBIDDEN  // the charge cannot be removed
    }

    /**
     * The states associated with each charge.
     */
    private final Map<Act, ChargeState> states = new LinkedHashMap<>();

    /**
     * Adds a status for a charge.
     *
     * @param act     the charge
     * @param status  the status
     * @param message the message
     */
    public void addStatus(Act act, Status status, String message) {
        ChargeState chargeState = states.computeIfAbsent(act, k -> new ChargeState());
        chargeState.addStatus(status, message);
    }

    /**
     * Determines if removal requires prompting.
     *
     * @return {@code true} if removal requires prompting
     */
    public boolean prompt() {
        return !states.isEmpty();
    }

    /**
     * Determines if an object can be deleted.
     *
     * @param object the object
     * @return {@code true} if the object can be deleted
     */
    public boolean canDelete(Act object) {
        return states.get(object).getStatus() != Status.FORBIDDEN;
    }

    /**
     * Displays a prompt to confirm/disallow deletion of charges.
     *
     * @param name     the charges display name
     * @param toRemove the callback to invoke when removal is confirmed
     * @param cancel   the callback to invoke when removal is cancelled
     * @param context  the context
     */
    public void show(String name, Consumer<Collection<Act>> toRemove, Runnable cancel, LayoutContext context) {
        List<Act> acts = getObjects();

        int forbidden = 0;
        int override = 0;
        for (ChargeState state : states.values()) {
            switch (state.getStatus()) {
                case FORBIDDEN:
                    forbidden++;
                    break;
                case OVERRIDE:
                    override++;
            }
        }

        String title;
        String message;
        if (forbidden == states.size()) {
            // no item may be removed
            title = Messages.format("customer.charge.delete.forbidden.title", name);
            message = Messages.format("customer.charge.delete.forbidden", states.size());
        } else {
            title = Messages.format("imobject.collection.delete.title", name);
            if (forbidden > 0) {
                // at least one item cannot be removed
                message = Messages.format("customer.charge.delete.someforbidden", name);
            } else if (override > 0) {
                message = Messages.format("customer.charge.delete.override", name);
            } else {
                message = Messages.format("customer.charge.delete.confirm", name);
            }
        }

        DeleteDialog dialog = new DeleteDialog(title, message, new ArrayList<>(acts), context);

        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                toRemove.accept(dialog.getSelections());
            }

            @Override
            public void onCancel() {
                cancel.run();
            }
        });
        dialog.show();
    }

    /**
     * Creates a table model to display charges.
     *
     * @param archetype the charge archetype
     * @param context   the layout context
     * @return the table model
     */
    protected IMObjectTableModel<Act> createModel(String archetype, LayoutContext context) {
        return new ChargeModel(archetype, context);
    }

    /**
     * Returns the charges available for deletion.
     *
     * @return the charges
     */
    protected List<Act> getObjects() {
        return new ArrayList<>(states.keySet());
    }

    protected class ChargeModel extends DefaultDescriptorTableModel<Act> {

        /**
         * The message column index.
         */
        private int messageIndex;

        /**
         * Bullet character.
         */
        private static final String BULLET = "\u2022";

        /**
         * Constructs a {@link ChargeModel}.
         *
         * @param archetype the charge archetype
         * @param context   the layout context
         */
        public ChargeModel(String archetype, LayoutContext context) {
            super(archetype, context, "startTime", "patient", "product");
        }

        /**
         * Creates a column model for a set of archetypes.
         *
         * @param shortNames the archetype short names
         * @param context    the layout context
         * @return a new column model
         */
        @Override
        protected TableColumnModel createColumnModel(String[] shortNames, LayoutContext context) {
            DefaultTableColumnModel model = (DefaultTableColumnModel) super.createColumnModel(shortNames, context);
            addColumns(model);
            return model;
        }

        /**
         * Adds columns.
         *
         * @param model the column model
         */
        protected void addColumns(DefaultTableColumnModel model) {
            messageIndex = getNextModelIndex(model);
            model.addColumn(new TableColumn(messageIndex));
            addMarkColumn(model);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the table column
         * @param row    the table row
         */
        @Override
        protected Object getValue(Act object, TableColumn column, int row) {
            Object result = null;
            if (column.getModelIndex() == messageIndex) {
                ChargeState chargeState = states.get(object);
                if (chargeState != null) {
                    List<String> messages = chargeState.getMessages();
                    if (messages.size() == 1) {
                        result = LabelFactory.text(messages.get(0), true);
                    } else if (messages.size() > 1) {
                        // need to jump through some hoops to display a bulleted list.
                        ComponentGrid grid = new ComponentGrid();
                        for (String message : messages) {
                            grid.add(LabelFactory.text(BULLET), LabelFactory.text(message));
                        }
                        result = grid.createGrid();
                    }
                }
            } else {
                result = super.getValue(object, column, row);
            }
            return result;
        }
    }

    /**
     * Dialog to display deletion confirmation.
     */
    private class DeleteDialog extends ConfirmationDialog {

        /**
         * The objects with minimum quantities.
         */
        private final List<Act> objects;

        /**
         * The layout context.
         */
        private final LayoutContext context;

        /**
         * The table model.
         */
        private MarkablePagedIMObjectTableModel<Act> markableModel;

        /**
         * Constructs a {@link DeleteDialog}.
         *
         * @param title   the dialog title
         * @param message the message to display
         * @param objects the objects with minimum quantities
         * @param context the layout context
         */
        DeleteDialog(String title, String message, List<Act> objects, LayoutContext context) {
            super(title, message, OK_CANCEL);
            setStyleName("ConfirmationDialog");
            this.objects = objects;
            this.context = context;
        }

        /**
         * Returns the charges selected for deletion.
         *
         * @return the charges selected for deletion
         */
        public List<Act> getSelections() {
            return markableModel.getMarked(objects);
        }

        /**
         * Lays out the component prior to display.
         */
        @Override
        protected void doLayout() {
            Label content = LabelFactory.create(true, true);
            content.setText(getMessage());
            String archetype = objects.get(0).getArchetype();
            ListQuery<Act> query = new ListQuery<>(objects, archetype, false, Act.class);
            query.setMaxResults(7); // TODO need a better dialog style. This is to support 1024x768
            IMObjectTableModel<Act> model = createModel(archetype, context);
            model.setRowMarkModel(new DefaultListMarkModel() {
                @Override
                public boolean canMark(int index) {
                    Act object = model.getObjects().get(index);
                    return canDelete(object);
                }
            });

            markableModel = new MarkablePagedIMObjectTableModel<>(model);
            for (Act object : objects) {
                markableModel.setMarked(object, states.get(object).getStatus() == Status.OK);
            }
            PagedIMTable<Act> table = new PagedIMTable<>(markableModel);
            table.setResultSet(new ListResultSet<>(objects, 7));

            Column column = ColumnFactory.create(CELL_SPACING, content, table.getComponent());
            markableModel.getRowMarkModel().addListener(new ListMarkModel.Listener() {
                @Override
                public void changed(int index, boolean marked) {
                    enableDelete();
                }

                @Override
                public void cleared() {
                    enableDelete();
                }
            });

            getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
            enableDelete();
            resize();
        }

        /**
         * Resizes the dialog if required.
         */
        @Override
        protected void resize() {
            resize("ChargeRemovalConfirmationDialog.size");
        }

        /**
         * Enables/disables the OK button if charges are selected.
         */
        private void enableDelete() {
            getButtons().setEnabled(OK_ID, !markableModel.getRowMarkModel().isEmpty());
        }
    }

    /**
     * Status and message.
     */
    private static class StatusMessage {

        private final ChargeRemovalStates.Status status;

        private final String message;

        public StatusMessage(ChargeRemovalStates.Status status, String message) {
            this.status = status;
            this.message = message;
        }
    }

    /**
     * The state for a single charge.
     */
    private static class ChargeState {

        private final List<StatusMessage> statusMessages = new ArrayList<>();

        /**
         * Adds a status.
         *
         * @param status  the status
         * @param message the status message
         */
        public void addStatus(Status status, String message) {
            statusMessages.add(new StatusMessage(status, message));
        }


        /**
         * Returns the cumulative status for the charge.
         *
         * @return the cumulative status
         */
        public Status getStatus() {
            Status result = Status.OK;
            for (StatusMessage statusMessage : statusMessages) {
                if (statusMessage.status.compareTo(result) > 0) {
                    result = statusMessage.status;
                }
            }
            return result;
        }

        /**
         * Returns the messages associated with the charge.
         *
         * @return the messages
         */
        public List<String> getMessages() {
            List<String> result = new ArrayList<>();
            for (StatusMessage statusMessage : statusMessages) {
                if (statusMessage.message != null) {
                    result.add(statusMessage.message);
                }
            }
            return result;
        }
    }
}