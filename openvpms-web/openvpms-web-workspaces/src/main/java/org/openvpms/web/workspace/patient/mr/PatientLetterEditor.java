/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;

import java.util.function.Consumer;

/**
 * An editor for <em>act.patientDocumentLetter</em> acts.
 *
 * @author Tim Anderson
 */
public class PatientLetterEditor extends PatientDocumentActEditor {

    /**
     * Constructs a {@link PatientLetterEditor}.
     *
     * @param act     the act
     * @param parent  the parent
     * @param context the layout context
     */
    public PatientLetterEditor(DocumentAct act, IMObject parent, LayoutContext context) {
        super(act, parent, context);
        getProperty("description").addModifiableListener(modifiable -> optionalAttributeModified());
    }

    /**
     * Sets the product.
     *
     * @param product the product. May be {@code null}
     */
    public void setProduct(Product product) {
        setParticipant("product", product);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     * @throws OpenVPMSException if a new instance cannot be created
     */
    @Override
    public IMObjectEditor newInstance() {
        // TODO - this can leave document references hanging
        return new PatientLetterEditor((DocumentAct) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Invoked when layout has completed.
     */
    @Override
    protected void onLayoutCompleted() {
        super.onLayoutCompleted();
        // flag that the document should be prompted to be regenerated if the clinician or product changes
        Consumer<Entity> listener = entity -> optionalAttributeModified();
        monitorParticipation("clinician", listener);
        monitorParticipation("product", listener);
    }

    /**
     * Flags an optional attribute has having been modified.
     * <p/>
     * This will prompt to regenerate the document on save.
     */
    private void optionalAttributeModified() {
        documentAttributeModified(false);
    }
}