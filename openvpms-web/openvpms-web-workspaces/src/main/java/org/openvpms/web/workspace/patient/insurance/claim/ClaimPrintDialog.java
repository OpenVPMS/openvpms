/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.mail.MailDialog;
import org.openvpms.web.component.print.BatchPrintDialog;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;
import java.util.function.Consumer;

/**
 * Dialog to print a claim and its attachments.
 *
 * @author Tim Anderson
 */
class ClaimPrintDialog extends BatchPrintDialog {

    /**
     * The claim.
     */
    private final Claim claim;

    /**
     * The claim act.
     */
    private final Act act;

    /**
     * The context.
     */
    private final Context context;

    /**
     * Mail button id.
     */
    private static final String MAIL_ID = "button.mail";

    /**
     * Constructs a {@link ClaimPrintDialog}.
     *
     * @param claim   the claim
     * @param act     the claim act
     * @param message the message to display. May be {@code null}
     * @param objects the objects to print. The boolean value indicates if the object should be selected by default
     * @param context the context
     * @param help    the help context
     */
    public ClaimPrintDialog(Claim claim, Act act, String message, List<IMObject> objects, Context context,
                            HelpContext help) {
        super(Messages.get("printdialog.title"), message, new String[]{OK_ID, CANCEL_ID, MAIL_ID}, objects, help);
        this.claim = claim;
        this.act = act;
        this.context = context;
    }

    /**
     * Invoked when the mail button is pressed. Displays the selected documents in a mail editor.
     */
    protected void onMail() {
        List<IMObject> selected = getSelected();
        if (!selected.isEmpty()) {
            mail(selected, dialog -> {
                dialog.addWindowPaneListener(new WindowPaneListener() {
                    @Override
                    public void onClose(WindowPaneEvent event) {
                        if (MailDialog.SEND_ID.equals(dialog.getAction())) {
                            ClaimPrintDialog.this.close(MAIL_ID);
                        }
                    }
                });
                dialog.show();
            });
        }
    }

    /**
     * Invoked when a button is pressed. This delegates to the appropriate
     * on*() method for the button if it is known, else sets the action to
     * the button identifier and closes the window.
     *
     * @param button the button identifier
     */
    @Override
    protected void onButton(String button) {
        if (MAIL_ID.equals(button)) {
            onMail();
        } else {
            super.onButton(button);
        }
    }

    /**
     * Asynchronously creates a {@link MailDialog} to email a claim.
     *
     * @param list     the attachments
     * @param listener the listener to notify when the dialog is created
     */
    private void mail(List<IMObject> list, Consumer<MailDialog> listener) {
        HelpContext email = getHelpContext().subtopic("email");
        ClaimMailer mailer = new ClaimMailer();
        mailer.mail(claim, act, list, context, email, listener);
    }

}
