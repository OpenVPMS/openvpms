/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.communication;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.text.TextComponent;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.AuditableIMObject;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.audit.AuditInfo;
import org.openvpms.web.component.im.filter.NodeFilter;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.ComponentSet;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.DocumentBackedTextProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.workspace.workflow.messaging.messages.AbstractMessageLayoutStrategy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openvpms.web.component.im.layout.ArchetypeNodes.exclude;
import static org.openvpms.web.component.im.layout.ArchetypeNodes.include;
import static org.openvpms.web.component.im.layout.ComponentGrid.layout;

/**
 * Base {@link IMObjectLayoutStrategy} for customer-related communication acts.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCommunicationLayoutStrategy extends AbstractMessageLayoutStrategy {

    /**
     * The created-by node name.
     */
    public static final String CREATED_BY = "createdBy";

    /**
     * The address node name.
     */
    public static final String ADDRESS = "address";

    /**
     * The description node name.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The location node name.
     */
    public static final String LOCATION = "location";

    /**
     * The patient node name.
     */
    public static final String PATIENT = "patient";

    /**
     * The start time node name.
     */
    public static final String START_TIME = "startTime";

    /**
     * The message node name.
     */
    public static final String MESSAGE = "message";

    /**
     * The note node name.
     */
    public static final String NOTE = "note";

    /**
     * The document node name.
     */
    public static final String DOCUMENT = "document";

    /**
     * The reason node name.
     */
    public static final String REASON = "reason";

    /**
     * Determines if the patient node should be displayed when editing.
     */
    private final boolean showPatient;

    /**
     * The name of the node holding the created time of the communication.
     */
    private final String createdTime;

    /**
     * The property representing the message.
     */
    private Property messageProxy;

    /**
     * Constructs an {@link AbstractCommunicationLayoutStrategy}.
     *
     * @param showPatient determines if the patient should be displayed
     * @param message     the message property, or {@code null} to use the default
     * @param createdTime the name of the node holding the created time of the communication
     */
    public AbstractCommunicationLayoutStrategy(boolean showPatient, Property message, String createdTime) {
        this.showPatient = showPatient;
        this.messageProxy = message;
        this.createdTime = createdTime;
    }

    /**
     * Apply the layout strategy.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        if (messageProxy == null) {
            Property message = properties.get(MESSAGE);
            if (properties.get(DOCUMENT) != null) {
                // message is backed by a document if it is too long
                messageProxy = new DocumentBackedTextProperty((DocumentAct) object, message);
            } else {
                messageProxy = message;
            }
        }
        Property descriptionProperty = properties.get(DESCRIPTION);
        if (descriptionProperty != null) {
            ComponentState description = createComponent(descriptionProperty, object, context);
            if (description.getComponent() instanceof TextComponent) {
                ((TextComponent) description.getComponent()).setWidth(Styles.FULL_WIDTH);
            }
            addComponent(description);
        }

        addComponent(createMessage(messageProxy, context));
        Property note = properties.get(NOTE);
        if (note != null) {
            addComponent(createMultiLineText(note, 2, 10, Styles.FULL_WIDTH, context));
        }
        return super.apply(object, properties, parent, context);
    }

    /**
     * Creates a component to display the message.
     *
     * @param property the message property
     * @param context  the layout context
     * @return a new component
     */
    protected ComponentState createMessage(Property property, LayoutContext context) {
        return createMultiLineText(property, 10, 20, Styles.FULL_WIDTH, context);
    }

    /**
     * Lay out out the object in the specified container.
     *
     * @param object     the object to lay out
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param container  the container to use
     * @param context    the layout context
     */
    @Override
    protected void doLayout(IMObject object, PropertySet properties, IMObject parent, Component container,
                            LayoutContext context) {
        ArchetypeDescriptor archetype = context.getArchetypeDescriptor(object);
        ArchetypeNodes nodes = getArchetypeNodes();
        NodeFilter filter = getNodeFilter(object, context);

        List<Property> simple = nodes.getSimpleNodes(properties, archetype, object, filter);
        List<Property> complex = nodes.getComplexNodes(properties, archetype, object, filter);

        List<Property> header = getHeaderProperties(simple);
        List<Property> patient = getPatientProperties(simple);
        List<Property> fields = exclude(simple, createdTime, MESSAGE, NOTE);
        List<Property> text = getTextProperties(simple, messageProxy);
        fields.removeAll(header);
        fields.removeAll(patient);
        fields.removeAll(text);

        if (!showPatient) {
            patient = ArchetypeNodes.exclude(patient, PATIENT);
        }

        if (!context.isEdit()) {
            // hide empty nodes in view layout
            patient = excludeEmptyPatientProperties(patient);
            header = excludeEmptyHeaderProperties(header);
            fields = excludeEmptyFields(fields);
            text = excludeEmptyTextProperties(text);
        }

        ComponentGrid componentGrid = new ComponentGrid();
        ComponentSet headerSet = createComponentSet(object, header, context);
        ComponentSet patientSet = createComponentSet(object, patient, context);
        ComponentSet fieldSet = createComponentSet(object, fields, context);
        ComponentSet textSet = createComponentSet(object, text, context);
        Component created = getCreated(properties.get(createdTime), object, context);
        componentGrid.set(0, 3, layout(Alignment.ALIGN_RIGHT), created);
        componentGrid.add(headerSet, 1, 2);
        if (patientSet.size() != 0) {
            // in view mode, display location and patient on separate rows as it looks odd on wide-screens.
            // In the dialog (width-constrained) it looks better on one row.
            int columns = (context.isEdit()) ? patientSet.size() : 1;
            componentGrid.add(patientSet, columns);
        }
        componentGrid.add(fieldSet, 2);
        addTextComponents(componentGrid, textSet);

        Grid grid = createGrid(componentGrid);
        grid.setWidth(new Extent(99, Extent.PERCENT)); // 100% screws up right insets on Firefox

        container.add(ColumnFactory.create(Styles.INSET, grid));
        doComplexLayout(object, parent, complex, container, context);
    }

    /**
     * Returns the text properties.
     * <p>
     * These are rendered under each other.
     *
     * @param properties the properties
     * @param message    the message property
     * @return the text properties
     */
    protected List<Property> getTextProperties(List<Property> properties, Property message) {
        List<Property> result = new ArrayList<>();
        result.add(message);
        result.addAll(include(properties, NOTE));
        return result;
    }

    /**
     * Adds text components to the grid.
     *
     * @param grid       the grid
     * @param components the components to add
     */
    protected void addTextComponents(ComponentGrid grid, ComponentSet components) {
        grid.add(components, 1, 2);
    }

    /**
     * Returns the properties to display in the header.
     * <p>
     * Note; this does not exclude empty properties the returned properties are used to determine the other nodes
     * to display. Empty properties are excluded via {@link #excludeEmptyHeaderProperties(List)} instead.
     *
     * @param properties the properties
     * @return the header properties
     */
    protected List<Property> getHeaderProperties(List<Property> properties) {
        return include(properties, ADDRESS, DESCRIPTION);
    }

    /**
     * Returns the patient-related properties.
     * <p/>
     * This implementation includes the location and patient
     *
     * @param properties the properties
     * @return the header properties
     */
    protected List<Property> getPatientProperties(List<Property> properties) {
        return include(properties, LOCATION, PATIENT);
    }

    /**
     * Excludes empty properties related to the patient.
     *
     * @param properties the patient properties
     * @return the properties to render
     */
    protected List<Property> excludeEmptyPatientProperties(List<Property> properties) {
        return excludeIfEmpty(properties, PATIENT, LOCATION);
    }

    /**
     * Excludes empty header properties when viewing.
     * <p>
     * This implementation excludes the description.
     *
     * @param properties the header properties
     * @return the properties to render
     */
    protected List<Property> excludeEmptyHeaderProperties(List<Property> properties) {
        return excludeIfEmpty(properties, DESCRIPTION);
    }

    /**
     * Excludes empty fields when viewing.
     *
     * @param properties the fields
     * @return the properties to render
     */
    protected List<Property> excludeEmptyFields(List<Property> properties) {
        return excludeIfEmpty(properties, REASON);
    }

    /**
     * Excludes empty text properties when viewing.
     *
     * @param properties the text properties
     * @return the properties to render
     */
    protected List<Property> excludeEmptyTextProperties(List<Property> properties) {
        return excludeIfEmpty(properties, NOTE);
    }

    /**
     * Helper to exclude properties if they are empty.
     *
     * @param properties the properties
     * @param names      the names of the properties to exclude
     * @return the filtered properties
     */
    protected List<Property> excludeIfEmpty(List<Property> properties, String... names) {
        List<Property> result = new ArrayList<>(properties);
        for (Property property : properties) {
            for (String name : names) {
                if (property.getName().equals(name) && property.isEmpty()) {
                    result.remove(property);
                }
            }
        }
        return result;
    }

    /**
     * Returns a component representing the created time and audit info of the communication.
     *
     * @param property the created-time property
     * @param object   the parent object
     * @return a corresponding component
     */
    protected Component getCreated(Property property, IMObject object, LayoutContext context) {
        Component result = null;
        Date date = property.getDate();
        if (date != null) {
            result = LabelFactory.text(DateFormatter.formatDateTimeAbbrev(date));
        } else {
            result = LabelFactory.create();
        }
        if (object instanceof AuditableIMObject) {
            AuditInfo info = new AuditInfo((AuditableIMObject) object, context.getNames());
            result = RowFactory.create(Styles.CELL_SPACING, result, info);
        }
        return result;
    }
}
