/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2025 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.insurance.claim;

import nextapp.echo2.app.Extent;
import nextapp.echo2.app.ListBox;
import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.practice.LocationRules;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.insurance.claim.Claim;
import org.openvpms.insurance.claim.GapClaim;
import org.openvpms.insurance.exception.InsuranceException;
import org.openvpms.insurance.internal.InsuranceFactory;
import org.openvpms.insurance.internal.claim.ClaimImpl;
import org.openvpms.insurance.internal.claim.GapClaimImpl;
import org.openvpms.insurance.service.ClaimValidationStatus;
import org.openvpms.insurance.service.Declaration;
import org.openvpms.insurance.service.GapClaimAvailability;
import org.openvpms.insurance.service.GapInsuranceService;
import org.openvpms.insurance.service.InsuranceService;
import org.openvpms.insurance.service.InsuranceServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.list.IMObjectListCellRenderer;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.component.workflow.DefaultTaskListener;
import org.openvpms.web.component.workflow.TaskEvent;
import org.openvpms.web.echo.dialog.ConfirmationDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.dialog.PopupWindow;
import org.openvpms.web.echo.dialog.SelectionDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.list.KeyListBox;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.insurance.CancelClaimDialog;
import org.openvpms.web.workspace.workflow.payment.PaymentWorkflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Submits insurance claims.
 *
 * @author Tim Anderson
 */
public class ClaimSubmitter {

    /**
     * The context.
     */
    private final Context context;


    /**
     * The help context.
     */
    private final HelpContext help;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The insurance services.
     */
    private final InsuranceServices insuranceServices;

    /**
     * The insurance factory.
     */
    private final InsuranceFactory factory;

    /**
     * Accept declaration button id.
     */
    private static final String ACCEPT_ID = "button.accept";

    /**
     * Decline declaration button it.
     */
    private static final String DECLINE_ID = "button.decline";


    /**
     * Constructs a {@link ClaimSubmitter}.
     *
     * @param service           the archetype service
     * @param factory           the insurance factory
     * @param insuranceServices the insurance services
     * @param context           the context
     * @param help              the help context
     */
    public ClaimSubmitter(IArchetypeService service, InsuranceFactory factory, InsuranceServices insuranceServices,
                          Context context, HelpContext help) {
        this.context = context;
        this.help = help;
        this.service = service;
        this.factory = factory;
        this.insuranceServices = insuranceServices;
    }

    /**
     * Returns a claim for a claim act.
     *
     * @param act the claim act
     * @return the claim
     */
    public Claim getClaim(Act act) {
        return factory.createClaim(act);
    }

    /**
     * Submits a claim being edited.
     *
     * @param editor   the claim editor
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void submit(ClaimEditor editor, Consumer<Throwable> listener) {
        try {
            if (!Claim.Status.PENDING.isA(editor.getObject().getStatus())) {
                throw new IllegalStateException("Claim must have PENDING status");
            }
            prepare(editor, state -> {
                if (state == null) {
                    listener.accept(null);
                } else {
                    submit(state, listener);
                }
            });
        } catch (Throwable exception) {
            listener.accept(exception);
        }
    }

    /**
     * Submits a finalised claim.
     * <p>
     * NOTE: this does not make any checks to determine if a gap claim can/can't be submitted, as by this stage
     * it is too late to change a gap to non-gap claim.
     *
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void submit(Act act, Consumer<Throwable> listener) {
        if (!Claim.Status.POSTED.isA(act.getStatus())) {
            throw new IllegalStateException("Claim must have POSTED status");
        }
        if (verifyNoDuplicates(act)) {
            Claim claim = factory.createClaim(act);
            Party insurer = claim.getPolicy().getInsurer();
            String title = Messages.get("patient.insurance.submit.title");
            if (insuranceServices.canSubmit(insurer)) {
                InsuranceService service = insuranceServices.getService(insurer);
                ConfirmationDialog.newDialog()
                        .title(title)
                        .message(Messages.format("patient.insurance.submit.online", insurer.getName(),
                                                 service.getName()))
                        .yesNo()
                        .yes(() -> submitWithDeclaration(claim, service, listener))
                        .no(() -> listener.accept(null))
                        .show();
            } else {
                ConfirmationDialog.newDialog()
                        .title(title)
                        .message(Messages.format("patient.insurance.submit.offline", insurer.getName()))
                        .yesNo()
                        .yes(() -> runProtected(listener, () -> claim.setStatus(Claim.Status.SUBMITTED)))
                        .no(() -> listener.accept(null))
                        .show();
            }
        } else {
            listener.accept(null);
        }
    }

    /**
     * Pays a gap claim.
     * <p>
     * If the claim hasn't been accepted, or no benefit amount has been received, this gives the user the option to wait
     * for it.
     *
     * @param act      the claim
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void pay(Act act, Consumer<Throwable> listener) {
        Claim claim = getClaim(act);
        if (!(claim instanceof GapClaimImpl)) {
            throw new IllegalArgumentException("Argument 'claim' is not a GapClaim");
        }
        GapClaimImpl gapClaim = (GapClaimImpl) claim;
        Claim.Status status = gapClaim.getStatus();
        GapClaim.GapStatus gapStatus = gapClaim.getGapStatus();
        if ((status == Claim.Status.SUBMITTED || status == Claim.Status.ACCEPTED)
            && gapStatus == GapClaim.GapStatus.PENDING) {
            waitForBenefit(gapClaim, listener);
        } else if (status == Claim.Status.ACCEPTED || status == Claim.Status.PRE_SETTLED) {
            if (gapStatus == GapClaim.GapStatus.RECEIVED) {
                promptToPayClaim(gapClaim, false, listener);
            } else if (gapStatus == GapClaim.GapStatus.PAID) {
                notifyInsurerOfPayment(gapClaim, listener);
            } else {
                listener.accept(null);
            }
        }
    }

    /**
     * Prints a claim.
     *
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void print(Act act, Consumer<Throwable> listener) {
        Claim claim = getClaim(act);
        print(claim, act, listener);
    }

    /**
     * Emails a claim.
     *
     * @param act the claim act
     */
    public void mail(Act act) {
        Claim claim = getClaim(act);
        Attachments attachments = new Attachments(act);
        if (attachments.missingDocuments()) {
            ConfirmationDialog.newDialog()
                    .title(Messages.get("patient.insurance.mail.title"))
                    .message(Messages.format("patient.insurance.mail.noattachment", attachments.getMissing()))
                    .yesNo()
                    .yes(() -> mail(claim, act, attachments))
                    .show();
        } else {
            mail(claim, act, attachments);
        }
    }

    /**
     * Cancels a claim.
     *
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void cancel(Act act, Consumer<Throwable> listener) {
        Claim claim = getClaim(act);
        Party insurer = claim.getPolicy().getInsurer();
        String title = Messages.get("patient.insurance.cancel.title");
        if (insuranceServices.canSubmit(insurer)) {
            InsuranceService service = getInsuranceService(insurer);
            if (service.canCancel(claim)) {
                String message = Messages.format("patient.insurance.cancel.online", service.getName());
                CancelClaimDialog dialog = new CancelClaimDialog(title, message, help);
                dialog.addWindowPaneListener(new PopupDialogListener() {
                    @Override
                    public void onYes() {
                        runProtected(listener, () -> service.cancel(claim, dialog.getReason()));
                    }
                });
                dialog.show();
            } else {
                info(title, Messages.format("patient.insurance.cancel.unsupported"), listener);
            }
        } else {
            String message = Messages.format("patient.insurance.cancel.offline", insurer.getName());
            CancelClaimDialog dialog = new CancelClaimDialog(title, message, help);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onYes() {
                    runProtected(listener, () -> claim.setStatus(Claim.Status.CANCELLED, dialog.getReason()));
                }

                @Override
                public void onNo() {
                    listener.accept(null);
                }
            });
            dialog.show();
        }
    }

    /**
     * Settles a claim.
     *
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void settle(Act act, Consumer<Throwable> listener) {
        Claim claim = getClaim(act);
        Party insurer = claim.getPolicy().getInsurer();
        String title = Messages.get("patient.insurance.settle.title");
        if (insuranceServices.canSubmit(insurer)) {
            info(title, Messages.format("patient.insurance.settle.online", insurer.getName()), listener);
        } else {
            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(Messages.format("patient.insurance.settle.offline", insurer.getName()))
                    .yesNo()
                    .yes(() -> runProtected(listener, () -> claim.setStatus(Claim.Status.SETTLED)))
                    .no(() -> listener.accept(null))
                    .show();
        }
    }

    /**
     * Declines a claim.
     *
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    public void decline(Act act, Consumer<Throwable> listener) {
        Claim claim = getClaim(act);
        Party insurer = claim.getPolicy().getInsurer();
        String title = Messages.get("patient.insurance.decline.title");
        if (insuranceServices.canSubmit(insurer)) {
            info(title, Messages.format("patient.insurance.decline.online", insurer.getName()), listener);
        } else {
            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(Messages.format("patient.insurance.decline.offline", insurer.getName()))
                    .yesNo()
                    .yes(() -> runProtected(listener, () -> claim.setStatus(Claim.Status.DECLINED)))
                    .no(() -> listener.accept(null))
                    .show();
        }
    }

    /**
     * Confirms submission of a claim prior to submitting it.
     *
     * @param state    the claim state
     * @param title    the dialog title
     * @param listener the listener to invoke on completion or failure
     */
    protected void confirmSubmit(ClaimState state, String title, Consumer<Throwable> listener) {
        Claim claim = state.getClaim();
        Party insurer = claim.getPolicy().getInsurer();
        InsuranceService service = state.getService();
        if (service != null) {
            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(Messages.format("patient.insurance.submit.online", insurer.getName(), service.getName()))
                    .yesNo()
                    .yes(() -> submitOnlineClaim(state, listener))
                    .no(() -> listener.accept(null))
                    .show();
        } else {
            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(Messages.format("patient.insurance.submit.offline", insurer.getName()))
                    .yesNo()
                    .yes(() -> submitOfflineClaim(state, listener))
                    .no(() -> listener.accept(null))
                    .show();
        }
    }

    /**
     * Prints a claim.
     *
     * @param claim    the claim
     * @param act      the claim act
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    protected void print(Claim claim, Act act, Consumer<Throwable> listener) {
        Attachments attachments = new Attachments(act);
        String message = (attachments.missingDocuments())
                         ? Messages.format("patient.insurance.print.noattachment", attachments.getMissing())
                         : null;
        Context context = createContext((ClaimImpl) claim);
        ClaimPrintDialog dialog = new ClaimPrintDialog(claim, act, message, attachments.getAttachments(), context,
                                                       help);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                List<IMObject> selected = dialog.getSelected();
                ClaimBatchPrinter printer = new ClaimBatchPrinter(claim, selected, context, help, listener);
                printer.print();
            }

            @Override
            public void onCancel() {
                listener.accept(null);
            }
        });
        dialog.show();
    }

    /**
     * Prepares a claim for finalisation.
     * <p/>
     * On completion the listener will be passed the claim state, or {@code null} if the claim cannot be prepared for
     * finalisation.
     *
     * @param editor   the claim editor
     * @param listener the listener
     * @throws InsuranceException for any error
     */
    protected void prepare(ClaimEditor editor, Consumer<ClaimState> listener) {
        if (editor.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            if (verifyNoDuplicates(editor.getObject())) {
                editor.generateAttachments(success -> {
                    ClaimState state = null;
                    boolean generated = success != null && success;
                    try {
                        if (generated && checkSubmission(editor)) {
                            state = createClaimState(editor);
                        }
                    } catch (Throwable exception) {
                        ErrorHelper.show(exception);
                    }
                    listener.accept(state);
                });
            } else {
                listener.accept(null);
            }
        } else {
            ErrorHelper.show(Messages.get("patient.insurance.submit.title"),
                             Messages.get("patient.insurance.noinvoice"));
            listener.accept(null);
        }
    }

    /**
     * Determines if a claim can be submitted.
     * <p>
     * If the claim is not a gap claim, it can be submitted.<br/>
     * If the claim is a gap claim, it can only be submitted if the insurer supports it.
     *
     * @param editor the claim editor
     * @return {@code true} if the claim can be submitted
     */
    protected boolean checkSubmission(ClaimEditor editor) {
        boolean canSubmit;
        boolean gapClaim = editor.isGapClaim();
        if (!gapClaim) {
            canSubmit = true;
        } else {
            GapClaimAvailability support = editor.getGapClaimAvailability();
            canSubmit = support.isAvailable();
            if (!canSubmit) {
                String message = support.getMessage();
                if (message == null) {
                    Party insurer = editor.getInsurer();
                    String name = insurer != null ? insurer.getName() : Messages.get("imobject.none");
                    message = Messages.format("patient.insurance.gap.notsupported", name);
                }
                InformationDialog.show(message);
            }
        }
        return canSubmit;
    }

    /**
     * Creates a benefit dialog for a gap claim.
     *
     * @param claim the claim
     * @return a new {@link BenefitDialog}
     */
    protected BenefitDialog createBenefitDialog(GapClaimImpl claim) {
        return new BenefitDialog(claim, help.subtopic("benefit"));
    }

    /**
     * Creates a {@link ClaimState}.
     *
     * @param editor the claim editor
     * @return the claim state
     */
    private ClaimState createClaimState(ClaimEditor editor) {
        Claim claim = factory.createClaim(editor.getObject());
        Party insurer = claim.getPolicy().getInsurer();
        ClaimValidationStatus status = null;
        InsuranceService service = null;
        if (insuranceServices.canSubmit(insurer)) {
            service = getInsuranceService(insurer);
            status = service.validate(claim);
        }
        return new ClaimState(editor.getObject(), claim, status, service);
    }

    /**
     * Submits a claim.
     *
     * @param state    the claim state
     * @param listener the listener to notify on completion or failure
     */
    private void submit(ClaimState state, Consumer<Throwable> listener) {
        String title = Messages.get("patient.insurance.submit.title");
        ClaimValidationStatus status = state.getStatus();
        if (status != null && status.getStatus() == ClaimValidationStatus.Status.ERROR) {
            ErrorHelper.show(title, status.getMessage(), () -> listener.accept(null));
        } else if (status != null && status.getStatus() == ClaimValidationStatus.Status.WARNING) {
            String message = Messages.format("patient.insurance.submit.warning", status.getMessage());
            ConfirmationDialog.newDialog()
                    .title(title)
                    .message(message)
                    .yesNo()
                    .yes(() -> confirmSubmit(state, title, listener))
                    .no(() -> listener.accept(null))
                    .show();
        } else {
            confirmSubmit(state, title, listener);
        }
    }

    /**
     * Checks a claim for duplicate invoices.
     * <p>
     * NOTE that it should not be possible to create claims with duplicates.
     *
     * @param claim the claim
     * @return {@code true} if the claim has no duplicates, {@code false} if it does
     */
    private boolean verifyNoDuplicates(Act claim) {
        boolean result = true;
        ClaimHelper helper = new ClaimHelper(service);
        IMObjectBean claimBean = service.getBean(claim);
        for (Act item : claimBean.getTargets("items", Act.class)) {
            IMObjectBean bean = service.getBean(item);
            for (Act charge : bean.getTargets("items", Act.class)) {
                Act otherClaim = helper.getClaim(charge, claim);
                if (otherClaim != null) {
                    result = false;
                    String error = Messages.format("patient.insurance.duplicatecharge", otherClaim.getId(),
                                                   DateFormatter.formatDate(otherClaim.getActivityStartTime(), false));
                    ErrorHelper.show(Messages.get("patient.insurance.submit.title"), error);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Notify the insurer that a claim has been fully or part paid.
     *
     * @param gapClaim the claim
     * @param listener the listener to notify on completion or failure
     */
    private void notifyInsurerOfPayment(GapClaimImpl gapClaim, Consumer<Throwable> listener) {
        runProtected(listener, () -> {
            Party insurer = gapClaim.getPolicy().getInsurer();
            GapInsuranceService service = (GapInsuranceService) getInsuranceService(insurer);
            service.notifyPayment(gapClaim);
        });
    }

    /**
     * Pay a claim or refund down to the gap amount.
     *
     * @param claim    the claim
     * @param amount   the amount to pay or refund
     * @param payment  if {@code true} collect a payment else make a refund
     * @param paid     the amount already paid
     * @param listener the listener to notify, on completion or failure
     */
    private void payOrRefund(GapClaimImpl claim, BigDecimal amount, boolean payment, BigDecimal paid,
                             Consumer<Throwable> listener) {
        if (MathRules.isZero(amount)) {
            gapClaimPartOrFullyPaid(claim, paid, listener);
        } else {
            Context local = createContext(claim); // make sure the correct customer is used
            PaymentWorkflow workflow = new GapClaimPaymentRefundWorkflow(amount, payment, paid, claim, local, help);
            workflow.addTaskListener(new DefaultTaskListener() {
                @Override
                public void taskEvent(TaskEvent event) {
                    if (event.getTask() == workflow) {
                        // reload the claim, as it may have been updated externally while payment was being made
                        GapClaimImpl reloaded = claim.reload();
                        GapClaim.GapStatus status = reloaded.getGapStatus();
                        if (status == GapClaim.GapStatus.PAID) {
                            notifyInsurerOfPayment(reloaded, listener);
                        } else {
                            listener.accept(null);
                        }
                    }
                }
            });
            workflow.start();
        }
    }

    /**
     * Invoked when a gap claim is part or fully paid.
     * <p/>
     * When part paid, the customer account is adjusted by the benefit amount.
     * The insurer is notified of the payment.
     *
     * @param claim    the gap claim
     * @param paid     the amount paid
     * @param listener the listener to notify, on completion or failure
     */
    private void gapClaimPartOrFullyPaid(GapClaimImpl claim, BigDecimal paid, Consumer<Throwable> listener) {
        if (paid.compareTo(claim.getTotal()) >= 0) {
            // the claim has already been fully paid
            runProtected(listener, false, () -> {
                claim.fullyPaid();
                notifyInsurerOfPayment(claim, listener);
            });
        } else {
            // the gap has already been paid. Need to adjust the customer account by the benefit amount
            gapPaid(claim, listener);
        }
    }

    /**
     * Invoked when the gap has already been paid.
     * <p/>
     * This displays a selection dialog to for the gap benefit till if none is configured.
     *
     * @param claim    the gap claim
     * @param listener the listener to notify on completion or failure
     */
    private void gapPaid(GapClaimImpl claim, Consumer<Throwable> listener) {
        Party gapLocation = claim.getLocationParty();
        LocationRules rules = ServiceHelper.getBean(LocationRules.class);
        Entity gapBenefitTill = rules.getGapBenefitTill(gapLocation);
        if (gapBenefitTill == null) {
            String title = Messages.get("patient.insurance.pay.till.title");
            String message = Messages.get("patient.insurance.pay.till.message");
            ListBox list = new KeyListBox(rules.getTills(gapLocation).toArray());
            list.setStyleName(Styles.DEFAULT);
            list.setHeight(new Extent(10, Extent.EM));
            list.setCellRenderer(IMObjectListCellRenderer.NAME);

            SelectionDialog dialog = new SelectionDialog(title, message, list);
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onAction(String action) {
                    Entity till = (Entity) dialog.getSelected();
                    if (till == null) {
                        // cancelled
                        listener.accept(null);
                    } else {
                        gapPaid(claim, listener, till, gapLocation);
                    }
                }
            });
            dialog.show();
        } else {
            gapPaid(claim, listener, gapBenefitTill, gapLocation);
        }
    }

    /**
     * Invoked when the gap has already been paid.
     *
     * @param claim    the gap claim
     * @param listener the listener to notify on completion or failure
     * @param till     the till
     * @param location the practice location
     */
    private void gapPaid(GapClaimImpl claim, Consumer<Throwable> listener, Entity till, Party location) {
        runProtected(listener, false, () -> {
            claim.gapPaid(till, location);
            notifyInsurerOfPayment(claim, listener);
        });
    }

    /**
     * Emails a claim.
     *
     * @param claim       the claim
     * @param act         the claim act
     * @param attachments the attachments to email
     */
    private void mail(Claim claim, Act act, Attachments attachments) {
        ClaimMailer claimMailer = new ClaimMailer();
        Context claimContext = createContext((ClaimImpl) claim);
        claimMailer.mail(claim, act, attachments.getAttachments(), claimContext, help.subtopic("email"),
                         PopupWindow::show);
    }

    /**
     * Submits an offline claim.
     *
     * @param state    the claim state
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, the argument will be {@code null}
     */
    private void submitOfflineClaim(ClaimState state, Consumer<Throwable> listener) {
        runProtected(listener, false, () -> {
            Claim claim = state.getClaim();
            claim.finalise();
            claim.setStatus(Claim.Status.SUBMITTED);
            print(claim, state.getAct(), listener);
        });
    }

    /**
     * Submits an online claim.
     *
     * @param state    the claim state
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, or was cancelled by the user, the argument will
     *                 be {@code null}
     */
    private void submitOnlineClaim(ClaimState state, Consumer<Throwable> listener) {
        runProtected(listener, false, () -> {
            Claim claim = state.getClaim();
            claim.finalise();
            submitWithDeclaration(claim, state.getService(), listener);
        });
    }

    /**
     * Submits a claim to an {@link InsuranceService}, after accepting a declaration if required.
     *
     * @param claim    the claim to submit
     * @param service  the service to submit to
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, the argument will be {@code null}
     */
    private void submitWithDeclaration(Claim claim, InsuranceService service, Consumer<Throwable> listener) {
        runProtected(listener, false, () -> {
            Declaration declaration = service.getDeclaration(claim);
            if (declaration != null) {
                ConfirmationDialog.newDialog()
                        .title(Messages.get("patient.insurance.declaration.title"))
                        .message(declaration.getText())
                        .button(ACCEPT_ID, () -> submitClaimWithDeclaration(claim, declaration, service, listener))
                        .button(DECLINE_ID, () -> listener.accept(null))
                        .show();
            } else if (claim instanceof GapClaimImpl) {
                submitGapClaim((GapClaimImpl) claim, (GapInsuranceService) service, null, listener);
            } else {
                service.submit(claim, null);
                listener.accept(null);
            }
        });
    }

    /**
     * Submits a claim to an {@link InsuranceService}, after accepting a declaration.
     *
     * @param claim       the claim to submit
     * @param declaration the declaration
     * @param service     the service to submit to
     * @param listener    the listener to notify on completion. If the operation fails, the exception will be passed as
     *                    the argument. If the operation is successful, the argument will be {@code null}
     */
    private void submitClaimWithDeclaration(Claim claim, Declaration declaration, InsuranceService service,
                                            Consumer<Throwable> listener) {
        if (claim instanceof GapClaim) {
            submitGapClaim((GapClaimImpl) claim, (GapInsuranceService) service, declaration, listener);
        } else {
            runProtected(listener, () -> service.submit(claim, declaration));
        }
    }

    /**
     * Submits a gap claim.
     *
     * @param claim       the claim to submit
     * @param service     the service to submit to
     * @param declaration the declaration
     * @param listener    the listener to notify on completion. If the operation fails, the exception will be passed as
     *                    the argument. If the operation is successful, the argument will be {@code null}
     */
    private void submitGapClaim(GapClaimImpl claim, GapInsuranceService service, Declaration declaration,
                                Consumer<Throwable> listener) {
        runProtected(listener, false, () -> {
            service.submit(claim, declaration);
            waitForBenefit(claim, listener);
        });
    }

    /**
     * Displays a dialog prompting to wait for a benefit to be received from the insurer.
     * <p>
     * If a benefit is received, it prompts the user to pay the gap or the full amount.
     *
     * @param claim    the claim
     * @param listener the listener to notify on completion. If the operation fails, the exception will be passed as
     *                 the argument. If the operation is successful, the argument will be {@code null}
     */
    private void waitForBenefit(GapClaimImpl claim, final Consumer<Throwable> listener) {
        BenefitDialog dialog = createBenefitDialog(claim);
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onClose(WindowPaneEvent event) {
                if (BenefitDialog.STOP_WAITING_ID.equals(dialog.getAction())) {
                    listener.accept(null);
                } else {
                    GapClaimImpl currentClaim = dialog.getClaim();
                    Claim.Status status = currentClaim.getStatus();
                    if (status == Claim.Status.ACCEPTED) {
                        boolean payFull = BenefitDialog.PAY_FULL_CLAIM_ID.equals(dialog.getAction());
                        promptToPayClaim(currentClaim, payFull, listener);
                    } else if (status == Claim.Status.CANCELLING) {
                        info(Messages.get("patient.insurance.pay.title"),
                             Messages.get("patient.insurance.pay.cancelling"), listener);
                    } else if (status == Claim.Status.CANCELLED) {
                        info(Messages.get("patient.insurance.pay.title"),
                             Messages.get("patient.insurance.pay.cancelled"), listener);
                    } else if (status == Claim.Status.DECLINED) {
                        info(Messages.get("patient.insurance.pay.title"),
                             Messages.get("patient.insurance.pay.declined"), listener);
                    } else {
                        listener.accept(null);
                    }
                }
            }
        });
        dialog.show();
    }

    /**
     * Prompts the user to pay a gap claim.
     *
     * @param claim        the gap claim
     * @param payFullClaim if {@code true}, pre-select the full amount
     * @param listener     the listener to notify on completion. If the operation fails, the exception will be passed as
     *                     the argument. If the operation is successful, the argument will be {@code null}
     */
    private void promptToPayClaim(GapClaimImpl claim, boolean payFullClaim, Consumer<Throwable> listener) {
        GapPaymentPrompt dialog = new GapPaymentPrompt(claim, payFullClaim, help.subtopic("pay"));
        dialog.addWindowPaneListener(new PopupDialogListener() {
            @Override
            public void onOK() {
                if (dialog.confirmGap()) {
                    gapClaimPartOrFullyPaid(claim, dialog.getPaid(), listener);
                } else if (dialog.refundGap()) {
                    payOrRefund(claim, dialog.getAmountToRefund(), false, dialog.getPaid(), listener);
                } else {
                    payOrRefund(claim, dialog.getAmountToPay(), true, dialog.getPaid(), listener);
                }
            }

            @Override
            public void onCancel() {
                listener.accept(null);
            }
        });
        dialog.show();
    }

    /**
     * Helper to display a information dialog, notifying the listener when it closes.
     *
     * @param title    the dialog title
     * @param message  the message
     * @param listener the listener to notify
     */
    private void info(String title, String message, Consumer<Throwable> listener) {
        InformationDialog.newDialog()
                .title(title)
                .message(message)
                .listener(() -> listener.accept(null))
                .show();
    }

    /**
     * Executes a {@code Runnable}.
     *
     * @param listener the listener to notify
     * @param runnable the {@code Runnable} to execute
     */
    private void runProtected(Consumer<Throwable> listener, Runnable runnable) {
        runProtected(listener, true, runnable);
    }

    /**
     * Executes a {@code Runnable}.
     *
     * @param listener        the listener to notify
     * @param notifyOnSuccess if {@code true}, notify the listener on success, otherwise only notify on failure
     * @param runnable        the {@code Runnable} to execute
     */
    private void runProtected(Consumer<Throwable> listener, boolean notifyOnSuccess, Runnable runnable) {
        try {
            runnable.run();
            if (notifyOnSuccess) {
                listener.accept(null);
            }
        } catch (Throwable exception) {
            listener.accept(exception);
        }
    }

    /**
     * Returns the insurance service for an insurer.
     *
     * @param insurer the insurer
     * @return the insurance service
     */
    private InsuranceService getInsuranceService(Party insurer) {
        return insuranceServices.getService(insurer);
    }

    /**
     * Creates a context for the claim.
     *
     * @param claim the claim
     * @return the context
     */
    private Context createContext(ClaimImpl claim) {
        Context local = new LocalContext(context);
        Party insurer = claim.getPolicy().getInsurer();
        local.setCustomer(claim.getCustomer());
        local.setPatient(claim.getPatient());
        local.setSupplier(insurer);
        return local;
    }

    /**
     * Collects attachments for emailing a claim.
     */
    private class Attachments {

        /**
         * The attachments.
         */
        private final List<IMObject> objects;

        /**
         * The no. of attachments with missing documents.
         */
        private int missing;

        /**
         * Constructs an {@link Attachments}.
         *
         * @param claim the claim
         */
        public Attachments(Act claim) {
            IMObjectBean bean = service.getBean(claim);
            objects = new ArrayList<>();
            objects.add(claim);
            missing = 0;
            for (DocumentAct attachment : bean.getTargets("attachments", DocumentAct.class)) {
                if (attachment.getDocument() != null) {
                    objects.add(attachment);
                } else {
                    missing++;
                }
            }
        }

        /**
         * Returns the attachments.
         *
         * @return the attachments
         */
        public List<IMObject> getAttachments() {
            return objects;
        }

        /**
         * Determines if there are claim attachments with missing documents.
         *
         * @return {@code true} if one or more attachments have missing documents, otherwise {@code false}
         */
        public boolean missingDocuments() {
            return missing != 0;
        }

        /**
         * Returns the number of claim attachments with missing documents.
         *
         * @return the number of attachments with missing documents
         */
        public int getMissing() {
            return missing;
        }
    }

    private static class ClaimState {

        private final Act act;

        private final Claim claim;

        private final ClaimValidationStatus status;

        private final InsuranceService service;

        ClaimState(Act act, Claim claim, ClaimValidationStatus status, InsuranceService service) {
            this.act = act;
            this.claim = claim;
            this.status = status;
            this.service = service;
        }

        public Act getAct() {
            return act;
        }

        public Claim getClaim() {
            return claim;
        }

        public ClaimValidationStatus getStatus() {
            return status;
        }

        public InsuranceService getService() {
            return service;
        }
    }
}

