/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.mr;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeRules;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductRules;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.act.ParticipationEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.patient.PatientActEditor;
import org.openvpms.web.component.im.product.ProductParticipationEditor;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.component.im.edit.Quantity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * An editor for <em>act.patientPrescription</em> acts.
 *
 * @author Tim Anderson
 */
public class PatientPrescriptionEditor extends PatientActEditor {

    /**
     * The quantity.
     */
    private final Quantity quantity;

    /**
     * The dispensing notes.
     */
    private final DispensingNotes dispensingNotes;

    /**
     * The dose manager.
     */
    private final DoseManager doseManager;

    /**
     * The practice rules.
     */
    private final PracticeRules rules;

    /**
     * The patient node.
     */
    private static final String PATIENT = "patient";

    /**
     * The product node.
     */
    private static final String PRODUCT = "product";

    /**
     * Quantity node.
     */
    private static final String QUANTITY = "quantity";

    /**
     * Label node.
     */
    private static final String LABEL = "label";

    /**
     * Medication dispensing instructions node.
     */
    private static final String DISP_INSTRUCTIONS = "dispInstructions";


    /**
     * Constructs a {@link PatientPrescriptionEditor}.
     *
     * @param act     the act to edit
     * @param parent  the parent object. May be {@code null}
     * @param context the layout context
     */
    public PatientPrescriptionEditor(Act act, Act parent, LayoutContext context) {
        super(act, parent, context);
        quantity = new Quantity(getProperty(QUANTITY), act, getLayoutContext());
        rules = ServiceHelper.getBean(PracticeRules.class);

        dispensingNotes = new DispensingNotes();
        if (act.isNew()) {
            calculateEndTime();
        }
        addStartEndTimeListeners(); // startTime is read-only so only the end time listener will trigger
        dispensingNotes.setProduct((Product) getParticipant(PRODUCT));
        doseManager = new DoseManager(ServiceHelper.getBean(PatientRules.class),
                                      ServiceHelper.getBean(ProductRules.class),
                                      ServiceHelper.getArchetypeService());
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        return new PatientPrescriptionEditor(reload(getObject()), (Act) getParent(), getLayoutContext());
    }

    /**
     * Returns the product.
     *
     * @return the product. May be {@code null}
     */
    public Product getProduct() {
        return (Product) getParticipant(PRODUCT);
    }

    /**
     * Sets the product.
     *
     * @param product the product. May be {@code null}
     */
    public void setProduct(Product product) {
        setParticipant(PRODUCT, product);
    }

    /**
     * Returns the quantity.
     *
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return getProperty(QUANTITY).getBigDecimal();
    }

    /**
     * Returns the label.
     *
     * @return the label. May be {@code null}
     */
    public String getLabel() {
        return getProperty(LABEL).getString();
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new LayoutStrategy();
    }

    /**
     * Invoked when layout has completed.
     * <p>
     * This registers a listener to be notified of product changes.
     */
    @Override
    protected void onLayoutCompleted() {
        monitorParticipation(PATIENT, entity -> updateQuantity());
        ProductParticipationEditor productEditor = getProductEditor();
        if (productEditor != null) {
            productEditor.setPatient(getPatient());
            productEditor.addModifiableListener(modifiable -> onProductChanged());
        }
    }

    /**
     * Invoked when the end time changes. Recalculates the end time if it is less than the start time.
     */
    @Override
    protected void onEndTimeChanged() {
        Date start = getStartTime();
        Date end = getEndTime();
        if (start != null && end != null) {
            if (end.compareTo(start) < 0) {
                calculateEndTime();
            }
        }
    }

    /**
     * Returns the product editor.
     *
     * @return the product editor
     */
    private ProductParticipationEditor getProductEditor() {
        return (ProductParticipationEditor) (ParticipationEditor<?>) getParticipationEditor(PRODUCT, true);
    }

    /**
     * Calculates the end time if the start time  and practice is set.
     *
     * @throws OpenVPMSException for any error
     */
    private void calculateEndTime() {
        Date start = getStartTime();
        Party practice = getLayoutContext().getContext().getPractice();
        if (start != null && practice != null) {
            setEndTime(rules.getPrescriptionExpiryDate(start, practice));
        }
    }

    /**
     * Invoked when the product changes. This updates the label with the product's dispensing instructions,
     * and determines the quantity.
     */
    private void onProductChanged() {
        Product product = getProduct();
        if (TypeHelper.isA(product, ProductArchetypes.MEDICATION)) {
            IMObjectBean bean = getBean(product);
            Property label = getProperty(LABEL);
            label.setValue(bean.getValue(DISP_INSTRUCTIONS));
        }
        dispensingNotes.setProduct(product);
        updateQuantity();
    }

    /**
     * Updates the quantity. If the product has a dose, this is used, otherwise the quantity is
     * set to {@code 1}.
     */
    private void updateQuantity() {
        Party patient = getPatient();
        Product product = getProduct();
        if (patient != null && product != null) {
            BigDecimal dose = doseManager.getDose(product, patient);
            if (!MathRules.isZero(dose)) {
                quantity.setValue(dose, true);
            } else {
                quantity.setValue(BigDecimal.ONE, false);
            }
        }
    }

    /**
     * Layout strategy to render the dispensing notes across both columns, if present.
     */
    private class LayoutStrategy extends PrescriptionLayoutStrategy {
        @Override
        public ComponentState apply(IMObject object, PropertySet properties, IMObject parent,
                                    LayoutContext context) {
            addComponent(quantity.getState());
            return super.apply(object, properties, parent, context);
        }

        /**
         * Lays out components in a grid.
         *
         * @param object     the object to lay out
         * @param properties the properties
         * @param context    the layout context
         * @param columns    the no. of columns to use
         */
        @Override
        protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context,
                                           int columns) {
            ComponentGrid grid = super.createGrid(object, properties, context, columns);
            ComponentState usage = dispensingNotes.getComponent(context);
            Component label = ColumnFactory.create(usage.getLabel());
            Component text = ColumnFactory.create(usage.getComponent());
            text.setLayoutData(ComponentGrid.layout(1, columns * 2 - 1));
            grid.add(label, text);
            return grid;
        }
    }
}
