/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.webcontainer.command.BrowserOpenWindowCommand;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.ExternalResults;
import org.openvpms.laboratory.report.View;
import org.openvpms.laboratory.report.WebView;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Renders a button to launch external results in a new window.
 *
 * @author Tim Anderson
 */
public class ExternalResultsViewer {

    /**
     * The investigation identifier.
     */
    private final long investigationId;

    /**
     * The viewer component.
     */
    private final Component component;

    /**
     * Constructs an {@link ExternalResultsViewer}.
     *
     * @param act the investigation
     */
    public ExternalResultsViewer(DocumentAct act) {
        this.investigationId = act.getId();
        Button button = ButtonFactory.create(null, "externallink", this::onView);
        button.setToolTipText(Messages.get("investigation.externalResults"));
        component = button;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        return component;
    }

    /**
     * View external results, if the laboratory service supports it.
     *
     * @param order the order
     */
    protected void view(Order order) {
        LaboratoryService service = InvestigationHelper.getLaboratoryService(order);
        ExternalResults results = service.getExternalResults(order);
        if (results != null && results.canView()) {
            View view = results.getView();
            if (view instanceof WebView) {
                ApplicationInstance.getActive().enqueueCommand(
                        new BrowserOpenWindowCommand(((WebView) view).getUrl(), "", ""));
            }
        }
    }

    /**
     * Invoked to view external results.
     */
    private void onView() {
        Order order = InvestigationHelper.getOrder(investigationId);
        if (order != null) {
            view(order);
        }
    }
}
