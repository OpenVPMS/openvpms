/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template.letterhead;

import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.LogoEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.workspace.admin.organisation.AbstractOrganisationEditor;
import org.openvpms.web.workspace.admin.template.LetterheadLayoutStrategy;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Editor for <em>entity.letterhead</em>.
 *
 * @author Tim Anderson
 */
public class LetterheadEditor extends AbstractOrganisationEditor {

    /**
     * Constructs a {@link LetterheadEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public LetterheadEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
    }

    /**
     * Sets the logo.
     *
     * @param document the logo
     */
    public void setLogo(Document document) {
        getLogoEditor().setImage(document);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance
     */
    @Override
    public IMObjectEditor newInstance() {
        getLogoEditor().cancel();
        return new LetterheadEditor((Entity) reload(getObject()), getParent(), getLayoutContext());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateLogo(validator);
    }

    /**
     * Verifies that one of logoFile or logo are present.
     *
     * @param validator the validator
     * @return {@code true} if a logo is present
     */
    protected boolean validateLogo(Validator validator) {
        boolean result = true;
        Property logoFile = getProperty("logoFile");
        String logo = logoFile.getString();
        if (logo != null) {
            Path path = Paths.get(logo);
            if (!Files.isRegularFile(path) || !Files.isReadable(path)) {
                validator.add(logoFile, new ValidatorError(logoFile, Messages.format("file.notfound", logo)));
                result = false;
            }
        } else if (!getLogoEditor().hasImage()) {
            validator.add(logoFile, new ValidatorError(logoFile, Messages.get("admin.template.letterhead.nologo")));
            result = false;
        }
        return result;
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new Layout();
    }

    private class Layout extends LetterheadLayoutStrategy {

        /**
         * Returns a component to display the logo.
         *
         * @param object  the letterhead
         * @param context the layout context
         * @return the logo component
         */
        @Override
        protected ComponentState getLogo(IMObject object, LayoutContext context) {
            LogoEditor logoEditor = getLogoEditor();
            return new ComponentState(logoEditor.getComponent(), null, logoEditor.getFocusGroup(),
                                      Messages.get("admin.practice.logo"));
        }
    }
}