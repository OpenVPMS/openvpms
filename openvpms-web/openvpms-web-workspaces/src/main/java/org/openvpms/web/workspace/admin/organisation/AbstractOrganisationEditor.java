/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.LogoService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.doc.LogoEditor;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.system.ServiceHelper;

/**
 * Editor for organisation entities that support logos.
 *
 * @author Tim Anderson
 */
public class AbstractOrganisationEditor extends AbstractIMObjectEditor {

    /**
     * The logo editor.
     */
    private final LogoEditor logoEditor;

    /**
     * Constructs an {@link AbstractOrganisationEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AbstractOrganisationEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);

        DocumentAct logo = getLogo(object);
        logoEditor = new LogoEditor(logo, object, layoutContext);
        addEditor(logoEditor);
    }

    /**
     * Returns the logo editor.
     *
     * @return the logo editor
     */
    protected LogoEditor getLogoEditor() {
        return logoEditor;
    }

    /**
     * Returns the logo act.
     *
     * @param object the organisation
     * @return the logo act
     */
    private DocumentAct getLogo(Entity object) {
        DocumentAct result = ServiceHelper.getBean(LogoService.class).getLogo(object);
        if (result == null) {
            result = (DocumentAct) IMObjectCreator.create(DocumentArchetypes.LOGO_ACT);
        }
        return result;
    }
}
