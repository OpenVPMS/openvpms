/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.plugin;

import echopointng.TabbedPane;
import nextapp.echo2.app.Component;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.edit.Saveable;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.TabbedPaneFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.tabpane.TabPaneModel;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.mapping.MappingEditors;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

/**
 * Dialog to edit a list of {@link Mappings}.
 *
 * @author Tim Anderson
 */
public class MappingEditDialog extends ModalDialog {

    /**
     * The mapping editors.
     */
    private final Editors mappingEditors = new Editors();

    /**
     * Constructs a {@link MappingEditDialog}.
     *
     * @param mappings the mappings to edit
     * @param selected the selected mapping
     * @param context  the layout context
     */
    public MappingEditDialog(List<Mappings<?>> mappings, int selected, LayoutContext context) {
        super(Messages.get("mapping.edit.title"), APPLY_OK_CANCEL, context.getHelpContext());
        Component mappingContainer = ColumnFactory.create(Styles.INSET_Y);
        TabPaneModel model = new TabPaneModel(mappingContainer);
        for (Mappings<?> m : mappings) {
            MappingEditors<?> editors = new MappingEditors<>(m, context);
            mappingEditors.add(editors);
            model.addTab(m.getDisplayName(), editors.getComponent());
        }
        TabbedPane pane = TabbedPaneFactory.create(model);
        if (selected >= 0 && selected < mappings.size()) {
            pane.setSelectedIndex(selected);
        }

        getLayout().add(ColumnFactory.create(Styles.INSET_Y, pane));
        resize("MappingEditDialog.size");
    }

    /**
     * Saves the mappings.
     */
    @Override
    protected void onApply() {
        save();
    }

    /**
     * Invoked when the 'OK' button is pressed. This sets the action and closes
     * the window.
     */
    @Override
    protected void onOK() {
        if (save()) {
            super.onOK();
        }
    }

    /**
     * Saves the mappings.
     *
     * @return {@code true} if the mappings were saved successfully, otherwise {@code false}
     */
    private boolean save() {
        boolean result = false;
        try {
            Validator validator = new DefaultValidator();
            if (mappingEditors.validate(validator)) {
                TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
                template.execute(new TransactionCallbackWithoutResult() {
                    @Override
                    protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                        for (Saveable saveable : mappingEditors.getModifiedSaveable()) {
                            saveable.save();
                        }
                    }
                });
                result = true;
            } else {
                ValidationHelper.showError(validator);
            }
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
        return result;
    }
}