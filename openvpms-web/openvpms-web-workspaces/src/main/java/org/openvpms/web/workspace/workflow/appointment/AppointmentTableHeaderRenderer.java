/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import echopointng.layout.TableLayoutDataEx;
import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.Row;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.layout.RowLayoutData;
import nextapp.echo2.app.table.TableCellRenderer;
import org.openvpms.archetype.rules.workflow.roster.RosterEvent;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.GridFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.table.TableHelper;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.openvpms.web.echo.style.Styles.BOLD;
import static org.openvpms.web.echo.style.Styles.CELL_SPACING;


/**
 * Header cell renderer for the {@link AppointmentTableModel}.
 *
 * @author Tim Anderson
 */
class AppointmentTableHeaderRenderer implements TableCellRenderer {

    /**
     * The singleton instance.
     */
    public static AppointmentTableHeaderRenderer INSTANCE = new AppointmentTableHeaderRenderer();


    /**
     * Default constructor.
     */
    private AppointmentTableHeaderRenderer() {
    }

    @Override
    public Component getTableCellRendererComponent(Table table, Object value, int column, int row) {
        Component component;
        AppointmentTableModel model = (AppointmentTableModel) table.getModel();
        boolean singleScheduleView = model.isSingleScheduleView();

        Schedule schedule = model.getSchedule(column, row);
        if (!singleScheduleView && schedule != null && !schedule.getAreas().isEmpty()) {
            component = getRosterHeading(schedule, model);
        } else {
            component = createHeading(model.getColumnName(column));
            component.setLayoutData(getLayoutData());
        }
        if (!singleScheduleView && schedule != null) {
            Entity entity = schedule.getSchedule();
            ++column;
            int span = 1;
            while (column < model.getColumnCount()) {
                if (!Objects.equals(entity, model.getScheduleEntity(column, row))) {
                    break;
                }
                ++column;
                ++span;
            }
            if (span > 1) {
                TableLayoutDataEx layout = (TableLayoutDataEx) component.getLayoutData();
                if (layout == null) {
                    layout = getLayoutData();
                    component.setLayoutData(layout);
                }
                layout.setColSpan(span);
            }
        }
        return component;
    }

    /**
     * Returns a component to display a schedule heading with roster information.
     *
     * @param schedule the schedule
     * @param model    the table model
     * @return the schedule heading with roster information
     */
    private Component getRosterHeading(Schedule schedule, AppointmentTableModel model) {
        Component result;
        if (model.showRoster()) {
            RosterService service = ServiceHelper.getBean(RosterService.class);
            List<PropertySet> roster = new ArrayList<>();
            for (Entity area : schedule.getAreas()) {
                List<PropertySet> events = service.getEvents(area, model.getGrid().getStartDate());
                roster.addAll(events);
            }
            Grid grid = GridFactory.create(2);
            if (!roster.isEmpty()) {
                for (PropertySet event : roster) {
                    String name = event.getString(RosterEvent.USER_NAME);
                    Date from = event.getDate(RosterEvent.ACT_START_TIME);
                    Date to = event.getDate(RosterEvent.ACT_END_TIME);
                    grid.add(LabelFactory.text(name));
                    grid.add(LabelFactory.text(DateFormatter.formatTime(from, false)
                                               + " - " + DateFormatter.formatTime(to, false)));
                }
            } else {
                grid.add(LabelFactory.create("workflow.scheduling.appointment.noroster"));
            }
            Row row = getRosterHeading(schedule, model, false);
            result = ColumnFactory.create(row, grid);
        } else {
            result = getRosterHeading(schedule, model, true);
        }
        result.setLayoutData(getLayoutData());
        return result;
    }

    private Row getRosterHeading(Schedule schedule, AppointmentTableModel model, boolean show) {
        String style = (show) ? "Roster.show" : "Roster.hide";
        Button button = ButtonFactory.create(null, style, () -> model.setShowRoster(show));
        Label heading = createHeading(schedule);
        RowLayoutData layout = RowFactory.layout(Alignment.ALIGN_TOP);
        heading.setLayoutData(layout);
        button.setLayoutData(layout);
        return RowFactory.create(CELL_SPACING, heading, button);
    }

    private Label createHeading(Schedule schedule) {
        return createHeading(schedule.getName());
    }

    private Label createHeading(String name) {
        return LabelFactory.text(name, BOLD);
    }

    private TableLayoutDataEx getLayoutData() {
        TableLayoutDataEx result = TableHelper.getTableLayoutDataEx("Table.Header.Layout");
        if (result == null) {
            result = new TableLayoutDataEx();
        }
        result.setAlignment(Alignment.ALIGN_TOP);
        return result;
    }

}
