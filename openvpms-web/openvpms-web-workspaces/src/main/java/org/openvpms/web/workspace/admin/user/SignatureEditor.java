/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.user;

import org.openvpms.archetype.rules.user.UserArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.doc.AbstractImageParticipationEditor;
import org.openvpms.web.component.im.layout.LayoutContext;

/**
 * User signature editor.
 *
 * @author Tim Anderson
 */
public class SignatureEditor extends AbstractImageParticipationEditor {

    /**
     * Constructs a {@link SignatureEditor}.
     *
     * @param act     the logo act
     * @param parent  the parent user
     * @param context the layout context
     */
    public SignatureEditor(DocumentAct act, User parent, LayoutContext context) {
        super(act, parent, UserArchetypes.SIGNATURE, "user", context);
    }
}