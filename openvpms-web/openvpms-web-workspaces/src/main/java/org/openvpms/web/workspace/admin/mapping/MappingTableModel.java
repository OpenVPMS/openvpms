/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.echo.table.TableColumnFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Table model for {@link Mapping} instances.
 *
 * @author Tim Anderson
 */
public class MappingTableModel<T extends IMObject> extends AbstractIMTableModel<Mapping> {

    /**
     * Source column index.
     */
    protected static final int SOURCE_INDEX = 0;

    /**
     * Target column index.
     */
    protected static final int TARGET_INDEX = 1;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MappingTableModel.class);

    /**
     * Constructs a {@link MappingTableModel}.
     *
     * @param mappings the mappings
     */
    public MappingTableModel(Mappings<T> mappings) {
        DefaultTableColumnModel model = new DefaultTableColumnModel();
        model.addColumn(TableColumnFactory.create(SOURCE_INDEX, mappings.getSourceDisplayName()));
        model.addColumn(TableColumnFactory.create(TARGET_INDEX, mappings.getTargetDisplayName()));
        setTableColumnModel(model);
    }

    /**
     * Returns the sort criteria.
     *
     * @param column    the primary sort column
     * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
     * @return the sort criteria, or {@code null} if the column isn't sortable
     */
    @Override
    public SortConstraint[] getSortConstraints(int column, boolean ascending) {
        return null;
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the column
     * @param row    the row
     * @return the value at the given coordinate.
     */
    @Override
    protected Object getValue(Mapping object, TableColumn column, int row) {
        if (column.getModelIndex() == SOURCE_INDEX) {
            return IMObjectHelper.getName(object.getSource());
        }
        return getTargetName(object);
    }

    /**
     * Returns the name of a target mapping.
     *
     * @param object the mapping
     * @return the target name. May be {@code null}
     */
    private String getTargetName(Mapping object) {
        String result = null;
        try {
            Target target = object.getTarget();
            if (target != null) {
                result = target.getName();
            }
        } catch (Throwable exception) {
            log.debug("Failed to target target name: " + exception.getMessage(), exception);
        }
        return result;
    }

}
