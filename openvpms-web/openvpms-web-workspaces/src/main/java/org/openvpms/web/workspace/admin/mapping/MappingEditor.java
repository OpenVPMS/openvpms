/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Source;
import org.openvpms.mapping.model.Target;
import org.openvpms.web.component.edit.Editors;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.Modifiable;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Objects;

/**
 * An editor for a {@link Mapping}.
 *
 * @author Tim Anderson
 */
class MappingEditor<T extends IMObject> extends Editors {

    /**
     * The mapping state.
     */
    private final MappingState mapping;

    /**
     * The available mappings.
     */
    private final Mappings<T> mappings;

    /**
     * The source.
     */
    private final MappingSource source;

    /**
     * The target editor.
     */
    private final TargetEditor target;

    /**
     * Constructs a {@link MappingEditor}.
     *
     * @param mapping  the mapping to edit
     * @param mappings available mappings
     * @param context  the layout context
     */
    MappingEditor(MappingState mapping, Mappings<T> mappings, LayoutContext context) {
        this.mapping = mapping;
        this.mappings = mappings;
//        if (mappings.getCardinality() == Cardinality.ONE_TO_ONE) {
//            SourceEditor editor = new SourceEditor(mapping.getSource(), mappings, context);
//            add(editor);
//            source = editor;
//        } else {
        // source cannot be edited
        source = new ReadOnlySource(mapping);
//        }
        target = new TargetEditor(mapping.getTarget(), mappings, context);
        add(target);
    }

    /**
     * Returns the source.
     *
     * @return the source
     */
    public MappingSource getSource() {
        return source;
    }

    /**
     * Returns the target editor.
     *
     * @return the target editor
     */
    public TargetEditor getTarget() {
        return target;
    }

    /**
     * Invoked when a {@link Modifiable} changes. Resets the cached valid status and forwards the event to any
     * registered listener.
     *
     * @param modified the changed instance
     */
    @Override
    protected void onModified(Modifiable modified) {
        Source newSource = source.getObject();
        Target newTarget = target.getObject();
        if (newSource != null && newTarget != null) {
            if (!Objects.equals(newSource.getId(), mapping.getSource())
                || !Objects.equals(newTarget, mapping.getTarget())) {
                Class<T> type = mappings.getType();
                T object = IMObjectHelper.getObject(newSource.getId(), type);
                if (mapping.getMapping() == null) {
                    Mapping newMapping = mappings.add(object, newTarget);
                    mapping.setMapping(newMapping);
                } else {
                    mappings.replace(mapping.getMapping(), object, newTarget);
                }
            }
        } else if (mapping.getMapping() != null) {
            Mapping existing = mapping.getMapping();
            mappings.remove(existing);
            mapping.setMapping(null);
        }
        super.onModified(modified);
    }

    private static class ReadOnlySource implements MappingSource {

        private final Label label;

        private final Source source;

        ReadOnlySource(MappingState state) {
            String name = IMObjectHelper.getName(state.getSource());
            if (name == null) {
                name = Messages.get("imobject.none");
            }
            label = LabelFactory.text(name);
            source = new SourceImpl(state.getSource(), name);
        }

        @Override
        public Source getObject() {
            return source;
        }

        @Override
        public Component getComponent() {
            return label;
        }
    }
}
