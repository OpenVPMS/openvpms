/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.payment;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.layout.PrintObjectLayoutStrategy;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.component.im.report.DocumentTemplateLocator;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.DocumentBackedTextProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.style.Styles;

import java.util.List;

/**
 * Layout strategy for <em>act.EFTPOSReceipt*</em>.
 *
 * @author Tim Anderson
 */
public class EFTPOSReceiptLayoutStrategy extends PrintObjectLayoutStrategy {

    /**
     * The receipt node name.
     */
    private static final String RECEIPT = "receipt";

    /**
     * The document node name.
     */
    private static final String DOCUMENT = "document";

    /**
     * Constructs an {@link EFTPOSReceiptLayoutStrategy}.
     */
    public EFTPOSReceiptLayoutStrategy() {
        super("button.print");
    }

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        Property text = new DocumentBackedTextProperty((DocumentAct) object, properties.get(RECEIPT));
        Label receipt = LabelFactory.preformatted(text.getString());
        receipt.setStyleName(Styles.MONOSPACE);
        addComponent(new ComponentState(receipt, text));
        return super.apply(object, properties, parent, context);
    }

    /**
     * Lays out components in a grid.
     *
     * @param object     the object to lay out
     * @param properties the properties
     * @param context    the layout context
     */
    @Override
    protected ComponentGrid createGrid(IMObject object, List<Property> properties, LayoutContext context) {
        ComponentGrid grid = new ComponentGrid();
        grid.add(getComponent(RECEIPT).getComponent());
        return grid;
    }

    /**
     * Creates the print button.
     *
     * @param buttonId the button identifier
     * @return the print button
     */
    @Override
    protected Button createButton(String buttonId) {
        return ButtonFactory.create(buttonId, false);  // disable shortcuts
    }

    /**
     * Creates a document template locator.
     *
     * @param object  the object to print
     * @param context the context
     * @return a document template locator
     */
    @Override
    protected DocumentTemplateLocator createDocumentTemplateLocator(IMObject object, Context context) {
        return new ContextDocumentTemplateLocator(EFTPOSArchetypes.CUSTOMER_RECEIPT, context);
    }
}
