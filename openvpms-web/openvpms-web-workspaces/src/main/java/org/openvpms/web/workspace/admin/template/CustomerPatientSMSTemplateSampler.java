/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.template;

import nextapp.echo2.app.Label;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.customer.CustomerReferenceEditor;
import org.openvpms.web.component.im.edit.PatientReferenceEditor;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.archetype.rules.workflow.ScheduleArchetypes.APPOINTMENT;

/**
 * A component to test the expression evaluation of SMS templates that use customers and patients.
 *
 * @author Tim Anderson
 */
public abstract class CustomerPatientSMSTemplateSampler extends SMSTemplateSampler {

    /**
     * The customer to test against.
     */
    private final SimpleProperty customer;

    /**
     * The patient to test against.
     */
    private final SimpleProperty patient;

    /**
     * Constructs an {@link CustomerPatientSMSTemplateSampler}.
     *
     * @param template      the template
     * @param layoutContext the layout context
     */
    public CustomerPatientSMSTemplateSampler(Entity template, LayoutContext layoutContext) {
        super(template, layoutContext);
        ArchetypeService service = ServiceHelper.getArchetypeService();
        customer = new SimpleProperty("customer", null, IMObjectReference.class,
                                      DescriptorHelper.getDisplayName(APPOINTMENT, "customer", service));
        patient = new SimpleProperty("patient", null, IMObjectReference.class,
                                     DescriptorHelper.getDisplayName(APPOINTMENT, "patient", service));

        customer.setArchetypeRange(new String[]{CustomerArchetypes.PERSON});
        patient.setArchetypeRange(new String[]{PatientArchetypes.PATIENT});
        ModifiableListener listener = modifiable -> evaluate();
        customer.addModifiableListener(listener);
        patient.addModifiableListener(listener);
    }

    /**
     * Lays out the editable fields in a grid.
     *
     * @param grid    the grid
     * @param group   the focus group
     * @param context the layout context
     */
    @Override
    protected void layoutFields(ComponentGrid grid, FocusGroup group, LayoutContext context) {
        CustomerReferenceEditor customerSelector = new CustomerReferenceEditor(customer, null, context);
        PatientReferenceEditor patientSelector = new PatientReferenceEditor(patient, null, context);
        Label customerLabel = LabelFactory.text(customer.getDisplayName());

        Label patientLabel = LabelFactory.text(patient.getDisplayName());

        grid.add(customerLabel, customerSelector.getComponent(), patientLabel, patientSelector.getComponent());
        group.add(customerSelector.getFocusGroup());
        group.add(patientSelector.getFocusGroup());
    }

    /**
     * Returns the selected customer.
     *
     * @return the customer. May be {@code null}
     */
    protected Reference getCustomer() {
        return customer.getReference();
    }

    /**
     * Returns the selected patient.
     *
     * @return the patient. May be {@code null}
     */
    protected Reference getPatient() {
        return patient.getReference();
    }

}