/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.firewall;

import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.archetype.rules.security.FirewallEntry;
import org.openvpms.archetype.rules.security.FirewallSettings;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;

/**
 * Layout strategy for <em>entity.globalSettingsFirewall</em>.
 *
 * @author Tim Anderson
 */
public class FirewallLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The first allowed address node.
     */
    public static final String FIRST_ALLOWED = "allowed0";

    /**
     * Apply the layout strategy.
     * <p>
     * This renders an object in a {@code Component}, using a factory to create the child components.
     *
     * @param object     the object to apply
     * @param properties the object's properties
     * @param parent     the parent object. May be {@code null}
     * @param context    the layout context
     * @return the component containing the rendered {@code object}
     */
    @Override
    public ComponentState apply(IMObject object, PropertySet properties, IMObject parent, LayoutContext context) {
        ArchetypeNodes nodes = new ArchetypeNodes();

        // allowed addresses are encoded onto the allowed0..allowedn nodes. Exclude
        // all but the first from display, and map the first to the allowedAddresses component
        for (Property property : properties.getProperties()) {
            if (property.getName().startsWith("allowed") && !property.getName().equals(FIRST_ALLOWED)) {
                nodes.exclude(property.getName());
            }
        }
        nodes.complex(FIRST_ALLOWED);
        setArchetypeNodes(nodes);
        if (getComponent(FIRST_ALLOWED) == null) {
            // if there is no pre-registered component to render addresses, render a table
            FirewallSettings settings = new FirewallSettings((Entity) object, getService());
            PagedIMTable<FirewallEntry> table = new PagedIMTable<>(new AddressTableModel());
            table.setResultSet(new ListResultSet<>(settings.getAllowedAddresses(), 10));
            addComponent(new ComponentState(table.getComponent(), properties.get(FIRST_ALLOWED)));
        }
        return super.apply(object, properties, parent, context);
    }

    private static class AddressTableModel extends AbstractIMTableModel<FirewallEntry> {

        private static final int ADDRESS_INDEX = 0;

        private static final int ACTIVE_INDEX = 1;

        private static final int DESCRIPTION_INDEX = 2;

        private static final int RANGE_INDEX = 3;

        public AddressTableModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(createTableColumn(ADDRESS_INDEX, "admin.system.firewall.address"));
            model.addColumn(createTableColumn(ACTIVE_INDEX, ACTIVE));
            model.addColumn(createTableColumn(DESCRIPTION_INDEX, DESCRIPTION));
            model.addColumn(createTableColumn(RANGE_INDEX, "admin.system.firewall.range"));
            setTableColumnModel(model);
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return new SortConstraint[0];
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(FirewallEntry object, TableColumn column, int row) {
            Object result = null;
            switch (column.getModelIndex()) {
                case ADDRESS_INDEX:
                    result = object.getAddress();
                    break;
                case ACTIVE_INDEX:
                    result = getCheckBox(object.isActive());
                    break;
                case DESCRIPTION_INDEX:
                    result = object.getDescription();
                    break;
                case RANGE_INDEX:
                    result = FirewallEntryEditor.getRange(object.getAddress());
                    break;
            }
            return result;
        }
    }

}
