/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.wip;

import org.openvpms.archetype.rules.act.FinancialActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.QueryBrowser;
import org.openvpms.web.component.workflow.DefaultTaskListener;
import org.openvpms.web.component.workflow.TaskEvent;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.customer.account.AccountActActions;
import org.openvpms.web.workspace.customer.charge.ChargePoster;
import org.openvpms.web.workspace.customer.charge.PostedChargeWorkflow;
import org.openvpms.web.workspace.reporting.charge.AbstractChargesCRUDWindow;

/**
 * Incomplete charges CRUD window.
 *
 * @author Tim Anderson
 */
public class IncompleteChargesCRUDWindow extends AbstractChargesCRUDWindow {

    /**
     * Post button identifier.
     */
    private static final String POST_ID = "button.post";

    /**
     * Post help topic.
     */
    private static final String POST_TOPIC = "customer/charge/post";

    /**
     * Constructs an {@link IncompleteChargesCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param browser    the query browser
     * @param context    the context
     * @param help       the help context
     */
    public IncompleteChargesCRUDWindow(Archetypes<FinancialAct> archetypes, QueryBrowser<FinancialAct> browser,
                                       Context context, HelpContext help) {
        super(archetypes, browser, "WORK_IN_PROGRESS_CHARGES", context, help);

        CustomerAccountRules rules = ServiceHelper.getBean(CustomerAccountRules.class);
        AccountReminderRules reminderRules = ServiceHelper.getBean(AccountReminderRules.class);
        PracticeService practiceService = ServiceHelper.getBean(PracticeService.class);
        setActions(new AccountActActions(rules, reminderRules, practiceService));
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button set
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(POST_ID, action(this::onPost, true, null));
        buttons.add(createDeleteButton());
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(POST_ID, enable);
        buttons.setEnabled(DELETE_ID, enable);
    }

    /**
     * Invoked when the 'post' button is pressed.
     *
     * @param object the latest instance of the selected object
     */
    protected void onPost(FinancialAct object) {
        AccountActActions actions = getActions();
        if (actions.canPost(object)) {
            HelpContext help = getHelpContext().topic(POST_TOPIC);
            ChargePoster poster = new ChargePoster(object, actions, createLayoutContext(help));
            poster.post(charge -> {
                if (FinancialActStatus.POSTED.equals(charge.getStatus())) {
                    onPosted(charge);
                } else {
                    onRefresh(charge);
                }
            });
        } else {
            onRefresh(object);
        }
    }

    /**
     * Invoked when a charge is posted.
     * <p/>
     * This prompts to pay the account, and pops up a dialog to print the act.
     *
     * @param charge the charge
     */
    protected void onPosted(FinancialAct charge) {
        HelpContext help = getHelpContext().topic(POST_TOPIC);
        PostedChargeWorkflow workflow = new PostedChargeWorkflow(charge, getContext(), help);
        workflow.addTaskListener(new DefaultTaskListener() {
            public void taskEvent(TaskEvent event) {
                onRefresh(charge);
            }
        });
        workflow.start();
    }

    /**
     * Determines the actions that may be performed on the selected object.
     *
     * @return the actions
     */
    @Override
    protected AccountActActions getActions() {
        return (AccountActActions) super.getActions();
    }
}
