/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import nextapp.echo2.app.Component;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.system.common.util.ClassHelper;
import org.openvpms.web.component.edit.Editor;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.edit.PropertyEditor;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Scheduled report parameter components.
 *
 * @author Tim Anderson
 */
class ParameterComponents {

    /**
     * The parameter properties, and their corresponding ids.
     */
    private final Map<Property, Integer> parameters = new LinkedHashMap<>();

    /**
     * The parameter properties, and their corresponding components.
     */
    private final Map<Property, ComponentState> components = new HashMap<>();

    /**
     * The parameter properties, and their corresponding editors.
     */
    private final Map<Property, Editor> editors = new LinkedHashMap<>();

    /**
     * Constructs a {@link ParameterComponents}.
     *
     * @param parameters the report parameters
     * @param context    the layout context
     */
    public ParameterComponents(Parameters parameters, LayoutContext context) {
        int i = 0;
        while (true) {
            Property paramName = parameters.getParamName(i);
            Property paramDisplayName = parameters.getParamDisplayName(i);
            Property paramType = parameters.getParamType(i);
            Property paramExpressionType = parameters.getParamExpressionType(i);
            Property paramValue = parameters.getParamValue(i);
            if (paramName != null && paramDisplayName != null && paramType != null && paramValue != null) {
                String name = paramName.getString();
                String displayName = StringUtils.trimToNull(paramDisplayName.getString());
                String typeName = paramType.getString();
                Class<?> type = getClass(typeName);
                if (!StringUtils.isEmpty(name) && type != null) {
                    Property property;
                    PropertyEditor editor = null;
                    ComponentState state;
                    boolean readOnly = !context.isEdit();
                    if (Date.class.isAssignableFrom(type)) {
                        if (readOnly) {
                            DateParameterViewer viewer = new DateParameterViewer(name, displayName, paramExpressionType,
                                                                                 paramValue, context);
                            property = viewer.getProperty();
                            state = new ComponentState(viewer.getComponent(), property);
                        } else {
                            DateParameter parameter = new DateParameter(name, displayName, paramExpressionType,
                                                                        paramValue, context);
                            property = parameter.getProperty();
                            editor = parameter;
                            state = new ComponentState(editor);
                        }
                    } else {
                        property = new SimpleProperty(name, paramValue.getValue(), type, displayName, readOnly);
                        Component component = context.getComponentFactory().create(property);
                        if (!readOnly) {
                            property.addModifiableListener(modifiable -> paramValue.setValue(property.getValue()));
                            editor = new PropertyComponentEditor(property, component);
                        }
                        state = new ComponentState(component, property);
                    }
                    components.put(property, state);
                    if (context.isEdit()) {
                        editors.put(property, editor);
                    }
                    this.parameters.put(property, i);
                }
            } else {
                break;
            }
            i++;
        }
    }

    /**
     * Returns the parameters.
     *
     * @return the parameters
     */
    public Collection<Property> getParameters() {
        return parameters.keySet();
    }

    /**
     * Returns a component for a parameter.
     *
     * @param parameter the parameter
     * @return the corresponding component, or {@code null} if none is found
     */
    public ComponentState getComponent(Property parameter) {
        return components.get(parameter);
    }

    /**
     * Returns the editors.
     *
     * @return the editors. Empty if viewing the parameters
     */
    public Collection<Editor> getEditors() {
        return editors.values();
    }

    /**
     * Helper to exclude all of the properties that make up a parameter from display.
     *
     * @param parameter the parameter
     * @param nodes     the nodes top exclude the properties from
     */
    public void exclude(Property parameter, ArchetypeNodes nodes) {
        Integer id = parameters.get(parameter);
        if (id != null) {
            nodes.exclude(Parameters.PARAM_NAME + id);
            nodes.exclude(Parameters.PARAM_DISPLAY_NAME + id);
            nodes.exclude(Parameters.PARAM_TYPE + id);
            nodes.exclude(Parameters.PARAM_EXPR_TYPE + id);
            nodes.exclude(Parameters.PARAM_VALUE + id);
        }
    }

    /**
     * Returns a class given its name.
     *
     * @param className the class name. May be {@code null}
     * @return the class, or {@code null}
     */
    private Class<?> getClass(String className) {
        Class<?> type = null;
        if (!StringUtils.isEmpty(className)) {
            try {
                type = ClassHelper.getClass(className);
            } catch (ClassNotFoundException exception) {
                // no-op
            }
        }
        return type;
    }
}
