/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory.io;

import nextapp.echo2.app.Label;
import nextapp.echo2.app.table.DefaultTableColumnModel;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.laboratory.internal.io.LaboratoryTestData;
import org.openvpms.web.component.im.query.ListResultSet;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.AbstractIMTableModel;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.im.table.PagedIMTableModel;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.TableColumnFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

import static org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes.TEST;

/**
 * Dialog to report laboratory test import errors.
 *
 * @author Tim Anderson
 */
public class LaboratoryTestImportErrorDialog extends PopupDialog {

    /**
     * Constructs a {@link LaboratoryTestImportErrorDialog}.
     *
     * @param errors the import errors
     * @param help   the help context
     */
    public LaboratoryTestImportErrorDialog(List<LaboratoryTestData> errors, HelpContext help) {
        super(Messages.get("admin.laboratory.import.error.title"), "BrowserDialog", OK, help);
        setModal(true);

        ResultSet<LaboratoryTestData> resultSet = new ListResultSet<>(errors, 20);
        PagedIMTableModel<LaboratoryTestData, LaboratoryTestData> model
                = new PagedIMTableModel<>(new ErrorTableModel());
        PagedIMTable<LaboratoryTestData> table = new PagedIMTable<>(model, resultSet);
        Label message = LabelFactory.create("admin.laboratory.import.error.message", true);
        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET,
                                             ColumnFactory.create(Styles.WIDE_CELL_SPACING, message,
                                                                  table.getComponent())));
    }

    private static class ErrorTableModel extends AbstractIMTableModel<LaboratoryTestData> {

        private static final int CODE = 0;
        private static final int NAME = 1;
        private static final int LINE = 2;
        private static final int ERROR = 3;

        public ErrorTableModel() {
            DefaultTableColumnModel model = new DefaultTableColumnModel();
            model.addColumn(TableColumnFactory.create(CODE, getDisplayName(TEST, "code")));
            model.addColumn(TableColumnFactory.create(NAME, getDisplayName(TEST, "name")));
            model.addColumn(createTableColumn(LINE, "admin.laboratory.import.line"));
            model.addColumn(createTableColumn(ERROR, "admin.laboratory.import.error"));
            setTableColumnModel(model);
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(LaboratoryTestData object, TableColumn column, int row) {
            Object result;
            switch (column.getModelIndex()) {
                case CODE:
                    result = object.getCode();
                    break;
                case NAME:
                    result = object.getName();
                    break;
                case ERROR:
                    result = object.getError();
                    break;
                case LINE:
                    result = object.getLine();
                    break;
                default:
                    result = null;
            }
            return result;
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return null;
        }
    }
}