/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.web.component.im.layout.ArchetypeNodes;
import org.openvpms.web.system.ServiceHelper;

/**
 * Factory for {@link ArchetypeNodes} for charges.
 *
 * @author Tim Anderson
 */
public class ChargeArchetypeNodesFactory {

    /**
     * Creates a {@link ArchetypeNodes} for charges.
     * <p/>
     * This suppresses the informational reminder nodes if they are empty, and hides the sendReminder flag if
     * account reminders are disabled.
     *
     * @return the nodes
     */
    public static ArchetypeNodes create() {
        return apply(ArchetypeNodes.all());
    }

    /**
     * Modifies the supplied nodes to exclude reminder nodes if required.
     *
     * @param nodes the nodes to update
     * @return the nodes
     */
    public static ArchetypeNodes apply(ArchetypeNodes nodes) {
        boolean enabled = ServiceHelper.getBean(PracticeService.class).accountRemindersEnabled();
        // Suppresses the reminder nodes if they are empty
        nodes.excludeIfEmpty("reminderCount", "reminderSent", "reminders");
        if (!enabled) {
            nodes.exclude("sendReminder");
        }
        return nodes;
    }
}