/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.summary;

import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;

/**
 * Context that prevents inheritance of the customer/patient if they are unset in the local context.
 *
 * @author Tim Anderson
 */
public class CustomerPatientContext extends LocalContext {

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The patient.
     */
    private Party patient;

    /**
     * Constructs a {@link CustomerPatientContext}.
     *
     * @param parent the parent context
     */
    public CustomerPatientContext(Context parent) {
        super(parent);
        setCustomer(parent.getCustomer());
        setPatient(parent.getPatient());
    }

    /**
     * Sets the current customer.
     *
     * @param customer the current customer. May be {@code null}
     */
    @Override
    public void setCustomer(Party customer) {
        this.customer = customer;
    }

    /**
     * Returns the current customer.
     *
     * @return the current customer, or {@code null} if there is no current customer
     */
    @Override
    public Party getCustomer() {
        return customer;
    }

    /**
     * Sets the current patient.
     *
     * @param patient the current patient. May be {@code null}
     */
    @Override
    public void setPatient(Party patient) {
        this.patient = patient;
    }

    /**
     * Returns the current patient.
     *
     * @return the current patient, or {@code null} if there is no current patient
     */
    @Override
    public Party getPatient() {
        return patient;
    }

    /**
     * Returns an object for the specified key.
     *
     * @param key the context key
     * @return the object corresponding to {@code key} or {@code null} if none is found
     */
    @Override
    public IMObject getObject(String key) {
        IMObject result;
        if (TypeHelper.matches(key, CUSTOMER_SHORTNAME)) {
            result = getCustomer();
        } else if (TypeHelper.matches(key, PATIENT_SHORTNAME)) {
            result = getPatient();
        } else {
            result = super.getObject(key);
        }
        return result;
    }

    /**
     * Returns a context object that matches the specified reference.
     *
     * @param reference the object reference
     * @return the context object whose reference matches {@code reference},
     * or {@code null} if there is no matches
     */
    @Override
    public IMObject getObject(Reference reference) {
        IMObject result = null;
        if (reference.isA(CUSTOMER_SHORTNAME)) {
            if (customer != null && customer.getObjectReference().equals(reference)) {
                result = customer;
            }
        } else if (reference.isA(PATIENT_SHORTNAME)) {
            if (patient != null && patient.getObjectReference().equals(reference)) {
                result = patient;
            }
        } else {
            result = super.getObject(reference);
        }
        return result;
    }
}
