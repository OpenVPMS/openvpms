/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.authority;

import org.openvpms.archetype.rules.user.UserRules;
import org.openvpms.component.business.domain.im.security.ArchetypeAwareGrantedAuthority;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * Editor for <em>security.grantedAuthority</em> instances.
 *
 * @author Tim Anderson
 */
public class AuthorityEditor extends AbstractIMObjectEditor {

    /**
     * The user rules.
     */
    private final UserRules rules;

    /**
     * Constructs an {@link AuthorityEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public AuthorityEditor(ArchetypeAwareGrantedAuthority object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        rules = ServiceHelper.getBean(UserRules.class);
    }

    /**
     * Sets the authority name.
     *
     * @param name the authority name
     */
    public void setName(String name) {
        getNameProperty().setValue(name);
    }

    /**
     * Returns the authority name.
     *
     * @return name the authority name
     */
    public String getName() {
        return getNameProperty().getString();
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return {@code null}
     */
    @Override
    public IMObjectEditor newInstance() {
        return new AuthorityEditor(reload((ArchetypeAwareGrantedAuthority) getObject()), getParent(),
                                   getLayoutContext());
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateUniqueAuthorityName(validator);
    }

    /**
     * Returns the name property.
     *
     * @return the name property
     */
    private Property getNameProperty() {
        return getProperty("name");
    }

    /**
     * Validates the authority name.
     * <p/>
     * This verifies that the name is unique within the authority's archetype to avoid duplicate authority errors.
     *
     * @param validator the validator
     * @return {@code true} if the name is unique
     */
    private boolean validateUniqueAuthorityName(Validator validator) {
        boolean valid = true;
        Property property = getNameProperty();
        String name = property.getString();
        IMObject object = getObject();
        if (name != null && rules.authorityExists(name, object.getId())) {
            String message = Messages.format("admin.authority.duplicate", name);
            ValidatorError error = new ValidatorError(object.getArchetype(), property.getName(), message);
            validator.add(property, error);
            valid = false;
        }
        return valid;
    }

}
