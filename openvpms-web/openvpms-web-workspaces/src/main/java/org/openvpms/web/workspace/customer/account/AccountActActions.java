/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.reminder.AccountReminderRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.user.User;
import org.openvpms.web.component.im.edit.FinancialActions;
import org.openvpms.web.component.im.util.DefaultIMObjectSaveListener;
import org.openvpms.web.component.im.util.UserHelper;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND;

/**
 * Determines the operations that may be performed on customer account acts.
 *
 * @author Tim Anderson
 */
public class AccountActActions extends FinancialActions<FinancialAct> {

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * The account reminder rules.
     */
    private final AccountReminderRules reminderRules;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;


    /**
     * Constructs an {@link AccountActActions}.
     *
     * @param rules           the customer account rules
     * @param reminderRules   the reminder rules
     * @param practiceService the practice service
     */
    public AccountActActions(CustomerAccountRules rules, AccountReminderRules reminderRules,
                             PracticeService practiceService) {
        this.rules = rules;
        this.reminderRules = reminderRules;
        this.practiceService = practiceService;
    }

    /**
     * Determines if an act can be posted (i.e finalised).
     * <p/>
     * This implementation returns {@code true} if the act status isn't {@code POSTED} or {@code CANCELLED},
     * and there are no outstanding EFTPOS transactions for payments and refunds.
     *
     * @param act the act to check
     * @return {@code true} if the act can be posted
     */
    @Override
    public boolean canPost(FinancialAct act) {
        boolean result = super.canPost(act);
        if (result) {
            if (act.isA(PAYMENT, REFUND) && rules.hasOutstandingEFTPOSTransaction(act, false)) {
                result = false;
                String displayName = DescriptorHelper.getDisplayName(act, ServiceHelper.getArchetypeService());
                InformationDialog.show(Messages.format("act.post.title", displayName),
                                       Messages.format("customer.account.post.incompleteeft", displayName));
            }
        }
        return result;
    }

    /**
     * Posts the act. This changes the act's status to {@code POSTED}, and saves it.
     *
     * @param act the act to check
     * @return {@code true} if the act was posted
     */
    @Override
    public boolean post(FinancialAct act) {
        boolean result = false;
        if (canPost(act)) {
            try {
                rules.post(act);
                result = true;
            } catch (Throwable exception) {
                DefaultIMObjectSaveListener.INSTANCE.error(act, exception);
            }
        }
        return result;
    }

    /**
     * Determines if a payment can be administered.
     *
     * @param act  the act
     * @param user the current user
     * @return {@code true} if the act is a payment that can be administered
     */
    public boolean canAdminPayment(FinancialAct act, User user) {
        return act != null && act.isA(CustomerAccountArchetypes.PAYMENT) && isAdmin(user);
    }

    /**
     * Determines if an act can be unhidden.
     *
     * @param act  the act
     * @param user the current user
     * @return {@code true} if the act can be unhidden
     */
    public boolean canUnhide(FinancialAct act, User user) {
        return act != null && isAdmin(user) && rules.isHidden(act);
    }

    /**
     * Determines if an act can be hidden.
     *
     * @param act  the act
     * @param user the current user
     * @return {@code true} if the act can be hidden
     */
    public boolean canHide(FinancialAct act, User user) {
        return act != null && isAdmin(user) && rules.canHide(act) && !rules.isHidden(act);
    }

    /**
     * Determines if reminders can be enabled for an act.
     *
     * @param act the act
     * @return {@code true} if reminders can be enabled for the act
     */
    public boolean canEnableReminders(FinancialAct act) {
        return act != null && practiceService.accountRemindersEnabled() && reminderRules.canEnableReminders(act);
    }

    /**
     * Determines if reminders can be disabled for an act.
     *
     * @param act the act
     * @return {@code true} if reminders can be disabled for the act
     */
    public boolean canDisableReminders(FinancialAct act) {
        return act != null && practiceService.accountRemindersEnabled() && reminderRules.canDisableReminders(act);
    }

    /**
     * Determines if a user is an administrator.
     *
     * @param user the user. May be {@code null}
     * @return {@code true} if {@code user} is an administrator; otherwise {@code false}
     */
    private boolean isAdmin(User user) {
        return UserHelper.isAdmin(user);
    }

}
