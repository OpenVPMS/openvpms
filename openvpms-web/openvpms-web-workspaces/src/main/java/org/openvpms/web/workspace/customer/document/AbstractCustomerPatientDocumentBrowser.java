/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import nextapp.echo2.app.Button;
import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.laboratory.report.ExternalResults;
import org.openvpms.laboratory.resource.Content;
import org.openvpms.laboratory.resource.Resource;
import org.openvpms.laboratory.resource.Resources;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.laboratory.service.Orders;
import org.openvpms.web.component.im.doc.DocumentActTableModel;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.DateRangeActQuery;
import org.openvpms.web.component.im.query.IMObjectSelections;
import org.openvpms.web.component.im.query.MultiSelectBrowser;
import org.openvpms.web.component.im.query.MultiSelectTableBrowser;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.TabbedBrowser;
import org.openvpms.web.component.im.table.IMObjectTableModel;
import org.openvpms.web.component.im.table.IMTableModel;
import org.openvpms.web.component.mail.AttachmentBrowser;
import org.openvpms.web.component.mail.ContentAttachment;
import org.openvpms.web.component.mail.MailAttachment;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.PopupDialogListener;
import org.openvpms.web.echo.factory.ButtonFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.patient.mr.PatientDocumentQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * A browser for customer and patient documents.
 *
 * @author Tim Anderson
 */
public abstract class AbstractCustomerPatientDocumentBrowser extends TabbedBrowser<MailAttachment>
        implements AttachmentBrowser {

    /**
     * The customer. May be {@code null}
     */
    private final Party customer;

    /**
     * The patient. May be {@code null}
     */
    private final Party patient;

    /**
     * If {@code true} display the customer tab first, otherwise display the patient tab.
     */
    private final boolean customerFirst;

    /**
     * The date to query from. May be {@code null}.
     */
    private final Date from;

    /**
     * The date to query to. May be {@code null}.
     */
    private final Date to;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * External documents to attach.
     */
    private final List<MailAttachment> externalDocuments = new ArrayList<>();

    /**
     * Document template node.
     */
    private static final String DOCUMENT_TEMPLATE = "documentTemplate";

    /**
     * Constructs an {@link AbstractCustomerPatientDocumentBrowser}.
     *
     * @param customer      the customer. May be {@code null}
     * @param patient       the patient. May be {@code null}
     * @param customerFirst if {@code true} display the customer tab first, otherwise display the patient tab
     * @param from          the from date. May  be {@code null}
     * @param to            the to date. May be {@code null}
     * @param context       the layout context
     */
    public AbstractCustomerPatientDocumentBrowser(Party customer, Party patient, boolean customerFirst, Date from,
                                                  Date to, LayoutContext context) {
        this.customer = customer;
        this.patient = patient;
        this.customerFirst = customerFirst;
        this.from = from;
        this.to = to;
        this.context = context;
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Returns the browser component.
     *
     * @return the browser component
     */
    public Component getComponent() {
        if (getBrowsers().isEmpty()) {
            if (customerFirst) {
                addCustomerBrowsers();
                addPatientBrowsers();
            } else {
                addPatientBrowsers();
                addCustomerBrowsers();
            }
        }
        return super.getComponent();
    }

    /**
     * Returns the selections from each browser.
     *
     * @return the selections
     */
    public Collection<MailAttachment> getSelections() {
        List<MailAttachment> result = new ArrayList<>();
        for (Browser<MailAttachment> browser : getBrowsers()) {
            if (browser instanceof MultiSelectBrowser) {
                result.addAll(((MultiSelectBrowser<MailAttachment>) browser).getSelections());
            }
        }
        result.addAll(externalDocuments);
        return result;
    }

    /**
     * Clears the selections.
     */
    @Override
    public void clearSelections() {
        for (Browser<MailAttachment> browser : getBrowsers()) {
            if (browser instanceof MultiSelectBrowser) {
                ((MultiSelectBrowser<MailAttachment>) browser).clearSelections();
            }
        }
        externalDocuments.clear();
    }

    /**
     * Adds the customer document browsers, if it the customer exists.
     */
    protected void addCustomerBrowsers() {
        if (customer != null) {
            addCustomerBrowsers(customer, patient);
        }
    }

    /**
     * Adds customer document browsers.
     *
     * @param customer the customer
     * @param patient  the patient. May be {@code null}
     */
    protected void addCustomerBrowsers(Party customer, Party patient) {
        addDocumentBrowser("customer.documentbrowser.customer", new CustomerDocumentQuery<>(customer), null);
    }

    /**
     * Adds the patient document browsers, if it the patient exists.
     */
    protected void addPatientBrowsers() {
        if (patient != null) {
            addPatientBrowsers(patient);
        }
    }

    /**
     * Adds patient document browsers.
     *
     * @param patient the patient
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    protected void addPatientBrowsers(Party patient) {
        IMObjectTableModel<Act> model = (IMObjectTableModel) new PatientDocumentModel(context);
        addDocumentBrowser("customer.documentbrowser.patient", new PatientDocumentQuery<>(patient), model);
    }

    /**
     * Adds a document browser.
     * <p/>
     * This disables selection of documents with no content.
     *
     * @param key   the browser tab i18n key
     * @param query the query
     * @param model the table model. May be {@code null}
     */
    protected void addDocumentBrowser(String key, DateRangeActQuery<Act> query, IMObjectTableModel<Act> model) {
        init(query);
        IMObjectSelections<Act> selections = new IMObjectSelections<Act>() {
            @Override
            public boolean canSelect(Act object) {
                return canSelectDocument(object);
            }
        };
        DocumentBrowser browser = (model != null) ? new DocumentBrowser(query, model, selections, context)
                                                  : new DocumentBrowser(query, selections, context);
        addBrowser(Messages.get(key), new ActAttachmentBrowser(browser));
    }

    /**
     * Adds a browser.
     *
     * @param key   the browser tab i18n key
     * @param query the query
     */
    protected void addBrowser(String key, DateRangeActQuery<Act> query) {
        init(query);
        addBrowser(Messages.get(key), new ActAttachmentBrowser(new MultiSelectTableBrowser<>(query, context)));
    }

    /**
     * Adds a browser.
     *
     * @param key   the browser tab i18n key
     * @param query the query
     * @param model the table model
     */
    protected void addBrowser(String key, DateRangeActQuery<Act> query, IMObjectTableModel<Act> model) {
        init(query);
        addBrowser(Messages.get(key), new ActAttachmentBrowser(new MultiSelectTableBrowser<>(query, model, context)));
    }

    /**
     * Initialises a query to set date ranges.
     *
     * @param query the query
     */
    protected void init(DateRangeActQuery<Act> query) {
        if (from != null || to != null) {
            query.getComponent();
            query.setAllDates(false);
            query.setFrom(from);
            query.setTo(to);
        }
    }

    /**
     * Adds resources to the set of selected documents.
     *
     * @param resources the resources
     */
    protected void addResources(Collection<Resource> resources) {
        ContentAttachment attachment = null;
        for (Resource resource : resources) {
            if (resource instanceof Content) {
                attachment = new ContentAttachment((Content) resource);
                externalDocuments.add(attachment);
            }
        }
        if (attachment != null) {
            notifyBrowsed(attachment);
        }
    }

    /**
     * Determines if a document can be selected.
     *
     * @param act the document act
     * @return {@code true} if the document can be selected
     */
    protected boolean canSelectDocument(Act act) {
        boolean result = false;
        if (act instanceof DocumentAct) {
            DocumentAct documentAct = (DocumentAct) act;
            if (documentAct.getDocument() != null) {
                result = true;
            } else if (act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
                // if there is no report, a document can be generated
                result = true;
            } else {
                IMObjectBean bean = service.getBean(documentAct);
                result = bean.hasNode(DOCUMENT_TEMPLATE) && bean.getTargetRef(DOCUMENT_TEMPLATE) != null;
            }
        }
        return result;
    }

    /**
     * Returns the layout context.
     *
     * @return the layout context
     */
    protected LayoutContext getContext() {
        return context;
    }

    class PatientDocumentModel extends DocumentActTableModel {

        /**
         * Constructs a {@link PatientDocumentModel}.
         *
         * @param context the layout context
         */
        public PatientDocumentModel(LayoutContext context) {
            super(context);
        }

        /**
         * Creates a component to view the associated document.
         *
         * @param act the document
         * @return a component to view the associated document
         */
        @Override
        protected Component createDocumentViewer(DocumentAct act) {
            Component component = super.createDocumentViewer(act);
            if (act.isA(InvestigationArchetypes.PATIENT_INVESTIGATION)) {
                try {
                    Component external = getExternalResults(act);
                    if (external != null) {
                        component = RowFactory.create(Styles.CELL_SPACING, component, external);
                    }
                } catch (Throwable exception) {
                    ErrorHelper.show(exception);
                }
            }
            return component;
        }

        /**
         * Returns a component to view external results, if an investigation has any.
         *
         * @param act the investigation
         * @return a component to view external results, or {@code null} if the investigation has none
         */
        private Component getExternalResults(DocumentAct act) {
            Component result = null;
            IMObjectBean bean = getBean(act);
            if (bean.getBoolean("externalResults")) {
                Entity laboratory = (Entity) getContext().getCache().get(bean.getTargetRef("laboratory"));
                if (laboratory != null) {
                    LaboratoryServices services = ServiceHelper.getBean(LaboratoryServices.class);
                    LaboratoryService service = services.getService(laboratory);
                    Orders orders = ServiceHelper.getBean(Orders.class);
                    ExternalResults results = service.getExternalResults(orders.getOrder(act.getId()));
                    if (results.hasDocuments()) {
                        Resources documents = results.getDocuments();
                        ResourceIteratorResultSet set = new ResourceIteratorResultSet(documents, 5);
                        if (set.hasNext()) {
                            Button button = ButtonFactory.create(null, "attach", () -> showExternalDocuments(set));
                            button.setToolTipText(Messages.get("document.attachexternal"));
                            result = button;
                        }
                    }
                }
            }
            return result;
        }

        /**
         * Shows external documents in a dialog.
         *
         * @param set the external document result set
         */
        private void showExternalDocuments(ResourceIteratorResultSet set) {
            ExternalDocumentsDialog dialog = new ExternalDocumentsDialog(set, getContext().getHelpContext());
            dialog.addWindowPaneListener(new PopupDialogListener() {
                @Override
                public void onOK() {
                    Collection<Resource> selected = dialog.getSelected();
                    if (!selected.isEmpty()) {
                        addResources(selected);
                    }
                }
            });
            dialog.show();
        }
    }

    private class DocumentBrowser extends MultiSelectTableBrowser<Act> {
        /**
         * Constructs a {@link DocumentBrowser} that queries IMObjects using the specified query.
         *
         * @param query   the query
         * @param tracker the selection tracker, used to determine initial selections
         * @param context the layout context
         */
        public DocumentBrowser(Query<Act> query, SelectionTracker<Act> tracker, LayoutContext context) {
            super(query, tracker, context);
        }

        /**
         * Constructs an {@code IMObjectTableBrowser} that queries IMObjects
         * using the specified query, displaying them in the table.
         *
         * @param query   the query
         * @param model   the table model
         * @param tracker the selection tracker, used to determine initial selections
         * @param context the layout context
         */
        public DocumentBrowser(Query<Act> query, IMTableModel<Act> model, SelectionTracker<Act> tracker,
                               LayoutContext context) {
            super(query, model, tracker, context);
        }

        /**
         * Notifies that selection is disabled for an object.
         *
         * @param object the object
         */
        @Override
        protected void notifySelectionDisabled(Act object) {
            InformationDialog.show(Messages.format("document.attachment.nocontent",
                                                   DescriptorHelper.getDisplayName(object, service)));


        }
    }

}
