/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides support to configure logging at runtime.
 * <p/>
 * Logging changes are transient and will not survive a restart.
 *
 * @author Tim Anderson
 */
public class LoggingConfig {

    /**
     * The user-configured categories.
     */
    private final Map<String, Level> categories = new HashMap<>();

    /**
     * Constructs a {@link LoggingConfig}.
     */
    public LoggingConfig() {
        super();
    }

    /**
     * Adds a category.
     *
     * @param category the category
     * @param level    the logging level
     */
    public synchronized void add(String category, Level level) {
        categories.put(category, level);
        Configurator.setLevel(category, level);
    }

    /**
     * Removes a category.
     *
     * @param category the category to remove
     */
    public synchronized void remove(String category) {
        categories.remove(category);
        Configurator.setLevel(category, Level.OFF);
    }

    /**
     * Returns the categories.
     *
     * @return the categories and their corresponding levels
     */
    public synchronized Map<String, Level> getCategories() {
        return Collections.unmodifiableMap(categories);
    }

    /**
     * Resets categories back to the default.
     */
    public synchronized void reset() {
        org.apache.logging.log4j.spi.LoggerContext context = LogManager.getContext(false);
        if (context instanceof LoggerContext) {
            ((LoggerContext) context).reconfigure();
        }
        categories.clear();
    }
}