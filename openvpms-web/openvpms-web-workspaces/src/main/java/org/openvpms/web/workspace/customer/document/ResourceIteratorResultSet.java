/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.document;

import org.openvpms.component.business.dao.im.Page;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.laboratory.resource.Resource;
import org.openvpms.laboratory.resource.ResourceIterator;
import org.openvpms.laboratory.resource.Resources;
import org.openvpms.web.component.im.query.AbstractCachingResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Results set for {@link Resource}s.
 *
 * @author Tim Anderson
 */
public class ResourceIteratorResultSet extends AbstractCachingResultSet<Resource> {

    /**
     * The resources.
     */
    private final Resources resources;

    /**
     * The resource iterator.
     */
    private final ResourceIterator iterator;

    /**
     * Construct an {@link ResourceIteratorResultSet}.
     *
     * @param pageSize the maximum no. of results per page
     */
    public ResourceIteratorResultSet(Resources resources, int pageSize) {
        super(pageSize, 1);
        this.resources = resources;
        this.iterator = resources.getResources();
    }

    /**
     * Returns the resources.
     *
     * @return the resources
     */
    public Resources getResources() {
        return resources;
    }

    /**
     * Sorts the set. This resets the iterator.
     *
     * @param sort the sort criteria. May be {@code null}
     */
    @Override
    public void sort(SortConstraint[] sort) {

    }

    /**
     * Determines if the node is sorted ascending or descending.
     *
     * @return {@code true} if the node is sorted ascending or no sort constraint was specified; {@code false} if it is
     * sorted descending
     */
    @Override
    public boolean isSortedAscending() {
        return false;
    }

    /**
     * Returns the sort criteria.
     *
     * @return the sort criteria. Never null
     */
    @Override
    public SortConstraint[] getSortConstraints() {
        return new SortConstraint[0];
    }

    /**
     * Determines if duplicate results should be filtered.
     *
     * @param distinct if true, remove duplicate results
     */
    @Override
    public void setDistinct(boolean distinct) {

    }

    /**
     * Determines if duplicate results should be filtered.
     *
     * @return {@code true} if duplicate results should be removed otherwise {@code false}
     */
    @Override
    public boolean isDistinct() {
        return false;
    }

    /**
     * Performs a query.
     *
     * @param firstResult the first result of the page to retrieve
     * @param maxResults  the maximum no. of results in the page
     * @return the page
     */
    @Override
    protected IPage<Resource> query(int firstResult, int maxResults) {
        List<Resource> result = new ArrayList<>();
        try {
            if (iterator.getPosition() != firstResult) {
                iterator.setPosition(firstResult);
            }
            int count = 0;
            while (count < maxResults && iterator.hasNext()) {
                result.add(iterator.next());
                count++;
            }
        } catch (NoSuchElementException ignore) {
        }

        return new Page<>(result, firstResult, maxResults, -1);
    }

    /**
     * Counts the no. of results matching the query criteria.
     *
     * @return the total number of results
     */
    @Override
    protected int countResults() {
        int result = iterator.getSize();
        if (result == -1) {
            // need to iterate to determine the number
            int position = iterator.getPosition();
            ResourceIterator clone = resources.getResources();
            try {
                clone.setPosition(position);
                while (clone.hasNext()) {
                    clone.next();
                    position = clone.getPosition();
                }
            } catch (NoSuchElementException ignore) {
                // the iteration has changed
            }
            result = position;
        }
        return result;
    }
}