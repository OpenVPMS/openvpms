/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.eftpos;

import nextapp.echo2.app.Column;
import nextapp.echo2.app.Component;
import org.openvpms.eftpos.service.ManagedTerminalRegistrar;
import org.openvpms.eftpos.service.ManagedTerminalRegistrar.Field;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.edit.DefaultEditableComponentFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * EFTPOS terminal registration dialog for EFTPOS services that provide a {@link ManagedTerminalRegistrar}.
 *
 * @author Tim Anderson
 */
public class ManagedTerminalRegistrationDialog extends ModalDialog {

    /**
     * The terminal registrar.
     */
    private final ManagedTerminalRegistrar registrar;

    /**
     * The properties to prompt for.
     */
    private final List<Property> properties = new ArrayList<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ManagedTerminalRegistrationDialog.class);

    /**
     * Constructs a {@link ManagedTerminalRegistrationDialog}.
     *
     * @param registrar the terminal registrar
     * @param help      the help context
     */
    public ManagedTerminalRegistrationDialog(ManagedTerminalRegistrar registrar, HelpContext help) {
        super(Messages.get("admin.eftpos.register.title"), OK_CANCEL, help);

        this.registrar = registrar;
        Column column = ColumnFactory.create(Styles.WIDE_CELL_SPACING);
        String message = registrar.getMessage();
        if (message != null) {
            column.add(LabelFactory.text(message));
        }

        FocusGroup group = getFocusGroup();
        ComponentGrid grid = new ComponentGrid();
        LayoutContext layoutContext = new DefaultLayoutContext(new LocalContext(), help);
        DefaultEditableComponentFactory factory = new DefaultEditableComponentFactory(layoutContext);
        for (Field field : registrar.getFields()) {
            SimpleProperty property = new SimpleProperty(field.getName(), null, field.getType(),
                                                         field.getDisplayName()) {
                @Override
                public boolean isPassword() {
                    return field.isPassword();
                }
            };
            property.setDescription(field.getDescription());
            properties.add(property);
            Component component = factory.create(property);
            ComponentState state = new ComponentState(component, property, group);
            grid.add(state);
        }

        column.add(grid.createGrid());

        getLayout().add(ColumnFactory.create(Styles.LARGE_INSET, column));
        resize("ManagedTerminalRegistrationDialog.size");
    }

    /**
     * Invoked when the 'OK' button is pressed. Registers the terminal.
     */
    @Override
    protected void onOK() {
        register();
    }

    /**
     * Registers the terminal.
     */
    private void register() {
        try {
            Map<String, Object> values = new HashMap<>();
            for (Property property : properties) {
                values.put(property.getName(), property.getValue());
            }
            registrar.register(values);
            InformationDialog.newDialog()
                    .title(Messages.get("admin.eftpos.register.title"))
                    .message(Messages.get("admin.eftpos.register.registered"))
                    .ok(() -> ManagedTerminalRegistrationDialog.this.close(OK_ID))
                    .show();
        } catch (Throwable exception) {
            String error = ErrorFormatter.format(exception);
            ErrorDialog.newDialog()
                    .title(Messages.get("admin.eftpos.register.title"))
                    .message(Messages.format("admin.eftpos.register.error", error))
                    .show();
        }
    }
}