/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.appointment;

import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.util.PropertySet;
import org.openvpms.web.workspace.workflow.scheduling.Schedule;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * An {@link AppointmentGrid} for a single schedule.
 * <p>
 * This does not support overlapping events, and will throw an exception if one is encountered.
 *
 * @author Tim Anderson
 */
public class SingleScheduleGrid extends AbstractAppointmentGrid {

    /**
     * The schedule.
     */
    private final Schedule schedule;

    /**
     * The slots.
     */
    private final List<ScheduleSlot> slots = new ArrayList<>();

    /**
     * Constructs a {@link SingleScheduleGrid}.
     *
     * @param scheduleView         the schedule view
     * @param date                 the appointment date
     * @param organisationSchedule the schedule
     * @param events               the events
     * @param rules                the appointment rules
     * @param rosterService        the roster service
     */
    SingleScheduleGrid(Entity scheduleView, Date date, Entity organisationSchedule, ScheduleEvents events,
                       AppointmentRules rules, RosterService rosterService) {
        super(scheduleView, date, -1, -1, rules, rosterService);
        schedule = createSchedule(organisationSchedule);
        setSchedules(Collections.singletonList(schedule));
        int startMins = schedule.getStartMins();
        int endMins = schedule.getEndMins();
        int slotSize = schedule.getSlotSize();
        setSlotSize(slotSize);

        // adjust the start and end minutes based on the appointments present
        for (PropertySet set : events.getEvents()) {
            Date startTime = set.getDate(ScheduleEvent.ACT_START_TIME);
            Date endTime = set.getDate(ScheduleEvent.ACT_END_TIME);
            int slotStart = getSlotMinutes(startTime, false);
            int slotEnd = getSlotMinutes(endTime, true);
            if (startMins > slotStart) {
                startMins = slotStart;
            }
            if (endMins < slotEnd) {
                endMins = slotEnd;
            }
        }

        setStartMins(startMins);
        setEndMins(endMins);

        setEvents(events);
    }

    /**
     * Returns the no. of slots in the grid.
     *
     * @return the no. of slots
     */
    @Override
    public int getSlots() {
        return slots.size();
    }

    /**
     * Returns the appointment for the specified schedule and slot.
     *
     * @param schedule the schedule
     * @param slot     the slot
     * @return the corresponding appointment, or {@code null} if none is found
     */
    public PropertySet getEvent(Schedule schedule, int slot) {
        PropertySet result = null;
        if (slot >= 0 && slot < getSlots()) {
            SlotGroup group = slots.get(slot).getGroup();
            result = (group != null) ? group.getAppointment() : null;
        }
        return result;
    }

    /**
     * Returns the no. of slots that an event occupies, from the specified slot.
     * <p>
     * If the event begins prior to the slot, the remaining slots will be returned.
     *
     * @param event    the event
     * @param schedule the schedule
     * @param slot     the starting slot
     * @return the no. of slots that the event occupies
     */
    @Override
    public int getSlots(PropertySet event, Schedule schedule, int slot) {
        SlotGroup group = slots.get(slot).getGroup();
        int result = 0;
        if (group != null) {
            int start = group.getStartSlot();
            result = (start + group.getSlots()) - slot;
        }
        return result;
    }

    /**
     * Returns the slot times.
     *
     * @return the slot time
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Slot> getSlotTimes() {
        return (List<Slot>) (List<?>) slots;
    }

    /**
     * Sets the events.
     *
     * @param events the events
     */
    private void setEvents(ScheduleEvents events) {
        int startMins = getStartMins();
        int endMins = getEndMins();
        int slotSize = getSlotSize();

        ZonedDateTime end = getSlotTimeForMinutes(endMins);
        ZonedDateTime slotStart = getSlotTimeForMinutes(startMins);
        ZonedDateTime slotEnd;

        // create slots
        while (slotStart.compareTo(end) < 0) {
            slotEnd = slotStart.plusMinutes(slotSize);
            if (slotEnd.compareTo(end) > 0) {
                slotEnd = end;
            }
            slots.add(new ScheduleSlot(slotStart, slotEnd));
            slotStart = slotEnd;
        }

        for (PropertySet event : events.getEvents()) {
            // add the event, and create a new SlotGroup for it
            addEvent(event);
        }
    }

    /**
     * Adds an event.
     * <p>
     * This adds it to the schedule, and creates a new SlotGroup for it.
     *
     * @param event the event
     */
    private void addEvent(PropertySet event) {
        schedule.addEvent(event);

        Date startTime = event.getDate(ScheduleEvent.ACT_START_TIME);
        Date endTime = event.getDate(ScheduleEvent.ACT_END_TIME);

        int startSlot = getSlot(startTime);
        int endSlot = getSlot(endTime, true);

        SlotGroup group = new SlotGroup(event, startSlot, endSlot - startSlot);
        for (int i = startSlot; i < endSlot && i < slots.size(); ++i) {
            ScheduleSlot scheduleSlot = slots.get(i);
            if (scheduleSlot.getGroup() != null) {
                throw new IllegalStateException("Overlapping event detected, slot=" + i + ", start="
                                                + scheduleSlot.getStart());
            }
            scheduleSlot.setGroup(group);
        }
    }

    /**
     * Represents a slot in the schedule.
     */
    private static class ScheduleSlot extends Slot {

        /**
         * The related slots, or {@code null} if this slot isn't related to any slots.
         */
        private SlotGroup group;

        /**
         * Constructs a {@link ScheduleSlot}.
         *
         * @param start the start time
         * @param end   the end time
         */
        ScheduleSlot(ZonedDateTime start, ZonedDateTime end) {
            this(start, end, null);
        }

        /**
         * Constructs a {@link ScheduleSlot}.
         *
         * @param start the start time
         * @param end   the end time
         * @param group the related slots. May be {@code null}
         */
        ScheduleSlot(ZonedDateTime start, ZonedDateTime end, SlotGroup group) {
            super(start, end);
            this.group = group;
        }

        /**
         * Sets the related slots.
         *
         * @param group the related slots
         */
        public void setGroup(SlotGroup group) {
            this.group = group;
        }

        /**
         * Returns the related slots.
         *
         * @return the related slots. May be {@code null}
         */
        SlotGroup getGroup() {
            return group;
        }
    }

    /**
     * A group of slots, associated with a single event.
     */
    private static class SlotGroup {

        /**
         * The event.
         */
        private final PropertySet appointment;

        /**
         * The start slot.
         */
        private final int start;

        /**
         * The no. of slots that the appointment occupies.
         */
        private final int slots;

        /**
         * Constructs a {@link SlotGroup}.
         *
         * @param appointment the appointment
         * @param start       the start slot
         * @param slots       the no. of slots that the appointment occupies
         */
        SlotGroup(PropertySet appointment, int start, int slots) {
            this.appointment = appointment;
            this.start = start;
            this.slots = slots;
        }

        /**
         * Returns the appointment.
         *
         * @return the appointment
         */
        public PropertySet getAppointment() {
            return appointment;
        }


        /**
         * Returns the slot that the group starts at.
         *
         * @return the start slot
         */
        int getStartSlot() {
            return start;
        }

        /**
         * Returns the no. of slots that the appointment occupies.
         *
         * @return the no. of slots
         */
        int getSlots() {
            return slots;
        }
    }

}
