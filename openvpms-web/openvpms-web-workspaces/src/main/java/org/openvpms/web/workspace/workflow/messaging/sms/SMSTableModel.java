/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import nextapp.echo2.app.table.TableColumn;
import nextapp.echo2.app.table.TableColumnModel;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.table.TableHelper;

import java.util.List;

/**
 * Table model for <em>act.smsMessage</em>.
 *
 * @author Tim Anderson
 */
public class SMSTableModel extends AbstractSMSTableModel {

    /**
     * The replies column model index.
     */
    private int repliesIndex;

    /**
     * Constructs an {@link SMSTableModel}.
     *
     * @param context the layout context
     */
    public SMSTableModel(LayoutContext context) {
        super(SMSArchetypes.MESSAGE, context);
    }

    /**
     * Creates a column model for one or more archetypes.
     * If there are multiple archetypes, the intersection of the descriptors
     * will be used.
     *
     * @param archetypes the archetypes
     * @param context    the layout context
     * @return a new column model
     */
    @Override
    protected TableColumnModel createColumnModel(List<ArchetypeDescriptor> archetypes, LayoutContext context) {
        TableColumnModel model = super.createColumnModel(archetypes, context);
        repliesIndex = getNextModelIndex(model);
        model.addColumn(createTableColumn(repliesIndex, "workflow.sms.replies"));
        return model;
    }

    /**
     * Returns the value found at the given coordinate within the table.
     *
     * @param object the object
     * @param column the table column
     * @param row    the table row
     */
    @Override
    protected Object getValue(Act object, TableColumn column, int row) {
        Object result;
        int index = column.getModelIndex();
        if (index == repliesIndex) {
            result = getReplies(object);
        } else {
            result = super.getValue(object, column, row);
        }
        return result;
    }

    private Object getReplies(Act object) {
        Object result;
        IMObjectBean bean = getCurrent(object);
        result = TableHelper.rightAlign("" + bean.getValues("replies").size());
        return result;
    }



}
