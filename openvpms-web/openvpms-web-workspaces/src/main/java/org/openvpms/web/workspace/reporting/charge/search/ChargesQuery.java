/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.charge.search;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Row;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRange;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.echo.util.StyleSheetHelper;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.reporting.charge.AbstractChargesQuery;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Query charges by type, status date, amount and location.
 *
 * @author Tim Anderson
 */
public class ChargesQuery extends AbstractChargesQuery {

    /**
     * The amount-from  field.
     */
    private final Property fromAmount = new SimpleProperty("amountFrom", BigDecimal.class);

    /**
     * The amount-to field.
     */
    private final Property toAmount = new SimpleProperty("amountTo", BigDecimal.class);

    /**
     * Listener for amountFrom changes.
     */
    private final ModifiableListener fromAmountListener;

    /**
     * Listener for amountTo changes.
     */
    private final ModifiableListener toAmountListener;

    /**
     * The maximum number of months to query when querying by amount.
     */
    private int dateRangeMonths = 6;

    /**
     * The act statuses.
     */
    private static final ActStatuses STATUSES;

    static {
        STATUSES = new ActStatuses(CustomerAccountArchetypes.INVOICE);
        STATUSES.setDefault((String) null);
    }

    /**
     * Constructs a {@link ChargesQuery}.
     *
     * @param context the layout context
     */
    public ChargesQuery(LayoutContext context) {
        super(STATUSES, null, context);
        setDefaultSortConstraint(ASCENDING_START_TIME);

        fromAmountListener = modifiable -> onAmountFromChanged();
        fromAmount.addModifiableListener(fromAmountListener);
        toAmountListener = modifiable -> onAmountToChanged();
        toAmount.addModifiableListener(toAmountListener);
    }

    /**
     * Sets the maximum no. of months to query when querying by amount.
     * <p/>
     * This is required to limit expensive queries. Must be invoked immediately after construction to take effect.
     *
     * @param months the maximum no. of months. Must be {@code > 0}
     */
    public void setDateRangeMonths(int months) {
        if (months > 0) {
            dateRangeMonths = months;
        }
    }

    /**
     * Returns the preferred height of the query when rendered.
     *
     * @return the preferred height, or {@code null} if it has no preferred height
     */
    @Override
    public Extent getHeight() {
        return super.getHeight(3);
    }

    /**
     * Performs the query.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return the query result set. May be {@code null}
     */
    @Override
    public ResultSet<FinancialAct> query(SortConstraint[] sort) {
        ResultSet<FinancialAct> result = null;
        if (haveAmount()) {
            DateRange range = getDateRange();
            if (range.getAllDates()) {
                ErrorHelper.show(Messages.get("reporting.charges.search.dateRangeRequired"));
            } else if (range.getFrom() == null || range.getTo() == null
                       || calculateFromDate(range.getTo()).compareTo(range.getFrom()) > 0) {
                ErrorHelper.show(Messages.format("reporting.charges.search.dateRangeTooBig", dateRangeMonths));
            } else {
                result = createResultSet(sort);
            }
        } else {
            result = createResultSet(sort);
        }
        return result;
    }

    /**
     * Lays out the component in a container.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addSearchField(container);
        addShortNameSelector(container);
        addStatusSelector(container);
        addDateRange(container);
        addLocation(container);
        addClinician(container);
        addAmount(container);
    }

    /**
     * Creates the date range.
     *
     * @return a new date range
     */
    @Override
    protected DateRange createDateRange() {
        Date to = DateRules.getTomorrow();
        Date from = calculateFromDate(to);
        return new LimitedDateRange(from, to);
    }

    /**
     * Creates a new result set.
     *
     * @param sort the sort constraint. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<FinancialAct> createResultSet(SortConstraint[] sort) {
        Party location = getLocation();
        return new ChargesResultSet(getArchetypeConstraint(), getValue(), location, getLocations(),
                                    getFrom(), getTo(), getClinician(), fromAmount.getBigDecimal(),
                                    toAmount.getBigDecimal(), getStatuses(), excludeStatuses(), getMaxResults(),
                                    sort);
    }

    /**
     * Calculates the 'from' date.
     *
     * @param to the 'to' date
     * @return the new 'from' date
     */
    private Date calculateFromDate(Date to) {
        return DateRules.getDate(to, -dateRangeMonths, DateUnits.MONTHS);
    }

    /**
     * Invoked when the 'to' amount changes.
     */
    private void onAmountToChanged() {
        BigDecimal from = fromAmount.getBigDecimal();
        BigDecimal to = toAmount.getBigDecimal();
        if (to != null) {
            getDateRange().setAllDates(false);
            if (from == null || to.compareTo(from) < 0) {
                fromAmount.removeModifiableListener(fromAmountListener);
                fromAmount.setValue(to);
                fromAmount.addModifiableListener(fromAmountListener);
            }
        }
    }

    /**
     * Invoked when the 'from' amount changes.
     */
    private void onAmountFromChanged() {
        BigDecimal from = fromAmount.getBigDecimal();
        BigDecimal to = toAmount.getBigDecimal();
        if (from != null) {
            getDateRange().setAllDates(false);
            if (to == null || to.compareTo(from) < 0) {
                toAmount.removeModifiableListener(toAmountListener);
                toAmount.setValue(from);
                toAmount.addModifiableListener(toAmountListener);
            }
        }
    }

    /**
     * Adds the amount range.
     *
     * @param container the container to add to
     */
    private void addAmount(Component container) {
        String amount = DescriptorHelper.getDisplayName(CustomerAccountArchetypes.INVOICE, "amount",
                                                        ServiceHelper.getArchetypeService());
        Row amountRange = RowFactory.create(Styles.CELL_SPACING);
        int numericLength = StyleSheetHelper.getNumericLength();
        TextField from = BoundTextComponentFactory.createNumeric(fromAmount, numericLength);
        TextField to = BoundTextComponentFactory.createNumeric(toAmount, numericLength);
        amountRange.add(from);
        amountRange.add(LabelFactory.text("-"));
        amountRange.add(to);
        container.add(LabelFactory.text(amount));
        container.add(amountRange);
        FocusGroup group = getFocusGroup();
        group.add(from);
        group.add(to);
    }

    /**
     * Determines if an amount has been specified.
     *
     * @return {@code true} if an amount has been specified
     */
    private boolean haveAmount() {
        return fromAmount.getBigDecimal() != null || toAmount.getBigDecimal() != null;
    }

    /**
     * A date range that tries to limit the range to N months if an amount is present.
     */
    private class LimitedDateRange extends DateRange {

        /**
         * Constructs a {@link LimitedDateRange}.
         *
         * @param from the 'from' date. May be {@code null}
         * @param to   the 'to' date. May be {@code null}
         */
        public LimitedDateRange(Date from, Date to) {
            super(true, from, to);
        }

        /**
         * Updates the 'from' date when the 'to' date changes.
         * <p/>
         * This ensures the date range doesn't exceed
         *
         * @param from the 'from' date. May be {@code null}
         * @param to   the 'to' date. May be {@code null}
         */
        @Override
        protected void updateFromDate(Date from, Date to) {
            if (haveAmount() && from != null && to != null) {
                Date min = calculateFromDate(to);
                if (min.compareTo(from) > 0) {
                    setFrom(min, true);
                } else {
                    super.updateFromDate(from, to);
                }
            } else {
                super.updateFromDate(from, to);
            }
        }

        @Override
        protected void updateToDate(Date from, Date to) {
            if (haveAmount() && from != null && to != null) {
                Date max = DateRules.getDate(from, dateRangeMonths, DateUnits.MONTHS);
                if (max.compareTo(to) < 0) {
                    setTo(max, true);
                } else {
                    super.updateToDate(from, to);
                }
            } else {
                super.updateToDate(from, to);
            }
        }
    }
}
