/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Window;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DialogManager}.
 *
 * @author Tim Anderson
 */
public class DialogManagerTestCase {

    private ApplicationInstance app;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        app = new ApplicationInstance() {
            @Override
            public Window init() {
                return new Window();
            }
        };
        ApplicationInstance.setActive(app);
        app.doInit();
    }

    /**
     * Verifies that the most recent error dialog is always displayed on top.
     */
    @Test
    public void testErrorDialogAlwaysOnTop() {
        PopupWindow window1 = createWindow();
        ErrorDialog error1 = new ErrorDialog("error1");
        ErrorDialog error2 = new ErrorDialog("error2");
        PopupWindow window2 = createWindow();
        DialogManager.show(window1);
        checkDialogs(window1);
        DialogManager.show(error1);
        checkDialogs(window1, error1);
        DialogManager.show(window2);
        checkDialogs(window1, window2, error1);
        DialogManager.show(error2);
        checkDialogs(window1, window2, error1, error2);
    }

    /**
     * Verifies that dialogs are displayed in z-index order, lowest to highest.
     *
     * @param windows the windows
     */
    private void checkDialogs(PopupWindow... windows) {
        PopupWindow lastModal = null;
        int zIndex = -1;
        for (PopupWindow window : windows) {
            assertTrue(window.isVisible());
            assertTrue(window.getZIndex() > zIndex);
            zIndex = window.getZIndex();
            if (window.isModal()) {
                lastModal = window;
            }
            if (window instanceof ErrorDialog) {
                assertTrue(window.isModal());
            }
        }

        // the modal dialog with the highest z-index should be the modal context root.
        assertEquals(lastModal, app.getModalContextRoot());
    }

    /**
     * Creates a new modal {@link PopupWindow}.
     *
     * @return a new window
     */
    private PopupWindow createWindow() {
        return new PopupWindow("") {
            {
                setModal(true);
            }
        };
    }
}
