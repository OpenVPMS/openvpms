/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.style;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Helper to load style sheet resources.
 *
 * @author Tim Anderson
 */
class StyleSheetResources {

    /**
     * Returns properties for the specified resource.
     *
     * @param name     the resource name
     * @param required determines if the resource is required or not
     * @return the resource, or {@code null} if it does exist and wasn't required
     * @throws IOException         if an I/O error occurs
     * @throws StyleSheetException if the resource cannot be found and is required
     */
    public static Map<String, String> getProperties(String name, boolean required) throws IOException {
        Map<String, String> result = null;
        InputStream stream = getResource(name, required);
        if (stream != null) {
            result = new HashMap<>();
            Properties properties = new Properties();
            properties.load(stream);
            for (String key : properties.stringPropertyNames()) {
                result.put(key, (String) properties.get(key));
            }
        }
        return result;
    }

    /**
     * Returns the stream for a resource.
     *
     * @param resource the resource path
     * @param required determines if the resource is required or not
     * @return the stream, or {@code null} if its not required
     * @throws StyleSheetException if the resource cannot be found and is required
     */
    public static InputStream getResource(String resource, boolean required) {
        InputStream stream = StyleSheetResources.class.getClassLoader().getResourceAsStream(resource);
        if (stream == null) {
            stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
        }
        if (stream == null) {
            if (required) {
                throw new StyleSheetException(StyleSheetException.ErrorCode.ResourceNotFound, resource);
            }
        }
        return stream;
    }
}