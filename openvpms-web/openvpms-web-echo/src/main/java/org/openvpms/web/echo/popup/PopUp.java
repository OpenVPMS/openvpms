/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.popup;

import nextapp.echo2.app.Component;

/**
 * Popup to enable {@link PopUpPeer} to be used to specify a modified javascript file,
 * <em>org/openvpms/web/resource/js/popup.js</em>.
 * <br/>
 * The binding is specified in <em>META-INF\nextapp\echo2\SynchronizePeerBindings.properties</em>
 *
 * @author Tim Anderson
 */
public class PopUp extends echopointng.PopUp {

    /**
     * Auto-position property name.
     */
    public static final String AUTO_POSITION = "autoPosition";

    /**
     * Constructs a {@link PopUp}.
     */
    public PopUp() {
        super();
    }

    /**
     * Constructs a {@link PopUp} with the specified target and popup component in place.
     *
     * @param targetComponent the target component of the popup
     * @param popUpComponent  the component to be shown in the popup box
     */
    public PopUp(Component targetComponent, Component popUpComponent) {
        super(targetComponent, popUpComponent);
    }


    /**
     * Determines if the popup can be automatically repositioned, if it cannot be displayed without clipping or
     * scrollbars in its default position.
     *
     * @return {@code true} if the popup can be automatically repositioned, otherwise false
     */
    public boolean getAutoPosition() {
        return this.getProperty(AUTO_POSITION, false);
    }

    /**
     * Determines if the popup can be automatically repositioned, if it cannot be displayed without clipping or
     * scrollbars in its default position.
     *
     * @param reposition if {@code true}, attempt to automatically reposition the popup, otherwise leave in its default
     *                   position
     */
    public void setAutoPosition(boolean reposition) {
        setProperty(AUTO_POSITION, reposition);
    }
}