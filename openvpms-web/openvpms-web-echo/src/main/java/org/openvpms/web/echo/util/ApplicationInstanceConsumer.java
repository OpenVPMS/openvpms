/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.TaskQueueHandle;
import nextapp.echo2.app.WindowPane;

import java.util.function.Consumer;

/**
 * A {@link Runnable} that runs an underlying instance via an {@link ApplicationInstance}
 * {@link ApplicationInstance#enqueueTask(TaskQueueHandle, Runnable) task queue}.
 * <p/>
 * This allows the {@link Runnable} to update the user interface.
 * <p/>
 * Note that instances created prior to a {@link WindowPane} being displayed will have their execution suspended until
 * the dialog is closed, to avoid unwanted UI synchronisation events being generated<br/>
 * By default, any invocations to {@link #accept(Object)} during this time will be discarded, unless a
 * {@link TaskQueues.QueueMode} is specified at construction.
 *
 * @author Tim Anderson
 */
public class ApplicationInstanceConsumer<T> extends ApplicationInstanceFunctor implements Consumer<T> {

    /**
     * The consumer to delegate to.
     */
    private final Consumer<T> consumer;

    /**
     * Constructs an {@link ApplicationInstanceConsumer}.
     *
     * @param consumer the consumer
     */
    public ApplicationInstanceConsumer(Consumer<T> consumer) {
        this(0, TaskQueues.QueueMode.DISCARD, consumer);
    }

    /**
     * Constructs an {@link ApplicationInstanceConsumer}.
     *
     * @param interval  the interval in seconds between asynchronous callbacks from the client to check
     *                  for queued tasks produced by this, or {@code 0} to use the default
     * @param queueMode determines how tasks are queued while the queue is stopped
     * @param consumer  the consumer
     */
    public ApplicationInstanceConsumer(int interval, TaskQueues.QueueMode queueMode, Consumer<T> consumer) {
        super(interval, queueMode);
        this.consumer = consumer;
    }

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    @Override
    public void accept(T t) {
        enqueueTask(() -> consumer.accept(t));
    }

}
