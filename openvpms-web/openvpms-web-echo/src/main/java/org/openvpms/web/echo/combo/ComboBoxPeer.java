/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.combo;

import echopointng.ui.util.RenderingContext;
import nextapp.echo2.app.Component;
import nextapp.echo2.webrender.Service;
import nextapp.echo2.webrender.WebRenderServlet;
import nextapp.echo2.webrender.service.JavaScriptService;
import org.openvpms.web.echo.popup.PopUpPeer;
import org.w3c.dom.Node;

/**
 * Replaces the EPNG ComboxBoxPeer javascript with a fixed version.
 *
 * @author Tim Anderson
 */
public class ComboBoxPeer extends echopointng.ui.syncpeer.ComboBoxPeer {

    private static final Service SERVICE = JavaScriptService.forResource("EPNG.ComboBox", "/org/openvpms/web/echo/js/ComboBox.js");

    static {
        WebRenderServlet.getServiceRegistry().remove(COMBOBOX_SERVICE);
        WebRenderServlet.getServiceRegistry().add(SERVICE);
        WebRenderServlet.getServiceRegistry().add(PopUpPeer.UTILS_SERVICE); // needed for getScrollParent()
    }

    /**
     * This is the rendering method you must implement if you are to use {@code AbstractEchoPointPeer}.
     *
     * @param rc        the RenderingContext to use
     * @param parent    the parent Node to put content into
     * @param component the component
     */
    @Override
    public void renderHtml(RenderingContext rc, Node parent, Component component) {
        super.renderHtml(rc, parent, component);
        rc.addLibrary(PopUpPeer.UTILS_SERVICE);
    }

}
