/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.error;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.text.StringEscapeUtils;

/**
 * Implementation of {@link ErrorHandler} that simply logs errors.
 *
 * @author Tim Anderson
 */
public class LoggingErrorHandler extends ErrorHandler {

    /**
     * Singleton instance.
     */
    static final ErrorHandler INSTANCE = new LoggingErrorHandler();

    /**
     * The logger.
     */
    private static final Log log = LogFactory.getLog(LoggingErrorHandler.class);

    /**
     * Handles an error.
     *
     * @param cause the cause of the error
     */
    @Override
    public void error(Throwable cause) {
        log.error(cause.getMessage(), cause);
    }

    /**
     * Handles an error.
     *
     * @param title    the error title. May be {@code null}
     * @param message  the error message
     * @param html     if {@code true}, the message is an html fragment, else it is plain text
     * @param cause    the cause. May be {@code null}
     * @param listener the listener. May be {@code null}
     */
    @Override
    public void error(String title, String message, boolean html, Throwable cause, Runnable listener) {
        if (html) {
            message = StringEscapeUtils.unescapeHtml4(message);
        }
        log.error(message, cause);
        if (listener != null) {
            listener.run();
        }
    }
}
