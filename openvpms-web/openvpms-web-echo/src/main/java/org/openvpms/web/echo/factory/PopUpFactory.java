/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.factory;

import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Grid;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.echo.popup.PopUp;
import org.openvpms.web.echo.util.StyleSheetHelper;

import java.util.Arrays;

/**
 * Factory {@link PopUp} instances.
 *
 * @author Tim Anderson
 */
public class PopUpFactory {

    /**
     * Default constructor.
     */
    private PopUpFactory() {
        super();
    }

    /**
     * Creates a popup that displays multi-line notes.
     *
     * @param notes the notes
     * @return a new popup
     */
    public static PopUp createPopUpNotes(String notes) {
        PopUp popUp = new PopUp();
        Label text = LabelFactory.text(notes, true);
        // Need to set the text to wrap, otherwise the popup will render as wide as possible. However, this means
        // that it renders the popup width too small. As a workaround, set the popup width based on the length
        // of the longest line in the string.

        int minWidth = 5;
        int maxWidth = 20;
        int width;
        int longestLine = Arrays.stream(StringUtils.split(notes, '\n'))
                .map(String::length)
                .max(Integer::compareTo)
                .orElse(0);
        if (longestLine > maxWidth) {
            width = maxWidth;
        } else {
            width = Math.max(longestLine, minWidth);
        }
        Grid grid = new Grid();
        grid.setWidth(new Extent(width * StyleSheetHelper.getProperty("font.size", 10)));
        grid.add(text);
        popUp.setPopUp(grid);
        popUp.setStyleName("PopUpNotes");
        return popUp;
    }
}