/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.image;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.StreamImageReference;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * An {@link StreamImageReference} intended for small images used as thumbnails.
 *
 * @author Tim Anderson
 */
public class ThumbnailImageReference extends StreamImageReference {

    /**
     * The image content.
     */
    private final byte[] content;

    /**
     * The mime type.
     */
    private final String mimeType;

    /**
     * The image reference identifier.
     */
    private final String id;

    /**
     * Constructs a {@link ThumbnailImageReference}.
     *
     * @param content  the image content
     * @param mimeType the image mime type
     */
    public ThumbnailImageReference(byte[] content, String mimeType) {
        this.id = ApplicationInstance.generateSystemId();
        this.content = content;
        this.mimeType = mimeType;
    }

    /**
     * Returns the valid RFC 1521 image content type (e.g., image/png,
     * image/jpeg, image/gif, etc) of the image.
     *
     * @return the content type of the image
     */
    @Override
    public String getContentType() {
        return mimeType;
    }

    /**
     * Renders the image data in its native format to the given output stream.
     *
     * @param out the output stream to write the image
     */
    @Override
    public void render(OutputStream out) throws IOException {
        IOUtils.copy(new ByteArrayInputStream(content), out);
    }

    /**
     * Returns an identifier that is unique within the
     * {@code ApplicationInstance} with which the implementor will be
     * used.
     */
    @Override
    public String getRenderId() {
        return id;
    }

    /**
     * Creates an {@link ThumbnailImageReference} from a stream.
     *
     * @param stream   the stream
     * @param mimeType the mime type
     * @return a new image reference, or {@code null} if the stream cannot be read
     */
    public static ThumbnailImageReference create(InputStream stream, String mimeType) {
        try {
            byte[] content = IOUtils.toByteArray(stream);
            if (content.length > 0) {
                return new ThumbnailImageReference(content, mimeType);
            }
        } catch (Throwable exception) {
            // no-op
        }
        return null;
    }

}
