/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import echopointng.command.JavaScriptEval;

/**
 * Helper to create JavaScript to click a link.
 * Usage:<br/>
 * {@code ApplicationInstance.getActive().enqueueCommand(new LinkClicker(url));}
 *
 * @author Tim Anderson
 */
public class LinkClicker extends JavaScriptEval {

    /**
     * Constructs a {@link LinkClicker}.
     *
     * @param url the link
     */
    public LinkClicker(String url) {
        super(null);
        String js = "var a = document.createElement('a');\n" +
                    "a.href = \"" + url + "\";\n" +
                    "a.click();";
        setJavaScript(js);
    }
}