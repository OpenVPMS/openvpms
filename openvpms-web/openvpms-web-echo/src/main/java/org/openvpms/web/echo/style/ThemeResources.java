/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.style;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Default implementation of {@link Themes}.
 * <p/>
 * This loads {@link Theme} resources from the classpath. located in <em>style/</em>.
 * <p/>
 * Theme resources must use the naming convention: <em>theme-&lt;id&gt;.properties</em>
 *
 * @author Tim Anderson
 */
public class ThemeResources implements Themes {

    /**
     * The themes.
     */
    private final Map<String, Theme> themes = new HashMap<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ThemeResources.class);

    /**
     * Constructs a {@link ThemeResources}.
     */
    public ThemeResources() {
        load();
    }

    /**
     * Returns the theme with the given identifier.
     *
     * @param id the theme identifier
     * @return the corresponding theme, or {@code null} if none is found
     */
    @Override
    public synchronized Theme getTheme(String id) {
        return themes.get(id);
    }

    /**
     * Returns the themes, keyed on identifier.
     *
     * @return the themes
     */
    @Override
    public synchronized Map<String, Theme> getThemes() {
        return Collections.unmodifiableMap(themes);
    }

    /**
     * Loads themes.
     */
    public void load() {
        Map<String, Theme> map = new HashMap<>();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            Resource[] resources = resolver.getResources("classpath*:style/theme-*.properties");
            for (Resource resource : resources) {
                String id = getId(resource);
                if (id != null) {
                    load(id, resource, map);
                }
            }
        } catch (IOException exception) {
            log.warn("Failed to load themes: {}", exception.getMessage(), exception);
        }
        if (!map.isEmpty()) {
            synchronized (this) {
                themes.clear();
                themes.putAll(map);
            }
        }
    }

    /**
     * Returns the theme identifier from a resource.
     *
     * @param resource the resource
     * @return the theme identifier, or {@code null} if the resource is not a theme
     */
    private String getId(Resource resource) {
        String id = null;
        String fileName = resource.getFilename();
        if (fileName != null) {
            fileName = fileName.toLowerCase();
            id = StringUtils.substringAfter(fileName.toLowerCase(), "theme-");
            id = StringUtils.substringBefore(id, ".properties");
            id = StringUtils.trimToNull(id);
        }
        return id;
    }

    /**
     * Loads a theme resource.
     *
     * @param id       the theme identifier
     * @param resource the resource to load
     * @param themes   collects the themes
     */
    private void load(String id, Resource resource, Map<String, Theme> themes) {
        Properties properties = new Properties();
        try (InputStream stream = resource.getInputStream()) {
            Map<String, String> map = new HashMap<>();
            properties.load(stream);
            String name = null;
            for (String key : properties.stringPropertyNames()) {
                if ("name".equals(key)) {
                    name = (String) properties.get(key);
                } else {
                    map.put(key, (String) properties.get(key));
                }
            }
            if (name == null) {
                name = id;
            }
            themes.put(id, new Theme(id, name, map));
        } catch (IOException exception) {
            log.warn("Failed to load theme from {}: {}", resource.getFilename(), exception.getMessage(), exception);
        }
    }
}
