/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Font;
import nextapp.echo2.app.Insets;
import nextapp.echo2.app.Label;
import org.openvpms.web.echo.spring.SpringApplicationInstance;
import org.openvpms.web.echo.style.Style;
import org.openvpms.web.echo.style.UserStyleSheets;

/**
 * Stylesheet helper methods.
 *
 * @author Tim Anderson
 */
public class StyleSheetHelper {

    /**
     * Returns the named property.
     *
     * @param name the property name
     * @return the property value, or {@code null} if the property doesn't exist
     */
    public static String getProperty(String name) {
        return getProperty(name, null);
    }

    /**
     * Returns the named property.
     *
     * @param name         the property name
     * @param defaultValue the default value, if the property doesn't exist
     * @return the property value, or {@code defaultValue} if the property doesn't exist
     */
    public static String getProperty(String name, String defaultValue) {
        String result = defaultValue;
        Style style = getStyle();
        if (style != null) {
            result = style.getProperty(name, defaultValue);
        }
        return result;
    }

    /**
     * Returns the default length for numeric fields.
     *
     * @return the default length
     */
    public static int getNumericLength() {
        return getProperty("numeric.length", 14);
    }

    /**
     * Returns the named property.
     *
     * @param name         the property name
     * @param defaultValue the default value, if the property doesn't exist
     * @return the property value, or {@code defaultValue} if the property doesn't exist
     */
    public static int getProperty(String name, int defaultValue) {
        int result = defaultValue;
        Style style = getStyle();
        if (style != null) {
            result = style.getProperty(name, defaultValue);
        }
        return result;
    }

    /**
     * Returns the default font.
     *
     * @return the default font, or {@code null} if it cannot be determined
     */
    public static Font getDefaultFont() {
        Font result = null;
        Style style = getStyle();

        if (style != null) {
            nextapp.echo2.app.Style labelStyle = style.getStylesheet().getStyle(Label.class, "default");
            if (labelStyle != null) {
                result = (Font) labelStyle.getProperty("font");
            }
        }
        return result;
    }

    /**
     * Returns the value of an extent property for a given component style.
     *
     * @param component the component class
     * @param styleName the style name
     * @param name      the property name
     * @return the extent, or {@code null} if the style doesn't exist
     */
    public static Extent getExtent(Class<?> component, String styleName, String name) {
        return (Extent) getProperty(component, styleName, name);
    }

    /**
     * Returns the value of an insets property for a given component style.
     *
     * @param component the component class
     * @param styleName the style name
     * @param name      the property name
     * @return the insets, or {@code null} if the style doesn't exist
     */
    public static Insets getInsets(Class<?> component, String styleName, String name) {
        return (Insets) getProperty(component, styleName, name);
    }

    /**
     * Returns the value of a property for a given component style.
     *
     * @param component the component class
     * @param styleName the style name
     * @param name      the property name
     * @return the value, or {@code null} if the style doesn't exist
     */
    public static Object getProperty(Class<?> component, String styleName, String name) {
        Object result = null;
        Style style = getStyle();
        if (style != null) {
            nextapp.echo2.app.Style s = style.getStylesheet().getStyle(component, styleName);
            if (s != null) {
                result = s.getProperty(name);
            }
        }
        return result;
    }

    /**
     * Returns the current style.
     *
     * @return the current style, or {@code null} if there is no {@link SpringApplicationInstance} registered
     */
    protected static Style getStyle() {
        if (ApplicationInstance.getActive() instanceof SpringApplicationInstance) {
            SpringApplicationInstance app = (SpringApplicationInstance) ApplicationInstance.getActive();
            UserStyleSheets styleSheets = app.getApplicationContext().getBean(UserStyleSheets.class);
            return styleSheets.getStyle();
        }
        return null;
    }

}
