/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.frame;

import echopointng.ContainerEx;
import echopointng.EPNG;
import echopointng.HttpPaneEx;
import echopointng.able.BackgroundImageable;
import echopointng.able.Stretchable;
import echopointng.ui.resource.Resources;
import echopointng.ui.syncpeer.AbstractEchoPointContainerPeer;
import echopointng.ui.syncpeer.ContainerExPeer;
import echopointng.ui.util.CssStyleEx;
import echopointng.ui.util.Render;
import echopointng.ui.util.RenderingContext;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.ContentPane;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Style;
import nextapp.echo2.app.update.ServerComponentUpdate;
import nextapp.echo2.webcontainer.ActionProcessor;
import nextapp.echo2.webcontainer.ContainerInstance;
import nextapp.echo2.webcontainer.PartialUpdateParticipant;
import nextapp.echo2.webcontainer.PropertyUpdateProcessor;
import nextapp.echo2.webcontainer.RenderContext;
import nextapp.echo2.webcontainer.propertyrender.ExtentRender;
import nextapp.echo2.webrender.ServerMessage;
import nextapp.echo2.webrender.Service;
import nextapp.echo2.webrender.WebRenderServlet;
import nextapp.echo2.webrender.service.JavaScriptService;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Peer for {@link MonitoredFrame}.
 * <p/>
 * Largely copied from {@link ContainerExPeer}.
 *
 * @author Tim Anderson
 */
public class MonitoredFramePeer extends AbstractEchoPointContainerPeer
        implements PropertyUpdateProcessor, ActionProcessor {

    /**
     * Service to provide supporting JavaScript library.
     */
    private static final Service FRAME_SERVICE
            = JavaScriptService.forResource("MonitoredFrame", "/org/openvpms/web/echo/js/MonitoredFrame.js");

    static {
        WebRenderServlet.getServiceRegistry().add(FRAME_SERVICE);
    }

    /**
     * Constructs a {@link MonitoredFramePeer}.
     */
    public MonitoredFramePeer() {
        super();
        partialUpdateManager.add(ContainerEx.PROPERTY_HORIZONTAL_SCROLL, new PartialUpdateParticipant() {

            public void renderProperty(RenderContext rc, ServerComponentUpdate update) {
                renderScrollDirective(rc, update.getParent(), true);
            }

            public boolean canRenderProperty(RenderContext rc, ServerComponentUpdate update) {
                return true;
            }
        });
        partialUpdateManager.add(ContentPane.PROPERTY_VERTICAL_SCROLL, new PartialUpdateParticipant() {

            public void renderProperty(RenderContext rc, ServerComponentUpdate update) {
                renderScrollDirective(rc, update.getParent(), false);
            }

            public boolean canRenderProperty(RenderContext rc, ServerComponentUpdate update) {
                return true;
            }
        });
    }

    @Override
    public void processAction(ContainerInstance containerInstance, Component component, Element element) {
        String value = element.getAttribute(ActionProcessor.ACTION_VALUE);
        containerInstance.getUpdateManager().getClientUpdateManager().setComponentAction(
                component, MonitoredFrame.MESSAGE, value);
    }

    /**
     * @see nextapp.echo2.webcontainer.PropertyUpdateProcessor#processPropertyUpdate(nextapp.echo2.webcontainer.ContainerInstance,
     * nextapp.echo2.app.Component, org.w3c.dom.Element)
     */
    public void processPropertyUpdate(ContainerInstance ci, Component component, Element propertyElement) {
        if ("horizontalScroll".equals(propertyElement.getAttribute(PropertyUpdateProcessor.PROPERTY_NAME))) {
            Extent newValue = ExtentRender.toExtent(propertyElement.getAttribute(PropertyUpdateProcessor.PROPERTY_VALUE));
            ci.getUpdateManager().getClientUpdateManager().setComponentProperty(component, ContainerEx.PROPERTY_HORIZONTAL_SCROLL, newValue);
        } else if ("verticalScroll".equals(propertyElement.getAttribute(PropertyUpdateProcessor.PROPERTY_NAME))) {
            Extent newValue = ExtentRender.toExtent(propertyElement.getAttribute(PropertyUpdateProcessor.PROPERTY_VALUE));
            ci.getUpdateManager().getClientUpdateManager().setComponentProperty(component, ContainerEx.PROPERTY_VERTICAL_SCROLL, newValue);
        }
    }

    /**
     * @see echopointng.ui.syncpeer.AbstractEchoPointPeer#renderHtml(echopointng.ui.util.RenderingContext,
     * org.w3c.dom.Node, nextapp.echo2.app.Component)
     */
    public void renderHtml(RenderingContext rc, Node parentNode, Component component) {
        rc.addLibrary(FRAME_SERVICE);
        rc.addLibrary(Resources.EP_SCRIPT_SERVICE);
        rc.addLibrary(Resources.EP_STRETCH_SERVICE);

        Style fallbackStyle = EPNG.getFallBackStyle(component);

        CssStyleEx style = new CssStyleEx(component, fallbackStyle);
        Render.asBackgroundImageable(rc, style, (BackgroundImageable) component, fallbackStyle);
        //
        style.setAttribute("position", "absolute");
        style.setAttribute("width", "100%");
        style.setAttribute("height", "100%");
        style.setAttribute("overflow", "auto");
        Render.layoutFix(rc, style);

        Element paneE = rc.createE("div");
        parentNode.appendChild(paneE);
        paneE.setAttribute("id", rc.getElementId());
        paneE.setAttribute("style", style.renderInline());
        rc.addStandardWebSupport(paneE);

        // render an IFRAME to display the URI
        style = new CssStyleEx();
        style.setAttribute("position", "relative");
        style.setAttribute("width", "100%");
        style.setAttribute("height", "100%");
        style.setAttribute("border-style", "none");

        Element iframeE = rc.createE("iframe");
        iframeE.setAttribute("id", rc.getElementId() + "_frame");
        iframeE.setAttribute("style", style.renderInline());
        iframeE.setAttribute("src", (String) rc.getRP(HttpPaneEx.PROPERTY_URI, fallbackStyle));
        paneE.appendChild(iframeE);

        renderInitDirective(rc, component);
    }

    /**
     * @see echopointng.ui.syncpeer.AbstractEchoPointPeer#renderDispose(nextapp.echo2.webcontainer.RenderContext,
     * nextapp.echo2.app.update.ServerComponentUpdate,
     * nextapp.echo2.app.Component)
     */
    public void renderDispose(RenderContext rc, ServerComponentUpdate update, Component component) {
        super.renderDispose(rc, update, component);
        rc.getServerMessage().addLibrary(Resources.EP_SCRIPT_SERVICE.getId());
        rc.getServerMessage().addLibrary(Resources.EP_STRETCH_SERVICE.getId());
        rc.getServerMessage().addLibrary(FRAME_SERVICE.getId());
        renderDisposeDirective(rc, component);
    }

    /**
     * a replacement that uses DIV instead of BDO for containership
     *
     * @see echopointng.ui.syncpeer.AbstractEchoPointPeer#renderReplaceableChild(nextapp.echo2.webcontainer.RenderContext,
     * nextapp.echo2.app.update.ServerComponentUpdate, org.w3c.dom.Node,
     * nextapp.echo2.app.Component)
     */
    protected Element renderReplaceableChild(RenderContext rc, ServerComponentUpdate update, Node parentNode, Component child) {
        throw new IllegalStateException("Not supported");
    }

    /**
     *
     */
    private void renderScrollDirective(RenderContext rc, Component component, boolean horizontal) {
        ServerMessage serverMessage = rc.getServerMessage();
        Element scrollElement = serverMessage.appendPartDirective(ServerMessage.GROUP_ID_POSTUPDATE, "MonitoredFrame.MessageProcessor",
                                                                  horizontal ? "scroll-horizontal" : "scroll-vertical");
        Extent position = (Extent) component.getRenderProperty(horizontal ? ContainerEx.PROPERTY_HORIZONTAL_SCROLL
                                                                          : ContainerEx.PROPERTY_VERTICAL_SCROLL, new Extent(0));
        scrollElement.setAttribute("eid", ContainerInstance.getElementId(component));
        scrollElement.setAttribute("position", ExtentRender.renderCssAttributeValue(position));
    }

    /**
     *
     */
    private void renderDisposeDirective(RenderContext rc, Component component) {
        ServerMessage serverMessage = rc.getServerMessage();
        Element itemizedUpdateElement = serverMessage.getItemizedDirective(ServerMessage.GROUP_ID_PREREMOVE, "MonitoredFrame.MessageProcessor",
                                                                           "dispose", new String[0], new String[0]);
        Element itemElement = serverMessage.getDocument().createElement("item");
        itemElement.setAttribute("eid", ContainerInstance.getElementId(component));
        itemizedUpdateElement.appendChild(itemElement);
    }

    /**
     *
     */
    private void renderInitDirective(RenderingContext rc, Component component) {
        String elementId = ContainerInstance.getElementId(component);
        ServerMessage serverMessage = rc.getServerMessage();

        Element itemizedUpdateElement = serverMessage.getItemizedDirective(ServerMessage.GROUP_ID_POSTUPDATE, "MonitoredFrame.MessageProcessor",
                                                                           "init", new String[0], new String[0]);
        Element itemElement = serverMessage.getDocument().createElement("item");
        itemElement.setAttribute("eid", elementId);
        Extent horizontalScroll = (Extent) component.getRenderProperty(ContainerEx.PROPERTY_HORIZONTAL_SCROLL);
        if (horizontalScroll != null && horizontalScroll.getValue() != 0) {
            itemElement.setAttribute("horizontal-scroll", ExtentRender.renderCssAttributeValue(horizontalScroll));
        }
        Extent verticalScroll = (Extent) component.getRenderProperty(ContainerEx.PROPERTY_VERTICAL_SCROLL);
        if (verticalScroll != null && verticalScroll.getValue() != 0) {
            itemElement.setAttribute("vertical-scroll", ExtentRender.renderCssAttributeValue(verticalScroll));
        }
        if (rc.getRP(Stretchable.PROPERTY_HEIGHT_STRETCHED, false)) {
            itemElement.setAttribute("heightStretched", String.valueOf(true));
            itemElement.setAttribute("minimumStretchedHeight", getExtentPixels(rc.getRP(Stretchable.PROPERTY_MINIMUM_STRETCHED_HEIGHT, null)));
            itemElement.setAttribute("maximumStretchedHeight", getExtentPixels(rc.getRP(Stretchable.PROPERTY_MAXIMUM_STRETCHED_HEIGHT, null)));
        }

        itemizedUpdateElement.appendChild(itemElement);
    }

    private static String getExtentPixels(Object extent) {
        if (extent instanceof Extent) {
            return String.valueOf(((Extent) extent).getValue());
        }
        return null;
    }

}
