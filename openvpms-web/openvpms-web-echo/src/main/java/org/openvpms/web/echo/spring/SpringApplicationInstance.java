/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.spring;

import nextapp.echo2.app.ApplicationInstance;
import org.openvpms.web.echo.util.TaskQueues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An {@code ApplicationInstance} for integrating Echo with Spring.
 *
 * @author Tim Anderson
 */
public abstract class SpringApplicationInstance extends ApplicationInstance {

    /**
     * The task queues.
     */
    private final TaskQueues taskQueues;

    /**
     * The application context.
     */
    private ApplicationContext context;

    /**
     * Determines if this has been disposed of.
     */
    private boolean disposed = false;

    /**
     * Instance counter for debugging purposes.
     */
    private static final AtomicInteger count = new AtomicInteger(0);

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SpringApplicationInstance.class);

    /**
     * Constructs a {@link SpringApplicationInstance}.
     */
    public SpringApplicationInstance() {
        taskQueues = new TaskQueues(this);
        log.debug("Active applications: {}", count.incrementAndGet());
    }

    /**
     * Sets the application context.
     *
     * @param context the application context
     */
    public void setApplicationContext(ApplicationContext context) {
        this.context = context;
    }

    /**
     * Returns the application context.
     *
     * @return the application context
     */
    public ApplicationContext getApplicationContext() {
        return context;
    }

    /**
     * Returns the task queues.
     *
     * @return the task queues
     */
    public TaskQueues getTaskQueues() {
        return taskQueues;
    }

    /**
     * Locks the application, until the user re-enters their password.
     * <p/>
     * Note that this method may be invoked outside a servlet request.
     */
    public abstract void lock();

    /**
     * Unlocks the application.
     * <p/>
     * Note that this method may be invoked outside a servlet request.
     */
    public abstract void unlock();

    /**
     * Invoked when the application is disposed and will not be used again.
     */
    @Override
    public synchronized void dispose() {
        if (!disposed) {
            disposed = true;
            super.dispose();
            taskQueues.dispose();
            log.debug("Active applications: {}", count.decrementAndGet());
        }
    }

}
