/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.style;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Implementation of {@link StyleSheetProperties} that uses resources.
 *
 * @author Tim Anderson
 */
public class ResourceStyleSheetProperties implements StyleSheetProperties {

    /**
     * The default style resource name.
     */
    private final String defaultName;

    /**
     * The override style resource name. May be {@code null}
     */
    private final String overrideName;

    /**
     * The properties.
     */
    private final Map<String, String> properties;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ResourceStyleSheetProperties.class);

    /**
     * Constructs a {@link ResourceStyleSheetProperties}.
     *
     * @param defaultName  the default style resource name, minus any extension
     * @param overrideName the override style resource name, minus any extension. May be {@code null}
     * @throws IOException for any I/O error
     */
    public ResourceStyleSheetProperties(String defaultName, String overrideName) throws IOException {
        this.defaultName = defaultName;
        this.overrideName = overrideName;
        properties = StyleSheetResources.getProperties(defaultName + ".properties", true);
        if (overrideName != null) {
            String name = overrideName + ".properties";
            Map<String, String> overrides = StyleSheetResources.getProperties(name, false);
            if (overrides != null) {
                log.info("Overriding default stylesheet properties using " + name);
                properties.putAll(overrides);
            } else {
                log.info("No style overrides found for: " + name);
            }
        }
    }

    /**
     * Returns the default style sheet name.
     * <p/>
     * This is the resource name, minus any extension.
     *
     * @return the default style sheet name
     */
    @Override
    public String getDefaultName() {
        return defaultName;
    }

    /**
     * Returns the override style sheet name.
     * <p/>
     * This is the resource name, minus any extension.
     *
     * @return the default style sheet name
     */
    @Override
    public String getOverrideName() {
        return overrideName;
    }

    /**
     * Returns the style sheet properties.
     *
     * @return the properties
     */
    @Override
    public Map<String, String> getProperties() {
        return properties;
    }
}