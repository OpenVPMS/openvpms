/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.ApplicationInstance;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Periodically executes a {@link Runnable} using the Echo task queue.
 * <p/>
 * NOTE: while an interval is provided to determine the interval in seconds between asynchronous callbacks from the
 * client, Echo uses the minimum interval from all registered queues.<br/>
 * To work around this, task execution only occurs if the task hasn't been run since the last interval elapsed, despite
 * being queued.<br/>
 * This means that tasks may run later than expected. E.g. if the Echo callback is 5 seconds, but the task is scheduled
 * to run every 8, the task may run after 10 seconds and next after 20.
 *
 * @author Tim Anderson
 * @see TaskQueues
 */
public class PeriodicTask implements RestartableTask {

    /**
     * The task queue.
     */
    private final RestartableTaskQueue taskQueue;

    /**
     * The delay before the very first execution, in seconds.
     */
    private final int initialDelay;

    /**
     * Determine if the task only runs once.
     */
    private final boolean once;

    /**
     * The task to run.
     */
    private Runnable task;

    /**
     * Determines if is the very first execution of the task.
     */
    private boolean firstRun = true;

    /**
     * The time the task was started.
     */
    private long startTime;

    /**
     * The time the task was last run.
     */
    private long lastRun = 0;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PeriodicTask.class);

    /**
     * Constructs a {@link PeriodicTask}.
     *
     * @param app          the application instance
     * @param initialDelay the delay before the very first execution, in seconds
     * @param interval     the interval
     * @param task         the task to run
     * @param once         if {@code true}, only run the task once
     */
    protected PeriodicTask(ApplicationInstance app, int initialDelay, int interval, boolean once, Runnable task) {
        this.initialDelay = initialDelay;
        taskQueue = new RestartableTaskQueue(app, interval, false, TaskQueues.QueueMode.DISCARD);
        this.once = once;
        this.task = task;
    }

    /**
     * Starts the task.
     * <p/>
     * If this is the very first time the task was started, and an {@code initialDelay} was specified, the task
     * won't execute until the delay has expired.
     *
     * @return {@code true} if the task was started, {@code false} if it was already running
     */
    @Override
    public synchronized boolean start() {
        boolean started = taskQueue.start();
        if (started) {
            startTime = System.currentTimeMillis();
            lastRun = 0;
            queue();
        }
        log.debug("Started PeriodicTask: {}", started);
        return started;
    }

    /**
     * Restarts the task.
     */
    public void restart() {
        stop();
        start();
    }

    /**
     * Stops the task.
     *
     * @return {@code true} if the task was stopped, {@code false} if it was already stopped
     */
    @Override
    public synchronized boolean stop() {
        boolean stopped = taskQueue.stop();
        log.debug("Stopped PeriodicTask: {}", stopped);
        return stopped;
    }

    /**
     * Disposes of the task.
     */
    @Override
    public synchronized void dispose() {
        stop();
        taskQueue.dispose();
        task = null;
        log.debug("Disposed PeriodicTask");
    }

    /**
     * Determines if the task is running.
     *
     * @return {@code true} if the task is running
     */
    @Override
    public synchronized boolean isRunning() {
        return taskQueue.isRunning();
    }

    /**
     * Queues the task.
     */
    private void queue() {
        if (taskQueue.isRunning()) {
            taskQueue.queue(this::execute);
        }
    }

    /**
     * Executes the task.
     */
    private void execute() {
        Runnable runnable;
        synchronized (this) {
            runnable = task;
        }
        boolean run = false;
        if (runnable != null && shouldRun()) {
            try {
                runnable.run();
            } catch (Exception exception) {
                log.warn("Task execution failed", exception);
            }
            lastRun = System.currentTimeMillis();
            run = true;
        }
        if (run && once) {
            dispose();
        } else {
            queue();
        }
    }

    /**
     * Determines if the task should run.
     *
     * @return {@code true} if the task should run
     */
    private boolean shouldRun() {
        boolean run = false;
        int interval = taskQueue.getInterval();
        if (firstRun) {
            if (initialDelay > 0) {
                long diff = getDiffToNow(startTime);
                if (diff >= initialDelay) {
                    run = true;
                    firstRun = false;
                    log.debug("Running task after initialDelay of {}s expired {}s ago", initialDelay,
                              diff - initialDelay);
                } else {
                    log.debug("Not running task as initialDelay of {}s has not expired yet. Elapsed={}s",
                              initialDelay, diff);
                }
            } else {
                log.debug("Running PeriodicTask for first time");
                run = true;
                firstRun = false;
            }
        } else {
            long diff = getDiffToNow(lastRun);
            if (lastRun == 0) {
                log.debug("Running PeriodicTask");
                run = true;
            } else if (diff >= interval) {
                log.debug("Running PeriodicTask. It is scheduled to run every {}s, and last ran {}s ago",
                          interval, diff);
                run = true;
            } else {
                log.debug("Not running PeriodicTask. It is scheduled to run every {}s, but last ran {}s ago",
                          interval, diff);
            }
        }
        return run;
    }

    /**
     * Returns the difference between a time and now, in seconds.
     *
     * @param time the time
     * @return the difference, in seconds
     */
    private long getDiffToNow(long time) {
        return (System.currentTimeMillis() - time) / DateUtils.MILLIS_PER_SECOND;
    }
}
