/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

import nextapp.echo2.app.event.WindowPaneListener;
import org.openvpms.web.resource.i18n.Messages;


/**
 * Modal information dialog box.
 *
 * @author Tim Anderson
 */
public class InformationDialog extends MessageDialog {

    /**
     * The style name.
     */
    static final String STYLE = "InformationDialog";


    /**
     * Constructs an {@link InformationDialog}.
     *
     * @param message the message to display
     */
    public InformationDialog(String message) {
        this(Messages.get("informationdialog.title"), message);
    }

    /**
     * Constructs an {@link InformationDialog}.
     *
     * @param title   the dialog title
     * @param message the message to display
     */
    public InformationDialog(String title, String message) {
        super(title, message, STYLE, OK);
        setDefaultButton(OK_ID);
    }

    /**
     * Constructs an {@link InformationDialog}.
     *
     * @param builder the builder
     */
    protected InformationDialog(InformationDialogBuilder builder) {
        super(builder);
    }

    /**
     * Helper to show a new information dialog.
     *
     * @param message dialog message
     */
    public static void show(String message) {
        InformationDialog dialog = new InformationDialog(message);
        dialog.show();
    }

    /**
     * Helper to show a new information dialog.
     *
     * @param title   the dialog title
     * @param message dialog message
     */
    public static void show(String title, String message) {
        show(title, message, null);
    }

    /**
     * Helper to show a new information dialog.
     *
     * @param message  dialog message
     * @param listener the listener to notify when the dialog closes. May be {@code null}
     */
    public static void show(String message, WindowPaneListener listener) {
        show(null, message, listener);
    }

    /**
     * Helper to show a new information dialog.
     *
     * @param title    the dialog title. May be {@code null}
     * @param message  dialog message
     * @param listener the listener to notify when the dialog closes. May be {@code null}
     */
    public static void show(String title, String message, WindowPaneListener listener) {
        InformationDialog dialog = title != null ? new InformationDialog(title, message)
                                                 : new InformationDialog(message);
        if (listener != null) {
            dialog.addWindowPaneListener(listener);
        }
        dialog.show();
    }

    /**
     * Creates a builder for a new dialog.
     *
     * @return the builder
     */
    public static InformationDialogBuilder newDialog() {
        return new InformationDialogBuilder();
    }

}
