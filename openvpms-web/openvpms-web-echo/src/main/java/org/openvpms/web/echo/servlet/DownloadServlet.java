/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.servlet;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Command;
import nextapp.echo2.webcontainer.command.BrowserOpenWindowCommand;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.service.archetype.ArchetypeServiceHelper;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.web.echo.util.LinkClicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


/**
 * Download servlet. Downloads {@link Document}s to clients.
 *
 * @author Tim Anderson
 */
public class DownloadServlet extends HttpServlet {

    /**
     * The document handlers.
     */
    private DocumentHandlers handlers;

    /**
     * The set of temporary documents. These are deleted after being served.
     */
    private static final Set<Reference> tempDocs = Collections.synchronizedSet(new HashSet<>());

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DownloadServlet.class);

    /**
     * Initialises the servlet.
     *
     * @throws ServletException for any error
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        handlers = context.getBean(DocumentHandlers.class);
    }

    /**
     * Start a download, using the current {@link ApplicationInstance}.
     *
     * @param document the document to download
     */
    public static void startDownload(Document document) {
        ApplicationInstance app = ApplicationInstance.getActive();
        startDownload(document, app);
    }

    /**
     * Start a download.
     *
     * @param document the document to download
     * @param app      the application to enqueue the download command
     */
    public static void startDownload(Document document, ApplicationInstance app) {
        boolean isNew = document.isNew();
        if (isNew) {
            // need to save the document in order for it to be served.
            IArchetypeService service = ArchetypeServiceHelper.getArchetypeService();
            service.save(document);
            tempDocs.add(document.getObjectReference());
        }
        String uri = getDocumentURI(document.getObjectReference());
        Command command;
        if (Boolean.getBoolean("openvpms.download.window")) {
            // the original download behaviour
            command = new BrowserOpenWindowCommand(
                    uri, null,
                    "width=800,height=600,menubar=yes,toolbar=yes,location=yes,resizable=yes,scrollbars=yes");
        } else {
            // launch a command to evaluate javascript to click a link to download the file
            command = new LinkClicker(uri);
        }
        app.enqueueCommand(command);
    }

    /**
     * Returns a relative URI for a document.
     *
     * @param reference the document reference
     * @return the document URI
     */
    public static String getDocumentURI(Reference reference) {
        return ServletHelper.getRedirectURI("document") + "?archetype=" + reference.getArchetype()
               + "&id=" + reference.getId();
    }

    /**
     * Handles a GET request.
     * <p/>
     * If the request is incorrectly formatted, returns an HTTP "Bad Request"
     * message.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        IArchetypeService service = ArchetypeServiceHelper.getArchetypeService();
        String archetype = request.getParameter("archetype");
        long id = NumberUtils.toLong(request.getParameter("id"), 0);
        if (StringUtils.isEmpty(archetype) || id <= 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            IMObject object = service.get(archetype, id);
            if (object == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            } else if (!(object instanceof Document)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                serveDocument((Document) object, response, service);
            }
        }
    }

    /**
     * Serves a document
     *
     * @param doc      the document
     * @param response the response
     * @param service  the archetype service
     * @throws IOException for any I/O error
     */
    private void serveDocument(Document doc, HttpServletResponse response, IArchetypeService service)
            throws IOException {
        try {
            DocumentHandler handler = handlers.get(doc.getName(), doc.getArchetype(), doc.getMimeType());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + doc.getName() + "\"");
            response.setContentType(doc.getMimeType());
            response.setContentLength(doc.getSize());
            try (InputStream stream = handler.getContent(doc)) {
                IOUtils.copy(stream, response.getOutputStream());
            }

            if (tempDocs.remove(doc.getObjectReference())) {
                service.remove(doc);
            }
        } catch (OpenVPMSException exception) {
            log.error("Failed to serve document: name=" + doc.getName() + ", archetype=" + doc.getArchetype()
                      + ", mimeType=" + doc.getMimeType(), exception);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
