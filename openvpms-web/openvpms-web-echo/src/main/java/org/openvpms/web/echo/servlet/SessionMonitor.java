/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.servlet;

import nextapp.echo2.webrender.Connection;
import nextapp.echo2.webrender.WebRenderServlet;
import org.apache.commons.collections4.map.ReferenceMap;
import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.web.echo.spring.SpringApplicationInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.WEAK;
import static org.apache.commons.lang.time.DurationFormatUtils.formatDurationHMS;

/**
 * Monitors HTTP sessions, forcing idle sessions to expire.
 * <p/>
 * This is required as echo2 asynchronous tasks keep sessions alive, such that web.xml {@code <session-timeout/>}
 * has no effect.
 *
 * @author Tim Anderson
 */
public class SessionMonitor implements DisposableBean {

    /**
     * Represents a user session.
     */
    public static class Session {

        private final long id;

        private final String name;

        private final String host;

        private final Date loggedIn;

        private final Date lastAccessed;

        public Session(long id, String name, String host, Date loggedIn, Date lastAccessed) {
            this.id = id;
            this.name = name;
            this.host = host;
            this.loggedIn = loggedIn;
            this.lastAccessed = lastAccessed;
        }

        /**
         * Returns the monitor id, used to associate the session monitor with a session.
         *
         * @return the monitor id
         */
        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getHost() {
            return host;
        }

        public Date getLoggedIn() {
            return loggedIn;
        }

        public Date getLastAccessed() {
            return lastAccessed;
        }
    }

    /**
     * The default period before screen-lock, in minutes.
     */
    public static final int DEFAULT_AUTO_LOCK_INTERVAL = 5;

    /**
     * The default session inactivity period before logout, in minutes.
     */
    public static final int DEFAULT_AUTO_LOGOUT_INTERVAL = 30;

    /**
     * Counter for generating monitor ids.
     */
    private final AtomicLong counter = new AtomicLong();

    /**
     * The monitors, keyed on their sessions.
     */
    private final Map<HttpSession, Monitor> monitors = Collections.synchronizedMap(new WeakHashMap<>());

    /**
     * The executor, used to expire sessions.
     */
    private final ScheduledExecutorService executor;

    /**
     * The time in milliseconds before inactive sessions have their screens locked.
     */
    private volatile long autoLock = DEFAULT_AUTO_LOCK_INTERVAL * DateUtils.MILLIS_PER_MINUTE;

    /**
     * The time in milliseconds before inactive sessions are logged out.
     */
    private volatile long autoLogout = DEFAULT_AUTO_LOGOUT_INTERVAL * DateUtils.MILLIS_PER_MINUTE;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SessionMonitor.class);

    /**
     * Constructs an {@link SessionMonitor}.
     */
    public SessionMonitor() {
        executor = Executors.newSingleThreadScheduledExecutor();
        log.info("Using default session auto-lock time={} minutes", autoLock / DateUtils.MILLIS_PER_MINUTE);
        log.info("Using default session auto-logout time={} minutes", autoLogout / DateUtils.MILLIS_PER_MINUTE);
    }

    /**
     * Adds a session to monitor.
     *
     * @param session the session
     */
    public void addSession(HttpSession session) {
        Monitor monitor = new Monitor(session, counter.getAndIncrement());
        monitors.put(session, monitor);
        monitor.schedule();
    }

    /**
     * Stops monitoring a session.
     *
     * @param session the session to remove
     */
    public void removeSession(HttpSession session) {
        Monitor monitor = monitors.remove(session);
        if (monitor != null) {
            monitor.destroy();
        }
    }

    /**
     * Marks the current session as active.
     */
    public void active() {
        Connection connection = getConnection();
        if (connection != null) {
            HttpServletRequest request = connection.getRequest();
            active(request, getAuthentication());
        }
    }

    /**
     * Marks a session as active.
     *
     * @param request        the request
     * @param authentication the user authentication
     */
    public void active(HttpServletRequest request, Authentication authentication) {
        Monitor monitor = monitors.get(request.getSession());
        if (monitor != null) {
            monitor.active(request, authentication);
        }
    }

    /**
     * Determines if a session is locked.
     *
     * @param session the session
     * @return {@code true} if the session is locked
     */
    public boolean isLocked(HttpSession session) {
        Monitor monitor = monitors.get(session);
        return monitor != null && monitor.status != Status.UNLOCKED;
    }

    /**
     * Registers a new application.
     *
     * @param application the new application
     * @param session     the session that created the application
     */
    public void newApplication(SpringApplicationInstance application, HttpSession session) {
        Monitor monitor = monitors.get(session);
        if (monitor != null) {
            monitor.newApplication(application);
        }
    }

    /**
     * Returns the time that sessions may remain idle before they are locked.
     *
     * @return the timeout, in minutes. A value of {@code 0} indicates that sessions don't lock
     */
    public int getAutoLock() {
        return (int) (autoLock / DateUtils.MILLIS_PER_MINUTE);
    }

    /**
     * Sets the time that sessions may remain idle before they are locked.
     *
     * @param time the timeout, in minutes. A value of {@code 0} indicates that sessions don't lock
     */
    public void setAutoLock(int time) {
        setAutoLockMS(time * DateUtils.MILLIS_PER_MINUTE);
    }

    /**
     * Invoked by the application when it locks.
     */
    public void locked() {
        Connection connection = getConnection();
        if (connection != null) {
            Monitor monitor = monitors.get(connection.getRequest().getSession());
            if (monitor != null) {
                monitor.locked();
            }
        }
    }

    /**
     * Unlocks the current session.
     */
    public void unlock() {
        Connection connection = getConnection();
        if (connection != null) {
            Monitor monitor = monitors.get(connection.getRequest().getSession());
            if (monitor != null) {
                monitor.unlock();
            }
        }
    }

    /**
     * Returns the current session monitor id.
     * <p/>
     * This is the identifier of the monitor associated with the current session.<p/>
     * Note that this is different to the session identifier assigned by the servlet container, which can change.
     *
     * @return the current session monitor id, or {@code -1} if it is not known
     */
    public long getCurrentSessionMonitorId() {
        long result = -1;
        Connection connection = getConnection();
        if (connection != null) {
            HttpSession session = connection.getRequest().getSession(false);
            if (session != null) {
                Monitor monitor = monitors.get(session);
                if (monitor != null) {
                    result = monitor.getId();
                }
            }
        }
        return result;
    }

    /**
     * Sets the time that sessions may remain idle before they are logged out.
     *
     * @param time the timeout, in minutes. A value of {@code 0} indicates that sessions don't expire
     */
    public void setAutoLogout(int time) {
        setAutoLogoutMS(time * DateUtils.MILLIS_PER_MINUTE);
    }

    /**
     * Returns the current sessions.
     *
     * @return the current sessions
     */
    public List<Session> getSessions() {
        List<Session> result = new ArrayList<>();
        for (Monitor monitor : new ArrayList<>(monitors.values())) {
            Session session = monitor.getSession();
            if (session != null) {
                result.add(session);
            }
        }
        return result;
    }

    /**
     * Terminates a session.
     *
     * @param session the session
     * @return {@code true} if the session was terminated, {@code false} if it wasn't found
     */
    public boolean terminate(Session session) {
        Monitor found = monitors.values()
                .stream()
                .filter(monitor -> session.getId() == monitor.getId())
                .findFirst()
                .orElse(null);
        if (found != null) {
            found.invalidate();
        }
        return found != null;
    }

    /**
     * Destroys this.
     */
    @Override
    public void destroy() {
        log.info("Shutting down SessionMonitor");
        executor.shutdownNow();
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                log.error("Pool did not terminate");
            }
        } catch (InterruptedException exception) {
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
        monitors.clear();
    }

    /**
     * Returns the active connection.
     *
     * @return the active connection, or {@code null} if there is none
     */
    protected Connection getConnection() {
        return WebRenderServlet.getActiveConnection();
    }

    /**
     * Returns the authenticated user.
     *
     * @return the authenticated user. May be {@code null}
     */
    protected Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * Sets the time that sessions may remain idle before they are locked.
     *
     * @param time the timeout, in milliseconds. A value of {@code 0} indicates that sessions don't lock
     */
    protected void setAutoLockMS(long time) {
        if (time != autoLock) {
            autoLock = time;
            if (time == 0) {
                log.warn("Sessions configured to not auto-lock");
            } else {
                log.info("Using session auto-lock time={} minutes", time / DateUtils.MILLIS_PER_MINUTE);
            }
            reschedule();
        }
    }

    /**
     * Sets the time that sessions may remain idle before they are logged out.
     *
     * @param time the timeout, in milliseconds. A value of {@code 0} indicates that sessions don't expire
     */
    protected void setAutoLogoutMS(long time) {
        if (time != autoLogout) {
            autoLogout = time;
            if (time == 0) {
                log.warn("Sessions configured to not auto-logout");
            } else {
                log.info("Using session auto-logout time={} minutes", time / DateUtils.MILLIS_PER_MINUTE);
            }

            reschedule();
        }
    }

    /**
     * Reschedule existing sessions.
     */
    private void reschedule() {
        for (Object state : monitors.values().toArray()) {
            ((Monitor) state).reschedule();
        }
    }

    /**
     * Monitors session activity.
     */
    private class Monitor implements Runnable {

        /**
         * The session.
         */
        private final WeakReference<HttpSession> session;

        /**
         * The applications linked to the session.
         */
        private final ReferenceMap<SpringApplicationInstance, SpringApplicationInstance> apps
                = new ReferenceMap<>(WEAK, WEAK);

        /**
         * The monitor id.
         */
        private final long id;

        /**
         * Determines if the applications are currently locked.
         */
        private volatile Status status = Status.UNLOCKED;

        /**
         * The time the session was last accessed, in milliseconds.
         */
        private volatile long lastAccessedTime;

        /**
         * Used to cancel the scheduling.
         */
        private volatile ScheduledFuture<?> future;

        /**
         * The user, used for logging.
         */
        private volatile String user;

        /**
         * The user's IP address, used for logging.
         */
        private volatile String address;

        /**
         * Constructs a {@link Monitor}.
         *
         * @param session the session
         * @param id      the monitor id
         */
        public Monitor(HttpSession session, long id) {
            lastAccessedTime = System.currentTimeMillis();
            this.session = new WeakReference<>(session);
            this.id = id;
        }

        /**
         * Returns the monitor id.
         *
         * @return the monitor id
         */
        public long getId() {
            return id;
        }

        /**
         * Marks the session as active.
         *
         * @param request        the servlet request
         * @param authentication the user authentication
         */
        public void active(HttpServletRequest request, Authentication authentication) {
            lastAccessedTime = System.currentTimeMillis();
            boolean noUser = (user == null);
            Object principal = authentication.getPrincipal();
            String currentUser;
            if (principal instanceof UserDetails) {
                currentUser = ((UserDetails) principal).getUsername();
            } else {
                currentUser = principal.toString();
            }
            user = currentUser;
            address = request.getRemoteAddr();
            if (noUser && currentUser != null) {
                // only log if there wasn't a user assigned to the session previously
                log.info("Active session, user={}, address={}, status={}, monitor={}", currentUser, address, status,
                         id);
            }

            if (status == Status.LOCK_PENDING) {
                // if the UI was scheduled to lock, but there was user activity prior to the screen locking,
                // unlock it
                unlock();
            }
        }

        /**
         * Registers an application.
         *
         * @param application the application.
         */
        public synchronized void newApplication(SpringApplicationInstance application) {
            apps.put(application, application);
        }

        /**
         * Invoked by the executor.
         * Delegates to {@link #monitor}.
         */
        @Override
        public synchronized void run() {
            try {
                monitor();
            } catch (Throwable exception) {
                log.error(exception.getMessage(), exception);
            }
        }

        /**
         * Schedules the monitor.
         */
        public void schedule() {
            long time = autoLock;
            long logout = autoLogout;
            if (time == 0 || (logout != 0 && logout < time)) {
                time = logout;
            }
            if (time != 0) {
                schedule(time);
            }
        }

        /**
         * Reschedules the monitor.
         */
        public void reschedule() {
            ScheduledFuture<?> current = future;
            if (current != null) {
                current.cancel(false);
            }
            long timeout = autoLock;
            if (timeout == 0) {
                timeout = autoLogout;
            }
            if (timeout != 0) {
                run(); // will either expire or schedule
            }
        }

        /**
         * Unlocks applications linked to the session.
         */
        public synchronized void unlock() {
            try {
                SpringApplicationInstance[] list = apps.values().toArray(new SpringApplicationInstance[0]);
                for (SpringApplicationInstance app : list) {
                    if (app != null) {
                        app.unlock();
                    }
                }
            } finally {
                status = Status.UNLOCKED;
                reschedule();
            }
            log.info("Unlocked session for user={}, address={}, monitor={}", user, address, id);
        }

        /**
         * Returns the session details.
         *
         * @return the session details, or {@code null} if the session hasn't been fully established, or is
         * destroyed
         */
        public Session getSession() {
            Session result = null;
            String name = user;
            HttpSession httpSession = session.get();
            if (name != null && httpSession != null) {
                try {
                    Date created = new Date(httpSession.getCreationTime());
                    Date lastAccessed = new Date(lastAccessedTime);
                    result = new Session(id, name, address, created, lastAccessed);
                } catch (IllegalStateException ignore) {
                    // do nothing - session has been invalidated
                }
            }
            return result;
        }

        /**
         * Destroys the monitor.
         */
        public void destroy() {
            ScheduledFuture<?> current = future;
            if (current != null) {
                current.cancel(true);
            }
        }

        /**
         * Checks the session activity. This:
         * <ul>
         * <li>invalidates the session if {@link #autoLogout} is non-zero and it hasn't been accessed for
         * {@link #autoLogout} milliseconds</li>
         * <li>locks the session if {@link #autoLock} is non-zero, and the session hasn't been accessed for
         * {@link #autoLock} milliseconds.</li>
         * </ul>
         * If it has been accessed, then it reschedules itself for another {@code autoLock} milliseconds.
         */
        private void monitor() {
            long inactive = System.currentTimeMillis() - lastAccessedTime;
            long logout = autoLogout;
            long lock = autoLock;
            if (log.isDebugEnabled()) {
                log.debug("Monitor user={}, address={}, monitor={}, inactive={}, logout={}, lock={}", user, address,
                          id, formatDurationHMS(inactive), formatDurationHMS(logout), formatDurationHMS(lock));
            }
            if (logout != 0 && inactive >= logout) {
                // session is inactive, so kill it
                invalidate();
            } else {
                long reschedule = (logout != 0) ? logout - inactive : 0;
                if (lock != 0 && lock < logout) {
                    if (inactive >= lock) {
                        if (status == Status.UNLOCKED) {
                            lock();
                        }
                    } else {
                        reschedule = lock - inactive;
                    }
                }
                if (reschedule != 0) {
                    schedule(reschedule);
                }
            }
        }

        /**
         * Schedules the monitor.
         *
         * @param delay the milliseconds to delay before invoking {@link #run}.
         */
        private void schedule(long delay) {
            if (log.isDebugEnabled()) {
                log.debug("Scheduling monitor in {},  user={}, address={}, monitor={}", formatDurationHMS(delay), user,
                          address, id);
            }
            future = executor.schedule(this, delay, TimeUnit.MILLISECONDS);
        }

        /**
         * Locks applications linked to the session.
         */
        private synchronized void lock() {
            status = Status.LOCK_PENDING;
            int count = 0;
            SpringApplicationInstance[] list = apps.values().toArray(new SpringApplicationInstance[0]);
            for (SpringApplicationInstance app : list) {
                if (app != null) {
                    count++;
                    app.lock();
                }
            }
            log.info("Locking {} apps for user={}, address={}, monitor={}", count, user, address, id);
        }

        /**
         * Flags the session as locked.
         */
        private void locked() {
            status = Status.LOCKED;
            log.info("Locked session for user={}, address={}, monitor={}", user, address, id);
        }

        /**
         * Invalidates a session.
         */
        private void invalidate() {
            SpringApplicationInstance[] list = apps.values().toArray(new SpringApplicationInstance[0]);
            for (SpringApplicationInstance app : list) {
                if (app != null) {
                    try {
                        app.dispose();
                    } catch (Throwable exception) {
                        log.debug("Error disposing app", exception);
                    }
                }
            }

            HttpSession httpSession = session.get();
            if (httpSession != null) {
                session.clear();
                httpSession.invalidate();
                log.info("Invalidated session, monitor={} for user={}, address={}", id, user, address);
            } else {
                log.info("Session with monitor={} for user={}, address={} garbage collected", id, user, address);
            }
        }
    }

    private enum Status {
        UNLOCKED, LOCK_PENDING, LOCKED
    }
}
