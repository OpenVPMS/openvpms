/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.text;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.system.common.util.StringUtilities;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Helper to filter characters from text.
 *
 * @author Tim Anderson
 */
public class TextHelper {

    /**
     * Determines if a string contains control characters that can cause rendering issues.
     * <p/>
     * These are those that match the regexp pattern {@code \p{Cntrl}}) except '\n', '\r', '\t'.
     *
     * @param str the string. May be {@code null}
     * @return {@code true} if the string contains control characters, otherwise {@code false}
     */
    public static boolean hasControlChars(String str) {
        return StringUtilities.hasControlChars(str);
    }

    /**
     * Replaces any control characters.
     * <p/>
     * These are those that match the regexp pattern {@code \p{Cntrl}}) except '\n', '\r', '\t'.
     *
     * @param str  the string. May be {@code null}
     * @param with the string to replace the characters with
     * @return the string with any control characters replaced
     */
    public static String replaceControlChars(String str, String with) {
        return StringUtilities.replaceControlChars(str, with);
    }

    /**
     * Uncamel cases the specified text.
     *
     * @param text the camel cased text
     * @return the uncamel-cased version of the text
     */
    public static String unCamelCase(String text) {
        ArrayList<String> words = new ArrayList<>();

        Pattern pattern = Pattern.compile("/(\\w+?)([A-Z].*)/");
        Matcher matcher = pattern.matcher(text);
        while (matcher.matches()) {
            String word = matcher.group(1);
            text = matcher.group(2);
            words.add(StringUtils.capitalize(word));
        }

        words.add(StringUtils.capitalize(text));

        return StringUtils.join(words.iterator(), " ");
    }
}
