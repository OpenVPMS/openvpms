/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2024 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.popup;

import echopointng.ui.util.RenderingContext;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Style;
import nextapp.echo2.webcontainer.ContainerInstance;
import nextapp.echo2.webrender.Service;
import nextapp.echo2.webrender.WebRenderServlet;
import nextapp.echo2.webrender.service.JavaScriptService;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * A peer to register a modified popup.js in order to change the sizing of popups, and reposition them.
 *
 * @author Tim Anderson
 */
public class PopUpPeer extends echopointng.ui.syncpeer.PopUpPeer {

    public static final Service POPUP_SERVICE = JavaScriptService.forResource("EPNG.PopUp",
                                                                              "/org/openvpms/web/echo/js/popup.js");

    public static final Service UTILS_SERVICE = JavaScriptService.forResource("OpenVPMS.Utils",
                                                                              "/org/openvpms/web/echo/js/utils.js");

    static {
        // NOTE: as this extends EPNG PopUpPeer, the EPNG popup.js script will always be registered prior to
        // this due to static construction order. It can therefore safely be removed and replaced.
        WebRenderServlet.getServiceRegistry().remove(POPUP_SERVICE);
        WebRenderServlet.getServiceRegistry().add(POPUP_SERVICE);
        WebRenderServlet.getServiceRegistry().add(UTILS_SERVICE);
    }

    /**
     * This is the rendering method you must implement if you are to use {@code AbstractEchoPointPeer}.
     *
     * @param rc        the RenderingContext to use
     * @param parent    the parent Node to put content into
     * @param component the component
     */
    @Override
    public void renderHtml(RenderingContext rc, Node parent, Component component) {
        super.renderHtml(rc, parent, component);
        rc.addLibrary(UTILS_SERVICE);
    }

    /**
     * Creates the init directive.
     *
     * @param rc            the rendering context
     * @param component     the component to render
     * @param fallbackStyle the fallback style
     */
    @Override
    protected void createInitDirective(RenderingContext rc, Component component, Style fallbackStyle) {
        super.createInitDirective(rc, component, fallbackStyle);

        boolean autoPosition = rc.getRP(PopUp.AUTO_POSITION, false);
        if (autoPosition) {
            // need to patch the existing element. This should be the last element added to the postupdate directive
            Element itemizedUpdateElement = rc.getServerMessage().getItemizedDirective(
                    "postupdate", "EPPU.MessageProcessor", "init", new String[0], new String[0]);
            Node child = itemizedUpdateElement.getLastChild();
            if (child instanceof Element) {
                Element element = (Element) child;

                String id = ContainerInstance.getElementId(component);
                if (element.getAttribute("eid").equals(id)) {
                    element.setAttribute("autoPosition", String.valueOf(autoPosition));
                }
            }
        }
    }
}
