/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.macro.impl;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.function.factory.ArchetypeFunctionsFactory;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.settings.Settings;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.archetype.test.builder.doc.TestDocumentTemplateBuilder;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.ReadOnlyArchetypeService;
import org.openvpms.component.business.service.archetype.helper.IMObjectVariables;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.report.ReportFactory;
import org.openvpms.report.ReportFactoryImpl;
import org.openvpms.report.jasper.JRXMLDocumentHandler;
import org.openvpms.report.openoffice.OpenOfficeService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ReportMacroRunnerTestCase}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ReportMacroRunnerTestCase extends ArchetypeServiceTest {

    /**
     * The OpenOffice service.
     */
    @Autowired
    private OpenOfficeService officeService;

    /**
     * The handlers for document serialisation.
     */
    private DocumentHandlers handlers;

    /**
     * The archetype functions factory.
     */
    @Autowired
    private ArchetypeFunctionsFactory functions;

    /**
     * The settings.
     */
    @Autowired
    private Settings settings;

    /**
     * The Jasper Report test report.
     */
    private static final String JASPER_REPORT = "/report.jrxml";

    /**
     * The Word test report.
     */
    private static final String WORD_REPORT = "/report.doc";

    /**
     * The OpenOffice test report.
     */
    private static final String OPEN_OFFICE_REPORT = "/report.odt";


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        ArchetypeService service = getArchetypeService();
        handlers = new DocumentHandlers(service, Collections.singletonList(new JRXMLDocumentHandler(service)));
    }

    /**
     * Verifies that a Jasper Report can be used as a report macro.
     */
    @Test
    public void testJasperReportMacro() {
        // creates a report
        Entity report = createReport(JASPER_REPORT);

        // create and run a report macro lookup that references the report
        String text = runMacro(report);

        // verify the macro output
        assertEquals("Foo Bar", text);
    }

    /**
     * Verifies that a Word document can be used as a report macro.
     */
    @Test
    public void testWordReport() {
        // create a report
        Entity report = createReport(WORD_REPORT);

        // create and run a report macro lookup that references the report
        String text = runMacro(report);

        // verify the macro output
        String expected = "First Name: Foo\nLast Name: Bar";
        text = text.replaceAll("\\s?\\n", "\n"); // make sure carriage returns handled in platform independent manner
        text = text.trim(); // remove extraneous whitespace
        assertEquals(expected, text);
    }

    /**
     * Verifies that an OpenOffice document can be used as a report macro.
     */
    @Test
    public void testOpenOfficeReport() {
        // creates a report
        Entity report = createReport(OPEN_OFFICE_REPORT);

        // create and run a report macro lookup that references the report
        String text = runMacro(report);

        // verify the macro output
        String expected = "First: Foo, Last: Bar";
        text = text.trim(); // remove extraneous whitespace
        assertEquals(expected, text);
    }

    /**
     * Runs a report macro supplying a $customer variable.
     *
     * @param report the entity.documentTemplate representing the report
     * @return the result of the macro
     */
    private String runMacro(Entity report) {
        // create a report macro lookup that references the report
        Lookup lookup = create(MacroArchetypes.REPORT_MACRO, Lookup.class);
        IMObjectBean macroBean = getBean(lookup);
        macroBean.setValue("report", report.getObjectReference());
        macroBean.setValue("expression", "$customer");

        // create a customer
        IArchetypeService service = getArchetypeService();
        Party customer = TestHelper.createCustomer("Foo", "Bar", false);
        IMObjectVariables variables = new IMObjectVariables(service, getLookupService());
        variables.add("customer", customer);

        // run the report macro against the customer
        ReportMacro macro = new ReportMacro(lookup, service);
        MacroContext context = new MacroContext(Collections.emptyMap(), null, null, variables,
                                                functions.create(new ReadOnlyArchetypeService(service), false));
        ReportFactory factory = new ReportFactoryImpl(service, getLookupService(), handlers, functions, null, null,
                                                      officeService, settings);
        ReportMacroRunner runner = new ReportMacroRunner(context, factory);
        return runner.run(macro, "");
    }

    /**
     * Helper to create an <em>entity.documentTemplate</em>, <em>act.documentTemplate</em> and <em>document.other</em>
     * for the supplied report.
     *
     * @param report the report name
     * @return a new template
     */
    private Entity createReport(String report) {
        TestDocumentTemplateBuilder builder = new TestDocumentTemplateBuilder(getArchetypeService(), handlers);
        return builder.name(report)
                .type("REPORT")
                .document(report)
                .build();
    }
}
