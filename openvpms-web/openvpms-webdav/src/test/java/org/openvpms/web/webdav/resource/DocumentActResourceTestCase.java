/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.webdav.resource;

import io.milton.http.LockManager;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.web.webdav.session.Session;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentActResource} class.
 *
 * @author Tim Anderson
 */
public class DocumentActResourceTestCase extends ArchetypeServiceTest {

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Verifies that file names are encoded correctly as a workaround for OVPMS-2220 and OVPMS-2738.
     *
     * @throws Exception for any error
     */
    @Test
    public void testFileNameEncoding() throws Exception {
        checkEncoding("Referral Letter for Muffet O'Brien.txt", "Referral Letter for Muffet OBrien.txt");
        checkEncoding("Vaccination & Checkup.txt", "Vaccination  Checkup.txt");
    }

    /**
     * Verifies that file names are encoded correctly.
     *
     * @param fileName    the file name
     * @param encodedName the expected encoded file name
     */
    private void checkEncoding(String fileName, String encodedName) throws Exception {
        Session session = Mockito.mock(Session.class);
        DocumentAct act = PatientTestHelper.createDocumentLetter(new Date(), patientFactory.createPatient());
        act.setFileName(fileName);
        LockManager lockManager = Mockito.mock(LockManager.class);
        DocumentHandlers handlers = new DocumentHandlers(getArchetypeService());
        DocumentActResource documentActResource = new DocumentActResource(act, session, getArchetypeService(),
                                                                          handlers, lockManager, new Date());
        assertEquals(Long.toString(act.getId()), documentActResource.getName());
        assertTrue(documentActResource.getChildren().isEmpty());

        // add some content
        String mimeType = "text/plain";
        DocumentHandler handler = handlers.get(fileName, mimeType);
        Document document = handler.create(fileName, IOUtils.toInputStream("some plain text", UTF_8), mimeType, -1);
        act.setDocument(document.getObjectReference());
        save(act, document);

        assertEquals(1, documentActResource.getChildren().size());

        assertNull(documentActResource.child(fileName)); // need to use the encoded name

        DocumentResource documentResource = (DocumentResource) documentActResource.child(encodedName);
        assertNotNull(documentResource);
        assertEquals(encodedName, documentResource.getName());
    }
}
