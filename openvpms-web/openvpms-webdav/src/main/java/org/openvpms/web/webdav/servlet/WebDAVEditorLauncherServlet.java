/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.webdav.servlet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.web.webdav.session.Session;
import org.openvpms.web.webdav.session.SessionManager;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Servlet to generate a JNLP to launch the {@code EditorLauncher}.
 * <p/>
 * For security, this must be supplied a session identifier and associated document id in order to generate the JNLP.
 *
 * @author Tim Anderson
 */
public class WebDAVEditorLauncherServlet extends HttpServlet {

    /**
     * The session manager.
     */
    private SessionManager manager;

    /**
     * The JNLP template path.
     */
    private static final String TEMPLATE_PATH = "/webstart/launch.jnlp";

    /**
     * Initialises the servlet.
     */
    @Override
    public void init() {
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        manager = context.getBean(SessionManager.class);
    }

    /**
     * Handles a GET request.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException for any I/O exception
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String sessionId = request.getParameter("session");
        String id = request.getParameter("id");
        Session session = getSession(sessionId, NumberUtils.toLong(id, -1));
        if (session == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            response.setContentType("application/x-java-jnlp-file");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Content-Disposition", "attachment; filename=\"externaledit.jnlp\"");
            String output = generateJNLP(request, session);
            response.getWriter().println(output);
        }
    }

    /**
     * Returns the session associated with a session and document act identifier.
     *
     * @param sessionId the session identifier. May be {@code null}
     * @param id        the document act identifier
     * @return the corresponding session, or {@code null} if none is found
     */
    private Session getSession(String sessionId, long id) {
        Session result = null;
        if (sessionId != null) {
            Session session = manager.get(sessionId);
            if (session != null && id == session.getDocumentAct().getId()) {
                result = session;
            }
        }
        return result;
    }

    /**
     * Generates the JNLP for a request.
     *
     * @param request the request. The document URL must be passed in the query string
     * @param session the session
     * @return the JNLP
     * @throws IOException for any I/O error
     */
    private String generateJNLP(HttpServletRequest request, Session session) throws IOException {
        String jnlp = getTemplate();
        String baseURL = getServerURL(request);
        String requestURI = request.getRequestURI();

        int index = requestURI.lastIndexOf('/');
        String name = requestURI.substring(index + 1);       // exclude /
        String context = requestURI.substring(0, index + 1); // Include
        String codebase = context + "webstart";
        String doc = baseURL + context + "webdav" + session.getPath();
        jnlp = StringUtils.replace(jnlp, "$$name", name);
        jnlp = StringUtils.replace(jnlp, "$$hostname", request.getServerName());
        jnlp = StringUtils.replace(jnlp, "$$codebase", baseURL + codebase);
        jnlp = StringUtils.replace(jnlp, "$$context", baseURL + context);
        jnlp = StringUtils.replace(jnlp, "$$site", baseURL);
        jnlp = StringUtils.replace(jnlp, "$$doc", doc);
        return jnlp;
    }

    /**
     * Returns the JNLP template.
     *
     * @return the JNLP template as a string
     * @throws IOException for any I/O error
     */
    private String getTemplate() throws IOException {
        String template;
        try (InputStream stream = getClass().getResourceAsStream(TEMPLATE_PATH)) {
            if (stream == null) {
                throw new IOException(TEMPLATE_PATH + " not found");
            }
            template = IOUtils.toString(stream, StandardCharsets.UTF_8);
        }
        return template;
    }

    /**
     * Returns the server URL.
     *
     * @param request the request
     * @return the server URL
     */
    private String getServerURL(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        return url.substring(0, url.length() - request.getRequestURI().length());
    }
}
