/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.webdav.resource;

import io.milton.resource.Resource;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.object.Reference;

/**
 * An {@link IMObject} WebDAV resource.
 *
 * @author Tim Anderson
 */
public interface IMObjectResource extends Resource {

    /**
     * Returns the object reference.
     *
     * @return the object reference
     */
    Reference getReference();

    /**
     * Returns the object version.
     *
     * @return the version
     */
    long getVersion();
}
