/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.event.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.IQueueClient;
import com.microsoft.azure.servicebus.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.event.EventDispatcher;
import org.openvpms.smartflow.event.EventStatus;
import org.openvpms.smartflow.model.Note;
import org.openvpms.smartflow.model.Notes;
import org.openvpms.smartflow.model.event.Event;
import org.openvpms.smartflow.model.event.NotesEvent;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests the {@link QueueDispatcher} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class QueueDispatcherTestCase extends ArchetypeServiceTest {

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The practice location.
     */
    private Party location;

    /**
     * The object mapper, for JSON serialisation.
     */
    private ObjectMapper mapper;

    /**
     * The mock queue client.
     */
    private IQueueClient queueClient;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        location = practiceFactory.newLocation().build(false);
        queueClient = Mockito.mock(IQueueClient.class);
        mapper = new ObjectMapper();
    }

    /**
     * Tests the {@link QueueDispatcher}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDispatcher() throws Exception {
        List<Event<?>> events = new ArrayList<>();
        EventDispatcher eventDispatcher = events::add;
        QueueDispatcher dispatcher = createQueueDispatcher(eventDispatcher);

        assertTrue(dispatcher.start());

        assertEquals(0, events.size());

        // check the status
        checkNullStatus(dispatcher);

        // simulate dispatch of a message
        Date now = new Date();
        Message message = createMessage();
        dispatcher.dispatch(message);

        // verify the message was removed from the queue
        verify(queueClient, Mockito.times(1)).complete(any());

        // verify it was passed to the event dispatcher
        assertEquals(1, events.size());
        assertTrue(events.get(0) instanceof NotesEvent);

        // check the new status
        EventStatus status2 = dispatcher.getStatus();
        assertNull(status2.getErrorMessage());
        assertNull(status2.getError());
        assertNotNull(status2.getReceived());
        assertTrue(status2.getReceived().compareTo(now) >= 0);

        // now destroy the dispatcher and verify it cannot be restarted
        dispatcher.destroy();
        assertTrue(dispatcher.isDestroyed());
        assertFalse(dispatcher.start());
    }

    /**
     * Verifies that if the {@link EventDispatcher} throws an exception, this is caught.
     */
    @Test
    public void testEventDispatcherError() throws Exception {
        // create a dispatcher that throws an exception on dispatch
        EventDispatcher eventDispatcher = event -> {
            throw new IllegalStateException("Some random error");
        };
        QueueDispatcher dispatcher = createQueueDispatcher(eventDispatcher);

        assertTrue(dispatcher.start());

        checkNullStatus(dispatcher);

        // simulate dispatch of a message
        Date now = new Date();
        Message message = createMessage();
        dispatcher.dispatch(message);

        // event dispatcher threw an exception, so the message should not have been removed from the queue
        verify(queueClient, never()).complete(any());

        // check the new status
        EventStatus status2 = dispatcher.getStatus();
        assertEquals("Some random error", status2.getErrorMessage());
        assertNotNull(status2.getError());
        assertTrue(status2.getError().compareTo(now) >= 0);
        assertNotNull(status2.getReceived());
        assertTrue(status2.getReceived().compareTo(now) >= 0);

        // now destroy the dispatcher and verify it cannot be restarted
        dispatcher.destroy();
        assertTrue(dispatcher.isDestroyed());
        assertFalse(dispatcher.start());
    }

    /**
     * Verifies that the dispatcher status is null.
     *
     * @param dispatcher the dispatcher to check
     */
    private void checkNullStatus(QueueDispatcher dispatcher) {
        EventStatus status = dispatcher.getStatus();
        assertNull(status.getErrorMessage());
        assertNull(status.getError());
        assertNull(status.getReceived());
    }

    /**
     * Creates a {@link QueueDispatcher} that uses the mock {@link IQueueClient}.
     *
     * @param eventDispatcher the event dispatcher to pass events to
     * @return a new queue dispatcher
     */
    private QueueDispatcher createQueueDispatcher(EventDispatcher eventDispatcher) {
        ExecutorService executorService = Mockito.mock(ExecutorService.class);
        return new QueueDispatcher(location, eventDispatcher, mapper,
                                   executorService,
                                   Mockito.mock(FlowSheetServiceFactory.class)) {
            @Override
            protected IQueueClient createQueue() {
                return queueClient;
            }
        };
    }

    private Message createMessage() throws JsonProcessingException {
        byte[] bytes = mapper.writeValueAsBytes(createNotesEvent());
        return new Message(bytes);
    }

    /**
     * Creates a new notes event, with a single note.
     *
     * @return a new event
     */
    private NotesEvent createNotesEvent() {
        NotesEvent event = new NotesEvent();
        event.setEventType("notes.entered");
        Notes list = new Notes();
        Note note = new Note();
        note.setNoteGuid(UUID.randomUUID().toString());
        note.setHospitalizationId("1234");
        note.setStatus(Note.ADDED_STATUS);
        note.setText("Some text");
        list.setNotes(Collections.singletonList(note));
        event.setObject(list);
        return event;
    }

}
