/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

/**
 * Smart Flow Sheet dental chart.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DentalChart {

    /**
     * Describes the type of the object transferred with the SFS API. Should be anesthetic.
     */
    private String objectType = "dentalchart";

    /**
     * EMR internal ID of the hospitalization.
     */
    private String hospitalizationId;

    /**
     * A unique internal identifier of the dental chart. This field will be transferred with the SFS events.
     */
    private String dentalChartGuid;

    /**
     * Dental chart finalisation time. Time format: YYYY-MM-DDThh:mm:ss.sssTZD (e.g. 1997-07-16T19:20:30.000+00:00)
     */
    private Date dateFinalized;

    /**
     * The path to the dental chart report file that has been generated during finalization of the dental chart.
     */
    private String reportPath;

    /**
     * Optional. The array of dental-media objects that correspond to the photos taken by the user on the dental chart.
     */
    private List<DentalMedia> media;

    /**
     * Returns the object type.
     *
     * @return the object type
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Sets the object type.
     *
     * @param objectType the object type
     */
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    /**
     * Returns the external hospitalization id (which was provided with hospitalization creation).
     *
     * @return the external hospitalization id
     */
    public String getHospitalizationId() {
        return hospitalizationId;
    }

    /**
     * Sets the external hospitalization id.
     *
     * @param hospitalizationId the external hospitalization id
     */
    public void setHospitalizationId(String hospitalizationId) {
        this.hospitalizationId = hospitalizationId;
    }

    /**
     * Returns the unique internal identifier of the surgery. This field will be transferred with the SFS events.
     *
     * @return the unique internal identifier of the surgery
     */
    public String getDentalChartGuid() {
        return dentalChartGuid;
    }

    /**
     * Sets the unique internal identifier of the surgery. This field will be transferred with the SFS events.
     *
     * @param dentalChartGuid the unique internal identifier of the surgery
     */
    public void setDentalChartGuid(String dentalChartGuid) {
        this.dentalChartGuid = dentalChartGuid;
    }

    /**
     * Returns the date when the chart was finalised.
     *
     * @return the dental chart finalisation time
     */
    public Date getDateFinalized() {
        return dateFinalized;
    }

    /**
     * Sets the date when the chart was finalised.
     *
     * @param dateFinalized the dental chart finalisation time
     */
    public void setDateFinalized(Date dateFinalized) {
        this.dateFinalized = dateFinalized;
    }

    /**
     * Returns the path to the anesthetic sheet report file that has been generated during finalization of the
     * anesthetic sheet.
     *
     * @return the report path
     */
    public String getReportPath() {
        return reportPath;
    }

    /**
     * Sets the path to the anesthetic sheet report file that has been generated during finalization of the
     * anesthetic sheet.
     *
     * @param reportPath the report path
     */
    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    /**
     * Returns the media objects that correspond to the photos taken by the user on the dental chart.
     *
     * @return the media. May be {@code null}
     */
    public List<DentalMedia> getMedia() {
        return media;
    }

    /**
     * Sets the media objects that correspond to the photos taken by the user on the dental chart.
     *
     * @param media the media. May be {@code null}
     */
    public void setMedia(List<DentalMedia> media) {
        this.media = media;
    }
}
