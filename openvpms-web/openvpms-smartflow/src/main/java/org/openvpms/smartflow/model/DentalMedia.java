/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Smart Flow Sheet dental media.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DentalMedia {

    /**
     * A unique internal identifier for the dental chart media.
     */
    private String dentalChartMediaGuid;

    /**
     * One of “before” or “after”, denoting the section where the user took the photo. This indicates whether the photo
     * was taken before or after the dental procedures were performed.
     */
    private String section;

    /**
     * The time photo was taken. Time format: YYYY-MM-DDThh:mm:ss.sssTZD (e.g. 1997-07-16T19:20:30.000+00:00).
     */
    private Date dateFinalized;

    /**
     * The path to the dental media file (photo) that has been taken by the user.
     */
    private String filePath;

    /**
     * The content type of media file (e.g. image/jpg, video/mp4, etc…). For Dental, this is currently limited to
     * image/jpg.
     */
    private String contentType;

    /**
     * Returns the unique internal identifier of the media.
     *
     * @return the unique internal identifier of the media
     */
    public String getDentalChartMediaGuid() {
        return dentalChartMediaGuid;
    }

    /**
     * Sets the unique internal identifier of the media.
     *
     * @param dentalChartMediaGuid the unique internal identifier of the media
     */
    public void setDentalChartMediaGuid(String dentalChartMediaGuid) {
        this.dentalChartMediaGuid = dentalChartMediaGuid;
    }

    /**
     * Returns the section where the user took the photo. This indicates whether the photo
     * was taken before or after the dental procedures were performed.
     *
     * @return the section. One of 'before' or 'after'
     */
    public String getSection() {
        return section;
    }

    /**
     * Sets the section where the user took the photo.
     *
     * @param section the section. Must be one of 'before' or 'after'
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * Returns the time when the photo was taken.
     *
     * @return the time when the photo was taken
     */
    public Date getDateFinalized() {
        return dateFinalized;
    }

    /**
     * Sets the time when the photo was taken.
     *
     * @param dateFinalized the the time when the photo was taken
     */
    public void setDateFinalized(Date dateFinalized) {
        this.dateFinalized = dateFinalized;
    }

    /**
     * Returns the path to the dental media file (photo) that has been taken by the user.
     *
     * @return the path
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the path to the dental media file (photo) that has been taken by the user.
     *
     * @param filePath the path
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Returns the content type of media file (e.g. image/jpg, video/mp4, etc…).
     * <br/>
     * For Dental, this is currently limited to image/jpg.
     *
     * @return the content type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets the content type of media file (e.g. image/jpg, video/mp4, etc…).
     *
     * @param contentType the content type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
