/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.event.impl;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.security.RunAs;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.event.EventDispatcher;
import org.openvpms.smartflow.model.Hospitalization;
import org.openvpms.smartflow.model.Hospitalizations;
import org.openvpms.smartflow.model.event.AdmissionEvent;
import org.openvpms.smartflow.model.event.AnestheticsEvent;
import org.openvpms.smartflow.model.event.DischargeEvent;
import org.openvpms.smartflow.model.event.Event;
import org.openvpms.smartflow.model.event.InventoryImportedEvent;
import org.openvpms.smartflow.model.event.MedicsImportedEvent;
import org.openvpms.smartflow.model.event.NotesEvent;
import org.openvpms.smartflow.model.event.TreatmentEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Dispatches Smart Flow Sheet events.
 *
 * @author Tim Anderson
 */
public class DefaultEventDispatcher implements EventDispatcher {

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The treatment event processor.
     */
    private final TreatmentEventProcessor treatmentProcessor;

    /**
     * The notes event processor.
     */
    private final NotesEventProcessor notesProcessor;

    /**
     * The anesthetics event processor.
     */
    private final AnestheticsEventProcessor anestheticsProcessor;

    /**
     * The discharge event processor.
     */
    private final DischargeEventProcessor dischargeProcessor;

    /**
     * The inventory imported event processor.
     */
    private final InventoryImportedEventProcessor inventoryImportedProcessor;

    /**
     * The medics imported event processor.
     */
    private final MedicsImportedEventProcessor medicsImportedProcessor;

    /**
     * The logger.
     */
    private final Logger log = LoggerFactory.getLogger(DefaultEventDispatcher.class);

    /**
     * Constructs a {@link DefaultEventDispatcher}.
     *
     * @param location           the practice location. May be {@code null}
     * @param service            the archetype service
     * @param lookups            the lookups
     * @param factory            the Smart Flow Sheet service factory
     * @param practiceService    the practice service
     * @param rules              the patient rules
     * @param transactionManager the transaction manager
     */
    public DefaultEventDispatcher(Party location, IArchetypeService service, LookupService lookups,
                                  FlowSheetServiceFactory factory, PracticeService practiceService,
                                  PatientRules rules, PlatformTransactionManager transactionManager) {
        this.practiceService = practiceService;
        this.transactionManager = transactionManager;
        FlowSheetConfigService configService = new FlowSheetConfigService(service, practiceService);
        treatmentProcessor = new TreatmentEventProcessor(location, service, lookups, rules);
        notesProcessor = new NotesEventProcessor(service, configService);
        anestheticsProcessor = new AnestheticsEventProcessor(service, factory);
        dischargeProcessor = new DischargeEventProcessor(service, factory, configService);
        inventoryImportedProcessor = new InventoryImportedEventProcessor(service);
        medicsImportedProcessor = new MedicsImportedEventProcessor(service);
    }

    /**
     * Dispatches an event.
     *
     * @param event the event
     */
    @Override
    public void dispatch(Event<?> event) {
        User user = practiceService.getServiceUser();
        if (user == null) {
            throw new IllegalStateException(
                    "Missing Practice Service User. Messages cannot be processed until " +
                    "this is configured");
        }
        RunAs.run(user, () -> dispatchEvent(event));
    }

    /**
     * Invoked when one or more patients are admitted.
     *
     * @param event the admission event
     */
    protected void admitted(AdmissionEvent event) {
        if (log.isDebugEnabled()) {
            Hospitalizations list = event.getObject();
            if (list != null) {
                for (Hospitalization hospitalization : list.getHospitalizations()) {
                    log.debug("Admitted: " + hospitalization.getPatient().getName());
                }
            }
        }
    }

    /**
     * Dispatches an event.
     * <p/>
     * The event is processed within a transaction so that individual processors aren't responsible for
     * transaction management.
     *
     * @param event the event
     */
    private void dispatchEvent(Event<?> event) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                if (event instanceof NotesEvent) {
                    notesProcessor.process((NotesEvent) event);
                } else if (event instanceof TreatmentEvent) {
                    treatmentProcessor.process((TreatmentEvent) event);
                } else if (event instanceof AdmissionEvent) {
                    admitted((AdmissionEvent) event);
                } else if (event instanceof DischargeEvent) {
                    dischargeProcessor.process((DischargeEvent) event);
                } else if (event instanceof AnestheticsEvent) {
                    // TODO: ignoring AnestheticsEvent for now as they can be handled at discharge and there is
                    //  currently
                    // no easy way to avoid duplicates
                    // anestheticsProcessor.process((AnestheticsEvent) event);
                } else if (event instanceof InventoryImportedEvent) {
                    inventoryImportedProcessor.process((InventoryImportedEvent) event);
                } else if (event instanceof MedicsImportedEvent) {
                    medicsImportedProcessor.process((MedicsImportedEvent) event);
                }
            }
        });
    }

}
