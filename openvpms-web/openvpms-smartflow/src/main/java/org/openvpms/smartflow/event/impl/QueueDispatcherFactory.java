/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2023 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.event.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.smartflow.client.FlowSheetException;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.event.EventDispatcher;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Factory for {@link QueueDispatcher} instances.
 *
 * @author Tim Anderson
 */
class QueueDispatcherFactory {

    /**
     * Factory for Smart Flow Sheet services.
     */
    private final FlowSheetServiceFactory factory;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The lookup service.
     */
    private final LookupService lookups;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The patient rules.
     */
    private final PatientRules rules;

    /**
     * The object mapper.
     */
    private final ObjectMapper mapper;

    /**
     * The executor service for dispatching messages.
     */
    private final ExecutorService executorService;

    /**
     * Thread factory for new threads.
     */
    private final static ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("SFS Event Dispatcher")
            .setDaemon(true).build();


    /**
     * Constructs a {@link QueueDispatcherFactory}.
     *
     * @param factory            the factory for SFS services
     * @param service            the archetype service
     * @param lookups            the lookup service
     * @param transactionManager the transaction manager
     * @param practiceService    the practice service
     * @param rules              the patient rules
     */
    public QueueDispatcherFactory(FlowSheetServiceFactory factory, IArchetypeService service, LookupService lookups,
                                  PlatformTransactionManager transactionManager, PracticeService practiceService,
                                  PatientRules rules) {
        this.factory = factory;
        this.service = service;
        this.lookups = lookups;
        this.transactionManager = transactionManager;
        this.practiceService = practiceService;
        this.rules = rules;
        mapper = new ObjectMapper();
        executorService = Executors.newSingleThreadExecutor(threadFactory);
    }


    /**
     * Creates a new queue dispatcher.
     *
     * @param location the location to dispatch events for
     * @return a new queue dispatcher
     */
    public QueueDispatcher createQueueDispatcher(Party location) {
        EventDispatcher dispatcher = createEventDispatcher(location);
        return new QueueDispatcher(location, dispatcher, mapper, executorService, factory);
    }

    /**
     * Returns the clinic API key for the practice.
     *
     * @param location the practice location
     * @return the clinic API key, or {@code null} if none exists
     * @throws FlowSheetException if the key exists but cannot be decrypted
     */
    public String getClinicAPIKey(Party location) {
        return factory.getClinicAPIKey(location);
    }

    /**
     * Creates a new event dispatcher.
     *
     * @param location the location to dispatch events for
     * @return a new event dispatcher
     */
    protected EventDispatcher createEventDispatcher(Party location) {
        return new DefaultEventDispatcher(location, service, lookups, factory, practiceService, rules,
                                          transactionManager);
    }

}
