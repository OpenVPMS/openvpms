/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.patient;

/**
 * Notifies registered services of patient events.
 *
 * @author Tim Anderson
 */
public interface PatientInformationService {

    /**
     * Notifies that a patient has been admitted.
     *
     * @param context the patient context
     */
    void admitted(PatientContext context);

    /**
     * Notifies that an admission has been cancelled.
     * <p/>
     * If a connector doesn't support Cancel Admit messages (ADT A11), a Discharge (ADT A03) message will be sent
     * instead.
     *
     * @param context the patient context
     */
    void admissionCancelled(PatientContext context);

    /**
     * Notifies that a patient has been discharged.
     *
     * @param context the patient context
     */
    void discharged(PatientContext context);

    /**
     * Notifies that a patient has been updated.
     *
     * @param context the patient context
     */
    void updated(PatientContext context);

}