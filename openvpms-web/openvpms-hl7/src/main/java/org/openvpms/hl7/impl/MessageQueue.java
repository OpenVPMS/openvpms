/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.impl;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.hl7.io.MessageService;
import org.openvpms.hl7.io.Statistics;
import org.openvpms.hl7.util.HL7MessageStatuses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * A queue of messages for a {@link MLLPSender}.
 * <p/>
 * TODO - cache counts, and pre-fetch messages to reduce database access.
 *
 * @author Tim Anderson
 */
class MessageQueue extends Queue<Message, MLLPSender> implements Statistics {

    /**
     * The message service.
     */
    private final MessageService service;

    /**
     * The message context.
     */
    private final HapiContext context;

    /**
     * The connector that this queue manages messages for.
     */
    private MLLPSender connector;

    /**
     * The current message act.
     */
    private DocumentAct currentAct;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MessageQueue.class);


    /**
     * Constructs an {@link MessageQueue}.
     *
     * @param connector the connector
     * @param service   the message service
     * @param context   the message context
     */
    public MessageQueue(MLLPSender connector, MessageService service, HapiContext context) {
        super(connector);
        this.connector = connector;
        this.service = service;
        this.context = context;
    }

    /**
     * Returns the interval to wait after a failure.
     *
     * @return the interval, in seconds
     */
    @Override
    public synchronized int getRetryInterval() {
        int interval = connector.getRetryInterval();
        if (interval <= 0) {
            interval = MLLPSender.DEFAULT_RETRY_INTERVAL;
        }
        return interval;
    }

    /**
     * Adds a message to the end of the queue.
     *
     * @param message the message to add
     * @return the added act
     * @throws HL7Exception if the message cannot be encoded
     */
    public DocumentAct add(Message message) throws HL7Exception {
        return service.save(message, connector);
    }

    /**
     * Retrieves, but does not remove, the first act in the queue.
     *
     * @return the head of this queue, or {@code null} if the queue is empty
     */
    public synchronized DocumentAct peekFirstAct() {
        peekFirst();
        return currentAct;
    }

    /**
     * Invoked when a message is sent.
     *
     * @param response the response
     * @return the act the act corresponding to the sent message
     */
    public synchronized DocumentAct sent(Message response) {
        if (currentAct == null) {
            throw new IllegalStateException("No current message");
        }
        long waitUntil = -1;
        DocumentAct result = currentAct;
        MSA msa = HL7MessageHelper.getMSA(response);
        if (msa != null) {
            String ackCode = msa.getAcknowledgmentCode().getValue();
            if (AcknowledgmentCode.AA.toString().equals(ackCode)) {
                processed();
            } else if (AcknowledgmentCode.AE.toString().equals(ackCode)) {
                handleError(response, HL7MessageStatuses.PENDING);
                waitUntil = System.currentTimeMillis() + 30 * 1000;
            } else {
                handleError(response, HL7MessageStatuses.ERROR);
            }
        } else {
            unsupportedResponse(response);
        }
        setWaitUntil(waitUntil);
        return result;
    }

    /**
     * Updates the connector.
     *
     * @param connector the connector
     */
    public synchronized void setConnector(MLLPSender connector) {
        this.connector = connector;
        setSuspended(connector.isSuspended());
    }

    /**
     * Returns the connector.
     *
     * @return the connector
     */
    public synchronized MLLPSender getConnector() {
        return connector;
    }

    /**
     * Returns the number of messages in the queue.
     *
     * @return the number of messages
     */
    public int getQueued() {
        return countMessages(HL7MessageStatuses.PENDING);
    }

    /**
     * Returns the number of messages in the error queue.
     * <p/>
     * Only applies to sending connectors.
     *
     * @return the number of messages
     */
    @Override
    public int getErrors() {
        return countMessages(HL7MessageStatuses.ERROR);
    }

    /**
     * Determines if the connector is running.
     *
     * @return {@code true} if the connector is running
     */
    @Override
    public boolean isRunning() {
        return !isSuspended();
    }

    /**
     * Invoked when an object is sucessfully processed.
     *
     * @param object    the processed object
     * @param processed the timestamp when it was processed
     */
    @Override
    protected void processed(Message object, Date processed) {
        try {
            service.accepted(currentAct, processed);
        } finally {
            completed(null, null);
        }
    }

    /**
     * Invoked after processing an object.
     *
     * @param errorDate the error date/time, or {@code null} if there was no error
     * @param error     the error message, or {@code null} if there was no error
     */
    @Override
    protected void completed(Date errorDate, String error) {
        super.completed(errorDate, error);
        currentAct = null;
    }

    /**
     * Retrieves the next object.
     *
     * @param owner the owner of the queue
     * @return the next object, or {@code null} if there is none
     */
    @Override
    protected Message getNext(MLLPSender owner) {
        Message result = null;
        DocumentAct act;
        while ((act = service.next(connector)) != null) {
            Message message = decode(act);
            if (message != null) {
                currentAct = act;
                result = message;
                break;
            }
        }
        return result;
    }

    /**
     * Invoked when an ack indicates an error.
     *
     * @param ack    the message acknowledgment
     * @param status the new act status
     */
    private void handleError(Message ack, String status) {
        String error = HL7MessageHelper.getErrorMessage(ack);
        error(status, error);
    }

    /**
     * Invoked when an unsupported response is received.
     *
     * @param response the response
     */
    private void unsupportedResponse(Message response) {
        StringBuilder error = new StringBuilder();
        error.append("Unsupported response: ");
        try {
            MSH header = (MSH) response.get("MSH");
            error.append(HL7MessageHelper.getMessageName(header));
        } catch (HL7Exception exception) {
            log.error("Failed to determine message type", exception);
            error.append("unknown");
        }
        error.append("\nMessage: ");
        try {
            error.append(HL7MessageHelper.toString(response));
        } catch (HL7Exception exception) {
            log.error("Failed to format message", exception);
            error.append("unknown");
        }
        error(HL7MessageStatuses.ERROR, error.toString());
    }

    /**
     * Decodes an HL7 message from an act.
     * <p/>
     * If the message cannot be decoded, the act status will be set to ERROR.
     *
     * @param act the <em>act.HL7Message</em> act
     * @return the message, or {@code null} if the message cannot be decoded
     */
    private Message decode(DocumentAct act) {
        Message result = null;
        try {
            result = service.get(act, context.getGenericParser());
        } catch (HL7Exception exception) {
            log.error(exception.getMessage(), exception);
            service.error(act, HL7MessageStatuses.ERROR, new Date(), exception.getMessage());
        }
        return result;
    }

    /**
     * Count messages with the specified status.
     *
     * @param status the message status
     * @return the no. of messages with th status
     */
    private int countMessages(String status) {
        return service.getMessages(connector, status);
    }

    /**
     * Updates the current act with a status and error message.
     *
     * @param status the act status
     * @param error  the error message
     */
    private void error(String status, String error) {
        Date now = new Date();
        try {
            log.error("Error received from " + connector + ":" + error);
            service.error(currentAct, status, now, error);
        } finally {
            completed(now, error);
        }
    }

}
