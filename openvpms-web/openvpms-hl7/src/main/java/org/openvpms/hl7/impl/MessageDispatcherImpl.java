/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.hl7.impl;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.util.idgenerator.IDGenerator;
import org.openvpms.archetype.component.dispatcher.Dispatcher;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.hl7.io.Connector;
import org.openvpms.hl7.io.Connectors;
import org.openvpms.hl7.io.MessageDispatcher;
import org.openvpms.hl7.io.MessageService;
import org.openvpms.hl7.io.Statistics;
import org.openvpms.hl7.util.HL7MessageStatuses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Default implementation of the {@link MessageDispatcher} interface.
 *
 * @author Tim Anderson
 */
public class MessageDispatcherImpl extends Dispatcher<Message, MLLPSender, MessageQueue>
        implements MessageDispatcher, InitializingBean {

    /**
     * The message service.
     */
    private final MessageService messageService;

    /**
     * The connectors.
     */
    private final ConnectorsImpl connectors;

    /**
     * The message header populator.
     */
    private final HeaderPopulator populator;

    /**
     * The message context.
     */
    private final HapiContext messageContext;

    /**
     * The message control ID generator.
     */
    private final IDGenerator generator;

    /**
     * The receivers, keyed on connector reference.
     */
    private final Map<Reference, MessageReceiver> receiverMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * The demultiplexing receivers, keyed on port.
     */
    private final Map<Integer, DemultiplexingReceiver> receivers = new HashMap<>();

    /**
     * Listener for connector updates.
     */
    private final ConnectorsImpl.Listener listener;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(MessageDispatcherImpl.class);

    /**
     * Constructs a {@link MessageDispatcherImpl}.
     *
     * @param messageService  the message service
     * @param connectors      the connectors
     * @param practiceService the practice service
     */
    public MessageDispatcherImpl(MessageService messageService, ConnectorsImpl connectors,
                                 PracticeService practiceService) {
        this(messageService, connectors, practiceService, HapiContextFactory.create());
    }

    /**
     * Constructs a {@link MessageDispatcherImpl}.
     *
     * @param messageService  the message service
     * @param connectors      the connectors
     * @param practiceService the practice service
     * @param context         the message context
     */
    public MessageDispatcherImpl(MessageService messageService, ConnectorsImpl connectors,
                                 PracticeService practiceService, HapiContext context) {
        super(practiceService, log);
        this.messageService = messageService;
        this.connectors = connectors;
        populator = new HeaderPopulator();
        messageContext = context;
        generator = messageContext.getParserConfiguration().getIdGenerator();

        listener = new ConnectorsImpl.Listener() {
            @Override
            public void added(Connector connector) {
                update(connector);
            }

            @Override
            public void removed(Connector connector) {
                remove(connector);
            }
        };
        init(new MessageQueues(messageService, messageContext));
    }

    /**
     * Returns the message context.
     *
     * @return the message context
     */
    public HapiContext getMessageContext() {
        return messageContext;
    }

    /**
     * Queues a message to a connector.
     *
     * @param message   the message to queue
     * @param connector the connector
     * @param config    the message population configuration
     * @return the queued message
     */
    @Override
    public DocumentAct queue(Message message, Connector connector, HL7Mapping config) {
        DocumentAct result;
        if (!(connector instanceof MLLPSender)) {
            throw new IllegalArgumentException("Unsupported connector: " + connector);
        }
        try {
            populate(message, (MLLPSender) connector, config);
            result = queue(message, (MLLPSender) connector);
        } catch (Throwable exception) {
            throw new IllegalStateException(exception);
        }
        return result;
    }

    /**
     * Resubmit a message.
     * <p/>
     * The associated connector must support message resubmission, and the message must have an
     * {@link HL7MessageStatuses#ERROR} status.
     *
     * @param message the message to resubmit
     */
    @Override
    public void resubmit(DocumentAct message) {
        messageService.resubmit(message);
        schedule();
    }

    /**
     * Registers an application to handle messages from the specified connector.
     * <p/>
     * Only one application can be registered to handle messages per connector.
     * <p/>
     * Listening only commences once {@link #start()} is invoked.
     *
     * @param connector   the connector
     * @param application the receiving application
     * @param user        the user responsible for messages received the connector
     */
    @Override
    public void listen(Connector connector, ReceivingApplication application, User user) throws InterruptedException {
        if (connector instanceof MLLPReceiver) {
            int port = ((MLLPReceiver) connector).getPort();
            DemultiplexingReceiver receiver;
            synchronized (receivers) {
                receiver = receivers.get(port);
                if (receiver == null) {
                    receiver = new DemultiplexingReceiver(messageService, messageContext, port);
                    receivers.put(port, receiver);
                }
            }
            MessageReceiver messageReceiver = receiver.add(connector, application, user);
            receiverMap.put(connector.getReference(), messageReceiver);
        }
    }

    /**
     * Start listening for messages.
     */
    @Override
    public void start() {
        for (Map.Entry<Integer, DemultiplexingReceiver> entry : receivers.entrySet()) {
            DemultiplexingReceiver receiver = entry.getValue();
            if (!receiver.isRunning()) {
                receiver.start();
            }
            if (!receiver.isRunning()) {
                log.error("Failed to start listener for port=" + entry.getKey());
            }
        }
    }

    /**
     * Stops listening to messages from a connector.
     *
     * @param connector the connector
     */
    @Override
    public void stop(Connector connector) {
        if (connector instanceof MLLPReceiver) {
            int port = ((MLLPReceiver) connector).getPort();
            synchronized (receivers) {
                DemultiplexingReceiver receiver = receivers.get(port);
                if (receiver != null) {
                    synchronized (receiver) {
                        receiver.remove(connector);
                        receiverMap.remove(connector.getReference());
                        if (receiver.isEmpty()) {
                            receivers.remove(port);
                            receiver.stop();
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the statistics for a connector.
     *
     * @param connector the connector reference
     * @return the statistics, or {@code null} if the connector doesn't exist or is inactive
     */
    @Override
    public Statistics getStatistics(Reference connector) {
        Statistics statistics = getQueues().getStatistics(connector);
        if (statistics == null) {
            statistics = receiverMap.get(connector);
        }
        return statistics;
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p/>
     * This delegates to {@link #initialise}
     */
    @Override
    public void afterPropertiesSet() {
        initialise();
    }

    /**
     * Initialises the dispatcher.
     */
    public void initialise() {
        initialise(connectors);
        connectors.addListener(listener);

        log.info("HL7 message dispatcher initialised");
        schedule();
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     *
     * @throws Exception in case of shutdown errors
     */
    @Override
    public void destroy() throws Exception {
        connectors.removeListener(listener);
        super.destroy();
        synchronized (receivers) {
            for (DemultiplexingReceiver receiver : receivers.values()) {
                receiver.stop();
            }
        }
        messageContext.getExecutorService();
        // force creation of an ExecutorService if one doesn't exist, to avoid NPE by close() below
        messageContext.close();
    }

    /**
     * Returns the queues.
     *
     * @return the queues
     */
    @Override
    protected MessageQueues getQueues() {
        return (MessageQueues) super.getQueues();
    }

    /**
     * Creates a new timestamp for MSH-7.
     *
     * @return a new timestamp
     */
    protected Date createMessageTimestamp() {
        return new Date();
    }

    /**
     * Creates a new message control ID, for MSH-10.
     *
     * @return a new ID
     * @throws IOException if the ID cannot be generated
     */
    protected String createMessageControlID() throws IOException {
        return generator.getID();
    }

    /**
     * Initialise the connector queues.
     *
     * @param connectors the connectors
     */
    protected void initialise(Connectors connectors) {
        // initialise the queues
        for (Connector connector : connectors.getConnectors()) {
            if (connector instanceof MLLPSender) {
                getMessageQueue((MLLPSender) connector);
            }
        }
    }

    /**
     * Queues a message to a sender.
     *
     * @param message the message
     * @param sender  the sender
     * @return the queued message
     * @throws HL7Exception if the message cannot be encoded
     */
    protected DocumentAct queue(Message message, final MLLPSender sender) throws HL7Exception {
        DocumentAct result;
        MessageQueue queue = getMessageQueue(sender);
        if (log.isDebugEnabled()) {
            log.debug("queue() - " + sender);
        }
        result = queue.add(message);
        schedule();
        return result;
    }

    /**
     * Sends a message, and returns the response.
     *
     * @param message the message to send
     * @param sender  the sender configuration
     * @param queue   the message queue
     * @return the response
     * @throws HL7Exception if the connection can not be initialised for any reason
     * @throws LLPException for any LLP error
     * @throws IOException  for any I/O error
     */
    protected Message send(Message message, MLLPSender sender, MessageQueue queue)
            throws HL7Exception, LLPException, IOException {
        Message response;
        boolean debug = log.isDebugEnabled();
        long start = -1;
        if (debug) {
            log.debug("sending message via " + sender);
            log.debug(toString(message));
            start = System.currentTimeMillis();
        }
        Connection connection = null;
        try {
            connection = messageContext.newClient(sender.getHost(), sender.getPort(), false);
            int timeout = sender.getResponseTimeout();
            if (timeout <= 0) {
                timeout = MLLPSender.DEFAULT_RESPONSE_TIMEOUT;
            }
            connection.getInitiator().setTimeout(timeout, TimeUnit.SECONDS);
            response = connection.getInitiator().sendAndReceive(message);
            if (debug) {
                long end = System.currentTimeMillis();
                log.debug("response received in " + (end - start) + "ms");
                log.debug(toString(response));
            }
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return response;
    }

    /**
     * Processes an object.
     *
     * @param object the object to process
     * @param queue  the queue
     * @throws Exception for any error
     */
    @Override
    protected void process(Message object, MessageQueue queue) throws Exception {
        MLLPSender connector = queue.getConnector();
        Message response = send(object, connector, queue);
        queue.sent(response);
    }

    /**
     * Returns a string representation of an object.
     *
     * @param object the object
     * @return a string representation of the object
     */
    @Override
    protected String toString(Message object) {
        try {
            return HL7MessageHelper.toString(object);
        } catch (HL7Exception exception) {
            log.error(exception.getMessage(), exception);
            return "Failed to encode message";
        }
    }

    /**
     * Populates the header of a message.
     *
     * @param message the message
     * @param sender  the sender
     * @param config  the message population configuration
     * @throws HL7Exception for any error
     * @throws IOException  if a message control ID cannot be generated
     */
    private void populate(Message message, MLLPSender sender, HL7Mapping config) throws HL7Exception, IOException {
        populator.populate(message, sender, createMessageTimestamp(), createMessageControlID(), config);
    }

    /**
     * Returns a queue for a sender, creating one if none exists.
     *
     * @param sender the sender
     * @return the queue
     */
    private MessageQueue getMessageQueue(MLLPSender sender) {
        return getQueues().getQueue(sender, true);
    }

    /**
     * Invoked when a connector is updated.
     *
     * @param connector the connector
     */
    private void update(Connector connector) {
        if (connector instanceof MLLPSender) {
            restartSender(connector);
        } else if (connector instanceof MLLPReceiver) {
            restartReceiver(connector);
        }
    }

    /**
     * Invoked to restart a receiver.
     *
     * @param connector the connector to use
     */
    private void restartReceiver(Connector connector) {
        MessageReceiver receiver = receiverMap.get(connector.getReference());
        if (receiver != null) {
            ReceivingApplication app = receiver.getReceivingApplication();
            stop(receiver.getConnector()); // stop the existing instance
            try {
                listen(connector, app, receiver.getUser());
            } catch (InterruptedException exception) {
                log.error("Failed to update " + connector, exception);
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Invoked to restart a sender.
     *
     * @param connector the connector
     */
    private void restartSender(Connector connector) {
        MLLPSender sender = (MLLPSender) connector;
        MessageQueue queue = getQueues().getQueue(sender, false);
        if (queue != null) {
            log.info("Updating " + connector);
            queue.setConnector(sender);
            queue.setWaitUntil(-1);
            schedule();
        } else {
            getMessageQueue(sender);
        }
    }

    /**
     * Invoked when a connector is removed or de-activated.
     *
     * @param connector the connector
     */
    private void remove(Connector connector) {
        if (connector instanceof MLLPSender) {
            MessageQueue queue = getQueues().remove(connector.getReference());
            if (queue != null) {
                // Note that a call to queue() could re-add the queue, even if it is inactive.
                log.info("Removed queue for " + connector);
                queue.setSuspended(true);
            }
        } else if (connector instanceof MLLPReceiver) {
            stop(connector);
        }
    }

}
